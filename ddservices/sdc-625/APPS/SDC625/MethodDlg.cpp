/*************************************************************************************************
 *
 * $Workfile: MethodDlg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		MethodDlg.cpp : implementation file
 */

// MethodDlg.cpp : implementation file
#include "stdafx.h"
#include "SDC625.h"

#include "MethodDlg.h"
#include "logging.h"
#include "Dyn_Controls.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/*************************************************************************************************
 # Function Name  : StartMethodExecutionProc

 # Prototype      :  UINT StartMethodExecutionProc(LPVOID ddlMethod)


 # Description    :  This is the global thread function that is called to execute the method in 
										 a seperate thread.
                 
 # Parameters     :   LPVOID ddlMethod
*******************************************************************************************************************/ 
/*UINT StartMethodExecutionProc(LPVOID MethDlg)
{
	CMethodDlg* pMethodDlg = (CMethodDlg*)MethDlg;
	pMethodDlg->StartMethodExecution();
  return(0);
}
**/

LRESULT CMethodDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
//	DEBUGLOG(CLOG_LOG,"            CMethodDlg message 0x%04x W[0x%04x] L[0x%04x] (in 0x%x)\n", 
//												message,wParam, lParam, GetCurrentThreadId());
	return sdCDialog::WindowProc(message, wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CMethodDlg dialog

//CMethodDlg::CMethodDlg(hCmethodCall& MethCall,CString szMethodname,CString szHelp,CWnd* pParent /*=NULL*/)
//	: CDialog(CMethodDlg::IDD, pParent),m_MethCall(MethCall),m_szMethodName(szMethodname),m_szHelp(szHelp)
CMethodDlg::CMethodDlg(hCmethodCall& MethCall,CWnd* pParent /*=NULL*/)
	: sdCDialog(CMethodDlg::IDD, pParent)/*CDialog(CMethodDlg::IDD, pParent)*/,m_MethCall(MethCall)
{	
	// get method name
	wstring strLabel;   // Use wstring for UTF-8 support, POB - 2 Aug 2008
	strLabel.empty();
	if(m_MethCall.m_pMeth != NULL)
		m_MethCall.m_pMeth->Label(strLabel);
	m_szMethodName = strLabel.c_str();

	//get method help
	wstring strHelp;    // Use wstring for UTF-8 support, POB - 2 Aug 2008
	strHelp.empty();
	if(m_MethCall.m_pMeth != NULL)
		m_MethCall.m_pMeth->Help(strHelp);
	m_szHelp = strHelp.c_str();

	//{{AFX_DATA_INIT(CMethodDlg)
	//}}AFX_DATA_INIT

	m_hModRichEdit = LoadLibrary(_T("riched20.dll")); 

	//create event
	// stevev 27jul05 use an hCevent...m_hUserAckEvent = CreateEvent(NULL,TRUE,FALSE,NULL);	
	p_hCevtCntl = NULL;
	p_hCevtCntl = m_hCevent.getEventControl();
	m_pEnumCheckBox = NULL; // stevev 21may07

	//initialize flags
	m_bTextOverflow = FALSE;
	m_bTextDisplayOnly = FALSE;
	m_MethodAborted = FALSE;
	m_NextButtonSet = FALSE;
	m_bAbortBtnClicked = FALSE;
	m_bNextBtnClicked = FALSE;  //Added by Prashant for dynamic updation of variables in Method dialog
    m_bDialogVisible = FALSE;//prashant 230204
	m_dlgReady = FALSE;

    m_pScope = NULL;

	// other misc variables that need initialization - stevev 14sep09
	m_nCurrentComboSelection  = 0;
	m_nPreviousDisplayControl = 0;
}


void CMethodDlg::DoDataExchange(CDataExchange* pDX)
{
TRACE(_T(">>>> Dialog Data Exchange.(in 0x%x)\n"),GetCurrentThreadId());
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMethodDlg)
	DDX_Control(pDX, IDC_BTN_ABORT, m_ctlAbort);
	DDX_Control(pDX, IDC_ST_STATUS, m_ctlStat);
	DDX_Control(pDX, ID_OK, m_ctlNext);
	DDX_Control(pDX, IDC_BTN_HELP, m_ctlHelp);
	//}}AFX_DATA_MAP
}

CMethodDlg::~CMethodDlg()
{
TRACE(_T(">>>> Dialog Destruction.(in 0x%x)\n"),GetCurrentThreadId());
	FreeLibrary(m_hModRichEdit);

  // close the event handle . 
// stevev 27jul05  CloseHandle(m_hUserAckEvent);
  RAZE(p_hCevtCntl);
  RAZE(m_pEnumCheckBox);
}


BEGIN_MESSAGE_MAP(CMethodDlg, sdCDialog) //CDialog)
	//{{AFX_MSG_MAP(CMethodDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BTN_HELP, OnBtnHelp)
	ON_BN_CLICKED(ID_OK, OnBtnNext)
	ON_BN_CLICKED(IDC_BTN_ABORT, OnBtnAbort)
	ON_COMMAND(IDCANCEL, OnCancel)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_DRAWMETHODUI, OnDrawMethodUI)
	ON_MESSAGE(WM_GETMETHODUSERINPUT, OnGetMethodUserInput)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////





BOOL CMethodDlg::OnInitDialog() 
{
//stevev 14jun06	CDialog::OnInitDialog();
	sdCDialog::OnInitDialog();

	/*Initialize the dialog contols */

	//Create a thread and call the method execution function in that thread.
//		AfxBeginThread(StartMethodExecutionProc,this);


	// stevev 18mar09 - try to get a unicode font into this dialog 
	//	set the dialog font
	// stevev 10jul2017 - make the font a part of the method so it is not destroyed 
	//                    before the controls it is used in.
	LOGFONT LogFont;

	memset(&LogFont, 0x00, sizeof(LogFont));
	wcscpy(LogFont.lfFaceName, CONST_STRING_FONT);//in 'SDC625.h' 
	// assume CONST_STRING_HIGT is in points
	// lfHeight is in logical units, use the formula for points to logical units::>
	LogFont.lfHeight = -MulDiv(CONST_STRING_HIGT, GetDeviceCaps(GetDC()->m_hDC, LOGPIXELSY), 72);

	m_Font.CreateFontIndirect(&LogFont);
	
	SetFont(&m_Font);
	//	m_bIsFontCreated = TRUE;


	//CString strTemp = "HONEYWELL STT25H"; //now hard coded . later will have the device name
	SetCaption(m_szMethodName);
	SetUpInitialClientAreaRect();
TRACE(_T(">> Dialog Init Msg @ 0x%08p '%s'\n"),this,m_szMethodName);
	//ShowWindow(SW_HIDE);
  m_bDialogVisible = FALSE;

  
	CMainFrame* m_pMainFrm  = (CMainFrame*)(AfxGetApp()->m_pMainWnd);
	if (m_pMainFrm)
	{
		m_pMainFrm->RegisterDialog(this);
	}

  //prashant 230204
  //commented by prashant 3rd mar 04
  /*CRect DialogRect;
  GetWindowRect(&m_InitialDialogRect);
  int DialogWidth = m_InitialDialogRect.Width();
  int DialogHeight = m_InitialDialogRect.Height();
  MoveWindow(0-DialogWidth, 0-DialogHeight, DialogWidth, DialogHeight);*/
  //commented by prashant 3rd mar 04 end
  //prashant 230204 end

	//temp code used for testing
/*			nNextCount = 0;
			m_structUIData.bUserAcknowledge = TRUE;
			m_structUIData.userInterfaceDataType = TEXT_MESSAGE;
			m_structUIData.textMessage.pchTextMessage = "A rich edit control is a window in which the user can enter and edit text. The text can be assigned character and paragraph formatting, and can include embedded OLE objects. Rich edit controls provide a programming interface for formatting text. However, an application must implement any user interface components necessary to make formatting operations available to the user.The CRichEditCtrl class provides the functionality of the rich edit control.This Windows Common control (and therefore the CRichEditCtrl class) is available only to programs running under Windows 95/98 and Windows NT versions 3.51 and later. The CRichEditCtrl class supports versions 2.0 and 3.0 of the Platform SDK rich edit control.abcdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxefg";
			SendMessage(WM_DRAWMETHODUI,(WPARAM)&m_structUIData,0);
*/
	//temp end
  m_dlgReady = TRUE;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


/****************************************************************************************************************
 # Function Name  : SetCaption

 # Prototype      :  void CMethodDlg::SetCaption(CString szDeviceName)

 # Description    :  Set the caption of the methods dialog
                 
 # Parameters     :   String szDeviceName
*******************************************************************************************************************/ 
void CMethodDlg::SetCaption(CString szName)
{
	SetWindowText(szName);
}

/****************************************************************************************************************
 # Function Name  : StartMethodExecution

 # Prototype      :  BOOL CMethodDlg :: StartMethodExecution()

 # Description    :  This is function is called from IntiDialog() ,in a seperate thread.
											Here a call is made to the device object function for method execution. 
                 
 # Parameters     :   none
*******************************************************************************************************************
BOOL CMethodDlg :: StartMethodExecution()
{
	CValueVarient retVal;
	ACTION_UI_DATA structMethodsUIData;
	ACTION_USER_INPUT_DATA structUserInputData;

	RETURNCODE rc =	m_MethCall.m_pMeth->selfExecute(m_MethCall,retVal);

		//close the methods dialog after the method execution is over
	SendMessage(WM_CLOSE);

	CString strStatusMsg;
	if(rc == FAILURE)
		{
			//m_ctlStat.SetWindowText("Method Execution Failed");
			LOGIT(CERR_LOG,"Method execution failed.selfExecute() function has returned an error.\n");
			return FALSE;
		}

  return TRUE;
}

***/

/****************************************************************************************************************
 # Function Name  : OnDrawMethodUI

 # Prototype      :  LRESULT CMethodDlg::OnDrawMethodUI(WPARAM wParam, LPARAM) 

 # Description    :  Message handler for the user defined message WM_DRAWMETHODUI to update the methods
											dialog with the infromation got from builtins.
                 
 # Parameters     :   WPARAM wParam, LPARAM
*******************************************************************************************************************/ 
LRESULT CMethodDlg::OnDrawMethodUI(WPARAM wParam, LPARAM) 
{

    CFont* pFont = GetFont(); //Get the font from the MethodDlg which is UTF-8 font, POB - 2 Aug 2008
#ifdef _DEBUG
   LOGFONT lf;
   pFont->GetLogFont(&lf);
	DEBUGLOG(CLOG_LOG,L">>XX Dialog DrawMethod  Entry (Infc %d), %s got Font with name of %s (THRD 0x%x)\n", 
		(int)m_structUIData.userInterfaceDataType,(m_MethodAborted)?L"Aborted":L"",
		lf.lfFaceName,GetCurrentThreadId());
#endif


	/*<START>Added by Prashant 20FEB2004	*/
	// bug 2236  if(m_structUIData.bDisplayDynamic)/* NOTE:stevev 26jul05 - the ONLY read of bDisplayDynamic */
	if(((ACTION_UI_DATA*)wParam)->bDisplayDynamic)
	{
		ACTION_UI_DATA dynamicData = *(ACTION_UI_DATA*)wParam; 	// added bug 2236 & changed the following accesses
		DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Dynamic. (current InfcType %d)\n", 
													(int)dynamicData.userInterfaceDataType);	
		//switch(m_structUIData.userInterfaceDataType)// changed bug 2236
		switch(dynamicData.userInterfaceDataType)
		{
		case UNKNOWN_UI_DATA_TYPE:
			{
				  break;
			}
		case TEXT_MESSAGE:
		case EDIT:
		case COMBO:
		case HARTDATE:
		case TIME:
			{
				m_RichEditDisplay.SetWindowText(dynamicData.textMessage.pchTextMessage);
				break;
			}
		}
		return TRUE;
		/*<END>Added by Prashant 20FEB2004	*/
	}
	
	m_structUIData = *(ACTION_UI_DATA*)wParam;// moved this to after the dynamic handling so  
											  // we don't change the true interface type. bug 2236
	
	int  nDisplayRichEditStyle;
	bool isRO = false;
	DWORD editBoxStyle   = WS_CHILD| WS_VISIBLE | ES_AUTOHSCROLL;// all editboxes

	if ( m_structUIData.pVar4ItemID != NULL && m_structUIData.pVar4ItemID->IsReadOnly() )
	{
		editBoxStyle |= ES_READONLY;
		isRO = true;// in case we're working on a combobox
	}
	if (EDIT_BOX_TYPE_PASSWORD == m_structUIData.EditBox.editBoxType)
	{
		editBoxStyle |= ES_PASSWORD;
	}

	//remove controls from previous display. This is called because the controls are generated each time
	//as per the UI data info got from the builtins.
	DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Clear dialog.(0x%x)\n",GetCurrentThreadId());	
	RemovePreviousControls();

	if(m_structUIData.bUserAcknowledge)
		{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod IS User Ack.(clear buttonclicks)\n");	
			m_bNextBtnClicked = FALSE;
			m_ctlNext.EnableWindow(TRUE);
			m_bAbortBtnClicked = FALSE;
			m_ctlAbort.EnableWindow(TRUE);
		}
	else
		{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Not User Ack (Delay).\n");	
			m_ctlNext.EnableWindow(FALSE);
			m_ctlAbort.EnableWindow(FALSE);
			/*Vibhor 030304: Start of Code*/
			if(m_structUIData.bEnableAbortOnly) 
			{// At this point we know that we'll come here thru "delay" or "DELAY" builtin only!!!
				m_bAbortBtnClicked = FALSE;
				m_ctlAbort.EnableWindow(TRUE);
			}
			/*Vibhor 030304: End of Code*/
		}

	//display the appropriate message
	CString strStatusMsg;
	if(m_MethodAborted)
		{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Aborted - disable.\n");	
			m_ctlAbort.EnableWindow(FALSE);
			if(!m_structUIData.bUserAcknowledge)
			{
				strStatusMsg.LoadString(IDS_METH_ABORTING); ////method aborting 
			}
			else
			{
				strStatusMsg.LoadString(IDS_METH_ABORTING_OK);
			}
			m_ctlStat.SetWindowText(strStatusMsg); ////method aborting . press OK
		}
	else if(!m_structUIData.bUserAcknowledge)
		{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod In Progress.\n");	
			strStatusMsg.LoadString(IDS_METHOD_IN_PROGRESS); //method execution in progress
			m_ctlStat.SetWindowText(strStatusMsg);
		}
	else
		{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Set OK/ok-abort.\n");	
			if( TRUE  == m_structUIData.bMethodAbortedSignalToUI)
			{
				strStatusMsg.LoadString(IDS_METHOD_PRESS_OK);//press OK
			}
			else
			{
				strStatusMsg.LoadString(IDS_METHOD_STATUS_NEXT);//press abort or OK
			}
			m_ctlStat.SetWindowText(strStatusMsg);
		}

	if(TRUE == m_structUIData.bMethodAbortedSignalToUI )
	{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod abort-disable Window.\n");	
		m_ctlAbort.EnableWindow(FALSE);
	}

	switch(m_structUIData.userInterfaceDataType)
		{
		case UNKNOWN_UI_DATA_TYPE:
			{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Unknown Type.\n");	
				break;
			}
		case TEXT_MESSAGE:
			{
DEBUGLOG(CLOG_LOG,L">>XX Dialog DrawMethod Text.|%s|\n",m_structUIData.textMessage.pchTextMessage);	
				m_nPreviousDisplayControl= TEXT_MESSAGE;
				m_bTextDisplayOnly = TRUE;
				m_DisplayRichEditRect.SetRectEmpty();
				SetUpDisplayRichEditRect(m_structUIData.textMessage.pchTextMessage);

				if(m_bTextOverflow)
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE_OVERFLOW;
				}
				else
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE;
				}

				int ret = m_RichEditDisplay.CWnd::Create(_T("RichEdit20W"), NULL, nDisplayRichEditStyle,
											m_DisplayRichEditRect, this, ID_DISPLAY_RICHEDIT_CUSTOM_CTRL);
		
				if(!ret)
				{
					LOGIT(CERR_LOG,"Could not create control to display text\n");
					return FALSE;
				}

                // stevev 18mar09 - try to set the control
				// note that the RichEdit20W doesn't update the control,
				//									so the you cannot work with the charformat

                m_RichEditDisplay.SetFont(pFont); // Set the parent's Font for the control, POB - 2 Aug 2008

				//set the background color the same as that of the dialog box
				COLORREF BackColor = GetSysColor(COLOR_3DFACE); 
				m_RichEditDisplay.SetBackgroundColor(FALSE,BackColor);
				m_RichEditDisplay.SetWindowText(m_structUIData.textMessage.pchTextMessage);
				break;
			}
		case EDIT:
			{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Edit.\n");	
				m_nPreviousDisplayControl= EDIT;
				m_bTextDisplayOnly = FALSE;

				m_DisplayRichEditRect.SetRectEmpty();
				SetUpDisplayRichEditRect(m_structUIData.textMessage.pchTextMessage);
				SetUpEditBoxRect();

				if(m_bTextOverflow)
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE_OVERFLOW;
				}
				else
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE;
				}

				//We need to call create for RICHEDIT20W instead of the CRichEditCtrl create 
				//	  because we need to be able to support display of localized (eg WIDE) text.
				if(!m_RichEditDisplay.CWnd::Create(_T("RichEdit20W"), NULL, nDisplayRichEditStyle, 
								m_DisplayRichEditRect, this, ID_DISPLAY_RICHEDIT_CUSTOM_CTRL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display text that is"
											" to be shown along with edit value\n");
					return FALSE ;
				}

               
                m_RichEditDisplay.SetFont(pFont); 
				// Set the parent's Font for the control, POB - 2 Aug 2008

				//set the background color the same as that of the dialog box
				COLORREF BackColor = GetSysColor(COLOR_3DFACE); 
				m_RichEditDisplay.SetBackgroundColor(FALSE,BackColor);
				m_RichEditDisplay.SetWindowText(m_structUIData.textMessage.pchTextMessage);

/* this is done more elegantly below - stevev 30nov11
				if(EDIT_BOX_TYPE_PASSWORD == m_structUIData.EditBox.editBoxType)
				{
					if(!m_EditBox.CreateEx(WS_EX_CLIENTEDGE, _T("Edit"), NULL, WS_CHILD| WS_VISIBLE | ES_AUTOHSCROLL|ES_PASSWORD,m_EditCtrlRect, this, ID_EDIT_CUSTOM_CTRL, NULL))
					{
						LOGIT(CERR_LOG,"Could not create control to display edit value\n");
						return FALSE ;
					}
				}
				else
				{
					if(!m_EditBox.CreateEx(WS_EX_CLIENTEDGE, _T("Edit"), NULL, WS_CHILD| WS_VISIBLE | ES_AUTOHSCROLL ,m_EditCtrlRect, this, ID_EDIT_CUSTOM_CTRL, NULL))
					{
						LOGIT(CERR_LOG,"Could not create control to display edit value\n");
						return FALSE ;
					}
				}
				********************/
				if(!m_EditBox.CreateEx(WS_EX_CLIENTEDGE, _T("Edit"), NULL, editBoxStyle,
									   m_EditCtrlRect, this, ID_EDIT_CUSTOM_CTRL, NULL)    )
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display edit value\n");
					return FALSE ;
				}				

				SetUpEditBox();
				m_EditBox.SetFocus();
				break;
			}
			case COMBO :
			{
				DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Combo.\n");	
				m_nPreviousDisplayControl= COMBO;
				m_bTextDisplayOnly = FALSE;

				m_DisplayRichEditRect.SetRectEmpty();
				SetUpDisplayRichEditRect(m_structUIData.textMessage.pchTextMessage);
				// text string is exactly the same on both types
				if(m_bTextOverflow)
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE_OVERFLOW;
				}
				else
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE;
				}

				//We need to call create for RICHEDIT20W instead of the CRichEditCtrl create 
				//          beacuse we need to be able to support display of localized text.
				if(!m_RichEditDisplay.CWnd::Create(_T("RichEdit20W"), NULL,
					nDisplayRichEditStyle, m_DisplayRichEditRect, this, 
					ID_DISPLAY_RICHEDIT_CUSTOM_CTRL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display text that is "
												"to be shown along with selection value\n");
					return FALSE ;
				}
				COLORREF BackColor = GetSysColor(COLOR_3DFACE); 
				m_RichEditDisplay.SetBackgroundColor(FALSE,BackColor);

				m_RichEditDisplay.SetWindowText(m_structUIData.textMessage.pchTextMessage);


				m_nCurrentComboSelection = m_structUIData.ComboBox.nCurrentIndex;

				CreateComboList();// same process for both types

				if (m_structUIData.ComboBox.comboBoxType == COMBO_BOX_TYPE_MULTI_SELECT)
				{// combo check box					
					CArray<EnumTriad_t,EnumTriad_t&> subDataList;
					int valLen  = 0;
					ulong value = m_nCurrentComboSelection;
					EnumTriad_t localTriad;

					/* stevev 22may07 - implement check box -start-*/
					// process list
					// find the longest selection and set up the combo box length appropriately
					POSITION pos;
					CString strLongestString,strTemp;
					pos = m_ComboElementsList.GetHeadPosition();
					ASSERT(m_ComboElementsList.GetCount() == m_ComboElementValueList.GetSize());
					for(int i=0;i<m_ComboElementsList.GetCount();i++)
					{
						strTemp = m_ComboElementsList.GetNext(pos);
						if((strTemp.GetLength()) >(strLongestString.GetLength()))
						{
							strLongestString = strTemp;
						}
						localTriad.descS = (LPCTSTR)strTemp;
						localTriad.val   = m_ComboElementValueList[i];
						subDataList.Add(localTriad);
						localTriad.clear();
					}
					// Make sure to set the OnKillFocus to False making it visible all the time
					m_pEnumCheckBox = 
								new CCheckComboBox(1,1, &subDataList, value, valLen, FALSE);

					SetUpComboBoxRect(m_ComboElementsList.GetCount(),strLongestString); 
					
					DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_VSCROLL;// | CBS_DISABLENOSCROLL;
					dwStyle      |= (CBS_OWNERDRAWVARIABLE | CBS_HASSTRINGS);	

					if ( isRO )
					{
						dwStyle |= WS_DISABLED   ;// leave it w/o a dropdownlist
					}
					else
					{
						dwStyle |= CBS_DROPDOWNLIST   ;// give it a dropdown
					}

					m_pEnumCheckBox->Create( dwStyle, m_ComboCtrlRect, this, 1);
					m_pEnumCheckBox->SetValue(m_nCurrentComboSelection);
					m_pEnumCheckBox->ShowWindow(SW_SHOW);

				}
				else
				{// regular combo

					// we already did this....CreateComboList();

					//find the longest combo selection so as to set up the combo box length appropriately
					POSITION pos;
					CString strLongestString,strTemp;
					pos = m_ComboElementsList.GetHeadPosition();
					int i;	// PAW 03/03/09
					for(/*int PAW 03/03/09 */i=0;i<m_ComboElementsList.GetCount();i++)
					{
						strTemp = m_ComboElementsList.GetNext(pos);
						if((strTemp.GetLength()) >(strLongestString.GetLength()))
						{
							strLongestString = strTemp;
						}
					}

					SetUpComboBoxRect(m_ComboElementsList.GetCount(),strLongestString); 
					
					DWORD dwStyle =  WS_CHILD | WS_VISIBLE | WS_VSCROLL ;//| CBS_DISABLENOSCROLL;

					if ( isRO )
					{
						dwStyle |= WS_DISABLED   ;// leave it w/o a dropdownlist
					}
					else
					{
						dwStyle |= CBS_DROPDOWNLIST   ;// give it a dropdown
					}


					if(!m_ComboBox.CreateEx(WS_EX_CLIENTEDGE, _T("Combobox"), NULL, dwStyle,
										    m_ComboCtrlRect, this, ID_COMBO_CUSTOM_CTRL, NULL))
					{
						LOGIT(CERR_LOG,"Could not create control to display selection value\n");
						return FALSE ;
					}
					m_ComboBox.SetFont(&m_Font);
					//if ( ! isRO )
					//{
						//fill combo box with choices
						pos = m_ComboElementsList.GetHeadPosition();
						for(i=0;i<m_ComboElementsList.GetCount();i++)
						{
							CString strComboItem;
							strComboItem = m_ComboElementsList.GetNext(pos);
							strComboItem.TrimLeft(szTRIM_CHARS);
							strComboItem.TrimRight(szTRIM_CHARS);
							m_ComboBox.AddString(strComboItem);
						}
					//}
					//	m_ComboBox.SetCurSel(-1); //Anil commented October 25 2005 for fixing the PAR 5436
					m_ComboBox.SetCurSel(m_structUIData.ComboBox.nCurrentIndex);//Added By Anil October 25 2005 for fixing the PAR 5436			
					m_ComboBox.SetFocus();
					m_ComboBox.ShowDropDown();
				}// end of regular combo
			}
			break;
		case HARTDATE:
			{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Date.\n");	

				m_nPreviousDisplayControl= HARTDATE;
				m_bTextDisplayOnly = FALSE;

				m_DisplayRichEditRect.SetRectEmpty();// stevev added 7oct08
				SetUpDisplayRichEditRect(m_structUIData.textMessage.pchTextMessage);
				// stevev 7oct08 - change to edit box .. SetUpDateTimeRect();
				SetUpEditBoxRect();

				if(m_bTextOverflow)
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE_OVERFLOW;
				}
				else
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE;
				}

				//We need to call create for RICHEDIT20W instead of the CRichEditCtrl create 
				//              beacuse we need to be able to support display of localized text.
				if(!m_RichEditDisplay.CWnd::Create(_T("RichEdit20W"), NULL, nDisplayRichEditStyle, m_DisplayRichEditRect, this, ID_DISPLAY_RICHEDIT_CUSTOM_CTRL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display text that is to be shown along with edit value\n");
					return FALSE ;
				}
				//set the background color the same as that of the dialog box
				COLORREF BackColor = GetSysColor(COLOR_3DFACE); 
				m_RichEditDisplay.SetBackgroundColor(FALSE,BackColor);
				m_RichEditDisplay.SetWindowText(m_structUIData.textMessage.pchTextMessage);


/*				if(!m_DateTimeCtrl.Create(WS_CHILD| WS_VISIBLE|DTS_SHORTDATEFORMAT|DTS_UPDOWN, m_DateTimeRect, this, ID_DATETIME_CUSTOM_CTRL))
				{
					LOGIT(CERR_LOG,"Could not create control to display date.\n");
					return FALSE;
				}

				m_DateTimeCtrl.SetFormat(szHARTDATE);
				GetDateFromString(m_structUIData.datetime.pchHartDate);
//				BOOL bRet = m_DateTimeCtrl.SetTime(m_OleDateTime);
*/
				DWORD dwStyle =  WS_CHILD| WS_VISIBLE | ES_AUTOHSCROLL ;

				if ( isRO )
				{
					dwStyle |= WS_DISABLED   ;// leave it w/o a dropdownlist
				}
				//else no-op 
   
				if(!m_EditBox.CreateEx(WS_EX_CLIENTEDGE, _T("Edit"), NULL, dwStyle ,
										m_EditCtrlRect, this, ID_EDIT_CUSTOM_CTRL, NULL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display edit value\n");
					return FALSE ;
				}

				SetUpEditBox();
				m_EditBox.SetFocus();

				break;
			}	
		case TIME:
			{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Time.\n");	
				m_nPreviousDisplayControl= TIME;
				m_bTextDisplayOnly = FALSE;

				m_DisplayRichEditRect.SetRectEmpty();
				//SetUpDisplayRichEditRect(m_structUIData.datetime.pchHartDate);
				SetUpDisplayRichEditRect(m_structUIData.textMessage.pchTextMessage);
				SetUpEditBoxRect();

				if(m_bTextOverflow)
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE_OVERFLOW;
				}
				else
				{
					nDisplayRichEditStyle = DISPLAY_RICHEDIT_STYLE;
				}

				//We need to call create for RICHEDIT20W instead of the CRichEditCtrl create 
				//            because we need to be able to support display of localized text.
				if(!m_RichEditDisplay.CWnd::Create(_T("RichEdit20W"), NULL, nDisplayRichEditStyle, m_DisplayRichEditRect, this, ID_DISPLAY_RICHEDIT_CUSTOM_CTRL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display text that is to be shown along with edit value\n");
					return FALSE ;
				}

				//set the background color the same as that of the dialog box
				COLORREF BackColor = GetSysColor(COLOR_3DFACE); 
				m_RichEditDisplay.SetBackgroundColor(FALSE,BackColor);
				//m_RichEditDisplay.SetWindowText(m_structUIData.datetime.pchHartDate);
				m_RichEditDisplay.SetWindowText(m_structUIData.textMessage.pchTextMessage);
			
			
				DWORD dwStyle =  WS_CHILD| WS_VISIBLE | ES_AUTOHSCROLL ;

				if ( isRO )
				{
					dwStyle |= WS_DISABLED   ;// leave it w/o a dropdownlist
				}
				//else no-op 
   
				if(!m_EditBox.CreateEx(WS_EX_CLIENTEDGE, _T("Edit"), NULL, dwStyle,
										m_EditCtrlRect, this, ID_EDIT_CUSTOM_CTRL, NULL))
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Could not create control to display edit value\n");
					return FALSE ;
				}

				SetUpEditBox();
				m_EditBox.SetFocus();
			}
			break;
		case VARIABLES_CHANGED_MSG:
			{
DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Changed.\n");	
				//display the message indicating that some variable values have changed during method execution and the 
				//user has to commit or cancel these changes once he exits the methods dialog.
				CString strMsg;
				strMsg.LoadString(IDS_VAR_VALS_CHANGED);
				AfxMessageBox(strMsg);
			}
			break;

			// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version

	}

DEBUGLOG(CLOG_LOG,">>XX Dialog DrawMethod Exit.(in 0x%x)(current InfcType %d)\n",GetCurrentThreadId(),
	  (int)m_structUIData.userInterfaceDataType);	
return TRUE;
}

/****************************************************************************************************************
 # Function Name  : ShowMethodsData

 # Prototype      :  BOOL ShowMethodsData(ACTION_UI_DATA structUIData)

 # Description    :  This function sends the user defined message WM_DRAWMETHODUI to update the 
											methods dialog
			
 # Parameters     :   ACTION_UI_DATA structUIData -  This structure contains all information about the 
																											data to be dispalyed.
*******************************************************************************************************************/ 
BOOL CMethodDlg :: ShowMethodsData(ACTION_UI_DATA structUIData)
{
	//reset the abort button clicked flag to FALSE
//	m_bAbortBtnClicked = FALSE;
  

TRACE(_T(">x>x Dialog sending Show Methods Data(from 0x%x)\n"),GetCurrentThreadId());
	SendMessage(WM_DRAWMETHODUI,(WPARAM)&structUIData,0);
TRACE(_T(">x>x Dialog finished sending Show Methods Data .\n"));

	return TRUE;
}


/****************************************************************************************************************
 # Function Name  : GetMethodsUserInputData

 # Prototype      :  LRESULT CMethodDlg::OnGetMethodUserInput(WPARAM wParam, LPARAM) 

 # Description    :  Message handler for the user defined message WM_GETMETHODUSERINPUT to get the 
											data entered by the user.
			
 # Parameters     :   WPARAM wParam, LPARAM.
*******************************************************************************************************************/ 

LRESULT CMethodDlg::OnGetMethodUserInput(WPARAM wParam, LPARAM) 
{
TRACE(_T(">>>> Dialog getting User Input. (current InfcType %d)\n"), (int)m_structUIData.userInterfaceDataType);
	m_structUserInput = *(ACTION_USER_INPUT_DATA*)wParam;

	switch(m_structUIData.userInterfaceDataType)
		{
		case UNKNOWN_UI_DATA_TYPE:
			{
				break;
			}
		case EDIT:
			{
				m_structUserInput.userInterfaceDataType = EDIT;
				m_structUserInput.EditBox.editBoxType = m_structUIData.EditBox.editBoxType;

				switch(m_structUIData.EditBox.editBoxType)
				{
				case EDIT_BOX_TYPE_INTEGER:
				case EDIT_BOX_TYPE_FLOAT:
					{	
						m_structUserInput.EditBox.editBoxValue = m_varUserEditValue;
						break;
					}
				case EDIT_BOX_TYPE_DATE:
				case EDIT_BOX_TYPE_TIME:
				case EDIT_BOX_TYPE_STRING:
				case EDIT_BOX_TYPE_PASSWORD:
					{
/* let the varient deal with these details---
						CString strValue;
						strValue = m_varUserEditValue.vh_Value.pStrVal;
						delete[] m_varUserEditValue.vh_Value.pStrVal;
						m_varUserEditValue.vh_Value.pStrVal = NULL;
// stevev - WIDE2NARROW char interface
					int nLength = strValue.GetLength()+1;
					m_structUserInput.EditBox.pchDefaultValue = new char[nLength];
					// possible memory leak, I don't see where this is destroyed
#ifdef _UNICODE
					UnicodeToASCII(strValue, m_structUserInput.EditBox.pchDefaultValue, nLength);
#else
					strncpy(m_structUserInput.EditBox.pchDefaultValue,strValue,nLength);
#endif
						m_structUserInput.EditBox.iDefaultStringLength = nLength;
-------------*/
						m_structUserInput.EditBox.editBoxValue = m_varUserEditValue;
						break;
					}
				case UNKNOWN_EDIT_BOX_TYPE:
					{
						break;
					}	
				}
					
				break;
			}
		case COMBO:
			{
				m_structUserInput.userInterfaceDataType   = COMBO;
				m_structUserInput.ComboBox.comboBoxType   = m_structUIData.ComboBox.comboBoxType;
				if (m_structUIData.ComboBox.comboBoxType == COMBO_BOX_TYPE_MULTI_SELECT)
				{
					if (m_pEnumCheckBox)
					{
						m_structUserInput.ComboBox.nCurrentIndex =
						m_structUserInput.nComboSelection        = 
						m_nCurrentComboSelection                 = m_pEnumCheckBox->GetSelectedValue();
					}
					else
					{// error
					}
				}
				else
				{
					m_structUserInput.nComboSelection = m_nCurrentComboSelection + 1;
				}

				break;
			}
		case HARTDATE:
			{
				m_structUserInput.userInterfaceDataType   = 
				   m_structUIData.userInterfaceDataType;
/* let the varient deal with all this ---
				CString strHartDate;
				//COleDateTime tmpOleDateTime;
				strHartDate = m_varUserEditValue.vh_Value.pStrVal;
				delete[] m_varUserEditValue.vh_Value.pStrVal;
				m_varUserEditValue.vh_Value.pStrVal = NULL;
				m_structUserInput.userInterfaceDataType = HARTDATE;

				int nLength = strHartDate.GetLength()+1;	
				m_structUserInput.datetime.pchHartDate = new wchar_t[nLength];
				memcpy(m_structUserInput.datetime.pchHartDate,(LPCTSTR) strHartDate,nLength*sizeof(wchar_t));
----------------*/
				m_structUserInput.EditBox.editBoxValue = m_varUserEditValue;
				break;
			}
		case TIME:
			{
				m_structUserInput.userInterfaceDataType   = 
				   m_structUIData.userInterfaceDataType;
/* this is another varient task ----
				CString strTime;
				strTime = m_varUserEditValue.vh_Value.pStrVal;
				delete[] m_varUserEditValue.vh_Value.pStrVal;
				m_varUserEditValue.vh_Value.pStrVal = NULL;
				m_structUserInput.userInterfaceDataType = TIME;
				int nLength = strTime.GetLength()+1;// stated to be counted as 2 per widechar..NOT

				m_structUserInput.datetime.pchHartDate = new wchar_t[nLength];
					// possible memory leak, I don't see where this is destroyed
#ifdef _UNICODE
				// assume its already in long format..  UnicodeToASCII(strTime, m_structUserInput.datetime.pchHartDate, nLength);
				memcpy(m_structUserInput.datetime.pchHartDate,(LPCTSTR) strTime,nLength*sizeof(wchar_t));
#else
				strncpy(m_structUserInput.datetime.pchHartDate,strTime,nLength);
#endif
----------------*/
				m_structUserInput.EditBox.editBoxValue = m_varUserEditValue;
				break;
			}
		}

	*(ACTION_USER_INPUT_DATA*)wParam = m_structUserInput;
TRACE(_T(">>>> Dialog finished getting User Input .\n"));

	return TRUE;

}


/****************************************************************************************************************
 # Function Name  : GetMethodsUserInputData

 # Prototype      :  BOOL CMethodDlg :: GetMethodsUserInputData(ACTION_USER_INPUT_DATA& structUserInput)


 # Description    :  This function sends the user defined message WM_GETMETHODUSERINPUT to update the 
											methods dialog
			
 # Parameters     :   ACTION_USER_INPUT_DATA& structUserInput -  This structure contains
											the data given by the user and which has to be returned to the builtins..
*******************************************************************************************************************/ 

BOOL CMethodDlg :: GetMethodsUserInputData(ACTION_USER_INPUT_DATA& structUserInput)
{
TRACE(_T(">>>> Dialog sending User Input Data .\n"));
  SendMessage(WM_GETMETHODUSERINPUT,(WPARAM)&structUserInput,0);
TRACE(_T(">>>> Dialog finished sending User Input Data .\n"));
	return TRUE;


}
/****************************************************************************************************************
 # Function Name  : SetUpDisplayRichEditRect

 # Prototype      :  void SetUpDisplayRichEditRect(CString strString)

 # Description    :  This function sets up the rich edit control with the appropriate size.The message 
											got from the builtins is displayed in this control. 
											
			
 # Parameters     :   CString strString.
*******************************************************************************************************************/ 

void CMethodDlg::SetUpDisplayRichEditRect(CString strString)
{
  CClientDC dc(this);

  //remove all trailing spaces ,tabs and new line characters
  strString.TrimRight();

  // find the optimum length and breadth of the of the rectangle taking \n into consideration
  CString strTemp;
  strTemp = strString;

  int nLength = strString.GetLength();

  int nStart=0;
  int nLineCount = 0;
  int nCount = 0;
  int nMaxLength = 0;
  int nTempLength = 0;
  int nPrevStart=0;
  int nAddToLineCount = 0;
  CSize size;
  CSize size1;

  //find the number of lines initially by looking for \n character
  while(nStart!= -1)
  {
    nStart = strTemp.Find(L'\n',nStart);
    if(nStart>=0)
    {
      nStart++;
    }
    nLineCount++;
  }

  //find the longest line
  nStart = 0;

  for(int i=0;i<nLineCount;i++)
  {
    nStart = strTemp.Find(L'\n',nStart);

    if(nStart != -1)
    {
      //if a line is longer than the user area add 1 to the line count i.e. add a new line
      size = dc.GetOutputTextExtent(strString,(nStart - nTempLength));
/*Vibhor 250105: Start of Code Modifications*/
/*Fix for SDC PAR #5484*/
/*Since the Rich Edit Control is relative to the UserClient Rect real estate, to get the exact
 length of Rich Edit control, we have to substract both the offsets of the control (right & left)
 & the UserClient Rect's left offset from the User Client Rect's Right offset*/
      if(size.cx > (m_UserClientRect.right - RICHEDIT_CTRL_RIGHT_OFFSET - RICHEDIT_CTRL_LEFT_OFFSET - USER_CLIENT_LEFT_OFFSET))
      {
        nAddToLineCount = size.cx/(m_UserClientRect.right - RICHEDIT_CTRL_RIGHT_OFFSET - RICHEDIT_CTRL_LEFT_OFFSET - USER_CLIENT_LEFT_OFFSET);
        nCount += nAddToLineCount;
      }
      if((nStart - nTempLength) > nMaxLength )
      {
        nMaxLength = nStart - nTempLength;
      }
      nTempLength = nStart;
      nStart++;
    }
    else  // no \n character is found
    {
      //if the last portion of the line is longer than the user area add 1 to the line count i.e. add a new line
      size = dc.GetOutputTextExtent(strString,(nLength - nPrevStart));
      if(size.cx > (m_UserClientRect.right - RICHEDIT_CTRL_RIGHT_OFFSET - RICHEDIT_CTRL_LEFT_OFFSET - USER_CLIENT_LEFT_OFFSET))
      {
        nAddToLineCount = size.cx/(m_UserClientRect.right - RICHEDIT_CTRL_RIGHT_OFFSET - RICHEDIT_CTRL_LEFT_OFFSET - USER_CLIENT_LEFT_OFFSET);
        nCount += nAddToLineCount;
				nCount++;
      }
/*Vibhor 250105: End of Code Modifications*/
      if((nLength - nTempLength)> nMaxLength)
      {
        nMaxLength = nLength - nTempLength;
      }
    }
    nPrevStart = nStart;
  }
  nLineCount += nCount;

  size1 = dc.GetOutputTextExtent(strString,nMaxLength);

  int x1,y1,x2,y2;


  x1 = m_UserClientRect.left + RICHEDIT_CTRL_LEFT_OFFSET; //left
  y1 = m_UserClientRect.top + RICHEDIT_CTRL_TOP_OFFSET;   //top
  x2 = m_UserClientRect.right - RICHEDIT_CTRL_RIGHT_OFFSET;  //right
  y2 = m_UserClientRect.top + ((size1.cy )* nLineCount) + RICHEDIT_CTRL_BOTTOM_OFFSET; //bottom


  if(y2 > (m_UserClientRect.bottom)) //if text overflow has occured
  {
    m_bTextOverflow = TRUE;
    if(!m_bTextDisplayOnly) //if text is being displayed with other controls
    {
      y2 = m_UserClientRect.bottom - RICHEDIT_CTRL_OVERFLOW_BOTTOM_OFFSET;
    }
    else //if only text is being displayed
    {
      y2 = m_UserClientRect.bottom - RICHEDIT_CTRL_OVERFLOW_TEXTONLY_BOTTOM_OFFSET;
    }
  }
  else
  {
    y1 = m_UserClientRect.top + RICHEDIT_CTRL_TOP_OFFSET_FOR_NOOVERFLOW;   //top
    m_bTextOverflow = FALSE;
  }



  m_DisplayRichEditRect .SetRect(x1,y1,x2,y2);
  m_DisplayRichEditRect.NormalizeRect();

}


/****************************************************************************************************************
 # Function Name  : SetUpInitialClientAreaRect

 # Prototype      :  void SetUpInitialClientAreaRect()

 # Description    :  This function sets up the client area.
			
 # Parameters     :   none
*******************************************************************************************************************/ 
void CMethodDlg::SetUpInitialClientAreaRect()
{
  //set user client are rect

  RECT * pClientRect = new RECT;
  this->GetClientRect(pClientRect);

  m_UserClientRect.top = (pClientRect->top) + USER_CLIENT_TOP_OFFSET;
  m_UserClientRect.left = (pClientRect->left) + USER_CLIENT_LEFT_OFFSET;
  m_UserClientRect.right = (pClientRect->right) - USER_CLIENT_RIGHT_OFFSET;
  m_UserClientRect.bottom = (pClientRect->bottom) - USER_CLIENT_BOTTOM_OFFSET;

  PTR_DELETE(pClientRect);

	m_ctlNext.EnableWindow(FALSE);
	m_ctlAbort.EnableWindow(FALSE);
	m_ctlHelp.EnableWindow(TRUE);


}

/****************************************************************************************************************
 # Function Name  : SetUpEditBoxRect

 # Prototype      :  void SetUpEditBoxRect()

 # Description    :  This function sets up the edit box rect.
			
 # Parameters     :   none
*******************************************************************************************************************/ 

void CMethodDlg::SetUpEditBoxRect()
{
  if(!m_bTextOverflow)
  {
    m_EditCtrlRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom,m_DisplayRichEditRect.left + EDITBOX_LENGTH,m_DisplayRichEditRect.bottom + EDITBOX_WIDTH_FOR_NO_OVERFLOW);
  }
  else
  {
    m_EditCtrlRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom + OFFSET_FROM_DISPLAY_RICHEDIT_CTRL ,m_DisplayRichEditRect.left + EDITBOX_LENGTH,m_DisplayRichEditRect.bottom + EDITBOX_WIDTH);
  }
  m_EditCtrlRect.NormalizeRect();

}

/****************************************************************************************************************
 # Function Name  : CreateComboList

 # Prototype      :  void CreateComboList()

 # Description    :  This function gets the combo selection as a string and populates a list.
			
 # Parameters     :   none
*******************************************************************************************************************/ 

void CMethodDlg::CreateComboList()
{
	CString strComboElements = m_structUIData.ComboBox.pchComboElementText;
	m_ComboElementsList.RemoveAll();
	m_ComboElementValueList.RemoveAll();
	int i;	// PAW 03/03/09
	for(/*int PAW 03/03/09*/i=0;i<m_structUIData.ComboBox.iNumberOfComboElements - 1;i++)
	{
		int nIndex = strComboElements.Find(';',0);

		CString strElement = strComboElements.Left(nIndex);
		strElement.TrimLeft();
		strElement.TrimRight();

		strComboElements = strComboElements.Right(strComboElements.GetLength() - nIndex - 1);

		m_ComboElementsList.AddTail(strElement);
		m_ComboElementValueList.Add(m_structUIData.ComboBox.m_lBitValues[i]);
	}
	m_ComboElementsList.AddTail(strComboElements);
	m_ComboElementValueList.Add(m_structUIData.ComboBox.m_lBitValues[i]);
}


/****************************************************************************************************************
 # Function Name  : SetUpComboBoxRect

 # Prototype      :  void SetUpComboBoxRect(int nCount,CString strLongestString)

 # Description    :  This function sets up the combo box rect.
			
 # Parameters     :   none
*******************************************************************************************************************/ 

void CMethodDlg::SetUpComboBoxRect(int nCount,CString strLongestString)
{
  CSize size;
  CClientDC dc(this);
  //get the size of the largest string in the combo box. This is used to set the sixze of the combo.
  size = dc.GetOutputTextExtent(strLongestString,strLongestString.GetLength());

  int nComboLength = m_DisplayRichEditRect.left + size.cx + COMBOBOX_OFFSET_LENGTH;

  //ensure that the combo box length does not exceed the client area
  if(nComboLength > m_DisplayRichEditRect.right)
  {
    nComboLength = m_DisplayRichEditRect.right - COMBOBOX_OFFSET_LENGTH;
  }

  //NOTE: COMBOBOX_OFFSET_LENGTH is used below to make up for the dropdown button at in the combo
  if(!m_bTextOverflow)
  {
    m_ComboCtrlRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom,nComboLength ,m_DisplayRichEditRect.bottom + (COMBOBOX_WIDTH * 10));
  }
  else
  {
    m_ComboCtrlRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom + OFFSET_FROM_DISPLAY_RICHEDIT_CTRL,nComboLength,m_DisplayRichEditRect.bottom + (COMBOBOX_WIDTH * 10));

  }
   
  m_ComboCtrlRect.NormalizeRect();

}

/****************************************************************************************************************
 # Function Name  : SetUpDateTimeRect

 # Prototype      :  void SetUpDateTimeRect()

 # Description    :  This function sets up the datetime rect.
			
 # Parameters     :   none
*******************************************************************************************************************/ 
void CMethodDlg::SetUpDateTimeRect()
{
  if(!m_bTextOverflow)
  {
    m_DateTimeRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom ,m_DisplayRichEditRect.left + DATETIME_LENGTH,m_DisplayRichEditRect.bottom + DATETIME_WIDTH_FOR_NO_OVERFLOW);
  }
  else
  {
    m_DateTimeRect.SetRect(m_DisplayRichEditRect.left,m_DisplayRichEditRect.bottom + OFFSET_FROM_DISPLAY_RICHEDIT_CTRL ,m_DisplayRichEditRect.left + DATETIME_LENGTH,m_DisplayRichEditRect.bottom + DATETIME_WIDTH);
  }
  m_DateTimeRect.NormalizeRect();

}



void CMethodDlg::OnDestroy() 
{	
	TRACE(_T("CMethodDlg:OnDestroy......(0x%x)\n"),GetCurrentThreadId());

	CMainFrame* m_pMainFrm  = (CMainFrame*)(AfxGetApp()->m_pMainWnd);
	if (m_pMainFrm)
	{
		m_pMainFrm->DeRegisterDialog(this);
	}
	// stevev 22may07
	if (m_pEnumCheckBox)
	{
		delete m_pEnumCheckBox;
		m_pEnumCheckBox = NULL;
	}
}


/****************************************************************************************************************
 # Function Name  : RemovePreviousControls

 # Prototype      :  void RemovePreviousControls()

 # Description    :  This function removes the previous controls.(The controls are created at runtime)
			
 # Parameters     :   none
*******************************************************************************************************************/ 

void CMethodDlg::RemovePreviousControls()
{
  switch(m_nPreviousDisplayControl)
  {
  case 0:
	  {
		  LOGIT(CLOG_LOG,"Previous Display was empty\n");
		  break;
	  }
  case TEXT_MESSAGE:
    {
      m_RichEditDisplay.DestroyWindow();
      break;
    }
  case EDIT:
    {
      m_EditBox.DestroyWindow();
      m_RichEditDisplay.DestroyWindow();

      break;
    }
  case COMBO:
    {
		m_nCurrentComboSelection = 0;
		m_ComboBox.DestroyWindow();
		m_RichEditDisplay.DestroyWindow();
		if (m_pEnumCheckBox != NULL)
		{
			m_pEnumCheckBox->ShowWindow(SW_HIDE);
			m_pEnumCheckBox->DestroyWindow();
			delete m_pEnumCheckBox;
			m_pEnumCheckBox = 0;
		}
		break;
    }
  case HARTDATE:
    {
//      m_DateTimeCtrl.DestroyWindow();
//      m_RichEditDisplay.DestroyWindow();
		
      m_EditBox.DestroyWindow();
      m_RichEditDisplay.DestroyWindow();
		
      break;
    }
  case TIME:
    {
      m_EditBox.DestroyWindow();
      m_RichEditDisplay.DestroyWindow();

      break;
    }
  default:
	  {
		  LOGIT(CLOG_LOG,"Unknown edit control type: %d\n",(int)m_nPreviousDisplayControl);
	  }
  }
}


/**********************************************************************************************
 # Function Name  : ValidateAndUpdateEditBoxValue

 # Prototype      :  BOOL ValidateAndUpdateEditBoxValue()

 # Description    :  This function validates the exit box values entered by the user.
			
 # Parameters     :   none
***********************************************************************************************/ 

BOOL CMethodDlg::ValidateAndUpdateEditBoxValue() 
{

  //VariantInit(&m_varUserEditValue);
  //m_varUserEditValue.vt = VT_VARIANT;
	m_varUserEditValue.clear();

	if (!m_EditBox.GetValue(m_varUserEditValue))
	{
		return FALSE;
	}
	CValueVarient l_vArient;

	BOOL bReturn = TRUE;
	CString strOutOfRange,strTemp;

	/* stevev 28feb06 - add access to device object for services */
//	hCVar* pVar = NULL;
	if (m_structUIData.pVar4ItemID == NULL &&
		m_structUIData.DDitemId    != 0    && m_MethCall.m_pMeth != NULL)
	{
		m_structUIData.pVar4ItemID = 
			                    m_MethCall.m_pMeth->getVarPtrBySymNumber(m_structUIData.DDitemId);
	}
	/* stevev 28feb06 - the rangelist relation is not an OR it's an AND */
  switch(m_structUIData.EditBox.editBoxType)
  {
  case EDIT_BOX_TYPE_FLOAT:
    {
		if ( m_structUIData.pVar4ItemID != NULL )
		{
			//l_vArient = (float)m_varUserEditValue.vh_Value.dblVal;
			//bReturn = pVar->isInRange(l_vArient);
			bReturn = m_structUIData.pVar4ItemID->isInRange(m_varUserEditValue);
			if ( ! bReturn )
			{
				strTemp = L"Float value out of range.";
				AfxMessageBox(strTemp);
			}
		}
		else
		{// local to method - has no id and only 1 upper & lower range values
			double fValue = (double)m_varUserEditValue;//.vh_Value.dblVal;
	/*<START>Added by ANOOP for validating a list of ranges	*/		
			MinMaxVal_t::iterator mmFnd;
			double tmpminVal,tmpmaxVal;

			for (mmFnd = m_structUIData.EditBox.MinMaxVal.begin(); 
				 mmFnd != m_structUIData.EditBox.MinMaxVal.end();     ++mmFnd)
			{ 
				tmpminVal=mmFnd->FloatMinMaxVal.fMinval;  
				tmpmaxVal=mmFnd->FloatMinMaxVal.fMaxval;
	/*<END>Added by ANOOP for validating a list of ranges	*/	
				if(fValue < tmpminVal || fValue > tmpmaxVal)
				{
					strTemp.LoadString(IDS_OUT_OF_RANGE_FLOAT);
					strOutOfRange.Format(strTemp,tmpminVal,tmpmaxVal);
					AfxMessageBox(strOutOfRange);
					bReturn = FALSE;
				}
				/* stevev 28feb06 - end change */
			}
		}
     }
     break;
  case EDIT_BOX_TYPE_INTEGER:
    {
		if ( m_structUIData.pVar4ItemID != NULL )
		{	//l_vArient = m_varUserEditValue.vh_Value.lVal;//hart variant
			//bReturn = pVar->isInRange(l_vArient);
			bReturn = m_structUIData.pVar4ItemID->isInRange(m_varUserEditValue);
			if ( ! bReturn )
			{
				strTemp = L"Integer value out of range.";
				AfxMessageBox(strTemp);
			}
		}
		else
		{
			__int64 nValue = (__int64)m_varUserEditValue;//.vh_Value.lVal;
/*<START>Added by ANOOP for validating a list of ranges	*/		
			MinMaxVal_t::iterator mmFnd;
			__int64 tmpminVal,tmpmaxVal;

			for (mmFnd = m_structUIData.EditBox.MinMaxVal.begin(); mmFnd != m_structUIData.EditBox.MinMaxVal.end(); mmFnd++)
			{ 
				tmpminVal = mmFnd->IntMinMaxVal.iMinval;  
				tmpmaxVal = mmFnd->IntMinMaxVal.iMaxval;
/*<END>Added by ANOOP for validating a list of ranges	*/		

				if(nValue < tmpminVal || nValue > tmpmaxVal)
				{
					strTemp.LoadString(IDS_OUT_OF_RANGE_INT);
					strOutOfRange.Format(strTemp,tmpminVal,tmpmaxVal);
					AfxMessageBox(strOutOfRange);
					bReturn = FALSE;
				}
			}
		}
    }
    break;
    case EDIT_BOX_TYPE_DATE:
    case EDIT_BOX_TYPE_TIME:
    case EDIT_BOX_TYPE_STRING:
	case EDIT_BOX_TYPE_PASSWORD:
    {		
		//CValueVarient l_vArient;
		//CString strValue = m_varUserEditValue.vh_Value.pStrVal;	
		if ( m_structUIData.pVar4ItemID != NULL )
		{
			//l_vArient = m_varUserEditValue.vh_Value.pStrVal;	
			//bReturn = pVar->isInRange(l_vArient);	
			bReturn = m_structUIData.pVar4ItemID->isInRange(m_varUserEditValue);
			if ( ! bReturn )
			{
				strTemp.LoadString(_OUT_OF_RANGE_TEXT);
				//strOutOfRange.Format(strTemp,m_structUIData.EditBox.iMaxStringLength);
				strOutOfRange.Format(strTemp,m_structUIData.EditBox.nSize);
				AfxMessageBox(strOutOfRange);
				bReturn = FALSE;
			}
		}
		else
		{
			//int nLength = strValue.GetLength();
			unsigned nLength = ((wstring)m_varUserEditValue).size();
			//if(nLength > m_structUIData.EditBox.iMaxStringLength && nLength != 0)
			if(nLength > m_structUIData.EditBox.nSize && nLength != 0)
			{
				strTemp.LoadString(_OUT_OF_RANGE_TEXT);
				//strOutOfRange.Format(strTemp,m_structUIData.EditBox.iMaxStringLength);
				strOutOfRange.Format(strTemp,m_structUIData.EditBox.nSize);
				AfxMessageBox(strOutOfRange);
				bReturn = FALSE;
			}
		}
		break;
    }
  }// endswitch

  return bReturn;
}


/****************************************************************************************************************
 # Function Name  : SetUpEditBox

 # Prototype      :  void SetUpEditBox()

 # Description    :  This function sets up the edit box with current value, min and max value etc.
			
 # Parameters     :   none
 
   //  stevev 22may09 - get rid of uncontrolled VARIANTs
*******************************************************************************************************************/ 

void CMethodDlg::SetUpEditBox(int nPrecision) 
{
	//VARIANT varValue;
	//VariantInit(&varValue);
	//VALUE_HOLD varValue;
	m_EditBox.SetEditFormatString(L"");// everything
	/* stevev 13sep10 - add access to device object for services */
	if (m_structUIData.pVar4ItemID == NULL &&
		m_structUIData.DDitemId    != 0    && m_MethCall.m_pMeth != NULL)
	{
		m_structUIData.pVar4ItemID = 
			                    m_MethCall.m_pMeth->getVarPtrBySymNumber(m_structUIData.DDitemId);
	}

	switch(m_structUIData.EditBox.editBoxType)
		{
			int nLimit_chars;
		case EDIT_BOX_TYPE_INTEGER:
			{
				m_EditBox.SetEditCtrlType(m_structUIData.EditBox.editBoxType);
			m_EditBox.SetValidValueString(_T(""));// everything
			m_EditBox.SetEditFormatString(L"");// everything
		//varValue.vt = VT_I4;
		//varValue.lVal = m_structUIData.EditBox.iValue;
//		varValue.bt            = EDIT_BOX_TYPE_INTEGER;
//		varValue.vh_Value.lVal = m_structUIData.EditBox.iValue;
		int ft = -1;
		if ( m_structUIData.pVar4ItemID != NULL )
		{
			ft = m_structUIData.pVar4ItemID->getEditFormatInfo(nLimit_chars, nPrecision);
		}
		
		if ( ft <= 0 )// can't get the info from the variable
		{
			// stevev 22may09 - calculate max size before overriding w/ format value
			//				    this gives us a limit size if the format doesn't have a size	
			char out_buf[24]={0};
			int nEdt_size   = m_structUIData.EditBox.nSize; 
			if ( nEdt_size <= 4 )
			{
				unsigned long value=(unsigned long)pow(2.0,8*nEdt_size) -1;// 2 change to 2.0 PAW 03/03/09
				/* stevev 27feb06 - expect user to input negative decimal numbers */
				_ultoa(value,out_buf,10);
			}
			else // a long-long type
			{
				unsigned __int64 maxv = 0xffffffffffffffff;
				maxv = maxv >> (8*(8 - nEdt_size));
				_ui64toa( maxv, out_buf, 10  );
			}
			if(out_buf[0])
				nLimit_chars = strlen(out_buf) + 1;
			else
				nLimit_chars = 0;		
		}// else it was filled in getInfo

		if( ! m_structUIData.EditBox.strEdtFormat.empty() )
		{
			wstring validValueString;
			if ( ft <= 0 )
			{// we have a format but the getInfo failed
			
				// stevev 22may09 - NOTE: this algorithm assumes (incorrectly) no int precision specifier
				TCHAR strFmt[24]={0},out_buf1[10]={0};
				TCHAR *pdest = NULL;
				int adder = 0, fmtLen, prelen;
				char asciiFmt[24]={0};
				
				_tcscpy(strFmt,m_structUIData.EditBox.strEdtFormat.c_str());//a tstring
	// stevev 01jun09 - we're always unicode
	//#ifdef _UNICODE  
	//			UnicodeToASCII(m_structUIData.EditBox.strEdtFormat.c_str(), asciiFmt, 
	//											m_structUIData.EditBox.strEdtFormat.length() + 1);
	//#else
	//			strcpy(asciiFmt,m_structUIData.EditBox.strEdtFormat.c_str());
	//#endif
	#ifndef _UNICODE
				assert(0);
	#endif
				// moved down  
				//m_EditBox.SetEditFormatString(m_structUIData.EditBox.strEdtFormat.c_str());

				fmtLen = _tcslen(strFmt);

				if ( (prelen = _tcscspn(strFmt,_T("di"))) < fmtLen )
				{// a signed fmt type exists
					adder = 1;          // add one for possible negative sign
					//m_EditBox.SetValidValueString(_T("-0987654321"));// numerics
					validValueString = _T("-0987654321");
				}
				
				if ( adder   || ( (prelen = _tcscspn(strFmt,_T("ouxX"))) < fmtLen ))
				{// a signed or  an unsigned format exists
					_tcsncpy(out_buf1,strFmt,prelen);
					if(out_buf1[0])
						nLimit_chars =_ttoi(out_buf1) + adder;
					if ( ! adder )	// than it has to be unsigned
					{
						if ( _tcschr(strFmt, _T('o')) != NULL )
							//m_EditBox.SetValidValueString(_T("07654321"));// octal numerics
							validValueString = L"07654321";
						else
						if ( _tcschr(strFmt, _T('u')) != NULL )
							//m_EditBox.SetValidValueString(_T("0987654321"));// unsigned numerics
							validValueString = L"0987654321";
						else
						if ( _tcschr(strFmt, _T('x')) != NULL )
							//m_EditBox.SetValidValueString(_T("0987654321abcdefABCDEF"));// unsigned hex
							validValueString = L"0987654321abcdefABCDEF";
						else
						if ( _tcschr(strFmt, _T('X')) != NULL )
							//m_EditBox.SetValidValueString(_T("0987654321abcdefABCDEF"));// unsigned hex
							validValueString = L"0987654321abcdefABCDEF";
						// else an invalid format has been entered, use default limit
					}
				}
				// else an invalid format has been entered, use default limit
			}
			else // getInfo worked so apply abbreviated version
			{
				if ( ft == L'o' )
					validValueString = L"07654321";// octal numerics
				else
				if ( ft == L'u')
					validValueString = L"0987654321";// unsigned numerics
				else
				if ( ft == L'x' || ft == L'X' )
					validValueString = L"0987654321abcdefABCDEF";// unsigned hex
				else
				if ( ft == L'd' || ft == L'i' )
					validValueString = L"-0987654321";// signed numerics
				// else an invalid format has been entered, use default limit
			}
			m_EditBox.SetValidValueString(validValueString.c_str() );
			m_EditBox.SetEditFormatString(m_structUIData.EditBox.strEdtFormat.c_str());// moved from above
		}
		// format string is empty, use default limit

		m_EditBox.SetLimitText(nLimit_chars);			
//		m_EditBox.SetValue(varValue);			
		m_EditBox.SetValue(m_structUIData.EditBox.editBoxValue);
		break;
	}
	case EDIT_BOX_TYPE_FLOAT:
	{
		m_EditBox.SetEditCtrlType(m_structUIData.EditBox.editBoxType);
		m_EditBox.SetValidValueString(_T(""));// everything
		int ft = 0;
		if ( m_structUIData.pVar4ItemID != NULL )
		{
			ft = m_structUIData.pVar4ItemID->getEditFormatInfo(nLimit_chars, nPrecision);
		}
		else
		{
			nLimit_chars = HANDHELD_FLOAT_MAXVAL_LEN;// default length - this is wrong stevev 13sep10
			// leave nPrecision the same as passed in
		}
		m_EditBox.SetPrecision(nPrecision);
		//varValue.vt = VT_R8;
		//varValue.dblVal = m_structUIData.EditBox.fValue;		
//		varValue.bt              = EDIT_BOX_TYPE_FLOAT;
//		varValue.vh_Value.dblVal = m_structUIData.EditBox.fValue;
				
//this is wrong stevev 13sep10		nLimit_chars = HANDHELD_FLOAT_MAXVAL_LEN;// default length 
	
		if( ! m_structUIData.EditBox.strEdtFormat.empty() )
		{
			m_EditBox.SetEditFormatString(m_structUIData.EditBox.strEdtFormat.c_str());

			if ( ft == 0 ) // getInfo failed
			{
				TCHAR strFmt[24]={0},out_buf1[10]={0},out_buf2[10]={0};
				TCHAR *pdest=NULL;
				int result, fmtLen, prelen, prec;
				char asciiFmt[24]={0};
				
				_tcscpy(strFmt,m_structUIData.EditBox.strEdtFormat.c_str());
// stevev 01jun09 - we're always unicode
//#ifdef _UNICODE  
//			UnicodeToASCII(m_structUIData.EditBox.strEdtFormat.c_str(), asciiFmt, 
//											m_structUIData.EditBox.strEdtFormat.length() + 1);
//#else
//			strcpy(asciiFmt,m_structUIData.EditBox.strEdtFormat.c_str());
//#endif
#ifndef _UNICODE
			assert(0);
#endif
			// moved up     m_EditBox.SetEditFormatString(m_structUIData.EditBox.strEdtFormat.c_str());

				fmtLen = _tcslen(strFmt);
				
				//if( NULL != ( pdest = _tcschr( strFmt, _T('f') )) )
				if ( (prelen = _tcscspn(strFmt,_T("fFeEgG"))) < fmtLen && (prelen > 0))
				{// we have something in front of the float
					_tcsncpy(out_buf1,strFmt,prelen);// upto the character
					out_buf1[prelen] = 0;
					TCHAR *pTmp =_tcschr( out_buf1, _T('.') );

					if( NULL != pTmp )
					{// we have a dp
						*pTmp++ = 0;// ptmp points to post dp string
						//result = pTmp - out_buf1;
						//_tcsncpy(out_buf2,strFmt,result);
						//pTmp++;
						//TCHAR *q=_tcschr( pTmp, _T('f') );
						//if(NULL != q)
						//{
						//	int nVal = q - pTmp ;
						//	_tcsncpy(out_buf2,pTmp,nVal);
						//}
						prec   = _ttoi(pTmp);	
					}
					else
					{// no dp
						prec   = 0;	
					}
					result = _ttoi(out_buf1);	
					
					if(out_buf1[0])
						nLimit_chars = _ttoi(out_buf1);	
					//else leave it default
				}
				// else no size/precision so leave it default 	
			}// else getInfo filled the needed values
		}
		//else no format string so leave it default

		m_EditBox.SetLimitText(nLimit_chars);
//		m_EditBox.SetValue(varValue);
		m_EditBox.SetValue(m_structUIData.EditBox.editBoxValue);
			
		break;
	}
	case EDIT_BOX_TYPE_DATE:
	case EDIT_BOX_TYPE_TIME:
	{
		m_EditBox.SetEditCtrlType(m_structUIData.EditBox.editBoxType);				
		m_EditBox.SetLimitText(m_structUIData.EditBox.nSize);
		if (m_structUIData.pVar4ItemID != NULL)
		{
			m_structUIData.EditBox.editBoxValue = m_structUIData.pVar4ItemID->getEditValueString();
		}
		//else - set it to empty
		m_EditBox.SetValue(m_structUIData.EditBox.editBoxValue);
		break;
	}
	case EDIT_BOX_TYPE_STRING:
	case EDIT_BOX_TYPE_PASSWORD:
	{
//		CString strValue;
		m_EditBox.SetEditCtrlType(m_structUIData.EditBox.editBoxType);	
//		varValue.bt              = EDIT_BOX_TYPE_STRING;
		//varValue.vt = VT_BSTR; 
//		if ( m_structUIData.EditBox.editBoxType == EDIT_BOX_TYPE_DATE ||
//			 m_structUIData.EditBox.editBoxType == EDIT_BOX_TYPE_TIME  )
//		{
//			strValue = m_structUIData.datetime.pchHartDate;
//		}
//		else // STRING
//		{
//			strValue = m_structUIData.EditBox.pchDefaultValue;//a char*
//		}
		//varValue.bstrVal = strValue.AllocSysString();
//		varValue.vh_Value.pStrVal = new wchar_t[strValue.GetLength()+1];
//		_tcsncpy(varValue.vh_Value.pStrVal,LPCTSTR(strValue),strValue.GetLength());
//		varValue.vh_Value.pStrVal[strValue.GetLength()] = 0;

//		m_EditBox.SetLimitText(m_structUIData.EditBox.iMaxStringLength);				
		m_EditBox.SetLimitText(m_structUIData.EditBox.nSize);
//		m_EditBox.SetValue(varValue);
		m_EditBox.SetValue(m_structUIData.EditBox.editBoxValue);
//		delete[] varValue.vh_Value.pStrVal;

		break;
	} 
//	case EDIT_BOX_TYPE_PASSWORD:
//	{
//		CString strValue;
//		m_EditBox.SetEditCtrlType(m_structUIData.EditBox.editBoxType);	
//		varValue.bt              = EDIT_BOX_TYPE_PASSWORD;
//		varValue.vh_Value.pStrVal= NULL;
		//varValue.vt = VT_BSTR;
		//strValue = "";
		//varValue.bstrVal = strValue.AllocSysString();

//		m_EditBox.SetLimitText(m_structUIData.EditBox.iMaxStringLength);
//		m_EditBox.SetValue(varValue);

//		break;
//	}
	case UNKNOWN_EDIT_BOX_TYPE:
	{
		break;
	}
	}// endswitch

}


void CMethodDlg::GetDateFromString(CString strDateTime)
{
  TCHAR pszValue[5];
  int nMonth=0,nDate=0,nYear=0;
	CString strMonth,strDate,strYear,strTemp;

	//month
	int nIndex = strDateTime.Find(_T('/'));
	strMonth = strDateTime.Left(nIndex);

	//date
	strDate = strDateTime.Mid(nIndex+1,2);

	//year
	strTemp = strDateTime.Right(4);
	strYear += strTemp;


	_tcscpy(pszValue,strMonth);
	nMonth = _ttoi(pszValue);

	_tcscpy(pszValue,strDate);
	nDate = _ttoi(pszValue);


	_tcscpy(pszValue,strYear);
	nYear = _ttoi(pszValue);


	COleDateTime tmpDateTime(nYear,nMonth,nDate,0,0,0);
	if(tmpDateTime.GetStatus() == 1 ||
			tmpDateTime.GetStatus() == -1 ||
				tmpDateTime.GetStatus() == 2)
	{		
		nDate = 1;
		nMonth = 1;
		nYear = 1900;
		COleDateTime tmpDtTime(nYear,nMonth,nDate,0,0,0);
//		m_DateTimeCtrl.SetTime(tmpDtTime);

	}
	else 
	{
//		m_DateTimeCtrl.SetTime(tmpDateTime);
	}

//	m_OleDateTime.SetDate(nYear,nMonth,nDate); 05APR2004 Commented by ANOOP to set the proper date.

}


void CMethodDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	// Do not call CDialog::OnPaint() for painting messages

//TRACE(_T(">> Dialog Paint Response @ 0x%08p\n"),this);
	    
    CDialog::OnPaint();
}



void CMethodDlg::OnBtnHelp() 
{
		//prashant oct 2003
		//format the help string to be displayed

		CString strCapTemp,strItemCaption,strHelp;

		strHelp = m_szHelp;

		strCapTemp.LoadString(IDS_HELP_CAPTION);
		strItemCaption.Format(strCapTemp, m_szMethodName);

		if(strHelp.IsEmpty())
		{
			strHelp.LoadString(IDS_NO_HELP);;
		}
		else
		{
			CString strTemp = m_szHelp;

			int nLength = strTemp.GetLength();
			int nInsertIndex = MAX_HELPSTRING_LENGTH;

			if((nLength > MAX_HELPSTRING_LENGTH))
			{
				strHelp.Empty();
				do
				{
					if((strTemp.GetAt(MAX_HELPSTRING_LENGTH) != ' '))//if not white space
					{
						nInsertIndex = strTemp.Find(' ',MAX_HELPSTRING_LENGTH);
						if(nInsertIndex < 0) //if no white space take the string as is.
						{
								strHelp += strTemp;
								break;
						}
					}
					strTemp.SetAt(nInsertIndex,'\n');
					strHelp += strTemp.Left(nInsertIndex);
					strTemp = strTemp.Right(nLength - nInsertIndex);
					nLength = strTemp.GetLength();
					nInsertIndex = MAX_HELPSTRING_LENGTH;
					if(nLength < MAX_HELPSTRING_LENGTH)
					{
						strHelp += strTemp;
					}
				}
				while(nLength > MAX_HELPSTRING_LENGTH);
			}
		}
		MessageBox(strHelp,strItemCaption);
	
}

void CMethodDlg::OnBtnNext() 
{
	DEBUGLOG(CLOG_LOG,">>XXXX  Dialog On Button Next.(0x%x)\n",GetCurrentThreadId());

	m_bNextBtnClicked = TRUE;
	m_NextButtonSet   = TRUE; // to handle the situation of next during MethodDisplay
//validate edit box values entered by the user
	if( m_structUIData.userInterfaceDataType == EDIT || 
		m_structUIData.userInterfaceDataType == TIME || 
		m_structUIData.userInterfaceDataType == HARTDATE  )
	{
		if(!ValidateAndUpdateEditBoxValue())
		{
			m_EditBox.SetFocus();
			DEBUGLOG(CLOG_LOG,">>XXXX Dialog Edit Box Exit On Button Next .\n");

			return;

			
		}
	}

//store the combo selection selected by the user
	if(m_structUIData.userInterfaceDataType == COMBO)
	{
		if (m_structUIData.ComboBox.comboBoxType == COMBO_BOX_TYPE_MULTI_SELECT)
		{
			if (m_pEnumCheckBox)
			{
				m_nCurrentComboSelection = m_pEnumCheckBox->GetSelectedValue();
			}
			else
			{// error
			}
		}
		else
		{
			m_nCurrentComboSelection = m_ComboBox.GetCurSel();
			if(CB_ERR == m_nCurrentComboSelection)
			{
				CString strMsg;
				strMsg.LoadString(IDS_INVALID_COMBO_SEL);
				AfxMessageBox(strMsg);
				DEBUGLOG(CLOG_LOG,">>XXXX  Dialog Illegal combo Exit On Button Next .\n");
				return;
			}
		}
	}


	m_ctlNext.EnableWindow(FALSE);
	m_ctlAbort.EnableWindow(FALSE);

	DEBUGLOG(CLOG_LOG,">>XXXX Dialog Set Event On Button Next .\n");
	//set event
	// stevev 27jul05 SetEvent(m_hUserAckEvent);
	p_hCevtCntl->triggerEvent();

	CString strStatusMsg;
	strStatusMsg.LoadString(IDS_METHOD_IN_PROGRESS);
	m_ctlStat.SetWindowText(strStatusMsg);

	
	DEBUGLOG(CLOG_LOG,">>XXXX  Dialog Normal Exit On Button Next .\n");



/*<START> temp code. used for testing
--- test code removed 24jun11 - see earlier version for content */
}


void CMethodDlg::OnBtnAbort() 
{
	//set abort button clicked flah to true and set event
	m_bAbortBtnClicked = TRUE;
	// stevev 27jul05 SetEvent(m_hUserAckEvent);
	DEBUGLOG(CLOG_LOG,">>XXXX Dialog OnBtnAbort trigger Event.\n");	
	p_hCevtCntl->triggerEvent();

	//set the method aborted flag to true
	m_MethodAborted = TRUE;

	m_ctlNext.EnableWindow(FALSE);
	m_ctlAbort.EnableWindow(FALSE);
	
	CString strMsg;
	strMsg.LoadString(IDS_METH_ABORTING);
	m_ctlStat.SetWindowText(strMsg);
	DEBUGLOG(CLOG_LOG,">>XXXX  Dialog Normal Exit On Abort Button.(%s)\n",
		(m_MethodAborted)?"MethodAborted":"MA clear");

}





//This is added to so that the user cannot quit the methods dialog using the
//esc key or the enter key during methods execution.

BOOL CMethodDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
					 return FALSE;
	
	CWnd *pWnd   = GetFocus();
	CWnd* pHelp  = GetDlgItem(IDC_BTN_HELP);
	CWnd* pNext  = GetDlgItem(ID_OK);
	CWnd* pAbort = GetDlgItem(IDC_BTN_ABORT);


	if((pWnd != pHelp) && (pWnd != pNext) && (pWnd != pAbort) )
	{
		if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
			return FALSE;
	}
	// stevev 20aug07 - if nobody has the focus, take it here
	//  We are losing the focus to other apps and can never get it back
	if (pWnd == NULL)
		SetFocus();
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CMethodDlg::ShowDialog() 
{
TRACE(_T(">>>> Dialog ShowDialog.\n"));
 //commented by prashant 3rd mar 04
  /*
  int DialogWidth = m_InitialDialogRect.Width();
  int DialogHeight = m_InitialDialogRect.Height();
  MoveWindow(m_InitialDialogRect.top, m_InitialDialogRect.left, DialogWidth, DialogHeight);
  */
  //commented by prashant 3rd mar 04 end
  return TRUE;

  
}

void CMethodDlg::OnCancel() 
{	// this is where EndDialog() is called...
TRACE(_T(">>>> Dialog rcv'd On Cancel.(in 0x%x)\n"),GetCurrentThreadId() );

	sdCDialog::OnCancel();
//	CDialog::OnCancel();
OnDestroy();// apparently not executed from sdCDialog...26jun06
	m_dlgReady = FALSE;// handshake to the outside world
}


//================================================================================================
//
//

CMenuDlg::CMenuDlg(ACTION_UI_DATA* pAUD, CWnd* pParent,CdrawBase* pP)// any could be NULL
		: m_dlgReady(FALSE),m_MethodAborted(FALSE),m_NextButtonSet(FALSE),CdrawDlg(pP)
{
TRACE(_T(">>>> DisplayMenu Constructor.(0x%x)\n"),GetCurrentThreadId() );
	p_hCevtCntl = m_hCevent.getEventControl();
	m_bAbortBtnClicked = m_bNextBtnClicked = m_bButOneClicked = 
	m_bButTwoClicked   = m_bButThrClicked  = FALSE;

	if (pAUD)
		m_structUIData=*pAUD;
	else
		//memset(&m_structUIData,0,sizeof(ACTION_UI_DATA));
		m_structUIData.clear();

	m_pButtonTxt  =  m_structUIData.ComboBox.pchComboElementText;
	m_exButtonCnt =  m_structUIData.ComboBox.iNumberOfComboElements;
LOGIT(CLOG_LOG,L"MenuDisplay has %d buttons with text =|%s|\n",m_exButtonCnt,m_pButtonTxt);
	m_pMnFrm = (CMainFrame*)pParent;
};

CMenuDlg::~CMenuDlg()
{
  RAZE(p_hCevtCntl);
}

// equivelent to domodal()
int CMenuDlg::RunDialog()
{
	m_symblID = m_structUIData.DDitemId;
	m_dlgReady = true; // assume all is well...
	Execute(m_structUIData.ComboBox.iNumberOfComboElements);
	//-//LOGIT(CLOG_LOG,"/////MenuDialog-Executed-triggering event to the outside world\n");
	//p_hCevtCntl->triggerEvent();
	// doesn't work...Sleep(2);// allow this message to be acquired and acted upon before continuing
	return 0;
}

BOOL CMenuDlg :: ShowMethodsData(ACTION_UI_DATA structUIData)
{
TRACE(_T(">>>> DisplayMenu sending Show Methods Data(0x%x)\n"),GetCurrentThreadId());
//if (hWnd)
//	SendMessage(hWnd,WM_DRAWMETHODUI,(WPARAM)&structUIData,0);
TRACE(_T(">>>> DisplayMenu finished sending Show Methods Data .\n"));

	return TRUE;
}


void CMenuDlg :: SendClose(void)
{
	if (m_pDynCtrl)
		m_pDynCtrl->SendMessage(WM_CLOSE);
}



