#if !defined(AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_)
#define AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Scope_Ctrl.h : header file
//

#ifdef _USE_PLOT_LIB
//Vibhor 250205: Added includes for other IoComp controls
#import "iplotLibrary.tlb" named_guids 
#import "isAnalogLibrary.tlb" named_guids 
#import "iProfessionalLibrary.tlb" named_guids
#include <atlbase.h>
extern CComModule _Module;
using namespace iPlotLibrary;
using namespace isAnalogLibrary;
#include <afxmt.h>

#endif

#include "StdAfx.h"
#include "sdc625.h"
#include "SDC625Doc.h"

#include "ddbItemBase.h"
#include "ddbGeneral.h"
#include "ddbDefs.h"
#include "ddbMenu.h"

#include "graphicInfc.h"

#include "ddbPublish.h"
#include "ddbEvent.h"		// stevev 15aug05
#include "BASETSD.H"	// Added by ClassView

#include "logging.h"
#include "Bit.h"

/*
//Declaration of Thread Data Structure
struct ThreadData
{
 
  CComPtr<iPlotLibrary::IiPlotX> PlotComponent;
 
  bool  Terminated;
  bool  HasTerminated;

  dataPointList_t dataList;
};
*/

#define COLORCOUNT  17

#define xadef_none  0
#define xadef_min   1
#define xadef_max   2
#define xadef_both  3
/////////////////////////////////////////////////////////////////////////////
// CLegendDlg dialog

class CLegendDlg : public CDialog
{
// Construction
public:
	CLegendDlg(CWnd* pParent = NULL);   // standard constructor

	void * m_pSource;

// Dialog Data
	//{{AFX_DATA(CLegendDlg)
	enum { IDD = IDD_DSPLY_LEGEND_DIALOG };
	CBit	m_legendButton7;
	CBit	m_legendButton6;
	CBit	m_legendButton5;
	CBit	m_legendButton4;
	CBit	m_legendButton3;
	CBit	m_legendButton2;
	CBit	m_legendButton1;
	CBit	m_legendButton0;
	CString	m_strLegend0;
	CString	m_strLegend1;
	CString	m_strLegend2;
	CString	m_strLegend3;
	CString	m_strLegend4;
	CString	m_strLegend5;
	CString	m_strLegend6;
	CString	m_strLegend7;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLegendDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLegendDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CColorHandler

class CColorHandler	// Modified by POB, 15 Aug 2005
{
public:
	UINT32 GetCurrentOLEColor();
	UINT32 GetCurrentColor();
	UINT32 GetRealColor(UINT32 ddColor);
	UINT32 GetOLEColor(UINT32 ddColor);

	CColorHandler();
	~CColorHandler();

	void UseColor(unsigned long linecolor);

	void ClearColors(void);


private:
	unsigned long GetNextColor(void);

	bool m_used[COLORCOUNT];
	static const unsigned m_colors[COLORCOUNT];
	UINT32 m_color;
};

/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl window
//typedef vector <isAnalogLibrary::IiAngularGaugeX *> AngGaugeList;  // POB, 11 Aug 2005 - Moved down and modified below
#ifdef _USE_PLOT_LIB
typedef vector <isAnalogLibrary::IiLinearGaugeX *> LinearGaugeList; //void * because it can have combination of linear & log gauges
#else
typedef vector <void *> LinearGaugeList; // no plot
#endif
/* Arun 120505 Start of code */

typedef vector<dataPointList_t> dataPoints_t;
typedef vector<unsigned>        uintList_t; // stevev 08may07
typedef uintList_t::iterator    uintLstIT_t;// stevev 10may07
/* End of code */

class CScopeCtrl : public CStatic ,public hCpubsub
{
	DECLARE_DYNAMIC(CScopeCtrl)

// Construction
public:
	CScopeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCtrl)
	//}}AFX_VIRTUAL
#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~@");} return CStatic::WindowProc(message,wParam,lParam);};
#endif
// Implementation
public:
	virtual ~CScopeCtrl();

	bool              m_bReady;// stevev 22aug07 - set once the initial display is complete

	int CreateChartControl(hCchart * pChart,bool bIsParentAMeth = false);
	void ShowControl();
	void HideControl();

	bool DisplayWaveforms(itemID_t iid);//hCgraph *pGraph,ddbItemList_t waveList);
	void StaleDynamic(void);// Used to stale dynamics in graphs
	void UpdateWaveforms(itemID_t iid); //update_value
	bool UpdatePlot(long type=-1,long SymID=-1);// return true at size change
	void DisplayChart(hCchart * pChart);
	void UpdateOneForm(unsigned fromDex) ;   //erase & rebuild one waveform
	bool RefreshWaveforms(itemID_t iid);//update_structure


#ifdef _USE_PLOT_LIB
	
	int CreateGraphControl(bool bIsParentAMeth = false);//returns success if iPlot component successfully created and no other problem	
	int InitGraph(hCgraph * pGraph, CString Label,ddbItemList_t *pWaveList = NULL); 
	int InitChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
	
#endif

	BOOL OnPopBox(UINT ctrlID, VARIANT_BOOL * Cancel,long * screenX, long * screenY);
	BOOL OnZoomBox(UINT ctrlID, long * Left,long * Top,long * Right,long * Bottom,VARIANT_BOOL * Cancel);
	BOOL OnLegendClick (UINT ctrlID, unsigned lIndex);

	chartType_t chartType;
	RETURNCODE publish(dataStructList_t dsL);

	dataPointList_t dataList;

private:
	triadList_t        m_WaveExtraList;     // desc help value...newly needed for labeling
	ddbItemList_t      m_currentWaveList;   //The list of valid waveform (ptrs) currently being shown in this graph
	uintList_t         m_channelIdxsList;      // one per waveform (-1 means unused)
	uintList_t         m_EMPchannelIdxsList;   // one per waveform (-1 means unused)
	vector<uintList_t> m_ListOfLimitIndexLists;// one per waveform (MT means unused)[only used w/ Horiz&Vert]
	uintList_t         m_yAxisList;            // one per waveform (-1 means unused)

	listOfPointList_t m_currentPointLists; //The list of points for each waveform
	listOfPointList_t m_editedPointLists;  //The list of points for edited waveform
	ddbItemList_t     m_currentSourceList; //This is temporary.. to be merged with currentWaveList.

	hCitemBase*       m_pItemBase;
	CDDIdeDoc* m_pDoc;

	RECT m_rect;

	// stevev 8may07-notused:bool m_bIsParentMethodDlg;

	bool m_bRefreshFlag;
	CColorHandler m_colorSrc;       // Added by POB, 12 Aug 2005

	
	int m_iSubHandle;
	int m_iSourcecount;             // Added by POB, 12 Aug 2005
	int m_iSrcRectSpan;             // Added by POB, 12 Aug 2005

	int xAxDefined;					// stevev 09jul13 - which end is defined

private:
	void     Plot(pointValueList_t xyList, int iChannel);
	unsigned Plot(pointValue_t point,int iChannel,unsigned uColor);
//Vibhor 250205: Adding following functions to be called by CreateChartControl Function
//	int CreateChart(RECT rect); //Todo modify into 3 different calls strip,sweep & scope
	int CreateStripChart(RECT rect);
	int CreateScopeChart(RECT rect);
	int CreateSweepChart(RECT rect);
	int CreateAngularGauge(hCchart * pChar, RECT rect);   // Modified by POB, 11 Aug 2005
	int CreateAngularLogGauge(RECT rect);
	int CreateLinearGauge(hCchart * pChart, RECT rect);
	int CreateLinearLogGauge(RECT rect);

	void CreateSourceRect(RECT & rect);	// Added By POB, 12 Aug 2005

//Vibhor 010305: Adding following functions to be called by InitChart Function
	int InitStripChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
	int InitScopeChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
	int InitSweepChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
	int InitAngularGauge(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
	int InitLinearGauge(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);
		
	int InitSources(hCchart * pChart);      // Added by POB, 12 Aug 2005
	int InitWaveforms(hCgraph * pGraph);    // Added by POB, 12 Aug 2005

//	Anil September 16 2005
	int InitLogarithmicGauge(hCchart * pChart, CString Label,ddbItemList_t *pSourceList = NULL);

//Vibhor 010305: Adding following functions to be called by DisplayChart Function

	void DisplayStripChart();
	void DisplaySweepChart();
	void DisplayScopeChart();
	void DisplayAngularGauge();
	void DisplayLinearGauge();
	void DisplayLogarithmicGauge();

	static UINT CScopeCtrl::PaintChartComponentProc(LPVOID pParam);  // Added by POB, 12 Aug 2005 - used to be a global function

	long SetYaxis(hCaxis *pYaxis);// helper function 01may07 - stevev:: returns axis index

	// Generated message map functions
protected:
	//{{AFX_MSG(CScopeCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnLegendButton();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()	
	DECLARE_EVENTSINK_MAP()

private:
	CLegendDlg m_LegendDlg;
	CBitmap m_Bitmap;

	CWnd* m_pWnd;

	vector <CWnd*> srcWindowList; //For Source group dummy windows
	vector <CWnd*> windowList; //for individual bar gauges

#ifdef _USE_PLOT_LIB
	
#ifdef _DEBUG
	unsigned m_ControlId;
#endif
public:
	iPlotLibrary::IiPlotX 	*m_piPlotX1;
	iPlotLibrary::IiXYPlotX *m_piPlotX;
	struct AngGauge
	{
		isAnalogLibrary::IiAngularGaugeX * piAngGauge;
		iProfessionalLibrary::IiAngularLogGaugeX * piAngLogGauge;//anil Added 
		int nSrcMembers;	// Number of source members
		CWnd * pSrcWnd;		// pointer to Window holding the source
		RECT srcRect;		// Rectangular region taken up by source
		bool bLogGauge;
		AngGauge()
		{
			bLogGauge = false;
			piAngGauge = NULL;
			piAngLogGauge = NULL;
		}

	};

	typedef vector <AngGauge> AngGaugeList;  // Added by POB, 11 Aug 2005

	AngGaugeList m_AngGaugeList;            // Added by POB, 11 Aug 2005
	LinearGaugeList m_LinearGaugeList;

#endif
public:
		
	void GetIDsOfNames();

public:
	struct SrcMember
	{
		CString strName;	
		unsigned uColor;
	};

	struct Source
	{
		CButton * legendButton;
		SrcMember srcMember[10];	
	};

	Source m_Legend[10];	
	
	/* Arun 120505 Start of code */
	dataPoints_t datapoints;
	HANDLE hThread;
	CRITICAL_SECTION csPublishedData;
	//CEvent QuitThreadEvent; // stevev 15aug05
	hCevent QuitThreadEvent;  // stevev 15aug05
	DWORD dwThreadID;
	int	m_nCycleTime;
	int	m_nLENGTH;

	/* End of code */
	/* stevev 01dec06 -- used to pass the Linear sublist to the display routine */
	ddbItemList_t subsList;
	/* end stevev 01dec06 */

	// stevev 15aug05 -- start --
	hCevent HaveDataEvent; 
	hCevent::eventPtrList_t  waiting4;	// [0] is haveData; [1] is QuitThread
	/* c is either empty, ptr2class->  or class.   depending on reference type */
#define ISQUITSIGNAL(c) ( c waiting4[1]->getResult() == hCevent::ere_Success)
#define ISDATA(c)       ( c waiting4[0]->getResult() == hCevent::ere_Success)
	// stevev 15aug05 -- end --
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_)
