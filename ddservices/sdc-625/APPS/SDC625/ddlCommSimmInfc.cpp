/*************************************************************************************************
 *
 * $Workfile: DDLpipeCommInfc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		docCommInfc.cpp : implementation file
 */

#include "stdafx.h"
#include "SDC625.h"
#include "messageUI.h"	/* added 02jan08 - stevev */

// For ATL's variant support
#include <atlbase.h>
#if _MSC_VER < 1400 /* pre 2005 */
#include <atlimpl.cpp>
#endif
#include <objbase.h>

//#include "HModemSvr.h"
//#include "HModemSvr_i.c"

//#include "IHartComm_i.c"

//#include "HUtil_i.c"
 
#include "ddlCommSimminfc.h"
#include "DDLCommErr.h"

#include "registerWaits.h"
#include "logging.h"

#include "HARTsupport.h"

/*Vibhor 040204: Start of Code*/

#include "Mainfrm.h"

/*Vibhor 040204: End of Code*/

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "HartTrans.h"	// stevev 16apr10 - part of getting rid of ddldevice.h

//#define DEBUG_TRANSACTION
//#define MSG_IS_BINARY

#ifndef IS_OLE	
				/*********  Entire File **************/
/////////////////////////////////////////////////////////////////////////////
// CdocSimmInfc


CdocSimmInfc::CdocSimmInfc(CDDIdeDoc* pD) : /*pDoc(pD),*/ thePktQueue(), hstAddr(2) // # initial pkts
{
	m_pDoc = pD,
	thisIdentity.clear();
	commHndl = NULL;
	
	m_CommStatus = commNC;// all of comm state rolled into one
	m_hadConfigChanged  = false;
	m_skipCount         = 0;
	m_pMainFrm	= 0;	//Added by ANOOP 19APR2004

	m_pending    = false;
/*Vibhor 120204: Start of Code*/
	
	m_bMoreStatusSent = false;
	thePktQueue.initialize(4);
/*Vibhor 120204: Start of Code*/
	if ( m_pMainFrm == NULL )
		m_pMainFrm = (CMainFrame*)AfxGetApp()->m_pMainWnd; 
}

CdocSimmInfc::~CdocSimmInfc()
{
	DisconnectNamedPipe(commHndl);
	CloseHandle(commHndl);
	commHndl = NULL;
	thePktQueue.destroy();
	LOGIT(CLOG_LOG,L"SimmInfc delete called.\n");
}


void CdocSimmInfc::setHostAddress(BYTE ha)// nop in simulation
{
	hstAddr = ha;
	return;
}; 

/**** stevev 25jan06 --- use DDLBaseComm version of this ****************************************/
/////////////////////////////////////////////////////////////////////////////
// CdocSimmInfc - virtual base class required functions  hCcommInfc
// stevev 2/26/04 - severly modified */
// The PUSH INTERFACE - see notes at end of "ddbCommInfc.h"
// stevev 18nov05 
//void  CdocSimmInfc::notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged)
//{	
//	hCitemBase* pIB = NULL;
//	hCVar*      pIV = NULL;
//	itemIDlist_t::iterator itmIT;
//	
//    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd(); // POB, 2 Aug 2004
//	HWND hwndMainWindow   = pMainWnd->GetSafeHwnd();		// POB, 2 Aug 2004
//
//	// deal with the copy-over first
//	if ( (isChanged == IS_changed)  &&						/* value change */
//		 (pDoc != NULL) && (pDoc->pCurDev != NULL) &&	/* we have pointers to work with */
//		 (SUCCESS == (pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB))) && /* item*/
//		 (pIB != NULL) && ( pIB->getIType() == iT_Variable)  ) // only variables are copied
//	{
//		pIV = (hCVar*)pIB;
//		pIV->CancelIt();
//	}
//
//    if (inActionPkt)
//    {
//    	switch (isChanged)
//    	{
//    	case IS_changed: /* same as original */
//    		{// stevev 23jan06 - added insertUnique
//				for ( itmIT = pstActPktList.begin(); itmIT != pstActPktList.end(); ++itmIT)
//				{ if ( (*itmIT) == changedItemNumber) break;  }
//				if ( itmIT == pstActPktList.end() )// not found
//				{
//    				pstActPktList.push_back(changedItemNumber);
//				}
//    		} 
//			break;
//		case STR_changed:
//			{// stevev 23jan06 - added insertUnique
//				for ( itmIT = pstActStrList.begin(); itmIT != pstActStrList.end(); ++itmIT)
//				{ if ( (*itmIT) == changedItemNumber) break;  }
//				if ( itmIT == pstActStrList.end() )// not found
//				{    				
//    				pstActStrList.push_back(changedItemNumber);
//				}
//			}
//			break;
//    	case STRT_actPkt:	/* start holding information */
//    		{/* stevev 23jan06 - try to stack the action packets */
//				ActionPktEntryCnt++;
//#ifdef _DEBUG
//	LOGIT(CLOG_LOG,"    ActionPkt lockout re-entry (now %d)\n",ActionPktEntryCnt);
//#endif
//				//				LOGIT(CERR_LOG,"ERROR: post action packet start w/o termination.\n");
//    			//				pstActPktList.clear();pstActStrList.clear();
//    			inActionPkt = true; // redundant
//    		} break;
//    	case END_actPkt:    /* act on held information */
//    		{				
//				/* stevev 23jan06 - try to stack the action packets */
//				ActionPktEntryCnt--;
//#ifdef _DEBUG
//	LOGIT(CLOG_LOG,"    ActionPkt lockout re-moval (now %d)\n",ActionPktEntryCnt);
//#endif
//				if ( ! ActionPktEntryCnt )// reached zero
//				{
//    				if (hwndMainWindow != NULL)
//					{
//						// structure first
//						for ( itemIDlist_t::iterator iT = pstActStrList.begin(); 
//							  iT != pstActStrList.end();                ++iT    )
//						{
//							::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, (*iT), NULL);
//						}
//						// then values
//						for (  iT = pstActStrList.begin();  iT != pstActStrList.end();     ++iT)
//						{
//							if ( (*iT) != DEVICE_RESPONSECODE   &&
//  								 (*iT) != DEVICE_COMM48_STATUS	&&
//								 (*iT) != DEVICE_COMM_STATUS    )
//							::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, (*iT), NULL);
//							// else do not send the message
//						}
//					}// else - no window to sent to - just exit
//
//    				inActionPkt = false; // redundant
//    				pstActPktList.clear();  pstActStrList.clear();
//				}// else not zero yet, keep going
//    		}break;
//    	case NO_change:  /* 18nov05 - no longer a legal input. */
//		default:
//			{
//				; // no op eration
//			}
//			break;
//    	}// endswitch
//    }
//    else /* not in packet's    hold till actions complete */
//    {
//    	switch (isChanged)
//    	{
//    	case IS_changed: /* value has changed */
//    		{    
//    			if (hwndMainWindow != NULL)
//    			{
//  				::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, changedItemNumber, NULL);
//				g_chWriteEnable = false;
//  				}		
//    		} 
//			break;
//		case STR_changed:
//			{
//    			if (hwndMainWindow != NULL)
//    			{
//  				::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, changedItemNumber, NULL);
//  				}	
//			}
//			break;
//    	case STRT_actPkt:	/* start holding information */
//    		{	inActionPkt = true; 
//    			pstActPktList.clear();pstActStrList.clear();
//				/* stevev 23jan06 - try to stack the action packets */
//				ActionPktEntryCnt = 1;
//    		} break;
//    	case END_actPkt:    /* act on held information */
//    		{
//    			LOGIT(CERR_LOG,"ERROR: post action packet termination w/o start.\n");
//    			inActionPkt = false; // redundant
//    			pstActPktList.clear();pstActStrList.clear();
//				/* stevev 23jan06 - try to stack the action packets */
//				ActionPktEntryCnt = 0;
//    		}// fall out
//    	case NO_change:  /* same as original */
//		default:
//    		{
//    			;/* no operation */;
//    		}
//    	}// endswitch
//    }// endelse - not in a post action packet
//}
/* stevev - end mods 2/26/04 */

/*Vibhor 120204: Start of Code*/

// this is an asycronous callback from the dispatcher
// the dispatcher handles the internal data acquasition and then calls this function	
void  CdocSimmInfc::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
#ifndef _CONSOLE
	RETURNCODE rc = SUCCESS;
    unsigned int cmdNo;		//stevev 20sep07-need wide command numbers...
	wstring   hs;
	hCitemBase* pIB = NULL;
	hCVar* pVr = NULL;
	hCBitEnum *pBe = NULL;
	BYTE byCommResp = 0;

	

            /*stevev - 2-13-04 modified Vibhor's NotifyCmd48 code */
            // just in constructor   bool m_bMoreStatusSent = false;
    
            /*<START>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/
  /*Vibhor 190304: Start of Code */
  /*      if(m_skipCount == 0)
            {
              if(NULL != m_pDoc->ptrCMD48Dlg) // Reset the suppression flag 
                {
                    m_pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();
                }
            }
  */
  /*Vibhor 190304: End of Code*/
            /*<END>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/


	/*stevev - 2-13-04 modified Vibhor's NotifyCmd48 code */
	/* moved itialization to  class level */
	if ( thisMsgCycle.pCmd )
	{
		cmdNo = thisMsgCycle.pCmd->getCmdNumber();
	}
	else
	{
		cmdNo = 0xff;
	}
	/* temporary */
	LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"     Cmd:%03d RC:0x%02X  DS:0x%02X\n", 
											cmdNo, thisMsgCycle.respCdVal, thisMsgCycle.devStatVal);
		
	if (thisMsgCycle.respCdVal != 0)
	{
		if (thisMsgCycle.respCdVal < 0x80)// isa response code
		{
				//hCRespCodeList		respCds;
			unsigned long t = 0;
			unsigned long c = thisMsgCycle.respCdVal;// stevev 4sep07 - moved from below 
/* stevev 07mar05 - brought up to ddlCommInfc standards */
			for ( hCRespCodeList::iterator iT = thisMsgCycle.respCds.begin(); 
															iT < thisMsgCycle.respCds.end(); iT++)
			{// iT isa ptr2a hCrespCode
				if (iT->getVal() == c)
				{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
					hs = iT->getDescStr();
#else
					hs = iT->get_DescStr();
#endif
					t = iT->getType();// stevev 4sep07 - moved from below to only fill when valid
					break;// out of for loop
				}
			}
/*Vibhor 220104: Start of Code*/
/*Decided fix for PAR 5299 "Command Not Implemented not being logged",
In realtime this won't generally happen,and its more intended towards,
the DD Developer usage of SDC. Since the standard response codes are
not available anywhere,	we need to just log the response code returned.	
*/
                if (hs.empty()) // not found
			{			
				hs = M_UI_NO_RESP_CD;
				/* the stuff that was here gets done in the next block */
			}
			// else use what we found above
/*Vibhor 220104: End of Code*/
									
			wchar_t x[MAX_PATH];
				_stprintf(x,M_UI_NO_RESP_CD_FORMAT,cmdNo,thisMsgCycle.respCdVal);
			hs += x;

            /*Vibhor 060204: Start of Code */
            /*Decided that we'll just show a Message Box for RespCode and Comm Status*/

			/* stevev 8feb05 - if the response code has no type (==0) then we will 
				set it according to spec 307 */
			// moved it above to only fill when valid::>unsigned long t = iT->getType();
			// moved it above ::>unsigned long c = thisMsgCycle.respCdVal;
			if (t==0)
			{/* code moved to response code static so all can use */
				t = hCrespCode::getType((unsigned char)(c & 0x00ff));
			}  
            /*Vibhor 060204: End of Code */
            LOGIT(CLOG_LOG|UI_LOG,L"%s\n", hs.c_str());
				hs += M_UI_CONTINUE_QUEST;

			/* it has a type: warnings do not pop up a box, they just get Logged*/
										// stevev 5sep07-use appCommEnabled to try & stop re-entry
			if ( RESPCD_ISERROR(t) && appCommEnabled())
			{// stevev 9aug07 - fix for par 709
/* skip for now
				int r = AfxMessageBox(hs.c_str(),MB_YESNO | MB_ICONEXCLAMATION  );
				if ( r == IDCANCEL  || r == IDNO )
				{	
					CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd; 		
					HWND hwndMainWindow = pMainWnd->GetSafeHwnd(); 

    				if (hwndMainWindow != NULL)
    				{
						// stevev 5sep07- - turn some stuff off first
						disableAppCommRequests();
						DEBUGLOG(CLOG_LOG,"Notify cmd 48 disables appComm\n");
    					::PostMessage(hwndMainWindow, WM_SDC_CLOSEDEVICE, NULL, NULL);
    				}
				}
**/
			}  
/* stevev 07mar05 - end of mods */
    

				/* VMKP 300104 Uddate the Response code here (7 bits of the first byte).
				  This will come only when No communicaton bit is set in the response
				  code */
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					//Commented by ANOOP since we are not displaying the response code
/*Vibhor 220204: Start of Code*/
//Uncommented : This will clear the Comm Status Tab
					m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(0);//sjv - changed to zero
/*Vibhor 220204: End of Code*/
				}		
				
			}
			else // isa comm error
			{// symbol DEVICE_COMM_STATUS
/*Vibhor 060204: Start of Code*/
				byCommResp = thisMsgCycle.respCdVal & ~ 0x80;

				rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB);
				if ( rc == SUCCESS && pIB != NULL )
				{
					pVr = (hCVar*) pIB; // a cast operator
					if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
					{// make it a bit enum
						pBe = (hCBitEnum*)pVr;
						if (pBe)
						{
							unsigned char msk = 1;
							do
							{// for each bit set
								if ( byCommResp & msk )
								{
									rc = pBe->procureString(msk, hs);
									if ( rc == SUCCESS && ! hs.empty() )
									{//	log the string
						
										AfxMessageBox(hs.c_str());
						
										LOGIT(UI_LOG,"Comm Status: %s",hs.c_str());
									}
									else
									{
                                        TCHAR strBuff[85];
                                        _stprintf(strBuff,M_UI_NO_RESP_CD_4_BIT, msk,cmdNo);
										AfxMessageBox(strBuff);

										LOGIT(UI_LOG,"No Description Avaliable for %d in Comm Status",msk);
									}
								}// else - not set, don't output string
							}while( (msk<<=1)  != 0);
						}
						else
						{
							LOGIT(CERR_LOG,"Comm Status: failed to cast from hCVar.");
						}
					}
					else
					{
						LOGIT(CERR_LOG,"Comm Status: failed to cast from hCItemBase to bit enum.");
					}

				}/*Endif rc = SUCCESS*/

				LOGIT(CLOG_LOG|UI_LOG,"     Cmd:%d  RC:0x%02X Comm error response code",cmdNo, thisMsgCycle.respCdVal);
				/* VMKP 300104 Uddate the Communication status (7 bits of the first byte).
				  This will come only when communicaton bit is set in the response
				  code */
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(byCommResp);  //MOdifed by ANOOP17FEB2004
				}
			}/* end else  sa comm error */

		}/*endif thisMsgCycle.respCdVal != 0*/
		else
		{
	//Resp Code = 0 , so no Comm Error,clear the comm lights in dialog	, this clears previously set bits if, any
			if(NULL != m_pDoc->ptrCMD48Dlg)
			{
				m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(thisMsgCycle.respCdVal);
			}

		}

		if ( ! m_hadConfigChanged )
		{// haven't had a config changed
            if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
            {// haven't had one, have one now
LOGIT(CLOG_LOG,"CONFIG: new Config Changed.\n");
                // leave it set, it'll be handled later
                /* stevev - 2/17/04 - callback to mainframe for event */
                if (m_pMainFrm)
                {
					rc = m_pMainFrm->ConfigChanged();
     				if ( rc == APP_CMD_RESPCODE_ERR )// unexpected but handled
					{// clear it for further processing
						thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); 
					}
					else
     				if (rc == SUCCESS )// expected and handled  
                    {  
/* held like ddlCommInfc.cpp
 !>                     if (m_pMainFrm->m_MainFrameState == FS_ISCOMMITTING)// it is expected
 !>                     {
 !>                         thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); // don't pop-up for it
 !>                         // leave ! m_hadConfigChanged
 !>                     }
 !>                     else
					{

*/                        m_hadConfigChanged = true;      // event - config changed to on
						// clear it for further processing
						thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); 
/* hold for now - - - - - from ddlCommInfc.cpp  - - - - - - 
     
 !>                         / * VMKP added on 020404:  Rebuild the menu if unexpected Config change
 !>                                  bit is set,  May be a secondary configuration might have changed
 !>                                 something in the device * /
 !>                         // else we don't have the pointers to do this yet
 !>                         HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);
 !>                         if (hwndMainWindow != NULL)
 !>                         {// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
 !> 
 !>                             ::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, NULL, NULL);
 !>                         }
 !>                         / * VMKP added on 030204 * /
 !> 
 !>                     }
 !>                     if (m_pMainFrm->ConfigChanged() != SUCCESS )// let it deal with it
 !>                     {
 !>                         m_hadConfigChanged = false;     // event - config changed to on 
- - - - - - - - - - - - - - - - - - - - - - - */
					}
                    //else - couldn't handle it; leave it so we will deal with it later
                }
                /* stevev - 2/17/04 - end */
            }
            // else - haven't had one, still don't have one
            // no operation
        }
		else 
		{// had one, filter the configuration changed bit
			if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
			{// had one, have one now
LOGIT(CLOG_LOG,"CONFIG: another Config Changed.\n");
				thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); // clear it for further processing
			}
			else // had one, don't have one now
			{
				LOGIT(UI_LOG|CLOG_LOG,"Configuration Changed bit has Cleared.");
				if (m_pMainFrm)
                {
     				m_pMainFrm->ConfigUNChanged();
				}
                m_hadConfigChanged = false;
                if(NULL != m_pDoc->ptrCMD48Dlg)
                {
                    m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
                }
			}
		}
		if (m_pDoc == NULL || m_pDoc->pCurDev == NULL)
		{
			LOGIT(CERR_LOG,"ERROR: notify interface call without a path to the device.\n");
			return;
		}

		if((m_bMoreStatusSent == true) && (cmdNo == 48))
		{
			m_bMoreStatusSent = false;
		}
		else //either bMoreStatus == false OR cmdNo != 48
		{
			if((m_skipCount == 0) && (thisMsgCycle.devStatVal != 0))
			{
				//If Dialog exists update Device Status
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
				}
				rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
				if( rc == SUCCESS && pIB != NULL )
				{
					pVr = (hCVar*) pIB; // a cast operator
					if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
					{// make it a bit enum
						pBe = (hCBitEnum*)pVr;
						if (pBe)
						{
							unsigned char msk = 1;
							do
							{// for each bit set
								if ( thisMsgCycle.devStatVal & msk )
								{
									if(msk == DS_MORESTATUSAVAIL && 
										thisMsgCycle.devStatVal != DS_MORESTATUSAVAIL)// sjv 2/13/04
										/* if this is the only bit, give the user option to suppress*/
									{
										continue; // loop the while
									}
									rc = pBe->procureString(msk, hs);
									if ( rc == SUCCESS && ! hs.empty() )
									{//	log the string
										
										m_pDoc->m_strDisplay = hs.c_str();
						
										CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd;

										pMainWnd->SendMessage(WM_NOTIFY_RESPCODE);

										if(true == m_pDoc->m_bIgnoreRespCode)
										{
											m_skipCount = m_pDoc->m_uSuppressionCnt;
											if(true == m_pDoc->m_bIgnoreRespCode)
											{			
												/* VMKP 300104 add an indicator in the extended device status
												tab saying "Updations Suppressed"  */
 /*Vibhor 180304: Start of Code*/
 /*	from ddlCommInfc.cpp 						if(NULL != pDoc->ptrCMD48Dlg)
												{
													pDoc->ptrCMD48Dlg->UpdateSuppressMessageTab();	//27FEB2004 Modified  by ANOOP since the message suppressed was not getting displayed properly.
												}
 */
 /*Vibhor 180304: End of Code*/
											}

											break;
										}

										LOGIT(UI_LOG,"Device Status: %s",hs.c_str());
									}
									else
									{
										LOGIT(UI_LOG,"No Description Avaliable for %d in Device Status",msk);
									}
								}// else - not set, don't output string
							}while( (msk<<=1)  != 0);
						}
						else
						{
							LOGIT(CERR_LOG,"Device Status: failed to cast from hCVar.");
						}
					}
					else
					{
						LOGIT(CERR_LOG,"Device Status: failed to cast from hCItemBase to bit enum.");
					}			
				}//Endif rc == SUCCESS

				if(thisMsgCycle.devStatVal & DS_MORESTATUSAVAIL)// We aren't in our owm Command 48 reply
				{
					//Send Command 48
					rc = m_pDoc->pCurDev->sendCommand(48,-1);
					m_bMoreStatusSent = true;

				}/*Endif DS_MORESTATUSAVAIL*/

			}
			else //either m_skipCount != 0 OR thisMsgCycle.devStatVal = 0
			{

				if(thisMsgCycle.devStatVal == 0)
				{
					m_skipCount = 0;
					
					if(NULL != m_pDoc->ptrCMD48Dlg)
					{			
						m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
					}
  /*Vibhor 180304: Start of Code*/
  /* from ddlCommInfc.cpp 
					if(NULL != pDoc->ptrCMD48Dlg)
					{
						pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();

					}
  */
  /*Vibhor 180304: End of Code*/
				}
				else //simply decrement the skipCount
				{
					m_skipCount--;
				}

			}

		}/*End else  bMoreStatus == false OR cmdNo != 48*/

		if(cmdNo == 48)
		{

			LOGIT(UI_LOG,"Received Command 48 response");
			// need to expand the bits here

		/* VMKP 300104 Update the Command 48 response bytes (Extended Device status bytes)
			here, if u reach here you need to remove the "supressions removed" message
			from the extended device status tab and the update it */
			if(NULL != m_pDoc->ptrCMD48Dlg)
			{			
/*Vibhor 130204: Start of Code*/	
			//was	HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Status"));

				HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Condition"));
/*Vibhor 130204: End of Code*/				
				if(NULL != hwndCMD48Window)  //Added by ANOOP fix for crashing 15FEB2004
				{
					m_pDoc->ptrCMD48Dlg->m_hWnd=hwndCMD48Window;

				
					if(false == m_pDoc->ptrCMD48Dlg->m_ExtendedTabCreated )
					{
						::PostMessage(hwndCMD48Window, WM_CREATE_EXTENDEDTAB, NULL, NULL);
					}
					else
					{
						::PostMessage(hwndCMD48Window, WM_UPDATE_EXTENDEDTAB, NULL, NULL);
					
					}
				}			
			}			
		}
#endif // _CONSOLE			
}/*End notifyRCDScmd48*/

/*Vibhor 120204: End of Code*/

// gets an empty packet for generating a command 
//		(gives caller ownership of the packet*)
//		pass back ownership with Sendxxx OR putMTPacket if command send aborted
//
hPkt* CdocSimmInfc::getMTPacket(bool pendOnMT)
{
	hPkt* pR = thePktQueue.GetEmptyObject(pendOnMT);
//clog <<"got  MT   Pkt "<<hex << pR   << dec << endl;
	return pR;
}

// returns the packet memory back to the interface 
//		(caller relinguishes ownership of the packet*)
//							
void  CdocSimmInfc::putMTPacket(hPkt* pPkt)
{	
//clog <<"return MT Pkt "<<hex << pPkt << dec << endl;
    thePktQueue.ReturnMTObject( pPkt );
}


RETURNCODE /* the pipe interface will run in sycronous mode */
CdocSimmInfc::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = SUCCESS, trc;

	// pipe format:
	// CC_BB_DDDDDDDD
	// where CC is ascii hex value of the command number
	//   and BB is ascii hex value of the byte count
	//   and DD is ascii hex values for each data byte in bytecount
	//   and _  is an ascii space

	DWORD  cbWritten, cbRead;
	BOOL   isSuccess;	
	HANDLE localHand = (HANDLE)commHndl;
	BYTE writeBuffer[512];
	BYTE read_Buffer[512];
	hPkt* pPkt = NULL;
	BYTE* pBuf = NULL;;
	BYTE  cntBuf;
	CString strTitle;

	HWND hwndMainWindow = 0;

	if (m_pMainFrm != NULL)
		hwndMainWindow = m_pMainFrm->GetSafeHwnd(); // POB, 2 Aug 2004

// New code for Packet dump Jeyabal
	CClient *pSocket = NULL;
#ifndef _CONSOLE
	if (m_pDoc)
		pSocket = m_pDoc->GetSocket();
// End - New code for Packet dump Jeyabal
#endif // _CONSOLE

	if (cmdNum < 0 || pData == NULL )
	{
		rc = APP_PARAMETER_ERR;
	}
	else
	{
		pPkt = getMTPacket();
		if ( pPkt == NULL )
		{
			cerr <<"ERROR: CdocCommInfc::SendCmd could NOT get an empty packet."<<endl;
			return APP_MEMORY_ERROR;
		}
		// else continue working

		// convert back to packet
		pPkt->cmdNum = cmdNum;
		pPkt->dataCnt= dataCnt;
		memcpy(&(pPkt->theData[0]),pData,dataCnt);

		DWORD cb ;

		trc = pkt2pipe(pPkt, writeBuffer, cb);// cb returns ascii length

		stxSocketDump(pPkt);
		/* commented-out code removed 18may07 by stevev - earlier versions have content */

writeBuffer[cb] = '\0';
LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Sending  |%s|\n",writeBuffer);  // was=>  << writeBuffer << "|"<<endl;
		cntBuf = (BYTE)(cb & 0xff);// max length is 255!

		if ( trc == SUCCESS )
		{				
			isSuccess = WriteFile( 
				  localHand,			// pipe handle 
				  writeBuffer,			// message 
				  cntBuf,				// message length 
				  &cbWritten,           // bytes written 
				  NULL);                // not overlapped 
			if (! isSuccess) 
			{//MyErrExit("WriteFile"); 
				cerr << "ERROR: file WRITE failed."<<endl;
				rc = APP_ER_FAILED;
			}
			else
			{
				rc = SUCCESS;
			}
		}
		else
		{
			rc = trc;
		}
			
    	if (hwndMainWindow != NULL)
    	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    		::PostMessage(hwndMainWindow, WM_SDC_GLOW_RED, NULL, NULL);			
			systemSleep(2);
		}

	}// end else - params ok
	if ( rc == SUCCESS) //write went OK
	{ 	// we have apPkt
		do 
		{ // Read from the pipe. 

			isSuccess = ReadFile( 
				 localHand,	// pipe handle 
				 read_Buffer,	// buffer to receive reply 
				 PIPEBUFFERSIZE,// size of buffer 
				 &cbRead,		// number of bytes read 
				 NULL);			// not overlapped 

			if (! isSuccess && GetLastError() != ERROR_MORE_DATA) 
			{
				cerr <<"ERROR: reading from pipe."<<endl;
				cbRead = 0;
				rc = APP_ER_FAILED;
				break; 
			}
			else
			{
				//dataCnt = cbRead;
				pPkt->dataCnt = (BYTE)(cbRead & 0x00ff);// char count - max 255
				trc = pipe2pkt(read_Buffer, cbRead, pPkt);
				rc  = SUCCESS; // force an abort
			}
			read_Buffer[cbRead] = '\0'; // be sure it's terminated
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"RECEIVED |%s| message.\n",read_Buffer); // was =>  <<read_Buffer<<"| message."<<endl;
			//		  TRACE(_T("RECEIVED |%s| message.\n"),pData);

		} while (! isSuccess);  // repeat loop if ERROR_MORE_DATA 

		// parse the packet
		if (cpRcvService != NULL && trc == SUCCESS)
		{
			ackSocketDump(pPkt);
			/* commented-out code removed 18may07 by stevev - earlier versions have content */

			/*  stevev 01mar05 - prevent a cmd38 following a cmd 38 in simulation 
			the user can't change it fast enough */
			if (pPkt->cmdNum == 38)
			{
				pPkt->theData[1] &= 0xBF; // clr the cnfg chng bit
			}	
			/*stevev 06jul09 - changed way we turn off the light...via timer...
			if (hwndMainWindow != NULL)
    		{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    			::PostMessage(hwndMainWindow, WM_SDC_GLOW_GREEN, NULL, NULL);
    		}
			****/
			pPkt->transactionID = cmdStatus.transactionID;
			cpRcvService->serviceReceivePacket(pPkt,&cmdStatus);
		}
		else
		if(cpRcvService == NULL)// from '!='  on 19may07 sjv
		{
		   cerr <<"ERROR: no service function to the receive packet."<<endl;
		   rc = APP_PROGRAMMER_ERROR;
		}
		else
		{
			rc = trc;// to return the packet parsing error
		}
	}
	// else - write failed, no reason to try a read...

	if ( pPkt != NULL )
	{
		putMTPacket(pPkt);
	}
	return rc;

}


// puts a packet into the send queue  		(takes ownership of the packet*)
RETURNCODE CdocSimmInfc::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = FAILURE;
	if ( pPkt == NULL)
	{
		cerr <<"ERROR: SendPkt has a null for a packet - discarding."<<endl;
		//return rc;
	}
	else
	{
		cmdStatus.transactionID = pPkt->transactionID;
		rc = SendCmd(pPkt->cmdNum,pPkt->theData,pPkt->dataCnt,timeout,cmdStatus);
	}
	putMTPacket(pPkt);
	return rc;
}


// puts a packet into the send queue  		(takes ownership of the packet*)
RETURNCODE CdocSimmInfc::SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{// does nothing for now
	return SendPkt(pPkt, timeout, cmdStatus);
}


// how many are in the send queue ( 0 == empty )
int CdocSimmInfc::sendQsize(void)
{
cerr << "CdocSimmInfc::sendQsize needs work..." << endl;
	return 9999;
}

// how many will fit	    
int	CdocSimmInfc::sendQmax(void) 
{
cerr << "CdocSimmInfc::sendQmax needs work..." << endl;
	return 2;
}
	
// setup connection (called after instantiation of device)	
RETURNCODE  CdocSimmInfc::initComm(void)
{
	RETURNCODE rc = SUCCESS, status;
	LPTSTR lpszPipename =  PIPENAME ;
	
	HANDLE Hand;
	bool   isSuccess;
	DWORD  dwErr = 0;
	TCHAR errMsg[128];

	while (1) 
	{ 
		status    = ST_OK;
		isSuccess = true;
		Hand = CreateFile( 
			lpszPipename,   // pipe name 
			GENERIC_READ |  // read and write access 
			GENERIC_WRITE, 
			0,              // no sharing 
			NULL,           // no security attributes
			OPEN_EXISTING,  // opens existing pipe 
			0,              // default attributes 
			NULL);          // no template file 

		// Break if the pipe handle is valid. 

		if (Hand != INVALID_HANDLE_VALUE) 
			break; // success

		// Exit if an error other than ERROR_PIPE_BUSY occurs. 

		errMsg[0] = '\0';
		if ((dwErr = GetLastError())   != ERROR_PIPE_BUSY)          
		{			
			FormatMessage( 
				/*FORMAT_MESSAGE_ALLOCATE_BUFFER |	/ DWORD dwFlags*/
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,								/*LPCVOID lpSource*/
				dwErr,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				errMsg,
				128,
				NULL 
				);

			LOGIT(CERR_LOG|UI_LOG,L"ERROR: Could not open pipe.\n       %s\n",errMsg);
			status = DDL_COMM_NO_RESPONSE;
			break; // out of while
		}
		else // All pipe instances are busy, so wait for 5 seconds
		if (! WaitNamedPipe(lpszPipename, 5000) ) 
		{
		  LOGIT(CERR_LOG,"ERROR: Wait pipe didn't connect - Shows Busy.\n");
		  status = DDL_COMM_NO_RESPONSE;
		  break; // out of while         
		}
		// else loop and try again
	} //wend
	if ( ! status )
	{
		commHndl = Hand; 
		rc = SUCCESS;
	}
	else
	{
		commHndl = 0L;
		rc = DDL_COMM_NO_RESPONSE;
	} 

	m_pMainFrm = (CMainFrame *)(AfxGetApp()->m_pMainWnd);
	return rc;
}

RETURNCODE  CdocSimmInfc::shutdownComm(void)
{
	RETURNCODE rc = SUCCESS;

	DEBUGLOG(CLOG_LOG,L"< doc-comm infc shutting down.\n");
	if (m_CommStatus > commNC )	// shuttingdown|shutdown  was:  ! m_enableComm)
	{// we're already shutdown, just leave
	DEBUGLOG(CLOG_LOG,L"< communications interface already gone (*)\n");
		return rc;
	}// else do the sequence

	DisconnectNamedPipe(commHndl);
	CloseHandle(commHndl);
	commHndl = NULL;
	thePktQueue.destroy();
	
	m_CommStatus = commShuttingDown;

	DEBUGLOG(CLOG_LOG,L"< communications interface is done. (19)\n");
	return rc;
}

RETURNCODE CdocSimmInfc::GetIdentity(Indentity_t& retIdentity, BYTE pollAddr)
{
	RETURNCODE rc = FAILURE;

	if ( m_pDoc == NULL )
	{
		return FAILURE;
	}

	//rc = pDoc->CnctToDevice(commHndl);// commHndl is a return value


    RETURNCODE status = ST_OK;
    CString error;
	DWORD lstErr = 0;
	TCHAR errMsg[128];


	DWORD  cbWritten, cbRead, dwMode;
	CHAR   chBuf[PIPEBUFFERSIZE];
	BOOL   isSuccess;	
 
	LPVOID lpvMessage = "00 00"; // command zero with no data
	HWND hwndMainWindow = 0;

   memset(chBuf,0,PIPEBUFFERSIZE);// prevent uninitialized memory warnings
   
	if (m_pMainFrm != NULL)
		hwndMainWindow = m_pMainFrm->GetSafeHwnd(); // POB, 2 Aug 2004

	dwMode   = PIPE_READMODE_MESSAGE; 
	isSuccess= SetNamedPipeHandleState( 
	  commHndl,
	  &dwMode,  // new pipe mode 
	  NULL,     // don't set maximum bytes 
	  NULL);    // don't set maximum time 


	if (!isSuccess) 
	{
		lstErr = GetLastError();
		FormatMessage( 
				/*FORMAT_MESSAGE_ALLOCATE_BUFFER |	/ DWORD dwFlags*/
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,								/*LPCVOID lpSource*/
				lstErr,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				errMsg,
				128,
				NULL 
				);
		cerr << "ERROR: SetNamedPipeHandleState failed.\n       "<<errMsg<<endl;
		status = DDL_COMM_NO_RESPONSE;      
	}

	LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Sim Send Pkt:|%s|\n",(char*)lpvMessage);		
	// Send a message to the pipe server. 
	isSuccess = WriteFile( 
	  //m_pComHARTSvr,          // pipe handle 
	  commHndl,
	  lpvMessage,             // message 
	  strlen((char*)lpvMessage) + 1, // message length 
	  &cbWritten,             // bytes written 
	  NULL);                  // not overlapped 
		

	if (! isSuccess) 
	{		
		lstErr = GetLastError();
		FormatMessage( 
				/*FORMAT_MESSAGE_ALLOCATE_BUFFER |	/ DWORD dwFlags*/
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,								/*LPCVOID lpSource*/
				lstErr,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				errMsg,
				128,
				NULL 
				);
	  cerr << "ERROR: file WRITE failed.\n       "<<errMsg<<endl;
	  status = DDL_COMM_NO_RESPONSE;
	}
	else
	{ 
    	if (hwndMainWindow != NULL)
    	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    		::PostMessage(hwndMainWindow, WM_SDC_GLOW_RED, NULL, NULL);			
			systemSleep(1);
		}
	   do 
	   { // Read from the pipe. 

		  isSuccess = ReadFile( 
			 //m_pComHARTSvr,	// pipe handle 
			 commHndl,
			 chBuf,			// buffer to receive reply 
			 PIPEBUFFERSIZE,// size of buffer 
			 &cbRead,		// number of bytes read 
			 NULL);			// not overlapped 

		  if (! isSuccess && (lstErr=GetLastError()) != ERROR_MORE_DATA) 
		  {		
				lstErr = GetLastError();
				FormatMessage( 
						/*FORMAT_MESSAGE_ALLOCATE_BUFFER |	/ DWORD dwFlags*/
						FORMAT_MESSAGE_FROM_SYSTEM | 
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,								/*LPCVOID lpSource*/
						lstErr,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						errMsg,
						128,
						NULL 
						);
			  cerr <<"ERROR: reading from pipe.\n       "<<errMsg<<endl;
			 break; 
		  }

		  // Reply from the pipe is written to STDOUT. 

		  //if (! WriteFile(GetStdHandle(STD_OUTPUT_HANDLE), 
		//	 chBuf, cbRead, &cbWritten, NULL)) 
		  //{
		//	 break; 
		  //}
	
		  chBuf[cbRead] = '\0'; // be sure it's terminated
		  LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Sim Recv Pkt:|%s| Identity message. (%d bytes)\n",chBuf, cbRead);
								// was =>    <<chBuf<<"| message. ("<< cbRead << " bytes)"<<endl;
#ifdef _DEBUG
		  wstring tws;string ts; ts = chBuf; tws = AStr2TStr(ts);
		  TRACE(_T("Received |%s| Identity message.\n"),tws.c_str());
#endif
			hPkt locPkt;
#ifdef MSG_IS_BINARY
			//0 == 0, 1=cmdNumHi, 2=cmdNum, 3=Bcnt, 4 = status, 5 = respcd,
			locPkt.cmdNum = (chBuf[1]<<8 )|chBuf[2];
			locPkt.dataCnt= chBuf[3];
			memcpy(&(locPkt.theData[0]),&(chBuf[4]),cbRead-4);
#else // msg is ascii

			BYTE tmpDat[6];
			char* ptd = (char*) &(tmpDat[0]);
			/*BYTE PAW 03/03/09*/int tmp, cnt = 0, inS = 6;;// PAW 03/03/09 resolve stack errors with using a BYTE

#define	dat2tmp( x ) tmpDat[0] = chBuf[x];tmpDat[1] = chBuf[x+1];tmpDat[2] = '\0';\
					 sscanf(ptd, "%x" ,&tmp);

			dat2tmp( 0 );
			locPkt.cmdNum = tmp;
			dat2tmp( 3 );// skip space
			locPkt.dataCnt= tmp;

			while (cnt < locPkt.dataCnt)
			{
				dat2tmp( inS );
				locPkt.theData[cnt++] = tmp;
				inS+= 2;
			}			
#endif
		  ackSocketDump(&locPkt);

	   } while (! isSuccess);  // repeat loop if ERROR_MORE_DATA 

	   rc = Array2Identity(cbRead, chBuf); // fills thisIdentity
	   if ( rc == SUCCESS )
	   {
		   //retIdentity = thisIdentity;
		   memcpy(&retIdentity, &thisIdentity, sizeof(Indentity_t));
	   }
	}// endif no errors so far


    return rc;
}

// DDIde specific calls
RETURNCODE CdocSimmInfc::Search(Indentity_t& retIdentity, BYTE& newPollAddr)
{
	cerr << "ERROR: CdocSimmInfc::Search needs a body"<<endl;
	return FAILURE;
}

// conversion routines
RETURNCODE CdocSimmInfc::pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt)
{
	RETURNCODE rc = SUCCESS;
	BYTE* pB = pipeBuf;

	int  tmpInt = -1, c = 0, L;
	char shortStr[5] = {'\0','\0','\0','\0','\0'};

	if ( pipeBuf == NULL || pPkt == NULL || pipeLen < 5 )// 5 is cmd & 00 byte count
	{
		cerr <<"ERROR: parameter is null in parseStr2Msg."<<endl;
		rc = APP_PARAMETER_ERR;
	}
	else
	{// parameters OK
		memset(&(pPkt->theData[0]),0,255);

		if ( pB[pipeLen-1] == '\0' )
		{	 pipeLen--;				}

		//if ( pB[2] == ' ' )
		
		if ( pB[2] == ' ' )
		{	
			L = 2;	}
		else
		if ( pB[3] == ' ' )
		{	
			L = 3;	}
		else
		if ( pB[4] == ' ' )
		{	
			L = 4;	}
		else
		{	
			L = 0;	}
		if ( L > 0 )
		{ 
			shortStr[0] = pB[0]; shortStr[1] = pB[1];
			//c = sscanf(shortStr,"%02x",&tmpInt);
			if ( L > 2 )
			{
				shortStr[2] = pB[2];
				if ( L > 3 )
				{
					shortStr[3] = pB[3];
					c = sscanf(shortStr,"%04x",&tmpInt);
				}
				else
				{
					c = sscanf(shortStr,"%03x",&tmpInt);
				}
			}
			else
			{
				c = sscanf(shortStr,"%02x",&tmpInt);
			}
			if ( c == 1 )
			{
				pPkt->cmdNum = tmpInt;
			}
			else
			{
				pPkt->cmdNum = -1;
				cerr << "ERROR: scanf of |" << shortStr << "| failed with " <<
																	c << " scanned."<<endl;
				rc = APP_COMMAND_ERROR;
			}
			L++;
			pB = &(pB[L]);
			pipeLen -= L;
			if ( pipeLen > 2 )
			{
				if ( pB[2] != ' ' )
				{
					cerr << "ERROR: message buffer has incorrect delimiting. |"<<
																			pB<< "|"<<endl;
					rc = APP_COMMAND_ERROR;
				}
				// else fall through
			}
			else // it had to be 5 or we wouldn't get here
			{
				pipeLen++; // to match later(needs 6)
			}
			shortStr[0] = pB[0]; shortStr[1] = pB[1];

			if ( rc == SUCCESS )// we didn't have a commend read error
			{// continue
				sscanf(shortStr,"%02x",&tmpInt);
				pPkt->dataCnt = tmpInt;
				pB = &(pB[3]);
				pipeLen -= 3;
				if ( pipeLen != tmpInt * 2) // 2 ascii bytes per data byte
				{
					cerr << "ERROR: message buffer has incorrect size. BufSize="<<pipeLen<<
						 " ByteCnt="<< tmpInt << endl;
					rc = APP_COMMAND_ERROR;
				}
				else
				{// good length match, do the data
					int y,z;
					for (y = 0, z = 0; y < pPkt->dataCnt; y++, z+=2)
					{
						shortStr[0] = pB[z]; shortStr[1] = pB[z+1];
						sscanf(shortStr,"%02x",&tmpInt);
						pPkt->theData[y] = tmpInt;
					}
				}
			}
			// else just return the rc
		}
		else
		{
			cerr << "ERROR: message buffer has incorrect delimiting. |"<< pB<< "|"<<endl;
			rc = APP_COMMAND_ERROR;
		}
	}

	return rc;
}
RETURNCODE CdocSimmInfc::pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen)
{
	RETURNCODE retVal = FAILURE;// deal with failure first
	int bufLoc = 0;
	int y, i, L;
	pipeLen = 0;

	if ( pipeBuf != NULL && pPkt != NULL)
	{
		L = y = sprintf((char*)pipeBuf, "%02x %02x ", pPkt->cmdNum, pPkt->dataCnt);// y == 6
		if ( pPkt->cmdNum < 256 && y != 6 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 6, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		if ( pPkt->cmdNum > 255 && pPkt->cmdNum < 4096 && y != 7 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 7, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		if ( pPkt->cmdNum > 4095 && pPkt->cmdNum < 65536 && y != 8 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 8, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		{
			for ( i = 0; i < pPkt->dataCnt; i++ )
			{
				y += sprintf( (char*)(&(pipeBuf[y])),"%02x",pPkt->theData[i]);
			}
			if ( y != ((pPkt->dataCnt*2)+L) )
			{
				LOGIT(CERR_LOG,L"ERROR: sprintf error in pkt2pipe. (expected %d got %d)\n",
					  											((pPkt->dataCnt*2)+L), y );
				pipeBuf[0] = '\0';
				retVal = APP_PARAMETER_ERR;
			}
			else
			{// OK, send it				
				pipeLen = y;
				retVal = SUCCESS;
			}
		}
	}
	// else return failure

	return retVal;
}
#ifndef _CONSOLE
void  CdocSimmInfc::disableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: disable App Comm with no Document.");
	}
	else
	{
		m_pDoc->disableAppCommRequests();
	}
}

void  CdocSimmInfc::enableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: enable App Comm with no Document.");
	}
	else
	{
		m_pDoc->enableAppCommRequests();
	}
}


bool  CdocSimmInfc::appCommEnabled (void)
{

	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: isEnable App Comm with no Document.");
		return false;
	}
	else
	{
		return m_pDoc->appCommEnabled();
	}
}
#else // is console
void  CdocSimmInfc::disableAppCommRequests(void)
{
}
void  CdocSimmInfc::enableAppCommRequests(void)
{
}
bool  CdocSimmInfc::appCommEnabled (void)
{
	return false;
}
#endif //_CONSOLE

/*************************************************************************************************
* clearQueue is used where FlushQueue is needed but all threads pending on done-event must be 
* released.
*  we have an issue with the msgCycle queue too.  There is one there for each of these.
*  we have an issue with all those variables left in the PENDING state too.
*
* >>>>caller should check m_CommStatus (for transition to ShuttingDown) when this function returns
*/
void  CdocSimmInfc::clearQueue(void)
{	
    hPkt*           pSendPkt;

	while (thePktQueue.GetQueueState() > 0)// while not empty
	{
		pSendPkt = thePktQueue.GetNextObject(12);
    	if ( pSendPkt != NULL)
    	{
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Stop)
			{// de-init
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a Stop.\n");
				m_CommStatus = commShuttingDown;
			}
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_RawSnd) // identity interface
			{// ptr is event and we return data in the packet and the caller returns it
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a RawSend.\n");
				if (pSendPkt->m_thrdCntl.threadLPARAM != NULL)
				{
					hCevent::hCeventCntrl* pEc = 
						((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
					if (pEc)
					{
						pEc->triggerEvent();
						delete pEc;
						delete ((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM));
						pSendPkt->m_thrdCntl.threadLPARAM = NULL;
					}// error - do nothing
				}// else no-op
			}// else we b done
    		thePktQueue.ReturnMTObject( pSendPkt );
		}// else an error, skip it
	}//wend more packets left
    thePktQueue.FlushQueue();// just in case

	/**** Now deal with all those variables that are out there pending ***/

    if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
	{
		CVarList* pVlist = (CVarList*)(m_pDoc->pCurDev->getListPtr(iT_Variable));
		if (pVlist != NULL)
		{
			for(CVarList::iterator VLIT = pVlist->begin(); VLIT != pVlist->end(); VLIT++)
			{// ptr 2a ptr 2a hCVar ---- for each device variable
				hCVar* pVar = (hCVar*)(*VLIT);
				if (pVar == NULL)
					continue;// should never happen
				INSTANCE_DATA_STATE_T tSt;
				tSt = pVar->getDataState();
				if ( tSt == IDS_PENDING )
				{
					pVar->markItemState(IDS_INVALID);// force a new read
				}// else, leave it alone
			}//next
		}//else list is unavailable
	}//we are in bas shape, just leave

	/**** and, finally, clear the msgQueue too ***/
	cpRcvService->serviceMsgCycleQue(0);// clear it all
}


RETURNCODE CdocSimmInfc::Array2Identity(int dataCnt, char* DataStr)// fils thisIdentity
{
	RETURNCODE rc = FAILURE;

	// parse the data to an identity structure -- m_currIdentity
	thisIdentity.clear();

#ifdef MSG_IS_BINARY
#pragma message( "CommSimmInfc is Binary." )

	if (dataCnt < 18 || DataStr[0] != '\0' )  // probably an error packet
	{
		clog << "Identity |"<<hex<<DataStr<<dec<<"| message. ("<< dataCnt << " bytes)"<<endl;
		TRACE(_T("Identity |%s| message.\n"),DataStr);
		rc = APP_CMD_RESPCODE_ERR;
	}
	else//      5                                 6                        7
	{	// break it up
		//00 BC ST RC DATA
		//DATA	FE
		//		HH  Mfg						  HH   Mfg				HHHH   Expanded DevType
		//		HH	DevType					  HH   DevType			  
		//		HH	PreAmb					  HH   Reqst-PreAmb		  HH   Reqst-PreAmb
		//		HH  CmdRev-- 5				  HH   CmdRev-- 6		  HH   CmdRev-- 7
		//		HH	DevRev					  HH   DevRev			  HH   DevRev
		//		HH	SoftRev					  HH   SoftRev			  HH   SoftRev
		//		HH	top 5 bits - Hardwr Rev	  5b   Hdwr Rev			  5b   Hdwr Rev
		//			low 3 bits - Signal Code  3b   SignalCd			  3b   SignalCd
		//		HH	FuncFlags				  HH   FuncFlags		  HH   FuncFlags
		//		HHHHHH DeviceID				HHHHHH DeviceID			HHHHHH DeviceID
		//									  HH   Resp-PreAmbl		  HH   Resp-PreAmbl
		//									  HH   Num Dev-Vars		  HH   Num Dev-Vars
		//									HHHH   Config-Chng-Cnt	HHHH   Config-Chng-Cnt
		//									  HH   Extend-Dev Stat    HH   Extend-Dev Stat
		//															HHHH   Mfg Id Code
		int offset  = 4;
		BYTE st     = DataStr[4];
		BYTE respCd = DataStr[5];
		BYTE* pID   = (BYTE*) &(thisIdentity.dwDeviceId);

		if (DataStr[2+offset] != 254)
		{
			cerr << "ERROR: Zero cmd Constant is not in the right place."<<endl;
			rc = DDL_COMM_EXTRACTION_FAILED;
		}
		else
		if (DataStr[6+offset] == 5 || DataStr[6+offset] == 6)
		{	rc = SUCCESS;
			retIdentity.wManufacturer = DataStr[ 3+offset]; 
			retIdentity.wDeviceType   = DataStr[ 4+offset];  
			retIdentity.cUniversalRev = DataStr[ 6+offset];
			retIdentity.cDeviceRev    = DataStr[ 7+offset];
			retIdentity.cSoftwareRev  = DataStr[ 8+offset];
			retIdentity.cHardwareRev  = DataStr[ 9+offset];
			retIdentity.cZeroFlags    = DataStr[10+offset];
			retIdentity.cReqPreambles = DataStr[ 5+offset];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = DataStr[11+offset];
			pID[1] = DataStr[12+offset];
			pID[0] = DataStr[13+offset];

			if (DataStr[6+offset] == 6 )
			{	rc = SUCCESS;
				retIdentity.cRespPreambles= DataStr[14+offset];
				retIdentity.cMaxDevVars   = DataStr[15+offset];
				retIdentity.wCfgChngCnt   =(DataStr[16+offset]<<8)+DataStr[17+offset];
				retIdentity.cExtDevStatus = DataStr[18+offset];
			}
		}
		else
		if (DataStr[6+offset] == 7 )
		{	rc = SUCCESS;
			retIdentity.wDeviceType   =(DataStr[ 3+offset]<<8)+DataStr[ 4+offset];  
			retIdentity.cUniversalRev = DataStr[ 6+offset];
			retIdentity.cDeviceRev    = DataStr[ 7+offset];
			retIdentity.cSoftwareRev  = DataStr[ 8+offset];
			retIdentity.cHardwareRev  = DataStr[ 9+offset];
			retIdentity.cZeroFlags    = DataStr[10+offset];
			retIdentity.cReqPreambles = DataStr[ 5+offset];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = DataStr[11+offset];
			pID[1] = DataStr[12+offset];
			pID[0] = DataStr[13+offset];
			retIdentity.cRespPreambles= DataStr[14+offset];
			retIdentity.cMaxDevVars   = DataStr[15+offset];
			retIdentity.wCfgChngCnt   =(DataStr[16+offset]<<8)+DataStr[17+offset];
			retIdentity.cExtDevStatus = DataStr[18+offset];
			retIdentity.wManufacturer =(DataStr[19+offset]<<8)+DataStr[20+offset]; 
		}
		else
		{// unknown universal rev
			rc = DDL_COMM_UNKNOWN_UNIV_REV;
		}
	}
#else /* message is ascii */
#pragma message( "CommSimmInfc is Ascii." )

	if (dataCnt < 34 || DataStr[0] != '0' || DataStr[1] != '0')  // probably an error packet
	{
		clog << "Identity |"<< DataStr<<"| message. ("<< dataCnt << " bytes)"<<endl;
		TRACE(_T("Identity |%s| message.\n"),DataStr);
		rc = APP_CMD_RESPCODE_ERR;
	}
	else
	{
		// break it up - ascii hex
		//00 BC STRCDATA
		//DATA	FE
		//		HH  Mfg
		//		HH	DevType
		//		HH	PreAmb
		//		HH  CmdRev
		//		HH	DevRev
		//		HH	SoftRev
		//		HH	top 5 bits - Hardware Revision
		//			low 3 bits - Signal Code
		//		HH	FuncFlags
		//		HHHHHH DeviceID
		BYTE tmpDat[6];
		char* ptd = (char*) &(tmpDat[0]);
		/*BYTE PAW 03/03/09*/ int bc, st, /*tmp,*/ htmp;	// using byte causes stack issues PAW
		/*BYTE PAW*/ int respCd;				// using byte causes stack issues PAW
		BYTE* pID   = (BYTE*) &(thisIdentity.dwDeviceId);
		unsigned tmp = 0;

		tmpDat[0] = DataStr[3];tmpDat[1] = DataStr[4];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&bc);

		tmpDat[0] = DataStr[6];tmpDat[1] = DataStr[7];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&st);

		tmpDat[0] = DataStr[8];tmpDat[1] = DataStr[9];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&respCd);

		tmpDat[0] = DataStr[10];tmpDat[1] = DataStr[11];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&tmp);

		if (tmp != 254)
		{
			cerr << "ERROR: Zero cmd Constant is not in the right place."<<endl;
			rc = APP_COMMAND_ERROR;
		}
		else
		{	rc = SUCCESS;
		}
		
		tmpDat[0] = DataStr[18];tmpDat[1] = DataStr[19];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&tmp);
		thisIdentity.cUniversalRev = tmp;

		if (thisIdentity.cUniversalRev <= 6)// 5 & 6 & uninitialized xmtr-dd's 0x01
		{		
			tmpDat[0] = DataStr[12];tmpDat[1] = DataStr[13];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wManufacturer = tmp; 

			tmpDat[0] = DataStr[14];tmpDat[1] = DataStr[15];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wDeviceType   = tmp; 

			tmpDat[0] = DataStr[16];tmpDat[1] = DataStr[17];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp); 
			thisIdentity.cReqPreambles = tmp;

			tmpDat[0] = DataStr[20];tmpDat[1] = DataStr[21];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cDeviceRev    = tmp;

			tmpDat[0] = DataStr[22];tmpDat[1] = DataStr[23];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cSoftwareRev  = tmp;

			tmpDat[0] = DataStr[24];tmpDat[1] = DataStr[25];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cHardwareRev  = tmp;

			tmpDat[0] = DataStr[26];tmpDat[1] = DataStr[27];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cZeroFlags    = tmp;

			tmpDat[0] = DataStr[28];tmpDat[1] = DataStr[29];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[2] = tmp;
			
			tmpDat[0] = DataStr[30];tmpDat[1] = DataStr[31];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[1] = tmp;

			tmpDat[0] = DataStr[32];tmpDat[1] = DataStr[33];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[0] = tmp;
			
			if (thisIdentity.cUniversalRev == 6)
			{			
				tmpDat[0] = DataStr[34];tmpDat[1] = DataStr[35];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cRespPreambles= tmp; 

				tmpDat[0] = DataStr[36];tmpDat[1] = DataStr[37];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cMaxDevVars   = tmp; 

				tmpDat[0] = DataStr[38];tmpDat[1] = DataStr[39];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp); 
				tmpDat[0] = DataStr[40];tmpDat[1] = DataStr[41];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&htmp); 
				thisIdentity.wCfgChngCnt   = (tmp << 8) | htmp;


				tmpDat[0] = DataStr[42];tmpDat[1] = DataStr[43];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cExtDevStatus = tmp;
			}
		}
		else
		if (thisIdentity.cUniversalRev == 7)
		{			
			tmpDat[0] = DataStr[12];tmpDat[1] = DataStr[13];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);		
			tmpDat[0] = DataStr[14];tmpDat[1] = DataStr[15];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&htmp);
			thisIdentity.wDeviceType   = ( tmp << 8 ) | htmp; 

			tmpDat[0] = DataStr[16];tmpDat[1] = DataStr[17];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp); 
			thisIdentity.cReqPreambles = tmp;

			tmpDat[0] = DataStr[20];tmpDat[1] = DataStr[21];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cDeviceRev    = tmp;

			tmpDat[0] = DataStr[22];tmpDat[1] = DataStr[23];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cSoftwareRev  = tmp;

			tmpDat[0] = DataStr[24];tmpDat[1] = DataStr[25];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cHardwareRev  = tmp;

			tmpDat[0] = DataStr[26];tmpDat[1] = DataStr[27];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cZeroFlags    = tmp;

			tmpDat[0] = DataStr[28];tmpDat[1] = DataStr[29];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);  
			pID[2] = tmp;
			
			tmpDat[0] = DataStr[30];tmpDat[1] = DataStr[31];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[1] = tmp;

			tmpDat[0] = DataStr[32];tmpDat[1] = DataStr[33];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[0] = tmp;

			tmpDat[0] = DataStr[34];tmpDat[1] = DataStr[35];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);  
			thisIdentity.cRespPreambles = tmp;
			
			tmpDat[0] = DataStr[36];tmpDat[1] = DataStr[37];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cMaxDevVars = tmp;

			tmpDat[0] = DataStr[38];tmpDat[1] = DataStr[39];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp); 			
			tmpDat[0] = DataStr[40];tmpDat[1] = DataStr[41];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&htmp);
			thisIdentity.wCfgChngCnt = ( tmp << 8 ) | htmp;

			tmpDat[0] = DataStr[42];tmpDat[1] = DataStr[43];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cExtDevStatus = tmp;

			tmpDat[0] = DataStr[44];tmpDat[1] = DataStr[45];tmpDat[2] = '\0';			
			tmpDat[2] = DataStr[46];tmpDat[3] = DataStr[47];tmpDat[4] = '\0';
			sscanf(ptd, "%x" ,&tmp); 			
		//	tmpDat[0] = DataStr[46];tmpDat[1] = DataStr[47];tmpDat[2] = '\0';
		//	sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wManufacturer = tmp;
		}
		else
		{// unknown universal rev
			rc = DDL_COMM_UNKNOWN_UNIV_REV;
			LOGIT(CERR_LOG|CLOG_LOG|UI_LOG|STAT_LOG,"Unknown Universal Revision 0x%02x\n",
																thisIdentity.cUniversalRev);
		}
	}
#endif

	return rc;
}


 // stevev 4/2/04 - extract the logging to a separate func
 void CdocSimmInfc::stxSocketDump(hPkt* pPkt)
 {// New code for Packet dump Jeyabal
#ifndef _CONSOLE
 	CClient *pSocket = NULL;
 	if (m_pDoc)
 		pSocket = m_pDoc->GetSocket();
 
 	if (pSocket)
 	{
 		char strBuff[MAX_PACKET_STRING];
if ( hstAddr != 1 )// is secondary
 		strcpy(strBuff,"Stx");
else
 		strcpy(strBuff,"STX");
 
 		char strTemp[MAX_PACKET_STRING];
 		memset(strTemp,0,MAX_PACKET_STRING);
 
 		sprintf(strTemp, " %s  %04d", strBuff, pPkt->cmdNum);
 		strcpy(strBuff, strTemp);

 		sprintf(strTemp, " %s  %02d", strBuff, pPkt->dataCnt);
 		strcpy(strBuff, strTemp);
 
 		// Pack bytes for Status bits
 //			sprintf(strTemp,"%s %02x %02x ", strBuff, 0, 0);
 		sprintf(strTemp,"%s        ", strBuff, 0, 0);
 		//			if ( pPkt->dataCnt > 0 )
 		//			{
 		//				sprintf(strTemp,"%s __ __ ", strBuff);
 		//			}
 		//			else
 		//			{
 		//				sprintf(strTemp,"%s ", strBuff);
 		//			}
 		strcpy(strBuff, strTemp);
 		
 		for (int nCount = 0; nCount < pPkt->dataCnt - (pPkt->dataCnt%2); nCount+=2)
 		{
 			sprintf(strTemp,"%02x%02x  ", pPkt->theData[nCount], pPkt->theData[nCount+1]);
 			strcat(strBuff,strTemp);
 		}
 		// stevev 4/2/04 - user complaint about too many bytes in the output
 		//for (nCount = 0; nCount < (pPkt->dataCnt%2); nCount++)
 		//{
 		//	sprintf(strTemp,"%02x", pPkt->theData[nCount]);
 		//	strcat(strBuff,strTemp);
 		//}
 		if (pPkt->dataCnt%2)
 		{
 			sprintf(strTemp,"%02x", pPkt->theData[pPkt->dataCnt-1]);
 			strcat(strBuff,strTemp);
 		}
 		// stevev - end 4/2/4 --
 		int y = strlen(strBuff);
 		strBuff[y++] = 22; strBuff[y] = '\0';
 		pSocket->SendMessage(strBuff, y);
 	}
 // End - New code for Packet dump
#endif //_CONSOLE
 }
 // end stevev 4/2/04
 
 // stevev 4/9/04 - extract the logging to a separate func
 void CdocSimmInfc::ackSocketDump(hPkt* pPkt)
 {// New code for Packet dump Jeyabal
#ifndef _CONSOLE
 	CClient *pSocket = NULL;
 	if (m_pDoc)
 		pSocket = m_pDoc->GetSocket();
 
 	if (pSocket)
 	{
 		char strBuff[MAX_PACKET_STRING];
 		strcpy(strBuff,"Ack");
 
 		char strTemp[MAX_PACKET_STRING];
 		memset(strTemp,0,MAX_PACKET_STRING);
 
 		sprintf(strTemp, " %s  %04d", strBuff, pPkt->cmdNum);
 		strcpy(strBuff, strTemp);
 
 		sprintf(strTemp, " %s  %02d", strBuff, pPkt->dataCnt);
 		strcpy(strBuff, strTemp);
 
 		if (pPkt->dataCnt >= 2)
 		{
 			// Pack bytes for Status bits
 			sprintf(strTemp,"%s %02x %02x  ", strBuff, pPkt->theData[0], pPkt->theData[1]);
 			strcpy(strBuff, strTemp);
 		}
 		
 		for (int nCount = 2; nCount < pPkt->dataCnt- (pPkt->dataCnt%2); nCount+=2)
 		{
 			sprintf(strTemp,"%02x%02x  ", pPkt->theData[nCount], pPkt->theData[nCount+1]);
 			strcat(strBuff,strTemp);
 		}
 
 		if (pPkt->dataCnt%2)
 		{
 			sprintf(strTemp,"%02x", pPkt->theData[pPkt->dataCnt-1]);
 			strcat(strBuff,strTemp);
 		}

 		int y = strlen(strBuff);
 		strBuff[y++] = 22; strBuff[y] = '\0';
 		pSocket->SendMessage(strBuff, y);//strlen(strBuff));
 	}
 // End - New code for Packet dump
#endif //_CONSOLE
 }
 // end stevev 4/9/04
 
#endif // not IS_OLE


/*************************************************************************************************
 *
 *   $History: DDLpipeCommInfc.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
