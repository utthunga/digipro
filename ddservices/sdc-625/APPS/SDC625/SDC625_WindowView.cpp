/*************************************************************************************************

// SDC625WindowView.cpp : implementation file
//
*/
#include "stdafx.h"
#include "sdc625.h"
#include "SDC625Doc.h"
#include "SDC625_WindowView.h"

// - located in ddbMenuLayout.h -- #define ARRANGE_IT	/* the new way to do it 13nov06 */

#ifdef ARRANGE_IT
#define WP   m_pDrawWin->
#else
#define WP
#endif
// define as empty to do it the old way

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _TESTDATA
extern int fillData(UINT mySymNum);
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView

IMPLEMENT_DYNCREATE(CSDC625WindowView, CFormView)

CSDC625WindowView::CSDC625WindowView()
	: CFormView(CSDC625WindowView::IDD),m_point(0,0)
{
	//{{AFX_DATA_INIT(CSDC625WindowView)
	//}}AFX_DATA_INIT
	mySymNum = 0;

	m_bTracking = FALSE;
	m_pHelp = NULL;
	m_isCntrlsReady = FALSE;
	m_pWindowFrame  = NULL;
	m_pDrawWin      = NULL;
	TRACE(_T("CSDC625WindowView:Constructor\n"));
}

CSDC625WindowView::~CSDC625WindowView()
{
	if (m_pDrawWin)
	{
		m_pDrawWin->Destroy(m_pDrawWin->m_symblID);
		m_pDrawWin = NULL;
	}
}

void CSDC625WindowView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSDC625WindowView)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSDC625WindowView, CFormView)
	//{{AFX_MSG_MAP(CSDC625WindowView)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
	ON_WM_CREATE()
	ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
	ON_COMMAND(ID_MENU_LABEL, OnMenuLabel) // Table and Window share the same label ID - POB, 3/24/14
	ON_COMMAND(ID_MENU_DISPLAY_VALUE, OnMenuDisplayValue)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	ON_WM_VSCROLL() // stevev 12jul07
	ON_WM_HSCROLL() // stevev 12jul07
END_MESSAGE_MAP()


BOOL CSDC625WindowView::PreCreateWindow(CREATESTRUCT& cs)
{
	// NOTE: Modify the Window class or styles here by modifying the CREATESTRUCT cs

	TRACE(_T("CSDC625WindowView:PreCreateWindow\n"));
	return CFormView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView drawing

void CSDC625WindowView::OnDraw(CDC* pDC)
{
	CDDIdeDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// NOTE: add draw code for native data here
}

void CSDC625WindowView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	TRACE(_T("CSDC625WindowView:OnInitialUpdate\n"));
	
	CDDIdeDoc* pDoc = GetDocument();
// 06dec05 -  was   UPDATE_REBUILD_ALL
	OnUpdate( NULL, UPDATE_STRUCTURE, NULL );

	TRACE(_T("CSDC625WindowView:OnInitialUpdate - Exit\n"));

}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView printing

BOOL CSDC625WindowView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSDC625WindowView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSDC625WindowView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView diagnostics

#ifdef _DEBUG
void CSDC625WindowView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSDC625WindowView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

CDDIdeDoc* CSDC625WindowView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDDIdeDoc)));
	return (CDDIdeDoc*)m_pDocument;
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView message handlers

BOOL CSDC625WindowView::OnCommand(WPARAM wParam, LPARAM lParam) 
{	//wParam
	//The low-order word of wParam identifies the command ID of the menu item, control, or accelerator.
	//The high-order word of wParam specifies the notification message if the message is from a control.
	//If the message is from an accelerator, the high-order word is 1.
	//If the message is from a menu, the high-order word is 0.

	//lParam
	//Identifies the control that sends the message if the message is from a control. Otherwise, lParam is 0.

	WORD wControlID = LOWORD(wParam);
	WORD wMessageID = HIWORD(wParam);

	BOOL result;

	if (wControlID != 0 && m_isCntrlsReady && GetDocument()->pCurDev != NULL && m_pDrawWin != NULL) 
	{
//		TRACE(_T("CSDC625WindowView::OnCommand - Execute, control ID = %d\n"), wControlID);
		if ( ! m_pDrawWin->rebuilding && GetDocument()->pCurDev->devState != ds_Closing )
			result = m_pDrawWin->ExecuteCID(wControlID, wMessageID);
	}

	return CFormView::OnCommand(wParam, lParam);
}

void CSDC625WindowView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	CDDIdeDoc *pDoc = GetDocument();
    CDDLBase *pItem = ( CDDLBase* )pHint;
    CDDLBase *pParent = NULL;
	CString strTemp;

	DEBUGLOG(CLOG_LOG,"MSG:CSDC625WindowView::OnUpdate, lParam = 0x%04x, rParam = 0x%08x\n", 
		lHint, pHint);

	if ( !m_pDrawWin || m_pDrawWin->rebuilding || GetDocument()->pCurDev->devState == ds_Closing ) return;
	
	switch( lHint )
    {
/* 06dec05 - stevev no longer used
        case UPDATE_REBUILD_ALL:  // ignore pHint
			break;

        case UPDATE_REBUILD_LIST:
			break;

        case UPDATE_DELETE_BELOW:  // pItem is parent.  delete below.
            break;

        case UPDATE_REMOVE_ITEM:  // pItem (menu item) is item to remove.
            break;

        case UPDATE_ADD_ITEM:  // pItem (menu item) is item to add.
            break;
*** end stevev 06dec05 ***/

		case UPDATE_T_DRAW_HOLD:
		//		SetRedraw( FALSE );
				DEBUGLOG(CLOG_LOG,"Update -- Draw HOLD---Window(no-op now).\n");
				break;
		case UPDATE_T_DRAW_DRAW:  
				DEBUGLOG(CLOG_LOG,"Update -- Draw Draw---Window.\n");
				SetRedraw( TRUE );
				Invalidate();// stevev 12jul07 - try to get the update to update w/o mouse
			break;

		case UPDATE_ONE_VALUE:
return; // while we try cdrawmain updates
break;
		case UPDATE_STRUCTURE:// stevev the drawmanin update only tests for size changes, doesn't actually change
		case UPDATE_ADD_DEVICE:
		case UPDATE_REMOVE_DEVICE:

//06dec05 - stevev renamed        case UPDATE_WINDOW_ITEM:   // pItem (menu item) is item to update
            {
				ulong id = (ulong)pHint;// 6 is UPDATE_ONE_VALUE, 2 is UPDATE_STRUCTURE
			
				// Check to see if the symbol exists
				if (m_isCntrlsReady)
				{
					
	pnt_t temp = {1,1};
//					TRACE(_T("CSDC625WindowView::OnUpdate, symbolID = %d\n"), id);
					// cDrawWin Update falls thru to CdrawLayout Update
					if (lHint != UPDATE_ONE_VALUE && m_pDrawWin->Update(lHint,id))
					{// I gotta rebuild 'em all
						LOGIT(CLOG_LOG,"I gotta rebuild 'em.\n");
						if ( m_pDrawWin )
						{
m_pDrawWin->rebuilding=true;
m_isCntrlsReady = false;
							m_pDrawWin->Destroy(mySymNum);// self
							// now rebuild like in Initialize
							int r = m_pDrawWin->FillSelf(id,menuHist_t(mds_do_not_use),NULL);

							
#ifdef LOGBIRTH
	ldepth = 1;
#endif	
	// Now create its menu items
	//           CWnd*, pnt_t (starts at 1,1)
	m_pDrawWin->createMenu(this, temp);

	//Update view
//	OnUpdate( NULL, UPDATE_REBUILD_ALL, NULL );

							
#ifdef LOGBIRTH
	ldepth = 0;
#endif	

	int mmode;
	CRect arect;
	SIZE total,Page,Line;
	GetDeviceScrollSizes(mmode, total, Page, Line ) ;

	arect.top = 0; arect.left = 0;
	arect.bottom = (m_pDrawWin->m_MyLoc.da_size.yval * (ROWSTEPSIZE + ROWGAP)) + FIRSTROW1;// 14 + 6 
	arect.right  = (m_pDrawWin->m_MyLoc.da_size.xval * (COLUMSTEPSIZE *3))     + FIXEDCOL1;// 70 + 6 

	Page.cx = COLUMSTEPSIZE;
	Page.cy = (long)((arect.bottom * 0.90)+0.5);//arect.bottom / 2;
	Line.cy = (ROWSTEPSIZE + ROWGAP);
	// leave line.cx

	MapDialogRect(m_hWnd ,&arect);

	total.cy = arect.Height();
	total.cx = arect.Width();
	SetScrollSizes( mmode, total, Page, Line);

	m_isCntrlsReady = TRUE;

	m_pWindowFrame->SetWindowText(m_pDrawWin->GetLabel());

m_pDrawWin->rebuilding = false;
m_isCntrlsReady = true;

						}// else NOT a drawWin - fall out
					}
#ifdef _DEBUG
					else
					{
						LOGIT(CLOG_LOG,"I really do get updates with no change.\n");
					}
#endif
				}
            }
            break;

		case UPDATE_TREE_VIEW:	// stevev 31jul07
		case UPDATE_LIST_VIEW:		// never used here
			break;



        default:
            break;
    }// endswitch

	UpdateData(FALSE); // Send data to dialog screen
}


void CSDC625WindowView::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	if ( nSBCode == SB_ENDSCROLL )
		Invalidate();
	CFormView::OnVScroll(nSBCode, nPos, pScrollBar);
}
void CSDC625WindowView::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	if ( nSBCode == SB_ENDSCROLL )
		Invalidate();
	CFormView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CSDC625WindowView::Initialize(ulong id)
{
	hCitemBase*    p_myItem;
	RETURNCODE rc = SUCCESS;
	pnt_t temp = {1,1};
	mySymNum = id;

	// moved to mainframe.....stevev 27sep11....CoInitialize(NULL);

	if (PCURDEV == NULL || m_pDrawWin == NULL )
	{
		return; /// error -  no device or no draw class!!!!!!!!!!!!
	}
	TRACE(_T("CSDC625WindowView:Initialize\n"));

	rc = PCURDEV->getItemBySymNumber(mySymNum, &p_myItem);
	
	if ( rc != SUCCESS || p_myItem == NULL)
	{
		return; // error:  item I am supposed to be doesn't exist !!!!!!!!!!!!
	}
	
	if (p_myItem->getIType() != iT_Menu)
	{
		return; // error:  I am supposed to be a menu of style window !!!!!!!!!!!!
	}

// stevev 30jul07 - stop under-window's page reset to zero *** High Risk Change *** test well
	m_pDrawWin->m_pDynTabCtrl = NULL;// don't my (abutton) page's tab cntrl get passed into the window.

	int r = m_pDrawWin->FillSelf(id,menuHist_t(mds_do_not_use),NULL);// do-not-use is Don't Care @Called

/* This is where the CdrawBase controls are instantiated.
   You will need to traverse the tree, creating as you go.
   Since you know all the sizes of everything before you start,
   you should be able to create these efficiently.
   I suspect you will have to pass one or more parameters ( like 'this' )
   to the create.
*/
#ifdef LOGBIRTH
	ldepth = 1;
#endif
	//           CWnd*, pnt_t (starts at 1,1)
	m_pDrawWin->createMenu(this, temp);

#ifdef LOGBIRTH
	ldepth = 0;
#endif	
	int mmode;
	CRect arect;
	SIZE total,Page,Line;
	GetDeviceScrollSizes(mmode, total, Page, Line ) ;

	arect.top = 0; arect.left = 0;
	arect.bottom = (m_pDrawWin->m_MyLoc.da_size.yval * (ROWSTEPSIZE + ROWGAP)) + FIRSTROW1;// 14 + 6 
	arect.right  = (m_pDrawWin->m_MyLoc.da_size.xval * (COLUMSTEPSIZE *3))     + FIXEDCOL1;// 70 + 6 

	MapDialogRect(m_hWnd ,&arect);

	total.cy = arect.Height();
	total.cx = arect.Width();
	SetScrollSizes( mmode, total, Page, Line);

	m_isCntrlsReady = TRUE;

	m_pWindowFrame->SetWindowText(m_pDrawWin->GetLabel());

	TRACE(_T("CSDC625WindowView::Initialize: - Exit Controls are ready\n"));
}

void CSDC625WindowView::OnDestroy() 
{	/* 
	 * iterate through all of the enteries in
	 * WindowMap to destroy controls and delete
	 * the associated memory
	 */
	if ( ! m_pDrawWin  || m_pDrawWin->rebuilding )
		return;
	//Destroy its children
	m_pDrawWin->Destroy(m_pDrawWin->m_symblID);

	// Set the CdrawWin object that invoke this to NULL
	if (m_pDrawWin)
	{
		m_pDrawWin->m_pWindow = NULL;
	}
	// else it was never generated
	CFormView::OnDestroy();//stevev 16aug05 - moved from first statement
}


void CSDC625WindowView::layoutWindow(void) // sizes and locates
{
#if 0
	// verify we have a map
	if ( m_WndDrawMap.size() <= 0 )
	{// error
		return;
	}

	// size the map
	Id2DrawMap_t::iterator i;
	for (  i = m_WndDrawMap.begin(); i != m_WndDrawMap.end(); ++i)
	{
		i->second->FillSize();
	}

	// locate the map
	for ( i = m_WndDrawMap.begin(); i != m_WndDrawMap.end(); ++i)
	{
		i->second->leftCol = 1;// TODO temporary
		i->second->top_Row = 1;
	}
#endif // 0 //
}
/*
void CSDC625WindowView::createControl(void)// make 'em to spec
{
	// create all the controls to spec
	CdrawBase * pBase;

	int count = 0;

	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{

		pBase = (*i);
		pBase->createControl(this);
		count++;

		if (count == 3)
			break;
	}\

	pnt_t temp = {0,0};
	createMenu(this, temp);

}*/

void CSDC625WindowView::OnChangeEdit2() 
{
	// NOTE: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// NOTE: Add your control notification handler code here
	
}


void CSDC625WindowView::OnMenuLabel() 
{	
	if ( ! m_pDrawWin  || m_pDrawWin->rebuilding || 
		 GetDocument()->pCurDev->devState == ds_Closing )
	{
		return;
	}
	
        /* code moved to CdrawBase::DisplayLabel - 11/27/13 POB */
	m_pDrawWin->DisplayLabel(m_point);
}


void CSDC625WindowView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CMenu menu;
	CMenu* pSubmenu;
	DWORD dwPosition = GetMessagePos();
    CPoint mesgpoint(LOWORD(dwPosition), HIWORD(dwPosition));
	m_point = mesgpoint;
	int nPos;

	if ( (m_pDrawWin  && ! m_pDrawWin->rebuilding) && GetDocument()->pCurDev->devState != ds_Closing )
	{
		CdrawBase * pBase = m_pDrawWin->HitTest(m_point);

		if (pBase)
		{
			pBase->LoadContextMenu( &menu, nPos ) ;
			pSubmenu = menu.GetSubMenu(nPos);
        
			pSubmenu->TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
										  m_point.x, m_point.y, this);

			
	//		SetToolTipText(new CString("window tip"));
		}
	}

	CFormView::OnRButtonDown(nFlags, point);
}

void CSDC625WindowView::OnMenuHelp() 
{
	if ( ! m_pDrawWin  || m_pDrawWin->rebuilding || GetDocument()->pCurDev->devState == ds_Closing )
		return;
	m_pDrawWin->DisplayHelp(m_point);
}

void CSDC625WindowView::OnMenuDisplayValue() 
{	
	if ( ! m_pDrawWin  || m_pDrawWin->rebuilding || 
		 GetDocument()->pCurDev->devState == ds_Closing )
	{
		return;
	}
	
    m_pDrawWin->DisplayValue(m_point);
}

void CSDC625WindowView::InitToolTip()
{
	if (m_ToolTip.m_hWnd == NULL)
	{
		// Create ToolTip control
		m_ToolTip.Create(this);
		// Create active
		m_ToolTip.Activate(TRUE);
	}
}

void CSDC625WindowView::SetToolTipText(CString *spText, BOOL bActivate)
{
	// We cannot accept NULL pointer
	if (spText == NULL) return;

	// Initialize ToolTip
	InitToolTip();

	// If there is no tooltip defined then add it
	if (m_ToolTip.GetToolCount() == 0)
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_ToolTip.AddTool(this, (LPCTSTR)*spText, rectBtn, 1);
	}

	// Set text for tooltip
	m_ToolTip.UpdateTipText((LPCTSTR)*spText, this, 1);
	m_ToolTip.Activate(bActivate);
}

BOOL CSDC625WindowView::PreTranslateMessage(MSG* pMsg) 
{
//	InitToolTip();
//	m_ToolTip.RelayEvent(pMsg);

	return CFormView::PreTranslateMessage(pMsg);
}

void CSDC625WindowView::OnMouseMove(UINT nFlags, CPoint point) 
{
/*	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = HOVER_DEFAULT;
		m_bTracking = _TrackMouseEvent(&tme);		
	}*/

	CFormView::OnMouseMove(nFlags, point);
}

LRESULT CSDC625WindowView::OnMouseHover(WPARAM wparam, LPARAM lparam) 
{
	DWORD dwPosition = GetMessagePos();
    CPoint mesgpoint(LOWORD(dwPosition), HIWORD(dwPosition));
	m_point = mesgpoint;
	
/*	CdrawBase * pBase = HitTest(m_point);

	if (pBase)
	{
		CString strHelp = pBase->GetHelp();
		SetToolTipText(new CString(strHelp));

	}

	m_pHelp = pBase;*/

	return 0;
}

LRESULT CSDC625WindowView::OnMouseLeave(WPARAM wparam, LPARAM lparam)
{
	m_bTracking = FALSE;
//	m_bHover=FALSE;
//	Invalidate();

/*	if (m_ToolTip.m_hWnd)
	{
		if (m_ToolTip.GetToolCount() != 0)
		{
			m_ToolTip.Activate(FALSE);
		}
	}*/
		
	return 0;
}

int CSDC625WindowView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
//	SetToolTipText(new CString("window tip"));
	return 0;
}

void CSDC625WindowView::DestroySymbol(ulong symblID)
{
	m_pDrawWin->Destroy(symblID);
}

void CSDC625WindowView::OnLButtonDown(UINT nFlags, CPoint point) 
{
/** now handled in setfocus of executecid
	DWORD dwPosition = GetMessagePos();
    CPoint mesgpoint(LOWORD(dwPosition), HIWORD(dwPosition));
	m_point = mesgpoint;
		
	if ( !( m_pDrawWin  && m_pDrawWin->rebuilding) && GetDocument()->pCurDev->devState != ds_Closing)
	{
		CdrawBase * pBase = m_pDrawWin->HitTest(m_point);

		if (pBase)
		{
			TRACE(_T("$$$$ Cntrl 0x%04x Rcv'd A Left Button Down in WindowView\n"),pBase->m_cntrlID);
			pBase->Edit();
		}
	}
** end now handled another way **/
	CFormView::OnLButtonDown(nFlags, point);
}

void CSDC625WindowView::Rebuild(void) // replace all in window with re-layed out controls
{
	if ( ! m_pDrawWin )
		return;
	//Destroy its children
	m_pDrawWin->Destroy(m_pDrawWin->m_symblID);
//	--------------------
	Initialize(m_pDrawWin->m_symblID);

TRACE(L"Rebuild -- Draw _ON---WindowView.\n");
	SetRedraw( TRUE );
	Invalidate();
	//CWnd::RedrawWindow
//BOOL RedrawWindow( LPCRECT lpRectUpdate = NULL, CRgn* prgnUpdate = NULL, UINT flags = RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE   | RDW_ALLCHILDREN   ); 
}

// Added OnNotify to support editing and activating DDL items on Grid, POB - 5/5/2014
BOOL CSDC625WindowView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
    NM_GRIDVIEW * nmgv = (NM_GRIDVIEW* )lParam;

    WORD wControlID = LOWORD(wParam);
	WORD wMessageID = nmgv->hdr.code;

    BOOL result;

    if (wControlID != 0 && m_isCntrlsReady && GetDocument()->pCurDev != NULL && m_pDrawWin != NULL) 
	{
		if ( ! m_pDrawWin->rebuilding && GetDocument()->pCurDev->devState != ds_Closing )
			result = m_pDrawWin->ExecuteCID(wControlID, wMessageID, lParam);
	}

    // this function is called as expecetd
    return CFormView::OnNotify(wParam, lParam, pResult);
}