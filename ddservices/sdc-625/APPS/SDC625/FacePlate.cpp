// FacePlate.cpp : implementation file
//

#pragma warning (disable : 4786)
#include "stdafx.h"
#include "SDC625.h"
#include "FacePlate.h"
#include "SDC625Doc.h"
#include "SDC625TreeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFacePlate dialog

CFacePlate::CFacePlate(CDDIdeTreeView *pTreeView, CWnd* pParent /*=NULL*/)
	: CDialog(CFacePlate::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFacePlate)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pParent = pTreeView;

	m_nVariableCount = 0;
	m_pProgress		 = NULL;

	for (int nCount = 0; nCount < MAX_VARIABLES; nCount++)
	{
		m_pVariables[nCount] = NULL;
		m_pLabels[nCount] = NULL;
		m_pValues[nCount] = NULL;
		m_pUnits[nCount] = NULL;
	}
}

CFacePlate::~CFacePlate()
{
	if (m_pProgress)
		delete m_pProgress;

	for (int nCount = 0; nCount < MAX_VARIABLES; nCount++)
	{
		if (m_pVariables[nCount])
			delete m_pVariables[nCount];

		if (m_pLabels[nCount])
			delete m_pLabels[nCount];

		if (m_pValues[nCount])
			delete m_pValues[nCount];

		if (m_pUnits[nCount])
			delete m_pUnits[nCount];
	}

	KillTimer(FACE_PLATE_TIMER);
}

void CFacePlate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFacePlate)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFacePlate, CDialog)
	//{{AFX_MSG_MAP(CFacePlate)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFacePlate message handlers

void CFacePlate::OnOK() 
{
	CDialog::OnOK();
	DestroyWindow();
}

void CFacePlate::OnCancel() 
{
	CDialog::OnCancel();
	DestroyWindow();
}

BOOL CFacePlate::OnInitDialog() 
{
	CDialog::OnInitDialog();

	RETURNCODE rc = SUCCESS;

	// Get the current device for getting the Process Variable item
	hCddbDevice* pCurDevice = m_pParent->GetDocument()->pCurDev;
	if (pCurDevice)
	{
		hCitemBase *pItemBase = NULL;
		vector<hCmenuItem> menuItemLst;
		string ty;

		// In the absense of Face Plate definition we are using this Process variable
		// eventualy this needs to change

		if (pCurDevice->getItemBySymNumber(DEVICE_FACE_PLATE, &pItemBase) == SUCCESS)
		{
			if (pItemBase == NULL) // Some of the DDs will not have this Process
				return FALSE;			   // variable definition, just a double check

			hCmenu* pMenu = (hCmenu*)pItemBase; // Process Variables is a Menu
			if (pMenu->aquire(menuItemLst) == SUCCESS)
			{
					for (vector<hCmenuItem>::iterator iv = menuItemLst.begin(); 
								 iv < menuItemLst.end() ; iv++)
					{
						rc = pCurDevice->procure(iv->getRef(), &pItemBase);
						if ( rc == SUCCESS && pItemBase != NULL)
						{// do something with the item
							if ( pItemBase->Label(ty) == SUCCESS )
							{
								if (pItemBase->getTypeVal() == VARIABLE_ITYPE)
								{
									hCVar *pVar = (hCVar *)pItemBase;

									if (m_nVariableCount >= MAX_VARIABLES - 1)
										continue;

									m_pVariables[m_nVariableCount] = new CDDLVariable(pVar);
									m_nVariableCount++;
								}
							}
						}
					}
			}
		}
	}
	
	DisplayVariables();

	// Create the timer for dynamic update
    int result = SetTimer(FACE_PLATE_TIMER, 5000, NULL); // 5 Seconds timer

	if (!result)
      MessageBox(M_UI_NO_TIMER);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFacePlate::DisplayVariables() 
{
	CRect rectLables(24,61,100,77);
	CRect rectValues(108,61,225,77);
	CRect rectUnits(230,61,275,77);

/*
	if (m_pProgress == NULL)
		m_pProgress = new CProgressCtrl;

	// Create a smooth child progress control.
	m_pProgress->Create(WS_CHILD|WS_VISIBLE|PBS_SMOOTH, CRect(30,40,260,57), this, 1);

	int nPercent = 0;
	
	// Assumption - 1st Value is the real value and 2nd Value is the range
	if (m_pVariables[0] && m_pVariables[1])
	{
		float fValue = *m_pVariables[0];
		float fRange = *m_pVariables[1];

		if (fValue > 0 && fRange > 0)
			nPercent = (fValue / fRange) * 100;
	}

	// Set the range to be 0 to 100.
	m_pProgress->SetRange( 0, 100 );
	m_pProgress->SetPos(nPercent);
*/	
	for (int nCount = 0; nCount < m_nVariableCount; nCount++)
	{
		m_pLabels[nCount] = new CStatic;
		m_pLabels[nCount]->Create(m_pVariables[nCount]->GetName(), WS_CHILD|WS_VISIBLE | SS_LEFT, rectLables, this, 100 + nCount); // 100 Any ID is acceptable

		m_pValues[nCount] = new CEdit;
		m_pValues[nCount]->Create(WS_CHILD|WS_VISIBLE|WS_BORDER, rectValues, this, 100 + nCount); // 100 Any ID is acceptable

		CString strValue = *m_pVariables[nCount];
		m_pValues[nCount]->SetWindowText(strValue);

		m_pUnits[nCount] = new CStatic;
		m_pUnits[nCount]->Create(m_pVariables[nCount]->GetUnitStr(), WS_CHILD|WS_VISIBLE | SS_LEFT, rectUnits, this, 100 + nCount); // 100 Any ID is acceptable

		rectLables.top = rectValues.top = rectUnits.top = rectLables.bottom + 8;
		rectLables.bottom = rectValues.bottom = rectUnits.bottom = rectLables.top + 16;

	}
}

void CFacePlate::OnTimer(UINT nIDEvent) 
{
	// Update the Progress Bar
	int nPercent = 0;
	vector <hCitemBase*> pVariablesToRead;
	int nCount;	// PAW 03/03/09
	for (/*int PAW 03/03/09*/ nCount = 0; nCount < m_nVariableCount; nCount++)
	{
		if (m_pVariables[nCount]->IsDynamic())
			pVariablesToRead.push_back((hCitemBase *)m_pVariables[nCount]->m_pddbVar);
	}

	CDDIdeDoc *pDoc = (CDDIdeDoc *)m_pParent->GetDocument();
	if (pDoc)
		pDoc->ReadVariables(pVariablesToRead);

	// Assumption - 1st Value is the real value and 2nd Value is the range
	if (m_pVariables[0] && m_pVariables[1])
	{
		float fValue = *m_pVariables[0];
		float fRange = *m_pVariables[1];

		if (fValue > 0 && fRange > 0)
			nPercent = (int)((fValue / fRange) * 100);
	}

//	if (m_pProgress)
//		m_pProgress->SetPos(nPercent);

	for (nCount = 0; nCount < m_nVariableCount; nCount++)
	{
		if (m_pVariables[nCount]->IsDynamic())
		{
			CString strValue = *m_pVariables[nCount];
			m_pValues[nCount]->SetWindowText(strValue);
		}
	}

	CDialog::OnTimer(nIDEvent);
}
