// FileVersion.h: interface for the CFileVersion class.
// by Manuel Laflamme - CodeGuru site
//////////////////////////////////////////////////////////////////////

#ifndef __FILEVERSION_H_
#define __FILEVERSION_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// test
struct VS_VERSIONINFO
{
	WORD wLength; // of this structure without the padding
	WORD  wValueLength; // byte length of the 'Value' member
	WORD  wType;		// 1 if text , 0 if binary
	WCHAR szKey[15]; //
	WORD  Padding1[2]; 
	VS_FIXEDFILEINFO Value; /* is::>
  DWORD dwSignature; // 0xfeef04bd
  DWORD dwStrucVersion; 
  WORD dwFileVersionMSSecond;  
  WORD dwFileVersionMSFirst; 
  WORD dwFileVersionLSFourth; 
  WORD dwFileVersionLSThird; 
  WORD dwProductVersionMSSecond; 
  WORD dwProductVersionMSFirst;
  WORD dwProductVersionLSFourth; 
  WORD dwProductVersionLSThird; 
  DWORD dwFileFlagsMask; 
  DWORD dwFileFlags; 
  DWORD dwFileOS; 
  DWORD dwFileType; 
  DWORD dwFileSubtype; 
  DWORD dwFileDateMS; 
  DWORD dwFileDateLS; 
  */
 
//	WORD  Padding2[]; 
//	WORD  Children[]; // StringFileInfo structures, and 0 or 1 VarFileInfo structures
};
/////////////////////////////////////////////////////////////
#define LEADINONE "SDC 625  " 
#define LEADINTWO "Version "	/* not used when version info is unavailable */
#define LEADIN  LEADINONE LEADINTWO
#define DBGTYPE "build "
//#define RLSTYPE "beta "
//#define RLSTYPE "candidate "
//#define RLSTYPE "Experimental "	
#define RLSTYPE ""	/* full release (not beta) -- comment out and use the above for candidates*/

class CFileVersion
{
// Construction
public:
    CFileVersion();

// Operations
public:
    BOOL    Open(LPCTSTR lpszModuleName);
    void    Close();

    CString QueryValue(LPCTSTR lpszValueName, DWORD dwLangCharset = 0);
    CString GetFileDescription()  {return QueryValue(_T("FileDescription")); };
    CString GetFileVersion()      {return QueryValue(_T("FileVersion"));     };
    CString GetInternalName()     {return QueryValue(_T("InternalName"));    };
    CString GetCompanyName()      {return QueryValue(_T("CompanyName"));     };
    CString GetLegalCopyright()   {return QueryValue(_T("LegalCopyright"));  };
    CString GetOriginalFilename() {return QueryValue(_T("OriginalFilename"));};
    CString GetProductName()      {return QueryValue(_T("ProductName"));     };
    CString GetProductVersion()   {return QueryValue(_T("ProductVersion"));  };

    BOOL    GetFixedInfo(VS_FIXEDFILEINFO& vsffi);
    CString GetFixedFileVersion();
    CString GetFixedProductVersion();

	BOOL    GetResourceInfo(VS_FIXEDFILEINFO& vsffi);
    CString GetResourceFileVersion();
    CString GetResourceProductVersion();

// Attributes
protected:
    LPBYTE  m_lpVersionData;
    DWORD   m_dwLangCharset;

	CString FormatVersionInfo
		(CString& leadIn, WORD msw, WORD ssw, WORD tsw, CString& finalType, WORD cnt);

// Implementation
public:
    ~CFileVersion();
};

#endif  // __FILEVERSION_H_



