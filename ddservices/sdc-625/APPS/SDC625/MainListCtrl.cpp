// MainListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "MainListCtrl.h"
#include "DDLBase.h"
#include "DDLMenuItem.h"
#include "DDLVariable.h"
#include "VariableDlg.h"
#include "SDC625Doc.h"
#include "DDLexpansion.h"

#include <strsafe.h> /* for length-safe string functions */

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define OFFSET  2
//#define OFFSET_FIRST  2
//#define OFFSET_OTHER  6

//#define LOG_MSGS 1

/////////////////////////////////////////////////////////////////////////////
// CMainListCtrl

CMainListCtrl::CMainListCtrl():m_pTableFrame(NULL)
{
    m_nSelectedItem = -1;

    // Vibhor 100603 : Start of Code
    m_pImageList = new CImageList;
    m_pImageList->Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    CBitmap bmp;
    bmp.LoadBitmap(IDB_IMAGELIST);
    m_pImageList->Add(&bmp, RGB(255,0,255));
    // Vibhor 100603 : End of Code

    // made font more static 
    VERIFY(font.CreateFont(
       13,                        // nHeight
       0,                         // nWidth
       0,                         // nEscapement
       0,                         // nOrientation
       FW_BOLD,                 // nWeight
       FALSE,                     // bItalic
       FALSE,                     // bUnderline
       0,                         // cStrikeOut
       ANSI_CHARSET,              // nCharSet
       OUT_DEFAULT_PRECIS,        // nOutPrecision
       CLIP_DEFAULT_PRECIS,       // nClipPrecision
       DEFAULT_QUALITY,           // nQuality
       DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
       _T("Verdana")));           // lpszFacename
}

CMainListCtrl::~CMainListCtrl()
{
    if (m_pImageList)
        delete m_pImageList;
        
    try
    {
        font.DeleteObject();
    }
    catch(...)
    {
    }

}


BEGIN_MESSAGE_MAP(CMainListCtrl, CListCtrl)
    //{{AFX_MSG_MAP(CMainListCtrl)
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONDBLCLK()
    ON_WM_RBUTTONDOWN()
    ON_WM_KEYDOWN()
    ON_WM_DRAWITEM()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainListCtrl message handlers

#define TEXT_FMT   DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER
CDDLBase *p_dbgBase;

// note: the items are filled in CDDIdeListView::BuildListFrom
void CMainListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
    CDC* pDC     = CDC::FromHandle(lpDrawItemStruct->hDC);

    LV_COLUMN lvc;
    lvc.mask=LVCF_FMT | LVCF_WIDTH;

    CRect rec = lpDrawItemStruct->rcItem;
    CStringW strTemp, unitsTemp;

    unsigned    rightmost = 0, topmost = 0, leftmost = 0;

    GetColumn(0,&lvc);
    rec.right = lvc.cx;

    CDDLBase *pBase = (CDDLBase *)GetItemData(lpDrawItemStruct->itemID);
    p_dbgBase = pBase;// debug only

    if (pBase == NULL)
        return;
    if ( ! pBase->isValid() )
        return;

    if(m_pImageList)                                                        /*ILD_TRANSPARENT*/
        m_pImageList->Draw(pDC,pBase->GetImage(),CPoint(rec.left,rec.top),ILD_NORMAL);  // Added function to get image ID; POB - 5/16/2014
    rec.left += 20; // Offset image

    int nSavedDC   = pDC->SaveDC();
    CFont *oldfont = NULL;

//COL_NAME  0 ------------------------------------------------------------------------------
    strTemp = GetItemText(lpDrawItemStruct->itemID, COL_NAME);
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"DRAW:ListCtrl. DrawItem  0x%04x '%s%\n",lpDrawItemStruct->itemID,strTemp);
#endif
    pDC->SetTextColor(RGB(0,0,0));  // POB, 13 May 2005 : Paint the controls black
    oldfont = pDC->SelectObject(&font); // Update the font for all controls, POB - 4/14/14

    if(pBase->IsVariable())
    {
        if (pBase->IsReadOnly())
        {
            pDC->SetTextColor(RGB(100,100,100));
        }
        // Removed Unecessary code, POB - 4/14/14
    }

    CString strTemp1 = strTemp;
    strTemp = MakeShortString(pDC, strTemp1, rec.right - rec.left, 2*OFFSET);

    if (lpDrawItemStruct->itemState  == ODS_SELECTED)
    {
        pDC->FillRect(rec,&CBrush(RGB(180,180,180)));   // Vibhor 100603 : Changed the color
        pDC->DrawText(strTemp, -1, rec, DT_LEFT | TEXT_FMT);
    }
    else
    {
        pDC->DrawText(strTemp, -1, rec, DT_LEFT | TEXT_FMT);
    }

    /* required to get the correct coordinates sjv 10/5/4*/ 
    GetItemRect(lpDrawItemStruct->itemID, rec, LVIR_LABEL);// get label coords for column 0
    rightmost = rec.right;
    topmost   = rec.top;
    leftmost  = rec.left;

    unitsTemp.Empty();// = "";
    strTemp.Empty();  // = "";

// COL_DATA 1 ----------------------------------------------------------------------------- 
    GetColumn(COL_DATA, &lvc);
    
    UINT nFormat = TEXT_FMT | DT_RIGHT;

    rec.left   = rec.right+ 4;
    rec.right += lvc.cx;

    CDDLMenuItem *pItem = (CDDLMenuItem *)pBase;// only use this when it is not a menu
    // see if we have data
    if ( (! pBase->IsMenu()) && (! pBase->IsEditDisplay()) && pItem->m_item != NULL )
    {// it's a menu-item that is not a constant string nor edit-display
        // only variables can hold data
        if ( pItem->IsVariable() )
        {                       
            CDDLVariable *pVar = (CDDLVariable *)pItem->m_item;
#ifdef _DEBUG
            unsigned myID = pVar->GetId();
#endif
            if ( pItem->HasNoLabel() || pItem->IsDisplayValue() )
            {// no-label forces value   or value gives value
                strTemp = *pVar;   // Update data value
                // units only make sense when there is a value  
                if (! pItem->HasNoUnit()) // does have units
                {
                    unitsTemp = pItem->GetUnitStr();
                }//else leave it blank
            }// else no-value, no units

            if (pVar->IsChanged())
            {
                pDC->FillRect(rec,&CBrush(RGB(255,255,0)));
                pDC->SetBkMode( OPAQUE );
                pDC->SetBkColor (RGB(255,255,0));
            }// else leave it the same
        }
    }// else its a menu or constant string that won't have data or units
    else  //PAR 5356                
    if(pBase->IsEditDisplay())
    {
        CDDLMenuItem *pItem     = (CDDLMenuItem *)pBase;
        CDDLEDisplay *pEditDisp = (CDDLEDisplay*)pItem->m_item;
        
        if(pEditDisp && pEditDisp->IsChanged())
        {
            pDC->FillRect(rec,&CBrush(RGB(255,255,0)));
            pDC->SetBkMode( OPAQUE );
            pDC->SetBkColor (RGB(255,255,0));
        }
    }
    strTemp1 = strTemp;
    strTemp  = MakeShortString(pDC, strTemp1, rec.right - rec.left, 2*OFFSET);      
    pDC->DrawText(strTemp ,-1, rec, nFormat);   
    
    GetColumn(COL_DATA, &lvc);
    
    nFormat = TEXT_FMT;
    rec.left   = rec.right+ 4;
    rec.right += lvc.cx;


    strTemp1 = unitsTemp;
    strTemp  = MakeShortString(pDC, strTemp1, rec.right - rec.left, 2*OFFSET);
    pDC->DrawText(strTemp ,-1, rec, nFormat);

    pDC->SetBkMode( TRANSPARENT );

    // pBase is either a menu or a menuitem
    if (! pBase->IsMenu() )
    {
        if ( ((CDDLMenuItem*)pBase)->HasOverline() )
        {
            CPen *oldpen, blackpen(PS_SOLID,1,RGB(0,0,0));
            oldpen = pDC->SelectObject(&blackpen);
            pDC->MoveTo(leftmost,topmost);
            pDC->LineTo(rightmost,topmost); 
            pDC->SelectObject(oldpen);
        }
    }

    if (oldfont)
        pDC->SelectObject(oldfont);
    
    pDC->RestoreDC(nSavedDC);
}

void CMainListCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{// one one click
 // one followed by ON_DBLCLK at double click
    UINT uFlags=0;
    int nHitItem=HitTest(point,&uFlags);

    if (nHitItem == m_nSelectedItem) // Same item no action
        return;

    if(nHitItem != -1 )
    {
        SetItemState(m_nSelectedItem, -1, -1);
        RedrawItems(m_nSelectedItem,m_nSelectedItem);
        // Must call SetSelectionMark() when a left click is performed (for the 1st time)
        // prior to an "enter" key in CDDIdeListView::OnKeyDown().  Aparently SetItemState()
        // alone here is not enough because  GetSelectionMark() afterwords returns -1 , POB - 4/14/2014
        SetSelectionMark(nHitItem);
        SetItemState(nHitItem, LVIS_SELECTED, LVIS_SELECTED);
        m_nSelectedItem = nHitItem;
        
        TRACE("List Clk got number %#02x\n",nHitItem);
    }
}


void CMainListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    POSITION pos = GetFirstSelectedItemPosition();
    if (pos != NULL)
    {
        int nItem = GetNextSelectedItem(pos);
        if (nItem != -1)
        {
            CDDLBase *pBase = (CDDLBase *)GetItemData(nItem);
            if (pBase)
            {
                bool isMenu = (pBase->IsMenu())? true: false;
                if (!pBase->IsReadOnly())
                {
                    if(pBase->IsMethod())
                    {
                        pBase->Execute(pBase->GetName(),pBase->GetHelp());
                    }
                    else
                    {
                        pBase->Execute();
                    }
                    /*** caution: pBase may not exist when we return from this call 
                         from here to the end of the function, no class variables
                         are accessable!! */
                    
                    if(isMenu)
                    {
                        CDDIdeDoc *pDoc = NULL;
                        pDoc = GetDocument();

                        if (pDoc != NULL && m_pTableFrame != NULL)
                        {
                            //((CTableChildFrame*)m_pTableFrame)->setSelectionDefault();//20may11 // Fixed defect #4257, do not set default, POB-4/11/2014t
                            // 31jul12 we can't update all views since we could have several treeviews open
                            //pDoc->UpdateAllViews( NULL, UPDATE_LIST_VIEW, pBase);
                            
                            // Fixed defect #4257, Update the tree view for this List menu change, POB - 4/11/2014
                            m_pTableFrame->m_pTreeView->OnUpdate(NULL, UPDATE_LIST_VIEW, pBase);

                            // OnUpdate will call tree & list update...
                            TRACE("List DblClk got handle %#08x\n",pBase->GetTreeItem());
                        }
                    }
                    else
                    {
                        RECT rect;
                        if (GetItemRect(nItem, &rect, LVIR_BOUNDS))
                        {
                            InvalidateRect(&rect);
                            UpdateWindow();
                        }
                    }
                }
                else
                {
                    // This is read only
                    if (pBase->IsVariable())
                    {
                        CDDLMenuItem *pMenuItem = (CDDLMenuItem *) pBase;
                        CDDLVariable *pDDLVar = (CDDLVariable *) pMenuItem->m_item;
                        
                        if (pDDLVar->GetType() == TYPE_BITENUM || pDDLVar->GetType() == TYPE_BIT)
                        {
                            /*
                             * Always call Execute() on Biteum and/or Bit reference.
                             * Editing or Displaying the bits in style TABLE
                             * shows the same dialog regardless of whether the 
                             * HANDLING is Read-only or not.
                             */
                            pBase->Execute(); 
                        }
                    }
                }
            }
        }
    }
    
    CListCtrl::OnLButtonDblClk(nFlags, point);
}
void CMainListCtrl::ResetSelection()
{
    m_nSelectedItem = -1;
}

void CMainListCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
    CDDLBase*      pMItem, *pItem;
    CMenu          menu;
    CMenu*         pSubmenu;
    CPoint         ptClient;
    LV_HITTESTINFO HitTestInfo;
    int nPos;

    // Make sure that we do the same selection for the Right click also
    OnLButtonDown(nFlags,point);

     //Get the mouse position associated with this message
     // and see if it is on a tree item.

    DWORD dwPosition = GetMessagePos();
    CPoint cursorPoint(LOWORD(dwPosition), HIWORD(dwPosition));
    ptClient = cursorPoint;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
    HitTest( &HitTestInfo );

    if( LVHT_ONITEM & HitTestInfo.flags )
    {
//         * The click was on a m_pList item.
//         * Get the CDDLBase pointer from the selected object
//         * and have it load the menu.  Must save the pointer
//         * so the frame knows where to process the menu selection.

        pMItem = (CDDLBase *)GetItemData( HitTestInfo.iItem );
        if (  pMItem->IsKindOf( RUNTIME_CLASS( CDDLMenuItem )) )
        {
            pItem  = ((CDDLMenuItem*)pMItem)->m_item;
        }
        else
        {   /*
             * This should not happen!
             * CDDLMain is no longer used as tree control root menu, POB - 5/18/2014
             */
            return;
        }


        if( pItem )
        {
            pItem->LoadContextMenu( &menu, nPos ) ;

            pSubmenu = menu.GetSubMenu(nPos);// edit/help/display-value...popup
            
            pSubmenu->TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
                cursorPoint.x, cursorPoint.y, GETMAINFRAME);// stevev 08oct10:AfxGetMainWnd());
        }
    }
}

CDDIdeDoc* CMainListCtrl::GetDocument()
{
    CDDIdeDoc * pDoc = NULL;
    /*
     * Get pointer to document if not already done so
     */
    CDocManager * pManager = AfxGetApp()->m_pDocManager;
    ASSERT ( pManager != NULL ); 

    POSITION posTemplate = pManager->GetFirstDocTemplatePosition();
    while ( posTemplate !=NULL )
    {
        // get the next template
        CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
        POSITION posDoc = pTemplate->GetFirstDocPosition();
        while( posDoc !=NULL )
        {
            pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
        }
    }

    return pDoc;
}

void CMainListCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CMainListCtrl::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
    //to fix PAR 5113
    //track the end drag and repaint the list control
    HD_NOTIFY *pHD = (HD_NOTIFY*)lParam;
    if((GetStyle() & LVS_TYPEMASK) == LVS_REPORT)
    {
        if ((pHD->hdr.code == HDN_ENDTRACKA || pHD->hdr.code == HDN_ENDTRACKW))
        {
            RECT rect;
            int nBottom = 0;
            GetClientRect(&rect);
            InvalidateRect(&rect);
        }
    }
    
    return CListCtrl::OnNotify(wParam, lParam, pResult);
}

//to fix PAR 5113
//function taken from XMTR code that puts in elipsis when column is resized and thus preventing overwrite
//of column texts
//this is called in DrawItem
static LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
    static const _TCHAR szThreeDots[]=_T("...");   
    SIZE size;
    int nStringLen=lstrlen(lpszLong);   
   
    if(nStringLen==0 )
        return(lpszLong);

    if (pDC->m_hAttribDC != NULL)
    { // NOTE: use GetTextExtentPoint32 in Win32 for better results
        if ( GetTextExtentExPoint(/*pDC->GetSafeHdc()*/pDC->m_hAttribDC, lpszLong, nStringLen,0,NULL,NULL, &size) == 0 )
        {//error
            TRACE(_T("TextExtent ------- Failure.\n"));
           return lpszLong;
      }
   }
   if(size.cx + nOffset <=nColumnLen )
        return(lpszLong);

    if(nStringLen==0 || ((pDC->GetTextExtent(lpszLong,nStringLen).cx + nOffset) <=nColumnLen) )
        return(lpszLong);


    static _TCHAR szShort[MAX_PATH];

    // crash source::>lstrcpy(szShort,lpszLong);
    lstrcpyn(szShort,lpszLong,MAX_PATH);
    szShort[MAX_PATH-1]=0;
    nStringLen = min(nStringLen,MAX_PATH);

    int nAddLen = pDC->GetTextExtent(szThreeDots, sizeof(szThreeDots) ).cx;

    for(int i = nStringLen-1; i>0; i--)
    {
        szShort[i]=0;
        if ( pDC->GetTextExtent(szShort,i).cx + nOffset + nAddLen<=nColumnLen )
            break;
    }

    // possible crash source::>lstrcat(szShort,szThreeDots);
    StringCchCat(szShort,MAX_PATH,szThreeDots);

    return(szShort);
}


/////////////////////////////////////////////////////////////////////////////
// CDlgListCtrl

CDlgListCtrl::CDlgListCtrl()
{
    m_nSelectedItem = -1;
    m_pImageList = new CImageList;
    m_pImageList->Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    CBitmap bmp;
    bmp.LoadBitmap(IDB_IMAGELIST);
    m_pImageList->Add(&bmp, RGB(255,0,255));
}

CDlgListCtrl::~CDlgListCtrl()
{
    if (m_pImageList)
        delete m_pImageList;
}


BEGIN_MESSAGE_MAP(CDlgListCtrl, CListCtrl)
    //{{AFX_MSG_MAP(CDlgListCtrl)
    ON_WM_LBUTTONDOWN()
    ON_WM_RBUTTONDOWN()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgListCtrl message handlers

/*
 * NOTE: CDlgListCtrl is very similar to CMainListCtrl.
 * We Need to devlope common code in order to remove the duplicate code 
 */
void CDlgListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    CDC* pDC            =CDC::FromHandle(lpDrawItemStruct->hDC);

    LV_COLUMN lvc;
    lvc.mask=LVCF_FMT | LVCF_WIDTH;

    CRect rec = lpDrawItemStruct->rcItem;
    CString strTemp;

    //pDC->FillRect(rec,&CBrush(RGB(255,0,0)));
    GetColumn(0,&lvc);
    rec.right = lvc.cx;

    CDDLBase *pBase = (CDDLBase *)GetItemData(lpDrawItemStruct->itemID);
    if (pBase == NULL)
        return;
        
    if(m_pImageList)
        m_pImageList->Draw(pDC,pBase->GetImage(),CPoint(rec.left,rec.top),ILD_NORMAL/*ILD_TRANSPARENT*/); // Added function to get image ID; POB - 5/16/2014

    strTemp = GetItemText(lpDrawItemStruct->itemID, COL_NAME);
    rec.left += 20; // Offset image

    CFont font, *oldfont = NULL;
    VERIFY(font.CreateFont(
       13,                        // nHeight
       0,                         // nWidth
       0,                         // nEscapement
       0,                         // nOrientation
       FW_BOLD,                 // nWeight
       FALSE,                     // bItalic
       FALSE,                     // bUnderline
       0,                         // cStrikeOut
       ANSI_CHARSET,              // nCharSet
       OUT_DEFAULT_PRECIS,        // nOutPrecision
       CLIP_DEFAULT_PRECIS,       // nClipPrecision
       DEFAULT_QUALITY,           // nQuality
       DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
       _T("Verdana")));           // lpszFacename

    
    pDC->SetTextColor(RGB(0,0,0));  // POB, 13 May 2005 : Paint the controls black

    if(pBase->IsVariable())
    {
        if (pBase->IsReadOnly())
            pDC->SetTextColor(RGB(100,100,100));
        else
        {
            oldfont = pDC->SelectObject(&font);
            pDC->SetTextColor(RGB(0,0,0)); // Vibhor 060703 : Changed to Black 
        }
    }


    //prashant .added this line to fix the overwrite problem during column resizing
    CString strTemp1 = strTemp;
    strTemp = MakeShortString(pDC, strTemp1, rec.right - rec.left, 2*OFFSET);
    //rec.right += 2;

    if ( (lpDrawItemStruct->itemState & ODS_SELECTED) == ODS_SELECTED)
    // 19aug10..added parens because bill's compiler doesn't think I know the order of precedence in C++
    {
        //pDC->FillRect(rec,&CBrush(RGB(0,128,192)));   // Select Blue brush for highlight selection
        pDC->FillRect(rec,&CBrush(RGB(180,180,180)));   // Vibhor 100603 : Changed the color
        //pDC->SetTextColor(RGB(255,255,255));          // White text too

        pDC->DrawText(strTemp,-1,rec,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
    }
    else
    /*Vibhor 076003 : Start of Code */
    {
                pDC->DrawText(strTemp,-1,rec,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
    }
    // pDC->SetTextColor(RGB(64,0,64)); // Vibhor: Commented this line 
    
    /*Vibhor 076003 : End of Code */ 

    /* required to get the correct coordinates sjv 10/5/4*/ 
    GetItemRect(lpDrawItemStruct->itemID,rec,LVIR_LABEL);// get label coords for column 0

    for(int nColumn=1; GetColumn(nColumn,&lvc); nColumn++)
    {
        UINT nFormat = DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER;

        rec.left = rec.right+ 4;
        rec.right += lvc.cx;

        //strTemp = GetItemText(lpDrawItemStruct->itemID,nColumn);
        strTemp = "";
        if (nColumn == COL_DATA)
        {
            nFormat |= DT_RIGHT;

            if (pBase->IsVariable())
            {
        //      CDDLMenuItem *pItem = (CDDLMenuItem *)pBase;
        //      CDDLVariable *pVar = (CDDLVariable *)pItem->m_item;
                CDDLVariable *pVar = (CDDLVariable *) pBase;

                if (pVar != NULL)
                {
                /* stevev 18may09 - special operations are not required...
                    // Check for type Enum
                    vector<EnumTriad_t> eList;
                    if (pVar->GetType() == TYPE_ENUM)
                    {
                        int nTemp = *pVar;
                        strTemp = pVar->GetEnumString(nTemp);
                    }
                    else
                *** enums are just another bvar */
                    {
                        strTemp = *pVar;   // Update data
                    }

                    if (pVar->IsChanged())
                    {
                        pDC->FillRect(rec,&CBrush(RGB(255,255,0)));
                        pDC->SetBkMode( OPAQUE );
                        pDC->SetBkColor (RGB(255,255,0));

                    }
                }
            }
/*          else
            {
                if(pBase->IsEditDisplay())
                {
                    CDDLMenuItem *pItem = (CDDLMenuItem *)pBase;
                    CDDLEDisplay *pEditDisp = (CDDLEDisplay*)pItem->m_item;
                    
                    if(pEditDisp && pEditDisp->IsChanged())
                    {
                        pDC->FillRect(rec,&CBrush(RGB(255,255,0)));
                        pDC->SetBkMode( OPAQUE );
                        pDC->SetBkColor (RGB(255,255,0));

                    }
                }
                
            }*/
        }

        if (nColumn == COL_UNITS)
        {
            if (pBase->IsVariable())
            {
                strTemp = pBase->GetUnitStr();   // Update data
//              TRACE(_T("\n"));
//              TRACE0(strTemp);
            }
        }
    //prashant .added this line to fix the overwrite problem during column resizing
    CString strTemp1 = strTemp;
    strTemp = MakeShortString(pDC, strTemp1, rec.right - rec.left, 2*OFFSET);

        pDC->DrawText(strTemp ,-1, rec, nFormat);
        pDC->SetBkMode( TRANSPARENT );
    }

    if (oldfont)
        pDC->SelectObject(oldfont);

    try
    {
        font.DeleteObject();
    }
    catch(...)
    {
    }
}

#if 0 /* use static call in this module */
/*
 * NOTE:  This is duplicate code copied from CMainListCtrl.
 * Eventually, we need to clean this up where all the objects
 * are calling one function.
 */
LPCTSTR CDlgListCtrl::MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
    static const _TCHAR szThreeDots[]=_T("...");

    int nStringLen=lstrlen(lpszLong);
    
   
   
   if(nStringLen==0 )
        return(lpszLong);   
    SIZE size;
#if 1
   if (pDC->m_hAttribDC != NULL)
   {
       // NOTE: use GetTextExtentPoint32 in Win32 for better results
   if ( GetTextExtentExPoint(/*pDC->GetSafeHdc()*/pDC->m_hAttribDC, lpszLong, nStringLen,0,NULL,NULL, &size) == 0 )
      {//error
        TRACE(_T("TextExtent ------- Failure.\n"));
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();

TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif
           return lpszLong;
      }
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();
CSize  extSz= pDC->GetViewportExt( ) ;
CSize  strSz= pDC->GetOutputTextExtent( lpszLong, nStringLen ) ;
TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif

   //  TRACE(_T("TextExtent Success.\n"));
   }
   if(size.cx + nOffset <=nColumnLen )
        return(lpszLong);
#else
    if(nStringLen==0 || ((pDC->GetTextExtent(lpszLong,nStringLen).cx + nOffset) <=nColumnLen) )
        return(lpszLong);
#endif

    static _TCHAR szShort[MAX_PATH];

    lstrcpy(szShort,lpszLong);
    int nAddLen=pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

    for(int i=nStringLen-1; i>0; i--)
    {
        szShort[i]=0;
        if(pDC->GetTextExtent(szShort,i).cx+nOffset+nAddLen<=nColumnLen)
            break;
    }

    lstrcat(szShort,szThreeDots);

    return(szShort);
}
#endif // if 0 --- use static function

void CDlgListCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
    SelectItem();

    CListCtrl::OnLButtonDown(nFlags, point);
}

void CDlgListCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
    SelectItem();

    CListCtrl::OnRButtonDown(nFlags, point);
}

void CDlgListCtrl::SelectItem()
{
    LV_HITTESTINFO HitTestInfo;
    CPoint         ptClient;
    int            nHitItem;

    DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));
    ptClient = point;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
    
    nHitItem = HitTest( &HitTestInfo );

    if (nHitItem == m_nSelectedItem) // Same item = no action
        return;

    if( LVHT_ONITEM & HitTestInfo.flags )
    {
        if (m_nSelectedItem != -1)  // -1 indicates that no item has been chosen yet
        {
            SetItemState(m_nSelectedItem, 0, LVIS_SELECTED); //NOTE:  POB, this is the correct way to clear a bit in SetItemState() that has been previously selected
            RedrawItems(m_nSelectedItem, m_nSelectedItem); // redraw the item that was cleared
        }
        
        m_nSelectedItem = nHitItem; // set the currently selected item to the one that clicked on 
    }
}

