/*************************************************************************************************
 *
 * $Workfile: DDLMenuItem.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDLMenuItem.cpp: implementation of the CDDLMenuItem class.
 */

#include "stdafx.h"
#include "SDC625.h"
#include "DDLMenuItem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLMenuItem, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLMenuItem::CDDLMenuItem(CDDLBase* item, unsigned qual) : CDDLBase(*item)
{
    ASSERT(item!=NULL);

    m_item = item;
    m_nImage = item->GetImage(); // Added function to get image ID; POB - 5/16/2014
    item->SetParent(this);

#ifdef _DEBUG
    parentID    = 0;
    srcLocation = "constructor";
#endif

    // DD qualifier to individual boolean conversion...because that's the way it is...
    if (qual & READ_ONLY_ITEM)      m_bIsReadOnly = true;
    else                            m_bIsReadOnly = false;

    if (qual & DISPLAY_VALUE_ITEM)  m_bIsDsplyVal = true;
    else                            m_bIsDsplyVal = false;

    if (qual & REVIEW_ITEM)         m_bIsReview = true;
    else                            m_bIsReview = false;

    if (qual & NO_LABEL_ITEM)       m_bHasNoLabel = true;
    else                            m_bHasNoLabel = false;

    if (qual & NO_UNIT_ITEM)        m_bHasNoUnit = true;
    else                            m_bHasNoUnit = false;

    if (qual & INLINE_ITEM)         m_bIsInline = true;
    else                            m_bIsInline = false;

    if (qual & OVERLINE)            m_bHasOverline = true;
    else                            m_bHasOverline = false;

    if (qual & ISREMOTE)            m_bHasRemote = true;
    else                            m_bHasRemote = false;

}

CDDLMenuItem::~CDDLMenuItem()
{
    /*
     * Delete the real item that MenuItem is pointing to
     * m_item is not apart of the childlist
     */
    if (m_item)
        RAZE( m_item );

}

//////////////////////////////////////////////////////////////////////
// Overrides
//////////////////////////////////////////////////////////////////////

// Operators
//////////////////////////////////////////////////////////////////////

// CString cast operator
CDDLMenuItem::operator CString(void)
{ 
    // There will be empty except
    // for CDDLVariable objects
    return *m_item;
}


unsigned long CDDLMenuItem::GetID()
{
    if ( m_item != NULL)
        return (m_item->GetID());
    else
        return 0L;
}

//////////////////////////////////////////////////////////////////////

void CDDLMenuItem::Execute()
{
    m_item->Execute();
}

//Vibhor 1405 : Start of Code
void CDDLMenuItem::Execute(CString szMenuSourceName,CString strHelp)
{
    m_item->Execute(szMenuSourceName,strHelp);
}
//Vibhor 1405 : End of Code

void CDDLMenuItem::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    m_item->LoadContextMenu(pMenu, nPos);  
}

void CDDLMenuItem::OnUpdateMenuExecute(CCmdUI* pCmdUI)
{
     m_item->OnUpdateMenuExecute(pCmdUI);
}

void CDDLMenuItem::DisplayValue()
{
    m_item->DisplayValue();
}

void CDDLMenuItem::SetTreeItem(HTREEITEM hTreeItem)
{
    m_item->SetTreeItem(hTreeItem);
}

HTREEITEM CDDLMenuItem::GetTreeItem()
{
    return m_item->GetTreeItem();
}

/*void CDDLMenuItem::SetParent(CDDLBase* pParent)
{
    m_item->SetParent(pParent);
}

CDDLBase * CDDLMenuItem::GetParent()
{
   return m_item->GetParent();
}*/

CDDLBasePtrList_t& CDDLMenuItem::GetChildList()
{
    return m_item->GetChildList();
}

void CDDLMenuItem::SetValidity(BOOL valid)
{
    m_item->SetValidity(valid);
}

BOOL CDDLMenuItem::IsValid()
{
if (m_item->m_pItemBase && m_item->m_pItemBase->getID() == 0x4052 )
{
    clog<<"Got4052."<<endl;
}


    return m_item->IsValid();
}

BOOL CDDLMenuItem::IsReview()
{
     // Intended only for Menu items
     return m_bIsReview;
}

BOOL CDDLMenuItem::IsReadOnly()
{
     // Intended only for Menu items
    
    /*
     * *** Read Only is only for variables ****
     * m_bIsReadOnly is equivalent to the qualifier READ_ONLY.
     * If not TRUE, check the actual object (i.e., VARIABLE) to 
     * determine if its handling is READ only.
     */
    if (m_bIsReadOnly)
    {
        return m_bIsReadOnly;
    }
    else
    {
        return m_item->IsReadOnly();
    }
        
}

BOOL CDDLMenuItem::IsDisplayValue()
{
     // Intended only for Menu items
     return m_bIsDsplyVal;
}

bool CDDLMenuItem::HasNoLabel()
{
     return m_bHasNoLabel;
}
bool CDDLMenuItem::HasNoUnit()
{
     return m_bHasNoUnit;
}
bool CDDLMenuItem::IsInline()
{
     return m_bIsInline;
}
bool CDDLMenuItem::HasOverline()
{
     return m_bHasOverline;
}
bool CDDLMenuItem::HasARemote()
{
     return m_bHasRemote;
}

void CDDLMenuItem::SetName(CString strName)
{
    m_item->SetName(strName);// never used
}

CString CDDLMenuItem::GetName()
{
    return m_item->GetName();
}

void CDDLMenuItem::SetHelp(CString strHelp)
{
    m_item->SetHelp(strHelp);
}

CString CDDLMenuItem::GetHelp()
{
    return m_item->GetHelp();
}

CString CDDLMenuItem::GetRelation()
{
    return m_item->GetRelation();
}

CDDLMenu* CDDLMenuItem::getMenuPtr(void)
{
    if (m_item && m_item->IsMenu())
        return (CDDLMenu*)m_item;
    else
        return NULL;
}

BOOL CDDLMenuItem::IsMenu()
{
    if (m_item)
        return m_item->IsMenu();
    else
        return FALSE;
}
void CDDLMenuItem::UpdateChildList(void)
{   
    if (m_item)
        m_item->UpdateChildList();
}
BOOL CDDLMenuItem::IsVariable()
{
    return m_item->IsVariable();
}

BOOL CDDLMenuItem::IsMethod()
{
    return m_item->IsMethod();
}

/*Vibhor 170304: Start of Code*/
BOOL CDDLMenuItem::IsEditDisplay()
{
    return m_item->IsEditDisplay();
}
/*Vibhor 170304: End of Code*/

CString CDDLMenuItem::GetUnitStr()
{
    return m_item->GetUnitStr();
}

//////////////////////////////////////////////////////////////////////
// Menu item Specific
//////////////////////////////////////////////////////////////////////
// stevev 04apr11 - I don't know what the elses are trying to do but they are just wrong
BOOL CDDLMenuItem::SetQualifer(unsigned qualifier)
{
    BOOL result = TRUE;

    if (qualifier & READ_ONLY_ITEM)
    {
        m_bIsReadOnly = true;
    }
    //else
    if (qualifier & DISPLAY_VALUE_ITEM)
    {
        m_bIsDsplyVal = true;
    }
    //else
    if (qualifier & REVIEW_ITEM)
    {
        m_bIsReview = true;
    }
    //else
    if (qualifier & NO_LABEL_ITEM)
    {
        m_bHasNoLabel = true;
    }
    //else
    if (qualifier & NO_UNIT_ITEM)
    {
        m_bHasNoUnit = true;
    }
    //else
    if (qualifier & INLINE_ITEM)
    {
        m_bIsInline = true;
    }
    //else
    if (qualifier & OVERLINE)
    {
        m_bHasOverline = true;
    }
    //else
    if (qualifier & ISREMOTE)
    {
        m_bHasRemote = true;
    }
    //else
    //{
    //  result = FALSE;
    //}

    return TRUE;
}


/*************************************************************************************************
 *
 *   $History: DDLMenuItem.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
