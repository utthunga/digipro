#define FILEVER        4,3,04,0
#define STRFILEVER     "4, 3, 04, 0\0"
#define PRODUCTVER     4,3,04,0
#define STRPRODUCTVER  "4.3.04.0\0"



/* the above must start at the very top*/
/*#define FILEVER        2,0,6,696		/ ** should be DEBUG   build only ** /
#define STRFILEVER     "2, 0, 6, 696\0"
#define PRODUCTVER     2,0,6,35			/ ** should be RELEASE build only ** /
#define STRPRODUCTVER  "2.0.6.35\0"
*/
/* a) put VersionMacro.dsm  into 
	C:\Program Files\Microsoft Visual Studio\Common\MSDev98\Macros
   b) Tools/Customize.../Add-Ins and Macros Files
	verify that VersionMacro is there and is checked (file-list is in registry)
	- otherwise browse and add it
	-- that's it --
*/
/********** rules ************
m		2.3.4.5
o		2 - A major format change, conversion to .NET or some other dramatic modification
d		  3 - a 'Release to the members' release ie a MAJOR Release
i			4 - a Release Number - Any release build that may leave the building
f			  5 - a release build number - incremented on every release build
i		The 3 field is incremented  and the 4 & 5 fields zeroed when a major release goes
e		out the door.  We will increment 4 & 5 as seen fit till that verson goes out the door.
d

There is no way to use as fine a grain release numbers as the above rules imply.
New Rules
2.3.4.0
2 - A major format change, conversion to .NET or some other dramatic modification
  3 - a 'Release to the members' release ie a MAJOR Release
	4 - a Build Number - A build that is used for internal release or external release
First used on version 4.0.15
  +++++++++++++++++++++++++++*/
