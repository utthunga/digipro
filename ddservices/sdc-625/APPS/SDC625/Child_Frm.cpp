// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "SDC625.h"

#include "Child_Frm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, FRAME_WND)

BEGIN_MESSAGE_MAP(CChildFrame, FRAME_WND)
	//{{AFX_MSG_MAP(CChildFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !FRAME_WND::PreCreateWindow(cs) )
		return FALSE;
IsOK = TRUE;
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	FRAME_WND::AssertValid();
}
#ifndef _WIN32_WCE
void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}
#endif
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

//DEL BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
//DEL {
//DEL 	
//DEL //	return CMDIChildWnd::OnCreateClient(lpcs, pContext);
//DEL 	CRect rect;
//DEL     CWinApp *pApp = AfxGetApp();
//DEL     CString strDefault = _T("default");
//DEL     CString strSplitterBarPos;
//DEL 
//DEL     if (!m_wndSplitter.CreateStatic(this, 1, 2))
//DEL     {
//DEL          TRACE0("Failed to create split bar ");
//DEL          return( FALSE );    // failed to create
//DEL     }
//DEL 
//DEL 
//DEL     /*
//DEL      * Attempt to restore it to the same size it was when last shutdown
//DEL      */
//DEL     GetClientRect( &rect );
//DEL     CSize size( rect.Size() );
//DEL     strSplitterBarPos = pApp->GetProfileString(  SETTINGS, SPLITTER_BAR, strDefault );
//DEL     if( strSplitterBarPos == strDefault || strSplitterBarPos == _T("") )
//DEL     {
//DEL          size.cx /= 3;
//DEL     }
//DEL     else
//DEL     {
//DEL          size.cx = atoi( (LPCSTR)strSplitterBarPos );
//DEL     }
//DEL 
//DEL     if( !m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CDDIdeTreeView), size, pContext) )
//DEL     {
//DEL          TRACE0("Failed to create tree pane");
//DEL          return(FALSE);    // failed to create
//DEL     }
//DEL 
//DEL 
//DEL 	
//DEL 
//DEL 
//DEL //<START>Added by ANOOP for Splitter pane
//DEL     size.cx *= 2;
//DEL //	size.cy /= 2;
//DEL 	m_wndSplitter_RightPane.CreateStatic(&m_wndSplitter,2,1,WS_CHILD|WS_VISIBLE,m_wndSplitter.IdFromRowCol(0,1));
//DEL 
//DEL     if( !m_wndSplitter_RightPane.CreateView(0, 0, RUNTIME_CLASS(CDDIdeListView), size, pContext) )
//DEL     {
//DEL          TRACE0("Failed to create list pane");
//DEL          return(FALSE);    // failed to create
//DEL     }
//DEL 
//DEL 	if( !m_wndSplitter_RightPane.CreateView(1, 0, RUNTIME_CLASS(CErrLogView), CSize(0,0), pContext) )
//DEL     {
//DEL          TRACE0("Failed to create list pane");
//DEL          return(FALSE);    // failed to create
//DEL     }
//DEL 
//DEL 	if(m_wndSplitter_RightPane) //Added by ANOOP 21FEB2004
//DEL 		m_wndSplitter_RightPane.LockBar();
//DEL 	
//DEL 	//Added By Deepak
//DEL 	// Make sure that errorlog menu item is unchecked
//DEL 	CMenu *pMenu =this ->GetMenu ();			
//DEL 	CMenu* pSubmenu = pMenu->GetSubMenu (1);	// view menu
//DEL 	pSubmenu->CheckMenuItem(ID_VIEW_ERRORLOG, MF_UNCHECKED | MF_BYCOMMAND);
//DEL 	//END mod
//DEL 
//DEL     return(TRUE);
//DEL }
