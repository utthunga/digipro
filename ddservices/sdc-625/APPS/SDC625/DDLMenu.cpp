/*************************************************************************************************
 *
 * $Workfile: DDLMenu.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		DDLMenu.cpp : implementation file
 */
#include "stdafx.h"
#include "SDC625.h"
#include "DDLMenu.h"

#include "ddbMenu.h"
#include "ddbCollAndArr.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLMenu, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLMenu::CDDLMenu(hCitemBase* pIB):CDDLBase(pIB)
{
    m_nImage  = IMG_MENU;
}

CDDLMenu::CDDLMenu(itemInfo_t& rIi):CDDLBase(rIi.p_Ib)
{
    m_nImage     = IMG_MENU;
	m_strName    = rIi.labelStr.c_str();// could be different from item label
	m_strNmValid = rIi.labelFilled;
	m_strHelp    = rIi.help_Str.c_str();
}

CDDLMenu::~CDDLMenu()
{

}


/////////////////////////////////////////////////////////////////////////////
// CDDLMenu message handlers

//////////////////////////////////////////////////////////////////////
// Overrides
//////////////////////////////////////////////////////////////////////

BOOL CDDLMenu::IsMenu()
{
    return TRUE;
}

//////////////////////////////////////////////////////////////////////
// Menu Specific
//////////////////////////////////////////////////////////////////////
// from hCmenu - returns count
int  CDDLMenu::GetMenuItemList(itemInfoList_t& returnList)
{
	if ( m_pItemBase == NULL )
	{
		LOGIT(CERR_LOG,"ERROR: CDDLMenu::GetMenuItemList with no menu item ptr.\n");
		return 0; // error-or none
	}
	itemInfo_t localII;
	menuItemList_t menuItemLst;
	string tmpStr;
	RETURNCODE rc;

#ifdef _DEBUG
	unsigned localID = NULL;
	if (m_pItemBase)  localID = m_pItemBase->getID();
#endif

	if (m_pItemBase->getIType() == iT_Menu )
	{
		rc = ((hCmenu*)m_pItemBase)->aquire(menuItemLst);
	}
	else
	//if (m_pItemBase->getIType() != iT_Collection)
	if (m_pItemBase->getIType() == iT_Collection)//STEVEV 29aug07
	{
		rc = ((hCcollection*)m_pItemBase)->aquire(menuItemLst);
	}
	else
	{
		LOGIT(CERR_LOG,"ERROR: CDDLMenu::GetMenuItemList without a menu type.\n");
		return 0; // error-or none
	}
	menuItemDrawingStyles_t the_style;
	bool notUsable = true;

/* if the last item on a menu is unresolvable, the entire menu is discarded PAR 5629
	if ( rc == SUCCESS && menuItemLst.size() > 0 )
change to the following::	31jan06 */
	if ( menuItemLst.size() > 0 )
	{
		hCmenuItem* pMI = NULL;
		bool haveSeparator = false; // used to carry separator to next item
		for (menuItmLstIT_t i_IT = menuItemLst.begin(); i_IT != menuItemLst.end(); ++i_IT)
		{			
			pMI = (hCmenuItem*) &(*i_IT);// 06apr11 stop using iterator as pointer
			hCreference& itemRef = pMI->getRef();
			the_style = pMI->getDrawAs();
			// mds_separator     mds_newline -- spec says either one will give a line
			// see http://www.vckbase.com/english/code/listview/grid_separator.shtml.htm
			// mds_constString
			// --	we cannot resize a single row of a listcntrl
			//		string will be as long as will fit via shortstring
			//		on dblclick, dialog will popup with the entire string in it
			//
			//	mds_image,
			//	mds_scope,		// charts and graphs
			//	mds_grid,	

			/* all are usable now  -- 30mar11 add support for the eddl constructs on a table
			notUsable = the_style == mds_separator   || 
						the_style == mds_newline     || 
						the_style == mds_constString;
			**/
			if ( the_style == mds_separator || the_style == mds_newline )
			{
				haveSeparator = true;// if this is on the last line, it won't get pushed
			}						 //  that's OK
			else
			if (the_style == mds_constString)
			{
				CValueVarient strVal;
				
				if (( rc = itemRef.resolveExpr(strVal) ) == SUCCESS  && 
					( strVal.vType == CValueVarient::isString   || 
					  strVal.vType == CValueVarient::isWideString )  &&
					strVal.vIsValid )
				{
					localII.labelStr = (wstring)strVal; 
				}
				else
				{//error
					localII.labelStr = L"UNRESOLVED CONSTANT STRING";
				}
				localII.labelFilled = true;
				localII.help_Str = L"";
				localII.p_Ib  = NULL;
				localII.qual  = READ_ONLY_ITEM;
				localII.qual |= ISREMOTE;// has a popup capability
				if (haveSeparator)
				{
					localII.qual |= OVERLINE;
					haveSeparator = false;
				}				
				// pushback makes a deep copy (ie does a new on the menu item )
				// we don't need to do it again...memory leak
				//localII.pddbMenuItem = new hCmenuItem(*pMI);
				localII.pddbMenuItem = pMI;
				returnList.push_back(localII);	
			}
			else
			{				
				if ( itemRef.resolveID(localII.p_Ib) == SUCCESS && localII.p_Ib != NULL )
				{
					// these have to be resolved before thay can be labeled
				rc=itemRef.getItemLabel(localII.labelStr,localII.labelFilled,localII.help_Str);
#ifdef XXXXX_DEBUG
// "everything is DISPLAY_VALUE in the SDC"  WAP 18may11 13:12
				if ( the_style == mds_var && ! pMI->IsDisplayValue())
				{
					localII.labelStr = L"NotDisplayingValue";//for a break point
				}
#endif


					localII.qual = 0;
					if (rc != SUCCESS)// ref label failed
					{
						localII.labelStr = L"Full-Label Not Found";// stevev 29dec10
					}// else all is set

					if ( localII.p_Ib->IsValid() ) // all usable  && (! notUsable) )
					{
						if ( pMI->IsReadOnly() )	localII.qual |= READ_ONLY_ITEM;
						if ( pMI->IsDisplayValue()) localII.qual |= DISPLAY_VALUE_ITEM;
						if ( pMI->IsReview() )	    localII.qual |= REVIEW_ITEM;
						if ( pMI->IsNoLabel() )		localII.qual |= NO_LABEL_ITEM;
						if ( pMI->IsNoUnit() )		localII.qual |= NO_UNIT_ITEM;
						if ( pMI->IsInline())		localII.qual |= INLINE_ITEM;

						if (haveSeparator)
						{
							localII.qual |= OVERLINE;
							haveSeparator = false;
						}						
						if (the_style == mds_image || 
							the_style == mds_scope || 
							the_style == mds_grid  ||
                            // added by POB - 11/30/2013 to handle bit enum display in table style 
                            the_style == mds_var  ||
	                        the_style == mds_index ||
	                        the_style == mds_enum ||
	                        the_style == mds_bitenum ||
	                        the_style == mds_bit)
						{// we're going to leave mds_edDisp as a special case
							localII.qual |= ISREMOTE;// uses popup dialog
							localII.pddbMenuItem = new hCmenuItem(*pMI);
						}
						if (the_style == mds_bit )
						{
							localII.qual |= INDEXED_ITEM;
							hCvarGroupTrail* pGT = itemRef.getGroupTrailPtr();
							localII.indexValue = pGT->containerNumber;
						}

						returnList.push_back(localII);
					}// else skip it
				}
				else
				{
					LOGIT(CERR_LOG,"Error: menu item's reference did not resolve.\n");
					//  just skip
				}
			}
			localII.clear();
		}// next
	}// else return 0
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"WARN: empty menu in 0x%04x (CDDLMenu::GetMenuItemList)\n",m_pItemBase->getID());
	}
#endif
		
	return returnList.size();
}
void CDDLMenu::UpdateChildList(void)
{// refetch from dev object
	TRACE("Updating a menu's childList.\n");
}
// from children - returns count
int  CDDLMenu::GetExistingList(itemInfoList_t& returnList)
{
	itemInfo_t localII;
	POSITION pos = GetChildList().GetHeadPosition();
	for( int nIdx = 0; pos != NULL; nIdx++ )
	{
		CDDLBase *pItem = (CDDLBase*)GetChildList().GetNext( pos );

		localII.p_Ib = pItem->m_pItemBase;
		if (localII.p_Ib ==  NULL)
		{
			// LOGIT(CLOG_LOG,"No itembase ptr in childList.\n");
			continue; // skip it
		}
		localII.labelStr    = (LPCTSTR) (pItem->GetName());
		localII.labelFilled = pItem->GetNameFull();
		returnList.push_back(localII);
	}
	return returnList.size();
}
//compare item IDs ONLY
bool CDDLMenu::areSame(itemInfoList_t& lst1,itemInfoList_t& lst2)
{
	bool ret = false;
	if ( lst1.size() == lst2.size() )
	{
		itmInfoIT_t l1IT , l2IT ;
		ret = true;
		if ( lst1.size() > 0 ) // sizes equal
		{
			// stevev 20may13- they are usually in opposite order
			for (l1IT = lst1.begin(); l1IT != lst1.end();  ++l1IT)
			{
				ret = false;// for next loop
				for (l2IT = lst2.begin(); l2IT != lst2.end(); ++l2IT)
				{
					if ( (l1IT->p_Ib->getID() == l2IT->p_Ib->getID())  )
					{
						ret = true;
						break; // out of for loop
					}
				}// next L2 item
				if ( !ret )// a single non-match is all that is needed
				{
					break;// out of outer loop
				}
			}// next L1 item
			// if the last item was true (and all the previous ones too) ret will be true.
		}// else - leave it true - both empty
	}// else leave it false
	return ret;
}

//compare item IDs AND Labels
bool CDDLMenu::areExactlySame(itemInfoList_t& lst1,itemInfoList_t& lst2)
{
	bool ret = false;
	if ( lst1.size() == lst2.size()  )
	{
		itmInfoIT_t l1IT , l2IT ;
		ret = true;// unless found false in the loop
		if ( lst1.size() > 0 ) // sizes equal
		{
			for (l1IT = lst1.begin(), l2IT = lst2.begin();
				 l1IT != lst1.end() && l2IT != lst2.end(); ++l1IT, ++l2IT)
			{
				if ( (l1IT->labelStr != l2IT->labelStr)          ||
					 (l1IT->p_Ib->getID() != l2IT->p_Ib->getID())  )
				{
					ret = false;
					break; // out of for loop
				}
			}
		}// else - leave it true - both empty
	}// else leave it false
	return ret;
}

// stevev added 02dec05
CString CDDLMenu::GetCurrentName(CDDLBase* pChildItm)
{	
	CString rS; rS.Empty();
	itemInfoList_t localList;
	itemInfo_t* pIInfo;
    
    if (pChildItm) // Check for NULL, POB - 5/19/2014
    {
	    // get latest DevObj menuList
	    GetMenuItemList(localList);
	    //find Item's id in it
	    for ( itmInfoIT_t iT = localList.begin(); iT != localList.end(); ++iT )
	    {
			pIInfo = &(*iT);  // stop using iterator for pointer SJV - 13feb15
            if (pIInfo->p_Ib) // Fixed crash for constant strings, POB - 5/19/2014
            {				  //    Constant strings have a null item pointer
		        if ( pChildItm->GetID() == pIInfo->p_Ib->getID() )
		        {// get referenced name
			        rS = pIInfo->labelStr.c_str();
			        break; // out of for loop
		        }
            }
	    }
    }

	return rS;
}


itemInfo_t& itemInfo_t::operator=(const itemInfo_t& src)
{
	labelStr = src.labelStr; labelFilled = src.labelFilled; 
	p_Ib = src.p_Ib; qual = src.qual;	
	help_Str = src.help_Str; 
	indexValue  = src.indexValue;  
	if (src.pddbMenuItem)
	{
		pddbMenuItem = new hCmenuItem(*(src.pddbMenuItem));
	}
	else
	{
		pddbMenuItem = NULL;
	}
	return *this;
};
itemInfo_t::~itemInfo_t()
{ 	
	if (pddbMenuItem != 0) 
		delete pddbMenuItem; 
	pddbMenuItem = NULL;
}

/*************************************************************************************************
 *
 *   $History: DDLMenu.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */