// BitEnum.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "math.h"
#include "Bit_Enum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


const COLORREF CBitEnum::m_CLOUDBLUE = RGB(128, 184, 223);
const COLORREF CBitEnum::m_WHITE = RGB(255, 255, 255);
const COLORREF CBitEnum::m_BLACK = RGB(1, 1, 1);
const COLORREF CBitEnum::m_DKGRAY = RGB(128, 128, 128);
const COLORREF CBitEnum::m_LTGRAY = RGB(192, 192, 192);
const COLORREF CBitEnum::m_YELLOW = RGB(255, 255, 0);
const COLORREF CBitEnum::m_DKYELLOW = RGB(128, 128, 0);
const COLORREF CBitEnum::m_RED = RGB(255, 0, 0);
const COLORREF CBitEnum::m_DKRED = RGB(128, 0, 0);
const COLORREF CBitEnum::m_BLUE = RGB(0, 0, 255);
const COLORREF CBitEnum::m_DKBLUE = RGB(0, 0, 128);
const COLORREF CBitEnum::m_CYAN = RGB(0, 255, 255);
const COLORREF CBitEnum::m_DKCYAN = RGB(0, 128, 128);
const COLORREF CBitEnum::m_GREEN = RGB(0, 255, 0);
const COLORREF CBitEnum::m_DKGREEN = RGB(0, 128, 0);
const COLORREF CBitEnum::m_MAGENTA = RGB(255, 0, 255);
const COLORREF CBitEnum::m_DKMAGENTA = RGB(128, 0, 128);

#define Rad2Deg	180.0/3.1415 
#define LIGHT_SOURCE_ANGLE	-2.356		// -2.356 radians = -135 degrees, i.e. From top left

#ifdef _WIN32_WCE	/*Vfunction missing PAW 29/04/09*/				
#define SET_PIXEL	SetPixel	
#else
#define SET_PIXEL	SetPixelV
#endif
/////////////////////////////////////////////////////////////////////////////
// CBitEnum

CBitEnum::CBitEnum()
{
	m_bDrawDashedFocusCircle = TRUE;

	m_colorOn = m_RED;
	m_colorOff = m_LTGRAY;
	m_bIsOn = FALSE;
	m_dsplState = TRUE;

	m_color = m_colorOff;
}

CBitEnum::~CBitEnum()
{
	m_rgn.DeleteObject();
}


BEGIN_MESSAGE_MAP(CBitEnum, CButton)
	//{{AFX_MSG_MAP(CBitEnum)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitEnum message handlers

int CBitEnum::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CButton::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	ModifyStyle(0, BS_OWNERDRAW);

	CRect rect;
	GetClientRect(rect);

	// Resize the window to make it square
	rect.bottom = rect.right = min(rect.bottom,rect.right);
	
	// Get the vital statistics of the window
	m_ptCentre = rect.CenterPoint();
	m_nRadius  = rect.bottom/2-1;

	// Set the window region so mouse clicks only activate the round section 
	// of the button
	m_rgn.DeleteObject(); 
	SetWindowRgn(NULL, FALSE);
#ifndef _WIN32_WCE		/*PAW function missing 29/04/09 */
	m_rgn.CreateEllipticRgnIndirect(rect);
#endif
	SetWindowRgn(m_rgn, TRUE);

	// Convert client coords to the parents client coords
	ClientToScreen(rect);
	CWnd* pParent = GetParent();
	if (pParent) pParent->ScreenToClient(rect);

	// Resize the window
	MoveWindow(rect.left, rect.top, rect.Width(), rect.Height(), TRUE);
	
//	EnableWindow(FALSE);
//	SetOn(TRUE);

	return 0;
}

void CBitEnum::PreSubclassWindow() 
{
	CButton::PreSubclassWindow();

	ModifyStyle(0, BS_OWNERDRAW);

	CRect rect;
	GetClientRect(rect);

	// Resize the window to make it square
	rect.bottom = rect.right = min(rect.bottom,rect.right);

	// Get the vital statistics of the window
	m_ptCentre = rect.CenterPoint();
	m_nRadius  = rect.bottom/2-1;

	// Set the window region so mouse clicks only activate the round section 
	// of the button
	m_rgn.DeleteObject(); 
	SetWindowRgn(NULL, FALSE);
#ifndef _WIN32_WCE		/*PAW function missing 29/04/09 */	
	m_rgn.CreateEllipticRgnIndirect(rect);
#endif	
	SetWindowRgn(m_rgn, TRUE);

	// Convert client coords to the parents client coords
	ClientToScreen(rect);
	CWnd* pParent = GetParent();
	if (pParent) pParent->ScreenToClient(rect);

	// Resize the window
	MoveWindow(rect.left, rect.top, rect.Width(), rect.Height(), TRUE);
}

//DEL HBRUSH CBitEnum::CtlColor(CDC* pDC, UINT nCtlColor) 
//DEL {
//DEL 	CBrush m_brush;
//DEL 
//DEL 	COLORREF clr = DKRED;
//DEL 
//DEL 	if ((GetState() & 0x0003))
//DEL 	{
//DEL 		// On color
//DEL 		pDC->SetTextColor(DKGREEN);
//DEL 	}
//DEL 	else
//DEL 	{
//DEL 		// Off color
//DEL 		pDC->SetTextColor(DKRED);
//DEL 	}
//DEL 
//DEL 	pDC->SetBkMode (TRANSPARENT);
//DEL 	
//DEL 
//DEL 	HBRUSH brush = (HBRUSH)m_brush;
//DEL 	brush = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
//DEL 
//DEL //	return NULL;
//DEL 	return brush;
//DEL }

void CBitEnum::OnClicked() 
{
/*	SetCheck( !(GetState() & 0x0003) );

	if ((GetState() & 0x0003))
	{
		SetWindowText("ON");
	}
	else
	{
		SetWindowText("OFF");
	}*/

	//	SetState( !(GetState() & 0x0004) );

/*	if (IsOn())
	{
		// If the button is on, turn it off
		SetOn(FALSE);
	}
	else
	{
		// If the button is off, turn it back on
		SetOn(TRUE);
	}*/
}

// Calculate colour for a point at the given angle by performing a linear
// interpolation between the colours crBright and crDark based on the cosine
// of the angle between the light source and the point.
//
// Angles are measured from the +ve x-axis (i.e. (1,0) = 0 degrees, (0,1) = 90 degrees )
// But remember: +y points down!

COLORREF CBitEnum::GetColor(double dAngle, COLORREF crBright, COLORREF crDark)
{

//	ASSERT(dAngle > -3.1416 && dAngle < 3.1416);
	double dAngleDifference = LIGHT_SOURCE_ANGLE - dAngle;

	if (dAngleDifference < -3.1415) dAngleDifference = 6.293 + dAngleDifference;
	else if (dAngleDifference > 3.1415) dAngleDifference = 6.293 - dAngleDifference;

	double Weight = 0.5*(cos(dAngleDifference)+1.0);

	BYTE Red   = (BYTE) (Weight*GetRValue(crBright) + (1.0-Weight)*GetRValue(crDark));
	BYTE Green = (BYTE) (Weight*GetGValue(crBright) + (1.0-Weight)*GetGValue(crDark));
	BYTE Blue  = (BYTE) (Weight*GetBValue(crBright) + (1.0-Weight)*GetBValue(crDark));

	//TRACE("LightAngle = %0.0f, Angle = %3.0f, Diff = %3.0f, Weight = %0.2f, RGB %3d,%3d,%3d\n", 
	//	  LIGHT_SOURCE_ANGLE*Rad2Deg, dAngle*Rad2Deg, dAngleDifference*Rad2Deg, Weight,Red,Green,Blue);

	return RGB(Red, Green, Blue);
}

void CBitEnum::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)  
{
	ASSERT(lpDrawItemStruct != NULL);
	
	CDC* pDC   = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rect = lpDrawItemStruct->rcItem;
	UINT state = lpDrawItemStruct->itemState;
	UINT nStyle = GetStyle();
	int nRadius = m_nRadius;

	int nSavedDC = pDC->SaveDC();

	pDC->SelectStockObject(NULL_BRUSH);
	pDC->FillSolidRect(rect, ::GetSysColor(COLOR_BTNFACE));

	// Draw the focus circle around the button
//	if ((state & ODS_FOCUS) && m_bDrawDashedFocusCircle)
//		DrawCircle(pDC, m_ptCentre, nRadius--, RGB(0,0,0));

	if (state & ODS_DISABLED)
	{
	}
	else
	{
		DrawCircle(pDC, m_ptCentre, nRadius--, RGB(0,0,0));
	}
	

	// Draw the raised/sunken edges of the button (unless flat)
	if (nStyle & BS_FLAT)
	{
		DrawCircle(pDC, m_ptCentre, nRadius--, RGB(0,0,0));
		DrawCircle(pDC, m_ptCentre, nRadius--, ::GetSysColor(COLOR_3DHIGHLIGHT));
	} 
	else 
	{
		if ((state & ODS_SELECTED))	
		{
			DrawCircle(pDC, m_ptCentre, nRadius--, 
					   ::GetSysColor(COLOR_3DDKSHADOW), ::GetSysColor(COLOR_3DHIGHLIGHT));
			DrawCircle(pDC, m_ptCentre, nRadius--, 
					   ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DLIGHT));
		} 
		else 
		{

			if (m_dsplState)
			{
/*				if (state & ODS_DISABLED)  // if window is not enabled (ie, Dimmed state)
				{
					DrawCircle(pDC, m_ptCentre, nRadius--, 
						   ::GetSysColor(COLOR_3DHIGHLIGHT), BLUE); // outside circle
				}
				else
				{*/
					DrawCircle(pDC, m_ptCentre, nRadius--, 
						   ::GetSysColor(COLOR_3DHIGHLIGHT), ::GetSysColor(COLOR_3DDKSHADOW));  // outside circle
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);
					DrawCircle(pDC, m_ptCentre, nRadius--, m_color, m_color);

//				}
			}
			else
			{
				DrawCircle(pDC, m_ptCentre, nRadius--, 
						   ::GetSysColor(COLOR_3DHIGHLIGHT), m_DKGRAY); // outside circle
			}
		}
	}
	
	// draw the text if there is any
	CString strText;
	GetWindowText(strText);

	if (!strText.IsEmpty())
	{
		CRgn rgn;
#ifndef _WIN32_WCE		/*PAW function missing 29/04/09 */
		rgn.CreateEllipticRgn(m_ptCentre.x-nRadius, m_ptCentre.y-nRadius, 
							  m_ptCentre.x+nRadius, m_ptCentre.y+nRadius);
#endif							  
		pDC->SelectClipRgn(&rgn);

		CSize Extent = pDC->GetTextExtent(strText);
		CPoint pt = CPoint( m_ptCentre.x - Extent.cx/2, m_ptCentre.x - Extent.cy/2 );

		if (state & ODS_SELECTED) pt.Offset(1,1);

		pDC->SetBkMode(TRANSPARENT);
#ifndef _WIN32_WCE		/*PAW function missing 29/04/09 */
		if (state & ODS_DISABLED)
			pDC->DrawState(pt, Extent, strText, DSS_DISABLED, TRUE, 0, (HBRUSH)NULL);
		else
			pDC->TextOut(pt.x, pt.y, strText);
#endif
		pDC->SelectClipRgn(NULL);
		rgn.DeleteObject();
	}

	// Draw the focus circle on the inside of the button
//	if ((state & ODS_FOCUS) && m_bDrawDashedFocusCircle)
//		DrawCircle(pDC, m_ptCentre, nRadius-2, RGB(0,0,0), TRUE);

	pDC->RestoreDC(nSavedDC);
//	CButton::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CBitEnum::DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crColour, BOOL bDashed)
{
	const int nDashLength = 1;
	LONG lError, lXoffset, lYoffset;
	int  nDash = 0;
	BOOL bDashOn = TRUE;

	//Check to see that the coordinates are valid
	ASSERT( (p.x + lRadius <= LONG_MAX) && (p.y + lRadius <= LONG_MAX) );
	ASSERT( (p.x - lRadius >= LONG_MIN) && (p.y - lRadius >= LONG_MIN) );

	//Set starting values
	lXoffset = lRadius;
	lYoffset = 0;
	lError   = -lRadius;

	do {
		if (bDashOn) {
			pDC->SET_PIXEL(p.x + lXoffset, p.y + lYoffset, crColour);
			pDC->SET_PIXEL(p.x + lXoffset, p.y - lYoffset, crColour);
			pDC->SET_PIXEL(p.x + lYoffset, p.y + lXoffset, crColour);
			pDC->SET_PIXEL(p.x + lYoffset, p.y - lXoffset, crColour);
			pDC->SET_PIXEL(p.x - lYoffset, p.y + lXoffset, crColour);
			pDC->SET_PIXEL(p.x - lYoffset, p.y - lXoffset, crColour);
			pDC->SET_PIXEL(p.x - lXoffset, p.y + lYoffset, crColour);
			pDC->SET_PIXEL(p.x - lXoffset, p.y - lYoffset, crColour);
		}

		//Advance the error term and the constant X axis step
		lError += lYoffset++;

		//Check to see if error term has overflowed
		if ((lError += lYoffset) >= 0)
			lError -= --lXoffset * 2;

		if (bDashed && (++nDash == nDashLength)) {
			nDash = 0;
			bDashOn = !bDashOn;
		}

	} while (lYoffset <= lXoffset);	//Continue until halfway point
} 

void CBitEnum::DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crBright, COLORREF crDark)
{
	LONG lError, lXoffset, lYoffset;

	//Check to see that the coordinates are valid
	ASSERT( (p.x + lRadius <= LONG_MAX) && (p.y + lRadius <= LONG_MAX) );
	ASSERT( (p.x - lRadius >= LONG_MIN) && (p.y - lRadius >= LONG_MIN) );

	//Set starting values
	lXoffset = lRadius;
	lYoffset = 0;
	lError   = -lRadius;

	do {
		const double Pi = 3.141592654, 
					 Pi_on_2 = Pi * 0.5,
					 Three_Pi_on_2 = Pi * 1.5;
		COLORREF crColour;
		double   dAngle = atan2((double)lYoffset, (double)lXoffset);// PAW (double)

		//Draw the current pixel, reflected across all eight arcs
		crColour = GetColor(dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x + lXoffset, p.y + lYoffset, crColour);

		crColour = GetColor(Pi_on_2 - dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x + lYoffset, p.y + lXoffset, crColour);

		crColour = GetColor(Pi_on_2 + dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x - lYoffset, p.y + lXoffset, crColour);

		crColour = GetColor(Pi - dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x - lXoffset, p.y + lYoffset, crColour);

		crColour = GetColor(-Pi + dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x - lXoffset, p.y - lYoffset, crColour);

		crColour = GetColor(-Pi_on_2 - dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x - lYoffset, p.y - lXoffset, crColour);

		crColour = GetColor(-Pi_on_2 + dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x + lYoffset, p.y - lXoffset, crColour);

		crColour = GetColor(-dAngle, crBright, crDark);
		pDC->SET_PIXEL(p.x + lXoffset, p.y - lYoffset, crColour);

		//Advance the error term and the constant X axis step
		lError += lYoffset++;

		//Check to see if error term has overflowed
		if ((lError += lYoffset) >= 0)
			lError -= --lXoffset * 2;

	} while (lYoffset <= lXoffset);	//Continue until halfway point
} 

void CBitEnum::SetOn(BOOL state)
{
	if (m_bIsOn == state)
		return; // no change return

	m_bIsOn = state;

	if (m_bIsOn == TRUE)
		m_color = m_colorOn;
	else m_color = m_colorOff;
	
	Invalidate();

}

BOOL CBitEnum::IsOn()
{
	return m_bIsOn;
}

void CBitEnum::EnableButton(BOOL state)
{
	m_dsplState = state;
	Invalidate();
}

BOOL CBitEnum::Toggle()
{
	if (IsOn())
	{
		// If the button is on, turn it off
		SetOn(FALSE);
	}
	else
	{
		// If the button is off, turn it back on
		SetOn(TRUE);
	}

	// return the toggle state
	return IsOn();
}
