// RespCodeDevStat.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "sdc625doc.h"
#include "RespCodeDevStat.h"

#include "Mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRespCodeDevStat dialog

/* stevev 09jun06 - make it an sdc Dialog
CRespCodeDevStat::CRespCodeDevStat(CWnd* pParent //*=NULL*x/)
	): CDialog(CRespCodeDevStat::IDD, pParent)
*/
CRespCodeDevStat::CRespCodeDevStat(CWnd* pParent /*=NULL*/)
	: sdCDialog(CRespCodeDevStat::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRespCodeDevStat)
	m_nSuppressCnt = DEFAULT_SUPPRESS_CNT;
	m_strDispMsg = _T("");
	m_bIgnoreFlag = FALSE;
	//}}AFX_DATA_INIT
}


void CRespCodeDevStat::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRespCodeDevStat)
	DDX_Control(pDX, IDOK, m_bOk);
	DDX_Control(pDX, ID_IGNORE, m_bIgnore);
	DDX_Control(pDX, IDC_SPIN, m_Spin);
	DDX_Text(pDX, IDC_EDIT_IGNORE_CNT, m_nSuppressCnt);
	DDV_MinMaxUInt(pDX, m_nSuppressCnt, 0, 9999);
	DDX_Text(pDX, IDC_EDIT_MESSAGE, m_strDispMsg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRespCodeDevStat, CDialog)
	//{{AFX_MSG_MAP(CRespCodeDevStat)
	ON_BN_CLICKED(ID_IGNORE, OnIgnore)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRespCodeDevStat message handlers

void CRespCodeDevStat::OnIgnore() 
{
	m_bIgnoreFlag = TRUE;

	UpdateData(TRUE);

/* stevev 09jun06 - make it an sdc Dialog	CDialog::EndDialog(0);*/
	sdcEndDialog(0);
}

void CRespCodeDevStat::OnOK() 
{
/* stevev 09jun06 - make it an sdc Dialog	CDialog::OnOK();*/
	sdCDialog::OnOK();
		
	m_bIgnoreFlag = FALSE;

}

BOOL CRespCodeDevStat::OnInitDialog() 
{
//stevev 14jun06	CDialog::OnInitDialog();
	sdCDialog::OnInitDialog();

	CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd;

	CDDIdeDoc *pDoc = pMainWnd->m_pDoc;

	m_nSuppressCnt = pDoc->m_uSuppressionCnt;

	m_Spin.SetRange32(0,99999);

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
