/*********************************************************************************************

// DynControls.cpp: implementation of the CDynDialogItemEx class.

*/

#include "stdafx.h"
#include "ddbItemBase.h"
#include "Dyn_Controls.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static CRect ZeroRect(0,0,0,0);


///////////////////////////////////////////////////////////////////////////////////////////////
// CDynControls dialog

// pPrevRect  Points to previous rectange inserted
// bNextRow   TRUE  - Insert the control on the next row (DEFAULT) after the
//                    pPrevRect rectangle region
//            FALSE - Insert the control on the next column to the right of 
//                    pPrevRect rectangle region (acts like SEPERATOR)

// stevev 28oct10 - added colMultiplier to to the signature to enable multi-width columns
//                  its value is canvasSize/rowSize used as multiplier to col size
CDynControls::CDynControls(pnt_t topLeft, double colMultiplier, 
						   CWnd* pWndParent /*=NULL*/, int xOffset )
	: sdCDialog()	/* CDialog()*/
{
	//{{AFX_DATA_INIT(CDynControls)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	m_DDLrow    = topLeft.yval;
	m_DDLcolumn = topLeft.xval;

	columnMultiplier = colMultiplier;// number of columns to add
	
	m_DialogTemplate.style				= WS_CAPTION | WS_SYSMENU | WS_VISIBLE | DS_SETFONT;
	m_DialogTemplate.dwExtendedStyle    = WS_EX_DLGMODALFRAME;
	m_DialogTemplate.x					= 0;
	m_DialogTemplate.y					= 0;
	m_DialogTemplate.cx					= 0; // 4 horizontal units are the width of one char
	m_DialogTemplate.cy					= 0; // 8 vertical units are the height of one char
	m_DialogTemplate.cdit				= 0;  // nr of dialog items in the dialog

	m_pMainFrameClass = GETMAINFRAME;// hold 26oct (CMainFrame*)pWndParent;
	m_pParentWnd	= pWndParent; // hold 26oct 
					// was::> CWnd::FromHandle(m_pMainFrameClass->GetSafeHwnd());//pWndParent;
	m_pDialogWnd    = m_pParentWnd;
	m_strCaption	= _T("");
	m_pFont			= NULL;
	m_pMenuButton   = NULL; // POB - 7 Sep 2004
	m_pImageButton  = NULL; // sjv - 9jan05
	m_wFontSize		= 0;
	m_nCurRow       = ( ( m_DDLrow    - 1 ) * ONEROW ) + FIRSTROW1;
//	m_nCurColumn    = ( ( m_DDLcolumn - 1 ) * ( COLUMSTEPSIZE * 3    ) ) + FIXEDCOL1 
//														   + ( COLUMNGAP * (m_DDLcolumn - 1) );
	
	int dbgVal      = ( ( m_DDLcolumn - 1 ) * COLSIZE ) + FIXEDCOL1 
													    + ( COLUMNGAP * (m_DDLcolumn - 1) );
/**** original offset/left locator alg
//  get variable width columns
double y = m_DDLcolumn - 1 + columnMultiplier;// space from left
//int c = COLUMSTEPSIZE * 3;
// get the normal space to left
////int w = COLSIZE * y;
// take normal left plus centering (column * multiplier)
double t = COLSIZE * y;
// add half the col diff to get the middle of expanded column
//16jul12 remove t += ((COLSIZE * (columnMultiplier-1))/2);
// add left offset
t += ( FIXEDCOL1 + ( COLUMNGAP * y ) );
// round it to int
m_nCurColumn = (int)(t + .5) + xOffset; // left of top left point
*****/

//      # col on left     number of columns to add for centering
double y =	(m_DDLcolumn - 1) + columnMultiplier;
double t = (COLSIZE * y) + FIXEDCOL1  + (COLUMNGAP * y);
// round it to int
m_nCurColumn = (int)(t + .5);  

//                                                         I
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"    DynControl Construction: "
		"CurCol= %d, CurRow= %d and DDLcol= %d DDLrow= %d     mul:%f  offset:%2d\n",
		m_nCurColumn, m_nCurRow, m_DDLcolumn, m_DDLrow, columnMultiplier, xOffset);

	m_bAddSystemButtons = FALSE;  // POB - 18 Aug 2004, changed from TRUE
	m_ButtonCount       = 0;	  // stevev 4oct05 - dialog=0;menuDisplay 2 to 5

	m_bIsFontCreated = FALSE;
	m_bModelessDlg   = FALSE;
	m_bAddGroupBox   = FALSE;
	m_bAddTab        = FALSE;

	m_nCurRowInit    = m_nCurRow;
	m_pdrawDlg = NULL;        // POB, 18 Oct 2004
	m_isCntrlsReady = FALSE;  // POB, 18 Oct 2004
	m_pIbLink  = NULL;		// sjv 7oct05
	tmrHndl = 0;
	
    m_vCurHeight = m_hCurWidth = 0;
	m_vScrollPos=m_hScrollPos = 0;
	baseunitX = baseunitY = 0;
	m_rect.SetRectEmpty( ); 
	vScrollBarWidth = hScrollBarWidth = 0;
	vPage = hPage = 0;
	// stevev 30jul07 - complete the clear on instantiation
	dskTopRect.left = dskTopRect.top = dskTopRect.right = dskTopRect.bottom = 0L;
	convRect = dskTopRect;
	ReturnObjRect = dskTopRect;
	m_nAvailLeftDlg = m_nAvailTopDlg = m_nAvailLeftColumn = m_nAvailTopRow = -1L;
	pDraw = NULL;
}
void CDynControls::OnDestroy() 
{	
	TRACE(_T("CDynControls:OnDestroy......\n"));
	if ( tmrHndl )
	{
		KillTimer(DLG_TIMER);
		tmrHndl = 0;
	}
	m_pMainFrameClass->DeRegisterDialog(this);

}
CDynControls::~CDynControls()
{
	/* added stevev 21jun06*/ m_pMainFrameClass->DeRegisterDialog(this);
	CDynDialogItemEx *pDynDialogItemEx = NULL;
    for (int i = m_arrDlgItemPtr.GetSize() - 1; i >= 0; i--)
    {
		pDynDialogItemEx = m_arrDlgItemPtr[i];
        if (pDynDialogItemEx != NULL)
        {
			delete pDynDialogItemEx;
			pDynDialogItemEx = NULL;
		}
	}

    if (m_bIsFontCreated)
    {
//		delete m_pFont;
	}

	m_pdrawDlg = NULL;
	/*
	 * The memory for m_pMenuButton was deleted above when
	 * pDynDialogItemEx was deleted
	 */ 
}



//This is added to so that the user cannot quit the methods dialog using the
//esc key or the enter key during methods execution.

BOOL CDynControls::PreTranslateMessage(MSG* pMsg) 
{
	if (m_ButtonCount <= 0)
	{
		return sdCDialog::PreTranslateMessage(pMsg);
	}
	else	// we're a displayMenu
	{
		if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
						 return FALSE;
		//if ( pMsg->message == WM_COMMAND && pMsg->wParam == IDCANCEL )
		//{
		//	TRACE(_T("got 0x%04x\n"),pMsg->message);
		//}		
		CWnd *pWnd   = GetFocus();
		bool mtch = false;
		for ( int y = 0; y < m_ButtonCount; y++)
		{
			mtch = mtch | (pWnd == GetDlgItem(BUTTONID[y]));
		}


		if( ! mtch )
		{
			if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
				return FALSE;
			if (pMsg->message == WM_LBUTTONDOWN)
			{
				//OnLButtonDown
			}
		}
		return sdCDialog::PreTranslateMessage(pMsg);
	}
}


LRESULT CDynControls::OnDrawMethodUI(WPARAM wParam, LPARAM) 
{
	// nop at this time - check out the m_structUIData.bMethodAbortedSignalToUI
	return TRUE;
}




CWnd *CDynControls::GetParent()
{
	return m_pParentWnd;
}

void CDynControls::AddStyles(DWORD dwStyles)
{
	m_DialogTemplate.style |= dwStyles;
}

void CDynControls::RemoveStyles(DWORD dwStyles)
{
	m_DialogTemplate.style &= (~dwStyles);
}

void CDynControls::DoDataExchange(CDataExchange* pDX)
{
	sdCDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynControls)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
/*	CDynDialogItemEx *pDynDialogItemEx = NULL;
	for (int i = m_arrDlgItemPtr.GetSize() - 1; i >= 0; i--) {
		pDynDialogItemEx = m_arrDlgItemPtr[i];
		if (pDynDialogItemEx != NULL) {
			pDynDialogItemEx->DoDataExchange(pDX);
		}
	}*/
}

BEGIN_MESSAGE_MAP(CDynControls, sdCDialog)/*CDialog)*/
	//{{AFX_MSG_MAP(CDynControls)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
ON_COMMAND(ID_MENU_LABEL, OnMenuLabel) // Table and Window share the same label ID - POB, 3/24/14
    ON_COMMAND(ID_MENU_DISPLAY_VALUE, OnMenuDisplayValue)
	ON_WM_RBUTTONDOWN()	
	ON_WM_LBUTTONDOWN()
#ifndef _WIN32_WCE	// PAW function missing 29/04/09
	ON_WM_ENTERIDLE()
#endif
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_HELP, OnHelpMsg)
	ON_WM_MEASUREITEM()		// TMB! 06-12-2001 Adjusted.. was wrongly an ON_MESSAGE()!!
	ON_WM_DRAWITEM()			// TMB! 06-12-2001 Adjusted.. was wrongly an ON_MESSAGE()!!
	ON_MESSAGE(WM_DRAWMETHODUI, OnDrawMethodUI)
	ON_MESSAGE(WM_SDC_VAR_CHANGED, OnVarChnge)// stevev 19oct05
	ON_MESSAGE(WM_SDC_STRUCTURE_CHANGED,OnStructChnge)// stevev 19oct05
	ON_MESSAGE(WM_SDC_VAR_CHANGEDVALUE,OnValueChnge)// stevev 19oct05
	ON_WM_VSCROLL() // stevev 08mar07
	ON_WM_HSCROLL() // stevev 08mar07
	ON_WM_SIZE( )   // stevev 08mar07
END_MESSAGE_MAP()
/* hold 26oct
	ON_COMMAND(WM_DLG_ABORT, OnDlgAbort)
	ON_COMMAND(WM_DLG_BUT01,OnDlgBut01)
	ON_COMMAND(WM_DLG_BUT02,OnDlgBut02)
	ON_COMMAND(WM_DLG_BUT03,OnDlgBut03)
	ON_COMMAND(WM_DLG_OK,	OnDlgOK)
* end hold */

/////////////////////////////////////////////////////////////////////////////
// CDynControls message handlers

/*
 * My own Create (no dialog is produced if CWnd is NULL (default)
 *
 * CWnd  =  NULL, A modeless dialog will be produced
 * CWnd !=  NULL, No dialog will be produced
 *
 * return   Pointer to CPoint for location of next
 *          available dialog coordinates to create another
 *          CDynControls object
 *
 * NOTE:  Only DoModal() will create modal dialog
 */
CRect & CDynControls::Create(CWnd * pDialogWnd)
{
	// use class's return object...CRect  objRect;
	
	if (pDialogWnd)
	{
		m_pDialogWnd = pDialogWnd;
//		AddFinalControls();
	}
	else
	{
		// There is no dialog window
		// Must create our own
        // SDC does not curently call this code.

        // This code is used to call a Modless child window.
        // SDC uses another class to generate a child window.
        // comment this out for now, POB - 25 Aug 2008
//		m_pDialogWnd = this;
//		AddFinalControls();
//		SetUseSystemButtons();  // Add OK and Cancel buttons
//		SetUseModeless();       // make it modeless
//		BuildDlgTemplate();     // make dialog size to fit
        // 19aug10....return CRect(0,0,0,0); 
		return ZeroRect;
	}

	GetDlgRect(&ReturnObjRect);

	CreateControls();
	Initialize();

	return ReturnObjRect;
}

int CDynControls::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (sdCDialog::OnCreate(lpCreateStruct) == -1)
    {
		return -1;
    }
	
	// Only if Modal
	if (!m_bModelessDlg)
	{
		// The controls must exist in this object
		CreateControls();
	}

	return 0;
}

int CDynControls::CreateControls(/*LPCREATESTRUCT lpCreateStruct*/) 
{
	SetFont();

	// if we have no font, create one here
	//
	// NOTE:  The font in m_pFont produced by SetFont() above
	//        is returning NULL when creating a dialog template
	//        window.  Must create one here if we are to have the
	//        same font for the "OK" and "CANCEL" buttons as the rest
	//        of the controls
	if (m_pFont == NULL) 
	{
        // Warning this font does not match the font used in other portions
        // of SDC.  If used, then text will be much smaller than expected.
        // 
        // CreateControls() should never be called if there is no font 
        // POB, 15 Aug 2008
/*		LOGFONT LogFont;

		// Can do better???
		memset(&LogFont, 0x00, sizeof(LogFont));
		_tcscpy(LogFont.lfFaceName, CONST_STRING_FONT);
		LogFont.lfHeight = CONST_STRING_HIGT;

		m_pFont = new CFont;
		if ( ! m_pFont->CreateFontIndirect(&LogFont) )
			LOGIT(CERR_LOG,"Couldnot create Font for dialog.\n");
		SetFont(m_pFont);
		m_bIsFontCreated = TRUE;*/
	}

	//Create all the controls on the dialog
	CDynDialogItemEx *pDynDialogItemEx = NULL;
    for (int i = 0; i < m_arrDlgItemPtr.GetSize(); i++)
    {
		pDynDialogItemEx = m_arrDlgItemPtr[i];
        if (pDynDialogItemEx != NULL)
        {
			if (!pDynDialogItemEx->IsDlgItemSubclassed()) 
			{
				if (!pDynDialogItemEx->CreateEx(/*this*/m_pDialogWnd)) 
				{
					AfxMessageBox(_T("Failed to create DlgItem."));
				}
				else 
				/*if (pDynDialogItemEx->GetSafeHwnd() != NULL)*/  
				         // Prevented Font from parent for subclassed controls- POB,8 Sept 2004
				{
                    if (m_pFont)
                    {
						pDynDialogItemEx->SetFont(m_pFont, FALSE);
					}
				}
			}
		}
	}
	
	return 0;
}

BOOL CDynControls::OnInitDialog() 
{
	//Reposition all the controls on the dialog...
	// stevev - removed commented out code 13oct05

	pnt_t temp = {1,1};

    // Adding System buttons code moved from BuildDlgTemplate(), POB - 25 Aug 2008
    //
    // Calling this code in OnInitDialog() ensures that we have an instantiated dialog
    // for us to get the font and apply it to the text in the system buttons ("OK", "Cancel") 
    // so that they appear the same as the other DDL buttons and controls.
    //
    // The BuildDlgTemplate() used to create these extra buttons as part of the dlg template,
    // but the font was being created via software and not from GetFont() from the dialog after
    // it had been instianted.

    // Now, we treat these buttons the same as we do for all the DDL items in a window since 
    // the other DDL controls are created in createMenu() below.

   	if (m_bAddSystemButtons)  //Do we need OK and Cancel buttons??
    {
		AddSystemButtons();
        CreateControls();    // Must create controls before calling Initialize() below
	}

	if (!m_bModelessDlg)
	{
		if (m_pdrawDlg)
		{
#ifdef LOGBIRTH
	ldepth++;
#endif
			// The controls are designed to be created outside of this object
			// Instantiate them here
			m_pdrawDlg->createMenu(this, temp, 0);
#ifdef LOGBIRTH
	ldepth--;
#endif

			m_isCntrlsReady = TRUE;

			SetWindowText(m_pdrawDlg->GetLabel());
		}
// moved to do modal...
// re-inserted for regression
		m_pMainFrameClass->RegisterDialog(this);
		/*
		 * Start a timer to keep the idle task running (don't know why....)
		 */
		tmrHndl = SetTimer(DLG_TIMER, 500, NULL); 	
		DEBUGLOG(CLOG_LOG,"Dialog's Timer set to half second.\n");

		if (!tmrHndl)
        {
			MessageBox(M_UI_NO_TIMER);
        }

		Initialize();

	}
	

	hScrollBarWidth = GetSystemMetrics(SM_CYHSCROLL);
	vScrollBarWidth = GetSystemMetrics(SM_CXVSCROLL);
	m_vScrollPos    = m_hScrollPos = 0;
	// now calc base units
	CRect entryRect;	
	GetDlgRect   ( entryRect );  // drawport in dialog coords

	resetBaseUnits(entryRect);

//	CRect tmpRect(hScrollBarWidth,vScrollBarWidth,0,0);// l t r b
//	Pixels2DlgUnits(&tmpRect);
//	hScrollBarWidth = tmpRect.left;
//	vScrollBarWidth = tmpRect.top;  // now in dialogUnits

	sizeDialog(); // all those messy calculations

//stevev 14jun06	CDialog::OnInitDialog();
	sdCDialog::OnInitDialog();


	CenterWindow();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDynControls::OnTimer(UINT nIDEvent) 
{
	timerCount++;
	if (timerCount > 1)
	{
		timerCount = 0;
		TRACE("    staleDynamics()  \n");
		m_pdrawDlg->staleDynamics();
	}// else no-op
// don't know why this is needed to keep idle running, but it is
return;
}


LRESULT CDynControls::OnVarChnge   (WPARAM wParam, LPARAM lParam)
{ return OnUpdate(WM_SDC_VAR_CHANGED,wParam,lParam);};
LRESULT CDynControls::OnStructChnge(WPARAM wParam, LPARAM lParam)
{ return OnUpdate(WM_SDC_STRUCTURE_CHANGED,wParam,lParam);};
LRESULT CDynControls::OnValueChnge (WPARAM wParam, LPARAM lParam)
{ return OnUpdate(WM_SDC_VAR_CHANGEDVALUE,wParam,lParam);};

LRESULT CDynControls::OnUpdate(UINT message, WPARAM wHint, LPARAM pHint)
{	
	long type = 0;
	switch( message )
    {
	case WM_SDC_VAR_CHANGED:
					type = UPDATE_ADD_DEVICE;
	case WM_SDC_VAR_CHANGEDVALUE:
		if (! type)	type = UPDATE_ONE_VALUE;
	case WM_SDC_STRUCTURE_CHANGED:
    {
		if (! type)	type = UPDATE_STRUCTURE;
		ulong id = (ulong)wHint;
	
		// Check to see if the symbol exists
		if (m_isCntrlsReady)
		{
			TRACE(_T("CDynControls::OnUpdate, symbolID = %d\n"), id);
			DEBUGLOG(CLOG_LOG,"MSG:CDynControls::OnUpdate, symbolID = 0x%04x\n", id);
			if (m_pdrawDlg && ( m_pdrawDlg != (CdrawDlg *)0xfefefefe))
                {
				m_pdrawDlg->Update(type,id);
		}
    }
        }
    break;

	default:
		break;
    }// endswitch
	return 0;
}

BOOL CDynControls::Initialize() 
{
	//Reposition all the controls on the dialog...
	CDynDialogItemEx *pDynDialogItemEx = NULL;
    for (int i = 0; i < m_arrDlgItemPtr.GetSize(); i++)
    {
		pDynDialogItemEx = m_arrDlgItemPtr[i];
        if (pDynDialogItemEx != NULL)
        {
			if (!pDynDialogItemEx->IsDlgItemSubclassed() && 
				!pDynDialogItemEx->SetWindowPos(/*this*/m_pDialogWnd)) 
			{
				CString strMessage;
				strMessage.Format(_T("Failed SetWindowPos for control %ls."), 
															pDynDialogItemEx->GetClassName());
				AfxMessageBox(strMessage);// format checked 22aug08
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CDynControls::DoModal() 
{
	m_pDialogWnd = this;
//	AddFinalControls();
	SetUseSystemButtons();  // Add OK and Cancel buttons
//	SetUseModeless();       // make it modeless
	
// hold for regression....m_pMainFrameClass->RegisterDialog(this);// was in oninit
	
	return BuildDlgTemplate();  // create template and show it
}

/* workers added 15mar07 stevev */
bool CDynControls::DlgUnits2Pixels(RECT* pRect)// returns true on success
{	// return MapDialogRect(pRect);
//	long baseunitX  = GetDialogBaseUnits();// low order X, hi order Y
//	long baseunitY  = baseunitX >> 16;
//	     baseunitX &= 0xffff;
	//pixelx      = dlgunitx * baseunitx / 4

	pRect->left   = MulDiv( pRect->left,   baseunitX, 4 ); // (a*b)/c ::64 bit result of
														   //    multiplication used for divide
	pRect->right  = MulDiv( pRect->right,  baseunitX, 4 );
	pRect->top    = MulDiv( pRect->top,    baseunitY, 8 );
	pRect->bottom = MulDiv( pRect->bottom, baseunitY, 8 );
	return true;
}

bool CDynControls::Pixels2DlgUnits(RECT *pRect)
{ // returns true on success
//	long baseunitX  = GetDialogBaseUnits();// low order X, hi order Y
//	long baseunitY  = baseunitX >> 16;
//	     baseunitX &= 0xffff;
	//dlgunitx    =   pixelx  *  4  /  baseunitx
	pRect->left   = MulDiv( pRect->left,   4, baseunitX );
	pRect->right  = MulDiv( pRect->right,  4, baseunitX );
	pRect->top    = MulDiv( pRect->top,    8, baseunitY );
	pRect->bottom = MulDiv( pRect->bottom, 8, baseunitY );
	return true;
}

/* end added workers */

int CDynControls::BuildDlgTemplate() 
{
    
	//Do we need OK and Cancel buttons??  - Code moved to OnInitDialog, POB - 25 Aug 2008
/*	if (m_bAddSystemButtons) {
		AddSystemButtons();
	}*/

	SetFont();

	//
	// Get font info from mainwindow of the application
	//
	CFont* pParentFont = m_pFont;
	if (pParentFont == NULL && m_pParentWnd != NULL) 
	{
		pParentFont = m_pParentWnd->GetFont();
	}
	if (pParentFont == NULL && AfxGetApp()->m_pActiveWnd != NULL)
	{
		pParentFont = AfxGetApp()->m_pActiveWnd->GetFont();
	}
	LOGFONT LogFont;
	memset(&LogFont, 0x00, sizeof(LogFont));
	if (pParentFont != NULL) 
	{
		pParentFont->GetLogFont(&LogFont);
	}
	else 
	{
		// Can do better???
	/* stevev 29mar07- replaced with below
		strcpy(LogFont.lfFaceName, );
		LogFont.lfHeight = 8;
	*/
		_tcscpy(LogFont.lfFaceName, CONST_STRING_FONT);
		LogFont.lfHeight = CONST_STRING_HIGT;		
	}

	//Prework for setting font in dialog...
	// set unicode name  & length
	//int cWC = MultiByteToWideChar(CP_ACP, 0, LogFont.lfFaceName, -1, NULL, 0);
	//int nFontNameLen = cWC + 1;
	int nFontNameLen  = _tcslen(LogFont.lfFaceName) +1;// stevev 14jan08
	WCHAR *szFontName = new WCHAR[nFontNameLen];
	// Copy the string
	//MultiByteToWideChar(CP_ACP, 0, LogFont.lfFaceName, -1, (LPWSTR) szFontName, cWC);
#ifdef _UNICODE
	_tcsncpy(szFontName, LogFont.lfFaceName, nFontNameLen);
#else
	AsciiToUnicode(LogFont.lfFaceName, szFontName, nFontNameLen);
#endif
	szFontName[nFontNameLen-1] = 0;
	nFontNameLen = (nFontNameLen-1) * sizeof(WCHAR);// number of bytes???

    if (m_wFontSize == 0)
    {
		m_wFontSize = (unsigned short)LogFont.lfHeight;
	}

	//Prework for setting caption in dialog...
	//cWC = MultiByteToWideChar(CP_ACP, 0, m_strCaption, -1, NULL, 0);
	//int szBoxLen = cWC + 1;
	int szBoxLen  = _tcslen(m_strCaption) +1;// stevev 14jan08
	WCHAR *szBoxCaption = new WCHAR[szBoxLen];
	// Copy the string
	//MultiByteToWideChar(CP_ACP, 0, m_strCaption, -1, (LPWSTR) szBoxCaption, cWC);
#ifdef _UNICODE
	_tcsncpy(szBoxCaption, m_strCaption, szBoxLen);
#else
	AsciiToUnicode(m_strCaption, szBoxCaption, szBoxLen);
#endif
	szBoxCaption[szBoxLen-1] = 0;
	szBoxLen = (szBoxLen-1) * sizeof(WCHAR);// number of bytes???

	int iRet = -1;
	//Here 's the stuff to build the dialog template in memory
	//without the controls being in the template
	//(Our first try, was this same template with some additional code
	//for each control placed on it, that's why this class is cold Ex :)
	//This gave some problems on WIN9x systems, where EDIT boxes
	//were not shown with 3D-look, but as flat controls)
	int nBufferSize =  sizeof(DLGTEMPLATE)       + (2 * sizeof(WORD)) /*menu and class*/ 
					 + szBoxLen /*size of caption*/
					 + sizeof(WORD) /*fontsize*/ + nFontNameLen       /*size of fontname*/;
	//Are there any subclassed controls...
    if (m_DialogTemplate.cdit > 0)
    {
		nBufferSize = (nBufferSize + 3) & ~3;  // adjust size to make 1st control DWORD aligned

		CDynDialogItemEx *pDynDialogItemEx = NULL;
        for (int i = 0; i < m_arrDlgItemPtr.GetSize(); i++)
        {
			pDynDialogItemEx = m_arrDlgItemPtr[i];
            if (pDynDialogItemEx != NULL)
            {
                if (pDynDialogItemEx->IsDlgItemSubclassed())
                {
					int nItemLength = sizeof(DLGITEMTEMPLATE) + 3 * sizeof(WORD);
					nItemLength += (pDynDialogItemEx->GetCaptionLength() + 1) * sizeof(WCHAR);

                    if (i != m_DialogTemplate.cdit - 1)   // the last control does not need extra bytes
                    {
						nItemLength = (nItemLength + 3) & ~3;  
						// take into account gap so next control is DWORD aligned
					}
					nBufferSize += nItemLength;
				}
			}
		}
	}

	HLOCAL hLocal = LocalAlloc(LHND, nBufferSize);
    if (hLocal != NULL)
    {
		BYTE*	pBuffer = (BYTE*)LocalLock(hLocal);
        if (pBuffer == NULL)
        {
			LocalFree(hLocal);
			AfxMessageBox(_T("CDynControls::DoModal() : LocalLock Failed"));
		}

		BYTE *pdest = pBuffer;
		// transfer DLGTEMPLATE structure to the buffer
		memcpy(pdest, &m_DialogTemplate, sizeof(DLGTEMPLATE));	// DLGTemplate
		pdest += sizeof(DLGTEMPLATE);
		*(WORD*)pdest = 0;						// no menu		   -- WORD to say it is 0 bytes
		pdest += sizeof(WORD);					// Increment
		*(WORD*)(pdest + 1) = 0;				// use default window class -- WORD ...
		pdest += sizeof(WORD);					// Increment
		memcpy(pdest, szBoxCaption, szBoxLen);	// Caption
		pdest += szBoxLen;

		*(WORD*)pdest = m_wFontSize;			// font size
		pdest += sizeof(WORD);
		memcpy(pdest, szFontName, nFontNameLen);// font name
		pdest += nFontNameLen;

		// will now transfer the information for each one of subclassed controls...
        if (m_DialogTemplate.cdit > 0)
        {
			CDynDialogItemEx *pDynDialogItemEx = NULL;
			for (int i = 0; i < m_arrDlgItemPtr.GetSize(); i++) 
			{
				pDynDialogItemEx = m_arrDlgItemPtr[i];
				if (pDynDialogItemEx != NULL) 
				{
					if (pDynDialogItemEx->IsDlgItemSubclassed()) 
					{
						pdest = pDynDialogItemEx->FillBufferWithItemTemplate(pdest);
					}
				}
			}
		}
		ASSERT(pdest - pBuffer == nBufferSize); // just make sure we did not overrun the heap

		//Next lines to make sure that MFC makes no ASSERTION when PREVIOUS/NEXT is pressed:)
        if (m_lpDialogTemplate != NULL)
        {
			m_lpDialogTemplate = NULL;
		}

		if ( m_pdrawDlg )
        {
			m_pdrawDlg->m_isReady = TRUE;
        }

		//These are the MFC functions, which do the job...
		if (m_bModelessDlg) 
		{
			iRet = CreateIndirect((LPDLGTEMPLATE)pBuffer, m_pParentWnd);		
		}
		else 
		{
			if ( ! InitModalIndirect((LPDLGTEMPLATE)pBuffer, m_pParentWnd) )
			// worked ok:: 26oct if ( ! InitModalIndirect((LPDLGTEMPLATE)pBuffer) )
			{
TRACE(_T("** DynControl Dialog Failed to create.\n"));
			}
			else
			{
TRACE(_T("** DynControl Dialog starting Modal. (dialog Wnd = %x) [parent Wnd = %x]\n"),m_hWnd,
	  (m_pParentWnd)?m_pParentWnd->m_hWnd:0);
// original:: was 
// workedOK::		26oct		iRet = CDialog::DoModal();
				iRet = sdcModl();
TRACE(_T("** DynControl Dialog ending Modal. (dialog Wnd = %x)\n"),m_hWnd);
			}
		}

		LocalUnlock(hLocal);
		LocalFree(hLocal);

		delete [] szBoxCaption;
		delete [] szFontName;
		return iRet;
	}
    else
    {
		AfxMessageBox(_T("CDynControls::DoModal() : LocalAllock Failed"));
		return -1;
	}
}

void CDynControls::SetFont(CFont *pFont)
{
	m_pFont = pFont;
}

void CDynControls::SetFont()
{
	// Get the font from the window that we are
	// drawing our controls on.
	if (::IsWindow(m_pDialogWnd->m_hWnd))
	{
		SetFont(m_pDialogWnd->GetFont());
		m_bIsFontCreated = TRUE;
	}
}

CFont *CDynControls::GetFont()
{
	return m_pFont;
}

void CDynControls::SetFontSize(WORD wSize)
{
	m_wFontSize = wSize;
}

WORD CDynControls::GetFontSize()
{
	return m_wFontSize;
}

void CDynControls::SetUseSystemButtons(BOOL bUse /*= TRUE*/)
{
	m_bAddSystemButtons = bUse;
}

void CDynControls::GetDlgRect(LPRECT lpRect)
{
	ASSERT(lpRect);
	lpRect->left   = m_DialogTemplate.x;
	lpRect->top    = m_DialogTemplate.y;
	lpRect->right  = lpRect->left + m_DialogTemplate.cx;
	lpRect->bottom = lpRect->top  + m_DialogTemplate.cy;
}

void CDynControls::SetDlgRect(LPRECT lpRect)
{
	ASSERT(lpRect);
//#pragma warning(disable : 4244)
	m_DialogTemplate.cx = (short)(lpRect->right  - lpRect->left);
	m_DialogTemplate.cy = (short)(lpRect->bottom - lpRect->top);
	m_DialogTemplate.x  = (short)(lpRect->left);
	m_DialogTemplate.y  = (short)(lpRect->top);
//#pragma warning(default : 4244)
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Set Template   x: %3d  y: %3d   w: %d   h: %d \n",
									(int)(m_DialogTemplate.x), (int)(m_DialogTemplate.y),
									(int)(m_DialogTemplate.cx),(int)(m_DialogTemplate.cy)  );
}

void CDynControls::SetDlgRectangle(LPRECT pRect)
{
	RECT rect;

	GetDlgRect(&rect);
    if (rect.left > pRect->left)
    {
		rect.left = pRect->left;
	}
    if (rect.right < pRect->right)
    {
		rect.right = pRect->right; //we don't want to change this after all the calcs done:+ 5;
	}
    if (rect.top > pRect->top)
    {
		rect.top = pRect->top;
	}
    if (rect.bottom < pRect->bottom)
    {
		rect.bottom = pRect->bottom;//we don't want to change this after all calcs done:+ 5;
	}
	SetDlgRect(&rect);
}

UINT CDynControls::AddDlgControl(DLGITEMTEMPLATECONTROLS TypeControl,
											LPCTSTR lpszCaption,
											DWORD dwStyle,
											DWORD dwExtendedStyle,
											LPRECT pRect /*= NULL*/,
											void *pData /*= NULL*/,
											UINT nID /*= 0*/)
{
	UINT nRet = 0;
	//In case no rectangle given create our own...
	CRect Rect(FIXEDCOL1, m_nCurRow, FIXEDCOL2, m_nCurRow + ROWSTEPSIZE);

	//if no rectangle given use our own...
    if (pRect == NULL)
    {
		pRect = &Rect;
	}
//	else {
//		m_nCurRow = max(m_nCurRow, pRect->bottom) - m_nCurRow;
//	}

	m_nCurRow += (ROWSTEPSIZE);

	//Update dialogtemplate boundaries
	SetDlgRectangle(pRect);

	//Create control and add to array of controls
	CDynDialogItemEx *pDynDialogItemEx = new CDynDialogItemEx;
    if (pDynDialogItemEx != NULL)
    {
		nRet = pDynDialogItemEx->InitDialogItem(TypeControl, dwStyle, dwExtendedStyle, 
												pRect, lpszCaption, nID, FALSE, pData);
		m_arrDlgItemPtr.Add(pDynDialogItemEx);
	}

	//Return ID of Control we created.
	return nRet;
}

UINT CDynControls::AddDlgControl(LPCTSTR lpszClassName,
											LPCTSTR lpszCaption,
											DWORD dwStyle,
											DWORD dwExtendedStyle,
											LPRECT pRect /*= NULL*/,
											void *pData /*= NULL*/,
											UINT nID /*= 0*/)
{
	UINT nRet = 0;
	
//	m_nCurRow += 6;			// Leave some room!
	
	//In case no rectangle given create our own...
	//           left         top              right                    bottom
	CRect Rect(m_nCurColumn, m_nCurRow, FIXEDCOL2 + m_nCurColumn, m_nCurRow + ROWSTEPSIZE);
	LOGIF(LOGP_LAYOUT)
		(CLOG_LOG,"default Rect:: L: %3d  R: %3d   T: %3d  B: %3d\n",
												Rect.left,Rect.right, Rect.top, Rect.bottom );
	//if no rectangle given use our own...
	if (pRect == NULL) 
	{
		pRect = &Rect;
		Rect.right += INPUTCOL;
	}
	else// 30jul12 - in attempt to fix the lack of dropdowns
	{
		Rect = *pRect;
		pRect = &Rect;
	}
/*	else {
		m_nCurRow = max(m_nCurRow, pRect->bottom) - m_nCurRow;
	}*/

/*	if (lpszClassName == "BUTTON")
	{
		Rect.left = FIXEDCOL1;
		Rect.top = m_nCurRow;
		Rect.right = FIXEDCOL2 //*- 30; // The -30 produces the button in the same size as IDOK
		Rect.bottom = m_nCurRow + ROWSTEPSIZE;
	}*/

	// The rules that govern the static strings are currently
	// identical to that of the constant string, ie 2 text lines
	// per row, 8 pixels in height.  POB - 6 Jan 2005
// static is also used for scopes--- let somebody else calculate the size...stop this override
//	if (lpszClassName == CString("STATIC"))
//	{
//		Rect.bottom = m_nCurRow + (8 * 2);  // 2 text lines per row, 8 pixels in height 
//	}

	LOGIF(LOGP_LAYOUT)
		(CLOG_LOG,"   used Rect:: L: %3d  R: %3d   T: %3d  B: %3d\n",
												Rect.left,Rect.right, Rect.top, Rect.bottom );
	//Update dialogtemplate boundaries
	SetDlgRectangle(pRect);// sets the template values

	if (lpszClassName == CString("COMBOBOX"))/* stevev 09dec05 - do not compare pointers */
	{
		// This code must come after the
		// SetDlgRectangle call
		Rect.bottom = Rect.bottom + 40  ;  
		// This determines how far the drop down portion will drop the dialog (look and feel)
	}

	//Create control and add to array of controls
	CDynDialogItemEx *pDynDialogItemEx = new CDynDialogItemEx;
    if (pDynDialogItemEx != NULL)
    {
		nRet = pDynDialogItemEx->InitDialogItem(lpszClassName, dwStyle, dwExtendedStyle, 
												pRect, lpszCaption, nID, FALSE, pData);
		m_arrDlgItemPtr.Add(pDynDialogItemEx);		
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"               L: %3d  T: %3d   B: %3d  R: %3d\n",
									pRect->left, pRect->top, pRect->bottom, pRect->right);
	}

	//Return ID of Control we created.
	return nRet;
}

UINT CDynControls::AddSubclassedDlgControl(LPCTSTR lpszClassName,
														LPCTSTR lpszCaption,
														DWORD dwStyle,
														DWORD dwExtendedStyle,
														LPRECT pRect /*= NULL*/,
														UINT nID /*= 0*/)
{
	UINT nRet = 0;
	//In case no rectangle given create our own...
	CRect Rect(FIXEDCOL1, m_nCurRow, FIXEDCOL2, m_nCurRow + ROWSTEPSIZE);

	//if no rectangle given use our own...
    if (pRect == NULL)
    {
		pRect = &Rect;
		Rect.right += INPUTCOL;
	}
//	else {
//		m_nCurRow = max(m_nCurRow, pRect->bottom) - m_nCurRow;
//	}

	m_nCurRow += (ROWSTEPSIZE);

	//Update dialogtemplate boundaries
	SetDlgRectangle(pRect);

	//Create control and add to array of controls
	CDynDialogItemEx *pDynDialogItemEx = new CDynDialogItemEx;
    if (pDynDialogItemEx != NULL)
    {
		nRet = pDynDialogItemEx->InitDialogItem(lpszClassName, dwStyle, dwExtendedStyle, 
												pRect, lpszCaption, nID, TRUE);
		m_arrDlgItemPtr.Add(pDynDialogItemEx);
		m_DialogTemplate.cdit++;
	}

	//Return ID of Control we created.
	return nRet;
}

void CDynControls::AddSystemButtons()
{
    // remove dead code, POB - 3/25/2014
	RECT r, rct;
	GetDlgRect(&rct);
	r = rct;				// keep original setting in rct
	TCHAR* butNames = NULL, *pT;
	TCHAR* strPtr[5];
	int c = 0;

	m_ButtonCount  = m_pdrawDlg->m_exButtonCnt;
	//if (m_ButtonCount > 5 ) m_ButtonCount = 5;// 3 plus help and abort
	if (m_ButtonCount > 5 ) m_ButtonCount = 4;// 3 plus abort, we don't do help
	if (m_ButtonCount > 0 && m_pdrawDlg->m_pButtonTxt != NULL && 
		_tcsclen(m_pdrawDlg->m_pButtonTxt))
	{
		butNames = new TCHAR[ _tcsclen(m_pdrawDlg->m_pButtonTxt) + 1 ] ;
		_tcscpy(butNames,m_pdrawDlg->m_pButtonTxt);

		strPtr[0] = butNames;
		c = 1;
		for ( pT = butNames; *pT != _T('\0') && c < m_ButtonCount; pT++)
		{
			if ( *pT == (';') )
			{
				*pT = _T('\0');
				if ( *(pT+1) != _T('\0') )
				{
					strPtr[c++] = pT+1;
				}
			}
		}
#ifdef _DEBUG
		if ( c != m_ButtonCount)
		{
			LOGIT(CERR_LOG,"DisplayMenu call with a mismatch of button counts.\n");
		}
#endif
	}
	
	r.right -= GetSystemMetrics(SM_CXVSCROLL);// offset all left the width of a vert scroll bar

    CRect rect;
    rect.left  = r.right - FIXEDCOL2 - int(FIXEDCOL2/3);  // Default - last button on the right, POB - 4/4/2014
    rect.right = rect.left + FIXEDCOL2; // FIXEDCOL2 = size of the buttons
    rect.top = r.bottom + ROWSTEPSIZE;
    rect.bottom = rect.top + ROWSTEPSIZE;

	if (m_ButtonCount == 0)// straight dialog
	{
		AddDlgControl(BUTTON, _T("Cancel"),STYLE_BUTTON,EXSTYLE_BUTTON, &rect, NULL, IDCANCEL);
			
        rect.left  -= FIXEDCOL2 + BUTTONGAP;  // modified by POB - 4/4/2014
        rect.right = rect.left + FIXEDCOL2;

		AddDlgControl(BUTTON, _T("OK"), STYLE_BUTTON, EXSTYLE_BUTTON, &rect, NULL, IDOK);

		rct.bottom = rect.bottom;
	}
    else if (m_ButtonCount == 1) //abort only
	{
        // Not sure when this would ever happen?   POB - 4/4/2014
		AddDlgControl(BUTTON, strPtr[0],STYLE_BUTTON,EXSTYLE_BUTTON, &rect,NULL,BUTTONID[0]);
		rct.bottom = rect.bottom;
	}
    else // has a button so we are in display menu
    {
		/* - Abort [0] on the right - First NON-abort button is default button
		*/
		int totW = ( m_ButtonCount * (BUTTONGAP+FIXEDCOL2)) + BUTTONGAP;
        if (totW > r.right) // won't fit - start at button 1
        {
			rct.right = totW ; 
			// actually set later...SetDlgRect(&rct); 
			TRACE(_T("%d Buttons* R= %d & L = %d \n"),m_ButtonCount,r.right,r.left);	
		}
		//fits (now)
        rect.left  -= FIXEDCOL2 + BUTTONGAP;   // modified by POB - 4/4/2014
		rect.right = rect.left + FIXEDCOL2;
		
        for (c = 1; c < m_ButtonCount; c++)       //strPtr[c]
        {
LOGIT(CLOG_LOG,"Button %d @ R= %d & L = %d  ",c,rect.right,rect.left);				
		 AddDlgControl(BUTTON, strPtr[c],STYLE_BUTTON,EXSTYLE_BUTTON, &rect,NULL,BUTTONID[c]);

LOGIT(CLOG_LOG," returns   R= %d & L = %d\n",rect.right,rect.left);		
			rect.left  -= (FIXEDCOL2 + BUTTONGAP);
			rect.right -= (FIXEDCOL2 + BUTTONGAP);
		}
		// add the abort button last
        rect.left  = r.right - FIXEDCOL2 - int(FIXEDCOL2/3);    // Last button on the right, POB - 4/4/2014
		rect.right = rect.left + FIXEDCOL2;
		AddDlgControl(BUTTON, strPtr[0],STYLE_BUTTON,EXSTYLE_BUTTON, &rect,NULL,BUTTONID[0]);
LOGIT(CLOG_LOG,"ButtonAbort @ R= %d & L = %d\n",rect.right,rect.left);	
		rct.bottom = rect.bottom;
	}

    rct.bottom += ROWSTEPSIZE; // add some room between the buttons and the bottom of the dialog (critical for vertical scrollbars) 

	SetDlgRect(&rct); // change the requested size. sizeDialog will add scroll bars as required
	delete [] butNames;
}

void CDynControls::SetWindowTitle(LPCSTR lpszCaption)
{
	m_strCaption = lpszCaption;
}

void CDynControls::SetUseModeless(BOOL bModelessDlg /*= TRUE*/)
{
	m_bModelessDlg = bModelessDlg;
}

void CDynControls::OnCancel()
{	
	if (m_ButtonCount == 0) // isa Menu of style Dialog
	{
		if (m_pdrawDlg)
		{
			m_pdrawDlg->Cancel(0); // Cancel All
			m_pdrawDlg->m_isReady = FALSE;
		}

		if (m_bModelessDlg) 
		{
			DestroyWindow();
		}
		else 
		{
//temp out			CDialog::OnCancel();
// temp in	
	//ASSERT(::IsWindow(m_hWnd));
	//
	//if (m_nFlags & (WF_MODALLOOP|WF_CONTINUEMODAL))
	//	EndModalLoop(IDCANCEL);		
 	sdCDialog::OnCancel();
//workedOK:: 26oct				CDialog::OnCancel();
// end temp
		}
	}
    else // MUST be modal    //it's a method's displayMenu
    {
		if (m_pdrawDlg)
		{
			m_pdrawDlg->m_isReady = FALSE;
		}
//temp out			CDialog::OnCancel();
// temp in	
	//ASSERT(::IsWindow(m_hWnd));
	//
	//if (m_nFlags & (WF_MODALLOOP|WF_CONTINUEMODAL))
	//	EndModalLoop(IDCANCEL);
		sdCDialog::OnCancel();
// workedOK:: 26oct				CDialog::OnCancel();
// end temp
	}

	// if no dialog is created (need to create conditional
//	m_pParentWnd->PostMessage(IDCANCEL);
}

void CDynControls::OnOK()
{
	if (m_bModelessDlg) 
	{
		DestroyWindow();
	}
	else 
	{
		sdCDialog::OnOK();
	}
}

BOOL CDynControls::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	//wParam
	//The low-order word of wParam identifies the command ID of the menu item, control, or 
	// accelerator.
	//The high-order word of wParam specifies the notification message if the message is from
	// a control.
	//If the message is from an accelerator, the high-order word is 1.
	//If the message is from a menu, the high-order word is 0.

	//lParam
	//Identifies the control that sends the message if the message is from a control. 
	// Otherwise, lParam is 0.

	// TODO: Add your specialized code here and/or call the base class

	WORD wControlID = LOWORD(wParam);
	WORD wMessageID = HIWORD(wParam);

	BOOL result;
#ifdef _DEBUG
	int exeResult;
#define RESULTIS exeResult =
#else
#define RESULTIS
#endif
	if (wControlID != 0 && m_isCntrlsReady) 
	{
		if (m_ButtonCount != 0 && wControlID >= BUTTONID[0] && 
            wControlID <= BUTTONID[m_ButtonCount - 1]) // handle button for displaymenu
        {
			//
			// trying to handle these with ON_COMMAND(WM_DLG_ABORT, OnDlgAbort) etc
			// crashes mfc when it returns to the control ID as an address (screws the stack)
			switch(wControlID)
			{
			case WM_DLG_ABORT:
				{	RESULTIS
					OnDlgAbort(wParam, lParam);
				}
				break;
			case WM_DLG_OK:
				{	RESULTIS
					OnDlgOK   (wParam, lParam);
				}
				break;
			case WM_DLG_BUT01:
				{	RESULTIS
					OnDlgBut01(wParam, lParam);
				}
				break;
			case WM_DLG_BUT02:
				{	RESULTIS
					OnDlgBut02(wParam, lParam);
				}
				break;
			case WM_DLG_BUT03:
				{	RESULTIS
					OnDlgBut03(wParam, lParam);
				}
				break;
			default:
				LOGIT(CERR_LOG,"ERROR:MenuDisplay() with button mismatch.\n");
			}// endswitch
			return TRUE;// we handled it - return non-zero
		}
        else // handle normally - true if handled; false if more is needed
        {
			result = m_pdrawDlg->ExecuteCID(wControlID, wMessageID);
		}
	}

	return sdCDialog::OnCommand(wParam, lParam);
}

// Hellup!
afx_msg LRESULT CDynControls::OnHelpMsg(WPARAM wParam, LPARAM lParam)
{
	//lParam		<<-- Contains: (LPHELPINFO)lParam
	// >> typedef  struct  tagHELPINFO { 
	//     UINT     cbSize; 
	//     int      iContextType 
	//     int      iCtrlId; 
	//     HANDLE   hItemHandle; 
	//     DWORD    dwContextId; 
	//     POINT    MousePos; 
	// } HELPINFO, FAR *LPHELPINFO;

	//
	// User pressed F1 in dialog, call the OnHelp() function, this can be overridden...
	//
#ifndef _WIN32_WCE	// PAW function missing 29/04/09
	OnHelp();
#endif
	return TRUE;
}

// Default implementation of the OnHelp()
// This one is to be overridden by a caller ;)
#ifndef _WIN32_WCE	// PAW No F1 in Win CE 29/04/09
void CDynControls::OnHelp()
{
	sdCDialog::OnHelp();
	m_pParentWnd->PostMessage(WM_HELP);
}
#endif

CWnd * CDynControls::GetDlgCtrl(UINT nID)
{
	CWnd * pControl = NULL;

	if (m_pDialogWnd)
	{
		if (::IsWindow(m_pDialogWnd->m_hWnd))
		{
		//	return m_pDialogWnd->GetDlgItem( nID );
			pControl = m_pDialogWnd->GetDlgItem( nID );
		}
	}

	return pControl;
}

void CDynControls::AddDlgControl(CdrawBase * pBase, int offset)
{
	UINT nID = 0;

	CRect Rect;

	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"    Adding At  L:%d  T:%d   B:%d  R:%d  \n",
		pBase->m_MyLoc.topleft.xval,pBase->m_MyLoc.topleft.yval, 
		pBase->m_MyLoc.topleft.yval + pBase->m_MyLoc.da_size.yval, 
		pBase->m_MyLoc.topleft.xval + pBase->m_MyLoc.da_size.xval     );

	switch (pBase->mdsType)
	{
		case mds_var:
		{
			CColorEdit * pEditBox = new CColorEdit;
			pBase->Set_Focus_Msg  = EN_SETFOCUS;
			pBase->Kil_Focus_Msg  = EN_KILLFOCUS;

			CdrawVar * pVar = (CdrawVar *) pBase;

			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Add Var Control 0x%04x \n",
														((hCVar*) pVar->GetItem())->getID());

	//CRect Rect(m_nCurColumn, m_nCurRow, FIXEDCOL2 + m_nCurColumn, m_nCurRow + ROWSTEPSIZE);
			Rect.bottom = m_nCurRow + LABELADDSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			if (!pBase->m_bIsNoLabel)
			{
				/* NEED TO USE control id method that pBase uses 
				   instead of the one in the AddDlgControl*/
				pVar->m_nLabelID = AddDlgControl(_T("STATIC"), _T("Label"), STYLE_STATIC, 
																		EXSTYLE_STATIC,&Rect);
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"          Label %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			}

			StepColumn(); // Move right to the next window column
			
			Rect.left  = m_nCurColumn + offset;
			Rect.right = FIXEDCOL2 + m_nCurColumn + offset;
            Rect.bottom = m_nCurRow + ROWSTEPSIZE;

		  			// stevev 04sept08 -added ES_AUTOHSCROLL to handle long long -see bug 2184
			nID =   AddDlgControl(_T("EDIT"), _T("VARIABLE"), STYLE_EDIT | ES_AUTOHSCROLL , 
								EXSTYLE_EDIT, &Rect, pEditBox, pBase->m_cntrlID);
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"           Edit %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
		
			StepColumn(); // Move right again
			
			Rect.left  = m_nCurColumn + offset;
			Rect.right = FIXEDCOL2 + m_nCurColumn + offset;

			
			if (!pBase->m_bIsNoUnit)
			{
				// We need to shorten any engineering field - 
				//    so don't use the standard size, POB - 6 Jan 2005, no longer shortening.
				pVar->m_nUnitID =  AddDlgControl(_T("STATIC"), _T("Units"), 
									STYLE_STATIC, EXSTYLE_STATIC, &Rect
	/*CRect(m_nCurColumn, m_nCurRow, FIXEDCOL2 + m_nCurColumn-15, m_nCurRow + ROWSTEPSIZE)*/);
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"           Unit %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			}

			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepColumn(LEFT);
			StepRow();
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Finished Var Control 0x%04x - - - - - - - - - - - \n",
														((hCVar*) pVar->GetItem())->getID());
		}
		break;

		case mds_index:
		{
			CClrComboBox * pComboBox = new CClrComboBox;
			pBase->Set_Focus_Msg  = CBN_SETFOCUS;
			pBase->Kil_Focus_Msg  = CBN_KILLFOCUS;

			CdrawIndex * pIdx = (CdrawIndex *) pBase;
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Add Idx Control 0x%04x \n",
														((hCVar*) pIdx->GetItem())->getID());
			Rect.bottom = m_nCurRow + LABELADDSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			if (!pBase->m_bIsNoLabel)
			{
				pIdx->m_nLabelID = AddDlgControl(_T("STATIC"), _T("Label"), STYLE_STATIC, 
																		EXSTYLE_STATIC,&Rect);
			}
			
			StepColumn();
			
			Rect.left  = m_nCurColumn + offset;
			Rect.right = FIXEDCOL2 + m_nCurColumn + offset;
            Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			
			nID = AddDlgControl(_T("COMBOBOX"), _T("DropDownComboText"), 
				STYLE_COMBOBOX_DROPDOWNLIST_NOSORT, EXSTYLE_COMBOBOX , &Rect, 
				pComboBox /*NULL*/, pBase->m_cntrlID);
			
			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Finished Idx Control 0x%04x - - - - - - - - - - - \n",
														((hCVar*) pIdx->GetItem())->getID());
		}
		break;

		case mds_enum:
		{
			/* NOTE: good article about resetting combobox settings dynamically
			   http://www.codeproject.com/KB/combobox/recreatecombobox.aspx
		   */
			CClrComboBox * pComboBox = new CClrComboBox;
			pBase->Set_Focus_Msg  = CBN_SETFOCUS;
			pBase->Kil_Focus_Msg  = CBN_KILLFOCUS;

			CdrawEnum * pEnum = (CdrawEnum *) pBase;
			
            Rect.bottom = m_nCurRow + LABELADDSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			if (!pBase->m_bIsNoLabel)
			{
				pEnum->m_nLabelID = AddDlgControl(_T("STATIC"), _T("Label"), STYLE_STATIC, 
																		EXSTYLE_STATIC, &Rect);
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"          Label %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			}
			
			StepColumn();
			
			Rect.left  = m_nCurColumn + offset;
			Rect.right = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			
			nID = AddDlgControl(_T("COMBOBOX"), _T("DropDownComboText"), 
				STYLE_COMBOBOX_DROPDOWNLIST_NOSORT, EXSTYLE_COMBOBOX , &Rect, 
				pComboBox /*NULL*/, pBase->m_cntrlID);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"           Edit %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			
			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
		}
		break;

		case mds_bitenum:
		{
		    // The Bit Enum control is composed of a Group Box with mds_bit controls inside
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("BUTTON"), _T("Bit Enum"), STYLE_GROUPBOX, 
									EXSTYLE_GROUPBOX, &Rect, NULL, pBase->m_cntrlID);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"       Bit Enum %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			/*nID = AddDlgControl(_T("STATIC"), _T("Bit Enum"), STYLE_STATIC, 
									EXSTYLE_STATIC, &Rect, NULL, pBase->m_cntrlID);*/
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"");
		}
		break;

		case mds_bit:
		{
			CBit * pBitButton = new CBit;
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;

			CdrawBit * pBitEnum = (CdrawBit *) pBase;
			
			// For "Bit" we are going to have the label show up on the right side 
			//		of the bit control so add the bit control first. POB - 31 March 2005
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("BUTTON"), _T("Bit"),  WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON
				/*| BS_RADIOBUTTON*/ /*| BS_LEFTTEXT*/,NULL	/*WS_EX_RIGHT*/, &Rect, 
				pBitButton /*&m_BitEnum*/, pBase->m_cntrlID);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"            Bit %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			
			// Do not jump to the next MFC column (as we have defined it) - POB, 31 March 2005
			// This is not to be confused with DDL columns (which always has a max of 3) 
			// We modified the control to take up two MFC columns (as we have defined it)
			// plus a portion of the button control's region
			Rect.left   = m_nCurColumn + BIT_SIZE + COLUMNGAP+offset;//curcol should have offset in it
			Rect.top    = m_nCurRow;
			Rect.right  = m_nCurColumn + (FIXEDCOL2 * 3) + 8 +offset ;
			Rect.bottom = m_nCurRow + (8 * 2);  // 2 text lines per row, 8 pixels in height 

			if (!pBase->m_bIsNoLabel)
			{
				pBitEnum->m_nLabelID = AddDlgControl(_T("STATIC"), _T("Label"), 
														  STYLE_STATIC, EXSTYLE_STATIC, &Rect);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"      Bit Label %d,%d to %d,%d \n",
					                             Rect.left,Rect.top,  Rect.right,Rect.bottom);
			}

			// Get us back to the column position where we started from
			// we never stepped, no need to unstep...StepColumn(LEFT);
			StepRow();
		}
		break;

		case mds_edDisp:
		{
			CSButton * pButton = new CSButton;
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;
			
			StepColumn();

			Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			
			nID = AddDlgControl(_T("BUTTON"), _T("Edit Display"), 
							   STYLE_BUTTON, EXSTYLE_BUTTON, &Rect, pButton, pBase->m_cntrlID);
									// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
		}
		break;

		case mds_menu:
		{

			if(m_pMenuButton)
			{
				// A menubutton should not exist, if it does, don't allow another one
				break;
			}

			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Add Menu Control 0x%04x \n",
													((hCVar*) (pBase->GetItem()))->getID());

			m_pMenuButton =  new CURLLinkButton(m_pDialogWnd);
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;

			m_pMenuButton->m_Menu.CreatePopupMenu();

            // Fixed defect - If a menu lives in a pop-up menu, then we do not need to 
            // draw a button for it.  A button is only needed for a menu of style menu on a
            // WINDOW, DIALOG, PAGE, GROUP, POB - 15 Aug 2008
            if (!pBase->class_history.inPopMenu)
            {

			StepColumn();
			Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			nID = AddDlgControl(_T("BUTTON"), _T("Menu"), 
								STYLE_BUTTON | BS_OWNERDRAW/*| WS_BORDER*/, EXSTYLE_BUTTON,
						        &Rect, m_pMenuButton, pBase->m_cntrlID );
			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Finished Menu Control 0x%04x - - - - - - - - - - -\n",
													((hCVar*) (pBase->GetItem()))->getID());
			}
		}
		break;
				
		case mds_method:
		{
			CSButton * pButton = new CSButton;
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;
			
			StepColumn();
			Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			
			nID = AddDlgControl(_T("BUTTON"), _T("Method"), // ID of the contol, default = zero
							   STYLE_BUTTON, EXSTYLE_BUTTON, &Rect, pButton, pBase->m_cntrlID);
			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
		}
		break;

		case mds_group:
		{
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Add Grp Control 0x%04x \n",
													((hCVar*) (pBase->GetItem()))->getID());
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("BUTTON"), _T("Group"), 
								STYLE_GROUPBOX, EXSTYLE_GROUPBOX,&Rect,NULL, pBase->m_cntrlID);
			StepRow();			
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Finished Grp Control 0x%04x - - - - - - - - - - -\n",
													((hCVar*) (pBase->GetItem()))->getID());
		}
		break;

		case mds_page:
			// NOT APPLICABLE
			break;

		case mds_tab:
			SetCtrlRect(Rect, pBase,0);// can't have an offset

			nID = AddDlgControl(_T("SysTabControl32"), _T("Tab 1 Text"), 
						WS_VISIBLE | WS_CHILD | WS_TABSTOP, 0, &Rect,&m_Tab, pBase->m_cntrlID);
			StepRow();
			break;

		case mds_constString:
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("STATIC"), _T("StaticText"), 
					STYLE_STATIC/*|WS_BORDER*/, EXSTYLE_STATIC, &Rect, NULL, pBase->m_cntrlID);
			StepRow();
			break;

		case mds_blank:
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("STATIC"), _T("StaticBlank"), 
					STYLE_STATIC/*|WS_BORDER*/, EXSTYLE_STATIC, &Rect, NULL, pBase->m_cntrlID);
			StepRow();
			break;

		case mds_image:
		{
			SetCtrlRect(Rect, pBase, offset);

			m_pImageButton =  new CButtonST;//CURLLinkButton(m_pDialogWnd);
			m_pImageButton->SetImage(((CdrawImage*)pBase)->m_pImage); 
			m_pImageButton->DrawBorder(FALSE, FALSE);
			m_pImageButton->SetPressedStyle(CButtonST::BTNST_PRESSED_NONEATALL, FALSE);
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;

#ifdef _DEBUG
	m_pImageButton->itemID = pBase->m_symblID;
#endif			
			hCimageItem*  pIi = (hCimageItem*)((CdrawImage*)pBase)->GetItem();
			string linkStr;
			if ( pIi->isLinkString() )
			{
				if ( pIi->getLink(linkStr) == SUCCESS )
				{
					CString locStr;
					AsciiToUnicode(linkStr.c_str(), locStr);
					//m_pImageButton->SetURL(linkStr.c_str());
					m_pImageButton->SetURL(locStr);

				}
				else
				{
					LOGIT(CERR_LOG,"Could not get the link string.\n");
					// leave it empty
				}
			}
			else
			{
				if ( pIi->getLink(m_pIbLink) == SUCCESS )
				{
					m_pImageButton->SetURL(m_pIbLink);
				}
                else // stevev - 5jul06 - normal condition, removed
                {
				 //	LOGIT(CLOG_LOG,"Did not get the link item.\n");
					// leave it empty
				}
			}

			DEBUGLOG(CLOG_LOG,L"Image adding DlgControl w/Top: %d Left: %d Rght: %d Bott: %d\n" 
												   ,Rect.top,Rect.left,Rect.right,Rect.bottom);
			nID = AddDlgControl(_T("BUTTON"), _T(""), 
					STYLE_BUTTON/*|WS_CAPTION*/ /*| BS_OWNERDRAW| WS_BORDER*/, EXSTYLE_BUTTON,
						        &Rect, m_pImageButton, pBase->m_cntrlID );
			StepRow();
		}
		break;

		
		case mds_scope: // Unless qualified using "INLINE",the image is centered alone in a row
		{				// spanning the entire width of the window and not scaled in any way.
						// In other words, the images are displayed in their native size.
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(_T("STATIC"), _T("StaticFrame"), 
						STYLE_STATIC|WS_DLGFRAME/*|SS_BITMAP|SS_CENTERIMAGE*/, EXSTYLE_STATIC, 
					            &Rect, &m_Scope, pBase->m_cntrlID);
			StepRow();
		}
		break;

		case mds_grid:
		{		
			CGridCtrl * pGridCtrl = new CGridCtrl;
			CdrawGrid * pGrid = (CdrawGrid *) pBase;
			
			SetCtrlRect(Rect, pBase, offset);

			nID = AddDlgControl(GRIDCTRL_CLASSNAME, _T("Grid"), 
								WS_VISIBLE | WS_CHILD | WS_TABSTOP/* | WS_CAPTION*/, 0, 
					            &Rect, pGridCtrl, pBase->m_cntrlID);
			StepRow();
		}
		break;
		
		case mds_parent:
		{
			CSButton * pButton = new CSButton;
			pBase->Set_Focus_Msg  = BN_SETFOCUS;
			pBase->Kil_Focus_Msg  = BN_KILLFOCUS;
			StepColumn();
			Rect.bottom = m_nCurRow + ROWSTEPSIZE;
			Rect.left   = m_nCurColumn + offset;
			Rect.right  = FIXEDCOL2 + m_nCurColumn + offset;
			Rect.top    = m_nCurRow;
			nID = AddDlgControl(_T("BUTTON"), _T("new Window"), //ID of the contol, default = 0
								STYLE_BUTTON, EXSTYLE_BUTTON, &Rect, pButton, pBase->m_cntrlID);
			// Get us back to the column position where we started from
			StepColumn(LEFT);
			StepRow();
		}
		break;
	}
}

CSTabCtrl * CDynControls::GetTabWnd()
{
	return &m_Tab;
}

void CDynControls::StepColumn(DIRECTION nDirection)
{
	// This code moves the position of the 
	// control to the begining of the next
	// MFC column
	// NOTE:  Three calls to this function
	//        equal the keyword "SEPERATOR"
	//        Only 3 DDL Columns are allowed
	
	switch(nDirection)
	{
		case RIGHT:
			m_nCurColumn += COLUMSTEPSIZE;
			break;

		case LEFT:
			m_nCurColumn -= COLUMSTEPSIZE;
			break;
	}
}

void CDynControls::StepRow()
{
	m_nCurRow += ONEROW; // +6 Leave some room between 
}

// Put placement of controls in 1st column
void CDynControls::ResetColumn()
{
	m_nCurColumn = FIXEDCOL1;
}

// Put placement of controls in 1st row
void CDynControls::ResetRow()
{
	m_nCurRow = m_nCurRowInit;
}

#if 0 /* <<<<<<< DynControls.cpp -- was my directory */

/* stevev 8/26/04
	This should create the object without too much actual window support
 */
ulong CDynControls::Generate(hCmenuItem* pMi, CdrawBase** pDrawItem)
{
	ulong newCntrl = 0;
/* MI
	hCreference				itemRef;
	hCbitString				qualifier;	
	menuItemDrawingStyles_t	draw_as;
	int                     sepCnt;
*/
// store control id
// store drawbase*
// set drawbase's ptr to this

	switch(pMi->getDrawAs())
	{
	case mds_window:
	case mds_dialog:
		TRACE(_T("++++++++++++++ NOT SUPPORTED YET +++++++++++++\n"));
		break;
	case mds_page:
		*pDrawItem = pDraw = new CdrawPage;
		pDraw->m_pDynCtrl = this;
		newCntrl = pDraw->cntrlID = getNewCntrlID();		
		mI = *pMi;
		break;
	case mds_group:
		*pDrawItem = pDraw = new CdrawGroup;
		pDraw->m_pDynCtrl = this;
		newCntrl = pDraw->cntrlID = getNewCntrlID();		
		mI = *pMi;
		break;
	case mds_menu:
	case mds_table:
		TRACE(_T("++++++++++++++ NOT SUPPORTED YET +++++++++++++\n"));
		break;
	case mds_var:
		*pDrawItem = pDraw = new CdrawVar;
		pDraw->m_pDynCtrl = this;
		newCntrl = pDraw->cntrlID = getNewCntrlID();
		break;
	case mds_edDisp:
	case mds_enum:
	case mds_bitenum:
	case mds_bit:
	case mds_method:
	case mds_image:
	//case mds_tab			// a pseudo drawing type we generate here
		TRACE(_T("++++++++++++++ NOT SUPPORTED YET +++++++++++++\n"));
		break;
	case mds_scope:			// charts and graphs
		*pDrawItem = pDraw = new CdrawScope;
		pDraw->m_pDynCtrl = this;
		newCntrl = pDraw->cntrlID = getNewCntrlID();		
		mI = *pMi;
		break;
	case mds_separator:
	case mds_constString:
	//case mds_tab			// a pseudo drawing type we generate here
		TRACE(_T("++++++++++++++ NOT SUPPORTED YET +++++++++++++\n"));
		break;
	default:
		break;
	}// endswitch
	return newCntrl;
}

ulong CDynControls::Generate(menuItemDrawingStyles_t dS, CdrawBase** pDrawItem)
{
	ulong rVal = 0;

	switch(dS)
	{
	case mds_window:
	case mds_dialog:
	case mds_page:
	case mds_group:
	case mds_menu:
	case mds_table:

	case mds_var:
	case mds_edDisp:
	case mds_enum:
	case mds_bitenum:
	case mds_bit:
	case mds_method:
	case mds_image:
	case mds_scope:			// charts and graphs
	case mds_separator:
	case mds_constString:
	case mds_grid:
		TRACE(_T("++++++++++++++ NOT SUPPORTED +++++++++++++++++\n"));
		break;
	case mds_tab:			// a pseudo drawing type we generate here
		*pDrawItem = pDraw = new CdrawTab;
		pDraw->m_pDynCtrl = this;
		rVal = pDraw->cntrlID = getNewCntrlID();
		break;
	default:
		break;
	}// endswitch

	return rVal;
}

ulong CDynControls::AddPage(hCmenuItem* pMi, CDynControls* pPage)
{
	ulong rVal = 0;

    if ( pMi == NULL || pPage == NULL || pDraw == NULL) // parameter error
    {
		return 0;// failure
	}
	// verify we are a tab control
    if (pDraw->mdsType != mds_tab) // type call error
    {
		return 0;// failure
	}

	return rVal;
}

ulong CDynControls::getNewCntrlID(void)
{
	return( (CDDIdeApp *) AfxGetApp())->getUniqueID();
}



#else /* ======= in cvs */

void CDynControls::AddTabPage(CString strName, UINT nPage)
{
	TC_ITEM tcItem;
	memset(&tcItem,0,sizeof(TC_ITEM));
	TCHAR  szName[1024];

	BOOL bIsPage = m_Tab.GetItem(nPage,&tcItem);

	if (!bIsPage)
	{
		//Configure the page
		_stprintf(szName, (LPCTSTR) strName);
		tcItem.mask = TCIF_TEXT;
		tcItem.pszText = szName;
		tcItem.cchTextMax = strName.GetLength();
		m_Tab.InsertItem(nPage,&tcItem);
	}
}

void CDynControls::RemoveTabPage(UINT nPage)
{
	if (::IsWindow(m_Tab.m_hWnd))
	{
		//m_Tab.SetCurSel(0); // moved to before removal 14aug08, removed 21aug08
		m_Tab.RemoveTab(nPage);
	}
}

// stevev21aug08 get page zero to the front
void CDynControls::setFirstPage(void)
{
	m_Tab.SetZeroSel();
}


void CDynControls::AddPageCtrl(UINT cntrlID, UINT page)
{
	/*
	 * Now add the controls to the appropriate tab page
	 *
	 * The actual MFC tab page does not get created
	 * until you attach the 1st control to the tab along
	 * with its page number
	 */
	m_Tab.AttachControlToTab(GetDlgCtrl(cntrlID), page);

	/*
	 * Initialize tab to the first page
	 */
	if (page == 0)
	{
		m_Tab.SetCurSel(page);
	}
#ifdef _DEBUG
	CWnd * pWnd = GetDlgCtrl(cntrlID);
	if ( pWnd == 0 )
	{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Control to Page %d Has no Window to size test.\n",page);
		return;
	}
	RECT clientRect, windowRect;// left,top,right,bottom
	pWnd->GetClientRect(&clientRect);
	pWnd->GetWindowRect(&windowRect);
	
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Control to Page %d Client: L:%3d  T:%3d  B:%3d  R:%3d\n"
		                        "                  Window: L:%3d  T:%3d  B:%3d  R:%3d\n",
			page,	clientRect.left,clientRect.top,clientRect.bottom,clientRect.right,
					windowRect.left,windowRect.top,windowRect.bottom,windowRect.right  );
#endif
}

void CDynControls::AppendMenu(CdrawBase * pBase, CString strMenu)
{
	if (m_pMenuButton)
	{
		if (pBase->mdsType == mds_menu)
		{
			CdrawMenu * pDrawMenu = (CdrawMenu *) pBase;

			// This menu lives on another menu, so this should be a pop-up
			// Add its label to the parent menu
			if (pDrawMenu->m_pMenu)
			{
				HMENU hChildMenu = m_pMenuButton->m_Menu.m_hMenu;

				// Add new popup to parents CMenu
				pDrawMenu->m_pMenu->AppendMenu(MF_POPUP, (UINT)hChildMenu, strMenu);
			}
		}
		else if (pBase->mdsType == mds_separator || pBase->mdsType == mds_newline)
		{
			m_pMenuButton->m_Menu.AppendMenu(MF_SEPARATOR, pBase->m_cntrlID);
		}
		else
		{
			// Add the CdrawBase menu item to our own menu
			m_pMenuButton->m_Menu.AppendMenu(MF_STRING, pBase->m_cntrlID, (LPCTSTR)strMenu);
		}
	}
}
#endif /* conflict >>>>>>> 1.1.2.2 */

CMenu * CDynControls::GetMenu()
{
	return &(m_pMenuButton->m_Menu);
}

void CDynControls::OnMenuHelp() 
{
	m_pdrawDlg->DisplayHelp(m_point);
}

void CDynControls::OnMenuLabel() 
{
	m_pdrawDlg->DisplayLabel(m_point);
}

void CDynControls::OnMenuDisplayValue() 
{	
    m_pdrawDlg->DisplayValue(m_point);
}

#ifndef _WIN32_WCE	// PAW No mouse in Win CE 29/04/09
void CDynControls::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CMenu menu;
	CMenu* pSubmenu;
	DWORD dwPosition = GetMessagePos();
    CPoint mesgpoint(LOWORD(dwPosition), HIWORD(dwPosition));
	m_point = mesgpoint;
	int nPos;

	CdrawBase * pBase = m_pdrawDlg->HitTest(m_point);

	if (pBase)
	{
		pBase->LoadContextMenu( &menu, nPos ) ;
		pSubmenu = menu.GetSubMenu(nPos);
        
		pSubmenu->TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
                                      m_point.x, m_point.y, this);

	}

	sdCDialog::OnRButtonDown(nFlags, point);
}


void CDynControls::OnLButtonDown(UINT nFlags, CPoint point) 
{// from a menu of type dialog...
/*** now handled in the setfocus part of executecid()...
	DWORD dwPosition = GetMessagePos();
    CPoint mesgpoint(LOWORD(dwPosition), HIWORD(dwPosition));
	m_point = mesgpoint;

	CdrawBase * pBase = m_pdrawDlg->HitTest(m_point);

	if (pBase)
	{
		pBase->Edit();
	}
*** end of handled elsewhere **/
	sdCDialog::OnLButtonDown(nFlags, point);
}
#endif
void CDynControls::SetCtrlRect(CRect & Rect, CdrawBase * pBase, int offset)
{
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"    Set Control Rect Entry: "
		"CurCol= %d, CurRow= %d and DDLcol= %d DDLrow= %d     mul:%f"
		"  Item:0x%04x  itemSize: w:%d, h:%d\n",m_nCurColumn, m_nCurRow, m_DDLcolumn, m_DDLrow, 
		 columnMultiplier, pBase->m_symblID,
		pBase->m_MyLoc.da_size.xval, pBase->m_MyLoc.da_size.yval);
	int LeftAdjust   = offset;// everybody gets offset
	int RightAdjust  = offset;
	int BottomAdjust = 0;
	int Bottom       = ( ( pBase->m_MyLoc.da_size.yval ) * ONEROW  );
    int Right        = ( ( pBase->m_MyLoc.da_size.xval ) * COLSIZE )   + 
										     ( COLUMNGAP * (pBase->m_MyLoc.da_size.xval - 1) );

	// NOTE:  These adjustments are having to be made because currently pBase->rectSize.xval 
	// and pBase->rectSize.yval are INTs and not floats.
	// These will probably dissapear when that change is made.
	switch (pBase->mdsType)
	{
		case mds_group:		
			{	
/***** original centering code
// try to get variable width columns
////int y = m_DDLcolumn - 1;
//int c = COLUMSTEPSIZE * 3;
// get the normal space to left
////int w = COLSIZE * y;
// take width * multiplier 
////double t = w * columnMultiplier;
//     we DO want to center the group on whatever we're on 
// add half the col diff to get the middle of expanded column
////t += ((COLSIZE * (columnMultiplier-1))/2);

// add left offset
////t += FIXEDCOL1;
******/
//      # col on left     number of columns for center
double y =	(m_DDLcolumn - 1) + columnMultiplier;
double t = (COLSIZE * y) + FIXEDCOL1 + (COLUMNGAP * y);
// round it to int
m_nCurColumn = (int)(t + .5);  
			}
			// Group
			LeftAdjust   += GRP_LEFT_ADJ;    // need to move the left side of the group over
			BottomAdjust += GRP_BOTM_ADJ;
			RightAdjust  += GRP_RGHT_ADJ;
#ifdef _DEBUG			
	Rect.left   = m_nCurColumn + LeftAdjust;
	Rect.top    = m_nCurRow;
	Rect.bottom = m_nCurRow    + BottomAdjust + Bottom; 
	Rect.right  = m_nCurColumn + RightAdjust  + Right; 
	LOGIT(CLOG_LOG,L"Group Column %d Calculations: \n",m_DDLcolumn);
	LOGIT(CLOG_LOG,L"             CurCol= %d  Xval= %d Yval= %d\n",m_nCurColumn,
									pBase->m_MyLoc.da_size.xval,pBase->m_MyLoc.da_size.yval);
	LOGIT(CLOG_LOG,L"             L:%d  T:%d   B:%d  R:%d  (m=%f)\n",
									Rect.left,Rect.top,Rect.bottom,Rect.right,columnMultiplier);
#endif
			break;

		case mds_bitenum:
            LeftAdjust   += GRP_LEFT_ADJ + 1; // need to move the left side over to make it appear a little different than mds_group
        BottomAdjust += GRP_BOTM_ADJ - 1;
			RightAdjust  += GRP_RGHT_ADJ - 1; // Adding this fixed defect 4541 - PB. 3/5/14
			break;

		case mds_image:
			BottomAdjust += GRP_BOTM_ADJ - 1;
        RightAdjust  += GRP_RGHT_ADJ - 1; // actually -1 per ( col - 1 )
			break;

		case mds_tab:
			// Tab
			// stevev 28oct10 - we have to redo the CDynControls initialization
			//  we need to remove the half col diff offset
			{	
/***** original centering code		
// try to get variable width columns
int y = m_DDLcolumn - 1;
//int c = COLUMSTEPSIZE * 3;
// get the normal space to left
int w = COLSIZE * y;
// take width * multiplier 
double t = w * columnMultiplier;
///// we don't want to center the page on the window
// add half the col diff to get the middle of expanded column
////t += ((COLSIZE * (columnMultiplier-1))/2);

// add left offset
t += FIXEDCOL1; 
****/
//            # col on left   number of columns for center ( should always be zero for tab )
double y = (m_DDLcolumn - 1) + columnMultiplier;
double t = (COLSIZE * y) + FIXEDCOL1 + (COLUMNGAP * y);
// round it to int
m_nCurColumn = (int)(t + .5); 

t = columnMultiplier * COLSIZE;// offset
// now rework the right to be multiplied columns wide
//t     = ((double)Right * columnMultiplier)+ 0.5;
//Right = (int)t ;
// right already has columngap added t += (( pBase->m_MyLoc.da_size.xval -1 ) * COLUMNGAP );
Right += (int)( t + FIXEDCOL1 + .5 );
			}
			LeftAdjust   += TAB_LEFT_ADJ;
			break;

		case mds_constString:
			// Constant String
			// This needs to take up an entire DD column.
			// NOTE: Currently constant groups are not multi-column so it is not necessary to 
			//       factor in COLUMNGAP.  We will leave it here for expandability. 
			//
			// 2 text lines per row, 8 pixels in height, subtract out the remaining equation
			BottomAdjust += ((8 * 2) * pBase->m_MyLoc.da_size.yval); 
        RightAdjust  += GRP_RGHT_ADJ - 4; //Move right side  more to the left to prevent the deleting of the group box - POB, 3/23/2014
			Bottom       = 0;					// Do not need the default equation from above
			break;

		case mds_blank:
			// Currently only used for a Column Break that takes up room
			// This needs to take up an entire DD column.
			// NOTE: Currently constants are not multi-column so it is not necessary to 
			//       factor in COLUMNGAP.  We will leave it here for expandability. 
			//
			// 2 text lines per row, 8 pixels in height, subtract out the remaining equation
			BottomAdjust += ((8 * 2) * 1); 
        RightAdjust  += GRP_RGHT_ADJ - 4; //Move right side  more to the left to prevent the deleting of the group box - POB, 3/23/2014
			Bottom       = 0;					// Do not need the default equation from above
			break;

		case mds_scope:
			// Scope
			BottomAdjust += - ROWSTEPSIZE;
        RightAdjust  += GRP_RGHT_ADJ - 5;
			break;

/*		case mds_bitenum:	// POB, 30 March 2005 - change the controls of the bit enum
			// Bit Enum
			// NOTE:  Treat like Group
			LeftAdjust   = GRP_LEFT_ADJ;    // need to move the left side of the group over
			BottomAdjust = GRP_BOTM_ADJ;
			RightAdjust  = GRP_BOTM_ADJ;
			break;*/

		case mds_bit:
			// Bit referrence
			Bottom       = BIT_SIZE;
		    Right        = BIT_SIZE;// 22mar13, was += BIT_SIZE
			break;

		case mds_dialog:
			// Dialog
        if (!pBase->class_history.expTblDlg) // Ensure that this is not a support dialog in style TABLE
        {
            /*
             * We need to ensure that all menus of style DIALOG 
             * have a 3 column minimum width canvas before
             * adding the system buttons, POB - 3/28/2014
             */
            if (m_nCurColumn + Right < (FIXEDCOL1 + (COLSIZE * 3)))
            {
                // The size is less than 3 columns, adjust width size to be 3 columns, POB - 3/28/2014
                RightAdjust = (FIXEDCOL1 + (COLSIZE * 3)) - (m_nCurColumn + Right);
            }
        }
			break;

		case mds_grid:
	//		BottomAdjust = 8;
	//		m_nCurRow+=8;
			BottomAdjust += (0 - ROWSTEPSIZE);
        RightAdjust  += GRP_RGHT_ADJ - 5;
			break;

		default:
			break;
	}

	Rect.left   = m_nCurColumn + LeftAdjust;
	Rect.top    = m_nCurRow;
	Rect.bottom = m_nCurRow    + BottomAdjust + Bottom; 
	Rect.right  = m_nCurColumn + RightAdjust  + Right; 
	
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"                      Exit: "
		"CurCol= %d, CurRow= %d and DDLcol= %d DDLrow= %d     \n"
         "                            mul:%f  Item:0x%04x  itemSize: w:%d, h:%d\n",
		 m_nCurColumn, m_nCurRow, m_DDLcolumn, m_DDLrow,columnMultiplier, pBase->m_symblID,
		pBase->m_MyLoc.da_size.xval, pBase->m_MyLoc.da_size.yval);
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"             Returned Rect: L:%d, T:%d, R:%d,  B:%d \n",
													Rect.left,Rect.top,Rect.right,Rect.bottom);
}


LRESULT CDynControls::OnDlgAbort(WPARAM wParam, LPARAM)
{
	if ( m_pdrawDlg )
    {
		m_pdrawDlg->m_bAbortBtnClicked = TRUE;
    }

    if (m_ButtonCount > 0) // we're done // a displaymenu
    {
		sdcEndDialog(0);
	}
	return TRUE;
}
LRESULT CDynControls::OnDlgOK   (WPARAM wParam, LPARAM lp)
{
	//-//LOGIT(CLOG_LOG,"/////CDynControls-OKbutton\n");
	if ( m_pdrawDlg )
    {
		m_pdrawDlg->m_bNextBtnClicked = TRUE;
    }

    if (m_ButtonCount > 0) // we're done // a displaymenu
    {
		sdcEndDialog(1);
		//sdcEndDialog(1);
	}
	return TRUE;
}
LRESULT CDynControls::OnDlgBut01(WPARAM wParam, LPARAM lp)
{
	//-//LOGIT(CLOG_LOG,"/////CDynControls-OneButton\n");
	if ( m_pdrawDlg )
    {
		m_pdrawDlg->m_bButOneClicked = TRUE;
    }

    if (m_ButtonCount > 0) // we're done // a displaymenu
    {
		sdcEndDialog(2);
	}
	return TRUE;
}
LRESULT CDynControls::OnDlgBut02(WPARAM wParam, LPARAM lp)
{
	//-//LOGIT(CLOG_LOG,"/////CDynControls-TwoButton\n");
	if ( m_pdrawDlg )
    {
		m_pdrawDlg->m_bButTwoClicked = TRUE;
    }

    if (m_ButtonCount > 0) // we're done // a displaymenu
    {
		sdcEndDialog(3);
	}
	return TRUE;
}
LRESULT CDynControls::OnDlgBut03(WPARAM wParam, LPARAM lp)
{
	//-//LOGIT(CLOG_LOG,"/////CDynControls-ThreButton\n");
	if ( m_pdrawDlg )
    {
		m_pdrawDlg->m_bButThrClicked = TRUE;
    }

    if (m_ButtonCount > 0) // we're done // a displaymenu
    {
		sdcEndDialog(4);
	}
	return TRUE;
}

void/*BOOL PAW 03/03/09 */ CDynControls::OnEnterIdle(UINT nWhy, CWnd* pWho)
{
#ifdef USED	// PAW 03/03/09
	//TRACE(_T("Dialog's OnIdle\n"));
	return FALSE;// no more to do
	// stevev 22jun06	return sdCDialog::OnIdle(4);
#else
	EnterIdle();	// call the function with a return type
#endif
}
// PAW 03/03/09 start
BOOL CDynControls::EnterIdle(void)
{
	//TRACE(_T("Dialog's OnIdle\n"));
	return FALSE;// no more to do
	// stevev 22jun06	return sdCDialog::OnIdle(4);
}
// PAW 03/03/09 end

void CDynControls::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	// assume that cx/cy are in client-area coords  
	/* Type SIZE_RESTORED 0 ; SIZE_MINIMIZED  1 ; SIZE_MAXIMIZED 2
	        SIZE_MAXSHOW  3 ; SIZE_MAXHIDE    4    ***************/
	
	RECT clientRect;
	clientRect.top    = clientRect.left      = 0;
	clientRect.bottom = cy/* + 0x19*/; clientRect.right = cx + 0x06;
	/* stevev 23mar07 - empirically determined to loose 0x19 Y pixels and 0x06 X pixels
						between each call here ie MoveWindow(x162,x2fc) gets here as (15c,2e3)
						so we call MoveWindow(x162,x2e3) which gets here as (15c,2ca)  */
	int vScrollMax, hScrollMax;
	// all our calculations are in client units...

    if (clientRect.bottom == m_vCurHeight && clientRect.right == m_hCurWidth)
    {
		return;// no change (stop the re-entrancy from MoveWindow() below)
    }
	else
	{
        m_vCurHeight = clientRect.bottom;
		m_hCurWidth = clientRect.right;
	}

	int rows = (int)(((float)clientRect.bottom/(float)convRect.bottom)+0.5);// viewport
	int cols = (int)(((float)clientRect.right/(float)convRect.right)+0.5);
	int Rows = (int)(((float)m_rect.bottom/(float)convRect.bottom)+0.5);	  // drawport
	int Cols = (int)(((float)m_rect.right /(float)convRect.right)+0.5);
	int uRow = min(rows,Rows) ;
	int uCol = min(cols,Cols);
	bool moveit = false;

	if (clientRect.bottom < m_rect.Height())
	{
	     vScrollMax = m_rect.Height() - clientRect.bottom;//drawport minus one viewport
		 if (vScrollMax < convRect.bottom && 
            m_rect.Height() <= (dskTopRect.bottom * PERCENT_OF_FULL_HIGHT)) // small scroll size while we are smaller than max
        {
			 clientRect.bottom = m_rect.Height();
			 vScrollMax = 0;
			 moveit = true;
		 }
	}
	else
    {
	     vScrollMax = 0;
    }
	
	if (clientRect.right < m_rect.Width())
	{
	     hScrollMax = m_rect.Width() - clientRect.right;//drawport minus one viewport
		 if (hScrollMax < (convRect.right/3) && 
            m_rect.Width() <= (dskTopRect.right * PERCENT_OF_FULL_WIDTH)) // small scroll size while we are smaller than max
        {
			 clientRect.right = m_rect.Width();
			 hScrollMax = 0;
			 moveit = true;
		 }
	}
	else
    {
	     hScrollMax = 0;
	}

    vPage = clientRect.bottom;
    hPage = clientRect.right + 1; // empirically determined, page must be '1' larger than client or scollbars get added even if not needed
	
	if (moveit)
	{
        /* NOTE:  MoveWindow  causes a defect in STYLE DIALOG and causes the dialog to become a splash screen
         * sizeDialog() appears to do everything we want to do for initialiation purposes so as of now it
         * appears that this is call is not necessary especially since the dialog is not able to be resized
         * by the user, POB - 3/25/2014
         */
		// set to newly calculated client rect
        //MoveWindow( &clientRect, TRUE); // remove for now, POB - 3/25/2014
	}
	// reuse client to convert sizes
	clientRect.top = vPage; clientRect.bottom = vScrollMax;
	clientRect.left= hPage; clientRect.right  = hScrollMax;

	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask  = SIF_ALL; // SIF_ALL = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin   = 0;
    si.nMax   = m_rect.Height();
    si.nPage  = clientRect.top; //vPage, Page Size (not number of pages)
	si.nPos   = 0;
        SetScrollInfo(SB_VERT, &si, TRUE); 
    si.nMax   = m_rect.Width();
    si.nPage  = clientRect.left; //Page Size (not number of pages)
	si.nPos   = 0;
        SetScrollInfo(SB_HORZ, &si, TRUE); 
} 

void CDynControls::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nDelta;
    int nMaxPos = m_rect.Height() - m_vCurHeight;

	switch (nSBCode)
	{
	case SB_LINEDOWN:
		if (m_vScrollPos >= nMaxPos)
			return;
		//nDelta = min(nMaxPos/100,nMaxPos-m_vScrollPos);
		nDelta = convRect.bottom;//(ONEROW)
		break;

	case SB_LINEUP:
		if (m_vScrollPos <= 0)
			return;
		//nDelta = -min(nMaxPos/100,m_vScrollPos);
		nDelta = - convRect.bottom;
		break;

    case SB_PAGEDOWN:
		if (m_vScrollPos >= nMaxPos)
			return;
		if (m_vScrollPos+vPage >=nMaxPos)
			nDelta = nMaxPos - m_vScrollPos;
		else
			nDelta = vPage;//min(nMaxPos/10,nMaxPos-m_vScrollPos);
		break;

	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_vScrollPos;
		break;

	case SB_PAGEUP:
		if (m_vScrollPos <= 0)
			return;
		if (m_vScrollPos-vPage <= 0)
			nDelta = - m_vScrollPos;
		else
			nDelta = -vPage;//-min(nMaxPos/10,m_vScrollPos);
		break;
	
         default:
		return;
	}
	m_vScrollPos += nDelta;
	int bPos = (int)(((float)m_vScrollPos/(float)nMaxPos)*(float)m_rect.Height());
	SetScrollPos(SB_VERT,/*bPos*/m_vScrollPos,TRUE);// moves the 'thumb' on the bar
	ScrollWindow(0,-nDelta);
	if (nSBCode == SB_ENDSCROLL)// stevev 12jul07 to get the images painted
		Invalidate();
	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
} 


void CDynControls::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nDelta;
	int nMaxPos = m_rect.Width() - m_hCurWidth;

	switch (nSBCode)
	{
	case SB_LINERIGHT:
		if (m_hScrollPos >= nMaxPos)
			return;
        nDelta = convRect.right; // move a column each time clicking on the right arrow
		break;

	case SB_LINELEFT:
		if (m_hScrollPos <= 0)
			return;
		nDelta = 0 - m_hScrollPos;//was: -ONECOL;
		break;

    case SB_PAGERIGHT:
		if (m_hScrollPos >= nMaxPos)
			return;
		if (m_hScrollPos+hPage >=nMaxPos)
			nDelta = nMaxPos - m_hScrollPos;
		else
			nDelta = hPage;
		break;

	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_hScrollPos;
		break;

	case SB_PAGELEFT:
		if (m_hScrollPos <= 0)
			return;
		if (m_hScrollPos-hPage <= 0)
			nDelta = - m_hScrollPos;
		else
			nDelta = -hPage;//-min(nMaxPos/10,m_vScrollPos);
		break;
	
         default:
		return;
	}
	m_hScrollPos += nDelta;
	int bPos = (int)(((float)m_hScrollPos/(float)nMaxPos)*(float)m_rect.Width());
	SetScrollPos(SB_HORZ,bPos,TRUE);// now in parts of whole
	ScrollWindow(-nDelta,0);// in device units unless window has style: CS_OWNDC or CS_CLASSDC
	if (nSBCode == SB_ENDSCROLL)// stevev 12jul07 to get the images painted
		Invalidate();
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
} 
#ifdef _DEBUG
/***/
#define lgSD(nm,R) \
LOGIT(CLOG_LOG,"SizeDlg:  %16s:Left:%4d  Right:%4d    Top:%4d  Bottm:%4d\n"\
							,nm,  R.left,R.right,R.top,R.bottom)
#else
#define lgSD(nm,R)
#endif

#if 1 // try new code  23aug12

void CDynControls::sizeDialog(void)
{
	RECT  clientRect, targetRect, windowRect;

	DWORD newStyleSet = 0;

	// we are located on the desktop
	SystemParametersInfo(
	  SPI_GETWORKAREA,  // system parameter to retrieve or set
	  0,   
	  &dskTopRect,     // workarea on primary monitor in virtual screen coordinates
	  0   
	);
/***/lgSD( "dskTopRect",dskTopRect);// screen units

	// set a rough target size

	/* 22aug12 - add a tad of code to deal with multiple monitors */
	int wc = (int)(dskTopRect.right / (dskTopRect.bottom * 1.5));
	if ( wc >= 3 ) wc = 3; else if (wc >= 2 ) wc = 2; else wc = 1;
	dskTopRect.right = dskTopRect.right / wc;

	targetRect.bottom = (LONG)(dskTopRect.bottom * PERCENT_OF_FULL_HIGHT);
	targetRect.right  = (LONG)(dskTopRect.right  * PERCENT_OF_FULL_WIDTH);
	targetRect.left = targetRect.top = 0;
/***/lgSD( "targetRect",targetRect);// screen units

	GetClientRect( &clientRect );// what bill chose to draw for viewport(info purposes only)
/***/lgSD( "clientRect",clientRect);// screen units...top left always zero


	GetDlgRect   ( m_rect );    // drawport in dialog coords < from m_DialogTemplate >
/***/lgSD( "m_rect",m_rect);	// dialog units
    DlgUnits2Pixels(&m_rect);   // now in screen-units(== pixels)
/***/lgSD( "m_rect-pix",m_rect);// DlgUnits2Pixels() proven correct conversion 23aug12

	// set some conversion values
	convRect.right = ONECOL;
	convRect.bottom= ONEROW;
	convRect.left  = FIXEDCOL1;
	convRect.top   = FIRSTROW1;
/***/lgSD( "convRect",convRect);// these are dialog units

    DlgUnits2Pixels(&convRect);   // convert to screen-units (== pixels)
/***/lgSD( "convRect-pix",convRect);
	int rows = (int)(((float)targetRect.bottom/(float)convRect.bottom)+0.5);
	int cols = (int)(((float)targetRect.right /(float)convRect.right)+0.5);
	int Rows = (int)(((float)m_rect.bottom/(float)convRect.bottom)+0.5);
	int Cols = (int)(((float)m_rect.right /(float)convRect.right)+0.5);
	int targetRows = min(rows,Rows) ;
	int targetCols = min(cols,Cols);

    // out of time to clean this up....leave in the 1 row for system buttons
    // This is necessary for smaller desktops, POB - 6/11/2014 
    targetRows++;
    
	// use the other half of target Rect for current calcs

	//target width
	targetRect.left = (targetCols * convRect.right) + convRect.left;
	while ((targetRect.left + vScrollBarWidth) > dskTopRect.right)// reduce to fit desktop
	{
		if ( targetCols > 1 ) targetCols--;
		targetRect.left = (targetCols * convRect.right) + convRect.left;
/**/	LOGIT(CLOG_LOG,"        targetCols reduced by 1 to %d\n",targetCols);
	}

	// target height
	targetRect.top  = (targetRows * convRect.bottom) + convRect.top;
	while ((targetRect.top + hScrollBarWidth) > dskTopRect.bottom)
	{
		if ( targetRows > 1 ) targetRows--;
		targetRect.top  = (targetRows * convRect.bottom) + convRect.top;
/**/	LOGIT(CLOG_LOG,"        targetRows reduced by 1 to %d\n",targetCols);
	}
/**/lgSD( "targetRectTL",targetRect);
/**/LOGIT(CLOG_LOG,"                hScrollBarWidth:%d          vScrollBarWidth:%d\n",
		  hScrollBarWidth,vScrollBarWidth);

	// target top/left is now a sized dialog client area without scroll bars(screen units)
	clientRect.right  = targetRect.left;//                   clientRect Left is always zero	
	clientRect.bottom = targetRect.top;	// new client area...clientRect Top  is always zero

	int vScrollAddition = 0;
	//see if we need a vertical scroll bar on right
    if ((m_rect.bottom - m_rect.top + hScrollBarWidth) > targetRect.top) //   drawport hight + scroll bar height bigger than possible visible vertical
    {
		newStyleSet |= WS_VSCROLL;          // then we need a vertical scroll bar
		vScrollAddition  =  vScrollBarWidth;
		clientRect.right += vScrollAddition;// offset the width of a vertical scroll bar
/**/LOGIT(CLOG_LOG,"Add Vertical Scrollbar. (%d was bigger than target %d)\n",
		  m_rect.bottom - m_rect.top + hScrollBarWidth,   targetRect.top);
	}

    if ((m_rect.right - m_rect.left + vScrollAddition) > targetRect.left) // logical width + scroll bar width   bigger than visible horizontal size
    {
		// see if it'll fit on the desktop
        if ((m_rect.right + vScrollAddition) < dskTopRect.right) // fits
        {
			//// what???  clientRect.right = m_rect.right + vScrollAddition;
		}
		else
		{
			// then we need a horizontal scroll bar
			newStyleSet |= WS_HSCROLL;
			clientRect.bottom += hScrollBarWidth;
/**/LOGIT(CLOG_LOG,"Add Horizontal Scrollbar. (%d was bigger than target %d)\n",
		  m_rect.right - m_rect.left + vScrollAddition,   targetRect.left);
		}
	}
/***/lgSD( "clientRect",clientRect);

 	ModifyStyle( WS_VSCROLL|WS_HSCROLL, 0 );// clear
	ModifyStyle( 0, newStyleSet);           // set

    //Removed duplicate code used in OnSize(), POB - 3/26/2014

	// now set the new window
	BOOL res = ::SetWindowPos(GetSafeHwnd(),HWND_TOP, clientRect.left, clientRect.top,
		clientRect.right-clientRect.left, clientRect.bottom-clientRect.top, SWP_SHOWWINDOW );

	if ( ! res )
	{	LPVOID lpMsgBuf;
		 FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		 );
		 LOGIT(CLOG_LOG,"      SetWindow   error String '%s'",lpMsgBuf);
		 TRACE(_T("     SetWindow    error String '%s'\n"),lpMsgBuf);
		LocalFree(lpMsgBuf);
	}
#ifdef _DEBUG
	GetClientRect( &clientRect );//what bill chose to draw for viewport(info purposes only)
/***/lgSD( "clientRect@exit",clientRect);

	GetWindowRect(&windowRect); //uses the same metrics as setwindowpos
/***/lgSD( "windowRect@exit",windowRect);

#endif
}
#else // old code here...for a while
void CDynControls::sizeDialog(void)
{
	RECT  clientRect, targetRect, dbgRect;
	DWORD newStyleSet = 0;

	// we are located on the desktop
	SystemParametersInfo(
	  SPI_GETWORKAREA,  // system parameter to retrieve or set
	  0,   
	  &dskTopRect,     // workarea on primary monitor in virtual screen coordinates
	  0   
	);
/***/lgSD( "dskTopRect",dskTopRect);
/**
RECT rMyRect;

   GetClientRect(hwnd, (LPRECT)&rMyRect);
   ClientToScreen(hwnd, (LPPOINT)&rMyRect.left);
   ClientToScreen(hwnd, (LPPOINT)&rMyRect.right); 

  ..virtual void CalcWindowRect( LPRECT lpClientRect, UINT nAdjustType = adjustBorder );

**/
//Pixels2DlgUnits(&dskTopRect);
	// set a rough target size

	/* 22aug12 - add a tad of code to deal with multiple monitors */
	int wc = (int)(dskTopRect.right / (dskTopRect.bottom * 1.5));
	if ( wc >= 3 ) wc = 3; else if (wc >= 2 ) wc = 2; else wc = 1;
	dskTopRect.right = dskTopRect.right / wc;

	targetRect.bottom = (LONG)(dskTopRect.bottom * PERCENT_OF_FULL_HIGHT);
	targetRect.right  = (LONG)(dskTopRect.right  * PERCENT_OF_FULL_WIDTH);
	targetRect.left = targetRect.top = 0;
/***/lgSD( "targetRect",targetRect);

	GetClientRect( &clientRect );// what bill chose to draw for viewport(info purposes only)
/***/lgSD( "clientRect",clientRect);
//Pixels2DlgUnits(&clientRect );

	GetDlgRect   ( m_rect );  // drawport in dialog coords < from m_DialogTemplate >
/***/lgSD( "m_rect",m_rect);	// dialog units
// no longer needed....23aug12////dbgRect = m_rect;
    DlgUnits2Pixels(&m_rect); // now in screen-units(== pixels)
/***/lgSD( "m_rect-pix",m_rect);
// DlgUnits2Pixels() proved to generate exactly the same results as MapDialogRect()
// no longer needed....23aug12////
//	MapDialogRect( &dbgRect ); //now in same coordinates as clientRect..dialogUnits2screenUnits
// no longer needed....23aug12////
/////lgSD( "dbgRect",dbgRect);// note that this is exactly the same result as DlgUnits2Pixels

	convRect.right = ONECOL;
	convRect.bottom= ONEROW;
	convRect.left  = FIXEDCOL1;
	convRect.top   = FIRSTROW1;
/***/lgSD( "convRect",convRect);
//RECT tstRect01 = mapRect;
//better without this    DlgUnits2Pixels(&convRect);// now in device-units(== pixels)
/***/lgSD( "convRect-pix",convRect);
//MapDialogRect( &mapRect );
/* bool DlgUnits2Pixels(RECT* pRect);
bool Pixels2DlgUnits(RECT* pRect);
*/
	int rows = (int)(((float)targetRect.bottom/(float)convRect.bottom)+0.5);
	int cols = (int)(((float)targetRect.right /(float)convRect.right)+0.5);
	int Rows = (int)(((float)m_rect.bottom/(float)convRect.bottom)+0.5);
	int Cols = (int)(((float)m_rect.right /(float)convRect.right)+0.5);
	int uRow = min(rows,Rows) ;
	int uCol = min(cols,Cols);
uRow ++;
	// use the other half of target for current calcs
	targetRect.left = (uCol * convRect.right) + convRect.left;
	if ((targetRect.left + vScrollBarWidth) > dskTopRect.right)
	{
		if ( uCol > 1 ) uCol--;
		targetRect.left = (uCol * convRect.right) + convRect.left;
	}

	targetRect.top  = (uRow * convRect.bottom) + convRect.top;
	if ((targetRect.top + hScrollBarWidth) > dskTopRect.bottom)
	{
		if ( uRow > 2 ) uRow-=2;
		targetRect.top  = (uRow * convRect.bottom) + convRect.top;
	}
/**/lgSD( "targetRectTL",targetRect);
/**/LOGIT(CLOG_LOG,"hScrollBarWidth:%d  vScrollBarWidth:%d\n",hScrollBarWidth,vScrollBarWidth);
	// target top/left is now a sized dialog client area without scroll bars
	int vScrollAddition = 0;
	clientRect.right = targetRect.left;
    if ( ( m_rect.bottom + hScrollBarWidth ) > targetRect.top ) //   drawport hight + scroll bar hight bigger than possible visible vertical
    {
		newStyleSet |= WS_VSCROLL;          // then we need a vertical scroll bar
		vScrollAddition  =  vScrollBarWidth;
		clientRect.right += vScrollAddition;// offset the width of a vertical scroll bar
	}

	clientRect.bottom = targetRect.top;	
    if ( (m_rect.right + vScrollAddition) > targetRect.left ) // logical width + scroll bar width   bigger than visible horizontal
    {
		// see if it'll fit on the desktop
		if ((m_rect.right + vScrollAddition) < dskTopRect.right)
		{
			clientRect.right = m_rect.right + vScrollAddition;
		}
		else
		{
			// then we need a horizontal scroll bar
			newStyleSet |= WS_HSCROLL;
			clientRect.bottom += hScrollBarWidth;
		}
	}
/***/lgSD( "clientRect",clientRect);

 	ModifyStyle( WS_VSCROLL|WS_HSCROLL, 0 );// clear
	ModifyStyle( 0, newStyleSet);           // set

	hPage = convRect.right;//not number of pages...(((float)clientRect.right/(float)uCol)+0.5);
	vPage = ((uRow-1) * convRect.bottom);
	if ( ((int)(((float)m_rect.bottom/(float)vPage)+0.5)) < 3 )// if less than 3 pages
	{	
		vPage = (int)(((float)uRow/2.0) * convRect.bottom);	
	} // make it a half page per scroll
	else
	if (uRow > 2)
	{
		vPage = ((uRow-1) * convRect.bottom); // make it one screen less a row per scroll
	}
	else
	{
		vPage = targetRect.top; // default to one screen
	}
//DlgUnits2Pixels(&clientRect);	
	// set to newly calculated client rect
	// temp  try setwindoe... MoveWindow( &clientRect);
	BOOL res = ::SetWindowPos(GetSafeHwnd(),HWND_TOP, clientRect.left, clientRect.top,
		clientRect.right-clientRect.left, clientRect.bottom-clientRect.top, SWP_SHOWWINDOW );

	if ( ! res )
	{	LPVOID lpMsgBuf;
		 FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		 );
		 LOGIT(CLOG_LOG,"      SetWindow   error String '%s'",lpMsgBuf);
		 TRACE(_T("     SetWindow    error String '%s'\n"),lpMsgBuf);
		LocalFree(lpMsgBuf);
	}
#ifdef _DEBUG
		GetClientRect( &clientRect );//what bill chose to draw for viewport(info purposes only)
/***/lgSD( "clientRect@exit",clientRect);
#endif
}

#endif // of try new code


void CDynControls::resetBaseUnits(CRect& arect)// right & bottom assumed filled
{
	CRect dbURect;
	if ( arect.right == 0 || arect.bottom == 0 ) return;// avoid divide by zero
	
	dbURect = arect;
	MapDialogRect( dbURect ); // now in delta from entry values
	baseunitX   = MulDiv( dbURect.right,  4, arect.right );  // calc the delta
	baseunitY   = MulDiv( dbURect.bottom, 8, arect.bottom ); // calc the delta

#ifdef _DEBUG
	double buX, buY;
	buX = (dbURect.right * 4) / arect.right;
	buY = (dbURect.bottom* 8) / arect.bottom;
#endif
}

// Added OnNotify to support editing and activating DDL items on Grid, POB - 5/6/2014
BOOL CDynControls::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
    NM_GRIDVIEW * nmgv = (NM_GRIDVIEW* )lParam;

    WORD wControlID = LOWORD(wParam);
	WORD wMessageID = nmgv->hdr.code;

    BOOL result;

    if (wControlID != 0 && m_isCntrlsReady)
	{
		if ( ! m_pdrawDlg->rebuilding && m_pdrawDlg->GetDocument()->pCurDev->devState != ds_Closing )
			result = m_pdrawDlg->ExecuteCID(wControlID, wMessageID, lParam);
	}

    // this function is called as expecetd
    return sdCDialog::OnNotify(wParam, lParam, pResult);
}