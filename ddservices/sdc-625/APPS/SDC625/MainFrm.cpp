/*********************************************************************************************
 *
 * $Workfile: MainFrm.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *      MainFrm.cpp : implementation of the CMainFrame class
 */

#pragma warning (disable : 4786)
#include "stdafx.h"
#include "SDC625.h"
//#include "SDC625Doc.h"
#include "MainFrm.h"
#include "SDC625_WindowView.h"
#include "DeviceDetails.h"

// stevev 5aug08 - added to do the execution from outside the object
#include "WaoDlg.h" 
#include "VariableDlg.h"

#include "RespCodeDevStat.h"

// Added By deepak
#include "Errlogview.h"
//#include "errorlog.h"
#include "logging.h"
// END MOD
#define RLSDBG

/* stevev 3/17/04 - modal override*/
#include <afxpriv.h>    /* Note: these definitions are allowed to change in future versions     */

#include "Command48_PropSheet.h"
#include "ddbCmdList.h"
#include "DDSelect.h"  //Added by ANOOP for DD Library
#include "Table_ChildFrm.h" // to allow initial selection

#ifdef _DEBUG
#include <time.h>
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#include <afx.h>

#ifndef STARTSEQ_LOG
//#define STARTSEQ_LOG 1
#endif
#endif

#ifdef _DEBUG /* and defined */
// turn off for now #define LOG_MSGS   1
 CDDIdeDoc *    gblDoc = NULL;
#else
#undef LOG_MSGS
#endif

/* Prasad start of code */
#define LIT_18_DOCUMENT_NAME "Lit-18.pdf"
#define NUMBER_OF_VIEW_ITEMS    8
// stevev added 3/17/04 - assumes a single mainframe per app
CRITICAL_SECTION  gcsDialog;  /* critical section for dialog multi modality */


/* stevev 06jul09 - There is only one mainframe, so there is only one hart beat light.
    So there is only one On/off timing and only one critical section  ***************/
bool LightIsRed = false;
int redTimeCnt  = 0;
int grnTimeCnt  = 0;
bool hasBeenActive = false;
CRITICAL_SECTION RedGrn_CriticalSection;
#define TIMERTICKS_ON   2
#define TIMERTICKS_OF   2
#define TIMERTICKS_WT   0

unsigned __updateSeconds = DEFAULTUPDTSECS;
/*************************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// statics - 26jun06 - protect the registry
hCmutex     CMainFrame::dlgRegistryMutex;

//IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)
IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

//BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
    //{{AFX_MSG_MAP(CMainFrame)
    ON_WM_CREATE()
    ON_WM_TIMER()
    ON_WM_DESTROY()
    ON_COMMAND(ID_DEVICE_HELP, OnDeviceHelp)
    ON_UPDATE_COMMAND_UI(ID_DEVICE_HELP, OnUpdateDeviceHelp)
    ON_COMMAND(ID_VIEW_ERRORLOG, OnViewErrorlog)
    ON_COMMAND(ID_VIEW_MORESTATUS, OnViewMorestatus)
    ON_UPDATE_COMMAND_UI(ID_VIEW_MORESTATUS, OnUpdateViewMorestatus)
    ON_COMMAND(ID_VIEW_COMM_LOG, OnViewCommLog)
    ON_UPDATE_COMMAND_UI(ID_VIEW_COMM_LOG, OnUpdateViewCommLog)
    ON_COMMAND(ID_DEV_HELP, OnDevHelp)
    ON_UPDATE_COMMAND_UI(ID_DEV_HELP, OnUpdateDevHelp)
    ON_COMMAND(ID_COMMIT, OnCommit)
    ON_UPDATE_COMMAND_UI(ID_COMMIT, OnUpdateCommitOrAbort) //Vibhor 230205: Modified
    ON_COMMAND(ID_ABORT, OnAbort)
    ON_COMMAND(ID_MFGRLIBRARY, OnMfgrlibrary) //Added by ANOOP for dd library
    ON_COMMAND_RANGE(1078, 32771, OnRange)  // Add by POB 14 Sep 2004
    ON_MESSAGE(WM_NOTIFY_RESPCODE,OnNotifyRespCode) //Added by Vibhor 040204
    ON_WM_ENABLE()
    ON_UPDATE_COMMAND_UI(ID_ABORT, OnUpdateCommitOrAbort) //Vibhor 230205: Modified
    ON_WM_CLOSE()
    //}}AFX_MSG_MAP 
    ON_MESSAGE(WM_DESTROY_CMD48DIALOG, OnDestroyCMD48Dialog)
    ON_MESSAGE(WM_SDC_SIZECHILD, SizeChildFrame)
    ON_MESSAGE(WM_SDC_ONWINDOW, OnWindow)
    ON_MESSAGE(WM_SDC_GENERATEDEVICE, OnGenDevice)
    ON_MESSAGE(WM_SDC_CLOSEDEVICE, OnCloseDevice)
    ON_MESSAGE(WM_LINK_OBJ, OnExeLink)
END_MESSAGE_MAP()

static UINT indicators[] =
{
    ID_SEPARATOR,           // status line indicator
    ID_SEPARATOR,       //Added by ANOOP
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
};

BOOL CMainFrame::OnIdle(LONG lCount)
{
TRACE(_T("OnIdle-Mainframe level\n"));
return FALSE;
}

#ifdef MEM_DEBUG
extern
int NewAllocHook(int nAllocType, void *pvData, 
      size_t nSize, int nBlockUse, long lRequest, 
      const unsigned char * szFileName, int nLine );
extern
int FileAllocHook(int nAllocType, void *pvData, 
      size_t nSize, int nBlockUse, long lRequest, 
      const unsigned char * szFileName, int nLine );
extern FILE *logFile;
#endif
/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
    m_pDoc = NULL;
    missingDevice = 0;
    m_icnt = 0;
    ptrCmdDlg=NULL;
//  m_bRightPaneSplitterCreated=FALSE; //Added by ANOOP - POB
    m_MainFrameState = FS_ISIDLE;      //Added by stevev
    bCMD48DlgExtendedTabCreatedOnce=FALSE;  //Added by ANOOP

    /* stevev 3/17/04 */
     InitializeCriticalSection(&gcsDialog);
     m_phMyWndDisable=NULL;
     m_pdrawMain = NULL;
     ticksTillUpdate = 0;
     pErrorFrame = NULL;

     hCevent::setUIthreadID( GetCurrentThreadId() );
#ifdef STARTSEQ_LOG
    LOGIT(CLOG_LOG,"CMainFrame instantiates\n");
    TRACE(_T("CMainFrame instantiates\n"));
#endif  
    m_pImageList_Enabled = NULL;
    m_pImageList_Disabled= NULL;
//17mar11   pTableChildFrm       = NULL;
    ticksTillUpdate   =0; 
    subpubChngCounter =0;
    dlgRegistry.clear();
    treeRedrawing = true;

    InitializeCriticalSection(&RedGrn_CriticalSection);
    isBusy = false;         // stevev 07oct09
// temporary 26may11
pTableChildFrm = NULL;
// end temporary
}

CMainFrame::~CMainFrame()
{
    /* Vibhor 130603 : Start of Code*/
    m_pImageList_Enabled->DeleteImageList();
    m_pImageList_Disabled->DeleteImageList();
    delete m_pImageList_Disabled;
    delete m_pImageList_Enabled;
    /* Vibhor 130603 : Start of Code*/
    if(NULL != ptrCmdDlg) //Added by ANOOP for DD Library
    {
//      delete ptrCmdDlg;
//      ptrCmdDlg=NULL; 
    }
    
    ResetWindowMenu();
    DeleteCriticalSection(&RedGrn_CriticalSection);
    
    m_wndToolBar.~CToolBar();
#ifdef STARTSEQ_LOG
    LOGIT(CLOG_LOG,"CMainFrame deleted\n");
    TRACE(_T("CMainFrame deleted\n"));
#endif
    
#ifdef MEM_DEBUG
    
    if ( logFile != NULL ) { fclose( logFile ); logFile = NULL; }

#endif
    // tell the main thread
    ::PostMessage(NULL,WM_QUIT,0,0);
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::OnCreate Entry\n"));
#endif
    //if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1) 
        return -1;

    /* removed from the window view and placed here in the principle owner of the UI thread
        stevev - 27sep11 ***/
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);// same as CoInitialize(NULL);

//Vibhor 230205: Restored Back as fixed the tooltip problem 
    if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
        | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
        !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
    {
        TRACE(_T("CMainFrame::OnCreate - Exit Failed to create toolbar\n"));
        return -1;      // fail to create
    }

/*Vibhor 130603 : Start of Code*/
// This takes care of properly displaying "256 color" bitmaps on the buttons on the Toolbar
    m_pImageList_Enabled = new CImageList;
    m_pImageList_Enabled ->Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    m_pImageList_Disabled = new CImageList;
    m_pImageList_Disabled ->Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    
    CBitmap bmp_enabled, bmp_disabled;

    bmp_enabled.LoadBitmap(IDB_TOOLBAR_ENABLED);
    bmp_disabled.LoadBitmap(IDB_TOOLBAR_DISABLED);


/*Vibhor 160603 : Start of Code*/
    m_pImageList_Enabled ->Add(&bmp_enabled, RGB(255,0,255));//Vibhor 160603 : Changed the Transparent color 
    m_pImageList_Disabled ->Add(&bmp_disabled, RGB(255,0,255));// to RGB (255, 0 ,255)
/*Vibhor 160603 : End of Code*/
    
    m_pImageList_Enabled ->Add(&bmp_enabled, GetSysColor(COLOR_WINDOW));
    m_pImageList_Disabled ->Add(&bmp_disabled, GetSysColor(COLOR_WINDOW));
    
    (m_wndToolBar.GetToolBarCtrl()).SetImageList (m_pImageList_Enabled);
    (m_wndToolBar.GetToolBarCtrl()).SetDisabledImageList (m_pImageList_Disabled);

/*Vibhor 130603 : End of Code*/

    if (!m_wndStatusBar.Create(this) ||
        !m_wndStatusBar.SetIndicators(indicators,
          sizeof(indicators)/sizeof(UINT)))
    {
        TRACE(_T("Failed to create status bar\n"));
        return -1;      // fail to create
    }

/*<START>Added by ANOOP for Transmit light LED display  */  
    m_wndStatusBar.SetPaneInfo(1,ID_SEPARATOR,SBPS_NORMAL,200);
    CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();

    CDDIdeApp* pApp = (CDDIdeApp*)AfxGetApp();
    HICON m_icon1= pApp->LoadIcon(IDI_GREEN);// stevev changed from IDI_RED);to grey 02/16/04 /* VMKP Changed from IDI_GREEN to IDI_RED on 160204*/
    barCtrl.SetIcon(1, m_icon1);
    barCtrl.SetText(_T("HART Beat"),1,0);
    // added by stevev to initialize green (no transmission)

/*<END>Added by ANOOP for Transmit light LED display    */  

    // NOTE: Delete these three lines if you don't want the toolbar to be dockable
    m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
    EnableDocking(CBRS_ALIGN_ANY);
    DockControlBar(&m_wndToolBar);

    /*
     * Start a timer to update the menu items once in 2 seconds.
     */
#ifdef _DEBUG
    {
        struct _timeb dbgTime;
        _ftime( &(dbgTime) );
//      DEBUGLOG(CLOG_LOG,"%I64d.%03hd >>Timer: SET and enbled.\n",
//                                                          dbgTime.time,dbgTime.millitm);
    }
#endif
    // was int result = SetTimer(VIEW_TIMER, ONE_SECOND * UPDATESECONDS, NULL);
    int result      = SetTimer(VIEW_TIMER, GRANULARITY, NULL);

    ticksTillUpdate = (ONE_SECOND * UPDATESECONDS)/GRANULARITY;
    LOGIT(CLOG_LOG,"Background Timer set to %d w/ ticks = %d.\n",GRANULARITY, ticksTillUpdate);
    if (!result)
      MessageBox(M_UI_NO_TIMER);

#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::OnCreate - Exit\n"));
#endif
#ifdef MEM_DEBUG
// use the new one   _CrtSetAllocHook( NewAllocHook );// mem tracking
    logFile = fopen( "MEM-LOG.TXT", "w" );
    if ( logFile == NULL )
        return 0;
   fprintf( logFile, "RqstNumb oper reqSize  -thread- Block Line    File Name\n");
    _CrtSetAllocHook( FileAllocHook );// mem tracking to file
#endif
    return 0;
}

/*<START>Added by ANOOP for transmit light LED display  */
void CMainFrame::SetTransmitterLight(bool bColorRed)
{
    CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
    HICON m_icon;
    CDDIdeApp* pApp = (CDDIdeApp*)AfxGetApp();
    
    if(bColorRed == true)
        m_icon = pApp->LoadIcon(IDI_RED);
    else
        m_icon= pApp->LoadIcon(IDI_GREEN);
    
    
    barCtrl.SetIcon(1, m_icon);
//  barCtrl.SetText("HART Beat",1,0);
    // stevev 12dec05 - added   
    barCtrl.UpdateWindow();
    systemSleep(1);

}
/*<END>Added by ANOOP for transmit light LED display*/

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
    //if( !CFrameWnd::PreCreateWindow(cs) )
    if( !CMDIFrameWnd::PreCreateWindow(cs) )
        return FALSE;

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
    //CFrameWnd::AssertValid();
    CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
    //CFrameWnd::Dump(dc);
    CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

// code for BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
// removed  080808 8aug08 - see earlier versions for contents

// global helper - returns list size
int addUnique(vector <hCitemBase*>& pV, hCitemBase* pIB)
{
    itemID_t IBid= pIB->getID();
    vector <hCitemBase*>::iterator ppIB;
    for (ppIB = pV.begin(); ppIB != pV.end(); ++ppIB)
    {
        if ((*ppIB)->getID() == IBid)
        {// if in list, get out
            return pV.size();
        }
    }
    pV.push_back(pIB);// if we got here it is not already in the list
    return pV.size();
}

#ifdef MEM_DEBUG
CMemoryState oldMemState, newMemState, diffMemState;
int entryCnt = InvalidItemID;
#endif
/* stevev 09dec04 - changed algorithm
   we now get a tick each GRANULARITY mS
   The update functions ( everything we've been doing )
   are performed at some multiple of this tick
   the publish functions are performed at some multiple of this tick as well
 */
void CMainFrame::OnTimer(UINT nIDEvent) 
{
#ifdef _DEBUG
    bool flag = false;
    struct _timeb dbgTime;
    _ftime( &(dbgTime) );
//  DEBUGLOG(CLOG_LOG,"%I64d.%03hd  >>Timer: Entry red = %d; grn = %d.\n",
//      dbgTime.time, dbgTime.millitm,  redTimeCnt,grnTimeCnt);
    
#endif
//  MessageBeep(0xFFFFFFFF);   // Beep
    vector <hCitemBase*> pVariablesToRead;
    BOOL bSendCMD48 = FALSE;    //For getting the latest device status in case no commands are sent
//  hCVar*      pVar= NULL;

//  if (m_pDoc == NULL) // POB - 2 Aug 2004
    if (GetDocument() == FALSE)
    {
        LOGIT(CLOG_LOG,"T<T< -- OnTimer's GetDocument failed.\n");
        return;
    }
    
#ifdef _DEBUG
    if ( gblDoc != m_pDoc )
    {
        TRACE(_T("******** Timer detected a mismatch of Document pointers.\n"));
    }
#endif

    /* new way to handle timer 09dec04 */
//  was int result = SetTimer(VIEW_TIMER, ONE_SECOND * UPDATESECONDS, NULL);
//  now int result = SetTimer(VIEW_TIMER, GRANULARITY, NULL);
    if ( m_pDoc->pCurDev != NULL && m_pDoc->appCommEnabled() )
    {
        m_pDoc->pCurDev->subscriptionHandler.publish(UPDATEPERIOD);
        // now done in publish under its mutex
        //if (m_pDoc->pCurDev->subscriptionHandler.isChanged(subpubChngCounter))
        //{
        //  m_pDoc->pCurDev->subscriptionHandler.calcTimerLen(UPDATEPERIOD,GRANULARITY);
        //}
#ifdef NOMORE_DEBUG
        LOGIT(CLOG_LOG," publish ");
#endif
    }
    /** stevev 06jul09 - change the way the hart beat works so it is timer driven. **/
    if ( LightIsRed )
    {
        if ( redTimeCnt--  <= 0 )// time to toggle - always
        {
            EnterCriticalSection(&RedGrn_CriticalSection); 
            SetTransmitterLight(false); // not red, so Green
            LightIsRed    = false;
            hasBeenActive = false;// we don't care if it was active while we were red...
            grnTimeCnt    = TIMERTICKS_OF;
            redTimeCnt    = 0;
            LeaveCriticalSection(&RedGrn_CriticalSection);
//          DEBUGLOG(CLOG_LOG,"%I64d.%03hd >>Timer: BEAT to Grn.\n",
//                                                          dbgTime.time,dbgTime.millitm);
        }
        // else - we're still red, just keep going
#ifdef _DEBUG
        else
        {
//          DEBUGLOG(CLOG_LOG,"%I64d.%03hd  >>Timer: stays red.(%d)\n",
//                                                  dbgTime.time,dbgTime.millitm,redTimeCnt);
        }
#endif
    }
    else // ! LightIsRed, so LightIsGreen
    {
        if ( grnTimeCnt--  <= 0 )// may be time to toggle
        {
            EnterCriticalSection(&RedGrn_CriticalSection); 
            if (hasBeenActive)
            {
#ifdef _DEBUG
                flag = true;
#endif
                SetTransmitterLight(true);//red
                LightIsRed    = true;
                hasBeenActive = false;
                grnTimeCnt    = 0;
                redTimeCnt    = TIMERTICKS_ON;
            }
            else // no activity, leave it green
            {
#ifdef _DEBUG
                flag = false;
#endif
                grnTimeCnt    = TIMERTICKS_WT;//leave it off till activity occurs
                redTimeCnt    = 0;        // making grn 0 will turn it on as soon as it occurs
            }
            LeaveCriticalSection(&RedGrn_CriticalSection);
//          DEBUGLOG(CLOG_LOG,"%I64d.%03hd%s>>Timer: %s\n",
//              dbgTime.time,dbgTime.millitm, (flag)?" ":"  ",
//                  (flag)?" BEAT to Red.":"stays grn(No Activity)." );
        }
        // else - we're still green, just keep going
#ifdef _DEBUG
        else
        {
//          DEBUGLOG(CLOG_LOG,"%I64d.%03hd  >>Timer: stays grn.(%d)\n",
//                                                  dbgTime.time,dbgTime.millitm,grnTimeCnt);
        }
#endif
    }// endif light-handling

    if ( --ticksTillUpdate <= 0 )
    {
        ticksTillUpdate = UPDATEPERIOD/GRANULARITY;
        // fall through to execute
    }
    else
    {
//      DEBUGLOG(CLOG_LOG,"%I64d.%03hd  >>Timer: Exit.\n",dbgTime.time,dbgTime.millitm);
        return; // it's not an update cycle yet
    }
    /* end 09dec04 */


/* now use - new button/menu        
    if ( m_pDoc->pCurDev == NULL )
    {
        missingDevice++;
        if (missingDevice > 2)
        {
            TRACE(_T(">>> Main frame sending Generate device.\n"));
            m_pDoc->genDev();
            missingDevice = 0;
        }
    }
    else
    {
        missingDevice = 0;
    }
*/
/* stevev - do more qualification
    if ( m_pDoc->isClosing)
    {
        KillTimer(VIEW_TIMER);
        return;
    }
*/
    DEBUGLOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"T>T>");

    bool needsStale;
    if (m_pDoc->appCommEnabled()   &&   
        m_pDoc->pCurDev != NULL    && 
        m_pDoc->pCurDev->pCmdDispatch != NULL && 
        m_pDoc->pCurDev->pCmdDispatch->isReady()    )
    {

    //this used to be the table's staleDynamics area...now done through the CDrawMain-17mar11 
    
/*Vibhor 180304: Start of Code*/
/*This code is for ensuring Commmand 48 is sent whenver Device Condition 
window is open irrespective of whether MoreStatus bit is set or not*/
        varPtrList_t itemList;
        if(NULL != m_pDoc->ptrCMD48Dlg)
        {           
            HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Condition"));
            if(NULL != hwndCMD48Window)  
            {//Just scan thru the command 48 reply and push all the dynamics on Read vector
                
                CCmdList *ptrCmndList = (CCmdList *)m_pDoc->pCurDev->getListPtr(iT_Command);
                if (ptrCmndList)
                {
                    hCcommand *pCommand = ptrCmndList->getCmdByNumber(48);
                    if (pCommand)
                    {
                        hCtransaction *pTrans = pCommand->getTransactionByNumber(0);
                        if (pTrans)
                        {
                            if (pTrans->getReplyList(itemList) == SUCCESS)
                            {
                                if (itemList.size() >0)
                                {
                                    LOGIT(CLOG_LOG,"Command 48 item being marked STALE.\n");
                                    for( unsigned nCnt=2;nCnt<itemList.size();nCnt++)
                                    {
                                        hCVar *pVar = itemList[nCnt];
                                            if(pVar)
                                        {
                                            needsStale = pVar->IsValid() &&   pVar->IsDynamic() &&
                                            (pVar->getDataState() != IDS_NEEDS_WRITE &&
                                             (! pVar->isChanged() ));/*This is not an unconfirmed write*/
                                             //stevev - remove for now 
                                             //(appears to bypass many of the filter conditions above)|| (pVar->m_pddbVar->getDataQuality() != DA_HAVE_DATA)) /* VMKP added on 290104*/
                                             
                                             /* don't try and read write-only variables*/
                                            needsStale = needsStale && (! pVar->IsWriteOnly());
                                            if ( needsStale )  
                                            {
                                                // 01dec06::pVariablesToRead.push_back((hCitemBase *)pVar);                                             
                                                addUnique(pVariablesToRead, (hCitemBase *)pVar  );                                              
                                            }
                                        }
                                    }                                                                       
                                }
                            }
                        }
                    }
                }
            }
        }//endif NULL != m_pDoc->ptrCMD48Dlg
    

/*Vibhor 180304: End of Code*/

/* Anoop Added on 24-11-03 */
/* To send a dummy command to a device when Non dynamic variable menus are selected though
   there is no variable to update in the menu.  This will update the Device status window
    and its menu item */
/* modified 12/10/03 by stevev
   Alternates command 0 and dynamic read */
        if (pVariablesToRead.size() == 0)
        {
            m_icnt++;
            if (m_icnt == CYCLES2FORCEREAD) 
            {
//              DEBUGLOG(CLOG_LOG,"%I64d.%03hd >>Timer: Cnt==2.\n",
//                                                              dbgTime.time,dbgTime.millitm);
#ifdef _DEBUG
                assert(this->m_pDoc->iNumberOfDynamicVars == 0);
#endif
                for(int nCntr=0;nCntr <= this->m_pDoc->iNumberOfDynamicVars;nCntr++) 
                {
                    itemID_t  dynVarItem= m_pDoc->DynamicVariableItemIds[nCntr];
                    if(dynVarItem)
                    {
                        hCitemBase* pIB;
                        if (m_pDoc->pCurDev->getItemBySymNumber(dynVarItem, &pIB) == SUCCESS)
                        {
                            hCVar* pVar = (hCVar *)pIB;
                            if ( pVar->IsVariable() )
                            {
                                //01dec06::pVariablesToRead.push_back((hCitemBase *)pVar); 
                                addUnique(pVariablesToRead, (hCitemBase *)pVar  );
                            }                       
                        }// else an error, we can discard here          
                    }//else it's invalid, skip                      
                }// next var
            }
            else
            if (m_icnt >= (2*CYCLES2FORCEREAD))
            {
//              DEBUGLOG(CLOG_LOG,"%I64d.%03hd >>Timer: Cnt==4.\n",
//                                                              dbgTime.time,dbgTime.millitm);
                m_icnt = 0;
                //force command zero
                CCmdList*  pCmdList = (CCmdList*) m_pDoc->pCurDev->getListPtr(iT_Command);
                hCcommand* pCmd = NULL;
                if (pCmdList)
                {
                    pCmd = pCmdList->getCmdByNumber(0);
                }
                if (pCmd)
                {
                    varPtrList_t aVarVector;
                    hIndexUse_t  anIndex;// generated clear
                    if (SUCCESS==pCmd->getReplyData4Trans(0, anIndex, aVarVector) && 
                        aVarVector.size() > 0)
                    {
                        for(varPtrList_t::iterator pV=aVarVector.begin();pV<aVarVector.end();pV++)
                        {// ptr2aPtr2a_hCVar
                            if ( (*pV)->IsVariable() )
                            {
                                //01dec06::pVariablesToRead.push_back((hCitemBase *)(*pV)); 
                                addUnique(pVariablesToRead,  (hCitemBase *)(*pV) );
                            }
                        }//next
                    }
                }
                //else discard error
            }
            //else - haven't reached count yet
        }
        else    // we have something to read for command 48
        {
            m_icnt = 0;
//          DEBUGLOG(CLOG_LOG,"%I64d.%03hd >>Timer: Cnt>>0.\n",dbgTime.time,dbgTime.millitm);
        }

        // Bulk read
#ifdef _DEBUG
        if (pVariablesToRead.size())
            LOGIT(CLOG_LOG,"Marking %d variables STALE",pVariablesToRead.size());
#endif
        m_pDoc->ReadVariables(pVariablesToRead);// will handle an empty list
#ifdef _DEBUG
        if (pVariablesToRead.size())
            LOGIT(CLOG_LOG,".\n");
#endif
        if (m_pdrawMain)   
            m_pdrawMain->staleDynamics();//does windows for food            <the window update>
    } // otherwise just skip any action

    //  CFrameWnd::OnTimer(nIDEvent);
/**** stevev removed 16mar11 to use OnWindow as standard table entry    
/x*<START>Added by ANOOP    *x/
    if (pTableChildFrm)  // POB - 2 Aug 2004
    {
        if(pTableChildFrm->m_bRightPaneSplitterCreated) // POB - Splitter is now in child window, 2 Aug 2004
        {
            CView *pView = static_cast<CView*> (pTableChildFrm->m_wndSplitter_RightPane.GetPane (1,0));
            
            if( NULL != pView )
            {
                CView *pView = static_cast<CView*> (pTableChildFrm->m_wndSplitter_RightPane.GetPane (1,0));
                
                if( NULL != pView )
                {
                    CErrLogView *pErrorLogView=(CErrLogView *)pView;
                    pErrorLogView->DisplayLogMessage(); 
                }
            }
        }
    }
/x*<END>Added by ANOOP      *x/
*** end remove ***/

    DEBUGLOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"T<Timer<\n");

}

void CMainFrame::OnDestroy() 
{   
    KillTimer(VIEW_TIMER);/* changed oder of execution 26jun06 */
    //CFrameWnd::OnDestroy();
    CMDIFrameWnd::OnDestroy();
}

/* stevev - 2/17/04 - added config changed handler */
/* stevev - 4/26/04 - returns APP_CMD_RESPCODE_ERR on unexpected*/
// called by notifyRCDScmd48() when the config changed bit becomes set
RETURNCODE CMainFrame::ConfigChanged(void) 
{
    RETURNCODE rc = FAILURE;

    if (m_pDoc != NULL  && m_pDoc->pCurDev != NULL)
    {
        if (m_pDoc->pCurDev->whatCompatability() != dm_Standard )// IS lenient mode
        {
            return SUCCESS;// disregard ccb when 275
        }
    }


    // discard if we are expecting it, else handle it
    if (m_MainFrameState == FS_ISIDLE)
    {// invalidate all data and repaint entire screen
        // we may be the first one in...
        if (m_pDoc == NULL)
            if (GetDocument() == FALSE)
            {               
                LOGIF(LOGP_START_STOP)(CLOG_LOG,
                    "< MnFrm::ConfigChanged -  did not get a document.\n");
                return rc;
            }

        if (m_pDoc != NULL  &&  
            m_pDoc->pCurDev != NULL && 
            m_pDoc->pCurDev->pCmdDispatch != NULL &&
            m_pDoc->pCurDev->pCmdDispatch->isReady()   )
        {
            CVarList* pVars= (CVarList*)m_pDoc->pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));
            if (pVars != NULL)
            {
                /* stevev 06apr10 - bug 3034 */
                m_pDoc->pCommInfc->disableAppCommRequests();// turn off weighing and sending
                DEBUGLOG(CLOG_LOG,"Mainfrm Config changed disables appComm\n");
				dState_t orig_devState = m_pDoc->pCurDev->devState;
				m_pDoc->pCurDev->devState = ds_OnIniting;
                Sleep(100);                                 // 
            
                LOGIT(UI_LOG|CLOG_LOG,"Configuration changed unexpectedly.");
                pVars->markAllItemState( IDS_INVALID ); 
                // now repaint
                m_MainFrameState = FS_UFIXING;// stevev 4/26/04 record what is going on
                fixConfigChangeBit();// reset it if possible
                /* stevev 06apr10 - bug 3034 - read critical & enable App Comm */
                m_pDoc->pCurDev->readCriticalparams();
				
				m_pDoc->pCurDev->devState = orig_devState;

                m_pDoc->pCommInfc->enableAppCommRequests ();
                DEBUGLOG(CLOG_LOG,"Mainfrm Config changed enables appComm\n");

                rc = APP_CMD_RESPCODE_ERR;
            }
            // else - we are listless - just exit
        }
        //else - not much we can do about it, leave
        return rc;
    }
    else
    if (m_MainFrameState == FS_ISCOMMITTING)
    {// we wrote something that caused this, just reset it if possible
        m_MainFrameState = FS_CFIXING;
        fixConfigChangeBit();// reset it if possible
        rc = SUCCESS;
    }
    else // discard and return ( already xFIXING )
    {
        rc = SUCCESS; // we expected a change bit, just continue
    }
    return rc;
}

/* stevev - 4/26/04 - added config cleared handler */
// called by notifyRCDScmd48() when the config changed bit becomes clear
// always successful
RETURNCODE CMainFrame::ConfigUNChanged(void) 
{
#ifdef _DEBUG
    if (m_MainFrameState == FS_ISIDLE)
    {// invalidate all data and repaint entire screen
        LOGIT(CLOG_LOG,"Configuration cleared while IDLE!.\n");
    }
    else
    if (m_MainFrameState == FS_ISCOMMITTING)
    {// invalidate all data and repaint entire screen
        LOGIT(CLOG_LOG,"Configuration cleared while COMMITTING!.\n");
    }
    //else -- as expected - xFIXING
#endif
    m_MainFrameState = FS_ISIDLE;
   /* stevev 01mar05 - somehow this got ommitted, changed now */
   ::PostMessage(GetSafeHwnd(), WM_SDC_STRUCTURE_CHANGED, 0, NULL);
   TRACE(_T("  POSTING REPAINT from ConfigUNChanged\n"));
    return SUCCESS;
}

void CMainFrame::fixConfigChangeBit(void)
{
    if (m_pDoc != NULL  &&  m_pDoc->pCurDev != NULL)
    {
        if ( m_pDoc->m_copyOfCmdLine.cl_HstAddr == 1 )// primary master owns cfgbit
        {
            CCmdList* pCmds = (CCmdList*)m_pDoc->pCurDev->getListPtr(CitemType(COMMAND_ITYPE));
            if (pCmds != NULL)
            {   
                hCcommand* pCmd38 = pCmds->getCmdByNumber(38);
                // removed 06apr10 if ( pCmd38 != NULL && m_pDoc->m_autoUpdateDisabled == false )
                if ( pCmd38 != NULL && m_pDoc->pCurDev->devIsExecuting == false )
                {// we have the command and we are not in a method
                    m_pDoc->pCurDev->sendMethodCmd(38);
                    LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Sending command 38.\n");    
                }
                else    // - capability not supported - do nothing
                {
                    if (pCmd38 == NULL)
                    {
                        LOGIT(CLOG_LOG|UI_LOG,"Device does NOT support command "
                                                                "38 (clear Cnfig Changed).\n"); 
                    }
                    else
                    {
                        LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Device is executing. Cmd 38 NOT sent.\n");  
                    }
                }
            }
            // else - we are listless - just exit
        }// else - secondary master must merely follow the crowd...
    }
    //else - not much we can do about it, leave
}
    


/* stevev - 2/17/04 - end */

/* stevev - 2/17/04 - end */

/*************************************************************************************************
 *
 *   $History: MainFrm.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */

/* Prasad start of code */
void CMainFrame::OnDeviceHelp() 
{
//prashant
    TCHAR pchCurrentDirectory[1000];
    _tcscpy(pchCurrentDirectory,M_UI_MT_STR);// was "\0");
    
    _tcscat(pchCurrentDirectory,m_szLit18File);

    SHELLEXECUTEINFO shellExecInfo;
    const TCHAR *pchVerb = _T("Open");

    shellExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    shellExecInfo.lpVerb = pchVerb;
    shellExecInfo.lpFile = pchCurrentDirectory;
    shellExecInfo.lpParameters = NULL;
    shellExecInfo.lpDirectory = NULL;
    shellExecInfo.fMask = SEE_MASK_CONNECTNETDRV | SEE_MASK_DOENVSUBST;
    shellExecInfo.nShow = SW_SHOW;

    BOOL bStatus = ShellExecuteEx(&shellExecInfo);
    if (FALSE == bStatus)
    {
        LOGIT(UI_LOG,"Device help document document %s not found.",m_szLit18File);
    }

}
/* End of code */

#ifdef _DEBUG
bool glblStructureDisabled = false;
int  delayCnt = 0;
#endif

// temp
int lstSt;


/* Prasad start of code 05/19/03 */
/* stevev 24may11 - major cleanup when adding element delete message handler. negligible
 *                          functional change.  See earlier versions for previous contents.  
 */
LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
    LONG idlCnt = 0;

    if (GetDocument() == FALSE    || 
        m_pDoc->pCurDev == NULL)
    {
        return CMDIFrameWnd::WindowProc(message, wParam, lParam);
    }

    if ( !IsWindowEnabled() )// we have to handle some of 'em even when disabled.
    {//#define WM_ENTERIDLE                    0x0121
        if ( message == WM_ENTERIDLE )
        {
            if (lstSt)
            {// we were enabled, now we're not
                    TRACE(_T("mainframeMsg 0x%04x (ENTERIDLE) while %s  (in 0x%x)\n"),
                        message,(IsWindowEnabled())?"Enabled":"Disabled",GetCurrentThreadId());
                lstSt = FALSE;
            }       
            
            m_pDoc->DoIdle(idlCnt);
        }
    }

    if ( message == WM_ENABLE_WIN  )    /* added 27jun06 to avoid a deadlock */
    {
        HWND locWnd = (HWND)lParam;
        if (wParam)
        {           TRACE(_T(" Enable  dialog with MWND = %x\n"),locWnd);
            ::EnableWindow(locWnd,TRUE);// OS uses caps, turn ot off
                    TRACE(_T("Activate dialog with MWND = %x\n"),locWnd);
            ::SetActiveWindow(locWnd);
                    TRACE(_T(" Exit    dialog with MWND = %x\n"),locWnd);
        }
        else
        {           TRACE(_T("Disable dialog with MWND = %x\n"),locWnd);
            ::EnableWindow(locWnd,FALSE);// OS uses caps, turn ot off
        }
    }


    if ( message == WM_DO_EXECUTE && lParam != NULL )// we need to execute(x422) and we know who
    {//WPARAM wParam, LPARAM lParam
        hCitemBase* pIB = (hCitemBase*)lParam;
        if (pIB->IsVariable())
        {
            TRACE("MainFrame's message handler to Execute a variable.\n");
            doExecuteVariable((hCVar*)pIB, wParam);
			return 0;// we handled it, there is no return, 
        }
        else
        if (pIB->getIType() == iT_Method)
        {
            TRACE("MainFrame's message handler to Execute a method.\n");
        }
        else
        if (pIB->getIType() == iT_Menu)
        {
            TRACE("MainFrame's message handler to activate a menu.\n");
        }
        else
        {// unknown type on a root menu, fall through
        }
    }

    if ( message != WM_TIMER )
    {
        #ifdef _DEBUG
        if (message == WM_ENABLE)
        {           
            TRACE(_T("mainframeMsg 0x%04x (WM_ENABLE)[0x%x]<%d> while %s  (in 0x%x)\n"),message
            ,wParam,lParam,(IsWindowEnabled())?L"Enabled":L"Disabled",GetCurrentThreadId());
        }
        #endif

        if ((lstSt==0) != (0==IsWindowEnabled()) )
        {// we were disabled And we are still disabled
                TRACE(_T("mainframeMsg 0x%04x (unknown) while %s  (in 0x%x)\n"),
                message,(IsWindowEnabled())?L"Enabled":L"Disabled",GetCurrentThreadId());

            lstSt = IsWindowEnabled();// always false
        }
    }
    else
    {
        //TRACE(_T("Got a Timer message.\n"));
    }


    if ((message >= WM_USER) && (message < WM_APP))// between 0x400 and 0x8000 PAW 03/03/09
    {
        switch(message)
        {
        case WM_SDC_VAR_CHANGED:    /* this should no longer be sent - handle like value */
        case WM_SDC_VAR_CHANGEDVALUE:
        {
            CMDIChildWnd* pChild = MDIGetActive();
            
            // The view is in the child window, if it does not exit, 
            // do not update - POB, 3 Aug 2004
            if (pChild == NULL)
                break;

            /*
             * Cycle through the child list in the entire DD and update 
             * only the changed variable
             */
            
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:MainFrame. WndowProc - WindowItem ");
if ( message ==  WM_SDC_VAR_CHANGED )
LOGIT(CLOG_LOG,"%s 0x%04x\n","Var-Changed",wParam);
if ( message ==   WM_SDC_VAR_CHANGEDVALUE )
LOGIT(CLOG_LOG,"%s 0x%04x\n","Var-Changed-Value",wParam);

#endif
            if ( ! setDrawState( true ) ) //drawing on (in case it's off)
            {// entry with drawing off is an issue
                LOGIT(CLOG_LOG,L"MainFrame windowproc entry while NOT drawing.\n");
            }

            updateAll(UPDATE_ONE_VALUE, wParam);//tell m_pDrawMain & All Views

            updateDialogs(message, wParam, lParam);
/* 06dec05 stevev...the list/window will do this - it ain't the mainframe's job 
16mar11 stevev...removed commented code */
        }
        break;

        case WM_SDC_STRUCTURE_CHANGED:
        {
#ifdef LOG_MSGS
            LOGIT(CLOG_LOG,"MSG:MainFrame. WindowProc - Struct Changed\n");
#endif
#ifdef _DEBUG
            if (glblStructureDisabled)
            {
                delayCnt++;
                if (delayCnt < 30)
                    break;
                else
                {
                    delayCnt = 0;
                    glblStructureDisabled = false;
                }
            }
#endif

            updateDialogs(message, wParam, lParam);

            setDrawState( false ); // turn off drawing  

            /* Moved code for remembering the selection menu to CDDIdeTreeView::OnUpdate(), POB - 5/21/2014 */
            /* copied from below the else 18jan05 */
            if( wParam == ROOT_MENU)
            {   
                systemSleep(10);// to allow any windows messages to be processed before deletion
#ifdef _DEBUG
                LOGIT(CLOG_LOG," About to regenerate the ROOT_MENU.\n");
#endif

                m_pDoc->UpdateAllViews( NULL, UPDATE_REMOVE_DEVICE, NULL );  // CDDLMain is no longer used as tree control root menu, POB - 5/18/2014

                m_pDoc->UpdateAllViews( NULL, UPDATE_TREE_VIEW, (CObject*)ROOT_MENU  ); // stevev 31jul07
                /* try to return the user to where he was before we started this - may not
                   always work, he could have been in a menu that no longer exists, he'll
                   be in the root in that case*/
/**stevev 17mar11
** try to move to childframe **/
            }
            else
            {
                updateAll(UPDATE_STRUCTURE, wParam);// updates DrawMain and all views
            }
                       
            /* Moved code for remembering the selection menu to CDDIdeTreeView::OnUpdate(), POB - 5/21/2014 */
            setDrawState( true ); // turn drawing back on
/* stevev 16mar11 - removed code and #ifdef & #else */
        }
        break;

        case WM_SDC_ELEMENT_DELETED:
        {

            LOGIF(LOGP_NOTIFY)(CLOG_LOG,"MSG: Element Deleted:0x%04x\n",(unsigned)wParam);
// 21mar13 - causing a crash on third list menu entered using window/no table
            /*
// temporary 26may11 - debugging
CDDLBase* pPreDelete = pTableChildFrm->getCurrentSelection();
// end temporary
            */
            updateAll(UPDATE_DELETE_ITEM, wParam);//tell m_pDrawMain & All Views

            updateDialogs(message, wParam, lParam);
// 21mar13 - causing a crash on third list menu entered using window/no table
            /*
// temporary 26may11 - debugging
CDDLBase* pPostDelete = pTableChildFrm->getCurrentSelection();
// end temporary
            */
            LOGIF(LOGP_NOTIFY)(CLOG_LOG,"MSG: Element Deleted:0x%04x exit\n",(unsigned)wParam);
        }
        break;


/* VMKP added on 160204 */
        case WM_SDC_GLOW_GREEN:
        {
            /**** stevev 06jul09 - changed beat to be timer driven ******
            CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
            HICON m_icon;
            CDDIdeApp* pApp = (CDDIdeApp*)AfxGetApp();
            
            m_icon = pApp->LoadIcon(IDI_GREEN);
            
            barCtrl.SetIcon(1, m_icon);
            barCtrl.UpdateWindow();
            systemSleep(1);
            ********* end beat change ***********************************/
        }
        break;

        case WM_SDC_GLOW_RED:// sent on every command SEND
        {
            /**** stevev 06jul09 - changed beat to be timer driven ******
            CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
            HICON m_icon;
            CDDIdeApp* pApp = (CDDIdeApp*)AfxGetApp();
            
            m_icon= pApp->LoadIcon(IDI_RED);// HART beat light PAW 03/03/09
            
            barCtrl.SetIcon(1, m_icon);
            barCtrl.UpdateWindow();
            ****************** end beat change delete ****************
            start change insert *************************************/
            // we just tell if something has gone out....
            EnterCriticalSection(&RedGrn_CriticalSection); 
            hasBeenActive = true;
            LeaveCriticalSection(&RedGrn_CriticalSection);

            systemSleep(1);
        }
        break;
/* END of VMKP added on 160204 */
            /* stevev 28sep06*/
        case WM_SDC_WAITCLOSEDLG:
        {/* stevev 15apr10 - bug 2493 fixed here */
            /*Afx*/MessageBox(M_UI_COMM_ERROR);
        }/* stevev 28sep06 -- end*/
        break;

        }// endswitch
    
    }

    LRESULT rty = CMDIFrameWnd::WindowProc(message, wParam, lParam);
    if (message == WM_ENABLE)
    {
        TRACE(_T("mainframeMsg Exit #0x%04x (WM_ENABLE)[0x%x]<%d> while %s  (in 0x%x)\n"),
        message,wParam,lParam,(IsWindowEnabled())?L"Enabled":L"Disabled",GetCurrentThreadId());
    }
    return rty;
}


BOOL CMainFrame::GetDocument()
{
    BOOL r= FALSE;
/*  there is only one document that cannot change thru execution */
    CDDLBase* pItem = NULL;
    CDDLBase* pBase = NULL;
    if (! ::IsWindow(m_hWnd))
    {
        r = FALSE;
    }
    else
    if ( m_pDoc != NULL && m_pDoc->IsKindOf( RUNTIME_CLASS( CDDIdeDoc ) ) )
    {
        r = TRUE;
    }
    else    // fill it
    {
        m_pDoc = NULL; //in case the other test failed
        CDocManager * pManager = AfxGetApp()->m_pDocManager;
        ASSERT ( pManager != NULL ); 

        POSITION posTemplate = pManager->GetFirstDocTemplatePosition();
        while ( posTemplate !=NULL && m_pDoc == NULL)
        {// get the next template
            CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
            POSITION posDoc = pTemplate->GetFirstDocPosition();
            while( posDoc !=NULL )
            {
                m_pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
                r = TRUE; // POB - 2 Aug 2004
            }
        }
        if (m_pDoc == NULL)
        {
#ifdef XXX_NORMAL_CONDITION_STARTSEQ_LOG
        // a normal condition at startup and shutdown
            LOGIT(CLOG_LOG|CERR_LOG,"   MainFrame's GetDocument failed.\n");
#endif
            r = FALSE; // be sure that a true will have a pointer!!
        }
#ifdef _DEBUG
        else // not null
        if ( gblDoc != m_pDoc )
        {
            TRACE(_T("******** MainFrame GetDocument detected a mismatch of Document pointers.\n"));
            TRACE(_T("         global = 0x%p         gotton = 0x%p\n"),gblDoc,m_pDoc);
            gblDoc = m_pDoc;
        }
#endif
    }
    return r;
}

void CMainFrame::OnViewCommLog() 
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    
    GetDocument();
    tstring s;
    TCHAR pa[512];
    TCHAR* pe = NULL;

    if ( SearchPath(NULL,_T("Communication Log.exe"),NULL,512,pa,&pe) )
    {
        s = pa;
    }
    else
    {
        s += _T("Communication Log.exe");
    }

    // Start the child process. 
    if( !CreateProcess( s.c_str()/*"Communication Log.exe"*/, // No module name (use command line). 
        M_UI_MT_STR, // Command line. 
        NULL,             // Process handle not inheritable. 
        NULL,             // Thread handle not inheritable. 
        FALSE,            // Set handle inheritance to FALSE. 
        0,                // No creation flags. 
        NULL,             // Use parent's environment block. 
        NULL,             // Use parent's starting directory. 
        &si,              // Pointer to STARTUPINFO structure.
        &pi )             // Pointer to PROCESS_INFORMATION structure.
    ) 
    {
        TRACE(_T("CreateProcess failed.") );
        LOGIT(UI_LOG,"Comm Log process failed to start. '%s'",s.c_str());

        return;
    }
    Sleep(2000);// give it a chance to come up...

    CClient *pSocket = NULL;

    if (m_pDoc)
    {

        if (m_pDoc->m_pSocket)
        {
            delete m_pDoc->m_pSocket;
            m_pDoc->m_pSocket = new CClient(m_pDoc);
            m_pDoc->m_pSocket->Createnew();
        }

        pSocket = m_pDoc->m_pSocket;
        if (pSocket->m_bConnected){m_pDoc->m_bSocketConnected=true;}else{m_pDoc->m_bSocketConnected=false;}

        CString strTitle = m_pDoc->GetTitle();
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
        char* locCptr = new char[strTitle.GetLength()+1];
        UnicodeToASCII(strTitle, locCptr, strTitle.GetLength()+1);
        string ss( locCptr );
#else
        string ss ( (LPCSTR)strTitle );
#endif

        if (pSocket)
        {
            // Set the window title
            pSocket->SendMessage("Title", 5);
        // stevev - WIDE2NARROW char interface
        //  pSocket->SendMessage(strTitle.GetBuffer(strTitle.GetLength()), strTitle.GetLength());
            pSocket->SendMessage((char*)(ss.c_str()), ss.length());
        }
    }


    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );

}

// Added By deepak
// View is switched between Errorlog and Item Listview  based on Check/Uncheck condition of menu item
//
// moved below to its own window
//void CMainFrame::OnViewErrorlog() 
//{
/*<START>Added by ANOOP for splitter pane   */  
/*  CRect rect;
    GetWindowRect( &rect );

    if(m_bRightPaneSplitterCreated)
    {
        m_bRightPaneSplitterCreated=FALSE;
        m_wndSplitter_RightPane.LockBar(TRUE);  //Added by ANOOP 21FEB2004
        m_wndSplitter_RightPane.SetRowInfo(0, rect.Height(), 5);
        m_wndSplitter_RightPane.SetRowInfo(1, 0, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 
    }
        else
    {
        m_bRightPaneSplitterCreated=TRUE;
        GetWindowRect( &rect );
        m_wndSplitter_RightPane.SetRowInfo(0, rect.Height()/2, 5);
        m_wndSplitter_RightPane.SetRowInfo(1, rect.Height()/2, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 

        m_wndSplitter_RightPane.LockBar(FALSE); //Added by ANOOP 21FEB2004  
        CView *pNewView = static_cast<CView*> (m_wndSplitter_RightPane.GetPane (1,0));
    pNewView->OnInitialUpdate();
        CErrLogView *pErrLogView=(CErrLogView *)pNewView;
    pNewView->ShowWindow(SW_SHOW);  

    }*/
    
/*<END>Added by ANOOP       */

    // The error log is now handled by the child window - POB, 2 Aug 2004
//17mar11   if (pTableChildFrm)
//17mar11       pTableChildFrm->OnViewErrorlog();
//}

void CMainFrame::OnUpdateViewCommLog(CCmdUI* pCmdUI) 
{
//  CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
//  barCtrl.SetText("Text For Pane 0", 0, 0) ;

    if (m_pDoc)
        if (m_pDoc->m_bSocketConnected ) // Already open grey the menu
            pCmdUI->Enable(FALSE);
        else
            pCmdUI->Enable();
}


void CMainFrame::OnViewMorestatus() 
{
/*<START>Added by ANOOP 15FEB 2004  */
    RETURNCODE rc = SUCCESS;
    hCitemBase* pIB = NULL;
    hCBitEnum *pBe = NULL;
    hCVar *pDevStatus=NULL;
    hCVar *pComStatus = NULL;
    int nCommStatusValue = 0;
    int nDevStatusValue = 0;
/*<END>Added by ANOOP 15FEB 2004    */

    if ( m_pDoc == NULL )
        GetDocument();
    if ( m_pDoc == NULL )
        return;
    if(m_pDoc->pCurDev == NULL)
        return;


    if (m_pDoc->pCurDev != NULL )
    {
/*<START>Added by ANOOP 15FEB 2004 and deleted the old code which was sending command48 */
        rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB);
        if ( rc == SUCCESS && pIB != NULL )
        {
            pComStatus = (hCVar*) pIB; // a cast operator
            if ( pComStatus != NULL && pComStatus->VariableType() == vT_BitEnumerated)
            {// make it a bit enum
                pBe = (hCBitEnum*)pComStatus;
                if (NULL != pBe)
            {
                    nCommStatusValue=pBe->getDispValue(); 
                }
                            }
                            }

        pIB=NULL;
        pBe=NULL;
                                    
                                                            
        rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
        if ( rc == SUCCESS && pIB != NULL )
        {
            pDevStatus = (hCVar*) pIB; // a cast operator
            if ( pDevStatus != NULL && pDevStatus->VariableType() == vT_BitEnumerated)
            {// make it a bit enum
                pBe = (hCBitEnum*)pDevStatus;
                if (NULL != pBe)
                {
                    nDevStatusValue=pBe->getDispValue(); 
                }   
            }
        }       

    /*<END>Added by ANOOP 15FEB 2004 and deleted the old code which was sending command48   */

        CDDLVariable *pvarDevStatus = new CDDLVariable(pDevStatus);
        
        CDDLVariable *pvarComStatus = new CDDLVariable((hCVar *)pComStatus);

        //ptrCmdDlg= new CCommand48_PropSheet(pvarDevStatus, pvarComStatus,
        //                         cmd48_resp_Array,nTot_Count,this,ID_DEV_STATUS_SHEET,NULL,0);
        ptrCmdDlg= new CCommand48_PropSheet(pvarDevStatus, pvarComStatus,this,
                                                                    ID_DEV_STATUS_SHEET,NULL,0);
        if(NULL != ptrCmdDlg )
        {
            ptrCmdDlg->Create(this,WS_SYSMENU | WS_POPUP | WS_CAPTION | DS_MODALFRAME | 
                                                                WS_VISIBLE|WS_MINIMIZEBOX,NULL);
            m_pDoc->ptrCMD48Dlg= ptrCmdDlg;
            //ptrCmdDlg->Create(this,WS_VISIBLE|WS_SYSMENU|WS_MINIMIZEBOX|WS_MAXIMIZEBOX,NULL);
        }
        

        if (NULL != pvarDevStatus)
            delete pvarDevStatus;

        if (NULL != pvarComStatus)
            delete pvarComStatus;   
    }// endif - we have a device            
}


void CMainFrame::OnUpdateViewMorestatus(CCmdUI* pCmdUI) 
{
    if (NULL != m_pDoc)
    {
        if (NULL != m_pDoc->ptrCMD48Dlg )
        {
            // Already open grey the menu
            pCmdUI->Enable(FALSE);
        }
        else
        {
            pCmdUI->Enable();
        }
    }
    
    
/*<START>Commented by ANOOP 06FEB2004
    pCmdUI->Enable(FALSE);
    if ( m_pDoc == NULL )
        GetDocument();
    if ( m_pDoc == NULL )
        return;
    // Get the current device for getting the Process Variable item
    hCddbDevice* pCurDevice = m_pDoc->pCurDev;
    if (pCurDevice)
    {
        hCVar* pVarResponseCd = NULL;
        hCitemBase* pIB;
        CValueVarient  rep;
        RETURNCODE rc = pCurDevice->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
        if ( rc == SUCCESS && pIB )
        {
            pVarResponseCd = (hCVar*) pIB;
            rep = pVarResponseCd->getRealValue();
            if ( rep.vType == CValueVarient::isIntConst && 
                (rep.vValue.iIntConst & MORE_STATUS_AVAIL)  )
            {
                pCmdUI->Enable(TRUE);
            }
        }
    }   <END>Commented by ANOOP */

}



//prashant nov 2003
void CMainFrame::OnUpdateDeviceHelp(CCmdUI* pCmdUI) 
{
    // if document does not exist, return
    if (!m_pDoc) // POB
        return;

    m_szLit18File = m_pDoc->m_copyOfCmdLine.cl_HelpDir;
    m_szLit18File += "\\";

    //generate the lit18 document name
    //it is of the form mmmmttdd.pdf where
    //mmmm - manufacturer in hex
    //tt - device type in hex
    //dd - device revision in hex
    if(m_pDoc->pCurDev != NULL)
        {
        CString szDDKey;
            DD_Key_t unDDKey = m_pDoc->pCurDev->getDDkey();
            //mmmmtttt00rr00dd
            unDDKey >>= 16;// loose the DD rev
            unsigned devrev  = (unsigned)(unDDKey & 0x00ff);
            unDDKey >>= 16;// loose the Device rev

            szDDKey.Format(_T("%08I64x%02x"),unDDKey,devrev);
            m_szLit18File += szDDKey;
            m_szLit18File += ".pdf";

            CFileFind finder;

            if(finder.FindFile(m_szLit18File))
                {
                    pCmdUI->Enable(TRUE);
                }
            else
                {
                    pCmdUI->Enable(FALSE);
                }
        }
    else
        {
            pCmdUI->Enable(FALSE);
        }
    
}

void CMainFrame::OnDevHelp() 
{
    OnDeviceHelp();
}

void CMainFrame::OnUpdateDevHelp(CCmdUI* pCmdUI) 
{
    OnUpdateDeviceHelp(pCmdUI);
    
}

void CMainFrame::OnCommit() 
{
    vector <hCitemBase*> pVariablesToWrite;
    
    CDDIdeDoc *pDoc = this->m_pDoc; 
    //  (CDDIdeDoc *)GetDocument();
    if (pDoc == NULL)
        return;
    /** Cycle through the child list on this DDL Menu and update all the variables    */
    // Bulk Write
    // Bulk check then Write
    CVarList* pVars = (CVarList*)pDoc->pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));
    if (pDoc->pCurDev && pVars && pVars->size() > 0)
    {// collect the changed variables
        for ( CVarList::iterator iT = pVars->begin(); iT < pVars->end(); iT++)
        {// iT isa ptr2a ptr 2a hCVar
            if ((*iT)->IsVariable() && (*iT)->isChanged() )
            {
                 /* VMKP added on 030204 */
                 g_chWriteEnable = TRUE;
                 /* VMKP added on 030204 */
                pVariablesToWrite.push_back((hCitemBase *) (*iT) );
            }
        }
    //  write the changed variables
        m_MainFrameState = FS_ISCOMMITTING;// added stevev - 2/17/04
        pDoc->pCurDev->Write(pVariablesToWrite);        
    }

  if(pDoc->m_bVariableEditedByUser)
  {
    pDoc->m_bVariableEditedByUser = FALSE;
  } 
}

/*Vibhor 230205: Start of Code*/
/*
To fix the tooltip problem when commit & cancel buttons were enabled
1.Merged the ON_UPDATE_COMMAND_UI handlers for Commit & Cancel buttons to one below
  becasue they had same duplicate code
2.Modified the logic to disable the buttons on each false condition, instead of disabling
  them on each entry (earlier).
  The earlier code was creating a state of enabled / disabled in a sort of loop thereby 
  confusing the Windows resulting in Tooltips not showing up
*/


void CMainFrame::OnUpdateCommitOrAbort(CCmdUI* pCmdUI) 
{
    if( FALSE == GetDocument())
    {
        pCmdUI->Enable(FALSE);
    }

    GetDocument();
    CDDIdeDoc *pDoc = this->m_pDoc; 

    if(NULL != pDoc)
    {
        // Get the current device for getting the Process Variable item
/*      hCddbDevice* pCurDevice = pDoc->pCurDev;
        if (NULL != pCurDevice)
        {
            pCmdUI->Enable(TRUE);
        }*/
        //prshant: commented above and replace with code below
//Vibhor 230205: With the generic code from Steve below, this is redundant, hence commenting out
/*  if(pDoc->m_bVariableEditedByUser)
        {
          pCmdUI->Enable(TRUE);

        }
*/    
        /* stevev - 2/17/04 - added to be generic */
        CVarList* pVars = NULL;
        if (pDoc->pCurDev != NULL)
        {
            pVars = (CVarList*)pDoc->pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));
        }
        if (pVars == NULL) 
        {
            pCmdUI->Enable(FALSE);
            return; // do not try anything else
        }

        /* We need to get both of 'em to determine the changed state *
        /* we must get both or none to avoid deadlock 
           Both are required to prevent flickering the cancel/commit buttons during
           command weighing
        */
        BOOL  isChnged = FALSE;

        int c = 0;
        while ( c < 50 )
        {
#ifdef _DEBUG
            pDoc->pCurDev->aquireItemMutex(__FILE__,__LINE__);// will wait forever..
            if ( pDoc->pCurDev->aquireIndexMutex(50,__FILE__,__LINE__) == SUCCESS )
#else
            pDoc->pCurDev->aquireItemMutex();// will wait forever..
            if ( pDoc->pCurDev->aquireIndexMutex(50) == SUCCESS )
#endif
            {
                isChnged = pVars->isChanged();
            
                pDoc->pCurDev->returnIndexMutex();  
                pDoc->pCurDev->returnItemMutex();
                break;// success  while exit
            }
            else
            {   
                pDoc->pCurDev->returnItemMutex();// index may be waiting for it to finish
                // loop to try again - we could sleep here but it proly wouldn't help
                systemSleep(10);// stevev 19dec05
            }
            c++; // stevev 19dec05 - oops
        }// wend
        if ( c >= 50 )// failure while exit
        {
            LOGIT(CLOG_LOG,"Could not get the needed resources for is changed.\n");
            return;
        }

        if (isChnged)// stevev 3/31/04 - get the else right
        {
            if ( pDoc->appCommEnabled() )
                pCmdUI->Enable(TRUE);
            // else leave it alone
        }
        else // - none changed - leave it disabled
        {   
            if (m_MainFrameState == FS_ISCOMMITTING )//we were committing, must be done
            {// transition
                m_MainFrameState = FS_ISIDLE;// commit operation complete
                /* stevev 4/26/04 - we need to wait till it's cleared.
                fixConfigChangeBit();
                end stevev 4/26/04 */
            }
            pCmdUI->Enable(FALSE);
        }
        /* end - stevev - 2/17/04 */
    }
}


/*Vibhor 230205: End of Code*/

void CMainFrame::OnAbort() 
{
    CDDIdeDoc *pDoc = this->m_pDoc;

    if (pDoc == NULL)
        return;

    /*stevev - 17mar11 - removed commented code */

    // loop thru all changed variables and cancel changes
    CVarList* pVars = (CVarList*)pDoc->pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));
    if (pDoc->pCurDev && pVars && pVars->size() > 0)
    {// collect the changed variables
        for ( CVarList::iterator iT = pVars->begin(); iT < pVars->end(); iT++)
        {// iT isa ptr2a ptr 2a hCVar
            if ((*iT)->IsVariable() && (*iT)->isChanged() )
            {
                //cancel changes
        (*iT)->CancelIt();
        //stevev 06dec05
        // stevev 03jul07 the line below does not update the window view when cancelled - PAR765
        //pDoc->UpdateAllViews( NULL, UPDATE_STRUCTURE, (CObject *)((*iT)->getID()) );
        SendMessage(WM_SDC_VAR_CHANGEDVALUE,(*iT)->getID(),0);// update 'em all

            }
        }
    }

  if(pDoc->m_bVariableEditedByUser)
  {
    pDoc->m_bVariableEditedByUser = FALSE;
  }

}

/*Vibhor 230205: Start of Code*/

#if 0
void CMainFrame::OnUpdateAbort(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(FALSE);

    CDDIdeDoc *pDoc=this->m_pDoc;
    
    if( NULL == pDoc )
        return;

    // Get the current device for getting the Process Variable item
/*  hCddbDevice* pCurDevice = pDoc->pCurDev;
    if (pCurDevice)
            pCmdUI->Enable(TRUE);*/
    //prshant: commented above and replace with code below
    if(pDoc->m_bVariableEditedByUser)
    {
            pCmdUI->Enable(TRUE);
    }
    /* stevev - 2/17/04 - added to be generic */
    CVarList* pVars = NULL;
    if (pDoc->pCurDev != NULL)
    {
        pVars = (CVarList*)pDoc->pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));
    }
    if (pVars == NULL) 
    {
        return; // do not try anything else
    }

    if (pVars->isChanged() )// stevev 3/31/04 - get the else right
    {
        if (pDoc->m_autoUpdateDisabled == false)
            pCmdUI->Enable(TRUE);
        // else leave it alone
    }
    else // - none changed - leave it disabled
    {
        if (m_MainFrameState == FS_ISCOMMITTING )//we were committing, must be done
        {// transition
            m_MainFrameState = FS_ISIDLE;// commit operation complete
            /* stevev 4/26/04 - we need to wait till it's cleared.
            fixConfigChangeBit();
            end stevev 4/26/04 */
        }
    }
        
    /* end - stevev - 2/17/04 */
}

#endif

/*Vibhor 230205: End of Code*/

void CMainFrame::OnMfgrlibrary() 
{
    /* stevev 10jul09 - modified to make ddselect a common file */
    DD_Key_t    activeDDkey = 0;
    CDDselect   dlg;
    
    hCddbDevice *pStandardDev = (hCddbDevice*)(this->m_pDoc->pDevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE));
    // getDevicePtr is ALWAYS successful
    if (pStandardDev->getDDkey() == 0)// the error device
    {// the standard device may not be loaded at this point
        Indentity_t mtIdentity;
        pStandardDev = this->m_pDoc->pDevMgr->newDevice(mtIdentity,NULL,NULL,NULL,NULL,0,TOK_NONE /* was false */);
        // we should handle a load error here!
    }
    dlg.setResp(pStandardDev,&activeDDkey);

    dlg.DoModal();

/*Vibhor 080404: Start of Code*xxx/ 
    CDDselect dlg(this->m_pDoc);
/xx*Vibhor 080404: End of Code*xxx/ 
//  dlg.setResp(&activeDDkey);
    dlg.DoModal();  **************/
}

/*<START>Added by ANOOP */
BOOL CMainFrame::GetExtendedDeviceStatusInfo(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &cmd48_resp_Array ,int &nTot_Count ) 
{
    int nCommStatusValue = 0;
    int nDevStatusValue = 0;
    nTot_Count=0;

//  CArray<CMD48_RESPONSE,CMD48_RESPONSE &> cmd48_resp_Array;
    CMD48_RESPONSE cmdresp;
//  int nTot_Count=0;


//  varPtrList_t itemList;  /*Commented by Anoop since we are not using it */  
    varPtrList_t itemList; /*Added by Anoop since we are not using it */  
    int nHandle = 0;        // Used for optimisation, 0 means resolve it otherwise use the existing resolved list.
    hCitemBase *pComStatus = NULL;

    if ( m_pDoc == NULL )
        GetDocument();
    if ( m_pDoc == NULL )
        return FALSE;

    // Device status dialog - Command 48
    CCmdList *ptrCmndList = (CCmdList *)m_pDoc->pCurDev->getListPtr(iT_Command);
    if (ptrCmndList)
    {
        hCcommand *pCommand = ptrCmndList->getCmdByNumber(48);
        if (pCommand)
        {
            hCtransaction *pTrans = pCommand->getTransactionByNumber(0);
            if (pTrans)
            {
                if (pTrans->getReplyList(itemList) == SUCCESS)
                {
                    if (itemList.size() >0)
                    {
                        // Item 0 will contain the Communication Error details
                        // or the response code for the commands depending on the Bit 7 value

                        hCVar *pVarCommStatus = (hCVar *)itemList[0];
                        // Item 0 is the response code if the Bit 7 is set then it is
                        // the variable contains the communication error codes
                        // otherwise it contains the response code and the device status
                        // Refer Status coding under Smart instruments and the HART protocol
                        if (pVarCommStatus->VariableType() == BIT_ENUMERATED || pVarCommStatus->VariableType() == ENUMERATED)
                        {
                            hCEnum* pEnCommStatus = (hCEnum*)pVarCommStatus;
                            nCommStatusValue = pEnCommStatus->getDispValue();

                                 if (m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pComStatus) == SUCCESS)
                                    pEnCommStatus = (hCEnum*)pComStatus;

                        }
                        // Item 1 will contain the Device Status
                        hCVar *pDevStatus = (hCVar *)itemList[1];

                        if (pDevStatus->VariableType() == BIT_ENUMERATED)
                        {
                            hCEnum* pEnDevStatus = (hCEnum*)pDevStatus;
                            nDevStatusValue = pEnDevStatus->getDispValue();
                            // Get the Bit enum list string
                            // Paint them on the screen
                        }


/*<START> Command 48 enhancements Added by ANOOP Starts 21/07/2003 */
                        nTot_Count=0;

                        for( unsigned nCnt=2;nCnt<itemList.size();nCnt++)
                        {                           
                            RETURNCODE rt = SUCCESS;
                            string str1="";
                            string t=""; 
                            CString sf="";
                            EnumTriad_t localData;// triad
                            
//as per WAP - show everything::    if (nCnt>7 && nCnt <15 ) continue;      //Not transmitter specific information so we are skipping this.

                            hCVar *pIm  = (hCVar *)itemList[nCnt];
                            if (pIm->VariableType() == BIT_ENUMERATED)
                            {
                                hCEnum* pEn  = (hCEnum*)pIm;
                                hCenumList eBitEnumList(pEn->devHndl());
                            
                                hCBitEnum* pE = (hCBitEnum*)pIm;
                                hCenumList eBitValList(pE->devHndl());
                                UINT val=pE->getDispValue();
                                pE->procureString(val,t) ; // error string if not found
                                //sf = t.c_str();   
                                
                                                        
                                if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS )
                                {
                                    LOGIT(CERR_LOG,"ERROR: No eList for bit enum.\n");
                                }
                                else
                                {
                                    // Fix Command 48 Tabs so that they show the VARAIBLE's label on the tab, POB - 17 Oct 2005

                                /*  char szCnt[24]={0};
                                    cmdresp.szByteIdentity="";
                                    cmdresp.szByteIdentity="BYTE";
                                    itoa(nTot_Count,szCnt,10); 
                                    cmdresp.szByteIdentity += szCnt;*/

                                    wstring szLabel;  // Use wstring for UTF-8 suppor, POB - 2 Aug 2008

                                    pE->Label(szLabel);
                                    cmdresp.szByteIdentity = szLabel.c_str();
                                    // End of change - POB - 17 Oct 2005

                                    nTot_Count++;

                                    for (hCenumList::iterator iT = eBitEnumList.begin(); iT < eBitEnumList.end(); iT++)
                                    {
                                        localData = *iT;
                                        unsigned long nPosition= localData.val; 
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
                                        wstring ss= localData.descS;
#else
                                        string ss= TStr2AStr(localData.descS);
#endif
                                        CString tmpStr = ss.c_str();
                                        cmdresp.szDesc=tmpStr;
                                        cmdresp.bIsDisplayed=FALSE; 
                                        if ( nPosition & val ) 
                                        {
                                            cmdresp.nVal=1;     //GREEN LED
                                        }
                                        else
                                        {
                                            cmdresp.nVal=0;         //RED LED
                                        }
                                        cmd48_resp_Array.Add(cmdresp);
                                        cmdresp.szDesc="";
                                    }   
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return TRUE;

}

char fSem = 0;
/*Vibhor 040204: Start of Code*/

LRESULT /*void PAW 03/03/09 */CMainFrame::OnNotifyRespCode(WPARAM wParam, LPARAM lParam)// parameters added PAW 03/03/09 
{
    CDDIdeDoc *pDoc=this->m_pDoc;
    
    if( NULL == pDoc )
        return (NULL);  // NULL added PAW 03/03/09

    CRespCodeDevStat respCodeDlg;
    
    respCodeDlg.m_strDispMsg = pDoc->m_strDisplay;
    if (m_nFlags & WF_MODALLOOP || m_cModalStack > 0)
    {// already in a modal loop
        if (fSem)
        {
            TRACE(_T("Already modal in OnNotifyRespCode.\n"));
        }
        else 
        {
            TRACE(_T("Already modal but not in OnNotifyRespCode.\n"));
/*          PostMessage(WM_NOTIFY_RESPCODE);// put it back and try again
            Sleep(20);
        If we don't supress, this should come back pretty quick...
        If somebody is modal, just discard this notification and handle it later.
        This normally occurs inside a method.  We let the method deal with it and
        we put up the notification if it still exists after the method completes
        (when there is no modal).
*/
        }
    }
    else
    {
        if (!fSem)
        {
            fSem = 1;
            int ret = respCodeDlg.DoModal();

            if (respCodeDlg.m_bIgnoreFlag){m_pDoc->m_bIgnoreRespCode=true;}else{m_pDoc->m_bIgnoreRespCode=false;}

            if(TRUE == m_pDoc->m_bIgnoreRespCode)
            {
                m_pDoc->m_uSuppressionCnt = respCodeDlg.m_nSuppressCnt;
            }
            fSem = 0;
        }
        else // error - just discard
        {
            TRACE(_T("Not modal but flagged in OnNotifyRespCode.\n"));
            fSem = 0;// this should never occur
        }
    }
//  return TRUE;
    return (NULL);  // PAW 03/03/09
}

/*Vibhor 040204: End of Code*/

LRESULT CMainFrame::OnDestroyCMD48Dialog(WPARAM wParam, LPARAM lParam)
{
    if( NULL != ptrCmdDlg )
    {
        delete ptrCmdDlg;
        m_pDoc->ptrCMD48Dlg=NULL;
    }
    return TRUE;
}

//prashant170402 To handle error window hiding

//DEL void CMainFrame::OnSize(UINT nType, int cx, int cy) 
//DEL {
//DEL   //CFrameWnd::OnSize(nType, cx, cy);
//DEL   CMDIFrameWnd::OnSize(nType, cx, cy);
//DEL   
//DEL /*<START>Added by ANOOP 23FEB2004 fixed issues in scrolling of the error log window   */  
//DEL       CRect rect;
//DEL   GetWindowRect( &rect );
//DEL 
//DEL   if(m_bRightPaneSplitterCreated && m_wndSplitter_RightPane )
//DEL   {
//DEL       m_wndSplitter_RightPane.SetRowInfo(0,rect.Height()/2, 5);
//DEL       m_wndSplitter_RightPane.SetRowInfo(1,rect.Height()/2, 5);
//DEL       m_wndSplitter_RightPane.RecalcLayout(); 
//DEL   }
//DEL   else if( ( FALSE == m_bRightPaneSplitterCreated ) && m_wndSplitter_RightPane ) 
//DEL   {
//DEL       m_wndSplitter_RightPane.SetRowInfo(0,rect.Height(), 5);
//DEL       m_wndSplitter_RightPane.SetRowInfo(1,0, 5);
//DEL   m_wndSplitter_RightPane.RecalcLayout(); 
//DEL   }
//DEL /*<END>Added by ANOOP 23FEB2004 fixed issues in scrolling of the error log window */
//DEL 
//DEL }

/*<START>Added by ANOOP 03FEB2004 Added for cleaning up the error log window when new device is clicked */
// moved below to single window
//BOOL CMainFrame::CloseErrorLog()
//{
/*  if(m_bRightPaneSplitterCreated)
    {
        CRect rect;
        GetWindowRect( &rect );

        if(m_bRightPaneSplitterCreated)
        {
            m_bRightPaneSplitterCreated=FALSE;

            CView *pNewView = static_cast<CView*> (m_wndSplitter_RightPane.GetPane (1,0));
            CErrLogView *pErrLogView=(CErrLogView *)pNewView;
            pErrLogView->EmptyListCtrl(); 

            m_wndSplitter_RightPane.LockBar(TRUE);  //Added by ANOOP 21FEB2004
            m_wndSplitter_RightPane.SetRowInfo(0, rect.Height(), 5);
            m_wndSplitter_RightPane.SetRowInfo(1, 0, 5);
            m_wndSplitter_RightPane.RecalcLayout(); 
        
        }
    }*/


//  return TRUE;
    // The error log is now handled by the child window - POB, 2 Aug 2004
    //return pTableChildFrm->CloseErrorLog(); - May not need this in MDI (look and see)

//}

/*<END>Added by ANOOP 03FEB2004 Added for cleaning up the error log window when new device is clicked   */

// start stevev 3/8/04 insertion
/***************************************************************************
 * FUNCTION NAME: CMainFrame::BeginModalState / EndModalState
 *
 * DESCRIPTION:
 *   Add Global critical section (assumes there is only one main frame per appp)
 *   Add flag semaphore to prevent reentrancy
 *
 ***************************************************************************/

char ms = 'U';  /* global assumes there is only one main frame per application */
                /* used as a semaphore to protect this thread walking on itself*/

void CMainFrame::BeginModalState()
{
TRACE(_T("== BegnModal CritS %d (in 0x%x)\n"),m_cModalStack,GetCurrentThreadId());
     EnterCriticalSection( &gcsDialog );// protects against other threads
TRACE(_T("== BegnModal State %d (in 0x%x)\n"),m_cModalStack,GetCurrentThreadId());
#ifdef _DEBUG
if ( gblDoc != m_pDoc )
{
    TRACE(_T("******** Begin Modal detected a mismatch of Document pointers.\n"));
}
#endif
     if (ms == 'U')
     {
          ms = 'B';
          MyBeginModalState();
          ms = 'U';
     }
TRACE(_T("=x BegnModal State %d (in 0x%x)\n"),m_cModalStack,GetCurrentThreadId());
     LeaveCriticalSection( &gcsDialog );
}

void CMainFrame::EndModalState()
{
TRACE(_T("= EndModal CritS %d (in 0x%x)\n"),m_cModalStack,GetCurrentThreadId());
     EnterCriticalSection( &gcsDialog );
TRACE(_T("== EndModal State %d (in 0x%x)"),m_cModalStack,GetCurrentThreadId());
#ifdef _DEBUG
if ( gblDoc != m_pDoc )
{
    TRACE(_T("******** End Modal detected a mismatch of Document pointers.\n"));
}
#endif
     if (ms == 'U')
     {
          ms = 'E';
TRACE(_T("\n"));
          MyEndModalState();
          ms = 'U';
     }
#ifdef _DEBUG
     else
     {
TRACE(_T(" is %c\n"),ms);
     }
#endif
TRACE(_T("=x EndModal State %d (in 0x%x)\n"),m_cModalStack,GetCurrentThreadId());
     LeaveCriticalSection( &gcsDialog );
}

extern BOOL AFXAPI AfxIsDescendant(HWND hWndParent, HWND hWndChild);// from AFXIMPL.H

void CMainFrame::MyBeginModalState()
{
    
     ASSERT(m_hWnd != NULL);
     ASSERT(::IsWindow(m_hWnd));

TRACE(_T("++ StrtModal State %d\n"),m_cModalStack);
     // allow stacking, but don't do anything
     if (++m_cModalStack > 1)
     {
          return;
     }

     // determine top-level parent, since that is the true parent of any
     //  modeless windows anyway...
     CWnd* pParent = GetTopLevelParent();

     // first count all windows that need to be disabled
     UINT nCount = 0;
     HWND hWnd = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
     while (hWnd != NULL)
     {
          if (::IsWindowEnabled(hWnd) &&
              CWnd::FromHandlePermanent(hWnd) != NULL &&
              AfxIsDescendant(pParent->m_hWnd, hWnd) &&
              ::SendMessage(hWnd, WM_DISABLEMODAL, 0, 0) == 0)
          {
               ++nCount;
          }
          hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
     }
TRACE(_T("++ StrtModal Disables %d desktop windows.\n"),nCount);
     m_phMyWndDisable = new HWND[nCount+1];
     if (nCount == 0)
     {
         if (m_phMyWndDisable != NULL )
         {
             m_phMyWndDisable[0] = NULL; // an empty list
//           TRACE(_T("Begin modal shows all windows disabled.\n"));
         }
         else
         {
             TRACE(_T("Begin modal failed to allocate memory.\n"));
         }
          return;
     }
    
     // disable all windows connected to this frame (and add them to the list)
     UINT nIndex = 0;
     hWnd = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
     while (hWnd != NULL)
     {
          if (::IsWindowEnabled(hWnd) &&
            CWnd::FromHandlePermanent(hWnd) != NULL &&
            AfxIsDescendant(pParent->m_hWnd, hWnd) &&
            ::SendMessage(hWnd, WM_DISABLEMODAL, 0, 0) == 0)
          {
TRACE(_T("++ StrtModal: sending DISABLE.\n"));
               ::EnableWindow(hWnd, FALSE);
               ASSERT(nIndex < nCount);
               m_phMyWndDisable[nIndex] = hWnd;
               ++nIndex;
          }
          hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
     }

     // terminate the list with a NULL
     ASSERT(nIndex < nCount+1);
     m_phMyWndDisable[nIndex] = NULL;
}
void CMainFrame::MyEndModalState()
{
TRACE(_T("++ EndModal State %d "),m_cModalStack);
     // pop one off the stack (don't undo modalness unless stack is down to zero)
     if (m_cModalStack == 0 || m_phMyWndDisable == NULL)
     {
TRACE(_T("to 0 \n"),m_cModalStack);
          return;
     }
     if (--m_cModalStack > 0)
     {
TRACE(_T("to %d \n"),m_cModalStack);
          return;
     }
     if (m_cModalStack == 0)
     {
TRACE(_T("(stack empty)\n"));
     }// can't be negative, would have returned earlier.

     // enable all the windows disabled by BeginModalState
     ASSERT(m_phMyWndDisable != NULL);
     UINT nIndex = 0;
     while (m_phMyWndDisable[nIndex] != NULL)
     {
          ASSERT(m_phMyWndDisable[nIndex] != NULL);
          if (::IsWindow(m_phMyWndDisable[nIndex]))
          {
              if (m_phMyWndDisable[nIndex] == m_hWnd)
              {
TRACE(_T("++ EndModal Sending SELF Enable.\n"));
              }
              else
              {
TRACE(_T("++ EndModal Sending Enable.\n"));
              }
              ::EnableWindow(m_phMyWndDisable[nIndex], TRUE);
          }// else just skip it
//        {
//            if (m_phMyWndDisable[nIndex] == m_hWnd)
//            {
//TRACE(_T("++ EndModal Sending SELF Enable.\n"));
//                SendMessage(WM_ENABLE,TRUE,2);// appears that EnableWindow only does this occasionally
//            }
             ++nIndex;
//        }
     }
TRACE(_T("++ EndModal Enabled %d \n"),nIndex);
     delete[] (void*)m_phMyWndDisable;
     //delete m_phMyWndDisable;
     m_phMyWndDisable = NULL;
}

// end stevev 3/8/04 insertion
// stevev 17oct05 add next level up
/* On enable: Called AFTER WS_DISABLED bit set but before EnableWindow() returns */
void CMainFrame::OnEnable(BOOL bEnable)
{
    if (bEnable && (m_nFlags & WF_STAYDISABLED))
    {// stevev 17oct05 - don't know what this is for so I'll just leave it
        // Work around for MAPI support. This makes sure the main window
        // remains disabled even when the mail system is booting.
        EnableWindow(FALSE);
        ::SetFocus(NULL);
        return;
    }

    // only for top-level (and non-owned) windows
    if (GetParent() != NULL)
	{
		TRACE(_T("%s: with a parent; bailing out.\n"),(bEnable)?_T("_EN"):_T("DIS"));
        return;
	}

    // InModalState() just tests to see if anything is on the modal stack
    if (!bEnable) // DISABLE
    {   //          noStack       same as       notFlagged   ( ie both true or both false)
        if (( InModalState() == 0 ) == ( (m_nFlags & WF_MODALDISABLE) == 0))
        {// notStacked & notFlagged OR stacked & Flagged
            //ASSERT((m_nFlags & WF_MODALDISABLE) == 0);
            //m_nFlags |= WF_MODALDISABLE;
			TRACE(_T("DIS: StrtMdl %s (in 0x%x)win:%x\n"),
      ((m_nFlags & WF_MODALDISABLE) == 0)?L"NoStkORFlg":L"StkANDFlg",GetCurrentThreadId(),m_hWnd);
            BeginModalState();  // @notStacked & notFlagged - will re-enter here with a stack & no flag
                                // @stacked & Flagged       - just counts and returns
        }
        else    // different
        {// stacked & notFlagged OR notStacked & Flagged
            if ((m_nFlags & WF_MODALDISABLE) == 0)  // notFlagged (means stacked)
            {// this is the re-entry - just flag it (don't call begin again)
                ASSERT((m_nFlags & WF_MODALDISABLE) == 0);
                m_nFlags |= WF_MODALDISABLE;

                // force WM_NCACTIVATE for floating windows too
                NotifyFloatingWindows(FS_DISABLE);
				TRACE(_T("DIS: StrtMdl Stk_NO_Flg (in 0x%x)win:%x\n"),GetCurrentThreadId(),m_hWnd);
            }
            else    // Flagged (means notStacked)
            {// error
				TRACE(_T("DIS: StrtMdl Flg_NO_Stk (in 0x%x)win:%x\n"),GetCurrentThreadId(),m_hWnd);
//temp to see what happens...apparently OK <sjv 21aug08.                ASSERT((m_nFlags & WF_MODALDISABLE) == 0);// force assertion
            }
        }
    }
    else // ENABLE
    {
        if (m_nFlags & WF_MODALDISABLE) // Flagged
        {
            if ( InModalState() != 0 ) // Stacked (and Flagged)
            {// call end to unstack it - it'll re-enter with no stack on the last one
				TRACE(_T("_EN: StrtMdl FlgANDstk (in 0x%x)win:%x\n"),GetCurrentThreadId(),m_hWnd);
                EndModalState();
            }
            else                       // notStacked (and Flagged)
            {// re-entry - last one out turns off the lights
                m_nFlags &= ~WF_MODALDISABLE;
				TRACE(_T("_EN: StrtMdl FlgAND_NO_stk (in 0x%x)win:%x\n"),GetCurrentThreadId(),m_hWnd);

                // cause normal focus logic to kick in
                if (::GetActiveWindow() == m_hWnd)
                {
					TRACE(_T("   : Sending Activate. (in 0x%x)win:%d\n"),GetCurrentThreadId(),m_hWnd);
                    SendMessage(WM_ACTIVATE, WA_ACTIVE);
                }

                // force WM_NCACTIVATE for floating windows too
                NotifyFloatingWindows(FS_ENABLE);
            }
        }
        else // notFlagged
        {// these are almost always an error - one more serious than the other
			TRACE(_T("_EN: StrtMdl NO_Flg (error)  (in 0x%x)win:%x\n"),GetCurrentThreadId(),m_hWnd);
            if ( InModalState() != 0 ) // Stacked (and notFlagged)
            {// call end to unstack it - it'll re-enter with no stack on the last one
//              ASSERT( 0 );    // force an error
                LOGIT(CERR_LOG|CLOG_LOG,"Error: Mainframe Has a Modal task on the "
                                                                "stack without being flagged.\n");
                EndModalState();
            }
            else                       // notStacked (and notFlagged)
            {// enabling an enabled windowset
                TRACE(_T("WARN: Enabling an enabled window.\n"));// I can't tell where these come from.
            }
        }
    }
    // force WM_NCACTIVATE because Windows may think it is unecessary
    if (bEnable && (m_nFlags & WF_STAYACTIVE))
        SendMessage(WM_NCACTIVATE, TRUE);
//  // force WM_NCACTIVATE for floating windows too
//  NotifyFloatingWindows(bEnable ? FS_ENABLE : FS_DISABLE);

}

// end stevev 17oct05 addition

// Added by POB, 3 Aug 2004
//DEL void CMainFrame::AddChildWnd(CMultiDocTemplate *pTemplate)
//DEL {
//DEL /*    // get the main window, make sure it's an MDI Frame
//DEL 
//DEL   CMDIChildWnd* pActiveChild = NULL;
//DEL 
//DEL   // get the document in that frame
//DEL 
//DEL   if (GetDocument())
//DEL   {
//DEL       // create a new view template and get it drawn
//DEL 
//DEL       CFrameWnd* pNewFrame = pTemplate->CreateNewFrame(
//DEL               m_pDoc, pActiveChild);
//DEL       pTemplate->InitialUpdateFrame(pNewFrame, m_pDoc);
//DEL   }*/
//DEL 
//DEL   CDDIdeApp* pApp =  (CDDIdeApp* )AfxGetApp();
//DEL   
//DEL   ulong symbol = 1000;//(ulong)wParam;
//DEL 
//DEL //    CString text;
//DEL //    text.Format(_T("symbol = %d"), symbol);
//DEL //    MessageBox(text);
//DEL 
//DEL   if (GetDocument())
//DEL   {
//DEL       // Get frame template
//DEL       CFrameTemplate* pChosenFrame = 
//DEL               pApp->m_pDocTemplate->FindFrameTemplate("CSDC625WindowView","CWindowChildFrame");
//DEL           
//DEL       // Create a new child frame based on the above template child and view class
//DEL       CMDIChildWnd* pChildFrame = ( CMDIChildWnd* ) pChosenFrame->CreateFrame();
//DEL 
//DEL       if (pChildFrame==NULL)
//DEL       {
//DEL           TRACE(_T("Warning: failed to create new frame.\n"));
//DEL           return;     // command failed
//DEL       }
//DEL       
//DEL       pChosenFrame->InitialUpdateFrame(pChildFrame, m_pDoc);
//DEL 
//DEL       CSDC625WindowView *pView =  (CSDC625WindowView *) pChildFrame->GetActiveView();
//DEL //        pView->Initialize(symbol);
//DEL 
//DEL       SizeChildFrame();
//DEL   }
//DEL }

// Added by POB - 3 Aug 2004
LRESULT /*void PAW 03/03/09*/CMainFrame::SizeChildFrame(WPARAM wParam, LPARAM lParam)// parameters added PAW 03/03/09 
{
//  MDITile(MDITILE_HORIZONTAL);
    return (NULL);  // PAW 03/03/09
}

// Added by POB - 4 Aug 2004
void CMainFrame::BusyDisplay(BOOL state)
{
    if (state == TRUE)
    {
        if ( !isBusy )
        {
            m_Modeless.Create (IDD_WAIT_DIALOG);
            m_Modeless.ShowWindow(SW_SHOWNA);
            m_Modeless.UpdateWindow();
            isBusy = true;
        }// else, just leave it up...
    }
    else
    {
        m_Modeless.DestroyWindow();
        isBusy = false;
    }
}

LRESULT CMainFrame::OnGenDevice(WPARAM wParam, LPARAM lParam)
{
    int r = 1; // failure   
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< MnFrm::OnGenDevice - Entry, GetDocument\n");

    if (GetDocument())
    {       
        LOGIF(LOGP_START_STOP)
            (CLOG_LOG,"< MnFrm::OnGenDevice - Got a Document, ResetWindowMenu()\n");

        ResetWindowMenu();// if we are going to generate a new one, 
                          //        we want to be sure the old menu is dead
        // stevev 08aug05 - removed redundant test
        m_pDoc->m_pStatusBar = &m_wndStatusBar;
        CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
        barCtrl.SetText(M_UI_GENERATING, 0, 0) ;

        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< MnFrm::OnGenDevice-call doc's generateDevice().\n");
        m_pDoc->generateDevice();

/*temp stevev 10jan06
        if (m_pDoc->pCurDev != NULL)
            r = 0; // success
*/
    }
    else
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< MnFrm::OnGenDevice - GetDocument failed.\n");
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< MnFrm::OnGenDevice - Exiting.\n");
    return r;
}

LRESULT /*void PAW 03/03/09*/ CMainFrame::OnExeLink(WPARAM wParam, LPARAM lParam)
{
    hCitemBase* pItem = (hCitemBase*)wParam;
    if ( GetDocument() )
    {
    }
    return (NULL);  // PAW 03/03/09 return parameter added
}


LRESULT /*void PAW 03/03/09*/ CMainFrame::OnWindow(WPARAM wParam, LPARAM lParam)
{

if ( this == NULL ) 
{
    return (NULL);  // PAW 03/03/09 added parameter
}

    CDDIdeApp* pApp =  (CDDIdeApp *)AfxGetApp();
    
    CdrawBase * pBase = (CdrawWin *)wParam;
	
    LOGIT(CLOG_LOG,"NOTE: CMainFrame::OnWindow (Wparam %d, Lparam %d)\n",wParam,lParam);

    if (pBase->mdsType == mds_window)
    {
        // Get the symbol number of the item that we want to display
        //ulong symbol = (ulong)wParam;
        CdrawWin * pWindow = (CdrawWin *)pBase;
        ulong symbol = pWindow->m_symblID;

    #ifdef RLSDBG
            LOGIT(CLOG_LOG,"NOTE: doing OnWindow.\n");
    #endif
        if (GetDocument())
        {
            // Get frame template
            CFrameTemplate* pChosenFrame = 
              pApp->m_pDocTemplate->FindFrameTemplate("CSDC625WindowView","CWindowChildFrame");
                
            if (pChosenFrame==NULL)
            {
                TRACE(_T("Warning: Could not find the window template.\n"));
                return (NULL);     // command failed 
            }
            // Create a new child frame based on the above template child and view class
            CMDIChildWnd* pChildFrame = ( CMDIChildWnd* ) pChosenFrame->CreateFrame();

            if (pChildFrame==NULL)
            {
                TRACE(_T("Warning: failed to create new frame.\n"));
                return (NULL);     // command failed PAW 03/03/09 added parameter
            }
    #ifdef RLSDBG
            LOGIT(CLOG_LOG,"NOTE: doing InitialUpdateFrame.\n");
    #endif
            pChosenFrame->InitialUpdateFrame(pChildFrame, m_pDoc);

            CSDC625WindowView *pWindowView =(CSDC625WindowView *) pChildFrame->GetActiveView();

            // showwindow was here

    #ifdef RLSDBG
            LOGIT(CLOG_LOG,"NOTE: doing Initialize on the view.\n");
    #endif
            // move below - sjv 10nov06:: pWindowView->Initialize(symbol);
            
            // Give the CdrawWin that lives in the "View" menu a pointer to the child window
            // that it represents when invoked from the menu that way the object knows which
            // child window to destroy when it is needs to destroy it.
            pWindow->m_pWindow = pChildFrame;

            // stevev 08apr11 -move frame assignment out of MDIActivate (it was crashing)
            pWindowView->m_pWindowFrame = pChildFrame;
            
            // Give the frame window the pointer to the CdrawWin object that invoked it.
            // If the user closes the window, the CdrawWin object (ie, pWindow) will need
            // to be set to NULL by "View" so that "pWindow" won't try to destroy it again.
            pWindowView->m_pDrawWin = pWindow;

            pWindowView->Initialize(symbol);//moved from above

    #ifdef RLSDBG
            LOGIT(CLOG_LOG,"NOTE: doing SizeChildFrame. (exits OnWindow())\n");
    #endif
            SizeChildFrame(wParam, lParam);// PAW 03/03/09 parameters added

			pChildFrame->ShowWindow(SW_SHOWMAXIMIZED); // copied from above  28jan15 stevev  // Fixed Defect # 4504, POB - 5/20/2014 

            pWindowView->Invalidate();//try to get grid to hide - 22aug07
        }// end have-document
    }
    else if (pBase->mdsType == mds_table)
    {   /*
         * The code in the FALSE conditional is working code for opening up a childframe
         * for the Table View.  The problem is that it always brings up the root directory
         * window.  Until we fix the Document/Tree & List View relationship, we will return
         * a message box reporting that the Table Style is not supported. - POB, 25 Aug 2005
         */
        if (TRUE)/* 14mar11 - made it true */
        {
            // Get the symbol number of the item that we want to display
            //ulong symbol = (ulong)wParam;
            CdrawTbl * pTable = (CdrawTbl *)pBase;
            ulong symbol = pTable->m_symblID;

            if (GetDocument())
            {
                // Get frame template
                CFrameTemplate* pChosenFrame = 
                  pApp->m_pDocTemplate->FindFrameTemplate("CDDIdeTreeView","CTableChildFrame");
                    
                // Create a new child frame based on the above template child and view class
                CMDIChildWnd* pChildFrame = ( CMDIChildWnd* ) pChosenFrame->CreateFrame();

                if (pChildFrame==NULL)
                {
                    TRACE(_T("Warning: failed to create new frame.\n"));
                    return (NULL);     // command failed PAW 03/03/09 added parameter
                }

                pChosenFrame->InitialUpdateFrame(pChildFrame, m_pDoc);

                CDDIdeTreeView *pTreeView =  (CDDIdeTreeView *) pChildFrame->GetActiveView();

                pChildFrame->ShowWindow(SW_SHOWMAXIMIZED);  // Fixed Defect # 4504, POB - 5/20/2014
                
                // Give the CdrawTbl that lives in the "View" menu a pointer to the child window
                // that it represents when invoked from the menu that way the object knows which
                // child window to destroy when it is needs to destroy it.
                pTable->m_pTable = pChildFrame;
                
                // Give the frame window the pointer to the CdrawTbl object that invoked it.
                // If the user closes the window, the CdrawTbl object (ie, pWindow) will need
                // to be set to NULL by "View" so that "pWindow" won't try to destroy it again.
                pTreeView->m_pTwin = pTable;

                // TO DO:  Initialize does not currently exist in CDDIdeTreeView
                // pTreeView->Initialize(symbol);

                SizeChildFrame(wParam, lParam); // PAW 03/03/09 added parameters
                                                        // replace with symbol

                /* this line will add the menu to every open table view on the screen....
                m_pDoc->UpdateAllViews( NULL, UPDATE_ADD_DEVICE, (CObject*)symbol );
                so this was replaced with the following 25mar11 ***/
                ((CTableChildFrame*)pChildFrame)->m_pTreeView->OnUpdate
                /*Sender,lHint,pHint*/             (NULL, UPDATE_ADD_DEVICE, (CObject*)symbol);
                
                ((CTableChildFrame*)pChildFrame)->m_pListView->OnUpdate
                /*Sender,lHint,pHint*/             (NULL, UPDATE_ADD_DEVICE, (CObject*)symbol);
// temporary 26may11
pTableChildFrm = (CTableChildFrame*)pChildFrame;
// end temporary                
            }
        }
        ///////////////////////// TEMPORARY ///////////////////////////////////////////////////
        else
        {
            MessageBox(M_UI_NO_TABLE_STYLE);
        }
        ///////////////////////// TEMPORARY ///////////////////////////////////////////////////
    }
    else
    {
        TRACE(_T("Unrecognizedable child window type"));
    }
    return (NULL);  // PAW 03/03/09
}

void CMainFrame::UpdateBar(TCHAR* newMsg)
{   
    CStatusBarCtrl & barCtrl = m_wndStatusBar.GetStatusBarCtrl();
    barCtrl.SetText(newMsg, 0, 0) ;
    m_wndStatusBar.RedrawWindow();//UpdateWindow();
}

BOOL CMainFrame::CreateWindowMenu()
{
#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::CreateWindowMenu Entry\n"));
#endif
    CdrawMain* pDrawMn = m_pdrawMain;//stevev 24mar11 -try to un-confuse the debugger
    // Build the window's menu
    CMenu* pViewMenu = NULL;
    // Get the top level menu on the Main Frame window
    // we are the mainframe window (notice the classname...)CMenu* pTopMenu = AfxGetMainWnd()->GetMenu();
    CMenu* pTopMenu = GetMenu();
    int iPos;  // menu position

    for (iPos = pTopMenu->GetMenuItemCount()-1; iPos >= 0; iPos--)
    {
        CMenu* pMenu = pTopMenu->GetSubMenu(iPos);

        // Locate the View submenu
        if (pMenu && pMenu->GetMenuItemID(0) == ID_VIEW_TOOLBAR)/* this is the first item on the view menu */
        {
            pViewMenu = pMenu;
            break;
        }
    }

    // This should never be NULL
    ASSERT(pViewMenu != NULL);
#if 0 // moved down to try and get the menue to show correctly
    if(pDrawMn != NULL)// debugger shows m_pdrawMain as zero but the return is taken
    {
#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::CreateWindowMenu - Exit - DrawMain Exists\n"));
#endif
        return TRUE;
    }
#endif
    HMENU hmenu = m_ViewMenu.m_hMenu;

    if (!hmenu)
    {
        // Get the Table Frame Window menu (once)
        m_ViewMenu.Attach(pViewMenu->GetSafeHmenu());
    }

    if(pDrawMn != NULL)
    {
#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::CreateWindowMenu - Exit - DrawMain Exists\n"));
#endif
        return TRUE;
    }

    CWnd * pParent = this;


    // Create our "one and only" CdrawMain object
    // Fill it with each of our entry menus (if exist)
    //CdrawMain * pdrawMain = new CdrawMain;
    //m_pdrawMain = pdrawMain;
    pDrawMn = new CdrawMain;//stevev 09nov06
TRACE(_T("DrawMain set with Newed\n"));

    // Give our CdrawMain object a pointer to the windows'
    // "View" menu
    //pdrawMain->m_pMenu = &m_ViewMenu;//pViewMenu;
    pDrawMn->m_pMenu = &m_ViewMenu;//pViewMenu;
    
    /* Removed commented out code, POB - 5/20/2014 */

#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::CreateWindowMenu - Adding Menues\n"));
#endif

    //pdrawMain->AddMenus(pParent);
    pDrawMn->AddMenus(pParent);

#ifdef STARTSEQ_LOG
    TRACE(_T("CMainFrame::CreateWindowMenu - Exit\n"));
#endif
    
    m_pdrawMain = pDrawMn;// - end trying to un-confuse the debugger
    return TRUE;
}

BOOL CMainFrame::OnCommand(WPARAM wParam, LPARAM lParam) 
{   //wParam
    //The low-order word of wParam identifies the command ID of the menu item, control, or accelerator.
    //The high-order word of wParam specifies the notification message if the message is from a control.
    //If the message is from an accelerator, the high-order word is 1.
    //If the message is from a menu, the high-order word is 0.

    //lParam
    //Identifies the control that sends the message if the message is from a control. Otherwise, lParam is 0.

    WORD wControlID = LOWORD(wParam);
    WORD wMessageID = HIWORD(wParam);
    BOOL result =0;

    if (wControlID != 0 && m_pdrawMain) 
    {
        CdrawMain * pdrawMain = (CdrawMain *) m_pdrawMain;

        LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,L"NOTE: ExecuteCID (0x%04x) on 0x%x from thread 0x%04x."
                                L" -mainframe-\n",wMessageID,wControlID,GetCurrentThreadId());

        result = pdrawMain->ExecuteCID(wControlID, wMessageID);
    }
    if ( ! result ) // not handled in the tree (0)
    {
        LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,L"NOTE: Doing CMDIFrameWnd::OnCommand from thread "
                                                            L"0x%04x.\n",GetCurrentThreadId());

        result = CMDIFrameWnd::OnCommand(wParam, lParam);// true if handled
#ifdef RLSDBG
    LOGIT(CLOG_LOG,"NOTE: Exiting OnCommand from thread 0x%04x.\n",GetCurrentThreadId());
#endif
    }
    return result;
}

void CMainFrame::ResetWindowMenu(void)
{
    CdrawMain * pdrawMain = (CdrawMain *) m_pdrawMain;
        
    LOGIT(CLOG_LOG,"< MnFrm.ResetWindowMenu - Entry\n");
    if (pdrawMain)
    {
        LOGIT(CLOG_LOG,"< MnFrm.ResetWindowMenu - Call drawMain's DeleteAllMenus()\n");
        pdrawMain->DeleteAllMenus();

        LOGIT(CLOG_LOG,"< MnFrm.ResetWindowMenu - delete & NULL drawMain\n");
        // note the following was added 01jun07, the above message was not - VERY curious
        // looks like a merge error - see version 1.40.8.39 to 1.40.8.40 transition
        delete pdrawMain;
        m_pdrawMain = NULL;
        // end 01jun07 add
    }
}

LRESULT CMainFrame::OnCloseDevice(WPARAM wParam, LPARAM lParam)
{
    LRESULT r = 1; // failure
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< CMainFrame::OnCloseDevice Entry\n");
    if (GetDocument())
    {
    // done in Doc's Clse Device-stevev 12jul07 ::> ResetWindowMenu();

    //  BusyDisplay(FALSE);
        if (pTableChildFrm != NULL )
        {
            pTableChildFrm->PostMessageW(WM_CLOSE);
            pTableChildFrm = NULL;
        }
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< CMainFrame.OnCloseDevice calling Doc's CloseDevice.\n");
        r = m_pDoc->CloseDevice(wParam,lParam);
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< CMainFrame::OnCloseDevice Exit\n");

    /*NOTE: 01sept05 stevev
     *Apparently Mr Gates does a CoInitializeEx(SINGLE_THREADED) somewhere in the instantiation
            *** stevev 27sep11..INit was in windowview now in mainframe OnCreate() above***
     * of the activeX library.  This overrides the earlier, MULTITHREADED connection to the 
     * comm COM.  The threading model gets screwed up on close and reopen so the Identify cannot
     * get to the comm COM object.
     * Adding the following CoUnitialize disconnects the activeX stuff and allows the COM to work
     * as intended.  The Window Menu Items from the DD should be closed before the new device
     * generation begins.
     * We will proly have to add functionality to the commInfc to make Identify go thru the
     * packet handling task instead of identifying in the UI thread.
     *****************************************************************************************/
    CoUninitialize(); // try to disconnect
    OleUninitialize();
    return r;
}

void CMainFrame::OnRange(UINT uint)// parameter added PAW 03/03/09 
{
    // This Handler serves no purpose other than to ensure
    // that the menu items with a particular control id is
    // enabled.
}


int CMainFrame::DeRegisterDialog(/*hold 26oct sd*/ sdCDialog* pDialog)
{// assumes a HWND is unique in the system
    dlgRegIT iT;
    sdCDialog* bT = NULL;
    hCmutex::hCmutexCntrl* pMtx = dlgRegistryMutex.getMutexControl("MnFrmRegistry");

    int r = 0; // must have a single exit point for mutex control

    pMtx->aquireMutex(hCevent::FOREVER); // waits forever

    if ( dlgRegistry.size() > 0 )
    {
        bT = dlgRegistry.back();

        if ( (bT == pDialog) || (bT->m_hWnd == pDialog->m_hWnd))
        {
            iT = dlgRegistry.end() - 1;
        }
        else
        {
TRACE(_T("%%%%%%%%%% DeRegistering a dialog that is not on top of the stack!\n"));
            bT = NULL; // 10aug06 - sjv force NULL if not found
            for ( iT = dlgRegistry.begin(); iT != dlgRegistry.end(); ++iT)
            {
                if ( (*iT) == pDialog || /*bT*/(*iT)->m_hWnd == pDialog->m_hWnd)
                {
                    bT = *iT;
                    break; // out of for loop with iT set
                }
            }
        }
    }


    if (bT != NULL &&  ::IsWindow(bT->m_hWnd) )
    {
TRACE(_T("DeRegister dialog with MWND = %x\n"),pDialog->m_hWnd);
        dlgRegistry.erase(iT);
        
        if ( dlgRegistry.size() > 0 )
        {
            bT = dlgRegistry.back();

            if ( ::IsWindow(bT->m_hWnd) )
            {
TRACE(_T("DeRegister - posts an enable message for %x\n"),bT->m_hWnd);
                PostMessage(WM_ENABLE_WIN,TRUE,(LPARAM)bT->m_hWnd);// to mainframe...
/* the following causes a deadlock when the main thread is pending on the mutex
    apparently Activate does a SendMessage() and it locks up.
TRACE(_T(" Enable  dialog with MWND = %x\n"),bT->m_hWnd);
                ::EnableWindow(bT->m_hWnd,TRUE);// OS uses caps, turn ot off
TRACE(_T("Activate dialog with MWND = %x\n"),bT->m_hWnd);
                ::SetActiveWindow(bT->m_hWnd);
TRACE(_T(" Exit    dialog with MWND = %x\n"),bT->m_hWnd);
*****/
            }
            else
            {
                TRACE(_T("DeRegister's next to enable does not have a window!\n"));
            }
        }
        else
        {
TRACE(_T("Enable dialog no more on the stack (final deregister)\n"));
        }
        r = 0;
    }
    else
    {
TRACE(_T("*-*-*-*-ERROR: Deregistering an un registered Dialog.\n"));
        r = -1;
    }
    pMtx->relinquishMutex();
    delete pMtx;
    return r;
}

int CMainFrame::RegisterDialog(/*hold 26oct  sd*/ sdCDialog* pDialog)
{
    sdCDialog* bT;
    hCmutex::hCmutexCntrl* pMtx = dlgRegistryMutex.getMutexControl("MnFrmRegistry");

    int r = 0; // must have a single exit point for mutex control

    pMtx->aquireMutex(hCevent::FOREVER); // waits forever

    if ( dlgRegistry.size() > 0 )
    {
        for ( dlgRegIT iT = dlgRegistry.begin(); iT != dlgRegistry.end(); ++iT)
        {
            if ( (*iT) == pDialog)
            {
TRACE(_T("MainFrame: registering a registered dialog.\n"));
                pMtx->relinquishMutex();
                delete pMtx;
                return -10;// already registered
            }
        }
        // only gets here if not found
        bT = dlgRegistry.back();
        if ( ::IsWindow(bT->m_hWnd) )
        {
TRACE(_T("  Register - posts a disable message for %x\n"),bT->m_hWnd);
            PostMessage(WM_ENABLE_WIN,FALSE,(LPARAM)bT->m_hWnd);// to mainframe...
/* another deadlock
            ::EnableWindow(bT->m_hWnd,FALSE);// OS uses caps, turn ot off
TRACE(_T("Disable dialog with MWND = %x\n"),bT->m_hWnd);
****/
        }
    }
    if (! ::IsWindow(pDialog->m_hWnd) )
    {
        TRACE(_T("%%%%%%%%%%%%%% registration without an HWND.\n"));
    }
TRACE(_T("  Register dialog with MWND = %x\n"),pDialog->m_hWnd);

    dlgRegistry.push_back(pDialog);
    r = dlgRegistry.size();

    pMtx->relinquishMutex();
    delete pMtx;
    return r;
}

void CMainFrame::doExecuteVariable(hCVar* pV,UINT32 wParam)
{
    if (pV == NULL) return;
    RETURNCODE rc = SUCCESS;
    UINT32 nRet;
    
    rc = pV->doPreEditActs();
    if(rc != SUCCESS)
    {
        LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
        return ;
    }
    // stevev 08oct10:was:>
    //CVariableDlg EditDlg(pV,wParam,AfxGetMainWnd());// changed to send in the dev object var
    CVariableDlg EditDlg(pV,wParam,GETMAINFRAME );
        
	DEBUGLOG(CLOG_LOG,"Variable dialog is going modal from mainframe.\n");
    nRet = EditDlg.DoModal();

    // stevev 19may11 - the dialog could have caused a DD abort
    if (m_pDoc->pCurDev == NULL)
    {
        return;// there is nothing we can do
    }

    //always call post edit action
    rc = pV->doPstEditActs();
    if(rc != SUCCESS)
    {
        LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
    }

    if(IDOK == nRet)
    {
        rc = pV->doPstUserActs();
        if(rc != SUCCESS)
        {
            LOGIT(CERR_LOG|UI_LOG,"Call to post user edit action returned an error.\n");
        }
    }
    //else cancel was pressed, leave it as is
    return;
}

void CMainFrame::OnClose() 
{
TRACE(_T("MainFrame:OnClose entry.\n"));
    if ( m_pDoc != NULL  )
    {
        if ( ! m_pDoc->ready2close() )
        {
    TRACE(_T("MainFrame:OnClose document not ready to close.\n"));
        }
        else
        if (m_pDoc->pCurDev != NULL && 
            m_pDoc->pCurDev->m_pMethSupportInterface != NULL &&
          ! m_pDoc->pCurDev->m_pMethSupportInterface->hasClients() )
        {
    TRACE(_T("MainFrame:OnClose frame window close.\n"));
            CMDIFrameWnd::OnClose();
        }
        else
        if (    (m_pDoc->pCurDev != NULL && 
                                   m_pDoc->pCurDev->m_pMethSupportInterface == NULL)   ||
                (m_pDoc->pCurDev == NULL)       || (m_pDoc == NULL )
            )
        {
    TRACE(_T("MainFrame:OnClose frame window close.\n"));
            CMDIFrameWnd::OnClose();
        }
        // else do not close
    }
    else
    {
    TRACE(_T("MainFrame:OnClose with no document pointer; frame window close.\n"));
            CMDIFrameWnd::OnClose();
    }
}


bool CMainFrame::CloseErrorLog()
{
    return false;
}
void CMainFrame::OnViewErrorlog()
{
    CDDIdeApp  * pApp     = (CDDIdeApp *)  AfxGetApp();
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    if ( pMainWnd != NULL)
    {
        pMainWnd->GetDocument();
        if ( pMainWnd->m_pDoc == NULL )
            return; // we can't get there from here
        // Get frame template
        CFrameTemplate* pChosenFrame = pApp->perrorlogtemplate;
        //22mar11   pApp->m_pDocTemplate->FindFrameTemplate("CErrLogView","CWindowChildFrame");
            
        // Create a new child frame based on the above template child and view class
        CMDIChildWnd* pChildFrame = ( CMDIChildWnd* ) pChosenFrame->CreateFrame();

        if (pChildFrame==NULL)
        {
            TRACE(_T("Warning: failed to create new frame.\n"));
            return;
        }

        pChosenFrame->InitialUpdateFrame(pChildFrame, pMainWnd->m_pDoc);

        CErrLogView *pWindowView =  (CErrLogView *) pChildFrame->GetActiveView();
    
#ifdef RLSDBG
        LOGIT(CLOG_LOG,"NOTE: doing Initialize on the view.\n");
#endif
        pErrorFrame = pChildFrame;      

//temp      pWindowView->Initialize();

#ifdef RLSDBG
        LOGIT(CLOG_LOG,"NOTE: doing SizeChildFrame. (exits OnViewErrorLog())\n");
#endif
//temp      SizeChildFrame(wParam, lParam);// PAW 03/03/09 parameters added
        pWindowView->Invalidate();//try to get grid to hide - 22aug07
    }// end have-document
}

/** some small functions to reduce the size and duplication in the windowsProc function ***/

bool CMainFrame::setDrawState(bool ds_ON)// returns previous state
{
    if ( ds_ON == treeRedrawing ) // nothing to do
    {
        TRACE("Mainframe already %s.\n",(treeRedrawing)?"Drawing":"Draw Suppressed");
        return treeRedrawing;
    }

    if (ds_ON)// ! treeRedrawing
    {
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:MainFrame. WndowProc - NOT redrawing: Enabling draw.\n");
#endif
        m_pDoc->UpdateAllViews( NULL, UPDATE_T_DRAW_DRAW, NULL );
        treeRedrawing = true;
    }
    else// ds_OFF && treeRedrawing
    {
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:MainFrame. WindowProc - drawing: setting DRAW_HOLD\n");
#endif
        m_pDoc->UpdateAllViews( NULL, UPDATE_T_DRAW_HOLD, NULL );
        treeRedrawing = false;
    }
    return (! ds_ON);
}


void CMainFrame::updateAll(long UpdateType, WPARAM wParam)// returns success/failure
{   
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:MainFrame. WndowProc - Update-All w/VAL|STRUCT & 0x%04x\n",wParam);
#endif   
    // stevev 12dec06 - start at the top menu
    if (m_pdrawMain)
    {
        m_pdrawMain->Update(UpdateType,wParam); // new update entry point
    }

    // this draws it all again..all views should be in drawMain!..
	m_pDoc->UpdateAllViews( NULL, UpdateType, (CObject *)wParam );
}


void CMainFrame::updateDialogs(UINT message, WPARAM wParam, LPARAM lParam)
{
    HWND hW;    
    hCmutex::hCmutexCntrl* pMtx = dlgRegistryMutex.getMutexControl("MnFrmRegistry");
            
    pMtx->aquireMutex(hCevent::FOREVER); // waits forever

    if ( dlgRegistry.size() > 0 )// echo to dialog tasks
    {
        for ( dlgRegIT iT = dlgRegistry.begin(); iT != dlgRegistry.end(); ++iT)
        {
            if ( (*iT) != NULL)
            {
                if ((*iT)->m_hWnd == NULL || ! ::IsWindow((*iT)->m_hWnd))
                {
                    TRACE(_T("No window in Registered Dialog (mainframe).\n"));
                }
                else
                {   
                    hW =    (*iT)->m_hWnd;
                    (*iT)->PostMessage(message, wParam, lParam);
                }
            }
        }
    }
    pMtx->relinquishMutex();
    
    if (pMtx)
        dlgRegistryMutex.returnMutexControl(pMtx);
}