/*************************************************************************************************
 *
 * $Workfile: DDLEDisplay.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		DDLEDisplay.cpp: implementation of the CDDLEDisplay class.
 */

#include "stdafx.h"
#include "SDC625.h"
#include "DDLEDisplay.h"
#include "EDisplayDlg.h"
#include "DDLMenu.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLEDisplay, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLEDisplay::CDDLEDisplay(hCitemBase* pIB):CDDLBase(pIB)
{
    m_nImage  = IMG_EDITDISPLAY;
	m_pEditDispPtr = NULL;
	// stevev 31jan06 - PAR 5631 - null pointer crash when editdisp invoked
	if ( pIB != NULL && pIB->getIType() == iT_EditDisplay)
	{
		m_pEditDispPtr = (hCeditDisplay*)m_pItemBase;
	}
}


// commented out stevev 01dec05--working wrong is worst than not working at all
//CDDLEDisplay::CDDLEDisplay(hCeditDisplay* pEditDisplay)
//{
//    m_nImage  = IMG_EDITDISPLAY;
//	m_pEditDispPtr = pEditDisplay;
//}

CDDLEDisplay::CDDLEDisplay(itemInfo_t& pIi):CDDLBase(pIi.p_Ib)
{
    m_nImage       = IMG_EDITDISPLAY;
	m_pEditDispPtr = (hCeditDisplay*) pIi.p_Ib;
	m_strName      = pIi.labelStr.c_str();// could be different from item label
	m_strNmValid   = pIi.labelFilled;
	
	m_strHelp      = pIi.help_Str.c_str();// stevev 28dec11
}

CDDLEDisplay::~CDDLEDisplay()
{

}

hCeditDisplay* CDDLEDisplay::baseItemPtr(void) 
{ 
	return m_pEditDispPtr;
};

void CDDLEDisplay::Execute()
{
	RETURNCODE rc;
	//call function to do pre edit actions
	if ( m_pEditDispPtr == NULL )
		return;// stevev 31jan06 - prevent crash on an error
	rc = m_pEditDispPtr->doPreEditActs();
	if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG,"Call to pre edit action returned an error.\n");
			return;
		}
	/* this change for CE should be a no-op for PC
    CEDisplayDlg m_eDisplayDlg(this);*/
// 01.01.00 start
	CWnd* pParent = AfxGetApp()->GetMainWnd();
	CEDisplayDlg m_eDisplayDlg(this, pParent);	// 01.01.00 added parent
// 01.01.00 end
  int nRet = m_eDisplayDlg.DoModal();

  // stevev 5aug08 - post edit actions must run if the pre-edit actions were successful
	//call function to do post edit actions
	rc = m_pEditDispPtr->doPstEditActs();
  if(IDOK == nRet)
  {
	if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG,"Call to post edit action returned an error.\n");
		}
  }
}

void CDDLEDisplay::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_EDITDISPLAY;    
//    pMenu->GetSubMenu(0)->CheckMenuItem( 0, 1 ? MF_CHECKED : MF_UNCHECKED );
}

/*Vibhor 170304: Start of Code*/

BOOL CDDLEDisplay::IsEditDisplay()
{
	return TRUE;
}

BOOL CDDLEDisplay::IsChanged()
{
	if(m_pEditDispPtr)
	{
		return m_pEditDispPtr->isChanged();
	}
	else
	{
		return FALSE;
	}
}

/*Vibhor 170304: End of Code*/

/*************************************************************************************************
 *
 *   $History: DDLEDisplay.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
