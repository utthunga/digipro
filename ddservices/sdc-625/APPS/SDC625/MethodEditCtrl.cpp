// MethodEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "MethodInterfacedefs.h"
#include "MethodEditCtrl.h"

#include "MethodsDefs.h"
#include <strstream> 
#include "logging.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*
#include "MethodsDefs.h"
//#include <strstrea.h> 
#include "strstream" 
*/
/////////////////////////////////////////////////////////////////////////////
// CMethodEditCtrl

CMethodEditCtrl::CMethodEditCtrl()
{
	m_nEditCtrlType = UNKNOWN_EDIT_BOX_TYPE;
	m_nPrecision    = DEFAULT_PRECISION;
	m_bIsUnsigned   = false;
	m_wMethodEditCtrlStyle = 0;
}

CMethodEditCtrl::~CMethodEditCtrl()
{
}


BEGIN_MESSAGE_MAP(CMethodEditCtrl, CEdit)
	//{{AFX_MSG_MAP(CMethodEditCtrl)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



LRESULT CMethodEditCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{	
	TRACE(_T("\t\tCMethodEditCntrl:message 0x%04x (in 0x%x)\n"), message,GetCurrentThreadId());
	return CEdit::WindowProc(message, wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CMethodEditCtrl message handlers

void CMethodEditCtrl::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
  BOOL bValidChar = FALSE;
  TRACE("\t\t>>Method Edit Control got char %d\n",nChar);

	int nFirstPos = 0,nLastPos = 0;
	CString szText = "";

	//if backspace
	if(nChar == '\b')
	{
		bValidChar = TRUE;
	}
	else
	{
		switch(m_nEditCtrlType)
		{
		case EDIT_BOX_TYPE_FLOAT:
			{
				if(((nChar >= '0') && (nChar <= '9'))) //if digits between 0 to 9
				{
					//this code is commented as it screws up float editbox editing
				/*	GetWindowText(szText);
					GetSel(nFirstPos,nLastPos);
					if(szText.GetLength() != (nLastPos - nFirstPos))
					{
						
						//check if the precision is not overflown
						if(CheckPrecision(szText))
						{
							bValidChar = TRUE;
						}
					}
					else*/
					{
							bValidChar = TRUE;
					}

				}
				else if(nChar == '.')//decimal point
				{
					//check if a decimal point is present. i.e.allow only one decimal point
					GetWindowText(szText);
					GetSel(nFirstPos,nLastPos);
					if(szText.GetLength() != (nLastPos - nFirstPos))
					{
						if(szText.Find('.') == -1) //no decimal point is found
						{
							bValidChar = TRUE;
						}
					}
					else
					{
						bValidChar = TRUE;
					}

				}
				else if(nChar == '-')//minus sign
				{
					//check if - is the first character to be entered
					GetWindowText(szText);
					GetSel(nFirstPos,nLastPos);
					if(szText.GetLength() == (nLastPos - nFirstPos))
					{
						bValidChar = TRUE;
					}
				}
				break;
			}
		case EDIT_BOX_TYPE_INTEGER:
			{
				if (_tcschr(m_VVstr,nChar) != NULL || m_VVstr[0] == 0)//default is all good
				//if(((nChar >= '0') && (nChar <= '9')))//numbers 
				{
					 bValidChar = TRUE;
				}
			/*  the whole thing is in the vallid-value string
				else if(nChar == '-')//minus sign
				{
					//check if '-' is the first character to be entered
					GetWindowText(szText);
					GetSel(nFirstPos,nLastPos);
					if(szText.GetLength() == (nLastPos - nFirstPos))
					{// entrie string selected
						bValidChar = TRUE;
					}
				}
			*/
				break;
			}
		case EDIT_BOX_TYPE_DATE:
		case EDIT_BOX_TYPE_TIME:
		case EDIT_BOX_TYPE_STRING://allow all characters
		case EDIT_BOX_TYPE_PASSWORD:
			{
					bValidChar = TRUE;
					break;
			}
		}// endswitch m_nEditCtrlType
	}
	
	if(bValidChar)
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
}



BOOL CMethodEditCtrl::Create(DWORD dwStyle, const RECT& rect,
    CWnd* pParentWnd, UINT nID)
{
  m_wMethodEditCtrlStyle = LOWORD(dwStyle);
  // figure out edit control style
  DWORD dwEditStyle = MAKELONG(ES_LEFT, HIWORD(dwStyle));
  return CWnd::Create(_T("EDIT"), NULL, dwEditStyle, rect, pParentWnd, nID);
}




void CMethodEditCtrl::SetEditCtrlType(int nEditType) 
{
  m_nEditCtrlType = nEditType;

}



BOOL CMethodEditCtrl::CheckPrecision(CString strValue) 
{
  if(m_nPrecision != -1)
  {
    if(m_nEditCtrlType == EDIT_BOX_TYPE_FLOAT)
    {
      int nDecimalPos = strValue.Find('.');
      if(nDecimalPos != -1)
      {
        CString strDecimal = strValue.Mid(nDecimalPos,(strValue.GetLength() - nDecimalPos - 1));
        if(strDecimal.GetLength() > m_nPrecision)
        {
          return FALSE;
        }
      }
    }
  }
  return TRUE;
}


void CMethodEditCtrl::SetPrecision(int nPrecision) 
{
  if(m_nEditCtrlType == EDIT_BOX_TYPE_FLOAT)
  {
    m_nPrecision = nPrecision;
  }
}

void CMethodEditCtrl::SetValidValueString(const wchar_t* pVVstr) 
{
	if( pVVstr != NULL )
	{
		wcsncpy(m_VVstr,pVVstr,VVSTRSIZE);
	}
}


void CMethodEditCtrl::SetEditFormatString(const wchar_t  *pStr) // stevev 26may09
{
	if( pStr != NULL )
	{
		wcsncpy(m_FmtStr,pStr,VVSTRSIZE);
	}
}

//BOOL CMethodEditCtrl::GetValue(VARIANT& varValue)
BOOL CMethodEditCtrl::GetValue(CValueVarient& varValue)
{
  CString str;
  GetWindowText(str);

  switch(m_nEditCtrlType)
  {
  case EDIT_BOX_TYPE_FLOAT:
    {
			//if user enters only these characters return false
			if(str == "." || str == "-" || str.IsEmpty())
				return FALSE;

      //varValue.vt = VT_R8;
//		varValue.bt = EDIT_BOX_TYPE_FLOAT;
//		varValue.vh_Value.dblVal = ConvertToDouble(str);
		varValue = ConvertToDouble(str);
      break;
    }
  case EDIT_BOX_TYPE_INTEGER:
    {
			//if user enters only these characters return false
			if(str == "-" || str.IsEmpty())
				return FALSE;

      //varValue.vt = VT_I4;
//		varValue.bt = EDIT_BOX_TYPE_INTEGER;
//		varValue.vh_Value.lVal = ConvertToLong(str);
		varValue = ConvertToLong(str);
		if (m_bIsUnsigned) varValue.vIsUnsigned = m_bIsUnsigned;
      break;
    }
  case EDIT_BOX_TYPE_DATE:
  case EDIT_BOX_TYPE_TIME:
  case EDIT_BOX_TYPE_STRING:
  case EDIT_BOX_TYPE_PASSWORD:
    {
      //varValue.vt = VT_BSTR;
//		varValue.bt = (enum EDIT_BOX_TYPE)m_nEditCtrlType;
      //varValue.bstrVal = str.AllocSysString();
//		varValue.vh_Value.pStrVal = new wchar_t[str.GetLength()+1];
//		_tcsncpy(varValue.vh_Value.pStrVal,LPCTSTR(str),str.GetLength());
//		varValue.vh_Value.pStrVal[str.GetLength()] = 0;
		wstring lS;
		lS = (LPCTSTR) str;
		varValue = lS;
      break;
    }

  };
  return TRUE;
}

double CMethodEditCtrl::ConvertToDouble(CString strText)
{
	if(strText.GetLength() > 20)
		{
			strText = strText.Left(20);
		}
  double dTemp = 0;
  char *pszValue;
  pszValue = new char [strText.GetLength() + 1];

#ifdef _UNICODE  
	UnicodeToASCII(strText, pszValue, strText.GetLength() + 1);
#else
	strcpy(pszValue,strText);
#endif

  // get double value out of text
  strstream str;
  str << pszValue;
  str >> dTemp;

 ARRAY_DELETE(pszValue);
 return dTemp;
}

// reworked 1jun09 - return value is cast to signed!!!
__int64 CMethodEditCtrl::ConvertToLong(CString strText)
{
	wchar_t pszValue[MAX_INT_DIGITS+1];
	wcsncpy(pszValue,(LPCTSTR)strText,MAX_INT_DIGITS);
	pszValue[MAX_INT_DIGITS] = 0;

	wchar_t fmt[VVSTRSIZE]     = L"%I64";
	if (m_FmtStr[0] !=  0 )// we have an edit format string
	{
		if (m_FmtStr[0] == L'%')// somebody else has modified the format appropriately
		{			
			wcsncpy(fmt,m_FmtStr,VVSTRSIZE);// fmt = m_FmtStr;
		}
		else
		{
			wcsncat(fmt,m_FmtStr,VVSTRSIZE);
		}
	}
	else
	{		
		if ( m_bIsUnsigned )
		{
			wcsncat(fmt,L"u",VVSTRSIZE);
		}
		else
		{// is signed
			wcsncat(fmt,L"d",VVSTRSIZE);
		}
	}

	if ( m_bIsUnsigned )
	{
		unsigned __int64 uTemp = 0;
		swscanf(pszValue,fmt,&uTemp);
		return (__int64)uTemp;
	}
	else
	{// is signed
		__int64 lTemp = 0;
		swscanf(pszValue,fmt,&lTemp);
		return lTemp;
	}
	return 0;// will never happen
}

// stevev 22may09 - get rid of uncontrolled VARIANTs BOOL CMethodEditCtrl::SetValue(VARIANT& varValue)
BOOL CMethodEditCtrl::SetValue(CValueVarient& varValue)
{
  CString str;
  if ( varValue.vIsValid )
	  {
	  switch(m_nEditCtrlType)
	  {
	  case EDIT_BOX_TYPE_FLOAT:
		{
				CString strPrecision = "%.",strTemp;
				strTemp .Format(_T("%d"),m_nPrecision);
				strPrecision += strTemp;
				strPrecision += _T("f");
		  str.Format(strPrecision, (double)varValue);//.vh_Value.dblVal);
		  SetWindowText(str);
		  break;
		}
	  case EDIT_BOX_TYPE_INTEGER:
		{
			wchar_t fmt[VVSTRSIZE]    = L"%";//I64";
			wchar_t tmpStr[VVSTRSIZE] = {0};
			if ( m_FmtStr[0] != 0 )
			{
				if (m_FmtStr[0] == L'%')// somebody has already got the edit format ready
				{
					
					wcsncpy(fmt,m_FmtStr,VVSTRSIZE);// fmt = m_FmtStr;
				}
				else
				{
					// CString seems to have issues with 64 bit numbers
					wchar_t tmpStr[VVSTRSIZE] = {0};
					wchar_t *pFmt = m_FmtStr;

					// stevev 04jun09 we need [flags][width][.precision][arg-type]type
					unsigned prelen = _tcscspn(m_FmtStr,_T("diouxX"));
					if ( prelen < wcslen(m_FmtStr) )
					{
						wcsncat(fmt, m_FmtStr,prelen);
						wcsncat(fmt, L"I64",3);
						pFmt += prelen;
						wcsncat(fmt, pFmt,VVSTRSIZE);
					}
					else // - no type exists
					{				
						wcsncat(fmt, m_FmtStr,VVSTRSIZE);
					}
				}
					
				if (varValue.vIsUnsigned)
					swprintf(tmpStr,fmt,(unsigned __int64)varValue);//.vh_Value.lVal);
				else // signed
					swprintf(tmpStr,fmt,(__int64)varValue);
				
				str = tmpStr;
			}
			else
			if (varValue.vIsUnsigned)
			{
				str.Format(L"%I64u",(unsigned __int64)varValue);
			}
			else// is signed
			{
				str.Format(L"%I64d",(__int64)varValue);//.vh_Value.lVal);
			}
			m_bIsUnsigned = varValue.vIsUnsigned;
		  SetWindowText(str);
		  break;
		}
	  case EDIT_BOX_TYPE_DATE:
	  case EDIT_BOX_TYPE_TIME:
	  case EDIT_BOX_TYPE_STRING: 
		{
		  str =  ((wstring)varValue).c_str();//.vh_Value.pStrVal;
		  SetWindowText(str);
		  break;
		} 
	  case EDIT_BOX_TYPE_PASSWORD:
		  SetWindowText(_T(""));
		  break;
	  }
  }
  else // not valid
  {	  
      SetWindowText(_T("Invalid"));
  }
  return TRUE;
}




