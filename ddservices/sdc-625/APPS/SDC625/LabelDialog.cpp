/**********************************************************************************************
 *
 * $Workfile: LabelDialog.cpp $
 * 29Jul11 - stevev
 *
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		7/29/11	sjv	started creation
 */

#include "stdafx.h"
#include "resource.h"
#include "LabelDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// CLabelDlg 

CLabelDlg::CLabelDlg(CString strCaption, CString strMessage, CWnd* pParent)
		  : sdCDialog(CLabelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLabelDlg)
	m_strMessage = strMessage;
    m_strCaption = strCaption;
	//}}AFX_DATA_INIT
}


void CLabelDlg::DoDataExchange(CDataExchange* pDX)
{
	sdCDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLabelDlg)
    DDX_Text(pDX, IDC_DISPLAY_TEXT, m_strMessage);
	//}}AFX_DATA_MAP
}

//////////////////////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CLabelDlg, sdCDialog)
	//{{AFX_MSG_MAP(CLabelDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////////////////////////
// CLabelDlg message handlers

BOOL CLabelDlg::OnInitDialog()
{
	sdCDialog::OnInitDialog();	

	UpdateData(FALSE);

	SetWindowText(m_strCaption);
    CWnd* pCtrl = GetDlgItem(IDC_DISPLAY_TEXT);
    //CRichEditCtrl* pCtrl = GetDlgItem(IDC_DISPLAY_EDIT);
    CRichEditCtrl* pRichEditCtrl = (CRichEditCtrl*)pCtrl;

    // Get the color othe parent dialog and use this as the background color
    // so the Rich Edit control is completely hidden (ie, removes the default
    // white background color)
    pRichEditCtrl -> SetBackgroundColor(FALSE,GetSysColor(COLOR_3DFACE));

	return TRUE; 
}


void CLabelDlg::OnOK()
{
	sdCDialog::OnOK();
}

void CLabelDlg::Display()  // Now using sdcDialog based objects - POB, 13Nove13
{
    sdcModl();// DoModal();
}

//////////////////////////////////////////////////////////////////////
// CHelpBox Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
CHelpBox::CHelpBox(CString strCaption, CString strHelp, CWnd* pParent)
: CLabelDlg(strCaption,strHelp, pParent)
{
	CString strCapTemp;
    CString strTemp;

	strCapTemp.LoadString(IDS_HELP_CAPTION);
	m_strCaption.Format(strCapTemp, strCaption);

    /* code removed 11/27/13 POB - No longer modifying the information in the DD.
       Use exactly what the tokenizer provides */
	
	/*if(strHelp.IsEmpty())
	{
		strHelp.LoadString(IDS_NO_HELP);;
	}
	else
	{
		strTemp = strHelp;

		int nLength = strTemp.GetLength();
		int nInsertIndex = MAX_HELPSTRING_LENGTH;

		if((nLength > MAX_HELPSTRING_LENGTH))
		{
			strHelp.Empty();
			do
			{
				if((strTemp.GetAt(MAX_HELPSTRING_LENGTH) != ' '))//if not white space
				{
					nInsertIndex = strTemp.Find(' ',MAX_HELPSTRING_LENGTH);
					if(nInsertIndex < 0) //if no white space take the string as is.
					{
						{
							strHelp += strTemp;
							break;
						}

					}
				}
				strTemp.SetAt(nInsertIndex,'\n');
				strHelp += strTemp.Left(nInsertIndex);
				strTemp = strTemp.Right(nLength - nInsertIndex);
				nLength = strTemp.GetLength();
				nInsertIndex = MAX_HELPSTRING_LENGTH;
				if(nLength < MAX_HELPSTRING_LENGTH)
				{
					strHelp += strTemp;
				}
			}
			while(nLength > MAX_HELPSTRING_LENGTH);
		}
	}*/

    //m_strMessage = strHelp;
}

CHelpBox::~CHelpBox()
{

}

/* code removed 11/27/13 POB -  Now using sdcDialog based objects.
   This is now called in base class CLabelDlg */
//void CHelpBox::Display()  
//{
    // Use the MainFrame's Message Box so that the user can not click on the application
    // when the dialog is being presented.
    //CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();
	//pMainWnd->MessageBox(m_strHelp,m_strItemCaption);  // POB, 13Nove13 - this modal can cause update problems
//}