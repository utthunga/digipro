/*************************************************************************************************
 *
 * $Workfile: HartTrans.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include HartTrans".h".		
 */

#if !defined(AFX_HARTTRANS_H__527180A5_87F9_4134_8A34_C79FF11A0F75__INCLUDED_)
#define AFX_HARTTRANS_H__527180A5_87F9_4134_8A34_C79FF11A0F75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ddbGeneral.h"
#include "HARTsupport.h"

/***************************************************************************
 * Global Class Definitions
 ***************************************************************************/

#define ST_OK                      0                    //SUCCESS

//#define APP_ERROR_BASE          2000
#define APP_ST_FALSE            (APP_ERROR_BASE +200)
#define APP_ST_TRUE             (APP_ERROR_BASE +201)
#define APP_ST_CHANGED          (APP_ERROR_BASE +202)
#define APP_ST_NOT_CHANGED      (APP_ERROR_BASE +203)
#define APP_ST_CANCELED         (APP_ERROR_BASE +204)   // User canceled
#define APP_ST_ABORTED          (APP_ERROR_BASE +205)
#define APP_ST_WORKING          (APP_ERROR_BASE +206)
#define APP_ER_FAILED           (APP_ERROR_BASE +207)   // COM worked,but result was not succesful
#define APP_ER_OLE_FAILED       (APP_ERROR_BASE +208)
#define APP_ER_OLE_NOT_FOUND    (APP_ERROR_BASE +209)   // COM error, method not excuted
#define APP_ER_TIMEOUT          (APP_ERROR_BASE +210)
#define APP_ER_CMD_FAILED       (APP_ERROR_BASE +211)
#define APP_ER_COMM_FAILURE     (APP_ERROR_BASE +212)   // Parity error, etc. from device
#define APP_ER_NORESPONSE       (APP_ERROR_BASE +213)   // Device did not respond
#define APP_ER_PARAMETER        (APP_ERROR_BASE +214)
#define APP_ER_BUF_FULL         (APP_ERROR_BASE +215)   // Buffer size was not big enough

//#define RETURNCODE              int

/*
 * Forward declaration of the doc to avoid a circular dependency
 */
class CDDIdeDoc;

class CHartTrans : public CObject  
{
public:
	CHartTrans();
    CHartTrans(const CHartTrans& HartTrans); // Copy Constructor
	virtual ~CHartTrans();

#ifndef XMTRDD
// Access Functions
    RETURNCODE Initialize();

// HART Transactions (Async)
    virtual RETURNCODE GetReply(int& transNum, int& cmdNum, BYTE* pData, BYTE dataCnt, cmdInfo_t& cmdStatus);
    RETURNCODE SendCmd(int transNum, int cmdNum, BYTE* pData, BYTE dataCnt, int timeout);

// HART Transactions (Sync)
    RETURNCODE SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

	RETURNCODE ParseCmd(BYTE* pData, BYTE& dataCnt, cmdInfo_t& cmdInfo);// fills the info struct
																		// moves the data up to the pointer
	RETURNCODE Connect(unsigned char addr = 0);

	void reset(void); // reset state/ device link

private:
    long m_handle;
	CDDIdeDoc * m_pDoc;
#endif
};

#endif // !defined(AFX_HARTTRANS_H__527180A5_87F9_4134_8A34_C79FF11A0F75__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: HartTrans.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
