
#ifndef _PARSED_EDIT
#define _PARSED_EDIT

class CParsedEdit : public CEdit
{
private:
//	using CEdit::Create;
	int nMax_Chars;
	int nDecimal_Limit;
	int nLeft_of_decimal;
protected:
	WORD  m_wParseStyle;      // C++ member data
public:
// Construction
	CParsedEdit();

	BOOL SubclassEdit(UINT nID, CWnd* pParent, WORD wParseStyle,int,int,int);

// Overridables
//	virtual void OnBadInput();

// Implementation
protected:
	//{{AFX_MSG(CParsedEdit)
	afx_msg void OnChar(UINT, UINT, UINT); // for character validation
//	afx_msg void OnVScroll(UINT, UINT, CScrollBar*); // for spin buttons
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////
// Parsed edit control sub-styles

#ifndef PES_UNDEFINED
#define PES_UNDEFINED		0x0000
#define PES_NUM_UNSIGNEDINT 0x0001
#define PES_NUM_INT			0x0002
#define PES_NUM_FLOAT       0x0003
#define PES_DATE			0x0004  /*Added by ANOOP */
#define PES_NUM_HEX         0x0005  /* added by stevev 3oct07*/
#define PES_TIME_FMT        0x0006  /* added by stevev 6aug08*/
#define PES_24TIME_FMT      0x0007  /* added by stevev 20jul09*/
#define PES_ALL             0xFFFF

inline int printfType2PES_type(wchar_t printfType)
{    if (printfType == L'f' || printfType == L'e' || printfType == L'g' ||
		 printfType == L'F' || printfType == L'E' || printfType == L'G'){return PES_NUM_FLOAT;}
else if ( printfType == L'd' ||  printfType == L'i')			{ return PES_NUM_INT;  }
else if ( printfType == L'u' )									{ return PES_NUM_UNSIGNEDINT;}
else if ( printfType == L'x' ||  printfType == L'X' )			{ return PES_NUM_HEX;  }
else if ( printfType == L's' )									{ return PES_ALL;  }
else if ( printfType == L't' )									{ return PES_TIME_FMT;  }
else if ( printfType == L'r' )									{ return PES_24TIME_FMT;  }
else if ( printfType == L'k' )	/*encoded type*/				{ return PES_DATE;  }
else															{ return PES_UNDEFINED;}
};


#endif  //PES_UNDEFINED


#endif // _PARSED_EDIT
