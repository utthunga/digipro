/**********************************************************************************************
 *
 * $Workfile: DDLexpansion.h $
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		The expansion classes are all grouped together.
 * #include "DDLexpansion.h"	
 */


#ifndef _DDLEXPANSION_
#define _DDLEXPANSION_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLMenu.h"
#include "ddbDevice.h"
#include "ddbImage.h"

#include "ddbChart.h"
#include "ddbGraph.h"

#include "ddbGrid.h"

class CDDLexpTbl
{
protected:
	unsigned m_itmQual;
	hCmenuItem* pMenuItem;// a ddbMenuItem delete when done...we own it

public:
	CDDLexpTbl();
	CDDLexpTbl(itemInfo_t& Iit);
	~CDDLexpTbl();

	unsigned getQual() {return m_itmQual;};
	hCddbDevice* getDevice(void);
	hCmenu* makeSingleItemMenu(void);
};


class CDDLImage : public CDDLexpTbl, public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLImage)

public:
	CDDLImage(hCitemBase* pIB=NULL);
	CDDLImage(itemInfo_t& pIi);

	virtual ~CDDLImage();

	void Execute();
    void LoadContextMenu(CMenu *pMenu, int& nPos);
	hCimage* baseItemPtr(void) { return m_pImagePtr; };
	
private:
	hCimage* m_pImagePtr;
};


class CDDLScope : public CDDLexpTbl, public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLScope)

public:

	CDDLScope(hCitemBase* pIB=NULL);
	CDDLScope(itemInfo_t& pIi);
	
	virtual ~CDDLScope();

// Special menu handling
	void Execute();
    void LoadContextMenu(CMenu *pMenu, int& nPos);
	hCitemBase* baseItemPtr(void) ;
	
private:
	hCitemBase* m_pItemPtr;
	bool isChart;
};


class CDDLGrid : public CDDLexpTbl, public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLGrid)

public:

	CDDLGrid(hCitemBase* pIB=NULL);
	CDDLGrid(itemInfo_t& pIi);
	
	virtual ~CDDLGrid();

// Special menu handling
	void Execute();
    void LoadContextMenu(CMenu *pMenu, int& nPos);
	hCgrid* baseItemPtr(void) ;
	
private:
	hCgrid* m_pGridPtr;
};

// This is for constant strings
class CDDLString : public CDDLexpTbl, public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLString)

public:

	// un-usable constructor    CDDLString(hCitemBase* pIB=NULL);
	CDDLString(itemInfo_t& pIi);
	
	virtual ~CDDLString();

// Special menu handling
	void Execute();
    void LoadContextMenu(CMenu* pMenu, int& nPos);
	
private:
};

#endif // _DDLEXPANSION_
/**********************************************************************************************
 * NOTES:
 **********************************************************************************************
 */
