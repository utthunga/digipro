#if !defined(AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)
#define AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// docCommInfc.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc thread

class CdocCommInfc : public CWinThread
{
	DECLARE_DYNCREATE(CdocCommInfc)
protected:
	CdocCommInfc();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CdocCommInfc)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CdocCommInfc();

	// Generated message map functions
	//{{AFX_MSG(CdocCommInfc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)
