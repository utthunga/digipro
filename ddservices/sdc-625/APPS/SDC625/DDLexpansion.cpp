/**********************************************************************************************
 *
 * Workfile: DDLexpansion.cpp 
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *      DDLexpansion.cpp: implementation of the eDDL expansion classes.
 */

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"

#include "DDLexpansion.h"

#include "SDC625_drawTypes.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLImage, CDDLBase)
IMPLEMENT_DYNAMIC( CDDLScope, CDDLBase)
IMPLEMENT_DYNAMIC( CDDLGrid,  CDDLBase)
IMPLEMENT_DYNAMIC( CDDLString,CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLImage::CDDLImage(hCitemBase* pIB):CDDLBase(pIB)
{
    m_nImage        = IMG_IMAGE;
    m_classExpanded = true;
    m_pImagePtr = NULL;
    
    if ( pIB != NULL && pIB->getIType() == iT_Image)
    {
        m_pImagePtr = (hCimage*)pIB;
    }
}


CDDLImage::CDDLImage(itemInfo_t& pIi):CDDLexpTbl(pIi),CDDLBase(pIi.p_Ib)
{
    m_nImage        = IMG_IMAGE;
    m_classExpanded = true;
    m_pImagePtr    = (hCimage*) pIi.p_Ib;

    m_strName      = pIi.labelStr.c_str();// could be different from item label
    m_strNmValid   = pIi.labelFilled;

    m_strHelp      = pIi.help_Str.c_str();
    // qual is in CDDLexpTbl
}

CDDLImage::~CDDLImage()
{
    return;
}

void CDDLImage::Execute()
{
    if ( m_pImagePtr == NULL || pMenuItem == NULL )
        return; // error
     
    //make psuedo menu
    hCmenu* pMenu = makeSingleItemMenu();

    //make a new drawdlg
    CdrawDlg* pDlg = new CdrawDlg(NULL);
    pDlg->m_pItem = pMenu;
    pDlg->m_symblID = pMenu->getID();
    pDlg->m_bSingleMenuItem = TRUE;

    //execute
    pDlg->Execute(NULL);
    
    delete pDlg;
}

void CDDLImage::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_ITEM;    
}


/*********************************************************************************************
 *
 *********************************************************************************************/
CDDLScope::CDDLScope(hCitemBase* pIB):CDDLBase(pIB)
{
    m_classExpanded = true;
    m_pItemPtr = NULL;
    
    if ( pIB != NULL && (pIB->getIType() == iT_Chart || pIB->getIType() == iT_Graph) )
    {
        m_pItemPtr = pIB;
        if (m_pItemPtr->getTypeVal() == iT_Chart)
        {
            m_nImage = IMG_CHART; // Added chart image, POB - 5/16/2014
            isChart = true;
        }
        else
        {
            m_nImage = IMG_GRAPH; // Added graph image, POB - 5/16/2014
            isChart = false;
        }
    }
}

CDDLScope::CDDLScope(itemInfo_t& pIi):CDDLexpTbl(pIi),CDDLBase(pIi.p_Ib)
{
    m_classExpanded = true;     // base
    m_pItemPtr    =  pIi.p_Ib;

    m_strName     = pIi.labelStr.c_str();// could be different from item label
    m_strNmValid  = pIi.labelFilled;

    m_strHelp     = pIi.help_Str.c_str();
    // qual is in CDDLexpTbl
    if (m_pItemPtr->getTypeVal() == iT_Chart)
    {
        m_nImage = IMG_CHART;  // Added chart image, POB - 5/16/2014
        isChart = true;
    }
    else
    {
        m_nImage = IMG_GRAPH;  // Added graph image, POB - 5/16/2014
        isChart = false;
    }
}

CDDLScope::~CDDLScope()
{

}

void CDDLScope::Execute()
{
    if ( m_pItemPtr == NULL  || pMenuItem == NULL )
        return;// error

    //make psuedo menu
    hCmenu* pMenu = makeSingleItemMenu();

    //make a new drawdlg
    CdrawDlg* pDlg = new CdrawDlg(NULL);
    pDlg->m_pItem = pMenu;
    pDlg->m_symblID = pMenu->getID();
    pDlg->m_bSingleMenuItem = TRUE;

    //execute
    pDlg->Execute(NULL);
    
    delete pDlg;
}


void CDDLScope::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_ITEM;    
}


/*********************************************************************************************
 *
 *********************************************************************************************/
CDDLGrid::CDDLGrid(hCitemBase* pIB):CDDLBase(pIB)
{
    m_nImage        = IMG_GRID;
    m_classExpanded = true;
    m_pGridPtr = NULL;
    
    if ( pIB != NULL && pIB->getIType() == iT_Grid)
    {
        m_pGridPtr = (hCgrid*)pIB;
    }
}

CDDLGrid::CDDLGrid(itemInfo_t& pIi):CDDLexpTbl(pIi),CDDLBase(pIi.p_Ib)
{
    m_nImage        = IMG_GRID;
    m_classExpanded = true;
    m_pGridPtr    =  (hCgrid*)pIi.p_Ib;

    m_strName     = pIi.labelStr.c_str();// could be different from item label
    m_strNmValid  = pIi.labelFilled;

    m_strHelp     = pIi.help_Str.c_str();
    // qual is in CDDLexpTbl
}

CDDLGrid::~CDDLGrid()
{

}

void CDDLGrid::Execute()
{
    if ( m_pGridPtr == NULL  || pMenuItem == NULL )
        return;// error

    //make psuedo menu
    hCmenu* pMenu = makeSingleItemMenu();

    //make a new drawdlg
    CdrawDlg* pDlg = new CdrawDlg(NULL);
    pDlg->m_pItem = pMenu;
    pDlg->m_symblID = pMenu->getID();
    pDlg->m_bSingleMenuItem = TRUE;

    //execute
    pDlg->Execute(NULL);
    
    delete pDlg;
}


void CDDLGrid::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_ITEM;    
}

/*********************************************************************************************
 *
 *********************************************************************************************/

CDDLString::CDDLString(itemInfo_t& pIi):CDDLexpTbl(pIi),CDDLBase((hCitemBase*)NULL)
{
    m_nImage        = IMG_STRING;
    m_classExpanded = true;

    m_strName     = pIi.labelStr.c_str();
    m_strNmValid  = pIi.labelFilled;

    m_strHelp     = L"";
    // qual is in CDDLexpTbl
}

CDDLString::~CDDLString()
{

}

void CDDLString::Execute()
{
    CWnd* pParent = AfxGetApp()->GetMainWnd();
//  m_eDisplayDlg = CEDisplayDlg CdrawDlg(NULL);    

//  int nRet = m_eDisplayDlg.DoModal();
}

void CDDLString::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_STRING;    
}

/*********************************************************************************************
 *
 *********************************************************************************************/
hCddbDevice* CDDLexpTbl::getDevice(void)
{
    CDDIdeDoc* pDoc = NULL;
    CMainFrame * pMainWnd = GETMAINFRAME;// get the actual document

    if (pMainWnd->GetDocument())
    {
        pDoc = pMainWnd->m_pDoc;
    }
    else
    {
        LOGIT(CERR_LOG, "ExpansionTable did not get a document\n");
        return NULL;
    }
    hCddbDevice* pDev = pDoc->pCurDev;//PCURDEV;

    if (pDev == NULL )
    {
        LOGIT(CERR_LOG, "ExpansionTable did not find a device in the document\n");
        return NULL ; /// error -  no device!!!!!!!!!!!!
    }
    return pDev;
}

hCmenu* CDDLexpTbl::makeSingleItemMenu(void)
{
    hCmenu*        p_retPtr =  NULL;

    hCddbDevice* pDev = getDevice();
    if ( pDev == NULL )
    {
        return NULL ; /// error -  no device!!!!!!!!!!!!
    }// else make it
    if ( pMenuItem == NULL )
    {
        LOGIT(CERR_LOG, "ExpansionTable does not have a menu item and can't make a menu.\n");
        return NULL ;
    }
    p_retPtr = (hCmenu*) (pDev->newPsuedoMenu(pMenuItem));
    p_retPtr->setStyle(menuStyleDialog);

    return p_retPtr;
}

CDDLexpTbl::CDDLexpTbl():m_itmQual(0), pMenuItem(NULL)   
{// empty
}
CDDLexpTbl::CDDLexpTbl(itemInfo_t& Iit) : m_itmQual(Iit.qual) 
{
    pMenuItem = (hCmenuItem*) new hCmenuItem(*Iit.pddbMenuItem);
}

CDDLexpTbl::~CDDLexpTbl()
{   
    if (pMenuItem)
    {
        delete pMenuItem;  
        pMenuItem = NULL;
    }
}
/*********************************************************************************************
 *
 *   
 * 
 *********************************************************************************************
 */
