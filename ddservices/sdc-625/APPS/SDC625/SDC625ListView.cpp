/*************************************************************************************************
 *
 * $Workfile: DDIdeListView.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDIdeListView.cpp : implementation file
 */
#pragma warning (disable : 4786)
#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"
#include "MainListCtrl.h"
//22mar11::>#include "SdcView.h"
#include "SDC625ListView.h"
#include "ddbDevice.h"
#include "DeviceDetails.h"
//#include "BitEnumDispValDlg.h"
#include "sdcDialog.h"
#include "Table_ChildFrm.h"
#include "LabelDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
//26aug08 #define LOG_MSGS 1
#else
#undef LOG_MSGS
#endif

extern CString rect2Registry(CRect& rect); // in sdc625.cpp

/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView

IMPLEMENT_DYNCREATE(CDDIdeListView, CView) //22mar11::>CSdcView)

CDDIdeListView::CDDIdeListView()
{
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView -instatiate.\n"));
#endif
    /*
     * set the CListCtrl attributes
     */
//    m_dwDefaultStyle |= LVS_OWNERDRAWFIXED | LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SINGLESEL;
    //22mar11::>m_pCtrl = NULL;
    m_pTableFrame = NULL;           //22mar11
    m_pCtrl = new CMainListCtrl();  //22mar11
    ctrlInitialized = false;
    m_pListedMenu = NULL;
}

CDDIdeListView::~CDDIdeListView()
{
    if (m_pCtrl != NULL)
        delete m_pCtrl;
}


BEGIN_MESSAGE_MAP(CDDIdeListView, CView) // 22mar11::>CSdcView)
    //{{AFX_MSG_MAP(CDDIdeListView)
    ON_WM_CREATE()
    ON_WM_DESTROY()
    ON_WM_SIZE()
    //Deadcode - removed ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk), POB - 4/13/2014
    //Deadcode - removed ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick), POB - 4/14/2014
    ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
    ON_COMMAND(ID_MENU_LABEL, OnMenuLabel)
    ON_COMMAND(ID_MENU_DISPLAY_VALUE, OnMenuDisplayValue)
    ON_COMMAND(ID_MENU_EXECUTE, OnMenuExecute)
    ON_UPDATE_COMMAND_UI(ID_MENU_EXECUTE, OnUpdateMenuExecute)
    ON_COMMAND(ID_DEVICE_DETAILS, OnDeviceDetails)
    ON_UPDATE_COMMAND_UI(ID_DEVICE_DETAILS, OnUpdateDeviceDetails)
    ON_WM_KEYDOWN()
//  ON_COMMAND(ID_VIEW_MORESTATUS, OnViewMorestatus)
//  ON_UPDATE_COMMAND_UI(ID_VIEW_MORESTATUS, OnUpdateViewMorestatus)
    //}}AFX_MSG_MAP 
END_MESSAGE_MAP()
/* moved to mainfrm:: */
//  ON_COMMAND(ID_VIEW_MORESTATUS, OnViewMorestatus)
//  ON_UPDATE_COMMAND_UI(ID_VIEW_MORESTATUS, OnUpdateViewMorestatus)
/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView drawing

void CDDIdeListView::OnDraw(CDC* pDC)
{
    CDDIdeDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    // TODO: add draw code here
    /* note from steve...if you are looking in here, you may want to check MainListCtrl.cpp */
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView diagnostics

#ifdef _DEBUG
void CDDIdeListView::AssertValid() const
{
    CView::AssertValid();
}

void CDDIdeListView::Dump(CDumpContext& dc) const
{
    CView::Dump(dc);
}

CDDIdeDoc* CDDIdeListView::GetDocument() // non-debug version is inline
{
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDDIdeDoc)));
    return (CDDIdeDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView message handlers

void CDDIdeListView::OnInitialUpdate() 
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView.OnInitialUpdate base class update.\n");
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView::OnInitialUpdate Entry\n"));
#endif
    //22mar11::>CSdcView::OnInitialUpdate();// CMainListCtrl is constructed here
    // moved to end 22mar11::>CView::OnInitialUpdate();
    if (m_pCtrl == NULL || ctrlInitialized)
        return;
    
    m_pCtrl->m_pTableFrame = m_pTableFrame;

    CString   strTitle;
    CRect     rect;
    GetClientRect(&rect);
    //22mar11::>CDDIdeDoc* pDoc = GetDocument();
    DWORD cntrlStyle = LVS_OWNERDRAWFIXED | LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SINGLESEL;
    m_pCtrl->Create(cntrlStyle, rect, this, 1);
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView.OnInitialUpdate created, showing window.\n");
    m_pCtrl->ShowWindow(SW_SHOW);
    
 //>>>22mar11 copied from sdcview verbatim
 //to fix PAR 5113
 //remove the header control style HDS_FULLDRAG so that the list control gets the HDN_ENDTRACK
 //message rather than the HDN_ITEMCHANGING message.
    BOOL bIsSet;
    if( SystemParametersInfo( SPI_GETDRAGFULLWINDOWS, 0, &bIsSet, 0 ) )
    {
        if( bIsSet )
        {
            HWND hWndHeaderCtrl = ::GetWindow( m_pCtrl->m_hWnd, GW_CHILD );
            if( hWndHeaderCtrl != NULL )
            {
                DWORD dwStyle = ::GetWindowLong( hWndHeaderCtrl ,GWL_STYLE );
                dwStyle &= ~HDS_FULLDRAG;
                ::SetWindowLong( hWndHeaderCtrl , GWL_STYLE, dwStyle );
            }
        }
    }

    /*
     * Clear any contents.
     */
    m_pCtrl->DeleteAllItems();
    while( m_pCtrl->DeleteColumn( 0 ) )
    {
    }

    /*
     * Get column widths from the registry, then rebuild the columns. honeywell branch
     */
    GetProfileSettings();
//    strTitle.LoadString( IDS_NAME ); /*Comment Added by Anoop for testing with CVS    */  
    strTitle = "Item";
    m_pCtrl->InsertColumn( COL_NAME, strTitle, LVCFMT_LEFT, m_anColWidth[COL_NAME], 0 );
//    strTitle.LoadString( IDS_ADDRESS );
    strTitle = "Value";
    m_pCtrl->InsertColumn( COL_DATA, strTitle, LVCFMT_RIGHT, m_anColWidth[COL_DATA], 0 );
//    strTitle.LoadString( IDS_TYPE );
    strTitle = "Units";
    m_pCtrl->InsertColumn( COL_UNITS, strTitle, LVCFMT_LEFT, m_anColWidth[COL_UNITS], 0 );
/*  Comment Added by Anoop for testing with CVS */
//    strTitle.LoadString( IDS_STATUS );
//    strTitle = "Column 4";
//    m_pCtrl->InsertColumn( COL_STATUS, strTitle, LVCFMT_LEFT, m_anColWidth[COL_STATUS], 0 );
    CView::OnInitialUpdate();
    ctrlInitialized = true;

//06dec05 - initializer should do this    OnUpdate( NULL, UPDATE_REBUILD_ALL, NULL );
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView::OnInitialUpdate - Exit\n"));
#endif  
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView.OnInitialUpdate Exit.\n");
}

/***************************************************************************
 * FUNCTION NAME: CDDIdeListView::OnUpdate
 *
 * DESCRIPTION:
 *   Function that updates the ListView whenever UpdateAllViews is called.
 * The hint that is passed in determines whether to rebuild the whole
 * ListView, or just one entry in it.
 *
 * ARGUMENTS:
 *   pSender
 *   lHint          one of the hint definitions
 *   pHint          pointer to base class that needs updating
 *
 * RETURNS:
 *   none
 ***************************************************************************/
void CDDIdeListView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
    CView::OnUpdate(pSender, lHint, pHint);// parent class

    CDDIdeDoc *pDoc   = GetDocument();
    CDDLBase  *pItem  = ( CDDLBase* )pHint;
    CDDLBase *pParent = NULL;
    CString strTemp;
    LVFINDINFO lvf;
    int  nIdx;

    CTableChildFrame* pTblChldFrm = static_cast<CTableChildFrame*>(m_pTableFrame);

    if (m_pCtrl == NULL)
    {
        LOGIT(CLOG_LOG,"MSG:ListView. OnUpdate - NO LIST (%d)\n",lHint);

        return;
    }
    if ( pDoc == NULL || pDoc->pCurDev == NULL || pTblChldFrm == NULL || 
         pDoc->pCurDev->devState == ds_Closing)
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"MSG:ListView.OnUpdate - Closing: not updating\n");
        return;
    }

    m_pCtrl->SetRedraw( FALSE );
    TRACE("Listview.onUpdate Listview (%d)\n",lHint);
    switch( lHint )
    {
/* 06dec05 - stevev converted to list update (normal selection changed)...
*        case UPDATE_REBUILD_ALL:  // ignore pHint
***/
    case UPDATE_LIST_VIEW:
        {
#ifdef LOG_MSGS
hCitemBase* pIB = NULL;
unsigned long ID = pItem->GetID();
string Nm;
if (pDoc->pCurDev->getItemBySymNumber(ID, &pIB) == SUCCESS && pIB != NULL )
{
    Nm = pIB->getName();
}else Nm = "";
LOGIT(CLOG_LOG,"MSG:ListView. OnUpdate - UpdateListView 0x%04x '%s' (0x%04x)\n",pItem,
        Nm.c_str(),ID );//->GetID());
if ( ID == 0x40f3 )// select_1 menu
{
    LOGIT(CLOG_LOG,"");
}
#endif
            TRACE("Listview.onUpdate update List.\n");
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView::OnUpdate UPDATE_LIST.\n");
            if(pTblChldFrm->getCurrentSelection() != NULL  && pDoc->pCurDev != NULL )
            {
                BuildListFrom( pTblChldFrm->getCurrentSelection() );
            }
            else
            {
                int icnt = m_pCtrl->GetItemCount();

                if (icnt > 0)
                {
                    int nItem    = -1;
                    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
                    if (pos != NULL)
                    {
                       nItem = m_pCtrl->GetNextSelectedItem(pos);
                    }
                    else
                    {
                       nItem = m_pCtrl->GetTopIndex();
                    }
                    //if (nItem)   
                    if (nItem >= 0)// timj 30sep10 allows you to double-click on folder in right-hand list to open it.
                        pItem = (CDDLBase*)(m_pCtrl->GetItemData( nItem ));
                }

                m_pCtrl->DeleteAllItems();
                m_pCtrl->ResetSelection();


                if (pItem && pDoc->pCurDev != NULL )
                {
                    // no longer needed to track...pTblChldFrm->m_pSelection = pItem;
                    BuildListFrom( pItem );
                }
            }
        }
        break;
    case UPDATE_STRUCTURE:
    case UPDATE_ADD_DEVICE:
    case UPDATE_TREE_VIEW:  // stevev 31jul07
        {
#ifdef LOG_MSGS
  hCitemBase* pIB = NULL;
  string Nm;
  unsigned long ID = (unsigned long)pItem;
  if (pDoc->pCurDev->getItemBySymNumber(ID, &pIB) == SUCCESS && pIB != NULL )
  {
    Nm = pIB->getName();
  }else Nm = "";
  LOGIT(CLOG_LOG,"MSG:ListView. OnUpdate - Update_STRUCT|ADDDEVICE - notused at this time"
      " update: 0x%04x "
      "\n              selected: 0x%04x '%s'\n",pItem, pTblChldFrm->getCurrentSelection()
      ->GetID(), Nm.c_str() );//->GetID());
  if ( ID == 0x417f)
  {
    LOGIT(CLOG_LOG,"");
  }
#endif

    TRACE("Listview.onUpdate update Structure.\n");
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView::OnUpdate got ADD_DEVICE, building list.\n");
        

            // note that there are many things that no longer exist in the current list
            // using one can ruin your whole day.
/********************** try 07jul06 * stevev ************************/
    if( pTblChldFrm->getCurrentSelection() != NULL  )
    {
        if ( pDoc->pCurDev      != NULL )
        {
            unsigned long uilng = pTblChldFrm->getCurrentSelection()->GetID();
            if ( uilng == (unsigned long)pItem )// when selection matches hint...regenerate
            {
                BuildListFrom( pTblChldFrm->getCurrentSelection() );
            }
            else
            {
                TRACE("List updateStruct skipped w/ selection = 0x%04x and hint = 0x%04x\n",
                                                                        uilng,(unsigned)pHint);
            }
        }
    }
/********************************************************************/
        }
        break;

/* code removed 13aug08 -paulb*/
    case UPDATE_REMOVE_DEVICE:
        {
            if( GetDocument()->pCurDev->getMEEdepth() <= 0 )
            { // do not delete if we are inside a method!!{ would delete the runner of the method}
    TRACE("Listview.onUpdate update Remove.\n");
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView::OnUpdate got REMOVE_DEVICE, deleting list.\n");
                //try the following
            
                m_pCtrl->DeleteAllItems();
                m_pCtrl->ResetSelection();
                m_pCtrl->Invalidate();// try to force an empty repaint - no worky
            }// else discard the message
        }
        break;
/* code removed 13aug08 -paulb*/
    case UPDATE_ONE_VALUE:
            lvf.flags = LVFI_PARAM;
            /* the following has pItem a symbol number and lvf.lParam a pointer to a CDDLBase
               this seldom matches in the find below.  I don't see what broke here but every other
               message handler is expecting a symbol number.
            */
            //lvf.lParam = (LPARAM)pItem;
            //nIdx = m_pCtrl->FindItem( &lvf, -1 );
            nIdx =  findBySymbolNumber((ITEM_ID)pItem);
            if( nIdx != -1 )
            {
                // success, so convert item number to ddvariable pointer
                pItem = (CDDLBase*)(m_pCtrl->GetItemData( nIdx ));
                strTemp = pItem->GetName();
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:ListView. OnUpdate - UpdateItem 0x%04x '%s'\n",pItem/*->GetID()*/,strTemp);
#endif
                m_pCtrl->SetItem( nIdx, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);
                
                if (pItem->IsDisplayValue())
                {
                    CDDLMenuItem *pMenu = (CDDLMenuItem *)pItem;

                    CDDLVariable *pVar = (CDDLVariable *)pMenu->m_item;                 
                /* stevev 18may09  I have no idea what the writer was thinking, 
                   but the following isn't DD, it's something else...
                    // Check for type Enum
                    vector<EnumTriad_t> eList;
                    
                    if (pVar->GetType() == TYPE_ENUM)
                    {
                        if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                        {
                            int nTemp = *pVar;
                            if (nTemp >= 0 && nTemp < eList.size())
                                strTemp = eList[nTemp].descS.c_str();
                        }
                    }
                    else
                ** end 18may09 removal ***/
                    {
                        strTemp = *pVar;   // Update data
                    }

                    m_pCtrl->SetItem(nIdx,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
                
                    if (pItem->IsVariable())
                    {
                        strTemp = pMenu->GetUnitStr();
                        m_pCtrl->SetItem(nIdx, COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
                    }

                }

            }
            break;
    case UPDATE_DELETE_ITEM:    // added 25may11
        {// we have to get the item indicated out of the menu system so Draw doesn't choke
            // That  means removing it from the tree item CDDLmenu AND the list
            CDDLMenuItem    *pMItem;
            CDDLBase        *pItem = NULL, *pMenu = NULL;
            int             nIdx             = 0;
            itemID_t        targetItemNumber = (itemID_t)pHint;
            
            TRACE("Listview.onUpdate update Delete: 0x%04x.\n",targetItemNumber);
            LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"ListView.OnUpdate:Element to Delete: 0x%04x\n",
                                                                            targetItemNumber);
            pMenu = m_pTableFrame->getCurrentSelection();
#ifdef _DEBUG
            if (  pMenu->IsKindOf( RUNTIME_CLASS( CDDLMenuItem )) && pMenu->IsMenu() )
            {
                pMItem = (CDDLMenuItem*) pMenu;
                pMenu  = pMItem->m_item;
                
                if (pMenu != m_pListedMenu)
                { 
                    TRACE("Listview MENU mismatch!\n");
                    m_pListedMenu = (CDDLMenu*) pMenu;
                }
            }
#endif
            // we only have responsibility for the list we have open.           
            if( m_pListedMenu != NULL  )
            {
                CDDLBasePtrList_t *pcl = &( m_pListedMenu->GetChildList() );
                POSITION pos = pcl->GetHeadPosition();
                for( nIdx = 0; pos != NULL; nIdx++ )
                {                                                 // 06apr11 - now
                    pMItem = (CDDLMenuItem*)(pcl->GetNext( pos ));// always a menu item
                    
                    if (pMItem->m_item != NULL)
                    {
                        pItem = pMItem->m_item;
                        if ( pItem->m_pItemBase->getID() == targetItemNumber )
                        {
                            pcl->RemoveAt(pos);
                            LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"ListView.OnUpdate:Element Deleted: "
                                                                L"0x%04x\n",targetItemNumber);
                            BuildListFrom( m_pListedMenu );
                            break; // out of for loop w/ index set...assumes one per menu...
                        }
                    }
                }//next
            }
            else
            {
                DEBUGLOG(CLOG_LOG,L"Deleting an item when we have no list.\n");
            }
            LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"ListView.OnUpdate:Element Deleted exit\n");
        }
        break;
/* code removed 13aug08 -paulb*/
        default:
            {
#ifdef LOG_MSGS
LOGIT(CLOG_LOG|CERR_LOG,"MSG:ListView. OnUpdate - UnknownMsg 0x%04x\n",(int)pItem);
#endif
            }
            break;
    }
    TRACE("Listview.onUpdate Exit.\n");

    m_pCtrl->SetRedraw( TRUE );
#ifdef STARTSEQ_LOG
//  TRACE(_T("CDDIdeListView::OnUpdate - Exit\n"));
#endif
}


#ifdef FNDMSG
LRESULT CDDIdeListView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{
if ( message == WM_LBUTTONDBLCLK || message == WM_RBUTTONDBLCLK)
{
    DWORD f = lParam;
}

    if (message==FNDMSG)
    {
        LOGIT(CLOG_LOG,"~^");
    } 
    //22mar11::> return CSdcView::WindowProc(message,wParam,lParam);
    return CView::WindowProc(message,wParam,lParam);
}
#endif

// Note: this routine was reworked april 4-6,2011  see previous versions for changes
//       This is now good for all types and qualifiers
// 06apr11 - all entries are now menuitems, that can point to a menu or other types
//           childlist is filled int CDDIdeTreeView::AddSubMenu(  )
void CDDIdeListView::BuildListFrom(CDDLBase *pBase)
{
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView::BuildListFrom Entry 0x%04x (%s)\n"),pBase->GetID(),
        (LPCTSTR)(pBase->GetName()));
#endif
#ifdef _DEBUG
	struct _timeb NewBuildTime;
	_ftime( & NewBuildTime );
	LOGIT(CLOG_LOG," %I64d.%hd  --  New ListView Build for %#04x  (%s)\n",NewBuildTime.time,
		NewBuildTime.millitm, pBase->GetID(), (LPCTSTR)(pBase->GetName()));
#endif
    int             nRow = 0;
    POSITION        pos;
    CDDLMenuItem*   pMItem = NULL; //menu items
    LV_ITEM         lv_item;
    CString strTemp, unitTemp;
    bool            inReview = false;

    /*
     * Get m_pCtrl control ready for redraw
     */
    m_pCtrl->DeleteAllItems();
    m_pCtrl->ResetSelection();


    if( NULL != pBase )
    { 
#ifdef _DEBUG
        unsigned menuID = pBase->GetID();
#endif
        /* stevev 25jan12 - this doesn't work when a menu is passed in instead of an item
        // added stevev 25may11
        CDDLMenuItem* pMI = (CDDLMenuItem*)pBase;
        if (pMI->m_item != NULL )
        { 
            if (pMI->IsMenu() && pMI->m_item->IsMenu())
            {
                m_pListedMenu = (CDDLMenu*) ((CDDLMenuItem*)pBase)->m_item;
            }
            else
            {
                LOGIT(CLOG_LOG," Listview is building a list from a NON menu item.\n");
                m_pListedMenu = (CDDLMenu*) ((CDDLMenuItem*)pBase)->m_item;
            };
        }// end 25may11
        end 25jan12 removal ---- start 25jan12 additions::>  */
        m_pListedMenu = pBase->getMenuPtr();
        if ( m_pListedMenu == NULL )
        {
            if ( ! pBase->IsTop() )
            {
                LOGIT(CLOG_LOG," Listview is trying to build a list from a NON menu item.\n");
            }
            return; // with no list
        }


        inReview = (pBase->IsReview())?true:false;

        CDDLBasePtrList_t* pll = &(pBase->GetChildList());
        /*
         * How many objects in this CList?
         */
        m_pCtrl->SetItemCount( pll->GetCount() );

        /*
         * Initialize the LV_ITEM structure that's used to populate the m_pCtrl
         */
        lv_item.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
        lv_item.iSubItem = COL_NAME;

        /*
         * Get first object in the Menu and keep popping them off til they're gone
         */
        pos = pll->GetHeadPosition();
        for( int nIdx = 0; pos != NULL; nIdx++ )
        {                                                                // 06apr11 - now
            pMItem = (CDDLMenuItem*)(pll->GetNext( pos ));// always a menu item

            if (inReview && !(pMItem->IsVariable() || pMItem->IsMenu()) )// only ones allowed
            {
                continue;// just skip it
            }
            
            if (pMItem->IsValid())
            {
        // COL_NAME-------------------------------------
                // used if not no-label
                if (pMItem->IsMenu() || (! pMItem->HasNoLabel()) )
                {
                    strTemp = pMItem->GetName();
                }
                else // it's a valid menu-item with no-label...we have to have a row
                // images and variables can have NO_LABEL...we will assume that the tok
                // checks the syntax on these
                {
                    strTemp = L"";
                }
                lv_item.iItem = 0; //Zero - Last in, last in the m_pCtrl  /*nIdx*/
                lv_item.pszText = (LPTSTR)((LPCTSTR)strTemp);
                lv_item.lParam  = (LPARAM)pMItem;//menuitem
#ifdef _DEBUG
                pMItem->parentID = menuID;
                pMItem->srcLocation="ListView insert";
#endif
                lv_item.iImage  = pMItem->GetImage(); // Added function to get image ID; POB - 5/16/2014 
                TRACE(_T("              ::Insert  0x%04x ( %p )"),pMItem->GetID(),pMItem);
                nRow = m_pCtrl->InsertItem(&lv_item);

        // COL_DATA ----------------------------------------    
                if ( (! pMItem->IsMenu()) && pMItem->m_item != NULL )// see if we have data
                {// it's a menu-item that is not a constant string
                    // only variables can hold data
                    if ( pMItem->IsVariable() )
                    {   
                        TRACE(_T(" V\n"));              
                        CDDLVariable *pVar = (CDDLVariable *)pMItem->m_item;
                        if ( pMItem->HasNoLabel() || pMItem->IsDisplayValue() )
                        {// no-label forces value   or value gives value
                            strTemp = *pVar;   // Update data value
                            // units only make sense when there is a value
        // COL_UNITS ----------------------------------------   
                            if (pMItem->HasNoUnit())
                            {
                                unitTemp = L"";
                            }
                            else // does have units
                            {
                                unitTemp = pMItem->GetUnitStr();
                            }
                        }
                        else // do not display the value
                        {
                            strTemp = L"";
                        }

                        m_pCtrl->SetItem(nRow,COL_DATA , LVIF_TEXT, strTemp,  0,0,0,0);
                        m_pCtrl->SetItem(nRow,COL_UNITS, LVIF_TEXT, unitTemp ,0,0,0,0);
                    }// else not-a-variable...value and units don't count
                    else                        
                        TRACE(_T(" nV\n")); 
                }// else it's a menu or constant string
                else                        
                    TRACE(_T(" M/S\n"));    
            }// else not valid, skip it
            else
            {
                DEBUGLOG(CLOG_LOG,"Menu item not valid.\n");
            }
        }//next in the childlist
    }// else - nothing to build from, just leave
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView::BuildListFrom - Exit\n"));
#endif
}

int CDDIdeListView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
#ifdef STARTSEQ_LOG
    TRACE(_T("CDDIdeListView::OnCreate - Only\n"));
#endif
    if (CView::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    return 0;
}

/***************************************************************************
 * FUNCTION NAME: CDDIdeListView::GetProfileSettings
 *
 * DESCRIPTION:
 *   Uses GetProfileString to read column widths from the registry. Since we
 * used SetRegistryKey to set up a registry section, GetProfileString will
 * look there rather than going to an ini file. The column widths are stored
 * as a string in the registry. e.g. 190,100,80,200. Use a CString to collect
 * and parse each column's width (looking for the , ), then convert to integers
 * and store in an array that is used by the OnInitialUpdate function where
 * the column widths actually get set
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   none
 ***************************************************************************/
void CDDIdeListView::GetProfileSettings( )
{
     CWinApp   *pApp = AfxGetApp();
     CString   strColumnWidths;
     CString   strOneWidth;
     CString   strDefaultWidths = _T("");
     char      szDefaultWidths[MAX_REG_STRLEN];
     CRect     rect;
     int       nIndex, nLength;

     /*
      * Set default column widths
      */
     GetWindowRect( &rect );
     for( nIndex = 0; nIndex < MAX_LIST_COLUMNS; nIndex++ )
     {
          m_anColWidth[nIndex] = (rect.right - rect.left) / MAX_LIST_COLUMNS;
          _itoa( m_anColWidth[nIndex], szDefaultWidths, 10 );
          strDefaultWidths += szDefaultWidths;
          strDefaultWidths += ",";
     }

     /*
      * Get column widths string from the registry. If the registry is corrupted,
      * or there is no entry there, the defaults will come back
      */
     strColumnWidths = pApp->GetProfileString( SETTINGS, COL_WIDTHS, strDefaultWidths );

     /*
      * Parse the Cstring for widths, and convert to integers
      */
     for( int j = 0; j < MAX_LIST_COLUMNS; j++ )
     {
          nLength = strColumnWidths.GetLength();
          nIndex  = strColumnWidths.Find( _T(",") );
          strOneWidth = strColumnWidths.Left( nIndex );
          m_anColWidth[j] = _ttoi( (LPCTSTR)strOneWidth );
          strColumnWidths = strColumnWidths.Right( nLength - ( nIndex + 1 ) );
     }
}


/***************************************************************************
 * FUNCTION NAME: CDDIdeListView::WriteProfileSettings
 *
 * DESCRIPTION:
 *   Calls GetColumnWidthString and GetMainWindowSizeString with pointers
 *   to CStrings to receive the strings, then writes the strings to the
 *   registry. This function will be called by the List View's DestroyWindow
 *   to save column widths, and the size of the main application window
 *   before shutting down.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   none
 ***************************************************************************/
void CDDIdeListView::WriteProfileSettings()
{
     CWinApp *pApp = AfxGetApp();
     CString strColWidths = _T("");
     CString strWndSize = _T("");

     GetColumnWidthString( &strColWidths );
     GetMainWindowSizeString( &strWndSize );

     pApp->WriteProfileString( SETTINGS, COL_WIDTHS, strColWidths );
     pApp->WriteProfileString( SETTINGS, MAIN_APP_WND_SIZE, strWndSize );
     DEBUGLOG(CLOG_LOG,"Write Registry: %ls with '%ls'\n",MAIN_APP_WND_SIZE,strWndSize);
}


/***************************************************************************
 * FUNCTION NAME: CDDIdeListView::GetColumnWidthString
 *
 * DESCRIPTION:
 *       Does the work to get the List View's column widths, and store them as
 *      a contiguous string in pstrColWidths separated by commas.
 *
 * ARGUMENTS:
 *   *pstrColWidths
 *
 * RETURNS:
 *   none
 *
 ***************************************************************************/
void CDDIdeListView::GetColumnWidthString( CString *pstrColWidths )
{
     int       nWidth;
     char      szWidths[MAX_REG_STRLEN];


     ASSERT( ::IsWindow( m_pCtrl->m_hWnd ) );

     for( int i = 0; i < MAX_LIST_COLUMNS; i++ )
     {
          nWidth = m_pCtrl->GetColumnWidth( i );
          _itoa( nWidth, szWidths, 10 );
          *pstrColWidths += szWidths;
          *pstrColWidths += ",";
     }
}

/***************************************************************************
 * FUNCTION NAME: CDDIdeListView::GetMainWindowSizeString
 *
 * DESCRIPTION:
 *     Stores the CRect coordinates of the main application window in a
 *   CString so it can be stored in the registry. The coordinates will
 *   be written as a contiguous string separated by commas in this order:
 *
 *                       "left,right,top,bottom,"
 *
 * ARGUMENTS:
 *   *pstrWndSize
 *
 * RETURNS:
 *   none
 *
 ***************************************************************************/
void CDDIdeListView::GetMainWindowSizeString( CString *pstrWndSize )
{
     CRect rect;
    // wchar_t  szSize[MAX_REG_STRLEN];
     CWnd  *pWnd = AfxGetApp()->GetMainWnd();

     /*
      * Need the main application's window rect
      */
     pWnd->GetWindowRect( &rect );

     *pstrWndSize = rect2Registry(rect);
     /*
     _itow( rect.left, szSize, 10 );
     *pstrWndSize = szSize;
     *pstrWndSize += "|";

     _itow( rect.top, szSize, 10 );
     *pstrWndSize += szSize;
     *pstrWndSize += "|";

     _itow( rect.right, szSize, 10 );
     *pstrWndSize += szSize;
     *pstrWndSize += "|";

     _itow( rect.bottom, szSize, 10 );
     *pstrWndSize += szSize;
     *pstrWndSize += "|";
     */
}

void CDDIdeListView::OnDestroy() 
{
    CView::OnDestroy();
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< ListView::OnDestroy recording position.\n");
    // NOTE: Add your message handler code here
    WriteProfileSettings();
}

// Deadcode - removed void CDDIdeListView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult), POB - 4/13/2014
// Deadcode - void CDDIdeListView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult),  POB - 4/14/2014

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
    void CDDIdeListView::OnMenuHelp() 
    {
    // New code added by Jeyabal
    if (m_pCtrl == NULL)
    return;

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    int nItem = m_pCtrl->GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);

        if (pBase)
        {
            pBase -> DisplayHelp();
        }

        /* code removed 13aug08 -paulb*/
    }
}

void CDDIdeListView::OnMenuLabel() 
{
    if (m_pCtrl == NULL)
      return;

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    int nItem = m_pCtrl->GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);

        if (pBase)
        {
            pBase -> DisplayLabel();
        }
    }
}

void CDDIdeListView::OnMenuExecute() 
{
    CDDIdeDoc *pDoc = GetDocument();    
    CTableChildFrame* pTblChldFrm = static_cast<CTableChildFrame*>(m_pTableFrame);
    // New code added by Jeyabal
    if (m_pCtrl == NULL || pTblChldFrm == NULL || pDoc == NULL)
        return;

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    int nItem = m_pCtrl->GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);
        CString strName = pBase->GetName();
        CString strHelp = pBase->GetHelp();

        // treeview tracks selection now
        // crashing and refreshing when editing a variable by right click                  

        if(pBase->IsMethod())
            pBase->Execute(strName,strHelp);
        else
            pBase->Execute();

        if(pBase->IsMenu())
        {// this must be in the current selection...
            //tree view tracks selection now
            pTblChldFrm->setCurrentSelection(pBase,nItem);
            // 06dec05 stevev     was   UPDATE_REBUILD_LIST
            pDoc->UpdateAllViews( NULL, UPDATE_LIST_VIEW, pTblChldFrm->getCurrentSelection() );
        }
        /*<START>Added by ANOOP 14APR2004 to fix the issue of crashing and refreshing when editing a variable by right click */                 
        else if(pBase->IsVariable())
        {
            CDDLMenuItem* pMenuItem = (CDDLMenuItem *)pBase;
            CDDLVariable* pVar = (CDDLVariable *)pMenuItem->m_item;
            if (pVar)
            {
                if (pVar->IsChanged())
                {// 06dec05 stevev        was   UPDATE_REBUILD_LIST
                    pDoc->UpdateAllViews( NULL, UPDATE_LIST_VIEW,   
                                                           pTblChldFrm->getCurrentSelection());
                }
            }
        }
        /*<END>Added by ANOOP 14APR2004 to fix the issue of crashing and refreshing when editing a variable by right click */                   

    }
    // End - New code added by Jeyabal
}

void CDDIdeListView::OnUpdateMenuExecute(CCmdUI* pCmdUI) 
{
    if (m_pCtrl == NULL)
        return;

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    int nItem = m_pCtrl->GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);
        
        if (pBase)
        {
            pBase->OnUpdateMenuExecute(pCmdUI);
        }
    }
}


void CDDIdeListView::OnMenuDisplayValue()
{
    if (m_pCtrl == NULL)
        return;

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    int nItem = m_pCtrl->GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);
        
        if (pBase)
        {
            pBase->DisplayValue();
        }
    }
}

// Deleted commnted out code, POB - 4/14/2014
void CDDIdeListView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    int nIndex = 0;
    LRESULT result = 0;

    if (m_pCtrl == NULL)
    {
        return;
    }

    if (m_pCtrl->GetSelectionMark() == -1)
    {
        m_pCtrl->SetSelectionMark(0);
        m_pCtrl->SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
    }

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    
    if (pos != NULL)
    {
        nIndex = m_pCtrl->GetNextSelectedItem(pos);
    }

    switch(nChar)
    {
        case 40:    // down arrow
            if (nIndex < m_pCtrl->GetItemCount())
            {
                m_pCtrl->SetItemState((nIndex + 1), LVIS_SELECTED, LVIS_SELECTED);
            }
            break;

        case 38:    // Up Arrow
            if (nIndex > 0)
            {
                m_pCtrl->SetItemState((nIndex - 1), LVIS_SELECTED, LVIS_SELECTED);
            }
            break;
    
        case 13:    // Call WM_LBUTTONDBLCLK of CMainListCtrl.  
            m_pCtrl->SendMessage(WM_LBUTTONDBLCLK); // This fixed "enter" defect, POB - 4/14/2014
            break;
    }

    Invalidate();
    UpdateWindow();

    CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

/* * * Moved to MainFrm: OnViewMorestatus, & OnUpdateViewMorestatus * * */

void CDDIdeListView::OnUpdateDeviceDetails(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(FALSE);

    POSITION pos = m_pCtrl->GetFirstSelectedItemPosition();
    if (pos != NULL)
    {
        int nItem = m_pCtrl->GetNextSelectedItem(pos);
        if (nItem != -1)
        {
            CDDLBase *pBase = (CDDLBase *)m_pCtrl->GetItemData(nItem);
            if (pBase)
            {
    /* VMKP Modified on 210104 */
                if ( ROOT_MENU == pBase->GetID()) 
    /*          CString strTmp = pBase->GetName();
                if ( 0 == strTmp.CompareNoCase("Online") ) */
                    pCmdUI->Enable(TRUE);
    /* VMKP Modified on 210104 */
            }
        }   
    }
}

void CDDIdeListView::OnDeviceDetails() 
{
    CDeviceDetails  dDeviceDetails(GetDocument());
    dDeviceDetails.DoModal();
}

/* Code moved to CDDLVariable::DisplayValue(), POB - 11/27/13 */
    
int CDDIdeListView::findBySymbolNumber(ITEM_ID symNum)
{
    int idIDX = -1;
    int lstlen= m_pCtrl->GetItemCount( );
    int cnt, idx;
    CDDLBase* pDDLB = NULL;

    if (symNum == 0 || lstlen <= 0)
        return -1;

    cnt = 
    idx = 0;
    while (idx > -1 && cnt < lstlen)
    {
        pDDLB = (CDDLBase*)(m_pCtrl->GetItemData( idx ));// get the LVI_ITEM's lParam value
        if (pDDLB != (CDDLBase*)0xffffffff)// not the unfound return value
        {
            if (pDDLB->GetID() == symNum)
            {
                idIDX = idx;
                break;// out of while stmnt-we;re done
            }
        }
        cnt++;
        idx = m_pCtrl->GetNextItem( idx, LVNI_ALL ) ;//LVNI_ALL is next by index
    }
    return idIDX;
}

// 22mar11

void CDDIdeListView::OnSize(UINT nType, int cx, int cy) 
{
    CView::OnSize(nType, cx, cy);

    if (m_pCtrl != NULL && ctrlInitialized)
        m_pCtrl->MoveWindow (0, 0, cx, cy);
}

//Remove the item from the Display/ListView - Leave it stay in the childlist

int CDDIdeListView::remove(ITEM_ID symbolID) 
{
    int index = findBySymbolNumber(symbolID);
    if (index < 0)
        return 0;// we removed none

    //
    if ( m_pCtrl->DeleteItem(index ) )// true at success
        return 1;// success
    else
        return 0;// failure

}
