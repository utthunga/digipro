// DevStatPageOne.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "Command48_PropSheet.h"
#include "DevStatPageOne.h"
#include "DevCommStatPropPage.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _WIN32_WCE	
 #define GAP1	10
 #define GAP2	16
#else // is CE	
 #define GAP1	 5
 #define GAP2	42
#endif
/////////////////////////////////////////////////////////////////////////////
// CDevStatPageOne property page

IMPLEMENT_DYNCREATE(CDevStatPageOne, CPropertyPage)

CDevStatPageOne::CDevStatPageOne() : CPropertyPage(CDevStatPageOne::IDD)
{
	//{{AFX_DATA_INIT(CDevStatPageOne)
	bSuppress_MsgDisplay=FALSE;
	bStaticCtrl_Created=FALSE;
	bAddMsgSuppressed=FALSE;

	for (unsigned int nCount = 0; nCount < 20 ; nCount++)
	{
		m_pBtnArray[nCount] = NULL;
		m_pStaticArray[nCount] = NULL;
	}
	//}}AFX_DATA_INIT
}

CDevStatPageOne::CDevStatPageOne(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> & m_Arr,CString &szByteIdent  ,BOOL bAddMsgSuppress )
	: CPropertyPage(CDevStatPageOne::IDD)
{
	//{{AFX_DATA_INIT(CDevStatPageOne)

	CMD48_RESPONSE cmdresp;

	bSuppress_MsgDisplay=FALSE;
	bStaticCtrl_Created=FALSE;
	bInitialCreationDone=FALSE;
	bAddMsgSuppressed=FALSE;
	m_szByteIdentifier="";	//Added by ANOOP 02MAR2004

	for(int i=0; i < m_Arr.GetSize(); i++ )
	{
		cmdresp=m_Arr.GetAt(i);
		if  ( cmdresp.szByteIdentity == szByteIdent )
		{
			m_szByteIdentifier = szByteIdent;	//Added by ANOOP 02MAR2004
			cmd48_PreviousArray.Add(cmdresp);
			cmd48_PresentArray.Add(cmdresp);	//Added by ANOOP 02MAR2004
		}
	}
			
	int nSize=cmd48_PreviousArray.GetSize();
	nSize = min(nSize,MAX_RANGE_VAL);// stevev 12feb14 - array bounds error
	for (int nTmpCount = 0; nTmpCount < nSize ; nTmpCount++)
	{
		m_pBtnArray[nTmpCount] = NULL;
		m_pStaticArray[nTmpCount] = NULL;
	}
	
		
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}	


CDevStatPageOne::~CDevStatPageOne()
{
	int nSize=cmd48_PreviousArray.GetSize();
	nSize = min(nSize,MAX_RANGE_VAL);// stevev 12feb14 - array bounds error
	for (int nCount = 0; nCount < nSize; nCount++)
	{
		RAZE( m_pBtnArray[nCount] );			
		
		RAZE( m_pStaticArray[nCount] );
	}
}

void CDevStatPageOne::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDevStatPageOne)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDevStatPageOne, CPropertyPage)
	//{{AFX_MSG_MAP(CDevStatPageOne)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDevStatPageOne message handlers

BOOL CDevStatPageOne::OnInitDialog() 
{
//	CPropertyPage::OnInitDialog();
#ifndef _WIN32_WCE	
	CRect rectDevStatus1(40,72,290,89);
	CRect rectDevStatus2(67,72,290,89);
#else // is CE
	CRect rectDevStatus1(10, 21, 260, 63/*40,72,290,89 PAW 14/08/09*/);
	CRect rectDevStatus2(30, 5, 260, 47/*67,72,290,89 PAW 14/08/09*/);
#endif	

	CRect rectCommStatus1(348,65,558,89);
	CRect rectCommStatus2(375,65,640,89);	

	CMD48_RESPONSE cmdresp;
	LPCTSTR strDesc;
	
    CFont* pFont = GetFont(); //Get the font from the Property Page Dialog for UTF-8 support, POB - 2 Aug 2008
	
	int nCount=0;
	int nSize=cmd48_PreviousArray.GetSize();
	
	for(int nCntr=0;nCntr<nSize;nCntr++)
	{
		cmdresp=cmd48_PreviousArray.GetAt(nCntr); 
		cmd48_PresentArray.Add(cmdresp); 	
			
		m_pBtnArray[nCount] = NULL;
		m_pStaticArray[nCount] = NULL;
		m_pBtnArray[nCount] = new CBitmapButton();
		
		if (nCount < 10 )
			m_pBtnArray[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectDevStatus1, this, 100);
		else
			m_pBtnArray[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectCommStatus1, this, 100);
		
		
		unsigned int lVal= cmdresp.nVal;
		CString strTmp= cmdresp.szDesc; 
		strDesc=strTmp.GetBuffer(strTmp.GetLength());
		
		if (lVal)
		{
			if(NULL != m_pBtnArray[nCount])
			{
			m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
			m_pBtnArray[nCount]->SizeToContent();
		}
			
		}
		else
		{
			if(NULL != m_pBtnArray[nCount])
			{
			m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
			m_pBtnArray[nCount]->SizeToContent();
		}

		}


		m_pStaticArray[nCount] = new CStatic();
		if (nCount < 10 )
		{
			m_pStaticArray[nCount]->Create(strDesc, WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectDevStatus2, this, 100);
			rectDevStatus1.top = rectDevStatus1.bottom + GAP1;
			rectDevStatus1.bottom = rectDevStatus1.top + GAP2;
			rectDevStatus2.top = rectDevStatus2.bottom + GAP1;
			rectDevStatus2.bottom = rectDevStatus2.top + GAP2;
		}
		else
		{
			m_pStaticArray[nCount]->Create(strDesc, WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectCommStatus2, this, 100);
			rectCommStatus1.top = rectCommStatus1.bottom + 10;
			rectCommStatus1.bottom = rectCommStatus1.top + 16;

			rectCommStatus2.top = rectCommStatus2.bottom + 10;
			rectCommStatus2.bottom = rectCommStatus2.top + 16;
		}

        // Set the parent's Font for the control, POB - 2 Aug 2008
        m_pStaticArray[nCount]->SetFont(pFont);

		nCount++;
	}	
	

	bInitialCreationDone=TRUE;


	if( FALSE == bStaticCtrl_Created )
	{
		CRect Msgrect(40,30,270,60);
		
		bStaticCtrl_Created=TRUE;
		m_StaticMsg.Create(_T(""), WS_CHILD|WS_VISIBLE|SS_CENTER,Msgrect, this);
		m_StaticMsg.ShowWindow(SW_SHOW);	
		OnAddSuppressMessage();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDevStatPageOne::OnUpdateExtendedDeviceStatus(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_PresentArray,CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_PreviousArray,int nLwrLimit,int nUpprLimit) 
{
	bool bRefresh=false;	//Modifed by ANOOP 17FEB2004

	CRect rectDevStatus1(40,72,290,89);
	CRect rectDevStatus2(67,72,290,89);

	CRect rectCommStatus1(348,72,558,89);
	CRect rectCommStatus2(375,72,558,89);

	CMD48_RESPONSE cmdresp_present,cmdresp_previous;
	
	int nCount=0;
	int nCntr=0,nTmpCntr=0;	//Added by ANOOP 03FEB2004


	int nSize=cmd48_PreviousArray.GetSize(); 
	int nPresent_Size=m_PresentArray.GetSize();	//Added by ANOOP 03FEB2004 

	for( int i=0; i < nPresent_Size; i++ )	//Added by ANOOP 03FEB2004
	{
		CMD48_RESPONSE cmdresp;
		cmdresp=m_PresentArray.GetAt(i);
//		cmd48_PresentArray.Add(cmdresp); Commented by ANOOP 03FEB2004
		/*<START>Added by ANOOP 03FEB2004 Issues in updating the extended status tabs	*/
		if  ( cmdresp.szByteIdentity == m_szByteIdentifier ) 
		{		
			if(cmd48_PresentArray.GetSize() > 0 )
			{
				cmd48_PresentArray.SetAt(nTmpCntr,cmdresp); 
				nTmpCntr++;
			}
		}
		/*<END>Added by ANOOP 03FEB2004 Issues in updating the extended status tabs	*/
	}
	
	
	if(TRUE == bInitialCreationDone)
	{
		
		for(nCntr=0; nCntr < nSize; nCntr++)
		{
			cmdresp_present=cmd48_PresentArray.GetAt(nCntr); 
			cmdresp_previous=cmd48_PreviousArray.GetAt(nCntr);
			
/*<START>Commented by ANOOP 03FEB2004
			if( nCntr < nLower_Limit || nCntr > nUpper_Limit ) break; 
<END>Commented by ANOOP 03FEB2004	*/
				
			unsigned int nPresentVal= cmdresp_present.nVal;
			
			unsigned int nPreviousVal= cmdresp_previous.nVal;
			
			if(nPresentVal != nPreviousVal)
			{	
				bRefresh=true;    ////Modifed by ANOOP 17FEB2004 Refresh the screen on the fly if the screen is active
				if (nPresentVal)
				{
					m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
					m_pBtnArray[nCount]->SizeToContent();
				}
				else
				{
					m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
					m_pBtnArray[nCount]->SizeToContent();
				}
				cmdresp_previous.nVal= nPresentVal;
				cmd48_PreviousArray.SetAt(nCntr,cmdresp_previous);

			}
			
			if (nCount < 10 )
			{
				rectDevStatus1.top = rectDevStatus1.bottom + 10;
				rectDevStatus1.bottom = rectDevStatus1.top + 16;
				rectDevStatus2.top = rectDevStatus2.bottom + 10;
				rectDevStatus2.bottom = rectDevStatus2.top + 16;
			}
			else
			{
				rectCommStatus1.top = rectCommStatus1.bottom + 10;
				rectCommStatus1.bottom = rectCommStatus1.top + 16;
				rectCommStatus2.top = rectCommStatus2.bottom + 10;
				rectCommStatus2.bottom = rectCommStatus2.top + 16;
			}
			nCount++;
		}

		/*<START>Added by ANOOP 17FEB2004 for refreshing the screen */
		if (true == bRefresh)
		{
			Invalidate();
			this->ShowWindow(SW_SHOW); 	
		}
		/*<END>Added by ANOOP 17FEB2004 for refreshing the screen */
	}
	else if (FALSE == bInitialCreationDone)
	{
	
		for(nCntr=0; nCntr < nSize; nCntr++)
		{
			cmdresp_present=cmd48_PresentArray.GetAt(nCntr); 
			cmdresp_previous=cmd48_PreviousArray.GetAt(nCntr);
			
			if( (unsigned)nCntr < nLower_Limit || (unsigned)nCntr > nUpper_Limit ) break; 
				
			unsigned int nPresentVal= cmdresp_present.nVal;
			
			unsigned int nPreviousVal= cmdresp_previous.nVal;
			
			if(nPresentVal != nPreviousVal)
			{	
				cmdresp_previous.nVal= nPresentVal;
				cmd48_PreviousArray.SetAt(nCntr,cmdresp_previous);
			}
		}
	}


	return TRUE;
}

BOOL CDevStatPageOne::CreateTab()
{
	CRect rectDevStatus1(40,72,290,89);
	CRect rectDevStatus2(67,72,670,89);

	CRect rectCommStatus1(348,72,558,89);
	CRect rectCommStatus2(375,72,670,89);

	CMD48_RESPONSE cmdresp;
	LPCTSTR strDesc;
	
	int nCount=0;
	//unsigned int nCntr=0;
	
	
	for(unsigned nCntr=nLower_Limit; nCntr < nUpper_Limit; nCntr++)
	{
		cmdresp=cmd48_PreviousArray.GetAt(nCount); 
		
		if( nCntr < nLower_Limit || nCntr > nUpper_Limit ) break; 
			
		m_pBtnArray[nCount] = NULL;
		m_pStaticArray[nCount] = NULL;
		m_pBtnArray[nCount] = new CBitmapButton();
		
		if (nCount < 10 )
			m_pBtnArray[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectDevStatus1, this, 100);
		else
			m_pBtnArray[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectCommStatus1, this, 100);
		
        // Set the parent's Font for the control, POB - 2 Aug 2008
        m_pBtnArray[nCount]->SetFont(AfxGetApp()->m_pActiveWnd->GetFont());
		
		unsigned int lVal= cmdresp.nVal;
		CString strTmp= cmdresp.szDesc; 
		strDesc=strTmp.GetBuffer(strTmp.GetLength());
		
		if (lVal)
		{
			m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
			m_pBtnArray[nCount]->SizeToContent();
		}
		else
		{
			m_pBtnArray[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
			m_pBtnArray[nCount]->SizeToContent();
		}


		m_pStaticArray[nCount] = new CStatic();
		if (nCount < 10 )
		{
			m_pStaticArray[nCount]->Create(strDesc, WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectDevStatus2, this, 100);
			rectDevStatus1.top = rectDevStatus1.bottom + 10;
			rectDevStatus1.bottom = rectDevStatus1.top + 16;
			rectDevStatus2.top = rectDevStatus2.bottom + 10;
			rectDevStatus2.bottom = rectDevStatus2.top + 16;
		}
		else
		{
			m_pStaticArray[nCount]->Create(strDesc, WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectCommStatus2, this, 100);
			rectCommStatus1.top = rectCommStatus1.bottom + 10;
			rectCommStatus1.bottom = rectCommStatus1.top + 16;

			rectCommStatus2.top = rectCommStatus2.bottom + 10;
			rectCommStatus2.bottom = rectCommStatus2.top + 16;
		}
		nCount++;
	}	

	return TRUE;
}

BOOL CDevStatPageOne::OnAddSuppressMessage()
{
	if(bStaticCtrl_Created)
	{
		if(TRUE == bSuppress_MsgDisplay)
		{
			if(m_StaticMsg)
			{
				/*<START>16APR2004 Commented by ANOOP 
	m_StaticMsg.SetWindowText(_T("Updations currently suppressed")); // Vibhor 020304: Changed text
				<END>*/
			}
		}
	}	
	return TRUE;
} 

BOOL CDevStatPageOne::OnRemoveSuppressMessage()
{
	if(bStaticCtrl_Created)
	{
		bSuppress_MsgDisplay=FALSE;
		if(m_StaticMsg)
		{
	m_StaticMsg.SetWindowText(_T("")); 
		}
	}	
	return TRUE;
} 

