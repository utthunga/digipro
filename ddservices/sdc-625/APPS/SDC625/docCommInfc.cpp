// docCommInfc.cpp : implementation file
//

#include "stdafx.h"
#include "DDIde.h"
#include "docCommInfc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc

IMPLEMENT_DYNCREATE(CdocCommInfc, CWinThread)

CdocCommInfc::CdocCommInfc()
{
}

CdocCommInfc::~CdocCommInfc()
{
}

BOOL CdocCommInfc::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CdocCommInfc::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CdocCommInfc, CWinThread)
	//{{AFX_MSG_MAP(CdocCommInfc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc message handlers
