/*************************************************************************************************
 *
 * $Workfile: DDLVariable.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDLVariable.cpp: implementation of the CDDLVariable class.
 */

#include "stdafx.h"
#include "SDC625.h"
#include "DDLVariable.h"
#include "VariableDlg.h"
#include "WaoDlg.h"
#include "SDC625Doc.h"
#include "SDC625_drawTypes.h"
#include "DDLexpansion.h"
#include "LabelDialog.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLVariable, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CDDLVariable::CDDLVariable(itemInfo_t& rIi):CDDLexpTbl(rIi),CDDLBase(rIi.p_Ib)
{
    initVariable(rIi.p_Ib);
    m_strName    = rIi.labelStr.c_str();// could be different from item label
// this gets realllll old    TRACE(_T("CdrawBase::CDDLVariable - reference Label = %s\n"), m_strName);
    m_strHelp    = rIi.help_Str.c_str();
    m_strNmValid = rIi.labelFilled;

    if ((rIi.qual & INDEXED_ITEM) == INDEXED_ITEM)
    {// only type BIT right now - stevev 18may11
        m_BitMask = rIi.indexValue;
        m_varData.vt = TYPE_BIT;
    }
}

CDDLVariable::CDDLVariable(hCitemBase* pIB):CDDLBase(pIB)
{
    initVariable(pIB);
}

void CDDLVariable::initVariable(hCitemBase* pIB)// PAW 03/03/09 void added
{
    m_nImage      = IMG_VARIABLE;

    m_bIsDsplyVal = FALSE;
    m_bIsReadOnly = FALSE;
    // in base class...m_bIsValid    = TRUE;
    m_constUnits  = "";
    m_pUnitRelVar = NULL; // not pointing to any CVar relational variable
    m_pddbVar     = NULL;

    m_edtFormat = "";
//    m_dsplyFormat = "";
    m_cLength = 255; // max length
    m_varData.vt      = TYPE_NONE;
    m_varData.bVal    = 0;   
    m_varData.fltVal  = 0.0f;   
    m_varData.dblVal  = 0.0f;   
    m_varData.boolVal = FALSE;   
    m_varData.intVal  = 0; 
    m_varData.strVal  = "";
//    m_varData.strEnum.RemoveAll();
    m_varData.min.RemoveAll();
    m_varData.max.RemoveAll();

    // stevev 01dec05..done in base class...SetID(pIB->getID()); // Moved here - POB, 28 Oct 05 

/* stevev 21mar05 - joined the two constructors */
    if ( pIB != NULL && pIB->IsVariable() )
    {
        string constUnits;

        // Get the DDB object
        m_pddbVar = (hCVar*)pIB;// pddbVar;

        //prashant 120204
        // stevev 01dec05..done in base class...string strName;
        // stevev 01dec05..done in base class...m_pddbVar->Label(strName);
        // stevev 01dec05..done in base class...SetName(strName.c_str()); 
        /* VMKP on 260204 */
        // stevev 01dec05..done in base class...m_pddbVar->Help(strName);
        // stevev 01dec05..done in base class...SetHelp(strName.c_str());
        /* VMKP on 260204 */
        //prashant 120204 end
    
        // Read DD attributes if CVar* is not NULL
        // stevev 01dec05..done in base class...if(m_pddbVar)
        // {
//        m_bIsReadOnly = m_pddbVar->IsReadOnly();
          // stevev 01dec05..done in base class...m_bIsValid = m_pddbVar->IsValid();

/*
          if (m_pddbVar->IsReadOnly())
             m_nImage = IMG_READONLY_VARIABLE;
          else
            m_nImage  = IMG_VARIABLE;
*/
        // done ealier...m_nImage  = IMG_VARIABLE;

/********** This does not work yet *********************************** 
         pddbVar->ReadUnits(constUnits); // Get constant units
          m_constUnits = constUnits.c_str();
*********************************************************************/

          switch(m_pddbVar->VariableType())
          {
                case INTEGER:
                    m_varData.vt = TYPE_INTEGER;
                    m_edtFormat = "%d";
                    break;
                case INDEX:
                    m_varData.vt = TYPE_INDEX;
                    m_edtFormat = "%d";
                    break;

                case UNSIGNED:
                    m_varData.vt = TYPE_UNSIGN_INT;
                    m_edtFormat = "%d";
                    break;

                case FLOATG_PT:
                    m_varData.vt = TYPE_FLT;
                    m_edtFormat = "%f";
                    break;

                case DOUBLE_FLOAT:
                    m_varData.vt = TYPE_DBL;
                    break;

                case ENUMERATED:
                    m_varData.vt = TYPE_ENUM;
                    m_edtFormat = "%d";
                    break;

                case BIT_ENUMERATED:
                    m_varData.vt = TYPE_BITENUM;//TYPE_BIT
                    m_edtFormat = "%d";
                    break;

                case ASCII:
                case PACKED_ASCII:
                    m_varData.vt = TYPE_STRING;
                    break;

                case HART_DATE_FORMAT:
                    m_varData.vt = TYPE_DATE;
                    break;

                case PASSWORD:
                    m_varData.vt = TYPE_PASSWORD; // POB, 9 May 2005
                    break;

                default:
                    break;
          }

        //}
    }
/* stevev 21mar05 - end code */
}
#if 0 /* replaced above */
CDDLVariable::CDDLVariable(hCVar* pddbVar)
{
    // Same list as that above
            m_nImage  = IMG_VARIABLE;
            m_bIsDsplyVal = FALSE;
            m_bIsReadOnly = FALSE;
            m_bIsValid = TRUE;
            m_constUnits = "";
            m_pUnitRelVar = NULL;
            m_pddbVar = NULL;

            m_edtFormat = "";
//    m_dsplyFormat = "";
            m_cLength = 255; // max length
            m_varData.vt      = TYPE_NONE;
            m_varData.bVal    = FALSE;   
            m_varData.fltVal  = 0.0f;   
         m_varData.dblVal  = 0.0f;   
         m_varData.boolVal = FALSE;   
            m_varData.intVal  = 0; 
            m_varData.strVal  = "";
//    m_varData.strEnum.RemoveAll();
            m_varData.min.RemoveAll();
            m_varData.max.RemoveAll();

    string constUnits;

    // Get the DDB object
    m_pddbVar = pddbVar;

    //prashant 120204
    string strName;
    pddbVar->Label(strName);
    SetName(strName.c_str()); 
    /* VMKP on 260204 */
    pddbVar->Help(strName);
    SetHelp(strName.c_str());
    /* VMKP on 260204 */
    //prashant 120204 end
    
    // Read DD attributes if CVar* is not NULL
    if(m_pddbVar)
    {
//        m_bIsReadOnly = m_pddbVar->IsReadOnly();
          m_bIsValid = m_pddbVar->IsValid();

/*
          if (m_pddbVar->IsReadOnly())
             m_nImage = IMG_READONLY_VARIABLE;
          else
            m_nImage  = IMG_VARIABLE;
*/
        m_nImage  = IMG_VARIABLE;

/********** This does not work yet *********************************** 
         pddbVar->ReadUnits(constUnits); // Get constant units
          m_constUnits = constUnits.c_str();
*********************************************************************/

          switch(m_pddbVar->VariableType())
          {
                case INTEGER:
                    m_varData.vt = TYPE_INTEGER;
                    m_edtFormat = "%d";
                    break;
                case INDEX:
                    m_varData.vt = TYPE_INDEX;
                    m_edtFormat = "%d";
                    break;

                case UNSIGNED:
                    m_varData.vt = TYPE_UNSIGN_INT;
                    m_edtFormat = "%d";
                    break;

                case FLOATG_PT:
                    m_varData.vt = TYPE_FLT;
                    m_edtFormat = "%f";
                    break;

                case DOUBLE_FLOAT:
                    m_varData.vt = TYPE_DBL;
                    break;

                case ENUMERATED:
                    m_varData.vt = TYPE_ENUM;
                    m_edtFormat = "%d";
                    break;

                case BIT_ENUMERATED:
                    m_varData.vt = TYPE_BITENUM;
                    m_edtFormat = "%d";
                    break;

                case ASCII:
                case PACKED_ASCII:
                    m_varData.vt = TYPE_STRING;
                    break;

                case HART_DATE_FORMAT:
                    m_varData.vt = TYPE_DATE;
                    break;

                default:
                    break;


          }

    }
}
#endif /* merged constructors */


#define INTEGER         2
#define UNSIGNED        3
#define FLOATG_PT       4
#define DOUBLE          5
#define ENUMERATED      6
#define BIT_ENUMERATED  7
#define INDEX           8
#define ASCII           9

CDDLVariable::~CDDLVariable()
{

}

/////////////////////////////////////////////////////////////////////////////
// CDDLBase Member Functions

// Assignment operator (used to set the variable to a float value)
const CDDLVariable &CDDLVariable::operator=(const float& fValue)
{
    hCFloat* pfddbVal;

    // Only work with the DDB object if it is not NULL
    if(m_pddbVar)
    {
        pfddbVal = (hCFloat*)m_pddbVar;
        CValueVarient vValue;
        vValue  = fValue;
        pfddbVal->setDispValue(vValue);
    }
    
/*    if (m_dsplyFormat == "")
        m_dsplyFormat = "%f";*/

    if (m_edtFormat == "")
        m_edtFormat = "%f";

    m_varData.vt = TYPE_FLT;
//    m_varData.fltVal = fValue;
//    m_varData.strVal.Format(m_dsplyFormat,m_varData.fltVal);

    return *this;
}

// Assignment operator (used to set the variable to an integer value)
const CDDLVariable &CDDLVariable::operator=(const int& nValue)
{
    hCEnum* pddbEnum;
    hCSigned* pnddb;
    hCUnsigned* puddb;
    hCindex*     piddb;
    hChartDate*   pdddb;
    hCBitEnum*      pddbitEnum;
    CValueVarient   x;
    UINT32 uVal;
    INT32  nVal;
/*   code removed 13aug08 */
    if (m_edtFormat == "")
        m_edtFormat = "%d";

/*   code removed 13aug08 */

    CValueVarient vValue;
    vValue  = nValue;
    // Only work with the DDB object if it is not NULL
    if(m_pddbVar)
    {
        switch(m_varData.vt)
        {
            case TYPE_INTEGER:
                nVal = nValue;
                pnddb = (hCSigned*)m_pddbVar;
                pnddb->setDispValue(vValue);
                break;

            case TYPE_INDEX:
                //nVal = nValue;
                uVal = nValue;
                piddb = (hCindex*)m_pddbVar;
                //piddb->Write(nVal);
                piddb->setDispValue(vValue);
                break;

            case TYPE_UNSIGN_INT:
                uVal = nValue;
                puddb = (hCUnsigned*)m_pddbVar;
                puddb->setDispValue(vValue);
                break;

            case TYPE_ENUM:
            case TYPE_BITENUM:
                uVal = nValue;
                pddbEnum = (hCEnum*)m_pddbVar;
                pddbEnum->setDispValue(vValue);
                break;

            case TYPE_BIT:
                x = nValue;
                pddbitEnum = (hCBitEnum*)m_pddbVar;
                pddbitEnum->setDispValue(x, m_BitMask, false);
                break;

            case TYPE_DATE:
                uVal = nValue;
                pdddb = (hChartDate*)m_pddbVar;
                pdddb->setDispValue(vValue);
                break;

            default:
                // Nothing to do
                break;

        }
    }
    
/*   code removed 13aug08 */
    return *this;
}

// Assignment operator (used to set the variable to a string value
const CDDLVariable &CDDLVariable::operator=(const CString& strValue)
{
/*   code removed 13aug08 */
    hCascii* psddbVal;
    string sValue;
    
    
    CValueVarient vValue;
    vValue  = (LPCTSTR)strValue;

    // Only work with the DDB object if it is not NULL
    if(m_pddbVar)
    {
        psddbVal = (hCascii*)m_pddbVar;
        psddbVal->setDispValue(vValue);
    }

    return *this;
}

// float cast operator (used to get the value from the variable)
CDDLVariable::operator float(void)
{
    hCFloat* pfddbVal;
    float fVal;
    hCEnum* pddbEnum;
    hCSigned* pnddb;
    hCUnsigned* puddb;
    hCindex*     piddb;
    hCBitEnum*     pddbitEnum;
    CValueVarient   x;
    UINT64 uVal = 0;
    INT64  nVal = 0;
    int  intVal = 0;

/*   code removed 13aug08 */
    if(m_pddbVar)
    {
        switch(m_varData.vt)
        {
        case TYPE_FLT:
            {
                pfddbVal = (hCFloat*)m_pddbVar;
                pfddbVal->Read(fVal);
                m_varData.fltVal = fVal;
            }
            break;
        case TYPE_INTEGER:
            pnddb = (hCSigned*)m_pddbVar;
            pnddb->Read(nVal);
            m_varData.fltVal = (float)uint64TOdbl(nVal);
            //intVal = nVal;
            //m_varData.intVal = intVal;
            m_varData.intVal = (INT32)nVal;// stevev 27aug10 - just lose any extra for now
            m_varData.fltVal = (float)nVal;
            break;

        case TYPE_INDEX:
            piddb = (hCindex*)m_pddbVar;
            piddb->Read(uVal);
            intVal = (INT32)uVal;//  stevev 27aug10 - restricted value set
            m_varData.intVal = intVal;
            //m_varData.fltVal = (float)uVal;
            m_varData.fltVal = (float)uint64TOdbl(uVal);
            break;

        case TYPE_UNSIGN_INT:
            puddb = (hCUnsigned*)m_pddbVar;
            puddb->Read(uVal);
            intVal = (INT32)uVal;// stevev 27aug10 - just lose any extra for now
            m_varData.intVal = intVal;
            //m_varData.fltVal = (float)uVal;
            m_varData.fltVal = (float)uint64TOdbl(uVal);
            break;

        case TYPE_ENUM:
        case TYPE_BITENUM:
            pddbEnum = (hCEnum*)m_pddbVar;
            pddbEnum->Read(uVal);
            nVal = uVal;
            m_varData.intVal = (INT32)nVal;//  stevev 27aug10 - restricted value set
            //m_varData.fltVal = (float)uVal;
            m_varData.fltVal = (float)uint64TOdbl(uVal);
            break;          

        case TYPE_BIT:
            pddbitEnum = (hCBitEnum*)m_pddbVar;
            x    = pddbitEnum->getDispValue(m_BitMask, false);
            intVal = (int) x;
            m_varData.boolVal = (intVal)?TRUE:FALSE;
            m_varData.fltVal  = (intVal)?1.0f:0.0f;
            break;

        default:
            break;
        }

    }
    return m_varData.fltVal;
}

// int cast operator (used to get the value from the variable)
CDDLVariable::operator int(void)
{
    hCEnum* pddbEnum;
    hCSigned* pnddb;
    hCUnsigned* puddb;
    hCindex*     piddb;
    hCBitEnum*     pddbitEnum;
    CValueVarient   x;
    UINT64 uVal = 0;
    INT64  nVal = 0;
    int  intVal = 0;

    if(m_pddbVar)
    {
        switch(m_varData.vt)
        {
            case TYPE_INTEGER:
                pnddb = (hCSigned*)m_pddbVar;
                pnddb->Read(nVal);
                //intVal = nVal;
                //m_varData.intVal = intVal;
                m_varData.intVal = (INT32)nVal;// stevev 27aug10 - just lose any extra for now
                break;

            case TYPE_INDEX:
                piddb = (hCindex*)m_pddbVar;
                piddb->Read(uVal);
                intVal = (INT32)uVal;//  stevev 27aug10 - restricted value set
                m_varData.intVal = intVal;
                break;

            case TYPE_UNSIGN_INT:
                puddb = (hCUnsigned*)m_pddbVar;
                puddb->Read(uVal);
                intVal = (INT32)uVal;// stevev 27aug10 - just lose any extra for now
                m_varData.intVal = intVal;
                break;

            case TYPE_ENUM:
            case TYPE_BITENUM:
                pddbEnum = (hCEnum*)m_pddbVar;
                pddbEnum->Read(uVal);
                nVal = uVal;
               m_varData.intVal = (INT32)nVal;//  stevev 27aug10 - restricted value set
                break;          

            case TYPE_BIT:
                pddbitEnum = (hCBitEnum*)m_pddbVar;
                x    = pddbitEnum->getDispValue(m_BitMask, false);
                intVal = (int) x;
                m_varData.boolVal = (intVal)?TRUE:FALSE;
                m_varData.intVal  = (intVal)?1   :0;
                break;

            default:
                break;
        }

    }

    return m_varData.intVal;
}

// CString cast operator (used to get the value from the variable)
CDDLVariable::operator CString(void)
{ 
    wstring strVal;

    // Only work with the DDB object if it is not NULL
    if(m_pddbVar)
    {
#ifdef _DEBUG
        unsigned symNum = m_pddbVar->getID();
#endif
/*   code removed 13aug08 */
        if (m_varData.vt == TYPE_BIT)
        {
            CValueVarient x;
            hCBitEnum* pbEnum = (hCBitEnum*)m_pddbVar;
            x = pbEnum->getDispValue(m_BitMask,false);// a boolean
            DATA_QUALITY_T  dq =  pbEnum->getDataQuality();
            if( (dq == DA_HAVE_DATA || dq == DA_STALE_OK || pbEnum->getWriteStatus() == 1) &&
                 x.vIsValid )
            {
                strVal = ((bool)x)? BIT_SET : BIT_CLR;
            }
            else
            {
                strVal = L"";// its bad, don't show it
            }               
        }
        else // everybody else
        {
            strVal = m_pddbVar->getValidDispValueString();
        }

        if(m_varData.vt == TYPE_STRING || m_varData.vt == TYPE_PASSWORD) // POB, 9 May 2005
        {
            CString strTemp= strVal.c_str();

            if(m_varData.vt == TYPE_PASSWORD) // POB, 9 May 2005
            {
                for (int i = 0; i < strTemp.GetLength(); i++)
                {
                    strTemp.SetAt(i, '*' ); // Hide the string value in the list view
                }   
            }

            m_varData.strVal = strTemp.Left(m_pddbVar->VariableSize());
        }
        else
        {
            m_varData.strVal = strVal.c_str();
        }
    }

    return m_varData.strVal;
}

//////////////////////////////////////////////////////////////////////
// Overrides
//////////////////////////////////////////////////////////////////////

void CDDLVariable::Execute()
{
    CDDLBase  *pParent = GetParent();
    BOOL bRO = 0;
    if (pParent && pParent->IsReadOnly() ) bRO = 1;

    ITEM_ID lWaoRel = m_pddbVar->getWaoRel();

    // If this VARIABLE is in a write as one, we need to
    // call the write as one dialog, POB - 27 Aug 2008
    if (lWaoRel > 0)
    {
        if (! IsReadOnly())
        {
            CWaoDlg WaoDlg(m_pddbVar, lWaoRel);
            int nRet = WaoDlg.DoModal();
        }
    }
    else
    {
        if (GetType() == TYPE_BITENUM || GetType() ==TYPE_BIT)  
        {
            /*
             * Always call Execute() on Biteum and/or Bit reference.
             * Editing or Displaying the bits in style TABLE
             * shows the same dialog regardless of whether the 
             * HANDLING is Read-only or not.
             */

            hCmenu* pMenu = makeSingleItemMenu();

            //make a new drawdlg
            CdrawDlg* pDlg = new CdrawDlg(NULL);
            pDlg->m_pItem = pMenu;
            pDlg->m_symblID = pMenu->getID();
            pDlg->m_bSingleMenuItem = TRUE;  // Ignore Layout rules - start in 1st column, POB - 3/30/2014

            //execute
            pDlg->Execute(NULL);
            
            delete pDlg;
        }
        else
        {
            if (! IsReadOnly())
            {
                //try to post AfxGetMainWnd()->SendMessage(WM_DO_EXECUTE, bRO,(LPARAM)m_pddbVar);
                // stevev 08oct10:was:>     AfxGetMainWnd()->PostMessage(WM_DO_EXECUTE, bRO,(LPARAM)m_pddbVar);
                GETMAINFRAME->PostMessage(WM_DO_EXECUTE, bRO,(LPARAM)m_pddbVar);
            }
        }
    }

    return;
}

#if 0 /*********************************** this is done in the mainframe ********************/
/* this must function as a static because the class instance could be destroyed
   at any time ---BE VERY CAREFULL--- do not use ANY class variables */
void CDDLVariable::Do_Execute(hCVar* pVar)
{                   
//    m_bTmpValDsply = TRUE;

    // Look for wao relation, if so pop up another dialog
    // Code for adding wao relation
    vector<hCVar*> listOvarPtrs;
    RETURNCODE rc;

    ITEM_ID lWaoRel = m_pddbVar->getWaoRel();

    if (lWaoRel > 0)
    {
        CWaoDlg WaoDlg(this, lWaoRel);

        //call the pre edit action method
        rc = m_pddbVar->doPreEditActs();
        if(rc != SUCCESS)
            {
                LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
                return ;
            }

        int nRet = WaoDlg.DoModal();

        //stevev if the pre-edits ran, the post edits must run
            //call post edit action
        rc = m_pddbVar->doPstEditActs();
        if(rc != SUCCESS)
        {
            LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
        }

        if(IDOK == nRet)
        {
            rc = m_pddbVar->doPstUserActs();
            if(rc != SUCCESS)
            {
                LOGIT(CERR_LOG|UI_LOG,"Call to post user edit action returned an error.\n");
            }
            return;
        }
            /*Vibhor 300104: Start of Code*/
        else /* Cancel was pressed or dialog closed*/
        {
            return;
        }
            /*Vibhor 300104: End of Code*/
    }/*Endif lWaoRel*/
    else  /*Vibhor 300104: Start of Code*/ // This else was not there
    {
        CValueVarient preEditValue,pstEditValue;
        preEditValue = m_pddbVar->getDispValue();

// stevev 15nov05 moved lower       CVariableDlg EditDlg(this);

        //call the pre edit action method
        rc = m_pddbVar->doPreEditActs();
        if(rc != SUCCESS)
        {
            LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
            return ;
        }
        // the actions could influence how this is created (stevev 15nov05)
        // stevev 13jun06 changed to below::> CVariableDlg EditDlg(this);
        CVariableDlg EditDlg(this,AfxGetMainWnd());

      //prashant 

        int nRet = EditDlg.DoModal();

        /* stevev 5aug08 - if the pre-edits ran, the post edits must run too.*/
        //call post edit action
        rc = m_pddbVar->doPstEditActs();
        if(rc != SUCCESS)
        {
            LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
        }
        if(IDOK == nRet)
        {
            rc = m_pddbVar->doPstUserActs();
            if(rc != SUCCESS)
            {
                LOGIT(CERR_LOG|UI_LOG,"Call to post user edit action returned an error.\n");
            }
        }
        //else
        //{
        //  return;
        //}
        /* stevev 5aug08 - the following update was moved out of the dialog due to mem crashes*/
        pstEditValue = m_pddbVar->getDispValue();
        
      //prshant: to fix the commit button enableing PAR
        if(m_pddbVar->isChanged() || (m_pddbVar->IsLocal() && !(pstEditValue == preEditValue))  )
        {
            CMainFrame* pMainFrm = (CMainFrame*)EditDlg.GetTopLevelParent();
            HWND hwndMainWindow  = 0;

            if ( pMainFrm != NULL && pMainFrm->m_pDoc != NULL && ::IsWindow(pMainFrm->m_hWnd) )
            {   
                pMainFrm->m_pDoc->m_bVariableEditedByUser = true;
                hwndMainWindow  = pMainFrm->GetSafeHwnd();
                             
                if (hwndMainWindow != NULL)
                {
                    ::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, 
                                                                        m_pddbVar->getID(),NULL);
                    LOGIF(LOGP_NOTIFY)(CLOG_LOG,"Notify: Value Changed from Variable edit: 0x%04x\n",
                                                                                p_doVar->getID());
                }// else - nop
            }// else (no pointers) don't notify, return

        }// else (no change) don't notify, return
        /* end move of 5aug08 */
        return; 
    } /*End else*/
    /*Vibhor 300104: End of Code*/
}
/*************************************************** above done in mainframe ****************/
#endif

BOOL CDDLVariable::IsReadOnly()
{
    // Read DD attributes if CVar* is not NULL
    if(m_pddbVar)
    {
        m_bIsReadOnly = m_pddbVar->IsReadOnly();
    }

    return m_bIsReadOnly;
}

BOOL CDDLVariable::IsDynamic()
{
    return m_pddbVar->IsDynamic();
}

/*   code removed 13aug08 */

BOOL CDDLVariable::IsVariable()
{
    return TRUE;
}

CString CDDLVariable::GetRelation()
{
    CString strUnits = "";
    string strVal;

/*
    // Make sure the relations variable is not NULL
    if (m_pUnitRelVar)
    {
        //strUnits = *m_pUnitRelVar;
        m_pUnitRelVar->Read(strVal);
        strUnits = strVal.c_str();
    }
    else
    {
        strUnits = m_constUnits;
    }

    // Finout whether the variable belongs to any Unit relation
    ITEM_ID unitId = m_pddbVar->getUnitRel();
    if (unitId != 0) // 
    {
        hCdeviceSrvc *pService = GetDevicePtr();

        if (pService != NULL)
        {
            hCitemBase *pBase;
            pService->getItemBySymNumber(unitId, &pBase);
            if (pBase->getTypeVal() == UNIT_ITYPE)
            {
                hCunitRel *pUnitRel = (hCunitRel *)pBase;
                if (pUnitRel->getIndependVarPtr(pUnit) == SUCCESS)
                {
                    CDDLVariable *pVar = new CDDLVariable(pUnit);
                    strUnits = *pVar;
                }
            }
        }
    }
*/

    return strUnits;
}


void CDDLVariable::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );

    if (GetType() == TYPE_BITENUM || GetType() == TYPE_BIT)  
    {
        nPos = POPUP_ITEM;
    }
    else
    {
        nPos = POPUP_VARIABLE;
    }

    //pMenu->GetSubMenu(0)->CheckMenuItem( 0, 1 ? MF_CHECKED : MF_UNCHECKED );
}

void CDDLVariable::OnUpdateMenuExecute(CCmdUI* pCmdUI)
{
    if (GetType() == TYPE_BITENUM || GetType() == TYPE_BIT)  
    {
        if (!IsReadOnly())
        {
            /*
             * This CMenu update is only needed for style TABLE
             * If bit enum is NOT read only, change the text 
             * from the default "Display" to "Display/Edit"
             * DO NOT DISABLE the menu.
             */
            CString text = "Display/Edit";
            pCmdUI -> SetText(text);
        }
    }
    else
    {   //This VARIABLE is not a bit enum or bit reference
        if (IsReadOnly())
        {
            pCmdUI -> Enable(FALSE);
        }
    }
}

void CDDLVariable::DisplayValue()
{
    if (GetType() == TYPE_BITENUM)  
    {
        /* 
         * Removed CBitEnumDispValDlg code. 
         * Now, calling the CDDLVariable::Execute() function
         * for both the edit and display of Bit Enumerated
         * and Bit references with the code from the
         * window draw style, POB - 11/30/13
         */

        //hCVar *pVar = (hCVar*)m_pItemBase;
        //CValueVarient tmpVar=pVar->getDispValue(); 
        //CBitEnumDispValDlg    dBitEnumDispVals(tmpVar,pVar,NULL);
        //dBitEnumDispVals.DoModal();
    }
    else
    {
        // Show the variable value using the CLabelDlg class
        CString strValue = *this;
        CString strUnits = GetUnitStr();

        if (!strUnits.IsEmpty())
        {
            strValue = strValue + " " + strUnits;
        }

        CLabelDlg dlgVarValue(GetName(), strValue);
        dlgVarValue.sdcModl();// DoModal();*/
    }
}

//////////////////////////////////////////////////////////////////////
// Variable specific
//////////////////////////////////////////////////////////////////////

void CDDLVariable::SetType(VarType type)
{
    m_varData.vt = type;
}

VarType CDDLVariable::GetType()
{
    return m_varData.vt;
}

// Write Value of Variable to the field device
int CDDLVariable::Write()
{
    return 0;  // success
}

//////////////////////////////////////////////////////////////////////

void CDDLVariable::SetRelation(hCVar* pVar)
{
    m_pUnitRelVar = pVar;
}

void CDDLVariable::SetRelation (CString strRel)
{
    m_constUnits = strRel;
}

BOOL CDDLVariable::SetHandling(BYTE qualifier)
{
    BOOL result = TRUE;

    switch(qualifier)
    {
        case READ_WRITE:
            m_bIsReadOnly = FALSE;
            break;

        case READ_ONLY:
            m_bIsReadOnly = TRUE;
            break;

        default:
            result = FALSE;
            break;
    }

    return result;
}

/*   code removed 13aug08 */
BOOL CDDLVariable::SetEditFormat(CString strFormat)
{
    BOOL status = FALSE;

    switch (GetType())
    {
        case TYPE_FLT:
            // The format must contain an 'f' or 'g' in it to be valid
            if (strFormat.Find('f') != -1 || strFormat.Find('g') != -1)
            {
                m_edtFormat = strFormat;
                status = TRUE;
            }
            break;
        
        case TYPE_DBL:
            break;

        case TYPE_INTEGER:
        case TYPE_INDEX:
            if (strFormat.Find('d') != -1)
            {
                m_edtFormat = strFormat;
                status = TRUE;
            }
            break;

        case TYPE_UNSIGN_INT:
            if (strFormat.Find('d') != -1)
            {
                m_edtFormat = strFormat;
                status = TRUE;
            }
            break;

        case TYPE_STRING:
            break;

        case TYPE_ENUM:
        case TYPE_BITENUM:
        case TYPE_BIT:
            break;

        default:
            break;
    }

    return status;
}

/* deprecated 08feb11  CString CDDLVariable::GetEditFormat()
{
    hCNumeric* pNum;
    wstring szEdit;
    CString strTemp;
        strTemp = "%";
     // Read DD attributes if CVar* is not NULL
    if(m_pddbVar)
    {
        pNum = (hCNumeric*)m_pddbVar;
        pNum->ReadForEdit(szEdit);
        strTemp += szEdit.c_str();
        if (strTemp.GetLength() > 1) // will always be != "")...we put a '%' in it, dufus
            m_edtFormat = strTemp;
    }
    m_edtFormat.MakeLower();// stevev 5oct07-uppers act like lowers - see C spec
    return m_edtFormat;
}
***/
/*   code removed 13aug08 */
BOOL CDDLVariable::SetCharLength(BYTE cLength)
{
    BOOL status = FALSE;
    if (GetType() == TYPE_STRING && cLength > 0)
    {
        m_cLength = cLength;
        status = TRUE;
    }
     
    return status;
}

BYTE CDDLVariable::GetCharLength()
{
    return m_cLength;
}

//////////////////////////////////////////////////////////////////////
// Min/Max Attributes
//////////////////////////////////////////////////////////////////////

void CDDLVariable::SetMinMax(int min, int max)
{
    m_varData.min.Add(min);
    m_varData.max.Add(max);
}

int * CDDLVariable::GetMin(UINT& size)
{
    size = m_varData.min.GetSize();
    int * pMin = new int[size];

    for (UINT i=0; i < size; i++)
    {
        pMin[i] = (int)m_varData.min.GetAt(i);
    }

    return pMin;
}

int * CDDLVariable::GetMax(UINT& size)
{
    size = m_varData.max.GetSize();
    int * pMax = new int[size];

    for (UINT i=0; i < size; i++)
    {
        pMax[i] = (int)m_varData.max.GetAt(i);
    }

    return pMax;
}

void CDDLVariable::SetMinMax(float min, float max)
{
    m_varData.min.Add(min);
    m_varData.max.Add(max);
}

float * CDDLVariable::GetMinFloat(UINT& size)
{
    size = m_varData.min.GetSize();
    float * pMin = new float[size];

    for (UINT i=0; i < size; i++)
    {
        pMin[i] = (float)m_varData.min.GetAt(i);
    }

    return pMin;
}

float * CDDLVariable::GetMaxFloat(UINT& size)
{
    size = m_varData.max.GetSize();
    float * pMax = new float[size];

    for (UINT i=0; i < size; i++)
    {
        pMax[i] = (float)m_varData.max.GetAt(i);
    }

    return pMax;
}

//////////////////////////////////////////////////////////////////////
// Enum Attributes
//////////////////////////////////////////////////////////////////////

/*   code removed 13aug08 */
CString CDDLVariable::GetEnumString(int nEnum)
{
    CString strEnum = "";
    UINT32 uEnum = nEnum;
    wstring sEnum;


    // This code is called to get engineering unit string
    // value for a ENUM for display purposes for the List View in TABLE, 
    // and CDDLEDisplay.  This code should probably work in CWaoDlg but
    // it currently has a defect, POB - 1 Aug 2008 (POB) 

//    m_varData.strEnum.Lookup( nEnum, strEnum );

    // Read DD attributes if CVar* is not NULL
    if(m_pddbVar)
    {
        hCEnum* pEnum = (hCEnum*)m_pddbVar;
        //pEnum->getString(uEnum, sEnum);
        if (SUCCESS ==  pEnum->procureString(uEnum, sEnum) )
        {
            strEnum = sEnum.c_str();
        }
        // else leave it blank
    }

    return strEnum;
}
RETURNCODE CDDLVariable::GetEnumList(vector<EnumTriad_t>& retList)
{
    //vector<EnumTriad_t> retList;
    RETURNCODE rt = SUCCESS;
    hCenumList eList(0);

    rt = ((hCEnum*)m_pddbVar)->procureList(eList);
    if ( rt == SUCCESS && eList.size() > 0 )
    {
        EnumTriad_t wrkTriad;
        retList.reserve(retList.size() + eList.size());
        for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
        {
            wrkTriad = *it;
            retList.push_back(wrkTriad);
        }
    }
    return rt;
}

RETURNCODE CDDLVariable::GetBitEnumList(CArray<EnumTriad_t,EnumTriad_t&> &subDataList, int &valLen, ulong &value)
{
    RETURNCODE rt = SUCCESS;

   // Add code here
    EnumTriad_t localData;
    hCVar*  pIm = m_pddbVar;
    hCEnum* pEn = NULL;

    pEn = (hCEnum*)pIm;
    value = pEn->getDispValue();
    hCenumList eBitEnumList(pEn->devHndl());
    if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS )
    {
        cerr << "ERROR: No eList for bit enum."<<endl;
        valLen = 0;
        return rt;//no lines
    }
    else
    {
        valLen = pIm->VariableSize();
        for (hCenumList::iterator iT = eBitEnumList.begin(); iT < eBitEnumList.end(); iT++)
        {
            //iT->theComm(pDevice->getCommAPI());
            localData = *iT;
            subDataList.Add(localData);
        }
    }

    return rt;
}


int CDDLVariable::GetEnumSize()
{
    //return m_varData.strEnum.GetSize();
//    return m_varData.strEnum.GetCount();
    return 0;
}

hCddbDevice* CDDLVariable::GetDevicePtr()
{
    hCddbDevice *pCurrDevice = NULL;
    CDDIdeDoc * pDoc = NULL;
    CDocManager * pManager = AfxGetApp()->m_pDocManager;
    ASSERT ( pManager != NULL ); 
    POSITION posTemplate = pManager->GetFirstDocTemplatePosition();

    if ( posTemplate !=NULL )
    {
          // get the next template
          CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
          POSITION posDoc = pTemplate->GetFirstDocPosition();
          // Get the first Documen Ptr
          pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
    }

    if (pDoc != NULL)
        return pDoc->pCurDev;

        return pCurrDevice;
}

CString CDDLVariable::GetUnitStr()
{
    wstring ty;
    //30nov06 - use the more complete version:was::>ty = m_pddbVar->getUnitStr();
    // 1 Aug 08 (POB) - This code is called only to get the engineering unit string
    // associated with this VARIABLE for display purposes as part of a Unit 
    // relation or a constant unit for the List View in TABLE, CWaoDlg and 
    // CDDLEDisplay
    m_pddbVar->getUnitString(ty);
    return ty.c_str();
}

BOOL CDDLVariable::IsValid()
{
    if(m_pddbVar)
    {
        return m_pddbVar->IsValid();
    }
    else
    {
        cerr<<"BIG ERROR *?"<<endl;
        return m_bIsValid;
        //return TRUE;
    }
}

BOOL CDDLVariable::IsChanged()
{
    BOOL bRetVal = FALSE;
    // Removed code that is no longer used, POB - 15 Aug 2008
    
    // Calling device object to determine if variable has changed
    // isChanged() in the device object is the same for all data types, POB - 15 Aug 2008
    bRetVal = m_pddbVar->isChanged(); 

    // Removed code that is no longer used, POB - 15 Aug 2008

    return bRetVal;
}
/*<REMOVE> stevev 020304 - PAR 5256 - use the baseclass name 
CString CDDLVariable::GetName()
{
    string strName = "";

    if (m_pddbVar)
        m_pddbVar->Label(strName);

    return strName.c_str();
}
end <REMOVE> stevev 020304 - PAR 5256 */
void CDDLVariable::Cancel()
{
    if (m_pddbVar)
        m_pddbVar->CancelIt();
}

ulong CDDLVariable::GetId()
{
    ulong nId = 0;

    if (m_pddbVar)
        nId = m_pddbVar->getID();

    return nId;
}
/*   code removed 13aug08 */


/*************************************************************************************************
 *
 *   $History: DDLVariable.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
