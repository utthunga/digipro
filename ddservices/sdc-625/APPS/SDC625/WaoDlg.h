#if !defined(AFX_WAODLG_H__AC33A0BC_8A04_4F7A_B29D_8048A8F07969__INCLUDED_)
#define AFX_WAODLG_H__AC33A0BC_8A04_4F7A_B29D_8048A8F07969__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaoDlg.h : header file
//

#include "DDLVariable.h"
#include "MainListCtrl.h"
/////////////////////////////////////////////////////////////////////////////
// CWaoDlg dialog

class CWaoDlg : public CDialog
{
// Construction
public:

	//CWaoDlg(CDDLVariable* pVar, ITEM_ID lItemId, CWnd* pParent = NULL);   // standard constructor
	CWaoDlg(hCVar* pVar, ITEM_ID lItemId, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CWaoDlg)
	enum { IDD = IDD_WAO_LIST };
	CDlgListCtrl	m_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWaoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkWaoList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickWaoList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMenuExecute();
	afx_msg void OnDestroy();
	afx_msg void OnMenuHelp();
	afx_msg void OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu );
	afx_msg void OnUpdateMenuExecute(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	//CDDLVariable*	m_pVar;
	hCVar*          m_pVar;		// the original var that triggered the wao
	hCwao*          m_pWao;		// the device object wao 
	ITEM_ID			m_lItemId;
	CImageList		m_ctlImage;

	void AddVariableToList(CDDLVariable *pVar, LV_ITEM	&lv_item);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAODLG_H__AC33A0BC_8A04_4F7A_B29D_8048A8F07969__INCLUDED_)
