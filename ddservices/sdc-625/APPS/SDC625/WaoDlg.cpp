// WaoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"
#include "WaoDlg.h"
#include "VariableDlg.h"
#include "SDC625ListView.h" // Column definitions
#include "LabelDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWaoDlg dialog


//CWaoDlg::CWaoDlg(CDDLVariable* pVar, ITEM_ID lItemId, CWnd* pParent /*=NULL*/)
CWaoDlg::CWaoDlg(hCVar* pVar, ITEM_ID lItemId, CWnd* pParent /*=NULL*/)
    : CDialog(CWaoDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CWaoDlg)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT

    m_pVar    = pVar;
    m_lItemId = lItemId;

    hCdeviceSrvc *pCurDev = m_pVar->devPtr();
    hCitemBase *pItemBase;

    m_pWao = NULL; // all the else's
    if (pCurDev != NULL)
    {
        if (pCurDev->getItemBySymNumber(m_lItemId, &pItemBase) == SUCCESS)
        {
            m_pWao = (hCwao *)pItemBase;
        }
    }
}


void CWaoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CWaoDlg)
    DDX_Control(pDX, IDC_WAO_LIST, m_List);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWaoDlg, CDialog)
    //{{AFX_MSG_MAP(CWaoDlg)
    ON_NOTIFY(NM_DBLCLK, IDC_WAO_LIST, OnDblclkWaoList)
    ON_NOTIFY(NM_RCLICK, IDC_WAO_LIST, OnRclickWaoList)
    ON_COMMAND(ID_MENU_EXECUTE, OnMenuExecute)
    ON_WM_DESTROY()
    ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
    ON_UPDATE_COMMAND_UI(ID_MENU_EXECUTE, OnUpdateMenuExecute)
    //}}AFX_MSG_MAP
    ON_WM_INITMENUPOPUP()   // Need this since ON_UPDATE_COMMAND_UI is not supported in CDialog for pop-up menus.
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWaoDlg message handlers

BOOL CWaoDlg::OnInitDialog() 
{
    CDialog::OnInitDialog();

    //caller does the pre edit action methods

    vector<hCVar*> varList;
    LV_ITEM     lv_item;
    CString     strTitle, strTemp;

    /*
     * Create the Image List
     */
    // Vibhor 130603 : Start of Code
    m_ctlImage.Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    CBitmap bmp;
    bmp.LoadBitmap(IDB_IMAGELIST);
    m_ctlImage.Add(&bmp, RGB(255,0,255));
    
    // Vibhor 130603 : End of Code
    /*
     * Attach images to List
     */
    m_List.SetImageList(&m_ctlImage, LVSIL_SMALL);
    strTitle = "Item";
    m_List.InsertColumn( COL_NAME, strTitle, LVCFMT_LEFT, 100, 0);
    strTitle = "Value";
    m_List.InsertColumn( COL_DATA, strTitle, LVCFMT_RIGHT, 100, 0);
    strTitle = "Units";
    m_List.InsertColumn( COL_UNITS, strTitle, LVCFMT_LEFT, 100, 0);

    /*
     * How many objects in this CList? Not sure since there are two lists
     */
//  m_cntlDsply.SetItemCount( m_pEDsply->m_ChildList.GetCount() );

    /*
     * Initialize the LV_ITEM structure that's used to populate the list
     */
    lv_item.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
    lv_item.iSubItem = COL_NAME;


    if ( m_pWao != NULL )
    {
        m_pWao->getWAOVarPtrs(varList);

        for (vector<hCVar*>::iterator iT = varList.begin(); iT < varList.end(); iT++)
        {
            CDDLVariable *pVar = new CDDLVariable(*iT);
            AddVariableToList(pVar, lv_item);
        }
        varList.clear();
    }
	DEBUGLOG(CLOG_LOG,"WAO init-dialog is done (Wnd:%08x).\n",m_hWnd);
    PostMessage(NM_DBLCLK, 0, 0);

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

void CWaoDlg::OnDblclkWaoList(NMHDR* pNMHDR, LRESULT* pResult) 
{

  CString       strTemp;
  int nItem;

    POSITION pos = m_List.GetFirstSelectedItemPosition();
    if (pos != NULL)
    {
        nItem = m_List.GetNextSelectedItem(pos);
        if (nItem != -1)
        {
            CDDLVariable *pVar = (CDDLVariable *)m_List.GetItemData(nItem);
            CDDLBase  *pParent = pVar->GetParent();
            BOOL bRO = 0;
            if (pParent && pParent->IsReadOnly() ) bRO = 1;
            if (!pVar->IsReadOnly())
            {// wao's have no actions   , the do-execute will handle var actions            
                AfxGetMainWnd()->SendMessage(WM_DO_EXECUTE,bRO,(LPARAM)pVar->m_pddbVar);
/*************************************************************************/
//              RETURNCODE rc = SUCCESS;
//              CVariableDlg dlg(pVar->m_pddbVar);          
//              
//              int nRet = dlg.DoModal();               
//                      
//              if(nRet != IDOK)
//                return;
                
            }
      //prashant 120204
      strTemp = *pVar;
      //prashant 120204 end

        }
    }
    
  //prashant 120204
   m_List.SetItem(nItem,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
  //prashant 120204 end

    *pResult = 0;
}

void CWaoDlg::AddVariableToList(CDDLVariable *pVar, LV_ITEM &lv_item)
{
    string      ty;
    CString     strTemp;
    int         nRow = 0;

    if (pVar->IsVariable())
    {
        // Make sure the Variable is valid before inserting it into list
        if (pVar->IsValid())
        {
            strTemp = pVar->GetName();
            lv_item.iItem = 0; 
            lv_item.pszText = (LPTSTR)((LPCTSTR)strTemp);//stevev was::> (char *)(const char *)strTemp;
            lv_item.lParam = (LPARAM)pVar;
            lv_item.iImage = pVar->GetImage();  // Added function to get image ID; POB - 5/16/2014
            nRow = m_List.InsertItem(&lv_item);
			
            /* Removed commented out code, POB - 5/20/2014 */

            strTemp = *pVar;   // Update data

            m_List.SetItem(nRow,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
            strTemp = pVar->GetUnitStr();
            m_List.SetItem(nRow,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
        }
    }
}

void CWaoDlg::OnRclickWaoList(NMHDR* pNMHDR, LRESULT* pResult) 
{
    // TODO: Add your control notification handler code here
    CDDLBase*      pItem;
    CMenu          menu;
    CMenu*         pSubmenu;
    CPoint         ptClient;
    CDDIdeDoc * pDoc = NULL;
    LV_HITTESTINFO HitTestInfo;
    int nPos;

    NMITEMACTIVATE* plistItem = (NMITEMACTIVATE*)pNMHDR;
    /*
     * Get the mouse position associated with this message
     * and see if it is on a tree item.
     */
 
    /*
     * Retrieve the class pointer for this item,
     * then use it to fill the list Control
     */
    int index = plistItem->iItem; // Get the index where this item is in control

    DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));
    ptClient = point;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
//    index = m_cntlEdit.HitTest( &HitTestInfo ); /* Hit test not working outside CListView*/
//    if( LVHT_ONITEM & HitTestInfo.flags )
    if (index != -1)
    {
        /*
         * The click was on a list item.
         * Get the CDDLBase pointer from the selected object
         * and have it load the menu.
         */
        pItem = (CDDLBase *)m_List.GetItemData( index );
        pItem->LoadContextMenu( &menu, nPos ) ;

        if( pItem )
        {
//            pDoc = GetDocument();
//            pDoc->m_pMenuSource = pItem;
            pSubmenu = menu.GetSubMenu(nPos);
            pSubmenu->TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
                                      point.x, point.y, this);
        }
    }

    /*
     * Set *pResult to 0 if you want MFC's default call to OnRClick which will result
     * in a call to OnContextMenu() as well which we don't want here, so set to 1.
     */
    *pResult = 1;
}

void CWaoDlg::OnMenuExecute() 
{
    LRESULT Result;

    OnDblclkWaoList(NULL, &Result);
}

void CWaoDlg::OnUpdateMenuExecute(CCmdUI* pCmdUI) 
{
    // TODO: Add your command update UI handler code here
    POSITION pos = m_List.GetFirstSelectedItemPosition();
    
    int nItem = m_List.GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_List.GetItemData(nItem);
        
        if (pBase)
        {
            pBase->OnUpdateMenuExecute(pCmdUI);
        }
    }
}


void CWaoDlg::OnDestroy() 
{
    CDialog::OnDestroy();
    // Clean up
    for (int nCount = 0; nCount < m_List.GetItemCount(); nCount++)
    {
        CDDLVariable *pVar = (CDDLVariable *)m_List.GetItemData(nCount);
        if (pVar)
        {
            delete pVar;
            pVar = NULL;
        }
    }

}

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
void CWaoDlg::OnMenuHelp() 
{
    // TODO: Add your command handler code here

    // TODO: Add your command handler code here
    CString strItemCaption;

    // New code added by Jeyabal

    POSITION pos = m_List.GetFirstSelectedItemPosition();
    int nItem = m_List.GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLMenuItem *pBase = (CDDLMenuItem *)m_List.GetItemData(nItem);
        CString strName = pBase->GetName();
        CString strHelp = pBase->GetHelp();

        CHelpBox helpBox(strName, strHelp);
        helpBox.Display();
        /* code removed 16aug08 */
    }
}

// CDialog based classes do not support ON_UPDATE_COMMAND_UI for pop-up menus.
// Must add code to support the enabling/disabling of menu items
void CWaoDlg::OnInitMenuPopup(CMenu *pPopupMenu, UINT nIndex,BOOL bSysMenu)
{
    ASSERT(pPopupMenu != NULL);
    // Check the enabled state of various menu items.

    CCmdUI state;
    state.m_pMenu = pPopupMenu;
    ASSERT(state.m_pOther == NULL);
    ASSERT(state.m_pParentMenu == NULL);

    // Determine if menu is popup in top-level menu and set m_pOther to
    // it if so (m_pParentMenu == NULL indicates that it is secondary popup).
    HMENU hParentMenu;
    if (AfxGetThreadState()->m_hTrackingMenu == pPopupMenu->m_hMenu)
        state.m_pParentMenu = pPopupMenu;    // Parent == child for tracking popup.
    else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
    {
        CWnd* pParent = this;
           // Child windows don't have menus--need to go to the top!
        if (pParent != NULL &&
           (hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
        {
           int nIndexMax = ::GetMenuItemCount(hParentMenu);
           for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
           {
            if (::GetSubMenu(hParentMenu, nIndex) == pPopupMenu->m_hMenu)
            {
                // When popup is found, m_pParentMenu is containing menu.
                state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
                break;
            }
           }
        }
    }

    state.m_nIndexMax = pPopupMenu->GetMenuItemCount();
    for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
      state.m_nIndex++)
    {
        state.m_nID = pPopupMenu->GetMenuItemID(state.m_nIndex);
        if (state.m_nID == 0)
           continue; // Menu separator or invalid cmd - ignore it.

        ASSERT(state.m_pOther == NULL);
        ASSERT(state.m_pMenu != NULL);
        if (state.m_nID == (UINT)-1)
        {
           // Possibly a popup menu, route to first item of that popup.
           state.m_pSubMenu = pPopupMenu->GetSubMenu(state.m_nIndex);
           if (state.m_pSubMenu == NULL ||
            (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
            state.m_nID == (UINT)-1)
           {
            continue;       // First item of popup can't be routed to.
           }
           state.DoUpdate(this, TRUE);   // Popups are never auto disabled.
        }
        else
        {
           // Normal menu item.
           // Auto enable/disable if frame window has m_bAutoMenuEnable
           // set and command is _not_ a system command.
           state.m_pSubMenu = NULL;
           state.DoUpdate(this, FALSE);
        }

        // Adjust for menu deletions and additions.
        UINT nCount = pPopupMenu->GetMenuItemCount();
        if (nCount < state.m_nIndexMax)
        {
           state.m_nIndex -= (state.m_nIndexMax - nCount);
           while (state.m_nIndex < nCount &&
            pPopupMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
           {
            state.m_nIndex++;
           }
        }
        state.m_nIndexMax = nCount;
    }
} 