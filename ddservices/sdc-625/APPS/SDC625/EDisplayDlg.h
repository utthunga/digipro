/*************************************************************************************************
 *
 * $Workfile: EDisplayDlg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "EDisplayDlg.h".		
 */
#if !defined(AFX_EDISPLAYDLG_H__EB268BDB_CF9B_426F_B23E_3A6029AC823C__INCLUDED_)
#define AFX_EDISPLAYDLG_H__EB268BDB_CF9B_426F_B23E_3A6029AC823C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MainListCtrl.h"

/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/

#define OFFSET	            2

class CEDisplayDlg : public CDialog
{
// Construction
public:
	CEDisplayDlg(CDDLEDisplay* pEDsply, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEDisplayDlg)
	enum { IDD = IDD_EDIT_DISPLAY_DIALOG };
	CDlgListCtrl	m_cntlEdit;
	CListCtrl	    m_cntlDsply;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEDisplayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEDisplayDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
    afx_msg void OnEditRclick(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDsplyRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDblclkEditList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMenuExecute();
	afx_msg void OnMenuHelp();
	afx_msg void OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu );
	afx_msg void OnUpdateMenuExecute(CCmdUI* pCmdUI);
#ifdef GE_BUILD
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
#endif	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
    float m_Test;
	void Update(CDDLBase* pBase);
	CDDIdeDoc* GetDocument();
    CImageList m_ctlImage;
	CDDLEDisplay* m_pEDsply;
public:
#ifdef GE_BUILD
    CString		dialog_title;	// 00.01.10
	BYTE number_of_items;		// 00.01.10
#endif	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDISPLAYDLG_H__EB268BDB_CF9B_426F_B23E_3A6029AC823C__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: EDisplayDlg.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
