// SDC625WindowView.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "SDC625Doc.h"
#include "SDC625WindowView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _TESTDATA
extern int fillData(UINT mySymNum);
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView

IMPLEMENT_DYNCREATE(CSDC625WindowView, CFormView)

CSDC625WindowView::CSDC625WindowView()
	: CFormView(CSDC625WindowView::IDD)
{
	//{{AFX_DATA_INIT(CSDC625WindowView)
	//}}AFX_DATA_INIT
}

CSDC625WindowView::~CSDC625WindowView()
{
}

void CSDC625WindowView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSDC625WindowView)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSDC625WindowView, CFormView)
	//{{AFX_MSG_MAP(CSDC625WindowView)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CSDC625WindowView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView drawing

void CSDC625WindowView::OnDraw(CDC* pDC)
{
	CDDIdeDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

void CSDC625WindowView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	CDDIdeDoc* pDoc = GetDocument();

	OnUpdate( NULL, UPDATE_REBUILD_ALL, NULL );

}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView printing

BOOL CSDC625WindowView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSDC625WindowView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSDC625WindowView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView diagnostics

#ifdef _DEBUG
void CSDC625WindowView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSDC625WindowView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

CDDIdeDoc* CSDC625WindowView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDDIdeDoc)));
	return (CDDIdeDoc*)m_pDocument;
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView message handlers

BOOL CSDC625WindowView::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	//wParam
	//The low-order word of wParam identifies the command ID of the menu item, control, or accelerator.
	//The high-order word of wParam specifies the notification message if the message is from a control.
	//If the message is from an accelerator, the high-order word is 1.
	//If the message is from a menu, the high-order word is 0.

	//lParam
	//Identifies the control that sends the message if the message is from a control. Otherwise, lParam is 0.

	WORD wControlID = LOWORD(wParam);
	WORD wMessageID = HIWORD(wParam);

	BOOL result;

	if (wControlID != 0) 
	{
		result = HandleCID(wControlID, wMessageID);
	}

	return CFormView::OnCommand(wParam, lParam);
}

void CSDC625WindowView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	CDDIdeDoc *pDoc = GetDocument();
    CDDLBase *pItem = ( CDDLBase* )pHint;
    CDDLBase *pParent = NULL;
	CString strTemp;
	
	switch( lHint )
    {
        case UPDATE_REBUILD_ALL:  // ignore pHint
			if( pDoc->m_pRoot/*pDoc->m_pMain*/ )
            {
				// This should rebuild all the menus and variables in a the window view
				// but right now the "test case" looks only for the string name "PV".
				// If this was done correctly I would ask the software to give me
				// the primary variable regardless of string name.
				CDDLBase *pBase = pDoc->m_pRoot;
				POSITION pos = pBase->GetChildList().GetHeadPosition();
				while( pos )
				{
					CDDLBase* pChild = pBase->GetChildList().GetNext( pos );

					strTemp = pChild->GetName();
                
					if (pChild->IsDisplayValue())
					{
						CDDLMenuItem *pMenu = (CDDLMenuItem *)pChild;

						CDDLVariable *pVar = (CDDLVariable *)pMenu->m_item;
						
						if (pVar->GetType() == TYPE_ENUM)
						{

						}
						else
						{
							ulong id = pVar->GetId();

							// Check to see if the symbol exists
							// The member function returns an iterator that designates the 
							// earliest element in the controlled sequence whose sort key equals key. 
							// If no such element exists, the iterator equals end().
							IdWindowMap::iterator pos;
	
							pos = m_WndCntrlMap.find(id);

							if (pos != m_WndCntrlMap.end())
							{
								CString strValue = *pVar;   // Get Value
								// symbol key exists - update the control
								m_WndCntrlMap[id].m_pWnd->SetWindowText(strValue);

								CWnd * pLabel = GetDlgItem( m_WndCntrlMap[id].nLabelID );

								pLabel->SetWindowText(strTemp);

								CWnd * pUnit = GetDlgItem( m_WndCntrlMap[id].nUnitID );

								if (pChild->IsVariable())
								{
									strTemp = pMenu->GetUnitStr();
									pUnit->SetWindowText(strTemp);
								}
							}

/*							if (strTemp == "PV")
							{
								ulong id = pVar->GetId();
								m_strPv = strTemp + "   " + *pVar;   // Update data
							}*/

							
						}

/*						if (pChild->IsVariable())
						{
							strTemp = pMenu->GetUnitStr();
							m_strPv = m_strPv + "  " + strTemp;
						}*/

					}

				}
			}
            break;

        case UPDATE_REBUILD_LIST:
			break;

        case UPDATE_DELETE_BELOW:  // pItem is parent.  delete below.
            break;

        case UPDATE_REMOVE_ITEM:  // pItem (menu item) is item to remove.
            break;

        case UPDATE_ADD_ITEM:  // pItem (menu item) is item to add.
            break;

        case UPDATE_ITEM:   // pItem (menu item) is item to update
            {
                strTemp = pItem->GetName();
                
                if (pItem->IsDisplayValue())
                {
					CDDLMenuItem *pMenu = (CDDLMenuItem *)pItem;

					CDDLVariable *pVar = (CDDLVariable *)pMenu->m_item;
					
					if (pVar->GetType() == TYPE_ENUM)
					{

					}
					else
					{
						ulong id = pVar->GetId();

						// Check to see if the symbol exists
						// The member function returns an iterator that designates the 
						// earliest element in the controlled sequence whose sort key equals key. 
						// If no such element exists, the iterator equals end().
						IdWindowMap::iterator pos;
	
						pos = m_WndCntrlMap.find(id);

						if (pos != m_WndCntrlMap.end())
						{
							CString strValue = *pVar;   // Get Value
							// symbol key exists - update the control
							m_WndCntrlMap[id].m_pWnd->SetWindowText(strValue);

							CWnd * pLabel = GetDlgItem( m_WndCntrlMap[id].nLabelID );

							pLabel->SetWindowText(strTemp);

							CWnd * pUnit = GetDlgItem( m_WndCntrlMap[id].nUnitID );

							if (pItem->IsVariable())
							{
								strTemp = pMenu->GetUnitStr();
								pUnit->SetWindowText(strTemp);
							}
						}
					}


/*					else
					{
						if (strTemp == "PV")
						{
//							m_strPv = strTemp + "   " + *pVar;   // Update data
						}
					}

					if (pItem->IsVariable())
					{
						strTemp = pMenu->GetUnitStr();
//						m_strPv = m_strPv + "  " + strTemp;
					}*/
					
                }

            }
            break;

        default:
            break;
    }

	UpdateData(FALSE); // Send data to dialog screen
}

void CSDC625WindowView::Initialize(ulong id)
{
	mySymNum = id;
/* stevev expansion 8/26/04
	ulong uSymbol = id;

	// Check to see if the symbol exists
	// The member function returns an iterator that designates the 
	// earliest element in the controlled sequence whose sort key equals key. 
	// If no such element exists, the iterator equals end().
	IdWindowMap::iterator pos;
	
	// Don't create a new edit control if there is already one
	pos = m_WndCntrlMap.find(uSymbol);
	if (pos != m_WndCntrlMap.end())
		return;

	UINT uControl = 1;
//	CEdit * pEdit = new CEdit;
*/
/*	if ( !pEdit->CreateEx(WS_EX_CLIENTEDGE,"edit", "Control #1", 
		              WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE | ES_AUTOHSCROLL,
					  CRect(10, 10, 100, 34), this, uControl ) )
	{
		DestroyWindow();
	}*/
/* stevev 8/26/04 */
	RETURNCODE rc = SUCCESS;

#ifdef _TESTDATA

    fillData(id); // prep all

#else

	hCitemBase*    p_myItem;

	if (PCURDEV == NULL )
	{
		return; /// error -  no device!!!!!!!!!!!!
	}
	rc = PCURDEV->getItemBySymNumber(mySymNum, &p_myItem);
	if ( rc != SUCCESS || p_myItem == NULL )
	{
		return; // error:  item I am supposed to be doesn't exist !!!!!!!!!!!!
	}
	if (p_myItem->getIType() != iT_Menu)
	{
		return; // error:  I am supposed to be a menu of type window !!!!!!!!!!!!
	}

	if ( m_WndDrawMap.size() > 0 )// we have been initialized before
	{
		// TODO clear entire window, deleteing all contents (a re-initialize)
	}
#define DEVHNDL ( p_myItem->devHndl() )

	RETURNCODE  pib->aquire(vmi, mds_window );/* NOT: pib->getStyle(), our draw style may not match our DD style*/
// TODO: when to change to mds_dialog
// verify success and size() > 0

#endif
/* end stevev 8/26/04 */	
/* replaced by stevev 8/26/04
	////////////////////////// my new stuff/////////////////////////////////
	CDynControls * pDlgControls = new CDynControls(this);
	CWindowMap * pWinMap = new CWindowMap;

	pDlgControls->AddDlgControl(GROUP);  // Add group box
	
	UINT nVarCtrl = pDlgControls->AddDlgControl(VAR,pWinMap);  // Insert VARIABLE
	
	

	pDlgControls->AddDlgControl(Tab);  // Add group box

	// Now create the controls
	pDlgControls->Create(this);

	CEdit * pEdit = (CEdit *) pDlgControls->GetDlgCtrl( nVarCtrl );

	pWinMap->m_pWnd = pEdit;

	m_WndCntrlMap[id] = *pWinMap;
	
	//////////////////////////////////////////////////////////////////////////
end stevev replace 8/26/04 */
/* stevev 08/26/04 - new stuff */

	int r = FillSelf(id);

	    FillSize();


/* Paul,  this is where the dyncontrols are instantiated.
   There is a dyncontrol pointer in each item.
   You will need to traverse the tree, creating as you go.
   Since you know all the sizes of everything before you start,
   you should be able to create these efficiently.
   I suspect you will have to pass one or more parameters ( like 'this' )
   to the create.
   I will be doing the update monday.
*/
	pnt_t temp = {1,1};
	createControl(this, temp);// make the DynControls here (entire tree)

/* end - stevev 08/26/04 - new stuff */

//	CFont * pFont = GetFont();
//	pEdit->SetFont(pFont, TRUE);

//	CWindowMap WndMap(pEdit);

	// Associate the symbol id with the Window Map for later updating
//	m_WndCntrlMap[uSymbol] = WndMap;

	//Update view
	OnUpdate( NULL, UPDATE_REBUILD_ALL, NULL );

}

void CSDC625WindowView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	
	/* 
	 * iterate through all of the enteries in
	 * WindowMap to destroy controls and delete
	 * the associated memory
	 */
}


void CSDC625WindowView::layoutWindow(void) // sizes and locates
{
#if 0
	// verify we have a map
	if ( m_WndDrawMap.size() <= 0 )
	{// error
		return;
	}

	// size the map
	Id2DrawMap_t::iterator i;
	for (  i = m_WndDrawMap.begin(); i != m_WndDrawMap.end(); ++i)
	{
		i->second->FillSize();
	}

	// locate the map
	for ( i = m_WndDrawMap.begin(); i != m_WndDrawMap.end(); ++i)
	{
		i->second->leftCol = 1;// TODO temporary
		i->second->top_Row = 1;
	}
#endif // 0 //
}
/*
void CSDC625WindowView::createControl(void)// make 'em to spec
{
	// create all the controls to spec
	CdrawBase * pBase;

	int count = 0;

	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{

		pBase = (*i);
		pBase->createControl(this);
		count++;

		if (count == 3)
			break;
	}\

	pnt_t temp = {0,0};
	createMenu(this, temp);

}*/

void CSDC625WindowView::OnChangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
}


