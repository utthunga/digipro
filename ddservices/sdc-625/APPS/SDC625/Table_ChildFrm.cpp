// TableChildFrm.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "Table_ChildFrm.h"
#include "SDC625Doc.h"
#include "MainFrm.h"
#include "Errlogview.h"
#include "MainListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrame

IMPLEMENT_DYNCREATE(CTableChildFrame, CChildFrame)

BEGIN_MESSAGE_MAP(CTableChildFrame, CChildFrame)
    //{{AFX_MSG_MAP(CTableChildFrame)
    ON_WM_CLOSE()
    ON_WM_SIZE()
    ON_WM_TIMER()
    ON_WM_MDIACTIVATE() 
    ON_WM_LBUTTONDBLCLK()/*testing- never hit*/
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CTableChildFrame::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    LOGIT(CLOG_LOG,"TestDouble Click\n");
}
// no longer used   ON_WM_TIMER()
/////////////////////////////////////////////////////////////////////////////
// CTableChildFrame construction/destruction

CTableChildFrame::CTableChildFrame():m_pTreeView(NULL),m_pListView(NULL)//,m_pSelection(NULL)
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame instantiated.\n");
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
//  pMainWnd->m_nchildren++;
//  AfxMessageBox("CTableChildFrame Created");
// main frame should no longer need this value....  pMainWnd->pTableChildFrm = this;
#ifdef LOGON_RGT_PANE
    m_bRightPaneSplitterCreated=FALSE;
#endif
    ticksTillUpdate = 0;
    m_nTimerNumber  = 0;
    running = true;
    inTimer = false;

    //  Intialize the User Selection information, POB - 5/22/2014
    m_UserSelect.selectedID = 0;   // No user selection has yet to be made
    m_UserSelect.parentID = 0;     // No user selection has yet to be made
	
	// Intialize the "root" (ie, top level) menu used as the first CDDLBase
	// in the tree control 
    m_pRoot = NULL;
}

CTableChildFrame::~CTableChildFrame()
{
    running = false;
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
// main frame should no longer need this value....      pMainWnd->pTableChildFrm = NULL;
//  if ( ! ::IsWindow(m_hWnd) )
//  {
//      assert(0);
//  }
//  KillTimer(   m_nTimerNumber );// timer will trigger and go nowhere, crashing system
}

BOOL CTableChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame PreCreateWindow.\n");
    cs.lpszName = _T("Table View");
    cs.style &= ~FWS_ADDTOTITLE;
    if( !CChildFrame::PreCreateWindow(cs) )
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame PreCreateWindow - Exit FALSE\n");
        return FALSE;
    }
    else
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame PreCreateWindow - Exit TRUE\n");
        return TRUE;
    }
}

CDDLBase *CTableChildFrame::getCurrentSelection(void) 
{
    CDDLBase* pRetBase = NULL;
    HTREEITEM hItem    = NULL;
    if ( m_pTreeView != NULL )
    {
        hItem = m_pTreeView->GetTreeCtrl().GetSelectedItem();
        if ( hItem != NULL )
        {
            pRetBase = (CDDLBase*)(m_pTreeView->GetTreeCtrl().GetItemData(hItem));
        }
    }
    return pRetBase;
}

// this is only usable to go DOWN a menu..ie set selection to the current selection's child
// we need to match the item data pointers and the position (in case there are multiple Bases)
// false on success, true on failure
bool CTableChildFrame::setCurrentSelection(CDDLBase* pinBase,int idx)
{
    CDDLBase* pBase = NULL, *pOrigBase = NULL, *pItem = NULL;
    HTREEITEM hItem = NULL,  hOrigItem = NULL;
    int r = 0;

    if ( m_pTreeView == NULL )
    {
        return true;// failure
    }
    // we have to get the current selection, check its children for the new item
    // once found, unselect the current and select new
    hOrigItem = m_pTreeView->GetTreeCtrl().GetSelectedItem();
    if ( hOrigItem != NULL )
    {
        pOrigBase = (CDDLBase*)(m_pTreeView->GetTreeCtrl().GetItemData(hOrigItem));

        HTREEITEM hmyItem = m_pTreeView->GetTreeCtrl().GetSelectedItem();

        // Delete all of the children of hmyItem.
        if ( m_pTreeView->GetTreeCtrl().ItemHasChildren(hmyItem) )
        {
            int nIdx = 0;
            HTREEITEM hNextItem,
            hChildItem = m_pTreeView->GetTreeCtrl().GetChildItem(hmyItem);// first

            while (hChildItem != NULL)
            {
                hNextItem = m_pTreeView->GetTreeCtrl().GetNextItem(hChildItem, TVGN_NEXT);

                if (nIdx == idx)
                {// check the data
                    pBase = (CDDLBase*)(m_pTreeView->GetTreeCtrl().GetItemData(hChildItem));
                    if (pBase == pinBase)
                    {// we found the one we want
                        // clear the old
                        r = m_pTreeView->GetTreeCtrl().
                            SetItemState(hOrigItem, 0, TVIS_SELECTED); // zero on failure
                        
                        // set the new
                        r = m_pTreeView->GetTreeCtrl().
                            SetItemState(hNextItem, TVIS_SELECTED, TVIS_SELECTED);
                        BOOL k = m_pTreeView->GetTreeCtrl().SelectItem(hNextItem);
                        r = (int)k;// successfull selection sets return false
                        // we are done
                        break; // out of while loop
                    }
                    else
                    {
                        TRACE("ERROR: data doesn't match index in selection.\n");
                    }
                }
                hChildItem = hNextItem;
                nIdx++; // assume caller is zero based index as well
            }
        }
        else
        {
            TRACE("ERROR::>>  Trying to set a child selection for item with no children.\n");
        }

    }
    if ( ! r )
        return true;// failure
    else
        return false;// success
}

/*
 * Modified to be recursive in order to search the entire tree control
 * in order to select the exact menu based on its symbol ID and its
 * parent's ID after destroying and rebuilding the entire menu structure,
 * POB - 5/22/2014
 */
HTREEITEM CTableChildFrame::setCurrentSelection(USERSELECT_S select, HTREEITEM hRoot)
{
    CDDLBase* pBase = NULL, *pOrigBase = NULL, *pItem = NULL;
    HTREEITEM hItem = NULL,  hOrigItem = NULL, hChildItem = NULL;
    int r = 0;
    
    unsigned symbolID = select.selectedID;
    unsigned parentID = select.parentID;

    if ( m_pTreeView == NULL || symbolID == 0 )
    {
        return NULL;    // failure
    }

    if (hRoot == NULL)
    {
        hRoot = m_pTreeView->GetTreeCtrl().GetRootItem();
    }

    pBase = (CDDLBase*)(m_pTreeView->GetTreeCtrl().GetItemData(hRoot));
    
    if (pBase)
    {
        unsigned baseParentID = 0;

        if (pBase->GetParent())
        {
            baseParentID = pBase->GetParent()->GetID();
        }

        if (pBase->GetID() == symbolID && baseParentID == parentID)
        {
            // we found the one we want
            
            // clear the old
            hOrigItem = m_pTreeView->GetTreeCtrl().GetSelectedItem();
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hOrigItem, 0, TVIS_SELECTED); // zero on failure

            // set the new
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hRoot, TVIS_SELECTED, TVIS_SELECTED);
            BOOL k = m_pTreeView->GetTreeCtrl().SelectItem(hRoot);
            r = (int)k;// k is zero on error
            
            // we are done
            return hRoot;
        }
    }

    hChildItem = m_pTreeView->GetTreeCtrl().GetChildItem(hRoot);// first

    while (hChildItem)
    {
      // check the children of the current item
      HTREEITEM hFound = setCurrentSelection(select, hChildItem);
      
      if (hFound)
      {
         return hFound;
      }
 
      // get the next sibling of the current item
      hChildItem = m_pTreeView->GetTreeCtrl().GetNextSiblingItem(hChildItem);
    }

    return NULL;    // failure
}


bool CTableChildFrame::setSelection2parent(void)// true on failure
{
    int r;
    HTREEITEM hItem = NULL,  hParentItem = NULL;

    if ( m_pTreeView == NULL )
    {
        return true;// failure
    }
    // we have to get the current selection, get its parent, unselect current and select new
    hItem = m_pTreeView->GetTreeCtrl().GetSelectedItem();
    if ( hItem != NULL )
    {
        hParentItem =  m_pTreeView->GetTreeCtrl().GetParentItem( hItem );
        if (hParentItem == NULL)
        {
            return true;// we can't set it that high
        }
        else
        {
            // clear the old
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hItem, 0, TVIS_SELECTED); 
            
            // set the new
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hParentItem, TVIS_SELECTED, TVIS_SELECTED);
            BOOL k = m_pTreeView->GetTreeCtrl().SelectItem(hParentItem);
            return (k)?false:true;// successfull selection sets return false
        }
    }
    return true;// fail
}

bool CTableChildFrame::setSelectionDefault(void)// true on failure
{
    bool retval =  true;
    int r;
    HTREEITEM hItem = NULL,  hDefaultItem = NULL;

    if ( m_pTreeView == NULL )
    {
        retval = true;// failure
    }
    else
    {
        // we have to get the current selection, get its parent, unselect current and select new
        hItem        = m_pTreeView->GetTreeCtrl().GetSelectedItem();
        hDefaultItem = m_pTreeView->GetTreeCtrl().GetRootItem(); 
        if ( hItem != NULL )
        {// clear the old
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hItem, 0, TVIS_SELECTED); 
        }
        if ( hDefaultItem != NULL )
        {// set the new
            r = m_pTreeView->GetTreeCtrl().
                SetItemState(hDefaultItem, TVIS_SELECTED, TVIS_SELECTED);
            BOOL k = m_pTreeView->GetTreeCtrl().SelectItem(hDefaultItem);

            retval = (k)?false:true;// successfull selection sets return false
        }
    }
    return retval;
}



/////////////////////////////////////////////////////////////////////////////
// CChildTableFrame diagnostics

#ifdef _DEBUG
void CTableChildFrame::AssertValid() const
{
    CChildFrame::AssertValid();
}

void CTableChildFrame::Dump(CDumpContext& dc) const
{
    CChildFrame::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrame message handlers

BOOL CTableChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
    CRect rect;
    CWinApp *pApp = AfxGetApp();
    CString strDefault = _T("default");
    CString strSplitterBarPos;

    if (!m_wndSplitter.CreateStatic(this, 1, 2))
    {
         TRACE(_T("Failed to create split bar "));
         return( FALSE );    // failed to create
    }

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame OnCreateClient.\n");
    /*
     * Attempt to restore it to the same size it was when last shutdown
     */
    GetClientRect( &rect );
    CSize size( rect.Size() );
    strSplitterBarPos = pApp->GetProfileString(  SETTINGS, SPLITTER_BAR, strDefault );
    if( strSplitterBarPos == strDefault || strSplitterBarPos == _T("") )
    {
         size.cx /= 3;
    }
    else
    {
         size.cx = _ttoi( (LPCTSTR)strSplitterBarPos );
    }

    if( !m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CDDIdeTreeView), size, pContext) )
    {
         TRACE(_T("Failed to create tree pane"));
         return(FALSE);    // failed to create
    }

#ifdef LOGON_RGT_PANE
//<START>Added by ANOOP for Splitter pane
    size.cx *= 2;
//  size.cy /= 2;
    int newID = m_wndSplitter.IdFromRowCol(0,1);
    m_wndSplitter_RightPane.CreateStatic(&m_wndSplitter, 2,1, WS_CHILD|WS_VISIBLE, newID);

    if(!m_wndSplitter_RightPane.CreateView(0,0, RUNTIME_CLASS(CDDIdeListView), size, pContext))
    {
         TRACE(_T("Failed to create list pane"));
         return(FALSE);    // failed to create
    }

    if(!m_wndSplitter_RightPane.CreateView(1,0,RUNTIME_CLASS(CErrLogView),CSize(0,0),pContext))
    {
         TRACE(_T("Failed to create error pane"));
         return(FALSE);    // failed to create
    }

    if(m_wndSplitter_RightPane) //Added by ANOOP 21FEB2004
        m_wndSplitter_RightPane.LockBar();
#else // only list view in right pane
    
    if( !m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CDDIdeListView), size, pContext) )
    {
         TRACE(_T("Failed to create list pane"));
         return(FALSE);    // failed to create
    }

    m_pTreeView = static_cast<CDDIdeTreeView*>(m_wndSplitter.GetPane(0,0));
    m_pListView = static_cast<CDDIdeListView*>(m_wndSplitter.GetPane(0,1));

    /* stevev 21mar11 - moved from MDIActivate...that function occurs WAYYY too late*/
    
    m_pTreeView->m_pTableFrame = this;
    m_pListView->m_pTableFrame = this;

    /* Removed commented out code, POB - 5/20/2014 */

    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    /* Removed dead code, POB - 5/15/2014 */

#endif
        
    /*
     * Start a timer to update the menu items once in 2 seconds.
     */

    m_nTimerNumber  = SetTimer(VIEW_TIMER, GRANULARITY, NULL);

    ticksTillUpdate = (ONE_SECOND * UPDATESECONDS)/GRANULARITY;
    LOGIT(CLOG_LOG,"Table Background Timer set to %d w/ ticks = %d.\n",
                                                                GRANULARITY, ticksTillUpdate);
    if (!ticksTillUpdate)
      MessageBox(M_UI_NO_TIMER);

    //Added By Deepak
    // Make sure that errorlog menu item is unchecked
// now done earlier.....    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd(); // POB
    //CMenu *pMenu =this ->GetMenu (); // POB
    CMenu *pMenu = pMainWnd->GetMenu();
    CMenu* pSubmenu = pMenu->GetSubMenu (1);    // view menu
    pSubmenu->CheckMenuItem(ID_VIEW_ERRORLOG, MF_UNCHECKED | MF_BYCOMMAND);
    //END mod
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame OnCreateClient - finished.\n");
    return(TRUE);
}


void CTableChildFrame::OnClose() 
{
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame OnClose.\n");
    
    ShowWindow(SW_HIDE);

    if ( m_nTimerNumber )
    {
        KillTimer(VIEW_TIMER);
        m_nTimerNumber = 0;
    }

    // Until the code in the CDDIdeTreeView gets moved to the CMainFrame
    // we must prevent the table frame from closing which would
    // result in the destruction of the CDDIdeTreeView
    CChildFrame::OnClose();  
}

void CTableChildFrame::OnSize(UINT nType, int cx, int cy) 
{
    CChildFrame::OnSize(nType, cx, cy);

#ifdef LOGON_RGT_PANE   
    /*<START>Added by ANOOP 23FEB2004 fixed issues in scrolling of the error log window */
    // Moved code from CMainFrame::OnSize - POB, 2 Aug 2004
    CRect rect;
    GetWindowRect( &rect );
    if(m_bRightPaneSplitterCreated && m_wndSplitter_RightPane )
    {
        m_wndSplitter_RightPane.SetRowInfo(0,rect.Height()/2, 5);
        m_wndSplitter_RightPane.SetRowInfo(1,rect.Height()/2, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 
    }
    else if( ( FALSE == m_bRightPaneSplitterCreated ) && m_wndSplitter_RightPane ) 
    {
        m_wndSplitter_RightPane.SetRowInfo(0,rect.Height(), 5);
        m_wndSplitter_RightPane.SetRowInfo(1,0, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 
  }
/*<END>Added by ANOOP 23FEB2004 fixed issues in scrolling of the error log window   */
#endif
}
#if 0 // moved to mainframe
void CTableChildFrame::OnViewErrorlog()
{
#ifdef LOGON_RGT_PANE
    /*<START>Added by ANOOP for splitter pane   */
    // Moved code from CMainFrame::OnSize - POB, 2 Aug 2004
    CRect rect;
    GetWindowRect( &rect );

    if(m_bRightPaneSplitterCreated)
    {
        m_bRightPaneSplitterCreated=FALSE;
        m_wndSplitter_RightPane.LockBar(TRUE);  //Added by ANOOP 21FEB2004
        m_wndSplitter_RightPane.SetRowInfo(0, rect.Height(), 5);
        m_wndSplitter_RightPane.SetRowInfo(1, 0, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 
    }
        else
    {
        m_bRightPaneSplitterCreated=TRUE;
        GetWindowRect( &rect );
        m_wndSplitter_RightPane.SetRowInfo(0, rect.Height()/2, 5);
        m_wndSplitter_RightPane.SetRowInfo(1, rect.Height()/2, 5);
        m_wndSplitter_RightPane.RecalcLayout(); 

        m_wndSplitter_RightPane.LockBar(FALSE); //Added by ANOOP 21FEB2004  
        CView *pNewView = static_cast<CView*> (m_wndSplitter_RightPane.GetPane (1,0));
    pNewView->OnInitialUpdate();
        CErrLogView *pErrLogView=(CErrLogView *)pNewView;
    pNewView->ShowWindow(SW_SHOW);  
    }   
/*<END>Added by ANOOP       */
#endif
}

BOOL CTableChildFrame::CloseErrorLog()
{
#ifdef LOGON_RGT_PANE
    if(m_bRightPaneSplitterCreated)
    {
        CRect rect;
        GetWindowRect( &rect );

        if(m_bRightPaneSplitterCreated)
        {
            m_bRightPaneSplitterCreated=FALSE;

            CView *pNewView = static_cast<CView*> (m_wndSplitter_RightPane.GetPane (1,0));
            CErrLogView *pErrLogView=(CErrLogView *)pNewView;
            pErrLogView->EmptyListCtrl(); 

            m_wndSplitter_RightPane.LockBar(TRUE);  //Added by ANOOP 21FEB2004
            m_wndSplitter_RightPane.SetRowInfo(0, rect.Height(), 5);
            m_wndSplitter_RightPane.SetRowInfo(1, 0, 5);
            m_wndSplitter_RightPane.RecalcLayout(); 
        
        }
    }
#endif
    return TRUE;
}
#endif // moved to mainframe
//***********************************************************************
// Function: CDynaMDIChildWnd::OnMDIActivate()
//
// Purpose:
//    OnMDIActivate is called for the MDI child window being
//    deactivated and the child window being activated.
//
//    We override this function so that we can update the View submenu
//    when a child window is activated.  A call to CreateWindowMenu()
//    performs the actual update.
//
// Parameters:
//    bActivate      -- TRUE if child window being activated, otherwise
//                      FALSE
//    pActivateWnd   -- pointer to MDI child window being activated
//    pDeactivateWnd -- pointer to MDI child window being deactivated
//
// Returns:
//    none
//
// Comments:
//    see the CWnd::OnMDIActivate() docs for further information.
//
//***********************************************************************
void CTableChildFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame OnMDIActivate. %s\n",
                                                        (bActivate)?"Activate":"DEactivate");
    // call the base class to let standard processing switch to
    // the top-level menu associated with this window
    CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

    if (bActivate)// we don't need to do anything on deactivate
    {

        CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
#if 0 // original table operations
        pMainWnd->GetDocument();

        // Create menu to show window views
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame OnMDIActivate.did %s get a document \n",
            (pMainWnd->m_pDoc == NULL)?"NOT":"");

        if ( pMainWnd != NULL &&   pMainWnd->m_pDoc != NULL          &&
            (! pMainWnd->m_pDoc->m_autoUpdateDisabled)   &&   
            pMainWnd->m_pDoc->pCurDev != NULL  && 
            pMainWnd->m_pDoc->pCurDev->pCmdDispatch != NULL && 
            pMainWnd->m_pDoc->pCurDev->pCmdDispatch->isReady()    )
        {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame::OnMDIActivate - calling main window's CreateWindowMenu.\n");
        
        pMainWnd->CreateWindowMenu();
    //      KillTimer(999);

        }
#else // new operations like window
        
        // Build the menu
        CMenu* pTopMenu = pMainWnd->GetMenu();
        CMenu* pViewMenu = NULL;
        CString strMenu;

        for(int pos = 0; pos < ((int)pTopMenu->GetMenuItemCount()); pos++)
        {
            pTopMenu->GetMenuString(pos, strMenu, MF_BYPOSITION);

            if (strMenu == "&View")
            {
                // A match is found!!

                // removes popup menu @ pos, deletes it,replaces it with ViewMwnu
                //                     pos is position & next param is a handle
                pTopMenu->ModifyMenu(pos, MF_BYPOSITION|MF_POPUP, 
                                          (UINT)(pMainWnd->m_ViewMenu.GetSafeHmenu()), strMenu);
                break;
            }
        }
#endif
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"CTableChildFrame::OnMDIActivate - Exit\n");
}


LRESULT CTableChildFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{   
    if ((message >= WM_USER) && (message < WM_APP))// between 0x400 and 0x800 
    {
        switch(message)
        {
        case WM_SDC_VAR_CHANGED:    /* this should no longer be sent - handle like value */
        case WM_SDC_VAR_CHANGEDVALUE:
            {
                TRACE("TableChildFrame: VAR CHANGED\n");
            }
            break;
            
        case WM_SDC_STRUCTURE_CHANGED:
            {
                TRACE("TableChildFrame: STRUCT CHANGED\n");
            }
            break;

        case WM_SDC_WAITCLOSEDLG:
            {
                TRACE("TableChildFrame: WAIT CLOSED\n");
                MessageBox(M_UI_COMM_ERROR);
                break;
            }
        }// endswitch
    }//not our message
    LRESULT rty = CMDIChildWnd::WindowProc(message, wParam, lParam);

    return rty;
}// end WindowProc

/**** no longer used
void CTableChildFrame::OnTimer(UINT nIDEvent) 
{
}
*****/

extern int addUnique(vector <hCitemBase*>& pV, hCitemBase* pIB);

void CTableChildFrame::StaleDynamics(vector <hCitemBase*>& pVariablesToRead) 
{   /*
     * Cycle through the child list on this DDL Menu and update 
     * all the variables
     */
    CDDLBase *pItem;
    //CDDLBase *pBase =  m_pSelection; // pBase points to DDL Menu
    CDDLBase *pBase =  getCurrentSelection(); // pBase points to DDL Menu
    
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (pBase == NULL || pMainWnd == NULL || 
                         pMainWnd->m_pDoc == NULL || pMainWnd->m_pDoc->pCurDev == NULL)
        return;
    
    bool needsStale;        
    if (pMainWnd->m_pDoc->pCurDev->updateCnt <= ENTRIES2AUTOLOAD )
    {
        pMainWnd->m_pDoc->pCurDev->updateCnt++;
    }
    else
    {
        pMainWnd->m_pDoc->pCurDev->setQueryWeight(QUERY_INC);// will start bumping
    }
    
    POSITION pos = pBase->GetChildList().GetHeadPosition();
    for( int nIdx = 0; pos != NULL; nIdx++ )
    {
        pItem = (CDDLBase*)pBase->GetChildList().GetNext( pos );

        if (pItem->IsVariable())
        {
            CDDLMenuItem* pMenuItem = (CDDLMenuItem *)pItem;
            CDDLVariable* pVar = (CDDLVariable *)pMenuItem->m_item;
            if (pVar)
            {
                /* stevev 2/16/04 - separate out the filter from the decision */
                needsStale = pItem->IsValid() &&    // it exists
                        pVar->IsDynamic() &&        // it's dynamic
                        ( pVar->m_pddbVar->getDataState() != IDS_NEEDS_WRITE &&
                         (! pVar->m_pddbVar->isChanged() )/*This is not an unconfirmed write*/
                           /*stevev - remove for now (appears to bypass many of the filter 
                               conditions above)|| (pVar->m_pddbVar->getDataQuality() != 
                               DA_HAVE_DATA)) /  * VMKP added on 290104*/
                        ); 
                 /* don't try and read write-only variables*/
                needsStale = needsStale && (! pVar->m_pddbVar->IsWriteOnly());

                if ( needsStale )  
                /* stevev - end 2/16/04 change */
                {
                    addUnique(pVariablesToRead, (hCitemBase *)pVar->m_pddbVar  );
                }   
                /* add a query bump to unitialized visible values...stevev 03jan13 */
                if ( pVar->m_pddbVar->getDataState() == IDS_UNINITIALIZED || // was  )
					/* in order to get the variables that have a DEFAULT_VALUE updated when visible*/
					 pVar->m_pddbVar->getDataState() == IDS_STALE )
                {
                    pMainWnd->m_pDoc->pCurDev->setQueryWeight(VISIBLE_MUL * QUERY_INC);
                    pVar->m_pddbVar->bumpQueryCnt();
                    pMainWnd->m_pDoc->pCurDev->setQueryWeight(QUERY_INC);
                }// TODO:  this unitialized bump needs to be put into the window's StaleDynamics

            }
        }
    }//next
}


/* stevev 07apr11 - copied from mainframe to deal with table updates
   stevev 09dec04 - changed algorithm
   we now get a tick each GRANULARITY mS
   The update functions ( everything we've been doing )
   are performed at some multiple of this tick
   the publish functions are performed at some multiple of this tick as well
 */
void CTableChildFrame::OnTimer(UINT nIDEvent) 
{
    if ( ! running )
        return;// skip this if we are closing down
    if ( inTimer )
    {
        TRACE("OnTimer Re-entrant\n");
        return;// skip this if we are closing down
    }
    inTimer = true;
//  MessageBeep(0xFFFFFFFF);   // Beep
    vector <hCitemBase*> pVariablesToRead;
    
    if ( --ticksTillUpdate <= 0 )
    {
        ticksTillUpdate = UPDATEPERIOD/GRANULARITY;
        // fall through to execute
    }
    else
    {
        inTimer = false;
        return; // it's not an update cycle yet
    }

    
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (pMainWnd == NULL || pMainWnd->m_pDoc == NULL || pMainWnd->m_pDoc->pCurDev == NULL)
    {
        inTimer = true;
        return;
    }

    if (    pMainWnd->m_pDoc->appCommEnabled() &&//(! pMainWnd->m_pDoc->m_autoUpdateDisabled)&&
            pMainWnd->m_pDoc->pCurDev != NULL  && 
            pMainWnd->m_pDoc->pCurDev->pCmdDispatch != NULL && 
            pMainWnd->m_pDoc->pCurDev->pCmdDispatch->isReady()    )
    {
        StaleDynamics(pVariablesToRead);
        
        if (pVariablesToRead.size())
        {
#ifdef _DEBUG
            LOGIT(CLOG_LOG,"Marking %d table variables STALE",pVariablesToRead.size());
#endif
            pMainWnd->m_pDoc->ReadVariables(pVariablesToRead);// will handle an empty list
        }

    }
    inTimer = false;

}// end OnTimer