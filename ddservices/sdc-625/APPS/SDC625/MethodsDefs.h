/*************************************************************************************************
 *
 * $Workfile: MethodsDefs.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This file gives the constants used in methods user interface
 */

//prashant

#include "MethodInterfaceDefs.h"

/*******************************************************************************************************
This file gives the constants used in methods user interface
*******************************************************************************************************/
#ifndef _METHODDEFS_H
#define _METHODDEFS_H

//the following are used to determine the size and position fo teh controls created 
//on the fly , wrt to the user client area.
//offsets for the  rich edit text display control w.r.t the user client area
#define RICHEDIT_CTRL_TOP_OFFSET                        2
#define RICHEDIT_CTRL_LEFT_OFFSET                       20
#define RICHEDIT_CTRL_RIGHT_OFFSET                      2
#define RICHEDIT_CTRL_BOTTOM_OFFSET                     20
#define RICHEDIT_CTRL_OVERFLOW_TEXTONLY_BOTTOM_OFFSET   2
#define RICHEDIT_CTRL_OVERFLOW_BOTTOM_OFFSET            60
#define RICHEDIT_CTRL_TOP_OFFSET_FOR_NOOVERFLOW         20


//offsets of the user client area w.r.t the total client area
#define USER_CLIENT_TOP_OFFSET       10  //5 Vibhor 240105: Restored
#define USER_CLIENT_LEFT_OFFSET      15
#define USER_CLIENT_RIGHT_OFFSET     20
#define USER_CLIENT_BOTTOM_OFFSET    100

//macro for deletion of pointers
#define PTR_DELETE(expr) if(expr){delete expr; expr = NULL;}
#define ARRAY_DELETE(expr) if(expr){delete[] expr; expr = NULL;}


//styles used in rich edit control creation
#define DISPLAY_RICHEDIT_STYLE_OVERFLOW         WS_CHILD| WS_VISIBLE|WS_VSCROLL|ES_MULTILINE|ES_READONLY
#define DISPLAY_RICHEDIT_STYLE                  WS_CHILD| WS_VISIBLE|ES_MULTILINE|ES_READONLY


//size of the edit and combo controls
#define EDITBOX_LENGTH            175
#define COMBOBOX_OFFSET_LENGTH    50
#define DATETIME_LENGTH           150
#define EDITBOX_WIDTH             30
#define COMBOBOX_WIDTH            20
#define DATETIME_WIDTH            30

#define EDITBOX_WIDTH_FOR_NO_OVERFLOW             20
#define DATETIME_WIDTH_FOR_NO_OVERFLOW            25

#define OFFSET_FROM_DISPLAY_RICHEDIT_CTRL            10


static const TCHAR szTRIM_CHARS[] = _T(" \n\t");
//static const TCHAR szHARTDATE[] = _T("MM/dd/yy");	Commented by ANOOP 05MAY2004
static const TCHAR szHARTDATE[] = _T("MM/dd/yyyy");	//Commented by ANOOP 05MAY2004
static const TCHAR szHARTDATEFORMAT[] = _T("%02d/%02d/%04d");


#endif //_METHODDEFS_H
