#if !defined(AFX_SDCVIEW_H__F487F79D_8476_4D11_A687_E4344201679E__INCLUDED_)
#define AFX_SDCVIEW_H__F487F79D_8476_4D11_A687_E4344201679E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SdcView.h : header file
//
#include "StdAfx.h"
#include "logging.h"

class CMainListCtrl;
////////////////////////////////////////////////////////////////////////////
// CSdcView view
class CSdcView : public CView
{
protected:
	CSdcView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSdcView)

	CMainListCtrl* m_pCtrl;
// Attributes
public:
	CFrameWnd* m_pTableFrame;// holds the ptr for the listview

// Operations
public:
	CMainListCtrl* GetListCtrl(){return m_pCtrl;};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSdcView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~1");} return CView::WindowProc(message,wParam,lParam);};
#endif

// Implementation
protected:
	virtual ~CSdcView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CSdcView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDCVIEW_H__F487F79D_8476_4D11_A687_E4344201679E__INCLUDED_)
