/**********************************************************************************************
 *
 * $Workfile: DDIdeDoc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *      DDIdeDoc.cpp : implementation of the CDDIdeDoc class
 */

#include "stdafx.h"
#include "SDC625.h"

#include "SDC625Doc.h"
#include "MethodDlg.h"
#include "SDC625_HiddenView.h"
#include "ErrLogView.h"
#include "args.h" // for file store env def
#include "messageUI.h"

#include "MethodSupport.h"

#include "HartTrans.h"  // stevev 16apr10 - part of getting rid of ddldevice.h

#include <time.h>

class CAnalysMethodSupport : public hCMethSupport
{
public:
    bool bEnableDynamicDisplay;  //Added 17FEB2004 for notify of dynamic variables in a method

public:
    CAnalysMethodSupport():bEnableDynamicDisplay(false){};// nothing else

    void PreExecute() {
        return;};

    RETURNCODE DoMethodSupportExecute(hCmethodCall& MethCall) {
        return FAILURE; };

    void PostExecute(){
        return;};
    
    bool hasClients(void){// true when method(s) running
        return false; };;

    bool MethodDisplay( ACTION_UI_DATA structMethodsUIData,
                        ACTION_USER_INPUT_DATA  & structUserInputData) 
    {
        return false; // abort button has been pressed
    };

    int  numberMethodsRunning() {
        return 0; }; 
    
    bool UIimageSize (int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline) {
        return true;}; // true on error

    bool UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, 
                                                        int heightLimit, bool isDialog=false){
        return true;}; // true on error;
};
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef STARTSEQ_LOG
#define STARTSEQ_LOG
#endif

extern TCHAR g_pchClassTabWndName[1000];// in treeview
char messageBuff[1000];// for generic conversion to narrow

/***************************************************************************
 * Global Variables
 ***************************************************************************/
//CddbDevice* CDDIdeDoc::pThisDevice = NULL;
//CDDLDevice* CDDIdeDoc::pThisDevice = NULL;
extern unsigned long snIDVal;

// TEMP make this a global
BOOL m_doingIdle;
/// TEMP//////////////////

////////////// memory use info collection /////////////////
#ifdef _DEBUG
//#define CheckDeviceMemory 1
#endif

#ifdef CheckDeviceMemory
MEMORYSTATUS Memstatus[3];
void showMemory(MEMORYSTATUS& preA, MEMORYSTATUS& post);// at end of this file
#endif

/////////////////////////////////////////////////////////////////////////////
// CDDIdeDoc

IMPLEMENT_DYNCREATE(CDDIdeDoc, CDocument)

BEGIN_MESSAGE_MAP(CDDIdeDoc, CDocument)
    //{{AFX_MSG_MAP(CDDIdeDoc)
        // NOTE - the ClassWizard will add and remove mapping macros here.
        //    DO NOT EDIT what you see in these blocks of generated code!
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDDIdeDoc construction/destruction

CDDIdeDoc::CDDIdeDoc()
{
//  EnableAutomation();

//  AfxOleLockApp();

//    EnableConnections();

    TRACE(_T(">> CDDIdeDoc instantiation...\n"));
    /* Removed CDDLMain pointer - It is no longer used as tree control root menu, POB - 5/18/2014 */
    /* Removed root menu pointer. It is moved to CTableChildFrame, POB - 5/18/2014 */
    m_pMenuSource = NULL;
    pCommInfc     = NULL;
    m_pStatusBar  = NULL;// stevev 30mar05
    InitializeCriticalSection( &m_csQue );
#ifndef IS_OLE
//  m_pComHARTSvr = NULL;
#endif
    pDevMgr       = NULL;
    pCurDev       = NULL;
    m_copyOfCmdLine.clear();
    /* stevev - no longer a static */
    iNumberOfDynamicVars = 0;
    m_bSocketConnected = FALSE;

    m_pSocket = new CClient(this);
    if (m_pSocket)
    {
        m_pSocket->Createnew();
        if (m_pSocket->m_bConnected){m_bSocketConnected= true;}else{m_bSocketConnected= false;}
    }

    m_autoUpdateDisabled = false;
    m_enteredFileNew     = false;
    m_doingIdle          = false;

    //prashant oct 2003
    m_pMethSupport = new  CMethodSupport(this);//01nov06-let support know doc
  m_bVariableEditedByUser = FALSE;
  /* stevev 09nov04 */
  m_pFileSup = NULL;
  /* end stevev */

/*<START>Added by ANOOP */
    for(int nCnt=0;nCnt <1000;nCnt++) 
    {
        DynamicVariableItemIds[nCnt]=0;
    }   
/*<END>Added by ANOOP   */
    ptrCMD48Dlg=NULL;   //ANOOP 31JAN04 

    /*Vibhor 090204: Start of Code*/

     m_bIgnoreRespCode = FALSE;
    
     m_uSuppressionCnt = DEFAULT_SUPPRESS_CNT;

    /*Vibhor 090204: End of Code*/

     /* stevev 4/6/4 */
     retryTime = DFLT_REPOLLTIME;
     retryCount= DFLT_POLLRETRIES;
     /* end stevev */
}



CDDIdeDoc::~CDDIdeDoc()
{
//  AfxOleUnlockApp();

TRACE(_T("<<< Document destructor.\n"));

/*<START>Added by ANOOP */
    //prashant dec03
    for(int nCnt=0;nCnt <this->iNumberOfDynamicVars ;nCnt++) 
    {
        DynamicVariableItemIds[nCnt]=0;
    }
/*<END>Added by ANOOP   */


TRACE(_T("<<< Document destructing Comm Interface.\n"));
    if( pCommInfc )
    {
    //VMKP 260304
//#ifdef DEMODEM
        if (m_copyOfCmdLine.cl_Modem.Compare(_T("HONHARTDE")) == 0)
            delete((HARTDECommInfc *)pCommInfc);
        else
//#endif
            delete((CdocCommInfc *)pCommInfc);
    //VMKP 260304
        pCommInfc = NULL;
    }


    //prashant oct 2003
    if( m_pMethSupport )
    {
        delete( m_pMethSupport );
                m_pMethSupport= NULL;
    }
        //prashant end
    RAZE(m_pFileSup);


TRACE(_T("<<< Document destructing Main Window's Main Menu.\n"));
    /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */

TRACE(_T("<<< Document destructing Device Manager.\n"));
    if (pDevMgr)
    {/* VMKP added on 260304*/
        hCdeviceSrvc* pSvc = pDevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE);
        if (pSvc->getIDbase() != ((__int64)0L)  )// we're not the error device
            pDevMgr->deleteDevice(&(pSvc));     
     /* End of VMKP added on 260304*/
if(pCurDev)
TRACE(_T("<<< Document destructing Device Manager with a device pointer.\n"));
        pDevMgr->destantiate();// protected destruction
        pDevMgr = NULL;
    }

TRACE(_T("<<< Document finished Device Manager.\n"));

    DeleteCriticalSection( &m_csQue );
#ifdef IS_OLE
//  clean up the COMM stuff's memory
#else
//  CloseHandle(m_pComHARTSvr); 
//  m_pComHARTSvr = 0L;
#endif
    if (m_pSocket)
        delete m_pSocket;
TRACE(_T("<<< Document killing COM container.\n"));
    killContainer();
}

/* ON_FILE_SAVE (and ID_ON_FILE_AS ) handler*/
BOOL CDDIdeDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace /* = TRUE */)
{
    TRACE(_T("      * Received an On File Save 2 DoSave Message.\n"));
    AfxMessageBox(M_UI_NO_SAVE);
    return FALSE;
}

int closecnt = 0;
bool CDDIdeDoc::ready2close()
{
    bool rVal = true;
    /* stevev 11jul07 -
       case 1: This thread is pending on an identity request further down in the stack.
        we have to let that time out, so don't close and return to pend.
    */
    if ( pCommInfc != NULL )
    {                                           // 254:1 that it'll match if other modem driver
        if ( ((CdocCommInfc*)pCommInfc)->m_pending == true && closecnt < 4)
        {
            LOGIT(USRACK_LOG,"Cannot close while identifying.\nTry again when it is done.");
            rVal = false;
            closecnt++;
            LOGIT(UI_LOG,"Close attempted while identifying.(%d)\n",closecnt);
        }
    }

    return rVal;
}

void CDDIdeDoc::OnCloseDocument() 
{
    captureStartTime();

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument Entry. Disable AutoUpdate.");
                            logTime();// isa logif start_stop (has newline)
    disableAppCommRequests();

/*Vibhor 020304: Start of Code*/    
    //Just give reasonable time for actions to finish
        //Sleep(500);
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument Allow others to finish.\n");
        systemSleep(500);
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument Sleep till NOT doing idle.\n");
    while ( m_doingIdle )
        systemSleep(100);

/*Vibhor 020304: End of Code*/
/*
    This method is called by the framework in the default message loop when the message queue 
    in the application is empty. Override this method to perform idle-time processing. 
    Use your override to call your own background idle-handler tasks.

virtual BOOL OnIdle( LONG lCount ); 
Parameters
lCount 
Specifies a counter incremented each time OnIdle is called when the message queue in the 
application is empty. This count is reset to zero each time a new message is processed. 
You can use the lCount parameter to determine the relative length of time the application has 
been idle without processing a message. 
Return Value
Nonzero to receive more idle processing time; zero if no more idle time is needed.
*/




/* steve - try this (based from mfc source code for this function) */
    CSDC625HiddenView *pHidView = NULL;
    CDDIdeTreeView    *pTblView = NULL;
    CDDIdeListView    *pLstView = NULL;
    CErrLogView       *pErrView = NULL;

    // destroy all frames viewing this document
  LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument Destroy all view's parent frames.");
    logTime();// isa logif start_stop (has newline)
    // the last destroy may destroy us
    BOOL bAutoDelete = m_bAutoDelete;
    m_bAutoDelete = FALSE;  // don't destroy document while closing views
    while (!m_viewList.IsEmpty())
    {   // get frame attached to the view
        CView* pView = (CView*)m_viewList.GetHead();
        ASSERT_VALID(pView);
        if ( pView->IsKindOf( RUNTIME_CLASS( CSDC625HiddenView )) && pHidView == NULL)
        {
            pHidView = (CSDC625HiddenView*)m_viewList.RemoveHead();
            continue;
        }
        if ( pView->IsKindOf( RUNTIME_CLASS( CDDIdeTreeView ))   && pTblView == NULL)
        {
            pTblView = (CDDIdeTreeView*)m_viewList.RemoveHead();
            continue;
        }
        if ( pView->IsKindOf( RUNTIME_CLASS( CDDIdeListView )) && pLstView == NULL)
        {
            pLstView = (CDDIdeListView*)m_viewList.RemoveHead();
            continue;
        }
        if ( pView->IsKindOf( RUNTIME_CLASS( CErrLogView ))    && pErrView == NULL)
        {
            pErrView = (CErrLogView*)m_viewList.RemoveHead();
            continue;
        }// save these for Close document
        CFrameWnd* pFrame = pView->GetParentFrame();
        ASSERT_VALID(pFrame);

        // and close it
        PreCloseFrame(pFrame);
        pFrame->DestroyWindow();
            // will destroy the view as well
    }
    m_bAutoDelete = bAutoDelete;
    if (pHidView != NULL)
    {
        m_viewList.AddTail(pHidView);
    }
    if (pTblView != NULL)
    {
        m_viewList.AddTail(pTblView);
    }
    if (pLstView != NULL)
    {
        m_viewList.AddTail(pLstView);
    }
    if (pErrView != NULL)
    {
        m_viewList.AddTail(pErrView);
    }

/* --- */

/* stevev 05aug05 */
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.OnCloseDocument calling Doc's CloseDevice. ");
                            logTime();// isa logif start_stop (has newline)
    CloseDevice(0,0);
/* stevev 05aug05 replaced by above...
    if (m_pTreeView)
    {
#ifdef STARTSEQ_LOG
TRACE(_T("<<< CDDIdeDoc::OnCloseDocument Closing Device.\n"));
clog<<"Doc's OnClose Document sending CLOSEDEVICE"<<endl;
#endif
        // Added by POB - 15 Sep 2004
        m_pTreeView->SendMessage(WM_SDC_CLOSEDEVICE, 0, 0);
        TRACE(_T("[OnCloseDoc sent Close Device]\n"));
    }
    systemSleep(100); // let the stuff get done
end 05aug05 ***********/

/* stevev 09/19/03 - done in the treeview where it was generated     
    if ( pDevMgr != NULL && pCurDev != NULL )
    {
        pDevMgr->deleteDevice((hCdeviceSrvc **)&pCurDev);
    }

    if (pCommInfc != NULL )
    {
        delete pCommInfc;
        pCommInfc = NULL;
    }
*/
LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<<doc.OnCloseDocument Post CloseDevice,Calling base class.");
    logTime();// isa logif start_stop (has newline)
//  AfxMessageBox("CDDIdeDoc::OnCloseDocument");
    CDocument::OnCloseDocument();// close the last two frames & delete doc
    // mainframes tries to use us later and crashes
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument nulling mainframe's doc ptr.");
                            logTime();// isa logif start_stop (has newline)
    CMainFrame *pMF = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();
    if (pMF)
        pMF->m_pDoc = NULL;
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<<< doc.OnCloseDocument Exiting.\n");

}
    
BOOL CDDIdeDoc::reGenerateComm(void)
{
    TRACE(_T(">> CDDIdeDoc regenerateComm...\n"));
    m_copyOfCmdLine = ((CDDIdeApp*)AfxGetApp())->getCmdLineInfo();

    if (pCommInfc != NULL )
    {
        // added by stevev 2sep05
        if (m_copyOfCmdLine.cl_IsSim)
        {
            delete((CdocSimmInfc *)pCommInfc);
        }
        else
        if (m_copyOfCmdLine.cl_Modem.Compare(_T("HONHARTDE")) == 0)
        {
            delete((HARTDECommInfc *)pCommInfc);
        }
        else
        {
            delete((CdocCommInfc *)pCommInfc);
        }// end added 2sep05
        pCommInfc = NULL;
    }

    if (m_copyOfCmdLine.cl_IsSim)
    {// instantiate a simulation comm interface
        pCommInfc = new CdocSimmInfc(this);
    }

    // timj 3/22/2004 begin
    /*
     * add specific modem support here
     */

#ifdef DEMODEM
    // instantiate a HARTModemServer comm interface
    else if (m_copyOfCmdLine.cl_Modem.Compare(_T("HONHARTDE")) == 0)
    {
        pCommInfc = new HARTDECommInfc(this); // * VMKP added *  /
    }
#endif

    /*
     * end of specific modem support here
     */

    else 
    {// URL used by HART Server or Modem Server
        pCommInfc = new CdocCommInfc(this);
    }

    // timj 3/22/2004 end

    return ( pCommInfc != NULL );
}// end of CDDIdeDoc::OnCloseDocument()

BOOL CDDIdeDoc::OnNewDocument()
{
    if (!CDocument::OnNewDocument())
        return FALSE;
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::OnNewDocument - Entry, GetDeviceComm\n");

    //reGenerateComm();
    CbaseComm* pBCb = GetDeviceComm();

    // POB, 11 Aug 2004
    CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();

    LOGIF(LOGP_START_STOP)(CLOG_LOG,
        "> doc::OnNewDocument - Generating Device w/ WM_SDC_GENERATEDEVICE to main wnd.\n");
    
    pMainWnd->PostMessage(WM_SDC_GENERATEDEVICE, 0, 0); // POB, 11 Aug 2004

    // the tree doesn't exist yet - this doesn't do anything

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::OnNewDocument - Exiting\n");
    return ( pCommInfc != NULL );
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeDoc serialization

void CDDIdeDoc::Serialize(CArchive& ar)
{
    if (ar.IsStoring())
    {
        // TODO: add storing code here
    }
    else
    {
        // TODO: add loading code here
    }
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeDoc diagnostics

#ifdef _DEBUG
void CDDIdeDoc::AssertValid() const
{
    CDocument::AssertValid();
}

void CDDIdeDoc::Dump(CDumpContext& dc) const
{
    CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDDIdeDoc commands

// aquire initial communication 
CbaseComm* CDDIdeDoc::GetDeviceComm()
{
    //if ( pCommInfc == NULL ) 
    {   
        reGenerateComm();
    }

    if ( pCommInfc == NULL )
    {
        CString error;
        // inform the user
        AfxMessageBox(M_UI_NO_COMMS);
        TRACE(_T("No Comm Interface when asked.\n"));
    }

    return (pCommInfc);
}

long CDDIdeDoc::AsyncResponse(long lHandle, const VARIANT FAR& varReply) 
{
//    TRACE(_T("CDDIdeDoc::AsyncResponse\n"));
    int Cmd;
    BYTE BC; 
    BYTE Status[2]; 
    BYTE Data[MAX];
    WORD cmdError = 0;
    UINT cmdStatus = 0;

    SAFEARRAY FAR * ptrReplySafeArray;

    ptrReplySafeArray = varReply.parray; // Get Reply SAFEARRAY

    BYTE  * ReplyArray = NULL; // Actual array pointer where Reply data is acquired
    SafeArrayAccessData( ptrReplySafeArray,(VOID **) &ReplyArray ); 

    CString maxStr;

    UINT resMax = ptrReplySafeArray -> rgsabound -> cElements;
    
    // ReplyArray[0] - command number
    // ReplyArray[1] - byte count
    // ReplyArray[2] - response code
    // ReplyArray[3] - Device Status
    // ReplyArray[4] - 1st byte of Data

    Cmd = ReplyArray[0];
    BC  = ReplyArray[1];

    Status[RESP_CODE] = ReplyArray[2]; //BYTE responseCode = ReplyArray[2];
    Status[DEVICE_STATUS] = ReplyArray[3]; //BYTE deviceStat = ReplyArray[3];

    const int Datamax = resMax - 4; // Data starts 4 places in the array

    for (int i = 0; i < Datamax; i++)
        Data[i] = ReplyArray[i + 4];

    // Check for Cmd error -  Zero data bytes & response code of non-zero
    if ( (Datamax==0) && Status[RESP_CODE] !=0)
    {
        cmdError = APP_ER_CMD_FAILED;

        // If the device did not respond, response code = 255
        if ( (Datamax==0) && Status[RESP_CODE] == 255)
            cmdError = APP_ER_NORESPONSE;
    }

    /* To Do:  Need to deal with communication Failure*/

    cmdStatus = (cmdError << 16 | Status[RESP_CODE] << 8 | Status[DEVICE_STATUS]);

    DeQueueTrans(Cmd, Data, Datamax, cmdStatus, lHandle);

    return 0;
}

RETURNCODE CDDIdeDoc::AttachToSegment()
{
#ifdef IS_OLE
    // Make connection
//    HRESULT hr;
#endif // unused var
    CString error;
    RETURNCODE status = ST_OK;
#ifdef NOT_USED_FOR_HART_MODEM_SVR___IS_OLE
    // Connect to the Pass Through mechanism of the OPC Server (only once)
    if (m_pComHARTSvr == NULL)
    {

        try
        {
            m_pComHARTSvr.CreateInstance(__uuidof(HartServerLib::HartComm));
        }
        catch (_com_error e)
        {
    
            error.Format(_T("%s"), e.ErrorMessage());
            AfxMessageBox(error);
            TRACE(_T("Cannot create server COM Object, Error #: %x")), e.Error());
            status = APP_ER_OLE_NOT_FOUND;
        }

        try
        { 
            // Make connection
            hr = AdviseSink(m_pComHARTSvr, GetControllingUnknown(), IID__IHartCommEvents,
                    &m_dwCookie);

            if (FAILED(hr))
            {
                AfxMessageBox(L"Advise Sink Failed");
                status = APP_ER_OLE_FAILED;
            }
        
        }
        catch (_com_error e)
        {
            error.Format(_T("%s"), e.ErrorMessage());
            AfxMessageBox(error);
            TRACE(_T("Connection Point Failed, Error #: %x")), e.Error());
            status = APP_ER_OLE_NOT_FOUND;
        }
/////////////////////////////////////////////////////////////////////////////#else  // is pipe
                // use named pipe
    LPTSTR lpszPipename = PIPENAME;
    
    HANDLE Hand;
    BOOL   isSuccess;

    while (1) 
    { 
        status    = ST_OK;
        isSuccess = TRUE;
        Hand = CreateFile( 
            lpszPipename,   // pipe name 
            GENERIC_READ |  // read and write access 
            GENERIC_WRITE, 
            0,              // no sharing 
            NULL,           // no security attributes
            OPEN_EXISTING,  // opens existing pipe 
            0,              // default attributes 
            NULL);          // no template file 

        // Break if the pipe handle is valid. 

        if (Hand != INVALID_HANDLE_VALUE) 
            break; // success

        // Exit if an error other than ERROR_PIPE_BUSY occurs. 

        if (GetLastError() != ERROR_PIPE_BUSY)          
        {// MyErrExit("Could not open pipe"); 
          LOGIT(CERR_LOG, "ERROR: Could not open pipe.\n");
          status = APP_ER_FAILED;
          break; // out of while
        }
        else // All pipe instances are busy, so wait for 20 seconds
        if (! WaitNamedPipe(lpszPipename, 20000) ) 
        {// MyErrExit("Could not open pipe"); 
          LOGIT(CERR_LOG, "ERROR: Wait pipe didn't connect.\n");
          status = APP_ER_FAILED;
          break; // out of while         
        }
        // else loop and try again
    } //wend
    if ( ! status )
    {
        m_pComHARTSvr = Hand; 
    }
    else
    {
        m_pComHARTSvr = 0L;
    } 


    }// else not NULL so it's ready to go

#endif //IS_OLE
   return status;
}

HRESULT CDDIdeDoc::AdviseSink(IUnknown * pUnkCP, IUnknown * pUnk, const IID & iid, LPDWORD pdw)
{
    HRESULT hRes;
    IConnectionPointContainer *pCPC;
    IConnectionPoint *pCP = NULL;

    // pUnkCP is the server
    // pUnk is the client

    hRes = pUnkCP->QueryInterface(IID_IConnectionPointContainer, (void**)&pCPC);
    if (SUCCEEDED(hRes))
        hRes = pCPC->FindConnectionPoint(iid, &pCP);
    if (SUCCEEDED(hRes))
        hRes = pCP->Advise(pUnk, pdw);
    return hRes;

}

RETURNCODE CDDIdeDoc::Send(int Cmd, BYTE* pData, BYTE& ByteCount, int timeout,
                                                                  UINT& cmdStatus, long handle)
{
    CString error;
    RETURNCODE result = ST_OK;
    BYTE Status[2];
    WORD cmdError = 0;

    // Intialize the data structures for the send buffer and 
    // receive buffer 

    VARIANT Request; // Buffer holding the Master message - includes
                // command, byte count and request data
    
    VARIANT Reply;  // Buffer holding the Reply from device - includes
                    // command # and ends with (including the checksum)

    VARIANT* ptrReply;
    ptrReply = &Reply; // set up pointer to Reply VARIANT


    // VARIANT will be sending a SAFEARRAY as a data type in VARIANT -
    // Need to configure size, dimension, and data type
    SAFEARRAY FAR * ptrRequestSafeArray;
    SAFEARRAYBOUND rgsabound[1];
    rgsabound[0].lLbound = 0;
    rgsabound[0].cElements = MAX;

    // 1st parameter - data type in array (VT_UI1 = unsigned char (BYTE) )
    // 2nd parameter - number of dimensions in array
    // 3rd parmater - SAFEARRAYBOUND data to allocate the arrray
    ptrRequestSafeArray = SafeArrayCreate(VT_UI1,1,rgsabound); // Create Safe array


    // Set up the VARRIANT to accept a SAFEARRAY (VT_ARRAY)
    // with data types of BYTES (VT_UI1)
    Request.vt = VT_ARRAY|VT_UI1;
    
    Request.parray = ptrRequestSafeArray; //point VARIANT to SAFEARRAY

    BYTE * RequestArray = NULL;

    SafeArrayAccessData( ptrRequestSafeArray,(VOID **) &RequestArray ); 

    // Configure actual request array
    RequestArray[0] = Cmd; // Command number

    RequestArray[1] = ByteCount;

    //  Data starts at RequestArray[2]
    UINT offset = 2;

    for (UINT j = 0; j < ByteCount; j++)
        RequestArray[j+2] = pData[j];

    // Protect the data
    SafeArrayUnaccessData(ptrRequestSafeArray); 
 

#ifdef NOT_USED_FOR_HART_MODEM_SVR___IS_OLE
    // Do not use the Send method of the Pass Through mechanism
    // if the COM instance is unavailable
    if (m_pComHARTSvr == NULL)
    {
        //Need to connect to instrument!
        result = APP_ER_OLE_NOT_FOUND; //Error, COM object not instantiated
    }

    // Using the handle that was obtain in the ConnectByTag() function,
    // send the HART command using the syncrous Pass Through mechanism
    if (!result)
    {
        try
        {
            // send the command
            HRESULT hr = m_pComHARTSvr->Send(handle, Request, ptrReply);

            if (FAILED(hr))
            {
                result = APP_ER_OLE_FAILED;
            }
        }

        catch (_com_error e)
        {
            error.Format(_T("%s"), e.ErrorMessage());
            AfxMessageBox(error);
            TRACE(_T("Cannot create server COM Object, Error #: %x")), e.Error());
            result = APP_ER_OLE_NOT_FOUND;
        }
#endif //IS_OLE

        SAFEARRAY FAR * ptrReplySafeArray;

        ptrReplySafeArray = ptrReply->parray; // Get Reply SAFEARRAY

        BYTE  * ReplyArray = NULL; // Actual array pointer where Reply data is acquired
        SafeArrayAccessData( ptrReplySafeArray,(VOID **) &ReplyArray ); 

        CString maxStr;

        UINT resMax = ptrReplySafeArray -> rgsabound -> cElements;
        
        // ReplyArray[0] - command number
        // ReplyArray[1] - byte count
        // ReplyArray[2] - response code
        // ReplyArray[3] - Device Status
        // ReplyArray[4] - 1st byte of Data

        ByteCount = ReplyArray[1];

        Status[RESP_CODE] = ReplyArray[2]; //BYTE responseCode = ReplyArray[2];
        Status[DEVICE_STATUS] = ReplyArray[3]; //BYTE deviceStat = ReplyArray[3];

        const int Datamax = resMax - 4;

        for (int i = 0; i < Datamax; i++)
            pData[i] = ReplyArray[i + 4];
       
        // Check for Cmd error -  Zero data bytes & response code of non-zero
        if ( (Datamax==0) && Status[RESP_CODE] !=0)
        {
            cmdError = APP_ER_CMD_FAILED;

            // If the device did not respond, response code = 255
            if ( (Datamax==0) && Status[RESP_CODE] == 255)
                cmdError = APP_ER_NORESPONSE;
        }

        /* To Do:  Need to deal with communication Failure*/

        cmdStatus = (cmdError << 16 | Status[RESP_CODE] << 8 | Status[DEVICE_STATUS]);

//    }
    
    return result;
}

RETURNCODE CDDIdeDoc::AsyncSend(int Cmd, BYTE ByteCount, BYTE * pData, long handle)
{
    TRACE(_T("CDDIdeDoc::AsyncSend - Cmd# %d\n"),Cmd);

    CString error;
    RETURNCODE result = ST_OK;

    /* Intialize the data structures for the send buffer and 
    receive buffer */

    VARIANT Request; // Buffer holding the Master message - includes
                // command, byte count and request data
    
    VARIANT Reply;  // Buffer holding the Reply from device - includes
                    // command # and ends with (including the checksum)

    VARIANT* ptrReply;
    ptrReply = &Reply; // set up pointer to Reply VARIANT


    // VARIANT will be sending a SAFEARRAY as a data type in VARIANT -
    // Need to configure size, dimension, and data type
    SAFEARRAY FAR * ptrRequestSafeArray;
    SAFEARRAYBOUND rgsabound[1];
    rgsabound[0].lLbound = 0;
    rgsabound[0].cElements = MAX;

    // 1st parameter - data type in array (VT_UI1 = unsigned char (BYTE) )
    // 2nd parameter - number of dimensions in array
    // 3rd parmater - SAFEARRAYBOUND data to allocate the arrray
    ptrRequestSafeArray = SafeArrayCreate(VT_UI1,1,rgsabound); // Create Safe array


    // Set up the VARRIANT to accept a SAFEARRAY (VT_ARRAY)
    // with data types of BYTES (VT_UI1)
    Request.vt = VT_ARRAY|VT_UI1;
    
    Request.parray = ptrRequestSafeArray; //point VARIANT to SAFEARRAY

    BYTE * RequestArray = NULL;

    SafeArrayAccessData( ptrRequestSafeArray,(VOID **) &RequestArray ); 

    // Configure actual request array
    RequestArray[0] = Cmd; // Command number

    RequestArray[1] = ByteCount;


    //  Data starts at RequestArray[2]
    UINT offset = 2;

    for (UINT j = 0; j < ByteCount; j++)
        RequestArray[j+2] = pData[j];
    
        
    SafeArrayUnaccessData(ptrRequestSafeArray); 

#ifdef NOT_USED_FOR_HART_MODEM_SVR___IS_OLE
 
    if (m_pComHARTSvr == NULL)
    {
        //Need to connect to instrument!
        result = APP_ER_OLE_NOT_FOUND; //Error, COM object not instantiated
    }

    if (!result)
    {

        try
        { 
            // send the command
            HRESULT hr = m_pComHARTSvr->AsyncSend(handle, Request);

            if (FAILED(hr))
            {
                result = APP_ER_OLE_FAILED;
            }
        
        }
        catch (_com_error e)
        {
            error.Format(_T("%s"), e.ErrorMessage());
            AfxMessageBox(error);
            TRACE(_T("Cannot create server COM Object, Error #: %x")), e.Error());
            result = APP_ER_OLE_NOT_FOUND;
        }

    }
#endif //IS_OLE
    return result;
}
#if 0
// commented out code removed 02apr07 - stevev - see ear;ier version for contents
#endif


/* called by the OnIdle() app function--
 *      You are NOT allowed to block in this function or any function it calls!!!
 *      No Sleeps, no WaitForSingle/MultipleObjects
 ************************************************************************************/
BOOL CDDIdeDoc::DoIdle(LONG lCount)
{
    static int seqLoc = 0;
    BOOL more2do = FALSE;
    m_doingIdle = true;
    if (m_pStatusBar)
    {
        wchar_t statStr[1024];
        GetStatusString(statStr);

        CStatusBarCtrl & barCtrl = m_pStatusBar->GetStatusBarCtrl();
        //barCtrl.SetText("SDC625", 0, 0) ;
//#ifdef _UNICODE
//      TCHAR StatStr[1024];
//      AsciiToUnicode(statStr,   StatStr, 1024);
//      barCtrl.SetText(StatStr, 0, 0) ;
//#else
        barCtrl.SetText(statStr, 0, 0) ;
//#endif
    }

    // add counter to reduce the incidence if needed
    if (++seqLoc > 10)// every tenth entry
    {
        seqLoc = 0;
    }
    // do svc routines // as small a bite as possible
    //                  will be back shortly to continue
if ( ! appCommEnabled() ) //m_autoUpdateDisabled)
{
DEBUGLOG(CLOG_LOG,"Doc's DoIdle with Update disabled\n");
}
    // we don't want to start till all are initialized...too early would hurt.
    if ( appCommEnabled()   &&   pCurDev != NULL && 
        pCurDev->pCmdDispatch != NULL && pCurDev->pCmdDispatch->isReady())
    {
//CVarList* pVL = (CVarList*)pCurDev->getListPtr(iT_Variable);
//clog<<"Doc's DoIdle with " << pVL->size() << " variables."<<endl;
/* stevev 4-7-04 - modify to always try the writes */
/* stevev 4-7-04
        if (seqLoc < 1 || seqLoc > 2 ) // error or haven't started yet
        {
            seqLoc = 1; // do the writes first
        }// fall thru

        if (seqLoc == 1) // do writes
        {
stevev 4-7-04*/
        // stevev - 5/11/04 - added
        if (! seqLoc)
        {           
            sniffFile();// if def this call to disable file sniffing.
        }
        if (pCurDev != NULL && pCurDev->pCmdDispatch != NULL)// can change in the sniffer
            pCurDev->pCmdDispatch->triggerIdleTask();// strobe the idle task
    }
    // else nothing to do with no device - return FALSE
    m_doingIdle = false;
    return more2do; 
}
// HART Transactions que functions

RETURNCODE CDDIdeDoc::QueueTrans(int cmdNum, BYTE * pData, BYTE dataCnt, long lHandle, 
                                                                                  int transNum)
{
    TRACE(_T("CDDIdeDoc::QueueTrans\n"));

    int nStatus = ST_OK;
    Transaction* pTrans = NULL;
    
    EnterCriticalSection( &m_csQue );
    ////////////////////////////////
    /*
     * Add the transaction pointer to the queue.
     * Must use critical section protection.
     */
    if (m_MsgQue.GetCount() < 10)
    {
        pTrans = new Transaction;
    
        pTrans->nCmdNum  = cmdNum;
        pTrans->lHandle  = lHandle;
        pTrans->nTransID = transNum;

        m_MsgQue.AddTail( pTrans );
        
        nStatus = AsyncSend(cmdNum, dataCnt, pData, lHandle);
    }
    else
    {
        nStatus = APP_ER_BUF_FULL;
    }
    ////////////////////////////////
    LeaveCriticalSection( &m_csQue );

    return nStatus;
}

RETURNCODE CDDIdeDoc::DeQueueTrans( int& cmdNum, BYTE* pdata, BYTE dataCnt, UINT& cmdStatus,
                                                                                long lHandle )
{
    TRACE(_T("CDDIdeDoc::DeQueueTrans\n"));

    int transNum = 0;
    POSITION   pos;
    Transaction* pTrans;

    EnterCriticalSection( &m_csQue );
    ////////////////////////////////
    TRACE(_T("Queue count before - %d\n"), m_MsgQue.GetCount());
    if( !m_MsgQue.IsEmpty() )
    {
        pos = m_MsgQue.GetHeadPosition();

        pTrans = m_MsgQue.GetAt( pos );

        if ( (pTrans->nCmdNum == cmdNum) & (pTrans->lHandle == lHandle) )
        {
            transNum = pTrans->nTransID;
        }
        else 
        {
            AfxMessageBox(M_UI_GENERIC);
        }

        m_MsgQue.RemoveAt( pos );
        delete pTrans;
        TRACE(_T("Queue count after- %d\n"), m_MsgQue.GetCount());
    }
//    cmdInfo_t cmdInfo;
//    m_HartTrans.GetReply(transNum, cmdNum, pdata, dataCnt, cmdInfo);//cmdStatus);
//  cmdStatus = cmdInfo.RespCd;
    ////////////////////////////////
    LeaveCriticalSection( &m_csQue );

    return ST_OK;
}

#if 0 /* this has been moved to the treeview!!!*/
// code removed 22aug07 - see code earllier to view the moved code
#endif //0 - moved above to treeview

void CDDIdeDoc::ReadVariables(vector <hCitemBase*>& VariablesToRead)
{
    if (pCurDev != NULL && 
        pCurDev->pCmdDispatch != NULL && 
        pCurDev->pCmdDispatch->isReady())
    {
        
        if ( VariablesToRead.size() )
        {
            CMainFrame * pMainFrm = GETMAINFRAME;
            pMainFrm->ReadVars(); // resets command zero count
        }
        pCurDev->Read(VariablesToRead);
    }
}


CClient* CDDIdeDoc::GetSocket()
{
    if (m_bSocketConnected)
        return m_pSocket;
    else
        return NULL;
}

void CDDIdeDoc::HostCommErrorCallback(long status, string& errStr)
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback Entry\n");

    //stevev 24aug05
    CMainFrame * pMainFrm = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();
    if ( pMainFrm != NULL )
    {       
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback disable AutoUpdate\n");
        disableAppCommRequests();// disable timer and idle task immediatly (early and often)

        /* stevev 28sep06*/
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback Sending WAITCLSDLG\n");
        pMainFrm->SendMessage(WM_SDC_WAITCLOSEDLG, NULL, NULL);
        // end 28sep'06
        //pMainFrm->CloseDevice();
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback "
                                                        "Sending CLOSEDEVICE to mainframe\n");
        pMainFrm->PostMessage(WM_SDC_CLOSEDEVICE, 0, 0);
//stevev 2sep05(remove to see if it delays the close        systemSleep(20);
        /* stevev 12dec05 - logit the error */
        LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"HostComm error: %s\n",errStr.c_str());
    }
    // end stevev 24aug05

    //close the methods dialog
    if(m_pMethSupport != NULL && 
        m_pMethSupport->pCurrentMethodCall != NULL &&
         m_pMethSupport->pCurrentMethodCall->m_pMethodDlg != NULL)
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,
                    "< doc.HostCommErrorCallback -method- posting CLOSE to method Dialog \n");
        m_pMethSupport->pCurrentMethodCall->m_pMethodDlg->SendMessage(WM_CLOSE);
        m_pMethSupport->m_CommErrorOccured = TRUE;
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback - Allow method to stop.\n");
/* stevev 12dec05 temporary -- appears to be a UI box from a non-UI thread
          holding up the entire shutdown process
          AfxMessageBox("Error In Communication with Device, Closing the Device...");
end temporary */
/*Vibhor 020304: Start of Code*/
    //Just give reasonable time for actions to finish
        //Sleep(500);
        systemSleep(500);
/*Vibhor 020304: End of Code*/  
/* 05aug05 replace...
        m_pTreeView->PostMessage(WM_SDC_CLOSEDEVICE, 0, 0);
        TRACE(_T("[HostCommError sent Close Device]\n"));
 * with...*/
// stevev 24aug05:: now called from mainframe...        CloseDevice(0,0);
/* end 05aug05 */
//stevev 24aug05 : not matched  }

/* we can't do this here
    isClosing = true;

    
    if ( pDevMgr != NULL && pCurDev != NULL )
    {
        pDevMgr->deleteDevice((hCdeviceSrvc **)&pCurDev);
    }

    if (pCommInfc != NULL )
    {
        delete pCommInfc;
        pCommInfc = NULL;
    }

    genDev();
*/
//  isClosing = false;
    /*
void  CdocCommInfc::getErrorStr(HRESULT hR, string& retStr)
{// values from winerror.h
    
    if (hR == S_OK) 
    {
        retStr = "HModemSvr: function was successful.";
    }
    else 
    if (hR == E_ACCESSDENIED) 
    {
        retStr = "HModemSvr: Cannot aquire exclusive rights.";
    }
    else 
    if (hR == E_FAIL) 
    {
        retStr = "HModemSvr: Unable to access connection to device.";
    }
    else 
    if (hR == E_INVALIDARG) 
    {
        retStr = "HModemSvr: Argument contains invalid characters.";
    }
    else 
    if (hR == E_OUTOFMEMORY) 
    {
        retStr = "HModemSvr: Unable to allocate memory.";
    }
    else 
    if (hR == E_HANDLE) 
    {
        retStr = "HModemSvr: Invalid handle.";
    }
    else 
    if (hR == E_NOTIMPL) 
    {
        retStr = "HModemSvr: Not implemented.";
    }
    //RPC_E_WRONG_THREAD = _HRESULT_TYPEDEF_(0x8001010EL)
    else 
    {
        char t[80];
        sprintf(t,"0x%x",hR);
        retStr = "HModemSvr: Error code " ;
        retStr += t;
    }
    */
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc::HostCommErrorCallback Exitting.\n");
}

void CDDIdeDoc::sniffFile(void)
{
    PFILE_INFO_T pInfo = pCurDev->getFileInfoPtr();

    WIN32_FIND_DATA FindFileData;
        
    HANDLE hFile = FindFirstFile(pInfo->file_name.c_str(), &FindFileData );
    FindClose(hFile);

    if(hFile  != INVALID_HANDLE_VALUE)
    {
#ifdef TURNED_OFF_DEBUG
        WORD Dt,Tm;
        int  Dy,Mt,Yr,Hr,Mn,Sc;
        // takes a ptr2FILETIME
#define FT2Date(PFT) {FileTimeToDosDateTime((PFT),&Dt,&Tm);Dy=(Dt&0x1f);Mt=((Dt>>5)&0x0f);\
        Yr=(((Dt>>9)&0x7f)+1980);Sc=((Tm&0x1f)*2);Mn=((Tm>>5)&0x3f);Hr=((Tm>>11)&0x1f);}
        
        LOGIT(CLOG_LOG, "File Check:%s\n    ",pInfo->file_name.c_str());
        FT2Date(&(FindFileData.ftLastAccessTime));
        LOGIT(CLOG_LOG, "     Access: %d/%d/%02d   %d:%d:%d\n",Mt,Dy,Yr,Hr,Mn,Sc);
        FT2Date(&(FindFileData.ftCreationTime));
        LOGIT(CLOG_LOG, "    Created: %d/%d/%02d   %d:%d:%d\n",Mt,Dy,Yr,Hr,Mn,Sc);
        FT2Date(&(FindFileData.ftLastWriteTime));
        LOGIT(CLOG_LOG, "    Written: %d/%d/%02d   %d:%d:%d\n",Mt,Dy,Yr,Hr,Mn,Sc);
#endif
        if (pCurDev != NULL)
        {
            ULARGE_INTEGER nowTime, thenTime;
            nowTime.LowPart   = FindFileData.ftLastWriteTime.dwLowDateTime;     
            nowTime.HighPart  = FindFileData.ftLastWriteTime.dwHighDateTime;
            thenTime.LowPart  = pInfo->LoFileTime;      
            thenTime.HighPart = pInfo->HiFileTime;
            int userReturn;

            if ( nowTime.QuadPart > thenTime.QuadPart )
            {
                userReturn =AfxMessageBox(M_UI_SRC_CHANGED, MB_OKCANCEL|MB_ICONQUESTION);
                
                if(userReturn == IDCANCEL) // not  IDOK
                {// if CANCEL - make pInfo hold the nowTime
                    memcpy(pInfo,&(FindFileData.ftLastWriteTime),sizeof(FILETIME));
                }
                else  
                {// if OK - close & reopen 
                    m_enteredFileNew = true; // flag for next entry
                //Just give reasonable time for actions to finish
                //NOTE : In future we may disable File New on existing device!!!
                //       (see App's OnFileNew)
                    //Sleep(500);
                    LOGIF(LOGP_START_STOP)
                                    (CLOG_LOG,"< doc.sniffFile sleep a bit to allow stop.\n");
                    systemSleep(500);
                /* 05aug05
                    m_pTreeView->PostMessage(WM_SDC_CLOSEDEVICE, 0, 0);
                    Sleep(100);
                replaceed with */
                    LOGIF(LOGP_START_STOP)
                                    (CLOG_LOG,"< doc.sniffFile calling Doc's CloseDevice.\n");
                    CloseDevice(0,0);
                /* end 05aug05 */
                    // from timj begin 4/6/04
                    m_copyOfCmdLine.cl_PollAddr = EMPTY_POLL_ADDR;
                    m_copyOfCmdLine.cl_Url = "";
                    // from timj end 4/6/04

                    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.sniffFile Exiting.\n");
                    TRACE(_T("[[Sniffer Closed.]]\n"));
                }
            }
        }
        else
        {
            memcpy(pInfo,&(FindFileData.ftLastWriteTime),sizeof(FILETIME));
        }
    }
}


/* stevev 09nov04 */


CsdcFileSupport* CDDIdeDoc::GetDeviceFile(void)
{
    // instantiates only - we are responsible for deletion
    if ( m_pFileSup ) delete m_pFileSup;
    m_pFileSup = (CsdcFileSupport*) new CsdcFileSupport(m_copyOfCmdLine);
    return m_pFileSup;
}


/* end stevev */

/* stevev 8jan5 - images */
RETURNCODE CDDIdeDoc::AddImage(unsigned idx, string& sLang,unsigned int siz,BYTE* pRaw)
{
    RETURNCODE rc = FAILURE;

    if (sLang.empty() || siz == 0 || pRaw == NULL )
    {
        LOGIT(CERR_LOG,"ERROR: add image with bad parameter.\n");
        return APP_PARAMETER_ERR;
    }// else process it
SDClangImage* pImg;
try {
    // instantiate CxImage with buffer size
    //SDClangImage* pImg = new SDClangImage(pRaw, siz);
    pImg = new SDClangImage(pRaw, siz);
    }
catch(...)
    {
        return -1;
    }
    
    if (pImg == NULL || ! pImg->IsValid() )
    {
        RAZE(pImg);
        LOGIT(CERR_LOG,"ERROR: Image failed to generate\n");
        return APP_CONSTRUCT_ERR;
    }// else success

    pImg->language = sLang;

    DDL_Image* pDDLImg = NULL;

    //   see if its a new image:: if (idx > (size() - 1) then new image into list
    if ( idx < 0 || idx >= ( m_ImageList.size() ) )
    {// not there
        pDDLImg = new DDL_Image;
        if ( idx != m_ImageList.size() )
        {
            LOGIT(CERR_LOG,"ERROR: Image being added out of order. is:%u, should be:%d\n",
                idx, m_ImageList.size() );
            idx = m_ImageList.size();
        }
        m_ImageList.push_back(pDDLImg);
    }
    else    //   get the image
    {
        pDDLImg = m_ImageList[idx];
    }
    //   add this cximage/lang to image's list && return success
    if ( pDDLImg != NULL )
    {
        pDDLImg->push_back(pImg);
        rc = SUCCESS;
    }
    else
    {
        rc = FAILURE;
        delete pImg;
    }
    return rc;
}

SDClangImage* CDDIdeDoc::GetImage(int idx, char* lang)
{   // look up idx & lang and return the pointer
    string lan("en");
	if (pDevMgr)
	{
		lan = pDevMgr->getLangStr();
		if (lan[0] == '|')
		{// assume there is a leading and trailing bar
			lan = lan.substr(1,lan.rfind('|')-1);
		}
	}
    if ( lang == NULL )
    {
        lang = (char*)lan.c_str();
    }
//caller can then draw the image via it's draw routine
    return m_ImageList.GetImage(idx, lang);
}

/* end stevev 8jan5 */

/* stevev 05aug05 - moved from treeview */

void CDDIdeDoc::generateDevice(void)
{
    // Removed unused CDDLBase pointer
    RETURNCODE   rc = SUCCESS;
    CMainFrame * pMainFrm = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::generateDevice Entry\n");
#ifdef STARTTIME_CHECK
    lastTickCnt=startOfInstantiation = GetTickCount();
#endif
    // Display Busy Dialog
    pMainFrm->BusyDisplay();    // POB - 4 Aug 2004
        
    if ( pDevMgr == NULL )
    {   
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::generateDevice  instantiating DvgMgr.\n");
// stevev 10jan08 - we're going to leave these short until we have an international PC
        //          to test the long string path names against
        int Sl = m_copyOfCmdLine.cl_DbP.GetLength()+3;
        int Ll = m_copyOfCmdLine.cl_Lang.GetLength()+3;
        int Al = m_copyOfCmdLine.cl_AppDir.GetLength()+3;
        char* locDir = new char[Sl];
        char* locLan = new char[Ll];
        char* locApp = new char[Al];
#ifdef _UNICODE
        UnicodeToASCII(m_copyOfCmdLine.cl_DbP, locDir, Sl);
        UnicodeToASCII(m_copyOfCmdLine.cl_Lang, locLan, Ll);
        UnicodeToASCII(m_copyOfCmdLine.cl_AppDir, locApp, Al);
#else //- not unicode
        strcpy(locDir, m_copyOfCmdLine.cl_DbP.GetBuffer(Sl-3));
        strcpy(locLan, m_copyOfCmdLine.cl_Lang.GetBuffer(Ll-3));
        strcpy(locApp, m_copyOfCmdLine.cl_AppDir.GetBuffer(Al-3));
#endif
        //                                      existing database path
// 10jan08:was::>pDevMgr = hCDeviceManger::instantiate(m_copyOfCmdLine.cl_DbP,
// 10jan08:was::>                   m_copyOfCmdLine.cl_Lang, m_copyOfCmdLine.cl_AppDir);
        pDevMgr = hCDeviceManger::instantiate(locDir,locLan, locApp);
        delete[] locDir;
        delete[] locLan;
        delete[] locApp;
    }

#ifdef STARTTIME_CHECK
    thisCnt = GetTickCount();
    LOGIT(CLOG_LOG,"> Instantiated DeviceMgr (loaded standards).ET:%1.3f (%1.3f)\n",
          (((float)thisCnt-(float)lastTickCnt)/1000),
          (((float)thisCnt-(float)startOfInstantiation)/1000 ));
    lastTickCnt = thisCnt;
#endif
    // if we got a manager and the DB is OK
    if ( pDevMgr != NULL && (! pDevMgr->IsdbNOTavailable())) 
    {   /* added  stevev 06jul05     */
        struct tm *pTime;
        time_t long_time;

        time( &long_time ); 
        pTime = localtime( &long_time );
        dmDate_t strtDt = {pTime->tm_mday,pTime->tm_mon,pTime->tm_year};

        pDevMgr->setStartDate(strtDt);
        /* end added  stevev 06jul05 */

        if ( pCurDev != NULL )
        {
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.generateDevice calling CloseDevice.\n");
            CloseDevice(0,0);
        }
        // generate a device
/*** insert listing here ***/
        if ( m_copyOfCmdLine.cl_IsDirDump )
        {
            Indentity_t     stdTblId;
            stdTblId.wManufacturer = 0x0000;
            stdTblId.wDeviceType   = 0x0000;// new technique 0x0001;
            stdTblId.cDeviceRev    = 0x00;  // new technique 0xff;// ff is search for latest
            stdTblId.cSoftwareRev  = 0x00;  // new technique 0xff;// ff is search for latest 
            stdTblId.cUniversalRev = 0x00;  // new technique 0x06;// use 1 byte mfg search 
            hCddbDevice * pRet =
                            pDevMgr->newDevice(stdTblId,NULL,NULL,NULL,NULL,(DD_Key_t)0,TOK_NONE /* was false */);
            // work
            hCddbDevice *stdTbl= (hCddbDevice*)(pDevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE));
            aCentry* pRoot = NULL;
            RETURNCODE brc =  allocDeviceList(stdTbl, &pRoot );

            if (brc == SUCCESS && pRoot != NULL  )
            {
                generateDirFile(pRoot,m_copyOfCmdLine.cl_dirName );
            }
            m_copyOfCmdLine.cl_IsDirDump = false; // don't do it again
            pMainFrm->BusyDisplay(FALSE);
            ASSERT(AfxGetApp()->m_pMainWnd != NULL);
            AfxGetApp()->m_pMainWnd->SendMessage(WM_CLOSE);

            //exit(0);
            return ;
        }

        LOGIT(STAT_LOG|CLOG_LOG,"      Loading Communication Driver\n");
        // first get a connected comm interface
        CbaseComm* pComm = GetDeviceComm();// instantiates only


#ifdef STARTTIME_CHECK
        thisCnt = GetTickCount();
        LOGIT(CLOG_LOG,"> Got a Device Comm Object.ET:%1.3f (%1.3f)\n",
              (float)(((float)thisCnt-(float)lastTickCnt)/1000),
              (((float)thisCnt-(float)startOfInstantiation)/1000 ));
        lastTickCnt = thisCnt;
#endif

        if (pComm != NULL) // success - we have communication
        {           
            LOGIF(LOGP_START_STOP)
                       (CLOG_LOG,"> doc::generateDevice - has a device comm, initializing.\n");

            rc = pComm->initComm();

            if (rc != SUCCESS)
            {
                LOGIF(LOGP_START_STOP)
                                (CLOG_LOG,"> doc::generateDevice - comm initialize failed.\n");

                pComm->shutdownComm();// we undo what we did
                LOGIT(CERR_LOG | UI_LOG | STAT_LOG,"      Communication Driver Failed");
                pMainFrm->BusyDisplay(FALSE);   // POB - 4 Aug 2004
                return;// rc;
            }

#ifdef STARTTIME_CHECK
            thisCnt = GetTickCount();
            LOGIT(CLOG_LOG,"> Intitialized Comm Object.ET:%1.3f (%1.3f)\n",
                  (((float)thisCnt-(float)lastTickCnt)/1000),
                  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
            lastTickCnt = thisCnt;
#endif
            // clear the comm log screen
            CClient *pSocket = GetSocket();
            char cnststr[8];
            cnststr[0] = 22;
            cnststr[1] ='\0';
            int y = 2;
            if (pSocket)
            {
                LOGIT(CLOG_LOG,"X generate device sends clear to the comm monitor.\n");
                pSocket->SendMessage(cnststr, 1);
            }

            /* stevev 12nov14 moved this function into the initComm function so it would
				access the COM item from a proper thread.  This was giving intermediate 
				failures on Windows 7
				pComm->setHostAddress(m_copyOfCmdLine.cl_HstAddr);
			***/

            pComm->enableComm();

            Indentity_t oneIdentity;
            TRACE(_T("CDDIdeDoc.generateDevice Getting Device Identity.\n"));
            LOGIT(CLOG_LOG | STAT_LOG ,"      Getting Device Identity\n");
            rc = pComm->GetIdentity(oneIdentity, m_copyOfCmdLine.cl_PollAddr);

#ifdef STARTTIME_CHECK
            thisCnt = GetTickCount();
            LOGIT(CLOG_LOG,"> Got an Identity.ET:%1.3f (%1.3f)\n",
                  (((float)thisCnt-(float)lastTickCnt)/1000),
                  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
            lastTickCnt = thisCnt;
#endif
            if ( rc != SUCCESS)
            {
                LOGIT(CERR_LOG | UI_LOG | CLOG_LOG |STAT_LOG,
                                                "ERROR: Failed to acquire a device identity.");
                // during shutdown, the identity pend is abandoned, comm goes null
                if ( pComm != NULL )
                {
                    pComm->shutdownComm();// we undo what we did
                }
                pMainFrm->BusyDisplay(FALSE);   // POB - 4 Aug 2004
                return ;
// now we fall through to check forever...was:  return;
            }
            
            // to get here we have to have a working comm interface
            //
            // NOTE: the comm interface passed to the new device MUST be connected
            //       to that device! The instance passed in is considered a direct
            //       connection - any multiplexer/tunneling etc must be hidden in or
            //       behind this comm interface instance.
                
            CsdcFileSupport* pFile = GetDeviceFile();// instantiates only(NULL if fails)
            TRACE(_T("CDDIdeDoc.generateDevice newDevice.\n"));
            LOGIF(LOGP_START_STOP)(CLOG_LOG,    "> doc::generateDevice newDevice.\n");
#ifdef CheckDeviceMemory
            GlobalMemoryStatus (&(Memstatus[0]));
#endif
int mtime;
struct _timeb timebuffer, endbuffer;
    _ftime( &timebuffer );
            //prashant.oct 2003. Added parameter m_pMethSupport
            pCurDev = pDevMgr->newDevice(oneIdentity, pFile, m_pMethSupport, pComm);
    _ftime( &endbuffer );
    mtime = endbuffer.millitm - timebuffer.millitm;
    if (mtime < 0) 
    {   mtime+=1000;endbuffer.time -= 1;
    }
    LOGIT(CLOG_LOG," NewDevice elapsed time = %I64d.%03d\n",
                                (endbuffer.time - timebuffer.time),mtime);



#ifdef CheckDeviceMemory
            GlobalMemoryStatus (&(Memstatus[1]));
            showMemory(Memstatus[0], Memstatus[1]);
            
            pCurDev = pDevMgr->newDevice(oneIdentity, pFile, m_pMethSupport, pComm);
            GlobalMemoryStatus (&(Memstatus[2]));
            showMemory(Memstatus[1], Memstatus[2]);
#endif

#ifdef STARTTIME_CHECK
            thisCnt = GetTickCount();
            LOGIT(CLOG_LOG,"> Have a New Device.ET:%1.3f (%1.3f)\n",
                  (((float)thisCnt-(float)lastTickCnt)/1000),
                  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
            lastTickCnt = thisCnt;
#endif
            
/***********************************************************************************
            if ( pCurDev == NULL )
            {
                LOGIT(STAT_LOG | CLOG_LOG, "Device did not instantiate..");
                char strTmp[1024] = {0};
                char strFile[256] = {0};

                CString tempStr = m_copyOfCmdLine.cl_DbP;
                tempStr.MakeUpper();
                strcpy(strTmp,m_copyOfCmdLine.cl_DbP);
                if(!tempStr.Find("RELEASE"))
                {
                    strcat(strTmp,"\\Release\\");
                }
                else
                {
                    strcat(strTmp,"\\");
                }
                
                sprintf(strFile,"Could not find %s%06x\\%04x\\%02x??.fms \n"
                    " \t\t Loading Generic DD...",strTmp, 
                    oneIdentity.wManufacturer,oneIdentity.wDeviceType,oneIdentity.cDeviceRev);
                pMainFrm->BusyDisplay(FALSE);   // POB - 4 Aug 2004
                pMainFrm->MessageBox(strFile, NULL, MB_OK|MB_ICONEXCLAMATION); 
                pMainFrm->BusyDisplay();    // POB - 4 Aug 2004

                if (oneIdentity.cUniversalRev == 7)
                {//  version of 'standard device'
                    oneIdentity.wManufacturer   =     DEFAULT_MFG_07; 
                    oneIdentity.wDeviceType     = DEFAULT_DEVTYPE_07;
                    oneIdentity.cDeviceRev      = DEFAULT_DEV_REV_07;
                }
                else
                if (oneIdentity.cUniversalRev == 6)
                {//  version of 'standard device'
                    oneIdentity.wManufacturer   =     DEFAULT_MFG_06; 
                    oneIdentity.wDeviceType     = DEFAULT_DEVTYPE_06;
                    oneIdentity.cDeviceRev      = DEFAULT_DEV_REV_06;
                }
                else
                {// 'standard' device 26800500
                    oneIdentity.wManufacturer   =     DEFAULT_MFG_05; 
                    oneIdentity.wDeviceType     = DEFAULT_DEVTYPE_05;
                    oneIdentity.cDeviceRev      = DEFAULT_DEV_REV_05;
                }

                LOGIT(STAT_LOG | CLOG_LOG, "Loading Generic");
                //prashant.oct 2003. Added parameter m_pMethSupport
                pCurDev = pDevMgr->newDevice(oneIdentity, pFile,m_pMethSupport, pComm);
                if (pCurDev==NULL)
                {
                    LOGIT(STAT_LOG|CLOG_LOG|UI_LOG, "Generic Device would not load.");
                }
                else
                {
                    LOGIT(STAT_LOG|CLOG_LOG|UI_LOG, "Generic Device is being used.");
                    // added 28nov05 - put into initializing until criticals are finished 
                    pCurDev->setDeviceState(ds_OnIniting);// was:: ds_OnLine);
                }
            }
            else
            ********************************************************/
            if ( pCurDev != NULL )
            {// we made it
                // leave this for error log identification  
                LOGIT(CERR_LOG|CLOG_LOG|STAT_LOG,"Device instantiated OK. %02x/%02x/%02x\n",
                    (int)oneIdentity.wManufacturer,(int)oneIdentity.wDeviceType,
                    (int)oneIdentity.cDeviceRev);
                // added 28nov05 - put into initializing until criticals are finished 
                pCurDev->setDeviceState(ds_OnIniting);// was:: ds_OnLine);
				pCurDev->idBase = ((__int64)snIDVal) << 32;// part of verify serial number
            }
        }
        else // pComm doesn't exist
        {// error completion
            // Doc's responsibility to inform the user, we just bail out
            pMainFrm->BusyDisplay(FALSE);   // POB - 4 Aug 2004
            return ;
        }
    }
    else
    {// dev mgr failed or db not available --- can do nothing
        
        if (pDevMgr == NULL )
        {   
            LOGIT(CERR_LOG,"ERROR: Device Manager failed to instantiate.\n");
        }
        else // db not available
        {
            LOGIT(CERR_LOG,"ERROR: Device Manager failed to aquire the binary library.\n");
            CString mb;
            mb.Format(_T("The Device Library is unavailable."));
            AfxMessageBox(mb);
            /* stevev 9/22/03 - let the document delete it when it leaves
            delete pDevMgr;
            pDevMgr = NULL;
            */
        }
        LOGIT(CLOG_LOG|STAT_LOG,"Device Failed to instantiate");
    }// endelse devMgr & DB 


    CString one, two, three, four, five, title;
        
    if ( pCurDev != NULL )
    {   /* stevev 07sep05 - this has to come before read imd''s */
        /* stevev 28jan05 - insert default compatability mode */
        if ( m_copyOfCmdLine.cl_Restriction == 'S' )
        {
            pCurDev->setCompatability(dm_Standard);
        }
        else
        if ( m_copyOfCmdLine.cl_Restriction == 'L' )
        {
            pCurDev->setCompatability(dm_275compatible);
        }
        else
        {
            pCurDev->setCompatability(dm_Standard);
        }

        pCurDev->readCriticalparams();// added stevev 06apr10

        /* the critical param read has been moved to :: void readCriticalparams(void)
           stevev 06apr10 working on bug 3034

        // stevev 10/13/04 - deal with critical items BEFORE population 
        LOGIT(STAT_LOG|CLOG_LOG,"Reading Critical Parameters\n");
        itemIDlist_t* pCrit = (itemIDlist_t*)pCurDev->getListPtr(CitemType(iT_Critical_Item));
        if (pCrit && pCrit->size() > 0)
        {// read 'em now
            LOGIT(CLOG_LOG,"$Critical Item read Length 0x%x\n",pCrit->size());
            CValueVarient vval;// we don't do anything with it
            hCitemBase* pIBase = NULL;
            for (itemIDlist_t::iterator z = pCrit->begin(); z < pCrit->end(); z++)
            {
                // stevev 13sep06 - with the expanded dependency, there are a lot of items in the list
                if ( pCurDev->getItemBySymNumber(*z,& pIBase) == SUCCESS && pIBase != NULL )
                {
                    if ( pIBase->IsVariable() )
                    {
                        LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"$   Critical read 0x%04x\n",*z);
                        // stevev 20sep06 - if an item is critical AND has a DEFAULT_VALUE
                        // we must force a read from device - use ReadCritical()
                        //
                        //rc = pCurDev->ReadImd(*z, vval);
                        rc = pCurDev->ReadCritical(pIBase, vval);// force a read

                        if (rc != SUCCESS )
                        {
                            LOGIT(CLOG_LOG|UI_LOG,"$   Critical read 0x%04x FAILED: "
                                                                    "abort generation.\n",*z);
                            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.generateDevice had a "
                                " Critical Read Failure, calling doc's CloseDevice.\n");                    
                            CloseDevice(0,0);
                            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.generateDevice Exiting.\n"); 
                            return ;
                        }
                        systemSleep(1);
                    }
#ifdef _DEBUG
                    else
                    {
                        LOGIT(CLOG_LOG,"Critical item %s (0x%04x) a %s skipped reading.\n",
                            pIBase->getName().c_str(),pIBase->getID(),pIBase->getTypeName());
                    }
#endif
                }
                // end stevev 13sep06
            }
#ifdef _DEBUG
            pCurDev->dmpVarNotification();
            pCurDev->dmpVarDependency();
#endif
        }
        else
        {
            LOGIT(CLOG_LOG,"$ Empty Critical item list.\n");
        }
        // added 28nov05 - we got criticals if we get here - clear initializing bit 

        /* end move of 06apr10 */
        
        dState_t tmp = (dState_t)(pCurDev->getDeviceState() & (~ (int)ds_OffIniting ));
        pCurDev->setDeviceState( tmp );

        LOGIF(LOGP_START_STOP)
                    (CLOG_LOG,"> doc::generateDevice calling mainFrame's CreateWindowMenu.\n");

        pMainFrm->CreateWindowMenu(); // get top-of-window view menu built

        TRACE(_T("CDDIdeDoc.generateDevice enabling autoUpdate.\n"));

        LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::generateDevice enabling autoUpdate.\n");

        // enable timer and idle task
        appCommEnabled(); // master start

        if ( pCurDev->DeviceDescStrings.size() > 0 )
            one   = pCurDev->DeviceDescStrings[0].c_str();
        if ( pCurDev->DeviceDescStrings.size() > 1 )
            two   = pCurDev->DeviceDescStrings[1].c_str();
        if ( pCurDev->DeviceDescStrings.size() > 2 )
            three = pCurDev->DeviceDescStrings[2].c_str();
        if ( pCurDev->DeviceDescStrings.size() > 3 )
            four  = pCurDev->DeviceDescStrings[3].c_str();          
        
        int id;
        if ( (id = one.ReverseFind(' ')) > 0 )
        {
            five = one.Mid(id);
            five += " ";
        }
        if ( (id = two.ReverseFind(' ')) > 0 )
        {
            five += two.Mid(id);
            five += " Dev Rev ";
        }
        if ( (id = three.ReverseFind(' ')) > 0 )
        {
            five += three.Mid(id + 4,2);
            five += ".";
        }
        if ( (id = four.ReverseFind(' ')) > 0 )
        {
            five += four.Mid(id + 4,2);
        }
        TRACE(_T("CDDIdeDoc.generateDevice Initializing menu.\n"));
        LOGIT(STAT_LOG|CLOG_LOG,"Initializing root menu\n");
// 02dec05 - changed out Doc's handling tree to treeview handling tree

        UpdateAllViews( NULL, UPDATE_ADD_DEVICE, (CObject*)ROOT_MENU );
// end 02dec05 - stevev

        /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
        /* Removed code, CTableChildFrame owns the root (top menu) for each Table view, POB - 5/18/2014 */
        
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"      Generating Title\n");
    /* modified indent 8jan05 */
        hCitemBase *pItem = NULL;
        if (pCurDev->getItemBySymNumber(DEVICE_TAG, &pItem) != SUCCESS)
            pCurDev->getItemBySymNumber(DEVICE_ID, &pItem);

        CString strName = "";
        if (pItem != NULL)
        {
            if (pItem->getTypeVal() == VARIABLE_ITYPE)
            {
               hCVar *pVar = (hCVar*)pItem; // make it a DDB variable
                
                // Check for the selection with Simulator or device
                if (theApp.m_CmdLine.cl_IsSim)
                    strName = "SIM /";
                else
                {
                    strName = theApp.m_CmdLine.cl_Url;
                    strName += " /";
                }

                CValueVarient varRetVal;
                // Do a immediate read as the other read takes time
                // to fetch values
                if (pCurDev->ReadImd(pItem, varRetVal) == SUCCESS)
                {
                    tstring strTag = pVar->getDispValueTString();
                    CString strTemp = strTag.c_str();
                    strName += strTemp.Left(pVar->VariableSize());
                }

                // Log the Event
                char strBuff[MAX_PATH];
                memset(strBuff,0,MAX_PATH);//stop unitialized memory warnings
                //strcpy(strBuff, );
                //strcat(strBuff, strName.GetBuffer(strName.GetLength()));
                //LOGIT(UI_LOG,strBuff);
                char* pB=NULL;
#ifdef _UNICODE
                /*int*/ WideCharToMultiByte(
                CP_ACP,     //UINT CodePage, 
                0,          //DWORD dwFlags, 
                strName,    //LPCWSTR lpWideCharStr, 
                -1,         // null terminated    int cchWideChar, 
                messageBuff,//LPSTR lpMultiByteStr, 
                1000,       //int cbMultiByte, 
                NULL,       //LPCSTR lpDefaultChar, 
                NULL        //LPBOOL lpUsedDefaultChar 
                );
                pB = messageBuff;
#else
                pB = strName.GetBuffer(strName.GetLength());
                strcpy(strBuff,pB);
#endif
                LOGIT(UI_LOG,"Connected to Device - %s",pB);
                // End - Log the Event

                SetTitle(strName);
                
                CClient *pSocket = GetSocket();
                char cnststr[8];
                strcpy(cnststr,"Title");
                cnststr[5]=22;
                cnststr[6]='\0';

                int y = strlen(strBuff);
                strBuff[y++] = 22;
                strBuff[y] = '\0';

                if (pSocket)
                {
                    pSocket->SendMessage(cnststr, 6);                   
                    LOGIT(CLOG_LOG,"X generate device sends clear 2 to the comm monitor.\n");
                    pSocket->SendMessage(strBuff, y);
                }
            }// endif - device id isa variable
        }// endif got an item
    
        _tcscpy(g_pchWindowName, strName);
        _tcscat (g_pchWindowName, _T(" - Browser"));
/*<START>For setting the window title in case of class tab view Anoop*/
        _tcscpy (g_pchClassTabWndName, _T("ClassTab - Browser"));
/* <END> For setting the window title in case of class tab view Anoop */
/* stevev 08jan05 - added image instantiation */
        Image_PtrList_t* pILst 
                           = (Image_PtrList_t*)pCurDev->getListPtr(CitemType(RESERVED_ITYPE2));
        if ( pILst && pILst->size() > 0)
        {// instantiate the images
            int imgCnt = 0;
            for (ImagePtrList_it i = pILst->begin(); i != pILst->end(); ++i,imgCnt++)
            {// ptr2aPtr2a hCimage
                for ( FramePtrList_it f = (*i)->begin(); f != (*i)->end(); ++f)
                {// ptr2aPtr2a hCframe
                    hCframe* pF = *f;
                    //if(AddImage(imgCnt,(*f)->cLang,(*f)->uSize,(*f)->pbRawImage) == SUCCESS )
                    if ( AddImage(imgCnt,pF->cLang,pF->uSize,pF->pbRawImage) == SUCCESS )
                    {
                    //  (*f)->destroy();// release the memory
                        pF->destroy();// release the memory
                    }
                    else
                    {
                        LOGIT(CERR_LOG,"ERROR: Doc failed to instantiate an image.");
                    }
                }
            }//next
        }
/* end stevev 8jan05 */

/* VMKP added on 290104 */
        CVarList* pVars = (CVarList*)pCurDev->getListPtr(CitemType(VARIABLE_ITYPE));

        if ( pVars && pVars->size() > 0)
        {// collect the changed variables
            for ( CVarList::iterator iT = pVars->begin(); iT < pVars->end(); iT++)
            {// iT isa ptr2a ptr 2a hCVar
                if ((*iT)->IsVariable() && (*iT)->isChanged() )
                {
                    (*iT)->CancelIt(); // moves realValue to DispValue
                }
            }
        }
/* End of VMKP added on 290104 */

        

        LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::generateDevice updating all views.\n");
            
        TRACE(_T("CDDIdeDoc.generateDevice updating all views.\n"));
        // 06dec05      was   UPDATE_REBUILD_LIST
        /* Removed code, CTableChildFrame owns the root (top menu) for each Table view, POB - 5/18/2014 */
    }// endif device initialized

     /* VMKP added on 030204 */
    g_chMenuInitialized = TRUE;
     /* VMKP added on 030204 */
    if ( pCurDev != NULL )
    {
        LOGIT(STAT_LOG|CLOG_LOG,STATUS_BAR_DEFAULT L"  %s\n",five);
    }
    else
    {
        LOGIT(STAT_LOG|CLOG_LOG,STATUS_BAR_DEFAULT);
    }
    pMainFrm->BusyDisplay(FALSE);   // POB - 4 Aug 2004
    

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc::generateDevice Exit.\n");
        
    TRACE(_T("CDDIdeDoc.generateDevice Exit.\n"));
    
}

// was OnCloseDevice - no longer a message handler, called from mainframe OnCloseDevice
LRESULT CDDIdeDoc::CloseDevice(WPARAM wParam, LPARAM lParam)
{// opposite of generate device
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.CloseDevice Entry.\n");

//    CDDLBase*      pTemp;
    CString strName = "";

    CMainFrame * p_MainFrm = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *)AfxGetMainWnd();
    /* stevev 14apr10 - delay while method running - bug 2493 */
    if (pCurDev && pCurDev->devIsExecuting)
    {
        Sleep(10);  //let it execute a bit
        p_MainFrm->PostMessage(WM_SDC_CLOSEDEVICE, 0, 0);// to us
    }
    /* end 14apr10 **/
    // set the device to close ds_Closing
    if (pCurDev) pCurDev->devState = ds_Closing;

/*<START>Added by ANOOP 03FEB2004 This is to clean up the error log window  */
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice disabling autoUpdate.\n");

    // disable timer and idle task immediatly
    disableAppCommRequests();

    if(NULL != p_MainFrm )
    {
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice close the error log.\n");
        p_MainFrm->CloseErrorLog();
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice reset the window menu.\n");
        p_MainFrm->ResetWindowMenu();
    }
/*<END>Added by ANOOP 03FEB2004 This is to clean up the error log window    */
        
    if(NULL != p_MainFrm->ptrCmdDlg )
    {
        /*<START>Added by ANOOP 03FEB2004 Ensure that CMD48 window is destroyed if it exists*/
        HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Condition"));
        if(NULL != hwndCMD48Window)
        {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice.destroy existing cmd48 window.\n");
            p_MainFrm->ptrCmdDlg->DestroyWindow(); 
            delete p_MainFrm->ptrCmdDlg;
            p_MainFrm->ptrCmdDlg = NULL;
        }
        /*<END>Added by ANOOP 03FEB2004 Ensure that CMD48 window is destroyed if it exists  */
        
        ptrCMD48Dlg=NULL;
    }
     
    /*<END>Added by ANOOP 01MAR2004 Close Your Cmd48 Dialog Box */
    /* Removed code, CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */

    // now loose the device
    // communications first
    // turn off the document's on idle and on timer
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice sets document isClosing to true (1)\n");
/**************** try **************************/
    /* Removed code, CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
    // CDDLMain is no longer used as tree control root menu, POB - 5/18/2014
 
/***********************************************/
    if ( pCurDev != NULL && pDevMgr != NULL )
    {
        hCdeviceSrvc* pSvc = (hCdeviceSrvc*)pCurDev;
        pCurDev = NULL;// assume we will be successful in deleting it
#ifdef STARTSEQ_LOG
        TRACE(_T("CDDIdeDoc::CloseDevice  delete device.\n"));
#endif
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice call deleteDevice.\n");
        pDevMgr->deleteDevice( &(pSvc) );
        ASSERT(pSvc == NULL);

        pCurDev = (hCddbDevice *)pSvc; // should be null
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice set device 0x%08x. "
                                                    "(was: x%08x)\n",pSvc,pCurDev);
        // Items that must be re-intialized, POB - 5/19/2014
        iNumberOfDynamicVars = 0;
        m_autoUpdateDisabled = false;
        m_bVariableEditedByUser = FALSE;
             
        ptrCMD48Dlg=NULL;
        m_bIgnoreRespCode = FALSE;    
        m_uSuppressionCnt = DEFAULT_SUPPRESS_CNT;
        appComStack.clear();

        for(int nCnt=0;nCnt <1000;nCnt++) 
        {
            DynamicVariableItemIds[nCnt]=0;
        }    
    }
    // else, there is no device or we can't do anything with it.
    // clean the display last

    /* Removed commented out code, POB - 5/20/2014 */


/* clear the title when disconnected */
    // Check for the selection with Simulator or device
    if (m_copyOfCmdLine.cl_IsSim)
        strName = "SIM /";
    else
    {
        strName = m_copyOfCmdLine.cl_Url;
        strName += " /";
    }

    // Log the Event
    char strBuff[MAX_PATH];
    CString discon = "";
    discon = GetTitle();
    strcpy(strBuff, " - Closed Device -");
    char* locStr = new char[discon.GetLength()+3];
#ifdef _UNICODE
    UnicodeToASCII(discon, locStr, discon.GetLength()+3);
#else //- not unicode
    strcpy(locStr, discon.GetBuffer(discon.GetLength()));
#endif
    strcat(strBuff, locStr);
    delete[] locStr;

    LOGIT(UI_LOG,strBuff);
    // End - Log the Event

    SetTitle(strName);
    if ( m_enteredFileNew )
    {
        LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice sending FILE_NEW to re-enter.\n");
        AfxGetApp()->m_pMainWnd->PostMessage(WM_COMMAND, ID_FILE_NEW, 0);
    }

/*Vibhor 020304: Start of Code*/    
/*Since we are not killing the doc on close device
 we need to reset the suppression count back to default*/
    m_uSuppressionCnt = DEFAULT_SUPPRESS_CNT;
/*Vibhor 020304: End of Code*/

    /* Removed code, CTableChildFrame owns the root (top menu) for each Table view, POB - 5/18/2014 */

    Sleep(10); // see if anybody else needs to run
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.closeDevice Exit\n");

    return 0;// no error
}

/* not implemented yet 
void CDDIdeDoc::StaleDynamics(vector <hCitemBase*>& pMenuedDynamics)
    // walk through all views
{
    if ( m_viewList.IsEmpty())
        return;

    POSITION pos = GetFirstViewPosition();
    while (pos != NULL)
    {
        CView* pView = GetNextView(pos);
        ASSERT_VALID(pView);
        SDCView* pSDCV = dynamic_cast<SDCView*>(pView);

        if (pSDCV != NULL)
            pSDCV->StaleDynamics(pMenuedDynamics);
    }
    // returns all dynamics in the views
}
**/

void CDDIdeDoc::generateDirFile(aCentry* pRoot,CString& fileName )
{
    vector<class aCentry>::iterator mfgIT,devtypeIT,devrevIT, ddrevIT;
    CString sMfg, sMfgDevTyp, outStr;
    int mfgWidth = 0, devTypWidth = 0;
    
    // open the file for writing
    CStdioFile myFile;
    CFileException fileException;

    if (! myFile.Open( fileName, CFile::modeCreate |  CFile::modeWrite , &fileException ))
    {
        TCHAR szError[1024];
        fileException.GetErrorMessage(szError, 1024);
        CString err;
        err.Format(_T("ERROR opening file %s (%s)"),fileName,szError);
        AfxMessageBox(err);

        TRACE(_T("Can't open file %s, error = %s (%u)\n"),
           fileName, szError, fileException.m_cause );
        return;
    }

    // write it
    bool cont = true;
    for (mfgIT = pRoot->children.begin(); mfgIT < pRoot->children.end() && cont; mfgIT++)
    {// IT points to a aCentry ---- MFG
        
        if ( mfgIT->name.empty() )
            mfgIT->name = L"";
        //as per Paul's e-mail of Dec 1st 
        //sMfg.Format("%-*s 0x%02X",mfgWidth,mfgIT->name.c_str(),  mfgIT->number); 
        //sMfg.Format(_T("%-*s 0x%06X"),mfgWidth,mfgIT->name.c_str(),  mfgIT->number);//18sep09
        sMfg.Format(_T("%-s\t0x%06X"),mfgIT->name.c_str(),  mfgIT->number);
        
        for (devtypeIT = mfgIT->children.begin(); 
             devtypeIT < mfgIT->children.end() && cont;    devtypeIT++)
        {// IT points to a aCentry ---- DEVTYPE

            if ( devtypeIT->name.empty() )
                devtypeIT->name = L" ";

            if ( devtypeIT->name[0] == '_' )
                devtypeIT->name.erase(1,1);
            //as per Paul's e-mail of Dec 1st 
            //sMfgDevTyp.Format("%s %*s  0x%02X",sMfg, 
            //sMfgDevTyp.Format(_T("%s %*s  0x%04X"),sMfg,                  // changed 18sep09
            //  devTypWidth,devtypeIT->name.c_str(),    devtypeIT->number); // changed 18sep09
            sMfgDevTyp.Format(_T("%s\t%-s\t0x%04X"),sMfg, devtypeIT->name.c_str(), 
                                                                            devtypeIT->number);

            for (devrevIT = devtypeIT->children.begin(); 
                 devrevIT < devtypeIT->children.end() && cont;    devrevIT++)
            {// IT points to a aCentry ---- DEVREV
                int dr = devrevIT->number & 0xFF;// this is the device revision
                // each device rev has multiple dd revs, each with multiple format revs
                int dv = -1;
                int dd = -1;
                
                string restOfStr;
                for ( ddrevIT = devrevIT->children.begin(); 
                      ddrevIT < devrevIT->children.end();     ++ddrevIT)
                {
                    // added 18sep09 - paul doesn't want so many items on his list
                    if ((ddrevIT->number & 0xFF) == dd)
                    {// skip all of this dd rev
                        continue;
                    }

                    dv = (ddrevIT->number & 0xFF00) >> 8;// this is actually the format number
                    dd = (ddrevIT->number & 0xFF);

                    /* stevev 6apr10 - make another column from the help string */
                    string::size_type loc = devtypeIT->help.find(' ');
                    if ( loc != string::npos)
                        devtypeIT->help[loc] = '\t';
                    /* column made */

                    outStr.Format(_T("%s\t%-3d\t%-3d\t%s\n"),sMfgDevTyp, dr, dd, 
                                                                    devtypeIT->help.c_str() );

                    ///////// write one line here ////////////////////
                    TRY
                    {
                        myFile.WriteString( outStr );
                    }
                    CATCH ( CFileException, peX )
                    {
                        TCHAR szError[1024];
                        peX->GetErrorMessage(szError, 1024);
                        CString err;
                        err.Format(_T("ERROR writing file %s (%s)"),fileName,szError);
                        AfxMessageBox(err);

                        TRACE(_T("File write error = %s (%u)\n"),
                           szError, fileException.m_cause );

                        cont = false;
                    }
                    END_CATCH

                }// next ddrev
            }// next devrev
        }
    }

    myFile.Close();
    releaseDevList ( &pRoot );
}

/* moved the following common code here to encapsulate the flag 15nov11 */

void  CDDIdeDoc::disableAppCommRequests(void)
{
    appComStack.push_back(m_autoUpdateDisabled);
    m_autoUpdateDisabled = true;
    DEBUGLOG(CLOG_LOG,"DOC:disable the App Comm\n");
}
    
void  CDDIdeDoc::enableAppCommRequests(void)// put back like we found it
{   
    bool newVal = false;
    if (appComStack.size() > 0)
    {
        newVal = appComStack.back();// FIFO
        appComStack.pop_back();
    }
    m_autoUpdateDisabled = newVal;// false;
    
    DEBUGLOG(CLOG_LOG,"DOC:%s the App Comm\n",(newVal)?"Re-disables":"Enables");
}

bool  CDDIdeDoc::appCommEnabled (void)
{   
    if ( pCurDev && pCurDev->devState == ds_OnLine )
    {
        return ! m_autoUpdateDisabled;
    }
    else
    {// auto updateis only valid if we have gotten online
        return false;
    }
    // was previously bassed on commInfc....if (m_CommStatus > commNC)
    //{// we are shut(ting) down
    //  return false;
    //}
}

// moved from .h file 27aug12 to help xmtrdd compile
void CDDIdeDoc::genDev(void)
{ 
    CMainFrame *pMF = GETMAINFRAME;// stevev 08oct10:was:> (CMainFrame *) AfxGetMainWnd();
    if (pMF) //if (m_pTreeView) 
    pMF->PostMessage(WM_SDC_GENERATEDEVICE, 0, 0); 
}

/* end move generate device from treeview */
#ifdef CheckDeviceMemory

// Used to convert MB to KB
#define DIV 1024
// Specify the width of the field in which to print the numbers.
// The asterisk in the format specifier "%*ld" takes an integer 
// argument and uses it to pad and right justify the number.
#define WIDTH 7


void showMemory(MEMORYSTATUS& preA, MEMORYSTATUS& post)
{
    if (preA.dwLength != sizeof(preA))
    {
        LOGIT(CLOG_LOG,"\nThe  pre-MEMORYSTATUS structure is %ld bytes long;"
            "it should be %d.\n", preA.dwLength, sizeof (preA));
    }
    if (post.dwLength != sizeof(post))
    {
        LOGIT(CLOG_LOG,"The post-MEMORYSTATUS structure is %ld bytes long;"
          "it should be %d.\n", post.dwLength, sizeof (post));
    }

  LOGIT(CLOG_LOG,"There is  %*ld percent of memory in use.\n",
          post.dwMemoryLoad);
  LOGIT(CLOG_LOG,"There are %*ld total Kbytes of physical memory.\n",
          WIDTH, post.dwTotalPhys/DIV);
  LOGIT(CLOG_LOG,"There are %*ld total Kbytes of paging file.\n",
          WIDTH, post.dwTotalPageFile/DIV);
  LOGIT(CLOG_LOG,"There are %*ld total Kbytes of virtual memory.\n",
          WIDTH, post.dwTotalVirtual/DIV);

LOGIT(CLOG_LOG,"Physical memory pre: %*ld free Kbytes  post: %*ld free Kbytes Diff: %*ld .\n",
          WIDTH, preA.dwAvailPhys/DIV, 
          WIDTH, post.dwAvailPhys/DIV, 
          WIDTH/2, ( preA.dwAvailPhys/DIV - post.dwAvailPhys/DIV));
LOGIT(CLOG_LOG,"Paging  memory pre: %*ld free Kbytes  post: %*ld free Kbytes Diff: %*ld .\n",
          WIDTH, preA.dwAvailPageFile/DIV, 
          WIDTH, post.dwAvailPageFile/DIV, 
          WIDTH/2, ( preA.dwAvailPageFile/DIV - post.dwAvailPageFile/DIV));
LOGIT(CLOG_LOG,"Virtual memory pre: %*ld free Kbytes  post: %*ld free Kbytes Diff: %*ld .\n\n",
          WIDTH, preA.dwAvailVirtual/DIV, 
          WIDTH, post.dwAvailVirtual/DIV, 
          WIDTH/2, ( preA.dwAvailVirtual/DIV - post.dwAvailVirtual/DIV));
}
#endif //CheckDeviceMemoryCheckDeviceMemory

/*********************************************************************************************
 *
 *   $History: DDIdeDoc.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *********************************************************************************************
 */
