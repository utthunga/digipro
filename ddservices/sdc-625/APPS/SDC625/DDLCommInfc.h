/*************************************************************************************************
 *
 * $Workfile: DDLCommInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "DDLCommInfc.h"
 */
#if !defined(AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)
#define AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DDLCommInfc.h : header file - for both DDLopc and DDLpipe
//
#ifndef _CONSOLE
#include "SDC625Doc.h"
#include "HARTsupport.h"
#include "DDLBaseComm.h"

//#include <initguid.h>
#include "IHartComm.h"

// timj begin 4/6/04
#include "HartServer.h"
//#include "HSelect.h"
// timj begin 4/6/04

#include "HUtil.h"
#else // is not console
class CDDIdeDoc;
#include "DDLBaseComm.h"
#endif

#include "ddbEvent.h"	/* stevev 18jan05 - for thread handshaking */
/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc 
#ifdef  DOCCOMMQSIZE
#undef  DOCCOMMQSIZE
#endif
#define DOCCOMMQSIZE	    2	/* the growby size */
#ifdef  DOCCOMMRETRIES
#undef  DOCCOMMRETRIES 
#endif
#define DOCCOMMRETRIES	    3
#define DOCCOMMMAXPOLLADDR	64	/* from 16 stevev 18sep08 - hart 6 goes higher */

// delayed response is now in the opc server, it may never come back...
#define HART_SERVER_TIMEOUT (( 5 * 60 ) * 1000)  // 5 minutes

#ifndef  __HSelect_h__
struct IHartSelect2;
struct IHartSelect3;
#endif//__HSelect_h__

extern void killContainer(void);// stevev 9sep10

typedef struct COMptr_s
{
#ifndef _CONSOLE
	IHartComm*      pComm;
	IHartUtilities* pUtil;
	
// timj begin 4/6/04
	IHartSelect2*	pSelect;
	IHartSelect3*   pSelect3;//stevev 29sep09
	IUnknown*		pServer;	
// timj end 4/6/04
	bool   suppressModemSrvr;
	struct COMptr_s():pComm(NULL),pUtil(NULL),pSelect(NULL),pServer(NULL),suppressModemSrvr(false){};
#endif
}
COMptr_t;

class CMainFrame;

class CdocCommInfc : public CbaseComm, public COMptr_t //COMptr_t belongs to the async thread
{
// Attributes
protected:
//use base class	CDDIdeDoc*   m_pDoc;
	CMainFrame*  m_pMainFrm;	/* stevev - for light control */

	Indentity_t  m_thisIdentity;
	long         m_commHndl; 
	HANDLE       hRegisteredTask;
	commStatus_t m_CommStatus;	// enabling all rolled into one

	bool         m_hadConfigChanged;
	int          m_skipCount;  // how many we have skipped

/*Vibhor 120204: Start of Code*/

	bool         m_bMoreStatusSent;

	/*Vibhor 120204: End of Code*/

// stevev 4/2/4::
	int lastDRstate;
	int rePollCount;

	// moved to DOC  15nov11    vector<bool> appComStack;



public:
	hCPktQueue      thePktQueue;
   
	BYTE        m_pollAddr;
	bool        m_connected;

	bool         m_pending;// stevev 11jul07 - try to prevent wait-forever lockup

// Implementation
public:
	CdocCommInfc(CDDIdeDoc* pD = NULL);
	virtual ~CdocCommInfc();          

// Operations
protected:		// helper functions
	RETURNCODE connect   (void);// expected to run in comminfctask
	RETURNCODE disconnect(void);// expected to run in comminfctask
	void       getErrorStr(HRESULT hR, string& retStr);
	RETURNCODE rawSend(hPkt* pSendPacket, hPkt* pRecvPacket);
	void       stxSocketDump(hPkt* pPkt);// stevev 4/2/04 - extract the logging to a separate func
	void       ackSocketDump(hPkt* pPkt);// stevev 4/9/04 - extract the logging to a separate func
	
	// stevev 4/2/4::
	RETURNCODE handleDelayedResp(int respCd);//  filter RC before entry
	// stevev 18jan05 - moved from public
#ifndef _CONSOLE
	CDDIdeDoc* GetDocument(void){ if( m_pDoc) return m_pDoc;
				else return ((CDDIdeDoc*)((CDDIdeApp*)AfxGetApp())->pDoc);}
#endif
public:
	// use parent's setRcvService(hCsvcRcvPkt* cpRcvSvc) // sets the dispatcher's service routine

	hPkt* getMTPacket(bool pendOnMT=false);// gets an empty packet for generating a command 
								//		(gives caller ownership of the packet*)
	void  putMTPacket(hPkt*);	// returns the packet memory back to the interface 
								//		(this takes ownership of the packet*)
	RETURNCODE
		  SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
						        // puts a packet into the send queue  
								//		(takes ownership of the packet*)
	RETURNCODE 
		SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
						        // puts a packet into the front of the send queue  
								//		(takes ownership of the packet*)

	RETURNCODE		
		  SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

	int  sendQsize(void); 	    // how many are in the send queue ( 0 == empty )
	int	 sendQmax(void) ;		// how many will fit
	void clearQueue(void);      // clear the que, dealing with all pending 

	RETURNCODE  initComm(void);	// starts the thread that does the init
	RETURNCODE  shutdownComm(void);
	RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr);
	long        getCommHandle(void) { return  m_commHndl; };// stevev 18jan05
	void enableComm(void){
	// do nothing...m_CommStatus = commOK;
	};

	void  disableAppCommRequests(void);
	void  enableAppCommRequests (void);
	bool  appCommEnabled        (void); 


// stevev 25jan06 - use DDLBaseComm version::
//	void  notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged = NO_change);		
	void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);
	RETURNCODE	getDelayedRetryOK();//APP_USER_OKED || APP_USER_CANCELED

	void setHostAddress(BYTE ha)
		{
#ifndef _CONSOLE
			if ( ha != 1 ) ha = 0; // boolean - set Primary or not (is set secondary)
			pComm->put_bPrimaryMaster(ha);
#endif
		};
	
	BYTE getHostAddress( void )
	{	BOOL ret; pComm->get_bPrimaryMaster(&ret);return (ret)?1:2; };

	short setNumberOfRetries( short newVal )
	{	short local = 0;		
		pComm->get_nRetries(&local);
		pComm->put_nRetries(newVal);
		return local;
	};

	// DDIde specific calls
	RETURNCODE Search(Indentity_t& retIdentity, BYTE& newPollAddr);
	void       commInfcTask(waitStatus_t isTimerExpired);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOCCOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *  The registered gfWaitAndTimerCallbackfunction is triggered each time there is an entry into 
 *the queue.  It is a 'C' routine that just calls this class's commInfcTask method.
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLCommInfc.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:52a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Verify/Add HART standard headers and footers
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
