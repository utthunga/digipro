/*************************************************************************************************
 *
 * $Workfile: DDLBase.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDLBase.cpp : implementation file
 */

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h" // this includes DDLBase.h
#include "LabelDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
//26aug08 #define LOG_MSGS 1
#endif
/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLBase, CObject )

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLBase::CDDLBase(hCitemBase* pIB)
{
    m_nImage     = IMG_MENU;
    m_pItemBase  = pIB;
#ifdef _DEBUG
    if (m_pItemBase)
        m_pItemBase->IsValid();// test for good pointer
#endif
    m_pReference = 0;

    m_hTreeItem  = NULL;
    m_bIsValid   = TRUE;
    m_pParent    = NULL;

    m_strName    = _T("");
    m_strNmValid = false;
    m_strHelp    = _T("");  
    m_lItemId    = 0;
    m_strMenuSourceName.Empty();
    m_classExpanded = false; 
    m_IndexNumber = -1; //unused

    if (pIB != NULL)
    {
    /*  if (pIB->HasHelp())
        {
            string szHelp;
            pIB->Help(szHelp);
            m_strHelp = szHelp.c_str();
        }*/
        m_lItemId = pIB->getID();
    }
#ifdef _DEBUG
    TRACE("CDDLBase created 0x%04x ( ptr %p )\n",GetID(),this);
#endif
}

// Copy Constructor
CDDLBase::CDDLBase(const CDDLBase & Base)
{/* re-arranged and made sure they were all there - stevev 30nov05 */
    m_nImage    = Base.m_nImage;
    m_pItemBase = Base.m_pItemBase;
#ifdef _DEBUG
    if (m_pItemBase)
        m_pItemBase->IsValid();// test for good pointer
#endif
    m_hTreeItem = Base.m_hTreeItem;
    m_bIsValid  = Base.m_bIsValid;
    m_pParent   = Base.m_pParent;

    m_strName   = Base.m_strName;
    m_strNmValid= Base.m_strNmValid;
    m_strHelp   = Base.m_strHelp;
    m_lItemId   = Base.m_lItemId;
    m_strMenuSourceName = Base.m_strMenuSourceName;
    m_IndexNumber = Base.m_IndexNumber;
    
    m_classExpanded = false;
#ifdef _DEBUG
    TRACE("CDDLBase copied 0x%04x (to ptr %p )\n",GetID(),this);
#endif
}

/**************************************************************************
 * FUNCTION: CDDLBase destructor
 *
 * DESCRIPTION:
 *   The base class contains a list of pointers to allocated data.  This
 * memory must be freed.
 ***************************************************************************/
CDDLBase::~CDDLBase()
{
    DeleteList();
#ifdef _DEBUG
    unsigned id = GetID();
    //try to see if selection is destroyed of just trashed
    TRACE("CDDLBase destroyed 0x%04x ( ptr %p )\n",id,this);
#endif
}

void CDDLBase::DeleteList(void)
{
    CDDLBase *pBaseTemp;
    TRACE("CDDLBase id 0x%4x deleting child list.\n", m_lItemId);

    /*
     * Delete all of the children
     */
    while( int y = m_ChildList.GetCount() )
    {
        pBaseTemp = m_ChildList.RemoveHead();
        if (pBaseTemp == NULL) // in case there are empty elements
            continue;// skip it
        if (pBaseTemp->IsMenu())
        {
            pBaseTemp->DeleteList();// get rid of the children
        }

        delete pBaseTemp;
    }
    /*
//BOOL TreeViewCntrl:DeleteItem(   HTREEITEM hItem );
    extern CTreeCtrl* pmyTreeCtrl;
// The item whose children will be deleted.
extern HTREEITEM hmyItem;

// Delete all of the children of hmyItem.
if (pmyTreeCtrl->ItemHasChildren(hmyItem))
{
   HTREEITEM hNextItem;
   HTREEITEM hChildItem = pmyTreeCtrl->GetChildItem(hmyItem);

   while (hChildItem != NULL)
   {
      hNextItem = pmyTreeCtrl->GetNextItem(hChildItem, TVGN_NEXT);
      pmyTreeCtrl->DeleteItem(hChildItem);
      hChildItem = hNextItem;
   }
}
*/


}
/////////////////////////////////////////////////////////////////////////////
// CDDLBase Member Functions

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

// CString cast operator
CDDLBase::operator CString(void)
{ 
    return "";
}

void CDDLBase::SetTreeItem(HTREEITEM hTreeItem)
{
    m_hTreeItem = hTreeItem;
}

HTREEITEM CDDLBase::GetTreeItem()
{
    return m_hTreeItem;
}

void CDDLBase::SetParent(CDDLBase* pParent)
{
    m_pParent = pParent;
}

CDDLBase * CDDLBase::GetParent()
{
   return m_pParent;
}

CDDLBasePtrList_t& CDDLBase::GetChildList()
{
    return m_ChildList;
}
void CDDLBase::UpdateChildList(void)
{// this actually doesn't HAVE TO get done except in a menu
    return;
}
/* VMKP added on 210104 */
void CDDLBase::SetID(unsigned long  lItemId)
{
    m_lItemId = lItemId;
}
/* VMKP added on 210104 */
void CDDLBase::SetName(CString strName)
{// never used
    m_strName = strName;
    m_strNmValid = true;
}

CString CDDLBase::GetName()
{
    //if ( m_strName.IsEmpty() && m_pItemBase != NULL )
    if ( (!m_strNmValid) && m_pItemBase != NULL )
    {
        // 1 Aug 08 (POB) This code appears to be only called by the
        // CWaoDlg class since the Label for most VARIABLEs are determined 
        // at construction time (which means it is not a null string
        // at this time).
        //
        // This may be a problem since this call for the
        // item's label may not show the appropriate concatenation of
        // a VARIABLE's Label when referenced via descriptions for
        // arrays/collection.
        // 
        wstring t; // for UTF-8 support, POB - 1 Aug 2008
        if ( m_pItemBase->Label(t) == SUCCESS && !t.empty() )
        {// this is called for menus to get their names for initial paint (from treeview)
            m_strName = t.c_str();
            m_strNmValid = true;
        }
    }
    return m_strName;
}
// stevev added 02dec05
CString CDDLBase::GetCurrentName(CDDLBase* pChildItm)
{// to get the current-reference based name for pChildItm
    return pChildItm->GetName();// for menues (& collections) only!
}
/* VMKP added on 210104 */
unsigned long CDDLBase::GetID()
{
    return m_lItemId;
}
/* VMKP added on 210104 */
void CDDLBase::SetHelp(CString strHelp)
{
    m_strHelp = strHelp;
}

CString CDDLBase::GetHelp()
{
    /* stevev 18dec13 - this is not a valid override of calculated help value */
    //if ( m_strHelp.IsEmpty() && m_pItemBase != NULL )
    //{
    //  wstring t; // for UTF-8 support, POB - 1 Aug 2008
    //  if ( m_pItemBase->Help(t) == SUCCESS && !t.empty() )
    //  {
    //      m_strHelp = t.c_str();
    //  }
    //}

    return m_strHelp;
}

CString CDDLBase::GetRelation()
{
    // Intended only for Variables
    return "";
}
CString CDDLBase::GetUnitStr()
{
    return "";
}

void CDDLBase::SetValidity(BOOL valid)
{
    m_bIsValid = valid;
}

BOOL CDDLBase::IsValid()
{// added resolution 21mar05 
    if ( m_pItemBase )
        return m_pItemBase->IsValid();
    else
        return m_bIsValid;
}

BOOL CDDLBase::IsReview()
{
     // Intended only for Menu items
     return FALSE;
}

BOOL CDDLBase::IsReadOnly()
{
     // Intended only for Menu items
     return FALSE;
}

BOOL CDDLBase::IsDisplayValue()
{
     // Intended only for Menu items
     return FALSE;
}

BOOL CDDLBase::IsMenu()
{
    return FALSE;
}

BOOL CDDLBase::IsVariable()
{
    return FALSE;    
}

BOOL CDDLBase::IsMethod()
{
    return FALSE;
}


BOOL CDDLBase::IsExpanded()
{
    return FALSE;
}
/*Vibhor 170304: Start of Code*/

BOOL CDDLBase::IsEditDisplay()
{
    return FALSE;
}

/*Vibhor 170304: End of Code*/

void CDDLBase::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_MENU;    
 
}

/* 06dec05 - no longer needed!
   13aug08 - removed 
 */

// insert without the update msg 30nov05
// returns true if updated (doesn't already exist)
bool CDDLBase::AddChild( CDDLBase* pChild )
{
    ASSERT( pChild != NULL  );
    pChild->SetParent( this );

    POSITION  pos = GetChildList().Find( pChild ) ;
    if ( pos  == NULL )
    {

        GetChildList().AddHead( pChild );
        // note: I left this at 'AddHead' because I think we compensate elsewhere
        //       by reverse-traversing the list - it should match insertChild().
        return true;
    }
    else
    {
        LOGIT(CLOG_LOG,"    UI.DDLbase:: Item 0x%04x not added as child (exists).\n",pChild->GetID());
        return false;
    }
}

void CDDLBase::Execute()
{
    // Base Class does nothing:  
    // Needs to be overriden

}
//Vibhor : Start of Code
void CDDLBase::Execute(CString strName,CString strHelp)
{

}

//Vibhor : End of Code

/** stevev 01dec05 **/
void CDDLBase::RemoveNdeleteChild( CDDLBase * pChild )
{   // find it and get rid of it
    CObject* pa;
    POSITION prepos, pos = GetChildList().GetHeadPosition();
    while( (prepos = pos) )// set equal, test for null
    {
        CDDLBase* pOneChild = (CDDLBase*)GetChildList().GetNext( pos );// moves to next
        if ( pChild == pOneChild )
        //yeah, i know it's a pointer comparision(but it's all we got)
        {
            pa = GetChildList().GetAt( prepos ); 
            GetChildList().RemoveAt( prepos );
            delete pa;
            break;// out of while loop
        }
    }
}
/* end stevev 01dec05 */

/** stevev 26may11 **/
int CDDLBase::deleteLeaf( ulong targetID )
{   // find it and get rid of it
    int removalcount = 0;
    CObject* pa;
    POSITION prepos, pos = GetChildList().GetHeadPosition();
    while( (prepos = pos) )// set equal, test for null
    {
        CDDLBase* pOneChild = (CDDLBase*)(GetChildList().GetNext( pos ));// moves to next
        if ( pOneChild->GetID() == targetID && !pOneChild->IsMenu() )// menues handled in tree
        {
            pa = GetChildList().GetAt( prepos ); 
            removalcount++;
            GetChildList().RemoveAt( prepos );
            delete pa;
        }
    }
    return removalcount;
}
/* end stevev 26may11 */



bool CDDLBase::isValid()
#ifdef _DEBUG
    {
        if (/* AfxIsValidAddress( (void*) m_pItemBase, 64, FALSE )&&*/
             AfxIsValidAddress( (void*) m_pParent, 64, FALSE ))
            return true;
        else
            return false;
    }
#else
    {return true;}
#endif

void CDDLBase::OnUpdateMenuExecute(CCmdUI* pCmdUI)
{
    //empty
}

void CDDLBase::DisplayValue()
{
    //empty
}

void CDDLBase::DisplayLabel()
{
    CLabelDlg dlgLabel("", GetName());  // When displaying label, provide no caption
    dlgLabel.sdcModl();// DoModal();
}

void CDDLBase::DisplayHelp()
{
    CHelpBox helpBox(GetName(), GetHelp());
    helpBox.Display();
}


// Access protected Image ID, POB - 5/16/2014
int CDDLBase::GetImage()
{
    return m_nImage;
}

/* 
 * Search up this branch from the selected
 * CDDLBase and find the 1st parent object
 * (from the top down) that is valid right
 * before the rest of the branch becomes
 * invalid.
 *
 * returns a CDDLBase*:  valid branch
 * returns a NULL:  valid branch not found
 * POB - 5/22/2014
 */
CDDLBase* CDDLBase::FindValidBranch()
{
    CDDLBase *pTemp = this;
    CDDLBase *pValidFalse = NULL;
    CDDLBase *pValidParent = NULL;

    do
    {
        if (!pTemp->IsValid())
        {
            // This is no longer valid                         
            pValidFalse = pTemp;
        }

        // Go to the next parent
        pTemp = pTemp->GetParent();

    }while (pTemp); // Loop unitl a NULL is dicovered

    if (pValidFalse)
    {
        /*
         * We have at least one parent menu in our
         * selection branch which is not valid
         * Get the object above this invalid one
         * It should be a valid parent (if not
         * NULL).
         */
        pTemp = pValidFalse->GetParent();

        if (pTemp)
        {
            /*
             * We have found a valid parent in the
             * branch
             */
            pValidParent = pTemp;
        }
    }
    else
    {
        /* There are no invalid parents
         * get its immediate parent
         */
        pValidParent = this;
    }

    return pValidParent;
}

/*************************************************************************************************
 *
 *   $History: DDLBase.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
