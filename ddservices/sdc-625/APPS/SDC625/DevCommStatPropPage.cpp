// DevCommStatPropPage.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "DevCommStatPropPage.h"
#ifdef GE_BUILD
#include "MainFrm.h"			// PAW 04/06/09
#include "Command48_PropSheet.h"// PAW 04/06/09
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CDevCommStatPropPage property page
CString strStatusStrings[] = {"Device malfunctioning", 
							 "Configuration changed",
							 "Cold start",
							 "More status available",
							 "PV AO fixed",
							 "PV AO saturated",
							 "Non primary variable out of limits",
							 "Primary variable out of limits"
};

//Modifed by ANOOP 17FEB2004 
CString strCommStatusStrings[] = {	"Undefined",
									"Vertical Parity error",
							"Overrun error",
							"Framing error",		
							"Longitudinal Parity error",
							"(reserved)",
							"Buffer overflow",
							"(undefined)"
								};

IMPLEMENT_DYNCREATE(CDevCommStatPropPage, CPropertyPage)

CDevCommStatPropPage::CDevCommStatPropPage() : CPropertyPage(CDevCommStatPropPage::IDD_ONE)
{
	//{{AFX_DATA_INIT(CDevCommStatPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

}

CDevCommStatPropPage::CDevCommStatPropPage(CDDLVariable *pVarDevStatus)
	: CPropertyPage(CDevCommStatPropPage::IDD_TWO)
{
	//{{AFX_DATA_INIT(CDevCommStatPropPage)
	bCommStatusDisplayed=FALSE;
	bDeviceStatusDisplayed=FALSE;


	if(NULL != pVarDevStatus)
	{
		bDeviceStatus=TRUE;
	}
	
	m_pVarCommStatus=NULL;
	m_pVarDevStatus = new CDDLVariable(pVarDevStatus->m_pddbVar);

	for (int nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		if(bDeviceStatus)
		{
			m_pBtnArray_DevStat[nCount] = NULL;
			m_pStaticArray_DevStat[nCount] = NULL;
			byArrPreviousDeviceResp[nCount]=0;
			byArrPresentDeviceResp[nCount]=0;	
		}
	}
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
#ifdef GE_BUILD
	num_buttons = MAX_STATUS_BITS;	// PAW 08/06/09
#endif
}


CDevCommStatPropPage::CDevCommStatPropPage(CDDLVariable *pVarDevStatus,	CDDLVariable *pVarCommStatus)
	: CPropertyPage(CDevCommStatPropPage::IDD_ONE)
{
	//{{AFX_DATA_INIT(CDevCommStatPropPage)
	bCommStatusDisplayed=FALSE;
	bDeviceStatusDisplayed=FALSE;
//	bSuppress_MsgDisplayed=FALSE;
//	bStaticCtrl_Created=FALSE;

	if(NULL != pVarDevStatus)
	{
		bDeviceStatus=TRUE;
	}
	else
	{
		bDeviceStatus=FALSE;
	}
	
	m_pVarDevStatus = NULL;
	m_pVarCommStatus	= new CDDLVariable(pVarCommStatus->m_pddbVar);

	for (int nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		if(bDeviceStatus)
		{
		m_pBtnArray_DevStat[nCount] = NULL;
		m_pStaticArray_DevStat[nCount] = NULL;
		}
		else
		{
		m_pBtnArray_Commn[nCount] = NULL;
		m_pStaticArray_Commn[nCount] = NULL;
			byArrPreviousCommResp[nCount]=0;
			byArrPresentCommResp[nCount]=0;
		}		

	}
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDevCommStatPropPage::~CDevCommStatPropPage()
{
		if(TRUE == bDeviceStatus)
		{
	for (int nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
				if (NULL != m_pBtnArray_DevStat[nCount])
			delete m_pBtnArray_DevStat[nCount];
		
				if (NULL != m_pStaticArray_DevStat[nCount])
			delete m_pStaticArray_DevStat[nCount];
			}
		}
		else if(FALSE == bDeviceStatus)
		{
			for (int nCount = 0; nCount < MAX_COMM_STATUS_BITS; nCount++)
			{
				if ( NULL != m_pBtnArray_Commn[nCount])
			delete m_pBtnArray_Commn[nCount];

				if (NULL != m_pStaticArray_Commn[nCount])
			delete m_pStaticArray_Commn[nCount];
	}
		}

		if(NULL != m_pVarDevStatus)
		{
			delete m_pVarDevStatus;
		}

		if(NULL != m_pVarCommStatus)
		{
			delete m_pVarCommStatus;
		}


}

void CDevCommStatPropPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDevCommStatPropPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDevCommStatPropPage, CPropertyPage)
	//{{AFX_MSG_MAP(CDevCommStatPropPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDevCommStatPropPage message handlers

BOOL CDevCommStatPropPage::OnInitDialog() 
{
	int		nVal =0 ;
	int		nByteVal = 0;

	CPropertyPage::OnInitDialog();
#ifndef GE_BUILD
	CRect rectDevStatus1(40,72,290,89);
	CRect rectDevStatus2(67,72,290,89);
#else
	CRect rectDevStatus1(/*40*/30,72,290,89);	// moved 04/06/09 PAW
	CRect rectDevStatus2(/*67*/57,72,290,89);	// moved 04/06/09 PAW
#endif

	CRect rectCommStatus1(40,72,290,89);
	CRect rectCommStatus2(67,72,290,89);

	int		nCount = 0;

    CFont* pFont = GetFont(); //Get the font from the Property Page Dialog for UTF-8 support, POB - 2 Aug 2008

	if(TRUE == bDeviceStatus)
	{
		if (m_pVarDevStatus == NULL) return FALSE;

//		if (m_pVarDevStatus->GetType() != TYPE_BITENUM)	return FALSE;

		nVal = m_pVarDevStatus->m_pddbVar->getDispValue();
		nByteVal = 0x80;  // 2nd Byte contains the Device status
							  // refer Status coding page under technical overview manual

#ifdef GE_BUILD
// PAW start 04/06/09 add OK and More status buttons

	nPage = 0;

	CRect OK_rect(172,300,262,350);
	CRect Prev_rect(10,300,90,350);
	CRect Next_rect(91,300,171,350);
#endif
        // formating changes for better readability, POB - 15 Aug 2008
	for (nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		m_pBtnArray_DevStat[nCount] = new CBitmapButton();
		m_pBtnArray_DevStat[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectDevStatus1, this, 100);
		
		if (nVal & nByteVal)
		{
					byArrPreviousDeviceResp[nCount]=1;
				byArrPresentDeviceResp[nCount]=1;
			if(NULL != m_pBtnArray_DevStat[nCount] )
			{
				m_pBtnArray_DevStat[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
			m_pBtnArray_DevStat[nCount]->SizeToContent();
		}
			
		}
		else
		{
					byArrPreviousDeviceResp[nCount]=0;
				byArrPresentDeviceResp[nCount]=0;
			if(NULL != m_pBtnArray_DevStat[nCount] )
			{
				m_pBtnArray_DevStat[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
			m_pBtnArray_DevStat[nCount]->SizeToContent();
		}
		}


		m_pStaticArray_DevStat[nCount] = new CStatic();
		m_pStaticArray_DevStat[nCount]->Create(strStatusStrings[nCount], WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectDevStatus2, this, 100);
			
		
		nByteVal = nByteVal >> 1;

		rectDevStatus1.top = rectDevStatus1.bottom + 10;
		rectDevStatus1.bottom = rectDevStatus1.top + 16;

		rectDevStatus2.top = rectDevStatus2.bottom + 10;
		rectDevStatus2.bottom = rectDevStatus2.top + 16;

            
            // Set the parent's Font for the control, POB - 2 Aug 2008
            m_pStaticArray_DevStat[nCount]->SetFont(pFont);

	}
		bDeviceStatusDisplayed=TRUE;

	}
	else if ( FALSE == bDeviceStatus)
	{
		if (m_pVarCommStatus == NULL) return FALSE;

//		if (m_pVarCommStatus->GetType() != TYPE_BITENUM)	return FALSE;


		nVal = m_pVarCommStatus->m_pddbVar->getDispValue();
		nByteVal = 0x80; 

        // formating changes for better readability, POB - 15 Aug 2008
	for (nCount = 0; nCount < MAX_COMM_STATUS_BITS; nCount++)
	{
			if (nCount == 5 || nCount == 7 || nCount == 0) // Unused bits
			{
				nByteVal = nByteVal >> 1;
			continue;
			}

		m_pBtnArray_Commn[nCount] = new CBitmapButton();
		m_pBtnArray_Commn[nCount]->Create(_T("Enabled"), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,rectCommStatus1, this, 100);
		
		if (nVal & nByteVal)
		{
					byArrPreviousCommResp[nCount]=1;
			byArrPresentCommResp[nCount]=1;
				m_pBtnArray_Commn[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
			m_pBtnArray_Commn[nCount]->SizeToContent();
		}
		else
		{
					byArrPreviousCommResp[nCount]=0;
			byArrPresentCommResp[nCount]=0;
				m_pBtnArray_Commn[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
			m_pBtnArray_Commn[nCount]->SizeToContent();
		}


		m_pStaticArray_Commn[nCount] = new CStatic();
		m_pStaticArray_Commn[nCount]->Create(strCommStatusStrings[nCount], WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP, rectCommStatus2, this, 100);
			
		
		nByteVal = nByteVal >> 1;

		rectCommStatus1.top = rectCommStatus1.bottom + 10;
		rectCommStatus1.bottom = rectCommStatus1.top + 16;

		rectCommStatus2.top = rectCommStatus2.bottom + 10;
		rectCommStatus2.bottom = rectCommStatus2.top + 16;
	}
		bCommStatusDisplayed=TRUE;
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


BOOL CDevCommStatPropPage::OnUpdateDeviceStatus(unsigned char cDevStat) 
{
	int nByteVal = 0x80;
	int nVal=cDevStat;
	bool	bRefresh=false;		//Added by ANOOP 17FEB2004

	CRect rectDevStatus1(40,72,290,89);
	CRect rectDevStatus2(67,72,290,89);

	int nCount;  // PAW 03/03/09
	for (/*int PAW 03/03/09*/nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		if (nVal & nByteVal)
			byArrPresentDeviceResp[nCount]=1;
		else
			byArrPresentDeviceResp[nCount]=0;
		
		nByteVal = nByteVal >> 1;

	}

	for (nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		int nVal=byArrPresentDeviceResp[nCount];

		if(byArrPresentDeviceResp[nCount] != byArrPreviousDeviceResp[nCount] )
		{
			if (nVal)
			{
				byArrPreviousDeviceResp[nCount]=1;
				if(NULL != m_pBtnArray_DevStat[nCount])
				{
                    // formating changes for better readability, POB - 15 Aug 2008
				m_pBtnArray_DevStat[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
				m_pBtnArray_DevStat[nCount]->SizeToContent();
			}
				
			}
			else
			{
				byArrPreviousDeviceResp[nCount]=0;
				if(NULL != m_pBtnArray_DevStat[nCount])
				{
                    // formating changes for better readability, POB - 15 Aug 2008
				m_pBtnArray_DevStat[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
				m_pBtnArray_DevStat[nCount]->SizeToContent();
			}
			}
				
		}
		
		nByteVal = nByteVal >> 1;

		rectDevStatus1.top = rectDevStatus1.bottom + 10;
		rectDevStatus1.bottom = rectDevStatus1.top + 16;

		rectDevStatus2.top = rectDevStatus2.bottom + 10;
		rectDevStatus2.bottom = rectDevStatus2.top + 16;
		
	}

	/*<START>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
	if (true == bRefresh)
	{
		Invalidate();
		this->ShowWindow(SW_SHOW); 	
	}
	/*<END>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
		
	return TRUE;
}

BOOL CDevCommStatPropPage::OnUpdateCommrespStatus(unsigned char cCommStat) 
{
	int nByteVal = 0x80;
	int nVal=cCommStat;
	bool bRefresh=false;	//Added by ANOOP 17FEB2004


	CRect rectCommStatus1(40,72,290,89);
	CRect rectCommStatus2(67,72,290,89);

	int nCount; // PAW 03/03/09
	for (/*int PAW */nCount = 0; nCount < MAX_COMM_STATUS_BITS; nCount++)
	{
		//Modifed by ANOOP 17FEB2004
		if (nCount == 5 || nCount == 7 || nCount == 0) // Unused bits
		{
				nByteVal = nByteVal >> 1;
				continue;
		}

		if (nVal & nByteVal)
			byArrPresentCommResp[nCount]=1;
		else
			byArrPresentCommResp[nCount]=0;

		nByteVal = nByteVal >> 1;
	}

	for (nCount = 0; nCount < MAX_COMM_STATUS_BITS; nCount++)
	{
		//Modifed by ANOOP 17FEB2004
		if (nCount == 5 || nCount == 7 || nCount == 0) // Unused bits
				continue;
		
		int nVal=byArrPresentCommResp[nCount];

		if(byArrPresentCommResp[nCount] != byArrPreviousCommResp[nCount] )
		{
			bRefresh=true;		//Modifed by ANOOP 17FEB2004
			if (nVal )
			{
				if(NULL != m_pBtnArray_Commn[nCount])
				{
                    // formating changes for better readability, POB - 15 Aug 2008
				m_pBtnArray_Commn[nCount]->LoadBitmaps(IDB_BITMAP_02,0,0,0); 
				m_pBtnArray_Commn[nCount]->SizeToContent();
			}
				
			}
			else
			{
				if(NULL != m_pBtnArray_Commn[nCount])
				{
                    // formating changes for better readability, POB - 15 Aug 2008
				m_pBtnArray_Commn[nCount]->LoadBitmaps(IDB_BITMAP_01,0,0,0); 
				m_pBtnArray_Commn[nCount]->SizeToContent();
			}
				
			}
			byArrPreviousCommResp[nCount]=byArrPresentCommResp[nCount];
					
		}

				
		rectCommStatus1.top = rectCommStatus1.bottom + 10;
		rectCommStatus1.bottom = rectCommStatus1.top + 16;

		rectCommStatus2.top = rectCommStatus2.bottom + 10;
		rectCommStatus2.bottom = rectCommStatus2.top + 16;
		
	}
	
	/*<START>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
	if (true == bRefresh)
	{
		Invalidate();
		this->ShowWindow(SW_SHOW); 	
	}
	/*<END>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	


	return TRUE;
}
#ifdef GE_BUILD
// 00.01.09 prevent clear of tree view after device status page loaded and config change
void CDevCommStatPropPage::OnOK()
{
	bDeviceStatusDisplayed=FALSE;
	CDialog::OnOK();
}
// 00.01.09 end
#endif

