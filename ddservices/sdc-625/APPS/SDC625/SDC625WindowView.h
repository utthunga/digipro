#if !defined(AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_)
#define AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SDC625WindowView.h : header file
//


#include "WindowMap.h"
#include "ddbDefs.h"
#include "SDC625drawTypes.h"
#include "DynControls.h"

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CSDC625WindowView : public CFormView,  CdrawWin
{
protected:
	CSDC625WindowView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSDC625WindowView)

	ulong mySymNum;		/*stevev 8/25/o4 expansion*/

// Form Data
public:
	//{{AFX_DATA(CSDC625WindowView)
	enum { IDD = IDD_SDC625_FORM };
	//}}AFX_DATA

// Attributes
public:
	CDDIdeDoc* GetDocument();

// Operations
public:
	void Initialize(ulong id);
	typedef map< UINT, CWindowMap > IdWindowMap;
	IdWindowMap m_WndCntrlMap;


	void layoutWindow(void);// sizes and locates

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDC625WindowView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
//	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSDC625WindowView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSDC625WindowView)
	afx_msg void OnDestroy();
	afx_msg void OnChangeEdit2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_)
