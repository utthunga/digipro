#if !defined(AFX_BITENUMDISPVALWRITEDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_)
#define AFX_BITENUMDISPVALWRITEDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitEnumDispValDlg.h : header file
//

#include "sdc625.h"
#include "SDC625Doc.h"

#define MAX_STAT_BITS 8

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValWriteDlg dialog

class CBitEnumDispValWriteDlg : public CDialog
{
// Construction
	CValueVarient		m_pVarval;
	CDDIdeDoc			*m_pDoc;
	CButton				*m_pBtnArray[MAX_STAT_BITS];
	CString				strStatusString[8];
	BYTE				byStatusValue[8];	//Added by ANOOP 06APR2004
	int					nBitCount;
	hCVar				*pVar;
public:
	CBitEnumDispValWriteDlg(CValueVarient tmpVar,hCVar *pVar,CDDIdeDoc *pDoc,CWnd* pParent = NULL);   // standard constructor
	~CBitEnumDispValWriteDlg();
// Dialog Data
	//{{AFX_DATA(CBitEnumDispValWriteDlg)
	enum { IDD = IDD_BITENUMVAL_WRITE_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitEnumDispValWriteDlg)
	protected:
	afx_msg void OnClose( );
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBitEnumDispValWriteDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITENUMDISPVALWRITEDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_)
