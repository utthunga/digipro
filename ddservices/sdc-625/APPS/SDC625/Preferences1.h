// Preferences.h: interface for the CPreferences class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PREFERENCES_H__7A68F9CC_65FD_4B4C_87F3_DD989FCAEC9A__INCLUDED_)
#define AFX_PREFERENCES_H__7A68F9CC_65FD_4B4C_87F3_DD989FCAEC9A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

enum {SearchPollAddr0 = 1, SearchFindFirst = 2, SearchPollAddrN = 3, SearchPollAddr15 = 4, 
		SearchPollAddr31 = 5, SearchPollAddr63 = 6, SearchPollAddrRange = 7};


class CPreferences  
{
	void WriteProfileInteger(TCHAR* key, int newVal);
	int  GetProfileInteger(TCHAR* key, int defVal);

public:
	CPreferences();
	virtual ~CPreferences();

	// Mmodem Server Prefedrences
	CString SearchSerialPort;		// only "COM1" thru "COM4" supported by Modem Server
	CString SearchBy;			// "Poll Address", "Short Tag", "Long Tag"
	CString SearchTag;			// holds long tag or short tag, SearchOption attribute tells you which
	int SearchOption;			// enum value from list above
	int SearchPollAddr;			// beginning address for search
	int SearchPollAddrStart;	// beginning address for search
	int SearchPollAddrEnd;		// last address for search

	// other preferences go here

	void Get();					// get from ini filae
	void Write();				// write to ini file

};

#endif // !defined(AFX_PREFERENCES_H__7A68F9CC_65FD_4B4C_87F3_DD989FCAEC9A__INCLUDED_)

