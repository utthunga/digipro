#if !defined(AFX_TABLECHILDFRM_H__A5A668D4_A06D_4365_BBCE_F67D4A309572__INCLUDED_)
#define AFX_TABLECHILDFRM_H__A5A668D4_A06D_4365_BBCE_F67D4A309572__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Table_ChildFrm.h : header file
//
#include "StdAfx.h"
#include "UsefulSplitterWnd.h"
#include "Child_Frm.h"

#include "SDC625TreeView.h"
#include "SDC625ListView.h"

#include "logging.h"

#ifndef VIRTUALCALLERR
#define VIRTUALCALLERR  (2)
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrame frame

class CTableChildFrame : public CChildFrame
{
    DECLARE_DYNCREATE(CTableChildFrame)
    
    int ticksTillUpdate; // downcounter till mark 'em all stale
    int m_nTimerNumber;

    // debugging information
    bool running;
    bool inTimer;

protected:
    CTableChildFrame();           // protected constructor used by dynamic creation

// Attributes
public:
    CDDIdeTreeView* m_pTreeView;
    CDDIdeListView* m_pListView;
    // we now let the treeview keep track of each selection (its doing it anyway)
    CDDLBase * m_pRoot;     // CTableChildFrame owns the root (top menu) for each Table view, POB - 5/18/2014
    
    struct USERSELECT_S     // struct to remember menu selection after rebuilding the entire menu structure, POB - 5/20/2014
    {
        CString  selectedName;   // Name of Menu selected on the Tree View
        unsigned selectedID;     // ItemId of the selected Item
        unsigned parentID;       // ItemID of parent menu
    };  

    USERSELECT_S  m_UserSelect;  // Actual user selection information, POB - 5/20/2014

// Operations
public:
void      StaleDynamics(vector <hCitemBase*>& pVariablesToRead) ;
bool      setCurrentSelection(CDDLBase* pinBase,int idx);
HTREEITEM setCurrentSelection(USERSELECT_S  select, HTREEITEM hRoot = NULL);  // Modified to be recursive and to use USERSELECT_S struct, POB - 5/20/2014
bool      setSelection2parent(void);// true on failure
bool      setSelectionDefault(void);
CDDLBase *getCurrentSelection(void);
// Implementation
public:
    /*>*>*> was here <*<*<*/  

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTableChildFrame)
protected:
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
    //}}AFX_VIRTUAL
    LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam); 
    
protected:
    virtual ~CTableChildFrame();

#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif
protected:
    CSplitterWnd        m_wndSplitter;

    // Generated message map functions
    //{{AFX_MSG(CTableChildFrame)
    afx_msg void OnTimer(UINT nIDEvent);/* added 07apr11 */
    afx_msg void OnClose();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/* was located at *>*>*> was here <*<*<*  confuses the colorizer...
#ifdef LOGON_RGT_PANE
    CUsefulSplitterWnd  m_wndSplitter_RightPane;
    BOOL                m_bRightPaneSplitterCreated; 
#endif  **/

// no longer used   afx_msg void OnTimer(UINT nIDEvent);
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLECHILDFRM_H__A5A668D4_A06D_4365_BBCE_F67D4A309572__INCLUDED_)
