// Child_Frm.h : interface of the CChildFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDFRM_H__A887A4EC_EEDF_4FC6_83B8_0584FD1943B0__INCLUDED_)
#define AFX_CHILDFRM_H__A887A4EC_EEDF_4FC6_83B8_0584FD1943B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "StdAfx.h"
#include "logging.h"

#ifndef _WIN32_WCE  /* PAW 21/05/09 */
#define FRAME_WND	CMDIChildWnd
#else
#define FRAME_WND	CFrameWnd
#endif


class CChildFrame : public FRAME_WND
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attributes
public:
	BOOL IsOK;	// Opened without incident

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~!");} return FRAME_WND::WindowProc(message,wParam,lParam);};
#endif

// Implementation
public:
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE		/* PAW 21/05/09 */
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CChildFrame)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDFRM_H__A887A4EC_EEDF_4FC6_83B8_0584FD1943B0__INCLUDED_)
