/*************************************************************************************************
 *
 * $Workfile: MainFrm.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface of the CMainFrame class
 * #include "MainFrm.h"		
 */

#if !defined(AFX_MAINFRM_H__9B8234E0_B0EB_4CAD_BD9E_90ADC14D31C7__INCLUDED_)
#define AFX_MAINFRM_H__9B8234E0_B0EB_4CAD_BD9E_90ADC14D31C7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Wait_Dlg.h"	// Added by ClassView
#include "StdAfx.h"

#include "Command48_PropSheet.h" //Added by ANOOP 
#include "Table_ChildFrm.h"
#include "Window_ChildFrm.h" 	

class sdCDialog;

#include < basetsd.h >
#include < vector >

#include "logging.h"

/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/
#define DEFAULTUPDTSECS     15 /* normal update: 3 */
#ifndef UPDATESECONDS	
extern  unsigned __updateSeconds;
#define UPDATESECONDS       __updateSeconds
#endif
#define CYCLES2FORCEREAD	2	/* number of UPDATESECONDS till a command is forced for 'stay-alive'*/
#define ENTRIES2AUTOLOAD	3 /*sjv04dec06-this was ~40 secs::20*/  /* number of UPDATESECONDS till we start updating all vars */

#define UPDATEPERIOD   (ONE_SECOND * UPDATESECONDS) /* may need to change to a get-pref call */

/* stevev - 2/17/04 - configuration changed state of device (the mainframe knows the most about it */
/* 4/26/04 - added states to handle queued commands sent between write and command 38 */
enum frameState
{
	FS_ISIDLE,
	FS_ISCOMMITTING,	// added states stevev 4/26/04
	FS_CFIXING,			// fixing bit from committing
	FS_UFIXING			// fixing bit from unexpected
};
/* stevev - 2/17/04 - end*/
class CUsefulSplitterWnd;	//Added by ANOOP 21FEB2004
class CCommand48_PropSheet;	//Added by ANOOP

class CdrawMain; // in lieu of including sdc625_drawtypes.h

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
//protected: // create from serialization only

	int ticksTillUpdate; // downcounter till mark 'em all stale
	int subpubChngCounter; // keep track of subscription Changes

	bool treeRedrawing;		// normally true
public:
	CMainFrame();
//	DECLARE_DYNCREATE(CMainFrame)
BOOL OnIdle(LONG lCount);

// Attributes
public:
  //prashant: moved to public from protected
  CDDIdeDoc *	m_pDoc;
  enum frameState m_MainFrameState;	//added by stevev 2/17/04 - config changed handling

  //Added by ANOOP 
	CCommand48_PropSheet *ptrCmdDlg;
	bool				 bCMD48DlgExtendedTabCreatedOnce; 	//Added by ANOOP for ensuring extended tasb appear in the startup after being closed once 
	CMDIChildWnd* pErrorFrame;
    // Aded by POB - 11 Aug 2004
//	void generateDevice(void); // common to browse and oninitialupdate
								//made public to generate from thr app
// Operations
	BOOL GetExtendedDeviceStatusInfo(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_Array,int &); //Added by ANOOP
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
     virtual void BeginModalState();  // stevev 3/17/04
     virtual void EndModalState();    // stevev 3/17/04
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

     void MyBeginModalState();  // stevev 3/17/04
     void MyEndModalState();	// stevev 3/17/04


// Implementation
public:
// now its really a message handler 05aug05	void OnCloseDevice();    // POB, 8 Oct
	BOOL CreateWindowMenu(); // POB, 10 Sep 2004
	void ResetWindowMenu(void);// sjv 08aug05 
	void BusyDisplay(BOOL state = TRUE);
	virtual ~CMainFrame();
	/*Vibhor 130603 : Start of Code*/
	CImageList *m_pImageList_Enabled,*m_pImageList_Disabled ; 
	/*Vibhor 130603 : End of Code*/
// 17mar11-stevev- the main frame has no busness knowing this information....
// 17mar11	CTableChildFrame* pTableChildFrm; // Pointer to Root Menu Table, POB, 2 Aug 2004

// temporary debug 26may11
CTableChildFrame* pTableChildFrm;
// end temporary
	CMenu m_ViewMenu; // Added by POB, 10 Oct 2004 - All child windows will use this as their common "View" menu


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	
	void SetTransmitterLight(bool bColorRed);  //Added by ANOOP - kinda needs to be public sjv
	RETURNCODE ConfigChanged(void);	// added by stevev - 2/17/04 - event callback
	RETURNCODE ConfigUNChanged(void); // added by stevev 4/26/04 to keep it changed till done
	bool CloseErrorLog();	//Added by ANOOP 03FEB2004 Added for cleaning up the error log window when new device is clicked	*/
	BOOL GetDocument();
	LRESULT /*void PAW 03/03/09 */SizeChildFrame(WPARAM wParam, LPARAM lParam);// parameters added PAW 03/03/09  // Added by POB, 3 Aug 2004
	void CloseDevice(void) {OnCloseDevice(0,0);};
	void UpdateBar(TCHAR* newMsg);

	int DeRegisterDialog(/*hold 26oct sd*/ sdCDialog* pDialog);
	int RegisterDialog(/*hold 26oct   sd*/ sdCDialog* pDialog);

	void ReadVars(void) 
	{ 
		m_icnt = 0; 
	};

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
//  CSplitterWnd   m_wndSplitter; - deleted by POB, 2 Aug 2004
//	CUsefulSplitterWnd	m_wndSplitter_RightPane; //Added by ANOOP  - deleted by POB, 2 Aug 2004
//	BOOL	m_bRightPaneSplitterCreated;	//Added by ANOOP 	- deleted by POB, 2 Aug 2004
	//CDDIdeDoc *	m_pDoc;
	int missingDevice;
	int m_icnt;

	//prashant
	CString m_szLit18File;

	/* stevev 3/17/04 for modality count*/
	HWND* m_phMyWndDisable;
	 
	
/* sjv - made public	void SetTransmitterLight(BOOL bSetColor);  *///Added by ANOOP

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnDeviceHelp();
	afx_msg void OnViewErrorlog();
	afx_msg void OnViewMorestatus();
	afx_msg void OnUpdateViewMorestatus(CCmdUI* pCmdUI);
	afx_msg void OnViewCommLog();
	afx_msg void OnUpdateViewCommLog(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeviceHelp(CCmdUI* pCmdUI);
	afx_msg void OnDevHelp();
	afx_msg void OnUpdateDevHelp(CCmdUI* pCmdUI);
	afx_msg void OnCommit();
	afx_msg void OnUpdateCommitOrAbort(CCmdUI* pCmdUI); //Vibhor 230205: Modified See more comments in cpp
	afx_msg void OnAbort();
	afx_msg void OnMfgrlibrary();  //Added by ANOOP
	afx_msg void OnRange(UINT uint);// void parameter added PAW 03/03/09 // Add by POB 14 Sep 2004
	afx_msg LRESULT /*void PAW 03/03/09*/ OnNotifyRespCode(WPARAM wParam, LPARAM lParam);// parameters added PAW 03/03/09 
	afx_msg void OnEnable(BOOL bEnable);
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg LRESULT OnDestroyCMD48Dialog(WPARAM wParam, LPARAM lParam);
//	afx_msg void OnGenDevice(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT /*void PAW 03/03/09*/ OnWindow(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT /*void PAW*/OnExeLink(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseDevice(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGenDevice(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
		
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
//	BOOL GetDocument(); // POB, 3 Aug 
	void fixConfigChangeBit(void);// added by stevev - 2/17/04 - helper function

	void doExecuteVariable(hCVar* pV,UINT32 wParam);

	/* helper functions */
	bool	setDrawState(bool ds_ON);// returns previous state
	void	updateAll(long UpdateType, WPARAM wParam);
	void	updateDialogs(UINT message, WPARAM wParam, LPARAM lParam);

private:
	CWaitDlg m_Modeless; // Added by POB - 4 Aug 2004
	bool     isBusy;     // Added by stevev 07oct09

	//void * m_pdrawMain;  // Added by POB - 20 Sep 2004: TEMPORARY void pointer - need to make it CdrawMain
	CdrawMain* m_pdrawMain; // stevev 09nov06
	vector</*hold 26oct  sd*/ sdCDialog*> dlgRegistry;
	typedef vector</*hold 26oct  sd*/ sdCDialog*>::iterator dlgRegIT;
	
	static hCmutex   dlgRegistryMutex;// protect the registry for re-entrancy
};

#ifdef _DEBUG
#define REG_MUTEX_NAME  "MnFrmRegistry"
#else
#define REG_MUTEX_NAME
#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__9B8234E0_B0EB_4CAD_BD9E_90ADC14D31C7__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: MainFrm.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
