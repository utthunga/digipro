// ErrLogView.h: interface for the CErrLogView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRLOGVIEW_H__25948AA3_3EDF_4281_A0CE_F1AC8E05E32F__INCLUDED_)
#define AFX_ERRLOGVIEW_H__25948AA3_3EDF_4281_A0CE_F1AC8E05E32F__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class CErrLogView : public CListView  
{
	int timerID;

protected:
	CErrLogView();
	DECLARE_DYNCREATE(CErrLogView)
	virtual ~CErrLogView();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSdcView)
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL
public:
	int CreateTimer();
/*<START>Added by ANOOP */	
	void CErrLogView::DisplayLogMessage();
	void EmptyListCtrl();	//Added by ANOOP 03FEB2004
	int m_nMessagesShown,m_nMesgShown;
	BYTE m_byInitialized;
/*<END>Added by ANOOP */	

	// Generated message map functions
protected:
	CListCtrl * m_pList;
	//{{AFX_MSG(CDDIdeListView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif // !defined(AFX_ERRLOGVIEW_H__25948AA3_3EDF_4281_A0CE_F1AC8E05E32F__INCLUDED_)
