/**********************************************************************************************
 *
 * $Workfile: DDLMethod.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		interface for the CDDLMethod class
 * #include "DDLMethod.h".		
 */
#if !defined(AFX_DDLMETHOD_H__0B427211_1ED7_46D5_B14B_08440ECE1F35__INCLUDED_)
#define AFX_DDLMETHOD_H__0B427211_1ED7_46D5_B14B_08440ECE1F35__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "DDLBase.h"
//#include "ddbMethod.h"
//#include "MethodSupport.h"
#include "ddbDevice.h"
#include "DDLMenu.h"

class CMethodSupport;
class CDDLMethod : public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLMethod)
	hCddbDevice* pDev;

public:
	CDDLMethod(hCitemBase* pIB=NULL);
	/* stevev 26sep08 - this is the wrong way to do this...method support should not be used
	CDDLMethod(hCmethodCall& MethodCall,CMethodSupport* pMethSupport,hCitemBase* pIB=NULL);	
	CDDLMethod(hCmethodCall& MethodCall,CMethodSupport* pMethSupport,itemInfo_t& pIi);
	    do it thru the device                                                           ****/
	CDDLMethod(hCmethodCall& MethodCall,hCddbDevice* pDSP,hCitemBase* pIB=NULL);	
	CDDLMethod(hCmethodCall& MethodCall,hCddbDevice* pDSP,itemInfo_t& pIi);
	virtual ~CDDLMethod();

// Special menu handling
	//Vibhor 1405 : Start of Code
	void Execute(CString szMethodName,CString szHelp);//Modified Function Signature
	//Vibhor 1405 : End of Code
    void LoadContextMenu(CMenu *pMenu, int& nPos);



	//hCmethod*   m_pddbMeth;
		hCmethodCall m_MethodCall;
	//prashant oct 2003.
	CMethodSupport* m_pMethSupport;

// Overrides
    BOOL IsMethod();

};

#endif // !defined(AFX_DDLMETHOD_H__0B427211_1ED7_46D5_B14B_08440ECE1F35__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLMethod.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
