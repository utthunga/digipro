/*************************************************************************************************
 *
 * $Workfile: DDLBase.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      
 * #include "DDLBase.h".        
 */
#if !defined(AFX_DDLBASE_H__20C9AACC_3511_4455_9120_862391D0661F__INCLUDED_)
#define AFX_DDLBASE_H__20C9AACC_3511_4455_9120_862391D0661F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DDLBase.h : header file
//

#include "ddbItemBase.h"

class CDDLMenuItem; // avoid circular include of DDLMenuItem.h
class CDDLMenu;

/***************************************************************************
 * Global Definitions
 ***************************************************************************/

/*
 * Image Identification Numbers.  These map to the bitmap positions.
 */
enum
{
    IMG_MENU     = 0,
    IMG_VARIABLE,       //1
    IMG_EDITDISPLAY,    //2
    IMG_METHOD,         //3
    IMG_IMAGE    = 8,   // temp value
    IMG_GRAPH    = 9,   // Changed IMG_SCOPE to IMG_GRAPH, POB - 5/16/2014
    IMG_CHART,          // 10, Added image for Chart, POB - 5/16/2014
    IMG_GRID     = 12,
    // removed IMG_READONLY_VARIABLE, POB - 5/16/2014
    IMG_STRING   = 14,  // was 16, POB - 5/16/2014
    IMG_BLOB     = 15,  // temp value
};

/*
 * Popup Identification Numbers.  These map to the Menu positions.
 */
enum
{
    POPUP_MAIN     = 0,  
    POPUP_MENU,
    POPUP_VARIABLE,
    POPUP_EDITDISPLAY,
    POPUP_WRITEASONE,
    POPUP_METHOD,
    POPUP_WNDMAIN = 6,
    POPUP_WNDVAR,
    POPUP_WNDENUM,
    POPUP_ITEM,
    POPUP_STRING,
};

#define NO_QUALIFIER    0x000000
#define READ_WRITE      0x000000
#define READ_ONLY       0x000001
#define DISPLAY_VALUE   0x000010
#define REVIEW          0x000100

#define _NO_LABEL       0x001000
#define _NO_UNIT        0x010000
#define _INLINE         0x100000    

#define OVERLINE        0x80000000      /* previous item was a separator */
#define ISREMOTE        0x40000000      /* no value no unit - will popup a dialog to display
                                           contents -image, chart,grid,graph,const-string    */
/***************************************************************************
 * Global Class Definitions
 ***************************************************************************/

/*
 * Forward declaration of the doc to avoid a circular dependency
 */
class CDDIdeDoc;
class CDDLBase;
typedef CTypedPtrList<CObList, CDDLBase*> CDDLBasePtrList_t;

/////////////////////////////////////////////////////////////////////////////
// CDDLBase window

class CDDLBase : public CObject
{
    DECLARE_DYNAMIC(CDDLBase)

// Construction
public:
    CDDLBase(hCitemBase*  pIB = NULL);
    CDDLBase(hCreference* pIR);
    CDDLBase(const CDDLBase & Base); // Copy Constructor

    virtual ~CDDLBase();

    bool isValid();

// Operations
public:
// Tree and List Control
    // Removed InsertChild( CDDLBase * pHdw, CDDIdeDoc* pDoc ), POB - 5/22/2014
    bool AddChild( CDDLBase * pChild );// true@success(doesn't exist)
    void RemoveNdeleteChild( CDDLBase * pChild );
    void DeleteList(void);
    int  deleteLeaf(ulong targetID);// returns number deleted or negative if error

// cast operators
    virtual operator CString(); // used to get a value from an object (VARABLES only)

// Special menu handling
    virtual void Execute();
    virtual void LoadContextMenu(CMenu *pMenu, int& nPos);
    virtual void OnUpdateMenuExecute(CCmdUI* pCmdUI);
    virtual void DisplayValue();
    void DisplayLabel();
    void DisplayHelp();

// Access Functions
    virtual BOOL IsMethod();
    virtual BOOL IsVariable();
    virtual BOOL IsMenu();

    virtual BOOL IsDisplayValue();  // Value is set for DISPLAY_VALUE
    virtual BOOL IsReadOnly();
    virtual BOOL IsReview();        // only difference about REVIEW menu is all items
                                    // VARIABLEs and all are DISPLAY_VALUE  
    virtual BOOL IsEditDisplay();  /*Vibhor 170304: added*/
    virtual BOOL IsValid();

    virtual BOOL IsExpanded();     // for the expansion classes
    virtual BOOL IsTop() { return FALSE; };// override in DDLMain

    virtual void SetValidity(BOOL valid);

    virtual CString GetRelation();

    virtual CString GetHelp();
    virtual void SetHelp( CString strHelp);

    virtual CString GetName();
    virtual CString GetCurrentName(CDDLBase* pItem);// stevev added 02dec05
    virtual bool    GetNameFull(){return(m_strNmValid);};
    virtual void SetName( CString strName );

    virtual CString GetUnitStr();
    virtual CDDLBasePtrList_t&       GetChildList();
    virtual void                     UpdateChildList(void);
    virtual CDDLBase* GetParent();
    virtual void SetParent( CDDLBase* pParent );
    virtual HTREEITEM GetTreeItem();
    virtual void SetTreeItem(HTREEITEM hTreeItem);
    virtual void SetID(unsigned long    lItemId);
    virtual unsigned long GetID();
    virtual CDDLMenu* getMenuPtr(void) { return NULL; };// menus and menuitems will override
    virtual CDDLBase* FindValidBranch(); // Added to aid in menu selection, POB - 5/22/2014

// Attributes
// These data fields are not serialized and must be reconstructed
public:
    virtual void Execute(CString strName,CString strHelp);
    virtual int GetImage();  // Added to access image id, POB - 5/16/2014
    //int m_nImage;           // image id number
//stevev 21mar05 - add a real link to the device object
    hCitemBase*  m_pItemBase;
    hCreference* m_pReference;
    unsigned     m_IndexNumber; // -1 if unused, usually used when m_pItemBase is an array
                                // or a bit
    unsigned long      m_lItemId;/* VMKP added on 210104 *///stevev 15may13 make public 4 debug
protected:
    int        m_nImage;      // image id number, move to protected, POB - 5/16/2014
    HTREEITEM  m_hTreeItem;   // value returned when this was put into tree
    BOOL       m_bIsValid;
    CDDLBase * m_pParent;     // Points to CDDLBase derived class that owns this
    CString    m_strName;     // Name used in the tree list and for sorting
    bool       m_strNmValid;  // true if strName correct (even if empty)
    CString    m_strHelp;
    bool       m_classExpanded;
//Vibhor 1405 : Start of Code
    CString    m_strMenuSourceName; //Menu Source Name
//Vibhor 1405 : End of Code
private:
    //CTypedPtrList<CObList, CDDLBase*> m_ChildList;         // List of children owned by this
    CDDLBasePtrList_t m_ChildList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDLBASE_H__20C9AACC_3511_4455_9120_862391D0661F__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLBase.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
