#if !defined(AFX_BITENUM_H__F0EDB1AE_D69F_41B5_A5A1_1DF2ECBC5C4D__INCLUDED_)
#define AFX_BITENUM_H__F0EDB1AE_D69F_41B5_A5A1_1DF2ECBC5C4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Bit_Enum.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBitEnum window

class CBitEnum : public CButton
{
// Construction
public:
	CBitEnum();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitEnum)
	protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL Toggle();
	void EnableButton(BOOL state = TRUE);
	BOOL IsOn();
	void SetOn(BOOL state = TRUE);
	virtual ~CBitEnum();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBitEnum)
	afx_msg void OnClicked();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	COLORREF GetColor(double dAngle, COLORREF crBright, COLORREF crDark);
	void DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crColour, BOOL bDashed = FALSE);
	void DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crBright, COLORREF crDark);
	BOOL m_bIsOn;
//	BOOL m_bIsEnabled;
	BOOL m_dsplState;
	COLORREF m_color;
	COLORREF m_colorOn;
	COLORREF m_colorOff;
	CRgn   m_rgn;
	CPoint m_ptCentre;
	int    m_nRadius;
	BOOL   m_bDrawDashedFocusCircle;
	static const COLORREF m_CLOUDBLUE;
	static const COLORREF m_WHITE;
	static const COLORREF m_BLACK;
	static const COLORREF m_DKGRAY;
	static const COLORREF m_LTGRAY;
	static const COLORREF m_YELLOW;
	static const COLORREF m_DKYELLOW;
	static const COLORREF m_RED;
	static const COLORREF m_DKRED;
	static const COLORREF m_BLUE;
	static const COLORREF m_DKBLUE;
	static const COLORREF m_CYAN;
	static const COLORREF m_DKCYAN;
	static const COLORREF m_GREEN;
	static const COLORREF m_DKGREEN;
	static const COLORREF m_MAGENTA;
	static const COLORREF m_DKMAGENTA;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITENUM_H__F0EDB1AE_D69F_41B5_A5A1_1DF2ECBC5C4D__INCLUDED_)
