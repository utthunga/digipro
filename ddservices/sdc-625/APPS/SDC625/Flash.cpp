// Flash.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "Flash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFlash dialog


#define ID_EDIT	140



CFlash::CFlash(CWnd* pParent /*=NULL*/)
	: CDialog(CFlash::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFlash)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bkBrush.CreateSolidBrush(RGB(255,255,255));
	m_StaticBrush.CreateSolidBrush(RGB(0,0,175));
	m_EditBrush.CreateSolidBrush(RGB(255,255,255));


}


void CFlash::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFlash)
	DDX_Control(pDX, IDC_HEADER, m_Head);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFlash, CDialog)
	//{{AFX_MSG_MAP(CFlash)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFlash message handlers

BOOL CFlash::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	MoveWindow(20,70,190,80);
//	CenterWindow();
	
/*
	static_txt = new CStatic;
	static_txt->Create(
						_T("SDC625"),
						WS_CHILD | WS_VISIBLE ,
						CRect(0, 0, 150, 30), 
						this,
						2);
	
*/


	 
	 pEdit = new CEdit;
	 pEdit->Create(
					ES_MULTILINE | WS_CHILD | WS_VISIBLE | WS_TABSTOP 
					,CRect(10, 30, 170, 100)
					,this 
					,ID_EDIT
				 );
	 
/*	CFont font;
	font.CreateFont(
	   12,                        // nHeight
	   5,                         // nWidth
	   0,                         // nEscapement
	   0,                         // nOrientation
	   FW_BOLD ,                 // nWeight
	   FALSE,                     // bItalic
	   FALSE,                     // bUnderline
	   0,                         // cStrikeOut
	   ANSI_CHARSET,              // nCharSet
	   OUT_DEFAULT_PRECIS,        // nOutPrecision
	   CLIP_DEFAULT_PRECIS,       // nClipPrecision
	   DEFAULT_QUALITY,           // nQuality
	   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
	   _T("Arial"));

	pEdit->SetFont(&font);*/

//	pEdit->CenterWindow();
	pEdit->EnableWindow(FALSE);

	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH CFlash::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr ;//= CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	// TODO: Change any attributes of the DC here
	switch(nCtlColor)
	{


		case CTLCOLOR_EDIT:
		{
			switch (pWnd->GetDlgCtrlID())
			{     
				// first CEdit control ID
				case ID_EDIT:  
					pDC->SetBkMode(TRANSPARENT);
					
					pDC->SetTextColor(RGB(255,0,0));
					hbr = (HBRUSH) m_EditBrush; 
 				break;

				default:
					hbr=CDialog::OnCtlColor(pDC,pWnd,nCtlColor); 
				break;

			}
		
		}
		break;


		case CTLCOLOR_STATIC:
		{
			// process my edit controls by ID
			switch (pWnd->GetDlgCtrlID())
			{     
				// first CEdit control ID
				case IDC_HEADER:  
					pDC->SetBkMode(TRANSPARENT);
					pDC->SetBkColor(RGB(0,0,250));
					pDC->SetTextColor(RGB(255,255,255));
					hbr = (HBRUSH) m_StaticBrush; 
					
 				break;


			}
		}
		break;
		default:
			hbr = (HBRUSH) m_bkBrush;
		break;

	}


	return hbr;
}
