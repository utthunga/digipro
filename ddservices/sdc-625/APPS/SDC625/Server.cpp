// Server.cpp : implementation file
//

#include "stdafx.h"
#include "Server.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServer

CServer::CServer()
{
	m_bConnected = FALSE;
	pConnected = new CSocket();
	m_nBytesBufferSize = 0;
	m_nBytesSent = 0;
}

CServer::~CServer()
{
	if (pConnected)
		delete pConnected;
}

void CServer::Createnew()
{
	AfxSocketInit();
	if (!Create(10, SOCK_STREAM))
	{
		TRACE0("\nSocket creation failed");
	}

	int z = Listen(5);
	if (z == 0)
	{
		TRACE0("\n Listen Failed");
		int n = GetLastError();
		TRACE1("Error: %d", n);

	}
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CServer, CSocket)
	//{{AFX_MSG_MAP(CServer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CServer member functions
void CServer::OnConnect(int nErrorCode) 
{

	CSocket::OnConnect(nErrorCode);
}
void CServer::SendMessage(char *strMessage,int nLength)
{
	if (m_bConnected)
	{
		m_sendBuffer = strMessage;
		m_nBytesBufferSize = nLength;
		m_nBytesSent = 0;

		int x = pConnected->Send(strMessage,nLength);
		int z = GetLastError();
	}
}

void CServer::OnAccept(int nErrorCode) 
{
	if (pConnected)
	{
		delete pConnected;
		pConnected = new CSocket();
	}

	m_bConnected = Accept(*pConnected);
	int x = GetLastError();

	CSocket::OnAccept(nErrorCode);
}

