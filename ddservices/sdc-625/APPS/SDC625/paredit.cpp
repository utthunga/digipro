// paredit.cpp : implementation file
//
#include "stdafx.h"
#include "paredit.h"

/////////////////////////////////////////////////////////////////////////////
// ParsedEdit

CParsedEdit::CParsedEdit()
{
	m_wParseStyle = 0;
	nLeft_of_decimal=0;
}

BEGIN_MESSAGE_MAP(CParsedEdit, CEdit)
	//{{AFX_MSG_MAP(CParsedEdit)
	ON_WM_CHAR()
// Deleted commented out code - POB, 15 Aug 2008
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Creating from C++ code

/////////////////////////////////////////////////////////////////////////////
// Aliasing on top of an existing Edit control

BOOL CParsedEdit::SubclassEdit(UINT nID, CWnd* pParent, WORD wParseStyle,int nMax_Chrs,int nDecimal_Lt,int nChar_cnt)
{
	m_wParseStyle = wParseStyle;
	nMax_Chars=nMax_Chrs;
	nDecimal_Limit=nDecimal_Lt;
	nLeft_of_decimal=nChar_cnt;
	return SubclassDlgItem(nID, pParent);
}

/////////////////////////////////////////////////////////////////////////////
// Input character filter

void CParsedEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	char *pdest=NULL;
	CString szText="",szDisp_val_old="",szDisp_val_new="";
	int nFirstPos = 0,nLastPos = 0;
	BOOL bChar=FALSE;
	bool makeNoise = false;
	
	GetWindowText(szText);
	szDisp_val_old=szText;

	if ( TRUE == szText.IsEmpty() )
	{
		SetLimitText(nMax_Chars);
	}

// VMKP Modified on 200104
	
	if( m_wParseStyle == PES_ALL )
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
	}
	else
	{
		if( m_wParseStyle == PES_NUM_UNSIGNEDINT ) 
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 )
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		else if( m_wParseStyle == PES_NUM_INT )
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 || nChar == 0x2D )
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		// stevev 3oct07
		else if( m_wParseStyle == PES_NUM_HEX )
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 || 
				( nChar >= 0x41 && nChar <= 0x46 ) || ( nChar >= 0x61 && nChar <= 0x66 )
				|| nChar == 0x58 ||  nChar == 0x78 )
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		else if ( m_wParseStyle == PES_NUM_FLOAT)
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || 
				  nChar == 0x2E ||  nChar == 0x08 || nChar == 0x2D || nChar == 0x2B 
					|| nChar == 0x45 || nChar == 0x65 )// added eE+ 28nov11
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		else if ( m_wParseStyle == PES_DATE) /*Added by ANOOP */
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x2F || nChar == 0x08 )
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		else if ( m_wParseStyle == PES_TIME_FMT) /*Added by ANOOP */
		{
			if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x3A /* : */ || 
				  nChar == 0x41 /* A */ || nChar == 0x50 /* P */ || nChar == 0x4D /* M */ ||
				  nChar == 0x08 /* BS */|| 
                  nChar == 0x61 /* a */ || nChar == 0x70 /* p */ || nChar == 0x6D /* m */ ||
                  nChar == 0x20 /*(space)*/ )
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}
		else if ( m_wParseStyle == PES_24TIME_FMT) /*Added by stevev - 24 hr format */
		{
			if( (nChar >= 0x30 && nChar <= 0x39) || nChar == 0x3A /*:*/|| nChar == 0x08 /*BS*/)
			{
				CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
			}
			else
				makeNoise = true;
		}		
	}
	if (makeNoise)
		//CEdit::OnChar(0x07, 1, nFlags);  
		//Beep(750,300);
		MessageBeep(0);


	GetWindowText(szText);
	szDisp_val_new=szText;	

	if(m_wParseStyle == PES_NUM_FLOAT)
	{
		if(FALSE == szText.IsEmpty() && szText.Find(_T('.')) >= 0)
		{
            // Deleted commented out code - POB, 15 Aug 2008
			int nRight_dec = decimalsRight(szText);

			if ( ( nRight_dec > nDecimal_Limit) && nDecimal_Limit >= 0 )// stevev 13sep10 -1 for no limit
			{
				SetWindowText(szDisp_val_old);
				// Don't allow cursor to go to the first character position,
				//if the values decimal position count equal to the display string 
				//decimal portion value 
				UINT tmpLen = szDisp_val_old.GetLength();
				this->SetSel(tmpLen,tmpLen,FALSE);
				CEdit::OnChar(0x07, 1, nFlags);				
			}
			return;			
		}
	}

    // Deleted commented out code - POB, 15 Aug 2008

	if ( m_wParseStyle == PES_ALL )
	{
		if(FALSE == szText.IsEmpty())
		{
			SetLimitText(nMax_Chars);
		}
	}
	else
	{
		if (  (IsCharAlphaNumeric((TCHAR)nChar) && !IsCharAlpha((TCHAR)nChar)) )
		{// it's numeric
			if(FALSE == szText.IsEmpty() && szText.Find(_T('.')) >= 0)
			{// has a dec pt already
				int len = decimalsRight(szText);
					
				GetSel(nFirstPos,nLastPos);// get location of currently selected text
				// stevev24jun08 - do not limit at --zero-- negative decimal places (that's the default)
				if ( ( len == nDecimal_Limit && nDecimal_Limit > 0)  && ( szText.GetLength() < nMax_Chars ))
				{// limit any more text input
					SetLimitText(szText.GetLength());
				}
			}
		}	
	}
}


