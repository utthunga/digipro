//prashant

/*******************************************************************************************************
This file gives the data structures and constants used in methods user interface
*******************************************************************************************************/
/*The types of data that will need to be shown by the UI*/
enum UI_DATA_TYPE
{
	UNKNOWN_UI_DATA_TYPE = 0
	, TEXT_MESSAGE
	, COMBO
	//, DATETIME //commented temporarily
};


/*This class  represents the string message to be displayed*/
class UI_DATA_TYPE_TEXT_MESSAGE
{
public:
	int		iTextMessageLength;
	char	*pchTextMessage;

	UI_DATA_TYPE_TEXT_MESSAGE():iTextMessageLength(0), pchTextMessage(NULL)
	{
	}

	~UI_DATA_TYPE_TEXT_MESSAGE()
	{
		
	}
};

/*The edit box type .see class UI_DATA_EDIT_BOX*/
enum EDIT_BOX_TYPE
{
	UNKNOWN_EDIT_BOX_TYPE
	, EDIT_BOX_TYPE_INTEGER
	, EDIT_BOX_TYPE_FLOAT
	, EDIT_BOX_TYPE_STRING
};

/*This class  represents the edit box type and the value to be displayed and other related info */
class UI_DATA_EDIT_BOX
{
public:
	EDIT_BOX_TYPE		editBoxType;
	int		iValue, iMinValue, iMaxValue;
	float	fValue, fMinValue, fMaxValue;
	int		iDefaultStringLength, iMaxStringLength;
	char	*pchDefaultValue;

	UI_DATA_EDIT_BOX()
	{
	}

	~UI_DATA_EDIT_BOX()
	{
	}
};


/*This class  represents the combo box type and the value to be displayed and other related info */
class UI_DATA_TYPE_COMBO
{
public:
	int		iNumberOfComboElements;
	char	**pchComboElementText; //This will be a list of options seperated by semi colon
									//seperated by semi-colon

	UI_DATA_TYPE_COMBO()
	{
	}

	~UI_DATA_TYPE_COMBO()
	{
	}
};

//temporarily commenteed
/*This class  represents the date time type and the value to be displayed and other related info */
/*class UI_DATA_TYPE_DATETIME
{
public:
	Time_t	DateTime;
};*/

/*This is the structure used to send the display info and values from the builtin(device object)
 to the SDC methods UI.
*/
struct ACTION_UI_DATA
{
	UI_DATA_TYPE userInterfaceDataType;

	UI_DATA_TYPE_TEXT_MESSAGE	textMessage;
	UI_DATA_TYPE_COMBO			combo;
//	UI_DATA_TYPE_DATETIME		datetime; //temporarily commented
	BOOL						bUserAcknowledge;
/*Vibhor 030304: Start of Code*/
	BOOL						bEnableAbortOnly; // There are certain cases where we need just the abort button enabled
/*Vibhor 030304: End of Code*/
/*Vibhor 040304: Start of Code*/
	unsigned	uDelayTime;  // in MilliSecs.
/*Vibhor 040304: End of Code*/


};

/*This is the structure used to send the user input from the SDC methods UI to the device object
(and then to the builtins)*/
struct ACTION_USER_INPUT_DATA
{
	UI_DATA_TYPE				userInterfaceDataType;
				 
	UI_DATA_EDIT_BOX			editbox;
//	UI_DATA_TYPE_DATETIME		datetime; //temporarily commented
	int							nComboSelection;

};



//offsets for the  rich edit text display control w.r.t the user client area
#define RICHEDIT_CTRL_TOP_OFFSET                        2
#define RICHEDIT_CTRL_LEFT_OFFSET                       20
#define RICHEDIT_CTRL_RIGHT_OFFSET                      2
#define RICHEDIT_CTRL_BOTTOM_OFFSET                     20
#define RICHEDIT_CTRL_OVERFLOW_TEXTONLY_BOTTOM_OFFSET   2
#define RICHEDIT_CTRL_OVERFLOW_BOTTOM_OFFSET            60
#define RICHEDIT_CTRL_TOP_OFFSET_FOR_NOOVERFLOW         20


//offsets of the user client area w.r.t the total client area
#define USER_CLIENT_TOP_OFFSET       10
#define USER_CLIENT_LEFT_OFFSET      15
#define USER_CLIENT_RIGHT_OFFSET     20
#define USER_CLIENT_BOTTOM_OFFSET    60

#define PTR_DELETE(expr) if(expr){delete expr; expr = NULL;}

