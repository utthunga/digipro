#if !defined(AFX_WINDOWMAP_H__B43A9FF4_5A23_4246_93EF_09A78BF8F768__INCLUDED_)
#define AFX_WINDOWMAP_H__B43A9FF4_5A23_4246_93EF_09A78BF8F768__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Window_Map.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWindowMap window

class CWindowMap : public CObject
{
// Construction
public:
	CWindowMap();
	CWindowMap(CWnd * pWnd);
	CWindowMap(const CWindowMap & WindowMap); // Copy Constructor
	const CWindowMap &operator=(const CWindowMap & WindowMap); // Overloaded assignment operator
	virtual ~CWindowMap();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWindowMap)
	//}}AFX_VIRTUAL

// Implementation
public:
	UINT nCtrlID;  // This is the ID for m_pWnd
	UINT nUnitID;
	UINT nLabelID;
	CWnd * m_pWnd;
	void * m_pStyle;  // This is DDL style.  This specifies the type of control to use
	                  // It will be used to determine whether to use CEdit or 
	                  // CButton.
	void * m_pBaseItem;  // This is a pointer to DDL device object to draw
	                     // for that control.  It can be a variable item
	                     // or another menu.  Use this to update the value if user
	                     // changes it and for use OnUpdate().
	CRect m_PositionSize;  // Location where control is to go on screen
	
	// Generated message map functions
protected:
	//{{AFX_MSG(CWindowMap)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()
private:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINDOWMAP_H__B43A9FF4_5A23_4246_93EF_09A78BF8F768__INCLUDED_)
