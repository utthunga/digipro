// ScopeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "ScopeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl

CScopeCtrl::CScopeCtrl()
{
}

CScopeCtrl::~CScopeCtrl()
{
}


BEGIN_MESSAGE_MAP(CScopeCtrl, CStatic)
	//{{AFX_MSG_MAP(CScopeCtrl)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl message handlers

int CScopeCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CStatic::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: Add your specialized creation code here
	
	/*
	 * This is an example of loading a bitmap in a static frame
	 * It is a place holder for future graphs and charts
	 */

	BOOL result =	m_Bitmap.LoadBitmap(IDB_BITMAP2);//was IDB_BITMAP_TEST);

	HBITMAP m_hBitmap =(HBITMAP) (m_Bitmap);

	SetBitmap( m_hBitmap );

	return 0;
}
