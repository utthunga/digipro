// Command48_PropSheet.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "MainFrm.h"
#include "SDC625Doc.h"
#include "Command48_PropSheet.h"
#include "DevCommStatPropPage.h"
#include "DevStatPageOne.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommand48_PropSheet
// stevev 4/26/04
#define PROP_PAGE_SIZE	TOTAL_PROP_PAGES /* stevev 26jan 05 - was 17, spec says 0 - 24...*/
//Added by ANOOP 15FEB2004
UINT PropPageIds[PROP_PAGE_SIZE]= {
						IDD_DEVSTAT_PAGE_ONE,
						IDD_DEVSTAT_PAGE_TWO,
						IDD_DEVSTAT_PAGE_THREE,
						IDD_DEVSTAT_PAGE_FOUR,
						IDD_DEVSTAT_PAGE_FIVE,
						IDD_DEVSTAT_PAGE_SIX,
						IDD_DEVSTAT_PAGE_SEVEN,
						IDD_DEVSTAT_PAGE_EIGHT,
						IDD_DEVSTAT_PAGE_NINE,
						IDD_DEVSTAT_PAGE_TEN,
						IDD_DEVSTAT_PAGE_ELEVEN,
						IDD_DEVSTAT_PAGE_TWELVE,
						IDD_DEVSTAT_PAGE_THIRTEEN,
						IDD_DEVSTAT_PAGE_FOURTEEN,
						IDD_DEVSTAT_PAGE_FIFTEEN,
						IDD_DEVSTAT_PAGE_SIXTEEN,
						IDD_DEVSTAT_PAGE_SEVENTEEN,
						IDD_DEVSTAT_PAGE_EIGHTEEN,
						IDD_DEVSTAT_PAGE_NINETEEN,
						IDD_DEVSTAT_PAGE_TWENTY,
						IDD_DEVSTAT_PAGE_TWENTYONE,
						IDD_DEVSTAT_PAGE_TWENTYTWO,
						IDD_DEVSTAT_PAGE_TWENTYTHREE,
						IDD_DEVSTAT_PAGE_TWENTYFOUR,
						IDD_DEVSTAT_PAGE_TWENTYFIVE
					};


IMPLEMENT_DYNAMIC(CCommand48_PropSheet, CPropertySheet)
CCommand48_PropSheet::CCommand48_PropSheet(CDDLVariable *pDevStat, CDDLVariable *pCommStat,CMainFrame *ptrMainFrm,UINT nIDCaption,CWnd* pParentWnd ,UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)

{
	if( NULL != ptrMainFrm )
	{
		m_pMainFrm=ptrMainFrm;
	}
		

	
	for ( int index=0;index<TOTAL_PROP_PAGES;index++)
	{
		m_pDevStatPageOne[index]=NULL;
	}



/*	Added by ANOOP on 31Jan2004	*/
	m_ExtendedTabCreated=FALSE;
	nExtendedInfoTabs=0;
	nTot_PresentCount=0;
	nTot_PreviousCount=0;
	
	m_pCommStatus=NULL;
	m_pDeviceStatus=NULL;	

	m_pDevStat=pDevStat;
	m_pCommStat=pCommStat;

	// Comm status not needed.  If there is a communication problem, the device will disconnect
	// - Deleted by POB, 19 Oct 2005

	//m_pCommStatus= new CDevCommStatPropPage(NULL,m_pCommStat);
	//m_pCommStatus->Construct(IDD_COMMUNI_RESP_STATUS_PROP_PAGE,0); 
	//AddPage(m_pCommStatus);
	
	// End of change - POB - 19 Oct 2005 

	/*<START>Added by ANOOP 31JAN04*/
	
	m_pDeviceStatus = new CDevCommStatPropPage(m_pDevStat);
	m_pDeviceStatus->Construct(IDD_DEVICE_STATUS_PROP_PAGE,0);
	AddPage(m_pDeviceStatus);
	/*<START>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
	if( TRUE == m_pMainFrm->bCMD48DlgExtendedTabCreatedOnce )
	{
		CreateExtendedInfoTab();
	}
	/*<END>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
}	


CCommand48_PropSheet::~CCommand48_PropSheet()
{
	if(NULL != m_pCommStatus)
		delete m_pCommStatus;
	
	if(NULL != m_pDeviceStatus)
		delete m_pDeviceStatus;

	for (int nCntr=0;nCntr < nExtendedInfoTabs; nCntr++) 
	{
		if(NULL != m_pDevStatPageOne[nCntr] )
		{
			delete m_pDevStatPageOne[nCntr];
			m_pDevStatPageOne[nCntr] = NULL;
		}
	}
	

		/* Close the event we had created earlier */

}


BEGIN_MESSAGE_MAP(CCommand48_PropSheet, CPropertySheet)
	//{{AFX_MSG_MAP(CCommand48_PropSheet)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP

	ON_MESSAGE(WM_CREATE_EXTENDEDTAB, OnMessageCreateExtendedTab)
	ON_MESSAGE(WM_UPDATE_EXTENDEDTAB, OnMessageUpdateExtendedTab)
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommand48_PropSheet message handlers


BOOL CCommand48_PropSheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();
//	BOOL bResult = TRUE;
	/* Create the stop event */
	
	return bResult;
}	


BOOL CCommand48_PropSheet::UpdateCommStatusOnNotification(unsigned char cCommStat)
{
	if(NULL != m_pCommStatus )
	{
		if(TRUE == m_pCommStatus->bCommStatusDisplayed )
		{
			m_pCommStatus->OnUpdateCommrespStatus(cCommStat );
	}
			
	}
	return TRUE;
}	

BOOL CCommand48_PropSheet::UpdateDeviceStatusOnNotification(unsigned char cDevStat)
{
	if(NULL != m_pDeviceStatus )
	{
		if(TRUE == m_pDeviceStatus->bDeviceStatusDisplayed )
		{
			m_pDeviceStatus->OnUpdateDeviceStatus(cDevStat);
		}
	}
	return TRUE;
}

BOOL CCommand48_PropSheet::AddSuppressMessageTab()
{
	if(	NULL != m_pMainFrm->m_pDoc )
	{
		if(TRUE == m_pMainFrm->m_pDoc->m_bIgnoreRespCode )
		{
			for(int nPages=0;nPages<nExtendedInfoTabs;nPages++)
	{
				if(NULL != m_pDevStatPageOne[nPages])
				{
					if( TRUE == m_pDevStatPageOne[nPages]->bStaticCtrl_Created)
					{
						if( FALSE == m_pDevStatPageOne[nPages]->bSuppress_MsgDisplay )
						{
							m_pDevStatPageOne[nPages]->bSuppress_MsgDisplay=TRUE;
							m_pDevStatPageOne[nPages]->OnAddSuppressMessage();
						}
			}
		}
				else
				{
					break;
			}
		}
		}
	}	
	return TRUE;
}
	
BOOL CCommand48_PropSheet::UpdateSuppressMessageTab()
{
	if(	NULL != m_pMainFrm->m_pDoc )
	{
		if(TRUE == m_pMainFrm->m_pDoc->m_bIgnoreRespCode )
		{
			for(int nPages=0;nPages<nExtendedInfoTabs;nPages++)
			{	
				if(NULL != m_pDevStatPageOne[nPages])
				{
					if( TRUE == m_pDevStatPageOne[nPages]->bStaticCtrl_Created)
					{
						if( FALSE == m_pDevStatPageOne[nPages]->bSuppress_MsgDisplay )
						{
							m_pDevStatPageOne[nPages]->bSuppress_MsgDisplay=TRUE;
							m_pDevStatPageOne[nPages]->OnAddSuppressMessage();
						}
					}
				}
				else
				{
					break;
				}
			}
		}
	}
	return TRUE;
}


BOOL CCommand48_PropSheet::RemoveSuppressMessageTab()
{
	if(	NULL != m_pMainFrm->m_pDoc )
	{
		if(TRUE == m_pMainFrm->m_pDoc->m_bIgnoreRespCode )
		{
			for(int nPages=0;nPages<nExtendedInfoTabs;nPages++)
	{
				if(NULL != m_pDevStatPageOne[nPages])
	{
					if( TRUE == m_pDevStatPageOne[nPages]->bStaticCtrl_Created)
					{
						m_pDevStatPageOne[nPages]->bSuppress_MsgDisplay=FALSE;
						m_pDevStatPageOne[nPages]->OnRemoveSuppressMessage();
					}
				}
				else
				{
					break;
				}
			}
		}
	}
	return TRUE;
}

BOOL CCommand48_PropSheet::CreateExtendedInfoTab()
{
	BOOL bRetVal;

	m_ExtendedTabCreated=TRUE;
	if( NULL != m_pMainFrm )
	{
		bRetVal=m_pMainFrm->GetExtendedDeviceStatusInfo( cmd48resp_PreviousArray,nTot_Count); 
		if(FALSE == bRetVal)
		{
			return FALSE;
		}
		else
		{
			bRetVal=CreateIndividualTabs(cmd48resp_PreviousArray,nTot_Count);
			if( FALSE == bRetVal) return FALSE;
		}
			
		/*<START>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */			
		if(FALSE == m_pMainFrm->bCMD48DlgExtendedTabCreatedOnce)
		{
			m_pMainFrm->bCMD48DlgExtendedTabCreatedOnce=TRUE;
		}
		/*<END>Added by ANOOP 17FEB2004 The extended tabs should reapper after the CMD48 dialog is closed once */	
		
	}

	return TRUE;
}

BOOL CCommand48_PropSheet::UpdateExtendedInfoTab()
{
	BOOL bRetVal;
	
	if( NULL != m_pMainFrm )
	{
		if( cmd48resp_PresentArray.GetSize() > 0 )
		{
			cmd48resp_PresentArray.RemoveAll();
		}
		bRetVal=m_pMainFrm->GetExtendedDeviceStatusInfo( cmd48resp_PresentArray,nTot_Count); 
	}

	int nlen_Present=cmd48resp_PresentArray.GetSize();
	int nlen_Previous=cmd48resp_PreviousArray.GetSize();

	if( nlen_Present != nlen_Previous )
	{
	//delete the extended tabs and recreate again
	/*	for(int nCntr=0;nCntr < nExtendedInfoTabs;nCntr++)
		{
			delete m_pDevStatPageOne[nCntr];
		}
		CreateExtendedInfoTab();	*/
	}
	else
	{
		int n_lowerlt=0,n_upperlt=19;
		if (nlen_Present < 20)
		{
			n_upperlt=nlen_Present;
		}		
			
		for(int nCntr=0;nCntr < nExtendedInfoTabs;nCntr++)
		{
			m_pDevStatPageOne[nCntr]->OnUpdateExtendedDeviceStatus(cmd48resp_PresentArray,cmd48resp_PreviousArray,n_lowerlt,n_upperlt);			
			nlen_Present -=20;
			if(nlen_Present >0 && nlen_Present < 20)
			{
				n_lowerlt += 20;
				n_upperlt += nlen_Present;
			}
			else
			{
				n_lowerlt += 20;
				n_upperlt += 20;
			}
		}
	
	
	}

	CMD48_RESPONSE cmdresp;
	if( cmd48resp_PreviousArray.GetSize() > 0 )
	{
		cmd48resp_PreviousArray.RemoveAll();
	}

	for(int i=0;i<cmd48resp_PresentArray.GetSize();i++ )
	{
		cmdresp=cmd48resp_PresentArray.GetAt(i); 
		cmd48resp_PreviousArray.Add(cmdresp);
	}	

	if(FALSE == bRetVal)
		return FALSE;
	else
		return TRUE;
}


BOOL CCommand48_PropSheet::CreateIndividualTabs(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_Array,int nTotCount)
{
	CString szByteIdentifier;
	int nCount= nTotCount;

	
	CString szByteIdentity[TOTAL_PROP_PAGES];;
		
	for ( int index=0;index<nCount;index++)
	{
		m_pDevStatPageOne[index]=NULL;	
	}

	// Fix Command 48 Tabs so that they show the VARAIBLE's label on the tab, POB - 17 Oct 2005
	CMD48_RESPONSE cmdresp;

	int nPages = 0;
	szByteIdentifier="";

	for(unsigned int i=0; i < ((unsigned)m_Array.GetSize()); i++ )
	{
		cmdresp=m_Array.GetAt(i);

		if (szByteIdentifier != cmdresp.szByteIdentity)
		{
			// We have found a new label for our next page
			szByteIdentity[nPages] = cmdresp.szByteIdentity;
			szByteIdentifier       = cmdresp.szByteIdentity;
			nPages++;
		}
	}

	// End of change - POB - 17 Oct 2005 

			
	for(nPages=0;nPages<nCount  && nPages < PROP_PAGE_SIZE;nPages++)
	{
		// Do not need this based on the addition of code above, POB - 17 Oct 2005
//		char szCnt[24]={0};
//		szByteIdentifier="";	
//		szByteIdentifier="BYTE"; 
//		itoa(nPages,szCnt,10); 
//		szByteIdentifier +=  szCnt;
		
		// End of change - POB - 17 Oct 2005
		
		m_pDevStatPageOne[nPages] = new CDevStatPageOne(m_Array, szByteIdentity[nPages], TRUE);
		m_pDevStatPageOne[nPages]->Construct(PropPageIds[nPages],0); 
				nExtendedInfoTabs++;
		AddPage(m_pDevStatPageOne[nPages]);
		

		// By the time this code is called the Create on CPropertySheet has already been called
		// We must use SetItem to change the name of the PropertyPage tabs, POB - 17 Oct 2005
		LPTSTR pszText = (LPTSTR)(LPCTSTR) szByteIdentity[nPages];

		if (IsWindow(m_hWnd))
		{
			// The PropertySheet has already been created, we need to get access to the tab control
			CTabCtrl* pTab = GetTabControl();
			ASSERT (pTab);

			int TabCount = pTab->GetItemCount();

			TC_ITEM ti;
			ti.mask = TCIF_TEXT;
			ti.pszText = pszText;

			// The index is zero based so we need to subtract 1 - This will set the Item for the page
			// that we just added.
			VERIFY (pTab->SetItem (TabCount - 1, &ti));
		}
		else
		{
			// The PropertySheet has not been created yet
			m_pDevStatPageOne[nPages]->m_psp.dwFlags |= PSP_USETITLE;
			m_pDevStatPageOne[nPages]->m_psp.pszTitle = pszText;
		}

		// End of change - POB - 17 Oct 2005
	}

	/* notify user in case of shortened list --- we need a scrolling tab list!!!*/
	if (nPages < nCount)
	{
		LOGIT(CERR_LOG|UI_LOG,"NOTICE: Command 48 window has %d fewer tabs then response.",(nCount-PROP_PAGE_SIZE));
	}

	if(	NULL != m_pMainFrm->m_pDoc )    //Add suppressed message to all extended tabs if the ignore button is clicked 
	{
		if(TRUE == m_pMainFrm->m_pDoc->m_bIgnoreRespCode )
		{
			for(int nPgs=0;nPgs<nCount && nPgs < PROP_PAGE_SIZE ;nPgs++)/*stevev 26jan05 -prevent crash*/
			{
				m_pDevStatPageOne[nPgs]->bSuppress_MsgDisplay=TRUE;
				AddSuppressMessageTab();
			}
		}
	}


	m_ExtendedTabCreated=TRUE;
	return TRUE;

}	


LRESULT CCommand48_PropSheet::OnMessageCreateExtendedTab(WPARAM wParam, LPARAM lParam)
{
	//	RemoveSuppressMessageTab();   //Call this function
	BOOL bRetVal=CreateExtendedInfoTab();
	if(FALSE == bRetVal) return 1;
	return 0;
}


LRESULT CCommand48_PropSheet::OnMessageUpdateExtendedTab(WPARAM wParam, LPARAM lParam)
{
	BOOL bRetVal=UpdateExtendedInfoTab();
	if(FALSE == bRetVal) return 1;
	return 0;
}

int CCommand48_PropSheet::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	EnableStackedTabs(FALSE);

	if (CPropertySheet::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}


BOOL CCommand48_PropSheet::DestroyWindow() 
{
//	HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);// POB, 20 Oct 2004 - This is returning NULL (comment out)
//	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();  // POB, 20 Oct 2004 - Fix, CMainFrame catching WM_DESTROY_CMD48DIALOG message
	CMainFrame * pMainWnd = GETMAINFRAME;                    // stevev 08oct10:was:> (CMainFrame *) AfxGetMainWnd();
	
	HWND hwndMainWindow = pMainWnd->GetSafeHwnd();  // POB, 20 Oct 2004 - Fix

	if (hwndMainWindow != NULL)
	{
		::PostMessage(hwndMainWindow, WM_DESTROY_CMD48DIALOG, NULL, NULL);
	}

	return CPropertySheet::DestroyWindow();
	
}
