#if !defined(AFX_BIT_H__D21C59D2_F1BB_4063_A680_D79DECBD0270__INCLUDED_)
#define AFX_BIT_H__D21C59D2_F1BB_4063_A680_D79DECBD0270__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Bit.h : header file
//


/***************************************************************************
 * Global Definitions
 ***************************************************************************/

/*
 * light status values.
 */
typedef enum
{
     CLR_NORMAL		= 0,
	 CLR_DIAGNOSTIC

}COLORTYPE;

/*typedef struct
{
     COLORREF bitOn;
     COLORREF bitOff;

}COLORBIT;*/

/////////////////////////////////////////////////////////////////////////////
// CBit window

class CBit : public CButton
{
	DECLARE_DYNAMIC(CBit)

// Construction
public:
	CBit();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBit)
	protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetColorOn(COLORREF color);
	void SetColor(COLORTYPE type);
	BOOL Toggle();
	void EnableButton(BOOL state = TRUE);
	BOOL IsOn();
	void SetOn(BOOL state = TRUE);
	void Highlight(BOOL state = TRUE);
	virtual ~CBit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBit)
	afx_msg void OnClicked();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	COLORREF GetColor(double dAngle, COLORREF crBright, COLORREF crDark);
	void DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crColour, BOOL bDashed = FALSE);
	void DrawCircle(CDC* pDC, CPoint p, LONG lRadius, COLORREF crBright, COLORREF crDark);
	void DrawCircle(CDC* pDC, LONG nRadius);
	
	struct COLORBIT
	{
		 COLORREF bitOn;
		 COLORREF bitOff;

	};

	BOOL m_bIsOn;
	BOOL m_dsplState;
	COLORREF m_colorCurrent;
	COLORREF m_colorOn;
	COLORREF m_colorOff;
	COLORREF m_colorFocus;
	CRgn   m_rgn;
	CPoint m_ptCentre;
	int    m_nRadius;
	BOOL   m_bDrawDashedFocusCircle;
	static const COLORREF m_CLOUDBLUE;
	static const COLORREF m_WHITE;
	static const COLORREF m_BLACK;
	static const COLORREF m_DKGRAY;
	static const COLORREF m_LTGRAY;
	static const COLORREF m_YELLOW;
	static const COLORREF m_DKYELLOW;
	static const COLORREF m_RED;
	static const COLORREF m_DKRED;
	static const COLORREF m_BLUE;
	static const COLORREF m_DKBLUE;
	static const COLORREF m_CYAN;
	static const COLORREF m_DKCYAN;
	static const COLORREF m_GREEN;
	static const COLORREF m_DKGREEN;
	static const COLORREF m_MAGENTA;
	static const COLORREF m_DKMAGENTA;
	static const COLORREF m_FOCUS;
	COLORBIT m_bitColor[2];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BIT_H__D21C59D2_F1BB_4063_A680_D79DECBD0270__INCLUDED_)
