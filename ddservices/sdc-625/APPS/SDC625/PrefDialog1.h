#if !defined(AFX_PREFDIALOG_H__821096FF_6BD1_4654_A678_8BC191661D3E__INCLUDED_)
#define AFX_PREFDIALOG_H__821096FF_6BD1_4654_A678_8BC191661D3E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrefDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrefDialog dialog

class CPrefDialog : public CDialog
{
// Construction
public:
	CPrefDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrefDialog)
	enum { IDD = IDD_PREF_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrefDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPrefDialog)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnRadio5();
	afx_msg void OnRadio6();
	afx_msg void OnRadio7();
	afx_msg void OnSelchangeSearchSerialPort();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool CheckPollAddr(CString &pa);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREFDIALOG_H__821096FF_6BD1_4654_A678_8BC191661D3E__INCLUDED_)

