#if !defined(AFX_DEVICEDETAILS_H__0AAF7764_9E14_4092_9665_99DB7D1CC677__INCLUDED_)
#define AFX_DEVICEDETAILS_H__0AAF7764_9E14_4092_9665_99DB7D1CC677__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DeviceDetails.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDeviceDetails dialog
#include "SDC625Doc.h"

class CDeviceDetails : public CDialog
{
// Construction
public:
	CDeviceDetails(CDDIdeDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDeviceDetails)
	enum { IDD = IDD_DEVICE_DETAILS };
	CString	m_strDDRevision;
	CString	m_strDistributor;
	CString	m_strRevision;
	CString	m_strType;
	CString	m_comp;
	//}}AFX_DATA
	int m_intComp;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDeviceDetails)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDeviceDetails)
	virtual BOOL OnInitDialog();
	afx_msg void OnComp();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CDDIdeDoc *m_pDoc;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVICEDETAILS_H__0AAF7764_9E14_4092_9665_99DB7D1CC677__INCLUDED_)
