// WindowChildFrm.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "WindowChildFrm.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame

IMPLEMENT_DYNCREATE(CWindowChildFrame, CChildFrame)

BEGIN_MESSAGE_MAP(CWindowChildFrame, CChildFrame)
	//{{AFX_MSG_MAP(CWindowChildFrame)
	ON_WM_CLOSE()
	ON_WM_MDIACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame construction/destruction

CWindowChildFrame::CWindowChildFrame()
{
}

CWindowChildFrame::~CWindowChildFrame()
{
}

BOOL CWindowChildFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	cs.lpszName = "Window View";
	cs.style &= ~FWS_ADDTOTITLE;

	return CChildFrame::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame diagnostics

#ifdef _DEBUG
void CWindowChildFrame::AssertValid() const
{
	CChildFrame::AssertValid();
}

void CWindowChildFrame::Dump(CDumpContext& dc) const
{
	CChildFrame::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame message handlers

void CWindowChildFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
	
	CChildFrame::OnClose();

	pMainWnd->PostMessage(WM_SDC_SIZECHILD);
}

void CWindowChildFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	// call the base class to let standard processing switch to
	// the top-level menu associated with this window
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	// do our modifications, if we are being activated
	
	// Create menu to show window views
	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
	pMainWnd->CreateWindowMenu();
	
}
