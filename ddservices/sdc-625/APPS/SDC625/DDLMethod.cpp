/*************************************************************************************************
 *
 * $Workfile: DDLMethod.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		DDLMethod.cpp: implementation of the CDDLMethod class.
 */

#include "stdafx.h"
#include "SDC625.h"
#include "DDLMethod.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

IMPLEMENT_DYNAMIC( CDDLMethod, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDDLMethod::CDDLMethod(hCitemBase* pIB):CDDLBase(pIB)
{
    m_nImage  = IMG_METHOD;
//	m_pddbMeth= NULL;


}
/* stevev 26sep08 - this is the wrong way to do this...method support should not be used
CDDLMethod::CDDLMethod(hCmethodCall& MethodCall,CMethodSupport* pMethSupport,hCitemBase* pIB) 
	: m_MethodCall(MethodCall),m_pMethSupport(pMethSupport),CDDLBase(pIB)
{
    m_nImage  = IMG_METHOD;


}

CDDLMethod::CDDLMethod(hCmethodCall& MethodCall,CMethodSupport* pMethSupport,itemInfo_t& pIi) 
	: m_MethodCall(MethodCall),m_pMethSupport(pMethSupport),CDDLBase(pIi.p_Ib)
{
    m_nImage  = IMG_METHOD;	
	m_strName = pIi.labelStr.c_str();// could be different from item label
}
the wrong way ***/
CDDLMethod::CDDLMethod(hCmethodCall& MethodCall,hCddbDevice* pDSP,hCitemBase* pIB) 
	: m_MethodCall(MethodCall),pDev(pDSP),CDDLBase(pIB)
{
    m_nImage  = IMG_METHOD;
}

CDDLMethod::CDDLMethod(hCmethodCall& MethodCall,hCddbDevice* pDSP,itemInfo_t& pIi) 
	: m_MethodCall(MethodCall),pDev(pDSP),CDDLBase(pIi.p_Ib)
{
    m_nImage     = IMG_METHOD;	
	m_strName    = pIi.labelStr.c_str();// could be different from item label
	m_strNmValid = pIi.labelFilled;	

	m_strHelp      = pIi.help_Str.c_str();// stevev 28dec11
}
CDDLMethod::~CDDLMethod()
{

}



//////////////////////////////////////////////////////////////////////
// Overrides
//////////////////////////////////////////////////////////////////////

//Vibhor 1405 : Start Of Code
void CDDLMethod::Execute(CString szMethodName,CString szHelp) //Changed Definition
{
// stevev - 31jan06 - refill the cleared methodcall...this may need to re-resolve??
m_MethodCall.m_pMeth  = (hCmethod*)m_pItemBase;
m_MethodCall.source   = msrc_EXTERN;
m_MethodCall.methodID = m_lItemId;
m_MethodCall.paramList.clear();

	LOGIT(CLOG_LOG,"Execute Method %d\n",m_lItemId);
	TRACE(L">>> Execute method '%s'\n",szMethodName);

/*stevev 26sep08 - this is the wrong way to do this...method support should not be used
	m_pMethSupport->PreExecute();
	m_pMethSupport->DoMethodExecute(m_MethodCall);
	m_pMethSupport->PostExecute();
*/
	CValueVarient retVarient;
	if (pDev)
		pDev->executeMethod(m_MethodCall, retVarient);

	m_MethodCall.clear();// stevev 31jan06 - clear it for the next entry...
	TRACE(L">>> End Execute method '%s'\n",szMethodName);
}



BOOL CDDLMethod::IsMethod()
{
    return TRUE;
}

void CDDLMethod::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_METHOD;    
//    pMenu->GetSubMenu(nPos)->EnableMenuItem(0, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
//    pMenu->GetSubMenu(nPos)->CheckMenuItem( ID_MENU_HELP, MF_CHECKED);
}


/*************************************************************************************************
 *
 *   $History: DDLMethod.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
