/*************************************************************************************************
 *
 * $Workfile: DDLMenu.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface for the CDDLVariable class.
 * #include "DDLMenu.h".		
 */
#if !defined(AFX_DDLMENU_H__C1ED7F16_D28C_4344_8FAC_3348763D8DBF__INCLUDED_)
#define AFX_DDLMENU_H__C1ED7F16_D28C_4344_8FAC_3348763D8DBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLBase.h"
#include "ddldefs.h"	// for qualifiers


class hCitemBase;
class hCmenuItem;

// qualifiers
//#define REVIEW_ITEM 						0x04	-- only valid for menu menu-items
	
//typedef struct itemInfo_s
class itemInfo_t
{
public:
	wstring labelStr;
	bool    labelFilled;
	hCitemBase* p_Ib;
	unsigned int qual;
	wstring help_Str;
	unsigned int indexValue;// invalid unless qual has INDEXED_ITEM

	hCmenuItem* pddbMenuItem;// only used for scope, image, grid & const string
							 // we own this copy!  delete it on our way out
public:
	
	itemInfo_t(){labelStr.erase();labelFilled=false;p_Ib=NULL;qual=0;help_Str.erase();
														   indexValue=0;pddbMenuItem = NULL; };
	~itemInfo_t();

	itemInfo_t(const itemInfo_t& s){ operator=(s);};
	itemInfo_t& operator=(const itemInfo_t& src);
	void clear(void) {labelStr = L""; labelFilled = false; p_Ib = NULL; qual = 0; 
					  help_Str = L""; indexValue  = 0; pddbMenuItem = NULL;};

};
/* typedef */// itemInfo_t;
typedef vector<itemInfo_t>    itemInfoList_t;
typedef itemInfoList_t::iterator itmInfoIT_t;

/***************************************************************************
 * Global Definitions
 ***************************************************************************/

class CDDLMenu : public CDDLBase
{
    DECLARE_DYNAMIC(CDDLMenu)

public:
	CDDLMenu(hCitemBase* pIB=NULL);
	CDDLMenu(itemInfo_t& pIi);// sets help too
 	virtual ~CDDLMenu();

// Special menu handling

// Overrides
    BOOL IsMenu();
	CDDLMenu* getMenuPtr(void) { return this; };

// Menu specific
	int  GetMenuItemList(itemInfoList_t& returnList);// from hCmenu
	int  GetExistingList(itemInfoList_t& returnList);// from children
	void UpdateChildList(void);

	bool areSame(itemInfoList_t& lst1,itemInfoList_t& lst2);// compare item IDs ONLY 
	bool areExactlySame(itemInfoList_t& lst1,itemInfoList_t& lst2);//compare item IDs AND Labels
	
	CString GetCurrentName(CDDLBase* pChildItm);

private:

};

#endif // !defined(AFX_DDLMENU_H__C1ED7F16_D28C_4344_8FAC_3348763D8DBF__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLMenu.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */