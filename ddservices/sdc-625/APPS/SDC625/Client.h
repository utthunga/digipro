#if !defined(AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_)
#define AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Client.h : header file
//

#include "afxsock.h"
/////////////////////////////////////////////////////////////////////////////
// CClient command target
class CDDIdeDoc;

class CClient : public CSocket
{
// Attributes
public:
	BOOL m_bConnected;

// Operations
public:
	CClient(CDDIdeDoc *pPdoc);
	virtual ~CClient();
	void Createnew();
	void SendMessage(char *strMessage,int nLength);
// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClient)
	public:
	virtual void OnSend(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClient)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CDDIdeDoc *m_pDoc;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_)
