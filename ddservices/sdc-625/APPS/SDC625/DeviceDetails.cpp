// DeviceDetails.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "DeviceDetails.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDeviceDetails dialog

CDeviceDetails::CDeviceDetails(CDDIdeDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CDeviceDetails::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDeviceDetails)
	m_strDDRevision = _T("");
	m_strDistributor = _T("");
	m_strRevision = _T("");
	m_strType = _T("");
	m_comp = _T("");
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_intComp = m_pDoc->pCurDev->compatability();
}


void CDeviceDetails::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDeviceDetails)
	DDX_Text(pDX, IDC_DDREVISION, m_strDDRevision);
	DDX_Text(pDX, IDC_MANUFACTURER, m_strDistributor);
	DDX_Text(pDX, IDC_REVISION, m_strRevision);
	DDX_Text(pDX, IDC_TYPE, m_strType);
	DDX_Text(pDX, IDC_COMP, m_comp);
	DDV_MaxChars(pDX, m_comp, 1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDeviceDetails, CDialog)
	//{{AFX_MSG_MAP(CDeviceDetails)
	ON_BN_CLICKED(IDC_COMP, OnComp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDeviceDetails message handlers

BOOL CDeviceDetails::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (m_pDoc && m_pDoc->pCurDev)
	{
		if (m_pDoc->pCurDev->DeviceDescStrings.size() == 4)
		{
			int nIndex = 0;
			CString strTemp;
			hCEnum *pEnum=NULL;

			hCitemBase *pItemBase = NULL;
			if (m_pDoc->pCurDev->getItemBySymNumber(DEVICE_DISTRIBUTOR, &pItemBase) == SUCCESS)
			{
				string strLabel = "";
				if (pItemBase->getTypeVal() == VARIABLE_ITYPE)
				{
					CDDLVariable *pVarDistributor = new CDDLVariable((hCVar *)pItemBase);
					m_strDistributor = *pVarDistributor;
					delete pVarDistributor;
				}
			}

			
			if( m_strDistributor.IsEmpty() )
			{
				hCDeviceManger *tempPtr2DevMgr = m_pDoc->pCurDev->pDevMgr;
				hCddbDevice *stdTbl = (hCddbDevice*)tempPtr2DevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE);


				if(stdTbl->getItemBySymNumber(COMPANY_IDENTIFICATION_CODE,&pItemBase) == SUCCESS )
				{
					if ( pItemBase->getIType() == iT_Variable)
					{
						int t = ((hCVar*)pItemBase)->VariableType() ;
						if ( t == vT_Enumerated || t == vT_BitEnumerated )
						{
							string tStr;
							pEnum=(hCEnum*)pItemBase;

							if ( pEnum->procureString(pEnum->getDispValue(), tStr) == SUCCESS)
							{
								m_strDistributor = tStr.c_str();
							
							}
						}	
					}
				}
			
			}

			pEnum=(hCEnum*)pItemBase;
			char strDstbr[256]={0};
			if (pEnum)		// prevent crash...
			{
				UINT nVal=pEnum->getDispValue();

				sprintf( strDstbr," ( 0x%02x )",nVal);
				m_strDistributor += strDstbr;
			}
		
/* VMKP Modified on 091203 */
/*			strTemp = m_pDoc->pCurDev->DeviceDescStrings[1].c_str();
			if ( (nIndex = strTemp.ReverseFind(' ')) > 0 )
			{
				m_strType = strTemp.Mid(nIndex + 8);
			}*/
			
			if (m_pDoc->pCurDev->getItemBySymNumber(DEVICE_DEVICE_TYPE, &pItemBase) == SUCCESS)
			{
				string strLabel = "";
				if (pItemBase->getTypeVal() == VARIABLE_ITYPE)
				{
					CDDLVariable *pDevType = new CDDLVariable((hCVar *)pItemBase);
					m_strType = *pDevType;
					delete pDevType;
			}
			}
			
			char strDevtype[256]={0};
			pEnum=(hCEnum*)pItemBase;
			UINT nDevType=pEnum->getDispValue();

			sprintf( strDevtype," ( 0x%02x )",nDevType);
			m_strType += strDevtype;

/* VMKP Modified on 091203 */

			strTemp = m_pDoc->pCurDev->DeviceDescStrings[2].c_str();
			if ( (nIndex = strTemp.ReverseFind(' ')) > 0 )
			{
				m_strRevision = strTemp.Mid(nIndex + 4,2);
			}
			strTemp = m_pDoc->pCurDev->DeviceDescStrings[3].c_str();
			if ( (nIndex = strTemp.ReverseFind(' ')) > 0 )
			{
				m_strDDRevision = strTemp.Mid(nIndex + 4,2);
			}
		}
		if (m_pDoc->pCurDev->whatCompatability() == dm_Standard)
		{
			m_comp = ' ';
		}
		else
		{
			m_comp = 'L';
		}
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDeviceDetails::OnComp() 
{	
	if ( m_comp == ' ' )// Strict
	{
		m_comp = 'L';
		m_pDoc->pCurDev->setCompatability(dm_275compatible);
	}
	else				// Libral
	{
		m_comp = ' ';
		m_pDoc->pCurDev->setCompatability(dm_Standard);
	}
	UpdateData(FALSE);
	
}