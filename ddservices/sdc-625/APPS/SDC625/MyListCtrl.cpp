// MainListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "MyListCtrl.h"
#include "DDLBase.h"
#include "DDLMenuItem.h"
#include "DDLVariable.h"
#include "VariableDlg.h"
#include "SDC625Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define OFFSET	2
//#define OFFSET_FIRST	2
//#define OFFSET_OTHER	6

/////////////////////////////////////////////////////////////////////////////
// CMyListCtrl

CMyListCtrl::CMyListCtrl()
{

}

CMyListCtrl::~CMyListCtrl()
{
}


BEGIN_MESSAGE_MAP(CMyListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CMyListCtrl)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(GN_CONTEXTMENU, OnContextMenu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyListCtrl message handlers


void CMyListCtrl::OnContextMenu(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMRGINFO *pInfo=(NMRGINFO*)pNMHDR;

	// TODO: Add your control notification handler code here
	CDDLBase*      pItem;
	CMenu          menu;
	CMenu*         pSubmenu;
	CPoint         ptClient;
	LV_HITTESTINFO HitTestInfo;
	int nPos;
    CDDIdeDoc* pDoc = GetDocument();

	// Make sure that we do the same selection for the Right click also
	//	OnLButtonDown(nFlags,point);
	//Get the mouse position associated with this message
	// and see if it is on a tree item.

	DWORD dwPosition = GetMessagePos();
	CPoint cursorPoint(LOWORD(dwPosition), HIWORD(dwPosition));
	ptClient = cursorPoint;
	ScreenToClient(&ptClient);
	HitTestInfo.pt = ptClient;
	HitTest( &HitTestInfo );

	if( LVHT_ONITEM & HitTestInfo.flags )
	{
//         * The click was on a m_pList item.
//         * Get the CDDLBase pointer from the selected object
//         * and have it load the menu.  Must save the pointer
//         * so the frame knows where to process the menu selection.
		pItem = (CDDLBase *)GetItemData( HitTestInfo.iItem );
		pItem->LoadContextMenu( &menu, nPos ) ;
		if( pItem )
		{
	            pDoc->m_pMenuSource = pItem;
/*<START>Anoop For fixing the PAR 5286. In the tree view context menu should be displayed only when the select menu item is online */			
				
				if(NULL != pDoc)
				{
						pSubmenu = menu.GetSubMenu(nPos);
					#ifndef _WIN32_WCE
						BYTE bzRetVal = pSubmenu->TrackPopupMenu( 
												  TPM_LEFTALIGN,     // | TPM_RIGHTBUTTON, Removed by VMKP
							 					  cursorPoint.x, 
												  cursorPoint.y, 
												  AfxGetMainWnd()
												  );
					#else
						CWnd *pWnd = GetParent();
						ASSERT_VALID(pWnd );
			//			pWnd->OnCmdMsg(ID_MENU_EXECUTE,CN_UPDATE_COMMAND_UI,NULL,NULL);
			//			pWnd->OnCommand(ID_MENU_EXECUTE,0);
						BYTE bzRetVal = pSubmenu->TrackPopupMenu( 
														TPM_LEFTALIGN,     // | TPM_RIGHTBUTTON, Removed by VMKP
														cursorPoint.x,
														cursorPoint.y, 
														pWnd
														);
					#endif
				}
		}
	}
	*pResult = 0;
}


void CMyListCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{// one one click
 // one followed by ON_DBLCLK at double click
	UINT uFlags=0;

	SHRGINFO shrgi = {0};

	shrgi.cbSize = sizeof(SHRGINFO);
	shrgi.hwndClient = m_hWnd;
	shrgi.ptDown.x = point.x;
	shrgi.ptDown.y = point.y;
	shrgi.dwFlags = SHRG_NOTIFYPARENT;

//	if(GN_CONTEXTMENU == ::SHRecognizeGesture(&shrgi))
    Default();
}


CDDIdeDoc* CMyListCtrl::GetDocument()
{
    CDDIdeDoc * pDoc = NULL;
    /*
     * Get pointer to document if not already done so
     */
    CDocManager * pManager = AfxGetApp()->m_pDocManager;
    ASSERT ( pManager != NULL ); 

	POSITION posTemplate = pManager->GetFirstDocTemplatePosition();
	while ( posTemplate !=NULL )
    {
        // get the next template
        CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
        POSITION posDoc = pTemplate->GetFirstDocPosition();
        while( posDoc !=NULL )
        {
            pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
        }
    }
    return pDoc;
}

