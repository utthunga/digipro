#if !defined(AFX_DEVSTATPAGEONE_H__5B8EFC7B_49B1_4AB8_9556_E9B93F1023F7__INCLUDED_)
#define AFX_DEVSTATPAGEONE_H__5B8EFC7B_49B1_4AB8_9556_E9B93F1023F7__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DevStatPageOne.h : header file
//
#include "DDLVariable.h" 
#include "Command48_PropSheet.h" 
#include "DevCommStatPropPage.h" 
#define MAX_RANGE_VAL 20


/////////////////////////////////////////////////////////////////////////////
// CDevStatPageOne dialog

class CDevStatPageOne : public CPropertyPage
{
	DECLARE_DYNCREATE(CDevStatPageOne)
	CArray<CMD48_RESPONSE,CMD48_RESPONSE &> cmd48_PreviousArray;
	CArray<CMD48_RESPONSE,CMD48_RESPONSE &> cmd48_PresentArray;
	BOOL bInitialCreationDone;
	CStatic m_StaticMsg;
public:
	BOOL bSuppress_MsgDisplay;
	BOOL bStaticCtrl_Created;
	BOOL bAddMsgSuppressed;
	CString m_szByteIdentifier;	//Added by ANOOP 02MAR2004
// Construction
public:
	CDevStatPageOne();
	CDevStatPageOne(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> & m_Arr,CString &,BOOL);
	BOOL CreateTab(); // PAW 03/03/09 BOOL added
	BOOL OnUpdateExtendedDeviceStatus(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &,CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &,int,int);	// PAW 03/03/09 BOOL 
	BOOL OnAddSuppressMessage();
	BOOL OnRemoveSuppressMessage();

	~CDevStatPageOne();

// Dialog Data
	//{{AFX_DATA(CDevStatPageOne)
	enum { IDD = IDD_DEVSTAT_PAGE_ONE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDevStatPageOne)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDevStatPageOne)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CBitmapButton *m_pBtnArray[MAX_RANGE_VAL];
	CStatic *m_pStaticArray[MAX_RANGE_VAL];

private:
	unsigned int nUpper_Limit;
	unsigned int nLower_Limit;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVSTATPAGEONE_H__5B8EFC7B_49B1_4AB8_9556_E9B93F1023F7__INCLUDED_)
