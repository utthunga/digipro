/**********************************************************************************************
 *
 * $Workfile: DDIde.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *      DDIde.cpp : Defines the class behaviors for the application.
 */

//// memory checking::
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

_CrtMemState ms[16],diffstate;// 16 points
// end memory checking

#include "stdafx.h"

//#if defined(_PROC_NAME) && _PROC_NAME == 102988	
///* _PROC_NAME set via PROCNAME environment variable 
//   c:>SET PROCNAME 102988
//   the command line has '_PROC_NAME=$(PROCNAME)' on it 
//   set PROCNAME to another number to turn off memory build
//   -- for more details on vld.h see Code_Project's Visual Leak Detector by Dan Moulding */
//#include "vld.h"
//#pragma message("Visual Leak Detector included.")
//#endif

#include <windows.h>
#include "ddbDevice.h"
//#include "DDLDevice.h"
#include "args.h"
#include "SDC625.h"

// timj begin 4/6/04
#include "PrefDialog1.h"
// timj end 4/6/04


#include "MainFrm.h"
#include "SDC625Doc.h"
#include "SDC625TreeView.h"
#include "SDC625_WindowView.h"
#include "SDC625_HiddenView.h"
#include "ErrLogView.h"
#include "ddb.h"
#include "logging.h"
#include "FileVersion.h"

#ifdef RK_STATIC /* license support */
#include "regkey.h"
#endif

#include "globals.h" // includes TLStorage.h

#include < stdio.h >
#include < conio.h >
#include < fstream >


#ifdef APSTUDIO_INVOKED
  #ifndef APSTUDIO_READONLY_SYMBOLS
   #include "resource.h"
  #else
   #define HLD APSTUDIO_READONLY_SYMBOLS
   #undef APSTUDIO_READONLY_SYMBOLS
   #include "resource.h"
   #define APSTUDIO_READONLY_SYMBOLS  HLD
  #endif
#else
#define APSTUDIO_INVOKED x
  #ifndef APSTUDIO_READONLY_SYMBOLS
   #include "resource.h"
  #else
   #define HLD APSTUDIO_READONLY_SYMBOLS
   #undef APSTUDIO_READONLY_SYMBOLS
   #include "resource.h"
   #define APSTUDIO_READONLY_SYMBOLS  HLD
  #endif
#undef  APSTUDIO_INVOKED
#endif

extern void logTime(void);


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// stevev 5-6-4 license support
#define VALIDATIONCODE                             "2Q8XH34Q2W"
#define USERNAME                             "Kenneth Holladay"
#define USERKEY                                       289213860
#define REG_STRING_SIZE                                       6
#define REG_KEY_SIZE                                         20

/////////////////////////////////////////////////////////////////////////////
// local forward declarations
int resetFiles(DDSargs &rArgs);
int exit_gracefully(const char* errMsg);
void front2backSlash(tstring& pathStr);


#ifdef RK_STATIC /* license support */
RETURNCODE verify_serial_num( void );
RETURNCODE get_reg_value( const char* szKey, const char* szSubkey, const char* szValue );
#endif
/***************************************************************************
 * Global Data Types
 ***************************************************************************/

//CDDLDevice* getDevPtr(void) { return CDDIdeDoc::pThisDevice;};
unsigned long CDDIdeApp::firstID = _APS_NEXT_CONTROL_VALUE;// resource start number
unsigned long CDDIdeApp::nextID  = firstID;
unsigned long snIDVal; // part of sn used as a unique application id 28aug09

bool DumpOnly = false;// this is xmtr-dd's flag to dump and exit, needs to be somewhere...

const TCHAR RegDirName[] = _T("FieldComm Group Inc.");

// for affinity recordation
DWORD  processMask = 0;
DWORD  system_Mask = 0;
DWORD  newProcMask = 1;

// moved to aurguements.cpp for the xmtr-dd......int QandA = 0;


unsigned mainThreadID  = 0;

/////////////////////////////////////////////////////////////////////////////
// CDDIdeApp

BEGIN_MESSAGE_MAP(CDDIdeApp, CWinApp)
    //{{AFX_MSG_MAP(CDDIdeApp)
    ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
    //}}AFX_MSG_MAP
    // Standard file based document commands
//  ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//  ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
    ON_COMMAND(ID_FILE_NEW,  OnFileNew)
    ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
    ON_COMMAND(ID_FILE_PREFERENCES, OnFilePreferences)
    // Standard print setup command
    ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
    //ON_COMMAND(ID_VIEW_PV, OnViewPV)
    //ON_COMMAND(ID_VIEW_PVAO, OnViewPVAO)
    //ON_COMMAND(ID_VIEW_PVURV, OnViewPVURV)
END_MESSAGE_MAP()

/* this may only be temporary */
/* this is an override (see args.h) to move data from the registry to the ini file */
/* this will be called from the arg parsing function */
void DDSargs::CopyFromRegistry(void)
{   
    CString localStr, keyStr;
    BOOL r;
    HKEY hCUKey, hUKey;
    LONG vL;
    DWORD lL = _MAX_PATH;
    unsigned long uLtype = REG_SZ;
    /*unsigned*/ char tmpStr[_MAX_PATH];
    CDDIdeApp *pApp = (CDDIdeApp*)AfxGetApp();
    if (pApp == NULL)
        return;// error
    keyStr  = "Software\\";
    keyStr += RegDirName;
    keyStr += "\\sdc625\\";
    keyStr += INI_FILE_SECTION;

    if (  RegOpenKeyEx( HKEY_CURRENT_USER,
               keyStr, 0, KEY_ALL_ACCESS , &hCUKey ) != ERROR_SUCCESS  ) 
    {
        hCUKey = NULL;
    }
    if (  RegOpenKeyEx( HKEY_USERS,
               keyStr, 0, KEY_ALL_ACCESS , &hUKey ) != ERROR_SUCCESS   )
    {
        hUKey = NULL;
    }
    if (hUKey != NULL || hCUKey != NULL )
    {// we have something to do
        // for each argument
        
        for( OptionMap::iterator it = m_mOptions.begin(); it != m_mOptions.end(); it++ )
        {
            if ( it->second.getArgCnt() )
            {
                Argument* pOptArg = it->second.getFirstArg();// we will only move one of 'em
                if (pOptArg != NULL)
                {
                    if ( hCUKey != NULL )
                    {
                        vL = RegQueryValueEx(hCUKey, pOptArg->getIniKey().c_str(),  // value name
                                NULL,   /* reserved */   &uLtype   , /* always */
                                (unsigned char*)tmpStr, /* data buffer */     
                                &lL  /* size of data buffer*/   );
                        if ( vL == ERROR_SUCCESS )//        if exists in the registry
                        {// we got it
#ifdef _UNICODE
    string locStr(tmpStr);
    wstring locWstr;
    locWstr =  AStr2TStr(locStr);
                        //  put into the ini file
                            r = WritePrivateProfileString( // true at success
                                Arguments::IniFile_Section.c_str(),  // section name
                                pOptArg->getIniKey().c_str(),            // key name
                                locWstr.c_str(),   // string to add
                                pApp->m_pszProfileName  // initialization file 
                                //                        - Bill doesn't care, it'll go into SDC625. in the windir!
                            );
#else
                        //  put into the ini file
                            r = WritePrivateProfileString( // true at success
                                Arguments::IniFile_Section.c_str(),  // section name
                                pOptArg->getIniKey().c_str(),            // key name
                                (char*)tmpStr,   // string to add
                                pApp->m_pszProfileName  // initialization file 
                                //                        - Bill doesn't care, it'll go into SDC625. in the windir!
                            );
#endif
                            if ( r )
                            {// remove it from the registry
                                //r = RegDeleteKey(hCUKey,it->m_iniLocString.c_str());
                                r = RegDeleteValue(hCUKey,pOptArg->getIniKey().c_str());

                                if ( r != ERROR_SUCCESS )
                                {
                                    LPVOID lpMsgBuf;
                                    FormatMessage( 
                                        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
                                        FORMAT_MESSAGE_FROM_SYSTEM | 
                                        FORMAT_MESSAGE_IGNORE_INSERTS,
                                        NULL,
                                        GetLastError(),
                                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                                        (LPTSTR) &lpMsgBuf,
                                        0,
                                        NULL 
                                    );

                                    TRACE(_T("++Reg++ CU KEY %s DID NOT DELETE.(%s) +++\n"),
                                        pOptArg->getIniKey().c_str(),lpMsgBuf);

                                    LocalFree( lpMsgBuf );
                                }
                                if (hUKey != NULL)
                                {
                                    r = RegDeleteKey(hUKey, pOptArg->getIniKey().c_str());
                                    if ( r != ERROR_SUCCESS )
                                    {
                                        TRACE(_T("++Reg++ -U KEY %s DID NOT DELETE.+++\n"),
                                            pOptArg->getIniKey().c_str());
                                    }
                                }
                            }
                            else
                            {// error - didn't write
                                TRACE(_T("++Reg++ KEY %s DID NOT WRITE TO THE INI FILE.+++\n"),
                                    pOptArg->getIniKey().c_str());
                            }

                        }
                        else
                        {
                            TRACE(_T("++Reg++ CU KEY %s is NOT in the Registry +++\n"),
                                        pOptArg->getIniKey().c_str());
                        }
                    }
                    else
                    {// main key doesn't exist
                        TRACE(_T("++Reg++ NO CURRENT_USER KEY EXISTS.+++\n"));
                    }
                }
            }
            else
            {
                TRACE(_T("++Reg++ Option %s has No Args.+++\n"),it->second.GetName().c_str());
            }
        //  for( ArgVector::iterator itArg = it->second.m_vOpArguments.begin(); 
        //           itArg != it->second.m_vOpArguments.end();                itArg++ )
        //  {
        //      if( itArg->m_bOptional )
        //          cerr << " [" << itArg->m_iniLocString << "]";
        //  }
        }
    }
    else
    {// base key does not exist
        TRACE(_T("++Reg++ NO KEY EXISTS.+++\n"));
    }
}


CString rect2Registry(CRect& rect)
{
     wchar_t  szSize[MAX_REG_STRLEN];
     CString  toGo;

     _itow( rect.left, szSize, 10 );
     toGo = szSize;         toGo += "|";

     _itow( rect.top, szSize, 10 );
     toGo += szSize;        toGo += "|";

     _itow( rect.right, szSize, 10 );
     toGo += szSize;        toGo += "|";

     _itow( rect.bottom, szSize, 10 );
     toGo += szSize;        toGo += "|";

     return toGo;
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeApp construction

CDDIdeApp::CDDIdeApp()
{// _MAX_PATH = 260, SCRATCH_STR_LEN=512
    // Place all significant initialization in InitInstance
    tstring locStr;
    TCHAR tmpEnv[_MAX_PATH];
    DWORD tmpCnt = 0;
    m_CmdLine.clear();
    pDoc = NULL;

	prepTLS(sizeof(parseGlbl));// get the storage ready
	mainThreadID = THREAD_ID;

    if (m_pszProfileName)
    {
        free((void*)m_pszProfileName);
    }
    tmpCnt = GetEnvironmentVariable(_T("windir"), tmpEnv, _MAX_PATH);
    if ( tmpCnt != 0 )
    {
        locStr = tmpEnv;
        if (locStr[locStr.length()-1] != _T('\\'))
        {
            locStr += _T("\\");
        }
    }
    else
    {
        locStr = _T(".\\");
    }
    locStr += _T("_TestIni.ini");

    //Change the name of the .INI file.
    //The CWinApp destructor will free the memory.
    m_pszProfileName=_tcsdup(locStr.c_str());

    /* stevev 9-9-9 set the affinity for this process **/
    HANDLE thisProcessHandle = GetCurrentProcess();

    BOOL 
	success = GetProcessAffinityMask(thisProcessHandle,&processMask,&system_Mask);
    // calculate the the proper affinity here
//hold    success = SetProcessAffinityMask(thisProcessHandle,newProcMask);





#if 0
#define S 9
    // test new algorithm
    extern double roundDbl(double toBrounded, int numberOfPlaces);
    double outputA[S],
           outputB[S];
    /*
    double inputA[S]= {-1234.5678, -1234.44555, -123.3444554455, -123.34445544554, -123.344455444554
        , -12345.6789, -1225432.4443, -1225432123.4443, -1225432123432.4443};
    double inputB[S]= {-1234.4567, -1234.44544, -123.3444545455, -123.34445544545, -123.344455444545
        , -12354.6789, -1223455.4443, -1254532123.4443, -1254532123432.4443};
    */
    double inputA[S]= {1234.5678, 1234.44555, 123.3444554455, 123.34445544554, 123.344455444554
        , 12345.6789, 1225432.4443, 1225432123.4443, 1225432123432.4443};
    double inputB[S]= {1234.4567, 1234.44544, 123.3444545455, 123.34445544545, 123.344455444545
        , 12354.6789, 1223455.4443, 1254532123.4443, 1254532123432.4443};
    int    sizes[S] ={     0,         3,         5,              9,                10\
        ,     -1,           -4,              -7,                -10};

    for ( int i = 0; i < S; i++)
    {
        outputA[i] = roundDbl(inputA[i],sizes[i]);
        outputB[i] = roundDbl(inputB[i],sizes[i]);
    }

    exit;
#endif
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDDIdeApp object

CDDIdeApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {461795FB-971E-46E6-95D1-50B5588441E9}
static const CLSID clsid =
{ 0x461795fb, 0x971e, 0x46e6, { 0x95, 0xd1, 0x50, 0xb5, 0x58, 0x84, 0x41, 0xe9 } };

#ifdef ENUM_ALLOC_DEBUG
vector<hCenumDesc*> gblDescList;
#endif
/////////////////////////////////////////////////////////////////////////////
// CDDIdeApp initialization


#ifdef ENUM_ALLOC_DEBUG
int CDDIdeApp::Run()
{
    if (m_pMainWnd == NULL && AfxOleGetUserCtrl())
    {
        // Not launched /Embedding or /Automation, but has no main window!
        TRACE(traceAppMsg, 0, "Warning: m_pMainWnd is NULL in CWinApp::Run - quitting application.\n");
        AfxPostQuitMessage(0);
    }
    int R = CWinThread::Run();
#if 0
vector<hCenumDesc*>::iterator iT;//ptr2ptr2hCenumDesc
hCenumDesc* pED;
int y = 0, z = 0;;
for (iT = gblDescList.begin(); iT != gblDescList.end(); ++iT)
{   pED = *iT;
    if (pED != NULL)
    {
        z++;
        //LOGIT(COUT_LOG,L"%4d iD:%4d eqFromID:%4d ccFromID:%4d val:%3d Desc:%s\n",z, pED->myId,
        //  pED->eqFromID, pED->ccFromID, pED->val, wstring(pED->descS).c_str() );
        cout<<z<<"iD:"<<pED->myId<<" eqFromID:"<<pED->eqFromID<<" ccFromID:"<< pED->ccFromID <<
            " val:"<< pED->val << " Desc:"<< wstring(pED->descS).c_str() <<endl;
    }
    y++;
}
#endif // see exitinstance
    return R;
}
#endif

CDDIdeApp::~CDDIdeApp()
{
    LOGIT(CLOG_LOG,"App destructor finished.\n");
}

// NOTE: fudge to get what's needed way down in the DDlString

BOOL CDDIdeApp::InitInstance()
{
    AfxInitRichEdit2(); 
    CString rStr;
    // stevev 5/10/04 - we need to do this early so that the parser can use it to find defaults
    // Change the registry key under which our settings are stored.
    // such as the name of your company or organization.
    SetRegistryKey(RegDirName);

    int r = handleOptions(rStr);
    if ( r != 0 )// not success
    {   
        // we may have enough to work with
        LOGIT(CLOG_LOG|CERR_LOG," App.Init option error. '%s'\n",LPCTSTR(rStr));
        // exit_gracefully((LPCTSTR)rStr);
        // return FALSE;
    }
	
#ifdef GE_BUILD
//#ifdef donotuse_IS_OLE try replacing PAW 27/03/09
        // Initialize OLE libraries
    if (!afxContextIsDLL)   // PAW 02/04/09
        AfxInitialize();
        if (!AfxOleInit())
        {
            AfxMessageBox(IDP_OLE_INIT_FAILED);
            return FALSE;
        }

        AfxEnableControlContainer();
//#endif //IS_OLE try replacing PAW 27/03/09
#endif // we don't do OLE under most circumstances

    AfxEnableControlContainer();
    //END changes 
    // Standard initialization
    // If you are not using these features and wish to reduce the size
    //  of your final executable, you should remove from the following
    //  the specific initialization routines you do not need.
#if _MSC_VER < 1400 /* before VS8 (2005) */
#ifdef _AFXDLL
    Enable3dControls();         // Call this when using MFC in a shared DLL
#else
    Enable3dControlsStatic();   // Call this when linking to MFC statically
#endif
#endif // no longer needed

    LoadStdProfileSettings();  // Load standard INI file options (including MRU)

// we need some functions in the verify...#ifndef _DEBUG   /* allows debugging without a license */
// the ifdef _DEBUG bypass is located there
    // Let's verify that this executable has a valid serial number

	bool byPass = false;
	DWORD nameLen = MAX_COMPUTERNAME_LENGTH + 1;
	wchar_t compName[MAX_COMPUTERNAME_LENGTH + 1];
	if( GetComputerName( compName, &nameLen ) )
	{
		wstring cName(compName);
		if ( cName == L"STEVEV-OCTAGON" )
			byPass = true;
			
	}//else leave it false

    if( verify_serial_num() && !byPass)
    {
        // There is either no key in the registry or the serial number
        // is invalid.  No need for message box since verify_serial_num
        // will do that for us.
        LOGIT(CERR_LOG,"< Serial number failed validation.\n");//box is OK 22aug08
        // Return FALSE so that the application does not
        // instantiate.
        return FALSE;
    }
//#endif

    // Register the application's document templates.  Document templates
    //  serve as the connection between documents, frame windows and views.

/*  CSingleDocTemplate* pDocTemplate;
    pDocTemplate = new CSingleDocTemplate(
        IDR_MAINFRAME,
        RUNTIME_CLASS(CDDIdeDoc),
        RUNTIME_CLASS(CMainFrame),       // main SDI frame window
        RUNTIME_CLASS(CDDIdeTreeView));
    AddDocTemplate(pDocTemplate);*/

    // MDI interface - POB
/*  CMultiDocTemplate* pDocTemplate;
    pDocTemplate = new CMultiDocTemplate(
        IDR_SDC625TYPE,
        RUNTIME_CLASS(CDDIdeDoc),
        RUNTIME_CLASS(CTableChildFrame), // custom MDI child frame
        RUNTIME_CLASS(CDDIdeTreeView));
    AddDocTemplate(pDocTemplate);

    m_pTableTemplate = pDocTemplate;

    m_pWindowTemplate = new CMultiDocTemplate(
        IDR_SDCWNDVIEWTYPE,
        RUNTIME_CLASS(CDDIdeDoc),
        RUNTIME_CLASS(CWindowChildFrame),
        RUNTIME_CLASS(CSDC625WindowView));
    AddDocTemplate(m_pWindowTemplate);*/

    // Single Document, Multiple View interface - POB
    CSDMVTemplate* pDocTemplate = 
        new CSDMVTemplate( IDR_MAINFRAME, RUNTIME_CLASS( CDDIdeDoc ) );

    pDocTemplate->AddFrameTemplate(
    new CFrameTemplate(
        IDR_SDC625TYPE,
        RUNTIME_CLASS(CTableChildFrame),      
        RUNTIME_CLASS(CDDIdeTreeView),
        ID_TABLE_FRAME,
        SW_SHOWNORMAL,  // default paint as
        FALSE)  );  // was:: TRUE)              );// show this view at open...default view

    // stevev 22mar11 - only the last view registered is findable for a given frame
    // so we made the window findable and the error log a direct access to the template
    // the pointer is in the app so just getting the app's pointer, anybody can show the errors
    perrorlogtemplate = new CFrameTemplate(
        IDR_SDC625TYPE,
        RUNTIME_CLASS(CWindowChildFrame),       
        RUNTIME_CLASS(CErrLogView),
        ID_WINDOW_FRAME,
        SW_SHOWNORMAL,  // default paint as
        FALSE )   ;
    pDocTemplate->AddFrameTemplate( perrorlogtemplate );// don't show this view at open

    pDocTemplate->AddFrameTemplate(
    new CFrameTemplate(
        IDR_SDC625TYPE,
        RUNTIME_CLASS(CWindowChildFrame),       
        RUNTIME_CLASS(CSDC625WindowView),
        ID_WINDOW_FRAME,
        SW_SHOWNORMAL,  // default paint as
        FALSE )                     );// don't show this view at open

    // Create a default hidden view that never is destroyed, except on application close
    // or on File New.  It will ensure that a doc is always available.
    pDocTemplate->AddFrameTemplate(
    new CFrameTemplate(
        IDR_MAINFRAME,
        RUNTIME_CLASS(CChildFrame),       
        RUNTIME_CLASS(CSDC625HiddenView),
        ID_HIDDEN_FRAME,
        SW_HIDE,    // This view is always hidden.  Its purpose is keep the doc alive - always
        TRUE )                      );// show this view at open...but no one will see it

    m_pTableTemplate = m_pDocTemplate = pDocTemplate;

    AddDocTemplate(pDocTemplate);

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"<< App::Init Loading Frame.\n");

    // create main MDI Frame window - POB

    CMainFrame* pMainFrame = new CMainFrame;
    if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
    {
        LOGIT(CERR_LOG|CLOG_LOG," App::Init LoadFrame failed.\n");
        return FALSE;
    }
    m_pMainWnd = pMainFrame;

//NOTE: the splitter window generates the listview to go with this tree view

#ifdef donotuse_IS_OLE
    /* removed by sjv 7feb05*/
#endif // 0


//  if (! nRetCode )
//    {
//      exit_gracefully("Finished");
//    }

//  return nRetCode;
//    AfxMessageBox(m_lpCmdLine);
//    m_lpCmdLine="";
    // Parse command line for standard shell commands, DDE, file open
    CCommandLineInfo cmdInfo;
//  ParseCommandLine(cmdInfo);

#ifdef donotuse_IS_OLE
    /* removed by sjv 7feb05*/
#endif //IS_OLE


    // Dispatch commands specified on the command line
    if (!ProcessShellCommand(cmdInfo)) 
        return FALSE;

    // The one and only window has been initialized, so show and update it. *** DELETE ***
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::Init showing main window\n");

//  m_pMainWnd->ShowWindow(SW_SHOW);
    // The main window has been initialized, so show and update it.
    pMainFrame->ShowWindow(m_nCmdShow); // POB


    POSITION posDoc = pDocTemplate->GetFirstDocPosition();

    while( posDoc !=NULL )// get last one
    {
        pDoc = (CDDIdeDoc *)pDocTemplate->GetNextDoc(posDoc);
    }

    RECT priDisplayArea;

    BOOL retBool = SystemParametersInfo(SPI_GETWORKAREA, 0 ,&priDisplayArea, 0);
    // GetSystemMetrics() &  GetSystemInfo () are other great places to find info about a platform

    WINDOWPLACEMENT wps;
//  pMainFrame->GetWindowPlacement( &wps );

    CRect dfltWindow, useWindow;
    
    if( retBool )
    {
        dfltWindow = priDisplayArea;
        dfltWindow.DeflateRect((int)((priDisplayArea.right * .2)+.5),(int)((priDisplayArea.bottom * .2)+.5));
    }
    else
    {
        dfltWindow = CRect(110,110,750,590);
    }
    int   sz[4], s, y, c;
    wchar_t  clf[10];
    CString  sdfltWin, sWindowLoc;

    //sdfltWin = DFLT_WINDOW_SIZE;
    sdfltWin = rect2Registry(dfltWindow);

    sWindowLoc = GetProfileString( SETTINGS, MAIN_APP_WND_SIZE, sdfltWin );
    // was saved in   CDDIdeListView::WriteProfileSettings()   
    DEBUGLOG(CLOG_LOG,"Read  Registry: %ls with '%ls'\n",MAIN_APP_WND_SIZE,sWindowLoc);

    // string to rectangle...left, top, right, bottom
    for ( s = 0, c = 0, y = 0; y < sWindowLoc.GetLength(); y++)
    {
        if ( iswdigit(sWindowLoc[y]) )
        {
            clf[c++] = sWindowLoc[y];
            clf[c]   = _T('\0');
        }
        else
        if (sWindowLoc[y] == L'-')
        {
            ;// just eat the negative. We'll make it a positive and pray
#ifdef _DEBUG
//          assert(0);
#endif
        }
        else
        {
            sz[s++] = _wtoi(clf);
            c = 0;
        }
    }
    useWindow = dfltWindow;
    if (sz[0] < 0 || sz[0] > dfltWindow.right )
        useWindow.left = dfltWindow.left;
    else
        useWindow.left = sz[0]; 
    
    if ( sz[1] < 0 || sz[1] > dfltWindow.bottom)
        useWindow.top = dfltWindow.top;
    else
        useWindow.top = sz[1];

    if ( sz[2] <  useWindow.left || sz[2] > priDisplayArea.right )
        useWindow.right = dfltWindow.right;
    else
        useWindow.right= sz[2]; 
    
    if ( sz[3] < useWindow.top || sz[3] > priDisplayArea.bottom )
        useWindow.bottom = dfltWindow.bottom;
    else
        useWindow.bottom = sz[3];

    wps.rcNormalPosition = useWindow;
#ifdef _DEBUG
    sdfltWin = rect2Registry(useWindow);
    LOGIT(CLOG_LOG,"Set window to %ls\n",sdfltWin);
#endif

    pMainFrame->SetWindowPlacement( &wps ); 

/* now done automagically via OnNewDocument
    if ( pDoc ) ((CDDIdeDoc*)pDoc)->genDev();
*/

//  m_pMainWnd->UpdateWindow();
    pMainFrame->UpdateWindow(); // POB

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::Init Exiting\n");
    return TRUE;
}

//   -H usage info -doesn't work since we don't have a window
//   -O outputFile                          -- default stdout
//   -G loggingFile                         -- default .\_log.txt
//   -E errorFile                           -- default .\_err.txt
// -U Comm Port (comx or server 'url')      -- default COM1
// -B DatabaseDirectory   // ** required    -- default C:\WORK\HART\DDDB
// removed 07feb07 @ hart 7 // -K ddKey ie '37040200'                   -- default 28800500
// -P PollingAddress                        -- default 0
// -F isFaceplate                           -- default false
// -S is Simulation (overrides comm port/url)- default false
// -L language code                         -- default 'en'
// -I fills HART revision, key and device id (hr/[mm]mm/[dt]dt/dr/devid)-- default empty
//
extern int maxVer;// in ddbParserInfc.cpp 
int CDDIdeApp::handleOptions(CString& rStr)
{
    /* get command line args */
    int nRetCode = 0, tmp;
    CString fileSpec;

    TRACE(L"Entry 'handleOptions()\n");
    DDSargs theArgs(_T(""), _T("(C) 2002-2014 HART Communications Foundation"), _T("-/"));
    {   // Arguments parsed...
        if( theArgs.Parse() )
        {
            rStr = "Command line parser error.";
            TRACE(L"Command line parser error.");
            nRetCode = 1;
        }
    }
    CFileVersion fv;
    if (! nRetCode )
    {
        int trying = 1;
        fileSpec = theArgs.getAppDir().c_str();
        fileSpec+= theArgs.getAppName().c_str();
        
    
        if (fileSpec.Right(4) != ".exe") fileSpec+= ".exe";
        if (0 && fv.Open((LPCTSTR)fileSpec))// true on success
        {
            if ( fv.GetFixedInfo(m_CmdLine.cl_VersionInfo) )
            {                   
            #ifdef _DEBUG           
                //SDC 625  Version 2.0.5 build 32
                m_CmdLine.cl_verString = fv.GetFixedFileVersion();
            #else // release
                //SDC 625  Version 2.0.5 beta 12
                m_CmdLine.cl_verString = fv.GetFixedProductVersion();
            #endif
            }
        }
        else
        {                   
        #ifdef _DEBUG           
            //SDC 625  Version 2.0.5 build 32
            m_CmdLine.cl_verString = fv.GetResourceFileVersion();
        #else // release
            //SDC 625  Version 2.0.5 beta 12
            m_CmdLine.cl_verString = fv.GetResourceProductVersion();
        #endif
        }
    }

    // Instantiate required resources
/* stevev 9/25/03 convert to generic logging
    if (! nRetCode && resetFiles(theArgs) )
*/
    LPTSTR pCmdLine = GetCommandLine();
    if (! nRetCode && logSetup(theArgs, pCmdLine) )
    {
        rStr = "File reset Failed";
        nRetCode = 2;
    }
    else
    {
        atexit( logExit ); // be sure we get rid of 'em 15aug08

        tstring vs((LPCTSTR)m_CmdLine.cl_verString);
        //tcout<<vs<<endl;
        LOGIT(COUT_LOG,"%s\n",(LPCTSTR)m_CmdLine.cl_verString);
        tcerr<<vs<<endl;
        tclog<<vs<<endl;
          /* special for 0debugging IDE environment problem */
        DEBUGLOG(CLOG_LOG,L"* App's FileSpec: '%s'\n",(LPCTSTR)fileSpec); 
    }
    
    // 9-9-9 - log the affinity
    DWORD  tmpWord, y;
    int sizeInBits = sizeof(DWORD) * 8;
    wchar_t binStr[(sizeof(DWORD) * 8) + 1];
    binStr[sizeInBits]=0;
    tmpWord = system_Mask;
    for ( y = sizeInBits; y > 0; y--)
    {   if ( (tmpWord & 01) == 1 )
        {
            binStr[y-1] = 0x31;
        }
        else
        {
            binStr[y-1] = 0x30;
        }
        tmpWord = tmpWord >> 1;
    }
    LOGIT(CLOG_LOG,L"System  Affinity '%s'\n",binStr);

    tmpWord = processMask;
    for ( y = sizeInBits; y > 0; y--)
    {   if ( (tmpWord & 01) == 1 )
        {
            binStr[y-1] = 0x31;
        }
        else
        {
            binStr[y-1] = 0x30;
        }
        tmpWord = tmpWord >> 1;
    }
    LOGIT(CLOG_LOG,L"Process Affinity '%s'\n",binStr);

    tmpWord = newProcMask;
    for ( y = sizeInBits; y > 0; y--)
    {   if ( (tmpWord & 01) == 1 )
        {
            binStr[y-1] = 0x31;
        }
        else
        {
            binStr[y-1] = 0x30;
        }
        tmpWord = tmpWord >> 1;
    }
    LOGIT(CLOG_LOG,L"Current Affinity '%s'\n",binStr);

        // request option "h" and print out usage if set...
  //    if(!nRetCode && theArgs['h'] )
    if(!nRetCode && theArgs[_T('h')] )
    {
        theArgs.Usage();
        rStr = "";
        nRetCode = 3;
    }

    if (! nRetCode )
    {
        tstring sKey,extraInfo;
        
        if (theArgs[_T('I')])
        {//deal with the conglomeration    hr/mm/dt/dr/devid
            tstring sList[5];

            extraInfo = theArgs[_T('I')][_T("FldID")];
            sKey = extraInfo;
            // parse on '/' 
            string::size_type idx;
            int i;
            for (i = 0, idx = sKey.find('/'); 
                 idx != string::npos && i < 5;
                 idx = sKey.find('/'))
            {
                sList[i] = sKey.substr(0,idx);
                sKey     = sKey.substr(idx+1);
                i++;
            }// i indexes the next empty in slist
            if ( i > 5 ) // too many items
            {
                LOGIT(CERR_LOG,
                            "ERROR: -I option has too many fields.(%s) not used.\n",sKey.c_str());
            }
            else
            {
                sList[i++] = sKey; 
            }

            if ( i < 4 ) // too few items - we need device rev
            {               
                LOGIT(CERR_LOG,"ERROR: -I option only has %d fields (needs at least 4)\n",(i-1));
            }
            else         // juust right
            {
                if ( i > 0 )
                {
                    if ( _stscanf(sList[0].data(),_T("%x"),&(m_CmdLine.cl_Hrev)) == 0 ) // failed
                    {
                        m_CmdLine.cl_Hrev = 5;
                        m_CmdLine.srcHrev = Arguments::src_External;        
                        LOGIT(CERR_LOG,
                        "ERROR: -I option's HART revision is illegal ('%s')\n",sList[0].c_str());
                    }
                    else
                    if (m_CmdLine.cl_Hrev < 5 || m_CmdLine.cl_Hrev > 7)
                    {
                        m_CmdLine.cl_Hrev = 5;
                        m_CmdLine.srcHrev = Arguments::src_External;        
                        LOGIT(CERR_LOG,
                        "ERROR: -I option's HART revision is illegal ('%s')\n",sList[0].c_str());
                    }
                    //else - all is well
                }
                int m,t,r;

                if ( i > 3 )// 1 thru 3 have values
                {
                    if ( _stscanf(sList[1].data(),_T("%x"),&(m)) == 0 ) // failed
                    {
                        m = 0;
                        if (m_CmdLine.cl_Hrev < 7) // this doesn't matter to DD location in 7       
                        LOGIT(CERR_LOG,
                    "ERROR: -I option's HART manufacturer is illegal ('%s')\n",sList[1].c_str());
                    }
                    
                    if ( _stscanf(sList[2].data(),_T("%x"),&(t)) == 0 )
                    {
                        t = 0;      
                        LOGIT(CERR_LOG,
                    "ERROR: -I option's HART device type is illegal ('%s')\n",sList[2].c_str());
                    }
                    else
                    if ( _stscanf(sList[3].data(),_T("%x"),&(r)) == 0 )
                    {
                        r = 0;  
                        LOGIT(CERR_LOG,
                    "ERROR: -I option's HART device revision is illegal('%s')\n",sList[3].c_str());
                    }
                    else // - all is well
                    {
                    /* changed -sjv 08feb07
                    hrtKey = ((m & 0xFF)<< 24)+((t & 0xFF)<<16)+((r && 0xFF)<<8) + 0;
                    */
                        if ( m_CmdLine.cl_Hrev == 7 )
                        {
                            m_CmdLine.cl_Mfg    = m & 0xffff;
                            m_CmdLine.cl_DevType= t & 0xffff;
                            m_CmdLine.cl_DevRev = r & 0xff;
                            // source set from arguement parser
                        }
                        else
                        {
                            m_CmdLine.cl_Mfg    = m & 0xff;
                            m_CmdLine.cl_DevType= t & 0xff;
                            m_CmdLine.cl_DevRev = r & 0xff;
                            // source set from arguement parser
                        }
                    }
                }
                else
                {// they are worthless without all of 'em
                    //hrtKey = 0;
                    m_CmdLine.cl_Mfg    = 0;
                    m_CmdLine.cl_DevType= 0;
                    m_CmdLine.cl_DevRev = 0;
                    m_CmdLine.srcKey    = Arguments::src_External;
                }

                if ( i > 4 )// we have a device id too
                {
                    m_CmdLine.cl_DevID = sList[4].c_str();
                }
                else
                {
                    m_CmdLine.cl_DevID.Empty();
                }
            }
        }
        else
        {
            m_CmdLine.cl_Mfg    = 0;
            m_CmdLine.cl_DevType= 0;
            m_CmdLine.cl_DevRev = 0;
            m_CmdLine.srcKey    = Arguments::src_External;
            // hrtKey = 0;
            m_CmdLine.cl_DevID.Empty();
            m_CmdLine.srcDevID = Arguments::src_External;
            m_CmdLine.cl_Hrev = 5;// default hart rev
            m_CmdLine.srcHrev = Arguments::src_External;
        }
/************** no longer used 07feb07 for hart 7 ***************************************
        if ( ulKey == 0 && hrtKey == 0)
        {
            clog << "No valid key value found.(" << theArgs['K']["clDdkey"]<< ")" << endl;
            m_CmdLine.cl_Key = 0;
        }
        else
        if ( ulKey == 0 )       
        {
            m_CmdLine.cl_Key = hrtKey;
        }
        else // ulKey must have a value
        {
            m_CmdLine.cl_Key = ulKey;
        }
*****************************************************************************************/
        //const char *pa;
        //pa = theArgs['I']["databaseDirectory"].c_str();
        tstring clrStr;
        clrStr = theArgs[_T('B')][_T("databaseDirectory")];
        front2backSlash(clrStr);
        m_CmdLine.cl_DbP = clrStr.c_str();
        m_CmdLine.srcDbP = theArgs[_T('B')].getSrc(_T("databaseDirectory"));
        {// check for existance
            tstring locS = clrStr;
            
            if ( locS[locS.length()-1] == _T('\\') )
            {
                
                locS = locS.substr(0,locS.length()-2);
            }
            WIN32_FIND_DATA FindFileData;
            HANDLE hFind;

            hFind = FindFirstFile(locS.c_str(), &FindFileData);

            if (hFind == INVALID_HANDLE_VALUE) 
            {
                LOGIT(CERR_LOG|UI_LOG,"ERROR: the library directory was not found (%s)\n",
                    locS.c_str());
            } 
            else 
            {
                FindClose(hFind);
            }
        }
        
        clrStr = theArgs[_T('M')][_T("clHelpDir")];
        front2backSlash(clrStr);
        m_CmdLine.cl_HelpDir = clrStr.c_str();
        m_CmdLine.srcHlpDir  = theArgs[_T('M')].getSrc(_T("clHelpDir"));

        /* Get the Type of Modem */ /* VMKP Added */
        m_CmdLine.cl_Modem = theArgs[_T('C')][_T("Modem")].c_str();

        if(m_CmdLine.cl_Modem.GetLength())
        {
            if((m_CmdLine.cl_Modem.Compare(_T("HART")) != 0) &&
                (m_CmdLine.cl_Modem.Compare(_T("HONHARTDE")) != 0))
            {
                m_CmdLine.cl_Modem = "HART";
            }
        }
        else
        {
            m_CmdLine.cl_Modem = "HART";
        }
            /* End of VMKP */
        m_CmdLine.srcModem  = theArgs[_T('C')].getSrc(_T("Modem"));

        // deal with the rest
        m_CmdLine.cl_Url  = (theArgs[_T('U')][_T("CommPortPath")]).c_str() ;
        if (m_CmdLine.cl_Url.GetLength() <= 5 )
        {
            CString y;
            y = m_CmdLine.cl_Url.Left(3);
            if (y.CompareNoCase(_T("COM")) == 0)
            {
                m_CmdLine.cl_Url.MakeUpper();
            }
        }
        m_CmdLine.srcUrl  = theArgs[_T('U')].getSrc(_T("CommPortPath"));
        //
        m_CmdLine.cl_Lang = (theArgs[_T('L')][_T("langCode")]).c_str() ;
        m_CmdLine.srcLang = theArgs[_T('L')].getSrc(_T("langCode"));

        //
        if (theArgs[_T('S')]) 
        {m_CmdLine.cl_IsSim   = true;
         m_CmdLine.srcIsSim   = theArgs[_T('S')].getSrc(_T(""));
        }
        else
        {m_CmdLine.cl_IsSim   = false;
         m_CmdLine.srcIsSim   = Arguments::src_initVal;
        }
        
        //
        
        sKey = theArgs[_T('R')][_T("Restriction")] ;// reuse sKey for scratch string
        if ( ! sKey.empty() )
        {
            if ( sKey.substr(0,1) == ARG_RESTRICT_DFLT )
            {
                m_CmdLine.cl_Restriction = ARG_RESTRICT_DFLT;
                m_CmdLine.srcRestrict    = theArgs[_T('R')].getSrc(_T(""));
            }
            else
            if ( sKey.substr(0,1) == _T("L") )
            {
                m_CmdLine.cl_Restriction = _T("L");
                m_CmdLine.srcRestrict    = theArgs[_T('R')].getSrc(_T(""));
            }
            else
            {
                m_CmdLine.cl_Restriction = ARG_RESTRICT_DFLT;
                m_CmdLine.srcRestrict    = Arguments::src_initVal;
            }
        }
        else
        {
            m_CmdLine.cl_Restriction = ARG_RESTRICT_DFLT;
            m_CmdLine.srcRestrict    = Arguments::src_initVal;
        }

        //
        if (theArgs[_T('F')]) {m_CmdLine.cl_IsFcPlt = true;}else{m_CmdLine.cl_IsFcPlt = false;}
        //
        sKey = theArgs[_T('P')][_T("PollAddr")] ;// reuse sKey for scratch string
        if ( _stscanf(sKey.data(),_T("%d"),&(tmp)) == 0 ) // failed
        {
            // timj begin 04/05/04
            m_CmdLine.cl_PollAddr = EMPTY_POLL_ADDR;
            // timj end 04/05/04
        }
        else 
        if (tmp > 15)
        {
            // timj begin 04/05/04
            m_CmdLine.cl_PollAddr = EMPTY_POLL_ADDR;
            // timj end 04/05/04
            m_CmdLine.srcPollAddr = Arguments::src_initVal;
        }
        else //it's good
        {
            m_CmdLine.cl_PollAddr = tmp & 0xFF;
            m_CmdLine.srcPollAddr = theArgs[_T('P')].getSrc(_T("PollAddr"));
        }
        //
        tstring x = theArgs[_T('A')][_T("HostAddr ")];
        if ( (theArgs[_T('A')][_T("HostAddr ")])/*.c_str()*/ == _T("1")) 
        {
            m_CmdLine.cl_HstAddr = 1;
        }
        else
        {
            m_CmdLine.cl_HstAddr = 2;
        }
        m_CmdLine.srcHstAddr = theArgs[_T('A')].getSrc(_T("HostAddr"));
        m_CmdLine.cl_AppDir = theArgs.getAppDir().c_str();
        if (theArgs[_T('D')])
        {
            m_CmdLine.cl_IsDirDump = true;
            m_CmdLine.cl_dirName   = (theArgs[_T('D')][_T("DDdirFile")]).c_str();
        }
        else
        {
            m_CmdLine.cl_IsDirDump = false;         
            m_CmdLine.cl_dirName   = _T("");
        }
        //sjv 17may07-- HART || Modem Server usage:::::::::::::::
        sKey = theArgs[_T('V')][_T("HARTServerFirstChoice")] ;// reuse sKey for scratch string
        if ( _stscanf(sKey.data(),_T("%d"),&(tmp)) == 0 ) // failed
        {
            m_CmdLine.cl_UseServer = true;// default
            m_CmdLine.srcUseSrvr = Arguments::src_initVal ;
        }
        else 
        {
            if (tmp == 0)
                m_CmdLine.cl_UseServer = false;
            else 
                m_CmdLine.cl_UseServer = true;
            m_CmdLine.srcUseSrvr = theArgs[_T('V')].getSrc(_T("HARTServerFirstChoice"));
        }

        //
        
        if ( (theArgs[_T('N')][_T("Max_EFF_Ver")])/*.c_str()*/ == _T("5")) 
        {
            m_CmdLine.cl_MaxEFFver = 5;
        }
		else
        if ( (theArgs[_T('N')][_T("Max_EFF_Ver")])/*.c_str()*/ == _T("6")) 
        {
            m_CmdLine.cl_MaxEFFver = 6;
        }
		else
        if ( (theArgs[_T('N')][_T("Max_EFF_Ver")])/*.c_str()*/ == _T("8")) 
        {
            m_CmdLine.cl_MaxEFFver = 8;
        }
		else
        if ( (theArgs[_T('N')][_T("Max_EFF_Ver")])/*.c_str()*/ == _T("10") || 
             (theArgs[_T('N')][_T("Max_EFF_Ver")])/*.c_str()*/ == _T("A"))
        {
            m_CmdLine.cl_MaxEFFver = 10;
        }
        else
        {
            m_CmdLine.cl_MaxEFFver = SUPPORTED_VERSION_MAX;
        }
		maxVer = m_CmdLine.cl_MaxEFFver;// pass it to parser infc
        m_CmdLine.srcMaxVer = theArgs[_T('N')].getSrc(_T("Max_EFF_Ver"));

    }// endif not help request

    DEBUGLOG(CLOG_LOG,L"*Application Dir: '%s'\n",(LPCTSTR)m_CmdLine.cl_AppDir);
    DEBUGLOG(CLOG_LOG,L"*  Database Path: '%s'\n",(LPCTSTR)m_CmdLine.cl_DbP);

    TRACE("Exit  'handleOptions()\n");
    return 0;//success    /* special for debugging IDE environment problem */
}

#ifdef MEM_DEBUG
CMemoryState oneMemState, otherMemState, diffMemState;
CMemoryState* pMemSt = NULL;
extern FILE *logFile;
#endif

BOOL CDDIdeApp::OnIdle(LONG lCount)
{
#ifdef MEM_DEBUG
    if (pMemSt==NULL)
    {
        fprintf( logFile, "-- First Checkpoint-------------------------------------------\n");
        oneMemState.Checkpoint();
        pMemSt = &oneMemState;
    }
    else
    if (pMemSt== &otherMemState)
    {
        oneMemState.Checkpoint();
        fprintf( logFile, "-- One Checkpoint---------------------------------------------\n");
        if ( diffMemState.Difference( *pMemSt, oneMemState ) )
        {// they are not the same..
            if (diffMemState.m_lTotalCount > 10000000 )
            {
                TRACE(_T("** Idle Diff **\n")));
                ASSERT(0);// always
                diffMemState.DumpStatistics();
            }
            pMemSt = &oneMemState; // for next time
        }
    }
    else
    {
        otherMemState.Checkpoint();
        fprintf( logFile, "-- Other Checkpoint-------------------------------------------\n");
        if ( diffMemState.Difference( *pMemSt, otherMemState ) )
        {// they are not the same..
            if (diffMemState.m_lTotalCount > 10000000 )
            {
                TRACE(_T("** Idle Diff **\n")));
                ASSERT(0);// always
                diffMemState.DumpStatistics();
            }
            pMemSt = &otherMemState; // for next time
        }
    }
#endif // MEM_DEBUG
    if ( pDoc && ! ((CDDIdeDoc *) pDoc)->appCommEnabled())//m_autoUpdateDisabled )
    {
        return FALSE;
    }
    BOOL bMore = CWinApp::OnIdle(lCount);

    if ( ! bMore ) // if app idle is done processing
    {
        CDDIdeDoc * pDoc = NULL;
        if ( m_pDocManager != NULL )
        {
            POSITION posTemplate = m_pDocManager->GetFirstDocTemplatePosition();
            while ( posTemplate !=NULL )
            {   // get the next template
                CDocTemplate * pTemplate = 
                                    m_pDocManager->GetNextDocTemplate(posTemplate);
                POSITION posDoc = pTemplate->GetFirstDocPosition();
                while( posDoc !=NULL )
                {
                    pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
                }
            }
            if ( pDoc != NULL  
                        && pDoc->appCommEnabled() )// stevev 03apr07 tracing shutdown crash
                // && ! pDoc->m_autoUpdateDisabled )
            {
                bMore = pDoc->DoIdle(lCount);
            }
        }
        // else we have no doc manager - do nothing
    }
    // else just return the base request

    return bMore;
     // return TRUE as long as there is any more idle tasks

}


void CDDIdeApp::OnFileNew(void)
{
    TRACE(_T("CDDIdeApp::OnFileNew Entry      * Received an On File New Message.\n"));
    CDDIdeDoc* pD = NULL; //(CDDIdeDoc*)pDoc; POB, 3 Aug 2004
    int cnt = 0, usrRet;
    CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();

    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew entry, getting document.\n");
    
    /*
     * Do not call disableAppCommRequests() here. 
     * It will prevent all read/write communications 
     * after restarting of device, POB - 5/19/2014
     */

    // Update the pointer to the document
    pMainWnd->GetDocument();    // POB, 3 Aug 2004

    pD = pMainWnd->m_pDoc;      // POB, 3 Aug 2004

    if ( pD )
    {
        if (pD->pCurDev != NULL)
        {
            usrRet = AfxMessageBox(M_UI_CLOSE_DEVICE, MB_OKCANCEL|MB_ICONQUESTION);
            if(usrRet == IDCANCEL) // not  IDOK
            {
                LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew user cancelled from msg box.\n");
                return;// doing nothing
            }
            pD->m_enteredFileNew = true; // flag for next entry
/*Vibhor 020304: Start of Code*/    
    //Just give reasonable time for actions to finish
    //NOTE : In future we may disable File New on existing device!!!
            LOGIF(LOGP_START_STOP)
                            (CLOG_LOG,"< App::OnFileNew Sleep & allow other threads to finish.\n");
            systemSleep(500);
/*Vibhor 020304: End of Code*/
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew MainWnd.CloseDevice.\n");
            
            pMainWnd->CloseDevice();  // POB, 9 Oct 2004

            //pD->m_pTreeView->PostMessage(WM_SDC_CLOSEDEVICE, 0, 0);

            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew if MainWnd.Table, Activate it.\n");
            // timj begin 4/6/04
            pD->m_copyOfCmdLine.cl_PollAddr = EMPTY_POLL_ADDR;
            pD->m_copyOfCmdLine.cl_Url = "";
            // timj end 4/6/04

/* table should be closed just like the Windows - stevev 17mar11*/
            
            // This is a method to get the "view" menus updated
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew MainWnd.CreateWindowMenu.\n");
            pMainWnd->CreateWindowMenu();

        }
        else // isnull
        {
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew entered with device Ptr NULL.\n");

            pD->UpdateAllViews(NULL);

            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew yield to others.\n");
            systemSleep(10);

            pD->m_enteredFileNew = false;

            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew call doc's genDev "
                                                "(posts WM_SDC_GENERATEDEVICE to mainframe).\n");
            pD->genDev();

            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew posted generate.\n");
        }
    }
    else
    {
        // This happens at a clean startup (InitInstance() was just called)
        LOGIF(LOGP_START_STOP)
                    (CLOG_LOG,"< App::OnFileNew didn't get a document from the mainframe.\n");

        CWinApp::OnFileNew();
        
        // Expand child fully on "NEW"
        // This gives somewhat the same appearance as the 
        // previous SDC
/* table should be closed just like the Windows - stevev 17mar11
        if (pMainWnd->pTableChildFrm)
        {
            pMainWnd->pTableChildFrm->MDIMaximize();
        }
**/
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::OnFileNew Exiting.\n");
}

void CDDIdeApp::OnFileOpen()
{
    TRACE(_T("      * Received an On File Open Message.\n"));
    AfxMessageBox(M_UI_NO_OPEN_SAVED);
}

// timj begin 4/6/04
void CDDIdeApp::OnFilePreferences()
{
    
    CPrefDialog dlg;
    dlg.DoModal();
}
// timj end 4/6/04


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

#define SN_SIZE           30
#define SN_FIELD_SIZE      5
#define REGSTRING_SIZE     6

class CAboutDlg : public CDialog
{
public:
//  CAboutDlg();
    CAboutDlg(CWnd* pParent = NULL);

// Dialog Data
    //{{AFX_DATA(CAboutDlg)
    enum { IDD = IDD_ABOUTBOX };
     CString m_strSerialNum;
    CString m_strVersionNum;
    //}}AFX_DATA

    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAboutDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    //{{AFX_MSG(CAboutDlg)
     virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
CAboutDlg::CAboutDlg(CWnd* pParent) : CDialog(CAboutDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CAboutDlg)
       m_strSerialNum = _T("");
    m_strVersionNum = _T("");
    //}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CAboutDlg)
    DDX_Text(pDX, IDC_SERIAL_NUM, m_strSerialNum);
    DDX_Text(pDX, IDC_VERSION_NUM, m_strVersionNum);
    DDV_MaxChars(pDX, m_strVersionNum, 46);
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
    //{{AFX_MSG_MAP(CAboutDlg)
        // No message handlers
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CDDIdeApp::OnAppAbout()
{
    CAboutDlg aboutDlg;
    aboutDlg.DoModal();
}

// CDlgAbout message handlers

BOOL CAboutDlg::OnInitDialog()
{
     RETURNCODE nResult;
     CString strSection = _T("Info");

     CString strSNPrefix = _T("Serial Number: ");
     CString strQueriedSN;
     char    szSerialNum[ SN_SIZE + 1 ];
     char    szTemp[1024];
     CDDIdeApp* pApp = (CDDIdeApp*)AfxGetApp();

     CDialog::OnInitDialog();

     /*
      *   Now, go get the serial number.
      */
     memset( szTemp, 0, sizeof( szTemp ) );
     nResult = get_reg_value( "SDC625.Document", "PSN",  szTemp );

     /*
      *   Insert the dashes.
      */
     memset( szSerialNum, '-', sizeof( szSerialNum ) );
     memcpy( szSerialNum,      szTemp,      SN_FIELD_SIZE );
     memcpy( &szSerialNum[SN_FIELD_SIZE + 1],     &szTemp[SN_FIELD_SIZE],     SN_FIELD_SIZE );
     memcpy( &szSerialNum[SN_FIELD_SIZE * 2 + 2], &szTemp[SN_FIELD_SIZE * 2], SN_FIELD_SIZE );
     memcpy( &szSerialNum[SN_FIELD_SIZE * 3 + 3], &szTemp[SN_FIELD_SIZE * 3], SN_FIELD_SIZE );
     memcpy( &szSerialNum[SN_FIELD_SIZE * 4 + 4], &szTemp[SN_FIELD_SIZE * 4], REGSTRING_SIZE );
     szSerialNum[ sizeof( szSerialNum ) - 1] = '\0';

     

     if(/* ER_GENERAL == */ nResult )
     {
          m_strSerialNum.LoadString( IDS_SN_UNKNOWN );
     }
     else
     {
         wstring valW;
         string  valN(szSerialNum);
         valW = AStr2TStr(valN);
         // strQueriedSN.Format(_T("%ls"), szSerialNum );
         strQueriedSN = valW.c_str();
          m_strSerialNum = strSNPrefix + strQueriedSN;
     }

     // now do the version information
     m_strVersionNum = pApp->m_CmdLine.cl_verString;
    
     UpdateData( FALSE );

     return(TRUE);  // return TRUE unless you set the focus to a control
                    // EXCEPTION: OCX Property Pages should return FALSE
}
/////////////////////////////////////////////////////////////////////////////
// CDDIdeApp message handlers
/*
static ofstream outRedir;
static ofstream errRedir;
static ofstream logRedir;

static streambuf* pHldOutBuf = NULL;
static streambuf* pHldErrBuf = NULL;
static streambuf* pHldLogBuf = NULL;
*/
static ifstream in_Redir;
static streambuf* pHld_InBuf = NULL;
/*
int resetFiles(DDSargs &rArgs)
{
*/  /* cin  will be the input file
     * cout will be the output file <dflt stdout>
     * cerr will be the error file  <dflt stderr>
     * clog will be the logging file<dflt stderr>
     */
/*  string afile;
    afile = rArgs['O']["outputFileName"];

    cout << "cout mapped to " << afile << endl;
    outRedir.open(afile.c_str() );
    if ( outRedir.is_open() )
    {
        pHldOutBuf = cout.rdbuf();
        cout.rdbuf(outRedir.rdbuf());
    }
    else
    {
        cerr << "ERROR: output redirection failed." << endl;
    }
cout << "cout for "<< rArgs.getAppName() << endl;

    afile = rArgs['E']["errorFileName"];
    
    errRedir.open(afile.c_str());
    if ( errRedir.is_open() )
    {
        pHldErrBuf = cerr.rdbuf();
        cerr.rdbuf(errRedir.rdbuf());
        cout << "cerr mapped to " << afile << endl;
    }
    else
    {
        cerr << "ERROR: error file redirection failed." << endl;
    }
cerr << "cerr for "<< rArgs.getAppName() << endl;
    
 
    afile = rArgs['G']["logFileName"];

    logRedir.open(afile.c_str());
    if ( logRedir.is_open() )
    {
        pHldLogBuf = clog.rdbuf();
        clog.rdbuf(logRedir.rdbuf());           
        cout << "clog mapped to " << afile << endl;
    }
    else
    {
        cerr << "ERROR: logging file open failed." << endl;
    }
clog << "clog for "<< rArgs.getAppName() << endl;

    return 0; // show all OK even if it's not (defaults will be used)
}
*/

int exit_gracefully(const char* errMsg)
{
    /* stevev 9/25/03 - convert to generic logging 
    if (errMsg != NULL) cerr << errMsg << "\nExiting.\n" << endl;
    cout.flush();   if (pHldOutBuf != NULL ) cout.rdbuf(pHldOutBuf);
                    if (pHld_InBuf != NULL ) cin. rdbuf(pHld_InBuf);
    cerr.flush();   if (pHldErrBuf != NULL ) cerr.rdbuf(pHldErrBuf);
    clog.flush();   if (pHldLogBuf != NULL ) clog.rdbuf(pHldLogBuf);
    */
    logExit();
    if (pHld_InBuf != NULL ) cin. rdbuf(pHld_InBuf);

#ifdef _DEBUG
    cerr << "Press any key to exit." << endl;
    _getch();
#endif
#ifdef INI_OK
    //exit((errMsg != NULL)?1:0);
    closeIni();
#endif // INI_OK
    return ((errMsg != NULL)?1:0);//0;- we have to exit the app
}

int CDDIdeApp::ExitInstance() 
{
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< App::ExitInstance Exiting gracefully...     ");
                            logTime("");// isa logif start_stop (has newline)
                            vector<hCenumDesc*>::iterator iT;//ptr2ptr2hCenumDesc
#ifdef ENUM_ALLOC_DEBUG
hCenumDesc* pED;
int y = 0, z = 0;;
for (iT = gblDescList.begin(); iT != gblDescList.end(); ++iT)
{   pED = *iT;
    if (pED != NULL)
    {
        z++;
        LOGIT(COUT_LOG,L"%4d iD:%4d Parent:0x%04x eqFromID:%4d ccFromID:%4d val:%3d Desc:%s\n",
            z, pED->myId,pED->ownerID,
            pED->eqFromID, pED->ccFromID, pED->val, wstring(pED->descS).c_str() );
        //cout<<z<<"iD:"<<pED->myId<<" eqFromID:"<<pED->eqFromID<<" ccFromID:"<< pED->ccFromID <<
        //  " val:"<< pED->val << " Desc:"<< wstring(pED->descS).c_str() <<endl;
    }
    y++;
}
#endif


    exit_gracefully("");
    return CWinApp::ExitInstance();
}

void front2backSlash(tstring& pathStr)
{
    tstring::size_type idx;
    
    while (  (idx = pathStr.find(_T('/')) ) != tstring::npos  )
    {
        pathStr.replace(idx,1,_T("\\"));
    }
}


#ifdef RK_STATIC

/***************************************************************************
 * FUNCTION NAME: verify_serial_num
 *
 * DESCRIPTION: 
 *   This function reads the serial number from the registry and verifies
 * its validity.
 *
 * ARGUMENTS: 
 *   NONE
 *
 * RETURNS: 
 *   SUCCESS      Verification was successful
 *   FAILURE      Verification was unsuccessful
 ***************************************************************************/
RETURNCODE verify_serial_num()
{
    RETURNCODE   nResult = FAILURE;
    char         szSerialNum[MAX_PATH];
    char         szRegString[REG_STRING_SIZE + 1];
    char         szRegKey[REG_KEY_SIZE + 1 ];
    TCHAR      szTitle[1024];
    RKVALID      RKValid = RK_UNREGISTERED;
    HINSTANCE    hInst;

    hInst = AfxGetInstanceHandle();
    CWnd *pWnd = AfxGetApp()->m_pMainWnd;
       
    memset( szSerialNum, 0, sizeof( szSerialNum ) );
    memset( szRegString, 0, sizeof( szRegString ) );
    memset( szRegKey,    0, sizeof( szRegKey ) );
    LoadString( hInst, AFX_IDS_APP_TITLE, szTitle, sizeof( szTitle ) );

    /*
     * Read info from the registry
     */
    if( !get_reg_value( "SDC625.Document", "PSN", szSerialNum ) )
    {
        /*
         *   We know there is a key!
         */

        /*
         * Grab the strings neccessary for validation
         */
        memcpy( szRegString, &szSerialNum[REG_KEY_SIZE], sizeof( szRegString ) - 1 );
        memcpy( szRegKey, szSerialNum, REG_KEY_SIZE );

        // stevev 28aug09 - get an app id.sn uses letters, just add 'em up to get a number
        snIDVal = 1;
        unsigned long lastrnd = 0;
        for ( int y = 0; y < REG_KEY_SIZE && snIDVal > lastrnd; y++)
        {
            lastrnd = snIDVal;
            snIDVal = (snIDVal * 3 ) + szSerialNum[y];
        }
        if (snIDVal < lastrnd)// rolled over
        {
            snIDVal = lastrnd;
        }

        /*
         *   Is the registration key valid?
         */
        if( RegKeyValidate( szRegString,          /*  Registration String                    */
                               szRegKey,          /*  Registration Key                       */
                         VALIDATIONCODE,          /*  HARTOPC Validation code from KEYGEN    */
                               USERNAME,          /*  User's Name from 3rd Party             */
                                USERKEY,          /*  User's RegKey registration key         */
                               &RKValid ) )       /*  Variable to receive result             */
        {
           /*
            *   Let's find out if ithe key is valid.
            */
            if( RKValid )
            {
                nResult = SUCCESS;                
            }
            else
            {
#ifndef _DEBUG
                MessageBox( *pWnd, M_UI_BAD_SN, szTitle, MB_OK | MB_ICONSTOP );
#endif
            }
        }
    }
    else
    {
#ifndef _DEBUG
         MessageBox( *pWnd, M_UI_NOT_REGISTERED, szTitle, MB_OK | MB_ICONSTOP );
#endif
    }


#ifdef _DEBUG
// license short circuit....return SUCCESS;
    return SUCCESS;
#else
    return ( nResult );
#endif
}

/***************************************************************************
 * FUNCTION NAME: get_reg_value
 *
 * DESCRIPTION: 
 *   This function reads a value from the registry specified by the key 
 * arguments.
 *
 * ARGUMENTS: 
 *   char* szKey
 *   char* szSubkey
 *   char* szValue
 *
 * RETURNS: 
 *   SUCCESS     Operation succesfull
 *   FAILURE     Operation failed
 ***************************************************************************/
RETURNCODE get_reg_value( const char* szKey, const char* szSubkey, const char* szValue )
{
    RETURNCODE nResult = FAILURE;
    LONG    lRes;
    HKEY    hKey;

    /*
     * Copy keyname into buffer and add subkey if necessary.
     */
#ifdef _UNICODE
    TCHAR    szKeyBuf[ 1024 ];
    TCHAR    tmpSubKey[1024];
    AsciiToUnicode(szKey,szKeyBuf,1024);
    if( szSubkey != NULL )
    {
        AsciiToUnicode(szSubkey,tmpSubKey,1024);
        wcscat( szKeyBuf, L"\\" );
        wcscat( szKeyBuf, tmpSubKey );
    }
#else
    char    szKeyBuf[ 1024 ];
    strncpy(szKeyBuf, szKey   , 1024);
    if( szSubkey != NULL )
    {
        strcat( szKeyBuf, "\\" );
        strcat( szKeyBuf, szSubkey );
    }
#endif

    /*
     * Open the key so we can read it.
     */
    lRes = RegOpenKeyEx( HKEY_CLASSES_ROOT, szKeyBuf, 0, KEY_QUERY_VALUE, &hKey );

    if( ( lRes == ERROR_SUCCESS ) )
    {
        /*
         *   Read the Value
         */
        DWORD dwSize = 1024;//888 MAX_PATH;
        DWORD dwType = REG_SZ;
        /* changed 888
        RegQueryValueEx( hKey, NULL, 0, &dwType, (BYTE *)szValue, &dwSize );
        nResult = SUCCESS;
        end 888 removal */
        char* locCharPtr = (char*)szValue;
        RegQueryValueEx( hKey, NULL, 0, &dwType, (BYTE *)szKeyBuf, &dwSize );
        UnicodeToASCII(szKeyBuf, locCharPtr, 1024);
        nResult = SUCCESS;
    }

    return ( nResult );
}

#endif // RK_STATIC

/*************************************************************************************************
 *
 *   $History: DDIde.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */

/*void CDDIdeApp::OnViewWindow() 
{
    // get the main window, make sure it's an MDI Frame
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    // Create window to show PV
    pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)16390);
}

void CDDIdeApp::OnViewPV() 
{
    // get the main window, make sure it's an MDI Frame
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    // Create window to show PV
    pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)16390);  // 0x4006
}

void CDDIdeApp::OnViewPVAO() 
{
    // get the main window, make sure it's an MDI Frame
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    // Create window to show PV AO
    pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)16395);  
}

void CDDIdeApp::OnViewPVURV() 
{
    // get the main window, make sure it's an MDI Frame
    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    // Create window to show PV AO
    pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)16402);  
}
*/
unsigned long CDDIdeApp::getUniqueID(void)
{// this may be more complicated in the future
    return nextID++;
}

bool CDDIdeApp::isUniqueID(unsigned long id)
{
    return ( ( id >= firstID ) && ( id < nextID ) );
}
