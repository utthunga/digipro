/*************************************************************************************************
 *
 * $Workfile: VariableDlg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      VariableDlg.cpp : implementation file
 */

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h" // this includes DDLVariable.h
#include "CheckComboBox.h"
#include "VariableDlg.h"
#include "float.h" //Defns for FLT_DIG &
#include "ComboBox.h"
#include "MethodDlg.h"
#include "MainFrm.h"
#include "ColorEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVariableDlg dialog

/* stevev 09jun06 - make it an sdc Dialog

CVariableDlg::CVariableDlg(CDDLVariable* pVar, CWnd* pParent //=NULL)
    ): CDialog(CVariableDlg::IDD, pParent)
*/
/* stevev 5aug08 - make it use the device object
CVariableDlg::CVariableDlg(CDDLVariable* pVar, CWnd* pParent //=NULL)
*/
CVariableDlg::CVariableDlg(hCVar* pVar, BOOL parentRO, CWnd* pParent //=NULL)
    ): sdCDialog(CVariableDlg::IDD, pParent)
{
    ASSERT(pVar != NULL);

TRACE(_T("## Variable Dialog Constructing (in 0x%x)\n"),GetCurrentThreadId());
    //{{AFX_DATA_INIT(CVariableDlg)
    m_enumString = _T("");
    m_fVarEdit = 0.0f;
    m_nVarEdit = 0;
    m_uVarEdit = 0;
    m_strVarEdit = _T("");
    m_strDsply = _T("");
    //}}AFX_DATA_INIT

    m_pVar= pVar;
    //was m_pVar->m_pddbVar;
    parentISreadOnly = parentRO;

//    m_pnMin = NULL;
//    m_pnMax = NULL;
//    m_pfMin = NULL;
//    m_pfMax = NULL;
    m_nSize=0;
    m_dsplFormat= "";

    //m_pnMin = m_pVar->GetMin(m_nSize);
    //m_pnMax = m_pVar->GetMax(m_nSize);

    //m_pfMin = m_pVar->GetMinFloat(m_nSize);
    //m_pfMax = m_pVar->GetMaxFloat(m_nSize);
    nMax_Chars=0;
    nDecimal_Limit=0;

    
    theBoxType = unknownBoxType;// covers most of the 'elses in the following
    if (m_pVar)
    {
        int typeN = m_pVar->VariableType();
        switch ( typeN )
        {
        case vT_Integer:
        case vT_Unsigned:       // 3
        case vT_FloatgPt:       // 4
        case vT_Double:         // 5
        case vT_Ascii:          // 9
        case vT_PackedAscii:    //10 - HART only
        case vT_Password:       //11
        case vT_TimeValue:      //20
            {
                theBoxType = editBoxType;
            }
            break;
        case vT_Enumerated:     // 6
            {
                theBoxType = comboBoxType;
            }
            break;
        case vT_Index:          // 8
            {
               if ( ((hCindex *)m_pVar)->pIndexed != NULL )
               {
                  hCitemBase* pItm = NULL;
                  if (((hCindex *)m_pVar)->pIndexed->getArrayPointer(pItm) == SUCCESS)
                  {
                     if (pItm->getIType() == iT_ItemArray)
                     {
                         theBoxType = comboBoxType;
                    }
                    else
                    if ( pItm->getIType() == iT_Array || pItm->getIType() == iT_List )
                    {
                         theBoxType = editBoxType;// numeric
                    }
                    // else - should never happen - leave unknown
                  }
                  // else we have to leave it unknown
               }
               // else leave it unknown
            }
            break;
        case vT_BitEnumerated:  // 7
            {
                theBoxType = chkComboType;
            }
            break;
        case vT_HartDate:       //13 - HART only
            {
                theBoxType = dateBoxType;
            }
            break;
        default:
              // leave it unknown
            break;
        }// endswitch   
    }
    // else leave it unknown
}

CVariableDlg::~CVariableDlg()
{
    
TRACE(_T("## Variable Dialog Destructing (in 0x%x)\n"),GetCurrentThreadId());

/*<START>Added by ANOOP to fix the memory leaks */  
/*  if(NULL != m_pnMin )
    {
        delete  m_pnMin;
        m_pnMin=NULL;
    }

    if(NULL != m_pnMax )
    {
        delete  m_pnMax;
        m_pnMax=NULL;
    }

    if(NULL != m_pfMin)
    {
        delete  m_pfMin;
        m_pfMin=NULL;
    }

    if(NULL != m_pfMax)
    {
        delete  m_pfMax;
        m_pfMax=NULL;
    }
*/
/*<END>Added by ANOOP to fix the memory leaks   */  
}

void CVariableDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CVariableDlg)
    DDX_Control(pDX, IDC_VALUE_STATIC,    m_statDsply);
    DDX_Control(pDX, IDC_VARIABLE_EDIT,   m_cntrlEdit);
    DDX_Control(pDX, IDC_VARIABLE_COMBO,  m_cntrlCombo);
    DDX_Control(pDX, IDC_DATECTRL_VARDLG, m_DateTimeCtrl);

    DDX_CBString(pDX, IDC_VARIABLE_COMBO, m_enumString);
    DDX_Text(pDX, IDC_VALUE_STATIC, m_strDsply);
    //}}AFX_DATA_MAP
//DDX_Text(Exchange, controlTo process, source/target);
return;///temp...
/*    if (m_pVar->GetType() == TYPE_FLT)
    {
        DDX_Format(pDX, IDC_VARIABLE_EDIT, m_fVarEdit);
        DDV_MinMaxFloat(pDX, m_fVarEdit, m_pfMin, m_pfMax, m_nSize);
    }
    else if (m_pVar->GetType() == TYPE_INTEGER )
    {
        DDX_Format(pDX, IDC_VARIABLE_EDIT, m_nVarEdit);
        DDV_MinMaxInt(pDX, m_nVarEdit,m_pnMin , m_pnMax, m_nSize);
    }
    else if (m_pVar->GetType() == TYPE_UNSIGN_INT )
    {
        // Work to do - multiple mins & maxs & DDX_Format 
        DDX_Text(pDX, IDC_VARIABLE_EDIT, m_uVarEdit);
    }
    else if (m_pVar->GetType() == TYPE_INDEX )
    {
        DDX_Text(pDX, IDC_VARIABLE_EDIT, m_uVarEdit);
    }
    else if (m_pVar->GetType() == TYPE_STRING || m_pVar->GetType() == TYPE_PASSWORD) // POB, 9 May 2005
    {
        DDX_Text(pDX, IDC_VARIABLE_EDIT, m_strVarEdit);
        DDV_MaxChars(pDX, m_strDsply, m_pVar->GetCharLength());
    }
    else if (m_pVar->GetType() == TYPE_DATE)
    {
        DDX_Text(pDX, IDC_VARIABLE_EDIT, m_strVarEdit);
        DDV_MaxChars(pDX, m_strVarEdit, 8); //<ANOOP> 300104 Modifed to limit the text to mm/dd/yy 
    }
*/
}


BEGIN_MESSAGE_MAP(CVariableDlg, CDialog)
    //{{AFX_MSG_MAP(CVariableDlg)
    ON_WM_DESTROY()
    ON_CBN_SELCHANGE(IDC_VARIABLE_COMBO, OnSelchangeVariableCombo)
    ON_WM_TIMER()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVariableDlg message handlers

void CVariableDlg::DDX_Format(CDataExchange * pDX, int nIDC, /* sjv 08feb06 was::float*/double & value)
{
    DDX_Text(pDX, nIDC, value);
//  CString svalue, fvalue = m_pVar->GetEditFormat();
//  int     ivalue = (int)value;
// stevev 3oct07 - reduce the volumn of code...
//  if ( fvalue.FindOneOf("eEfgG") < 1 ) // not float
//  {
//      if ( fvalue.FindOneOf("sS") < 1 )// not string
//      {
//          svalue.Format( fvalue, ivalue);
//      }
//      else
//      {
//          svalue.Format( fvalue, ivalue);// error
//      }
//  }
//  else
//  {
//      svalue.Format( fvalue, value);
//  }

//  CEdit * pEdit = (CEdit *) pDX->m_pDlgWnd-> GetDlgItem(nIDC);
//  pEdit-> SetWindowText(svalue); //Send individual data to dialog
}

void CVariableDlg::DDX_Format(CDataExchange * pDX, int nIDC, int & value)
{
    DDX_Text(pDX, nIDC, value);
    CString svalue;

//    svalue.Format( m_pVar->GetEditFormat(),value);
//
//  CEdit * pEdit = (CEdit *) pDX->m_pDlgWnd-> GetDlgItem(nIDC);
//  pEdit-> SetWindowText(svalue); //Send individual data to dialog
}

void CVariableDlg::DDV_MinMaxInt(CDataExchange* pDX, int value, int* pMinVal, int* pMaxVal, UINT nSize)
{
    ASSERT(pMinVal !=NULL || pMaxVal!=NULL);

    for (UINT i =0; i < nSize; i++)
    {
        ASSERT(pMinVal[i] <= pMaxVal[i]);
    
        if ( ( value < pMinVal[0] ) || ( (value > pMaxVal[i]) && (value < pMinVal[i+1]) ) || 
             ( value > pMaxVal[nSize - 1] ) )
        {
            FailMinMaxWithFormat(pDX, (long *)pMinVal, (long *)pMaxVal, nSize,  _T("%ld"),
                AFX_IDP_PARSE_INT_RANGE);
        }
    }
}

void CVariableDlg::DDV_MinMaxFloat(CDataExchange* pDX, float value, float* pMinVal, float* pMaxVal, UINT nSize)
{
    double * pdMinVal = new double[nSize];
    double * pdMaxVal = new double[nSize];

    ASSERT(pMinVal !=NULL || pMaxVal!=NULL);
    UINT i; // PAW
    for (/*UINT PAW 03/03/09*/i =0; i < nSize; i++)
    {
        pdMinVal[i] = pMinVal[i];
        pdMaxVal[i] = pMaxVal[i];
    }
    
    for (i =0; i < nSize; i++)
    {
        ASSERT(pMinVal[i] <= pMaxVal[i]);
 
        if ( ( value < pMinVal[0] ) || ( (value > pMaxVal[i]) && (value < pMinVal[i+1]) ) || 
             ( value > pMaxVal[nSize - 1] ) )
        {
            FailMinMaxReal(pDX, pdMinVal, pdMaxVal, nSize, FLT_DIG,
                AFX_IDP_PARSE_REAL_RANGE);
        }
    }

    if (pdMinVal)
        delete[] pdMinVal;

    if (pdMaxVal)
        delete[] pdMaxVal;
}

void CVariableDlg::FailMinMaxWithFormat(CDataExchange* pDX,
     long* pMinVal, long* pMaxVal, UINT nSize, LPCTSTR lpszFormat, UINT nIDPrompt)
    // error string must have '%1' and '%2' strings for min and max values
    // wsprintf formatting uses long values (format should be '%ld' or '%lu')
{
    ASSERT(lpszFormat != NULL);

    if (!pDX->m_bSaveAndValidate)
    {
        TRACE(_T("Warning: initial dialog data is out of range.\n"));
        return;     // don't stop now
    }

    TCHAR szMin[255][32]; // Maximum of 255 rows of 32 columns,
    TCHAR szMax[255][32]; // 255 pairs of max and min in our DD

    CString prompt;
    CString temp;
    
    for (UINT i =0; i < nSize; i++)
    {
        wsprintf(szMin[i], lpszFormat, pMinVal[i]);
        wsprintf(szMax[i], lpszFormat, pMaxVal[i]);

        AfxFormatString2(temp, nIDPrompt, szMin[i], szMax[i]);
        prompt = prompt + temp + L"\n"; // group all mins and maxs together in one string

        if (i != nSize - 1)
            prompt = prompt + L"or\n";  // place an or between all conditionals exept last one.
    }

    AfxMessageBox(prompt, MB_ICONEXCLAMATION, nIDPrompt);
    prompt.Empty(); // exception prep
    pDX->Fail();
}

void CVariableDlg::FailMinMaxReal(CDataExchange* pDX,
     double*  pMinVal, double*  pMaxVal, UINT nSize, int precision, UINT nIDPrompt)
    // error string must have '%1' and '%2' in it
{
    if (!pDX->m_bSaveAndValidate)
    {
        TRACE(_T("Warning: initial dialog data is out of range.\n"));
        return;         // don't stop now
    }

    TCHAR szMin[255][32]; // Maximum of 255 rows of 32 columns,
    TCHAR szMax[255][32]; // 255 pairs of max and min in our DD

    CString prompt;
    CString temp;
    
    for (UINT i =0; i < nSize; i++)
    {
        _stprintf(szMin[i], _T("%.*g"), precision, pMinVal[i]);
        _stprintf(szMax[i], _T("%.*g"), precision, pMaxVal[i]);

        AfxFormatString2(temp, nIDPrompt, szMin[i], szMax[i]);
        prompt = prompt + temp + L"\n"; // group all mins and maxs together in one string

        if (i != nSize - 1)
            prompt = prompt + L"or\n";  // place an or between all conditionals exept last one.
    }

    AfxMessageBox(prompt, MB_ICONEXCLAMATION, nIDPrompt);
    prompt.Empty(); // exception prep
    pDX->Fail();
}

BOOL CVariableDlg::OnInitDialog() 
{   
TRACE(_T("## Variable Dialog Init (in 0x%x)\n"),GetCurrentThreadId());

    if ( theBoxType == unknownBoxType )// no variable pointer
    {
      // message box :ERROR: uneditable variable.
      return TRUE;
    }
    
	// stevev 18mar09 - try to get a unicode font into this dialog 
	//	set the dialog font
	// stevev 10jul2017 - make the font a part of the method so it is not destroyed 
	//                    before the controls it is used in.
	LOGFONT LogFont;

	memset(&LogFont, 0x00, sizeof(LogFont));
	wcscpy(LogFont.lfFaceName, CONST_STRING_FONT);//in 'SDC625.h' 
	// assume CONST_STRING_HIGT is in points
	// lfHeight is in logical units, use the formula for points to logical units::>
	LogFont.lfHeight = -MulDiv(CONST_STRING_HIGT, GetDeviceCaps(GetDC()->m_hDC, LOGPIXELSY), 72);

	m_Font.CreateFontIndirect(&LogFont);
	
	SetFont(&m_Font);
	/// end 10jul2017 addition

    RETURNCODE rt = SUCCESS;
    wstring label;
    if (m_pVar->Label(label) == SUCCESS)
    {
      SetWindowText(label.c_str());
    }

    sdCDialog::OnInitDialog();// has to be done early to initialize the controls

    int nIndex = 0; // Index to set the 32-bit value associated with combo box 
    int nEnum  = 0; // Initialize to zero

    COleDateTime dtMin = COleDateTime(1900, 1, 1, 0, 0, 0);
    COleDateTime dtMax = COleDateTime(2155, 12, 31, 0, 0, 0);
    m_DateTimeCtrl.SetRange(&dtMin, &dtMax);

//    m_fVarEdit   = *m_pVar;
//    m_nVarEdit   = *m_pVar;
//    m_uVarEdit   = m_nVarEdit; // convert to unsigned
//    m_strVarEdit = m_strDsply = *m_pVar;
//    m_strDsply = m_strDsply + "  " + m_pVar->GetUnitStr();

 
   if (theBoxType == chkComboType)  // bit enums only
   {
        m_cntrlEdit.ShowWindow(FALSE);   // Turn edit box off, this is misc
        m_DateTimeCtrl.ShowWindow(FALSE);// Turn the date off, time control
        m_cntrlCombo.ShowWindow(FALSE);  // Turn ComboBox off, this is an enum

        CArray<EnumTriad_t,EnumTriad_t&> subDataList;
        int valLen  = 0;
        ulong value = 0;
        
        /* this is the only call to this function...we'll put the functionality here
           if (m_pVar->GetBitEnumList(subDataList, valLen, value) == SUCCESS)
        */
        EnumTriad_t localData;
        hCEnum* pEn = (hCEnum*)m_pVar;

        value = pEn->getDispValue();
        hCenumList eBitEnumList(pEn->devHndl());
        if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS || eBitEnumList.size() <= 0 )
        {
            LOGIT(CERR_LOG, "ERROR: No eList for bit enum.\n");
        }
        else
        {//note: we have to go into a MSN array of triads NOT a triadList_t (no procureList)
            valLen = m_pVar->VariableSize();
            for (hCenumList::iterator iT = eBitEnumList.begin(); iT < eBitEnumList.end(); iT++)
            {
                ptr2EnumDesc_t pED = (ptr2EnumDesc_t)&(*iT);// PAW 03/03/09 added &*
                localData = *pED;// triad op=, 3 items transfered
                subDataList.Add(localData);
            }
            // Make sure to set the OnKillFocus set to False to make it visible all the time.
            m_pEnumCheckBox = new CCheckComboBox(1,1, &subDataList, value, valLen, FALSE);

            DWORD dwStyle = WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL    |WS_HSCROLL
                                             |CBS_DROPDOWNLIST;//| CBS_DISABLENOSCROLL

            dwStyle |= (CBS_OWNERDRAWVARIABLE | CBS_HASSTRINGS); 

            RECT rctB = {42,42,210,230};
            m_pEnumCheckBox->Create( dwStyle, rctB, this, 1);			
			m_pEnumCheckBox->SetFont(&m_Font);		// stevev 10jul2017
            m_pEnumCheckBox->ShowWindow(SW_SHOW);
            // done at the end...UpdateData(FALSE);  // Send data to dialog box
        }
   }
   else
   if (theBoxType == comboBoxType)// enums and indexes of item arrays
   {
        m_cntrlEdit.ShowWindow(FALSE);   // Turn edit box off, this is a numeric
        m_DateTimeCtrl.ShowWindow(FALSE);// Turn the date off, time control
		m_cntrlCombo.SetFont(&m_Font);	 // stevev 10jul2017
        m_cntrlCombo.ShowWindow(TRUE);   // Turn ComboBox ON , this is an enum

        /*
         * Now we need to populate the combox list box with all valid values.
         */

        triadList_t    listOfDesc;
        vector<EnumTriad_t>::iterator triIT;
        EnumTriad_t    *pTri = NULL;

        if (m_pVar->VariableType() == vT_Index)// or enum
        {// the constructor determined that this index variable pointed to a reference array
        // or it would not be a combo
            hCitemArray    *pArr = NULL;
            hCitemBase     *pIB  = NULL;

            if (((hCindex *)m_pVar)->pIndexed->getArrayPointer(pIB) == SUCCESS && 
                                                                pIB->getIType() == iT_ItemArray)
            {
                pArr = (hCitemArray*)pIB;
                if ( pArr->getDescriptionList(listOfDesc) != SUCCESS )
                {// size tested to give SUCCESS
                    listOfDesc.clear();// how we tell it failed
                }
            }
        }
        else    // must be enumerated
        {
            if (((hCEnum*)m_pVar)->procureList(listOfDesc)!= SUCCESS || listOfDesc.size()<= 0 )
            {
                listOfDesc.clear();// how we tell it failed
            }
        }

        /* 16jan12 - find the longest string to set how far to scroll
        -- they say its required to get the horiz scroll bar to appear
        */
        CString     str;
        CSize       sz;
        int         dx = 0;
        TEXTMETRIC  tm;
        CDC*        pDC   = m_cntrlCombo.GetDC();
        CFont*      pFont = m_cntrlCombo.GetFont();
        CFont*   pOldFont = pDC->SelectObject(pFont);// get the listbox font, save the old font
        pDC->GetTextMetrics(&tm);// Get the text metrics for avg char width
        // then do the string loading....(end of 16jan12 section)

        for(triIT = listOfDesc.begin(); triIT != listOfDesc.end();  ++triIT )
        {
            pTri = (EnumTriad_t*)&(*triIT);// PAW 03/03/09 added &*
            str  = pTri->descS.c_str();    // sjv new 16jan12- replaced below
            nIndex = m_cntrlCombo.AddString(str);
            //nIndex = m_cntrlCombo.AddString(pTri->descS.c_str());
            m_cntrlCombo.SetItemData(nIndex, pTri->val );//enum index to itemarray index map
            
            // the following added 16jan12
            sz = pDC->GetTextExtent(str);           
// we do this below     sz.cx += tm.tmAveCharWidth;// Add the avg width to prevent clipping
            if (sz.cx > dx)            // we just need the max
              dx = sz.cx;
            // end 16jan12 section
        }
        // more 16jan12 additions
        // Select the old font back into the DC
        pDC->SelectObject(pOldFont);
        m_cntrlCombo.ReleaseDC(pDC);

        dx +=  2*::GetSystemMetrics(SM_CXEDGE);
        if (m_cntrlCombo.GetCount() > NORMAL_DROP_ROWS)
        {
            dx += ::GetSystemMetrics(SM_CXVSCROLL) ;
            dx += 4;// needs a bit more to get separation from vert scroll bar
        }

        // looking in the resource file, the edit box is 111 pixels
        if ( dx > ( 2 * 111 ) )
        {
            m_cntrlCombo.SetDroppedWidth(2 * 111);
            m_cntrlCombo.SetHorizontalExtent(dx);
        }
        else
        {
            // Set the width of the list box so that every item is completely visible.
            m_cntrlCombo.SetDroppedWidth(dx);
        }



        // Set the horizontal extent so every character of all strings can 
        // be scrolled to.
//      m_cntrlCombo.SetHorizontalExtent(dx);
        // end 16jan12 section

        m_enumString = m_pVar->getDispValueString().c_str(); 
        wstring ws;    m_pVar->getUnitString(ws);
        m_strDsply   = m_enumString + "  " + ws.c_str();
        // done at the end...UpdateData(FALSE);  // Send data to dialog box

#ifdef _DEBUG
        DWORD dwStyle   = m_cntrlCombo.GetStyle();
        DWORD dwStyleEx = m_cntrlCombo.GetExStyle();
#endif
    }   
    /*<START>Added by ANOOP 05APR2004   */
    else 
    if (theBoxType == dateBoxType )
    {
/***        
        m_DateTimeCtrl.SetRange(&dtMin, &dtMax);

        string stTmpdate;
        m_cntrlEdit.ShowWindow(FALSE);   // Turn edit  box off, this is an integer, float, etc
        m_cntrlCombo.ShowWindow(FALSE);  // Turn combo box off, this is an enum                 
        m_DateTimeCtrl.ShowWindow(TRUE); // Turn the date  ON,  time control

        UINT32 nMonth=0,nDate=0,nYear=0;
        ((hChartDate *)m_pVar)->getDispDate(nYear, nMonth, nDate);
        
        COleDateTime tmpDateTime(nYear,nMonth,nDate,0,0,0); 
        if(tmpDateTime.GetStatus() == 1 || 
                tmpDateTime.GetStatus() == -1 ||
                    tmpDateTime.GetStatus() == 2    )
        {
            nDate  = 1;
            nMonth = 1;
            nYear  = 1900;
            COleDateTime tmpDtTime(nYear,nMonth,nDate,0,0,0);
            m_DateTimeCtrl.SetTime(tmpDtTime);
        
        }
        else
        {
            m_DateTimeCtrl.SetTime(tmpDateTime);
        }
        // done at the end...added..UpdateData(FALSE);  // Send data to dialog box
***/
        
        m_cntrlEdit.ShowWindow(TRUE);    // Turn edit box ON, this is a catch-all
        m_cntrlCombo.ShowWindow(FALSE);  // Turn combo box off, this is an integer, float, etc
        m_DateTimeCtrl.ShowWindow(FALSE);// Turn the date  off, time control

        // Get the edit control and set some initial properties for it.
        
        wstring locStr( m_pVar->getEditValueString() );
        nMax_Chars = 10;  // mm/dd/yyyy
        nDecimal_Limit = 1;
    
        CString strDisp_val(locStr.c_str());
        WORD    Parse_style = PES_DATE;
            
        m_cntrlEdit.SetLimitText(nMax_Chars); 

        m_strVarEdit = strDisp_val; // to all the DDX stuff to work
        m_cntrlEdit.SetWindowText(strDisp_val);
        m_cntrlEdit.SubclassEdit(0x7F, this, PES_DATE,nMax_Chars,nDecimal_Limit,nMax_Chars);
    }   
    else    /// its the catch-all edit box
    {
        m_cntrlEdit.ShowWindow(TRUE);    // Turn edit box ON, this is a catch-all
        m_cntrlCombo.ShowWindow(FALSE);  // Turn combo box off, this is an integer, float, etc
        m_DateTimeCtrl.ShowWindow(FALSE);// Turn the date  off, time control

        // Get the edit control and set some initial properties for it.
        int type;
        wstring eFmt;// to see if it uses # flag
        wstring locStr( m_pVar->getEditValueString() );
        //int     type  = (VarType)( m_pVar->getEditFormatInfo( nMax_Chars, nDecimal_Limit) );
        type = m_pVar->getEditFormatInfo(nMax_Chars, nDecimal_Limit, &eFmt);// for '#'
        // floats no longer default to      nMax_Chars=HANDHELD_FLOAT_MAXVAL_LEN (30jul08)
        //  their biggest +/- numbers are formatted and measured to an actual size
        CString strEdit_val(locStr.c_str());
        CString strDisp_val(m_pVar->getDispValueString().c_str());
        WORD    Parse_style = printfType2PES_type(type);

        if (m_pVar->IsPassword() )
        {
            m_cntrlEdit.SetPasswordChar('*');
        }   
            
        if (nMax_Chars >0)
        {
            m_cntrlEdit.SetLimitText(nMax_Chars); 
        }
        m_strVarEdit = strEdit_val; // to all the DDX stuff to work
        m_cntrlEdit.SetWindowText(strEdit_val);
        m_cntrlEdit.SubclassEdit(0x7F, this, Parse_style,nMax_Chars,nDecimal_Limit,nMax_Chars);
#ifdef _DEBUG
		int x =  m_cntrlEdit.GetDlgCtrlID( );
		TRACE(_T("## Variable Dialog Init Edit id 0x%02x (from 0x%x)\n"),x,GetCurrentThreadId());
#endif
    }// end else is catch-all edit box
    UpdateData(FALSE);  // Send data to dialog box

    /*
     * Get the menu item attributes. It will default to
     * the Variables attributes if not present
     */
    
//    CDDLBase *pParent = m_pVar->GetParent();

//  if (pParent != NULL)
//  {
        if (parentISreadOnly)
        {
           // Disable edit or combo box if handling is read-only (no edit allowed)
           // or qualifer was READ_ONLY
           m_cntrlEdit.EnableWindow(FALSE);
           m_cntrlCombo.EnableWindow(FALSE);
        }
//  }

/*<START>Commented by ANOOP for making the format of the edit box same as that of handheld 
    UpdateData(FALSE);  // Send data to dialog box
<END>Commented by ANOOP for making the format of the edit box same as that of handheld*/

TRACE(_T("## Variable Dialog Init Exit (from 0x%x)\n"),GetCurrentThreadId());

	DEBUGLOG(CLOG_LOG,"VAR init-dialog is done (Wnd:%08x).\n",m_hWnd);

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

void CVariableDlg::Update()
{
    wstring lS;
    lS = m_pVar->getDispValueString();
    //  m_strDsply   = *m_pVar;
    m_strDsply = lS.c_str();
    m_pVar->getUnitString(lS);
    //  m_strDsply = m_strDsply + "  " + m_pVar->GetUnitStr();
    m_strDsply = m_strDsply + "  " + lS.c_str();
    m_statDsply.SetWindowText(m_strDsply);
}

void CVariableDlg::OnDestroy() 
{
    CDialog::OnDestroy();

    // TODO: Add your message handler code here
    KillTimer(VRBL_TIMER);
}

void CVariableDlg::OnSelchangeVariableCombo() 
{
    // TODO: Add your control notification handler code here
/*    UpdateData(TRUE); // Get the new selection into m_enumString, etc

    int select = m_cntrlCombo.GetCurSel();
    int nEnum = m_cntrlCombo.GetItemData(select);

    *m_pVar = nEnum; // Update the value to the new selection.

    m_strDsply = *m_pVar;  // Update the display string from Variable

    UpdateData(FALSE);  // Send data to dialog box*/
}

void CVariableDlg::OnTimer(UINT nIDEvent) 
{
    // TODO: Add your message handler code here and/or call default

    Update();

    CDialog::OnTimer(nIDEvent);
}

void CVariableDlg::OnOK() 
{
    RETURNCODE rc = SUCCESS;
    CValueVarient value;
    int select;
    int nEnum;
    vector<EnumTriad_t> eList;
TRACE(_T("## Variable Dialog OK (from 0x%x)\n"),GetCurrentThreadId());
/* VMKP added on 310104, To return with out setting any Variable write value
     in case user has not changed any thing in the Edit control,  If user
    has edited the value and written the same old value, depends on the display
    format of the variable the value will be read and set.
    which may cause the original value
    to change in fraction format.  No gurantee that display format and edit format
    or same. The value is read on Edit format basis which may not be equal to
    Display format value in Fraction part, so UI shows value has changed with color 
    yellow as background */
    //UINT nType = m_pVar->GetType();

    //if( nType  ==  TYPE_INTEGER || nType  ==  TYPE_UNSIGN_INT ||
    //  nType  ==  TYPE_FLT)    //16APR2004 Modifed by ANOOP for index type variable
    /* this is seldom correct!!!!!!!!!!!!!!!!!!!!!
    if ( theBoxType == editBoxType ) // stevev 30jul08
    {
        if (!m_cntrlEdit.GetModify())// false == unchanged on screen
        {
          /x* stevev 09jun06 EndDialog(IDOK);*x/
          sdcEndDialog(IDOK);
          return;
        }
    }
    *** end bills'stuff doesn't work sometimes ***/
    m_cntrlEdit.SetModify(FALSE);// clear the modified flag - reason unknown...sjv 25jul-08

    /* VMKP added on 310104 */

  //prashant_begin
  
    if(TRUE == m_pVar->m_bLoopWarnVariable)
    {
            CString strLoopWarnMessage,strLoopWarnCaption;
            strLoopWarnMessage.LoadString(IDS_LOOP_WARN_VARIABLE_MESSAGE);
            strLoopWarnCaption.LoadString(IDS_LOOP_CHANGE_WARNING);
      int ret = MessageBox(strLoopWarnMessage,strLoopWarnCaption,MB_ICONEXCLAMATION|MB_YESNO);
      if(IDNO == ret)
      {
        return;
      }
    }

    BOOL valid = UpdateData(TRUE); // Deal with data exchange and controls
    if (valid == FALSE)
    {
        this->m_cntrlEdit.Undo();
        return;
    }

    int entryStatus = m_pVar->getWriteStatus();
    m_pVar->setWriteStatus(1);
    switch (theBoxType)
    {
    case dateBoxType:
    case editBoxType:
        {
            wstring ls;
            m_cntrlEdit.GetWindowText(m_strVarEdit);

            // No Need to trim spaces here.  validateTrim() will
            // handle all necessary triming, POB - 5/9/2014;

            ls = (LPCTSTR)m_strVarEdit;

            rc = m_pVar->setDisplayValueString(ls);
        }
        break;
    case chkComboType:
        {// bitenum
            nEnum = m_pEnumCheckBox->GetSelectedValue();
            value = nEnum;
            value.vIsUnsigned = true;// these are all unsigned.

            rc = m_pVar->setDisplayValue(value);
        }
        break;
    case comboBoxType:
        {// enum and index          
            select = m_cntrlCombo.GetCurSel();
            select = m_cntrlCombo.GetItemData(select); //get Index/enum value mapped to selection
            value = select;
            value.vIsUnsigned = true;// these are all unsigned.

            rc = m_pVar->setDisplayValue(value);
        }
        break;
// doesn't work
//  case dateBoxType:
//      {           
//          COleDateTime tmpOleDateTime;
//          m_DateTimeCtrl.GetTime(tmpOleDateTime);
//          UINT32 
//          nMonth = tmpOleDateTime.GetMonth(),
//          nDate  = tmpOleDateTime.GetDay(),
//            nYear  = tmpOleDateTime.GetYear();
//
//          rc = ((hChartDate *)m_pVar)->setDispDate(nYear, nMonth, nDate);
//      }
//      break;
    default:
            AfxMessageBox(M_UI_INVALID_TYPE);// should never happen
        break;
    }//endswitch

    if ( rc )
    {// problem: tell the user and reset the screen and exit with the box active
        m_pVar->setWriteStatus(entryStatus);// return it to original
        if ( rc == APP_STRING_NOT_ASCII )
        {
            AfxMessageBox(M_UI_NON_ASCII_STR);
        }
        else
        if( rc == APP_OUT_OF_RANGE_ERR )
        {
            AfxMessageBox(M_UI_INVALID_ENTRY);
        }
        else
        if( rc == APP_PARSE_FAILURE )
        {
            AfxMessageBox(M_UI_PARSE_NUM_FAIL);
        }
        else
        {
            AfxMessageBox(M_UI_INPUT_FAILURE);
        }
        
        CValueVarient vValue;
        vValue = m_pVar->getDispValue();
        m_fVarEdit   = //*m_pVar;
        m_nVarEdit   = //*m_pVar;
        m_uVarEdit   = (float)vValue;//(*m_pVar);//m_nVarEdit; // convert to unsigned
        wstring lS; lS = m_pVar->getDispValueString();
        m_strVarEdit = m_strDsply = lS.c_str();//*m_pVar;
        UpdateData(FALSE);//Send data from member variables to the Dialog
    }
    //else // all is well - exit
    //{     
    //leave it one  m_pVar->setWriteStatus(1);// user written
    //}

/*     
    if ( m_pVar->VariableType() == vT_BitEnumerated )
    // switch(m_pVar->GetType())
     {   case TYPE_INTEGER:
            value = m_nVarEdit;
            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;

        case TYPE_UNSIGN_INT:
            value = m_uVarEdit;
            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;

        case TYPE_INDEX:
        //<START>16APR2004Added by ANOOP to support index type variables 
            select = m_cntrlCombo.GetCurSel();
            select = m_cntrlCombo.GetItemData(select); // POB, 1 April 2005 - Index of control not always equal to DD index
            value = select;

            ((hCindex *)(m_pVar->m_pddbVar))->setDispValue(value);
            ((hCindex *)(m_pVar->m_pddbVar))->setWriteStatus(1);// user written
            //<END>16APR2004Added by ANOOP to support index type variables 
            break;

        case TYPE_FLT:
            value = m_fVarEdit;
            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;

        case TYPE_ENUM:
             select = m_cntrlCombo.GetCurSel();
            if (m_pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                value = (int) eList[select].val;

            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;

        case TYPE_BITENUM:
            nEnum = m_pEnumCheckBox->GetSelectedValue();
            value = nEnum;
            m_pVar->setDispValue(value);
            m_pVar->setWriteStatus(1);// user written
            break;

        case TYPE_STRING:
        case TYPE_PASSWORD: // POB, 9 May 2005
            value = m_strVarEdit.GetBuffer(m_strVarEdit.GetLength());
            m_strVarEdit.ReleaseBuffer();
            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;

        case TYPE_DATE:
            ((hChartDate *)pVar)->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            break;
        default:  // Nothing to do
            //<START>16APR2004Added by ANOOP to handle variables whose type is not supported
            value = m_uVarEdit;
            pVar->setDispValue(value);
            pVar->setWriteStatus(1);// user written
            //<END>16APR2004Added by ANOOP to handle variables whose type is not supported
            break;  
     }
*/

/* Prasad start of code 05/19/03 */
/*
    HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);

    if (hwndMainWindow != NULL)
    {
        ::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGED, NULL, NULL);
    }
*/
#ifdef _DEBUG
    extern CDDIdeDoc *  gblDoc;

    CMainFrame* pMainFrm = (CMainFrame*)this->GetTopLevelParent();
    if ( gblDoc != pMainFrm->m_pDoc )
    {
        TRACE(_T("******** VarDialog OnOK detected a mismatch of Document pointers.\n"));
    }
#endif
    
  //prshant: to fix the commit button enableing PAR
     if(m_pVar->isChanged())
     {
         CMainFrame* pMainFrm = (CMainFrame*)this->GetTopLevelParent();
         HWND hwndMainWindow  = 0;

         if ( pMainFrm != NULL && pMainFrm->m_pDoc != NULL && ::IsWindow(pMainFrm->m_hWnd) )
         {  
            pMainFrm->m_pDoc->m_bVariableEditedByUser = true;
            hwndMainWindow  = pMainFrm->GetSafeHwnd();
                         
            if (hwndMainWindow != NULL)
            {
            ::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, m_pVar->getID(), NULL);
            LOGIF(LOGP_NOTIFY)(CLOG_LOG,"Notify: Value Changed from Variable edit: 0x%04x\n",m_pVar->getID());
            }// else - nop
         }// else (no pointers) don't notify, return

     }// else (no change) don't notify, return

//  prashant end
//  CDialog::OnOK();

     // If the value was out of range or other setDisplayValue error, the message box has been
     // shown, the value has been reset to the display value, now lock the user into the modal
     // Dialog till he gets it right...[notes by sjv]  POB - 14 Aug 2008
     if (!rc) // no error
     {  /* stevev 09jun06 EndDialog(IDOK);*/
        sdcEndDialog(IDOK);
     }
}

void CVariableDlg::Float_Format_before_Display(CString strEdt_fmt,CString val,CString &strDisp_val )
{
    strDisp_val = val;
/**** stevev 16jan08 - this truncation is a little scary and we don't need it in modern displays
                     - if there is enough hoopla generated by leaving this off, we'll address it later
    int len=0;
    
    if ( strEdt_fmt.Find('%') != -1 )
    {
        char buf[10]={0};
        if(strEdt_fmt.Find('f') != -1)
        {
            if ( strEdt_fmt.Find('.') != -1)
            {
                CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find('.')-1);
                strcpy(buf,(LPCTSTR)tmp);
                len=atoi(buf);
                if ( len == 0 ) len=HANDHELD_FLOAT_MAXVAL_LEN;
            }   
            else
            {
                CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find('f')-1);
                if(tmp.IsEmpty())
                {
                    len=HANDHELD_FLOAT_MAXVAL_LEN;
                }
                else
                {
                strcpy(buf,(LPCTSTR)tmp);
                len=atoi(buf);
            }
                
            }
        }
//<START>Added by ANOOP 29MAR2004 to support the %g format  
        else if(strEdt_fmt.Find('g') != -1)
        {
            if ( strEdt_fmt.Find('.') != -1)
            {
                CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find('.')-1);
                strcpy(buf,(LPCTSTR)tmp);
                len=atoi(buf);
                if ( len == 0 ) len=HANDHELD_FLOAT_MAXVAL_LEN;
            }   
            else
            {
                CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find('g')-1);
                if(tmp.IsEmpty())
                {
                    len=HANDHELD_FLOAT_MAXVAL_LEN;
                }
                else
                {
                strcpy(buf,(LPCTSTR)tmp);
                len=atoi(buf);
            }
                
            }
        }

//<END>Added by ANOOP 29MAR2004 to support the %g format    

        
        if( val.Find('.') != -1)
        {
            CString tmpVal=val.Mid( 0,val.Find('.') );
            int tmpLen=tmpVal.GetLength();

            if( tmpLen  < len)
            {
                int Tmp= len - tmpLen;
                strDisp_val=val.Mid( 0,tmpLen + Tmp );
            }
            else if( tmpLen == len )
            {
                strDisp_val=val;
            }
            else if ( tmpLen  > len)
            {
                int Tmp=  tmpLen - len;
                strDisp_val=val.Mid( 0,tmpLen - Tmp );
            }

        }
        else
        {
            CString tmpVal=val.Mid( 0,val.GetLength() );
            int tmpLen=tmpVal.GetLength();

            if( tmpLen  < len)
            {
                int Tmp= len - tmpLen;
                strDisp_val=val.Mid( 0,tmpLen + Tmp );
            }
            else if( tmpLen == len )
            {
                strDisp_val=val.Mid( 0,tmpLen);
            }
            else if ( tmpLen  > len)
            {
                int Tmp=  tmpLen - len;
                strDisp_val=val.Mid( 0,tmpLen - Tmp );
            }

        }
    }
*************************************************************/
    return; 
} 

#ifdef _DEBUG
LRESULT CVariableDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{					   
	DEBUGLOG(CLOG_LOG,"VARProc: hWnd= %08x Msg: %04x Wparam: %02x Lparam: %04x\n",
								(unsigned)m_hWnd,message,wParam,lParam);
	LRESULT rty = sdCDialog::WindowProc(message, wParam, lParam);
	return rty;
}
#endif

/* terrible technique
void CVariableDlg::GetDateFromString(CString strDateTime)
{
    TCHAR pszValue[5];
    int nMonth=0,nDate=0,nYear=0;
    CString strMonth,strDate,strYear,strTemp;
    
    //month
    int nIndex = strDateTime.Find(_T('/'));
    strMonth = strDateTime.Left(nIndex);

    //date
    strDate = strDateTime.Mid(nIndex+1,2);

    //year
    strTemp = strDateTime.Right(4);
    strYear += strTemp;


    _tcscpy(pszValue,strMonth);
    nMonth = _ttoi(pszValue);

    _tcscpy(pszValue,strDate);
    nDate = _ttoi(pszValue);


    _tcscpy(pszValue,strYear);
    nYear = _ttoi(pszValue);


    COleDateTime tmpDateTime(nYear,nMonth,nDate,0,0,0); 
    if(tmpDateTime.GetStatus() == 1 || 
            tmpDateTime.GetStatus() == -1 ||
                tmpDateTime.GetStatus() == 2    )
    {
        nDate = 1;
        nMonth = 1;
        nYear = 1900;
        COleDateTime tmpDtTime(nYear,nMonth,nDate,0,0,0);
        m_DateTimeCtrl.SetTime(tmpDtTime);
    
    }
    else
    {
        m_DateTimeCtrl.SetTime(tmpDateTime);
    }

}
*/
/*************************************************************************************************
 *
 *   $History: VariableDlg.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
