#if !defined(AFX_METHODEDITCTRL_H__529270B8_E352_4207_BE93_08E6F2447D42__INCLUDED_)
#define AFX_METHODEDITCTRL_H__529270B8_E352_4207_BE93_08E6F2447D42__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MethodEditCtrl.h : header file
//
#define MAX_INT_DIGITS  20

#define VVSTRSIZE	64
/////////////////////////////////////////////////////////////////////////////
// CMethodEditCtrl window

#define DEFAULT_PRECISION   6

class CMethodEditCtrl : public CEdit
{
// Construction
public:
	CMethodEditCtrl();

// Attributes
public:

// Operations

	void SetEditCtrlType(int nEditType) ;

	BOOL Create(DWORD dwStyle /* includes PES_ style*/, const RECT& rect,CWnd* pParentWnd, UINT nID);

	BOOL GetValue(CValueVarient& varValue);

	// stevev 22may09 - get rid of uncontrolled VARIANTs BOOL SetValue(VARIANT& varValue)
	BOOL SetValue(CValueVarient& varValue);

	void SetValidValueString(const wchar_t *pStr);// stevev 22may09 & 01jun09
	void SetEditFormatString(const wchar_t *pStr);// stevev 26may09 & 01jun09

	void SetPrecision(int nPrecision); 

public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMethodEditCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMethodEditCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMethodEditCtrl)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);



private:

	//attributes
	int m_nEditCtrlType;
	int m_nPrecision;
	bool m_bIsUnsigned;

	  //style settings applicable to the edit control
	WORD    m_wMethodEditCtrlStyle; 
	wchar_t m_VVstr[VVSTRSIZE];
	wchar_t m_FmtStr[VVSTRSIZE];


	//operations
	BOOL CheckPrecision(CString strValue) ;
	double  ConvertToDouble(CString strText);
	__int64 ConvertToLong  (CString strText);


};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_METHODEDITCTRL_H__529270B8_E352_4207_BE93_08E6F2447D42__INCLUDED_)
