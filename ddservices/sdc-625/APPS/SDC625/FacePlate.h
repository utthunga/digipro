#if !defined(AFX_FACEPLATE_H__4EFE9BE0_64E6_4D52_9B4F_6C292CB3F781__INCLUDED_)
#define AFX_FACEPLATE_H__4EFE9BE0_64E6_4D52_9B4F_6C292CB3F781__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FacePlate.h : header file
//

#include "DDLBase.h"
#include "DDLVariable.h"
#include "SDC625TreeView.h"

#define MAX_VARIABLES 10

/////////////////////////////////////////////////////////////////////////////
// CFacePlate dialog

class CFacePlate : public CDialog
{
// Construction
public:
	CFacePlate(CDDIdeTreeView *pTreeView, CWnd* pParent = NULL);   // standard constructor
	~CFacePlate();

// Dialog Data
	//{{AFX_DATA(CFacePlate)
	enum { IDD = IDD_FACE_PLATE_NEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	void DisplayVariables();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFacePlate)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFacePlate)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CDDIdeTreeView	*m_pParent;
	CDDLVariable	*m_pVariables[MAX_VARIABLES]; // Maximum of 10 variables
	int				m_nVariableCount; 

	CStatic			*m_pLabels[MAX_VARIABLES]; // Dynamic creation of Variable Labels and their values.
	CEdit			*m_pValues[MAX_VARIABLES];
	CStatic			*m_pUnits[MAX_VARIABLES];
	CProgressCtrl	*m_pProgress;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FACEPLATE_H__4EFE9BE0_64E6_4D52_9B4F_6C292CB3F781__INCLUDED_)
