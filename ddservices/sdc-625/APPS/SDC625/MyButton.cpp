// MyButton.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "MyButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define BLUE RGB( 0, 0,127)
#define LIGHTRED RGB(255, 0, 0)
#define LIGHTGREEN RGB( 0,255, 0)
#define LIGHTBLUE RGB( 0, 0,255)
#define BLACK RGB( 0, 0, 0)
#define WHITE RGB(255,255,255)
#define GRAY RGB(192,192,192)

#define RED		RGB(255,0,0)
#define GREEN	RGB(128,128,128)
/////////////////////////////////////////////////////////////////////////////
// CMyButton

CMyButton::CMyButton()
{
	m_RedBrush.CreateSolidBrush(RED);
	m_GreenBrush.CreateSolidBrush(GREEN);
}

CMyButton::~CMyButton()
{
}


BEGIN_MESSAGE_MAP(CMyButton, CButton)
	//{{AFX_MSG_MAP(CMyButton)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyButton message handlers


HBRUSH CMyButton::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	HBRUSH hbr;	


	CDDIdeApp *pApp = ((CDDIdeApp*)AfxGetApp());

	if(pApp->m_bLED == FALSE)
	{
		pDC->SetBkColor(GREEN);
		hbr = m_GreenBrush;
	}
	else
	{
		pDC->SetBkColor(RED);
		hbr = m_RedBrush;
	}

	return hbr;
	// TODO: Return a non-NULL brush if the parent's handler should not be called
}
