#if !defined(AFX_DYND90LOGEX_H__CF5AF5E8_BD00_11D3_AA7C_0008C7083CA9__INCLUDED_)
#define AFX_DYNCONTROLS_H__CF5AF5E8_BD00_11D3_AA7C_0008C7083CA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynDialogEx.h : header file
//

#include "stdafx.h"
#include "sdcDialog.h"
#include "SDC625.h" 
#include "Dyn_DialogItemEx.h"
#include "STab_Ctrl.h"
#include "Window_Map.h"
#include "SDC625_drawTypes.h"
#include "Scope_Ctrl.h" // Added by ClassView
#include "URL_LinkButton.h"
#include "Dyn_DialogItemEx.h"
#include "GridCtrl.h"
#include "Window_Map.h"
#include "SDC625_drawTypes.h"

#include "ddbMenu.h"
#include "Bit_Enum.h"   // Added by ClassView

#include "BtnST.h"      // image button 9jan05 sjv

class CMainFrame;

// from the .cpp file 06jul12
static 
unsigned long BUTTONID[] = {WM_DLG_ABORT, WM_DLG_OK, WM_DLG_BUT01, WM_DLG_BUT02, WM_DLG_BUT03};
#define BUTTONGAP   5
#define PERCENT_OF_FULL_HIGHT   0.95  /* dialog opens at 85% of full screen window hight*/
#define PERCENT_OF_FULL_WIDTH   0.95  /* dialog opens at 98% of full screen window width*/
// ::these are the delta
#define COLSIZE    ( COLUMSTEPSIZE * 3 )        /*actually SubColumnStepSize * 3 */
#define ONECOL     ( COLSIZE + COLUMNGAP )
#define ONEROW     ( ROWSTEPSIZE + ROWGAP ) // 14 + 6 = 20 = x14
// end from the .cpp file


// Control styles
#define STYLE_EDIT                          (WS_VISIBLE | WS_CHILD | WS_TABSTOP | SS_LEFT)
#define STYLE_MULTIEDIT                 (WS_VISIBLE | WS_CHILD | WS_TABSTOP | SS_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | WS_VSCROLL | ES_WANTRETURN)
#define STYLE_STATIC                        (WS_VISIBLE | WS_CHILD | SS_LEFT)
#define STYLE_STATIC_CENTER             (WS_VISIBLE | WS_CHILD | SS_CENTER)
#define STYLE_STATIC_RIGHT              (WS_VISIBLE | WS_CHILD | SS_RIGHT)
#define STYLE_RADIO                         (WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON)
#define STYLE_RADIO_GROUP                   (STYLE_RADIO| WS_GROUP | WS_TABSTOP)
#define STYLE_BUTTON                        (WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON)
#define STYLE_GROUPBOX                      (WS_VISIBLE | WS_CHILD | BS_GROUPBOX)
#define STYLE_CHECKBOX                      (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | BS_AUTOCHECKBOX)

#define STYLE_COMBOBOX_DROPDOWN             (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_DROPDOWN     | CBS_AUTOHSCROLL | CBS_SORT ) // | CBS_DISABLENOSCROLL)
#define STYLE_COMBOBOX_DROPDOWN_NOSORT      (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_DROPDOWN     | CBS_AUTOHSCROLL) // | CBS_DISABLENOSCROLL)
#define STYLE_COMBOBOX_SIMPLE               (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_SIMPLE       | CBS_AUTOHSCROLL | CBS_SORT) // | CBS_DISABLENOSCROLL)
#define STYLE_COMBOBOX_SIMPLE_NOSORT        (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_SIMPLE       | CBS_AUTOHSCROLL) // | CBS_DISABLENOSCROLL)
#define STYLE_COMBOBOX_DROPDOWNLIST         (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | CBS_SORT  ) // | CBS_DISABLENOSCROLL)
#define STYLE_COMBOBOX_DROPDOWNLIST_NOSORT  (WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_DROPDOWNLIST | CBS_AUTOHSCROLL | WS_HSCROLL) // | CBS_DISABLENOSCROLL)
#define  STYLE_LISTBOX                      (WS_VISIBLE | WS_CHILD | WS_TABSTOP | LBS_STANDARD)

#define STYLE_DATETIMEPICKER                (WS_VISIBLE | WS_CHILD | WS_TABSTOP | DTS_RIGHTALIGN)
#define STYLE_TIMEPICKER                    (WS_VISIBLE | WS_CHILD | WS_TABSTOP | DTS_RIGHTALIGN | DTS_TIMEFORMAT)
// Default combo-style
#define STYLE_COMBOBOX                      (STYLE_COMBOBOX_DROPDOWNLIST)
#define STYLE_COMBOBOX_NOSORT               (STYLE_COMBOBOX_DROPDOWNLIST_NOSORT)

// Control Extended styles
#define EXSTYLE_EDIT                        (WS_EX_CLIENTEDGE)
#define EXSTYLE_MULTIEDIT                   (WS_EX_CLIENTEDGE)
#define EXSTYLE_LISTBOX                 (WS_EX_CLIENTEDGE)
#define EXSTYLE_STATIC                      (0)
#define EXSTYLE_RADIO                       (0)
#define EXSTYLE_BUTTON                      (0)
#define EXSTYLE_GROUPBOX                    (0)
#define EXSTYLE_CHECKBOX                    (0)
#define EXSTYLE_COMBOBOX                    (0)
#define  EXSTYLE_DATETIMEPICKER         (0)
#define  EXSTYLE_TIMEPICKER             (0)

/************************** moved to ddbMenuLayout.h
#define ROWSTEPSIZE      14//12 
#define FIRSTROW1        10
#define FIRSTROW2        37
#define FIXEDCOL1        10
#define FIXEDCOL2        65//120
#define INPUTCOL          0//150
#define GROWLIMIT         6
#define COLUMSTEPSIZE    70
#define ROWGAP            6
#define COLUMNGAP        12
#define BIT_SIZE         17  // was 18 POB - 31 March 2005
#define GRP_LEFT_ADJ     -5
#define GRP_BOTRGHT_ADJ  -2 // was -3
#define GRP_LEFT_ADJ     -5
#define TAB_LEFT_ADJ     -8

//following need to be made into equations using the above defines !!!!/
#define INITIAL_ROWHEIGHT       23  // first row has top & bottom margins 
#define ADDITIONAL_ROWHEIGHT    33  // other rows are added as-is 

#define COL_WIDTH               333 
**************************** end moved to ddbMenuLayout.h ***/

//#define MAX_COLS_PER_DESCR        25                      // Just a number..

typedef enum
{
    RIGHT = 0,
    LEFT
} DIRECTION;


/////////////////////////////////////////////////////////////////////////////
// CDynControls dialog

class CDynControls : public     sdCDialog// CDialog
{
protected:
    unsigned timerCount; // used to divide the timer tick
    int m_vCurHeight;// client coords
    int m_hCurWidth;// client coords
    int m_vScrollPos;
    int m_hScrollPos;
    int vScrollBarWidth;// const
    int hScrollBarWidth;// const
    long baseunitX;//   semiconst conversion factor
    long baseunitY;//   semiconst conversion factor
    CRect m_rect; // draw-area in client units
    RECT dskTopRect;//  const - client units
    int  vPage;// page sizes  - client units
    int  hPage;
    double columnMultiplier;
    // centering offset due to multiplier is COLSIZE*columnMultiplier

    RECT convRect; //.right is ONECOL; .bottom is ONEROW; .left is FIXEDCOL1; .top is FIRSTROW1

    virtual
    void sizeDialog(void);// does all the size acquisition, calculation and emplacement
    void resetBaseUnits(CRect& arect);// recalculates base x  y units
// Construction
public:
// standard constructor
//  CDynControls(CWnd* pWndParent = NULL, CRect * pPrevRect = NULL, BOOL bNextRow = TRUE);   
    CDynControls(pnt_t topLeft, double colMultiplier, CWnd* pWndParent = NULL,int xOffset = 0);

    ~CDynControls();

#ifdef _DEBUG
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
    {if (message==FNDMSG){LOGIT(CLOG_LOG,"~%");} 

    if ( message == WM_CHAR )
    {
        clog<<"~%";
    }
    return sdCDialog::WindowProc(message,wParam,lParam);};
#endif

    virtual CWnd *GetParent();
    
    int tmrHndl;

// Dialog Data
    //{{AFX_DATA(CDynDialogEx)
        // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
protected:
    virtual UINT AddDlgControl(DLGITEMTEMPLATECONTROLS TypeControl,
                                        LPCTSTR lpszCaption,
                                        DWORD dwStyle,
                                        DWORD dwExtendedStyle,
                                        LPRECT pRect = NULL,
                                        void *pData = NULL,
                                        UINT nID = 0);
//public: // POB - removed public, 2 Sep 2004
    virtual UINT AddDlgControl(LPCTSTR lpszClassName,
                                        LPCTSTR lpszCaption,
                                        DWORD dwStyle,
                                        DWORD dwExtendedStyle,
                                        LPRECT pRect = NULL,
                                        void *pData = NULL,
                                        UINT nID = 0);
    virtual UINT AddSubclassedDlgControl(LPCTSTR lpszClassName,
                                                     LPCTSTR lpszCaption,
                                                     DWORD dwStyle,
                                                     DWORD dwExtendedStyle,
                                                     LPRECT pRect = NULL,
                                                     UINT nID = 0);
    void SetWindowTitle(LPCSTR lpszCaption);
    void SetFont(CFont *pFont);
    void SetFont();
    CFont *GetFont();
    void SetFontSize(WORD wSize);
    WORD GetFontSize();
    void SetUseSystemButtons(BOOL bUse = TRUE);
    void SetUseModeless(BOOL bModelessDlg = TRUE);
    long GetNumberOfConrols() {return m_arrDlgItemPtr.GetSize();}

    //Additional functions by Tom Daffin
    void AddStyles(DWORD dwStyles);
    void RemoveStyles(DWORD dwStyles);

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDynControls)
public:
    virtual int DoModal();
//  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual void OnCancel();
    virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
    virtual void OnOK();
    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);  // Added OnNotify, POB - 5/6/2014
    //}}AFX_VIRTUAL
#ifndef _WIN32_WCE  // function missing PAW 05/05/09
    virtual void OnHelp();                                      // To be overridden..
#endif

private:
    CWnd       *m_pParentWnd;
    CWnd       *m_pDialogWnd;
    CMainFrame* m_pMainFrameClass; // the actual Class, donot use for MFC calls
    CString     m_strCaption;
    CFont      *m_pFont;
    WORD m_wFontSize;
    long m_nCurRow;
    long m_nCurColumn;
    BOOL m_bAddSystemButtons;
    int  m_ButtonCount;
    BOOL m_bIsFontCreated;
    BOOL m_bModelessDlg;
    BOOL m_bAddGroupBox;
    BOOL m_bAddTab;
    BOOL m_isCntrlsReady;
    int  m_DDLrow;
    int  m_DDLcolumn;

public:
    DLGTEMPLATE m_DialogTemplate;
    CArray<CDynDialogItemEx*, CDynDialogItemEx*>    m_arrDlgItemPtr;

// Implementation
public:
    void AppendMenu(CdrawBase * pBase, CString strMenu);
    void AddTabPage(CString strName, UINT nPage);
    void RemoveTabPage(UINT nPage);
	int  GetActiveTabPage(void){return m_Tab.GetCurSel();};
	void SetActiveTabPage(int newActive){m_Tab.SetCurSel(newActive);};
    void AddPageCtrl(UINT cntrlID, UINT page);
    CSTabCtrl * GetTabWnd();
    int BuildDlgTemplate(); // POB
    CWnd * GetDlgCtrl(UINT nID); // POB
    void AddDlgControl(CdrawBase * pBase, int offset); // POB - 1 Sep 2004
    CRect & Create(CWnd * pDialogWnd = NULL);  // POB
    CMenu * GetMenu();  // POB - 15 Sep 2004
    void SetCtrlRect(CRect & Rect, CdrawBase * pBase, int offset); 
    bool DlgUnits2Pixels(RECT* pRect);// returns true on success
    bool Pixels2DlgUnits(RECT* pRect);// returns true on success
    void setFirstPage(void);// stevev21aug08 get page zero to the front
    BOOL EnterIdle(void);   // PAW 03/03/09 added
    bool isCentered(void) { return (columnMultiplier != 0.0); };
    int  xOffset(void)    { return (int)((COLSIZE * columnMultiplier)+.5); };

protected:
    void GetDlgRect(LPRECT lpRect);
    void SetDlgRect(LPRECT lpRect);
    void SetDlgRectangle(LPRECT pRect);

    // Generated message map functions
    //{{AFX_MSG(CDynControls)
    virtual BOOL OnInitDialog();
    afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnMenuHelp();
    afx_msg void OnMenuLabel();
    afx_msg void OnMenuDisplayValue(); 
#ifndef _WIN32_WCE  // function missing PAW 05/05/09
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
#endif

    afx_msg /*BOOL PAW 03/03/09 */void OnEnterIdle(UINT nWhy, CWnd* pWho);
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar); 
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    //}}AFX_MSG
    afx_msg LRESULT OnHelpMsg(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnGetMethodUserInput(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDrawMethodUI(WPARAM wParam, LPARAM);
    /* hold 26oct afx_msg LRESULT OnDlgOK   (WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgAbort(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut01(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut02(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut03(WPARAM wParam, LPARAM);
    * end hold */
    afx_msg LRESULT OnVarChnge   (WPARAM wParam, LPARAM);
    afx_msg LRESULT OnStructChnge(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnValueChnge (WPARAM wParam, LPARAM);
    DECLARE_MESSAGE_MAP()

    /* temp 26oct*/
    afx_msg LRESULT OnDlgOK   (WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgAbort(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut01(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut02(WPARAM wParam, LPARAM);
    afx_msg LRESULT OnDlgBut03(WPARAM wParam, LPARAM);

    BOOL PreTranslateMessage(MSG* pMsg);
    
    LRESULT OnUpdate(UINT message, WPARAM wHint, LPARAM pHint); 
private:
    long m_nAvailLeftDlg;    // POB - 24 Aug 2004
    long m_nAvailTopDlg;     // POB - 24 Aug 2004
    long m_nAvailLeftColumn; // POB - 23 Aug 2004
    long m_nAvailTopRow;     // POB - 23 Aug 2004
    long m_nCurRowInit;      // POB - 23 Aug 2004
    void AddSystemButtons();
    int  CreateControls();
    BOOL Initialize();
    void StepRow();
    void StepColumn(DIRECTION nDirection = RIGHT);
    void ResetRow();
    void ResetColumn();

    CSTabCtrl m_Tab;  // There can only be one tab object per CDynControls object
    CScopeCtrl m_Scope;  // The design limits one CWnd control per CDynControls object, POB - 1 Sep 2004
    CURLLinkButton * m_pMenuButton; // The design limits one CWnd control per CDynControls object, POB - 2 Sep 2004
    CButtonST*  m_pImageButton;
    CPoint m_point;

/* stevev new 8/26/04 */
public:
    CdrawBase* pDraw;
// you can't do this... hCmenuItem mI;
    CRect  ReturnObjRect;

    ulong Generate(hCmenuItem* pMi, CdrawBase** pDrawItem);
    ulong Generate(menuItemDrawingStyles_t dS, CdrawBase** pDrawItem);
    ulong AddPage (hCmenuItem* pMi, CDynControls* pDynPage);
    ulong getNewCntrlID(void);
	CSTabCtrl* tabAccess(void);

    CdrawDlg *  m_pdrawDlg;
    hCitemBase* m_pIbLink;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNCONTROLS_H__CF5AF5E8_BD00_11D3_AA7C_0008C7083CA9__INCLUDED_)
