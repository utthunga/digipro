// Client.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"
#include "Client.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClient

#define MAX_LIST_ITEMS 2000

CClient::CClient(CDDIdeDoc *pDoc)
{
	m_bConnected = FALSE;
	m_pDoc = pDoc;
}

CClient::~CClient()
{
}
void CClient::Createnew()
{
	AfxSocketInit();
	if (!Create(0, SOCK_STREAM))
		TRACE(_T("\nSocket creation failed"));

	m_bConnected = Connect(_T("localhost"), 10);
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClient, CSocket)
	//{{AFX_MSG_MAP(CClient)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClient member functions

void CClient::SendMessage(char *strMessage,int nLength)
{
	if (m_bConnected)
		Send(strMessage,nLength);
}

void CClient::OnSend(int nErrorCode)
{

	CSocket::OnSend(nErrorCode);
}
void CClient::OnConnect(int nErrorCode)
{
	CSocket::OnConnect(nErrorCode);
}
void CClient::OnClose(int nErrorCode)
{
	if (m_pDoc)
		m_pDoc->m_bSocketConnected = FALSE;

	CSocket::OnClose(nErrorCode);
}

