// Preferences.cpp: implementation of the CPreferences class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "sdc625.h"
#include "Preferences1.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPreferences::CPreferences()
{
	Get();	// preferences object comes up initilaized
}

CPreferences::~CPreferences()
{

}

// we need to read and write by strings to be compatible with command line */
int  CPreferences::GetProfileInteger(TCHAR* key, int defVal)
{
	CWinApp *pApp = AfxGetApp();
	TCHAR *pref = _T("Modem Server Preferences");
	CString  tmpStr;
	int      tmpVal = 0;

	tmpStr = pApp->GetProfileString(pref, key, _T(""));
	if (tmpStr.IsEmpty())
	{
		return defVal;
	}
	else
	{
		if ( ! _stscanf((LPCTSTR) tmpStr, _T("%d"), &tmpVal) )
		{
			TRACE(_T("ERROR: sscanf failed.\n"));
			return defVal;
		}
		else
		{
			return tmpVal;
		}
	}

}
void CPreferences::WriteProfileInteger(TCHAR* key, int newVal)
{
	CWinApp *pApp = AfxGetApp();
	TCHAR *pref = _T("Modem Server Preferences");
	CString  tmpStr;
	//CString tKey;
	//AsciiToUnicode(key, tKey);

	tmpStr.Format(_T("%d"),newVal);
	pApp->WriteProfileString(pref, key, tmpStr);
}

void CPreferences::Get(void)
{
	CWinApp *pApp = AfxGetApp();
	TCHAR *pref = _T("Modem Server Preferences");
	SearchBy = pApp->GetProfileString(pref, _T("SearchBy"), _T("Poll Address"));

	SearchSerialPort = pApp->GetProfileString(pref, _T("SearchSerialPort"), _T("COM1"));

	SearchOption = GetProfileInteger(_T("SearchOption"), 1);
	SearchPollAddr = GetProfileInteger(_T("SearchPollAddr"), 0);
	SearchPollAddrStart = GetProfileInteger(_T("SearchPollAddrStart"), 0);
	SearchPollAddrEnd = GetProfileInteger(_T("SearchPollAddrEnd"), 0);
}


void CPreferences::Write(void)
{
	CWinApp *pApp = AfxGetApp();
	TCHAR *pref = _T("Modem Server Preferences");
	pApp->WriteProfileString(pref, _T("SearchBy"), SearchBy);
	pApp->WriteProfileString(pref, _T("SearchSerialPort"), SearchSerialPort);

	WriteProfileInteger(_T("SearchOption"), SearchOption);
	WriteProfileInteger(_T("SearchPollAddr"), SearchPollAddr);
	WriteProfileInteger(_T("SearchPollAddrStart"), SearchPollAddrStart);
	WriteProfileInteger(_T("SearchPollAddrEnd"), SearchPollAddrEnd);
}
