// FMA_reader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <vector>


#include "FMA.h"
#include "FM8.h"
#include "FMA_LitStrings.h" 
#include "FM8_LitStrings.h" 
#include "FMA_Dict.h" 
#include "FM8_Dict.h"
#include "Convert.h"
#include "logging.h"
#include "FMx_Support_primatives.h"
#include "FMA_Symbol.h"
#include "time.h"
#include "v_N_v+Debug.h"
#include <signal.h>

#include "globals.h" // includes TLStorage.h

using namespace std;

/* stevev 20feb14 moved these to the FMx.cpp file....
    they really need to be in the output functions that generate printout..*/

//FMx* daFile = NULL;		// this is global so we can get to the tables using get_A_TablePtr

int QandA = 0;// this is located in Arguments.cpp when building SDC or XMTR

unsigned mainThreadID  = 0;

static char setFileType(char type)
{
	obfuscateByteString(type == 'A');
	return type;
}
void SignalHandler(int signal)
{
	throw "Access Violation";
}

int _tmain(int argc, _TCHAR* argv[])
{
	RETURNCODE r = -1;
	int filenum = 1;
	char type = '0';

	if ( argc < 2 )
	{
		wprintf(L"Usage: %s filespec\n", argv[0]);
	}
	else
	{// 0 = app, 1 is first index
		/************************************************************************
		FMA_reader [-[cC]] [-[rR]] [-[iI]] [-[mM]] [-8] [-[aA]] filespec.fm[aA8]

		-c compress			rearanges the hex output
		-r reconcile		removes trailing file type from printed filename
							supresses the filesize from printed output
							supresses all hex output
							supresses the Fixed portion of an object from being printed
							supresses V8 ODES from being printed
							supresses most of the V8 device table output (string table etc)
							supressed most of the version 8 block table output
							supresses the 'annex' string // fma don't have annexes //
							supresses Action List header <asCactionListSpecA.h;ln 47>
							collapses the reference list output
							abbreviates status class output on bit-enums
							suppreses numeric for attribute type
							converts string type from enum to enum-ref & adjusts printout
							suppresses string table location
							supresses dictionary string location
							supresses attribute name <icCactionListElem.h;ln56>
													 <icCrefList.h;ln52>
							maskes error bit in CLASS
							--->huge number more<----
		-i print indexes	enables the string number and dictionary number to be printed
		-m maintenance		supresses the filesize from printed output
							supresses all hex output
							supresses the Fixed portion of an object from being printed
							supresses V8 ODES from being printed
							supresses the V8 device directory output (string table etc)
							supresses the 'annex' string // fma don't have annexes //
							supresses annex hex out  & heap size in header
							supresses signature from header
		-8 isa fm8 file
		-a isa fma file
		//// stevev added [-[sS]] xx suppress stuff, xx isa hex number without the '0x'
			see 'v_N_v+Debug.h' for full description of the bit usage
		************************************************************************/
		for ( int y = 1; y < argc; y++)
		{
			if( wcscmp(argv[y], L"-C") == 0 || wcscmp(argv[y], L"-c") == 0 )
			{
				compress=true;
			}
			else if ( wcscmp(argv[y], L"-R") == 0 || wcscmp(argv[y], L"-r") == 0 )
			{
				reconcile = true;
			}
			else if ( wcscmp(argv[y], L"-M") == 0 || wcscmp(argv[y], L"-m") == 0 )
			{
				maintenance = true;
			}
			else if (wcscmp(argv[y], L"-S") == 0 || wcscmp(argv[y], L"-s") == 0)
			{
				y++; // requires a bitmapped integer numeric to follow
				suppress = _wcstoi64(argv[y],NULL,16);// a base 16 (hex) number w/o 0x
			}
			else if (wcscmp(argv[y], L"-8") == 0)
			{
				// set based on the last letter of the filename, below
			}
			else if ( wcscmp(argv[y], L"-A") == 0 || wcscmp(argv[y], L"-a") == 0 )
			{
				// set based on the last letter of the filename, below
			}
			else// its gotta be the file name
			{
				filenum = y;
				int n = wcslen(argv[y]);
				if (wcscmp(argv[y] + n - 1, L"8") == 0)
				{
					type = setFileType('8');
				}
				else
				{
					type = setFileType('A');
				}
			}
		}

		if (reconcile)
		{
			suppress = suppress | 0x80;	// SUPPRESS_STRINGINDEX
		}

		/* initialize random seed: 
		 * rand() is used to force a difference
		 * if no table is detected in both A and 8.
		 * initializing rand in this manner gives a different first rand
		 * value each execution.
		 */
		srand ((unsigned) time(NULL));

		prepTLS(sizeof(parseGlbl));// get the storage ready
		mainThreadID = THREAD_ID;


		logSetup();
#ifdef _DEBUG
		logPer = LOGP_START_STOP;
#endif

		if (type == '0')
		{
			wprintf(L"Please specify the file type.  i.e. -8 or -A\n");
			LOGIT(COUT_LOG,L"Please specify the file type.  i.e. -8 or -A\n");
		}

		//FMx holds most of the info.  We have to make it global so the lower level classes
		//		can get table information
		// above  FMx            daFile;

		FMx* daFile;
		FMx_Dict* daStrings;
		FMx_Dict* daDict;

		if (type == 'A')
		{
			daStrings = new FMA_LitStrings;
			daDict = new FMA_Dict;

			daFile = new FMA;
		}
		else // must be 8
		{
			daStrings = new FM8_LitStrings;
			daDict = new FM8_Dict;

			daFile = new FM8;
		}
		
//		ConvertToString::setFilePtr(daFile);
		daStrings->saveBinary = daFile->saveBinary;
		daDict   ->saveBinary = daFile->saveBinary;

		if (reconcile == false && SUPPRESS_SIGNATURE==false)
		{
			LOGIT(COUT_LOG,L"%s Version %d.%d.%d\n",argv[0],VERSION_MAJOR,VERSION_MINOR,VERSION_BETA);
		}

		// convert linux file paths to MS
		for (_TCHAR *p = argv[filenum]; *p; *p++)
		{
			if (*p == '/')
				*p = '\\';
		}
		r = daFile->open(argv[filenum], NULL, daDict, daStrings);

		if (r != SUCCESS)
		{
			LOGIT(COUT_LOG,L"File '%s' failed to open\n",argv[filenum]);
			logExit();
			return r;
		}// else continue working
		pFILE = daFile;

		//if (reconcile)
		//{
		//	int n = wcslen(argv[filenum]);
		//	argv[filenum][n-1] = NULL;
		//}
		if (reconcile == false && SUPPRESS_SIGNATURE==false)
		{
			LOGIT(COUT_LOG, L"%sOpened '%s'\n", _T(FMAREADERLOG), argv[filenum]);
		}
		
		if (!SUPPRESS_SIGNATURE && !maintenance)
		{
			LOGIT(COUT_LOG, L"%s'%s' Size = %6u (0x%06x)\n\n", 
				_T(FMAREADERLOG), argv[1], daFile->fileSize, daFile->fileSize);
		}
		daFile->daSymbolTbl = new FMA_Symbol;
		daFile->daSymbolTbl->saveBinary = daFile->saveBinary;

		typedef void (*SignalHandlerPointer)(int);
		SignalHandlerPointer previousHandler;
		previousHandler = signal(SIGSEGV , SignalHandler);
		try
		{
captureStartTime();
		r = daFile->indexFile();	// get locations
LOGIF(LOGP_START_STOP)(CLOG_LOG,"TTTT Indexed File.");logTime();
		if (r == SUCCESS)
		{
			r = daFile->readIn_File();	// read payloads
LOGIF(LOGP_START_STOP)(CLOG_LOG,"TTTT Read In File.");logTime();

			if (r == SUCCESS)
			{
				r = daFile->process_File();	// interpret data
LOGIF(LOGP_START_STOP)(CLOG_LOG,"TTTT Processed File.");logTime();

				daFile->out();				// output to file
LOGIF(LOGP_START_STOP)(CLOG_LOG,"TTTT Output File.");logTime();
				daFile->release();			// release payloads
LOGIF(LOGP_START_STOP)(CLOG_LOG,"TTTT Released File.");logTime();
			}
		}

		delete daStrings;
		delete daDict;

	//		delete daFile;  // UNCOMMENT after we fix the unhandled exception
		}
		catch (char *msg)
		{
			fprintf(stderr, "CRASH: %s\n", msg);
		}

		logExit();
	}

	return r;
}

#define BytesPerLine  16

//            data      initial-offset   data-length
void outHex( UCHAR* pS, unsigned offset, unsigned len )
{
	if (reconcile  ||  maintenance || SUPPRESS_HEXDUMP)
	{
		return;
	}
	string s;
	if (pS == NULL || len == 0 )//  || len >0x7fff )
	{
		LOGIT(COUT_LOG,"- - - - - \n");
		return;
	}


	int lnCnt = 0;
	int chCnt = offset % BytesPerLine;

	if (compress)
	{
		chCnt = 0;
		while ( len > 0 )
		{
			if ( lnCnt % BytesPerLine == 0 )
			{
				if (lnCnt != 0 )// not the first char of first line
				{
					LOGIT(COUT_LOG," %s\n", s.c_str()); 
					s = "";
				}
			}
			LOGIT(COUT_LOG," %02x", *pS);
			if ( *pS > 0x1f && *pS < 0x7f )
			{
				s += *pS;
			}
			else
			{
				s += "?";
			}
			pS++;
			lnCnt++;
			len--;
			offset++;
		}//wend
	}
	else
	{	
		LOGIT(COUT_LOG,"0x%08x:",offset);// first offset
		if (chCnt != BytesPerLine )
		{
			for (int p = 0; p < chCnt; p++)
			{
				LOGIT(COUT_LOG,"   ");// space right to first char
				s += " ";
				lnCnt++;
			}
		}

		while ( len > 0 )
		{
			if ( lnCnt % BytesPerLine == 0 )
			{
				if (lnCnt != 0 )// not the first line
				{
					LOGIT(COUT_LOG," %s\n", s.c_str()); 
					LOGIT(COUT_LOG,"0x%08x:",offset);
					s = "";
				}
				//else// lncnt is zero so the first line starts at an even boundry
			}
			LOGIT(COUT_LOG," %02x", *pS);
			if ( *pS > 0x1f && *pS < 0x7f )
			{
				s += *pS;
			}
			else
			{
				s += "?";
			}
			pS++;
			lnCnt++;
			len--;
			offset++;
		}//wend
		while ( lnCnt % BytesPerLine != 0 )
		{
			LOGIT(COUT_LOG,"   ");
			lnCnt++;
		}
	}// endelse

	if (s.size() > 0)
	{
		LOGIT(COUT_LOG," %s\n", s.c_str()); 
	}
	else
	{
		LOGIT(COUT_LOG,"\n");
	}
}



// when no other stuff is wanted - i stick it here ---------------------------------

// from dbg_dllApi.cpp
/*  this is in dllapi as an inline.............................
void aExpressionList::destroy(void)
{
	aExpressionListElement* pele;
	FOR_this_iT(aExpressionListElement_t, expressionL)
	{
		pele = * iT;
		if (pele)
		{
			delete pele;
			*iT = NULL;
		}
	}; 
	aExpressionListElement_t::clear();  
	if (loadtype)
	{
		aPayldType::destroy();
	}
	else
	{
		DEBUGLOG(CERR_LOG,"Payload with no payload type.\n");
	}
};
***************/

/**********************************************************************************************
 *	19apr11 - implement some of dllapi.h code in ddbdefs.cpp precuding a near-total rebuild 4 each change
 *********************************************************************************************/
outStatusList_t& outStatusList_t::operator=(const outStatusList_t& src)
{
	outStatusList_td::const_iterator osIT;
	outStatusList_td::clear();
	if (src.size() > 0 )
	{
		for (osIT = src.begin(); osIT != src.end(); ++ osIT)
		{// ptr2outputStatus_t
			push_back(*osIT);
		}
	}// else, just clear and leave
	return (*this);
}

void outStatusList_t::clear(void)
{ 
	outStatusList_td::const_iterator osIT;
	outStatusList_td::iterator beg,dne;
	outputStatus_t* pOSC = NULL;
	int k = size();
	if ( ! k ) return;
	beg = begin();
	dne = end();
	for (osIT = beg; osIT != dne; ++ osIT)
	{
		pOSC = (outputStatus_t*) &(*osIT);
		pOSC->clear();
	}
	//erase(const_cast<struct oStatus_s *>(beg),const_cast<struct oStatus_s *>(dne));
	outStatusList_td::clear();
	erase(begin(),end());
}