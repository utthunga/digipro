#if !defined(AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_)
#define AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Client.h : header file
//


#include "afxsock.h"
/////////////////////////////////////////////////////////////////////////////
// CClient command target
class CDebugClientView;

class CClient : public CSocket
{
// Attributes
public:
	CDebugClientView *m_pParent;

// Operations
public:
	CClient(CDebugClientView *pParent);
	virtual ~CClient();
	void Createnew();
	BOOL SetFileName(CString strNewFile);
	CString GetFileName();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClient)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClient)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CString m_strFileName;
	CStdioFile 	*m_fp;
	int m_nListCount;
	BOOL	m_bTitle;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENT_H__8F86E6AA_8104_428D_9F01_B6ADF556ABBA__INCLUDED_)
