// DebugClientDoc.cpp : implementation of the CDebugClientDoc class
//

#include "stdafx.h"
#include "afx.h"
#include <afxtempl.h>
#include "CommLog.h"
#include "CommLogDoc.h"
#include "CommLogView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDebugClientDoc

IMPLEMENT_DYNCREATE(CDebugClientDoc, CDocument)

IMPLEMENT_SERIAL( CCommLog, CObject, 1 )
IMPLEMENT_SERIAL( CCommList, CObList, 1 )


BEGIN_MESSAGE_MAP(CDebugClientDoc, CDocument)
	//{{AFX_MSG_MAP(CDebugClientDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugClientDoc construction/destruction

CDebugClientDoc::CDebugClientDoc()
{
	m_pList = NULL;
}

CDebugClientDoc::~CDebugClientDoc()
{
	if (m_pList)
	{
		POSITION pos;
		for (pos = m_pList->GetHeadPosition(); pos != NULL;)
		{
			if (pos != NULL)
			{
				CCommLog *pLog = (CCommLog *)m_pList->GetNext(pos);
				delete pLog;
			}
		}
		delete m_pList;
	}
}

BOOL CDebugClientDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	SetTitle("");

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CDebugClientDoc serialization

void CDebugClientDoc::Serialize(CArchive& ar)
{
	CDebugClientView *pView = GetView();
	if (pView == NULL)
		return;
	
	CListCtrl &lstCtrl = pView->GetListCtrl();

	if (ar.IsStoring())
	{
		if (m_pList)
			delete m_pList;

		m_pList = new CCommList;

		for (int nCount = 0; nCount < lstCtrl.GetItemCount(); nCount++)
		{
			
			CCommLog *pLog = new CCommLog;
			
			pLog->strSequenceNo = lstCtrl.GetItemText(nCount, 0);
			pLog->strLog = lstCtrl.GetItemText(nCount, 1);

			m_pList->AddHead((CObject *)pLog);
			//delete pLog;
		}

		ar.WriteObject(m_pList);
	}
	else
	{
		lstCtrl.DeleteAllItems();

		if (m_pList)
			delete m_pList;

		m_pList = (CCommList *)ar.ReadObject(RUNTIME_CLASS(CCommList));
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDebugClientDoc diagnostics

#ifdef _DEBUG
void CDebugClientDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDebugClientDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

CDebugClientView* CDebugClientDoc::GetView()
{
	CDebugClientView* pView = NULL;
	POSITION pos = GetFirstViewPosition();
	while (pos != NULL)
	{
		pView = (CDebugClientView*)GetNextView(pos);
	}

   return pView;
}
/////////////////////////////////////////////////////////////////////////////
// CDebugClientDoc commands

