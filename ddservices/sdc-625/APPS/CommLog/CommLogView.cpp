// DebugClientView.cpp : implementation of the CDebugClientView class
//

#include "stdafx.h"
#include "CommLog.h"
#include "CommLogDoc.h"
#include "CommLogView.h"
#include <afxcview.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COL1WIDTH  60
#define COL2WIDTH	600
/////////////////////////////////////////////////////////////////////////////
// CDebugClientView

BOOL bGlobalFlag = FALSE;

IMPLEMENT_DYNCREATE(CDebugClientView, CListView)

BEGIN_MESSAGE_MAP(CDebugClientView, CListView)
	//{{AFX_MSG_MAP(CDebugClientView)
	ON_COMMAND(ID_FILE_LOG, OnFileLog)
	ON_UPDATE_COMMAND_UI(ID_FILE_LOG, OnUpdateFileLog)
	ON_COMMAND(ID_FILE_LOG_AS, OnFileLogAs)
	ON_UPDATE_COMMAND_UI(ID_FILE_LOG_AS, OnUpdateFileLogAs)
	ON_COMMAND(ID_VIEW_FONT, OnViewFont)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CListView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CListView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CListView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugClientView construction/destruction

CDebugClientView::CDebugClientView()
{
	m_pServer = NULL;
	m_dwDefaultStyle = WS_CHILD| WS_VISIBLE|  LVS_REPORT ;
	m_strCol1 = "Serial";
	m_strCol2 = "Data Stream";
	m_strLogFileName = "CommLog.log";
	m_bLogOn      = FALSE;
}

CDebugClientView::~CDebugClientView()
{
	if (m_pServer)
	{
		m_pServer->m_pParent = NULL;
		delete m_pServer;
	}
	m_font.DeleteObject();
}

BOOL CDebugClientView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	CListView::PreCreateWindow(cs);

	GetProfileSettings();

/*/ The following settings will intialize the font as
	// Courier, Regular, Size 10
	LOGFONT lf;                        // Used to create the CFont.
	memset(&lf, 0, sizeof(LOGFONT));   // Clear out structure.
	lf.lfHeight = -13;//3;                  // Request a 13-pixel-high font
	strcpy(lf.lfFaceName, "Courier");    //    with face name "courier".
	m_font.CreateFontIndirect(&lf);    // Create the font.*/

   // Use the font to paint a control. This code assumes
  
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CDebugClientView drawing

void CDebugClientView::OnDraw(CDC* pDC)
{
/*	CDebugClientDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	int m_PosX = 10;
	int m_PosY = 0;//10;

	CListCtrl& theCtrl = GetListCtrl();

	pDC->SelectObject(m_pPrintFont);

	for (int nCount = m_nPrintLine; nCount < theCtrl.GetItemCount(); nCount++)
	{
		m_PosY += 50;

		CString strSequenceNo = theCtrl.GetItemText(nCount, 0);
		CString strLog = theCtrl.GetItemText(nCount, 1);

		strLog.TrimLeft();

		pDC->TextOut( m_PosX,       m_PosY, strSequenceNo );
		pDC->TextOut( m_PosX + 160, m_PosY, strLog );
	}*/
}

/////////////////////////////////////////////////////////////////////////////
// CDebugClientView printing

BOOL CDebugClientView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CDebugClientView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add extra initialization before printing
/*	LOGFONT lfPrintFont;                        // Used to create the CFont.
	memset(&lfPrintFont, 0, sizeof(LOGFONT));   // Clear out structure.
	strcpy(lfPrintFont.lfFaceName, "Courier");    //    with face name "courier".
	
	m_nPointSize = 120;
	m_pPrintFont = new CFont;
	lfPrintFont.lfHeight = MulDiv(m_nPointSize, pDC->GetDeviceCaps(LOGPIXELSX), 720);
	m_pPrintFont->CreateFontIndirect(&lfPrintFont);

	CalcPageCount(pDC, pInfo);*/
}

void CDebugClientView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CDebugClientView diagnostics

#ifdef _DEBUG
void CDebugClientView::AssertValid() const
{
	CListView::AssertValid();
}

void CDebugClientView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CDebugClientDoc* CDebugClientView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDebugClientDoc)));
	return (CDebugClientDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDebugClientView message handlers


void CDebugClientView::OnInitialUpdate() 
{
	CListView::OnInitialUpdate();

	if (bGlobalFlag == FALSE)
	{
		bGlobalFlag = TRUE;

		m_pServer = new CServer(this);

		if (m_pServer)
			m_pServer->Createnew();
	}
	  

	// Create the list box
	   // this code only works for a report-mode list view
   ASSERT(GetStyle() & LVS_REPORT);

   // Gain a reference to the list control itself
   CListCtrl& theCtrl = GetListCtrl();
 
   theCtrl.SetFont(&m_font);
   theCtrl.DeleteAllItems();
   LV_COLUMN lvc;
   lvc.mask=LVCF_FMT | LVCF_WIDTH;

   for (int nCount = 0; theCtrl.GetColumn(nCount,&lvc); nCount++)
	   theCtrl.DeleteColumn(nCount);

   // Insert a column. This override is the most convenient.
   theCtrl.InsertColumn(0, m_strCol1, LVCFMT_LEFT);

   // The other InsertColumn() override requires an initialized
   // LVCOLUMN structure.
   LVCOLUMN col;
   col.mask = LVCF_FMT | LVCF_TEXT;
   col.pszText = (char *)(LPCTSTR)m_strCol2;
   col.fmt = LVCFMT_LEFT;
   theCtrl.InsertColumn(1, &col);

   // Set reasonable widths for our columns
//   theCtrl.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
//   theCtrl.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
   theCtrl.SetColumnWidth(0,  COL1WIDTH);
   theCtrl.SetColumnWidth(1,  COL2WIDTH);

   CDebugClientDoc *pDoc = (CDebugClientDoc *)GetDocument();
   
   CCommList *pList = pDoc->m_pList;
   if (pList == NULL)
	   return;

   POSITION pos;

   for (pos = pList->GetHeadPosition(); pos != NULL;)
	{
		if (pos != NULL)
		{
			CCommLog *pLog = (CCommLog *)pList->GetNext(pos);
			LV_ITEM   lv_item;

			char strTest[TXT_BUF_LEN];
			memset (strTest,0, TXT_BUF_LEN);

			lv_item.mask = LVIF_TEXT;
			lv_item.iSubItem = 0;

			CString strTemp = pLog->strSequenceNo;
			strcpy(strTest, strTemp.GetBuffer(strTemp.GetLength()));

			lv_item.iItem = 0; //Zero - Last in, last in the m_pList  /*nIdx*/
			lv_item.pszText = strTest;
			lv_item.lParam = NULL;
			lv_item.iImage = NULL;
			int nRow = theCtrl.InsertItem(&lv_item);

			strTemp = pLog->strLog;
			strcpy(strTest, strTemp.GetBuffer(strTemp.GetLength()));

			theCtrl.SetItem(nRow,1 ,LVIF_TEXT, strTest, 0,0,0,0);
		}
	}
}

void CDebugClientView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CListView::OnPrepareDC(pDC, pInfo);

	// There are 63 lines on each page using the font we have chosen
	// so we must start each page at a multiple of 64 (ie, Page 1 starts
	// at line zero, Page 2 starts at line 64
//	m_nPrintLine = (pInfo->m_nCurPage - 1) * 64;

}

void CDebugClientView::CalcPageCount(CDC* pDC, CPrintInfo* pInfo)
{
	// set up printing info structure
/*	CListCtrl& theCtrl = GetListCtrl();

	// figure out the page width and height
	// remember that the physical page size isn't completely
	// printable--subtract the nonprintable area from it.

	m_nPageWidth = pDC->GetDeviceCaps(PHYSICALWIDTH)
			- 2*(pDC->GetDeviceCaps(PHYSICALOFFSETX));
	m_nPageHeight = pDC->GetDeviceCaps(PHYSICALHEIGHT)
			- 2*(pDC->GetDeviceCaps(PHYSICALOFFSETY));

	// find out how high our font is
	// figure out how many lines of that font fit on a page

	int nHeight = MeasureFontHeight(m_pPrintFont, pDC);
 	int nPages;
	
	nPages = theCtrl.GetItemCount()/64 + 1;

	pInfo->SetMinPage(1);
	pInfo->SetMaxPage(nPages);*/
}

int CDebugClientView::MeasureFontHeight(CFont* pFont, CDC* pDC)
{ 
	// how tall is the identified font in the identified DC?

	CFont* pOldFont;
	pOldFont = pDC->SelectObject(pFont);

	CRect rectDummy;
 	CString strRender = _T("1234567890ABCDEF- ");
	int nHeight = pDC->DrawText(strRender, -1, rectDummy,
		DT_TOP | DT_SINGLELINE | DT_CALCRECT);
	pDC->SelectObject(pOldFont);

	return nHeight;
}

void CDebugClientView::Log(char * pStr, int ListCount)
{
	CString strSequenceNo;
	CString strLog = pStr;
	CString output;

	int const size = 2000;
	char buffer[size];

	if (m_bLogOn == TRUE)
	{
		strLog.TrimLeft();
		strLog.TrimRight();

		strSequenceNo.Format(_T("%03d"),ListCount);
		output = strSequenceNo + "   "+ strLog + "\n";

		// Copy info to data buffer
		lstrcpy(buffer, output);

		m_File.Write((LPCTSTR) output,output.GetLength());
	}
}

void CDebugClientView::OnFileLog() 
{
	CFileException ex;
	
	if (m_bLogOn == FALSE)
	{
		if ( !m_File.Open(m_strLogFileName,CFile::modeCreate| CFile::modeWrite /*|CFile::modeNoTruncate*/| CFile::shareDenyNone, &ex ) )
		{
			CString results;
			TCHAR szError[1024];
			ex.GetErrorMessage(szError, 1024);
			results.Format("File could not be opened.  %s",szError);
			AfxMessageBox(results);
		}
		else
		{
			m_bLogOn = TRUE;
		}
	}
	else
	{
		// Close the file
		m_File.Close();

		m_bLogOn = FALSE;
	}
}

void CDebugClientView::OnUpdateFileLog(CCmdUI* pCmdUI) 
{
	if (m_bLogOn)
	{
		pCmdUI->SetCheck(1);
	}
	else
	{
		pCmdUI->SetCheck(0);
	}
}

void CDebugClientView::OnFileLogAs() 
{
	CFileDialog Dlg(FALSE,".Log",m_strLogFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"Comm Log (*.log)|*.log|All Files (*.*)|*.*||",this );
	Dlg.DoModal();
	m_strLogFileName = Dlg.GetPathName();
}

void CDebugClientView::OnUpdateFileLogAs(CCmdUI* pCmdUI) 
{
	if (m_bLogOn)
	{
		// We don't want the user changing files during an active log
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
}

void CDebugClientView::OnViewFont() 
{
	LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));   // Clear out structure.
	m_font.GetLogFont(&lf);
	
	CFont chosenFont;
	// Gain a reference to the list control itself
    CListCtrl& theCtrl = GetListCtrl();
	
	// We don't want the user to try to clear color are any of the
	// other effects.  The default of the 2nd paramter is 
	// CF_EFFECTS | CF_SCREENFONTS. The  color, underline, strikethrough
	// is shown when CF_EFFECTS is presnet so don't use it. 
	CFontDialog dlg(&lf,CF_SCREENFONTS);

	if (dlg.DoModal() == IDOK)
	{
		m_font.DeleteObject();
		dlg.GetCurrentFont(&lf);
		m_font.CreateFontIndirect(&lf);
		theCtrl.SetFont(&m_font);
	}
}

void CDebugClientView::GetProfileSettings()
{
	CWinApp *pApp = AfxGetApp();

	// The following settings will intialize the font as
	// Courier New, Regular, Size 10
	CString strFaceName = "Courier New";
	int nHeight         = -13;
	int nWeight         = 0;

	LOGFONT lf;                        // Used to create the CFont.
	memset(&lf, 0, sizeof(LOGFONT));   // Clear out structure.

	// Get the font characteristics from the registry
    lf.lfWeight = pApp->GetProfileInt(SETTINGS, FONT_STYLE, nWeight);
	lf.lfHeight = pApp->GetProfileInt(SETTINGS, FONT_SIZE , nHeight);

    strFaceName = pApp->GetProfileString(SETTINGS, FONT, strFaceName);
	strcpy(lf.lfFaceName, strFaceName);    //    with face name "courier".

	m_font.CreateFontIndirect(&lf);    // Create the font.
}

void CDebugClientView::WriteProfileSettings()
{
	CWinApp *pApp = AfxGetApp();

	CString strFont = _T("");
	CString strFontStyle = _T("");
	CString strFontSize = _T("");
	int nStyle;
	int nSize;

	LOGFONT lf;
	
	// Get the current font charactersics
	m_font.GetLogFont(&lf);
	strFont = lf.lfFaceName;
	nStyle  = lf.lfWeight;
	nSize   = lf.lfHeight;

	// Save the font characteristics to the registry
    pApp->WriteProfileString( SETTINGS, FONT, strFont);
    pApp->WriteProfileInt(SETTINGS, FONT_STYLE, nStyle);
	pApp->WriteProfileInt(SETTINGS, FONT_SIZE, nSize);
}

void CDebugClientView::OnDestroy() 
{
	CListView::OnDestroy();
	
	WriteProfileSettings();
}
