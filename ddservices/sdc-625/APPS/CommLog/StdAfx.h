// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__5E5D1C10_7114_48A3_8976_E72C3AC2F990__INCLUDED_)
#define AFX_STDAFX_H__5E5D1C10_7114_48A3_8976_E72C3AC2F990__INCLUDED_
#pragma message("StdAfx apps/CommLog")
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef WINVER
 #ifdef _WIN32_WINNT
  // set WINVER based on _WIN32_WINNT
  #define WINVER          _WIN32_WINNT
 #else
  #define WINVER          0x0600
 #endif
#endif

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#define TXT_BUF_LEN		1024	/* takes the place of MAX_PATH (260) */

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__5E5D1C10_7114_48A3_8976_E72C3AC2F990__INCLUDED_)
