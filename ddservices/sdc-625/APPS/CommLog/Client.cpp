// Client.cpp : implementation file
//

#include "stdafx.h"
#include "CommLog.h"
#include "CommLogView.h"
#include "Client.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClient

#define MAX_LIST_ITEMS 2000

CClient::CClient(CDebugClientView *pParent)
{
//	m_strFileName = "c:\\temp\\SocketDump.txt";
	m_strFileName = "";
	m_fp = NULL;
	m_pParent = pParent;
	m_nListCount = 0;
	m_bTitle = FALSE;
}

CClient::~CClient()
{
	if (m_fp)
	{
		m_fp->Close();
		delete m_fp;
	}
}
void CClient::Createnew()
{
	AfxSocketInit();
	if (!Create(0, SOCK_STREAM))
	{
		TRACE0("\nSocket creation failed");
	}
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClient, CSocket)
	//{{AFX_MSG_MAP(CClient)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClient member functions

void CClient::OnReceive(int nErrorCode) 
{
	char strTest[TXT_BUF_LEN];
	memset (strTest,0, TXT_BUF_LEN);
	int nLength = Receive(strTest,TXT_BUF_LEN);
	CFileException fileException;
	if (m_pParent == NULL)
		return;

	CListCtrl& theCtrl = m_pParent->GetListCtrl();

	char* pDelim = NULL;
	char* pStr   = strTest;
	while ( (pDelim = strchr(pStr,22)) != NULL)
	{
		*pDelim = '\0';
		pDelim += 1;// pts to start of next string
		// added separate command (empty line) to clear display
		if (strlen(pStr) == 0)
		{
			theCtrl.DeleteAllItems();
			m_nListCount = 0;
			pStr = pDelim;
			continue;//return;
		}
		if (strcmp(pStr, "Title") == 0)
		{
			m_bTitle = TRUE; 
			// CSocket::OnReceive(nErrorCode);
			pStr = pDelim;
			continue;//return;
		}

		if (m_bTitle)
		{
			m_pParent->GetDocument()->SetTitle(pStr);
			m_bTitle = FALSE;
			// now has a separate command::> theCtrl.DeleteAllItems();
			//CSocket::OnReceive(nErrorCode);
			pStr = pDelim;
			continue;//return;
		}

		TRACE0("\n|");
		TRACE0(strTest);
		TRACE0("|");
		LV_ITEM   lv_item;

		if (m_nListCount > MAX_LIST_ITEMS)
		{
			m_nListCount = 0;
			theCtrl.DeleteAllItems();
		}

		lv_item.mask = LVIF_TEXT;
		lv_item.iSubItem = 0;

		char strBuff[TXT_BUF_LEN];
		ltoa(m_nListCount, strBuff, 10);

		lv_item.iItem = 0; //Zero - Last in, last in the m_pList  /*nIdx*/
		lv_item.pszText = strBuff;
		lv_item.lParam = NULL;
		lv_item.iImage = NULL;
		int nRow = theCtrl.InsertItem(&lv_item);

		theCtrl.SetItem(nRow,1 ,LVIF_TEXT, pStr, 0,0,0,0);

		// Call Log for any needed communication
		// logging.
		m_pParent->Log(pStr,m_nListCount);

		m_nListCount++;
	
		pStr = pDelim;			
	}

	CSocket::OnReceive(nErrorCode);
}

void CClient::OnConnect(int nErrorCode) 
{

	CSocket::OnConnect(nErrorCode);
}
BOOL CClient::SetFileName(CString strNewFile)
{
	m_strFileName = strNewFile;

	return TRUE;
}

CString CClient::GetFileName()
{
	return m_strFileName;
}
