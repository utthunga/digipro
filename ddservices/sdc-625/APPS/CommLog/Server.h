#if !defined(AFX_SERVER_H__954FBAB3_F3E0_4046_A8D8_3E67DFE06B3A__INCLUDED_)
#define AFX_SERVER_H__954FBAB3_F3E0_4046_A8D8_3E67DFE06B3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Server.h : header file
//

#include "afxsock.h"

class CDebugClientView;
/////////////////////////////////////////////////////////////////////////////
// CServer command target

class CServer : public CSocket
{
// Attributes
public:

	CDebugClientView *m_pParent;

// Operations
public:
	CServer(CDebugClientView *pParent);
	virtual ~CServer();
	void Createnew();
	void SendMessage(char *strMessage,int nLength);

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServer)
	public:
	virtual void OnConnect(int nErrorCode);
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CServer)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	BOOL m_bConnected;
	int m_nBytesSent;
	int m_nBytesBufferSize;
	CString m_sendBuffer;
	CSocket *pConnected;
	int m_nListCount;
	BOOL	m_bTitle;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVER_H__954FBAB3_F3E0_4046_A8D8_3E67DFE06B3A__INCLUDED_)
