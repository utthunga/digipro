// DebugClient.h : main header file for the DEBUGCLIENT application
//

#if !defined(AFX_DEBUGCLIENT_H__324D6FD3_BDFB_40D4_AB56_19513E375D90__INCLUDED_)
#define AFX_DEBUGCLIENT_H__324D6FD3_BDFB_40D4_AB56_19513E375D90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDebugClientApp:
// See DebugClient.cpp for the implementation of this class
//

class CDebugClientApp : public CWinApp
{
public:
	CDebugClientApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugClientApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDebugClientApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGCLIENT_H__324D6FD3_BDFB_40D4_AB56_19513E375D90__INCLUDED_)
