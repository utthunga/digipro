// DebugClientDoc.h : interface of the CDebugClientDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGCLIENTDOC_H__D0AD649C_3496_4A89_98F1_4BC6CA5F7840__INCLUDED_)
#define AFX_DEBUGCLIENTDOC_H__D0AD649C_3496_4A89_98F1_4BC6CA5F7840__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDebugClientView;

class CCommLog : public CObject
{
	DECLARE_SERIAL( CCommLog )
public:
	CString	strSequenceNo;
	CString strLog;

	CCommLog(){};

	CCommLog& operator=(CCommLog &newLog)
	{
		strSequenceNo = newLog.strSequenceNo;
		strLog = newLog.strLog;
	}
	
	void Serialize( CArchive& archive )
	{
		CObject::Serialize( archive );
		
		if( archive.IsStoring() )
		{
			archive << strSequenceNo << strLog;
		}
		else
			archive >> strSequenceNo >> strLog;
	}
};

class CCommList : public CObList
{
	DECLARE_SERIAL( CCommList )

public:
	CCommList(){};

	void Serialize( CArchive& archive )
	{
		CObject::Serialize( archive );
		POSITION pos;

		
		if( archive.IsStoring() )
		{
			for (pos = GetHeadPosition(); pos != NULL;)
			{
				if (pos != NULL)
				{
					CCommLog *pLog = (CCommLog *)GetNext(pos);
					archive << pLog;
				}
			}
		}
		else
		{
			CCommLog *pLog = NULL;

			try
			{
				while (pLog = (CCommLog *)archive.ReadObject(RUNTIME_CLASS(CCommLog)))
				{
					AddTail((CObject *)pLog);
				}
			}
			catch(...)
			{
			}
		}
	}
};

class CDebugClientDoc : public CDocument
{
protected: // create from serialization only
	CDebugClientDoc();
	DECLARE_DYNCREATE(CDebugClientDoc)

// Attributes
public:
		CCommList *m_pList;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugClientDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDebugClientDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDebugClientDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CDebugClientView* GetView();
	BOOL bFirstView;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGCLIENTDOC_H__D0AD649C_3496_4A89_98F1_4BC6CA5F7840__INCLUDED_)
