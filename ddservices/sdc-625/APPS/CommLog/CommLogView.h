// DebugClientView.h : interface of the CDebugClientView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGCLIENTVIEW_H__A69A2377_5099_4BE9_B2C9_44C1F0D49922__INCLUDED_)
#define AFX_DEBUGCLIENTVIEW_H__A69A2377_5099_4BE9_B2C9_44C1F0D49922__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxcview.h>
#include "Client.h"
#include "Server.h"
#include "CommLogDoc.h"

/*
 * List view settings
 */
#define SETTINGS       _T("Settings")
#define FONT           _T("Font")
#define FONT_STYLE     _T("Font Style")
#define FONT_SIZE      _T("Font Size")


class CDebugClientView : public CListView
{
	CFont m_font;
	CString m_strCol1;
	CString m_strCol2;
	CString m_strLogFileName;
	CFile   m_File;
	BOOL    m_bLogOn;
//	BOOL	m_bPrinting;
//	CFont*	m_pPrintFont;
//	int		m_nPointSize;
//	int		m_nPageHeight;
//	int		m_nPageWidth;
//	int		m_nPrintLine;

	void CalcPageCount(CDC* pDC, CPrintInfo* pInfo);
	int MeasureFontHeight(CFont* pFont, CDC* pDC);

protected: // create from serialization only
	CDebugClientView();
	DECLARE_DYNCREATE(CDebugClientView)

	CServer *m_pServer;

// Attributes
public:
	CDebugClientDoc* GetDocument();

// Operations
public:
	void Log(char * pStr, int ListCount);
	void GetProfileSettings();
	void WriteProfileSettings();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugClientView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDebugClientView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDebugClientView)
	afx_msg void OnFileLog();
	afx_msg void OnUpdateFileLog(CCmdUI* pCmdUI);
	afx_msg void OnFileLogAs();
	afx_msg void OnUpdateFileLogAs(CCmdUI* pCmdUI);
	afx_msg void OnViewFont();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in DebugClientView.cpp
inline CDebugClientDoc* CDebugClientView::GetDocument()
   { return (CDebugClientDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGCLIENTVIEW_H__A69A2377_5099_4BE9_B2C9_44C1F0D49922__INCLUDED_)
