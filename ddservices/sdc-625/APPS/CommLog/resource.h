//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CommLog.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_DEBUGCTYPE                  129
#define ID_FILE_LOG                     32772
#define ID_FILE_LOG_AS                  32773
#define ID_VIEW_FONT                    32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
