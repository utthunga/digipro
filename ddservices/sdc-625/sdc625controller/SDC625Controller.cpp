/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    SDC625Controller class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <string>

#include "IConnectionManager.h"
#include "IParameterCache.h"
#include "SDC625Controller.h"
#include "ddbDevice.h"
#include "ddbDeviceMgr.h"
#include "CommonProtocolManager.h"'
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include "MethodSupport.h"
#include "MEE.h"


/* ========================================================================== */

/* static member initialization ***********************************************/
HART_METHOD_EXECUTION_DATA SDC625Controller::s_hartMEData = {};
HART_VAR_ACTION_DATA  SDC625Controller::s_hartVarActionData = {};

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    SDC625Controller class instance initialization
*/
SDC625Controller::SDC625Controller() :
    m_pCM(nullptr),
    m_pPC(nullptr),
    m_pMgr(nullptr),
    m_pDev(nullptr),
    m_pComm(nullptr),
    m_pMethodSupport(nullptr)

{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
SDC625Controller::~SDC625Controller()
{
     m_pCM = nullptr;
     m_pPC = nullptr;
     m_pHARTAdaptor = nullptr;

     if( nullptr != m_pMethodSupport)
     {
         delete m_pMethodSupport;
         m_pMethodSupport = nullptr;
     }

     if( nullptr != m_pMgr)
     {
         m_pMgr->destantiate();
         m_pMgr = nullptr;
     }
}

/*  ----------------------------------------------------------------------------
    This function is used to initialize the module
*/
PMStatus SDC625Controller::initialize(IConnectionManager* cm,
                                      IParameterCache* pc,
                                      HARTAdaptor* hartAdaptor)
{
    PMStatus status;

    if (nullptr != cm &&
        nullptr != pc &&
        nullptr != hartAdaptor)
    {
        m_pCM = cm;
        m_pPC = pc;
        m_pHARTAdaptor = hartAdaptor;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Failed to initializing the SDC-625 interface\n");
        status.setError(SDC_CTRL_ERR_INITIALIZE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to initialize the device manager
*/
PMStatus SDC625Controller::initializeDeviceManager(std::string ddlibDir)
{
    PMStatus status;
    if (ddlibDir.length() > 0)
    {
        // Create device manager
        m_pMgr = createManager(ddlibDir);

        if (nullptr == m_pMgr)
        {
            LOGF_ERROR(CPMAPP, "Failed to initializing the SDC-625 interface\n");
            status.setError(SDC_CTRL_ERR_INITIALIZE_FAILED,
                            PM_ERR_CL_SDC_CTRL,
                            HARTDDS_ERR_SC_DDS);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Failed to initializing the SDC-625 interface\n");
        status.setError(SDC_CTRL_ERR_INITIALIZE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to uninitialize the device manager
*/
void SDC625Controller::uninitializeDeviceManager()
{
    // Unintialize the device manager
    if( nullptr != m_pMgr)
    {
        m_pMgr->destantiate();
        m_pMgr = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Creates the Device
*/
PMStatus SDC625Controller::createDevice(Indentity_t *identity,
                                        hCcommInfc* pComm)
{
    PMStatus status;

    m_identity = *identity;
    m_pComm = pComm;

    // Create the device
    m_pDev = createDevice(m_identity, m_pComm, m_pMgr);

    if (nullptr == m_pDev)
    {
        LOGF_ERROR(CPMAPP, "Failed to create the device\n");
        status.setError(SDC_CTRL_CREATE_DEVICE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Creates a device manager object, which is required before actual device
    objects can be created
*/
hCDeviceManger* SDC625Controller::createManager(std::string ddLibDir)
{
    hCDeviceManger* pDevMgr = nullptr;

    std::string langCode = CommonProtocolManager::getInstance()->getLangCode();

    pDevMgr = hCDeviceManger::instantiate(ddLibDir.c_str(), langCode.c_str());

    // If the library wasn't loaded correctly
    if (pDevMgr->IsdbNOTavailable())
    {
        LOGF_ERROR(CPMAPP, "Failed to load DD library!\n");
        pDevMgr->destantiate();
        pDevMgr = nullptr;
    }
    return pDevMgr;
}

/*  ----------------------------------------------------------------------------
    Creates a device object based on the identity retrieved from the connected
    device
*/
hCddbDevice* SDC625Controller::createDevice(Indentity_t& oneIdentity,
                                            hCcommInfc* pComm,
                                            hCDeviceManger* pDevMgr)
{
    hCddbDevice* pCurDev = nullptr;

    if (oneIdentity.cUniversalRev == 0 && oneIdentity.cDeviceRev   == 0 &&
        oneIdentity.cHardwareRev  == 0 && oneIdentity.cSoftwareRev == 0)
    {
        LOGF_ERROR(CPMAPP, "Invalid device identity!\n");
        return nullptr;
    }
    else if (nullptr != pDevMgr)
    {
        m_pMethodSupport = new MethodSupport();
        m_pMethodSupport->Initialize(this);
        // Create the new device
        pCurDev = pDevMgr->newDevice(oneIdentity, nullptr,
                                     m_pMethodSupport, pComm);
        if (nullptr == pCurDev)
        {
            LOGF_ERROR(CPMAPP, "Device object creation failed!\n");
            return nullptr;
        }

        // Sets SDC625 to standard mode for compatability purposes
        pCurDev->setCompatability(dm_Standard);
        pCurDev->setDeviceState(ds_OnLine);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "No device manager present!\n");
        return nullptr;
    }
    return pCurDev;
}

/*  ----------------------------------------------------------------------------
    Deletes current device
*/
PMStatus SDC625Controller::deleteDevice()
{
    PMStatus status;

    if (nullptr != m_pDev)
    {
        hCdeviceSrvc* pSvcDev = dynamic_cast<hCdeviceSrvc*>(m_pDev);
        if (nullptr != pSvcDev && nullptr != m_pMgr)
        {
            int errorCode = m_pMgr->deleteDevice(&pSvcDev);
            if (SUCCESS != errorCode)
            {
                status.setError(SDC_CTRL_DELETE_DEVICE_FAILED,
                                PM_ERR_CL_SDC_CTRL,
                                HARTDDS_ERR_SC_DDS);
            }
        }
        if (!status.hasError())
        {
            pSvcDev = nullptr;
            m_pDev = nullptr;

            if (nullptr != m_pMethodSupport)
            {
                delete m_pMethodSupport;
                m_pMethodSupport = nullptr;
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    returns the valriable pointer
*/
hCVar* SDC625Controller::getVariablePtr(itemID_t itemID)
{
    if (nullptr != m_pDev)
    {
        return m_pDev->getVarPtrBySymNumber(itemID);
    }

    return nullptr;
}

/*  ----------------------------------------------------------------------------
    Retrieves the image item list
*/
CimageItemList* SDC625Controller::getImageItemList()
{
    return dynamic_cast<CimageItemList*>(m_pDev->getListPtr(iT_Image));
}

/*  ----------------------------------------------------------------------------
    Retrieves the image list
*/
Image_PtrList_t* SDC625Controller::getImageList()
{
    Image_PtrList_t* imageList = reinterpret_cast<Image_PtrList_t*>
                            (m_pDev->getListPtr(iT_ReservedOne));

    return imageList;
}

/*  ----------------------------------------------------------------------------
    Reads the specific parameter
*/
PMStatus SDC625Controller::readCacheValue(VariableType* pVarItem, CValueVarient& value)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;

   
    if (nullptr != pVarItem)
    {
        hCVar* pVar = m_pDev->getVarPtrBySymNumber(pVarItem->itemID);

        if (nullptr != pVar)
        {
            // Added below condition to handle the single bit for bit-enum variable
            if (pVar->VariableType() == vT_BitEnumerated && pVarItem->bitIndex > 0)
            {
                // Read the value of only specified bit for the bit-enum variable
                hCBitEnum* bitEnumVar = nullptr;
                bitEnumVar = dynamic_cast<hCBitEnum*>(pVar);

                if (nullptr != bitEnumVar)
                {
                    // Retrieves the specified bit item value
                    value = bitEnumVar->getDispValue(pVarItem->bitIndex);
                    value.vValue.iIntConst =
                            value.vValue.bIsTrue == true ? BIT_ON : BIT_OFF;
                    retCode = SUCCESS;
                }
            }
            else
            {
                // retrieves the value with scaling factor applied
                value = pVar->getDispValue();
                retCode = SUCCESS;
            }
        }
    }

    if (FAILURE == retCode)
    {
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Reads the specific parameter from the cache if not available then
    read from device
*/
PMStatus SDC625Controller::readDeviceValue(VariableType* varItem, CValueVarient& value)
{
    PMStatus status;
    RETURNCODE rCode;

    hCVar* pVar = m_pDev->getVarPtrBySymNumber(varItem->itemID);

    if (nullptr != pVar)
    {
        // Read the parameter from device
        rCode  = m_pDev->ReadImd(dynamic_cast<hCitemBase*>(pVar), value, false);
    }

    if (SUCCESS != rCode)
    {
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Reads the specific parameter from the cache if not available then
    read from device
*/
PMStatus SDC625Controller::asyncReadDeviceValue(VariableType* varItem, CValueVarient& value)
{
    PMStatus status;
    hCVar* var = m_pDev->getVarPtrBySymNumber(varItem->itemID);

    if (nullptr != var)
    {
        //below state set to read value directly from the device
        var->markItemState(IDS_STALE);
        var->setDataQuality(DA_STALEUNK);

        m_pDev->pCmdDispatch->ServiceReads();

        dispatchCondition_t cond = dc_Nothing;
        do
        {
            Sleep(2);
            cond = m_pDev->pCmdDispatch->getCond();
        }
        while (cond != dc_Nothing);

        value = var->getRealValue();
    }
    else
    {
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

     return status;
}

/*  ----------------------------------------------------------------------------
    Reads the parameters from device
*/
PMStatus SDC625Controller::asyncReadService()
{
    PMStatus status;

    if (nullptr != m_pDev && nullptr != m_pDev->pCmdDispatch)
    {
        m_pDev->pCmdDispatch->ServiceReads();
    }


    return status;
}

/*  ----------------------------------------------------------------------------
    Writes the specific parameter
*/
PMStatus SDC625Controller::writeDeviceValue(VariableType* varItem, CValueVarient& value)
{
    PMStatus status;
    RETURNCODE rCode = SUCCESS;

    hCVar* pVar = m_pDev->getVarPtrBySymNumber(varItem->itemID);

    if (nullptr != pVar)
    {
        /* Perform read operation atleast once if the variable is not yet read
            before writing. Otherwise, write is not working as expected */
        if (pVar->getDataState() == IDS_UNINITIALIZED)
        {
            CValueVarient deviceValue{};
            status = readDeviceValue(varItem, deviceValue);

            if (status.hasError())
            {
                rCode = FAILURE;
            }
        }

        if (SUCCESS == rCode)
        {
            // Added below condition to handle the single bit for bit-enum variable
            if (pVar->VariableType() == vT_BitEnumerated && varItem->bitIndex > 0)
            {
                /* For single bit-enumeration item, before writing to device,
                   clear and reset only the affected bit in varItem->bitIndex
                   and do not modify other bits in the variable
                */
                hCBitEnum* bitEnumVar = nullptr;
                bitEnumVar = dynamic_cast<hCBitEnum*>(pVar);

                if (nullptr != bitEnumVar)
                {                    
                    if (varItem->value.stdValue.ul == BIT_ON)
                    {
                        /* If the value is 1, set the bit mask value itself
                        which is same as bitIndex */
//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
                        value = varItem->bitIndex;
#else
                        value = static_cast<unsigned int>(varItem->bitIndex);
#endif
                    }


                    // clears the resets only the affected bit
                    bitEnumVar->setDispValue(value, varItem->bitIndex);
                    value = pVar->getDispValue();
                }
            }
            // Write the parameter to device
            rCode  = m_pDev->WriteImd(varItem->itemID, &value);
        }

        if (SUCCESS != rCode)
        {
            // If write is not succeed, set the last good value to the cache
            value = pVar->getRealValue();
            pVar->setDispValue(value);

            status.setError(SDC_CTRL_WRITE_VALUE_FAILED,
                            PM_ERR_CL_SDC_CTRL,
                            HARTDDS_ERR_SC_DDS);
        }

    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Reads the specific parameter
*/
PMStatus SDC625Controller::writeCacheValue(VariableType* varItem,
                                            CValueVarient& value)
{
    PMStatus status;
    RETURNCODE rCode = SUCCESS;

    if (nullptr == varItem)
    {
        rCode = FAILURE;
    }

    if (SUCCESS == rCode)
    {
        hCVar* pVar = m_pDev->getVarPtrBySymNumber(varItem->itemID);

        if (nullptr != pVar)
        {
            // Added below condition to handle the single bit for bit-enum variable
            if (pVar->VariableType() == vT_BitEnumerated && varItem->bitIndex > 0)
            {
                /* For single bit-enumeration item, before writing to device,
                   clear and reset only the affected bit in varItem->bitIndex
                   and do not modify other bits in the variable
                */
                hCBitEnum* bitEnumVar = nullptr;
                bitEnumVar = dynamic_cast<hCBitEnum*>(pVar);

                if (nullptr != bitEnumVar)
                {
                    // clears and resets only the affected bit to the cache table
                    bitEnumVar->setDispValue(value, varItem->bitIndex);
                    value = pVar->getDispValue();
                }
            }
            else
            {
                // writes the value to the cache table
                 pVar->setDispValue(value);
            }
        }
    }

    if (SUCCESS != rCode)
    {
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        HARTDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Checks if actions exists for the given variable Item ID or
    not and action type
*/
bool SDC625Controller::varActionExists(ItemID itemID,
                                       varAttrType_t varActionType)
{
    RETURNCODE retCode = SUCCESS;
    bool isActionsExists = false;
    vector<itemID_t> methodList;

    hCVar* pVar = m_pDev->getVarPtrBySymNumber(itemID);

    if (nullptr != pVar)
    {
        retCode = pVar->getActionList(varActionType, methodList);
    }

    if (SUCCESS == retCode && methodList.size() > 0)
    {
        isActionsExists = true;
    }

    return isActionsExists;
}

/*  ----------------------------------------------------------------------------
    Checks if actions exists for the given Edit Display Item ID or
    not and action type
*/
bool SDC625Controller::editDispActionExists(ItemID itemID,
                                            eddispAttrType_t editDispActionType)
{
    RETURNCODE retCode = SUCCESS;
    bool isActionsExists = false;
    vector<hCmethodCall> methodList;
    hCeditDisplay* pEditDisp = nullptr;

    getEditDisplayItem(itemID, &pEditDisp);

    if (nullptr != pEditDisp)
    {
        retCode = pEditDisp->getMethodList(editDispActionType, methodList);
    }

    if (SUCCESS == retCode && methodList.size() > 0)
    {
        isActionsExists = true;
    }

    return isActionsExists;
}

/*  ----------------------------------------------------------------------------
    Performs post edit actions for the specified item base
*/
PMStatus SDC625Controller::postEditActions(ItemID itemID)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;
    hCVar* pVar = m_pDev->getVarPtrBySymNumber(itemID);

    if (nullptr != pVar)
    {
        retCode = pVar->doPstEditActs();
    }

    if (FAILURE == retCode)
	{
		if(m_pDev->pMEE->latestMethodStatus != me_Aborted)
		{
            LOGF_ERROR(CPMAPP, "Failed to execute Post edit Method Item : " << itemID);
			status.setError(SDC_CTRL_EXECUTE_EDIT_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
		}
		else
		{
           LOGF_ERROR(CPMAPP, "Failed to execute Method Item : " << itemID);
		   status.setError(SDC_CTRL_EXECUTE_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
		}
	}

    return status;
}

/*  ----------------------------------------------------------------------------
    Executes the post edit actions method for edit display
*/
PMStatus SDC625Controller::postEditDispActions(ItemID itemID)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;
    hCeditDisplay* pEditDisp = nullptr;

    getEditDisplayItem(itemID, &pEditDisp);

    if (nullptr != pEditDisp)
    {
        retCode = pEditDisp->doPstEditActs();
    }

    if (FAILURE == retCode)
    {
        if(m_pDev->pMEE->latestMethodStatus != me_Aborted)
        {
            LOGF_ERROR(CPMAPP, "Failed to execute Post EditMethod Item : " << itemID);
            status.setError(SDC_CTRL_EXECUTE_EDIT_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
        else
        {
           LOGF_ERROR(CPMAPP, "Failed to execute Method Item : " << itemID);
           status.setError(SDC_CTRL_EXECUTE_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves menu information given menu Item ID
*/
PMStatus SDC625Controller::getMenuItem(itemID_t itemId, hCmenu** menuInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;
    hCitemBase* hcItemBase = nullptr;
    hCmenu* hcMenu = nullptr;

    errorCode = m_pDev->getItemBySymNumber(itemId, &hcItemBase);

    if(SUCCESS != errorCode || nullptr == hcItemBase)
    {
        LOGF_ERROR(CPMAPP, "Failed to get Menu Item ID : " << itemId);
        status.setError(SDC_CTRL_GET_MENU_FAILED, PM_ERR_CL_SDC_CTRL, 0);
    }
    else
    {
        // downcast it to get menu information
        hcMenu = dynamic_cast<hCmenu*>(hcItemBase);
        *menuInfo = hcMenu;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves edit display information for given edit display Item ID
*/
PMStatus SDC625Controller::getEditDisplayItem(itemID_t itemId,
                                              hCeditDisplay** pEditdisp)
{
    PMStatus status;
    int errorCode = SUCCESS;
    hCitemBase* hcItemBase = nullptr;
    hCeditDisplay* pEditdispInfo = nullptr;

    errorCode = m_pDev->getItemBySymNumber(itemId, &hcItemBase);

    if(SUCCESS != errorCode || nullptr == hcItemBase)
    {
        LOGF_ERROR(CPMAPP, "Failed to get Edit Display Item ID : " << itemId);
        status.setError(SDC_CTRL_GET_EDIT_DISPLAY_FAILED, PM_ERR_CL_SDC_CTRL, 0);
    }
    else
    {
        // downcast it to get edit display information
        pEditdispInfo = dynamic_cast<hCeditDisplay*>(hcItemBase);
        *pEditdisp = pEditdispInfo;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves WAO information for the given WAO Item ID
*/
PMStatus SDC625Controller::getWaoItem(itemID_t itemId,
                                              hCwao** pWaoInfoPtr)
{
    PMStatus status;
    int errorCode = SUCCESS;
    hCitemBase* hcItemBase = nullptr;
    hCwao* pWaoInfo = nullptr;

    errorCode = m_pDev->getItemBySymNumber(itemId, &hcItemBase);

    if(SUCCESS != errorCode || nullptr == hcItemBase)
    {
        LOGF_ERROR(CPMAPP, "Failed to get WAO Item ID : " << itemId);
        status.setError(SDC_CTRL_GET_WAO_FAILED, PM_ERR_CL_SDC_CTRL, 0);
    }
    else
    {
        // downcast it to get WAO information
        pWaoInfo = dynamic_cast<hCwao*>(hcItemBase);
        *pWaoInfoPtr = pWaoInfo;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrives the method information given method Item ID
*/
PMStatus SDC625Controller::getMethodItem(itemID_t itemId, hCmethod **pMethod)
{
    PMStatus status;
    int errorCode = SUCCESS;
    hCitemBase* hcItemBase = nullptr;
    hCmethod* hcmethod = nullptr;

    errorCode = m_pDev->getItemBySymNumber(itemId, &hcItemBase);

    if (SUCCESS != errorCode || nullptr == hcItemBase)
    {
        LOGF_ERROR(CPMAPP, "Failed to get Method Item ID : " << itemId);
        status.setError(SDC_CTRL_GET_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
    }
    else
    {
        // downcast it to get method information
        hcmethod = dynamic_cast<hCmethod*>(hcItemBase);
        *pMethod = hcmethod;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Executes the specified method
*/
PMStatus SDC625Controller::executeMethod(itemID_t itemID)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;
    hCmethod* pMethod = nullptr;
    string itemLabel, itemHelp;

    // Retrieves method information
    status = getMethodItem(itemID, &pMethod);

    if (status.hasError() || nullptr == pMethod)
    {            
        LOGF_ERROR(CPMAPP, "Failed to get Method Item ID : " << itemID);
        return status;
    }

    pMethod->Label(itemLabel); // Get the item label
    pMethod->Help(itemHelp); // Get the item help

    // set help and label to global structure to send notify message
    SDC625Controller::s_hartMEData.methodName = itemLabel;
    SDC625Controller::s_hartMEData.methodHelp = itemHelp;

    hCmethodCall methodCall;
    methodCall.m_pMeth  = pMethod;
    // set msrc_EXTERN to call the method from external location (usually the UI)
    methodCall.source   = msrc_EXTERN;
    methodCall.methodID = itemID;

    CValueVarient retVarient;
    retCode = m_pDev->executeMethodForMS(methodCall, retVarient, m_pDev);

    if (FAILURE == retCode &&
            m_pDev->pMEE->latestMethodStatus != me_Aborted)
    {
       LOGF_ERROR(CPMAPP, "Failed to execute Method Item : " << itemID);
       status.setError(SDC_CTRL_EXECUTE_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Executes the pre edit actions method
*/
PMStatus SDC625Controller::preEditActions(ItemID itemID)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;
    hCVar* pVar = m_pDev->getVarPtrBySymNumber(itemID);

    if (nullptr != pVar)
    {
        retCode = pVar->doPreEditActs();
    }

    if (FAILURE == retCode)
    {
        if(m_pDev->pMEE->latestMethodStatus != me_Aborted)
        {
            LOGF_ERROR(CPMAPP, "Failed to execute Pre EditMethod Item : " << itemID);
            status.setError(SDC_CTRL_EXECUTE_EDIT_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
        else
        {
           LOGF_ERROR(CPMAPP, "Failed to execute Method Item : " << itemID);
           status.setError(SDC_CTRL_EXECUTE_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Executes the pre edit actions method for edit display
*/
PMStatus SDC625Controller::preEditDispActions(ItemID itemID)
{
    PMStatus status;
    RETURNCODE retCode = FAILURE;
    hCeditDisplay* pEditDisp = nullptr;

    getEditDisplayItem(itemID, &pEditDisp);

    if (nullptr != pEditDisp)
    {
        retCode = pEditDisp->doPreEditActs();
    }

    if (FAILURE == retCode)
    {
        if(m_pDev->pMEE->latestMethodStatus != me_Aborted)
        {
            LOGF_ERROR(CPMAPP, "Failed to execute Pre EditMethod Item : " << itemID);
            status.setError(SDC_CTRL_EXECUTE_EDIT_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
        else
        {
           LOGF_ERROR(CPMAPP, "Failed to execute Method Item : " << itemID);
           status.setError(SDC_CTRL_EXECUTE_METHOD_FAILED, PM_ERR_CL_SDC_CTRL, 0);
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    It acquires the thread lock and wait till release
*/
void SDC625Controller::acquireLock()
{
    // Waits until get the input from user
    std::unique_lock<std::mutex> methodLock(m_methodExecutionLock);
    m_methodExecutionConditionVariable.wait(methodLock, []
    {
        return SDC625Controller::s_hartMEData.isItemSet;
    });
}

/*  ----------------------------------------------------------------------------
    It releases the locked thread and notify for further process
*/
void SDC625Controller::releaseLock()
{
    // Unlocks the thread and notify that user's input arrived
    std::unique_lock<std::mutex> methodLock(m_methodExecutionLock);
    methodLock.unlock();
    m_methodExecutionConditionVariable.notify_one();
}

/*  ----------------------------------------------------------------------------
    Sends method information to adaptor to get acknowledgement from UI
*/
int SDC625Controller::displayAckUpcall(ACTION_UI_DATA structMethodsUIData)
{

    string message = "";

    if (structMethodsUIData.textMessage.pchTextMessage != nullptr)
    {
         wstring wsMessage(structMethodsUIData.textMessage.pchTextMessage);
         // converts Unicode to UTF8
         message = TStr2AStr(wsMessage);
    }

    // Notify Ack information, user need to acknowledge it
    if (structMethodsUIData.bUserAcknowledge)
    {
        // Sends ack message and expect acknowledgement from UI
        m_pHARTAdaptor->sendMethodUIMessage(message, JSON_METHOD_NOTIFY_ACK);

        // Waits until get the input from user
        acquireLock();
        SDC625Controller::s_hartMEData.isItemSet = false;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sends method information to adaptor and interanlly waits for specified
        delay time
*/
int SDC625Controller::displayDelayUpcall(ACTION_UI_DATA structMethodsUIData)
{
    string message = "";

    if (structMethodsUIData.textMessage.pchTextMessage != nullptr)
    {
        wstring wsMessage(structMethodsUIData.textMessage.pchTextMessage);

        // converts Unicode to UTF8
        message = TStr2AStr(wsMessage);
    }


    m_pHARTAdaptor->sendMethodUIMessage(message, JSON_METHOD_NOTIFY_DISPLAY);

    // Delay to display the message on screen
    std::this_thread::sleep_for(std::chrono::milliseconds(structMethodsUIData.uDelayTime));

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sends method information to adaptor to get new selection from user
*/
int SDC625Controller::displayMenuUpcall(ACTION_UI_DATA structMethodsUIData,
              ACTION_USER_INPUT_DATA& structMethodUserInputData)
{
    string message = "";

    if (structMethodsUIData.textMessage.pchTextMessage != nullptr)
    {
        wstring wsMessage(
                    structMethodsUIData.textMessage.pchTextMessage);

        // converts Unicode to UTF8
        message = TStr2AStr(wsMessage);
    }

    SDC625Controller::s_hartMEData.userInputType.userInterfaceDataType =
            structMethodsUIData.userInterfaceDataType;

    SDC625Controller::s_hartMEData.userInputType.ComboBox.comboBoxType   =
            structMethodsUIData.ComboBox.comboBoxType;


    m_pHARTAdaptor->fillMethodEnumValAsJson(message, structMethodsUIData);


    // Waits until gets the input from user
    acquireLock();
    SDC625Controller::s_hartMEData.isItemSet = false;

    structMethodUserInputData =
            SDC625Controller::s_hartMEData.userInputType;

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sends method information to adaptor to get new value from user
*/
int SDC625Controller::displayGetUpcall(ACTION_UI_DATA structMethodsUIData,
                        ACTION_USER_INPUT_DATA& structMethodUserInputData)
{
    string message = "";

    if (structMethodsUIData.textMessage.pchTextMessage != nullptr)
    {
        wstring wsMessage(
                    structMethodsUIData.textMessage.pchTextMessage);

        // converts Unicode to UTF8
        message = TStr2AStr(wsMessage);
    }


    SDC625Controller::s_hartMEData.userInputData = structMethodsUIData;

    SDC625Controller::s_hartMEData.userInputType.userInterfaceDataType =
            structMethodsUIData.userInterfaceDataType;

    SDC625Controller::s_hartMEData.userInputType.EditBox.editBoxType  =
            structMethodsUIData.EditBox.editBoxType;

    m_pHARTAdaptor->getMethodData(message, structMethodsUIData);

    // Waits until gets the input from user
    acquireLock();
    SDC625Controller::s_hartMEData.isItemSet = false;

    structMethodUserInputData =
            SDC625Controller::s_hartMEData.userInputType;

    return SUCCESS;
}


/*  ----------------------------------------------------------------------------
    Sends method started information to adaptor to notify UI
*/
int SDC625Controller::methodStartUpcall()
{
    if (nullptr != m_pHARTAdaptor->getParameterCacheInstance())
    {
        //pauses subscription
        m_pHARTAdaptor->getParameterCacheInstance()->pauseSubscription();
    }

    SDC625Controller::s_hartMEData.isAborted = false;

    // Intimate UI, that Method execution has been started
    m_pHARTAdaptor->sendMethodUIMessage("", JSON_METHOD_NOTIFY_START);

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sends method completed information to adapor to notify UI
*/
int SDC625Controller::methodCompleteUpcall()
{
    if (nullptr != m_pHARTAdaptor->getParameterCacheInstance())
    {
       //resumes subscription
       m_pHARTAdaptor->getParameterCacheInstance()->resumeSubscription();
    }

    // Intimate UI, Method execution completed
    m_pHARTAdaptor->sendMethodUIMessage("", JSON_METHOD_NOTIFY_COMPLETED);

    // Destroys the ME thread after completion
    m_pHARTAdaptor->destroyMethodExecutionThread();

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sends method aborted information to adaptor to notify UI
*/
int SDC625Controller::methodErrorUpcall(ACTION_UI_DATA structMethodsUIData)
{
    string message = "";

    if (structMethodsUIData.textMessage.pchTextMessage != nullptr)
    {
        wstring wsMessage(
                    structMethodsUIData.textMessage.pchTextMessage);

        // converts Unicode to UTF8
        message = TStr2AStr(wsMessage);
    }

    // Notify Ack information, user need to acknowledge it
    if (structMethodsUIData.bUserAcknowledge)
    {
         // Sends the error message to UI
        m_pHARTAdaptor->sendMethodUIMessage(message,
                                            JSON_METHOD_NOTIFY_ERROR);

        // Waits until get the input from user
        acquireLock();
        SDC625Controller::s_hartMEData.isItemSet = false;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user about Method error status
*/
void SDC625Controller::sendMethodErrorMessage()
{
    m_pHARTAdaptor->sendMethodErrorMessage();
}

/*  ----------------------------------------------------------------------------
    Converts epoch time to user time
*/
std::string SDC625Controller::convertsEpochTimeToUserTime(time_t epochTime,
                                                          const char* format)
{
    char timestamp[64] = {0};
    strftime(timestamp, sizeof(timestamp), format, gmtime(&epochTime));
    return timestamp;
}

/*  ----------------------------------------------------------------------------
    Converts user time to epoch time
*/
time_t SDC625Controller::convertsUserTimeToEpochTime(const char* userTime,
                                                     const char* format)
{
    std::tm tmTime;
    memset(&tmTime, 0, sizeof(tmTime));
    strptime(userTime, format, &tmTime);
    return timegm(&tmTime);
}

/*  ----------------------------------------------------------------------------
    Retrieves the current device object
*/
 hCddbDevice* SDC625Controller::getCurrDevice()
{
    return m_pDev;
}

 /*  ----------------------------------------------------------------------------
     Retrieves the DD item type
 */
  PMStatus SDC625Controller::getItemType(itemID_t itemID, itemType_t& itemType)
 {
      PMStatus status;
      int errorCode = SUCCESS;
      hCitemBase* hcItemBase = nullptr;

      errorCode = m_pDev->getItemBySymNumber(itemID, &hcItemBase);

      if(SUCCESS != errorCode || nullptr == hcItemBase)
      {
          LOGF_ERROR(CPMAPP, "Failed to get DD Item type : " << itemID);
          status.setError(SDC_CTRL_GET_ITEM_TYPE_FAILED, PM_ERR_CL_SDC_CTRL, 0);
      }
      else
      {

         itemType = hcItemBase->getIType();
      }

      return status;
 }
