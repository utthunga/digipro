/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    SDC625Controller class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _SDC625_CONTROLLER_H
#define _SDC625_CONTROLLER_H

/* INCLUDE FILES **************************************************************/

typedef unsigned long ItemID;

#include "ParameterCache.h"
#include "PMErrorCode.h"
#include "PMStatus.h"
#include "DDLBaseComm.h"
#include "ddbGeneral.h"
#include "ddbVarList.h"
#include "ddbItemLists.h"
#include <condition_variable>
#include "AdaptorDef.h"
#include "MethodSupport.h"
#include "HARTAdaptor.h"

class IConnectionManager;
class IParameterCache;
class hCcommInfc;
class hCDeviceManger;
class hCddbDevice;
class MethodSupport;
class HARTAdaptor;

/*----------------------------------------------------------------------------*/
/// Method Execution data structure
typedef struct HARTMethodExecutionData
{
    ACTION_UI_DATA userInputData; /// Holds the existing data
    ACTION_USER_INPUT_DATA userInputType; /// Currently exepecting input type
    bool isItemSet; /// Is user input item has been set or not
    bool isAborted; /// Is user abort the currently executing method or not
    ITEM_ID methodID; /// Currently executing method's block ID
    std::string methodName; /// Currently executing method name
    std::string methodHelp; /// Currently executing method help information
} HART_METHOD_EXECUTION_DATA;

/// Method Execution data structure
typedef struct HARTVarActionData
{
    ITEM_ID itemID;
    EditActionTypes actionType;
    EditActionItemTypes actionItemType;
    VariableType  varParam;
}HART_VAR_ACTION_DATA;

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** SDC625Controller Class Definition.

    SDC625Controller class implements the DD Services
    mechanism for HART protocol.
*/

class SDC625Controller
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a SDC625Controller
    SDC625Controller();

    /*------------------------------------------------------------------------*/
    /// Destruct a SDC625Controller
    virtual ~SDC625Controller();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the SDC625Controller with IConnectionManager and
        IParameterCache instances

        @param cm - IConnectionManager instance
        @param pc - IParameterCache instance
        @param hartAdaptor - hartAdaptor instance

        @return status
    */
    PMStatus initialize(IConnectionManager* cm, IParameterCache* pc,
                        HARTAdaptor* hartAdaptor);

    /*------------------------------------------------------------------------*/
    /** Initialize the Device Manager

        @param ddlibDir - DD libarary directory

        @return status
    */
    PMStatus initializeDeviceManager(std::string ddlibDir);

    /*------------------------------------------------------------------------*/
    /** Uninitialize the Device Manager

        @return NONE
    */
    void uninitializeDeviceManager();

    /*------------------------------------------------------------------------*/
    /** Creates a device object based on the identity retrieved from the
        connected device

        @param oneIdentity - contains device identity
        @param pComm - contains communication interface
        @param pDevMgr - contains device manager
        @return DDB device
    */
    hCddbDevice* createDevice(Indentity_t& oneIdentity,
                              hCDeviceManger* pDevMgr);

    /*------------------------------------------------------------------------*/
    /** Creates a device object based on the identity retrieved from the
        connected device

        @param identity - contains device identity
        @param pComm - contains communication interface
        @return status
    */
    PMStatus createDevice(Indentity_t *identity, hCcommInfc* pComm);

    /*------------------------------------------------------------------------*/
    /** Deletes current device object

        @return status
    */
    PMStatus deleteDevice();

    /*------------------------------------------------------------------------*/
    /** gives menu info given menu Item ID

        @param itemId - Item ID of variable
        @param menuInfo - contains menu information
        @return status
    */
    PMStatus getMenuItem(itemID_t itemId, hCmenu **menuInfo);

    /*------------------------------------------------------------------------*/
    /** gives edit display info for the given edit display Item ID

        @param itemId - Item ID of edit display
        @param pEditdisp - contains edit display information
        @return status
    */
    PMStatus getEditDisplayItem(itemID_t itemId, hCeditDisplay** pEditdisp);

    /*------------------------------------------------------------------------*/
    /** gives WAO variable lists info for the given WAO Item ID

        @param itemId - Item ID of WAO
        @param pWaoInfoPtr - contains WAO information
        @return status
    */
    PMStatus getWaoItem(itemID_t itemId, hCwao** pWaoInfoPtr);

    /*------------------------------------------------------------------------*/
    /** returns the valriable pointer

        @return pointer to hCVar
    */
    hCVar* getVariablePtr(itemID_t itemID);

    /*------------------------------------------------------------------------*/
    /** Retrieves the image item list
        @return list of image items
    */
    CimageItemList* getImageItemList();

    /*------------------------------------------------------------------------*/
    /** Retrieves the image list
        @return list of images
    */
    Image_PtrList_t *getImageList();

    /*------------------------------------------------------------------------*/
    /** Reads the specific parameter from the cache if not available then
        read from device

        @param pVarItem - contains variable information
        @param value - contains variable value information
        @return status
    */
    PMStatus readCacheValue(VariableType *pVarItem, CValueVarient &value);

    /*------------------------------------------------------------------------*/
    /** Reads the specific parameter from the device

        @param itemId - Item ID of variable
        @param value - contains variable information
        @return status
    */
    PMStatus readDeviceValue(VariableType *varItem, CValueVarient &value);

    /*------------------------------------------------------------------------*/
    /** Reads the specific parameter from the cache if not available then
        read from device

        @param itemId - Item ID of variable
        @param value - contains variable information
        @return status
    */
    PMStatus asyncReadDeviceValue(VariableType* varItem, CValueVarient& value);

    /*------------------------------------------------------------------------*/
    /** Writes the specific parameter

        @param itemId - Item ID of variable
        @param value - contains variable information
        @return status
    */
    PMStatus writeDeviceValue(VariableType *varItem, CValueVarient& value);

    /*------------------------------------------------------------------------*/
    /** Writes the specific parameter to the cache

        @param itemId - Item ID of variable
        @param value - contains variable information
        @return status
    */
    PMStatus writeCacheValue(VariableType *varItem, CValueVarient& value);

    /*------------------------------------------------------------------------*/
    /** Reads the parameters from device

        @return status
    */
    PMStatus asyncReadService();

    /*------------------------------------------------------------------------*/
    /** Perform pre edit actions

        @param item - Item base for pre edit actions
        @return status
    */
    PMStatus preEditActions(ItemID itemID);

    /*------------------------------------------------------------------------*/
    /** Perform pre edit actions for edit display

        @param item - Item base for pre edit actions
        @return status
    */
    PMStatus preEditDispActions(ItemID itemID);

    /*------------------------------------------------------------------------*/
    /** Checks if actions exists for the given variable Item ID or
        not and action type

        @param itemID - holds variable item ID
        @param varActionType - holds variable action type
        @return true or false
    */
    bool varActionExists(ItemID itemID, varAttrType_t varActionType);

    /*------------------------------------------------------------------------*/
    /** Checks if actions exists for the given Edit Display Item ID or
        not and action type

        @param itemID - holds edit display item ID
        @param eddispAttrType_t - holds edit display action type
        @return true or false
    */
    bool editDispActionExists(ItemID itemID, eddispAttrType_t editDispActionType);

    /*------------------------------------------------------------------------*/
    /** Perform post edit actions

        @param item - Item base for post edit actions
        @return status
    */
    PMStatus postEditActions(ItemID itemID);

    /*------------------------------------------------------------------------*/
    /** Perform post edit actions for Edit Display

        @param item - Item base for post edit actions
        @return status
    */
    PMStatus postEditDispActions(ItemID itemID);

    /*------------------------------------------------------------------------*/
    /** Retrieves the image for specified item base

        @param item - Item base for retrive image
        @return status
    */
    PMStatus getImage(ItemID itemID);

    /*------------------------------------------------------------------------*/
    /** gives method information given method Item ID

        @param itemId - Item ID of variable
        @param methodInfo - contains method information
        @return status
    */
    PMStatus getMethodItem(itemID_t itemId, hCmethod **pMethod);

    /*------------------------------------------------------------------------*/
    /** Executes the specified method

        @param itemID - holds method Item ID
        @return status
    */
    PMStatus executeMethod(itemID_t itemID);

    /*------------------------------------------------------------------------*/
    /// It acquires the thread lock and wait till release
    void acquireLock();

    /*------------------------------------------------------------------------*/
    /// It releases the locked thread and notify for further process
    void releaseLock();

    /*------------------------------------------------------------------------*/
    /** Sends method information to adaptor to get acknowledgement from UI

        @param structMethodsUIData - holds method info to display to UI

        @return status
    */
    int displayAckUpcall(ACTION_UI_DATA structMethodsUIData);

    /*------------------------------------------------------------------------*/
    /** Sends method information to adaptor and interanlly waits for specified
        delay time

        @param structMethodsUIData - holds method info to display to UI

        @return status
    */
    int displayDelayUpcall(ACTION_UI_DATA structMethodsUIData);

    /*------------------------------------------------------------------------*/
    /** Sends method information to adaptor to get new selection from user

        @param structMethodsUIData - holds method info to display to UI

        @return status
    */
    int displayMenuUpcall(ACTION_UI_DATA structMethodsUIData,
                          ACTION_USER_INPUT_DATA& structMethodUserInputData);

    /*------------------------------------------------------------------------*/
    /** Sends method information to adaptor to get new value from user

        @param structMethodsUIData - holds method info to display to UI
        @param structMethodUserInputData - holds user input from methods UI
               to SDC core

        @return status
    */
    int displayGetUpcall(ACTION_UI_DATA structMethodsUIData,
                            ACTION_USER_INPUT_DATA& structMethodUserInputData);

    /*------------------------------------------------------------------------*/
    /** Sends method started information to adaptor to notify UI

    */
    int methodStartUpcall();

    /*------------------------------------------------------------------------*/
    /** Sends method completed information to adapor to notify UI

    */
    int methodCompleteUpcall();

    /*------------------------------------------------------------------------*/
    /** Sends method aborted information to adaptor to notify UI

    */
    int methodErrorUpcall(ACTION_UI_DATA structMethodsUIData);

    /*------------------------------------------------------------------------*/
    /** Requests adaptor to notify the internal error occurred during method
        execution
    */
    void sendMethodErrorMessage();

    /*------------------------------------------------------------------------*/
    /** Retrieves the current device object

        @return hCddbDevice instance
    */
    hCddbDevice* getCurrDevice();

    /*------------------------------------------------------------------------*/
    /** Retrieves the DD item type

        @param itemID - contains the item ID
        @param itemType - contains the DD item type

        @return status
    */
    PMStatus getItemType(itemID_t itemID,itemType_t& itemType);

    /*------------------------------------------------------------------------*/
    /** Converts epoch time to user time

        @param epochTime - contains the epoch time
        @param format - contains the time format
    */
    std::string convertsEpochTimeToUserTime(time_t epochTime, const char* format);

    /*------------------------------------------------------------------------*/
    /** Converts user time to epoch time

        @param userTime - contains the user time
        @param format - contains the time format
    */
    time_t convertsUserTimeToEpochTime(const char* userTime, const char* format);

    /*------------------------------------------------------------------------*/
    /*= Explicitly Disabled Methods ==========================================*/
    SDC625Controller& operator=(const SDC625Controller& rhs) = delete;
    SDC625Controller(const SDC625Controller& rhs) = delete;

    /// Mutex Thread Lock
    mutex m_methodExecutionLock;

    /// Condition variable for handle thread
    condition_variable m_methodExecutionConditionVariable;

    /* Static Data Members ===================================================*/
    /// Currently exepecting input type
    static HART_METHOD_EXECUTION_DATA s_hartMEData;

    /* Static Data Members ===================================================*/
    /// Currently executing Variable Action information
    static HART_VAR_ACTION_DATA s_hartVarActionData;

private:

    /* Private Data Members ==================================================*/
    /// Connection Manager instance
    IConnectionManager* m_pCM;

    /// Parameter Cache instance
    IParameterCache* m_pPC;

    /// Parameter Cache instance
    HARTAdaptor* m_pHARTAdaptor;

    /// Common Interface
    hCcommInfc* m_pComm;

    /// Device Identity
    Indentity_t m_identity;

    /// Device manager
    hCDeviceManger* m_pMgr;

    /// DDB Device instance
    hCddbDevice* m_pDev;

    ///Method Support Interface instance
    MethodSupport* m_pMethodSupport;


    hCDeviceManger* createManager(std::string ddLibDir);

    hCddbDevice* createDevice(Indentity_t& oneIdentity, hCcommInfc* pComm,
                              hCDeviceManger* pDevMgr);
};

#endif  // _SDC625_CONTROLLER_H
