#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    macro definitions ######################################################
#############################################################################

#############################################################################
#    directories needed to be built as part of the project ##################
#############################################################################
add_subdirectory(ispro)
add_subdirectory(simatic)
add_subdirectory(loopback)
add_subdirectory(softing)
