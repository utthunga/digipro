
/* 
 * $Id: pb_loopback_data.h,v 1.1 2009/06/19 08:36:03 fna Exp $
 * 
 * Copyright (C) 2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_loopback_data_h
#define _pb_loopback_data_h

#include "condition.h"
#include "driver_interface.h"

enum pb_loopback_error
{
	PB_LOOPBACK_ERROR               = 64,

	PB_LOOPBACK_DEVICE_IN_USE       = PB_LOOPBACK_ERROR|(1 << 8),
	PB_LOOPBACK_NO_RECEIVER         = PB_LOOPBACK_ERROR|(2 << 8),
	PB_LOOPBACK_TO_MANY_CONNECTIONS = PB_LOOPBACK_ERROR|(3 << 8),
};

struct pb_loopback_conn
{
	int used;
	int connected;
};

struct pb_loopback_data
{
	struct pb_dev *dev;

	/* receiver */
	int (*read)(void *arg,
		    struct pb_dev *dev,
		    pb_slave_t slave,
		    struct pb_request *req);
	int (*write)(void *arg,
		    struct pb_dev *dev,
		    pb_slave_t slave,
		    struct pb_request *req);

	/* receiver specific argument */
	void *read_arg;
	void *write_arg;

#define PB_CONNECTIONS_MAX 32
	struct pb_loopback_conn connections[PB_CONNECTIONS_MAX];
};

#endif /* _pb_loopback_data_h */
