
/* 
 * $Id: pb_loopback.c,v 1.4 2009/06/24 22:11:41 fna Exp $
 * 
 * Copyright (C) 2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pb_loopback_data.h"
#include "pb_loopback.h"


/* static int pb_loopback_probe(unsigned char driver, struct pb_info *, size_t len); */

static int pb_loopback_open(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_loopback_close(struct pb_dev *);

static int pb_loopback_ioctl(struct pb_dev *, int request, va_list);

/* static int pb_loopback_set_bus_parameter(struct pb_dev *, const struct pb_bus_parameter *); */

/* static int pb_loopback_get_input(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen); */

/* static int pb_loopback_set_output(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen); */

/* static int pb_loopback_set_mode(struct pb_dev *,
		enum pb_mode mode); */

/* static int pb_loopback_get_mode(struct pb_dev *,
		enum pb_mode *mode); */

/* static int pb_loopback_set_slave_param(struct pb_dev *,
		unsigned char slaveaddr, const char *data, size_t datalen); */

/* static int pb_loopback_change_slave_addr(struct pb_dev *, unsigned char slaveaddr,
		unsigned char addr, unsigned short ident_number); */

/* static int pb_loopback_livelist(struct pb_dev *,
		struct pb_livelist_info *livelist); */

static int pb_loopback_connect(struct pb_dev *,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *);

static int pb_loopback_disconnect(struct pb_dev *, pb_slave_t);

static int pb_loopback_read(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static int pb_loopback_write(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static const char *pb_loopback_errstr(int result, const char **descr);

struct pb_funcs pb_loopback_funcs =
{
	NULL, /* pb_loopback_probe, */
	pb_loopback_open,
	pb_loopback_close,
	pb_loopback_ioctl,
	NULL, /* pb_loopback_set_bus_parameter, */
	NULL, /* pb_loopback_get_input, */
	NULL, /* pb_loopback_set_output, */
	NULL, /* pb_loopback_set_mode, */
	NULL, /* pb_loopback_get_mode, */
	NULL, /* pb_loopback_set_slave_param, */
	NULL, /* pb_loopback_change_slave_addr, */
	NULL, /* pb_loopback_livelist, */
	pb_loopback_connect,
	pb_loopback_disconnect,
	pb_loopback_read,
	pb_loopback_write,
	pb_loopback_errstr,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static char devices[32];

static int pb_loopback_open(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
	struct pb_loopback_data *data;

	if (dev->device >= (sizeof(devices)/sizeof(devices[0])))
		return PB_INVALID_DEVICE;

	if (devices[dev->device])
		return PB_LOOPBACK_DEVICE_IN_USE;

	data = malloc(sizeof(*data));
	if (data)
	{
		memset(data, 0, sizeof(*data));

		data->dev = dev;
		dev->data = data;

		/* mark as used */
		devices[dev->device] = 1;

		return PB_NO_ERROR;
	}

	return PB_OUTOFMEMORY;
}

static int pb_loopback_close(struct pb_dev *dev)
{
	free(dev->data);

	assert(dev->device < (sizeof(devices)/sizeof(devices[0])));
	devices[dev->device] = 0;

	return PB_NO_ERROR;
}

static int pb_loopback_ioctl(struct pb_dev *dev, int request, va_list args)
{
	struct pb_loopback_data *data = dev->data;
	int ret = PB_UNIMPLEMENTED;

	switch (request)
	{
		case 0x55a1:
		{
			data->read = va_arg(args, void *);
			data->read_arg = va_arg(args, void *);

			ret = PB_NO_ERROR;
			break;
		}
		case 0x55a2:
		{
			data->write = va_arg(args, void *);
			data->write_arg = va_arg(args, void *);

			ret = PB_NO_ERROR;
			break;
		}
	}

	return ret;
}

static int pb_loopback_connect(struct pb_dev *dev,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *slave)
{
	struct pb_loopback_data *data = dev->data;
	struct pb_loopback_conn *conn;
	int i;

	if (!data->read || !data->write)
		return PB_LOOPBACK_NO_RECEIVER;

	for (i = 0; i < PB_CONNECTIONS_MAX; i++)
	{
		if (!data->connections[i].used)
		{
			data->connections[i].used = 1;
			break;
		}
	}

	if (i == PB_CONNECTIONS_MAX)
		return PB_LOOPBACK_TO_MANY_CONNECTIONS;

	conn = &(data->connections[i]);
	*slave = conn;

	return PB_NO_ERROR;
}

static int pb_lookup_conn(struct pb_loopback_data *data, struct pb_loopback_conn *conn)
{
	int i;

	for (i = 0; i < PB_CONNECTIONS_MAX; i++)
	{
		if (&(data->connections[i]) == conn)
			return i;
	}

	return -1;
}

static int pb_loopback_disconnect(struct pb_dev *dev, pb_slave_t slave)
{
	struct pb_loopback_data *data = dev->data;
	struct pb_loopback_conn *conn = slave;
	int i;

	i = pb_lookup_conn(data, conn);
	if (i == -1)
		return PB_INVALID_HANDLE;

	/* free connection */
	conn->used = 0;

	return PB_NO_ERROR;
}

static int pb_loopback_read(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *pb_req)
{
	struct pb_loopback_data *data = dev->data;
	struct pb_loopback_conn *conn = slave;

	if (pb_lookup_conn(data, conn) == -1)
		return PB_INVALID_HANDLE;

	if (data->read)
		return data->read(data->read_arg, dev, slave, pb_req);

	return PB_LOOPBACK_NO_RECEIVER;
}

static int pb_loopback_write(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *pb_req)
{
	struct pb_loopback_data *data = dev->data;
	struct pb_loopback_conn *conn = slave;

	if (pb_lookup_conn(data, conn) == -1)
		return PB_INVALID_HANDLE;

	if (data->write)
		return data->write(data->write_arg, dev, slave, pb_req);

	return PB_LOOPBACK_NO_RECEIVER;
}

#ifndef _
# define _(x) x
#endif

static const char *pb_loopback_errstr(int result, const char **descr)
{
	const char *msg = _("unknown error in loopback driver");

	switch (result)
	{
		case PB_LOOPBACK_DEVICE_IN_USE:
			msg = _("device in use");
			break;
		case PB_LOOPBACK_NO_RECEIVER:
			msg = _("no receiver assigned");
			break;
		case PB_LOOPBACK_TO_MANY_CONNECTIONS:
			msg = _("to many connections");
			break;
	}

	assert(msg);
	return msg;
}
