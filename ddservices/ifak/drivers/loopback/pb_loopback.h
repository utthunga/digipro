
/* 
 * $Id: pb_loopback.h,v 1.1 2009/06/19 08:36:03 fna Exp $
 * 
 * Copyright (C) 2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_loopback_h
#define _pb_loopback_h

#include "driver_interface.h"

extern struct pb_funcs pb_loopback_funcs;

#endif /* _pb_loopback_h */
