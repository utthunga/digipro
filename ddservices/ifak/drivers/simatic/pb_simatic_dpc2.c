
/* 
 * $Id: pb_simatic_dpc2.c,v 1.9 2009/07/07 17:06:09 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "pb_simatic_dpc2_data.h"
#include "pb_simatic_dpc2_dll.h"
#include "pb_simatic_dpc2.h"


static int pb_simatic_dpc2_probe(unsigned char driver, struct pb_info *, size_t len);

static int pb_simatic_dpc2_open(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_simatic_dpc2_close(struct pb_dev *);

/* static int pb_simatic_dpc2_ioctl(struct pb_dev *, int request, va_list); */

/* static int pb_simatic_dpc2_set_bus_parameter(struct pb_dev *, const struct pb_bus_parameter *); */

/* static int pb_simatic_dpc2_get_input(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen); */

/* static int pb_simatic_dpc2_set_output(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen); */

/* static int pb_simatic_dpc2_set_mode(struct pb_dev *,
		enum pb_mode mode); */

/* static int pb_simatic_dpc2_get_mode(struct pb_dev *,
		enum pb_mode *mode); */

/* static int pb_simatic_dpc2_set_slave_param(struct pb_dev *,
		unsigned char slaveaddr, const char *data, size_t datalen); */

/* static int pb_simatic_dpc2_change_slave_addr(struct pb_dev *, unsigned char slaveaddr,
		unsigned char addr, unsigned short ident_number); */

/* static int pb_simatic_dpc2_livelist(struct pb_dev *,
		struct pb_livelist_info *livelist); */

static int pb_simatic_dpc2_connect(struct pb_dev *,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *);

static int pb_simatic_dpc2_disconnect(struct pb_dev *, pb_slave_t);

static int pb_simatic_dpc2_read(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static int pb_simatic_dpc2_write(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static const char *pb_simatic_dpc2_errstr(int result, const char **descr);

struct pb_funcs pb_simatic_dpc2_funcs =
{
	pb_simatic_dpc2_probe,
	pb_simatic_dpc2_open,
	pb_simatic_dpc2_close,
	NULL, /* pb_simatic_dpc2_ioctl, */
	NULL, /* pb_simatic_dpc2_set_bus_parameter, */
	NULL, /* pb_simatic_dpc2_get_input, */
	NULL, /* pb_simatic_dpc2_set_output, */
	NULL, /* pb_simatic_dpc2_set_mode, */
	NULL, /* pb_simatic_dpc2_get_mode, */
	NULL, /* pb_simatic_dpc2_set_slave_param, */
	NULL, /* pb_simatic_dpc2_change_slave_addr, */
	NULL, /* pb_simatic_dpc2_livelist, */
	pb_simatic_dpc2_connect,
	pb_simatic_dpc2_disconnect,
	pb_simatic_dpc2_read,
	pb_simatic_dpc2_write,
	pb_simatic_dpc2_errstr,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

#define MAX_NR_OF_CARDS 16

static int pb_simatic_dpc2_probe(unsigned char driver, struct pb_info *info, size_t len)
{
	int found = 0;
	int ret;

	ret = SimaticDpc2OpenDLL();
	if (ret == 0)
	{
		int i;

		for (i = 0; i < MAX_NR_OF_CARDS; i++)
		{
			struct dpc2_error error;
			unsigned long board_ref;
			unsigned short ret;

			ret = pdpc2_activate(i, DPC2_SYNC, &board_ref, &error);
			if (ret == DPC2_OK)
			{
				info[found].driver = driver;
				info[found].device = i;
				info[found].in_use = 0;
				info[found].channels = 1;
				info[found].flags = 0;

				snprintf(info[found].name, sizeof(info[found].name),
					"Simatic CP_L2_%i:/SCP", i);

				pdpc2_reset(board_ref, &error);

				++found;
				if ((size_t)found >= len)
					break;
			}
		}

		SimaticDpc2CloseDLL();
	}

	return found;
}

static int pb_simatic_dpc2_error(const struct dpc2_error *error)
{
	assert(error);

	switch (error->Error_Class)
	{
		case DPC2_OK:
		{
			return PB_NO_ERROR;
		}
		case DPC2_ERROR_EVENT:
		{
			return dpv1error2result(error->Error_Decode,
						error->Error_Code_1,
						error->Error_Code_2);
		}
		case DPC2_ERROR_EVENT_NET:
		{
			int Error_Code = error->Error_Code;
			return PB_SIMATIC_DPC2_EVENT_NET | (Error_Code << 16);
		}
		case DPC2_ERROR_REQUEST_PARAM:
		{
			int Error_Code = error->Error_Code;
			return PB_SIMATIC_DPC2_REQUEST_PARAM | (Error_Code << 16);
		}
		case DPC2_ERROR_BOARD_ACCESS:
		{
			int Error_Code = error->Error_Code;
			return PB_SIMATIC_DPC2_BOARD_ACCESS | (Error_Code << 16);
		}
		case DPC2_ERROR_INT_RESOURCE:
		{
			int Error_Code = error->Error_Code;
			return PB_SIMATIC_DPC2_INT_RESOURCE | (Error_Code << 16);
		}
	}

	return PB_SIMATIC_DPC2_UNKNOWN;
}

static int pb_simatic_dpc2_open(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
	struct pb_simatic_dpc2_data *data;
	struct dpc2_error error;
	int ret;

	if (dev->device >= MAX_NR_OF_CARDS)
		return PB_INVALID_DEVICE;

	ret = SimaticDpc2OpenDLL();
	if (ret)
		return ret;

	data = malloc(sizeof(*data));
	if (data)
	{
		memset(data, 0, sizeof(*data));

		data->dev = dev;
		dev->data = data;
	}
	else
	{
		ret = PB_OUTOFMEMORY;
		goto error;
	}

	pdpc2_activate(dev->device, DPC2_SYNC, &(data->board_ref), &error);
	ret = pb_simatic_dpc2_error(&error);
	if (ret != PB_NO_ERROR)
		goto error;

	return PB_NO_ERROR;

error:
	SimaticDpc2CloseDLL();

	if (data)
		free(data);

	return ret;
}

static int pb_simatic_dpc2_close(struct pb_dev *dev)
{
	struct pb_simatic_dpc2_data *data = dev->data;
	struct dpc2_error error;
	int ret;

	pdpc2_reset(data->board_ref, &error);
	ret = pb_simatic_dpc2_error(&error);

	SimaticDpc2CloseDLL();
	free(data);

	return ret;
}

static int pb_simatic_dpc2_connect(struct pb_dev *dev,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *slave)
{
	struct pb_simatic_dpc2_data *data = dev->data;
	struct pb_simatic_dpc2_conn *conn = NULL;
	struct dpc2_initiate_rb request;
	int i, ret;

	for (i = 0; i < CONNECTIONS_MAX; i++)
	{
		if (!data->connections[i].used)
		{
			data->connections[i].used = 1;
			conn = &(data->connections[i]);
			break;
		}
	}

	if (!conn)
		return PB_SIMATIC_DPC2_TO_MANY_CONNECTIONS;

	memset(&request, 0, sizeof(request));
	request.Rem_Add = slaveaddr;
	request.Features_Supported_1_m = 1;
	request.Add_Addr_Param.S_Len = DPC2_S_ADDR_SIZE;
	request.Add_Addr_Param.D_Len = DPC2_D_ADDR_SIZE;

	if (palinkaddr)
	{
		request.Rem_Add = palinkaddr;
		request.Add_Addr_Param.D_Type = 1;
		request.Add_Addr_Param.D_Addr.MAC_Address_Len = 1;
     		request.Add_Addr_Param.D_Addr.MAC_Address = &(data->D_MAC_Address);
		data->D_MAC_Address = slaveaddr;
	}

	pdpc2_initiate(data->board_ref, DPC2_SYNC_ORDERID, &request);

	ret = pb_simatic_dpc2_error(&(request.error));
	if (ret == PB_NO_ERROR)
	{
		conn->c_ref = request.C_Ref;
		*slave = conn;
	}
	else
		conn->used = 0;

	return ret;
}

static int
lookup_conn(struct pb_simatic_dpc2_data *data, struct pb_simatic_dpc2_conn *conn)
{
	int i;

	for (i = 0; i < CONNECTIONS_MAX; i++)
	{
		if (&(data->connections[i]) == conn)
			return i;
	}

	return -1;
}

static int pb_simatic_dpc2_disconnect(struct pb_dev *dev, pb_slave_t slave)
{
	struct pb_simatic_dpc2_data *data = dev->data;
	struct pb_simatic_dpc2_conn *conn = slave;
	struct dpc2_abort_rb request;

	if (lookup_conn(data, conn) == -1)
		return PB_INVALID_HANDLE;

	memset(&request, 0, sizeof(request));
	request.C_Ref = conn->c_ref;
	request.Locally_Generated = DPC2_LOCALLY_GENERATED;
	request.Subnet = DPC2_SUBNET_NO;
	request.Instance = DPC2_INSTANCE_USER;
	request.Reason_Code = 0;

	pdpc2_abort(DPC2_SYNC_ORDERID, &request);
	conn->used = 0;

	return pb_simatic_dpc2_error(&(request.error));
}

static int pb_simatic_dpc2_read(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *req)
{
	struct pb_simatic_dpc2_data *data = dev->data;
	struct pb_simatic_dpc2_conn *conn = slave;
	struct dpc2_read_rb request;
	int ret;

	if (lookup_conn(data, conn) == -1)
		return PB_INVALID_HANDLE;

	memset(&request, 0, sizeof(request));
	request.C_Ref = conn->c_ref;
	request.Slot_Number = req->slot;
	request.Index = req->index;
	request.Length_s = req->datalen; // must be DPC2_DATA_LEN_S following SIEMENS docu
	request.Data_s = data->buf;

	pdpc2_read(DPC2_SYNC_ORDERID, &request);

	ret = pb_simatic_dpc2_error(&(request.error));
	if (ret == PB_NO_ERROR)
	{
		req->datalen = request.Length_s;
		memcpy(req->data,
		       data->buf,
		       req->datalen);
	}

	return ret;
}

static int pb_simatic_dpc2_write(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *req)
{
	struct pb_simatic_dpc2_data *data = dev->data;
	struct pb_simatic_dpc2_conn *conn = slave;
	struct dpc2_write_rb request;

	if (lookup_conn(data, conn) == -1)
		return PB_INVALID_HANDLE;

	memset(&request, 0, sizeof(request));
	request.C_Ref = conn->c_ref;
	request.Slot_Number = req->slot;
	request.Index = req->index;
	request.Length_m = req->datalen;
	request.Data_m = req->data;

	pdpc2_write(DPC2_SYNC_ORDERID, &request);

	return pb_simatic_dpc2_error(&(request.error));
}

#ifndef _
# define _(x) x
#endif

static const char *pb_simatic_dpc2_errstr(int result, const char **descr)
{
	static char buf[250];
	const char *msg = NULL;

	switch (result & 0xffff)
	{
		case PB_SIMATIC_DPC2_TO_MANY_CONNECTIONS:
			msg = _("to many connections");
			break;
		case PB_SIMATIC_DPC2_LOADING_DLL:
			msg = _("error loading simatic dpc2 library");
			break;
		case PB_SIMATIC_DPC2_EVENT_NET:
			snprintf(buf, sizeof(buf), _("slave communication error [ EVENT_NET 0x%x ]"),
				 result >> 16);
			msg = buf;
			break;
		case PB_SIMATIC_DPC2_REQUEST_PARAM:
			msg = _("invalid parameter in request");
			break;
		case DPC2_ERROR_BOARD_ACCESS:
			snprintf(buf, sizeof(buf), _("error while accessing the CP [ BOARD_ACCESS 0x%x ]"),
				 result >> 16);
			msg = buf;
			break;
		case DPC2_ERROR_INT_RESOURCE:
			snprintf(buf, sizeof(buf), _("not enough resources [ INT_RESOURCES 0x%x ]"),
				 result >> 16);
			msg = buf;
			break;
		default:
			msg = _("unknown error in simatic dpc2 driver");
			break;
	}

	assert(msg);
	return msg;
}
