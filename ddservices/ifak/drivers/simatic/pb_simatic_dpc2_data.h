
/* 
 * $Id: pb_simatic_dpc2_data.h,v 1.3 2009/04/01 23:16:40 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_simatic_dpc2_data_h
#define _pb_simatic_dpc2_data_h

#include "condition.h"
#include "driver_interface.h"

#define DP32STD_DLL
#include "dpc2_usr.h"

enum pb_simatic_dpc2_error
{
	PB_SIMATIC_DPC2_ERROR               = 64,

	PB_SIMATIC_DPC2_UNKNOWN             = PB_SIMATIC_DPC2_ERROR|(1 << 8),
	PB_SIMATIC_DPC2_TO_MANY_CONNECTIONS = PB_SIMATIC_DPC2_ERROR|(2 << 8),
	PB_SIMATIC_DPC2_LOADING_DLL         = PB_SIMATIC_DPC2_ERROR|(3 << 8),
	PB_SIMATIC_DPC2_EVENT_NET           = PB_SIMATIC_DPC2_ERROR|(4 << 8),
	PB_SIMATIC_DPC2_REQUEST_PARAM       = PB_SIMATIC_DPC2_ERROR|(5 << 8),
	PB_SIMATIC_DPC2_BOARD_ACCESS        = PB_SIMATIC_DPC2_ERROR|(6 << 8),
	PB_SIMATIC_DPC2_INT_RESOURCE        = PB_SIMATIC_DPC2_ERROR|(7 << 8),
};

struct pb_simatic_dpc2_conn
{
	int used;
	int c_ref;
};

struct pb_simatic_dpc2_data
{
	struct pb_dev *dev;
	unsigned long board_ref;
	unsigned char D_MAC_Address;

#define CONNECTIONS_MAX 32
	struct pb_simatic_dpc2_conn connections[CONNECTIONS_MAX];

	unsigned char buf[DPC2_DATA_LEN_S];
};

#endif /* _pb_simatic_dpc2_data_h */
