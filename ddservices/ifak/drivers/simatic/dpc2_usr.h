/***************************************************************************/
/*    Copyright (c) SIEMENS AG, 1997                                       */
/*    All Rights Reserved.                                                 */
/***************************************************************************/
/*                                                                         */
/*    Component            &K: DPC2-Library           :K&                  */
/*                                                                         */
/*    Name of module       &M: dpc2_usr.h             :M&                  */
/*                                                                         */
/*    Version              &V: V1.0                   :V&                  */
/*                                                                         */
/***************************************************************************/
/*    Description:  This file contains the declarations needed by          */
/*                  a DPC2 user application                                */
/***************************************************************************/

#ifndef __DPC2_USER__			/* DMo 04.06.97*/
#define __DPC2_USER__			


#ifdef WIN32
# pragma pack (1)
#endif



 /***************/
 /* Event Types */
 /***************/
 #define DPC2_NO_CONFIRMATION           0x0000

 #define DPC2_INITIATE_EVENT            0x0001
 #define DPC2_DATA_TRANSPORT_EVENT      0x0002
 #define DPC2_WRITE_EVENT               0x0003
 #define DPC2_READ_EVENT                0x0004
 #define DPC2_ABORT_EVENT               0x0005

 #define DPC2_OPEN_EVENT                0x0006 // internally used
 #define DPC2_DATA_TRANSPORT_INT_EVENT  0x0007 // internally used

 /******************************************/
 /* (a)synchronous access (dpc2_activate)  */
 /******************************************/

 #define DPC2_SYNC   0x0000
 #define DPC2_ASYNC  0x0001

 /**************************************/
 /* orderid for synchronous functions  */
 /**************************************/

#define  DPC2_SYNC_ORDERID   0xffff


 /*************************************/
 /* Values for dpc2_abort() - request */
 /*************************************/

 #define DPC2_LOCALLY_GENERATED      0xff
 #define DPC2_REMOTE_GENERATED       0x00

 #define DPC2_SUBNET_NO              0x00
 #define DPC2_SUBNET_LOCAL           0x01
 #define DPC2_SUBNET_REMOTE          0x02
 
 #define DPC2_INSTANCE_FDL           0x00
 #define DPC2_INSTANCE_DDLM          0x01
 #define DPC2_INSTANCE_USER          0x02

 #define DPC2_REASON_CODE_ABT_SE     0x01
 #define DPC2_REASON_CODE_ABT_FE     0x02   
 #define DPC2_REASON_CODE_ABT_TO     0x03
 #define DPC2_REASON_CODE_ABT_RE     0x04
 #define DPC2_REASON_CODE_ABT_IV     0x05
 #define DPC2_REASON_CODE_ABT_STO    0x06
 #define DPC2_REASON_CODE_ABT_IA     0x07
 #define DPC2_REASON_CODE_ABT_OC     0x08

  
  

 /**************/
 /* Structures */
 /**************/

 struct dpc2_error
 {
  unsigned char Error_Decode; 
  unsigned char Error_Code_1;
  unsigned char Error_Code_2;
  unsigned short Error_Class;  
  unsigned short Error_Code;  
 };


 
 struct dpc2_add_addr_param
 {
   unsigned char S_Type;
   unsigned char S_Len;
   unsigned char D_Type;
   unsigned char D_Len;
   struct
   {
     unsigned char  API;
     unsigned char  Reserved;
     unsigned char  Network_Address[6];
     unsigned short MAC_Address_Len;
     unsigned char  * MAC_Address;
   } S_Addr;

   struct
   { unsigned char API;
     unsigned char Reserved;
     unsigned char Network_Address[6];
     unsigned short MAC_Address_Len;
     unsigned char * MAC_Address;
   } D_Addr;
 };


 struct dpc2_data_transport_rb
 { // in
   unsigned long C_Ref;
   unsigned char Slot_Number; 
   unsigned char Index; 
   unsigned char Length_m;
   unsigned char *Data_m; 
   // out
   unsigned char Length_s; 
   unsigned char *Data_s; 
   struct dpc2_error error; 
 };


 struct dpc2_read_rb
 { 
  // in
  unsigned long C_Ref; 
  unsigned char Slot_Number;
  unsigned char Index; 
  // out
  unsigned char Length_s; 
  unsigned char *Data_s; 
  struct dpc2_error error; 
 };


 struct dpc2_write_rb
 { 
  // in
  unsigned long C_Ref; 
  unsigned char Slot_Number;
  unsigned char Index; 
  unsigned char Length_m; 
  unsigned char *Data_m; 
  // out
  struct dpc2_error error; 
 };


 struct dpc2_initiate_rb
 {
   // in
   unsigned char     Rem_Add;
   unsigned char     Features_Supported_1_m;
   unsigned char     Features_Supported_2_m;
   unsigned char     Profile_Features_Supported_1_m;
   unsigned char     Profile_Features_Supported_2_m;
   unsigned short    Profile_Ident_Number_m;
   struct dpc2_add_addr_param Add_Addr_Param; 
   // out  
   unsigned char     Features_Supported_1_s;
   unsigned char     Features_Supported_2_s;
   unsigned char     Profile_Features_Supported_1_s;
   unsigned char     Profile_Features_Supported_2_s;
   unsigned short    Profile_Ident_Number_s;
   struct dpc2_error error;
   unsigned long     C_Ref;
 };


struct dpc2_abort_rb
{
   unsigned long     C_Ref;
   unsigned char     Locally_Generated;
   unsigned char     Subnet;
   unsigned char     Instance;
   unsigned char     Reason_Code;
   unsigned short    Additional_Detail;
   struct dpc2_error error;
};


 /*************/
 /* Constants */
 /*************/

#define DPC2_S_ADDR_SIZE    14
#define DPC2_D_ADDR_SIZE    14
#define DPC2_DATA_LEN_S    240


/* Maximal values */
/* ============== */

#define DPC2_INITIATE_MAX_REM_ADD        126 
#define DPC2_DATA_TRANSPORT_MAX_LENGTH_M 240
#define DPC2_WRITE_MAX_LENGTH_M          240



/***************/
/* Prototyping */
/***************/


/* Win 32 */
/* ------ */

#ifdef WIN32

  #ifdef DP32STD_DLL
    #define CLV   _stdcall
  #else
    #define CLV   PASCAL
  #endif
          
  
  #ifdef  MIDL_PASS     /* necessary for WINDOSW NT */
    #define W32EXPORT __declspec(dllimport)
  #else               
    #define W32EXPORT __declspec(dllexport)
  #endif

#endif

#ifndef W32EXPORT
# define W32EXPORT
#endif

#ifndef CLV
# define CLV
#endif

/* C, C++ */
/* ------ */  
      
#ifdef __cplusplus
  #define CT  "C"     /* necessary for C++ */
#else
  #define CT  
#endif



  extern CT W32EXPORT unsigned short  CLV   dpc2_activate  (unsigned short    board,
                                                            unsigned short    async,
                                                            unsigned long     *board_ref_ptr,
                                                            struct dpc2_error *error_ptr);




  extern CT W32EXPORT unsigned short  CLV   dpc2_reset     (unsigned long     board_ref,
                                                            struct dpc2_error *error_ptr );

  extern CT W32EXPORT unsigned short  CLV   dpc2_get_event (unsigned long  board_ref,
                                                            unsigned short *event_type_ptr,
                                                            unsigned short *orderid_ptr,
                                                            void *         *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_initiate  (unsigned long           board_ref,
                                                            unsigned short          orderid,
                                                            struct dpc2_initiate_rb *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_abort     (unsigned short order_id,struct dpc2_abort_rb *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_data_transport  (unsigned short orderid,
                                                                  struct dpc2_data_transport_rb *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_read      (unsigned short      orderid,
                                                            struct dpc2_read_rb *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_write     (unsigned short       orderid,
                                                            struct dpc2_write_rb *request_ptr);

  extern CT W32EXPORT unsigned short  CLV   dpc2_use_messages (unsigned long     board_ref,
                                                               unsigned long     hWnd,
                                                               unsigned long     msgID,
                                                               struct dpc2_error *error_ptr);


// only for internal use (V 6.1.0)

  extern CT W32EXPORT unsigned short  CLV  dpc2_activate_name  (char              *board_name,
                                                                unsigned short    async,
                                                                unsigned long     *board_ref_ptr,
                                                                struct dpc2_error *error_ptr);
                                                                //Example "board_name": "/CP5611(PROFIBUS)/SCP"

 /***************/
 /* Error Class */
 /***************/


 #define DPC2_OK                           0x0000
 #define DPC2_NO_EVENT                     0x0001
 #define DPC2_ERROR_EVENT                  0x0002
 #define DPC2_ERROR_EVENT_NET              0x0003
 #define DPC2_ERROR_REQUEST_PARAM          0x0004
 #define DPC2_ERROR_BOARD_ACCESS           0x0005
 #define DPC2_ERROR_INT_RESOURCE           0x0006




 /***************/
 /* Error Codes */ 
 /***************/

 /* class DPC2_ERROR_REQUEST_PARAM */
 /* ------------------------------ */

 #define DPC2_ERROR_BOARD_REF              0x1000
 #define DPC2_ERROR_C_REF                  0x1001
 #define DPC2_PAR_REM_ADD                  0x1002
 #define DPC2_PAR_S_ADDR_S_TYPE            0x1003
 #define DPC2_PAR_S_ADDR_D_TYPE            0x1004
 #define DPC2_PAR_S_LEN                    0x1005
 #define DPC2_PAR_D_LEN                    0x1006
 #define DPC2_PAR_S_ADDR_MAC_ADDRESS       0x1007
 #define DPC2_PAR_D_ADDR_MAC_ADDRESS       0x1008
 #define DPC2_PAR_DATA_LEN_M               0x1009
 #define DPC2_PAR_DATA_DAT_M               0x100a
 #define DPC2_PAR_DATA_LEN_S               0x100b
 #define DPC2_PAR_DATA_DAT_S               0x100c
 #define DPC2_PAR_LOC_GEN                  0x100d
 #define DPC2_PAR_REASON_CODE              0x100e
 #define DPC2_PAR_ZERO_PTR                 0x100f
 #define DPC2_PAR_SYNC                     0x1010
 #define DPC2_PAR_ORDERID                  0x0011
 #define DPC2_ERROR_ASYNC                  0x1012
 #define DPC2_ERROR_SYNC                   0x1013
 #define DPC2_PAR_HWND_MSGID               0x1014

 /* class DPC2_ERROR_INT_RESOURCE  */
 /* ------------------------------ */

 #define DPC2_RES_USR_LIMIT                0x2000
 #define DPC2_RES_REQ_LIMIT                0x2001
 #define DPC2_ERROR_MEMORY                 0x2002
 #define DPC2_REQ_OVERRUN                  0x2003


 /* class DPC2_ERROR_EVENT_NET */
 /* ---------------------------*/

 #define DPC2_NET_UNKNOWN_REFERENCE        0x0006
 #define DPC2_NET_REM_ABORT                0x000e
 #define DPC2_NET_LOC_TIMEOUT              0x0010
 #define DPC2_NET_CONN_REJECT              0x0016
 #define DPC2_NET_NETWORK_ERROR            0x001c
 #define DPC2_NET_NO_RESOURCES             0x0004
 #define DPC2_NET_CONNECTION_DOWN          0x0009
 #define DPC2_NET_PROTOCOL_ERR             0x001e
  
 
 /* class DPC2_ERROR_BOARD_ACCESS */
 /* ----------------------------- */

 #define E_RESOURCES                       0x012a  
 #define E_PAR_ERR                         0x012e  
 #define E_DPRAM                           0x0132 
 #define SCP_RESOURCE                      0x00ca  
 #define SCP_CONFIG_ERR                    0x00cb
 #define SCP_DEVOPEN                       0x00cf
 #define SCP_BOARD                         0x00d0
 #define SCP_BOARD_OPEN                    0x00e7
 #define SCP_NO_WIN_SERV                   0x00e9
 #define EPROTECT                          0x00ea 
 #define SCP_SEND_NOT_SUCCESSFUL           0x00f2
 #define SCP_RECEIVE_NOT_SUCCESSFUL        0x00f3
 #define SCP_NO_DEVICE_AVAILABLE           0x00f4
     

#ifdef WIN32
# pragma pack ()
#endif

#endif			// __DPC2_USER__ 

/***************************************************************************/
/*    Copyright (c) SIEMENS AG, 1997                                       */
/*    All Rights Reserved.                                                 */
/***************************************************************************/



