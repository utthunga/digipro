
/* 
 * $Id: pb_simatic_dpc2_dll.c,v 1.2 2006/06/28 14:20:23 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdlib.h>

#include "pb_simatic_dpc2_dll.h"


static size_t refs = 0;

#ifdef WIN32
#include <windows.h>
static HINSTANCE handle;
#define LIBRARY "dpc2lib.dll"
#define library_load(path)		LoadLibrary(path)
#define library_close(handle)		FreeLibrary(handle)
#define library_symbol(handle, name)	GetProcAddress(handle, name)
#else
void *handle;
#include <dlfcn.h>
#define LIBRARY "dpc2lib.so.1"
#define library_load(path)		dlopen(path, RTLD_LAZY)
#define library_close(handle)		dlclose(handle)
#define library_symbol(handle, name)	dlsym(handle, name)
#endif

dpc2_activate_type       pdpc2_activate;
dpc2_reset_type          pdpc2_reset;
dpc2_get_event_type      pdpc2_get_event;
dpc2_initiate_type       pdpc2_initiate;
dpc2_abort_type          pdpc2_abort;
dpc2_data_transport_type pdpc2_data_transport;
dpc2_read_type           pdpc2_read;
dpc2_write_type          pdpc2_write;
dpc2_use_messages_type   pdpc2_use_messages;

int
SimaticDpc2OpenDLL(void)
{
	int ret = PB_SIMATIC_DPC2_LOADING_DLL;

	if (refs == 0)
	{
		handle = library_load(LIBRARY);
		if (handle)
		{
			pdpc2_activate       = (dpc2_activate_type)       library_symbol(handle, "dpc2_activate");
			pdpc2_reset          = (dpc2_reset_type)          library_symbol(handle, "dpc2_reset");
			pdpc2_get_event      = (dpc2_get_event_type)      library_symbol(handle, "dpc2_get_event");
			pdpc2_initiate       = (dpc2_initiate_type)       library_symbol(handle, "dpc2_initiate");
			pdpc2_abort          = (dpc2_abort_type)          library_symbol(handle, "dpc2_abort");
			pdpc2_data_transport = (dpc2_data_transport_type) library_symbol(handle, "dpc2_data_transport");
			pdpc2_read           = (dpc2_read_type)           library_symbol(handle, "dpc2_read");
			pdpc2_write          = (dpc2_write_type)          library_symbol(handle, "dpc2_write");
			pdpc2_use_messages   = (dpc2_use_messages_type)   library_symbol(handle, "dpc2_use_messages");

			if (
			       pdpc2_activate
			    && pdpc2_reset
			    && pdpc2_get_event
			    && pdpc2_initiate
			    && pdpc2_abort
			    && pdpc2_data_transport
			    && pdpc2_read
			    && pdpc2_write
			    && pdpc2_use_messages
			)
			{
				ret = 0;
				refs++;
			}
			else
				library_close(handle);
		}
	}
	else
	{
		ret = 0;
		refs++;
	}

	return ret;
}

void
SimaticDpc2CloseDLL(void)
{
	assert(refs);

	if (--refs == 0)
		library_close(handle);
}
