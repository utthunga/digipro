
/* 
 * $Id: pb_simatic_dpc2_dll.h,v 1.2 2006/06/28 14:20:23 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_simatic_dpc2_dll_h
#define _pb_simatic_dpc2_dll_h

#include "pb_simatic_dpc2_data.h"


int SimaticDpc2OpenDLL(void);
void SimaticDpc2CloseDLL(void);


/* interface types */

typedef unsigned short (CLV *dpc2_activate_type)(
				unsigned short board,
				unsigned short async,
				unsigned long *board_ref_ptr,
				struct dpc2_error *error_ptr);

typedef unsigned short (CLV *dpc2_reset_type)(
				unsigned long board_ref,
				struct dpc2_error *error_ptr);

typedef unsigned short (CLV *dpc2_get_event_type)(
				unsigned long board_ref,
				unsigned short *event_type_ptr,
				unsigned short *orderid_ptr,
				void **request_ptr);

typedef unsigned short (CLV *dpc2_initiate_type)(
				unsigned long board_ref,
				unsigned short orderid,
				struct dpc2_initiate_rb *request_ptr);

typedef unsigned short (CLV *dpc2_abort_type)(
				unsigned short order_id,
				struct dpc2_abort_rb *request_ptr);

typedef unsigned short (CLV *dpc2_data_transport_type)(
				unsigned short orderid,
				struct dpc2_data_transport_rb *request_ptr);

typedef unsigned short (CLV *dpc2_read_type)(
				unsigned short orderid,
                                struct dpc2_read_rb *request_ptr);

typedef unsigned short (CLV *dpc2_write_type)(
				unsigned short orderid,
				struct dpc2_write_rb *request_ptr);

typedef unsigned short (CLV *dpc2_use_messages_type)(
				unsigned long board_ref,
				unsigned long hWnd,
				unsigned long msgID,
				struct dpc2_error *error_ptr);

/* the entry points */

extern dpc2_activate_type		pdpc2_activate;
extern dpc2_reset_type			pdpc2_reset;
extern dpc2_get_event_type		pdpc2_get_event;
extern dpc2_initiate_type		pdpc2_initiate;
extern dpc2_abort_type			pdpc2_abort;
extern dpc2_data_transport_type		pdpc2_data_transport;
extern dpc2_read_type			pdpc2_read;
extern dpc2_write_type			pdpc2_write;
extern dpc2_use_messages_type		pdpc2_use_messages;

#endif /* _pb_simatic_dpc2_dll_h */
