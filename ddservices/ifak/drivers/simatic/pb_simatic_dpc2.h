
/* 
 * $Id: pb_simatic_dpc2.h,v 1.2 2009/06/19 08:42:00 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_simatic_dpc2_h
#define _pb_simatic_dpc2_h

#include "driver_interface.h"

extern struct pb_funcs pb_simatic_dpc2_funcs;

#endif /* _pb_simatic_dpc2_h */
