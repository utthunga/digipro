/***********************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    git clone git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - FLUKE IDC
***********************************************************************/

#include <pthread.h>
#include "fbapi_if.h"
#include "pb_if.h"
#include "pb_fmb.h"
#include "pb_type.h"
#include "pb_softing_data.h"

// Softing driver specific error codes are defined here
#define PB_SOFTING_ERROR                64
#define PB_SOFTING_CONFIG_ERR           (PB_SOFTING_ERROR + 1)
#define PB_SOFTING_ACT_ERR              (PB_SOFTING_ERROR + 2)
#define PB_SOFTING_BUS_PARAM_ERR        (PB_SOFTING_ERROR + 3)
#define PB_SOFTING_LIVE_LIST_ERR        (PB_SOFTING_ERROR + 4)
#define PB_SOFTING_FMB_IND              (PB_SOFTING_ERROR + 5)
#define PB_SOFTING_READ_ERR             (PB_SOFTING_ERROR + 6)
#define PB_SOFTING_WRITE_ERR            (PB_SOFTING_ERROR + 7)
#define PB_SOFTING_INIT_ERR             (PB_SOFTING_ERROR + 8)
#define PB_SOFTING_CONN_ERR             (PB_SOFTING_ERROR + 9)


#define DEFAULT_INITIAL       0
#define DEFAULT_PARAMS        1
#define DEFAULT_MASTER        2

#define WRITE_PARAM_LENGTH    2
#define MAX_CHECKING_TIME     20
#define MAX_BUFF_LEN          32

#define CON_IND_TIMEOUT       10

#define MAX_NUM_SLAVES        125
#define MAX_BUS_PARAM_LEN     128
#define MAX_WRITE_LEN         240
#define DEFAULT_BUFF_LEN      255
#define MAX_SLAVE_ENTRY       300
#define RCV_BUFFER_LEN        512
#define MAX_WRITE_BUFF_LEN    1024

// Maximum retry for the response from the passive station
#define MAX_RETRY_LIMIT       25

struct pbCommReq g_comm_req;

pthread_attr_t threadAttr;
char g_response_data[RCV_BUFFER_LEN];

void acquireRequestLock();
void releaseRequestLock();

int createPbStackThread(struct pbSoftingData* pbData);

void dpSetMaxLength(int maxDataLen);
