/***********************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    git clone git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - FLUKE IDC
***********************************************************************/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#include "pb_softing.h"
#include "pb_softing_service.h"

static int pb_softing_open(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_softing_close(struct pb_dev *);

static int pb_softing_ioctl(struct pb_dev *, int, va_list);

static int pb_softing_set_bus_parameter(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_softing_livelist(struct pb_dev *, struct pb_livelist_info *);

static int pb_softing_connect(struct pb_dev *, unsigned char,
                              unsigned char, unsigned short, pb_slave_t *);

static int pb_softing_disconnect(struct pb_dev *, pb_slave_t);

static int pb_softing_read(struct pb_dev *, pb_slave_t, struct pb_request *);

static int pb_softing_write(struct pb_dev *, pb_slave_t, struct pb_request *);


/*  ----------------------------------------------------------------------------
    Callback interface function pointers from iFak to Softing stack
*/
struct pb_funcs pb_softing_funcs =
{
    NULL, /* pb_softing_probe, */
    pb_softing_open,
    pb_softing_close,
    pb_softing_ioctl,
    pb_softing_set_bus_parameter,
    NULL, /* pb_softing_get_input, */
    NULL, /* pb_softing_set_output, */
    NULL, /* pb_softing_set_mode, */
    NULL, /* pb_softing_get_mode, */
    NULL, /* pb_softing_set_slave_param, */
    NULL, /* pb_softing_change_slave_addr, */
    pb_softing_livelist,
    pb_softing_connect,
    pb_softing_disconnect,
    pb_softing_read,
    pb_softing_write,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

/*  ----------------------------------------------------------------------------
    This function used to start the communication
*/
static int pb_softing_open(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
    struct pbSoftingData *data;

    if (!dev)
        return PB_INVALID_DEVICE;

    if (!par)
        return PB_NULL_POINTER;

    data = malloc(sizeof(*data));

    if (!data)
        return PB_OUTOFMEMORY;

    memset(data, DEFAULT_INITIAL, sizeof(*data));

    data->handle = condition_create();
    if (!data->handle)
        return PB_OUTOFMEMORY;

    memcpy(&(data->busParam), par, sizeof (struct pb_bus_parameter));

    dev->data = data;

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to close the communication
*/
static int pb_softing_close(struct pb_dev *dev)
{
    if (!dev)
        return PB_INVALID_DEVICE;

    // Erase the configuration
    struct pbSoftingData *data = dev->data;

    condition_destroy(data->handle);
    free(data);

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to initialize the communication stack
*/
static int pb_softing_ioctl(struct pb_dev *dev, int request, va_list args)
{
    struct pbSoftingData* pb_data;
    pb_data = (struct pbSoftingData *) dev->data;

    switch(request)
    {
        case START_PBSERVICE_REQ:
        {
            // Create profibus stack processing thread
            createPbStackThread(pb_data);

            // Send request for stack initialization
            acquireRequestLock();
            g_comm_req.requestType = INITIALIZE_STACK;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            if (g_comm_req.responseVal != SUCCESS)
            {
                LOGC_ERROR(CPM_PB_STACK,"Stack not initialized\n");
                return g_comm_req.responseVal;
            }

            // Send request for FMB_SET_CONFIGURATION
            acquireRequestLock();
            g_comm_req.requestType = FMB_SET_CONFIG;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            if (g_comm_req.responseVal != SUCCESS)
            {
                LOGC_ERROR(CPM_PB_STACK,"Failed to set FMB configuration data\n");
                return g_comm_req.responseVal;
            }

            // Send request for FMB_BUS_PARAMETER
            acquireRequestLock();
            g_comm_req.requestType = FMB_SET_BUSPARAM;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            if (g_comm_req.responseVal != SUCCESS)
            {
                LOGC_ERROR(CPM_PB_STACK,"Failed to set FMB bus parameter\n");
                return g_comm_req.responseVal;
            }

            // Send request for DP_INIT_MASTER
            acquireRequestLock();
            g_comm_req.requestType = DP_INIT;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            if (g_comm_req.responseVal != SUCCESS)
            {
                LOGC_ERROR(CPM_PB_STACK,"Failed to init DP master\n");
                return g_comm_req.responseVal;
            }

            // Send request for DP_ACT_LOC_PARAM
            acquireRequestLock();
            g_comm_req.requestType = DP_ACT_LOCAL_PARAM;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            if (g_comm_req.responseVal != SUCCESS)
            {
                LOGC_ERROR(CPM_PB_STACK,"Failed to get response for DP local param\n");
                return g_comm_req.responseVal;
            }

            return g_comm_req.responseVal;
        }
        break;

        case STOP_PBSERVICE_REQ:
        {
            // Shutdown the stack
            acquireRequestLock();
            g_comm_req.requestType = SHUTDOWN_STACK;
            g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
            releaseRequestLock();

            // Wait for response
            condition_lock(pb_data->handle);
            condition_wait(pb_data->handle);
            condition_unlock(pb_data->handle);

            return g_comm_req.responseVal;
        }
        break;

        default:
            break;
    }

    return PB_UNSPECIFIC_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to set the Bus Parameters
*/
static int pb_softing_set_bus_parameter(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
    return PB_INVALID_DEVICE;
}

/*  ----------------------------------------------------------------------------
    This function used to retreive the device live list
*/
static int pb_softing_livelist(struct pb_dev *dev, struct pb_livelist_info *livelist)
{
    T_FMB_GET_LIVE_LIST_CNF* live_list_cnf_data = NULL;
    T_FMB_LIVE_LIST* live_list_elem = NULL;

    struct pbSoftingData* pb_data = dev->data;

    // Send request for DP_ACT_LOC_PARAM
    acquireRequestLock();
    g_comm_req.requestType = GET_LIVE_LIST;
    g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
    releaseRequestLock();

    // Wait for response
    condition_lock(pb_data->handle);
    condition_wait(pb_data->handle);
    condition_unlock(pb_data->handle);

    if (g_comm_req.responseVal != SUCCESS)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to get live list data\n");
        return g_comm_req.responseVal;
    }

    live_list_cnf_data = (T_FMB_GET_LIVE_LIST_CNF *) g_response_data;
    live_list_elem = (T_FMB_LIVE_LIST*) (live_list_cnf_data + DEFAULT_PARAMS);

    livelist->length = live_list_cnf_data->no_of_elements;
    for (int index = DEFAULT_INITIAL; index < live_list_cnf_data->no_of_elements; index++)
    {
        livelist->list[index].addr = live_list_elem->station;
        livelist->list[index].state = live_list_elem->status;
        live_list_elem++;
    }

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function is used to connect device
*/
static int pb_softing_connect(struct pb_dev *dev,
        unsigned char palinkaddr, unsigned char slaveaddr,
        unsigned short sendTimeOut, pb_slave_t *slave)
{
    struct pbSoftingData* pb_data = dev->data;

    // Send request for DP_CONNECT
    acquireRequestLock();
    g_comm_req.requestType = CONNECT_DEVICE;
    g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
    g_comm_req.pbSlaveReq.paLinkAddr = palinkaddr;
    g_comm_req.pbSlaveReq.slaveAddr = slaveaddr;
    g_comm_req.pbSlaveReq.sendTimeout = sendTimeOut;
    g_comm_req.pbSlaveReq.slave = slave;// TODO: Need to understand the usage of this parameter
    releaseRequestLock();

    // Wait for response
    condition_lock(pb_data->handle);
    condition_wait(pb_data->handle);
    condition_unlock(pb_data->handle);

    if (g_comm_req.responseVal != SUCCESS)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to connect to the device");
        return g_comm_req.responseVal;
    }

    T_DP_INITIATE_CON* pInitCon = (T_DP_INITIATE_CON *) g_response_data;
    dpSetMaxLength(pInitCon->max_len_data_unit);

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to disconnect currently connected device
*/
static int pb_softing_disconnect(struct pb_dev *dev, pb_slave_t slave)
{
    struct pbSoftingData* pb_data = dev->data;

    // Send request for DP_ABORT
    acquireRequestLock();
    g_comm_req.requestType = DISCONNECT_DEVICE;
    g_comm_req.requestState = PB_REQUEST_IN_PROGRESS;
    g_comm_req.pbSlaveReq.slave = slave;// TODO: Need to understand the usage of this parameter
    releaseRequestLock();

    // Wait for response
    condition_lock(pb_data->handle);
    condition_wait(pb_data->handle);
    condition_unlock(pb_data->handle);

    if (g_comm_req.responseVal != PB_ABORT_INDICATION)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to disconnect from the device\n");
        return g_comm_req.responseVal;
    }

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to read the data from the device
*/
static int pb_softing_read(struct pb_dev *dev, pb_slave_t slave,
        struct pb_request *pb_req)
{
    struct pbSoftingData* pb_data = dev->data;

    // Send request for DP_ABORT
    acquireRequestLock();

    g_comm_req.requestType              = DATA_READ;
    g_comm_req.requestState             = PB_REQUEST_IN_PROGRESS;
    g_comm_req.pbSlaveReq.pbReq.slot    = pb_req->slot;
    g_comm_req.pbSlaveReq.pbReq.index   = pb_req->index;
    g_comm_req.pbSlaveReq.slave         = slave; // TODO: Need to understand the usage of this parameter

    releaseRequestLock();

    // Wait for response
    condition_lock(pb_data->handle);
    condition_wait(pb_data->handle);
    condition_unlock(pb_data->handle);

    if (g_comm_req.responseVal != SUCCESS)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to read the data from the device");
        return g_comm_req.responseVal;
    }
    
    // Copy the read data from network
    T_DP_READ_CON* pb_read_data = (T_DP_READ_CON *) g_response_data;
    pb_req->datalen = pb_read_data->length;
    memcpy(pb_req->data, (unsigned char*) (pb_read_data + DEFAULT_PARAMS), pb_read_data->length);

    return PB_NO_ERROR;
}

/*  ----------------------------------------------------------------------------
    This function used to write the data to the device
*/
static int pb_softing_write(struct pb_dev *dev, pb_slave_t slave,
        struct pb_request *pb_req)
{
    struct pbSoftingData* pb_data = dev->data;

    // Send request for DP_ABORT
    acquireRequestLock();

    g_comm_req.requestType              = DATA_WRITE;
    g_comm_req.requestState             = PB_REQUEST_IN_PROGRESS;
    g_comm_req.pbSlaveReq.pbReq.slot    = pb_req->slot;
    g_comm_req.pbSlaveReq.pbReq.index   = pb_req->index;

    memcpy(g_comm_req.pbSlaveReq.pbReq.data, pb_req->data, pb_req->datalen);

    g_comm_req.pbSlaveReq.pbReq.datalen = pb_req->datalen;
    g_comm_req.pbSlaveReq.slave         = slave;// TODO: Need to understand the usage of this parameter

    releaseRequestLock();

    // Wait for response
    condition_lock(pb_data->handle);
    condition_wait(pb_data->handle);
    condition_unlock(pb_data->handle);

    if (g_comm_req.responseVal != SUCCESS)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to write the data to the device");
        return g_comm_req.responseVal;
    }

    return PB_NO_ERROR;
}
