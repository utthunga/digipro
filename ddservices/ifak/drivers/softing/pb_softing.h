/***********************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    git clone git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            
***********************************************************************/

#ifndef _pb_softing_h
#define _pb_softing_h

#define SUCCESS 0
#define FAILURE -1

#include "driver_interface.h"

extern struct pb_funcs pb_softing_funcs;

#endif /* _pb_softing_h */
