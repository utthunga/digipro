/*********************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    git clone git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - FLUKE IDC
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#include "pb_softing_service.h"
#include "linux_win32_funcs.h"

int maxLength = DEFAULT_INITIAL;
pthread_mutex_t gRequestMutex = PTHREAD_MUTEX_INITIALIZER;

/*  ----------------------------------------------------------------------------
    This function acquire the request lock
*/
void acquireRequestLock()
{
    pthread_mutex_lock(&gRequestMutex);
}

/*  ----------------------------------------------------------------------------
    This function release the request lock
*/
void releaseRequestLock()
{
    pthread_mutex_unlock(&gRequestMutex);
}

/*  ----------------------------------------------------------------------------
    This function sets the maximum data length
*/
void dpSetMaxLength(int maxDataLen)
{
    maxLength = maxDataLen;
    LOGC_TRACE(CPM_PB_STACK, "Maximum length for DATA_READ and DATA_WRITE data exchange: %d", maxDataLen);
}

/*  ----------------------------------------------------------------------------
    This function used to send the request data to get the particular response
    from the stack
*/
static short int stackSndReqRes(unsigned int ChannelId, void* pSdb, void* pData)
{
    int retVal = fbapi_snd_req_res(ChannelId, pSdb, pData);

    if (retVal != E_OK)
    {
        LOGC_ERROR(CPM_PB_STACK,"Failed to send stack request/response data\n");
    }

    return retVal;
}

/*  ----------------------------------------------------------------------------
    This function handler DP_USR messages
*/
static short int handleDPUSRMessages
(
    T_PROFI_SERVICE_DESCR* pbSerDesc,
    char* cnfIndBuffer,
    int bufferLen
)
{
    int serviceStatus = PB_NO_ERROR;

    switch (pbSerDesc->service)
    {
        case DP_INIT_MASTER:
        {
            if (pbSerDesc->result != POS)
            {
                serviceStatus = PB_SOFTING_INIT_ERR;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Failed to initialize DP master, status: %d", pbSerDesc->result);
            }
        }
        break;

        case DP_ACT_PARAM_LOC:
        {
            if (pbSerDesc->result != POS)
            {
                serviceStatus = PB_SOFTING_ACT_ERR;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Failed to set DP master state to stop, status: %d", pbSerDesc->result);
            }
        }
        break;

        case DP_INITIATE:
        {
            memset(g_response_data, '\0', RCV_BUFFER_LEN);
            memcpy(g_response_data, cnfIndBuffer, bufferLen);

            if (pbSerDesc->result != POS)
            {       
                serviceStatus = PB_SOFTING_CONN_ERR;    
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
                T_DP_INITIATE_CON* pInitCon = (T_DP_INITIATE_CON *) cnfIndBuffer;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Failed to establish connection to the device, status: %d", pInitCon->status);
            }
        }
        break;

        case DP_ABORT:
        {
            serviceStatus = PB_ABORT_INDICATION;

            T_DP_ABORT_IND* pAbortInd = (T_DP_ABORT_IND *) cnfIndBuffer;

            LOGC_TRACE(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
            LOGC_TRACE(CPM_PB_STACK, "CON_IND: ABORT indication"); 

            if (pAbortInd->locally_generated)
            {
                LOGC_TRACE(CPM_PB_STACK, "CON_IND: Locally generated");
            }
            else
            {
                LOGC_TRACE(CPM_PB_STACK, "CON_IND: Remotely generated");
            }
                
            LOGC_TRACE(CPM_PB_STACK, "CON_IND: Subnet: %d", pAbortInd->subnet);
            LOGC_TRACE(CPM_PB_STACK, "CON_IND: Reason: %d", pAbortInd->reason);
        }
        break;

        case DP_READ:
        {
            memset(g_response_data, '\0', RCV_BUFFER_LEN);
            memcpy(g_response_data, cnfIndBuffer, bufferLen);

            if (pbSerDesc->result != POS)
            {
                serviceStatus = PB_SOFTING_READ_ERR;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
                T_DP_READ_CON* pb_read_data = (T_DP_READ_CON *) cnfIndBuffer;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Failed to read value from device, status: %d", pb_read_data->status);
            }
        }
        break;

        case DP_WRITE:
        {
            memset(g_response_data, '\0', RCV_BUFFER_LEN);
            memcpy(g_response_data, cnfIndBuffer, bufferLen);

            if (pbSerDesc->result != POS)
            {
                serviceStatus = PB_SOFTING_WRITE_ERR;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
                T_DP_WRITE_CON* pb_write_data = (T_DP_WRITE_CON *) cnfIndBuffer;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Failed to write value to device, status: %d", pb_write_data->status);
            }
        }
        break;

        default:
        {
            serviceStatus = PB_UNSPECIFIC_ERROR;
            LOGC_ERROR(CPM_PB_STACK, "CON_IND: DP_USR Layer"); 
            LOGC_ERROR(CPM_PB_STACK, "CON_IND: Unknown service: %d", pbSerDesc->service);
        }
        break;
    }

    return serviceStatus;
}

/*  ----------------------------------------------------------------------------
    This function handler DP_USR messages
*/
static short int handleFMBUSRMessages
(
    T_PROFI_SERVICE_DESCR* pbSerDesc,
    char* cnfIndBuffer,
    int bufferLen
)
{
    int serviceStatus = PB_NO_ERROR;

    switch (pbSerDesc->primitive)
    {
        case CON:
        {
            if (pbSerDesc->service == FMB_SET_CONFIGURATION)
            {
                if (pbSerDesc->result != POS)
                {
                    serviceStatus = PB_SOFTING_CONFIG_ERR;
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: FMB_USR Layer"); 
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: Set Configuration failed, status: %d", pbSerDesc->result); 
                }
            }
            else if (pbSerDesc->service == FMB_SET_BUSPARAMETER)
            {
                if (pbSerDesc->result != POS)
                {
                    serviceStatus = PB_SOFTING_BUS_PARAM_ERR;
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: FMB_USR Layer"); 
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: Set Bus Parameter failed, status: %d", pbSerDesc->result);
                }
            }
            else if (pbSerDesc->service == FMB_GET_LIVE_LIST)
            {
                if (pbSerDesc->result == POS)
                {
                    memset(g_response_data, '\0', RCV_BUFFER_LEN);
                    memcpy(g_response_data, cnfIndBuffer, bufferLen);

                    T_FMB_GET_LIVE_LIST_CNF* liveList = ((T_FMB_GET_LIVE_LIST_CNF *)cnfIndBuffer);
                    T_FMB_LIVE_LIST *pStationInfo;

                    LOGC_TRACE(CPM_PB_STACK, "CON_IND: Live list information, total stations: %d", liveList->no_of_elements);
                    pStationInfo = (T_FMB_LIVE_LIST*) (liveList + DEFAULT_PARAMS);
                    for (int i = 0; i < liveList->no_of_elements; i++)
                    {
                        LOGC_TRACE(CPM_PB_STACK, "CON_IND: Station: %d, Status: %d", pStationInfo->station, pStationInfo->status);
                        pStationInfo++;
                    }
                }
                else
                {
                    serviceStatus = PB_SOFTING_LIVE_LIST_ERR;
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: FMB_USR Layer"); 
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: Get device live list failed, status: %d", pbSerDesc->result);
                }
            }
            else
            {
                serviceStatus = PB_UNSPECIFIC_ERROR;
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: FMB_USR Layer"); 
                LOGC_ERROR(CPM_PB_STACK, "CON_IND: Invalid service: %d", pbSerDesc->service);
            }
        }
        break;

        case IND:
        {
            serviceStatus = PB_SOFTING_FMB_IND;
            LOGC_TRACE(CPM_PB_STACK, "CON_IND: FMB_USR Layer"); 
            LOGC_TRACE(CPM_PB_STACK, "CON_IND: Indication message");
            break;
        }
    }

    return serviceStatus;
}    

/*  ----------------------------------------------------------------------------
    This function waits for the response data from the stack
*/
static int waitForCnfInd(struct pbHostConfigData* pbHostConfig)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    char cnfIndBuffer[RCV_BUFFER_LEN];
    short int confIndStatus = PB_INVALID_HANDLE;
    short int serviceStatus = PB_INVALID_HANDLE;
    unsigned short bufferLen;

    while (PB_INVALID_HANDLE == serviceStatus)
    {
        bufferLen = sizeof(cnfIndBuffer);

        // Receive confirmation indication response from passive device
        confIndStatus = fbapi_rcv_con_ind(pbHostConfig->deviceChannel,
                                  (T_PROFI_SERVICE_DESCR*) &pbSerDesc,
                                  (VOID *) cnfIndBuffer,
                                  &bufferLen);

        switch (confIndStatus)
        {
            case NO_CON_IND_RECEIVED:
                {
                    // Sleep for sometime as passive device may take time to respond within slot time
                    usleep (CON_IND_TIMEOUT * 1000);
                }
                break;

            case CON_IND_RECEIVED:
                {
                    // Dispatch Data
                    switch (pbSerDesc.layer)
                    {
                        case DP_USR:
                        {
                            // Handle all DP_USR messages
                            serviceStatus = handleDPUSRMessages(&pbSerDesc, cnfIndBuffer, bufferLen);
                        }
                        break;

                        case FMB_USR:
                        {
                            // Handle all FMB_USR messages
                            serviceStatus = handleFMBUSRMessages(&pbSerDesc, cnfIndBuffer, bufferLen);

                            // TODO: (Santhosh Kumar Selvara)
                            // Need to check whether PB_SOFTING_FMB_IND can be continued
                            if (PB_SOFTING_FMB_IND == serviceStatus)
                            {
                                serviceStatus = PB_INVALID_HANDLE;
                                continue;
                            }
                        }
                        break;

                        default:
                        {    
                            serviceStatus = PB_UNSPECIFIC_ERROR;
                            LOGC_ERROR(CPM_PB_STACK, "CON_IND: Unknown Layer: %d", pbSerDesc.layer);
                        }
                        break;
                    }

                }
                break;

            default:
                {
                    serviceStatus = PB_UNSPECIFIC_ERROR;
                    LOGC_ERROR(CPM_PB_STACK, "CON_IND: Receive Error when waiting for CON/IND, status: %d", confIndStatus);
                }
                break;
        }
    }

    return serviceStatus;
}

/*  ----------------------------------------------------------------------------
    This function used to initialise communication
*/
static int initializeStack(struct pbHostConfigData* pbHostConfig)
{
    int retVal;
    unsigned short int addDetail;
    T_FBAPI_INIT fbApiInitParam;

    fbApiInitParam.serial.ComportName  = malloc(strlen(pbHostConfig->comPortName) + DEFAULT_PARAMS);
    memset(fbApiInitParam.serial.ComportName, '\0', strlen(pbHostConfig->comPortName) + DEFAULT_PARAMS);
    memcpy(fbApiInitParam.serial.ComportName, pbHostConfig->comPortName, strlen(pbHostConfig->comPortName));
    fbApiInitParam.AcknowledgeTimeout  = pbHostConfig->ackTimeout;
    fbApiInitParam.ReadTimeout         = pbHostConfig->readTimeout;

    retVal = fbapi_init(pbHostConfig->deviceChannel, PROTOCOL_PA, &fbApiInitParam);

    if (retVal != E_OK)
    {
        LOGC_ERROR(CPM_PB_STACK,"Could not initialize stack\n");
    }

    retVal = fbapi_start(pbHostConfig->deviceChannel, &addDetail);

    if (retVal != E_OK)
    {
        LOGC_ERROR(CPM_PB_STACK,"Could not start firmware\n");
    }
    
    return retVal;
}

/*  ----------------------------------------------------------------------------
    This function is used to close the PB stack communication
*/
static int shutdownStack(struct pbHostConfigData* pbHostConfig)
{
    unsigned short exitParam = TRIGGER_HW_RESET;

    return fbapi_exit(pbHostConfig->deviceChannel, exitParam);
}

/*  ----------------------------------------------------------------------------
    This function used to set the Configuration
*/
static short int fmbSetConfiguration(struct pbHostConfigData* pbHostConfig)
{
    T_FMB_SET_CONFIGURATION_REQ  fmbConfigReq;
    T_PROFI_SERVICE_DESCR        pbSerDesc;
    T_FMB_CONFIG_DP*             configDpPtr;

    // Do only activate DP, but no other module
    fmbConfigReq.fms_active         = PB_FALSE;
    fmbConfigReq.dp_active          = PB_TRUE;
    fmbConfigReq.fdlif_active       = PB_FALSE;
    fmbConfigReq.sm7_active         = PB_FALSE;
    fmbConfigReq.fdl_evt_receiver   = FMB_USR;
    fmbConfigReq.data_buffer_length = DEFAULT_BUFF_LEN;

    // Set DP-Parameter-Block
    configDpPtr = (T_FMB_CONFIG_DP*) &(fmbConfigReq.dp);
    configDpPtr->max_number_slaves      = MAX_NUM_SLAVES;
    configDpPtr->max_slave_output_len   = MAX_BUFF_LEN;
    configDpPtr->max_slave_input_len    = MAX_BUFF_LEN;
    configDpPtr->max_slave_diag_entries = MAX_SLAVE_ENTRY;
    configDpPtr->max_slave_diag_len     = MAX_BUFF_LEN;
    configDpPtr->max_bus_para_len       = MAX_BUS_PARAM_LEN;
    configDpPtr->max_slave_para_len     = MAX_BUS_PARAM_LEN;

    // Set Description block
    pbSerDesc.comm_ref  = DEFAULT_INITIAL;
    pbSerDesc.layer     = FMB;
    pbSerDesc.service   = FMB_SET_CONFIGURATION;
    pbSerDesc.primitive = REQ;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, &fmbConfigReq);
}

/*  ----------------------------------------------------------------------------
    This function used to set the Bus Parameters
*/
static short int fmbSetBusparameter(struct pbHostConfigData* pbHostConfig,
                                    struct pb_bus_parameter* busParams)
{
    T_FMB_SET_BUSPARAMETER_REQ  busParamReq;
    T_PROFI_SERVICE_DESCR       pbSerDesc;

    // set data block parameters
    busParamReq.loc_add         = busParams->FDL_Add;
    busParamReq.in_ring_desired = IN_RING_DESIRED;
    busParamReq.baud_rate       = KBAUD_31_25;
    busParamReq.loc_segm        = DEFAULT_BUFF_LEN;
    busParamReq.medium_red      = NO_REDUNDANCY;

    // Set the Bus parameters
    busParamReq.tsl             = busParams->Tsl;
    busParamReq.min_tsdr        = busParams->minTsdr;
    busParamReq.max_tsdr        = busParams->maxTsdr;
    busParamReq.tqui            = busParams->Tqui;
    busParamReq.tset            = busParams->Tset;
    busParamReq.ttr             = busParams->Ttr;
    busParamReq.g               = busParams->G;
    busParamReq.hsa             = busParams->HSA;
    busParamReq.max_retry_limit = busParams->max_retry_limit;

    // Set Description block
    pbSerDesc.comm_ref  = DEFAULT_INITIAL;
    pbSerDesc.layer     = FMB;
    pbSerDesc.service   = FMB_SET_BUSPARAMETER;
    pbSerDesc.primitive = REQ;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, &busParamReq);
}

/*  ----------------------------------------------------------------------------
    This function used to Initialize the Master
*/
static short int dpInitMaster(int masterDefaultAddr, struct pbHostConfigData* pbHostConfig)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    T_DP_INIT_MASTER_REQ  initMasterReq;

    initMasterReq.master_default_address = masterDefaultAddr;
    initMasterReq.master_class2          = PB_TRUE;
    initMasterReq.lowest_slave_address   = DEFAULT_INITIAL;
    initMasterReq.slave_io_address_mode  = DP_AAM_ARRAY;
    initMasterReq.clear_outputs          = PB_FALSE;
    initMasterReq.auto_remote_services   = DP_AUTO_REMOTE_SERVICES;
    initMasterReq.cyclic_data_transfer   = PB_FALSE;

    memset((VOID*)&initMasterReq.master_class2_name[DEFAULT_INITIAL], ' ', MAX_BUFF_LEN);

    // Set Description block
    pbSerDesc.comm_ref  = DEFAULT_INITIAL;
    pbSerDesc.layer     = DP;
    pbSerDesc.service   = DP_INIT_MASTER;
    pbSerDesc.primitive = REQ;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, &initMasterReq);
}

/*  ----------------------------------------------------------------------------
    This function used to activate the parameter location
*/
static short int dpActParamLocReq(struct pbHostConfigData* pbHostConfig)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    char                  actParamBuffer[DEFAULT_BUFF_LEN];
    T_DP_ACT_PARAM_REQ*   actParamLocReqPtr = (T_DP_ACT_PARAM_REQ*) &actParamBuffer[DEFAULT_INITIAL];

    actParamLocReqPtr->area_code = DP_AREA_SET_MODE;
    actParamLocReqPtr->activate  = DP_OP_MODE_STOP;

    // Set Description block
    pbSerDesc.comm_ref    = DEFAULT_INITIAL;
    pbSerDesc.layer       = DP;
    pbSerDesc.service     = DP_ACT_PARAM_LOC;
    pbSerDesc.primitive   = REQ;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, actParamLocReqPtr);
}

/*  ----------------------------------------------------------------------------
    This function used to retreive the device live list
*/
static short fmbGetLiveListReq(struct pbHostConfigData* pbHostConfig)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;

    // Set Description block
    pbSerDesc.comm_ref  = DEFAULT_INITIAL;
    pbSerDesc.layer     = FMB;
    pbSerDesc.service   = FMB_GET_LIVE_LIST;
    pbSerDesc.primitive = REQ;
    pbSerDesc.invoke_id = DEFAULT_INITIAL;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, NULL);
}

/*  ----------------------------------------------------------------------------
    This function is used to connect device
*/
static short dpInitiateReq(struct pbHostConfigData* pbHostConfig,
                           unsigned char remAdd, unsigned short sendTimeout)
{
    char initiateDataBuffer[DEFAULT_BUFF_LEN];

    T_PROFI_SERVICE_DESCR pbSerDesc;
    T_DP_INITIATE_REQ*  initiateReq = (T_DP_INITIATE_REQ*) &initiateDataBuffer[DEFAULT_INITIAL];

    // Zero all standard parameters, especially the reserved ones
    memset((VOID*)initiateReq, DEFAULT_INITIAL, sizeof(T_DP_INITIATE_REQ));

    // Set mandatory parameters
    initiateReq->rem_add               = remAdd;
    initiateReq->send_timeout          = sendTimeout;
    initiateReq->features_supported[DEFAULT_INITIAL] = DEFAULT_PARAMS;
    initiateReq->features_supported[DEFAULT_PARAMS]  = DEFAULT_INITIAL;
    initiateReq->profile_ident_number  = DEFAULT_INITIAL;

    // Set additional address parameters
    initiateReq->add_addr_param.d_type = DEFAULT_INITIAL;
    initiateReq->add_addr_param.d_len  = DEFAULT_MASTER;
    initiateReq->add_addr_param.s_type = DEFAULT_INITIAL;
    initiateReq->add_addr_param.s_len  = DEFAULT_MASTER;

    // Before using the macros, s_len MUST be set
    DP_INITIATE_S_ADDR(initiateReq)->api = DEFAULT_INITIAL;
    DP_INITIATE_D_ADDR(initiateReq)->api = DEFAULT_INITIAL;
    DP_INITIATE_S_ADDR(initiateReq)->scl = DEFAULT_INITIAL;
    DP_INITIATE_D_ADDR(initiateReq)->scl = DEFAULT_INITIAL;

    // Set Description block
    pbSerDesc.comm_ref   = DEFAULT_PARAMS;
    pbSerDesc.layer      = DP;
    pbSerDesc.service    = DP_INITIATE;
    pbSerDesc.primitive  = REQ;
    pbSerDesc.invoke_id  = DEFAULT_INITIAL;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, initiateReq);
}

/*  ----------------------------------------------------------------------------
    This function used to disconnect currently connected device
*/
static short dpAbortReq(struct pbHostConfigData* pbHostConfig)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    T_DP_ABORT_REQ        abortReq;

    // Zero all standard parameters
    memset((VOID*)&abortReq, DEFAULT_INITIAL, sizeof(T_DP_ABORT_REQ));

    // Set mandatory parameters
    abortReq.subnet     = DPM_ABORT_SUBNET_LOCAL;
    abortReq.reason     = DEFAULT_INITIAL;

    // Set Description block
    pbSerDesc.comm_ref   = DEFAULT_PARAMS;
    pbSerDesc.layer      = DP;
    pbSerDesc.service    = DP_ABORT;
    pbSerDesc.primitive  = REQ;
    pbSerDesc.invoke_id  = DEFAULT_INITIAL;

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, &abortReq);
}

/*  ----------------------------------------------------------------------------
    This function used to read the data from the device
*/
static short dpReadReq(struct pbHostConfigData* pbHostConfig,
                       unsigned char slotNumber, unsigned char index)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    T_DP_READ_REQ readReq;

    // Zero all standard parameters
    memset((VOID*)&readReq, DEFAULT_INITIAL, sizeof(T_DP_READ_REQ));

    // Set mandatory parameters
    readReq.rem_add     = DEFAULT_INITIAL;
    readReq.slot_number = slotNumber;
    readReq.index       = index;
    readReq.length      = (USIGN8)maxLength;

    // Set Description block
    pbSerDesc.comm_ref   = DEFAULT_PARAMS;
    pbSerDesc.layer      = DP;
    pbSerDesc.service    = DP_READ;
    pbSerDesc.primitive  = REQ;
    pbSerDesc.invoke_id  = DEFAULT_INITIAL;

    LOGC_TRACE(CPM_PB_STACK, "READ_REQUEST: Slot: %d, Index: %d, Length: %d", slotNumber, index, maxLength);

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, &readReq);
}

/*  ----------------------------------------------------------------------------
    This function used to write the data to the device
*/
static signed short dpWriteReq(struct pbHostConfigData* pbHostConfig,
                               unsigned char slotNumber,
                               unsigned char index, unsigned char* data,
                               unsigned char dataLen)
{
    T_PROFI_SERVICE_DESCR pbSerDesc;
    char writeBuffer[MAX_WRITE_BUFF_LEN];
    T_DP_WRITE_REQ* writeReqPtr = (T_DP_WRITE_REQ*) &writeBuffer[DEFAULT_INITIAL];

    // Zero all standard parameters
    memset((VOID*)writeReqPtr, DEFAULT_INITIAL,sizeof(T_DP_WRITE_REQ));

    // Set mandatory parameters
    writeReqPtr->rem_add     = DEFAULT_INITIAL;
    writeReqPtr->slot_number = slotNumber;
    writeReqPtr->index       = index;
    writeReqPtr->length      = dataLen;

    // Set the Data to write into the slave object
    memcpy((VOID*)(writeReqPtr + DEFAULT_PARAMS), data, dataLen);

    // Set Description block
    pbSerDesc.comm_ref   = DEFAULT_PARAMS;
    pbSerDesc.layer      = DP;
    pbSerDesc.service    = DP_WRITE;
    pbSerDesc.primitive  = REQ;
    pbSerDesc.invoke_id  = DEFAULT_INITIAL;

    LOGC_TRACE(CPM_PB_STACK, "WRITE_REQUEST: Slot: %d, Index: %d, Length: %d", slotNumber, index, dataLen);

    // Send Request
    return stackSndReqRes(pbHostConfig->deviceChannel, &pbSerDesc, writeReqPtr);
}

/*  ----------------------------------------------------------------------------
    This function handles the data to be performed the particular action
*/
static void handlePbData(void *args)
{
    struct pbSoftingData* pb_data = (struct pbSoftingData*) args;
    bool process_request = true;

    pthread_mutex_init(&gRequestMutex, NULL);

    while (process_request)
    {
        acquireRequestLock();

        if (g_comm_req.requestState == PB_REQUEST_IN_PROGRESS)
        {
            switch (g_comm_req.requestType)
            {
                case INITIALIZE_STACK:
                {
                    g_comm_req.responseVal = initializeStack(&(m_pbHostConfig));
                    condition_notify (pb_data->handle);
                }
                break;

                case FMB_SET_CONFIG:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == fmbSetConfiguration(&(m_pbHostConfig)))
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));

                    condition_notify (pb_data->handle);
                }
                break;

                case FMB_SET_BUSPARAM:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == fmbSetBusparameter(&(m_pbHostConfig), &(pb_data->busParam)))
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));

                    condition_notify (pb_data->handle);
                }
                break;

                case DP_INIT:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpInitMaster(pb_data->masterDefaultAddr, &(m_pbHostConfig)))
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));

                    condition_notify (pb_data->handle);
                }
                break;

                case DP_ACT_LOCAL_PARAM:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpActParamLocReq(&(m_pbHostConfig)))
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));

                    condition_notify (pb_data->handle);
                }
                break;

                case SHUTDOWN_STACK:
                {
                    g_comm_req.responseVal = shutdownStack(&(m_pbHostConfig));
                    process_request = false;
                    condition_notify (pb_data->handle);
                }
                break;

                case GET_LIVE_LIST:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == fmbGetLiveListReq(&(m_pbHostConfig)))
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));

                    condition_notify (pb_data->handle);
                }
                break;

                case CONNECT_DEVICE:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpInitiateReq((&(m_pbHostConfig)),
                                             g_comm_req.pbSlaveReq.slaveAddr,
                                             g_comm_req.pbSlaveReq.sendTimeout))
                    {
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));
                    }

                    condition_notify (pb_data->handle);
                }
                break;

                case DISCONNECT_DEVICE:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpAbortReq((&(m_pbHostConfig))))
                    {
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));
                    }

                    condition_notify (pb_data->handle);
                }
                break;

                case DATA_READ:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpReadReq((&(m_pbHostConfig)),
                                         g_comm_req.pbSlaveReq.pbReq.slot,
                                         g_comm_req.pbSlaveReq.pbReq.index))
                    {
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));
                    }

                    condition_notify (pb_data->handle);
                }
                break;

                case DATA_WRITE:
                {
                    g_comm_req.responseVal = PB_UNSPECIFIC_ERROR;

                    if (E_OK == dpWriteReq((&(m_pbHostConfig)),
                                          g_comm_req.pbSlaveReq.pbReq.slot,
                                          g_comm_req.pbSlaveReq.pbReq.index,
                                          g_comm_req.pbSlaveReq.pbReq.data,
                                          g_comm_req.pbSlaveReq.pbReq.datalen))
                    {
                        g_comm_req.responseVal = waitForCnfInd(&(m_pbHostConfig));
                    }

                    condition_notify (pb_data->handle);
                }
                break;

                default:
                {
                    LOGC_ERROR(CPM_PB_STACK,"Invalid request\n");
                }
                break;
            }

            g_comm_req.requestState = PB_REQUEST_OK;
        }

        releaseRequestLock();
    }
}

/*  ----------------------------------------------------------------------------
    Create the ProfiBus stack thread
*/
int createPbStackThread(struct pbSoftingData* pbData)
{
    pthread_t handle;
    pthread_attr_init(&threadAttr);
    pthread_create(&handle, &threadAttr, &handlePbData, (void *)pbData);
}
