/***********************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    git clone git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - FLUKE IDC
***********************************************************************/

#ifndef _pb_softing_data_h
#define _pb_softing_data_h

#include "condition.h"
#include "profibus.h"
#include "driver_interface.h"
#include <stdarg.h>

#define PB_COMM_REF                 0x0000
#define PB_REQUEST_IN_PROGRESS      0x0001
#define PB_REQUEST_OK               0x0002

#define MASTER_MAX_LEN   32
#define COMPORT_MAX_LEN  64

// Service request to the PB layer to initialize and shuutdown stack
enum PBSERVICE_REQ
{
    START_PBSERVICE_REQ = 0,
    STOP_PBSERVICE_REQ,
};

// Request that can sent to softing layer
enum PBSTACK_REQ
{
    INITIALIZE_STACK = 0,
    FMB_SET_CONFIG,
    FMB_SET_BUSPARAM,
    DP_INIT,
    DP_ACT_LOCAL_PARAM,
    GET_LIVE_LIST,
    CONNECT_DEVICE,
    DATA_READ,
    DATA_WRITE,
    DISCONNECT_DEVICE,
    SHUTDOWN_STACK
};

// Holds host configuration data
struct pbHostConfigData
{
    char comPortName[COMPORT_MAX_LEN];  /* Contains the port that softing stack uses */
    char masterName[MASTER_MAX_LEN];    /* Name of the master that should be available in the network */
    unsigned int ackTimeout;
    unsigned int readTimeout;
    int deviceChannel;
};

// Holds softing data information to sync between user and softing thread
struct pbSoftingData
{
    int masterDefaultAddr;
    struct condition* handle;
    struct pb_bus_parameter busParam;
    struct pbHostConfigData hostConfig;
};

// Holds slave request information
struct pbCommSlaveReq
{
    unsigned char paLinkAddr;
    unsigned char slaveAddr;
    unsigned short sendTimeout;
    pb_slave_t *slave;
    struct pb_request pbReq;
};

// Holds FMB, DP request information from application layer
struct pbCommReq
{
    int requestType;
    int requestState;
    int responseVal;
    struct pbCommSlaveReq pbSlaveReq;
};

struct pbHostConfigData m_pbHostConfig;

#endif
