
#ifndef __ISMULTIDEF_H__
#define __ISMULTIDEF_H__


#ifndef PORTED_LINUX
#include <windows.h>
#else
#define __cdecl
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long ULONG;
typedef unsigned long DWORD;
typedef BYTE BOOLEAN;
#endif

/********************************/
/* alle unterst�tzen Protokolle */
/* all supported protocols      */
/********************************/

#define SUPPORTED_DPEM
#define SUPPORTED_FMS
//#define SUPPORTED_LIST
#define SUPPORTED_DPS
//#define SUPPORTED_PB2M


/******************************/
/* ... und ihre Protokoll-IDs */
/******************************/

#define NO_PROTOCOL     0  /* kein Protokoll/no protocol                              */
#define DPS_PROTOCOL    1  /* DP-Slave                                                */
#define DPM1_PROTOCOL   2  /* DP-Master (Klasse 1)                                    */
#define DPM2_PROTOCOL   3  /* DP-Master (Klasse 2)                                    */
#define L7S_PROTOCOL    4  /* FMS-Slave                                               */
#define L7M_PROTOCOL    5  /* FMS-Master                                              */
#define L7DPS_PROTOCOL  6  /* Mischprotokoll FMS-Slave  + DP-Slave                    */
#define L7DPM1_PROTOCOL 7  /* Mischprotokoll FMS-Master + DP-Master (Klasse 1)        */
#define L7DPM2_PROTOCOL 8  /* Mischprotokoll FMS-Master + DP-Master (Klasse 2)        */
#define ILS_PROTOCOL    9  /* FMS-Slave  mit Interface-Layer (IL)                     */
#define ILM_PROTOCOL    10 /* FMS-Master mit Interface-Layer (IL)                     */
#define ILDPS_PROTOCOL  11 /* Mischprotokoll FMS-Slave  mit IL + DP-Slave             */
#define ILDPM1_PROTOCOL 12 /* Mischprotokoll FMS-Master mit IL + DP-Master (Klasse 1) */
#define ILDPM2_PROTOCOL 13 /* Mischprotokoll FMS-Master mit IL + DP-Master (Klasse 2) */
#define PA7_PROTOCOL    14 /* PA7-Master                                              */
#define PB2M_PROTOCOL   15 /* Schicht 2 Zugang (Master)                               */
#define PB2S_PROTOCOL   16 /* Schicht 2 Zugang (Slave)                                */
#define DPV1M_PROTOCOL  17 /* DPV1 Master                                             */
#define LIST_PROTOCOL   18 /* DP-Listener                                             */
#define DPEM_PROTOCOL   19 /* DPE-Master                                             */

/*******************************/
/* Definitionen der ErrorCodes */
/* definitions of error codes. */
/*******************************/

/* Diese Codes werden von InitDll zur�ckgegeben.    */
/* return values returned by a call to InitDll.     */
#define DLL_NO_ERROR                    0
#define DLL_OS_ERROR                    1
#define DLL_INIT_ERROR                  2
#define DLL_DEVICE_ERROR                3

/* Diese Codes sind in DeviceError_type zu finden.  */
/* Sie sollten ausgewertet werden, wenn InitDll     */
/* DLL_DEVICE_ERROR zur�ckgibt                      */
#define DEVICE_NO_ERROR                 0
#define DEVICE_OPEN_ERROR               1
#define DEVICE_DRIVER_ERROR             2
#define DEVICE_FIRMWARE_INIT_ERROR      3
#define DEVICE_PROTOCOL_INIT_ERROR      4
#define DEVICE_NO_SUPPORTED_PROTOCOL    5
#define DEVICE_INVALID_DPR_PARAMETER    6	// nur unter Win9x
#define DEVICE_OVERLAPPED_DPR_PARAMETER 7	// nur unter Win9x
#define DEVICE_CHANNEL_ERROR            8
#define DEVICE_ERROR_ID_NOT_FOUND       9	// f�r USB- u. PCI-DeviceID
#define DEVICE_ALREADY_IN_USE          10   // Ger�t schon in Benutzung
#define DEVICE_INVALID_PARAMETER       11

/* Diese Codes sind in ChannelError_type zu finden. */
/* Sie sollten ausgewertet werden, wenn in das ent- */
/* sprechende Device DEVICE_CHANNEL_ERROR anzeigt.  */
#define CHANNEL_NO_ERROR                0
#define CHANNEL_PROTOCOL_NOT_SUPPORTED  1
#define CHANNEL_PROTOCOL_INIT_ERROR     2

/* Diese Codes werden von WatchMailbox zur�ckgegeben */
/* return values returned by a call to WatchMailbox. */
#define WATCH_NO_ERROR                  0
#define WATCH_INVALID_DEVICE_NUMBER     1
#define WATCH_INVALID_CHANNEL_NUMBER    2
#define WATCH_CHANNEL_NOT_INITIALIZED   3
#define WATCH_THREAD_ALREADY_STARTED    4
#define WATCH_THREAD_NOT_STARTED        5

/* Diese Codes werden von den Modulen bei */
/* Aufruf ihrer Funktionen zur�ckgegeben  */
#define MODUL_NO_ERROR                  0
#define MODUL_INVALID_DEVICE_NUMBER     1
#define MODUL_INVALID_CHANNEL_NUMBER    2
#define MODUL_CHANNEL_NOT_INITIALIZED   3
#define MODUL_WRONG_PROTOCOL            4
#define MODUL_DRIVER_ERROR              5
#define MODUL_COMMANDBOX_NOT_FREE       6
#define MODUL_REPORTBOX_NO_DATA         7
#define MODUL_UNKNOWN_FUNCTION          8
#define MODUL_RESOURCE_RANGE_VIOLATED   9

/* verschiedene Ger�tetypen / different device types */
#define DEVTYPE_NO_DEV  0
#define DEVTYPE_ISA     1
#define DEVTYPE_PCI     2
#define DEVTYPE_USB     3
#define DEVTYPE_PCMCIA  4
#define DEVTYPE_USB12   5
#define DEVTYPE_PCI12   6
#define DEVTYPE_NETCUBE 7
#define DEVTYPE_MODULE  0x81

/* Flags und Konstanten, die f�r isPRO_AttachedDevices ben�tigt werden */
#define FIND_UNCONFIGURED 0x20
#define FIND_CONFIGURED   0x40
#define FIND_ALL_DEVICES  0x60
#define FIND_NEXT         0x80

/****************/
/*   typedefs   */
/****************/

typedef signed char     Int8;
typedef signed short    Int16;
typedef signed long     Int32;
typedef unsigned char   UInt8;
typedef unsigned short  UInt16;
typedef unsigned long   UInt32;
typedef float           Float32;
typedef double          Float64;

typedef UInt8           UNSIGNED8;
typedef UInt16          UNSIGNED16;
typedef UInt32          UNSIGNED32;


#ifndef PORTED_LINUX
#pragma pack(push, 1)
#endif
typedef struct
{
	BYTE DeviceType;     /* no device = 0, USB = 1, PCI = 2, PCMCIA = 3 ... */
	DWORD DeviceID;
	BYTE bAttached;      /* 0: not attached, 1: attached, 2: uncertain */
	BYTE bInUse;         /* 0: available, 1: in use, 2: uncertain */
} DeviceInfo_type;
#ifndef PORTED_LINUX
#pragma pack(pop)
#endif

/*************************/
/* sonstige Definitionen */
/*************************/

#define MAX_SIZE_OF_INIT_DATA    2
#define NUMBER_OF_PHYS_CHAN      4
#define MAX_NR_OF_CARDS         16

#define DPR_EXTENSION_1



#endif //__ISMULTIDEF_H__
