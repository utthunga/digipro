
/* 
 * $Id: pb_ispro.h,v 1.3 2009/06/19 08:42:00 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_ispro_h
#define _pb_ispro_h

#include "driver_interface.h"

extern struct pb_funcs pb_ispro_funcs;

#endif /* _pb_ispro_h */
