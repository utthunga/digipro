
#ifndef __DPEM_H__
#define __DPEM_H__


#include "dpemdef.h"

typedef UNSIGNED16 dpInOutAreaSize_type;

typedef struct
{
	BYTE SlaveAdd;
	WORD OffsetOutputData;
	BYTE LengthOutputData;
	BYTE* OutputData;
	WORD OffsetInputData;
	BYTE LengthInputData;
	BYTE* InputData;
} SlaveInfo_struct, *pSlaveInfo;


/***********************************/
/*       F U N C T I O N E N       */
/***********************************/

#ifndef PORTED_LINUX
#pragma pack(push, 8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Verwaltung des Befehlsbereiches */
BYTE __cdecl DPEM_WriteCommand(
					BYTE  device,
					BYTE  channel,
					DPEMDEF_Box_type CommandBox
					);
BYTE __cdecl DPEM_GetReport(
					BYTE  device,
					BYTE  channel,
					DPEMDEF_Box_type ReportBox
					);


/* Verwaltung des Ein- und Ausgabebereiches  */
BYTE __cdecl DPEM_WriteOutputData(
					BYTE       device,
					BYTE       channel,
					BYTE       *src_ptr,
					UNSIGNED16 offset,
					BYTE       len,
					BYTE       slaveAdd
					);
BYTE __cdecl DPEM_ReadInputData(
					BYTE       device,
					BYTE       channel,
					BYTE       *dest_ptr,
					UNSIGNED16 offset,
					BYTE       len,
					BYTE       slaveAdd
					);
BYTE __cdecl DPEM_DataExchange(
					BYTE       device,
					BYTE       channel,
					pSlaveInfo* SlaveArray
					);


/* Verwaltung des Statusbereiches  */
BYTE __cdecl DPEM_WriteRemoteAccessFlag(
					BYTE  device,
					BYTE  channel,
					BYTE  remAccessFlag
					);
BYTE __cdecl DPEM_ReadDataTransferList(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr
					);
BYTE __cdecl DPEM_ReadSystemDiagnostic(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr
					);
BYTE __cdecl DPEM_ReadDiagState(
					BYTE  device,
					BYTE  channel,
					BOOLEAN* diagstate
					);


/* Stationszustand */
BYTE __cdecl DPEM_StationOnline(
					BYTE  device,
					BYTE  channel,
					BOOLEAN* online
					);

#ifndef PORTED_LINUX
/* die gleichen Funktionen nochmal mit einer anderen */
/* Aufrufkonvention zum Aufruf von Visual Basic aus. */
BYTE __stdcall DPEMvb_WriteCommand(BYTE device, BYTE channel, BYTE* boxPtr);
BYTE __stdcall DPEMvb_GetReport(BYTE device, BYTE channel, BYTE* boxPtr);
BYTE __stdcall DPEMvb_WriteOutputData(BYTE device, BYTE channel, BYTE *src_ptr,
									  UNSIGNED16 offset, BYTE len, BYTE slaveAdd);
BYTE __stdcall DPEMvb_ReadInputData(BYTE device, BYTE channel, BYTE *dest_ptr,
									UNSIGNED16 offset, BYTE len, BYTE slaveAdd);
BYTE __stdcall DPEMvb_WriteRemoteAccessFlag(BYTE device, BYTE channel, BYTE remAccessFlag);
BYTE __stdcall DPEMvb_ReadDataTransferList(BYTE device, BYTE channel, BYTE* dest_ptr);
BYTE __stdcall DPEMvb_ReadSystemDiagnostic(BYTE device, BYTE channel, BYTE* dest_ptr);
BYTE __stdcall DPEMvb_ReadDiagState(BYTE device, BYTE channel, BOOLEAN* diagstate);
BYTE __stdcall DPEMvb_StationOnline(BYTE device, BYTE channel, BOOLEAN* online);
#endif

#ifdef __cplusplus
}
#endif

#ifndef PORTED_LINUX
#pragma pack(pop)
#endif

#endif // __DPEM_H__
