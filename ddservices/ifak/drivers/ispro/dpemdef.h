/****************************************************************************/
/****************************************************************************/
/**                                                                        **/
/**  Dateiname: dpemdef.h                                                  **/
/**                                                                        **/
/**  Beschreibung:                                                         **/
/**     Diese Definitionsdatei enthaelt Datentypen und Konstanten, die     **/
/**     an der Schnittstelle von DPE-Master und Anwendungs-                **/
/**     seite gebraucht werden.                                            **/
/**                                                                        **/
/**  Anmerkung:                                                            **/
/**    Durch Setzen der Schalter                                           **/
/**       DPM1_EXTENSION_x                                                 **/
/**       DPR_EXTENSION_x                                                  **/
/**    in der Konfigurationsdatei 'config.h' werden spezielle Protokoll-   **/
/**    erweiterungen eingefuegt.                                           **/
/**                                                                        **/
/**                                                                        **/
/**  Implementierungs-Hinweise:                                            **/
/**    Programmiersprache   : ANSI-C                                       **/
/**    Betriebssystem       :  -                                           **/
/**                                                                        **/
/****************************************************************************/
/**                                                                        **/
/**                                                                        **/
/**                            erstellt am:                                **/
/**                                                                        **/
/**                                                                        **/
/**       I N S T I T U T   F U E R   A U T O M A T I O N   U N D          **/
/**                                                                        **/
/**        K O M M U N I K A T I O N   e. V.   M A G D E B U R G           **/
/**                                                                        **/
/**                           ( I F A K )                                  **/
/**                                                                        **/
/**                                                                        **/
/**                   Steinfeldstrasse 3  (IGZ)                            **/
/**                       D-39179 Barleben                                 **/
/**                                                                        **/
/**                                                                        **/
/****************************************************************************/
/****************************************************************************/
/*
SD-Vers.: $Log: dpemdef.h,v $
SD-Vers.: Revision 1.3  2005/11/21 22:38:25  fna
SD-Vers.: Removed the not used BINFILES, EXTRAFILES, MISCFILES from the Makefile
SD-Vers.: system, changes to compile the io_driver under Unix.
SD-Vers.:
SD-Vers.: Revision 1.2  2005/11/15 15:34:32  fna
SD-Vers.: Updated ispro ifak-system header and DLL bindings, new probe
SD-Vers.: functionality in generic profibus interface, new mutex and condition
SD-Vers.: abstraction.
SD-Vers.:
 * 
 * 5     19.08.03 10:32 Sscharf
 * 
 * 3     8.05.00 13:21 Sscharf
 * 
 * 2     18.02.00 14:17 Sscharf
SD-Vers.: Revision 1.3  1999/05/05 13:00:58  pkr
SD-Vers.: Korektur der funcNum-Position in DPEMDEF_MSAC2MRejectInd_struct
SD-Vers.: Revision 1.2  1999/04/12 10:06:44  pkr
SD-Vers.: - Kodierungsaenderung der C2-Funktionen
SD-Vers.: - Entfernen SM_Init
SD-Vers.: - Anbindung DDLM-Funktionen
*/

#ifndef __DPEMDEF__
#define __DPEMDEF__

#include "ismbdef.h"	//	! sscharf

//#include <config.h>	//	! sscharf
//#ifdef DPE_MASTER		//	! sscharf

//#include <define.h>	//	! sscharf
//#include <ifdef.h>	//	! sscharf

#define PKD				//  ! sscharf
#ifndef PORTED_LINUX
#pragma pack(push, 1)	//  ! sscharf
#endif

/*--------------------------------------------------------------------------*/
/******************************* E X P O R T ********************************/
/*--------------------------------------------------------------------------*/


/*-------------*/
/* M A C R O S */
/*-------------*/

/* Funktionskennung */
#define DPEMDEF_FC_GET_MASTER_DIAG_REQ          0x21
#define DPEMDEF_FC_GET_MASTER_DIAG_CON          0xE1
#define DPEMDEF_FC_GET_BUS_PAR_REQ              0x22
#define DPEMDEF_FC_GET_BUS_PAR_CON              0xE2
#define DPEMDEF_FC_GET_SLAVE_PAR_REQ            0x23
#define DPEMDEF_FC_GET_SLAVE_PAR_CON            0xE3
#define DPEMDEF_FC_LOAD_BUS_PAR_REQ             0x24
#define DPEMDEF_FC_LOAD_BUS_PAR_CON             0xE4
#define DPEMDEF_FC_LOAD_SLAVE_PAR_REQ           0x25
#define DPEMDEF_FC_LOAD_SLAVE_PAR_CON           0xE5
#define DPEMDEF_FC_CHANGE_SLAVE_PAR_REQ         0x26
#define DPEMDEF_FC_CHANGE_SLAVE_PAR_CON         0xE6
#define DPEMDEF_FC_SET_MODE_REQ                 0x27
#define DPEMDEF_FC_SET_MODE_CON                 0xE7
#define DPEMDEF_FC_MODE_CHANGED_IND             0xA8
#define DPEMDEF_FC_MARK_REQ                     0x2B
#define DPEMDEF_FC_MARK_CON                     0xEB
#define DPEMDEF_FC_GLOBAL_CONTROL_REQ           0x2E
#define DPEMDEF_FC_GLOBAL_CONTROL_CON           0xEE
#define DPEMDEF_FC_DPEM_FAULT_IND               0xAF
#define DPEMDEF_FC_LIVE_LIST_REQ                0x35
#define DPEMDEF_FC_LIVE_LIST_CON                0xF5
#define DPEMDEF_FC_DDLM_SLAVE_DIAG_REQ          0x31
#define DPEMDEF_FC_DDLM_SLAVE_DIAG_CON          0xF1
#define DPEMDEF_FC_DDLM_RD_INP_REQ              0x36
#define DPEMDEF_FC_DDLM_RD_INP_CON              0xF6
#define DPEMDEF_FC_DDLM_RD_OUTP_REQ             0x37
#define DPEMDEF_FC_DDLM_RD_OUTP_CON             0xF7
#define DPEMDEF_FC_DDLM_GET_CFG_REQ             0x38
#define DPEMDEF_FC_DDLM_GET_CFG_CON             0xF8
#define DPEMDEF_FC_DDLM_SET_SLAVE_ADD_REQ       0x39
#define DPEMDEF_FC_DDLM_SET_SLAVE_ADD_CON       0xF9
#define DPEMDEF_FC_MSAC2_REQ                    0x3D
#define DPEMDEF_FC_MSAC2_IND                    0xBD
#define DPEMDEF_FC_MSAC2_CON                    0xFD


/* Funktiosnummer */
#define DPEMDEF_FN_MSAC2_INITIATE               0x57
#define DPEMDEF_FN_MSAC2_INITIATE_NEG           0xD7
#define DPEMDEF_FN_MSAC2_ABORT                  0x58
#define DPEMDEF_FN_MSAC2_REJECT                 0x41
#define DPEMDEF_FN_MSAC2_READ                   0x5E
#define DPEMDEF_FN_MSAC2_READ_NEG               0xDE
#define DPEMDEF_FN_MSAC2_WRITE                  0x5F
#define DPEMDEF_FN_MSAC2_WRITE_NEG              0xDF
#define DPEMDEF_FN_MSAC2_DATA_TRANSPORT         0x51
#define DPEMDEF_FN_MSAC2_DATA_TRANSPORT_NEG     0xD1


/* Konstanten fuer die Parameter von DPEM-Fault.ind */

/* Instance-Identifier */
#define DPEMDEF_PM_FAULT_ID_DPEM  0
#define DPEMDEF_PM_FAULT_ID_MSCY  1
#define DPEMDEF_PM_FAULT_ID_MSAC2 2
#define DPEMDEF_PM_FAULT_ID_DDLM  3

/* Error_Code */
#define DPEMDEF_PM_FAULT_DPEM_EC_OFFLINE 0



/* Konstanten fuer das RemoteAccessFlag */
#define DPEMDEF_READ_DATA_REM_BIT          0x80
#define DPEMDEF_LOAD_BUS_PARAM_REM_BIT     0x40
#define DPEMDEF_LOAD_SLAVE_PARAM_REM_BIT   0x20
#define DPEMDEF_CLEAR_COUNTER_REM_BIT      0x10
#define DPEMDEF_ACT_DEACT_SLAVES_REM_BIT   0x08
#define DPEMDEF_ACT_PARAM_REM_BIT          0x04
#define DPEMDEF_CHANGE_MODE_REM_BIT        0x02



/*------------*/
/*  T Y P E S */
/*------------*/

/* Datentyp fuer den Statusbereich */
typedef PKD struct DPEMDEF_StateArea_struct
{
	UInt8          dataTransferList[16];
  UInt8          systemDiagnostic[16];
  UInt8          stationOnline;
  UInt8          remoteAccessFlag;
  volatile UInt8 inDataState;
  volatile UInt8 inDataLastWrite;
  volatile UInt8 outDataState;
  volatile UInt8 outDataLastWrite;
} DPEMDEF_StateArea_type;


/* Datentyp fuer die Funktionsbeschreibung */
typedef PKD struct DPEMDEF_FuncDescr_struct
{
  UInt8 funcCode; /* Funktionskennung */
  UInt8 res[11];
  UInt8 funcNum;  /* Funktionsnummer */
} DPEMDEF_FuncDescr_type;


/**************************************/
/* Datentypen fuer die DPE-Funktionen */
/**************************************/

/* Get_Master_Diag.req */
typedef PKD struct DPEMDEF_GetMasterDiagReq_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 identifier;
} DPEMDEF_GetMasterDiagReq_type;

/* Get_Master_Diag.con */
typedef PKD struct DPEMDEF_GetMasterDiagCon_struct
{
  UInt8 funcCode;
  UInt8 res[6];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_GetMasterDiagCon_type;


/* Get_Bus_Par.req */
typedef PKD struct DPEMDEF_GetBusParReq_struct
{
  UInt8 funcCode;
  UInt8 res[4];
  UInt8 addOffsetHigh;
  UInt8 addOffsetLow;
} DPEMDEF_GetBusParReq_type;

/* Get_Bus_Par.con */
typedef PKD struct DPEMDEF_GetBusParCon_struct
{
  UInt8 funcCode;
  UInt8 res[6];
  UInt8 length;
  UInt8 data[248];
} DPEMDEF_GetBusParCon_type;


/* Get_Slave_Par.req */
typedef PKD struct DPEMDEF_GetSlaveParReq_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 slaveAddress;
  UInt8 res1;
  UInt8 addOffsetHigh;
  UInt8 addOffsetLow;
} DPEMDEF_GetSlaveParReq_type;

/* Get_Slave_Par.con */
typedef PKD struct DPEMDEF_GetSlaveParCon_struct
{
  UInt8 funcCode;
  UInt8 res[6];
  UInt8 length;
  UInt8 data[248];
} DPEMDEF_GetSlaveParCon_type;


/* Load_Bus_Par.req */
typedef PKD struct DPEMDEF_LoadBusParReq_struct
{
  UInt8 funcCode;
  UInt8 res[4];
  UInt8 addOffsetHigh;
  UInt8 addOffsetLow;
  UInt8 length;
  UInt8 data[248];
} DPEMDEF_LoadBusParReq_type;

/* Load_Bus_Par.con */
typedef PKD struct DPEMDEF_LoadBusParCon_struct
{
  UInt8 funcCode;
  UInt8 res;
  UInt8 state;
} DPEMDEF_LoadBusParCon_type;


/* Load_Slave_Par.req */
typedef PKD struct DPEMDEF_LoadSlaveParReq_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 slaveAddress;
  UInt8 res1;
  UInt8 addOffsetHigh;
  UInt8 addOffsetLow;
  UInt8 length;
  UInt8 data[246];
} DPEMDEF_LoadSlaveParReq_type;

/* Load_Slave_Par.con */
typedef PKD struct DPEMDEF_LoadSlaveParCon_struct
{
  UInt8 funcCode;
  UInt8 res;
  UInt8 state;
} DPEMDEF_LoadSlaveParCon_type;


/* Change_Slave_Par.req */
typedef PKD struct DPEMDEF_ChangeSlaveParReq_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 slaveAddress;
  UInt8 paraIdentifier;
  UInt8 res1[2];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_ChangeSlaveParReq_type;

/* Change_Slave_Par.con */
typedef PKD struct DPEMDEF_ChangeSlaveParCon_struct
{
  UInt8 funcCode;
  UInt8 res;
  UInt8 state;
} DPEMDEF_ChangeSlaveParCon_type;


/* Set_Mode.req */
typedef PKD struct DPEMDEF_SetModeReq_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 usifState;
} DPEMDEF_SetModeReq_type;

/* Set_Mode.con */
typedef PKD struct DPEMDEF_SetModeCon_struct
{
  UInt8 funcCode;
  UInt8 res[1];
  UInt8 state;
} DPEMDEF_SetModeCon_type;


/* Mode_Changed.ind */
typedef PKD struct DPEMDEF_ModeChangedInd_struct
{
  UInt8 funcCode;
  UInt8 res[2];
  UInt8 usifState;
  UInt8 res1;
  UInt8 locallyGenerated;
} DPEMDEF_ModeChangedInd_type;


/* Mark.req */
typedef PKD struct DPEMDEF_MarkReq_struct
{
  UInt8 funcCode;
} DPEMDEF_MarkReq_type;

/* Mark.con */
typedef PKD struct DPEMDEF_MarkCon_struct
{
  UInt8 funcCode;
  UInt8 res;
  UInt8 state;
  UInt8 dia;
} DPEMDEF_MarkCon_type;


/* Global_Control.req */
typedef PKD struct DPEMDEF_GlobalControlReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 res;
  UInt8 controlCommand;
  UInt8 groupSelect;
} DPEMDEF_GlobalControlReq_type;

/* Global_Control.con */
typedef PKD struct DPEMDEF_GlobalControlCon_struct
{
  UInt8 funcCode;
  UInt8 res;
  UInt8 state;
} DPEMDEF_GlobalControlCon_type;


/* MSAC2_Initiate.req */
typedef PKD struct DPEMDEF_MSAC2InitiateReq_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res;
  UInt8 remAdd;
  UInt8 res1[8];
  UInt8 funcNum;
  UInt8 res2[3];
  UInt8 sendTimeOutHigh;
  UInt8 sendTimeOutLow;
  UInt8 featSupp1;
  UInt8 featSupp2;
  UInt8 proFeatSupp1;
  UInt8 proFeatSupp2;
  UInt8 proIdentNumHigh;
  UInt8 proIdentNumLow;
  UInt8 addAddrParam[232];
} DPEMDEF_MSAC2InitiateReq_type;

/* MSAC2_Initiate.con */
typedef PKD struct DPEMDEF_MSAC2InitiateCon_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res;
  UInt8 remAdd;
  UInt8 res1[8];
  UInt8 funcNum;
  UInt8 maxLenDataUnit;
  UInt8 featSupp1;
  UInt8 featSupp2;
  UInt8 proFeatSupp1;
  UInt8 proFeatSupp2;
  UInt8 proIdentNumHigh;
  UInt8 proIdentNumLow;
  UInt8 addAddrParam[236];
} DPEMDEF_MSAC2InitiateCon_type;


/* MSAC2_Abort.req */
typedef PKD struct DPEMDEF_MSAC2AbortReq_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 subnet;
  UInt8 reasonCode;
} DPEMDEF_MSAC2AbortReq_type;

/* MSAC2_Abort.ind */
typedef PKD struct DPEMDEF_MSAC2AbortInd_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 abortClosed;
  UInt8 res[3];
  UInt8 locallyGenerated;
  UInt8 addDetailHigh;
  UInt8 addDetailLow;
  UInt8 res1[3];
  UInt8 funcNum;
  UInt8 subnet;
  UInt8 reasonCode;
} DPEMDEF_MSAC2AbortInd_type;


/* MSAC2M_Reject.ind */
typedef PKD struct DPEMDEF_MSAC2MRejectInd_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[3];
  UInt8 reasonCode;
  UInt8 res1[6];
  UInt8 funcNum;
} DPEMDEF_MSAC2MRejectInd_type;


/* MSAC2_Read.req */
typedef PKD struct DPEMDEF_MSAC2ReadReq_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 slotNumber;
  UInt8 index;
  UInt8 length;
} DPEMDEF_MSAC2ReadReq_type;

/* MSAC2_Read.con */
typedef PKD struct DPEMDEF_MSAC2ReadCon_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 res1[2];
  UInt8 length;
  UInt8 data[240];
} DPEMDEF_MSAC2ReadCon_type;


/* MSAC2_Write.req */
typedef PKD struct DPEMDEF_MSAC2WriteReq_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 slotNumber;
  UInt8 index;
  UInt8 length;
  UInt8 data[240];
} DPEMDEF_MSAC2WriteReq_type;

/* MSAC2_Write.con */
typedef PKD struct DPEMDEF_MSAC2WriteCon_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 res1[2];
  UInt8 length;
} DPEMDEF_MSAC2WriteCon_type;


/* MSAC2_Data_Transport.req */
typedef PKD struct DPEMDEF_MSAC2DataTransportReq_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 slotNumber;
  UInt8 index;
  UInt8 length;
  UInt8 data[240];
} DPEMDEF_MSAC2DataTransportReq_type;

/* MSAC2_Data_Transport.con */
typedef PKD struct DPEMDEF_MSAC2DataTransportCon_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 res1[2];
  UInt8 length;
  UInt8 data[240];
} DPEMDEF_MSAC2DataTransportCon_type;


/* MSAC2_Negative.con */
typedef PKD struct DPEMDEF_MSAC2NegativeCon_struct
{
  UInt8 funcCode;
  UInt8 cRef;
  UInt8 res[10];
  UInt8 funcNum;
  UInt8 errorDecode;
  UInt8 errorCode1;
  UInt8 errorCode2;
} DPEMDEF_MSAC2NegativeCon_type;


/* DDLM_Slave_Diag.req */
typedef PKD struct DPEMDEF_DDLMSlaveDiagReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
} DPEMDEF_DDLMSlaveDiagReq_type;

/* DDLM_Slave_Diag.con */
typedef PKD struct DPEMDEF_DDLMSlaveDiagCon_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 state;
  UInt8 res1[4];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_DDLMSlaveDiagCon_type;


/* DDLM_RD_Inp.req */
typedef PKD struct DPEMDEF_DDLMRdInpReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
} DPEMDEF_DDLMRdInpReq_type;

/* DDLM_RD_Inp.con */
typedef PKD struct DPEMDEF_DDLMRdInpCon_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 state;
  UInt8 res1[4];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_DDLMRdInpCon_type;


/* DDLM_RD_Outp.req */
typedef PKD struct DPEMDEF_DDLMRdOutpReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
} DPEMDEF_DDLMRdOutpReq_type;

/* DDLM_RD_Outp.con */
typedef PKD struct DPEMDEF_DDLMRdOutpCon_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 state;
  UInt8 res1[4];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_DDLMRdOutpCon_type;


/* DDLM_Get_Cfg.req */
typedef PKD struct DPEMDEF_DDLMGetCfgReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
} DPEMDEF_DDLMGetCfgReq_type;

/* DDLM_Get_Cfg.con */
typedef PKD struct DPEMDEF_DDLMGetCfgCon_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 state;
  UInt8 res[4];
  UInt8 length;
  UInt8 data[244];
} DPEMDEF_DDLMGetCfgCon_type;


/* DDLM_Set_Slave_Add.req */
typedef PKD struct DPEMDEF_DDLMSetSlaveAddReq_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 res;
  UInt8 newSlaveAdd;
  UInt8 idendNumberHigh;
  UInt8 idendNumberLow;
  UInt8 noAddChg;
  UInt8 length;
  UInt8 data[240];
} DPEMDEF_DDLMSetSlaveAddReq_type;

/* DDLM_Set_Slave_Add.con */
typedef PKD struct DPEMDEF_DDLMSetSlaveAddCon_struct
{
  UInt8 funcCode;
  UInt8 remAdd;
  UInt8 state;
} DPEMDEF_DDLMSetSlaveAddCon_type;

/* DPEM_Fault.ind */
typedef PKD struct DPEMDEF_DPEMFaultInd_struct
{
  UInt8 funcCode;
  UInt8 instanceId;
  UInt8 errorCode;
} DPEMDEF_DPEMFaultInd_type;

/* FMA1/2_Life_List.req */
typedef PKD struct DPEMDEF_LiveListReq_struct
{
  UInt8 funcCode;
} DPEMDEF_LiveListReq_type;

/* FMA1/2_Life_List.con */
typedef PKD struct DPEMDEF_LiveListCon_struct
{
  UInt8 funcCode;
  UInt8 liveListLength;
  struct
  {
    UInt8 address;
    UInt8 state;

  } liveList[127];
} DPEMDEF_LiveListCon_type;


/* Union fuer die DPE-Funktionen */
typedef PKD union DPEMDEF_Function_union
{
  DPEMDEF_GetMasterDiagReq_type      *getMasterDiagReq_ptr;
  DPEMDEF_GetMasterDiagCon_type      *getMasterDiagCon_ptr;
  DPEMDEF_GetBusParReq_type          *getBusParReq_ptr;
  DPEMDEF_GetBusParCon_type          *getBusParCon_ptr;
  DPEMDEF_GetSlaveParReq_type        *getSlaveParReq_ptr;
  DPEMDEF_GetSlaveParCon_type        *getSlaveParCon_ptr;
  DPEMDEF_LoadBusParReq_type         *loadBusParReq_ptr;
  DPEMDEF_LoadBusParCon_type         *loadBusParCon_ptr;
  DPEMDEF_LoadSlaveParReq_type       *loadSlaveParReq_ptr;
  DPEMDEF_LoadSlaveParCon_type       *loadSlaveParCon_ptr;
  DPEMDEF_ChangeSlaveParReq_type     *changeSlaveParReq_ptr;
  DPEMDEF_ChangeSlaveParCon_type     *changeSlaveParCon_ptr;
  DPEMDEF_SetModeReq_type            *setModeReq_ptr;
  DPEMDEF_SetModeCon_type            *setModeCon_ptr;
  DPEMDEF_ModeChangedInd_type        *modeChangedInd_ptr;
  DPEMDEF_MarkReq_type               *markReq_ptr;
  DPEMDEF_MarkCon_type               *markCon_ptr;
  DPEMDEF_GlobalControlReq_type      *globalControlReq_ptr;
  DPEMDEF_GlobalControlCon_type      *globalControlCon_ptr;
  DPEMDEF_MSAC2InitiateReq_type      *msac2InitiateReq_ptr;
  DPEMDEF_MSAC2InitiateCon_type      *msac2InitiateCon_ptr;
  DPEMDEF_MSAC2AbortReq_type         *msac2AbortReq_ptr;
  DPEMDEF_MSAC2AbortInd_type         *msac2AbortInd_ptr;
  DPEMDEF_MSAC2MRejectInd_type       *msac2mRejectInd_ptr;
  DPEMDEF_MSAC2ReadReq_type          *msac2ReadReq_ptr;
  DPEMDEF_MSAC2ReadCon_type          *msac2ReadCon_ptr;
  DPEMDEF_MSAC2WriteReq_type         *msac2WriteReq_ptr;
  DPEMDEF_MSAC2WriteCon_type         *msac2WriteCon_ptr;
  DPEMDEF_MSAC2DataTransportReq_type *msac2DataTransportReq_ptr;
  DPEMDEF_MSAC2DataTransportCon_type *msac2DataTransportCon_ptr;
  DPEMDEF_MSAC2NegativeCon_type      *negativeMsac2Con_ptr;
  DPEMDEF_DDLMSlaveDiagReq_type      *ddlmSlaveDiagReq_ptr;
  DPEMDEF_DDLMSlaveDiagCon_type      *ddlmSlaveDiagCon_ptr;
  DPEMDEF_DDLMRdInpReq_type          *ddlmRdInpReq_ptr;
  DPEMDEF_DDLMRdInpCon_type          *ddlmRdInpCon_ptr;
  DPEMDEF_DDLMRdOutpReq_type         *ddlmRdOutpReq_ptr;
  DPEMDEF_DDLMRdOutpCon_type         *ddlmRdOutpCon_ptr;
  DPEMDEF_DDLMGetCfgReq_type         *ddlmGetCfgReq_ptr;
  DPEMDEF_DDLMGetCfgCon_type         *ddlmGetCfgCon_ptr;
  DPEMDEF_DDLMSetSlaveAddReq_type    *ddlmSetSlaveAddReq_ptr;
  DPEMDEF_DDLMSetSlaveAddCon_type    *ddlmSetSlaveAddCon_ptr;
  DPEMDEF_DPEMFaultInd_type          *faultInd_ptr;
  DPEMDEF_LiveListReq_type           *fma12LiveListReq_ptr;
  DPEMDEF_LiveListCon_type           *fma12LiveListCon_ptr;
  UInt8                              *commonBox_ptr;
} DPEMDEF_Function_type;


/* Datentyp fuer Kommando- und Meldungsbereich */
typedef PKD union DPEMDEF_Box_union
{
  DPEMDEF_FuncDescr_type *funcDescr_ptr;
  DPEMDEF_Function_type   function;
} DPEMDEF_Box_type;

#ifndef PORTED_LINUX
#pragma pack(pop)	//  ! sscharf
#endif

/*--------------------------------------------------------------------------*/
/******************************* L O K A L **********************************/
/*--------------------------------------------------------------------------*/


/*-------------*/
/* M A C R O S */
/*-------------*/

/* Positionen der Bits in den Semaphoren */
#define DPEMDEF_RESET_COLD_BIT   0x10
#define DPEMDEF_RESET_WARM_BIT   0x08
#define DPEMDEF_DIAG_BIT         0x04
#define DPEMDEF_REPORT_BOX_BIT   0x02
#define DPEMDEF_COMMAND_BOX_BIT  0x01

/* Adresse des Default-Slaves */
#define DPEMDEF_DEFAULT_SLAVE_ADDRESS  126

/* Konstanten fuer den an wendungsspezifischen Statusbereich */
#ifdef DPM1_EXTENSION_1

/* Bitmasken fuer anwendungsspezifischen Statusbereich */
#define DPEMDEF_MASK_ERROR_BIT         0x08
#define DPEMDEF_MASK_ERROR_LATCH_BIT   0x04
#define DPEMDEF_MASK_DTA_BIT           0x02

/* Groesse des anwendungsspezifischen Statusbereiches */
#define DPEMDEF_APP_STATE_AREA_SIZE    65

/* Offsets im anwendungsspezifischen Statusbereich */
#define DPEMDEF_OFFSET_ERROR_BIT       0
#define DPEMDEF_OFFSET_ERROR_LATCH_BIT 1
#define DPEMDEF_OFFSET_DTA_BIT         2

/* Maske fuer Sammelbit Error-Latch */
#define DPEMDEF_GROUP_ERROR_LATCH      0x40

#endif /* ifdef DPM1_EXTENSION_1 */

/*------------*/
/*  T Y P E S */
/*------------*/



//#endif /* ifdef DPE_MASTER */  ! sscharf
#endif /* ifndef __DPEMDEF__ */
