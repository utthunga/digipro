
#ifndef __FMS_H__
#define __FMS_H__



/***********************************/
/*       F U N C T I O N E N       */
/***********************************/

#ifndef PORTED_LINUX
#pragma pack(push, 8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Verwaltung des Befehlsbereiches */
BYTE __cdecl FMS_WriteCommand(
					BYTE  device,
					BYTE  channel,
					BYTE* source_ptr,
					BYTE  len);

BYTE __cdecl FMA7_WriteCommand(
					BYTE  device,
					BYTE  channel,
					BYTE* source_ptr,
					BYTE  len);

BYTE __cdecl FMS_GetReport(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr);

BYTE __cdecl FMA7_GetReport(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr);

#ifdef __cplusplus
}
#endif

#ifndef PORTED_LINUX
#pragma pack(pop)
#endif

#endif // __FMS_H__
