
/* 
 * $Id: pb_ispro_mailbox.c,v 1.10 2006/02/24 12:58:40 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 2005 Matthias Neumann <matthias.neumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <string.h>
#include <time.h>

#include "pb_ispro_data.h"
#include "pb_ispro_dll.h"
#include "pb_ispro_mailbox.h"


int
pb_ispro_send_and_wait(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	DPEMDEF_Box_type CommandBox;
	int ret;

	condition_lock(data->handle);

	/* wrong driver usage */
	assert(data->request_pending == 0);

	CommandBox.function.commonBox_ptr = data->data;

	ret = pDPEM_WriteCommand(dev->device, dev->channel, CommandBox);
	if (ret == 0)
	{
		data->request_pending = 1;
		condition_wait(data->handle);
		data->request_pending = 0;

		ret = data->result;
	}
	else
		ret = PB_ISPRO_MODULE_ERROR | (ret << 8);

	condition_unlock(data->handle);

	return ret;
}

static void
pb_ispro_read(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	DPEMDEF_Box_type ReportBox;
	ReportBox.funcDescr_ptr = (struct DPEMDEF_FuncDescr_struct *)data->data;

	ret = pDPEM_GetReport(dev->device, dev->channel, ReportBox);
	if (ret)
	{
		data->result = PB_ISPRO_MODULE_ERROR | (ret << 8);
		return;
	}

	switch (ReportBox.funcDescr_ptr->funcCode)
	{
		case DPEMDEF_FC_LOAD_BUS_PAR_CON:
		{
			data->result = state2result(ReportBox.function.loadBusParCon_ptr->state);
			break;
		}
		case DPEMDEF_FC_SET_MODE_CON:
		{
			data->result = state2result(ReportBox.function.setModeCon_ptr->state);
			break;
		}
		case DPEMDEF_FC_GET_MASTER_DIAG_CON:
		{
			data->result = 0;
			data->data[2] = ReportBox.function.getMasterDiagCon_ptr->data[0];
			break;
		}
		case DPEMDEF_FC_DDLM_SET_SLAVE_ADD_CON:
		{
			data->result = state2result(ReportBox.function.ddlmSetSlaveAddCon_ptr->state);
			break;
		}
		case DPEMDEF_FC_LIVE_LIST_CON:
		{
			int i, len;

			assert(data->livelist);

			len = ReportBox.function.fma12LiveListCon_ptr->liveListLength;
			if (len >= 3)
			{
				len -= 1;
				len /= 2;
				data->livelist->length = len;

				for (i = 0; i < len; i++)
				{
					data->livelist->list[i].addr = ReportBox.function.fma12LiveListCon_ptr->liveList[i].address;
					data->livelist->list[i].state = ReportBox.function.fma12LiveListCon_ptr->liveList[i].state;
				}

				data->result = 0;
			}
			else
				data->result = state2result(len);

			break;
		}
		case DPEMDEF_FC_MSAC2_CON:
		{
			switch (ReportBox.funcDescr_ptr->funcNum)
			{
				case DPEMDEF_FN_MSAC2_INITIATE_NEG:
				{
					assert(data->conn);

					data->result = dpv1error2result(
								ReportBox.function.negativeMsac2Con_ptr->errorDecode,
								ReportBox.function.negativeMsac2Con_ptr->errorCode1,
								ReportBox.function.negativeMsac2Con_ptr->errorCode2);

					/* automatically free the handle */
					data->conn->used = 0;
					data->conn = NULL;

					break;
				}
				case DPEMDEF_FN_MSAC2_INITIATE:
				{
					assert(data->conn);

					data->result = 0;
					if (data->req)
					{
						data->req->datalen = 11;
						memcpy(data->req->data,
						       &ReportBox.function.msac2InitiateCon_ptr->maxLenDataUnit,
						       data->req->datalen);
					}

					data->conn->connected = 1;
					data->conn->maxlength = ReportBox.function.msac2InitiateCon_ptr->maxLenDataUnit;

					break;
				}
				case DPEMDEF_FN_MSAC2_READ:
				{
					assert(data->req && data->conn);

					data->result = 0;
					data->req->datalen = ReportBox.function.msac2ReadCon_ptr->length;
					memcpy(data->req->data,
					       &ReportBox.function.msac2ReadCon_ptr->data,
					       data->req->datalen);

					break;
				}
				case DPEMDEF_FN_MSAC2_WRITE:
				{
					assert(data->req && data->conn);

					data->result = 0;
					data->req->datalen = ReportBox.function.msac2WriteCon_ptr->length;

					break;
				}
				case DPEMDEF_FN_MSAC2_READ_NEG:
				case DPEMDEF_FN_MSAC2_WRITE_NEG:
				{
					assert(data->req && data->conn);

					data->result = dpv1error2result(
								ReportBox.function.negativeMsac2Con_ptr->errorDecode,
								ReportBox.function.negativeMsac2Con_ptr->errorCode1,
								ReportBox.function.negativeMsac2Con_ptr->errorCode2);

					break;
				}
			}
			break;
		}
		case DPEMDEF_FC_MSAC2_IND:
		{
			if (DPEMDEF_FN_MSAC2_ABORT == ReportBox.funcDescr_ptr->funcNum)
			{
				assert(data->conn);

				data->result = reasoncode2result(ReportBox.function.msac2AbortInd_ptr->reasonCode);

				if (data->conn)
					data->conn->connected = 0;
			}
			else
				data->result = PB_ISPRO_ERROR;

			break;
		}
		case DPEMDEF_FC_MODE_CHANGED_IND:
		{
			/* Device changed itself mode */
			data->mode_changed = 1;
			data->result = PB_ISPRO_ERROR;
			break;
		}
		case DPEMDEF_FC_DPEM_FAULT_IND:
		{
			data->result = PB_ISPRO_ERROR;
			break;
		}
		case DPEMDEF_FC_LOAD_SLAVE_PAR_CON:
		{
			data->result = state2result(ReportBox.function.loadSlaveParCon_ptr->state);
			break;
		}
		case DPEMDEF_FC_CHANGE_SLAVE_PAR_CON:
		{
			data->result = state2result(ReportBox.function.changeSlaveParCon_ptr->state);
			break;
		}
		default:
		{
			data->result = PB_ISPRO_ERROR;
			break;
		}
	}
}

static void
ispro_notification(BYTE device, BYTE channel, void *_dev)
{
	struct pb_dev *dev = _dev;
	struct pb_ispro_data *data = dev->data;

	assert(dev->device == device);
	assert(dev->channel == channel);

	pb_ispro_read(dev);

	condition_lock(data->handle);

	if (data->request_pending)
		condition_notify(data->handle);

	condition_unlock(data->handle);
}

static struct pb_ispro_data *devices[MAX_NR_OF_CARDS];

int
CheckDevice(struct pb_dev *dev)
{
	struct pb_ispro_data *list;
	int ret = 0;

	list = devices[dev->device];
	while (list)
	{
		assert(list->dev->device == dev->device);

		if (list->dev->channel == dev->channel)
		{
			ret = PB_ISPRO_CHANNEL_IN_USE;
			break;
		}

		list = list->next;
	}

	return ret;
}

int
InitDevice(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	if (!devices[dev->device])
	{
		int i;

		initData_type initData;
		WORD DeviceMask = 1 << dev->device;

		DeviceError_type devError;
		ChannelError_type chError;

		memset(&initData, 0, sizeof(initData));
		for (i = 0; i < data->channels; i++)
		{
			initData[dev->device].Protocol[i].ProtocolID = DPEM_PROTOCOL; // =19 DPE-Master
			initData[dev->device].Protocol[i].Parameter.dpInOutAreaSize = 100;
		}

		ret = pInitDll(initData, DeviceMask, &devError, &chError);
		if (ret)
			return PB_ISPRO_INIT_DLL_ERROR | (ret << 8);
	}

	ret = pWatchMailbox(dev->device, dev->channel, ispro_notification, dev, 10);
	if (ret)
	{
		if (!devices[dev->device])
			pResetDevice(dev->device);

		return PB_ISPRO_WATCHMAILBOX_ERROR | (ret << 8);
	}

	data->next = devices[dev->device];
	devices[dev->device] = data;

	data->total_input_data_offset = 0;
	data->total_output_data_offset = 0;

	return PB_NO_ERROR;
}

int
CloseDevice(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	struct pb_ispro_data **list;
	int ret = 0;

	list = &(devices[dev->device]);
	while (*list)
	{
		if (*list == data)
			break;

		list = &((*list)->next);
	}

	assert(*list);
	*list = data->next;

	pStopWatchThread(dev->device, dev->channel);

	if (!devices[dev->device])
	{
		ret = pResetDevice(dev->device);

		if (ret)
			return PB_ISPRO_MODULE_ERROR | (ret << 8);
	}

	return PB_NO_ERROR;
}

int
check_online(struct pb_dev *dev)
{
	BOOLEAN online;
	int ret;

	ret = pDPEM_StationOnline(dev->device, dev->channel, &online);
	if (ret == 0)
	{
		if (!online)
			ret = PB_ISPRO_NOT_ONLINE;
	}

	return ret;
}