
/* 
 * $Id: pb_ispro.c,v 1.36 2009/08/05 11:33:01 fna Exp $
 * 
 * Copyright (C) 2005 Matthias Neumann <matthias.neumann@ifak-md.de>
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "pb_ispro_data.h"
#include "pb_ispro_dll.h"
#include "pb_ispro_mailbox.h"
#include "pb_ispro.h"


static int pb_ispro_probe(unsigned char driver, struct pb_info *, size_t len);

static int pb_ispro_open(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_ispro_close(struct pb_dev *);

/* static int pb_ispro_ioctl(struct pb_dev *, int request, va_list); */

static int pb_ispro_set_bus_parameter(struct pb_dev *, const struct pb_bus_parameter *);

static int pb_ispro_get_input(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);

static int pb_ispro_set_output(struct pb_dev *,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);

static int pb_ispro_set_mode(struct pb_dev *,
		enum pb_mode mode);

static int pb_ispro_get_mode(struct pb_dev *,
		enum pb_mode *mode);

static int pb_ispro_set_slave_param(struct pb_dev *,
		unsigned char slaveaddr, const char *data, size_t datalen);

static int pb_ispro_change_slave_addr(struct pb_dev *, unsigned char slaveaddr,
		unsigned char addr, unsigned short ident_number);

static int pb_ispro_livelist(struct pb_dev *,
		struct pb_livelist_info *livelist);

static int pb_ispro_connect(struct pb_dev *,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *);

static int pb_ispro_disconnect(struct pb_dev *, pb_slave_t);

static int pb_ispro_read(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static int pb_ispro_write(struct pb_dev *, pb_slave_t,
		struct pb_request *);

static const char *pb_ispro_errstr(int result, const char **descr);

static int pb_ispro_load_slave_param(struct pb_dev *dev,
				const BYTE address,
				const WORD ident_number,
				const BYTE cfg_data[],
				const BYTE cfg_data_len,
				const BYTE user_prm_data[],
				const BYTE user_prm_data_len,
				const BYTE input_data_len,
				WORD *input_data_offset,
				const BYTE output_data_len,
				WORD *output_data_offset);
static int pb_ispro_read_system_diagnostic(struct pb_dev *dev, BYTE dest[16]);
static int pb_ispro_get_slave_diag(struct pb_dev *dev, const BYTE slave_address, BYTE *diag_data[], BYTE *diag_data_len);

static int pb_ispro_read_data_transfer_list(struct pb_dev *dev, BYTE dest[16]);
static int pb_ispro_change_slave_param(
				struct pb_dev *dev,
				const BYTE address,
				const BYTE action,
				const BYTE data_len,
				const BYTE prm_data[]
				);


struct pb_funcs pb_ispro_funcs =
{
	pb_ispro_probe,
	pb_ispro_open,
	pb_ispro_close,
	NULL, /* pb_ispro_ioctl, */
	pb_ispro_set_bus_parameter,
	pb_ispro_get_input,
	pb_ispro_set_output,
	pb_ispro_set_mode,
	pb_ispro_get_mode,
	pb_ispro_set_slave_param,
	pb_ispro_change_slave_addr,
	pb_ispro_livelist,
	pb_ispro_connect,
	pb_ispro_disconnect,
	pb_ispro_read,
	pb_ispro_write,
	pb_ispro_errstr,
	pb_ispro_load_slave_param,
	pb_ispro_get_slave_diag,
	pb_ispro_read_system_diagnostic,
	pb_ispro_read_data_transfer_list,
	pb_ispro_change_slave_param
};

static int nr_of_channels(int DeviceType, const char **name)
{
	const char *dev_name = "isPro unknown";
	int channels = 1;

	switch (DeviceType)
	{
	case DEVTYPE_ISA:
		channels = 4;
		dev_name = "isPro ISA";
		break;
	case DEVTYPE_PCI:
		channels = 4;
		dev_name = "isPro PCI";
		break;
	case DEVTYPE_USB:
		channels = 1;
		dev_name = "isPro USB";
		break;
	case DEVTYPE_PCMCIA:
		channels = 1;
		dev_name = "isPro PCMCIA";
		break;
	case DEVTYPE_USB12:
		channels = 1;
		dev_name = "isPro USBx12";
		break;
	case DEVTYPE_PCI12:
		channels = 2;
		dev_name = "isPro PCIx12";
		break;
	case DEVTYPE_NETCUBE:
		channels = 2;
		dev_name = "isNet Cube";
		break;
	}

	if (name)
		*name = dev_name;

	return channels;
}

static int pb_ispro_probe(unsigned char driver, struct pb_info *info, size_t len)
{
	int found = 0;
	int ret;

	ret = IsProOpenDLL();
	if (ret == 0)
	{
		int i;

		for (i = 0; i < MAX_NR_OF_CARDS; i++)
		{
			DeviceInfo_type dev_info;

			ret = pGetDeviceInfo(i, &dev_info);
			if (ret == 0)
			{
				if (dev_info.DeviceType && dev_info.bAttached)
				{
					const char *name;

					info[found].driver = driver;
					info[found].device = i;
					info[found].in_use = (dev_info.bInUse == 1) ? 1 : 0;
					info[found].channels = nr_of_channels(dev_info.DeviceType, &name);
					info[found].flags = PB_DRIVER_FEATURES_BUSPARAMETER|
							    PB_DRIVER_FEATURES_LIVELIST|
							    PB_DRIVER_FEATURES_IN_USE;

					if (dev_info.DeviceID)
					{
						snprintf(info[found].name, sizeof(info[found].name),
							 "%s [ID %lx]", name, dev_info.DeviceID);
					}
					else
					{
						snprintf(info[found].name, sizeof(info[found].name),
							 "%s", name);
					}

					++found;
					if ((size_t)found >= len)
						break;
				}
			}
		}

		IsProCloseDLL();
	}

	return found;
}

static int pb_ispro_open(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
	DeviceInfo_type dev_info;
	struct pb_ispro_data *data;
	int ret;

	if (dev->device >= MAX_NR_OF_CARDS)
		return PB_INVALID_DEVICE;

	ret = CheckDevice(dev);
	if (ret)
		return ret;

	ret = IsProOpenDLL();
	if (ret)
		return ret;

	data = malloc(sizeof(*data));
	if (data)
	{
		memset(data, 0, sizeof(*data));

		data->handle = condition_create();
		if (!data->handle)
		{
			ret = PB_OUTOFMEMORY;
			goto error;
		}

		data->dev = dev;
		dev->data = data;
	}
	else
	{
		ret = PB_OUTOFMEMORY;
		goto error;
	}

	ret = pGetDeviceInfo(dev->device, &dev_info);
	if (ret)
		goto error;

	if (dev_info.DeviceType == DEVTYPE_NO_DEV)
	{
		ret = PB_INVALID_DEVICE;
		goto error;
	}

	if (dev_info.bAttached == 0)
	{
		ret = PB_ISPRO_DEVICE_NOT_ATTACHED;
		goto error;
	}

	if (dev_info.bInUse == 1)
	{
		ret = PB_DEVICE_IN_USE;
		goto error;
	}

	data->channels = nr_of_channels(dev_info.DeviceType, NULL);
	if (dev->channel >= data->channels)
	{
		ret = PB_INVALID_CHANNEL;
		goto error;
	}

	ret = InitDevice(dev);
	if (ret)
		goto error;

	ret = pb_ispro_set_bus_parameter(dev, par);
	if (ret)
	{
		CloseDevice(dev);
		goto error;
	}

	return PB_NO_ERROR;

error:
	IsProCloseDLL();

	if (data)
	{
		if (data->handle)
			condition_destroy(data->handle);

		free(data);
	}

	return ret;
}

static int pb_ispro_close(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	ret = CloseDevice(dev);
	IsProCloseDLL();

	condition_destroy(data->handle);
	free(data);

	return ret;
}

static unsigned char
baudrate2ispro(enum pb_baudrate baudrate)
{
	unsigned char ret = 0;

	switch (baudrate)
	{
		case PB_BAUD_9_6_K:
			ret = 0;
			break;
		case PB_BAUD_19_2_K:
			ret = 1;
			break;
		case PB_BAUD_93_75_K:
			ret = 2;
			break;
		case PB_BAUD_187_5_K:
			ret = 3;
			break;
		case PB_BAUD_500_K:
			ret = 4;
			break;
		case PB_BAUD_1_5_M:
			ret = 6;
			break;
		case PB_BAUD_3_M:
			ret = 7;
			break;
		case PB_BAUD_6_M:
			ret = 8;
			break;
		case PB_BAUD_12_M:
			ret = 9;
			break;
		case PB_BAUD_31_25_K:
			ret = 10;
			break;
		case PB_BAUD_45_45_K:
			ret = 11;
			break;
		case PB_BAUD_57_60_K:
			ret = 12;
			break;
		default:
			assert(0);
			break;
	}

	return ret;
}

static int pb_ispro_set_bus_parameter(struct pb_dev *dev, const struct pb_bus_parameter *par)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	DPEMDEF_LoadBusParReq_type *bus_par_req;
	unsigned short len1, len2;

	data->req = NULL;
	data->conn = NULL;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	bus_par_req = (DPEMDEF_LoadBusParReq_type *)data->data;

	bus_par_req->funcCode		= DPEMDEF_FC_LOAD_BUS_PAR_REQ;
	bus_par_req->addOffsetHigh	= 0;
	bus_par_req->addOffsetLow	= 0;
	len1				= 66 + par->Master_User_Data_Len;
	bus_par_req->length		= (BYTE)len1;

	bus_par_req->data[ 0]		= len1 >> 8;
	bus_par_req->data[ 1]		= (BYTE)len1;
	bus_par_req->data[ 2]		= par->FDL_Add; 
	bus_par_req->data[ 3]		= baudrate2ispro(par->Baud_rate); 
	bus_par_req->data[ 4]		= par->Tsl >> 8;
	bus_par_req->data[ 5]		= (BYTE)(par->Tsl);
	bus_par_req->data[ 6]		= par->minTsdr >> 8;
	bus_par_req->data[ 7]		= (BYTE)(par->minTsdr);
	bus_par_req->data[ 8]		= par->maxTsdr >> 8;
	bus_par_req->data[ 9]		= (BYTE)(par->maxTsdr);
	bus_par_req->data[10]		= par->Tqui;
	bus_par_req->data[11]		= par->Tset;
	bus_par_req->data[12]		= (BYTE)(par->Ttr >> 24); 
	bus_par_req->data[13]		= (BYTE)(par->Ttr >> 16);
	bus_par_req->data[14]		= (BYTE)(par->Ttr >> 8);
	bus_par_req->data[15]		= (BYTE)(par->Ttr);
	bus_par_req->data[16]		= par->G;
	bus_par_req->data[17]		= par->HSA;
	bus_par_req->data[18]		= par->max_retry_limit;
	bus_par_req->data[19]		= par->Bp_Flag;
	bus_par_req->data[20]		= par->Min_Slave_Intervall >> 8;  
	bus_par_req->data[21]		= (BYTE)(par->Min_Slave_Intervall);
	bus_par_req->data[22]		= par->Poll_Timeout >> 8;  
	bus_par_req->data[23]		= (BYTE)(par->Poll_Timeout);
	bus_par_req->data[24]		= par->Data_control_Time >> 8;  
	bus_par_req->data[25]		= (BYTE)(par->Data_control_Time);

	len2 = par->Master_User_Data_Len + 34;
	bus_par_req->data[32]		= len2 >> 8;     
	bus_par_req->data[33]		= (BYTE)(len2);

	memcpy(&bus_par_req->data[34], par->Master_Class2_Name, sizeof(par->Master_Class2_Name));
	memcpy(&bus_par_req->data[66], par->Master_User_Data, par->Master_User_Data_Len);

	ret = pb_ispro_send_and_wait(dev);

	return ret;
}

static int pb_ispro_get_input(struct pb_dev *dev,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	return pDPEM_ReadInputData(dev->device, dev->channel,
				   data, (UNSIGNED16)offset, (BYTE)datalen, slaveaddr);
}

static int pb_ispro_set_output(struct pb_dev *dev,
		unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	return pDPEM_WriteOutputData(dev->device, dev->channel,
				     data, (UNSIGNED16)offset, (BYTE)datalen, slaveaddr);
}

/* mode: USIF-State
 *       Offline      00h
 *       Stop         40h
 *       Clear        80h
 *       Operate      C0h
 */
static int pb_ispro_set_mode(struct pb_dev *dev,
		enum pb_mode mode)
{
	struct pb_ispro_data *data = dev->data;
	int ret;
	data->mode = mode;

	data->req = NULL;
	data->conn = NULL;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[0] = DPEMDEF_FC_SET_MODE_REQ;
	data->data[3] = mode;

	ret = pb_ispro_send_and_wait(dev);
	if (ret == 0)
	{
		ret = check_online(dev);
		if (ret == PB_ISPRO_NOT_ONLINE)
		{
			time_t finish = time(NULL) + 3;

			while ((ret = check_online(dev)))
			{
				if (ret != PB_ISPRO_NOT_ONLINE)
					break;

				if (finish < time(NULL))
				{
					ret = PB_ISPRO_NOT_ONLINE;
					break;
				}

				ms_sleep(20);
			}

			/* If the pbIspro was not online and is going to be
			 * online the pbIspro driver claims to be online (see
			 * check_online() function) but in reality it isn't
			 * fully online. We need to wait here a little bit
			 * more as an immediatly following connect fail
			 * otherwise.
			 */
			ms_sleep(500);
		}
	}

	return ret;
}

static int pb_ispro_get_mode(struct pb_dev *dev,
		enum pb_mode *mode)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	data->req = NULL;
	data->conn = NULL;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[0] = DPEMDEF_FC_GET_MASTER_DIAG_REQ;
	data->data[3] = 127; /* Master Status */

	ret = pb_ispro_send_and_wait(dev);
	if (ret == 0)
		*mode = data->data[2];

	return ret;
}

static int pb_ispro_set_slave_param(struct pb_dev *dev,
		unsigned char slaveaddr, const char *data, size_t datalen)
{
	/* XXX todo
	 * int retval;
	 * retval = SetSlaveParam(slaveaddr,data,datalen);
	 */

	return PB_UNIMPLEMENTED;
}

static int pb_ispro_change_slave_addr(struct pb_dev *dev, unsigned char slaveaddr,
		unsigned char addr, unsigned short ident_number)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	data->req = NULL;
	data->conn = NULL;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[0] = DPEMDEF_FC_DDLM_SET_SLAVE_ADD_REQ;	/* funcCode */
	data->data[1] = slaveaddr;				/* remAdd */
	data->data[3] = addr;					/* newSlaveAdd */
	data->data[4] = ident_number >> 8;			/* idendNumberHigh */
	data->data[5] = ident_number & 0xff;			/* idendNumberLow */

	ret = pb_ispro_send_and_wait(dev);

	return ret;
}

static int check_for_mode(struct pb_dev *dev, enum pb_mode test)
{
	struct pb_ispro_data *data = dev->data;
	int ret = 0;

	enum pb_mode mode;
	
	ret = pb_ispro_get_mode(dev, &mode);
	
	if (ret == 0 && mode != test)
		ret = PB_ISPRO_MODE_MISMATCH;

	return ret;
}

static int check_mode(struct pb_dev *dev)
{
	struct pb_ispro_data *data = dev->data;
	int ret = 0;

	if (data->mode_changed)
	{
		/* The isPro changed itself it's operating mode.
		 * If the isPro is now OFFLINE try to restore the
		 * mode the application wanted.
		 */
		enum pb_mode mode;

		ret = pb_ispro_get_mode(dev, &mode);
		if (ret == 0 && mode == OFFLINE && data->mode != OFFLINE)
		{
			ret = pb_ispro_set_mode(dev, STOP);
			if (ret == 0 && data->mode != STOP)
			{
				ret = pb_ispro_set_mode(dev, CLEAR);
				if (ret == 0 && data->mode != CLEAR)
				{
					ret = pb_ispro_set_mode(dev, OPERATE);
				}
			}
		}

		data->mode_changed = 0;
	}

	if (ret == 0)
		ret = check_online(dev);

	return ret;
}

static int pb_ispro_livelist(struct pb_dev *dev,
		struct pb_livelist_info *livelist)
{
	struct pb_ispro_data *data = dev->data;
	int ret;

	ret = check_mode(dev);
	if (ret)
		return ret;

	data->req = NULL;
	data->conn = NULL;
	data->livelist = livelist;

	memset(data->data, 0, sizeof(data->data));
	data->data[0] = DPEMDEF_FC_LIVE_LIST_REQ;

	livelist->length = 0;
	ret = pb_ispro_send_and_wait(dev);

	return ret;
}

static int pb_ispro_read_data_transfer_list(struct pb_dev *dev, BYTE dest[16])
{
	return pDPEM_ReadDataTransferList(dev->device, dev->channel, dest);
}


static int pb_ispro_get_slave_diag(struct pb_dev *dev, const BYTE slave_address, BYTE *diag_data[], BYTE *diag_data_len)
{
	struct pb_ispro_data *data = dev->data;
	int ret;
	DPEMDEF_GetMasterDiagCon_type *res;
	DPEMDEF_GetMasterDiagReq_type *bfunc = (DPEMDEF_GetMasterDiagReq_type *)data->data;

	ret = check_mode(dev);
	if (ret)
		return ret;

	memset(data->data, 0, sizeof(data->data));

	bfunc->funcCode = DPEMDEF_FC_GET_MASTER_DIAG_REQ;
	bfunc->identifier = slave_address;
	ret = pb_ispro_send_and_wait(dev);
	if (ret)
		return ret;

	res = (DPEMDEF_GetMasterDiagCon_type *)data->data;
	if (res->length == 0)
		return PB_ISPRO_NO_DIAGNOSIS_READ;

	*diag_data = res->data;
	*diag_data_len = res->length;

	return ret;
}

static int pb_ispro_read_system_diagnostic(struct pb_dev *dev, BYTE dest[16])
{
	BOOLEAN diagstate;
	int ret = pDPEM_ReadDiagState(dev->device, dev->channel, &diagstate);

	if (ret == 0)
	{
		if (diagstate)
			ret = pDPEM_ReadSystemDiagnostic(dev->device, dev->channel, dest);
		else
			ret = PB_ISPRO_NO_DIAGNOSIS_READ;
	}

	return ret;
}

static int pb_ispro_load_slave_param(
				struct pb_dev *dev,
				const BYTE address,
				const WORD ident_number,
				const BYTE cfg_data[],
				const BYTE cfg_data_len,
				const BYTE user_prm_data[],
				const BYTE user_prm_data_len,
				const BYTE input_data_len,
				WORD *input_data_offset,
				const BYTE output_data_len,
				WORD *output_data_offset)
{
	struct pb_ispro_data *data = dev->data;
	int ret;
	DPEMDEF_LoadSlaveParReq_type *bfunc;
	BYTE len;
	BYTE DataLen;
	BYTE i;

	ret = check_for_mode(dev, STOP);
	if (ret)
		return ret;

	memset(data->data, 0, sizeof(data->data));

	bfunc = (DPEMDEF_LoadSlaveParReq_type *)data->data;
	
	bfunc->funcCode = DPEMDEF_FC_LOAD_SLAVE_PAR_REQ;
	bfunc->slaveAddress = address;  // slave address
	bfunc->addOffsetHigh = 0;
	bfunc->addOffsetLow = 0;
	
	bfunc->data[2] = 0x80;	// S1-Flag: aktiv
	bfunc->data[3] = 0;		// Slave_Typ = DP-Slave
	
	// Prm_Data
	bfunc->data[18] = 0;			// Station_status
	bfunc->data[19] = 0;			// WD_Fact_1
	bfunc->data[20] = 0;			// WD_Fact_2
	bfunc->data[21] = 11;		// min TSDR
	bfunc->data[22] = ident_number >> 8;		// Identnumber (hi)
	bfunc->data[23] = ident_number & 0xFF;	// Identnumber (lo)
	bfunc->data[24] = 0;			// Group_Ident
	
	
	// User Prm Data
	len = user_prm_data_len;
	for (i = 0; i < len; i++)
		bfunc->data[25 + i] = user_prm_data[i];
	bfunc->data[16] = 0;			// L�nge (HighByte)
	bfunc->data[17] = 9 + len;	// L�nge (LowByte)
	
	DataLen = 25 + len;
	
	// Cfg_Data
	len = cfg_data_len + 2;
	bfunc->data[DataLen] = 0;		// L�nge = len (HighByte)
	bfunc->data[DataLen + 1] = len;	//  ==//==     (LowByte)
	for (i = 2; i < len; i++)
		bfunc->data[DataLen + i] = cfg_data[i - 2];
	
	DataLen += len;
	
	// Add_Tab
	bfunc->data[DataLen] = 0;			// L�nge = 6
	bfunc->data[DataLen + 1] = 6;		//  ==//==
	
	// offset Inputdata
	*input_data_offset = data->total_input_data_offset;
	bfunc->data[DataLen + 2] = *input_data_offset >> 8;
	bfunc->data[DataLen + 3] = *input_data_offset & 0xFF;
	
	// offset Outputdata (! Inputdata and Outputdata must not overlap !)
	*output_data_offset = data->total_output_data_offset;
	bfunc->data[DataLen + 4] = *output_data_offset >> 8;
	bfunc->data[DataLen + 5] = *output_data_offset & 0xFF;
	
	// Slave_User_Data
	bfunc->data[DataLen + 6] = 0;		// L�nge = 2
	bfunc->data[DataLen + 7] = 2;		//  ==//==
	
	// LengthBytes
	DataLen += 8;
	bfunc->length = DataLen;
	bfunc->data[0] = 0;
	bfunc->data[1] = DataLen;
	
	ret = pb_ispro_send_and_wait(dev);
	if (!ret)
	{
		data->total_output_data_offset += output_data_len;
		data->total_input_data_offset += input_data_len;
	}

	return ret;
}

static int pb_ispro_change_slave_param(
				struct pb_dev *dev,
				const BYTE address,
				const BYTE action,
				const BYTE data_len,
				const BYTE prm_data[]
				)
{
	struct pb_ispro_data *data = dev->data;
	int ret;
	DPEMDEF_ChangeSlaveParReq_type *bfunc;

	ret = check_for_mode(dev, STOP);
	if (ret)
		return ret;

	memset(data->data, 0, sizeof(data->data));

	bfunc = (DPEMDEF_ChangeSlaveParReq_type *)data->data;
	
	bfunc->funcCode = DPEMDEF_FC_CHANGE_SLAVE_PAR_REQ;
	bfunc->slaveAddress = address;
	
	if (action == 0x40 )
	{
		if (prm_data == NULL)
			return PB_NULL_POINTER;
		bfunc->length = data_len;
		memcpy(bfunc->data, prm_data, data_len < 244 ? data_len : 244);
	}

	bfunc->paraIdentifier = action;
	
	ret = pb_ispro_send_and_wait(dev);

	return ret;
}

static int pb_ispro_connect(struct pb_dev *dev,
		unsigned char palinkaddr, unsigned char slaveaddr,
		unsigned short sendTimeOut, pb_slave_t *slave)
{
	struct pb_ispro_data *data = dev->data;
	int i, ret;

	ret = check_mode(dev);
	if (ret)
		return ret;

	for (i = 0; i < CONNECTIONS_MAX; i++)
	{
		if (!data->connections[i].used)
		{
			data->connections[i].used = 1;
			break;
		}
	}

	if (i == CONNECTIONS_MAX)
		return PB_ISPRO_TO_MANY_CONNECTIONS;

	data->req = NULL;
	data->conn = &(data->connections[i]);
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[ 0] = DPEMDEF_FC_MSAC2_REQ;		/* funcCode */
	data->data[ 1] = i;				/* cRef */
	data->data[ 3] = slaveaddr;			/* remAddr */
	data->data[12] = DPEMDEF_FN_MSAC2_INITIATE;	/* funcNum */
	data->data[16] = sendTimeOut >> 8;		/* sendTimeOutHigh */
	data->data[17] = (BYTE) sendTimeOut;		/* sendTimeOutLow */
	data->data[18] = 1;				/* featSupp1 */
	data->data[19] = 0;				/* featSupp2 */
	/* addAddrParam[232] */
	data->data[24] = 0;				/* addAddrParam S-Type */
	data->data[25] = 2;				/* addAddrParam S-Len */
	data->data[26] = 0;				/* addAddrParam D-Type */
	data->data[27] = 2;				/* addAddrParam D-Len */

	if (palinkaddr)
	{
		data->data[ 3] = palinkaddr;		/* remAddr */
		data->data[26] = 1;			/* addAddrParam D-Type */
		data->data[27] += 7;			/* addAddrParam D-Len */
		data->data[38] = slaveaddr;
	}

	ret = pb_ispro_send_and_wait(dev);
	if (ret == 0)
		*slave = data->conn;
	else
		data->conn->used = 0;

	return ret;
}

static int
lookup_conn(struct pb_ispro_data *data, struct pb_ispro_conn *conn)
{
	int i;

	for (i = 0; i < CONNECTIONS_MAX; i++)
	{
		if (&(data->connections[i]) == conn)
			return i;
	}

	return -1;
}

static int pb_ispro_disconnect(struct pb_dev *dev, pb_slave_t slave)
{
	struct pb_ispro_data *data = dev->data;
	struct pb_ispro_conn *conn = slave;
	int i, ret;

	i = lookup_conn(data, conn);
	if (i == -1)
		return PB_INVALID_HANDLE;

	data->req = NULL;
	data->conn = conn;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[ 0] = DPEMDEF_FC_MSAC2_REQ;
	data->data[ 1] = i;
	data->data[12] = DPEMDEF_FN_MSAC2_ABORT;

	ret = pb_ispro_send_and_wait(dev);

	/* free connection */
	conn->used = 0;

	return ret;
}

static int pb_ispro_read(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *req)
{
	struct pb_ispro_data *data = dev->data;
	struct pb_ispro_conn *conn = slave;
	int i, ret;

	ret = check_mode(dev);
	if (ret)
		return ret;

	i = lookup_conn(data, conn);
	if (i == -1)
		return PB_INVALID_HANDLE;

	if (req->datalen > conn->maxlength)
		req->datalen = conn->maxlength;

	data->req = req;
	data->conn = conn;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[ 0] = DPEMDEF_FC_MSAC2_REQ;
	data->data[ 1] = i;
	data->data[12] = DPEMDEF_FN_MSAC2_READ;
	data->data[13] = req->slot;
	data->data[14] = req->index;
	data->data[15] = req->datalen;

	req->datalen = 0;
	ret = pb_ispro_send_and_wait(dev);

	return ret;
}

static int pb_ispro_write(struct pb_dev *dev, pb_slave_t slave,
		struct pb_request *req)
{
	struct pb_ispro_data *data = dev->data;
	struct pb_ispro_conn *conn = slave;
	int i, ret;

	ret = check_mode(dev);
	if (ret)
		return ret;

	i = lookup_conn(data, conn);
	if (i == -1)
		return PB_INVALID_HANDLE;

	data->req = req;
	data->conn = conn;
	data->livelist = NULL;

	memset(data->data, 0, sizeof(data->data));
	data->data[ 0] = DPEMDEF_FC_MSAC2_REQ;
	data->data[ 1] = i;
	data->data[12] = DPEMDEF_FN_MSAC2_WRITE;
	data->data[13] = req->slot;
	data->data[14] = req->index;
	data->data[15] = req->datalen;
	memcpy(data->data+16, req->data, req->datalen);
	req->datalen = 0;

	ret = pb_ispro_send_and_wait(dev);

	return ret;
}



#ifndef _
# define _(x) x
#endif

static const char *init_dll_errstr(int code)
{
	const char *msg = NULL;

	switch (code)
	{
		default:
		case DLL_OS_ERROR:
		case DLL_INIT_ERROR:
			msg = _("ispro: initialization failed");
			break;
		case DLL_DEVICE_ERROR:
			msg = _("ispro: device initialization failed");
			break;
	}

	assert(msg);
	return msg;
}

static const char *watchmailbox_errstr(int code)
{
	const char *msg = NULL;

	switch (code)
	{
		case WATCH_INVALID_DEVICE_NUMBER:
			msg = _("ispro: invalid device number");
			break;
		case WATCH_INVALID_CHANNEL_NUMBER:
			msg = _("ispro: invalid channel number");
			break;
		case WATCH_CHANNEL_NOT_INITIALIZED:
			msg = _("ispro: channel not initialized");
			break;
		case WATCH_THREAD_ALREADY_STARTED:
			msg = _("ispro: thread already started");
			break;
		case WATCH_THREAD_NOT_STARTED:
			msg = _("ispro: thread not started");
			break;
		default:
			msg = _("ispro: unknown mailbox error");
			break;
	}

	assert(msg);
	return msg;
}

static const char *module_errstr(int code)
{
	const char *msg = NULL;

	switch (code)
	{
		case MODUL_INVALID_DEVICE_NUMBER:
			msg = _("ispro: invalid device number");
			break;
		case MODUL_INVALID_CHANNEL_NUMBER:
			msg = _("ispro: invalid channel number");
			break;
		case MODUL_CHANNEL_NOT_INITIALIZED:
			msg = _("ispro: channel not initialized");
			break;
		case MODUL_WRONG_PROTOCOL:
			msg = _("ispro: wrong protocol");
			break;
		case MODUL_DRIVER_ERROR:
			msg = _("ispro: driver error");
			break;
		case MODUL_COMMANDBOX_NOT_FREE:
			msg = _("ispro: no free command box available");
			break;
		case MODUL_REPORTBOX_NO_DATA:
			msg = _("ispro: report box is empty");
			break;
		case MODUL_UNKNOWN_FUNCTION:
			msg = _("ispro: function code incorrect");
			break;
		case MODUL_RESOURCE_RANGE_VIOLATED:
			msg = _("ispro: resource range violated");
			break;
		default:
			msg = _("ispro: unknown module error");
			break;
	}

	assert(msg);
	return msg;
}

static const char *pb_ispro_errstr(int result, const char **descr)
{
	const char *msg = NULL;

	if (result & (PB_ISPRO_INIT_DLL_ERROR & ~PB_ISPRO_ERROR))
	{
		msg = init_dll_errstr((result & 0xff00) >> 8);
	}
	else if (result & (PB_ISPRO_WATCHMAILBOX_ERROR & ~PB_ISPRO_ERROR))
	{
		msg = watchmailbox_errstr((result & 0xff00) >> 8);
	}
	else if (result & (PB_ISPRO_MODULE_ERROR & ~PB_ISPRO_ERROR))
	{
		msg = module_errstr((result & 0xff00) >> 8);
	}
	else
	{
		switch (result)
		{
			case PB_ISPRO_TO_MANY_CONNECTIONS:
				msg = _("to many connections");
				break;
			case PB_ISPRO_NOT_ONLINE:
				msg = _("device not online");
				break;
			case PB_ISPRO_CHANNEL_IN_USE:
				msg = _("channel already in use");
				break;
			case PB_ISPRO_LOADING_DLL:
				msg = _("error loading ispro library");
				break;
			case PB_ISPRO_DEVICE_NOT_ATTACHED:
				msg = _("device not attached");
				break;
			case PB_ISPRO_NO_DIAGNOSIS_READ:
				msg  =_("no diagnosis could be read");
				break;
			case PB_ISPRO_MODE_MISMATCH:
				msg = _("wrong mode");
				break;
			default:
				msg = _("unknown error in ispro driver");
				break;
		}
	}

	assert(msg);
	return msg;
}


