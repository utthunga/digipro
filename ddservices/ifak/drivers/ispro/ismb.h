

#ifndef __ISMBDRV_H__
#define __ISMBDRV_H__


#include "ismbdef.h"


/***************************************************************************/
/* Includes aller ben�tigten Protokolle f�r die Definition von Param_type, */
/* die Schalter SUPPORTED_XXX werden in isMBdef.h definiert                */
/***************************************************************************/

#ifdef SUPPORTED_DPEM
#include "dpem.h"
#endif

#ifdef SUPPORTED_FMS
#include "fmsm.h"
#endif

#ifdef SUPPORTED_LIST
#include "list.h"
#endif

#ifdef SUPPORTED_DPS
#include "dps.h"
#endif


/***********************************************/
/* Datentypen zur Nutzung der Funktion InitDll */
/***********************************************/

typedef union
{
	#ifdef SUPPORTED_DPEM
	dpInOutAreaSize_type dpInOutAreaSize;
	#endif

	#ifdef SUPPORTED_LIST
	dpInAreaSize_type dpInAreaSize;
	#endif

} Param_type;


typedef struct
{
	ULONG DPR_Address;      /* obsolet, auf 0 setzen */
	ULONG DPR_Length;       /* obsolet, auf 0 setzen */
	ULONG PortAddress;      /* obsolet, auf 0 setzen */
	struct
	{
		BYTE  ProtocolID;     /* gew�nschtes Protokoll pro Kanal            */
		Param_type Parameter; /* je nach Protokoll zu �bergebende Parameter */
	} Protocol[NUMBER_OF_PHYS_CHAN];
} initDeviceData_type;

/* davon brauchen wir MAX_NR_OF_CARDS (= 16) an der Zahl */
typedef initDeviceData_type initData_type[MAX_NR_OF_CARDS];


/* R�ckgabefelder f�r die Fehlercodes */
typedef BYTE DeviceError_type[MAX_NR_OF_CARDS];   // Fehlercodes des Ger�tes oder DPRs
typedef BYTE ChannelError_type[MAX_NR_OF_CARDS];  // Fehlercodes f�r die Kan�le jedes Devices


/********************************************************/
/* Initialisierung der Schnittstellen,                  */
/* DeviceMask: Bit 0 = Device 0, Bit 1 = Device 1  usw. */
/********************************************************/

#ifdef __cplusplus
extern "C" {
BYTE __cdecl InitDll(initData_type initData,
					 WORD DeviceMask,
					 DeviceError_type* devError_ptr = NULL,
					 ChannelError_type* chError_ptr = NULL);
}
#else
BYTE __cdecl InitDll(initData_type initData,
					 WORD DeviceMask,
					 DeviceError_type* devError_ptr,
					 ChannelError_type* chError_ptr);
#endif

#ifdef __cplusplus
extern "C" {
#endif
#ifndef PORTED_LINUX
/**********************************************************/
/* Funktion zur Nutzung einer Karte von Visual Basic aus, */
/* diese Funktion initialisiert nur eine einzelne Karte.  */
/**********************************************************/
BYTE __stdcall isPRO_InitDevice(BYTE device, BYTE ProtocolID[4], WORD dpInOutAreaSize[4]);
#endif

#ifdef __cplusplus
}
#endif


/********************************************************/
/* Macros und Funktionen zur Nutzung des MailboxThreads */
/********************************************************/

/* Macros f�r R�ckrufe auf nichtstat. Memberfunktionen einer Klasse */
#ifndef PORTED_LINUX

#define LinkTo(x)  x##LINK

#define REGISTER_CALLBACKFUNC(className, entryPoint)				\
	void entryPoint(BYTE device, BYTE channel);						\
	static void LinkTo(entryPoint)(BYTE device,						\
								   BYTE channel,					\
								   void* pContext)					\
	{																\
		if (pContext == NULL)										\
			return;													\
		className* pInstance = static_cast<className*>(pContext);	\
		pInstance->entryPoint(device, channel);						\
	}

#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Zeigertyp auf die R�ckruf-Funktion */
typedef void (__cdecl *pMailboxNotification)(BYTE, BYTE, void*);


/* Prototyp f�r R�ckrufe */
void __cdecl MailboxNotification(BYTE device,
								 BYTE channel,
								 void* pContext);

/* Beenden des Threads */
BYTE __cdecl StopWatchThread(BYTE device,
							 BYTE channel);

#ifdef __cplusplus
}
#endif


/* Starten des Threads */
#ifdef __cplusplus
extern "C" {
BYTE __cdecl WatchMailbox(BYTE device,
						  BYTE channel,
						  pMailboxNotification CallbackFunc,
						  void* pContext = NULL,
						  WORD delaytime = 10);
}
#else
BYTE __cdecl WatchMailbox(BYTE device,
						  BYTE channel,
						  pMailboxNotification CallbackFunc,
						  void* pContext,
						  WORD delaytime);
#endif


/****************************************/
/* Zur�cksetzen der Karte / des Ger�tes */
/****************************************/

#ifdef __cplusplus
extern "C" {
BYTE __cdecl ResetDevice(BYTE device, ULONG PortAddress = 0);
}
#else
BYTE __cdecl ResetDevice(BYTE device, ULONG PortAddress);
#endif

#ifdef __cplusplus
extern "C" {
#endif
#ifndef PORTED_LINUX
/**********************************************************/
/* Funktion zur Nutzung einer Karte von Visual Basic aus. */
/**********************************************************/
BYTE __stdcall isPRO_CloseDevice(BYTE device);
#endif

#ifdef __cplusplus
}
#endif



/*******************************/
/* Plug- und Play - Funktionen */
/*******************************/
#ifdef __cplusplus
extern "C" {
#endif

BYTE __cdecl isPRO_GetDeviceInfo(BYTE device, DeviceInfo_type *pInfo);
BYTE __cdecl isPRO_AssignDevice(BYTE device, BYTE DeviceType, DWORD DeviceID);
BYTE __cdecl isPRO_AttachedDevices(BYTE flag, DeviceInfo_type* pInfo);

#ifndef PORTED_LINUX
BYTE __stdcall isPROvb_GetDeviceInfo(BYTE device, DeviceInfo_type *pInfo);
BYTE __stdcall isPROvb_AssignDevice(BYTE device, BYTE DeviceType, DWORD DeviceID);
BYTE __stdcall isPROvb_AttachedDevices(BYTE flag, DeviceInfo_type* pInfo);
#endif

#ifdef __cplusplus
}
#endif

#endif // __ISMBDRV_H__
