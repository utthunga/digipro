
#ifndef __LIST_H__
#define __LIST_H__


typedef UNSIGNED16 dpInAreaSize_type;

#ifndef PORTED_LINUX
#pragma pack(push, 1)
#endif
typedef struct 
{
	UNSIGNED32 timeTick;
	BYTE  len;
	BYTE  data[255];
} LIST_SlaveStruct;
#ifndef PORTED_LINUX
#pragma pack(pop)
#endif


/***********************************/
/*       F U N C T I O N E N       */
/***********************************/

#ifndef PORTED_LINUX
#pragma pack(push, 8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Verwaltung des Befehlsbereiches */
BYTE __cdecl LIST_WriteCommand(
					BYTE  device,
					BYTE  channel,
					BYTE* source_ptr,
					BYTE  len
					);
BYTE __cdecl LIST_GetReport(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr
					);


/* Verwaltung des Datenbereiches  */
BYTE __cdecl LIST_ReadInputData(
					BYTE       device,
					BYTE       channel,
					LIST_SlaveStruct* dest_ptr,
					UNSIGNED16 offset,
					BYTE       slaveAdd
					);


#ifdef __cplusplus
}
#endif

#ifndef PORTED_LINUX
#pragma pack(pop)
#endif

#endif // __LIST_H__
