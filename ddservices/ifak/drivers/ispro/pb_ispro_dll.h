
/* 
 * $Id: pb_ispro_dll.h,v 1.6 2006/02/08 19:37:52 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_ispro_dll_h
#define _pb_ispro_dll_h

#ifndef WIN32
# define PORTED_LINUX
#endif

#include "ismb.h"
#include "dpem.h"


int IsProOpenDLL(void);
void IsProCloseDLL(void);


/* ismb interface types */

typedef BYTE (__cdecl *InitDll_type)(initData_type initData,
				     WORD DeviceMask,
				     DeviceError_type* devError_ptr,
				     ChannelError_type* chError_ptr);

typedef BYTE (__cdecl *StopWatchThread_type)(BYTE device, BYTE channel);

typedef BOOLEAN (__cdecl *WatchMailbox_type)(BYTE device, 
					     BYTE channel, 
					     pMailboxNotification CallbackFunc,
					     void *pContext ,	// = NULL
					     WORD delaytime );	// = 10

typedef BYTE (__cdecl *ResetDevice_type)(BYTE device);

typedef BYTE (__cdecl *isPRO_GetDeviceInfo_type)(BYTE device, DeviceInfo_type *pInfo);


/* DPEM interface types */

typedef BYTE (__cdecl *DPEM_WriteCommand_type)(
					BYTE  device,
					BYTE  channel,
					DPEMDEF_Box_type CommandBox);

typedef BYTE (__cdecl *DPEM_GetReport_type)(
					BYTE  device,
					BYTE  channel,
					DPEMDEF_Box_type ReportBox);

typedef BYTE (__cdecl *DPEM_WriteOutputData_type)(
					BYTE device, 
					BYTE channel,
					BYTE *src_ptr,
					UNSIGNED16 offset,
					BYTE len,
					BYTE slaveAdd);

typedef BYTE (__cdecl *DPEM_ReadInputData_type)(
					BYTE device, 
					BYTE channel,
					BYTE *dest_ptr,
					UNSIGNED16 offset,
					BYTE len,
					BYTE slaveAdd);

typedef BYTE (__cdecl *DPEM_StationOnline_type)(
					BYTE  device,
					BYTE  channel,
					BOOLEAN *online);


typedef BYTE (__cdecl *DPEM_WriteRemoteAccessFlag_type)(
					BYTE  device,
					BYTE  channel,
					BYTE  remAccessFlag);

typedef BYTE (__cdecl *DPEM_ReadDataTransferList_type)(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr);

typedef BYTE (__cdecl *DPEM_ReadSystemDiagnostic_type)(
					BYTE  device,
					BYTE  channel,
					BYTE* dest_ptr);

typedef BYTE (__cdecl *DPEM_ReadDiagState_type)(
					BYTE  device,
					BYTE  channel,
					BOOLEAN* diagstate);


/* the entry points */

extern InitDll_type			pInitDll;
extern StopWatchThread_type		pStopWatchThread;
extern WatchMailbox_type		pWatchMailbox;
extern ResetDevice_type			pResetDevice;
extern isPRO_GetDeviceInfo_type		pGetDeviceInfo;

extern DPEM_WriteCommand_type		pDPEM_WriteCommand;
extern DPEM_GetReport_type		pDPEM_GetReport;
extern DPEM_WriteOutputData_type	pDPEM_WriteOutputData;
extern DPEM_ReadInputData_type		pDPEM_ReadInputData;
extern DPEM_StationOnline_type		pDPEM_StationOnline;

extern DPEM_ReadDataTransferList_type	pDPEM_ReadDataTransferList;
extern DPEM_ReadSystemDiagnostic_type	pDPEM_ReadSystemDiagnostic;
extern DPEM_ReadDiagState_type		pDPEM_ReadDiagState;


#endif /* _pb_ispro_dll_h */
