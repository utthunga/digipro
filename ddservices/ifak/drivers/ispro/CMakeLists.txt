#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    Compiler Options #######################################################
#############################################################################
add_compile_options(${FLUKE_IGNORE_COMPILE_WARNINGS})

#############################################################################
#    include the source files ###############################################
#############################################################################
set (ISPRO_SRC	${CMAKE_CURRENT_SOURCE_DIR}/pb_ispro.c
                ${CMAKE_CURRENT_SOURCE_DIR}/pb_ispro_mailbox.c
                ${CMAKE_CURRENT_SOURCE_DIR}/pb_ispro_dll.c)

#############################################################################
#    include the header files ###############################################
#############################################################################
include_directories (${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/include
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/profibus
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/edd
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/ispro
                     ${CMAKE_PROSTAR_SRC_DIR}/clogger)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(ispro OBJECT ${ISPRO_SRC})
