
/* 
 * $Id: pb_ispro_mailbox.h,v 1.2 2005/11/22 15:33:50 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 2005 Matthias Neumann <matthias.neumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_ispro_mailbox_h
#define _pb_ispro_mailbox_h

int pb_ispro_send_and_wait(struct pb_dev *dev);

int CheckDevice(struct pb_dev *dev);
int InitDevice(struct pb_dev *dev);
int CloseDevice(struct pb_dev *dev);

int check_online(struct pb_dev *dev);

#endif /* _pb_ispro_mailbox_h */
