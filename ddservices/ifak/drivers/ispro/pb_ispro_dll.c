
/* 
 * $Id: pb_ispro_dll.c,v 1.8 2006/02/08 19:37:52 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdlib.h>

#include "pb_ispro_data.h"
#include "pb_ispro_dll.h"

static size_t refs = 0;

#ifdef WIN32
static HINSTANCE handle;
#define LIBRARY "isMBDrv.dll"
#define library_load(path)		LoadLibrary(path)
#define library_close(handle)		FreeLibrary(handle)
#define library_symbol(handle, name)	GetProcAddress(handle, name)
#else
void *handle;
#include <dlfcn.h>
#define LIBRARY "isPROlib.so.1"
#define library_load(path)		dlopen(path, RTLD_LAZY)
#define library_close(handle)		dlclose(handle)
#define library_symbol(handle, name)	dlsym(handle, name)
#endif

InitDll_type				pInitDll;
StopWatchThread_type			pStopWatchThread;
WatchMailbox_type			pWatchMailbox;
ResetDevice_type			pResetDevice;
isPRO_GetDeviceInfo_type		pGetDeviceInfo;

DPEM_WriteCommand_type			pDPEM_WriteCommand;
DPEM_GetReport_type			pDPEM_GetReport;
DPEM_WriteOutputData_type		pDPEM_WriteOutputData;
DPEM_ReadInputData_type			pDPEM_ReadInputData;
DPEM_StationOnline_type			pDPEM_StationOnline;

DPEM_ReadDataTransferList_type		pDPEM_ReadDataTransferList;
DPEM_ReadSystemDiagnostic_type		pDPEM_ReadSystemDiagnostic;
DPEM_ReadDiagState_type			pDPEM_ReadDiagState;

int
IsProOpenDLL(void)
{
	int ret = PB_ISPRO_LOADING_DLL;
	
	if (refs == 0)
	{
		handle = library_load(LIBRARY);
		if (handle)
		{
			pInitDll              = (InitDll_type)               library_symbol(handle, "InitDll");
			pStopWatchThread      = (StopWatchThread_type)       library_symbol(handle, "StopWatchThread");
			pWatchMailbox         = (WatchMailbox_type)          library_symbol(handle, "WatchMailbox");
			pResetDevice          = (ResetDevice_type)           library_symbol(handle, "ResetDevice");
			pGetDeviceInfo        = (isPRO_GetDeviceInfo_type)   library_symbol(handle, "isPRO_GetDeviceInfo");

			pDPEM_WriteCommand    = (DPEM_WriteCommand_type)     library_symbol(handle, "DPEM_WriteCommand");
			pDPEM_GetReport       = (DPEM_GetReport_type)        library_symbol(handle, "DPEM_GetReport");
			pDPEM_WriteOutputData = (DPEM_WriteOutputData_type)  library_symbol(handle, "DPEM_WriteOutputData");
			pDPEM_ReadInputData   = (DPEM_ReadInputData_type)    library_symbol(handle, "DPEM_ReadInputData");
			pDPEM_StationOnline   = (DPEM_StationOnline_type)    library_symbol(handle, "DPEM_StationOnline");

			pDPEM_ReadDataTransferList	= (DPEM_ReadDataTransferList_type)	library_symbol(handle, "DPEM_ReadDataTransferList");
			pDPEM_ReadSystemDiagnostic	= (DPEM_ReadSystemDiagnostic_type)	library_symbol(handle, "DPEM_ReadSystemDiagnostic");
			pDPEM_ReadDiagState			= (DPEM_ReadDiagState_type)			library_symbol(handle, "DPEM_ReadDiagState");

			if (
			       pInitDll
			    && pStopWatchThread
			    && pWatchMailbox
			    && pResetDevice
			    && pGetDeviceInfo

			    && pDPEM_WriteCommand
			    && pDPEM_GetReport
			    && pDPEM_WriteOutputData
			    && pDPEM_ReadInputData
			    && pDPEM_StationOnline

				&& pDPEM_ReadDataTransferList
				&& pDPEM_ReadSystemDiagnostic
				&& pDPEM_ReadDiagState
			)
			{
				ret = 0;
				refs++;
			}
			else
				library_close(handle);
		}
	}
	else
	{
		ret = 0;
		refs++;
	}

	return ret;
}

void
IsProCloseDLL(void)
{
	assert(refs);

	if (--refs == 0)
		library_close(handle);
}
