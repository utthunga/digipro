
/* 
 * $Id: pb_ispro_data.h,v 1.8 2009/02/12 16:27:15 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _pb_ispro_data_h
#define _pb_ispro_data_h

#include "condition.h"
#include "driver_interface.h"

enum pb_ispro_error
{
	PB_ISPRO_ERROR               = 64,

	PB_ISPRO_TO_MANY_CONNECTIONS = PB_ISPRO_ERROR|(1 << 8),
	PB_ISPRO_NOT_ONLINE          = PB_ISPRO_ERROR|(2 << 8),
	PB_ISPRO_CHANNEL_IN_USE      = PB_ISPRO_ERROR|(3 << 8),
	PB_ISPRO_LOADING_DLL         = PB_ISPRO_ERROR|(4 << 8),
	PB_ISPRO_DEVICE_NOT_ATTACHED = PB_ISPRO_ERROR|(5 << 8),
	PB_ISPRO_NO_DIAGNOSIS_READ   = PB_ISPRO_ERROR|(6 << 8),
	PB_ISPRO_MODE_MISMATCH       = PB_ISPRO_ERROR|(7 << 8),

	PB_ISPRO_INIT_DLL_ERROR      = PB_ISPRO_ERROR|0x10000,
	PB_ISPRO_WATCHMAILBOX_ERROR  = PB_ISPRO_ERROR|0x20000,
	PB_ISPRO_MODULE_ERROR        = PB_ISPRO_ERROR|0x40000,
};

struct pb_ispro_conn
{
	int used;
	int connected;
	unsigned char maxlength;
};

struct pb_ispro_data
{
	struct pb_dev *dev;
	struct pb_ispro_data *next;
	int channels;

	/* read/write synchronization */
	struct condition *handle; /* wait condition */
	int request_pending; /* non-zero if an application request is pending */

	/* mode handling */
	enum pb_mode mode; /* mode requested by the application */
	int mode_changed; /* set to non-zero if dpem indicate mode change */

	/* request inputs/outputs */
	struct pb_request *req;
	struct pb_ispro_conn *conn;
	struct pb_livelist_info *livelist;
	int result;

	/* input and output offsets */
	unsigned short total_input_data_offset;
	unsigned short total_output_data_offset;

	/* the command box */
	unsigned char data[256];

#define CONNECTIONS_MAX 32
	struct pb_ispro_conn connections[CONNECTIONS_MAX];
};

#endif /* _pb_ispro_data_h */
