
#ifndef __DPS_H__
#define __DPS_H__


/* Kommunikations-Events */
#define USERDPSDPR_NO_EVENT        0x00
#define USERDPSDPR_FAULT           0x01
#define USERDPSDPR_TIMEOUT         0x02
#define USERDPSDPR_ADD_CHG_DATA    0x04
#define USERDPSDPR_NEW_PRM_DATA    0x08
#define USERDPSDPR_OUTPUT_DATA     0x10


/***********************************/
/*       S T R U K T U R E N       */
/***********************************/

#ifndef PORTED_LINUX
#pragma pack(push, 1)
#endif

/* Datentyp fuer die Kommunikationsparameter */
typedef PKD struct
{
	BYTE     stationAdd;
	BYTE     bitrate;
	BYTE     defaultMinTsdr;
	BYTE     identNumberH;
	BYTE     identNumberL;
	BOOLEAN  noAddressChg;
	BOOLEAN  syncSupported;
	BOOLEAN  freezeSupported;
} DPS_ComPar_type;

/* Datentyp fuer die Diagnosedaten */
typedef PKD struct
{
	BOOLEAN  extDiag;
	BOOLEAN  statDiag;
	BOOLEAN  extDiagOverflow;
	BYTE     extDiagLength;
	BYTE     extDiagData [244 - 7];
} DPS_DiagData_type;

#ifndef PORTED_LINUX
#pragma pack(pop)
#endif


/***********************************/
/*       F U N C T I O N E N       */
/***********************************/

#ifndef PORTED_LINUX
#pragma pack(push, 8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Ermitteln eines neuen Kommunikations-Events */
BYTE __cdecl DPS_GetNewCommEvent(
					BYTE  device,
					BYTE  channel,
					BYTE *event
					);

/* Lesen der Ausgabedaten */
BYTE __cdecl DPS_ReadOutputData(
					BYTE  device,
					BYTE  channel,
					BYTE *dest_ptr,
					BYTE *len
					);

/* Lesen der Parametrierdaten */
BYTE __cdecl DPS_ReadPrmData(
					BYTE  device,
					BYTE  channel,
					BYTE *dest_ptr,
					BYTE *len
					);

/* Lesen einer neuen Stationsadresse und anwenderspezifischer Daten */
BYTE __cdecl DPS_ReadStationAddress(
					BYTE  device,
					BYTE  channel,
					BYTE *dest_ptr,
					BYTE *len,
					BYTE *addr
					);

/* Schreiben der Eingabedaten */
BYTE __cdecl DPS_WriteInputData(
					BYTE  device,
					BYTE  channel,
					BYTE *src_ptr
					);

/* Schreiben der Kommunikationsparameter */
BYTE __cdecl DPS_WriteComPar(
					BYTE  device,
					BYTE  channel,
					DPS_ComPar_type *src_ptr
					);

/* Schreiben der Konfigurationsdaten */
BYTE __cdecl DPS_WriteCfgData(
					BYTE  device,
					BYTE  channel,
					BYTE *src_ptr,
					BYTE  len
					);

/* Schreiben der Diagnosedaten */
BYTE __cdecl DPS_WriteDiagData(
					BYTE  device,
					BYTE  channel,
					DPS_DiagData_type *src_ptr
					);


#ifdef __cplusplus
}
#endif

#ifndef PORTED_LINUX
#pragma pack(pop)
#endif

#endif // __DPS_H__
