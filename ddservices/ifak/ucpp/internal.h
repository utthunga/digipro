
/*
 * $Id: internal.h,v 1.4 2008/05/15 11:30:51 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _ucpp_internal_h
#define _ucpp_internal_h

// C stdlib
#include <stdlib.h>

// own header
#include "cpp.h"

#ifdef _MSC_VER
#pragma warning (disable: 4018 4146 4244)
#endif


/* These declarations are used only internally by ucpp */

namespace ucpp
{

static inline void
die(void)
{
	abort();
}

static inline void *
incmem(void *m, size_t x, size_t nx)
{
	void *nm;
	
	if (x > nx)
		x = nx;
	
	nm = new char[nx];
	if (!nm) throw 0;
	
	memcpy(nm, m, x);
	
	return nm;
}

/*
 * this macro adds the object obj at the end of the array list, handling
 * memory allocation when needed; ptr contains the number of elements in
 * the array, and memg is the granularity of memory allocations (a power
 * of 2 is recommanded, for optimization reasons).
 *
 * list and ptr may be updated, and thus need to be lvalues.
 */
#define aol(list, ptr, obj, memg, type)	do { \
		if (((ptr) % (memg)) == 0) { \
			if ((ptr) != 0) { \
				type *nm = (type *) incmem((list), (ptr) * sizeof(obj), \
						((ptr) + (memg)) * sizeof(obj)); \
				delete [] (list); \
				(list) = nm; \
			} else { \
				(list) = new type[memg]; \
			} \
		} \
		(list)[(ptr)++] = (obj); \
	} while (0)

/*
 * this macro adds the object obj at the end of the array list, doubling
 * the size of list when needed; as for aol(), ptr and list must be
 * lvalues, and so must be llng
 */
#define wan(list, ptr, obj, llng, type)	do { \
		if ((ptr) == (llng)) { \
			(llng) += (llng); \
			type *nm = (type *) incmem((list), (ptr) * sizeof(obj), \
					(llng) * sizeof(obj)); \
			delete [] (list); \
			(list) = nm; \
		} \
		(list)[(ptr)++] = (obj); \
	} while (0)

/*
 * This macro tells whether the name field of a given token type is
 * relevant, or not. Irrelevant name field means that it might point
 * to outerspace.
 */
#ifdef SEMPER_FIDELIS
inline bool STRING_TOKEN(int x) { return (x == cpp::NONE || (x >= cpp::COMMENT && x <= cpp::CHAR)); }
#else
inline bool STRING_TOKEN(int x) { return (x >= cpp::NUMBER && x <= cpp::CHAR); }
#endif

/*
 * S_TOKEN(x)	checks whether x is a token type with an embedded string
 * ttMWS(x)	checks whether x is macro whitespace (space, comment...)
 * ttWHI(x)	checks whether x is whitespace (MWS or newline)
 */
inline bool S_TOKEN(int x) { return STRING_TOKEN(x); }
inline bool ttMWS(int x) { return (x == cpp::NONE || x == cpp::COMMENT || x == cpp::OPT_NONE); }
inline bool ttWHI(int x) { return (ttMWS(x) || x == cpp::NEWLINE); }

} /* namespace ucpp */

#endif /* _ucpp_internal_h */
