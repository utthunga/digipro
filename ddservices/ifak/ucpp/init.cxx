
/*
 * $Id: init.cxx,v 1.3 2006/07/30 18:14:35 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <time.h>
#include "cpp.h"
#include "internal.h"


namespace ucpp
{

const int cpp::CPPERR_NEST			=  900;
const int cpp::CPPERR_EOF			= 1000;

/*
 * Flags for struct lexer_state
 */

/* warning flags */
const unsigned long cpp::WARN_STANDARD		= 0x000001UL;	/* emit standard warnings */
const unsigned long cpp::WARN_ANNOYING		= 0x000002UL;	/* emit annoying warnings */
const unsigned long cpp::WARN_TRIGRAPHS		= 0x000004UL;	/* warn when trigraphs are used */
const unsigned long cpp::WARN_TRIGRAPHS_MORE	= 0x000008UL;	/* extra-warn for trigraphs */
const unsigned long cpp::WARN_PRAGMA		= 0x000010UL;	/* warn for pragmas in non-lexer mode */

/* error flags */
const unsigned long cpp::FAIL_SHARP		= 0x000020UL;	/* emit errors on rogue '#' */
const unsigned long cpp::CCHARSET		= 0x000040UL;	/* emit errors on non-C characters */

/* emission flags */
const unsigned long cpp::DISCARD_COMMENTS	= 0x000080UL;	/* discard comments from text output */
const unsigned long cpp::CPLUSPLUS_COMMENTS	= 0x000100UL;	/* understand C++-like comments */
const unsigned long cpp::LINE_NUM		= 0x000200UL;	/* emit #line directives in output */
const unsigned long cpp::GCC_LINE_NUM		= 0x000400UL;	/* same as #line, with gcc-syntax */

/* language flags */
const unsigned long cpp::HANDLE_ASSERTIONS	= 0x000800UL;	/* understand assertions */
const unsigned long cpp::HANDLE_PRAGMA		= 0x001000UL;	/* emit PRAGMA tokens in lexer mode */
const unsigned long cpp::MACRO_VAARG		= 0x002000UL;	/* understand macros with '...' */
const unsigned long cpp::UTF8_SOURCE		= 0x004000UL;	/* identifiers are in UTF8 encoding */
const unsigned long cpp::HANDLE_TRIGRAPHS	= 0x008000UL;	/* handle trigraphs */

/* global ucpp behaviour */
const unsigned long cpp::LEXER			= 0x010000UL;	/* behave as a lexer */
const unsigned long cpp::KEEP_OUTPUT		= 0x020000UL;	/* emit the result of preprocessing */
const unsigned long cpp::COPY_LINE		= 0x040000UL;	/* make a copy of the parsed line */
const unsigned long cpp::PDM_MODE		= 0x080000UL;	/* try to be simatic PDM compatible */
const unsigned long cpp::APPEND_NEWLINE		= 0x100000UL;	/* implicitly append '\n' to every input file */

/* internal flags */
const unsigned long cpp::READ_AGAIN		= 0x200000UL;	/* emit again the last token */
const unsigned long cpp::TEXT_OUTPUT		= 0x400000UL;	/* output text */

/* default configurations */
const unsigned long cpp::DEFAULT_CPP_FLAGS	= _DEFAULT_CPP_FLAGS;
const unsigned long cpp::DEFAULT_LEXER_FLAGS	= _DEFAULT_LEXER_FLAGS;


cpp::cpp(void)
{
	struct tm *ct;
	time_t t;
	
	/*
	 * init_cppm() initializes static tables inside ucpp. It needs not be
	 * called more than once.
	 */
	init_cppm();
	
	include_path = 0;
	include_path_nb = 0;
	
	no_special_macros = 0;
	c99_compliant = 1;
	c99_hosted = 1;
	emit_dependencies = 0;
	emit_defines = 0;
	emit_assertions = 0;
	emit_output = 0;
	
	current_filename = 0;
	current_long_filename = 0;
	current_incdir = -1;
	
	found_files = 0;
	found_files_sys = 0;
	
	protect_detect.macro = 0;
	protect_detect.state = 0;
	protect_detect.ff = 0;
	protect_detect_stack = 0;
	
	ls_stack = 0;
	ls_depth = 0;
	
	find_file_error = 0;
	
	assertions = 0;
	macros = 0;
	
	eval_line = 0;
	emit_eval_warnings = 0;
	
	transient_characters = 0;
	
	init_buf_lexer_state(&dsharp_lexer, 0);
#ifdef PRAGMA_TOKENIZE
	init_buf_lexer_state(&tokenize_lexer, 0);
#endif
	time(&t);
	ct = localtime(&t);
	strftime(compile_time, 12, "\"%H:%M:%S\"", ct);
	strftime(compile_date, 24, "\"%b %d %Y\"", ct);
	
	init_macros();
	init_assertions();
	init_found_files();
};

/*
 * This function cleans the memory. It should release all allocated
 * memory structures and may be called even if the current pre-processing
 * is not finished or reported an error.
 */
cpp::~cpp(void)
{
	struct lexer_state ls;
	
	init_include_path(0);
	
	if (current_filename)
	{
		delete [] current_filename;
		current_filename = 0;
	}
	current_long_filename = 0;
	current_incdir = -1;
	
	protect_detect.state = 0;
	if (protect_detect.macro)
	{
		delete protect_detect.macro;
		protect_detect.macro = 0;
	}
	protect_detect.ff = 0;
	
	init_lexer_state(&ls);
	while (ls_depth > 0)
		pop_file_context(&ls);
	
	free_lexer_state(&ls);
	free_lexer_state(&dsharp_lexer);
#ifdef PRAGMA_TOKENIZE
	free_lexer_state(&tokenize_lexer);
#endif
	
	if (found_files)
	{
		delete found_files;
		found_files = 0;
	}
	
	if (found_files_sys)
	{
		delete found_files_sys;
		found_files_sys = 0;
	}
	
	wipe_macros();
	wipe_assertions();
}

/*
 * check_cpp_errors() must be called when the end of input is reached;
 * it checks pending errors due to truncated constructs (actually none,
 * this is reserved for future evolutions).
 */
int
cpp::check_cpp_errors(struct lexer_state *ls)
{
	return 0;
}

/*
 * Resets the include path.
 */
void
cpp::init_include_path(const char * const *incpath)
{
	if (include_path_nb > 0)
	{
		size_t i;
		
		for (i = 0; i < include_path_nb; i++)
			delete [] include_path[i];
		
		delete [] include_path;
		
		include_path = 0;
		include_path_nb = 0;
	}
	
	if (incpath)
	{
		int i;
		
		for (i = 0; incpath[i]; i++)
		{
			aol(include_path, include_path_nb,
			    sdup(incpath[i]), INCPATH_MEMG, char *);
		}
	}
}

/*
 * add_incpath() adds "path" to the standard include path.
 */
void
cpp::add_incpath(const char *path)
{
	aol(include_path, include_path_nb, sdup(path), INCPATH_MEMG, char *);
}

void
cpp::print_includes(FILE *f, const char *fmt)
{
	size_t i;
	
	for (i = 0; i < include_path_nb; i++)
		fprintf(f, fmt, include_path[i]);
}

} /* namespace ucpp */
