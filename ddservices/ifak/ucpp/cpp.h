
/*
 * $Id: cpp.h,v 1.5 2006/07/30 18:14:35 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _ucpp_ucpp_h
#define _ucpp_ucpp_h

// C stdlib
#include <stdio.h>
#include <string_edd.h>

// C++ stdlib
#include <set>
#include <string>

// own header
#include "hashtable.h"
#include "tune.h"


#if !defined(NATIVE_UINTMAX) && !defined(SIMUL_UINTMAX)
# if __STDC__ && __STDC_VERSION__ >= 199901L
#  include <stdint.h>
#  define NATIVE_UINTMAX uintmax_t
#  define NATIVE_INTMAX intmax_t
# else
#  define NATIVE_UINTMAX unsigned long
#  define NATIVE_INTMAX long
# endif
#endif

#ifndef NATIVE_UINTMAX
#error NATIVE_UINTMAX must be defined here
#endif

/**
 * Namespace for the micro cpp component.
 * ucpp is a C preprocessor compliant to ISO-C99. This
 * component is heavily modified by Frank Naumann (C++
 * class conversion).
 * 
 * Orignal author is Thomas Pornin.
 * Main site: http://www.di.ens.fr/~pornin/ucpp/
 */
namespace ucpp
{

typedef NATIVE_UINTMAX my_ulong;
typedef NATIVE_INTMAX my_slong;

typedef struct
{
	int sign;
	union
	{
		my_ulong uv;
		my_slong sv;
	} u;
} ppval;

/*
 * Some memory allocations are manually garbage-collected; essentially,
 * strings duplicated in the process of macro replacement. Each such
 * string is referenced in the garbage_fifo, which is cleared when all
 * nested macros have been resolved.
 */
struct garbage_fifo
{
	char **garbage;
	size_t ngarb, memgarb;
};

/*
 * To speed up deeply nested and repeated inclusions, we:
 * -- use a hash table to remember where we found each file
 * -- remember when the file is protected by a #ifndef/#define/#endif
 *    construction; we can then avoid including several times a file
 *    when this is not necessary.
 * -- remember in which directory, in the include path, the file was found.
 */
struct found_file
{
	char *long_name;	/* first field */
	char *name;
	char *protect;
};

/*
 * For files from system include path.
 */
struct found_file_sys
{
	char *name;
	struct found_file *rff;
	int incdir;
};

struct stack_context
{
	char *long_name, *name;
	long line;
};

struct token
{
	int type;
	long line;
	char *name;
};

struct token_fifo
{
	struct token *t;
	size_t nt;
	size_t art;
};

struct assert
{
	char *name;	/* this must be the first field */
	size_t nbval;
	struct token_fifo *val;
};

/*
 * A macro represented in a compact form; simple tokens are represented
 * by one byte, containing their number. Tokens with a string value are
 * followed by the value (string finished by a 0). Macro arguments are
 * followed by the argument number (in one byte -- thus implying a hard
 * limit of 254 arguments (number 255 is for __VA_ARGS__).
 */
struct comp_token_fifo
{
	size_t length;
	size_t rp;
	unsigned char *t;
};

struct macro
{
	char *name;	/* this must be the first field */
	int narg;
	char **arg;
	int nest;
	int vaarg;
	#ifdef LOW_MEM
	struct comp_token_fifo cval;
	#else
	struct token_fifo val;
	#endif
};

class cpp
{
public:
	cpp(void);
	virtual ~cpp(void);
	
public:
#ifdef _MSC_VER
	enum { CPPERR = 512 };
#else
	static const int CPPERR = 512;
#endif
	/*
	 * Errors from CPPERR_EOF and above are not real erros, only show-stoppers.
	 * Errors below CPPERR_EOF are real ones.
	 */
	static const int CPPERR_NEST;
	static const int CPPERR_EOF;
	
	/*
	 * Tokens (do not change the order unless checking operators_name[] in cpp.c)
	 *
	 * It is important that the token NONE is 0
	 * Check the STRING_TOKEN macro
	 */
	enum
	{
		NONE = 0,	/* whitespace */
		NEWLINE,	/* newline */
		COMMENT,	/* comment */
		NUMBER,		/* number constant */
		NAME,		/* identifier */
		BUNCH,		/* non-C characters */
		PRAGMA,		/* a #pragma directive */
		CONTEXT,	/* new file or #line */
		STRING,		/* constant "xxx" */
		CHAR,		/* constant 'xxx' */
		SLASH,		/*	/	*/
		ASSLASH,	/*	/=	*/
		MINUS,		/*	-	*/
		MMINUS,		/*	--	*/
		ASMINUS,	/*	-=	*/
		ARROW,		/*	->	*/
		PLUS,		/*	+	*/
		PPLUS,		/*	++	*/
		ASPLUS,		/*	+=	*/
		LT,		/*	<	*/
		LEQ,		/*	<=	*/
		LSH,		/*	<<	*/
		ASLSH,		/*	<<=	*/
		GT,		/*	>	*/
		GEQ,		/*	>=	*/
		RSH,		/*	>>	*/
		ASRSH,		/*	>>=	*/
		ASGN,		/*	=	*/
		SAME,		/*	==	*/
		#ifdef CAST_OP
		CAST,		/*	=>	*/
		#endif
		NOT,		/*	~	*/
		NEQ,		/*	!=	*/
		AND,		/*	&	*/
		LAND,		/*	&&	*/
		ASAND,		/*	&=	*/
		OR,		/*	|	*/
		LOR,		/*	||	*/
		ASOR,		/*	|=	*/
		PCT,		/*	%	*/
		ASPCT,		/*	%=	*/
		STAR,		/*	*	*/
		ASSTAR,		/*	*=	*/
		CIRC,		/*	^	*/
		ASCIRC,		/*	^=	*/
		LNOT,		/*	!	*/
		LBRA,		/*	{	*/
		RBRA,		/*	}	*/
		LBRK,		/*	[	*/
		RBRK,		/*	]	*/
		LPAR,		/*	(	*/
		RPAR,		/*	)	*/
		COMMA,		/*	,	*/
		QUEST,		/*	?	*/
		SEMIC,		/*	;	*/
		COLON,		/*	:	*/
		DOT,		/*	.	*/
		MDOTS,		/*	...	*/
		SHARP,		/*	#	*/
		DSHARP,		/*	##	*/
		
		OPT_NONE,	/* optional space to separate tokens in text output */
		
		DIGRAPH_TOKENS,	/* there begin digraph tokens */
		
		/* for DIG_*, do not change order, unless checking undig() in cpp.c */
		DIG_LBRK,	/*	<:	*/
		DIG_RBRK,	/*	:>	*/
		DIG_LBRA,	/*	<%	*/
		DIG_RBRA,	/*	%>	*/
		DIG_SHARP,	/*	%:	*/
		DIG_DSHARP,	/*	%:%:	*/
		
		DIGRAPH_TOKENS_END, /* digraph tokens end here */
		
		LAST_MEANINGFUL_TOKEN, /* reserved words will go there */
		
		MACROARG,	/* special token for representing macro arguments */
		
		UPLUS = CPPERR,	/* unary + */
		UMINUS		/* unary - */
	};
	static const char *operators_name[DIGRAPH_TOKENS_END];
	
	struct lexer_state
	{
		/* input control */
		FILE *input;
#ifndef NO_UCPP_BUF
		unsigned char *input_buf;
#endif
		unsigned char *input_string;
		size_t ebuf;
		size_t pbuf;
		
		int lka[32];
		int lka_rptr;
		int lka_wptr;
		int nlka;
		
		int macfile, eof;
		int last;
		int discard;
		unsigned long utf8;
		unsigned char copy_line[COPY_LINE_LENGTH];
		int cli;
		
		/* output control */
		FILE *output;
		struct token_fifo *output_fifo, *toplevel_of;
#ifndef NO_UCPP_BUF
		unsigned char *output_buf;
#endif
		size_t sbuf;
		
		/* token control */
		struct token *ctok;
		struct token *save_ctok;
		size_t tknl;
		int ltwnl;
		int pending_token;
#ifdef INMACRO_FLAG
		int inmacro;
		long macro_count;
#endif
		
		/* lexer options */
		long line;
		long oline;
		unsigned long flags;
		long count_trigraphs;
		struct garbage_fifo *gf;
		int ifnest;
		int condnest;
		int condcomp;
		int condmet;
		unsigned long condf[2];
	};
	
	/*
	 * Flags for struct lexer_state
	 */
	/* warning flags */
	static const unsigned long WARN_STANDARD;	/* emit standard warnings */
	static const unsigned long WARN_ANNOYING;	/* emit annoying warnings */
	static const unsigned long WARN_TRIGRAPHS;	/* warn when trigraphs are used */
	static const unsigned long WARN_TRIGRAPHS_MORE;	/* extra-warn for trigraphs */
	static const unsigned long WARN_PRAGMA;		/* warn for pragmas in non-lexer mode */
	
	/* error flags */
	static const unsigned long FAIL_SHARP;		/* emit errors on rogue '#' */
	static const unsigned long CCHARSET;		/* emit errors on non-C characters */
	
	/* emission flags */
	static const unsigned long DISCARD_COMMENTS;	/* discard comments from text output */
	static const unsigned long CPLUSPLUS_COMMENTS;	/* understand C++-like comments */
	static const unsigned long LINE_NUM;		/* emit #line directives in output */
	static const unsigned long GCC_LINE_NUM;	/* same as #line, with gcc-syntax */
	
	/* language flags */
	static const unsigned long HANDLE_ASSERTIONS;	/* understand assertions */
	static const unsigned long HANDLE_PRAGMA;	/* emit PRAGMA tokens in lexer mode */
	static const unsigned long MACRO_VAARG;		/* understand macros with '...' */
	static const unsigned long UTF8_SOURCE;		/* identifiers are in UTF8 encoding */
	static const unsigned long HANDLE_TRIGRAPHS;	/* handle trigraphs */
	
	/* global ucpp behaviour */
	static const unsigned long LEXER;		/* behave as a lexer */
	static const unsigned long KEEP_OUTPUT;		/* emit the result of preprocessing */
	static const unsigned long COPY_LINE;		/* make a copy of the parsed line */
	static const unsigned long PDM_MODE;		/* try to be simatic PDM compatible */
	static const unsigned long APPEND_NEWLINE;	/* implicitly append '\n' to every input file */
	
	/* internal flags */
	static const unsigned long READ_AGAIN;		/* emit again the last token */
	static const unsigned long TEXT_OUTPUT;		/* output text */
	
	/* default configurations */
	static const unsigned long DEFAULT_CPP_FLAGS;
	static const unsigned long DEFAULT_LEXER_FLAGS;
	
	/* configurable options */
	int no_special_macros;
	int c99_compliant;
	int c99_hosted;
	int emit_dependencies;
	int emit_defines;
	int emit_assertions;
	FILE *emit_output;
	
protected:
	char **include_path;
	size_t include_path_nb;
	
	char *current_filename;
	char *current_long_filename;
	int current_incdir;
	
	hashtable<const char *, struct found_file> *found_files;
	hashtable<const char *, struct found_file_sys> *found_files_sys;
	
	std::set<std::string> included_files;
	
	/*
	 * To keep up with the #ifndef/#define/#endif protection mechanism
	 * detection.
	 */
	struct protect
	{
		char *macro;
		int state;
		struct found_file *ff;
	};
	struct protect protect_detect;
	struct protect *protect_detect_stack;
	
	hashtable<const char *, struct assert> *assertions;
	hashtable<const char *, struct macro> *macros;
	
	long eval_line;
	int emit_eval_warnings;
	
	struct lexer_state dsharp_lexer;
#ifdef PRAGMA_TOKENIZE
	struct lexer_state tokenize_lexer;
#endif
	
	/*
	 * Two strings evaluated at initialization time, to handle the __TIME__
	 * and __DATE__ special macros.
	 *
	 * C99 specifies that these macros should remain constant throughout
	 * the whole preprocessing.
	 */
	char compile_time[12];
	char compile_date[24];
	
	
	
	/* assert */
protected:
	void print_token_fifo(struct token_fifo *tf);
	void print_assert(struct assert *a);
	
	int cmp_token_list(struct token_fifo *, struct token_fifo *);
	int handle_assert(struct lexer_state *);
	int handle_unassert(struct lexer_state *);
	struct assert *get_assertion(const char *);
	void wipe_assertions(void);
	
	void init_assertions(void);
public:
	int make_assertion(char *);
	int destroy_assertion(char *);
	void print_assertions(void);
	
	
	/* eval */
protected:
	my_ulong op_slash_u(my_ulong x, my_ulong y);
	my_ulong op_pct_u(my_ulong x, my_ulong y);
	my_slong op_slash_s(my_slong x, my_slong y);
	my_slong op_pct_s(my_slong x, my_slong y);
	
	int boolval(ppval x);
	int pp_suffix(const char *d, const char *refc);
	ppval pp_decconst(const char *c, int sign, const char *refc);
	ppval pp_octconst(const char *c, int sign, const char *refc);
	ppval pp_hexconst(const char *c, int sign, const char *refc);
	unsigned long pp_char(const char *c, const char *refc);
	ppval pp_strtoconst(const char *refc);
	unsigned long strtoconst(const char *c);
	ppval eval_opun(int op, ppval v);
	ppval eval_opbin(int op, ppval v1, ppval v2);
	int op_prec(int op);
	ppval eval_shrd(struct token_fifo *tf, int minprec, int do_eval);
	
	unsigned long eval_expr(struct token_fifo *, int *, int);
	
	/*
	 * If you want to hardcode a conversion table, define a static array
	 * of 256 int, and make transient_characters point to it.
	 */
	const int *transient_characters;
	
	
	/* cpp */
protected:
	struct stack_context *report_context(void);
	
	void init_buf_lexer_state(struct lexer_state *, int);
	#ifdef PRAGMA_TOKENIZE
	struct comp_token_fifo compress_token_list(struct token_fifo *);
	#endif
	
	const char *token_name(struct token *);
	
	void init_garbage_fifo(struct garbage_fifo *gf);
	void free_garbage_fifo(struct garbage_fifo *gf);
	void throw_away(struct garbage_fifo *, char *);
	void garbage_collect(struct garbage_fifo *);
	
	void init_found_files(void);
	void reinit_lexer_state(struct lexer_state *ls, int wb);
	void restore_lexer_state(struct lexer_state *ls, struct lexer_state *lsbak);
	
	void close_input(struct lexer_state *ls);
	
	/*
	 * file_context (and the two functions push_ and pop_) are used to save
	 * all that is needed when including a file.
	 */
	struct file_context
	{
		struct lexer_state ls;
		char *name, *long_name;
		int incdir;
	};
	
	struct file_context *ls_stack;
	size_t ls_depth;
	
	void push_file_context(struct lexer_state *ls);
	void pop_file_context(struct lexer_state *ls);
	
	void print_line_info(struct lexer_state *ls, unsigned long flags);
	
	/*
	 * Find a file by looking through the include path.
	 * return value: a FILE * on the file, opened in "r" mode, or 0.
	 *
	 * find_file_error will contain:
	 *   FF_ERROR      on error (file not found or impossible to read)
	 *   FF_PROTECT    file is protected and therefore useless to read
	 *   FF_KNOWN      file is already known
	 *   FF_UNKNOWN    file was not already known
	 */
	enum { FF_ERROR, FF_PROTECT, FF_KNOWN, FF_UNKNOWN };
	
	int find_file_error;
	
	FILE *find_file(const char *name, int localdir);
	FILE *find_file_next(const char *name);
	
	int handle_if(struct lexer_state *ls);
	int handle_include(struct lexer_state *ls, unsigned long flags, int next);
	int handle_line(struct lexer_state *ls, unsigned long flags);
	void handle_error(struct lexer_state *ls);
	void handle_pragma(struct lexer_state *ls);
	int handle_cpp(struct lexer_state *ls, int sharp_type);
	
	int undig(int type);
	int llex(struct lexer_state *ls);
	
public:
	virtual void fatal(const char *, ...);
	virtual void error(long, const char *, ...);
	virtual void warning(long, const char *, ...);
	
	int check_cpp_errors(struct lexer_state *);
	
	void set_init_filename(const char *, int);
	void add_incpath(const char *);
	void init_include_path(const char * const *);
	void print_includes(FILE *f, const char *fmt);
	const std::set<std::string> &get_included_files(void) const { return included_files; }
	
	int enter_file(struct lexer_state *, unsigned long);
	
	void init_lexer_state(struct lexer_state *);
	void init_lexer_mode(struct lexer_state *);
	void free_lexer_state(struct lexer_state *);
	
	int run_cpp(struct lexer_state *);
	int lex(struct lexer_state *);
	
	
	/* lexer */
protected:
	void init_cppm(void);
	int space_char(int);
	#ifndef NO_UCPP_BUF
public:
	void flush_output(struct lexer_state *);
	#endif
protected:
	void write_char(struct lexer_state *ls, unsigned char c);
public:
	/* schedule a character for output */
	void put_char(struct lexer_state *ls, unsigned char c)
	{ if (ls->flags & KEEP_OUTPUT) write_char(ls, c); }
private:
	int read_char(struct lexer_state *ls);
	int next_fifo_char(struct lexer_state *ls);
	int char_lka1(struct lexer_state *ls);
	int char_lka2(struct lexer_state *ls);
	int char_lkaX(struct lexer_state *ls, int x);
	int next_char(struct lexer_state *ls);
	int grap_char(struct lexer_state *);
	void discard_char(struct lexer_state *);
	int utf8_to_string(unsigned char buf[], unsigned long utf8);
	void canonize_id(struct lexer_state *ls, char *id);
	
	int read_token(struct lexer_state *ls);
	int next_token(struct lexer_state *);
	
	
	/* macro */
protected:
	int check_special_macro(const char *name);
	void add_special_macros(void);
	void print_macro(struct macro *m);
	void print_token_nailed(struct lexer_state *ls, struct token *t, long nail_line);
	int collect_arguments(struct lexer_state *ls, struct token_fifo *tfi,
				int penury, struct token_fifo *atl, int narg, int vaarg, int *wr);
	int concat_token(struct token *t1, struct token *t2);
	char *tokenize_string(struct lexer_state *ls, const char *buf);
	char *stringify_string(const char *x);
	char *stringify(struct token_fifo *tf);
	
	void print_token(struct lexer_state *, struct token *, long);
	int handle_define(struct lexer_state *);
	int handle_undef(struct lexer_state *);
	int handle_ifdef(struct lexer_state *);
	int handle_ifndef(struct lexer_state *);
	int substitute_macro(struct lexer_state *, struct macro *,
			     struct token_fifo *, int, int, long);
	struct macro *get_macro(const char *);
	void wipe_macros(void);
	
	void init_macros(void);
public:
	int define_macro(struct lexer_state *, const char *);
	int undef_macro(struct lexer_state *, const char *);
	void print_defines(void);
	
	
	/* misc */
protected:
	char *sdup(const char *src)
	{
		size_t n = 1 + strlen(src);
		char *x = new char[n];
		
		memcpy(x, src, n);
		return x;
	}
};

} /* namespace ucpp */

#endif /* _ucpp_ucpp_h */
