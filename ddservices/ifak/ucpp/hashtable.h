
/*
 * $Id: hashtable.h,v 1.2 2005/10/03 10:35:33 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a general purpose hash template class.
 */

# ifndef _ucpp_hashtable_h
# define _ucpp_hashtable_h

# ifdef _MSC_VER
# pragma warning (disable: 4786)
# endif

// C++ stdlib
# include <vector>


namespace ucpp
{

/** Compare function template used by the hashtable template class. */
template <class Key> struct equalfunc { };

/** Hash function template used by the hashtable template class. */
template <class Key> struct hashfunc { };

/* some defaults
 */

/* C string keys */

/**
 * Compare function template specialization used by the hashtable template
 * class.
 * This is a specialization for C style strings of the Compare function
 * template equalfunc used by the hashtable template class.
 */
template<>
struct equalfunc<const char *>
{
	bool operator()(const char *__x, const char *__y) const
	{ return !strcmp(__x, __y); }
};
/**
 * Hash function template specialization used by the hashtable template class.
 * This is a specialization for C style strings of the Hash function template
 * hashfunc used by the hashtable template class.
 */
template<>
struct hashfunc<const char *>
{
	unsigned long operator()(const char *__s, const unsigned long __HASHBITS) const
	{
		register unsigned long hashval = 0;
		
		while (*__s)
		{
			hashval = ((hashval << 5) - hashval) + *__s;
			__s++;
		}
		
		hashval ^= (hashval >> __HASHBITS) ^ (hashval >> (__HASHBITS << 1));
		
		return hashval;
	}
};

/* skalar type keys */

# define proto(type) \
template<> \
struct equalfunc<type> \
{ \
	bool operator()(const type __x, const type __y) const \
	{ return (__x == __y); } \
}

proto(char);
proto(unsigned char);
proto(short);
proto(unsigned short);
proto(int);
proto(unsigned int);
proto(long);
proto(unsigned long);

# undef proto

# define proto(type) \
template<> \
struct hashfunc<type> \
{ \
	unsigned long operator()(type __x, const unsigned long) const \
	{ return (unsigned long) __x; } \
}

proto(char);
proto(unsigned char);
proto(short);
proto(unsigned short);
proto(int);
proto(unsigned int);
proto(long);
proto(unsigned long);

# undef proto


/**
 * The hashtable template class.
 * This template implements an efficient general purpose hashing
 * algorithm. It work with a fixed size hash table and linked lists
 * for collision handling (open hashing).
 */
template <
	class Key,
	class T,
	class EqualFunc = equalfunc<Key>,
	class HashFunc = hashfunc<Key>
>
class hashtable
{
private:
	const unsigned long HASHBITS;
	const unsigned long HASHSIZE;
	const unsigned long HASHMASK;
	void (*del_data)(T *);
	
public:
	/**
	 * Internal data holder for an hash object.
	 * This struct save the hash object and the corresponding key. It
	 * realize the linked list for collision handling too.
	 */
	struct map
	{
		map *next;
		const Key key;
		T *data;
		
		map(map *n, const Key k, T *d) : next(n), key(k), data(d) { }
	};
	
	/** The main hash table. */
	std::vector<struct map *> table;
	
private:
	/** The concrete compare function. */
	EqualFunc equal;
	/** The concrete hash function. */
	HashFunc hasher;
	
# ifdef STATISTIC
	/** Statistic data. */
	//!{
	mutable unsigned long entries;
	mutable unsigned long collisions;
	mutable unsigned long mem;
	//!}
# define HASHTABLE_STATISTIC_CONSTRUCT , entries(0), collisions(0), mem(0)
# else
# define HASHTABLE_STATISTIC_CONSTRUCT
# endif
	
public:
	/** default constructor with a 10bit sized hash table. */
	hashtable(void (*del)(T *))
	: HASHBITS(10UL), HASHSIZE(1UL << HASHBITS), HASHMASK(HASHSIZE - 1UL),
	  del_data(del),
	  table(HASHSIZE) HASHTABLE_STATISTIC_CONSTRUCT { }
	/**
	 * special constructor with a user defined sized hash table.
	 * \note The argument is the hash table size in bits. The value cannot
	 * be greater than 32.
	 */
	explicit hashtable(void (*del)(T *), unsigned long bits)
	: HASHBITS(bits), HASHSIZE(1UL << HASHBITS), HASHMASK(HASHSIZE - 1UL),
	  del_data(del),
	  table(HASHSIZE) HASHTABLE_STATISTIC_CONSTRUCT { }
	
	~hashtable(void)
	{
		int i, end;
		
		for (i = 0, end = table.size(); i < end; i++)
		{
			map *m = table[i];
			
			while (m)
			{
				map *next = m->next;
				
				if (del_data)
					del_data(m->data);
				
				delete m;
				
				m = next;
			}
		}
	}
	
# ifdef STATISTIC
	void stats(unsigned long data[5])
	{
		data[0] = HASHBITS;
		data[1] = HASHSIZE;
		data[2] = entries;
		data[3] = collisions;
		data[4] = mem;
	}
# endif
	
	/** Calculate the hash value for the key. */
	inline unsigned long hash(const Key key) const
	{
		return (hasher(key, HASHBITS) & HASHMASK);
	}
	
	/**
	 * Lookup the hashtable for an hash object corresponding to this key.
	 * \param key The key where the hashtable is searched for.
	 * \return The corresponding hash object or 0.
	 */
	T *lookup(const Key key) const
	{
		const unsigned long hashval = hash(key);
		map *m;
		
		for (m = table[hashval]; m != NULL; m = m->next)
			if (equal(m->key, key))
				return m->data;
		
		return 0;
	}
	
	/** Same as lookup(). \see lookup */
	T *operator[](const Key key) const { return lookup(key); }
	
	/**
	 * Install a new hash object for the specified key.
	 * \param key The key for the hash object.
	 * \param data The hash object.
	 * \note It's assumed that there isn't anything already installed for
	 * the specified key. The caller is responsible to verify this
	 * condition. The behaviour of this class is undefined in case that
	 * these condition is not true.
	 */
	T *install(const Key key, T *data)
	{
		T *check;
		
		check = lookup(key);
		if (!check)
		{
			const unsigned long hashval = hash(key);
			map *m;
			
			m = new map(table[hashval], key, data);
			if (!m) throw 0; // RuntimeError(xxOutOfMemory);
			
# ifdef STATISTIC
			entries++;
			if (table[hashval])
				collisions++;
			
			mem += sizeof(map);
# endif
			
			table[hashval] = m;
		}
		
		return check;
	}
	
	/**
	 * Delete the hash object for the specified key.
	 * \param key The key of the hash object that should be deleted. If
	 * there is no such key with an assoziated hash object nothing is
	 * done.
	 */
	void remove(const Key key)
	{
		const unsigned long hashval = hash(key);
		map **temp = &table[hashval];
		
		while (*temp)
		{
			if (equal((*temp)->key, key))
			{
				map *m = *temp;
				
				*temp = m->next;
				m->next = NULL;
				
				if (del_data)
					del_data(m->data);
				
				delete m;
				
# ifdef STATISTIC
				entries--;
				if (table[hashval])
					collisions--;
				
				mem -= sizeof(map);
# endif
				break;
			}
			
			/* next */
			temp = &(*temp)->next;
		}
	}
		
};

} /* namespace ucpp */

# endif /* _ucpp_hashtable_h */
