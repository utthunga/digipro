
/*
 * $Id: ucpp.cxx,v 1.3 2006/07/20 13:30:35 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#define VERS_MAJ	1
#define VERS_MIN	2

#include <stdlib.h>
#include "cpp.h"

using namespace ucpp;


/*
 * The standard path where includes are looked for.
 */
static char *include_path_std[] = { STD_INCLUDE_PATH, 0 };

static char *system_macros_def[] = { STD_MACROS, 0 };
static char *system_assertions_def[] = { STD_ASSERT, 0 };


/*
 * print some help
 */
static void
usage(const char *command_name)
{
	fprintf(stderr,
		"Usage: %s [options] [file]\n"
		"language options:\n"
		"  -C              keep comments in output\n"
		"  -s              keep '#' when no cpp directive is recognized\n"
		"  -l              do not emit line numbers\n"
		"  -lg             emit gcc-like line numbers\n"
		"  -CC             disable C++-like comments\n"
		"  -a, -na, -a0    handle (or not) assertions\n"
		"  -V              disable macros with extra arguments\n"
		"  -u              understand UTF-8 in source\n"
		"  -X              enable -a, -u and -Y\n"
		"  -c90            mimic C90 behaviour\n"
		"  -t              disable trigraph support\n"
		"warning options:\n"
		"  -wt             emit a final warning when trigaphs are encountered\n"
		"  -wtt            emit warnings for each trigaph encountered\n"
		"  -wa             emit warnings that are usually useless\n"
		"  -w0             disable standard warnings\n"
		"directory options:\n"
		"  -I directory    add 'directory' before the standard include path\n"
		"  -J directory    add 'directory' after the standard include path\n"
		"  -zI             do not use the standard include path\n"
		"  -M              emit Makefile-like dependencies instead of normal output\n"
		"  -Ma             emit also dependancies for system files\n"
		"  -o file         store output in file\n"
		"macro and assertion options:\n"
		"  -Dmacro         predefine 'macro'\n"
		"  -Dmacro=def     predefine 'macro' with 'def' content\n"
		"  -Umacro         undefine 'macro'\n"
		"  -Afoo(bar)      assert foo(bar)\n"
		"  -Bfoo(bar)      unassert foo(bar)\n"
		"  -Y              predefine system-dependant macros\n"
		"  -Z              do not predefine special macros\n"
		"  -d              emit defined macros\n"
		"  -e              emit assertions\n"
		"misc options:\n"
		"  -v              print version number and settings\n"
		"  -h              show this help\n",
		command_name);
}

/*
 * print version and compile-time settings
 */
static void
version(class cpp *cpp)
{
	fprintf(stderr, "ucpp version %d.%d\n", VERS_MAJ, VERS_MIN);
	fprintf(stderr, "search path:\n");
	cpp->print_includes(stderr, "  %s\n");
}

/*
 * parse_opt() initializes many things according to the command-line
 * options.
 * Return values:
 * 0  on success
 * 1  on semantic error (redefinition of a special macro, for instance)
 * 2  on syntaxic error (unknown options for instance)
 */
static int
parse_opt(int argc, char *argv[], cpp::lexer_state *ls, class cpp *cpp)
{
	int i, ret = 0;
	char *filename = 0;
	int with_std_incpath = 1;
	int print_version = 0, print_defs = 0, print_asserts = 0;
	int system_macros = 0, standard_assertions = 1;
	
	cpp->init_lexer_state(ls);
	ls->flags = cpp->DEFAULT_CPP_FLAGS;
	cpp->emit_output = ls->output = stdout;
	for (i = 1; i < argc; i++) if (argv[i][0] == '-')
	{
		if (!strcmp(argv[i], "-h"))
			return 2;
		else if (!strcmp(argv[i], "-C"))
			ls->flags &= ~cpp->DISCARD_COMMENTS;
		else if (!strcmp(argv[i], "-CC"))
			ls->flags &= ~cpp->CPLUSPLUS_COMMENTS;
		else if (!strcmp(argv[i], "-a"))
			ls->flags |= cpp->HANDLE_ASSERTIONS;
		else if (!strcmp(argv[i], "-na"))
		{
			ls->flags |= cpp->HANDLE_ASSERTIONS;
			standard_assertions = 0;
		}
		else if (!strcmp(argv[i], "-a0"))
			ls->flags &= ~cpp->HANDLE_ASSERTIONS;
		else if (!strcmp(argv[i], "-V"))
			ls->flags &= ~cpp->MACRO_VAARG;
		else if (!strcmp(argv[i], "-u"))
			ls->flags |= cpp->UTF8_SOURCE;
		else if (!strcmp(argv[i], "-X"))
		{
			ls->flags |= cpp->HANDLE_ASSERTIONS;
			ls->flags |= cpp->UTF8_SOURCE;
			system_macros = 1;
		}
		else if (!strcmp(argv[i], "-c90"))
		{
			ls->flags &= ~cpp->MACRO_VAARG;
			ls->flags &= ~cpp->CPLUSPLUS_COMMENTS;
			cpp->c99_compliant = 0;
			cpp->c99_hosted = -1;
		}
		else if (!strcmp(argv[i], "-t"))
			ls->flags &= ~cpp->HANDLE_TRIGRAPHS;
		else if (!strcmp(argv[i], "-wt"))
			ls->flags |= cpp->WARN_TRIGRAPHS;
		else if (!strcmp(argv[i], "-wtt"))
			ls->flags |= cpp->WARN_TRIGRAPHS_MORE;
		else if (!strcmp(argv[i], "-wa"))
			ls->flags |= cpp->WARN_ANNOYING;
		else if (!strcmp(argv[i], "-w0"))
		{
			ls->flags &= ~cpp->WARN_STANDARD;
			ls->flags &= ~cpp->WARN_PRAGMA;
		}
		else if (!strcmp(argv[i], "-s"))
			ls->flags &= ~cpp->FAIL_SHARP;
		else if (!strcmp(argv[i], "-l"))
			ls->flags &= ~cpp->LINE_NUM;
		else if (!strcmp(argv[i], "-lg"))
			ls->flags |= cpp->GCC_LINE_NUM;
		else if (!strcmp(argv[i], "-M"))
		{
			ls->flags &= ~cpp->KEEP_OUTPUT;
			cpp->emit_dependencies = 1;
		}
		else if (!strcmp(argv[i], "-Ma"))
		{
			ls->flags &= ~cpp->KEEP_OUTPUT;
			cpp->emit_dependencies = 2;
		}
		else if (!strcmp(argv[i], "-Y"))
			system_macros = 1;
		else if (!strcmp(argv[i], "-Z"))
			cpp->no_special_macros = 1;
		else if (!strcmp(argv[i], "-d")) {
			ls->flags &= ~cpp->KEEP_OUTPUT;
			print_defs = 1;
		}
		else if (!strcmp(argv[i], "-e"))
		{
			ls->flags &= ~cpp->KEEP_OUTPUT;
			print_asserts = 1;
		}
		else if (!strcmp(argv[i], "-zI"))
			with_std_incpath = 0;
		else if (!strcmp(argv[i], "-I") || !strcmp(argv[i], "-J"))
			i++;
		else if (!strcmp(argv[i], "-o"))
		{
			if ((++i) >= argc)
			{
				cpp->error(-1, "missing filename after -o");
				return 2;
			}
			
			if (argv[i][0] == '-' && argv[i][1] == 0)
				cpp->emit_output = ls->output = stdout;
			else
			{
				ls->output = fopen(argv[i], "w");
				if (!ls->output)
				{
					cpp->error(-1, "failed to open for "
						"writing: %s", argv[i]);
					return 2;
				}
				cpp->emit_output = ls->output;
			}
		}
		else if (!strcmp(argv[i], "-v"))
			print_version = 1;
		else if (argv[i][1] != 'I' && argv[i][1] != 'J'
			 && argv[i][1] != 'D' && argv[i][1] != 'U'
			 && argv[i][1] != 'A' && argv[i][1] != 'B')
			cpp->warning(-1, "unknown option '%s'", argv[i]);
	}
	else
	{
		if (filename != 0)
		{
			cpp->error(-1, "spurious filename '%s'", argv[i]);
			return 2;
		}
		
		filename = argv[i];
	}
	
	if (filename)
	{
		ls->input = fopen(filename, "r");
		if (!ls->input)
		{
			cpp->error(-1, "file '%s' not found", filename);
			return 1;
		}
#ifdef NO_LIBC_BUF
		setbuf(ls->input, 0);
#endif
		cpp->set_init_filename(filename, 1);
	}
	else
	{
		ls->input = stdin;
		cpp->set_init_filename("<stdin>", 0);
	}
	
	for (i = 1; i < argc; i++)
		if (argv[i][0] == '-' && argv[i][1] == 'I')
			cpp->add_incpath(argv[i][2] ? argv[i] + 2 : argv[i + 1]);
	
	if (system_macros) for (i = 0; system_macros_def[i]; i++)
		ret = ret || cpp->define_macro(ls, system_macros_def[i]);
	
	for (i = 1; i < argc; i++)
		if (argv[i][0] == '-' && argv[i][1] == 'D')
			ret = ret || cpp->define_macro(ls, argv[i] + 2);
	
	for (i = 1; i < argc; i++)
		if (argv[i][0] == '-' && argv[i][1] == 'U')
			ret = ret || cpp->undef_macro(ls, argv[i] + 2);
	
	if (ls->flags & cpp->HANDLE_ASSERTIONS)
	{
		if (standard_assertions)
			for (i = 0; system_assertions_def[i]; i++)
				cpp->make_assertion(system_assertions_def[i]);
		
		for (i = 1; i < argc; i++)
			if (argv[i][0] == '-' && argv[i][1] == 'A')
				ret = ret || cpp->make_assertion(argv[i] + 2);
		
		for (i = 1; i < argc; i++)
			if (argv[i][0] == '-' && argv[i][1] == 'B')
				ret = ret || cpp->destroy_assertion(argv[i] + 2);
	}
	else
	{
		for (i = 1; i < argc; i++)
			if (argv[i][0] == '-' && (argv[i][1] == 'A' || argv[i][1] == 'B'))
				cpp->warning(-1, "assertions disabled");
	}
	
	if (with_std_incpath)
		for (i = 0; include_path_std[i]; i++)
			cpp->add_incpath(include_path_std[i]);
	
	for (i = 1; i < argc; i++)
		if (argv[i][0] == '-' && argv[i][1] == 'J')
			cpp->add_incpath(argv[i][2] ? argv[i] + 2 : argv[i + 1]);

	if (print_version)
	{
		version(cpp);
		return 1;
	}
	
	if (print_defs)
	{
		cpp->print_defines();
		cpp->emit_defines = 1;
	}
	
	if (print_asserts && (ls->flags & cpp->HANDLE_ASSERTIONS))
	{
		cpp->print_assertions();
		cpp->emit_assertions = 1;
	}
	
	return ret;
}

int
main(int argc, char *argv[])
{
	class cpp cpp;
	cpp::lexer_state ls;
	int r, fr = 0;
	
	r = parse_opt(argc, argv, &ls, &cpp);
	if (r != 0)
	{
		if (r == 2) usage(argv[0]);
		return EXIT_FAILURE;
	}
	
	cpp.enter_file(&ls, ls.flags);
	while ((r = cpp.run_cpp(&ls)) < cpp.CPPERR_EOF)
		fr = fr || (r > 0);
	
	fr = fr || cpp.check_cpp_errors(&ls);
	if (ls.flags & cpp.KEEP_OUTPUT)
		/* while (ls.line > ls.oline) put_char(&ls, '\n'); */
		cpp.put_char(&ls, '\n');
	
	if (cpp.emit_dependencies)
		fputc('\n', cpp.emit_output);
	
#ifndef NO_UCPP_BUF
	if (!(ls.flags & cpp.LEXER))
		cpp.flush_output(&ls);
#endif
	
	if (ls.flags & cpp.WARN_TRIGRAPHS && ls.count_trigraphs)
		cpp.warning(0, "%ld trigraphs encountered", ls.count_trigraphs);
	
	cpp.free_lexer_state(&ls);
	
	return fr ? EXIT_FAILURE : EXIT_SUCCESS;
}
