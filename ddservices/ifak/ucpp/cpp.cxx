
/*
 * $Id: cpp.cxx,v 1.4 2006/07/30 18:14:34 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdarg.h>
#include "cpp.h"
#include "internal.h"


namespace ucpp
{

/*
 * fatal is the name for an internal ucpp error. If AUDIT is not defined,
 * no code calling this function will be generated;
 */
void
cpp::fatal(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	fprintf(stderr, "%s: fatal, ", current_filename);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
	va_end(ap);
	die();
}

/*
 * report an error, with current_filename, line, and printf-like syntax
 */
void
cpp::error(long line, const char *fmt, ...)
{
	va_list ap;

	if (line > 0)
		fprintf(stderr, "%s: line %ld: ", current_filename, line);
	else if (line == 0)
		fprintf(stderr, "%s: ", current_filename);
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	fprintf(stderr, "\n");
	if (line >= 0)
	{
		struct stack_context *sc = report_context();
		size_t i;

		for (i = 0; sc[i].line >= 0; i++)
			fprintf(stderr, "\tincluded from %s:%ld\n",
				sc[i].long_name ? sc[i].long_name : sc[i].name,
				sc[i].line);
		delete sc;
	}
}

/*
 * like error(), with the mention "warning"
 */
void
cpp::warning(long line, const char *fmt, ...)
{
	va_list ap;
	
	if (line > 0)
		fprintf(stderr, "%s: warning: line %ld: ", current_filename, line);
	else if (line == 0)
		fprintf(stderr, "%s: warning: ", current_filename);
	else
		fprintf(stderr, "warning: ");
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	fprintf(stderr, "\n");
	if (line >= 0)
	{
		struct stack_context *sc = report_context();
		size_t i;

		for (i = 0; sc[i].line >= 0; i++)
			fprintf(stderr, "\tincluded from %s:%ld\n",
				sc[i].long_name ? sc[i].long_name : sc[i].name,
				sc[i].line);
		delete sc;
	}
}

/*
 * throw_away() marks a string to be collected later
 */
void
cpp::throw_away(struct garbage_fifo *gf, char *n)
{
	wan(gf->garbage, gf->ngarb, n, gf->memgarb, char *);
}

/*
 * free marked strings
 */
void
cpp::garbage_collect(struct garbage_fifo *gf)
{
	size_t i;

	for (i = 0; i < gf->ngarb; i++)
		delete [] gf->garbage[i];
	
	gf->ngarb = 0;
}

void
cpp::init_garbage_fifo(struct garbage_fifo *gf)
{
	gf->memgarb = GARBAGE_LIST_MEMG;
	gf->garbage = new char *[gf->memgarb];
	gf->ngarb = 0;
}

void
cpp::free_garbage_fifo(struct garbage_fifo *gf)
{
	garbage_collect(gf);
	delete [] gf->garbage;
	delete gf;
}

/*
 * order is important: it must match the token-constants declared as an
 * enum in the header file.
 */
const char *cpp::operators_name[] =
{
	" ", "\n", " ",
	"0000", "name", "bunch", "pragma", "context",
	"\"dummy string\"", "'dummy char'",
	"/", "/=", "-", "--", "-=", "->", "+", "++", "+=", "<", "<=", "<<",
	"<<=", ">", ">=", ">>", ">>=", "=", "==",
#ifdef CAST_OP
	"=>",
#endif
	"~", "!=", "&", "&&", "&=", "|", "||", "|=", "%", "%=", "*", "*=",
	"^", "^=", "!",
	"{", "}", "[", "]", "(", ")", ",", "?", ";",
	":", ".", "...", "#", "##", " ", "fatal", "<:", ":>", "<%", "%>",
	"%:", "%:%:"
};

/* the ascii representation of a token */
#ifdef SEMPER_FIDELIS
#define tname(x)	(ttWHI((x).type) ? " " : S_TOKEN((x).type) \
			? (x).name : operators_name[(x).type])
#else
#define tname(x)	(S_TOKEN((x).type) ? (x).name \
			: operators_name[(x).type])
#endif

const char *
cpp::token_name(struct token *t)
{
	return tname(*t);
}

/*
 * Set the lexer state at the beginning of a file.
 */
void
cpp::reinit_lexer_state(struct lexer_state *ls, int wb)
{
#ifndef NO_UCPP_BUF
	ls->input_buf = wb ? new unsigned char[INPUT_BUF_MEMG] : 0;
#endif
	ls->input = 0;
	ls->ebuf = ls->pbuf = 0;
	ls->lka_rptr = 0;
	ls->lka_wptr = 0;
	ls->nlka = 0;
	ls->macfile = 0;
	ls->eof = 0;
	ls->discard = 1;
	ls->last = 0;		/* we suppose '\n' is not 0 */
	ls->line = 1;
	ls->ltwnl = 1;
	ls->oline = 1;
	ls->pending_token = 0;
	ls->cli = 0;
	ls->copy_line[COPY_LINE_LENGTH - 1] = 0;
	ls->ifnest = 0;
	ls->condf[0] = ls->condf[1] = 0;
}

/*
 * Initialize the struct lexer_state, with optional input and output buffers.
 */
void
cpp::init_buf_lexer_state(struct lexer_state *ls, int wb)
{
	reinit_lexer_state(ls, wb);
#ifndef NO_UCPP_BUF
	ls->output_buf = wb ? new unsigned char[OUTPUT_BUF_MEMG] : 0;
#endif
	ls->sbuf = 0;
	ls->output_fifo = 0;

	ls->ctok = new struct token;
	ls->tknl = TOKEN_NAME_MEMG;
	ls->ctok->name = new char[ls->tknl];
	ls->pending_token = 0;

	ls->flags = 0;
	ls->count_trigraphs = 0;
	ls->gf = new struct garbage_fifo;
	init_garbage_fifo(ls->gf);
	ls->condcomp = 1;
	ls->condnest = 0;
#ifdef INMACRO_FLAG
	ls->inmacro = 0;
	ls->macro_count = 0;
#endif
}

/*
 * Initialize the (complex) struct lexer_state.
 */
void
cpp::init_lexer_state(struct lexer_state *ls)
{
	init_buf_lexer_state(ls, 1);
	ls->input = 0;
}

/*
 * Restore what is needed from a lexer_state. This is used for #include.
 */
void
cpp::restore_lexer_state(struct lexer_state *ls, struct lexer_state *lsbak)
{
#ifndef NO_UCPP_BUF
	delete [] ls->input_buf;
	ls->input_buf = lsbak->input_buf;
#endif
	ls->input = lsbak->input;
	ls->ebuf = lsbak->ebuf;
	ls->pbuf = lsbak->pbuf;
	memcpy(ls->lka, lsbak->lka, sizeof(ls->lka));
	ls->lka_rptr = lsbak->lka_rptr;
	ls->lka_wptr = lsbak->lka_wptr;
	ls->nlka = lsbak->nlka;
	ls->eof = lsbak->eof;
	ls->discard = lsbak->discard;
	ls->line = lsbak->line;
	ls->oline = lsbak->oline;
	ls->ifnest = lsbak->ifnest;
	ls->condf[0] = lsbak->condf[0];
	ls->condf[1] = lsbak->condf[1];
}

/*
 * close input file operations on a struct lexer_state
 */
void
cpp::close_input(struct lexer_state *ls)
{
	if (ls->input)
	{
		fclose(ls->input);
		ls->input = 0;
	}
}

/*
 * init_lexer_mode() is used to end initialization of a struct lexer_state
 * if it must be used for a lexer
 */
void
cpp::init_lexer_mode(struct lexer_state *ls)
{
	ls->flags = DEFAULT_LEXER_FLAGS;
	ls->output_fifo = new struct token_fifo;
	ls->output_fifo->art = ls->output_fifo->nt = 0;
	ls->toplevel_of = ls->output_fifo;
	ls->save_ctok = ls->ctok;
}

/*
 * release memory used by a struct lexer_state; this implies closing
 * any input stream held by this structure.
 */
void
cpp::free_lexer_state(struct lexer_state *ls)
{
	close_input(ls);
	
#ifndef NO_UCPP_BUF
	if (ls->input_buf)
	{
		delete [] ls->input_buf;
		ls->input_buf = 0;
	}
	
	if (ls->output_buf)
	{
		delete [] ls->output_buf;
		ls->output_buf = 0;
	}
#endif
	if (ls->ctok && (!ls->output_fifo || ls->output_fifo->nt == 0))
	{
		delete [] ls->ctok->name;
		delete ls->ctok;
		ls->ctok = 0;
	}
	
	if (ls->gf)
	{
		free_garbage_fifo(ls->gf);
		ls->gf = 0;
	}
	
	if (ls->output_fifo)
	{
		delete ls->output_fifo;
		ls->output_fifo = 0;
	}
}

/*
 * The #if directive. This function parse the expression, performs macro
 * expansion (and handles the "defined" operator), and call eval_expr.
 * return value: 1 if the expression is true, 0 if it is false, -1 on error.
 */
int
cpp::handle_if(struct lexer_state *ls)
{
	struct token_fifo tf, tf1, tf2, tf3, *save_tf;
	long l = ls->line;
	unsigned long z;
	int ret = 0, ltww = 1;

	/* first, get the whole line */
	tf.art = tf.nt = 0;
	while (!next_token(ls) && ls->ctok->type != NEWLINE)
	{
		struct token t;

		if (ltww && ttMWS(ls->ctok->type)) continue;
		ltww = ttMWS(ls->ctok->type);
		t.type = ls->ctok->type;
		t.line = l;
		if (S_TOKEN(ls->ctok->type))
		{
			t.name = sdup(ls->ctok->name);
			throw_away(ls->gf, t.name);
		}
		aol(tf.t, tf.nt, t, TOKEN_LIST_MEMG, struct token);
	}
	if (ltww && tf.nt) if ((--tf.nt) == 0) delete tf.t;
	if (tf.nt == 0)
	{
		error(l, "void condition for a #if/#elif");
		return -1;
	}
	/* handle the "defined" operator */
	tf1.art = tf1.nt = 0;
	while (tf.art < tf.nt)
	{
		struct token *ct, rt;
		struct macro *m;
		size_t nidx, eidx;

		ct = tf.t + (tf.art++);
		if (ct->type == NAME && !strcmp(ct->name, "defined"))
		{
			if (tf.art >= tf.nt) goto store_token;
			nidx = tf.art;
			if (ttMWS(tf.t[nidx].type))
				if (++nidx >= tf.nt) goto store_token;
			if (tf.t[nidx].type == NAME)
			{
				eidx = nidx;
				goto check_macro;
			}
			if (tf.t[nidx].type != LPAR) goto store_token;
			if (++nidx >= tf.nt) goto store_token;
			if (ttMWS(tf.t[nidx].type))
				if (++nidx >= tf.nt) goto store_token;
			if (tf.t[nidx].type != NAME) goto store_token;
			eidx = nidx + 1;
			if (eidx >= tf.nt) goto store_token;
			if (ttMWS(tf.t[eidx].type))
				if (++eidx >= tf.nt) goto store_token;
			if (tf.t[eidx].type != RPAR) goto store_token;
			goto check_macro;
		}
	store_token:
		aol(tf1.t, tf1.nt, *ct, TOKEN_LIST_MEMG, struct token);
		continue;

	check_macro:
		static char *s1 = "1L";
		static char *s2 = "0L";
		m = get_macro(tf.t[nidx].name);
		rt.type = NUMBER;
		rt.name = m ? s1 : s2;
		aol(tf1.t, tf1.nt, rt, TOKEN_LIST_MEMG, struct token);
		tf.art = eidx + 1;
	}
	delete [] tf.t;
	if (tf1.nt == 0)
	{
		error(l, "void condition (after expansion) for a #if/#elif");
		return -1;
	}

	/* perform all macro substitutions */
	tf2.art = tf2.nt = 0;
	save_tf = ls->output_fifo;
	ls->output_fifo = &tf2;
	while (tf1.art < tf1.nt)
	{
		struct token *ct;

		ct = tf1.t + (tf1.art++);
		if (ct->type == NAME)
		{
			struct macro *m = get_macro(ct->name);

			if (m)
			{
				if (substitute_macro(ls, m, &tf1, 0,
#ifdef NO_PRAGMA_IN_DIRECTIVE
					1,
#else
					0,
#endif
					ct->line))
				{
					ls->output_fifo = save_tf;
					goto error1;
				}
				continue;
			}
		}
		else if ((ct->type == SHARP || ct->type == DIG_SHARP)
			 && (ls->flags & HANDLE_ASSERTIONS))
		{
			static char *s_1 = "1";
			static char *s_0 = "0";
			
			/* we have an assertion; parse it */
			int nnp, ltww = 1;
			size_t i = tf1.art;
			struct token_fifo atl;
			char *aname;
			struct assert *a;
			int av = 0;
			struct token rt;
			
			atl.art = atl.nt = 0;
			while (i < tf1.nt && ttMWS(tf1.t[i].type)) i++;
			if (i >= tf1.nt) goto assert_error;
			if (tf1.t[i].type != NAME) goto assert_error;
			aname = tf1.t[i++].name;
			while (i < tf1.nt && ttMWS(tf1.t[i].type)) i++;
			if (i >= tf1.nt) goto assert_generic;
			if (tf1.t[i].type != LPAR) goto assert_generic;
			i++;
			for (nnp = 1; nnp && i < tf1.nt; i++)
			{
				if (ltww && ttMWS(tf1.t[i].type)) continue;
				if (tf1.t[i].type == LPAR) nnp++;
				else if (tf1.t[i].type == RPAR
					&& (--nnp) == 0)
				{
					tf1.art = i + 1;
					break;
				}
				ltww = ttMWS(tf1.t[i].type);
				aol(atl.t, atl.nt, tf1.t[i], TOKEN_LIST_MEMG, struct token);
			}
			if (nnp) goto assert_error;
			if (ltww && atl.nt && (--atl.nt) == 0) delete atl.t;
			if (atl.nt == 0) goto assert_error;
			
			/* the assertion is in aname and atl; check it */
			a = get_assertion(aname);
			if (a) for (i = 0; i < a->nbval; i++)
			{
				if (!cmp_token_list(&atl, a->val + i))
				{
					av = 1;
					break;
				}
			}
			rt.type = NUMBER;
			rt.name = av ? s_1 : s_0;
			aol(tf2.t, tf2.nt, rt, TOKEN_LIST_MEMG, struct token);
			if (atl.nt) delete atl.t;
			continue;

		assert_generic:
			tf1.art = i;
			rt.type = NUMBER;
			rt.name = get_assertion(aname) ? s_1 : s_0;
			aol(tf2.t, tf2.nt, rt, TOKEN_LIST_MEMG, struct token);
			continue;

		assert_error:
			error(l, "syntax error for assertion in #if");
			ls->output_fifo = save_tf;
			goto error1;
		}
		aol(tf2.t, tf2.nt, *ct, TOKEN_LIST_MEMG, struct token);
	}
	ls->output_fifo = save_tf;
	delete [] tf1.t;
	if (tf2.nt == 0)
	{
		error(l, "void condition (after expansion) for a #if/#elif");
		return -1;
	}

	/*
	 * suppress whitespace and replace rogue identifiers by 0
	 */
	tf3.art = tf3.nt = 0;
	while (tf2.art < tf2.nt)
	{
		struct token *ct = tf2.t + (tf2.art++);

		if (ttMWS(ct->type)) continue;
		if (ct->type == NAME)
		{
			/*
			 * a rogue identifier; we replace it with "0".
			 */
			struct token rt;

			rt.type = NUMBER;
			rt.name = "0";
			aol(tf3.t, tf3.nt, rt, TOKEN_LIST_MEMG, struct token);
			continue;
		}
		aol(tf3.t, tf3.nt, *ct, TOKEN_LIST_MEMG, struct token);
	}
	delete [] tf2.t;

	if (tf3.nt == 0)
	{
		error(l, "void condition (after expansion) for a #if/#elif");
		return -1;
	}
	eval_line = l;
	z = eval_expr(&tf3, &ret, (ls->flags & WARN_STANDARD) != 0);
	delete [] tf3.t;
	if (ret) return -1;
	return (z != 0);

error1:
	if (tf1.nt) delete tf1.t;
	if (tf2.nt) delete tf2.t;
	return -1;
}

/*
 * A #include was found; parse the end of line, replace macros if
 * necessary.
 *
 * If nex is set to non-zero, the directive is considered as a #include_next
 * (extension to C99, mimicked from GNU)
 */
int
cpp::handle_include(struct lexer_state *ls, unsigned long flags, int nex)
{
	int c, string_fname = 0;
	char *fname;
	unsigned char *fname2;
	size_t fname_ptr = 0;
	long l = ls->line;
	int x, y;
	FILE *f;
	struct token_fifo tf, tf2, *save_tf;
	size_t nl;
	int tgd;
	struct lexer_state alt_ls;

#define left_angle(t)	((t) == LT || (t) == LEQ || (t) == LSH \
			|| (t) == ASLSH || (t) == DIG_LBRK || (t) == LBRA)
#define right_angle(t)	((t) == GT || (t) == RSH || (t) == ARROW \
			|| (t) == DIG_RBRK || (t) == DIG_RBRA)

	while ((c = grap_char(ls)) >= 0 && c != '\n')
	{
		if (space_char(c))
		{
			discard_char(ls);
			continue;
		}
		if (c == '<')
		{
			discard_char(ls);
			while ((c = grap_char(ls)) >= 0)
			{
				discard_char(ls);
				if (c == '\n') goto include_last_chance;
				if (c == '>') break;
				aol(fname, fname_ptr, (char)c, FNAME_MEMG, char);
			}
			aol(fname, fname_ptr, (char)0, FNAME_MEMG, char);
			string_fname = 0;
			goto do_include;
		}
		else if (c == '"')
		{
			discard_char(ls);
			while ((c = grap_char(ls)) >= 0)
			{
				discard_char(ls);
				if (c == '\n')
				{
				/* macro replacements won't save that one */
					if (fname_ptr) delete fname;
					goto include_error;
				}
				if (c == '"') break;
				aol(fname, fname_ptr, (char)c, FNAME_MEMG, char);
			}
			aol(fname, fname_ptr, (char)0, FNAME_MEMG, char);
			string_fname = 1;
			goto do_include;
		}
		goto include_macro;
	}

include_last_chance:
	/*
	 * We found a '<' but not the trailing '>'; so we tokenize the
	 * line, and try to act upon it. The standard lets us free in that
	 * matter, and no sane programmer would use such a construct, but
	 * it is no reason not to support it.
	 */
	if (fname_ptr == 0)
		goto include_error;
	
	fname2 = new unsigned char[fname_ptr + 1];
	memcpy(fname2 + 1, fname, fname_ptr);
	fname2[0] = '<';
	
	/*
	 * We merely copy the lexer_state structure; this should be ok,
	 * since we do want to share the memory structure (garbage_fifo),
	 * and do not touch any other context-full thing.
	 */
	alt_ls = *ls;
	alt_ls.input = 0;
	alt_ls.input_string = fname2;
	alt_ls.pbuf = 0;
	alt_ls.ebuf = fname_ptr + 1;
	tf.art = tf.nt = 0;
	while (!next_token(&alt_ls))
	{
		if (!ttMWS(alt_ls.ctok->type))
		{
			struct token t;

			t.type = alt_ls.ctok->type;
			t.line = l;
			if (S_TOKEN(alt_ls.ctok->type))
			{
				t.name = sdup(alt_ls.ctok->name);
				throw_away(alt_ls.gf, t.name);
			}
			aol(tf.t, tf.nt, t, TOKEN_LIST_MEMG, struct token);
		}
	}
	delete fname2;
	if (alt_ls.pbuf < alt_ls.ebuf) goto include_error;
		/* tokenizing failed */
	goto include_macro2;
	
include_error:
	error(l, "invalid '#include'");
	return 1;

include_macro:
	tf.art = tf.nt = 0;
	while (!next_token(ls) && ls->ctok->type != NEWLINE)
	{
		if (!ttMWS(ls->ctok->type))
		{
			struct token t;

			t.type = ls->ctok->type;
			t.line = l;
			if (S_TOKEN(ls->ctok->type))
			{
				t.name = sdup(ls->ctok->name);
				throw_away(ls->gf, t.name);
			}
			aol(tf.t, tf.nt, t, TOKEN_LIST_MEMG, struct token);
		}
	}
include_macro2:
	tf2.art = tf2.nt = 0;
	save_tf = ls->output_fifo;
	ls->output_fifo = &tf2;
	while (tf.art < tf.nt)
	{
		struct token *ct;

		ct = tf.t + (tf.art++);
		if (ct->type == NAME)
		{
			struct macro *m = get_macro(ct->name);
			if (m)
			{
				if (substitute_macro(ls, m, &tf, 0,
#ifdef NO_PRAGMA_IN_DIRECTIVE
					1,
#else
					0,
#endif
					ct->line))
				{
					ls->output_fifo = save_tf;
					return -1;
				}
				continue;
			}
		}
		aol(tf2.t, tf2.nt, *ct, TOKEN_LIST_MEMG, struct token);
	}
	delete tf.t;
	ls->output_fifo = save_tf;
	for (x = 0; x < tf2.nt && ttWHI(tf2.t[x].type); x++);
	for (y = tf2.nt - 1; y >= 0 && ttWHI(tf2.t[y].type); y--);
	if (x >= tf2.nt) goto include_macro_err;
	if (tf2.t[x].type == STRING)
	{
		if (y != x) goto include_macro_err;
		if (tf2.t[x].name[0] == 'L')
		{
			if (ls->flags & WARN_STANDARD)
				warning(l, "wide string for #include");
			fname = sdup(tf2.t[x].name);
			nl = strlen(fname);
			*(fname + nl - 1) = 0;
			memmove(fname, fname + 2, nl - 2);
		}
		else
		{
			fname = sdup(tf2.t[x].name);
			nl = strlen(fname);
			*(fname + nl - 1) = 0;
			memmove(fname, fname + 1, nl - 1);
		}
		string_fname = 1;
	}
	else if (left_angle(tf2.t[x].type) && right_angle(tf2.t[y].type))
	{
		int i, j;

		if (ls->flags & WARN_ANNOYING)
			warning(l, "reconstruction "
				      "of <foo> in #include");
		for (j = 0, i = x; i <= y; i++) if (!ttWHI(tf2.t[i].type))
			j += strlen(tname(tf2.t[i]));
		fname = new char[j + 1];
		for (j = 0, i = x; i <= y; i++)
		{
			if (ttWHI(tf2.t[i].type)) continue;
			strcpy(fname + j, tname(tf2.t[i]));
			j += strlen(tname(tf2.t[i]));
		}
		*(fname + j - 1) = 0;
		memmove(fname, fname + 1, j);
		string_fname = 0;
	}
	else goto include_macro_err;
	delete tf2.t;
	goto do_include_next;

include_macro_err:
	error(l, "macro expansion did not produce a valid filename "
		    "for #include");
	if (tf2.nt) delete tf2.t;
	return 1;

do_include:
	tgd = 1;
	while (!next_token(ls))
	{
		if (tgd && !ttWHI(ls->ctok->type)
		    && (ls->flags & WARN_STANDARD))
		{
			warning(l, "trailing garbage in #include");
			tgd = 0;
		}
		if (ls->ctok->type == NEWLINE)
			break;
	}

	/* the increment of ls->line is intended so that the line
	   numbering is reported correctly in report_context() even if
	   the #include is at the end of the file with no trailing newline */
	if (ls->ctok->type != NEWLINE)
		ls->line++;
	
do_include_next:
	if (!(ls->flags & LEXER) && (ls->flags & KEEP_OUTPUT))
		put_char(ls, '\n');
	push_file_context(ls);
	reinit_lexer_state(ls, 1);
#ifdef MSDOS
	/* on msdos systems, replace all / by \ */
	{
		char *d;

		for (d = fname; *d; d++)
			if (*d == '/')
				*d = '\\';
	}
#endif
	f = nex ? find_file_next(fname) : find_file(fname, string_fname);
	if (!f)
	{
		current_filename = 0;
		pop_file_context(ls);
		if (find_file_error == FF_ERROR)
		{
			error(l, "file '%s' not found", fname);
			delete fname;
			return 1;
		}
		/* file was found, but it is useless to include it again */
		delete fname;
		return 0;
	}
	ls->input = f;
	current_filename = fname;
	enter_file(ls, flags);
	included_files.insert(current_long_filename);
	return 0;

#undef left_angle
#undef right_angle
}

/*
 * for #line directives
 */
int
cpp::handle_line(struct lexer_state *ls, unsigned long flags)
{
	char *fname;
	long l = ls->line;
	struct token_fifo tf, tf2, *save_tf;
	size_t nl, j;
	unsigned long z;

	tf.art = tf.nt = 0;
	while (!next_token(ls) && ls->ctok->type != NEWLINE)
	{
		if (!ttMWS(ls->ctok->type))
		{
			struct token t;

			t.type = ls->ctok->type;
			t.line = l;
			if (S_TOKEN(ls->ctok->type))
			{
				t.name = sdup(ls->ctok->name);
				throw_away(ls->gf, t.name);
			}
			aol(tf.t, tf.nt, t, TOKEN_LIST_MEMG, struct token);
		}
	}
	tf2.art = tf2.nt = 0;
	save_tf = ls->output_fifo;
	ls->output_fifo = &tf2;
	while (tf.art < tf.nt)
	{
		struct token *ct;

		ct = tf.t + (tf.art++);
		if (ct->type == NAME)
		{
			struct macro *m = get_macro(ct->name);
			if (m)
			{
				if (substitute_macro(ls, m, &tf, 0,
#ifdef NO_PRAGMA_IN_DIRECTIVE
					1,
#else
					0,
#endif
					ct->line))
				{
					ls->output_fifo = save_tf;
					return -1;
				}
				continue;
			}
		}
		aol(tf2.t, tf2.nt, *ct, TOKEN_LIST_MEMG, struct token);
	}
	delete tf.t;
	
	for (tf2.art = 0; tf2.art < tf2.nt && ttWHI(tf2.t[tf2.art].type); tf2.art++)
		;
	
	ls->output_fifo = save_tf;
	
	if (tf2.art == tf2.nt || (tf2.t[tf2.art].type != NUMBER
	    && tf2.t[tf2.art].type != CHAR))
	{
		error(l, "not a valid number for #line");
		goto line_macro_err;
	}
	
	for (j = 0; tf2.t[tf2.art].name[j]; j++)
	{
		if (tf2.t[tf2.art].name[j] < '0'
		    || tf2.t[tf2.art].name[j] > '9')
		{
			if (ls->flags & WARN_STANDARD)
				warning(l, "non-standard line number in #line");
		}
	}
	{
		bool flag = false;
		
		try
		{
			z = strtoconst(tf2.t[tf2.art].name);
			if (j > 10 || z > 2147483647U)
			{
				error(l, "out-of-bound line number for #line");
				flag = true;
			}
		}
		catch (...)
		{
			flag = true;
		}
		
		if (flag)
			goto line_macro_err;
	}
	
	ls->oline = ls->line = z;
	if ((++tf2.art) < tf2.nt)
	{
		int i;

		for (i = tf2.art; i < tf2.nt && ttMWS(tf2.t[i].type); i++)
			;
		
		if (i < tf2.nt)
		{
			if (tf2.t[i].type != STRING)
			{
				error(l, "not a valid filename for #line");
				goto line_macro_err;
			}
			
			if (tf2.t[i].name[0] == 'L')
			{
				if (ls->flags & WARN_STANDARD)
					warning(l, "wide string for #line");
				
				fname = sdup(tf2.t[i].name);
				nl = strlen(fname);
				*(fname + nl - 1) = 0;
				memmove(fname, fname + 2, nl - 2);
			}
			else
			{
				fname = sdup(tf2.t[i].name);
				nl = strlen(fname);
				*(fname + nl - 1) = 0;
				memmove(fname, fname + 1, nl - 1);
			}
			
			if (current_filename)
				delete current_filename;
			
			current_filename = fname;
		}
		
		for (i++; i < tf2.nt && ttMWS(tf2.t[i].type); i++)
			;
		
		if (i < tf2.nt && (ls->flags & WARN_STANDARD))
			warning(l, "trailing garbage in #line");
		
	}
	delete tf2.t;
	
	enter_file(ls, flags);
	return 0;
	
line_macro_err:
	
	if (tf2.nt)
		delete tf2.t;
	
	return 1;
}

/*
 * a #error directive: we emit the message without any modification
 * (except the usual backslash+newline and trigraphs)
 */
void
cpp::handle_error(struct lexer_state *ls)
{
	int c;
	size_t p = 0, lp = 128;
	long l = ls->line;
	unsigned char *buf = new unsigned char[lp];

	while ((c = grap_char(ls)) >= 0 && c != '\n')
	{
		discard_char(ls);
		wan(buf, p, (unsigned char)c, lp, unsigned char);
	}
	wan(buf, p, 0, lp, unsigned char);
	error(l, "#error%s", buf);
	delete buf;
}

/*
 * convert digraph tokens to their standard equivalent.
 */
int
cpp::undig(int type)
{
	static int ud[6] = { LBRK, RBRK, LBRA, RBRA, SHARP, DSHARP };

	return ud[type - DIG_LBRK];
}

#ifdef PRAGMA_TOKENIZE
/*
 * Make a compressed representation of a token list; the contents of
 * the token_fifo are freed. Values equal to 0 are replaced by
 * PRAGMA_TOKEN_END (by default, (unsigned char)'\n') and the compressed
 * string is padded by a 0 (so that it may be * handled like a string).
 * Digraph tokens are replaced by their non-digraph equivalents.
 */
struct comp_token_fifo
cpp::compress_token_list(struct token_fifo *tf)
{
	struct comp_token_fifo ct;
	size_t l;

	for (l = 0, tf->art = 0; tf->art < tf->nt; tf->art++)
	{
		l++;
		if (S_TOKEN(tf->t[tf->art].type))
			l += strlen(tf->t[tf->art].name) + 1;
	}
	ct.length = l;
	ct.t = new unsigned char[ct.length + 1];
	for (l = 0, tf->art = 0; tf->art < tf->nt; tf->art++)
	{
		int tt = tf->t[tf->art].type;

		if (tt == 0) tt = PRAGMA_TOKEN_END;
		if (tt > DIGRAPH_TOKENS && tt < DIGRAPH_TOKENS_END)
			tt = undig(tt);
		ct.t[l++] = tt;
		if (S_TOKEN(tt))
		{
			char *tn = tf->t[tf->art].name;
			size_t sl = strlen(tn);

			memcpy(ct.t + l, tn, sl);
			l += sl;
			ct.t[l++] = PRAGMA_TOKEN_END;
			delete tn;
		}
	}
	ct.t[l] = 0;
	if (tf->nt) delete tf->t;
	ct.rp = 0;
	
	return ct;
}
#endif

/*
 * A #pragma directive: we make a PRAGMA token containing the rest of
 * the line.
 *
 * We strongly hope that we are called only in LEXER mode.
 */
void
cpp::handle_pragma(struct lexer_state *ls)
{
	unsigned char *buf;
	struct token t;
	long l = ls->line;

#ifdef PRAGMA_TOKENIZE
	struct token_fifo tf;

	tf.art = tf.nt = 0;
	while (!next_token(ls) && ls->ctok->type != NEWLINE)
		if (!ttMWS(ls->ctok->type)) break;
	if (ls->ctok->type != NEWLINE)
	{
		do {
			struct token t;

			t.type = ls->ctok->type;
			if (ttMWS(t.type)) continue;
			if (S_TOKEN(t.type)) t.name = sdup(ls->ctok->name);
			aol(tf.t, tf.nt, t, TOKEN_LIST_MEMG, struct token);
		}
		while (!next_token(ls) && ls->ctok->type != NEWLINE);
	}
	if (tf.nt == 0)
		/* void pragma are silently ignored */
		return;
	
	buf = (compress_token_list(&tf)).t;
#else
	int c, x = 1, y = 32;

	while ((c = grap_char(ls)) >= 0 && c != '\n')
	{
		discard_char(ls);
		if (!space_char(c))
			break;
	}
	/* void #pragma are ignored */
	if (c == '\n') return;
	buf = new char[y];
	buf[0] = c;
	while ((c = grap_char(ls)) >= 0 && c != '\n')
	{
		discard_char(ls);
		wan(buf, x, c, y);
	}
	for (x--; x >= 0 && space_char(buf[x]); x--);
	x++;
	wan(buf, x, 0, y);
#endif
	t.type = PRAGMA;
	t.line = l;
	t.name = (char *)buf;
	aol(ls->output_fifo->t, ls->output_fifo->nt, t, TOKEN_LIST_MEMG, struct token);
	throw_away(ls->gf, (char *)buf);
}

/*
 * We saw a # at the beginning of a line (or preceeded only by whitespace).
 * We check the directive name and act accordingly.
 */
int
cpp::handle_cpp(struct lexer_state *ls, int sharp_type)
{
#define condfset(x)	do { \
		ls->condf[(x) / 32] |= 1UL << ((x) % 32); \
	} while (0)
#define condfclr(x)	do { \
		ls->condf[(x) / 32] &= ~(1UL << ((x) % 32)); \
	} while (0)
#define condfval(x)	((ls->condf[(x) / 32] & (1UL << ((x) % 32))) != 0)

	long l = ls->line;
	unsigned long save_flags = ls->flags;
	int ret = 0;

	save_flags = ls->flags;
	ls->flags |= LEXER;
	while (!next_token(ls))
	{
		int t = ls->ctok->type;

		switch (t)
		{
		case COMMENT:
			if (ls->flags & WARN_ANNOYING)
				warning(l, "comment in the middle of a cpp directive");
			
			/* fall through */
		case NONE:
			continue;
		case NEWLINE:
			/* null directive */
			if (ls->flags & WARN_ANNOYING)
				/* truly an annoying warning; null directives
				   are rare but may increase readability of
				   some source files, and they are legal */
				warning(l, "null cpp directive");
			
			if (!(ls->flags & LEXER))
				put_char(ls, '\n');
			goto handle_exit2;
		
		case NAME:
			break;
		default:
			if (ls->flags & FAIL_SHARP)
			{
				if (ls->condcomp)
				{
					error(l, "rogue '#'");
					ret = 1;
				}
				else
				{
					if (ls->flags & WARN_STANDARD)
					{
						warning(l, "rogue '#' in code compiled out");
						ret = 0;
					}
				}
				ls->flags = save_flags;
				goto handle_warp_ign;
			}
			else
			{
				struct token u;

				u.type = sharp_type;
				u.line = l;
				ls->flags = save_flags;
				print_token(ls, &u, 0);
				print_token(ls, ls->ctok, 0);
				if (ls->flags & WARN_ANNOYING)
					warning(l, "rogue '#' dumped");
				
				goto handle_exit3;
			}
		}
		if (ls->condcomp)
		{
			if (!strcmp(ls->ctok->name, "define"))
			{
				ret = handle_define(ls);
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "undef"))
			{
				ret = handle_undef(ls);
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "if"))
			{
				if ((++ls->ifnest) > 63)
					goto too_many_if;
				condfclr(ls->ifnest - 1);
				ret = handle_if(ls);
				if (ret > 0) ret = 0;
				else if (ret == 0)
				{
					ls->condcomp = 0;
					ls->condmet = 0;
					ls->condnest = ls->ifnest - 1;
				}
				else ret = 1;
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "ifdef"))
			{
				if ((++ls->ifnest) > 63) goto too_many_if;
				condfclr(ls->ifnest - 1);
				ret = handle_ifdef(ls);
				if (ret > 0) ret = 0;
				else if (ret == 0)
				{
					ls->condcomp = 0;
					ls->condmet = 0;
					ls->condnest = ls->ifnest - 1;
				}
				else ret = 1;
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "ifndef"))
			{
				if ((++ls->ifnest) > 63) goto too_many_if;
				condfclr(ls->ifnest - 1);
				ret = handle_ifndef(ls);
				if (ret > 0) ret = 0;
				else if (ret == 0) {
					ls->condcomp = 0;
					ls->condmet = 0;
					ls->condnest = ls->ifnest - 1;
				}
				else ret = 1;
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "else"))
			{
				if (ls->ifnest == 0
				    || condfval(ls->ifnest - 1))
				{
					error(l, "rogue #else");
					ret = 1;
					goto handle_warp;
				}
				condfset(ls->ifnest - 1);
				if (ls->ifnest == 1)
					protect_detect.state = 0;
				ls->condcomp = 0;
				ls->condmet = 1;
				ls->condnest = ls->ifnest - 1;
				goto handle_warp;
			}
			else if (!strcmp(ls->ctok->name, "elif"))
			{
				if (ls->ifnest == 0
				    || condfval(ls->ifnest - 1))
				{
					error(l, "rogue #elif");
					ret = 1;
					goto handle_warp_ign;
				}
				if (ls->ifnest == 1)
					protect_detect.state = 0;
				ls->condcomp = 0;
				ls->condmet = 1;
				ls->condnest = ls->ifnest - 1;
				goto handle_warp_ign;
			}
			else if (!strcmp(ls->ctok->name, "endif"))
			{
				if (ls->ifnest == 0)
				{
					error(l, "unmatched #endif");
					ret = 1;
					goto handle_warp;
				}
				if ((--ls->ifnest) == 0
				    && protect_detect.state == 2)
					protect_detect.state = 3;
				
				goto handle_warp;
			}
			else if (!strcmp(ls->ctok->name, "include"))
			{
				ret = handle_include(ls, save_flags, 0);
				goto handle_exit3;
			}
			else if (!strcmp(ls->ctok->name, "include_next"))
			{
				ret = handle_include(ls, save_flags, 1);
				goto handle_exit3;
			}
			else if (!strcmp(ls->ctok->name, "pragma"))
			{
				if (!(save_flags & LEXER))
				{
#ifdef PRAGMA_DUMP
					/* dump #pragma in output */
					struct token u;

					u.type = sharp_type;
					u.line = l;
					ls->flags = save_flags;
					print_token(ls, &u, 0);
					print_token(ls, ls->ctok, 0);
					while (ls->flags |= LEXER, !next_token(ls))
					{
						long save_line;

						ls->flags &= ~LEXER;
						save_line = ls->line;
						ls->line = l;
						print_token(ls, ls->ctok, 0);
						ls->line = save_line;
						if (ls->ctok->type == NEWLINE)
							break;
					}
					goto handle_exit3;
#else
					if (ls->flags & WARN_PRAGMA)
						warning(l, "#pragma ignored and not dumped");
					
					goto handle_warp_ign;
#endif
				}
				if (!(ls->flags & HANDLE_PRAGMA))
					goto handle_warp_ign;
				
				handle_pragma(ls);
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "error"))
			{
				ret = 1;
				handle_error(ls);
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "line"))
			{
				ret = handle_line(ls, save_flags);
				goto handle_exit;
			}
			else if ((ls->flags & HANDLE_ASSERTIONS)
				 && !strcmp(ls->ctok->name, "assert"))
			{
				ret = handle_assert(ls);
				goto handle_exit;
			}
			else if ((ls->flags & HANDLE_ASSERTIONS)
				 && !strcmp(ls->ctok->name, "unassert"))
			{
				ret = handle_unassert(ls);
				goto handle_exit;
			}
		}
		else
		{
			if (!strcmp(ls->ctok->name, "else"))
			{
				if (condfval(ls->ifnest - 1)
				    && (ls->flags & WARN_STANDARD))
					warning(l, "rogue #else in code compiled out");
				
				if (ls->condnest == ls->ifnest - 1)
					if (!ls->condmet)
						ls->condcomp = 1;
				
				condfset(ls->ifnest - 1);
				if (ls->ifnest == 1)
					protect_detect.state = 0;
				
				goto handle_warp;
			}
			else if (!strcmp(ls->ctok->name, "elif"))
			{
				if (condfval(ls->ifnest - 1)
				    && (ls->flags & WARN_STANDARD))
					warning(l, "rogue #elif in code compiled out");
				
				if (ls->condnest != ls->ifnest - 1
				   || ls->condmet)
					goto handle_warp_ign;
				
				if (ls->ifnest == 1)
					protect_detect.state = 0;
				
				ret = handle_if(ls);
				if (ret > 0)
				{
					ls->condcomp = 1;
					ls->condmet = 1;
					ret = 0;
				}
				else if (ret < 0)
					ret = 1;
				
				goto handle_exit;
			}
			else if (!strcmp(ls->ctok->name, "endif"))
			{
				if ((--ls->ifnest) == ls->condnest)
				{
					if (ls->ifnest == 0
					    && protect_detect.state == 2)
						protect_detect.state = 3;
					ls->condcomp = 1;
				}
				
				goto handle_warp;
			}
			else if (!strcmp(ls->ctok->name, "if")
				|| !strcmp(ls->ctok->name, "ifdef")
				|| !strcmp(ls->ctok->name, "ifndef"))
			{
				if ((++ls->ifnest) > 63)
					goto too_many_if;
				
				condfclr(ls->ifnest - 1);
			}
			
			goto handle_warp_ign;
		}
		/*
		 * Unrecognized directive. We emit either an error or
		 * an annoying warning, depending on a command-line switch.
		 */
		if (ls->flags & FAIL_SHARP)
		{
			error(l, "unknown cpp directive '#%s'", ls->ctok->name);
			goto handle_warp_ign;
		}
		else
		{
			struct token u;

			u.type = sharp_type;
			u.line = l;
			ls->flags = save_flags;
			print_token(ls, &u, 0);
			print_token(ls, ls->ctok, 0);
			
			if (ls->flags & WARN_ANNOYING)
				warning(l, "rogue '#' dumped");
		}
	}
	return 1;

handle_warp_ign:
	while (!next_token(ls))
		if (ls->ctok->type == NEWLINE)
			break;
	
	goto handle_exit;
	
handle_warp:
	while (!next_token(ls))
	{
		if (!ttWHI(ls->ctok->type) && (ls->flags & WARN_STANDARD))
			warning(l, "trailing garbage in preprocessing directive");
		
		if (ls->ctok->type == NEWLINE)
			break;
	}
	
handle_exit:
	if (!(ls->flags & LEXER))
		put_char(ls, '\n');
	
handle_exit3:
	if (protect_detect.state == 1)
		protect_detect.state = 0;
	else if (protect_detect.state == -1)
		/* just after the #include */
		protect_detect.state = 1;
	
handle_exit2:
	ls->flags = save_flags;
	return ret;
	
too_many_if:
	error(l, "too many levels of conditional inclusion (max 63)");
	ret = 1;
	goto handle_warp;
	
#undef condfset
#undef condfclr
#undef condfval
}

/*
 * This is the main entry function. It maintains count of #, and call the
 * appropriate functions when it encounters a cpp directive or a macro
 * name.
 * return value: positive on error; CPPERR_EOF means "end of input reached"
 */
int
cpp::run_cpp(struct lexer_state *ls)
{
	int r = 0;
	
	while (next_token(ls))
	{
		if (protect_detect.state == 3)
		{
			/*
			 * At that point, protect_detect.ff->protect might
			 * be non-zero, if the file has been recursively
			 * included, and a guardian detected.
			 */
			if (!protect_detect.ff->protect)
			{
				/* Cool ! A new guardian has been detected. */
				protect_detect.ff->protect = protect_detect.macro;
			}
			else if (protect_detect.macro)
			{
				/* We found a guardian but an old one. */
				delete protect_detect.macro;
			}
			protect_detect.macro = 0;
		}
		
		if (ls->ifnest)
		{
			error(ls->line, "unterminated #if construction (depth %ld)", ls->ifnest);
			r = CPPERR_NEST;
		}
		
		if (ls_depth == 0)
			return CPPERR_EOF;
		
		close_input(ls);
		
		if (!(ls->flags & LEXER) && !ls->ltwnl)
			put_char(ls, '\n');
		
		pop_file_context(ls);
		ls->oline++;
		
		if (enter_file(ls, ls->flags))
			break;
	}
	
	if (!(ls->ltwnl && (ls->ctok->type == SHARP
	    || ls->ctok->type == DIG_SHARP))
	    && protect_detect.state == 1 && !ttWHI(ls->ctok->type))
	{
		/* the first non-whitespace token encountered is not
		   a sharp introducing a cpp directive */
		protect_detect.state = 0;
	}
	
	if (protect_detect.state == 3 && !ttWHI(ls->ctok->type))
	{
		/* a non-whitespace token encountered after the #endif */
		protect_detect.state = 0;
	}
	
	if (ls->condcomp)
	{
		if (ls->ltwnl && (ls->ctok->type == SHARP
		    || ls->ctok->type == DIG_SHARP))
		{
			int x = handle_cpp(ls, ls->ctok->type);

			ls->ltwnl = 1;
			return r ? r : x;
		}
		
		if (ls->ctok->type == NAME)
		{
			struct macro *m;

			if ((m = get_macro(ls->ctok->name)) != 0)
			{
				int x;
				
				x = substitute_macro(ls, m, 0, 1, 0, ls->ctok->line);
				
				if (!(ls->flags & LEXER))
					garbage_collect(ls->gf);
				
				return r ? r : x;
			}
			
			if (!(ls->flags & LEXER))
				print_token(ls, ls->ctok, 0);
		}
	}
	else
	{
		if (ls->ltwnl && (ls->ctok->type == SHARP
			|| ls->ctok->type == DIG_SHARP))
		{
			int x = handle_cpp(ls, ls->ctok->type);
			
			ls->ltwnl = 1;
			return r ? r : x;
		}
	}
	
	if (ls->ctok->type == NEWLINE)
		ls->ltwnl = 1;
	else if (!ttWHI(ls->ctok->type))
		ls->ltwnl = 0;
	
	return r ? r : -1;
}

/*
 * llex() and lex() are the lexing functions, when the preprocessor is
 * linked to another code. llex() should be called only by lex().
 */
int
cpp::llex(struct lexer_state *ls)
{
	struct token_fifo *tf = ls->output_fifo;
	int r;

	if (tf->nt != 0)
	{
		if (tf->art < tf->nt)
		{
#ifdef INMACRO_FLAG
			if (!ls->inmacro)
			{
				ls->inmacro = 1;
				ls->macro_count++;
			}
#endif
			ls->ctok = tf->t + (tf->art++);
			if (ls->ctok->type > DIGRAPH_TOKENS
			    && ls->ctok->type < DIGRAPH_TOKENS_END)
				ls->ctok->type = undig(ls->ctok->type);
			
			return 0;
		}
		else
		{
#ifdef INMACRO_FLAG
			ls->inmacro = 0;
#endif
			delete tf->t;
			tf->art = tf->nt = 0;
			garbage_collect(ls->gf);
			ls->ctok = ls->save_ctok;
		}
	}
	
	r = run_cpp(ls);
	if (ls->ctok->type > DIGRAPH_TOKENS
	    && ls->ctok->type < LAST_MEANINGFUL_TOKEN)
		ls->ctok->type = undig(ls->ctok->type);
	
	if (r > 0) return r;
	if (r < 0) return 0;
	
	return llex(ls);
}

/*
 * lex() reads the next token from the processed stream and stores it
 * into ls->ctok.
 * return value: non zero on error (including CPPERR_EOF, which is not
 * quite an error)
 */
int
cpp::lex(struct lexer_state *ls)
{
	int r;

	do {
		r = llex(ls);
#ifdef SEMPER_FIDELIS
	}
	while (!r && !ls->condcomp);
#else
	}
	while (!r && (!ls->condcomp || (ttWHI(ls->ctok->type) &&
		(!(ls->flags & LINE_NUM) || ls->ctok->type != NEWLINE))));
#endif
	return r;
}

} /* namespace ucpp */
