
/*
 * $Id: file.cxx,v 1.4 2006/07/17 12:15:21 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "port.h"

#include "cpp.h"
#include "internal.h"


namespace ucpp
{

void
cpp::push_file_context(struct lexer_state *ls)
{
	struct file_context fc;

	fc.name = current_filename;
	fc.long_name = current_long_filename;
	fc.incdir = current_incdir;
	memcpy(&(fc.ls), ls, sizeof(struct lexer_state));
	aol(ls_stack, ls_depth, fc, LS_STACK_MEMG, struct file_context);
	ls_depth--;
	aol(protect_detect_stack, ls_depth, protect_detect, LS_STACK_MEMG, struct protect);
	protect_detect.macro = 0;
}

void
cpp::pop_file_context(struct lexer_state *ls)
{
#ifdef AUDIT
	if (ls_depth <= 0)
		fatal("prepare to meet thy creator");
#endif
	close_input(ls);
	
	restore_lexer_state(ls, &(ls_stack[--ls_depth].ls));
	
	if (protect_detect.macro)
		delete [] protect_detect.macro;
	
	protect_detect = protect_detect_stack[ls_depth];
	
	if (current_filename)
		delete [] current_filename;
	
	current_filename = ls_stack[ls_depth].name;
	current_long_filename = ls_stack[ls_depth].long_name;
	current_incdir = ls_stack[ls_depth].incdir;
	
	if (ls_depth == 0)
	{
		delete [] ls_stack;
		delete [] protect_detect_stack;
	}
}

/*
 * report_context() returns the list of successive includers of the
 * current file, ending with a dummy entry with a negative line number.
 * The caller is responsible for freeing the returned pointer.
 */
struct stack_context *
cpp::report_context(void)
{
	struct stack_context *sc;
	size_t i;

	sc = new struct stack_context[ls_depth + 1];
	for (i = 0; i < ls_depth; i++)
	{
		sc[i].name = ls_stack[ls_depth - i - 1].name;
		sc[i].long_name = ls_stack[ls_depth - i - 1].long_name;
		sc[i].line = ls_stack[ls_depth - i - 1].ls.line - 1;
	}
	sc[ls_depth].line = -1;
	
	return sc;
}


static struct found_file *
new_found_file(void)
{
	struct found_file *ff = new struct found_file;

	ff->name = ff->long_name = 0;
	ff->protect = 0;
	return ff;
}

static void
del_found_file(struct found_file *ff)
{
	if (ff->name) delete [] ff->name;
	if (ff->long_name) delete [] ff->long_name;
	if (ff->protect) delete [] ff->protect;
	delete ff;
}

static struct found_file_sys *
new_found_file_sys(void)
{
	struct found_file_sys *ffs = new struct found_file_sys;
	
	ffs->name = 0;
	ffs->rff = 0;
	ffs->incdir = -1;
	return ffs;
}

static void
del_found_file_sys(struct found_file_sys *ffs)
{
	if (ffs->name)
		delete [] ffs->name;
	
	delete ffs;
}

void
cpp::set_init_filename(const char *x, int real_file)
{
	included_files.clear();
	
	if (current_filename)
		delete [] current_filename;
	
	current_filename = sdup(x);
	current_long_filename = 0;
	current_incdir = -1;
	
	if (real_file)
	{
		protect_detect.macro = 0;
		protect_detect.state = 1;
		protect_detect.ff = new_found_file();
		protect_detect.ff->name = sdup(x);
		protect_detect.ff->long_name = sdup(x);
		
		found_files->install(protect_detect.ff->long_name, protect_detect.ff);
		
		included_files.insert(current_filename);
	}
	else
		protect_detect.state = 0;
}

void
cpp::init_found_files(void)
{
	if (found_files)
		delete found_files;
	
	found_files = new hashtable<const char *, struct found_file>(del_found_file, 10);
	
	if (found_files_sys)
		delete found_files_sys;
	
	found_files_sys = new hashtable<const char *, struct found_file_sys>(del_found_file_sys, 10);
}

/*
 * Print line information.
 */
void
cpp::print_line_info(struct lexer_state *ls, unsigned long flags)
{
	char *fn = current_long_filename ? current_long_filename : current_filename;
	char *b, *d;
	size_t len;
	
	len = strlen(fn) + 50;
	b = new char [len];
	if (flags & GCC_LINE_NUM)
		snprintf(b, len, "# %ld \"%s\"\n", ls->line, fn);
	else
		snprintf(b, len, "#line %ld \"%s\"\n", ls->line, fn);
	
	for (d = b; *d; d++)
		put_char(ls, (unsigned char)(*d));
	
	delete [] b;
}

/*
 * Enter a file; this implies the possible emission of a #line directive.
 * The flags used are passed as second parameter instead of being
 * extracted from the struct lexer_state.
 *
 * As a command-line option, gcc-like directives (with only a '#',
 * without 'line') may be produced.
 *
 * enter_file() returns 1 if a (CONTEXT) token was produced, 0 otherwise.
 */
int
cpp::enter_file(struct lexer_state *ls, unsigned long flags)
{
	char *fn = current_long_filename ?
		current_long_filename : current_filename;

	if (!(flags & LINE_NUM))
		return 0;
	
	if ((flags & LEXER) && !(flags & TEXT_OUTPUT))
	{
		struct token t;

		t.type = CONTEXT;
		t.line = ls->line;
		t.name = fn;
		print_token(ls, &t, 0);
		return 1;
	}
	
	print_line_info(ls, flags);
	ls->oline--;	/* emitted #line troubled oline */
	
	return 0;
}

FILE *
cpp::find_file(const char *name, int localdir)
{
	FILE *f;
	int i, incdir = -1;
	size_t nl = strlen(name);
	char *s = 0;
	struct found_file *ff = 0, *nff;
	int lf = 0;
	int nffa = 0;

	find_file_error = FF_ERROR;
	protect_detect.state = -1;
	protect_detect.macro = 0;
	
	if (localdir)
	{
		int i;
		char *rfn = current_long_filename ?
			current_long_filename : current_filename;
		
		for (i = strlen(rfn) - 1; i >= 0; i--)
#ifdef MSDOS
			if (rfn[i] == '\\') break;
#else
			if (rfn[i] == '/') break;
#endif
#if defined MSDOS
		if (i >= 0 && *name != '\\' && (nl < 2 || name[1] != ':'))
#elif defined AMIGA
		if (i >= 0 && *name != '/' && (nl < 2 || name[1] != ':'))
#else
		if (i >= 0 && *name != '/')
#endif
		{
			/*
			 * current file is somewhere else, and the provided
			 * file name is not absolute, so we must adjust the
			 * base for looking for the file; besides,
			 * found_files and found_files_loc are irrelevant
			 * for this search.
			 */
			s = new char[i + 2 + nl];
			memcpy(s, rfn, i);
#ifdef MSDOS
			s[i] = '\\';
#else
			s[i] = '/';
#endif
			memcpy(s + i + 1, name, nl);
			s[i + 1 + nl] = 0;
			ff = found_files->lookup(s);
		}
		else ff = found_files->lookup(name);
	}
	
	if (!ff)
	{
		struct found_file_sys *ffs = found_files_sys->lookup(name);

		if (ffs)
		{
			ff = ffs->rff;
			incdir = ffs->incdir;
		}
	}
	/*
	 * At that point: if the file was found in the cache, ff points to
	 * the cached descriptive structure; its name is s if s is not 0,
	 * name otherwise.
	 */
	if (ff) goto found_file_cache;

	/*
	 * This is the first time we find the file, or it was not protected.
	 */
	protect_detect.ff = new_found_file();
	nffa = 1;
	if (localdir && (f = fopen(s ? s : name, "r")))
	{
		lf = 1;
		goto found_file_label;
	}
	/*
	 * If s contains a name, that name is now irrelevant: it was a
	 * filename for a search in the current directory, and the file
	 * was not found.
	 */
	if (s)
	{
		delete [] s;
		s = 0;
	}
	
	for (i = 0; i < include_path_nb; i++)
	{
		size_t ni = strlen(include_path[i]);

		s = new char[ni + nl + 2];
		memcpy(s, include_path[i], ni);
#ifdef AMIGA
		/* contributed by Volker Barthelmann */
		if (ni == 1 && *s == '.')
		{
			*s = 0;
			ni = 0;
		}
		
		if (ni > 0 && s[ni - 1] != ':' && s[ni - 1] != '/')
		{
			s[ni] = '/';
			memcpy(s + ni + 1, name, nl + 1);
		else
			memcpy(s + ni, name, nl + 1);
#else
		s[ni] = '/';
		memcpy(s + ni + 1, name, nl + 1);
#endif
#ifdef MSDOS
		/* on msdos systems, replace all / by \ */
		{
			char *c;

			for (c = s; *c; c++)
				if (*c == '/')
					*c = '\\';
		}
#endif
		incdir = i;
		ff = found_files->lookup(s);
		if (ff != 0)
		{
			/*
			 * The file is known, but not as a system include
			 * file under the name provided.
			 */
			struct found_file_sys *ffs = new_found_file_sys();

			ffs->name = sdup(name);
			ffs->rff = ff;
			ffs->incdir = incdir;
			found_files_sys->install(ffs->name, ffs);
			delete [] s;
			s = 0;
			if (nffa)
			{
				del_found_file(protect_detect.ff);
				protect_detect.ff = 0;
				nffa = 0;
			}
			goto found_file_cache;
		}
		f = fopen(s, "r");
		if (f) goto found_file_label;
		delete [] s;
		s = 0;
	}
zero_out:
	if (s) delete [] s;
	if (nffa)
	{
		del_found_file(protect_detect.ff);
		protect_detect.ff = 0;
		nffa = 0;
	}
	return 0;

	/*
	 * This part is invoked when the file was found in the
	 * cache.
	 */
found_file_cache:
	if (ff->protect)
	{
		if (get_macro(ff->protect))
		{
			/* file is protected, do not include it */
			find_file_error = FF_PROTECT;
			goto zero_out;
		}
		/* file is protected but the guardian macro is
		   not available; disable guardian detection. */
		protect_detect.state = 0;
	}
	protect_detect.ff = ff;
	f = fopen(ff->long_name, "r");
	if (!f) goto zero_out;
	find_file_error = FF_KNOWN;
	goto found_file_2;

	/*
	 * This part is invoked when we found a new file, which was not
	 * yet referenced. If lf == 1, then the file was found directly,
	 * otherwise it was found in some system include directory.
	 * A new found_file structure has been allocated and is in
	 * protect_detect.ff
	 */
found_file_label:
	if (f && ((emit_dependencies == 1 && lf && current_incdir == -1)
	    || emit_dependencies == 2))
		fprintf(emit_output, " %s", s ? s : name);
	
	nff = protect_detect.ff;
	nff->name = sdup(name);
	nff->long_name = s ? s : sdup(name);
#ifdef AUDIT
	if (
#endif
	found_files->install(nff->long_name, nff)
#ifdef AUDIT
	) fatal("filename collided with a wraith")
#endif
	;
	if (!lf) {
		struct found_file_sys *ffs = new_found_file_sys();

		ffs->name = sdup(name);
		ffs->rff = nff;
		ffs->incdir = incdir;
		found_files_sys->install(ffs->name, ffs);
	}
	s = 0;
	find_file_error = FF_UNKNOWN;
	ff = nff;

found_file_2:
	if (s) delete [] s;
	current_long_filename = ff->long_name;
#ifdef NO_LIBC_BUF
	setbuf(f, 0);
#endif
	current_incdir = incdir;
	return f;
}

/*
 * Find the named file by looking through the end of the include path.
 * This is for #include_next directives.
 * #include_next <foo> and #include_next "foo" are considered identical,
 * for all practical purposes.
 */
FILE *
cpp::find_file_next(const char *name)
{
	int i;
	size_t nl = strlen(name);
	FILE *f;
	struct found_file *ff;

	find_file_error = FF_ERROR;
	protect_detect.state = -1;
	protect_detect.macro = 0;
	for (i = current_incdir + 1; i < include_path_nb; i++)
	{
		char *s;
		size_t ni = strlen(include_path[i]);

		s = new char[ni + nl + 2];
		memcpy(s, include_path[i], ni);
		s[ni] = '/';
		memcpy(s + ni + 1, name, nl + 1);
#ifdef MSDOS
		/* on msdos systems, replace all / by \ */
		{
			char *c;

			for (c = s; *c; c++)
				if (*c == '/')
					*c = '\\';
		}
#endif
		ff = found_files->lookup(s);
		if (ff)
		{
			/* file was found in the cache */
			if (ff->protect)
			{
				if (get_macro(ff->protect))
				{
					find_file_error = FF_PROTECT;
					delete [] s;
					return 0;
				}
				/* file is protected but the guardian macro is
				   not available; disable guardian detection. */
				protect_detect.state = 0;
			}
			protect_detect.ff = ff;
			f = fopen(ff->long_name, "r");
			if (!f)
			{
				/* file is referenced but yet unavailable. */
				delete [] s;
				return 0;
			}
			find_file_error = FF_KNOWN;
			delete [] s;
			s = ff->long_name;
		}
		else
		{
			f = fopen(s, "r");
			if (f)
			{
				if (emit_dependencies == 2)
					fprintf(emit_output, " %s", s);
				
				ff = protect_detect.ff = new_found_file();
				ff->name = sdup(s);
				ff->long_name = s;
#ifdef AUDIT
				if (
#endif
				found_files->install(ff->long_name, ff)
#ifdef AUDIT
				) fatal("filename collided with a wraith")
#endif
				;
				find_file_error = FF_UNKNOWN;
			}
		}
		
		if (f)
		{
			current_long_filename = s;
			current_incdir = i;
			return f;
		}
		
		delete [] s;
	}
	return 0;
}

} /* namespace ucpp */
