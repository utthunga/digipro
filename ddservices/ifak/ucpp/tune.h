
/*
 * $Id: tune.h,v 1.3 2006/04/07 14:37:49 fna Exp $
 * 
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * Copyright (C) 1999, 2000 Thomas Pornin
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _ucpp_tune_h
#define _ucpp_tune_h

/* ====================================================================== */
/*
 * Define LOW_MEM for low memory machines. It seems to improve performance
 * on larger systems, so this is on by default.
 */
#define LOW_MEM

/* ====================================================================== */
/*
 * Define AMIGA for systems using "drive letters" at the beginning of
 * some paths; define MSDOS on systems with drive letters and using
 * backslashes to seperate directory components.
 */
#if 0
#define AMIGA
#endif
#ifdef _MSC_VER
#define MSDOS
#endif

/* ====================================================================== */
/*
 * Buffering: there are two levels of buffering on input and output streams:
 * the standard libc buffering (manageable with setbuf() and setvbuf())
 * and some buffering provided by ucpp itself. The ucpp buffering uses
 * two buffers, of size respectively INPUT_BUF_MEMG and OUTPUT_BUF_MEMG
 * (as defined below).
 * You can disable one or both of these bufferings by defining the macros
 * NO_LIBC_BUF and NO_UCPP_BUF.
 *
 * Performance may vary, depending on the target architecture. On a
 * Linux/Alpha workstation, use both bufferings (which means, disable none
 * of them) for maximum performance. On a Minix-86 machine, disabling
 * ucpp buffering saves some memory and does not seem to impact performance.
 */
/* #define NO_LIBC_BUF */
/* #define NO_UCPP_BUF */

/* ====================================================================== */
/*
 * Define this if you want ucpp to generate tokenized PRAGMA tokens;
 * otherwise, it will generate raw string contents. This setting is
 * irrelevant to the stand-alone version of ucpp.
 */
#define PRAGMA_TOKENIZE

/*
 * Define this to the special character that marks the end of tokens with
 * a string value inside a tokenized PRAGMA token. The #pragma and _Pragma()
 * directives which use this character will be a bit more difficult to
 * decode (but ucpp will not mind). 0 cannot be used. '\n' is fine because
 * it cannot appear inside a #pragma or _Pragma(), since newlines cannot be
 * embedded inside tokens, neither directly nor by macro substitution and
 * stringization. Besides, '\n' is portable.
 */
#define PRAGMA_TOKEN_END	((unsigned char)'\n')

/*
 * Define this if you want ucpp to include encountered #pragma directives
 * in its output in non-lexer mode; _Pragma() are translated to equivalent
 * #pragma directives.
 */
#define PRAGMA_DUMP

/*
 * According to my interpretation of the C99 standard, _Pragma() are
 * evaluated wherever macro expansion could take place. However, Neil Booth,
 * whose mother language is English (contrary to me) and who is well aware
 * of the C99 standard (and especially the C preprocessor) told me that
 * it was unclear whether _Pragma() are evaluated inside directives such
 * as #if, #include and #line. If you want to disable the evaluation of
 * _Pragma() inside such directives, define the following macro.
 */
/* #define NO_PRAGMA_IN_DIRECTIVE */

/* ====================================================================== */
/*
 * Define INMACRO_FLAG to include two flags to the structure lexer_state,
 * that tell whether tokens come from a macro-replacement, and count those
 * macro-replacements.
 */
/* #define INMACRO_FLAG */

/* ====================================================================== */
/*
 * Paths where files are looked for by default, when #include is used.
 * Typical path is /usr/local/include and /usr/include, in that order.
 * If you want to set up no path, define the macro to 0.
 *
 * For Linux, get gcc includes too, or you will miss things like stddef.h.
 * The exact path varies much, depending on the distribution.
 */
#if 1
#define STD_INCLUDE_PATH	0
#else
#define STD_INCLUDE_PATH	"/usr/local/include", "/usr/include"
#endif

/* ====================================================================== */
/*
 * For the evaluation of #if expression.
 * See the README file for details.
 */
/*
 * to force signedness of wide character constants, define WCHAR_SIGNEDNESS
 * to 0 for unsigned, 1 for signed.
#define WCHAR_SIGNEDNESS	0
 */

/* ====================================================================== */
/*
 * Standard assertions. They should include one cpu() assertion, one machine()
 * assertion (identical to cpu()), and one or more system() assertions.
 *
 * for Linux/PC:      cpu(i386),  machine(i386),  system(unix), system(linux)
 * for Linux/Alpha:   cpu(alpha), machine(alpha), system(unix), system(linux)
 * for Sparc/Solaris: cpu(sparc), machine(sparc), system(unix), system(solaris)
 *
 * These are only suggestions. On Solaris, machine() should be defined
 * for i386 or sparc. For cross-compilation, define assertions related
 * to the target architecture.
 *
 * If you want no standard assertion, define STD_ASSERT to 0.
 */
#if 1
#define STD_ASSERT	0
#else
#define STD_ASSERT	"cpu(i386)", "machine(i386)", \
			"system(unix)", "system(freebsd)"
#endif

/* ====================================================================== */
/*
 * System predefined macros. Nothing really mandatory, but some programs
 * might rely on those.
 * Each string must be either "name" or "name=token-list". If you want
 * no predefined macro, define STD_MACROS to 0.
 */
#if 1
#define STD_MACROS	0
#else
#define STD_MACROS	"__FreeBSD=4", "__unix", "__i386", \
			"__FreeBSD__=4", "__unix__", "__i386__"
#endif

/* ====================================================================== */
/*
 * Default flags; HANDLE_ASSERTIONS is required for Solaris system headers.
 * See cpp.h for the definition of these flags.
 */
#define _DEFAULT_CPP_FLAGS	(DISCARD_COMMENTS | WARN_STANDARD \
				| WARN_PRAGMA | FAIL_SHARP | MACRO_VAARG \
				| CPLUSPLUS_COMMENTS | LINE_NUM | TEXT_OUTPUT \
				| KEEP_OUTPUT | HANDLE_TRIGRAPHS \
				| HANDLE_ASSERTIONS)
#define _DEFAULT_LEXER_FLAGS	(DISCARD_COMMENTS | WARN_STANDARD | FAIL_SHARP \
				| MACRO_VAARG | CPLUSPLUS_COMMENTS | LEXER \
				| HANDLE_TRIGRAPHS | HANDLE_ASSERTIONS)

/* ====================================================================== */
/*
 * Maximum value (plus one) of a character handled by the lexer; 128 is
 * alright for ASCII native source code, but 256 is needed for EBCDIC.
 * 256 is safe in both cases; you will have big problems if you set
 * this value to INT_MAX or above. On Minix-86 or Msdos (small memory
 * model), define MAX_CHAR_VAL to 128.
 *
 * Set MAX_CHAR_VAL to a power of two to increase lexing speed. Beware
 * that lexer.c defines a static array of size MSTATE * MAX_CHAR_VAL
 * values of type int (MSTATE is defined in lexer.c and is about 40).
 */
#define MAX_CHAR_VAL	256

/*
 * If you want some extra character to be considered as whitespace,
 * define this macro to that space. On ISO-8859-1 machines, 160 is
 * the code for the unbreakable space.
 */
/* #define UNBREAKABLE_SPACE	160 */

/*
 * If you want whitespace tokens contents to be recorded (making them
 * tokens with a string content), define this. The macro STRING_TOKEN
 * will be adjusted accordingly.
 * Without this option, whitespace tokens are not even returned by the
 * lex() function. This is irrelevant for the non-lexer mode (almost --
 * it might slow down a bit ucpp, and with this option, comments will be
 * kept inside #pragma directives).
 */
/* #define SEMPER_FIDELIS */

/* ====================================================================== */
/*
 * Some constants used for memory increment granularity. Increasing these
 * values reduces the number of calls to malloc() but increases memory
 * consumption.
 *
 * Values should be powers of 2.
 */

/* for cpp.c */
#define COPY_LINE_LENGTH	80
#define INPUT_BUF_MEMG		8192
#define OUTPUT_BUF_MEMG		8192
#define TOKEN_NAME_MEMG		64	/* must be at least 4 */
#define TOKEN_LIST_MEMG		32
#define INCPATH_MEMG		16
#define GARBAGE_LIST_MEMG	32
#define LS_STACK_MEMG		4
#define FNAME_MEMG		32

/* ====================================================================== */

#endif /* _ucpp_tune_h */
