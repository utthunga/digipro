
/* 
 * $Id: profibus.h,v 1.21 2009/06/19 08:36:03 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _profibus_h
#define _profibus_h

#include "profibus_config.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * error codes (32bit coding)
 * 
 * Bits 0..7:
 * ----------
 * 
 * 1..63    - profibus interface error
 * 
 * 64       - driver specific error
 *            bits 8..31 driver specific meaning
 * 
 * 65..125  - reserved
 * 
 * 126      - error confirmation
 *            bits 8..15 state
 * 
 * 127      - abort/reject indication
 *            bits 8..15 reason code
 * 
 * 128      - DPV1 error:
 *            bits  8..15 Error_Decode
 *            bits 16..24 Error_Code_1
 *            bits 25..31 Error_Code_2
 * 
 * 129..255 - reserved
 * 
 */
enum pb_error
{
	PB_NO_ERROR             =   0,

	PB_UNSPECIFIC_ERROR     =   1,
	PB_NULL_POINTER         =   2,
	PB_INVALID_DRIVER       =   3,
	PB_INVALID_DEVICE       =   4,
	PB_INVALID_CHANNEL      =   5,
	PB_INVALID_HANDLE       =   6,
	PB_INVALID_DATALEN	=   7,
	PB_OUTOFMEMORY          =   8,
	PB_UNIMPLEMENTED	=   9,
	PB_DEVICE_IN_USE	=  10,
	PB_INVALID_ARGS		=  11,

	PB_DRIVER_SPECIFIC      =  64,

	PB_ERROR_CONFIRMATION   = 126,
	PB_ABORT_INDICATION     = 127,
	PB_DPV1_ERROR           = 128,
};

WINDLL_PROFIBUS const char *pb_errstr(int result, const char **descr, unsigned char driver);
WINDLL_PROFIBUS const char *pb_errstr0(int result, const char **descr);

/**
 * Info structure for device probing.
 */
struct pb_info
{
	unsigned char driver;	/**< driver number for this device */
	unsigned char device;	/**< device number */
	unsigned char channels;	/**< number of channels supported by the device */
	unsigned char in_use;	/**< non-zero if the device is already in use */
	unsigned int  flags;	/**< driver flags */
	char name[64];		/**< human readable name of the device */
};
#define PB_DRIVER_FEATURES_BUSPARAMETER	0x01
#define PB_DRIVER_FEATURES_LIVELIST	0x02
#define PB_DRIVER_FEATURES_IN_USE	0x04

/**
 * Probe for attached profibus interfaces.
 * Get a vector of pb_info that is filled with found
 * profibus interfaces. Return the number of found
 * interfaces.
 * \param info Vector of pb_info elements.
 * \param len Size of the vector info.
 * \return The number of found profibus interfaces.
 */
WINDLL_PROFIBUS int pb_probe(struct pb_info *info, size_t len);

/**
 * Profibus baudrates
 */
enum pb_baudrate
{
	PB_BAUD_9_6_K = 0,
	PB_BAUD_19_2_K,
	PB_BAUD_31_25_K,
	PB_BAUD_45_45_K,
	PB_BAUD_57_60_K,
	PB_BAUD_93_75_K,
	PB_BAUD_187_5_K,
	PB_BAUD_500_K,
	PB_BAUD_1_5_M,
	PB_BAUD_3_M,
	PB_BAUD_6_M,
	PB_BAUD_12_M,
	PB_BAUD_MAX
};

WINDLL_PROFIBUS const char **pb_baudnames(size_t *len);
WINDLL_PROFIBUS const char *pb_baud2name(enum pb_baudrate);
WINDLL_PROFIBUS enum pb_baudrate pb_name2baud(const char *);

/**
 * Profibus bus parameters.
 */
struct pb_bus_parameter
{
	unsigned char		FDL_Add;		/**< FDL-Adress from the DPE-Master */
	enum pb_baudrate	Baud_rate;		/**< Baudrate */
	unsigned short		Tsl;			/**< Slot-Time */
	unsigned short		minTsdr;		/**< Station-Delay-Responder */
	unsigned short		maxTsdr;		/**< Station-Delay-Responder */
	unsigned char		Tqui;			/**< Quit-Time */
	unsigned char		Tset;			/**< Setup-Time */
	unsigned int		Ttr;			/**< Target-Rotation-Time */
	unsigned char		G;			/**< GAP-update factor */
	unsigned char		HSA;			/**< highest slave adress */
	unsigned char		max_retry_limit;	/**< */
	unsigned char		Bp_Flag;		/**< */
	unsigned short		Min_Slave_Intervall;	/**< */
	unsigned short		Poll_Timeout;		/**< */
	unsigned short		Data_control_Time;	/**< */
	unsigned short		Master_User_Data_Len;	/**< */
	unsigned char		Master_Class2_Name[32];	/**< */
	unsigned char		Master_User_Data[64];	/**< */
};

/**
 * Abstract device type to identify an open devices
 * on subsequent calls to the library.
 */
typedef void *pb_dev_t;
/**
 * Abstract slave type to identify open slaves
 * on subsequent calls to the library.
 */
typedef void *pb_slave_t;

/**
 * Device open.
 * \param driver The number of driver that is accessed.
 * \param device The device which should be opened.
 * \param channel The channel of the device which should be opened.
 * \param par The profibus parameter for the device.
 * \param dev Pointer to a pb_dev_t where the device identifier of
 * the opened device is stored.
 * \return 0 on success (the device identifier is stored in dev); any
 * other value specify an error.
 */
WINDLL_PROFIBUS int pb_open(unsigned char driver, unsigned char device, unsigned char channel,
			    const struct pb_bus_parameter *par, pb_dev_t *dev);

/**
 * Device close.
 * Close a device specified by the device identifier dev that was
 * previously opened by pb_open.
 * \param dev The device that should be closed.
 * \return 0 if the device reset was successful; any other value
 * specify an error during the device reset. In any case the device
 * is closed and the attached resources are freed.
 */
WINDLL_PROFIBUS int pb_close(pb_dev_t dev);

/**
#include <stdarg.h> * Device control.
 * Dispatch driver specific control data.
 * \param dev The device for the ioctl.
 * \param request The type of the ioctl.
 * \return 0 if successful; any other value specify an error.
 */
WINDLL_PROFIBUS int pb_ioctl(pb_dev_t dev, int request, ...);

/**
 * Return human readable string for result error code.
 */
WINDLL_PROFIBUS const char *pb_err2str(pb_dev_t dev, int result, const char **descr);

/**
 * Set bus parameter.
 */
WINDLL_PROFIBUS int pb_set_bus_parameter(pb_dev_t, const struct pb_bus_parameter *);

/*
 * MS0 services
 */

WINDLL_PROFIBUS int pb_get_input(pb_dev_t, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);
WINDLL_PROFIBUS int pb_set_output(pb_dev_t, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);

/**
 * Operating mode of the profibus device
 */
enum pb_mode
{
	OFFLINE = 0x00,	/**< device is offline, no services are available */
	STOP	= 0x40,	/**< device is in stop mode, MS2 services are available */
	CLEAR	= 0x80,	/**< device is in clear mode, MS2 and MS0 services are available */
	OPERATE	= 0xC0	/**< device is in operate mode, same as clear but with ... */
};
WINDLL_PROFIBUS int pb_set_mode(pb_dev_t, enum pb_mode mode);
WINDLL_PROFIBUS int pb_get_mode(pb_dev_t, enum pb_mode *mode);

/**
 * Set slave parameter.
 * \todo Specify abstract interface and implement this in the drivers.
 */
WINDLL_PROFIBUS int pb_set_slave_param(pb_dev_t, unsigned char slaveaddr, const char *data, size_t datalen);

WINDLL_PROFIBUS int pb_change_slave_addr(pb_dev_t, unsigned char slaveaddr, unsigned char addr,
					 unsigned short ident_number);

struct pb_livelist_info
{
	size_t length;
	struct { unsigned char addr; unsigned char state; } list[127];
};

WINDLL_PROFIBUS int pb_livelist(pb_dev_t, struct pb_livelist_info *);

/*
 * MS2 services
 */

/**
 * MS2 read or write request structure.
 * Define the arguments for a MS2 read/write request.
 */
struct pb_request
{
	unsigned char slot;		/**< slot that should be read */
	unsigned char index;		/**< index that should be read */
	unsigned char data[240];	/**< space for the data to be read/written */
	unsigned char datalen;		/**< number of bytes that should be read/written;
					 * If the request succeed it contain the total number
					 * of bytes that are read or written.
					 */
};

/**
 * connect request.
 */
WINDLL_PROFIBUS int pb_connect(pb_dev_t, unsigned char slaveaddr,
			       unsigned short sendTimeOut, pb_slave_t *);

/**
 * connect request through PA-Link.
 */
WINDLL_PROFIBUS int pb_connect_pa_link(pb_dev_t, unsigned char palinkaddr, unsigned char slaveaddr,
				       unsigned short sendTimeOut, pb_slave_t *);

/**
 * disconnect request.
 */
WINDLL_PROFIBUS int pb_disconnect(pb_dev_t, pb_slave_t);

/**
 * read request.
 */
WINDLL_PROFIBUS int pb_read(pb_dev_t, pb_slave_t, struct pb_request *);

/**
 * write request.
 */
WINDLL_PROFIBUS int pb_write(pb_dev_t, pb_slave_t, struct pb_request *);

WINDLL_PROFIBUS int pb_load_slave_param(pb_dev_t dev,
				const unsigned char address,
				const unsigned short ident_number,
				const unsigned char cfg_data[],
				const unsigned char cfg_data_len,
				const unsigned char user_prm_data[],
				const unsigned char user_prm_data_len,
				const unsigned char input_data_len,
				unsigned short *input_data_offset,
				const unsigned char output_data_len,
				unsigned short *output_data_offset);

WINDLL_PROFIBUS int pb_get_slave_diag(pb_dev_t dev, const unsigned char slave_address, unsigned char *diag_data[], unsigned char *diag_data_len);

WINDLL_PROFIBUS int pb_read_system_diagnostic(pb_dev_t dev, unsigned char dest[16]);

WINDLL_PROFIBUS int pb_read_data_transfer_list(pb_dev_t dev, unsigned char dest[16]);

WINDLL_PROFIBUS int pb_change_slave_param(pb_dev_t dev, const unsigned char address,
				const unsigned char action,
				const unsigned char data_len,
				const unsigned char prm_data[]);

#ifdef __cplusplus
}
#endif

#endif /* _profibus_h */
