
/*
 * $Id: pb_dpem_err.h,v 1.2 2009/07/07 15:47:59 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _profibus_errmsg_h
#define _profibus_errmsg_h

#include "profibus_config.h"

// C stdlib

// own libs

// own


const char *pb_dpem_state_str(unsigned char state);

const char *pb_dpem_reason_code_str(unsigned char reason_code);

const char *pb_dpem_errcode_1_str(unsigned char code, const char **descr);

const char *pb_dpem_errdecode(unsigned char ErrorDecode,
			      unsigned char ErrorCode1,
			      unsigned char ErrorCode2,
			      const char **descr);

#endif /* _profibus_errmsg_h */
