
/*
 * $Id: mutex.h,v 1.1 2005/11/15 15:34:32 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _mutex_h
#define _mutex_h

#include "profibus_config.h"

// C stdlib

// own libs

// own

#ifdef __cplusplus
extern "C"
{
#endif

struct mutex *mutex_create(void);
void mutex_destroy(struct mutex *);
void mutex_lock(struct mutex *);
void mutex_unlock(struct mutex *);

#ifdef __cplusplus
}
#endif

#endif /* _mutex_h */
