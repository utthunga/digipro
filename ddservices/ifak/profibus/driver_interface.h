
/* 
 * $Id: driver_interface.h,v 1.16 2009/07/07 15:47:59 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _profibus_driver_interface_h
#define _profibus_driver_interface_h

#include <stdarg.h>

#include "profibus_config.h"
#include "profibus.h"


struct pb_dev
{
	/* function vector */
	struct pb_funcs *funcs;

	/* device address */
	unsigned char device;
	unsigned char channel;

	/* driver private data */
	void *data;
};

struct pb_funcs
{
/* O */	int (*probe)(unsigned char driver, struct pb_info *, size_t len);

/* M */	int (*open)(struct pb_dev *, const struct pb_bus_parameter *);
/* M */	int (*close)(struct pb_dev *);
/* O */	int (*ioctl)(struct pb_dev *, int request, va_list);

/* O */	int (*set_bus_parameter)(struct pb_dev *, const struct pb_bus_parameter *);

/* O */	int (*get_input)(struct pb_dev *, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);
/* O */	int (*set_output)(struct pb_dev *, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);

/* O */	int (*set_mode)(struct pb_dev *, enum pb_mode mode);
/* O */	int (*get_mode)(struct pb_dev *, enum pb_mode *mode);

/* O */	int (*set_slave_param)(struct pb_dev *, unsigned char slaveaddr, const char *data, size_t datalen);
/* O */	int (*change_slave_addr)(struct pb_dev *, unsigned char slaveaddr, unsigned char addr,
				 unsigned short ident_number);

/* O */	int (*livelist)(struct pb_dev *, struct pb_livelist_info *livelist);

/* M */	int (*connect)(struct pb_dev *, unsigned char palinkaddr, unsigned char slaveaddr,
		       unsigned short sendTimeOut, pb_slave_t *);
/* M */	int (*disconnect)(struct pb_dev *, pb_slave_t);
/* M */	int (*read)(struct pb_dev *, pb_slave_t, struct pb_request *);
/* M */	int (*write)(struct pb_dev *, pb_slave_t, struct pb_request *);

/* M */	const char *(*errstr)(int result, const char **descr);

/* O */ int (*load_slave_param)(struct pb_dev *dev,
				const unsigned char address,
				const unsigned short ident_number,
				const unsigned char cfg_data[],
				const unsigned char cfg_data_len,
				const unsigned char user_prm_data[],
				const unsigned char user_prm_data_len,
				const unsigned char input_data_len,
				unsigned short *input_data_offset,
				const unsigned char output_data_len,
				unsigned short *output_data_offset);

/* O */ int (*get_slave_diag)(struct pb_dev *dev, const unsigned char slave_address, unsigned char *diag_data[], unsigned char *diag_data_len);
/* O */ int (*read_system_diagnostic)(struct pb_dev *dev, unsigned char dest[16]);
/* O */ int (*read_data_transfer_list)(struct pb_dev *dev, unsigned char dest[16]);
/* O */ int (*change_slave_param)(struct pb_dev *dev,
				const unsigned char address,
				const unsigned char action,
				const unsigned char data_len,
				const unsigned char prm_data[]);
};

int state2result(unsigned char state);

int reasoncode2result(unsigned char reasoncode);

int dpv1error2result(unsigned char ErrorDecode,
		     unsigned char ErrorCode1,
		     unsigned char ErrorCode2);

union PBIOStatus
{
	unsigned long code;
	struct
	{
		unsigned char ErrorCode;
		unsigned char ErrorDecode;
		unsigned char ErrorCode1;
		unsigned char ErrorCode2;
	} d;
};

int status2result(unsigned long status);

#ifdef WIN32
# if _MSC_VER >= 1400
#  define snprintf(a,b,c,...) _snprintf_s(a,b,_TRUNCATE,c,__VA_ARGS__)
#  define vsnprintf(a,b,c,d) _vsnprintf_s(a,b,_TRUNCATE,c,d)
# else
#  define snprintf _snprintf
#  define vsnprintf(a,b,c,d) _vsnprintf(a,b,c,d)
# endif
# define ms_sleep(x) Sleep(x)
#else
# include <unistd.h>
# define ms_sleep(x) usleep(x * 1000)
#endif

#endif /* _profibus_driver_interface_h */
