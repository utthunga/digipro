
/*
 * $Id: condition_pthread.c,v 1.2 2009/06/09 14:47:49 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>

// other libs
#include <pthread.h>

// own libs

// own
#include "condition.h"



struct condition
{
	pthread_mutex_t mutex_;
	pthread_cond_t cond_;
	int locked_;
};


struct condition *
condition_create(void)
{
	struct condition *c;

	c = malloc(sizeof(*c));
	if (c)
	{
		if (pthread_mutex_init(&(c->mutex_), NULL) == 0)
		{
			if (pthread_cond_init(&(c->cond_), NULL) != 0)
			{
				pthread_mutex_destroy(&(c->mutex_));

				free(c);
				c = NULL;
			}
			else
				c->locked_ = 0;
		}
		else
		{
			free(c);
			c = NULL;
		}
	}

	return c;
}

void
condition_destroy(struct condition *c)
{
	int ret;

	assert(c && !c->locked_);

	ret = pthread_cond_destroy(&(c->cond_));
	assert(ret == 0);

	ret = pthread_mutex_destroy(&(c->mutex_));
	assert(ret == 0);

	free(c);
}

void
condition_lock(struct condition *c)
{
	int ret;

	assert(c);

	ret = pthread_mutex_lock(&(c->mutex_));
	assert(ret == 0);

	c->locked_ = 1;
}

void
condition_unlock(struct condition *c)
{
	int ret;

	assert(c && c->locked_);
	c->locked_ = 0;

	ret = pthread_mutex_unlock(&(c->mutex_));
	assert(ret == 0);
}

void
condition_wait(struct condition *c)
{
	int ret;

	assert(c && c->locked_);
	c->locked_ = 0;

	ret = pthread_cond_wait(&(c->cond_), &(c->mutex_));
	assert(ret == 0);

	c->locked_ = 1;
}

void
condition_notify(struct condition *c)
{
	int ret;

	assert(c);

	ret = pthread_cond_signal(&(c->cond_));
	assert(ret == 0);
}

void
condition_notify_all(struct condition *c)
{
	int ret;

	assert(c);

	ret = pthread_cond_broadcast(&(c->cond_));
	assert(ret == 0);
}
