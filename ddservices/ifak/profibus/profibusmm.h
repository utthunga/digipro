
/* 
 * $Id: profibusmm.h,v 1.10 2009/06/23 14:45:14 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _profibusmm_h
#define _profibusmm_h

#include <string>
#include <vector>

#include "profibus.h"


namespace io_driver
{

/**
 * C++ wrapper for the profibus library.
 * A simple C++ wrapper around the C profibus library interface. Please see at
 * the corresponding C functions for a detailed documentationof the data
 * types and functions.
 */
class WINDLL_PROFIBUS pb
{
public:
	typedef struct pb_info info_t;
	typedef enum pb_baudrate baudrate_t;
	typedef struct pb_bus_parameter bus_parameter_t;
	typedef enum pb_mode mode_t;
	typedef struct pb_livelist_info livelist_info_t;
	typedef struct pb_request request_t;
	typedef pb_slave_t slave_t;

	static const char *errstr(int result, const char **descr = NULL);
	static const char *errstr(int result, const char **descr, unsigned char driver);
	static const char *errstr(int result, unsigned char driver);

	typedef std::vector<info_t> device_info_t;
	static device_info_t probe(void);

	static std::vector<std::string> baudnames(void);
	static std::string baud2name(baudrate_t);
	static baudrate_t name2baud(const std::string &);

	pb(unsigned char driver, unsigned char device, unsigned char channel, const bus_parameter_t &);
	~pb(void);

	int ioctl(int request, void *, void *);
	const char *err2str(int result, const char **descr = NULL);

	int set_bus_parameter(const bus_parameter_t &);

	int get_input(unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);
	int set_output(unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen);

	int set_mode(mode_t mode);
	int get_mode(mode_t &mode);

	int change_slave_addr(unsigned char slaveaddr, unsigned char addr, unsigned short ident_number);
	int livelist(livelist_info_t &);

	int connect(unsigned char slaveaddr, unsigned short sendTimeOut, slave_t &);
	int connect_pa_link(unsigned char palinkaddr, unsigned char slaveaddr, unsigned short sendTimeOut, slave_t &);
	int disconnect(slave_t);
	int read(slave_t, request_t &);
	int write(slave_t, request_t &);

	int load_slave_param(
				const unsigned char address,
				const unsigned short ident_number,
				const unsigned char cfg_data[],
				const unsigned char cfg_data_len,
				const unsigned char user_prm_data[],
				const unsigned char user_prm_data_len,
				const unsigned char input_data_len,
				unsigned short *input_data_offset,
				const unsigned char output_data_len,
				unsigned short *output_data_offset);

	int get_slave_diag(const unsigned char slave_address, unsigned char *diag_data[], unsigned char *diag_data_len);

	int read_system_diagnostic(unsigned char dest[16]);
	int read_data_transfer_list(unsigned char dest[16]);
	int change_slave_param(const unsigned char address,
				const unsigned char action,
				const unsigned char data_len,
				const unsigned char prm_data[]);
private:
	pb_dev_t dev;
};

} /* namespace io_driver */

#endif /* _profibusmm_h */
