
/* 
 * $Id: profibus.c,v 1.28 2009/07/15 16:31:38 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "driver_interface.h"
#include "pb_dpem_err.h"
#include "profibus.h"

#include "pb_ispro.h"
#include "pb_loopback.h"
#include "pb_simatic_dpc2.h"
#include "pb_softing.h"

/* vector of drivers */
static struct pb_funcs *allfuncs[] =
{
	&pb_ispro_funcs,
	&pb_simatic_dpc2_funcs,
	&pb_loopback_funcs,
	&pb_softing_funcs
};
#define ALLFUNCS_SIZE (sizeof(allfuncs)/sizeof(allfuncs[0]))

#ifndef _
# define _(x) x
#endif

/* error decoding */
WINDLL_PROFIBUS const char *pb_errstr(int result, const char **descr, unsigned char driver)
{
	if ((result & 0xff) == PB_DRIVER_SPECIFIC)
	{
		if (driver < ALLFUNCS_SIZE)
		{
			if (descr)
				*descr = NULL;

			return allfuncs[driver]->errstr(result, descr);
		}
	}

	return pb_errstr0(result, descr);
}
WINDLL_PROFIBUS const char *pb_errstr0(int result, const char **descr)
{
	const char *msg = NULL;

	if (descr)
		*descr = NULL;

	switch (result & 0xff)
	{
		case PB_NO_ERROR:
			msg = _("No Error");
			break;

		case PB_UNSPECIFIC_ERROR:
			msg = _("Unspecific error");
			break;
		case PB_NULL_POINTER:
			msg = _("NULL pointer");
			break;
		case PB_INVALID_DRIVER:
			msg = _("invalid driver number");
			break;
		case PB_INVALID_DEVICE:
			msg = _("invalid device number");
			break;
		case PB_INVALID_CHANNEL:
			msg = _("invalid channel number");
			break;
		case PB_INVALID_HANDLE:
			msg = _("invalid device/slave handle");
			break;
		case PB_INVALID_DATALEN:
			msg = _("invalid datalen in request");
			break;
		case PB_OUTOFMEMORY:
			msg = _("out of memory");
			break;
		case PB_UNIMPLEMENTED:
			msg = _("unimplemented function");
			break;
		case PB_DEVICE_IN_USE:
			msg = _("device in use");
			break;
		case PB_INVALID_ARGS:
			msg = _("invalid argument(s)");
			break;

		case PB_DRIVER_SPECIFIC:
			msg = _("driver specific error");
			break;

		case PB_ERROR_CONFIRMATION:
		{
			unsigned char code = (result & 0xff00) >> 8;
			msg = pb_dpem_state_str(code);
			break;
		}
		case PB_ABORT_INDICATION:
		{
			unsigned char code = (result & 0xff00) >> 8;
			msg = pb_dpem_reason_code_str(code);
			break;
		}
		case PB_DPV1_ERROR:
		{
			unsigned char ErrorDecode = (result & 0x0000ff00) >>  8;
			unsigned char ErrorCode1  = (result & 0x00ff0000) >> 16;
			unsigned char ErrorCode2  = (result & 0xff000000) >> 24;
			msg = pb_dpem_errdecode(ErrorDecode, ErrorCode1, ErrorCode2, descr);
			break;
		}

		default:
			msg = _("invalid error code");
			break;
	}

	assert(msg);
	return msg;
}

/* internal helper */
int state2result(unsigned char state)
{
	int i, result = 0;

	if (state)
	{
		result = PB_ERROR_CONFIRMATION; 

		i = state;
		result |= i << 8;
	}

	return result;
}
int reasoncode2result(unsigned char reasoncode)
{
	int i, result = 0;

	if (reasoncode)
	{
		result = PB_ABORT_INDICATION;

		i = reasoncode;
		result |= i << 8;
	}

	return result;
}
int dpv1error2result(unsigned char ErrorDecode,
		     unsigned char ErrorCode1,
		     unsigned char ErrorCode2)
{
	int i, result;

	result = PB_DPV1_ERROR;

	i = ErrorDecode;
	result |= i << 8;

	i = ErrorCode1;
	result |= i << 16;

	i = ErrorCode2;
	result |= i << 24;

	return result;
}
int status2result(unsigned long status)
{
	if (status)
	{
		union PBIOStatus d;

		d.code = status;

		return dpv1error2result(d.d.ErrorDecode, d.d.ErrorCode1, d.d.ErrorCode2);
	}

	return PB_NO_ERROR;
}

WINDLL_PROFIBUS int pb_probe(struct pb_info *info, size_t len)
{
	int found = 0;
	int i;

	for (i = 0; i < ALLFUNCS_SIZE && len > 0; i++)
	{
		int ret = 0;

		if (allfuncs[i]->probe)
			ret = allfuncs[i]->probe(i, info, len);

		found += ret;
		info += ret;
		len -= ret;
	}

	return found;
}

/* baudrate description data & routines */
static const char *baudnames[] = 
{
	"9.6 kBit/s",	// PB_BAUD_9_6_K = 0,
	"19.2 kBit/s",	// PB_BAUD_19_2_K,
	"31.25 kBit/s",	// PB_BAUD_31_25_K,
	"45.45 kBit/s",	// PB_BAUD_45_45_K,
	"57.60 kBit/s",	// PB_BAUD_57_60_K,
	"93.75 kBit/s",	// PB_BAUD_93_75_K,
	"187.5 kBit/s",	// PB_BAUD_187_5_K,
	"500 kBit/s",	// PB_BAUD_500_K,
	"1.5 MBit/s",	// PB_BAUD_1_5_M,
	"3 MBit/s",	// PB_BAUD_3_M,
	"6 MBit/s",	// PB_BAUD_6_M,
	"12 MBit/s",	// PB_BAUD_12_M,
	NULL
};

WINDLL_PROFIBUS const char **pb_baudnames(size_t *len)
{
	assert(PB_BAUD_MAX+1 == (sizeof(baudnames)/sizeof(baudnames[0])));

	if (len)
		*len = PB_BAUD_MAX;

	return baudnames;
}

WINDLL_PROFIBUS const char *pb_baud2name(enum pb_baudrate baud)
{
	const char *ret = NULL;

	if (baud >= 0 && baud < PB_BAUD_MAX)
		ret = baudnames[baud];

	return ret;
}

WINDLL_PROFIBUS enum pb_baudrate pb_name2baud(const char *baudname)
{
	enum pb_baudrate ret = PB_BAUD_9_6_K;
	size_t i;

	for (i = 0; i < PB_BAUD_MAX; i++)
	{
		if (strcmp(baudnames[i], baudname) == 0)
		{
			ret = i;
			break;
		}
	}

	return ret;
}

WINDLL_PROFIBUS int pb_open(unsigned char driver, unsigned char device, unsigned char channel,
			    const struct pb_bus_parameter *par, pb_dev_t *dev)
{
	int ret = PB_OUTOFMEMORY;
	struct pb_dev *pb_dev;

	if (!par || !dev)
		return PB_NULL_POINTER;

	*dev = NULL;

	if (driver >= ALLFUNCS_SIZE)
		return PB_INVALID_DRIVER;

	pb_dev = malloc(sizeof(*pb_dev));
	if (pb_dev)
	{
		memset(pb_dev, 0, sizeof(*pb_dev));

		pb_dev->funcs = allfuncs[driver];
		pb_dev->device = device;
		pb_dev->channel = channel;

		ret = pb_dev->funcs->open(pb_dev, par);
		if (ret == 0)
			*dev = pb_dev;
		else
			free(pb_dev);
	}

	return ret;
}

WINDLL_PROFIBUS int pb_close(pb_dev_t dev)
{
	struct pb_dev *pb_dev = dev;
	int ret = PB_INVALID_HANDLE;

	if (pb_dev)
	{
		ret = pb_dev->funcs->close(pb_dev);
		free(pb_dev);
	}

	return ret;
}

WINDLL_PROFIBUS int pb_ioctl(pb_dev_t dev, int request, ...)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->ioctl)
		{
			va_list args;
			int result;

			va_start(args, request);
			result = pb_dev->funcs->ioctl(pb_dev, request, args);
			va_end(args);

			return result;
		}

		return PB_UNIMPLEMENTED;
	}

	return PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS const char *pb_err2str(pb_dev_t dev, int result, const char **descr)
{
	if ((result & 0xff) == PB_DRIVER_SPECIFIC)
	{
		struct pb_dev *pb_dev = dev;

		if (pb_dev)
		{
			if (descr)
				*descr = NULL;

			return pb_dev->funcs->errstr(result, descr);
		}
	}

	return pb_errstr0(result, descr);
}

WINDLL_PROFIBUS int pb_set_bus_parameter(pb_dev_t dev, const struct pb_bus_parameter *par)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && par)
	{
		if (pb_dev->funcs->set_bus_parameter)
			return pb_dev->funcs->set_bus_parameter(pb_dev, par);

		return PB_UNIMPLEMENTED;
	}

	return pb_dev ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_get_input(pb_dev_t dev, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->get_input)
			return pb_dev->funcs->get_input(pb_dev, slaveaddr, data, offset, datalen);

		return PB_UNIMPLEMENTED;
	}

	return PB_INVALID_HANDLE;
}
WINDLL_PROFIBUS int pb_set_output(pb_dev_t dev, unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->set_output)
			return pb_dev->funcs->set_output(pb_dev, slaveaddr, data, offset, datalen);

		return PB_UNIMPLEMENTED;
	}

	return PB_INVALID_HANDLE;
}

/* mode: USIF-State
 *       Offline      00h
 *       Stop         40h
 *       Clear        80h
 *       Operate      C0h
 */
WINDLL_PROFIBUS int pb_set_mode(pb_dev_t dev, enum pb_mode mode)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->set_mode)
			return pb_dev->funcs->set_mode(pb_dev, mode);

		if (mode != STOP)
			return PB_UNIMPLEMENTED;

		return PB_NO_ERROR;
	}

	return PB_INVALID_HANDLE;
}
WINDLL_PROFIBUS int pb_get_mode(pb_dev_t dev, enum pb_mode *mode)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && mode)
	{
		if (pb_dev->funcs->get_mode)
			return pb_dev->funcs->get_mode(pb_dev, mode);

		*mode = STOP;
		return PB_NO_ERROR;
	}

	return pb_dev ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_set_slave_param(pb_dev_t dev, unsigned char slaveaddr, const char *data, size_t datalen)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->set_slave_param)
			return pb_dev->funcs->set_slave_param(pb_dev, slaveaddr, data, datalen);

		return PB_UNIMPLEMENTED;
	}

	return PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_change_slave_addr(pb_dev_t dev, unsigned char slaveaddr, unsigned char addr,
					 unsigned short ident_number)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev)
	{
		if (pb_dev->funcs->change_slave_addr)
			return pb_dev->funcs->change_slave_addr(pb_dev, slaveaddr, addr, ident_number);

		return PB_UNIMPLEMENTED;
	}

	return PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_livelist(pb_dev_t dev, struct pb_livelist_info *livelist)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && livelist)
	{
		if (pb_dev->funcs->livelist)
			return pb_dev->funcs->livelist(pb_dev, livelist);
		return PB_UNIMPLEMENTED;
	}

	return pb_dev ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_connect(pb_dev_t dev, unsigned char slaveaddr,
			       unsigned short sendTimeOut, pb_slave_t *slave)
{
	struct pb_dev *pb_dev = dev;
	if (pb_dev && slave)
		return pb_dev->funcs->connect(pb_dev, 0, slaveaddr, sendTimeOut, slave);

	return PB_NULL_POINTER;
}

WINDLL_PROFIBUS int pb_connect_pa_link(pb_dev_t dev, unsigned char palinkaddr, unsigned char slaveaddr,
				       unsigned short sendTimeOut, pb_slave_t *slave)
{
	struct pb_dev *pb_dev = dev;

	if (!palinkaddr)
		return PB_INVALID_ARGS;

	if (pb_dev && slave)
		return pb_dev->funcs->connect(pb_dev, palinkaddr, slaveaddr, sendTimeOut, slave);

	return PB_NULL_POINTER;
}

WINDLL_PROFIBUS int pb_disconnect(pb_dev_t dev, pb_slave_t slave)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && slave)
		return pb_dev->funcs->disconnect(pb_dev, slave);

	return PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_read(pb_dev_t dev, pb_slave_t slave, struct pb_request *req)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && req)
	{
		if (req->datalen > 240)
			return PB_INVALID_DATALEN;

		return pb_dev->funcs->read(pb_dev, slave, req);
	}

	return pb_dev ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_write(pb_dev_t dev, pb_slave_t slave, struct pb_request *req)
{
	struct pb_dev *pb_dev = dev;

	if (pb_dev && req)
	{
		if (req->datalen > 240)
			return PB_INVALID_DATALEN;

		return pb_dev->funcs->write(pb_dev, slave, req);
	}

	return pb_dev ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_load_slave_param(pb_dev_t dev,
				const unsigned char address,
				const unsigned short ident_number,
				const unsigned char cfg_data[],
				const unsigned char cfg_data_len,
				const unsigned char user_prm_data[],
				const unsigned char user_prm_data_len,
				const unsigned char input_data_len,
				unsigned short *input_data_offset,
				const unsigned char output_data_len,
				unsigned short *output_data_offset)
{
	struct pb_dev *d = dev;
	if (d)
	{
		return d->funcs->load_slave_param(d, address, ident_number, cfg_data, cfg_data_len, user_prm_data, user_prm_data_len, input_data_len, input_data_offset, output_data_len, output_data_offset);
	}
	return d ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_get_slave_diag(pb_dev_t dev, const unsigned char slave_address, unsigned char *diag_data[], unsigned char *diag_data_len)
{
	struct pb_dev *d = dev;
	if (d)
	{
		return d->funcs->get_slave_diag(d, slave_address, diag_data, diag_data_len);
	}
	return d ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_read_system_diagnostic(pb_dev_t dev, unsigned char dest[16])
{
	struct pb_dev *d = dev;
	if (d)
	{
		return d->funcs->read_system_diagnostic(d, dest);
	}
	return d ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_read_data_transfer_list(pb_dev_t dev, unsigned char dest[16])
{
	struct pb_dev *d = dev;
	if (d)
	{
		return d->funcs->read_data_transfer_list(d, dest);
	}
	return d ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}

WINDLL_PROFIBUS int pb_change_slave_param(pb_dev_t dev, const unsigned char address,
				const unsigned char action,
				const unsigned char data_len,
				const unsigned char prm_data[])
{
	struct pb_dev *d = dev;
	if (d)
	{
		return d->funcs->change_slave_param(d, address, action, data_len, prm_data);
	}
	return d ? PB_NULL_POINTER : PB_INVALID_HANDLE;
}
