
/*
 * $Id: mutex_pthread.c,v 1.2 2009/06/09 14:47:49 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>

// other libs
#include <pthread.h>

// own libs

// own
#include "mutex.h"


struct mutex
{
	pthread_mutex_t mutex_;
	int locked_;
};

struct mutex *
mutex_create(void)
{
	struct mutex *m;

	m = malloc(sizeof(*m));
	if (m)
	{
		m->locked_ = 0;
		
		if (pthread_mutex_init(&(m->mutex_), NULL) != 0)
		{
			free(m);
			m = NULL;
		}
	}

	return m;
}

void
mutex_destroy(struct mutex *m)
{
	int ret;

	assert(m && !m->locked_);

	ret = pthread_mutex_destroy(&(m->mutex_));
	assert(ret == 0);

	free(m);
}

void
mutex_lock(struct mutex *m)
{
	int ret;

	assert(m);

	ret = pthread_mutex_lock(&(m->mutex_));
	assert(ret == 0);

	m->locked_ = 1;
}

void
mutex_unlock(struct mutex *m)
{
	int ret;

	assert(m && m->locked_);
	m->locked_ = 0;

	ret = pthread_mutex_unlock(&(m->mutex_));
	assert(ret == 0);
}
