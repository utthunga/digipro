
/*
 * $Id: condition.h,v 1.1 2005/11/15 15:34:32 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _condition_h
#define _condition_h

#include "profibus_config.h"

// C stdlib

// own libs

// own

#ifdef __cplusplus
extern "C"
{
#endif

struct condition;

struct condition *condition_create(void);
void condition_destroy(struct condition *);
void condition_lock(struct condition *);
void condition_unlock(struct condition *);
void condition_wait(struct condition *);
void condition_notify(struct condition *);
void condition_notify_all(struct condition *);

#ifdef __cplusplus
}
#endif

#endif /* _condition_h */
