
/*
 * $Id: mutex_win32.c,v 1.2 2005/11/21 22:38:25 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>

// own libs

// own
#include "mutex.h"

/* include windows header always at last */
#include <winsock2.h>


struct mutex
{
	CRITICAL_SECTION critsec_;
	int locked_;
};

struct mutex *
mutex_create(void)
{
	struct mutex *m;

	m = malloc(sizeof(*m));
	if (m)
	{
		m->locked_ = 0;
		InitializeCriticalSection(&(m->critsec_));
	}

	return m;
}

void
mutex_destroy(struct mutex *m)
{
	assert(m && !m->locked_);

	DeleteCriticalSection(&(m->critsec_));

	free(m);
}

void
mutex_lock(struct mutex *m)
{
	assert(m);

	EnterCriticalSection(&(m->critsec_));

	m->locked_ = 1;
}

void
mutex_unlock(struct mutex *m)
{
	assert(m && m->locked_);

	m->locked_ = 0;

	LeaveCriticalSection(&(m->critsec_));
}
