
/*
 * $Id: pb_dpem_err.c,v 1.4 2009/07/07 15:47:59 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <assert.h>
#include <stdlib.h>

#include "pb_dpem_err.h"

#ifndef _
# define _(x) x
#endif


const char *
pb_dpem_state_str(unsigned char state)
{
	const char *msg = NULL;

	switch (state)
	{
		/* OK */ case 0x00:
			msg = _("OK: Service completed without errors");
			break;
		/* FE */ case 0x01:
			msg = _("FE: Format error in request telegram");
			break;
		/* NI */ case 0x02:
			msg = _("NI: Function not available at responder");
			break;
		/* AD */ case 0x03:
			msg = _("AD: Access not allowed");
			break;
		/* EA */ case 0x04:
			msg = _("EA: Limit of area reached");
			break;
		/* LE */ case 0x05:
			msg = _("LE: Data block too large");
			break;
		/* RE */ case 0x06:
			msg = _("RE: Format error in response telegram");
			break;
		/* IP */ case 0x07:
			msg = _("IP: Error in a parameter");
			break;
		/* SC */ case 0x08:
			msg = _("SC: Function not allowed in current state");
			break;
		/* SE */ case 0x09:
			msg = _("SE: Sequence error");
			break;
		/* NE */ case 0x0a:
			msg = _("NE: Area not available at responder");
			break;
		/* DI */ case 0x0b:
			msg = _("DI: Data incomplete or with errors");
			break;
		/* NC */ case 0x0c:
			msg = _("NC: Master parameter set incompatible");
			break;
		/* NO */ case 0x10:
			msg = _("NO: Service not allowed in current state");
			break;
		/* LR */ case 0x13:
			msg = _("LR: Insufficient local resources");
			break;
		/* DS */ case 0x16:
			msg = _("DS: Local-FDL/PHY control not in token ring or disconnected from fieldbus");
			break;
		/* IV */ case 0x80:
			msg = _("IV: Parameter in request incorrect");
			break;
		/* TO */ case 0x17:
			msg = _("TO: Time out in function");
			break;
		/* RR */ case 0x18:
			msg = _("RR: Remote-FDL resources insufficient or not available");
			break;
		/* NA */ case 0x19:
			msg = _("NA: No reaction from remote user");
			break;
		/* RS */ case 0x1a:
			msg = _("RS: Remote SAP not activated");
			break;
		/* UE */ case 0x1b:
			msg = _("UE: Remote-DDLM/FDL interface error");
			break;
		/* NR */ case 0x1c:
			msg = _("NR: No response date available");
			break;
		/* WD */ case 0x1d:
			msg = _("WD: Avaiting function with further data (loading sequence)");
			break;
		/* NN */ /* case  0x??:
			msg = _("NN: Negative confirmation");
			break; */
		default:
			msg = _("??: Unknown value in STATE");
			break;
	}

	assert(msg);
	return msg;
}

const char *
pb_dpem_reason_code_str(unsigned char reason_code)
{
	const char *msg = NULL;

	switch (reason_code)
	{
		case 0x00:
			msg = _("No additional informations about the current error.");
			break;

		/* FDL */

		/* UE  */ case 0x01:
			msg = _("Reason_Code UE: Remote DDLM/FDL interface error");
			break;
		/* RR  */ case 0x02:
			msg = _("Reason_Code RR: Resources of remote FDL not sufficent/available");
			break;
		/* RS  */ case 0x03:
			msg = _("Reason_Code RS: Service or remote address not available at remote-LSAP or remote-SAP not activated");
			break;
		/* NR  */ case 0x09:
			msg = _("Reason_Code NR: No response data");
			break;
		/* DH  */ case 0x0a:
			msg = _("Reason_Code DH: Response data (high prior)");
			break;
		/* LR  */ case 0x0b:
			msg = _("Reason_Code LR: Local resources not sufficent");
			break;
		/* RDL */ case 0x0c:
			msg = _("Reason_Code RDL: Response data (low prior), no resources for sending");
			break;
		/* RDH */ case 0x0d:
			msg = _("Reason_Code RDH: Response data (high prior), no resources for sending");
			break;
		/* DS  */ case 0x0e:
			msg = _("Reason_Code DS: Local FDL/PHY control not within token ring or disconnected from bus");
			break;
		/* NA  */ case 0x0f:
			msg = _("Reason_Code NA: No response from remote FDL");
			break;

		/* MSAC2 */

		/* ABT_SE  */ case 0x11:
			msg = _("Reason_Code ABT_SE: Sequence error. Service not allowed at current state");
			break;
		/* ABT_FE  */ case 0x12:
			msg = _("Reason_Code ABT_FE: Received a incorrect request PDU");
			break;
		/* ABT_TO  */ case 0x13:
			msg = _("Reason_Code ABT_TO: Connection timeout");
			break;
		/* ABT_RE  */ case 0x14:
			msg = _("Reason_Code ABT_RE: Received a incorrect response PDU");
			break;
		/* ABT_IV  */ case 0x15:
			msg = _("Reason_Code ABT_IV: Incorrect service from user");
			break;
		/* ABT_STO */ case 0x16:
			msg = _("Reason_Code ABT_STO: Send_Timeout too small, Correct value within the Addition_Detail parameter");
			break;
		/* ABT_IA  */ case 0x17:
			msg = _("Reason_Code ABT_IA: Addition address information incorrect");
			break;
		/* ABT_OC  */ case 0x18:
			msg = _("Reason_Code ABT_OC: Waiting for a FDL_DATA_REPLY.con");
			break;
		/* REJ_PS  */ case 0x19:
			msg = _("Reason_Code REJ_PS: Max. number of parallel DP-functions reached");
			break;
		/* REJ_LE  */ case 0x1a:
			msg = _("Reason_Code REJ_LE: Max. PDU-length reached");
			break;

		/* SM_LR */ /* case 0x??:
			msg = _("Reason_Code SM_LR: Local resources insufficent");
			break; */
		/* SM_FE */ /* case 0x??:
			msg = _("Reason_Code SM_FE: Format error within the request telegram");
			break; */

		default:
			switch (reason_code & 0xf0)
			{
				/* FDL */ case 0x00:
					msg = _("Reason_Code ??: Unknown FDL value in REASON_CODE.");
					break;
				/* MSAC2 */ case 0x10:
					msg = _("Reason_Code ??: Unknown MSAC2 value in REASON_CODE.");
					break;
				/* user */ case 0x20:
					msg = _("Reason_Code ??: user value in REASON_CODE.");
					break;
				/* reserved */ case 0x30:
					msg = _("Reason_Code ??: reserved value in REASON_CODE.");
					break;
				/* invalid */ default:
					msg = _("Reason_Code ??: invalid value in REASON_CODE.");
					break;
			}
			break;
	}

	assert(msg);
	return msg;
}

static const char *
pb_dpem_errcode_1_str_application(unsigned char code, const char **descr)
{
	const char *msg = NULL;
	const char *add_msg = NULL;

	switch (code)
	{
		case 0x0:
			msg = _("Error_Class Application, Error_Code 0: read error");
			add_msg = _("Data can not be read from the application (may happen if there is an error in the EEPROM or in local bus)");
			break;
		case 0x1:
			msg = _("Error_Class Application, Error_Code 1: write error");
			add_msg = _("Data can not be written to the application (may happen if there is an error in the EEPROM or in local bus)");
			break;
		case 0x2:
			msg = _("Error_Class Application, Error_Code 2: module failure");
			add_msg = _("Happen, if a module is not available in a device anymore");
			break;
		case 0x3:
			msg = _("Error_Class Application, Error_Code 3: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x4:
			msg = _("Error_Class Application, Error_Code 4: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x5:
			msg = _("Error_Class Application, Error_Code 5: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x6:
			msg = _("Error_Class Application, Error_Code 6: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x7:
			msg = _("Error_Class Application, Error_Code 7: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x8      :
			msg = _("Error_Class Application, Error_Code 8: version conflict");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x9     :
			msg = _("Error_Class Application, Error_Code 9: feature not supported");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0xA:
			msg = _("Error_Class Application, Error_Code 10: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xB:
			msg = _("Error_Class Application, Error_Code 11: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xC:
			msg = _("Error_Class Application, Error_Code 12: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xD:
			msg = _("Error_Class Application, Error_Code 13: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xE:
			msg = _("Error_Class Application, Error_Code 14: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xF:
			msg = _("Error_Class Application, Error_Code 15: other");
			add_msg = _("The reason is unspecific");
			break;
	}

	assert(msg && add_msg);

	if (descr)
		*descr = add_msg;

	return msg;
}

static const char *
pb_dpem_errcode_1_str_access(unsigned char code, const char **descr)
{
	const char *msg = NULL;
	const char *add_msg = NULL;

	switch (code)
	{
		case 0x0:
			msg = _("Error_Class Access, Error_Code 0: invalid index");
			add_msg = _("The parameter can not be accessed because:\n - it is never used (for optional parameters, which are not implemented)\n - it is invisible at the moment.");
			break;
		case 0x1:
			msg = _("Error_Class Access, Error_Code 1: write length error");
			add_msg = _("The length in the write request does not match (larger or smaller) to the size of the parameter.");
			break;
		case 0x2:
			msg = _("Error_Class Access, Error_Code 2: invalid slot");
			add_msg = _("Accessed a slot that contains no parameters at all.");
			break;
		case 0x3:
			msg = _("Error_Class Access, Error_Code 3: type conflict");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x4:
			msg = _("Error_Class Access, Error_Code 4: invalid area");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x5:
			msg = _("Error_Class Access, Error_Code 5: state conflict");
			add_msg = _("Device is busy because it has to work internally. This may happen:\n - after writing the reset parameter\n - time delay during calibration\n - specific parameters are not changeble after the start-up of the device.");
			break;
		case 0x6:
			msg = _("Error_Class Access, Error_Code 6: access denied");
			add_msg = _("The parameter can not be written because the device is write protected.");
			break;
		case 0x7:
			msg = _("Error_Class Access, Error_Code 7: invalid range");
			add_msg = _("The parameter can not be written because the value is out of range e.g.: \n - configurations which are not allowed (e.g. fixed channel between FB and TB)\n - commands which are not supported (e.g. FACTORY_RESET)\n - functions which are not supported (e.g. LIN_TYPE sphere, invalid TARGET_MODE value)\n - a specific order in writing parameters is necessary, for consistency reasons");
			break;
		case 0x8:
			msg = _("Error_Class Access, Error_Code 8: invalid parameter");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x9:
			msg = _("Error_Class Access, Error_Code 9: invalid type");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0xA:      
			msg = _("Error_Class Access, Error_Code 10: read only");
			add_msg = _("The parameter can never be written.");
			break;
		case 0xB:
			msg = _("Error_Class Access, Error_Code 11: temporal invalid ");
			add_msg = _("The parameter can't be accessed because it is temporal invisible (not valid) at the moment.");
			break;
		case 0xC:
			msg = _("Error_Class Access, Error_Code 12: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xD:
			msg = _("Error_Class Access, Error_Code 13: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xE:
			msg = _("Error_Class Access, Error_Code 14: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xF:
			msg = _("Error_Class Access, Error_Code 15: other");
			add_msg = _("The reason is unspecific");
			break;
	}

	assert(msg && add_msg);

	if (descr)
		*descr = add_msg;

	return msg;
}

static const char *
pb_dpem_errcode_1_str_resource(unsigned char code, const char **descr)
{
	const char *msg = NULL;
	const char *add_msg = NULL;

	switch (code)
	{
		case 0x0:
			msg = _("Error_Class Resource, Error_Code 0: read constrain conflict");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x1:
			msg = _("Error_Class Resource, Error_Code 1: write constrain conflict");
			add_msg = _("not used in PA-Profile 3.0");
			break;
		case 0x2:         
			msg = _("Error_Class Resource, Error_Code 2: resource busy");
			add_msg = _("Because the device need some time for EEPROM access or get a download");
			break;
		case 0x3:
			msg = _("Error_Class Resource, Error_Code 3: resource unavailable");
			add_msg = _("Tried to access up-/download parameter objects but the device does not support up-/downloading.");
			break;
		case 0x4:
			msg = _("Error_Class Resource, Error_Code 4: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x5:
			msg = _("Error_Class Resource, Error_Code 5: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x6:
			msg = _("Error_Class Resource, Error_Code 6: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x7:
			msg = _("Error_Class Resource, Error_Code 7: reserved");
			add_msg = _("reserved by PROFIBUS-DP/DPV1");
			break;
		case 0x8:
			msg = _("Error_Class Resource, Error_Code 8: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0x9:
			msg = _("Error_Class Resource, Error_Code 9: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xA:
			msg = _("Error_Class Resource, Error_Code 10: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xB:
			msg = _("Error_Class Resource, Error_Code 11: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xC:
			msg = _("Error_Class Resource, Error_Code 12: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xD:
			msg = _("Error_Class Resource, Error_Code 13: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xE:
			msg = _("Error_Class Resource, Error_Code 14: manufacturer specific");
			add_msg = _("defined by manufacturer");
			break;
		case 0xF:
			msg = _("Error_Class Resource, Error_Code 15: other");
			add_msg = _("The reason is unspecific");
			break;
	}

	assert(msg && add_msg);

	if (descr)
		*descr = add_msg;

	return msg;
}

const char *
pb_dpem_errcode_1_str(unsigned char code, const char **descr)
{
	const char *msg = NULL;

	switch (code & 0xF0)
	{
		case 0xA0:
			msg = pb_dpem_errcode_1_str_application(code & 0x0F, descr);
			break;
		case 0xB0:
			msg = pb_dpem_errcode_1_str_access(code & 0x0F, descr);
			break;
		case 0xC0:
			msg = pb_dpem_errcode_1_str_resource(code & 0x0F, descr);
			break;

		case 0xD0:
		case 0xE0:
		case 0xF0:
			msg = _("Error_Class manufacturer specific");
			if (descr) *descr = _("defined by manufacturer");
			break;

		default:
			msg = _("Error_Class reserved");
			if (descr) *descr = _("reserved for future use");
			break;
	}

	return msg;
}

const char *
pb_dpem_errdecode(unsigned char ErrorDecode,
		  unsigned char ErrorCode1,
		  unsigned char ErrorCode2,
		  const char **descr)
{
	const char *msg = NULL;

	switch (ErrorDecode)
	{
		case 0x80: /* DPV1 / PNIORW */
			msg = pb_dpem_errcode_1_str(ErrorCode1, descr);
			break;
		case 0xFE: /* FMS */
			msg = _("Profibus FMS error code");
			break;
		case 0xFF: /* HART */
			msg = _("HART error code");
			break;

		default: /* all other values are reserved */
			msg = _("Reserved ErrorDecode Value");
			break;
	}

	assert(msg);
	return msg;
}
