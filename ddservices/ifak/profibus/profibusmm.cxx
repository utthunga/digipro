
/* 
 * $Id: profibusmm.cxx,v 1.9 2009/06/23 14:45:14 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <stdexcept>
#include "profibusmm.h"


namespace io_driver
{

const char *
pb::errstr(int result, const char **descr)
{
	return pb_errstr0(result, descr);
}

const char *
pb::errstr(int result, const char **descr, unsigned char driver)
{
	return pb_errstr(result, descr, driver);
}

const char *
pb::errstr(int result, unsigned char driver)
{
	return pb_errstr(result, NULL, driver);
}

pb::device_info_t
pb::probe(void)
{
	info_t info[32];
	device_info_t ret;
	int i, found;

	found = pb_probe(info, 32);
	ret.resize(found);

	for (i = 0; i < found; i++)
		ret[i] = info[i];

	return ret;
}

std::vector<std::string>
pb::baudnames(void)
{
	std::vector<std::string> ret;
	const char **names;
	size_t len;

	names = pb_baudnames(&len);
	ret.reserve(len);

	for (size_t i = 0; i < len; i++)
		ret.push_back(names[i]);

	return ret;
}

std::string
pb::baud2name(baudrate_t baud)
{
	return pb_baud2name(baud);
}

pb::baudrate_t
pb::name2baud(const std::string &name)
{
	return pb_name2baud(name.c_str());
}

pb::pb(unsigned char driver, unsigned char device, unsigned char channel, const bus_parameter_t &par)
{
	int ret;

	ret = pb_open(driver, device, channel, &par, &dev);
	if (ret)
		throw ret; 
}

pb::~pb(void)
{
	pb_close(dev);
}

int
pb::ioctl(int request, void *arg1, void *arg2)
{
	return pb_ioctl(dev, request, arg1, arg2);
}

const char *
pb::err2str(int result, const char **descr)
{
	return pb_err2str(dev, result, descr);
}

int
pb::set_bus_parameter(const bus_parameter_t &par)
{
	return pb_set_bus_parameter(dev, &par);
}

int
pb::set_mode(mode_t mode)
{
	return pb_set_mode(dev, mode);
}
int
pb::get_mode(mode_t &mode)
{
	return pb_get_mode(dev, &mode);
}

int
pb::change_slave_addr(unsigned char slaveaddr, unsigned char addr, unsigned short ident_number)
{
	return pb_change_slave_addr(dev, slaveaddr, addr, ident_number);
}

int
pb::livelist(livelist_info_t &info)
{
	return pb_livelist(dev, &info);
}

int
pb::connect(unsigned char slaveaddr, unsigned short sendTimeOut, slave_t &slave)
{
	return pb_connect(dev, slaveaddr, sendTimeOut, &slave);
}
int
pb::connect_pa_link(unsigned char palinkaddr, unsigned char slaveaddr, unsigned short sendTimeOut, slave_t &slave)
{
	return pb_connect_pa_link(dev, palinkaddr, slaveaddr, sendTimeOut, &slave);
}
int
pb::disconnect(slave_t slave)
{
	return pb_disconnect(dev, slave);
}
int
pb::read(slave_t slave, request_t &req)
{
	return pb_read(dev, slave, &req);
}
int
pb::write(slave_t slave, request_t &req)
{
	return pb_write(dev, slave, &req);
}

int
pb::get_input(unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	return pb_get_input(dev, slaveaddr, data, offset, datalen);
}
	
int
pb::set_output(unsigned char slaveaddr, unsigned char *data, unsigned int offset, size_t datalen)
{
	return pb_set_output(dev, slaveaddr, data, offset, datalen);
}

int
pb::load_slave_param(
				const unsigned char address,
				const unsigned short ident_number,
				const unsigned char cfg_data[],
				const unsigned char cfg_data_len,
				const unsigned char user_prm_data[],
				const unsigned char user_prm_data_len,
				const unsigned char input_data_len,
				unsigned short *input_data_offset,
				const unsigned char output_data_len,
				unsigned short *output_data_offset)
{
	return pb_load_slave_param(dev, address, ident_number, cfg_data, cfg_data_len, user_prm_data, user_prm_data_len, input_data_len, input_data_offset, output_data_len, output_data_offset);
}

int
pb::get_slave_diag(const unsigned char slave_address, unsigned char *diag_data[], unsigned char *diag_data_len)
{
	return pb_get_slave_diag(dev, slave_address, diag_data, diag_data_len);
}

int
pb::read_system_diagnostic(unsigned char dest[16])
{
	return pb_read_system_diagnostic(dev, dest);
}

int
pb::read_data_transfer_list(unsigned char dest[16])
{
	return pb_read_data_transfer_list(dev, dest);
}

int
pb::change_slave_param(const unsigned char address,
				const unsigned char action,
				const unsigned char data_len,
				const unsigned char prm_data[])
{
	return pb_change_slave_param(dev, address, action, data_len, prm_data);
}

} /* namespace io_driver */
