
/*
 * $Id: profibus_config.h,v 1.4 2005/11/21 22:38:25 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _profibus_config_h
#define _profibus_config_h

#ifdef _MSC_VER

/* export as DLL */
# ifdef PROFIBUS_AS_DLL
/* DLL import/export defines */
#  ifdef PROFIBUS_DLL
#   define WINDLL_PROFIBUS __declspec(dllexport)
#  else
#   define WINDLL_PROFIBUS __declspec(dllimport)
#  endif
# endif

/* ignore stupid non-dll interface warnings in use of stdc++ */
# pragma warning (disable: 4251)
/* ignore name for debugger to long warnings on it's own STL */
# pragma warning (disable: 4786)

#endif

#ifndef WINDLL_PROFIBUS
# define WINDLL_PROFIBUS
#endif

#ifdef WIN32
# define SYSTEM_WIN32
#else
# define SYSTEM_PTHREAD
# define SYSTEM_UNIX
# define _cdecl
#endif

#endif /* _profibus_config_h */
