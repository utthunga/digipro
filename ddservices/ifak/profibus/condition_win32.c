
/*
 * $Id: condition_win32.c,v 1.3 2005/11/29 10:31:23 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>

// own libs

// own
#include "condition.h"
#include "mutex.h"

/* include windows header always at last */
#include <winsock2.h>


struct condition
{
	struct mutex *mutex_;
	HANDLE handle_;
};


struct condition *
condition_create(void)
{
	struct condition *c;

	c = malloc(sizeof(*c));
	if (c)
	{
		c->mutex_ = mutex_create();
		c->handle_ = CreateEvent(0, FALSE, FALSE, NULL);

		if (!c->mutex_ || !c->handle_)
		{
			if (c->mutex_)
				mutex_destroy(c->mutex_);

			if (c->handle_)
				CloseHandle(c->handle_);

			free(c);
			c = NULL;
		}
	}

	return c;
}

void
condition_destroy(struct condition *c)
{
	assert(c);

	mutex_destroy(c->mutex_);
	CloseHandle(c->handle_);
	free(c);
}

void
condition_lock(struct condition *c)
{
	assert(c);

	mutex_lock(c->mutex_);
}

void
condition_unlock(struct condition *c)
{
	assert(c);

	mutex_unlock(c->mutex_);
}

void
condition_wait(struct condition *c)
{
	assert(c);

	condition_unlock(c);

	WaitForSingleObject(c->handle_, INFINITE);

	condition_lock(c);
}

void
condition_notify(struct condition *c)
{
	assert(c);

	SetEvent(c->handle_);
}

void
condition_notify_all(struct condition *c)
{
	assert(c);

	SetEvent(c->handle_);
}
