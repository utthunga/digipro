
/* 
 * $Id:  $
 *
 * Copyright (C) 2011 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>
#include <stdlib.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{

/**
 * C++ version of settimeofday.
 */
int
settimeofday(struct timeval *tp,
	     struct timezone *tzp)
{
	int ret = ::settimeofday(tp, tzp);

	if (ret != 0)
		throw error_errno(errno);

	return ret;
}

} /* namespace sys */
