
/* 
 * $Id: rmdir.cxx,v 1.1 2008/07/01 21:09:57 mmeier Exp $
 *
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#ifdef SYSTEM_UNIX
# include <unistd.h>
# define RMDIR ::rmdir
#endif

#ifdef SYSTEM_WIN32
# include <direct.h>
# define RMDIR ::_rmdir
#endif


namespace sys
{

/**
 * C++ version of rmdir(2).
 */
void
rmdir(const std::string &path)
{
	int ret = RMDIR(path.c_str());

	if (ret != 0)
		throw error_errno(errno);
}

} /* namespace sys */
