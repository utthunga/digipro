
/*
 * 
 * Copyright (C) 2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "uuid.h"


namespace sys
{

uuid::uuid(void)
{
	memset(m_uuid, 0, 16);
	generate();
}

void
uuid::generate(void)
{
	uuid_generate(m_uuid);
	m_data.clear();
}

void
uuid::from_string(const std::string &data)
{
	uuid_parse(data.c_str(), m_uuid);
	m_data.clear();
}

std::string
uuid::to_string(void) const throw()
{
	if (!m_data.empty())
		return m_data;

	char out[37];

	uuid_unparse(m_uuid, out);

	m_data = out;

	return m_data;
}

} /* namespace sys */
