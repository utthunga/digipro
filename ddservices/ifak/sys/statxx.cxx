
/*
 * $Id: statxx.cxx,v 1.1 2005/06/23 13:28:52 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own libs

// own
#include "statxx.h"
#include "error_errno.h"


namespace sys
{

int
stat(const std::string &path, class statxx &st)
{
	return ::stat(path.c_str(), st.c_st());
}

} /* namespace sys */
