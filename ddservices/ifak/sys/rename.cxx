
/* 
 * $Id: rename.cxx,v 1.3 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>
#include <stdio.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{

/**
 * C++ version of rename(2).
 */
void
rename(const std::string &from, const std::string &to)
{
#ifdef SYSTEM_WIN32
	_unlink(to.c_str());
#endif

	int ret = ::rename(from.c_str(), to.c_str());

	if (ret != 0)
		throw error_errno(errno);
}

} /* namespace sys */
