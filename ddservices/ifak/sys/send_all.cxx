
/*
 * $Id: send_all.cxx,v 1.1 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

void
send_all(int s, const void *msg, size_t len, int flags)
{
	ssize_t ret;

retry:
	ret = ::send(s, SOCKET_SEND_BUFPTR(msg), len, flags);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	assert(ret >= 0);
	size_t done = ret;

	if (done < len)
	{
		const char *s = static_cast<const char *>(msg);

		s += done; msg = s;
		len -= done;

		goto retry;
	}
}

} /* namespace sys */
