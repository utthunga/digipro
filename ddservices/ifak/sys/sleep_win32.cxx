
/*
 * $Id: sleep_win32.cxx,v 1.2 2005/03/04 09:02:22 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib

// own libs

// own
#include "sys.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

void
sleep(size_t seconds)
{
	Sleep(seconds * 1000);
}

} /* namespace sys */
