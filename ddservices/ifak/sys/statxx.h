
/*
 * $Id: statxx.h,v 1.2 2009/07/18 09:40:57 fna Exp $
 * 
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_statxx_h
#define _sys_statxx_h

#include "sys_config.h"

// C stdlib
#include <sys/types.h>
#include <sys/stat.h>

// C++ stdlib
#include <string>

// own libs

// own
#include "sys_types.h"


namespace sys
{

class statxx;

WINDLL_SYS int stat(const std::string &path, class statxx &);

#ifdef _MSC_VER
# define S_ISREG(m)	((m & _S_IFMT) == _S_IFREG)
# define S_ISDIR(m)	((m & _S_IFMT) == _S_IFDIR)
#endif

class WINDLL_SYS statxx
{
public:
	bool isreg(void) const throw() { return S_ISREG(st.st_mode); }
	bool isdir(void) const throw() { return S_ISDIR(st.st_mode); }

	unsigned long size(void) const throw() { return st.st_size; }

	time_t last_accessed(void) const throw() { return st.st_atime; }

protected:
	friend int stat(const std::string &path, class statxx &);

	struct stat *c_st(void) throw() { return &st; }

private:
	struct stat st;
};

} /* namespace sys */

#endif /* _sys_statxx_h */
