
/*
 * $Id: statxx.h,v 1.2 2009/07/18 09:40:57 fna Exp $
 * 
 * Copyright (C) 2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_uuid_h
#define _sys_uuid_h

#include "sys_config.h"

// C stdlib

#ifdef SYSTEM_UNIX
#include <uuid/uuid.h>
#else
#include <Rpc.h>
#include <Rpcdce.h>
#endif

// C++ stdlib

// own libs

// own
#include "sys_types.h"


namespace sys
{

class WINDLL_SYS uuid
{
public:
	uuid(void);

	void generate(void);

	/* string conversion */
	void from_string(const std::string &data);
	std::string to_string(void) const throw();

private:
	mutable std::string m_data;

#ifdef SYSTEM_UNIX
	uuid_t m_uuid;
#else
	UUID m_uuid;
#endif

};

} /* namespace sys */

#endif /* _sys_uuid_h */
