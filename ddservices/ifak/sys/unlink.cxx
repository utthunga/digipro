
/* 
 * $Id: unlink.cxx,v 1.2 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#ifdef SYSTEM_UNIX
# include <unistd.h>
# define UNLINK ::unlink
#endif

#ifdef SYSTEM_WIN32
# include <stdio.h>
# define UNLINK ::_unlink
#endif


namespace sys
{

/**
 * C++ version of unlink(2).
 */
void
unlink(const std::string &path)
{
	int ret = UNLINK(path.c_str());

	if (ret != 0)
		throw error_errno(errno);
}

} /* namespace sys */
