
/*
 * $Id: select_win32.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

int
select(int nfds, fd_set *rfds, fd_set *wfds, fd_set *efds, struct timeval *timeout)
{
	int ret;

	ret = ::select(nfds, rfds, wfds, efds, timeout);

	if (ret == -1)
		throw error_win32(WSAGetLastError());

	assert(ret >= 0);
	return ret;
}

} /* namespace sys */
