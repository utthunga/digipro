
/*
 * $Id: inet_addr.cxx,v 1.2 2010/02/04 14:52:54 rsc Exp $
 * 
 * Copyright (C) 2010 Robert Schneckenhaus <trobt@web.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "bsdsocket.h"

#ifndef INADDR_NONE
# define INADDR_NODE (in_addr_t)(-1)
#endif

namespace sys
{

in_addr_t
inet_addr(const char *addr)
{
	in_addr_t ret = INADDR_NONE;

#ifdef __CYGWIN__
	assert(0);
#else
	ret = ::inet_addr(addr);
#endif

	return ret; 
}

} /* namespace sys */
