
/*
 * $Id: sleep.cxx,v 1.2 2005/03/04 08:14:56 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"


namespace sys
{

void
sleep(size_t seconds)
{
	::sleep(seconds);
}

} /* namespace sys */
