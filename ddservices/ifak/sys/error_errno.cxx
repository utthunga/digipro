
/*
 * $Id: error_errno.cxx,v 1.3 2006/06/02 19:31:33 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib

// own libs

// own
#include "error_errno.h"


namespace sys
{

const char *
error_errno::what(void) const throw()
{
	return strerror(errno_);
}

} /* namespace sys */
