
/* 
 * $Id: dirent.cxx,v 1.2 2008/05/15 11:30:51 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <dirent.h>
#include <sys/types.h>

// C++ stdlib
#include <memory>
#include <string.h>
#include <dirent.h>

// own header
#include "string_edd.h"

// own
#include "dirent_edd.h"


namespace sys
{

struct dir
{
	::DIR *handle;
};

DIR *
opendir(const std::string &path)
{
	std::auto_ptr<DIR> dir(new DIR);
	
	dir->handle = ::opendir(path.c_str());
	if (!dir->handle)
		dir.reset(NULL);
	
	return dir.release();
}

std::string
readdir(DIR *dirp)
{
	struct dirent *entry;

	entry = ::readdir(dirp->handle);
	if (entry)
		return entry->d_name;

	return std::string();
}

void
closedir(DIR *dirp)
{
	::closedir(dirp->handle);
	delete dirp;
}

} /* namespace sys */
