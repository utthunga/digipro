
/*
 * $Id: freeaddrinfo.cxx,v 1.2 2005/03/03 23:17:06 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "bsdsocket.h"


namespace sys
{

void
freeaddrinfo(struct addrinfo *ai)
{
#ifdef __CYGWIN__
	assert(0);
#else
	::freeaddrinfo(ai);
#endif
}

} /* namespace sys */
