
/*
 * $Id: sys.h,v 1.7 2008/07/01 21:09:57 mmeier Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_sys_h
#define _sys_sys_h

#include "sys_config.h"

// C stdlib
#include <sys/types.h>
#include <time.h>
#ifdef SYSTEM_UNIX
#include <sys/time.h>
#endif

// C++ stdlib
#include <string>

// own libs

// own
#include "sys_types.h"


/**
 * Namespace for the system call mapping library.
 */
namespace sys
{

WINDLL_SYS std::string getcwd(void);

WINDLL_SYS void chdir(const std::string &);

WINDLL_SYS int clock_gettime(clockid_t clk_id, struct timespec *tp);

WINDLL_SYS int gettimeofday (struct timeval *tp, struct timezone *tzp);

WINDLL_SYS void mkdir(const std::string &, int );

WINDLL_SYS void rename(const std::string &from, const std::string &to);

WINDLL_SYS void rmdir(const std::string &);

WINDLL_SYS int settimeofday (struct timeval *tp, struct timezone *tzp);

WINDLL_SYS void sleep(size_t seconds);

WINDLL_SYS void system(const std::string &);

WINDLL_SYS void unlink(const std::string &);

WINDLL_SYS void usleep(useconds_t microseconds);

} /* namespace sys */

#endif /* _sys_sys_h */
