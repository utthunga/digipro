
/*
 * $Id: pollemulation.cxx,v 1.3 2006/05/31 13:14:31 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

int
poll(struct pollfd *fds, unsigned int nfds, int timeout)
{
	int ret;

	fd_set rfds, wfds, xfds;

	FD_ZERO(&rfds);
	FD_ZERO(&wfds);
	FD_ZERO(&xfds);

	struct pollfd *pfds = fds;
	size_t i;

#define LEGAL_FLAGS \
	(POLLIN | POLLOUT | POLLPRI)

	for (i = 0; i < nfds; i++)
	{
		if ((pfds[i].events | LEGAL_FLAGS) != LEGAL_FLAGS)
			throw error_errno(EINVAL);

		if (pfds[i].events & POLLIN)
			FD_SET(pfds[i].fd, &rfds);
		if (pfds[i].events & POLLOUT)
			FD_SET(pfds[i].fd, &wfds);
		if (pfds[i].events & POLLPRI)
			FD_SET(pfds[i].fd, &xfds);
	}

	struct timeval tv;
	struct timeval *tvptr = NULL;

	if (timeout < -1)
		throw error_errno(EINVAL);

	if (timeout >= 0)
	{
		tvptr = &tv;
		tv.tv_sec = timeout / 1000;
		tv.tv_usec = (timeout % 1000) * 1000;
	}

	ret = select(nfds, &rfds, &wfds, &xfds, tvptr);
	assert(ret >= 0);

	/* convert results back */
	for (i = 0; i < nfds; i++)
	{
		pfds[i].revents = 0;

		if (FD_ISSET(pfds[i].fd, &rfds))
			pfds[i].revents = POLLIN;
		if (FD_ISSET(pfds[i].fd, &wfds))
			pfds[i].revents |= POLLOUT;
		if (FD_ISSET(pfds[i].fd, &xfds))
			pfds[i].revents |= POLLPRI;
	}

	return ret;
}

} /* namespace sys */
