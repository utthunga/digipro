
/*
 * $Id: socket_win32.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_win32.h"


namespace sys
{

int
socket(int domain, int type, int protocol)
{
	int ret;

	ret = ::socket(domain, type, protocol);

	if (ret == -1)
		throw error_win32(WSAGetLastError());

	assert(ret >= 0);
	return ret;
}

/* special WIN32 initialization */

namespace /* anonymous */
{

class init_win32_sockets
{
public:
	init_win32_sockets(void);
	~init_win32_sockets(void);
};

class init_win32_sockets init_win32_sockets;

init_win32_sockets::init_win32_sockets(void)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD(2, 0);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0)
		/* "WinSock initialization failure" */
		assert(err == 0);

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 0)
		/* "WinSock version mismatch (2.0 or compatible required)" */
		assert(0);
}

init_win32_sockets::~init_win32_sockets(void)
{
	WSACleanup();
}

} /* anonymous namespace */

} /* namespace sys */
