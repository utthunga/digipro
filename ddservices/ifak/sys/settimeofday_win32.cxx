
/* 
 * $Id:  $
 *
 * Copyright (C) 2011 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// Windows
#include <windows.h>

// C stdlib
#include <errno.h>
#include <stdlib.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{

/**
 * C++ version of settimeofday.
 */
int
settimeofday(struct timeval *tp,
	     struct timezone *tzp)
{
	// !!!! Attention !!!!
	// data types of parameters must be adapted, original is comming from ptpv2d
	// up to now not tested

	int ret = 0;
	unsigned long long U64WindowsTime;
	FILETIME           WindowsUTCTime;
	SYSTEMTIME         WindowsSystemTime;


	U64WindowsTime = (unsigned long long)tp->tv_sec;

	// Change epoch in seconds from January 1, 1970
	// to Windows January 1, 1601

	U64WindowsTime += 11644473600ULL;

/*	// Change seconds from January 1, 1601 UTC time to TAI time

	U64WindowsTime  -= (unsigned long long) utc_offset;*/

	// Convert seconds to 100 ns increments since January 1, 1601

	U64WindowsTime *= 10000ULL;

	// Add in nanoseconds converted to 100 ns increments

	U64WindowsTime += (unsigned long long)(tp->tv_usec * 10U);

	// Convert unsigned long long 64 bit variable to
	// File time structure time

	WindowsUTCTime.dwLowDateTime  = (unsigned long)(U64WindowsTime & (unsigned long long)0xFFFFFFFF);
	WindowsUTCTime.dwHighDateTime = (unsigned long)(U64WindowsTime >> 32);

	// Convert Windows UTC "file time" to Windows UTC "system time"

	ret = (::FileTimeToSystemTime(&WindowsUTCTime, &WindowsSystemTime)) ? 0 : -1;

	if (ret != 0)
		throw error_errno(errno);

	// Now finally we can set the windows system time
	ret = (::SetSystemTime(&WindowsSystemTime)) ? 0 : -1; // Sets the current system time

	if (ret != 0)
		throw error_errno(errno);

	return ret;
}

} /* namespace sys */
