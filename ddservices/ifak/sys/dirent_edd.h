
/* 
 * $Id: dirent_edd.h,v 1.1 2005/06/23 13:28:52 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_dirent_h
#define _sys_dirent_h

#include "sys_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own libs

// own


namespace sys
{

struct dir;
typedef struct dir DIR;

WINDLL_SYS DIR *opendir(const std::string &path);
WINDLL_SYS std::string readdir(DIR *);
WINDLL_SYS void closedir(DIR *);

} /* namespace sys */

#endif /* _sys_dirent_h */
