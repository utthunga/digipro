
/*
 * $Id: error_win32.h,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_error_win32_h
#define _sys_error_win32_h

#include "sys_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib
#include <exception>
#include <string>

// own libs

// own


namespace sys
{

class WINDLL_SYS error_win32 : public std::exception
{
public:
	error_win32(int nr);
	~error_win32(void) throw() { }

	virtual const char *what(void) const throw();

	const int nr_;

private:
	mutable std::string msg_;
};

} /* namespace sys */

#endif /* _sys_error_win32_h */
