
/*
 * $Id: recvfrom.cxx,v 1.2 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

ssize_t
recvfrom(int s, void *buf, size_t len, int flags,
	 struct sockaddr *from, socklen_t *fromlen)
{
	ssize_t ret;

retry:
	ret = ::recvfrom(s, SOCKET_RECV_BUFPTR(buf), len, flags, from, fromlen);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	return ret;
}

} /* namespace sys */
