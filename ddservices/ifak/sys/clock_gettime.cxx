
/* 
 * $Id:  $
 *
 * Copyright (C) 2011 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>
#include <stdlib.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{

/**
 * C++ version of clock_gettime.
 */
int
clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int ret = ::clock_gettime(clk_id, tp);

	if (ret != 0)
		throw error_errno(errno);

	return ret;
}

} /* namespace sys */
