
/*
 * $Id: bsdsocket.h,v 1.10 2010/02/08 10:32:47 rsc Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_bsdsocket_h
#define _sys_bsdsocket_h

#include "sys_config.h"

// C stdlib

// C++ stdlib

// own libs

// own
#include "sys_types.h"


#ifdef SYSTEM_WIN32

// windows header
#include <winsock2.h>
#include <ws2tcpip.h>

#if defined(min) || defined(max)
# undef min
# undef max
#endif

/* missing defines */
#define SHUT_RD   SD_RECEIVE
#define SHUT_WR   SD_SEND
#define SHUT_RDWR SD_BOTH

/* missing types */
typedef int socklen_t;
typedef unsigned long in_addr_t;

/* old style BSD interface, require char * instead void * */
#define SOCKET_RECV_BUFPTR(ptr) static_cast<char *>(ptr)
#define SOCKET_SEND_BUFPTR(ptr) static_cast<const char *>(ptr)

/* poll emulation */
struct pollfd
{
	int	fd;		/* file descriptor */
	short	events;		/* events to look for */
	short	revents;	/* events returned */
};
#define POLLIN	0x1
#define POLLOUT	0x2
#define POLLPRI	0x4

#endif /* SYSTEM_WIN32 */

#ifdef SYSTEM_UNIX
# include <poll.h>
# include <netdb.h>
# include <netinet/in.h>
# include <sys/socket.h>
# include <sys/un.h>
# include <arpa/inet.h>
#endif

#ifndef SOCKET_RECV_BUFPTR
# define SOCKET_RECV_BUFPTR(ptr) ptr
# define SOCKET_SEND_BUFPTR(ptr) ptr
#endif


namespace sys
{

/* syscalls */

WINDLL_SYS int socket(int domain, int type, int protocol);

WINDLL_SYS int accept(int s, struct sockaddr *addr, socklen_t *addrlen);

WINDLL_SYS void connect(int s, const struct sockaddr *name, socklen_t namelen);

WINDLL_SYS void bind(int s, const struct sockaddr *name, socklen_t namelen);

WINDLL_SYS int fcntl(int fd, int cmd, long arg);

WINDLL_SYS void listen(int s, int backlog);

WINDLL_SYS ssize_t send(int s, const void *msg, size_t len, int flags);

WINDLL_SYS void send_all(int s, const void *msg, size_t len, int flags);

WINDLL_SYS ssize_t recv(int s, void *buf, size_t len, int flags);

WINDLL_SYS bool recv_all(int s, void *buf, size_t len, int flags);

WINDLL_SYS ssize_t sendto(int s, const void *msg, size_t len, int flags,
			  const struct sockaddr *to, socklen_t tolen);

WINDLL_SYS ssize_t recvfrom(int s, void *buf, size_t len, int flags,
			    struct sockaddr *from, socklen_t *fromlen);

WINDLL_SYS void shutdown(int s, int how);

WINDLL_SYS void close(int d);

WINDLL_SYS int poll(struct pollfd *fds, unsigned int nfds, int timeout);

WINDLL_SYS int select(int nfds, fd_set *rfds, fd_set *wfds, fd_set *efds,
		      struct timeval *timeout);

WINDLL_SYS void getsockname(int s, struct sockaddr *name, socklen_t *namelen);

WINDLL_SYS void getpeername(int s, struct sockaddr *name, socklen_t *namelen);

/* DNS */

WINDLL_SYS void getaddrinfo(const char *nodename, const char *servname,
			    const struct addrinfo *hints, struct addrinfo **res);
WINDLL_SYS void getnameinfo(const struct sockaddr *sa, socklen_t salen,
			    char *host, size_t hostlen,
			    char *serv, size_t servlen, int flags);

WINDLL_SYS void freeaddrinfo(struct addrinfo *ai);

/* helper */

WINDLL_SYS void inet_ntop(int af, const void *src, char *dst, socklen_t size);

WINDLL_SYS in_addr_t inet_addr(const char* addr);

} /* namespace sys */

#endif /* _sys_bsdsocket_h */
