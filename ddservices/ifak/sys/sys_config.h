
/*
 * $Id: sys_config.h,v 1.4 2009/02/04 14:37:13 fna Exp $
 * 
 * Copyright (C) 2005-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_config_h
#define _sys_config_h

#if defined(_MSC_VER) || defined(__MINGW32__)
# ifdef SYS_AS_DLL
/* DLL import/export defines */
#  ifdef SYS_DLL
#   define WINDLL_SYS __declspec(dllexport)
#  else
#   define WINDLL_SYS __declspec(dllimport)
#  endif
# endif
/* ignore stupid non-dll interface warnings in use of stdc++ */
# pragma warning (disable: 4251)
/* ignore name for debugger to long warnings on it's own STL */
# pragma warning (disable: 4786)
#endif

#ifndef WINDLL_SYS
# define WINDLL_SYS
#endif

#if defined(_WIN32) && !defined(WIN32)
# define WIN32
#endif

#ifdef WIN32
# define SYSTEM_WIN32
# if !defined(_MSC_VER) && !defined(__MINGW32__)
#  error Only VC++ and MinGW supported yet!
# endif
/* enable Windows 2000 API */
# ifdef _WIN32_WINNT
#  undef _WIN32_WINNT /* avoid VS compiler warnings */
# endif
# define _WIN32_WINNT 0x0501
# ifdef WINVER
#  undef WINVER /* avoid VS compiler warnings */
# endif
# define WINVER 0x0500
#else
# define SYSTEM_UNIX
# define SYSTEM_PTHREAD
#endif

#endif /* _sys_config_h */
