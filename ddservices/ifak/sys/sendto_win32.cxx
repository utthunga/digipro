
/*
 * $Id: sendto_win32.cxx,v 1.2 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

ssize_t
sendto(int s, const void *msg, size_t len, int flags,
       const struct sockaddr *to, socklen_t tolen)
{
	ssize_t ret;

	ret = ::sendto(s, SOCKET_SEND_BUFPTR(msg), len, flags, to, tolen);

	if (ret == -1)
		throw error_win32(WSAGetLastError());

	return ret;
}

} /* namespace sys */
