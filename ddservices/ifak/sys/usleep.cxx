
/*
 * $Id: usleep.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

void
usleep(useconds_t microseconds)
{
	int ret;

	if (microseconds >= 1000000)
		throw error_errno(EINVAL);

	ret = ::usleep(microseconds);

	if (ret == -1)
		throw error_errno(errno);

	assert(ret == 0);
}

} /* namespace sys */
