
/*
 * $Id: recv_all_win32.cxx,v 1.1 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

bool
recv_all(int s, void *buf, size_t len, int flags)
{
	ssize_t ret;

retry:
	ret = ::recv(s, SOCKET_RECV_BUFPTR(buf), len, flags);

	if (ret == -1)
		throw error_win32(WSAGetLastError());

	if (ret == 0)
		/* connection has been closed */
		return false;

	assert(ret > 0);
	size_t done = ret;

	if (done < len)
	{
		char *s = static_cast<char *>(buf);

		s += done; buf = s;
		len -= done;

		goto retry;
	}

	return true;
}

} /* namespace sys */
