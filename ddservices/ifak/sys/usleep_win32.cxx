
/*
 * $Id: usleep_win32.cxx,v 1.3 2006/05/31 13:14:31 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib

// own libs

// own
#include "error_errno.h"
#include "sys.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

void
usleep(useconds_t microseconds)
{
	if (microseconds >= 1000000)
		throw error_errno(EINVAL);

	Sleep(microseconds / 1000);
}

} /* namespace sys */
