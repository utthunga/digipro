
/*
 * $Id: getnameinfo.cxx,v 1.2 2009/04/06 23:59:50 fna Exp $
 * 
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "bsdsocket.h"


namespace sys
{

void
getnameinfo(const struct sockaddr *sa, socklen_t salen,
	    char *host, size_t hostlen,
	    char *serv, size_t servlen, int flags)
{
#ifdef __CYGWIN__
	assert(0);
#else
	int ret;

	ret = ::getnameinfo(sa, salen, host, hostlen, serv,
			    servlen, flags);
	if (ret)
		throw std::runtime_error(gai_strerror(ret));
#endif
}

} /* namespace sys */
