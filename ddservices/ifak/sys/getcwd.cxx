
/* 
 * $Id: getcwd.cxx,v 1.5 2008/05/15 11:30:51 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>
#include <stdlib.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#ifdef SYSTEM_UNIX
# include <unistd.h>
# define GETCWD ::getcwd
#endif

#ifdef SYSTEM_WIN32
# include <direct.h>
# define GETCWD ::_getcwd
#endif


namespace sys
{

/**
 * C++ version of getcwd(3).
 */
std::string
getcwd(void)
{
	std::string cwd(".");
	char *str;

	str = GETCWD(NULL, 0);
	if (!str)
		throw error_errno(errno);

	cwd = str;
	free(str);

	return cwd;
}

} /* namespace sys */
