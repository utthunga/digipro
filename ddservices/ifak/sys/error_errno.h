
/*
 * $Id: error_errno.h,v 1.3 2006/06/02 19:31:33 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_error_errno_h
#define _sys_error_errno_h

#include "sys_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib
#include <exception>
#include <string>

// own libs

// own


namespace sys
{

class WINDLL_SYS error_errno : public std::exception
{
public:
	error_errno(int e) throw() : errno_(e) { }
	~error_errno(void) throw() { }
	virtual const char *what(void) const throw();

	const int errno_;
};

} /* namespace sys */

#endif /* _sys_error_errno_h */
