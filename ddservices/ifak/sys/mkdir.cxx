
/* 
 * $Id: chdir.cxx,v 1.4 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#ifdef SYSTEM_UNIX
# include <sys/stat.h>
# define MKDIR(a,b) ::mkdir(a,b) 
#endif

#ifdef SYSTEM_WIN32
# include <direct.h>
# define MKDIR(a,b) ::mkdir(a) 
#endif


namespace sys
{

/**
 * C++ version of chdir(3).
 */
void
mkdir(const std::string &path, int dir_access)
{
	int ret = MKDIR(path.c_str(),  dir_access);

	if (ret != 0)
		throw error_errno(errno);
}

} /* namespace sys */
