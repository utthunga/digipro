
/*
 * $Id: connect.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

void
connect(int s, const struct sockaddr *name, socklen_t namelen)
{
	int ret;

retry:
	ret = ::connect(s, name, namelen);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	assert(ret == 0);
}

} /* namespace sys */
