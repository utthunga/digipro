
/* 
 * $Id:  $
 *
 * Copyright (C) 2011 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// Windows
#include <windows.h>

// C stdlib
#include <time.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif


namespace sys
{

/**
 * C++ version of gettimeofday.
 */
int
gettimeofday(struct timeval *tp,
	     struct timezone *tzp)
{
	// !!!! Attention !!!!
	// code is copied from http://www.suacommunity.com/dictionary/gettimeofday-entry.php
  	// up to now not tested

	// Define a structure to receive the current Windows filetime
	FILETIME ft;
	int ret = 0;

	// Initialize the present time to 0 and the timezone to UTC
	unsigned __int64 tmpres = 0;
	static int tzflag = 0;

	if (NULL != tp)
	{
		GetSystemTimeAsFileTime(&ft);

		// The GetSystemTimeAsFileTime returns the number of 100 nanosecond 
		// intervals since Jan 1, 1601 in a structure. Copy the high bits to 
		// the 64 bit tmpres, shift it left by 32 then or in the low 32 bits.
		tmpres |= ft.dwHighDateTime;
		tmpres <<= 32;
		tmpres |= ft.dwLowDateTime;

		// Convert to microseconds by dividing by 10
		tmpres /= 10;

		// The Unix epoch starts on Jan 1 1970.  Need to subtract the difference 
		// in seconds from Jan 1 1601.
		tmpres -= DELTA_EPOCH_IN_MICROSECS;

		// Finally change microseconds to seconds and place in the seconds value. 
		// The modulus picks up the microseconds.
		tp->tv_sec = (long)(tmpres / 1000000UL);
		tp->tv_usec = (long)(tmpres % 1000000UL);
	}

	if (NULL != tzp)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}

		// Adjust for the timezone west of Greenwich
		tzp->tz_minuteswest = _timezone / 60;
		tzp->tz_dsttime = _daylight;
	}

	if (ret != 0)
		throw error_errno(errno);

	return ret;
}

} /* namespace sys */
