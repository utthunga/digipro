
/* 
 * $Id: fcntl.cxx,v 1.2 2007/07/05 09:42:37 fna Exp $
 *
 * Copyright (C) 2007 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <fcntl.h>
#include <errno.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

int
fcntl(int fd, int cmd, long arg)
{
	int ret;

retry:
	ret = ::fcntl(fd, cmd, arg);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	return ret;
}

} /* namespace sys */
