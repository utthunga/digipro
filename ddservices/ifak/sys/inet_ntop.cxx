
/*
 * $Id: inet_ntop.cxx,v 1.3 2005/08/31 12:36:32 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <errno.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

void
inet_ntop(int af, const void *src, char *dst, socklen_t size)
{
#ifdef __CYGWIN__
	if (af != AF_INET)
		throw std::runtime_error("inet_ntop not implemented for other "
					 "protocols than AF_INET");

	const char *ret;

	ret = ::inet_ntoa(*((const struct in_addr *) src));
	if (!ret)
		throw error_errno(errno);

	strncpy(dst, ret, size);
	dst[size-1] = '\0';
#else
	const char *ret;

	ret = ::inet_ntop(af, src, dst, size);
	if (!ret)
		throw error_errno(errno);
#endif
}

} /* namespace sys */
