
/*
 * $Id: listen_win32.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

void
listen(int s, int backlog)
{
	int ret;

	ret = ::listen(s, backlog);

	if (ret == -1)
		throw error_win32(WSAGetLastError());

	assert(ret == 0);
}

} /* namespace sys */
