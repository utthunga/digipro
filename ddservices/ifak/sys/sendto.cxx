
/*
 * $Id: sendto.cxx,v 1.2 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

ssize_t
sendto(int s, const void *msg, size_t len, int flags,
       const struct sockaddr *to, socklen_t tolen)
{
	ssize_t ret;

retry:
	ret = ::sendto(s, SOCKET_SEND_BUFPTR(msg), len, flags, to, tolen);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	return ret;
}

} /* namespace sys */
