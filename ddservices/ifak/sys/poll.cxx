
/*
 * $Id: poll.cxx,v 1.2 2005/03/03 23:17:06 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

int
poll(struct pollfd *fds, unsigned int nfds, int timeout)
{
	int ret;

retry:
	ret = ::poll(fds, nfds, timeout);
	if (ret == -1)
	{
		/* retry if interrupted */
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	assert(ret >= 0);
	return ret;
}

} /* namespace sys */
