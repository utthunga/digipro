
/* 
 * $Id:  $
 *
 * Copyright (C) 2011 Matthias Riedl <matthias.riedl@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// Windows
#include <windows.h>

// C stdlib
#include <errno.h>
#include <stdlib.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{
	// !!!! Attention !!!!
	// code is copied from http://stackoverflow.com/questions/5404277/porting-clock-gettime-to-windows
  	// up to now not tested

LARGE_INTEGER
getFILETIMEoffset()
{
	SYSTEMTIME s;
	FILETIME f;
	LARGE_INTEGER t;

	s.wYear = 1970;
	s.wMonth = 1;
	s.wDay = 1;
	s.wHour = 0;
	s.wMinute = 0;
	s.wSecond = 0;
	s.wMilliseconds = 0;
	SystemTimeToFileTime(&s, &f);
	t.QuadPart = f.dwHighDateTime;
	t.QuadPart <<= 32;
	t.QuadPart |= f.dwLowDateTime;
	return (t);
}



/**
 * C++ version of clock_gettime.
 */
int
clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int ret = 0;
	LARGE_INTEGER           t;
	FILETIME            f;
	double                  nanoseconds;
	static LARGE_INTEGER    offset;
	static double           frequencyToNanoseconds;
	static int              initialized = 0;
	static BOOL             usePerformanceCounter = 0;

	if (!initialized) 
	{
		LARGE_INTEGER performanceFrequency;
		initialized = 1;
		usePerformanceCounter = QueryPerformanceFrequency(&performanceFrequency);
		if (usePerformanceCounter) 
		{
			QueryPerformanceCounter(&offset);
			frequencyToNanoseconds = (double)performanceFrequency.QuadPart / 1000000000.;
		} 
		else 
		{
			offset = getFILETIMEoffset();
			frequencyToNanoseconds = 0.010;
		}
	}
	if (usePerformanceCounter) 
		QueryPerformanceCounter(&t);
	else 
	{
		GetSystemTimeAsFileTime(&f);
		t.QuadPart = f.dwHighDateTime;
		t.QuadPart <<= 32;
		t.QuadPart |= f.dwLowDateTime;
	}

	t.QuadPart -= offset.QuadPart;
	nanoseconds = (double)t.QuadPart / frequencyToNanoseconds;
	t.QuadPart = static_cast<LONGLONG>(nanoseconds);
	tp->tv_sec = t.QuadPart / 1000000000;
	tp->tv_nsec = t.QuadPart % 1000000000;


	if (ret != 0)
		throw error_errno(errno);

	return ret;
}

} /* namespace sys */
