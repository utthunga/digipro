
/* 
 * $Id: system.cxx,v 1.2 2008/05/18 12:33:13 fna Exp $
 *
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

// C++ stdlib
#include <sstream>
#include <stdexcept>

// own header
#include "error_errno.h"
#include "sys.h"


namespace sys
{

/**
 * C++ version of system(3).
 */
void
system(const std::string &path)
{
	int ret = ::system(path.c_str());

	if (ret == -1)
		throw error_errno(errno);
	else if (ret != 0)
	{
		std::ostringstream str;
		str << "system() failed: " << WEXITSTATUS(ret);
		throw std::runtime_error(str.str());
	}
}


} /* namespace sys */
