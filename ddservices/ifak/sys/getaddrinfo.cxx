
/*
 * $Id: getaddrinfo.cxx,v 1.2 2005/03/03 23:17:06 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "bsdsocket.h"


namespace sys
{

void
getaddrinfo(const char *nodename, const char *servname,
	    const struct addrinfo *hints, struct addrinfo **res)
{
#ifdef __CYGWIN__
	assert(0);
#else
	int ret;

	ret = ::getaddrinfo(nodename, servname, hints, res);
	if (ret)
		throw std::runtime_error(gai_strerror(ret));
#endif
}

} /* namespace sys */
