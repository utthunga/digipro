
/*
 * $Id: inet_ntop_win32.cxx,v 1.2 2006/06/01 21:44:04 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "lib/port.h"

// own
#include "bsdsocket.h"
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{

void
inet_ntop(int af, const void *src, char *dst, socklen_t size)
{
	if (af != AF_INET)
		throw std::runtime_error("inet_ntop not implemented for other "
					 "protocols than AF_INET");

	const char *ret;

	ret = ::inet_ntoa(*((const struct in_addr *) src));
	if (!ret)
		throw error_win32(WSAGetLastError());

	strncpy_s(dst, size, ret, size);
	dst[size-1] = '\0';
}

} /* namespace sys */
