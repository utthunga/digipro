
/*
 * $Id: getpeername.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

void
getpeername(int s, struct sockaddr *name, socklen_t *namelen)
{
	int ret;

	ret = ::getpeername(s, name, namelen);
	if (ret == -1)
		throw error_errno(errno);

	assert(ret == 0);
}

} /* namespace sys */
