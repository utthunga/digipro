
/* 
 * $Id: dirent_win32.cxx,v 1.2 2006/06/01 21:44:04 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <sys/types.h>

// C++ stdlib
#include <memory>

// own libs
#include "lib/port.h"

// own
#include "dirent.h"

#include <windows.h>


namespace sys
{

struct dir
{
	HANDLE handle;
	std::string cache;
};

static int
myFindNextFile(HANDLE handle, WIN32_FIND_DATA *data)
{
	int res;

	res = FindNextFile(handle, data);
	while (res && (data->dwFileAttributes & FILE_ATTRIBUTE_SYSTEM))
		res = FindNextFile(handle, data);

	return res;
}

DIR *
opendir(const std::string &path)
{
	std::auto_ptr<DIR> dirp(new DIR);
	assert(dirp.get());

	WIN32_FIND_DATA data;
	char buf[_MAX_PATH + 64];
	int failed = 1;

	strcpy_s(buf, sizeof(buf), path.c_str());
	strcat_s(buf, sizeof(buf), "/*");

	dirp->handle = FindFirstFile(buf, &data);
	if (dirp->handle != INVALID_HANDLE_VALUE)
	{
		if (data.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)
		{
			if (myFindNextFile(dirp->handle, &data))
				failed = 0;
		}
		else
			failed = 0;

		if (!failed)
                        dirp->cache = data.cFileName;
	}

	if (failed)
		dirp.reset(NULL);

	return dirp.release();
}

std::string
readdir(DIR *dirp)
{
	std::string ret;

	if (!dirp->cache.empty())
	{
		ret = dirp->cache;
		dirp->cache.clear();
	}
	else
	{
		WIN32_FIND_DATA data;

		if (myFindNextFile(dirp->handle, &data))
			ret = data.cFileName;
	}

	return ret;
}

void
closedir(DIR *dirp)
{
	FindClose(dirp->handle);
	delete dirp;
}

} /* namespace sys */
