
/*
 * $Id: sys_types.h,v 1.3 2009/07/01 12:50:42 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _sys_types_h
#define _sys_types_h

#include "sys_config.h"

// C stdlib
#ifdef SYSTEM_UNIX
# ifndef __STDC_FORMAT_MACROS
# define __STDC_FORMAT_MACROS
# endif
# include <inttypes.h>
# include <unistd.h>
#endif

// C++ stdlib
#include <string>

// own libs

// own


#if defined(SYSTEM_WIN32)
typedef int clockid_t;
struct timespec
{
	time_t tv_sec;
	long tv_nsec;
};

struct timezone
{
	int  tz_minuteswest; /* minutes W of Greenwich */
	int  tz_dsttime;     /* type of dst correction */
};
#endif


namespace sys
{

#if defined(SYSTEM_WIN32)

typedef __int8 int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;

typedef unsigned __int8 uint8;
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;

typedef signed int ssize_t;
typedef __time64_t time_t;
typedef size_t useconds_t;

#ifdef PRId8
# error Config error, PRI?? macros already defined
#endif

#define PRId8    "d"
#define PRId16   "d"
#define PRId32   "d"
#define PRId64   "I64d"

#define PRIu8    "u"
#define PRIu16   "u"
#define PRIu32   "u"
#define PRIu64   "I64u"

#elif defined(SYSTEM_UNIX)

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

using ::ssize_t;
using ::time_t;
using ::useconds_t;

#else
#error unknown target
#endif

typedef float float32;
typedef double float64;

using std::string;

#ifndef PRId8
# error PRI?? macros must be defined!
#endif

} /* namespace sys */

#endif /* _sys_types_h */
