
/*
 * 
 * Copyright (C) 2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "uuid.h"

namespace sys
{

uuid::uuid(void)
{
	memset(&m_uuid, 0, sizeof(UUID));
	generate();
}

void
uuid::generate(void)
{
	UuidCreate(&m_uuid);
	m_data.clear();
}

void
uuid::from_string(const std::string &data)
{
	UuidFromString(const_cast<unsigned char *>((const unsigned char *) data.c_str()), &m_uuid);
	m_data.clear();
}

std::string
uuid::to_string(void) const throw()
{
	if (!m_data.empty())
		return m_data;
	
	char out[37];
	
	memset(out, 0, 37);
	
	sprintf(&out[0], "%02lx%02lx%02lx%02lx-", (m_uuid.Data1 >> 24) & 0xFF, (m_uuid.Data1 >> 16) & 0xFF, (m_uuid.Data1 >> 8) & 0xFF, m_uuid.Data1 & 0xFF);
	sprintf(&out[9], "%02x%02x-", (m_uuid.Data2 >> 8) & 0xFF, m_uuid.Data2 & 0xFF);
	sprintf(&out[14], "%02x%02x-", (m_uuid.Data3 >> 8) & 0xFF, m_uuid.Data3 & 0xFF);
	sprintf(&out[19], "%02x%02x-", (m_uuid.Data4[0] >> 8) & 0xFF, m_uuid.Data4[1] & 0xFF);
	
	for (size_t i = 2; i < 8; ++i)
		sprintf(&out[24+(i-2)*2], "%02x", m_uuid.Data4[i]);
	
	m_data = out;

	return m_data;
}

} /* namespace sys */
