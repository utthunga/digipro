
/*
 * $Id: accept.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <unistd.h>

// C++ stdlib

// own libs

// own
#include "bsdsocket.h"
#include "error_errno.h"


namespace sys
{

int
accept(int s, struct sockaddr *addr, socklen_t *addrlen)
{
	int ret;

retry:
	ret = ::accept(s, addr, addrlen);

	if (ret == -1)
	{
		if (errno == EINTR)
			goto retry;

		throw error_errno(errno);
	}

	assert(ret >= 0);
	return ret;
}

} /* namespace sys */
