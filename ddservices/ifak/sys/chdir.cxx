
/* 
 * $Id: chdir.cxx,v 1.4 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <errno.h>

// C++ stdlib

// own header
#include "error_errno.h"
#include "sys.h"

#ifdef SYSTEM_UNIX
# include <unistd.h>
# define CHDIR ::chdir
#endif

#ifdef SYSTEM_WIN32
# include <direct.h>
# define CHDIR ::_chdir
#endif


namespace sys
{

/**
 * C++ version of chdir(3).
 */
void
chdir(const std::string &path)
{
	int ret = CHDIR(path.c_str());

	if (ret != 0)
		throw error_errno(errno);
}

} /* namespace sys */
