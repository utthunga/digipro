
/*
 * $Id: error_win32.cxx,v 1.1 2005/03/03 14:34:26 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib

// own libs

// own
#include "error_win32.h"

// the all-in-one header
#include <windows.h>


namespace sys
{
 error_win32::error_win32(int nr)
	 : nr_(nr) { }
const char *
error_win32::what(void) const throw()
{
	LPVOID lpMsgBuf;

	if (FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		nr_,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL ))
	{
		msg_ = (LPCTSTR)lpMsgBuf;
		LocalFree(lpMsgBuf);
	}
	else
		msg_ = "Unknown error";

	return msg_.c_str();
}

} /* namespace sys */
