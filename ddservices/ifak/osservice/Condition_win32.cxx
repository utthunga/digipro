
/*
 * $Id: Condition_win32.cxx,v 1.8 2007/06/13 08:15:45 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "sys/error_win32.h"

// own
#include "Condition.h"
#include "os_service_debug.h"


namespace os_service
{

Condition::Condition(void)
{
	handle_[wait_single] = CreateEvent(0, FALSE, FALSE, NULL);
	if (!handle_[wait_single])
		throw std::runtime_error("Condition: can't allocate resource");

	handle_[wait_all] = CreateEvent(0, TRUE, FALSE, NULL);
	if (!handle_[wait_all])
		throw std::runtime_error("Condition: can't allocate resource");
}

Condition::~Condition(void)
{
	CloseHandle(handle_[wait_single]);
	CloseHandle(handle_[wait_all]);
}

void
Condition::lock(void)
{
	mutex_.lock();
}

void
Condition::unlock(void)
{
	mutex_.unlock();
}

bool
Condition::try_lock(void)
{
	return mutex_.try_lock();
}

void
Condition::wait(void)
{
	unlock();

	ResetEvent(handle_[wait_all]);
	int ret = WaitForMultipleObjects(2, handle_, FALSE, INFINITE);

	if (ret == -1)
	{
		lock();
		throw sys::error_win32(WSAGetLastError());
	}


	lock();
}

void
Condition::notify(void)
{
	SetEvent(handle_[wait_single]);
}

void
Condition::notify_all(void)
{
	SetEvent(handle_[wait_all]);
}

} /* namespace os_service */
