
/*
 * $Id: Timeval.h,v 1.12 2007/02/07 09:47:02 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Timeval_h
#define _Timeval_h

#include "os_service_config.h"

// C stdlib
#ifdef SYSTEM_UNIX
# include <sys/time.h>
#endif

// C++ stdlib

// own libs
#include "sys_types.h"

// own


namespace os_service
{

using sys::useconds_t;

/**
 * Time type abstraction.
 */
class WINDLL_OS_SERVICE Timeval
{
private:
	typedef class Timeval Self;

public:
	Timeval(void) throw();

	/* copy/assignment constructor semantic */
	Timeval(const Self &) throw();
	Self& operator=(const Self &) throw();

	/* conversions */
	std::string strftime(const char *fmt) const;
	operator std::string() const { return strftime("%c"); }

	/* binary operations */
	Self operator+ (const Self &) const throw();
	Self operator- (const Self &) const throw();
	bool operator< (const Self &) const throw();
	bool operator> (const Self &) const throw();
	bool operator==(const Self &) const throw();
	bool operator!=(const Self &) const throw();
	bool operator<=(const Self &) const throw();
	bool operator>=(const Self &) const throw();

	/* assignment operations */
	Self& operator+=(const Self &) throw();
	Self& operator-=(const Self &) throw();

	void set(time_t, useconds_t)  throw();
	void set_to_now(void);
	void set_to_seconds(time_t) throw();
	void set_to_useconds(useconds_t) throw();

	time_t to_seconds(void) const throw();
	useconds_t to_useconds(void) const throw();
	useconds_t get_useconds(void) const throw();

	static Self now(void);

private:
	void localtime(struct tm &) const;

#ifdef SYSTEM_UNIX
	Timeval(const struct timeval &) throw();
	struct timeval tv_;
#endif

#ifdef SYSTEM_WIN32
	struct timeval
	{
		__time64_t tv_sec;
		long tv_usec;
	};
	Timeval(const struct timeval &) throw();
	struct timeval tv_;
#endif
};

} /* namespace os_service */

#endif /* _Timeval_h */
