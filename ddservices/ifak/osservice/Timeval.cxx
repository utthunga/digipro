
/*
 * $Id: Timeval.cxx,v 1.13 2009/09/14 11:25:34 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <string.h>
#include <time.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "port.h"

// own
#include "Timeval.h"


#ifdef SYSTEM_WIN32

#undef timerclear
#define timerclear(tvp)	(tvp)->tv_sec = (tvp)->tv_usec = 0

#undef timercmp
#define timercmp(tvp, uvp, cmp)					\
	(((tvp)->tv_sec == (uvp)->tv_sec) ?			\
		((tvp)->tv_usec cmp (uvp)->tv_usec) :		\
		((tvp)->tv_sec cmp (uvp)->tv_sec))

#undef timeradd
#define timeradd(tvp, uvp, vvp)					\
do {								\
	(vvp)->tv_sec = (tvp)->tv_sec + (uvp)->tv_sec;		\
	(vvp)->tv_usec = (tvp)->tv_usec + (uvp)->tv_usec;	\
	if ((vvp)->tv_usec >= 1000000) {			\
		(vvp)->tv_sec++;				\
		(vvp)->tv_usec -= 1000000;			\
	}							\
} while (0)

#undef timersub
#define timersub(tvp, uvp, vvp)					\
do {								\
	(vvp)->tv_sec = (tvp)->tv_sec - (uvp)->tv_sec;		\
	(vvp)->tv_usec = (tvp)->tv_usec - (uvp)->tv_usec;	\
	if ((vvp)->tv_usec < 0) {				\
		(vvp)->tv_sec--;				\
		(vvp)->tv_usec += 1000000;			\
	}							\
} while (0)

#endif /* SYSTEM_WIN32 */

namespace os_service
{

Timeval::Timeval(void) throw()
{
	timerclear(&tv_);
}

Timeval::Timeval(const struct timeval &tv) throw()
: tv_(tv)
{
}

Timeval::Timeval(const Self &t) throw()
: tv_(t.tv_)
{
}

std::string
Timeval::strftime(const char *fmt) const
{
	char buf[64];
	struct tm tm;

	localtime(tm);
	::strftime(buf, sizeof(buf), fmt, &tm);

	return buf;
}

Timeval&
Timeval::operator=(const Self &t) throw()
{
	tv_ = t.tv_;
	return *this;
}

Timeval
Timeval::operator+ (const Self &t) const throw()
{
	struct timeval tv;

	timeradd(&tv_, &t.tv_, &tv);

	return Timeval(tv);
}

Timeval
Timeval::operator- (const Self &t) const throw()
{
	struct timeval tv;

	timersub(&tv_, &t.tv_, &tv);

	return Timeval(tv);
}

Timeval&
Timeval::operator+=(const Self &t) throw()
{
	struct timeval tv;

	timeradd(&tv_, &t.tv_, &tv);
	tv_ = tv;

	return *this;
}

Timeval&
Timeval::operator-=(const Self &t) throw()
{
	struct timeval tv;

	timersub(&tv_, &t.tv_, &tv);
	tv_ = tv;

	return *this;
}

bool Timeval::operator< (const Self &t) const throw() { return timercmp(&tv_, &t.tv_, < ); }
bool Timeval::operator> (const Self &t) const throw() { return timercmp(&tv_, &t.tv_, > ); }
bool Timeval::operator==(const Self &t) const throw() { return timercmp(&tv_, &t.tv_, ==); }
bool Timeval::operator!=(const Self &t) const throw() { return timercmp(&tv_, &t.tv_, !=); }
bool Timeval::operator<=(const Self &t) const throw() { return timercmp(&tv_, &t.tv_, <=); }
bool Timeval::operator>=(const Self &t) const throw() { return timercmp(&tv_, &t.tv_, >=); }

void
Timeval::set(time_t sec, useconds_t us)  throw()
{
	tv_.tv_sec = sec + us / 1000000;
	tv_.tv_usec = us % 1000000;
}

void
Timeval::set_to_seconds(time_t sec) throw()
{
	tv_.tv_sec = sec;
	tv_.tv_usec = 0;
}

void
Timeval::set_to_useconds(useconds_t us) throw()
{
	tv_.tv_sec = us / 1000000;
	tv_.tv_usec = us % 1000000;
}

time_t
Timeval::to_seconds(void) const throw()
{
	return tv_.tv_sec;
}

useconds_t
Timeval::to_useconds(void) const throw()
{
	useconds_t us;

	us = tv_.tv_sec;
	us *= 1000000;
	us += tv_.tv_usec;

	return us;
}

useconds_t
Timeval::get_useconds(void) const throw()
{
	return tv_.tv_usec;
}

} /* namespace os_service */
