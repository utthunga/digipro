
/*
 * $Id: Socket_TCP.cxx,v 1.16 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "port.h"

// own
#include "Socket_TCP.h"

#ifdef SYSTEM_WIN32
# include <winsock2.h>
#else
# include <sys/socket.h>
#endif


namespace os_service
{

TCPSocket::TCPSocket(const std::string &nodename)
{
	class Sockaddr sa;

	fd_ = sa.socket(nodename, SOCK_STREAM);

	try
	{
		sys::connect(fd_, sa.addr(), sa.len());
	}
	catch (const std::exception &)
	{
		sys::close(fd_);
		throw;
	}
}

TCPSocket::TCPSocket(int fd)
: ClientSocket(fd)
{
}

TCPSocket::~TCPSocket(void)
{
	try
	{
		try
		{
			sys::shutdown(fd_, SHUT_RDWR);
		}
		catch (const std::exception &)
		{
			/* ignore */
		}

		sys::close(fd_);
	}
	catch (const std::exception &)
	{
		/* ignore */
	}
}

std::string
TCPSocket::remote_name(void) const
{
	class Sockaddr sa;

	sys::getpeername(fd_, sa.addr(), &sa.len());

	char buf[INET_ADDRSTRLEN];
	sys::inet_ntop(AF_INET, &(sa.addr_in()->sin_addr.s_addr), buf, sizeof(buf));

	return buf;
}

std::string
TCPSocket::remote_address(void) const
{
	class Sockaddr sa;

	sys::getpeername(fd_, sa.addr(), &sa.len());

	return print(sa);
}

std::string
TCPSocket::local_address(void) const
{
	class Sockaddr sa;

	sys::getsockname(fd_, sa.addr(), &sa.len());

	return print(sa);
}

std::string
TCPSocket::print(const class Sockaddr &sa) const
{
	char buf[INET_ADDRSTRLEN];
	sys::inet_ntop(AF_INET, &(sa.addr_in()->sin_addr.s_addr), buf, sizeof(buf));

	std::string ret;

	ret.reserve(strlen(buf) + 3 + 5);
	ret += "//";
	ret += buf;
	ret += ':';

	snprintf(buf, sizeof(buf), "%u", ntohs(sa.addr_in()->sin_port));
	ret += buf;

	return ret;
}


TCPServerSocket::TCPServerSocket(const std::string &nodename)
{
	class Sockaddr sa;

	fd_ = sa.socket(nodename, SOCK_STREAM);

	try
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(yes));

		sys::bind(fd_, sa.addr(), sa.len());
		sys::listen(fd_, SOMAXCONN);
	}
	catch (const std::exception &)
	{
		sys::close(fd_);
		throw;
	}
}

TCPServerSocket::TCPServerSocket(sys::uint16 port)
{
	class Sockaddr sa;
	struct sockaddr_in *sinp = sa.addr_in();

	sa.len() = sizeof(*sinp);

	sinp->sin_family = AF_INET;
	sinp->sin_port = htons(port);
	sinp->sin_addr.s_addr = INADDR_ANY;

	fd_ = sys::socket(AF_INET, SOCK_STREAM, 0);

	try
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(yes));

		sys::bind(fd_, sa.addr(), sa.len());
		sys::listen(fd_, SOMAXCONN);
	}
	catch (const std::exception &)
	{
		sys::close(fd_);
		throw;
	}
}

TCPServerSocket::~TCPServerSocket(void)
{
	try
	{
		try
		{
			sys::shutdown(fd_, SHUT_RDWR);
		}
		catch (const std::exception &)
		{
			/* ignore */
		}

		sys::close(fd_);
	}
	catch (const std::exception &)
	{
		/* ignore */
	}
}

bool
TCPServerSocket::poll(int timeout)
{
	struct pollfd pfd;
	bool result = false;
	int ret;

	pfd.fd = fd_;
	pfd.events = POLLIN;
	pfd.revents = 0;

	ret = sys::poll(&pfd, 1, timeout);
	if (ret > 0)
	{
		assert(ret == 1);

		if (pfd.revents & POLLIN)
			result = true;
	}
	else
		; /* timeout */

	return result;
}

std::auto_ptr<class TCPSocket>
TCPServerSocket::wait_for_client(void)
{
	int client_fd = sys::accept(fd_, NULL, 0);
	std::auto_ptr<class TCPSocket> client;

	try
	{
		client.reset(new class TCPSocket(client_fd));
	}
	catch (const std::exception &)
	{
		sys::close(client_fd);
		throw;
	}

	return client;
}

int
TCPServerSocket::portinfo(void) const
{
	class Sockaddr sa;

	sys::getsockname(fd_, sa.addr(), &sa.len());

	return ntohs(sa.addr_in()->sin_port);
}

TCPServerSocket::operator std::string() const
{
	char buf[300];
	class Sockaddr sa;

	sys::getsockname(fd_, sa.addr(), &sa.len());

	if (sa.addr_in()->sin_addr.s_addr == INADDR_ANY)
	{
		/* don't make much sense to print out INADDR_ANY */

		int r;

		r = gethostname(buf, sizeof(buf));
		if (r == 0)
		{
			struct hostent *h;

			h = gethostbyname(buf);
			if (h) strcpy_s(buf, sizeof(buf), h->h_name);
		}
		else
			strcpy_s(buf, sizeof(buf), "localhost");
	}
	else
		sys::inet_ntop(AF_INET, &(sa.addr_in()->sin_addr.s_addr), buf, sizeof(buf));

	std::string ret("//");
	ret += buf;
	ret += ':';

	snprintf(buf, sizeof(buf), "%u", ntohs(sa.addr_in()->sin_port));
	ret += buf;

	return ret;
}

} /* namespace os_service */
