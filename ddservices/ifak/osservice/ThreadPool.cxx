
/*
 * $Id: ThreadPool.cxx,v 1.3 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2006-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "os_service_debug.h"

// C stdlib
#include <assert.h>

// C++ stdlib
#include <memory>

// own libs

// own
#include "ThreadPool.h"


namespace os_service
{

ThreadPool::PoolThread::PoolThread(Self &i, void (Self::*m)(class PoolThread &))
: thread(i, m, *this, "PoolThread")
{
}

ThreadPool::ThreadPool(size_t suggested)
: low_watermark(2)
, high_watermark(suggested + 5)
, shutdown(false)
{
	/* critical section --threads-- */
	{
		os_service::Guard guard(threads_lock);

		while (suggested--)
		{
			std::auto_ptr<class PoolThread> thread;

			thread.reset(new PoolThread(*this, &Self::pool_main));
			thread->thread.start();

			threads_running.insert(thread.get());
			thread.release();
		}
	}

	while (runnable() < low_watermark)
		Thread::yield();

	DEBUG(("ThreadPool(%lx)::construct: [runnable %lu]\n",
		(unsigned long)this, (unsigned long)runnable()));
}

ThreadPool::~ThreadPool(void)
{
	DEBUG(("ThreadPool(%lx)::destruct: enter [runnable %lu]\n",
		(unsigned long)this, (unsigned long)runnable()));

	shutdown = true;

	/* critical section --threads-- */
	{
		os_service::Guard guard(threads_lock);

		while (!threads_waiting.empty())
		{
			threads_t::iterator iter(threads_waiting.begin());

			threads_running.insert(*iter);
			threads_waiting.erase(iter);
		}
	}

	threads_lock.notify_all();

	for (;;)
	{
		/* critical section --threads-- */
		{
			os_service::Guard guard(threads_lock);

			assert(threads_waiting.empty());

			if (threads_running.empty())
				break;
		}

		Thread::yield();
	}

	reap_threads();

	DEBUG(("ThreadPool(%lx)::destruct: leave\n", (unsigned long)this));
}

void
ThreadPool::new_task(const class PoolThreadFunctor &task)
{
	assert(!shutdown);

	/* critical section --threads-- */
	{
		os_service::Guard guard(threads_lock);

		if (threads_waiting.empty())
		{
			std::auto_ptr<class PoolThread> thread;

			if (threads_dead.empty())
			{
				thread.reset(new PoolThread(*this, &Self::pool_main));

				DEBUG(("ThreadPool(%lx)::new_task: allocated new thread %lx [runnable %lu]\n",
					(unsigned long)this, (unsigned long)thread.get(), (unsigned long)runnable0()));
			}
			else
			{
				thread.reset(*threads_dead.begin());
				threads_dead.erase(threads_dead.begin());

				if (thread->thread.running())
					thread->thread.wait();

				DEBUG(("ThreadPool(%lx)::new_task: reuse dead thread %lx [runnable %lu]\n",
					(unsigned long)this, (unsigned long)thread.get(), (unsigned long)runnable0()));
			}

			assert(thread.get());

			thread->task.reset(task.clone());
			thread->thread.start();

			threads_running.insert(thread.get());
			thread.release();
		}
		else
		{
			threads_t::iterator iter(threads_waiting.begin());

			(*iter)->task.reset(task.clone());

			DEBUG(("ThreadPool(%lx)::new_task: using waiting thread %lx with task %lx [runnable %lu]\n",
				(unsigned long)this, (unsigned long)*iter, (unsigned long)(*iter)->task.get(), (unsigned long)runnable0()));

			threads_running.insert(*iter);
			threads_waiting.erase(iter);

			threads_lock.notify_all();
		}
	}
}

size_t
ThreadPool::runnable(void)
{
	os_service::Guard guard(threads_lock);

	return runnable0();
}

size_t
ThreadPool::runnable0(void)
{
	return threads_waiting.size();
}

void
ThreadPool::reap_threads(void)
{
	/* critical section --threads-- */
	{
		os_service::Guard guard(threads_lock);
		size_t minsize = runnable0();

		if (!shutdown && high_watermark > minsize)
			minsize = high_watermark - minsize;
		else
			minsize = 0;

		while (threads_dead.size() > minsize)
		{
			threads_t::iterator iter(threads_dead.begin());

			DEBUG(("ThreadPool(%lx)::reap_threads: delete thread %lx [runnable %lu]\n",
				(unsigned long)this, (unsigned long)*iter, (unsigned long)runnable0()));

			std::auto_ptr<class PoolThread> thread(*iter);

			threads_dead.erase(iter);

			if (thread->thread.running())
				thread->thread.wait();
		}
	}
}

void
ThreadPool::pool_main(class PoolThread &thread)
{
	for (;;)
	{
		if (shutdown)
			break;

		/* critical section --threads-- */
		if (!thread.task.get())
		{
			os_service::Guard guard(threads_lock);

			if (runnable0() >= high_watermark)
				break;

			threads_waiting.insert(&thread);
			threads_running.erase(&thread);

			DEBUG(("ThreadPool(%lx)::pool_main: waiting for task [runnable %lu]\n",
				(unsigned long)this, (unsigned long)runnable0()));

			while (!shutdown && !thread.task.get())
				threads_lock.wait();

			assert(threads_running.find(&thread) != threads_running.end());
		}

		if (shutdown)
			break;

		assert(thread.task.get());

		DEBUG(("ThreadPool(%lx)::pool_main: handling task %lx [runnable %lu]\n",
			(unsigned long)this, (unsigned long)thread.task.get(), (unsigned long)runnable()));

		try
		{
			(*thread.task)();
		}
		catch (const std::exception &e)
		{
			DEBUG(("ThreadPool(%lx)::pool_main: exception while executing task (%s)\n",
				(unsigned long)this, e.what()));
		}

		thread.task.reset(NULL);
	}

	/* critical section --threads-- */
	{
		os_service::Guard guard(threads_lock);

		threads_running.erase(&thread);
		threads_dead.insert(&thread);
	}
}

} /* namespace os_service */
