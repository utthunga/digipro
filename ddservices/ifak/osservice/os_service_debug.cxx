
/*
 * $Id: os_service_debug.cxx,v 1.1 2005/05/08 09:23:25 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdarg.h>
#include <stdio.h>
#include <string>

// C++ stdlib

// own libs


// own
#include "os_service_debug.h"

#ifdef SYSTEM_WIN32
#include <windows.h>
#endif


namespace os_service
{

#ifdef OS_SERVICE_DEBUG

enum debug_level debug_level = DEBUG_LEVEL;

void
debug_printf(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vprintf(fmt, args);	
	va_end(args);

	fflush(stdout);
}

#endif
} /* namespace os_service */
