
/*
 * $Id: Thread.cxx,v 1.12 2007/06/29 20:27:12 fna Exp $
 * 
 * Copyright (C) 2004-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "os_service_debug.h"

// C stdlib
#include <assert.h>
#include <stdio.h>

// C++ stdlib
#include <stdexcept>
#include <iostream>

// own libs

// own
#include "Thread.h"


namespace os_service
{

Thread::Thread(class ThreadFunctor &e, const char *n)
: execute(e)
, name_(n ? n : "")
, running_(false)
, priority_(Priority_Normal)
{
	TRACE(("%sThread(%p)\n", name_, this));
	std::cout<<"YOGA: calling inside Thread::Thread" <<"LINE_NUM"<<__LINE__<<"FILE:"<<__FILE__<<std::endl;
	std::cout<<"\n";
}

void
Thread::set_priority(enum Thread::Priority priority)
{
	assert(priority >= Priority_Normal && priority <= Priority_Urgent);

	TRACE(("%sThread(%p) - set_priority(%i)\n", name_, this, priority));
	priority_ = priority;

	if (running())
		set_priority();
}

void
Thread::main(void)
{
	TRACE(("%sThread(%p) - enter main\n", name_, this));

	try
	{
		execute();
	}
	catch (const std::exception &e)
	{
		DEBUG(("%sThread::main - exception(%s)\n", name_, e.what()));
	}

	TRACE(("%sThread(%p) - exit\n", name_, this));
}

} /* namespace os_service */
