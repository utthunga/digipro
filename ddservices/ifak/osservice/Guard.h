
/*
 * $Id: Guard.h,v 1.4 2007/06/29 21:20:12 fna Exp $
 * 
 * Copyright (C) 2004-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Guard_h
#define _Guard_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own


namespace os_service
{

/**
 * Base class for Mutex and Condition.
 * Defines the interfaces that both share.
 */
class WINDLL_OS_SERVICE GuardObject
{
public:
	virtual ~GuardObject(void) { }

	virtual void lock(void) = 0;
	virtual void unlock(void) = 0;
	virtual bool try_lock(void) = 0;
};

/**
 * Synchronization helper.
 * This is an small class to easily handle Mutex and Condition
 * objects. It's exception safe, aquire lock on construction
 * and release lock on destruction.
 * 
 * \code
 * os_service::Mutex m_lock;
 * 
 * ...
 * 
 *     // crictical section
 *     {
 *         os_service::Guard guard(m_lock);
 * 
 *         // work on the critical data
 *     }
 * \endcode
 */
class WINDLL_OS_SERVICE Guard
{
private:
	typedef class Guard Self;

	/* no copy/assignment constructor semantic defined */
	Guard(const Self &);
	Self& operator=(const Self &);

public:
	Guard(GuardObject &o) : object_(o) { object_.lock(); }
	~Guard(void) { object_.unlock(); }

private:
	GuardObject &object_;
};

/**
 * Synchronization helper.
 * This is an small class to easily handle Mutex and Condition
 * objects. It's exception safe, try to aquire lock on construction
 * and release lock on destruction (if the lock was aquired).
 * After construction have_look() shall be called to check
 * if the lock was aquired.
 * 
 * \code
 * os_service::Mutex m_lock;
 * 
 * ...
 * 
 *     // crictical section
 *     {
 *         os_service::TryGuard tryguard(m_lock);
 * 
 *         if (tryguard.have_lock())
 *         {
 *             // work on the critical data
 *         }
 *         else
 *         {
 *             // lock already aquired by someone other
 *         }
 *     }
 * \endcode
 */
class WINDLL_OS_SERVICE TryGuard
{
private:
	typedef class TryGuard Self;

	/* no copy/assignment constructor semantic defined */
	TryGuard(const Self &);
	Self& operator=(const Self &);

public:
	TryGuard(GuardObject &o) : object_(o) { locked_ = object_.try_lock(); }
	~TryGuard(void) { if (locked_) object_.unlock(); }

	bool have_lock(void) const throw() { return locked_; }

private:
	GuardObject &object_;
	bool locked_;
};

} /* namespace os_service */

#endif /* _Guard_h */
