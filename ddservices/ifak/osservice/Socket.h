
/*
 * $Id: Socket.h,v 1.14 2005/01/27 14:27:58 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Socket_h
#define _Socket_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own
#include "Socket_TCP.h"
#include "Socket_UDP.h"


namespace os_service
{

} /* namespace os_service */

#endif /* _Socket_h */
