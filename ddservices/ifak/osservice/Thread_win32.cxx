
/*
 * $Id: Thread_win32.cxx,v 1.14 2007/07/02 13:18:15 fna Exp $
 * 
 * Copyright (C) 2004, 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "os_service_debug.h"

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "sys/error_win32.h"

// own
#include "Thread.h"

// windows header
#include <process.h>


namespace os_service
{

Thread::~Thread(void)
{
	assert(!running_);

	TRACE(("~%sThread(%p)\n", name_, this));
}

void
Thread::start(void)
{
	TRACE(("%sThread(%p) - start\n", name_, this));
	assert(!running());

	handle_ = (HANDLE)_beginthreadex(NULL, 0, trampoline, this, 0, &id_);
	if (!handle_)
		throw std::runtime_error("resource limit reached, can't create new thread");

	running_ = true;

	if (priority_ != Priority_Normal)
		set_priority();
}

void
Thread::wait(void)
{
	/* not allowed to wait on myself */
	assert(GetCurrentThreadId() != id_);

	TRACE(("%sThread(%p) - wait\n", name_, this));

	int ret = WaitForSingleObject(handle_, INFINITE);
	
	if (ret == -1)
		throw sys::error_win32(WSAGetLastError());

	CloseHandle(handle_);

	running_ = false;
}

void
Thread::set_priority(void)
{
	static const int conv_tab[] =
	{
		THREAD_PRIORITY_LOWEST,
		THREAD_PRIORITY_NORMAL,
		THREAD_PRIORITY_HIGHEST,
		THREAD_PRIORITY_TIME_CRITICAL
	};

	int ret = SetThreadPriority(handle_, conv_tab[priority_]);

	if (!ret)
		throw sys::error_win32(WSAGetLastError());
}

unsigned int _stdcall
Thread::trampoline(void *arg)
{
	/* unsafe C type interface */
	class Thread *t = static_cast<class Thread *>(arg);

	/* enter C++ */
	t->main();

	return 0;
}

bool
Thread::self(void) const
{
	return (id_ == GetCurrentThreadId());
}

bool
Thread::operator==(const Thread &t) const
{
	return (id_ == t.id_);
}

bool
Thread::operator!=(const Thread &t) const
{
	return (id_ != t.id_);
}

void
Thread::yield(void)
{
	Sleep(0);
}

} /* namespace os_service */
