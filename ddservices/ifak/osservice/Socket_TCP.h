
/*
 * $Id: Socket_TCP.h,v 1.5 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Socket_TCP_h
#define _Socket_TCP_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib
#include <memory>

// own libs
#include "bsdsocket.h"

// own
#include "Sockaddr.h"
#include "SocketBase.h"


namespace os_service
{

class WINDLL_OS_SERVICE TCPSocket : public ClientSocket
{
public:
	TCPSocket(const std::string &);
	TCPSocket(int);
	~TCPSocket(void);

	void shutdown(void) { sys::shutdown(fd_, SHUT_RDWR); }

	std::string remote_name(void) const;
	std::string remote_address(void) const;
	std::string local_address(void) const;

	operator std::string() const { return remote_address(); }

private:
	std::string print(const class Sockaddr &sa) const;
};

class WINDLL_OS_SERVICE TCPServerSocket
{
private:
	typedef class TCPServerSocket Self;

	/* no copy/assignment constructor semantic defined */
	TCPServerSocket(const Self &);
	Self& operator=(const Self &);

public:
	TCPServerSocket(const std::string &);
	TCPServerSocket(sys::uint16 port);
	~TCPServerSocket(void);

	/* poll until a client connect or timeout */
	bool poll(int timeout = -1);

	/* wait for a client connect */
	std::auto_ptr<class TCPSocket> wait_for_client(void);

	int portinfo(void) const;
	operator std::string() const;

private:
	int fd_;
};

} /* namespace os_service */

#endif /* _Socket_TCP_h */
