
/*
 * $Id: Mutex.h,v 1.11 2009/06/29 10:44:09 fna Exp $
 * 
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Mutex_h
#define _Mutex_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own
#include "Guard.h"

#ifdef SYSTEM_PTHREAD
# include <pthread.h>
#endif

#ifdef SYSTEM_WIN32
# include <winsock2.h>
# if defined(min) || defined(max)
#  undef min
#  undef max
# endif
#endif


namespace os_service
{

/**
 * Mutual exlusion synchronization primitive.
 * 
 * A Mutex is also a GuardObject and thus can be used in conjunction with
 * Guard and TryGuard.
 * 
 * The Mutex have 3 main methods:
 * - lock - aquire lock, block if lock is held by another one.
 * - unlock - release lock previously aquired by lock or try_lock.
 * - try_lock - try to aquire the lock, don't block and return true if the
 *              lock is aquired and false if the Mutex is already locked;
 *              if the lock is aquired unlock must be called afterwards (and
 *              only in this case).
 * .
 */
class WINDLL_OS_SERVICE Mutex : public GuardObject
{
private:
	typedef class Mutex Self;

	/* no copy/assignment constructor semantic defined */
	Mutex(const Self &);
	Self& operator=(const Self &);

public:
	Mutex(void);
	virtual ~Mutex(void);

	/* return if Mutex is locked */
	bool locked(void) const throw() { return locked_; }

	/* implement GuardObject */
	virtual void lock(void);
	virtual void unlock(void);
	virtual bool try_lock(void);

private:

#ifdef SYSTEM_PTHREAD
	pthread_mutex_t mutex_;
	bool locked_;
#endif

#ifdef SYSTEM_WIN32
	CRITICAL_SECTION critsec_;
	bool locked_;
#endif
};

} /* namespace os_service */

#endif /* _Mutex_h */
