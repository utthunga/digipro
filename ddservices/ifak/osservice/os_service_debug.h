
/*
 * $Id: os_service_debug.h,v 1.3 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _os_service_debug_h
#define _os_service_debug_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own libs
#include "lib.h"

// own


/**
 * Namespace for the system service mapping library.
 */
namespace os_service
{

#ifndef NDEBUG
#define OS_SERVICE_DEBUG
#endif

/* compile with debug */
#ifdef OS_SERVICE_DEBUG

enum debug_level { FORCE_LEVEL = 0, DEBUG_LEVEL = 1, TRACE_LEVEL = 2 };
extern WINDLL_OS_SERVICE enum debug_level debug_level;

WINDLL_OS_SERVICE void debug_printf(const char *fmt, ...) CHECK_PRINTF(1, 2);

#define DEBUG(x) do { \
	if (debug_level >= DEBUG_LEVEL) { \
		debug_printf("%s, %u: ", __FILE__, __LINE__); \
		debug_printf x; \
	} \
} while (0)

#define TRACE(x) do { \
	if (debug_level >= TRACE_LEVEL) { \
		debug_printf("%s, %u: ", __FILE__, __LINE__); \
		debug_printf x; \
	} \
} while (0)

#define FORCE(x) do { \
	if (debug_level >= FORCE_LEVEL) { \
		debug_printf("%s, %u: ", __FILE__, __LINE__); \
		debug_printf x; \
	} \
} while (0)

#else

#define TRACE(x)
#define DEBUG(x)
#define FORCE(x)

#endif /* OS_SERVICE_DEBUG */

} /* namespace os_service */

#endif /* _os_service_debug_h */
