
/*
 * $Id: Thread_pthread.cxx,v 1.13 2010/03/17 15:29:28 rsc Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "os_service_debug.h"

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "Thread.h"


namespace os_service
{

Thread::~Thread(void)
{
	assert(!running_);

	TRACE(("~%sThread(%p)\n", name_, this));
}

void
Thread::start(void)
{
	TRACE(("%sThread(%p) - start\n", name_, this));
	assert(!running());

	int ret = pthread_create(&id_, NULL, trampoline, this);
	if (ret)
		throw std::runtime_error("resource limit reached, can't create new thread");

	running_ = true;

	if (priority_ != Priority_Normal)
		set_priority();
}

void
Thread::wait(void)
{
	/* not allowed to wait on myself */
	assert(pthread_equal(id_, pthread_self()) == 0);

	TRACE(("%sThread(%p) - wait\n", name_, this));

	int ret = pthread_join(id_, NULL);
	if (ret)
		throw std::runtime_error("failed to wait for thread termination");

	running_ = false;
}

void
Thread::set_priority(void)
{
	struct sched_param sched;
	int policy;

	/* ignore errors */
	if (pthread_getschedparam(id_, &policy, &sched) == 0)
	{
		sched.sched_priority = priority_;
		pthread_setschedparam(id_, policy, &sched);
	}
}

void *
Thread::trampoline(void *arg)
{
	/* unsafe C type interface */
	class Thread *t = static_cast<class Thread *>(arg);

	/* enter C++ */
	t->main();

	return NULL;
}

bool
Thread::self(void) const
{
	return (pthread_equal(id_, pthread_self()) != 0);
}

bool
Thread::operator==(const Thread &t) const
{
	return (pthread_equal(id_, t.id_) != 0);
}

bool
Thread::operator!=(const Thread &t) const
{
	return (pthread_equal(id_, t.id_) == 0);
}

void
Thread::yield(void)
{
	sched_yield();
}

} /* namespace os_service */

