
/*
 * $Id: Condition.h,v 1.10 2009/06/29 10:44:09 fna Exp $
 * 
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Condition_h
#define _Condition_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own
#include "Guard.h"
#include "Mutex.h"

#ifdef SYSTEM_PTHREAD
# include <pthread.h>
#endif

#ifdef SYSTEM_WIN32
# include <winsock2.h>
# if defined(min) || defined(max)
#  undef min
#  undef max
# endif
#endif


namespace os_service
{

/**
 * Event synchronization primitive.
 * Condition is a synchronization primitive for waiting
 * and notifying about an event.
 * The two main features the Condition support is wait
 * and notify. wait block the caller until it's unblocked
 * with notify or notify_all. notify wakeup one waiter,
 * notify_all wakeup all waiters.
 * Before wait can be called by a waiter the waiter must
 * first obtain the lock on the Condition. The result
 * is undefined otherwise.
 * \note wait can only be called if the caller hold the
 * lock on the Condition. wait return with the lock hold by the
 * caller.
 */
class WINDLL_OS_SERVICE Condition : public GuardObject
{
private:
	typedef class Condition Self;

	/* no copy/assignment constructor semantic defined */
	Condition(const Self &);
	Self& operator=(const Self &);

public:
	Condition(void);
	virtual ~Condition(void);

	/* implement GuardObject */
	virtual void lock(void);
	virtual void unlock(void);
	virtual bool try_lock(void);

	void wait(void);
	void notify(void);
	void notify_all(void);

protected:

#ifdef SYSTEM_PTHREAD
	pthread_mutex_t mutex_;
	pthread_cond_t cond_;
	bool locked_;
#endif

#ifdef SYSTEM_WIN32
	Mutex mutex_;
	enum {wait_single, wait_all, num_handles};
	HANDLE handle_[num_handles];
#endif
};

} /* namespace os_service */

#endif /* _Condition_h */
