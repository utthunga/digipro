
/*
 * $Id: ThreadPool.h,v 1.2 2006/01/09 13:58:05 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _ThreadPool_h
#define _ThreadPool_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib
#include <memory>
#include <set>

// own libs

// own
#include "Condition.h"
#include "Mutex.h"
#include "Thread.h"


namespace os_service
{

class PoolThreadFunctor : public ThreadFunctor
{
public:
	virtual class PoolThreadFunctor *clone(void) const = 0;
};

template<typename T>
class MethodPoolThread : public PoolThreadFunctor
{
public:
	MethodPoolThread(T &i, void (T::*m)(void))
	: instance(i), method(m) { }

	virtual void operator() (void) { (instance.*method)(); }
	virtual class PoolThreadFunctor *clone(void) const
	{ return new MethodPoolThread(instance, method); }

private:
	T &instance;
	void (T::*method)(void);
};

class WINDLL_OS_SERVICE ThreadPool
{
private:
	typedef class ThreadPool Self;

	/* no copy/assignment constructor semantic defined */
	ThreadPool(const Self &);
	Self& operator=(const Self &);

public:
	ThreadPool(size_t suggested = 10);
	~ThreadPool(void);

	void new_task(const class PoolThreadFunctor &);

private:
	class PoolThread
	{
	public:
		PoolThread(Self &, void (Self::*)(class PoolThread &));

		class MethodThread1<Self, class PoolThread &> thread;
		std::auto_ptr<class PoolThreadFunctor> task;
	};
	typedef std::set<class PoolThread *> threads_t;
	threads_t threads_running, threads_waiting, threads_dead;
	os_service::Condition threads_lock;

	size_t runnable(void);
	size_t runnable0(void);

	void reap_threads(void);
	void pool_main(class PoolThread &);

	size_t low_watermark, high_watermark;
	bool shutdown;
};

} /* namespace os_service */

#endif /* _ThreadPool_h */
