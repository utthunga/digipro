
/*
 * $Id: Timed_Condition_win32.cxx,v 1.6 2010/03/25 12:50:49 mmeier Exp $
 *
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 *               2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 *
 */

// C stdlib
#include <assert.h>

// c++ stdlib

// own libs

// own
#include "Timed_Condition.h"


namespace os_service
{

int
Timed_Condition::timedwait(Timeval tv)
{
	long useconds;
	Timeval tv_;

	tv_.set_to_now();

	useconds = (tv.to_seconds() - tv_.to_seconds()) * 1000000 + (tv.get_useconds() - tv_.get_useconds());

	return timedwait(useconds);
}

int
Timed_Condition::timedwait(unsigned int wait_time)
{
	unlock();

	int ret = WaitForMultipleObjects(2, handle_, FALSE, (DWORD) (wait_time / 1000));
	assert(ret == WAIT_OBJECT_0 || ret == WAIT_TIMEOUT);

	lock();

	switch(ret)
	{
		case WAIT_OBJECT_0:	return NOTIFIED;
		case WAIT_TIMEOUT:	return TIMEDOUT;
		default:		return FAILED;
	}
}

} /* namespace os_service */
