
/*
 * $Id: Timed_Condition.h,v 1.5 2010/03/25 12:50:49 mmeier Exp $
 * 
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 *               2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Timed_Condition_h
#define _Timed_Condition_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own
#include "Condition.h"
#include "Timeval.h"


namespace os_service
{

/**
 * Event synchronization primitive.
 * Timed_Condition is a synchronization primitive for
 * waiting and notifying about an event.
 * The main feature the Timed_Condition supports is
 * timedwait, as well as the methods inherited from
 * the Condition class.
 * timedwait blocks the caller until it's unblocked with notify
 * or notify_all, or the specified timeout (in useconds)
 * is reached.
 * Before timedwait can be called by a waiter the waiter must
 * first obtain the lock on the Timed_Condition. The result
 * is undefined otherwise.
 * \note timedwait can only be called if the caller holds the
 * lock on the Condition.
 * \return One of the three constants NOTIFIED, TIMEDOUT or FAILED
 */
class WINDLL_OS_SERVICE Timed_Condition : public Condition
{
public:
	Timed_Condition(void) { }

	int timedwait(Timeval);
	int timedwait(unsigned int);

	enum
	{
		NOTIFIED = 0,
		TIMEDOUT = 1,
		FAILED = -1
	};
};

} /* namespace os_service */

#endif /* _Timed_Condition_h */
