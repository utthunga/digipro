
/*
 * $Id: Timeval_win32.cxx,v 1.7 2009/08/03 10:17:58 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <time.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "sys/error_win32.h"

// own
#include "Timeval.h"
#include "os_service_debug.h"

// the ugly windows header
#include <winsock2.h>


namespace os_service
{

const __time64_t SECS_BETWEEN_EPOCHS = 11644473600;
const __time64_t SECS_TO_100NS = 10000000;

static void
unix2win(__time64_t tv_sec, long tv_usec, FILETIME &ft) throw()
{
	__time64_t t;

	t = (tv_sec + SECS_BETWEEN_EPOCHS) * SECS_TO_100NS;
	t += ((tv_usec * 1000) / 100);

	ft.dwLowDateTime = (DWORD)t;
	ft.dwHighDateTime = (DWORD)(t >> 32);
}

static void
win2unix(const FILETIME &ft, __time64_t &tv_sec, long &tv_usec) throw()
{
	/* get the full win32 value, in 100ns */
	tv_sec = ((__time64_t)ft.dwHighDateTime << 32) + ft.dwLowDateTime;

	/* convert to the Unix epoch */
	tv_sec -= (SECS_BETWEEN_EPOCHS * SECS_TO_100NS);

	/* get the number of 100ns, convert to us */
	tv_usec = ((tv_sec % SECS_TO_100NS) * 100) / 1000;

	/* convert to seconds */
	tv_sec /= SECS_TO_100NS;
}

void
Timeval::localtime(struct tm &tm) const
{
#if _MSC_VER >= 1400
	_localtime64_s(&tm, &tv_.tv_sec);
#else
	struct tm *tm0 = _localtime64(&tv_.tv_sec);
	assert(tm0);
	tm = *tm0;
#endif
}

void
Timeval::set_to_now(void)
{
	SYSTEMTIME st;
	GetSystemTime(&st);

	FILETIME ft;
	int ret = SystemTimeToFileTime(&st, &ft);

	if (!ret)
		throw sys::error_win32(WSAGetLastError());

	win2unix(ft, tv_.tv_sec, tv_.tv_usec);
}

Timeval
Timeval::now(void)
{
	SYSTEMTIME st;
	GetSystemTime(&st);

	FILETIME ft;
	int ret = SystemTimeToFileTime(&st, &ft);

	if (!ret)
		throw sys::error_win32(WSAGetLastError());

	struct timeval tv;
	win2unix(ft, tv.tv_sec, tv.tv_usec);

	return Timeval(tv);
}

} /* namespace os_service */
