
/*
 * $Id: Timed_Condition_pthread.cxx,v 1.5 2010/03/25 12:50:49 mmeier Exp $
 *
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 *               2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 *
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <sys/time.h>

// c++ stdlib

// own libs

// own
#include "Timed_Condition.h"


namespace os_service
{

int
Timed_Condition::timedwait(Timeval tv)
{
	assert(locked_);
	locked_ = false;

	struct timespec ts;

	ts.tv_sec = tv.to_seconds();
	ts.tv_nsec = tv.get_useconds() * 1000;

	int ret = pthread_cond_timedwait(&cond_, &mutex_, &ts);

	assert(ret == 0 || ret == ETIMEDOUT);

	assert(!locked_);
	locked_ = true;

	switch(ret)
	{
		case 0:		return NOTIFIED;
		case ETIMEDOUT:	return TIMEDOUT;
		default:	return FAILED;
	}

	return ret;
}

int
Timed_Condition::timedwait(unsigned int wait_time)
{
	Timeval tv, tv_;

	tv.set_to_now();

	tv_.set_to_useconds(wait_time);

	tv += tv_;

	return timedwait(tv);
}

} /* namespace os_service */
