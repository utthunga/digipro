
/*
 * $Id: Timeval_unix.cxx,v 1.2 2005/03/12 20:38:28 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <sys/time.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "Timeval.h"


namespace os_service
{

void
Timeval::localtime(struct tm &tm) const
{
	localtime_r(&tv_.tv_sec, &tm);
}

void
Timeval::set_to_now(void)
{
	::gettimeofday(&tv_, NULL);
}

Timeval
Timeval::now(void)
{
	struct timeval tv;
	::gettimeofday(&tv, NULL);
	return Timeval(tv);
}

} /* namespace os_service */
