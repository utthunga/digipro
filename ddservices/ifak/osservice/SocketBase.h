
/*
 * $Id: SocketBase.h,v 1.4 2009/07/21 19:16:13 mmeier Exp $
 * 
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _SocketBase_h
#define _SocketBase_h

#include "os_service_config.h"

// C stdlib
#include <signal.h>

// C++ stdlib

// own libs
#include "bsdsocket.h"

// own


namespace os_service
{

class WINDLL_OS_SERVICE ClientSocket
{
private:
	typedef class ClientSocket Self;

	/* no copy/assignment constructor semantic defined */
	ClientSocket(const Self &);
	Self& operator=(const Self &);

public:
	sys::ssize_t send(const void *msg, size_t len) { return sys::send(fd_, msg, len, 0); }
	sys::ssize_t recv(void *buf, size_t len) { return sys::recv(fd_, buf, len, 0); }

	/* send/recv exactly len, may block */
	void send_all(const void *msg, size_t len) { sys::send_all(fd_, msg, len, 0); }
	bool recv_all(void *buf, size_t len) { return sys::recv_all(fd_, buf, len, 0); }

	/* poll modes */
	static const int poll_read = 0x1;
	static const int poll_write = 0x2;
	static const int poll_timeout = 0x4;

	/* wait for I/O */
	int poll(int mask, int timeout = -1);

protected:
	ClientSocket(int fd = -1)
	{
#ifndef SYSTEM_WIN32
		struct sigaction act;
		act.sa_handler = SIG_IGN;
		sigemptyset(&act.sa_mask);
		act.sa_flags = 0;
		sigaction(SIGPIPE, &act, NULL);
#endif

		if (fd > -1)
			fd_ = fd;
	}

	int fd_;
};

} /* namespace os_service */

#endif /* _SocketBase_h */
