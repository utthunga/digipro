
/*
 * $Id: Sockaddr.h,v 1.4 2005/05/08 09:23:25 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Sockaddr_h
#define _Sockaddr_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs
#include "bsdsocket.h"

// own

#ifdef SYSTEM_UNIX
# include <netinet/in.h>
# include <sys/un.h>
#endif


namespace os_service
{

class WINDLL_OS_SERVICE Sockaddr
{
public:
	Sockaddr(void) : sa_len(sizeof(sa_buf)) { }

	/* generic */
	void *addr_void(void) throw()
	{ return static_cast<void *>(&sa_buf); }
	const void *addr_void(void) const throw()
	{ return static_cast<const void *>(&sa_buf); }

	/* sockaddr */
	struct sockaddr *addr(void) throw()
	{ return static_cast<struct sockaddr *>(static_cast<void *>(&sa_buf)); }
	const struct sockaddr *addr(void) const throw()
	{ return static_cast<const struct sockaddr *>(static_cast<const void *>(&sa_buf)); }

	/* AF_UNIX */
	struct sockaddr_un *addr_un(void) throw()
	{ return static_cast<struct sockaddr_un *>(static_cast<void *>(&sa_buf)); }
	const struct sockaddr_un *addr_un(void) const throw()
	{ return static_cast<const struct sockaddr_un *>(static_cast<const void *>(&sa_buf)); }

	/* AF_INET */
	struct sockaddr_in *addr_in(void) throw()
	{ return static_cast<struct sockaddr_in *>(static_cast<void *>(&sa_buf)); }
	const struct sockaddr_in *addr_in(void) const throw()
	{ return static_cast<const struct sockaddr_in *>(static_cast<const void *>(&sa_buf)); }

	/* AF_INET6 */
	struct sockaddr_in6 *addr_in6(void) throw()
	{ return static_cast<struct sockaddr_in6 *>(static_cast<void *>(&sa_buf)); }
	const struct sockaddr_in6 *addr_in6(void) const throw()
	{ return static_cast<const struct sockaddr_in6 *>(static_cast<const void *>(&sa_buf)); }

	socklen_t& len(void) throw() { return sa_len; }
	socklen_t len(void) const throw() { return sa_len; }

	/* fill in Sockaddr and create sockect */
	int socket(const std::string &address, int type);

private:
	struct sockaddr_storage sa_buf;
	socklen_t sa_len;
};

} /* namespace os_service */

#endif /* _Sockaddr_h */
