
/*
 * $Id: Mutex_win32.cxx,v 1.6 2007/06/13 15:02:35 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "Mutex.h"


namespace os_service
{

Mutex::Mutex(void)
: locked_(false)
{
	InitializeCriticalSection(&critsec_);
}

Mutex::~Mutex(void)
{
	assert(!locked_);

	DeleteCriticalSection(&critsec_);
}

void
Mutex::lock(void)
{
	EnterCriticalSection(&critsec_);

	assert(!locked_);
	locked_ = true;
}

void
Mutex::unlock(void)
{
	assert(locked_);
	locked_ = false;

	LeaveCriticalSection(&critsec_);
}

bool
Mutex::try_lock(void)
{
	if (TryEnterCriticalSection(&critsec_))
	{
		assert(!locked_);
		locked_ = true;

		return true;
	}

	return false;
}

} /* namespace os_service */
