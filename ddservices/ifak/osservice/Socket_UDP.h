
/*
 * $Id: Socket_UDP.h,v 1.10 2009/11/11 15:50:23 mmeier Exp $
 * 
 * Copyright (C) 2004-2007 Frank Naumann <frank.naumann@ifak-md.de>
 *               2006 Thomas Thuem
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Socket_UDP_h
#define _Socket_UDP_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib
#include <memory>

// own libs
#include "bsdsocket.h"

// own
#include "Sockaddr.h"
#include "SocketBase.h"


namespace os_service
{

class WINDLL_OS_SERVICE UDPSocket : public ClientSocket
{
public:
	UDPSocket(const std::string &, bool allow_broadcast = false, bool reuse = false);
	~UDPSocket(void);

};

class WINDLL_OS_SERVICE UDPServerSocket
{
private:
	typedef class UDPServerSocket Self;

	/* no copy/assignment constructor semantic defined */
	UDPServerSocket(const Self &);
	Self& operator=(const Self &);

public:
	UDPServerSocket(const std::string &, bool allow_broadcast = false, bool reuse = false);
	~UDPServerSocket(void);

	int portinfo(void) const;

	sys::ssize_t sendto(const void *msg, size_t len, const class Sockaddr &sa)
	{ return sys::sendto(fd_, msg, len, 0, sa.addr(), sa.len()); }

	sys::ssize_t recvfrom(void *buf, size_t len, class Sockaddr &sa)
	{ return sys::recvfrom(fd_, buf, len, 0, sa.addr(), &sa.len()); }

	int poll(int mask, int timeout);

private:
	int fd_;
};

} /* namespace os_service */

#endif /* _Socket_UDP_h */
