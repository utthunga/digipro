
/*
 * $Id: os_service_config.h,v 1.5 2005/11/20 14:02:16 fna Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _os_service_config_h
#define _os_service_config_h

#ifdef _MSC_VER

/* export os_service lib as DLL */
# ifdef OS_SERVICE_AS_DLL
/* import system mapping library as DLL */
#  ifndef SYS_AS_DLL
#   define SYS_AS_DLL
#  endif
/* import support lib as DLL */
#  ifndef LIB_AS_DLL
#   define LIB_AS_DLL
#  endif
/* DLL import/export defines */
#  ifdef OS_SERVICE_DLL
#   define WINDLL_OS_SERVICE __declspec(dllexport)
#  else
#   define WINDLL_OS_SERVICE __declspec(dllimport)
#  endif
/* ignore stupid non-dll interface warnings in use of stdc++ */
#  pragma warning (disable: 4251)
# endif

#endif /* _MSC_VER */

#ifndef WINDLL_OS_SERVICE
# define WINDLL_OS_SERVICE
#endif

#include "sys_config.h"
#include "lib_config.h"

#endif /* _os_service_config_h */
