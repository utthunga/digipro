
/*
 * $Id: SocketBase.cxx,v 1.1 2007/07/04 07:38:20 fna Exp $
 * 
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own
#include "SocketBase.h"


namespace os_service
{

int
ClientSocket::poll(int mask, int timeout)
{
	struct pollfd fds = { fd_, 0, 0 };
	int ret;

	if (mask & poll_read)
		fds.events |= POLLIN;

	if (mask & poll_write)
		fds.events |= POLLOUT;

	ret = sys::poll(&fds, 1, timeout);
	if (ret > 0)
	{
		assert(ret == 1);
		ret = 0;

		if (fds.revents & POLLIN)
			ret |= poll_read;

		if (fds.revents & POLLOUT)
			ret |= poll_write;
	}
	else
		ret = poll_timeout;

	return ret;
}

} /* namespace os_service */
