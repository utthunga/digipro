
/*
 * $Id: Thread.h,v 1.18 2009/06/29 10:44:09 fna Exp $
 * 
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _Thread_h
#define _Thread_h

#include "os_service_config.h"

// C stdlib

// C++ stdlib

// own libs

// own

#ifdef SYSTEM_PTHREAD
# include <pthread.h>
#endif

#ifdef SYSTEM_WIN32
# include <winsock2.h>
# if defined(min) || defined(max)
#  undef min
#  undef max
# endif
#endif


namespace os_service
{

/**
 * Abstract execution unit for a Thread.
 */
class ThreadFunctor
{
public:
	virtual ~ThreadFunctor(void) { }

	virtual void operator() (void) = 0;
};

/**
 * Thread abstraction.
 * 
 * For easier usage the MethodThread, MethodThread1, MethodThread2 and
 * MethodThread3 template classes are designed. See at the documentation
 * of this classes for usage examples.
 */
class WINDLL_OS_SERVICE Thread
{
private:
	typedef class Thread Self;

	/* no copy/assignment constructor semantic defined */
	Thread(const Self &);
	Self& operator=(const Self &);

public:
	Thread(class ThreadFunctor &, const char *name = NULL);
	virtual ~Thread(void);

	const char *name(void) const throw() { return name_; }
	bool running(void) const throw() { return running_; }

	void start(void);
	void wait(void);

	/**
	 * Thread priorities.
	 */
	enum Priority
	{
		Priority_Low    = 0,	/**< A priority lower than normal. */
		Priority_Normal = 1, 	/**< The default priority. */
		Priority_High   = 2,	/**< A priority higher than normal. */
		Priority_Urgent = 3	/**< The highest priority. */
	};

	void set_priority(enum Priority priority);
	enum Priority get_priority(void) const throw() { return priority_; }

	bool self(void) const;
	bool operator==(const Thread &) const;
	bool operator!=(const Thread &) const;

	static void yield(void);

private:
	/* execution unit */
	class ThreadFunctor &execute;

	/* thread name */
	const char *name_;
	/* thread status */
	bool running_;

	/* thread priority */
	enum Priority priority_;
	void set_priority(void);

	/* thread main */
	void main(void);

#ifdef SYSTEM_PTHREAD
	pthread_t id_;
	static void *trampoline(void *);
#endif

#ifdef SYSTEM_WIN32
	HANDLE handle_;
	unsigned int id_;
	static unsigned int _stdcall trampoline(void *);
#endif
};

/**
 * Bound class method as thread execution unit.
 * This bounds a method of class T of type void (T::*)(void)
 * as execution unit for a Thread.
 * 
 * \code
 * #include "os_service/Thread.h"
 * 
 * 
 * class MyClass
 * {
 * public:
 *     typedef class MyClass Self;
 * 
 *     MyClass(void);
 *     ~MyClass(void);
 * 
 * private:
 *     os_service::MethodThread<Self> m_thread;
 *     void thread(void);
 * 
 *     bool running;
 * };
 * 
 * 
 * MyClass::MyClass(void)
 * : m_thread(*this, &Self::thread, "WorkerThread")
 * , running(true)
 * {
 *     // start thread
 *     m_thread.start();
 * }
 * 
 * MyClass::~MyClass(void)
 * {
 *     running = false;
 * 
 *     // wakeup thread if neccessary
 * 
 *     // wait for thread completition
 *     if (m_thread.running())
 *         m_thread.wait();
 * }
 * 
 * void
 * MyClass::thread(void)
 * {
 *     while (running)
 *     {
 *         // work
 *     }
 * }
 * 
 * \endcode
 */
template<typename T>
class MethodThread : public Thread, public ThreadFunctor
{
public:
	MethodThread(T &i, void (T::*m)(void), const char *name = NULL)
	: Thread(*down(this), name)
	, instance(i), method(m) { }

	virtual void operator() (void) { (instance.*method)(); }

private:
	T &instance;
	void (T::*method)(void);

	static inline class ThreadFunctor *down(class MethodThread<T> *ptr) throw() { return ptr; }
};

/**
 * Bound class method as thread execution unit.
 * This bounds a method of class T of type void (T::*)(T1)
 * as execution unit for a Thread.
 * 
 * \code
 * #include "os_service/Thread.h"
 * 
 * 
 * class MyClass
 * {
 * public:
 *     typedef class MyClass Self;
 * 
 *     MyClass(void);
 *     ~MyClass(void);
 * 
 * private:
 *     os_service::MethodThread1<Self, int> m_thread;
 *     void thread(int arg1);
 * 
 *     bool running;
 * };
 * 
 * 
 * MyClass::MyClass(void)
 * : m_thread(*this, &Self::thread, 100, "WorkerThread")
 * , running(true)
 * {
 *     // start thread with arg
 *     m_thread.start(10);
 * 
 *     // or start thread without arg (use default arg from cosntructor)
 *     // m_thread.start();
 * }
 * 
 * MyClass::~MyClass(void)
 * {
 *     running = false;
 * 
 *     // wakeup thread if neccessary
 * 
 *     // wait for thread completition
 *     if (m_thread.running())
 *         m_thread.wait();
 * }
 * 
 * void
 * MyClass::thread(int arg1)
 * {
 *     // arg1 = 10
 * 
 *     while (running)
 *     {
 *         // work
 *     }
 * }
 * 
 * \endcode
 */
template<typename T, typename A1>
class MethodThread1 : public Thread, public ThreadFunctor
{
public:
	MethodThread1(T &i, void (T::*m)(A1), A1 _a1, const char *name = NULL)
	: Thread(*down(this), name)
	, instance(i), method(m), a1(_a1) { }

	virtual void operator() (void) { (instance.*method)(a1); }

	void start(A1 _a1) { a1 = _a1; start(); }
	using Thread::start;

private:
	T &instance;
	void (T::*method)(A1);
	A1 a1;

	static inline class ThreadFunctor *down(class MethodThread1<T, A1> *ptr) throw() { return ptr; }
};

/**
 * Bound class method as thread execution unit.
 * This bounds a method of class T of type void (T::*)(T1, T2)
 * as execution unit for a Thread.
 * 
 * \code
 * #include "os_service/Thread.h"
 * 
 * 
 * class MyClass
 * {
 * public:
 *     typedef class MyClass Self;
 * 
 *     MyClass(void);
 *     ~MyClass(void);
 * 
 * private:
 *     os_service::MethodThread3<Self, int, float> m_thread;
 *     void thread(int arg1, float arg2);
 * 
 *     bool running;
 * };
 * 
 * 
 * MyClass::MyClass(void)
 * : m_thread(*this, &Self::thread, 100, 233.3, "WorkerThread")
 * , running(true)
 * {
 *     // start thread with arg
 *     m_thread.start(10, 100.0);
 * 
 *     // or start thread without arg (use default args from cosntructor)
 *     // m_thread.start();
 * }
 * 
 * MyClass::~MyClass(void)
 * {
 *     running = false;
 * 
 *     // wakeup thread if neccessary
 * 
 *     // wait for thread completition
 *     if (m_thread.running())
 *         m_thread.wait();
 * }
 * 
 * void
 * MyClass::thread(int arg1, float arg2)
 * {
 *     // arg1 = 10
 *     // arg2 = 100.0
 * 
 *     while (running)
 *     {
 *         // work
 *     }
 * }
 * 
 * \endcode
 */
template<typename T, typename A1, typename A2>
class MethodThread2 : public Thread, public ThreadFunctor
{
public:
	MethodThread2(T &i, void (T::*m)(A1, A2), A1 _a1, A2 _a2, const char *name = NULL)
	: Thread(*down(this), name)
	, instance(i), method(m), a1(_a1), a2(_a2) { }

	virtual void operator() (void) { (instance.*method)(a1, a2); }

	void start(A1 _a1, A2 _a2) { a1 = _a1; a2 = _a2; start(); }
	using Thread::start;

private:
	T &instance;
	void (T::*method)(A1, A2);
	A1 a1;
	A2 a2;

	static inline class ThreadFunctor *down(class MethodThread2<T, A1, A2> *ptr) throw() { return ptr; }
};

/**
 * Bound class method as thread execution unit.
 * This bounds a method of class T of type void (T::*)(T1, T2, T3)
 * as execution unit for a Thread.
 * 
 * \code
 * #include <string>
 * #include "os_service/Thread.h"
 * 
 * 
 * class MyClass
 * {
 * public:
 *     typedef class MyClass Self;
 * 
 *     MyClass(void);
 *     ~MyClass(void);
 * 
 * private:
 *     os_service::MethodThread3<Self, int, float, std::string> m_thread;
 *     void thread(int arg1, float arg2, std::string arg3);
 * 
 *     bool running;
 * };
 * 
 * 
 * MyClass::MyClass(void)
 * : m_thread(*this, &Self::thread, 100, 233.3, "default", "WorkerThread")
 * , running(true)
 * {
 *     // start thread with arg
 *     m_thread.start(10, 100.0, "Hello Thread!");
 * 
 *     // or start thread without arg (use default args from cosntructor)
 *     // m_thread.start();
 * }
 * 
 * MyClass::~MyClass(void)
 * {
 *     running = false;
 * 
 *     // wakeup thread if neccessary
 * 
 *     // wait for thread completition
 *     if (m_thread.running())
 *         m_thread.wait();
 * }
 * 
 * void
 * MyClass::thread(int arg1, float arg2, std::string arg3)
 * {
 *     // arg1 = 10
 *     // arg2 = 100.0
 *     // arg3 = "Hello Thread!"
 * 
 *     while (running)
 *     {
 *         // work
 *     }
 * }
 * 
 * \endcode
 */
template<typename T, typename A1, typename A2, typename A3>
class MethodThread3 : public Thread, public ThreadFunctor
{
public:
	MethodThread3(T &i, void (T::*m)(A1, A2, A3), A1 _a1, A2 _a2, A3 _a3, const char *name = NULL)
	: Thread(*down(this), name)
	, instance(i), method(m), a1(_a1), a2(_a2), a3(_a3) { }

	virtual void operator() (void) { (instance.*method)(a1, a2, a3); }

	void start(A1 _a1, A2 _a2, A3 _a3) { a1 = _a1; a2 = _a2; a3 = _a3; start(); }
	using Thread::start;

private:
	T &instance;
	void (T::*method)(A1, A2, A3);
	A1 a1;
	A2 a2;
	A3 a3;

	static inline class ThreadFunctor *down(class MethodThread3<T, A1, A2, A3> *ptr) throw() { return ptr; }
};

} /* namespace os_service */

#endif /* _Thread_h */
