
/*
 * $Id: Condition_pthread.cxx,v 1.6 2007/06/13 15:02:35 fna Exp $
 * 
 * Copyright (C) 2004-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "Condition.h"


namespace os_service
{

Condition::Condition(void)
: locked_(false)
{
	int ret;

	if (pthread_mutex_init(&mutex_, NULL) != 0)
		goto exception;

	if (pthread_cond_init(&cond_, NULL) != 0)
	{
		ret = pthread_mutex_destroy(&mutex_);
		assert(ret == 0);

exception:
		throw std::runtime_error("Condition: can't allocate resource");
	}
}

Condition::~Condition(void)
{
	int ret;

	assert(!locked_);

	ret = pthread_cond_destroy(&cond_);
	assert(ret == 0);

	ret = pthread_mutex_destroy(&mutex_);
	assert(ret == 0);
}

void
Condition::lock(void)
{
	int ret = pthread_mutex_lock(&mutex_);
	assert(ret == 0);

	assert(!locked_);
	locked_ = true;
}

void
Condition::unlock(void)
{
	assert(locked_);
	locked_ = false;

	int ret = pthread_mutex_unlock(&mutex_);
	assert(ret == 0);
}

bool
Condition::try_lock(void)
{
	int ret = pthread_mutex_trylock(&mutex_);
	assert(ret == 0 || ret == EBUSY);

	if (ret == 0)
	{
		assert(!locked_);
		locked_ = true;

		return true;
	}

	return false;
}

void
Condition::wait(void)
{
	assert(locked_);
	locked_ = false;

	int ret = pthread_cond_wait(&cond_, &mutex_);
	assert(ret == 0);

	assert(!locked_);
	locked_ = true;
}

void
Condition::notify(void)
{
	int ret = pthread_cond_signal(&cond_);
	assert(ret == 0);
}

void
Condition::notify_all(void)
{
	int ret = pthread_cond_broadcast(&cond_);
	assert(ret == 0);
}

} /* namespace os_service */
