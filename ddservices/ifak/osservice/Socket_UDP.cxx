
/*
 * $Id: Socket_UDP.cxx,v 1.10 2009/11/11 15:50:23 mmeier Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 *               2006 Thomas Thuem
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <exception>

// own libs
#include "error_errno.h"

// own
#include "Socket_UDP.h"


namespace os_service
{

UDPSocket::UDPSocket(const std::string &nodename, bool allow_broadcast, bool reuse)
{
	class Sockaddr sa;

	fd_ = sa.socket(nodename, SOCK_DGRAM);

	if (allow_broadcast)
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_BROADCAST, (const char*)&yes, sizeof(yes));
	}

	if (reuse)
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes));
	}

	try
	{
		sys::connect(fd_, sa.addr(), sa.len());
	}
	catch (const std::exception &)
	{
		sys::close(fd_);
		throw;
	}
}

UDPSocket::~UDPSocket(void)
{
	try
	{
		sys::close(fd_);
	}
	catch (const std::exception &)
	{
		/* ignore */
	}
}

UDPServerSocket::UDPServerSocket(const std::string &nodename, bool allow_broadcast, bool reuse)
{
	class Sockaddr sa;

	fd_ = sa.socket(nodename, SOCK_DGRAM);
	
	if (allow_broadcast)
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_BROADCAST, (const char*)&yes, sizeof(yes));
	}

	if (reuse)
	{
		int yes = 1;
		setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes));
	}

	try
	{
		sys::bind(fd_, sa.addr(), sa.len());
	}
	catch (const std::exception &)
	{
		sys::close(fd_);
		throw;
	}
}

UDPServerSocket::~UDPServerSocket(void)
{
	try
	{
		sys::close(fd_);
	}
	catch (const std::exception &)
	{
		/* ignore */
	}
}

int
UDPServerSocket::portinfo(void) const
{
	class Sockaddr sa;

	sys::getsockname(fd_, sa.addr(), &sa.len());

	return ntohs(sa.addr_in()->sin_port);
}

int 
UDPServerSocket::poll(int mask, int timeout)
{
	struct pollfd fds = { fd_, 0, 0 };
	int ret;

	if (mask & ClientSocket::poll_read)
		fds.events |= POLLIN;

	if (mask & ClientSocket::poll_write)
		fds.events |= POLLOUT;

	ret = sys::poll(&fds, 1, timeout);
	if (ret > 0)
	{
		assert(ret == 1);
		ret = 0;

		if (fds.revents & POLLIN)
			ret |= ClientSocket::poll_read;

		if (fds.revents & POLLOUT)
			ret |= ClientSocket::poll_write;
	}
	else
		ret = ClientSocket::poll_timeout;

	return ret;
} 

} /* namespace os_service */
