
/*
 * $Id: Sockaddr.cxx,v 1.6 2009/11/24 11:36:47 rsc Exp $
 * 
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "auto_array.h"
#include "port.h"

// own
#include "Sockaddr.h"

#ifdef SYSTEM_UNIX
# include <arpa/inet.h>
# include <netdb.h>
# include <unistd.h>
#endif

#ifdef SYSTEM_WIN32
# include <winsock2.h>
# include <ws2tcpip.h>
#endif


namespace os_service
{

int
Sockaddr::socket(const std::string &nodename, int type)
{
	int domain;

	if (strncmp(nodename.c_str(), "//", 2) == 0)
	{
		domain = AF_INET;

		lib::auto_array<char> buf(nodename.length()+1);
		char *node = buf.data();
		char *service;

		strcpy_s(node, buf.size(), nodename.c_str());
		node += 2;

        service = strchr(node, ':');
		if (!service)
			throw std::invalid_argument("No service specified");

		*service++ = '\0';

		struct addrinfo hint;

		bzero(&hint, sizeof(hint));
		hint.ai_family = domain;
		hint.ai_socktype = type;

		struct addrinfo *infop = NULL;

		if(strlen(node) == 0)
			throw std::runtime_error("Empty nodenames are invalid");

		sys::getaddrinfo(node, service, &hint, &infop);

		memcpy(addr_void(), infop->ai_addr, infop->ai_addrlen);
		len() = infop->ai_addrlen;

		sys::freeaddrinfo(infop);
	}
	else
	{
#ifdef SYSTEM_WIN32
		throw std::runtime_error("AF_UNIX sockets not available on this platform");
#else
		domain = AF_UNIX;

		struct sockaddr_un *sunp = addr_un();

		if (nodename.length() >= sizeof(sunp->sun_path))
			throw std::invalid_argument("Address too long");

		strcpy(sunp->sun_path, nodename.c_str());
		sunp->sun_family = domain;
		len() = sizeof(*sunp);
#endif
	}

	return sys::socket(domain, type, 0);
}

} /* namespace os_service */
