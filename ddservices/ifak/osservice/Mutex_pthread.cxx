
/*
 * $Id: Mutex_pthread.cxx,v 1.6 2007/06/13 15:02:35 fna Exp $
 * 
 * Copyright (C) 2004-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "Mutex.h"


namespace os_service
{

Mutex::Mutex(void)
: locked_(false)
{
	if (pthread_mutex_init(&mutex_, NULL) != 0)
		throw std::runtime_error("Mutex: can't allocate resource");
}

Mutex::~Mutex(void)
{
	assert(!locked_);

	int ret = pthread_mutex_destroy(&mutex_);
	assert(ret == 0);
}

void
Mutex::lock(void)
{
	int ret = pthread_mutex_lock(&mutex_);
	assert(ret == 0);

	assert(!locked_);
	locked_ = true;
}

void
Mutex::unlock(void)
{
	assert(locked_);
	locked_ = false;

	int ret = pthread_mutex_unlock(&mutex_);
	assert(ret == 0);
}

bool
Mutex::try_lock(void)
{
	int ret = pthread_mutex_trylock(&mutex_);
	assert(ret == 0 || ret == EBUSY);

	if (ret == 0)
	{
		assert(!locked_);
		locked_ = true;

		return true;
	}

	return false;
}

} /* namespace os_service */
