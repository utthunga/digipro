
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "external_builtins.h"

#include "edd_ast.h"
#include "edd_tree.h"

namespace builtins
{

using namespace edd;

edd_int32
GET_LOCAL_VAR_VALUE(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	lib::IDENTIFIER local_var_name)
{
	edd_int32 ret = 0;

	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("GET_LOCAL_VAR_VALUE"));

	return ret;
}

edd_int32
get_local_var_value(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &global_var_ids,
	lib::IDENTIFIER local_var_name)
{
	edd_int32 ret = 0;

	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("get_local_var_value"));

	return ret;
}

} /* namespace builtins */
