
/*
 * Wed Nov 20 09:30:48 2013 CET
 *
 * DO NOT EDIT!
 *
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 *
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 *
 */

/*
 * edd_builtins_null.cxx
 *
 *  Created on: Apr 29, 2014
 *  Implementation of UI builtins without
 *  using any GUI library. Just the console
 *  prints are used.
 *
*/

#include <stdio.h>
#include "edd_builtins_null.h"
#include "edd_builtins_impl.h"
#include "edd_values_arithmetic.h"
#include "edd_ast.h"
#include "edd_tree.h"
#include <iostream>
#include <cstring>
#include <string>
#include "pbInterpreter.h"
#include <cstdlib>


namespace builtins
{

using namespace edd;

edd_int32 box;
/* Needs to invoke Text Box here in QtWidgets.
 * Prompt will be shown in text box.
 * Currently using console prints.
*/
edd_int32
ACKNOWLEDGE(
	BUILTINS_STDARGS_DEF,
	edd_string prompt)
{
	edd_int32 __ret = BI_ERROR;
	edd_string ack;
	bool input_loop;
	input_loop=0;
	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(prompt, NULL , NULL));
    std::cout<<msg<<std::endl;

	do
	{
		try
		{
			std::cout << "Press OK to acknowledge" << std::endl;
			std::cout << "Press CANCEL to abort" << std::endl;
			std::cin >> ack;
			if (ack == "OK")
			{
				__ret=BI_SUCCESS;
			}
			else if(ack== "CANCEL")
			{
				abort(env, caller_pos);
				__ret=BI_ERROR;
			}
			else
				throw("Invalid Input");
			input_loop=0;

		}
		catch (const char *str)
		{
			 std::cout << str<<std::endl;
			 input_loop=1;
			 continue;
		}
	}while(input_loop);

	return __ret;

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("ACKNOWLEDGE"));
#endif

}

/* Needs to invoke Text Box here in QtWidgets.
 * Prompt will be shown in text box.
 * Currently using console prints.
*/
void
DELAY(
	BUILTINS_STDARGS_DEF,
	edd_int32 delay_time,
	edd_string prompt)
{
	box=EDIT_BOX_TYPE_STRING;
	do
	{
		std::string msg(env->builtin_printf(prompt, NULL , NULL));
		std::cout<<msg<<std::endl;
	}
	while(delay_time=sleep(delay_time));

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("DELAY"));
#endif
}

/* Nothing to display to user.
 * Kept debug prints as of now.
*/
void
DELAY_TIME(
	BUILTINS_STDARGS_DEF,
	edd_int32 delay_time)
{

    edd_int32 delay;
	std::cout<< "DELAY_TIME built-ins" <<std::endl;  //debug statement
	sleep(delay_time);

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("DELAY_TIME"));
#endif
}

/* Needs to invoke Text Box here in QtWidgets.
 * Prompt will be shown in text box.
 * Currently using console prints.
 * TODO: Need to incorporate dynamic value updation mechanism.
*/
void
DISPLAY(
	BUILTINS_STDARGS_DEF,
	edd_string prompt)
{
	edd_string ack;
	bool input_loop;
	input_loop=0;
	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(prompt, NULL , NULL));
	std::cout<<msg<<std::endl;

	do
	{
		try
		{
			std::cout << "Press OK to acknowledge" << std::endl;
			std::cout << "Press CANCEL to abort" << std::endl;
			std::cin >> ack;
			if (ack == "OK")
			{
				//Exit safely
			}
			else if(ack== "CANCEL")
			{
				abort(env, caller_pos);

			}
			else
				throw("Invalid Input");
			input_loop=0;

		}
		catch (const char *str)
		{
			 std::cout << str<<std::endl;
			 input_loop=1;
			 continue;
		}
	}while(input_loop);

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("DISPLAY"));
#endif
}

/* This Builtin will display the specified
 * prompt message, and allow the user to
 * edit the value of a device VARIABLE.
 */
edd_int32
GET_DEV_VAR_VALUE(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	class tree * device_var_name)
{
	edd_int32 __ret = BI_ERROR;
	std::string msg(env->builtin_printf(prompt, NULL ,NULL));
	std::cout<<msg<<std::endl;

	return SetNewValue(device_var_name,env);
}

/* This Builtin will display the specified
 * prompt message, and allow the user to
 * edit the value of a local variable.
 */
edd_int32
GET_LOCAL_VAR_VALUE(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	lib::IDENTIFIER local_var_name)
{

	edd_int32 __ret = BI_ERROR;
	std::string new_val;
	std::string outstring(env->builtin_printf(prompt, NULL ,NULL));
	std::cout<<outstring<<std::endl;
	bool input_loop ;
	input_loop= 0;

	stackmachine::Type  *t;
    t=env->frame_lookup(local_var_name);

	// Displays the current value of the local variable here.

	if (t !=NULL)
	{
		std::string msg(t->print());
		std::cout<<"Current value of the variable is "<<msg<<std::endl;

		do
		{
			try
			{	std::cout<<"Please enter the new " << t->type_descr()<< " value"<<std::endl;
				std::cin>>new_val;
				t->scan(new_val);
				input_loop=0;
			}
			catch (std::exception& e)
			{
				 std::cout << e.what()<<std::endl;
				 input_loop=1;
				 continue;
			}
		}while(input_loop);

        std::string msg2(t->print());
		std::cout<<"The new value of the variable is"<<msg2<<std::endl;
		__ret=BI_SUCCESS;
	}
	else
	{
		throw RuntimeError(
				xxIdentifierUnknown,
				(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
				env->parent.ident().create("GET_LOCAL_VAR_VALUE"));
		__ret=BI_ERROR;
	}

	return __ret;

}

void
LOG_MESSAGE(
	BUILTINS_STDARGS_DEF,
	edd_int32 priority,
	edd_string message)
{

	switch(priority)
	{
	case 0:
		env->error("BUILTIN", message.c_str());
		break;
	case 1:
		env->warning("BUILTIN",message.c_str());
		break;
	case 2:
		env->trace("BUILTIN", message.c_str());
		break;
	default:
		_ERROR_1(env,caller_pos,"Undefined Priority");
		std::cout<<"Undefined priority"<<std::endl;
    }


#if 0
	throw RuntimeError(
		xxUnimplementedBuiltin,
		(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
		env->parent.ident().create("LOG_MESSAGE"));
#endif
}

/* Needs to invoke combo box here in QtWidgets.
 * Each option will be shown in combo box and
 * user may select any one of it.
 * Currently using console prints.
*/
edd_int32
MenuDisplay(
	BUILTINS_STDARGS_DEF,
	class tree * menu,
	edd_string options,
	edd_int32_ref &selection)
{
	edd_int32 __ret = BI_ERROR;
	edd_int32 index = 0;
	box=COMBO_BOX_SINGLE_SELECT;

	char * cstr = new char[options.length() + 1];
	std::strncpy(cstr, options.c_str(),options.length() +1);

	// cstr now contains a c-string copy of str
	eval_menu(env, menu,0);

	//std::cout << menu->ident().str() << std::endl;

	char * p = std::strtok(cstr, " ; ");
	while (p != 0)
	{
	    /* p is an option,which should be sent to UI.
	     * And these options should be displayed as buttons on the dialogue box .
         */
		p = strtok(NULL, " ; ");
		std::cout << index << " for " << p << '\n';
		index++;
	}
	std::cin >> __ret;
	selection = __ret;
	__ret = BLTIN_SUCCESS;
	delete[] cstr;

#if 0
	throw RuntimeError(
		xxUnimplementedBuiltin,
		(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
		env->parent.ident().create("MenuDisplay"));
#endif
	return __ret;
}

/* Needs to invoke Text Box here in QtWidgets.
 * Prompt will be shown in text box.
 * Currently using console prints.
*/
void
PUT_MESSAGE(
	BUILTINS_STDARGS_DEF,
	edd_string message)
{

	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(message, NULL , NULL));
	std::cout<<msg<<std::endl;

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("PUT_MESSAGE"));
#endif
}

/* Needs to invoke combo box here in QtWidgets.
 * Each option will be shown in combo box and
 * user may select any one of it.
 * Currently using console prints.
*/
edd_int32
SELECT_FROM_LIST(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_string option_list)
{
    edd_int32 __ret = BI_ERROR;
    edd_int32 index=0;
    box=COMBO_BOX_SINGLE_SELECT;
    std::string msg(env->builtin_printf(prompt, NULL , NULL));
    std::cout<<msg<<std::endl;

    char * cstr = new char [option_list.length()+1];
    std::strncpy (cstr, option_list.c_str(),option_list.length() +1);

    char * p = std::strtok (cstr," ; ");
    while (p!=0)
    {
        std::string options(env->builtin_printf(p, NULL , NULL));
      	std::cout << index  << " for "<< options << '\n';
      	p = strtok(NULL," ; ");
      	index++;
    }
    if(std::cin >>__ret)
	{
    	delete[] cstr;
		__ret=BI_SUCCESS;
	}

#if 0
	throw RuntimeError(
		xxUnimplementedBuiltin,
		(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
		env->parent.ident().create("SELECT_FROM_LIST"));
#endif
	return __ret;
}

/*
 * It puts a textual information in a log.
 * This information is put into the category error.
 */
void
_ERROR(
	BUILTINS_STDARGS_DEF,
	edd_int32 value)
{
	env->error("BUILTINS","%d" ,value);

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("_ERROR"));
#endif
}

/*
 * It puts a textual information in a log.
 * This information is put into the category error.
 */
void
_ERROR_1(
	BUILTINS_STDARGS_DEF,
	edd_string message)
{
	env->error("BUILTINS",message.c_str());
}

/*
 * It puts a textual information in a log.
 * This information is put into the category error.
 */
void
_ERROR_2(
	BUILTINS_STDARGS_DEF,
	edd_real32 value)
{
	env->error("BUILTINS","%f", value);

}

/*
 * It puts a textual information in a log.
 * This information is put into the category error.
 */
void
_ERROR_3(
	BUILTINS_STDARGS_DEF,
	edd_uint32 value)
{
	env->error("BUILTINS","%u", value);
}

/*
 * It puts a textual information in a log.
 * This information is put into the category trace.
 */
void
_TRACE(
	BUILTINS_STDARGS_DEF,
	edd_int32 value)
{
	env->trace("BUILTINS","%d",value);

}

/*
 * It puts a textual information in a log.
 * This information is put into the category trace.
 */
void
_TRACE_1(
	BUILTINS_STDARGS_DEF,
	edd_string message)
{
	env->trace("BUILTINS",message.c_str());
}

/*
 * It puts a textual information in a log.
 * This information is put into the category trace.
 */
void
_TRACE_2(
	BUILTINS_STDARGS_DEF,
	edd_real32 value)
{
	env->trace("BUILTINS","%f",value);
}

/*
 * It puts a textual information in a log.
 * This information is put into the category trace.
 */
void
_TRACE_3(
	BUILTINS_STDARGS_DEF,
	edd_uint32 value)
{
	env->trace("BUILTINS","%u",value);
}

/*
 * It puts a textual information in a log.
 * This information is put into the category warning.
 */
void
_WARNING(
	BUILTINS_STDARGS_DEF,
	edd_int32 value)
{
	env->warning("BUILTINS","%d",value);
}

/*
 * It puts a textual information in a log.
 * This information is put into the category warning.
 */
void
_WARNING_1(
	BUILTINS_STDARGS_DEF,
	edd_string message)
{
	env->warning("BUILTINS",message.c_str());
}

/*
 * It puts a textual information in a log.
 * This information is put into the category warning.
 */
void
_WARNING_2(
	BUILTINS_STDARGS_DEF,
	edd_real32 value)
{
	env->warning("BUILTINS","%f",value);
}

/*
 * It puts a textual information in a log.
 * This information is put into the category warning.
 */
void
_WARNING_3(
	BUILTINS_STDARGS_DEF,
	edd_uint32 value)
{
	env->warning("BUILTINS","%u",value);
}

/* It will display a message indicating that the method
 * has been aborted and wait for acknowledgement from the user.
 * Once acknowledgement has been made, the system will execute
 * any abort methods in the abort method list and will exit the method.
 */
void
abort(
	BUILTINS_STDARGS_DEF)
{
	edd_string ack;
	bool input_loop;
	box=EDIT_BOX_TYPE_STRING;

	std::cout<<"METHOD ABORTING"<<std::endl;
	std::cout<<"Press OK to continue aborting"<<std::endl;

	std::cin >> ack;  // Waits for User Acknowledgement

	do
		{
			try
			{
				std::cout << "Press OK to continue aborting" << std::endl;
				std::cin >> ack;
				if (ack == "OK")
				{
					if(!(env->aborted()))
					env->abort();
				}
				else
					throw("Invalid Input");
				input_loop=0;
			}
			catch (const char *str)
			{
				 std::cout << str<<std::endl;
				 input_loop=1;
				 continue;
			}
		}while(input_loop);

	node_tree *node =env->context_node();
    std::cout<< "Aborted from the method : "<<node->ident().str()<<std::endl;

#if 0
	throw RuntimeError(xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ?caller_pos : env->context_node()->pos,
			env->parent.ident().create("abort"));
#endif
}


edd_int32
acknowledge(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &global_var_ids)
{
	edd_int32 __ret = BI_ERROR;
	edd_string ack;
	bool input_loop;
	input_loop=0;
	box=EDIT_BOX_TYPE_STRING;

	std::string msg(env->builtin_printf(prompt, NULL , NULL));
    std::cout<<msg<<std::endl;

	do
	{
		try
		{
			std::cout << "Press OK to acknowledge" << std::endl;
			std::cout << "Press CANCEL to abort" << std::endl;
			std::cin >> ack;
			if (ack == "OK")
			{
				__ret=BI_SUCCESS;
			}
			else if(ack== "CANCEL")
			{
				abort(env, caller_pos);
				__ret=BI_ERROR;
			}
			else
				throw("Invalid Input");
			input_loop=0;

		}
		catch (const char *str)
		{
			 std::cout << str<<std::endl;
			 input_loop=1;
			 continue;
		}
	}while(input_loop);

	return __ret;

}

void
delay(
	BUILTINS_STDARGS_DEF,
	edd_int32 delay_time,
	edd_string prompt,
	edd_int32_array &global_var_ids)
{
	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(prompt, &global_var_ids , NULL));
	do
	{
	 	std::cout<<msg<<std::endl;

	}
	while(delay_time=sleep(delay_time));

}


void
display(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &global_var_ids)
{
	edd_string ack;
	bool input_loop;
	input_loop=0;
	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(prompt, NULL , NULL));
    std::cout<<msg<<std::endl;

    do
    	{
    		try
    		{
    			std::cout << "Press OK to acknowledge" << std::endl;
    			std::cout << "Press CANCEL to abort" << std::endl;
    			std::cin >> ack;
    			if (ack == "OK")
    			{
					// Exit safely
    			}
    			else if(ack== "CANCEL")
    			{
    				abort(env, caller_pos);

    			}
    			else
    				throw("Invalid Input");
    			input_loop=0;

    		}
    		catch (const char *str)
    		{
    			 std::cout << str<<std::endl;
    			 input_loop=1;
    			 continue;
    		}
    	}while(input_loop);

return ;
}

/* This Builtin will display the specified
 * prompt message, and allow the user to
 * edit the value of a device VARIABLE.
 * Prompt will be shown in text box.
 * Currently using console prints.
 */

edd_int32
get_dev_var_value(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &ids,
	class tree * device_var_name)
{
	std::string msg(env->builtin_printf(prompt, &ids ,NULL));
	std::cout<<msg<<std::endl;

	return SetNewValue(device_var_name,env);
}


/* This Builtin will display the specified
 * prompt message, and allow the user to
 * edit the value of a local variable.
 * Prompt will be shown in text box.
 * Currently using console prints.
 */
edd_int32
get_local_var_value(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &global_var_ids,
	lib::IDENTIFIER local_var_name)
{
	edd_int32 __ret = BI_ERROR;
	edd_string new_val;
	bool input_loop ;
	input_loop= 0;

	std::string outstring(env->builtin_printf(prompt, &global_var_ids ,NULL));
	std::cout<<outstring<<std::endl; // need to displayed to user.

    stackmachine::Type  *t;
    t=env->frame_lookup(local_var_name);
	if (t !=NULL)
	{
		std::string msg(t->print());
		std::cout<<"Current value of the variable is "<<msg<<std::endl;

		do
		{
			try
			{
				std::cout<<"Please enter the new " << t->type_descr()<< " value"<<std::endl;
				std::cin>>new_val;
				t->scan(new_val);
				input_loop=0;
			}
			catch (std::exception& e)
			{
				 std::cout << e.what()<<std::endl;
				 input_loop=1;
				 continue;
			}
		}while(input_loop);

        std::string msg2(t->print());
		std::cout<<"The new value of the variable is"<<msg2<<std::endl;
		__ret=BI_SUCCESS;
	}
	else
	{
		throw RuntimeError(
				xxIdentifierUnknown,
				(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
				env->parent.ident().create("GET_LOCAL_VAR_VALUE"));
		__ret=BI_ERROR;
	}
	return __ret;
}


void
put_message(
	BUILTINS_STDARGS_DEF,
	edd_string message,
	edd_int32_array &global_var_ids)
{

	box=EDIT_BOX_TYPE_STRING;
	std::string msg(env->builtin_printf(message, &global_var_ids , NULL));
	std::cout<<msg<<std::endl;
}


edd_int32
select_from_list(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_int32_array &global_var_ids,
	edd_string option_list)
{
	edd_int32 __ret = BI_ERROR;
	box=COMBO_BOX_SINGLE_SELECT;
	// cstr now contains a c-string copy of prompt
	std::string msg(env->builtin_printf(prompt, &global_var_ids , NULL));
	std::cout<<msg<<std::endl;

	edd_int32 index=0;
    char * cstr = new char [option_list.length()+1];
    std::strncpy (cstr, option_list.c_str(),option_list.length() +1);

    char * p = std::strtok (cstr," ; ");
    while (p!=0)
    {
      std::string option(env->builtin_printf(p, &global_var_ids , NULL));
      std::cout << index  << "for"<< option << '\n';
      p = strtok(NULL," ; ");
      index++;
    }
    if(std::cin >>__ret)
	{
		delete[] cstr;
		__ret=BI_SUCCESS;
	}

return __ret;

}

const long SetNewValue( class tree* device_var_name, class method_env* env)
{
	edd_string value;

	if(edd::EDD_VARIABLE == device_var_name->kind)
	{
		class VARIABLE_tree *v = (class VARIABLE_tree *)device_var_name;
		v->run_actions(edd::EDD_OBJECT_tree::PRE_EDIT_ACTIONS, env);
		edd::operand_ptr op(device_var_name->value(env));
		std::cout<<"Current Value of the "<< device_var_name->ident().str()<< " is "<<op->pretty_print()<<std::endl;

		unsigned long type = v->gettype();
		switch(v->gettype())
		{
			case EDD_VARIABLE_TYPE_ARITHMETIC:
				switch(v->getsubtype())
				{
					case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_INTEGER:
						box=EDIT_BOX_TYPE_INTEGER;
						std::cout<<"Please enter the new integer value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_UNSIGNED:
						box=EDIT_BOX_TYPE_INTEGER;
						std::cout<<"Please enter the new unsigned integer value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_DOUBLE:
						box=EDIT_BOX_TYPE_FLOAT;
						std::cout<<"Please enter the new double value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_FLOAT:
						box=EDIT_BOX_TYPE_FLOAT;
						std::cout<<"Please enter the new float value"<<std::endl;
					break;

				}
			break;
			case EDD_VARIABLE_TYPE_BOOLEAN:
				std::cout<<"Please enter the boolean value"<<std::endl;
			break;
			case EDD_VARIABLE_TYPE_STRING:
				switch(v->getsubtype())
				{
					case EDD_VARIABLE_TYPE_STRING_TYPE_ASCII:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new ASCII value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_BITSTRING:
						box=COMBO_BOX_MULTI_SELECT;
						std::cout<<"Please enter the new BITSTRING value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_EUC:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new EUC value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_OCTET:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new OCTET value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new PACKED_ASCII value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_PASSWORD:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new PASSWORD value"<<std::endl;
					break;
					case EDD_VARIABLE_TYPE_STRING_TYPE_VISIBLE:
						box=EDIT_BOX_TYPE_STRING;
						std::cout<<"Please enter the new VISIBLE value"<<std::endl;
					break;

				}
			break;
			case EDD_VARIABLE_TYPE_INDEX:
			break;
			case EDD_VARIABLE_TYPE_DATE_TIME:
				switch(v->getsubtype())
				{
				case EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DATE:
					box=EDIT_BOX_TYPE_DATE;
					std::cout<<"Please enter the new DATE"<<std::endl;
					break;
				case EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DATE_AND_TIME:
					box=EDIT_BOX_TYPE_DATE_AND_TIME;
					std::cout<<"Please enter the new DATE AND TIME"<<std::endl;
					break;
				case EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DURATION:
					box=EDIT_BOX_TYPE_DATE_AND_TIME;
					std::cout<<"Please enter the new DURATION"<<std::endl;
					break;
				case EDD_VARIABLE_TYPE_DATE_TIME_TYPE_TIME:
					box=EDIT_BOX_TYPE_TIME;
					std::cout<<"Please enter the new TIME"<<std::endl;
					break;
				case EDD_VARIABLE_TYPE_DATE_TIME_TYPE_TIME_VALUE:
					box=EDIT_BOX_TYPE_TIME;
					std::cout<<"Please enter the new TIME VALUE"<<std::endl;
					break;
				}

			break;
			case EDD_VARIABLE_TYPE_OBJECT:
			break;
			case EDD_VARIABLE_TYPE_ENUMERATED:
			{

				switch(v->getsubtype())
				{
					case EDD_VARIABLE_TYPE_ENUMERATED_TYPE_ENUMERATED:
					{
						box=COMBO_BOX_SINGLE_SELECT;
						std::cout<<"Please select a value from the list"<<std::endl;

					}
					case EDD_VARIABLE_TYPE_ENUMERATED_TYPE_BITENUMERATED:
				    {
				    	box=COMBO_BOX_MULTI_SELECT;
						std::cout<<"Please select values from the list"<<std::endl;
					}
					default:
					std::vector<edd::VARIABLE_ENUMERATOR_tree *> enum_tree= v->get_enumerations(env);
					for(int i=0;i<enum_tree.size();i++)
					{
						std::cout<< enum_tree[i]->description(env)<<std::endl;
					}
				}
			}
			break;
			default:
				std::cout<<"Invalid variable type "<<std::endl;
				return BI_ERROR;
		}

		std::cin>>value;
		try
		{
			op->pretty_scan(value);
		}
		catch(const std::exception &e)
		{
			std::cout<<e.what()<<std::endl;
			return BI_ERROR;
		}

		std::cout<<"The new Value of the "<< device_var_name->ident().str()<< " is "<<op->pretty_print()<<std::endl;
		v->run_actions(edd::EDD_OBJECT_tree::POST_EDIT_ACTIONS, env);
		return BI_SUCCESS;
    }
	else
	    return BI_ERROR;
}


void
eval_menu(edd::method_env *env, edd::tree *t, unsigned char lev)
{
	level(lev);
	// operate on the offline data set -> pass NULL as environment argument
	std::cout << t->label(NULL) << " (" << t->ident().str() << ")" << std::endl;

	if (t)
	{
		Interpreter interobj;

		// Step1: evaluate menu
		edd::Interpreter::menuitems_t menu = interobj.evalmenu(t,NULL);

		// Step2: iterate over menu items
		for (edd::Interpreter::menuitem_t &item : menu)
		{
			// Step3: recurse into submenu
			if (item.ref->kind == edd::EDD_MENU)
				eval_menu(env, item.ref, lev+1);

			// or Step4: print variable value
			else if (item.ref->kind == edd::EDD_VARIABLE)
				{
				edd::VARIABLE_tree* vt = (edd::VARIABLE_tree *) item.ref;
				level(lev+1);
				//std::cout << "Variable " << vt->label(env) << " ";
				std::cout << "Variable " << " ";

				// evaluate variable value
				edd::operand_ptr op(vt->value(env));
				std::cout << "(" << vt->label(env) << " = " << op->pretty_print();

				// print unit if present
				if (vt->have_unit())
					std::cout << " [" << vt->get_unit(env) << "]";

				std::cout << ")" << std::endl;

				// prints HELP string
				std::cout << "HELP string :" << vt->help(env)<<std::endl;

				
				}
		}
	}
	else
		throw std::runtime_error("Identifier not found");
}

/* Provides tab space to differentiate between menu, sub-menu and variables*/
void
level(unsigned char ind)
{
	while (ind--)
		std::cout << "\t";
}


/*
 * Not a part of PROFIBUS profile according to IEC spec.
 * Confirmed by ifak.
 * Implementation is not needed.
 *
*/
edd_int32
delayfor(
	BUILTINS_STDARGS_DEF,
	edd_int32 duration,
	edd_string prompt,
	edd_uint32_array &ids,
	edd_uint32_array &indices,
	edd_int32 id_count)
{
	edd_int32 __ret = BI_ERROR;

	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("delayfor"));

	return __ret;
}

/*
 * Not a part of PROFIBUS profile according to IEC spec.
 * Confirmed by ifak.
 * Implementation is not needed.
 *
*/
edd_int32
edit_device_value(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_uint32_array &ids,
	edd_uint32_array &indices,
	edd_int32 id_count,
	edd_uint32 param_id,
	edd_uint32 param_member_id)
{
	edd_int32 __ret = 0;

	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("edit_device_value"));

	return __ret;
}

/*
 * Not a part of PROFIBUS profile according to IEC spec.
 * Confirmed by ifak.
 * Implementation is not needed.
 *
*/
edd_int32
edit_local_value(
	BUILTINS_STDARGS_DEF,
	edd_string prompt,
	edd_uint32_array &ids,
	edd_uint32_array &indices,
	edd_int32 id_count,
	edd_string local_var)
{
	edd_int32 __ret = 0;

	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("edit_local_value"));

	return __ret;
}

/*
 * Not a part of PROFIBUS profile according to IEC spec.
 * Confirmed by ifak.
 * Implementation is not needed.
 *
*/
void
method_abort(
	BUILTINS_STDARGS_DEF,
	edd_string prompt)
{

	throw RuntimeError(xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ?caller_pos : env->context_node()->pos,
			env->parent.ident().create("method_abort"));

}


/* Not a part of EDDL specification.
 * Needs confirmation on this one. Does our DPC support opening urls.
 * Pragya: Dont know the actual use case of this wrt to DPC functionalities and support.
*/
void
ShellExecute(
	BUILTINS_STDARGS_DEF,
	edd_string arg)
{
	edd_string application;
	std::cout<<"Please enter the application needed to launch"<<std::endl;
	std::cin>>application;

	system((application+' ' +arg).c_str());

#if 0
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("ShellExecute"));
#endif
}


/* Not a part of EDDL specification.
 * Refreshes the UI view. Can be handled after Qt integration
 */
void
refresh(
	BUILTINS_STDARGS_DEF,
	edd_int32_array &var_ids)
{

	// Read values of passed variables to the built-in.
	std::cout<<"Needs to be handled in QT by updating the value of variables in the current view"<<std::endl;
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("refresh"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
method_set(
	BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("method_set"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_Funktionsleiste(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_Funktionsleiste"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_LoadinDevice(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_LoadinDevice"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_LoadinPC(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_LoadinPC"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_NodeAddress(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_NodeAddress"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_Statuszeile(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_Statuszeile"));
}

/*
 *
 * We do not know the implementation requirements and approach for this built-in. No information documented.
*/
void
std_Update(
        BUILTINS_STDARGS_DEF)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("std_Update"));
}

} /* namespace builtins */



