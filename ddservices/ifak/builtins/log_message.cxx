
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "external_builtins.h"

#include "edd_ast.h"
#include "edd_tree.h"

namespace builtins
{

using namespace edd;

void
LOG_MESSAGE(
	BUILTINS_STDARGS_DEF,
	edd_int32 priority,
	edd_string message)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("LOG_MESSAGE"));
}

} /* namespace builtins */
