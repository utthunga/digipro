#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    Compiler Options #######################################################
#############################################################################
add_compile_options(${FLUKE_IGNORE_COMPILE_WARNINGS})

#############################################################################
#    include the source files ###############################################
#############################################################################
set (BUILTIN_SRC  ${CMAKE_CURRENT_SOURCE_DIR}/edd_builtins_null.cxx)

#############################################################################
#    include the header files ###############################################
#############################################################################
include_directories (${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/edd
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/osservice
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/support
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/sys
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/profibus
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/stackmachine
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/ucpp
                     ${CMAKE_PROSTAR_SRC_DIR}/clogger)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(builtin OBJECT ${BUILTIN_SRC})
