
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "external_builtins.h"

#include "edd_ast.h"
#include "edd_tree.h"

namespace builtins
{

using namespace edd;

void
_WARNING(
	BUILTINS_STDARGS_DEF,
	edd_int32 value)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("_WARNING"));
}

void
_WARNING_1(
	BUILTINS_STDARGS_DEF,
	edd_string message)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("_WARNING"));
}

void
_WARNING_2(
	BUILTINS_STDARGS_DEF,
	edd_real32 value)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("_WARNING"));
}

void
_WARNING_3(
	BUILTINS_STDARGS_DEF,
	edd_uint32 value)
{
	throw RuntimeError(
			xxUnimplementedBuiltin,
			(caller_pos != lib::NoPosition) ? caller_pos : env->context_node()->pos,
			env->parent.ident().create("_WARNING"));
}

} /* namespace builtins */
