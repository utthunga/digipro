
/* 
 * $Id: paths.cxx,v 1.9 2007/07/16 11:07:15 fna Exp $
 *
 * Copyright (C) 2005-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>
#include <dirent.h>

// own libs
#include "dirent.h"
#include "statxx.h"
#include "sys.h"
#include "dirent_edd.h"

// own header
#include "paths.h"
#include "string.h"


namespace lib
{

static std::string
next_path_element(const std::string &path, std::string::size_type &start)
{
	std::string result;

	if (start != std::string::npos)
		start = path.find_first_not_of(PATHSEPARATORS, start);

	if (start != std::string::npos)
	{
		std::string::size_type pos;

		pos = path.find_first_of(PATHSEPARATORS, start);
		if (pos != std::string::npos)
		{
			result = path.substr(start, pos - start);
			start = pos + 1;
		}
		else
		{
			result = path.substr(start);
			start = std::string::npos;
		}
	}

	return result;
}

WINDLL_LIB std::string
rel_path(const std::string &path, const std::string &base)
{
#ifdef WIN32
	/*
	 * special handling under DOS/Windows
	 * if path and base are not on the same drive
	 * just return path
	 */
	bool not_possible = true;

	if (path.size() > 2 && base.size() > 2)
	{
		if (   path[0] == '\\' && base[0] == '\\'
		    && path[1] == '\\' && base[1] == '\\')
		{
			std::string::size_type path_pos = path.find_first_of(PATHSEPARATORS, 2);
			std::string::size_type base_pos = base.find_first_of(PATHSEPARATORS, 2);

			if (path_pos != std::string::npos
			    && path_pos == base_pos)
			{
				if (path.substr(0, path_pos) == base.substr(0, base_pos))
					not_possible = false;
			}
		}
		else if (   path[1] == ':'  && base[1] == ':'
			 && path[2] == '\\' && base[2] == '\\')
		{
			if (tolower(path[0]) == tolower(base[0]))
				not_possible = false;
		}
	}

	if (not_possible)
		return path;
#endif
	std::string::size_type path_pos = 0, base_pos = 0;
	std::string result;

	for (;;)
	{
		std::string path_element(next_path_element(path, path_pos));
		std::string base_element(next_path_element(base, base_pos));

#ifdef WIN32
		/* under WIN32 compare case insensnitive */
		std::string path_element_i(path_element); strlwr(path_element_i);
		std::string base_element_i(base_element); strlwr(base_element_i);

		if (path_element_i != base_element_i)
#else
		if (path_element != base_element)
#endif
		{
			result = path_element;

			if (path_pos != std::string::npos)
				result += path.substr(path_pos - 1);

			if (!base_element.empty())
			{
				do {
					if (result.empty())
						result = "..";
					else
						result.insert(0, ".." DEF_PATHSEPARATOR);
				}
				while (!next_path_element(base, base_pos).empty());
			}

			break;
		}

		if (path_pos == std::string::npos && base_pos == std::string::npos)
		{
			result = '.';
			break;
		}

		assert(!path_element.empty() && !base_element.empty());
	}

	return result;
}

static void
strip_last_path_element(std::string &path)
{
	std::string::size_type pos;

	pos = path.find_last_of(PATHSEPARATORS);
	if (pos != std::string::npos)
		path.erase(pos);
	else
		path.clear();
}

WINDLL_LIB bool
is_abs_path(const std::string &path)
{
	if (path.size())
	{
		if (path.find_first_not_of(PATHSEPARATORS) != 0)
			return true;

#ifdef WIN32
		/* special check under DOS/Windows
		 */
		if (path.size()> 2)
		{
			if (path[1] == ':' && path[2] == '\\')
				return true;
		}
#endif
	}

	return false;
}

WINDLL_LIB std::string
abs_path(const std::string &path, const std::string &base)
{
	/* if path is alreaday absolut just return it
	 */
	if (is_abs_path(path))
		return path;

	static const std::string p(".");
	static const std::string pp("..");

	std::string::size_type path_pos = 0;
	std::string result(base);

	for (;;)
	{
		std::string path_element(next_path_element(path, path_pos));

		if (path_element == p)
			continue;

		if (path_element != pp)
		{
			result += DEF_PATHSEPARATOR;
			result += path_element;

			if (path_pos != std::string::npos)
				result += path.substr(path_pos - 1);

			break;
		}

		strip_last_path_element(result);
	}

	return result;
}


WINDLL_LIB void
rrm(const std::string &path)
{
	sys::DIR *dir = sys::opendir(path);
	sys::statxx st;

	if (!dir)
	{
		std::string msg("Unable to open ");
		msg += path;

		throw std::invalid_argument(msg);
	}

	std::string entry = sys::readdir(dir);

	for (; !entry.empty(); entry = sys::readdir(dir))
	{
		if (entry == "." || entry == "..")
			continue;

		entry = path + DEF_PATHSEPARATOR + entry;

		sys::stat(entry, st);

		if (st.isdir())
			rrm(entry);

		if (st.isreg())
			sys::unlink(entry);
	}

	sys::closedir(dir);

	sys::rmdir(path);
}
} /* namespace lib */
