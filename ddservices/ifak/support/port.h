
/* 
 * $Id: port.h,v 1.7 2009/07/06 22:33:43 fna Exp $
 *
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_port_h
#define _lib_port_h

#include "lib_config.h"

// C stdlib
#include <stdio.h>
#include <stdlib.h>
#include <string_edd.h>

// C++ stdlib

// own header


#define strcpy_s(buf,buflen,src) strcpy(buf,src)
#define strcat_s(buf,buflen,src) strcat(buf,src)

#define strncpy_s(buf,buflen,src,count) strncpy(buf,src,count)
#define strncat_s(buf,buflen,src,count) strncat(buf,src,count)

#if defined(_MSC_VER)
# if _MSC_VER < 1400
#  define snprintf _snprintf
# else
#  define snprintf(a,b,c,...) _snprintf_s(a,b,_TRUNCATE,c,__VA_ARGS__)
#  define localtime_r(a,b) (localtime_s(b,a), b)
/* safe str* functions are available */
#  undef strcpy_s
#  undef strcat_s
#  undef strncpy_s
#  undef strncat_s
# endif
# define putenv _putenv
# define strdup _strdup
# define strtoll _strtoi64
# define strtoull _strtoui64
# define isnan _isnan
namespace lib { WINDLL_LIB int isinf(double); }
# define isinf lib::isinf
#endif

#if defined(_MSC_VER) || defined(__MINGW32__)
# define bzero(ptr,size) memset((ptr),0,(size))
#endif

#if defined(__MINGW32__)
# define sleep _sleep
#endif

namespace lib
{

} /* namespace lib */

#endif /* _lib_port_h */
