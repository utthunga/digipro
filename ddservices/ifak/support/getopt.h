
/* 
 * $Id: getopt.h,v 1.1 2005/06/23 13:28:52 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_getopt_h
#define _lib_getopt_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header

#ifdef _MSC_VER
# define HAVE_GETOPT	0
#endif

#ifdef __CYGWIN__
# define HAVE_GETOPT	0
#endif

#ifdef __MINGW32__
# define HAVE_GETOPT	0
#endif

#ifndef HAVE_GETOPT
# define HAVE_GETOPT	1
#endif


#if HAVE_GETOPT

#include <getopt.h>

#else /* HAVE_GETOPT */

#ifdef __cplusplus
extern "C" {
#endif

#define no_argument        0
#define required_argument  1
#define optional_argument  2

struct option
{
	/* name of long option */
	const char *name;
	/*
	 * one of no_argument, required_argument, and optional_argument:
	 * whether option takes an argument
	 */
	int has_arg;
	/* if not NULL, set *flag to val when option found */
	int *flag;
	/* if flag not NULL, value to set *flag to; else return value */
	int val;
};

WINDLL_LIB int getopt(int, char * const *, const char *);
WINDLL_LIB int getopt_long (int, char * const *, const char *, const struct option *, int *);

extern WINDLL_LIB int opterr;
extern WINDLL_LIB int optind;
extern WINDLL_LIB int optopt;
extern WINDLL_LIB int optreset;
extern WINDLL_LIB char *optarg;

#ifdef __cplusplus
}
#endif

#endif /* HAVE_GETOPT */

#endif /* _lib_getopt_h */
