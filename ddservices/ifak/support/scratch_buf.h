
/* 
 * $Id: scratch_buf.h,v 1.12 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a small scratch buffer.
 */

#ifndef _lib_scratch_buf_h
#define _lib_scratch_buf_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>
#include <stdarg.h>
#include <string_edd.h>

// C++ stdlib
#include <string>

// own header
#include "vsnprintf.h"


namespace lib
{

/**
 * scratch buffer.
 */
class WINDLL_LIB scratch_buf
{
public:
	scratch_buf(size_t s);
	~scratch_buf(void) { delete [] buf; }
	
	/* Copy constructor. */
	scratch_buf(const scratch_buf &);
	
	/* Assignment constructor. */
	class scratch_buf& operator=(const scratch_buf &);
	
	/**
	 * Clear the buffer.
	 * This reset the buffer to the empty state.
	 */
	void clear(void) throw() { ptr = buf; space = size - 1; }
	
	/**
	 * Format print.
	 * The same as libc vprintf.
	 * \note The string is appended to the existing content of the buffer.
	 */
	void vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);
	/**
	 * Format print.
	 * The same as libc printf.
	 * \note The string is appended to the existing content of the buffer.
	 */
	void printf(const char *fmt, ...) throw() CHECK_PRINTF(2, 3);
	/**
	 * Append buffer.
	 * Append len characters from buffer str.
	 */
	void put(const char *str, size_t len) throw();
	/**
	 * Append C string.
	 * Append the C string str.
	 */
	void put(const char *str) throw() { put(str, strlen(str)); }
	/**
	 * Append C++ string.
	 * Append the C++ string str.
	 */
	void put(const std::string &str) { put(str.data(), str.length()); }
	/**
	 * Append character.
	 */
	void put(const char c) throw() { if (space) { *ptr++ = c; space--; } }
	/**
	 * Remove characters.
	 * This remove up to len characters from this buffer (but only
	 * length() characters maximum).
	 */
	void del(size_t len) throw();
	
	/**
	 * Get C string.
	 * Return C style string (readonly) of the encapsuled string buffer.
	 */
	const char *c_str(void) const throw() { *ptr = '\0'; return buf; }
	/**
	 * Get std::string.
	 * Return a std::string object of the encapsuled string buffer.
	 */
	std::string str(void) const { return std::string(buf, length()); }
	/**
	 * Get pointer to the data.
	 * \note It's not a C string, there is no terminating '\\0'.
	 */
	const char *data(void) const throw() { return buf; }
	/**
	 * Get data length.
	 * Return the number of used bytes.
	 */
	size_t length(void) const throw() { return (ptr - buf); }
	/**
	 * Check if the buffer is empty.
	 * Return 'true' if the buffer is empty.
	 */
	bool empty(void) const throw() { return (ptr == buf); }
	
	     operator const char *() const throw() { return c_str(); }
	
	void operator= (const std::string &str) throw() { clear(); put(str.data(), str.length()); }
	void operator= (const char *str) throw() { clear(); put(str); }
	
	void operator+=(const std::string &str) throw() { put(str.data(), str.length()); }
	void operator+=(const char *str) throw() { put(str); }
	void operator+=(const char c) throw() { put(c); }
	
private:
	char *buf, *ptr;
	size_t size, space;
};

} /* namespace lib */

#endif /* _lib_scratch_buf_h */
