
/* 
 * $Id: nlz.cxx,v 1.4 2009/11/19 14:37:16 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "nlz.h"

#ifdef _MSC_VER
#pragma warning (disable: 4146)
#endif

namespace lib
{

/**
 * Number of leading zeros.
 * Branch-free binary search implementation. Should be compiled to
 * 28 basic RISC instructions.
 * \note Taken from Hacker's Delight; Henry S. Warren; Addison-Wesley
 */
WINDLL_LIB int
nlz(sys::int32 x)
{
	int y, m, n;
	
	y = -(x >> 16);		// If left half of x is 0,
	m = (y >> 16) & 16;	// set n = 16. If left half
	n = 16 - m;		// is nonzero, set n = 0 and
	x = x >> m;		// shift x right 16.
				// Now x is of the form 0000xxxx.
	y = x - 0x100;		// If position 8-15 are 0,
	m = (y >> 16) & 4;	// add 8 to n and shift x left 8.
	n += m;
	x >>= m;
	
	y = x - 0x1000;		// If positions 12-15 are 0,
	m = (y >> 16) & 4;	// add 4 to n and shift x left 4.
	n += m;
	x <<= m;
	
	y = x - 0x4000;		// If positions 14-15 are 0,
	m = (y >> 16) & 2;	// add 2 to n and shift x left 2.
	n += m;
	x <<= m;
	
	y = x >> 14;		// Set y = 0, 1, 2 or 3.
	m = y & ~(y >> 1);	// Set m = 0, 1, 2 or 3 resp.
	
	return n + 2 - m;
}

} /* namespace lib */
