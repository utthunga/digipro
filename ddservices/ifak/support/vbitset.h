
/* 
 * $Id: vbitset.h,v 1.8 2009/11/19 14:37:17 fna Exp $
 *
 * Copyright (C) 2003 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_vbitset_h
#define _lib_vbitset_h

#include "lib_config.h"

#ifdef _MSC_VER
# pragma warning (disable: 4786)
#endif

// C stdlib
#include <stdlib.h>

// C++ stdlib

// own lib
#include "sys_types.h"
#include "stream.h"


namespace lib
{

/**
 * Variable length bitset.
 * This implement an variable length bitset. It behave like a builtin type.
 */
class WINDLL_LIB vbitset
{
private:
	unsigned long *data; // 0 is the least significant word.
	size_t bits, words;
	
	unsigned long &getword(size_t);
	unsigned long  getword(size_t) const;
	
	unsigned long &hiword(void);
	unsigned long  hiword(void) const;
	
	void do_extend(size_t nbits);
	void do_extend(size_t nbits, size_t nwords);
	void do_align(const vbitset &);
	void do_sanitize(void);
	
	void do_and(const vbitset &);
	void do_or(const vbitset &);
	void do_xor(const vbitset &);
	void do_left_shift(size_t);
	void do_right_shift(size_t);
	void do_set(void);
	void do_reset(void);
	void do_flip(void);
	
	bool is_equal(const vbitset &) const;
	bool is_any(void) const;
	
public:
	~vbitset(void);
	
	vbitset(void);
	vbitset(unsigned long);
	vbitset(const vbitset &);
	
	vbitset& operator=  (const vbitset &);
	
	vbitset& operator&= (const vbitset &);
	vbitset& operator|= (const vbitset &);
	vbitset& operator^= (const vbitset &);
	vbitset& operator<<=(size_t);
	vbitset& operator>>=(size_t);
	
	vbitset& operator|= (sys::uint64);
	
	vbitset  operator~  (void) const;
	vbitset  operator<< (size_t) const;
	vbitset  operator>> (size_t) const;
	
	bool operator==(const vbitset &) const;
	bool operator!=(const vbitset &) const;
	
	bool test(size_t) const;
	bool operator[](size_t) const;
	
	vbitset& set(void);
	vbitset& set(size_t, int = 1);
	
	vbitset& reset(void);
	vbitset& reset(size_t);
	
	vbitset& flip(void);
	vbitset& flip(size_t);
	
	bool any(void) const;
	bool none(void) const;
	
	size_t size(void) const;
	
	unsigned long to_ulong(void) const;
	
	void to_buf(unsigned char *buf, size_t len, bool msb_first = true) const;
	void from_buf(const unsigned char *buf, size_t len, bool msb_first = true);
	
	void dump(class stream &f) const;
};

WINDLL_LIB vbitset operator&(const vbitset &, const vbitset &);
WINDLL_LIB vbitset operator|(const vbitset &, const vbitset &);
WINDLL_LIB vbitset operator^(const vbitset &, const vbitset &);

} /* namespace lib */

#endif /* _lib_vbitset_h */
