
/* 
 * $Id: IniFile.cxx,v 1.2 2005/06/23 13:28:52 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// other libs

// own header
#include "IniFile.h"
#include "fstream.h"


namespace lib
{

IniFile::IniFile(void)
{
}

IniFile::IniFile(const std::string &path)
{
	if (!path.empty())
	{
		set_path(path);
		load();
	}
}

void
IniFile::load(void)
{
	assert(!path.empty());

	std::auto_ptr<fstream> f(fstream_open(path, "r"));

	if (!f.get())
		throw std::invalid_argument("No such file or directory");

	IniParser::load(*f, NULL);
}

void
IniFile::save(void)
{
	assert(!path.empty());

	std::auto_ptr<fstream> f(fstream_open(path, "w"));

	if (!f.get())
		throw std::invalid_argument("No such file or directory");

	IniParser::save(*f);
}

} /* namespace Gtklib */
