
/* 
 * $Id: lib.h,v 1.4 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2003 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_lib_h
#define _lib_lib_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header


/**
 * Namespace for the support library.
 * In this namespace are utility classes and functions collected that
 * are often used and not directly related to the concrete
 * implementation.
 *
 * This mainly includes:
 * - The lib::stream abstraction with concrete implementation mappings
 *   to the C FILE interface (lib::fstream) and memory
 *   (lib::mstream) or to null (lib::nstream)
 * - efficient lib::IDENTIFIER management (through the lib::IDENT manager
 *   class)
 * - lib::hashtable and lib::maptable templates
 * - the buffer classes lib::scratch_buf and lib::dyn_buf
 * - the %bitset implementation lib::mybitset and lib::vbitset
 * - unicode character conversion primitives (lib::iso_latin1_to_utf8)
 * .
 */
namespace lib
{

} /* namespace lib */

#endif /* _lib_lib_h */
