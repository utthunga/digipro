
/* 
 * $Id: address_comparator.cxx,v 1.4 2010/03/03 09:36:56 rsc Exp $
 *
 * Copyright (C) 2010 Robert Schneckenhaus <trobt@web.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "bsdsocket.h"
#include "address_comparator.h"


namespace lib
{

address_comparator::address_comparator(void)
: fqdn_("")
, name_("")
, ipv4_("")
{
}

address_comparator::address_comparator(const std::string &addr)
: fqdn_("")
, name_("")
, ipv4_("")
{
	try
	{
		set(addr);
	}
	catch(...)
	{
		/* ignore */
		fqdn_ = "";
		name_ = "";
		ipv4_ = "";
	}
}

address_comparator::~address_comparator(void)
{
}

void
address_comparator::set(const std::string &str)
{
	struct addrinfo *ai = NULL;

	try
	{
		struct addrinfo hints;
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_INET;

		sys::getaddrinfo(str.c_str(), 0, &hints, &ai);
	}
	catch(const std::exception &)
	{
		ai = NULL;
	}

	if (!ai)
	{
		/* no valid ip address or unresolvable hostname */
		/* we treat as a hostname or fqdn */
		if(is_fqdn(str))
			fqdn_ = str;

		name_ = str;

		std::string::size_type i = name_.find_first_of('.');

		if (i != std::string::npos)
			name_.erase(i);

		return ;
	}

	/* set address */
	char ipv4[46];
	sys::inet_ntop(AF_INET, &((struct sockaddr_in*)ai->ai_addr)->sin_addr, ipv4, sizeof(ipv4));
	ipv4_ = ipv4;

	/* set hostname and/or fqdn */
	char hostname[1024];

	try
	{
		sys::getnameinfo(ai->ai_addr,
			ai->ai_addrlen,
			hostname, sizeof(hostname),
			NULL, 0, NI_NAMEREQD);
	}
	catch(const std::exception &)
	{
		/* unresolvable */
		sys::freeaddrinfo(ai);

		return;
	}

	name_ = hostname;

	if (is_fqdn(name_))
		fqdn_ = name_;

	std::string::size_type i = name_.find_first_of('.');

	if (i != std::string::npos)
		name_.erase(i);

	sys::freeaddrinfo(ai);
}

/* helper */
bool
address_comparator::is_fqdn(const std::string &name) const
{
	return (name.find('.') != std::string::npos);
}

std::string
address_comparator::to_string(void) const
{
	if(!fqdn_.empty())
		return fqdn_;

	if(!name_.empty())
		return name_;

	if(!ipv4_.empty())
		return ipv4_;

	return std::string("");
}

} /* namespace lib */
