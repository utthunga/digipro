
/* 
 * $Id: check_fit_float.h,v 1.1 2008/10/13 16:15:23 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_check_fit_float_h
#define _lib_check_fit_float_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header


namespace lib
{

WINDLL_LIB int check_fit_float(double value);

} /* namespace lib */

#endif /* _lib_check_fit_float_h */
