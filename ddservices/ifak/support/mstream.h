
/* 
 * $Id: mstream.h,v 1.12 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for my idea of I/O streams.
 */

#ifndef _lib_mstream_h
#define _lib_mstream_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib
#include <memory>

// own header
#include "dyn_buf.h"
#include "stream.h"


namespace lib
{

/**
 * memory stream.
 */
class WINDLL_LIB mstream : public stream
{
public:
	mstream(void);
	mstream(class dyn_buf &buf);

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &str);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

	virtual int get(void) const;

	class dyn_buf &get_buf(void) const throw() { return buf; }

private:
	std::auto_ptr<class dyn_buf> mybuf;
	class dyn_buf &buf;
	mutable off_t pos;
};

} /* namespace lib */

#endif /* _lib_mstream_h */
