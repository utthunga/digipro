/*
 * $Id: AddressString.h,v 1.1 2008/09/24 12:41:12 mmeier Exp $
 *
 * Copyright (C) 2008 Tomasz Linkowski
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Tomasz Linkowski
 * \remarks Copyright &copy; ifak e.V. Magdeburg
 */

#ifndef _lib_AddressString_h
#define _lib_AddressString_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// other libs

// own libs

// my lib

namespace lib
{

/** A wrapper class for an \c std::string as a network address, e.g.\ "tcp://localhost:1234".
 * It provides some extra functions for conveniently accessing parts of the address,
 * i.e. get_protocol(), get_hostname() and get_port().
 */
class WINDLL_LIB AddressString
{
public:
	/// Constructor: implicit conversion from \c std::string.
	/// \param address Value of the address, e.g. "tcp://localhost:1234".
	AddressString(const std::string& address=""): _address(address) { }
	/// Assignment operator from \c std::string.
	/// \param address Value of the address, e.g. "tcp://localhost:1234".
	AddressString& operator=(const std::string& address);
	/// Implicit conversion to \c std::string.
	/// \return Value of the address as \c std::string, e.g. "tcp://localhost:1234".
	operator std::string() const { return _address; }

	/// Retrieves the protocol from the address. 
	/// \return Protocol as \c std::string, e.g. "tcp" from "tcp://localhost:1234".
	std::string get_protocol() const;
	/// Retrieves the hostname from the address.
	/// \return Hostname as \c std::string, e.g. "localhost" from "tcp://localhost:1234".
	std::string get_hostname() const;
	/// Retrieves the port from the address.
	/// \return Port as \c int, e.g. \c 1234 from "tcp://localhost:1234".
	int get_port() const;
private:
	std::string _address;
};

} /* namespace lib */

#endif /* _lib_AddressString_h */
