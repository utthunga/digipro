
/* 
 * $Id: Logger.cxx,v 1.6 2009/08/03 10:17:58 fna Exp $
 * 
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <string.h>

// C++ stdlib

// other libs

// own header
#include "dyn_buf.h"
#include "port.h"

#include "Logger.h"


namespace lib
{

const std::string Logger::CLASS_INFO     ("Info");
const std::string Logger::CLASS_WARNING  ("Warning");
const std::string Logger::CLASS_ERROR    ("Error");
const std::string Logger::CLASS_TRACE    ("Trace");
const std::string Logger::CLASS_LOWTRACE ("Lowtrace");

Logger::Logger(void)
: out(NULL)
, memory(0)
, memory_limit(16 * 1024)
{
	create_logclass(CLASS_INFO,     "Information messages");
	create_logclass(CLASS_WARNING,  "Warning messages");
	create_logclass(CLASS_ERROR,    "Error messages");
	create_logclass(CLASS_TRACE,    "Trace messages", false);
	create_logclass(CLASS_LOWTRACE, "Lowlevel trace messages", false);
}

void
Logger::create_logclass(const std::string &name, const std::string &descr, bool enabled)
{
	logclasses.insert(infos_t::value_type(name, info_t(descr, enabled)));
}

void
Logger::create_category(const std::string &name, const std::string &descr, bool enabled)
{
	categories.insert(infos_t::value_type(name, info_t(descr, enabled)));
}

void
Logger::set_mem_limit(size_t limit)
{
	if (limit < 4 * 1024)
		limit = 4 * 1024;

	memory_limit = limit;

	while (memory > memory_limit)
		pop_entry_front();
}

Logger::infos_t &
Logger::get_infos(int type)
{
	infos_t *infos = NULL;

	switch (type)
	{
		default:
			/* logic error */ assert(0);
			break;

		case LOGCLASS:
			infos = &logclasses;
			break;

		case CATEGORY:
			infos = &categories;
			break;
	}

	assert(infos);
	return *infos;
}

std::vector<std::string>
Logger::get_names(int type)
{
	const infos_t &infos = get_infos(type);

	std::vector<std::string> ret;
	ret.reserve(infos.size());

	infos_t::const_iterator iter(infos.begin());
	infos_t::const_iterator end(infos.end());

	while (iter != end)
	{
		ret.push_back(iter->first);
		++iter;
	}

	return ret;
}

Logger::info_t
Logger::get_info(int type, const std::string &name)
{
	const infos_t &infos = get_infos(type);

	infos_t::const_iterator iter(infos.find(name));

	if (iter != infos.end())
		return iter->second;

	return info_t(std::string());
}

void
Logger::set_active(int type, const std::string &name, bool active)
{
	infos_t &infos = get_infos(type);

	infos_t::iterator iter(infos.find(name));

	if (iter != infos.end())
		iter->second.enabled = active;
}

void
Logger::log_info(const std::string &cat, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(CLASS_INFO, cat, fmt, args);
	va_end(args);
}

void
Logger::log_warning(const std::string &cat, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(CLASS_WARNING, cat, fmt, args);
	va_end(args);
}

void
Logger::log_error(const std::string &cat, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(CLASS_ERROR, cat, fmt, args);
	va_end(args);
}

void
Logger::log_trace(const std::string &cat, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(CLASS_TRACE, cat, fmt, args);
	va_end(args);
}

void
Logger::log_lowtrace(const std::string &cat, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(CLASS_LOWTRACE, cat, fmt, args);
	va_end(args);
}

void
Logger::log(const std::string &logclass,
	    const std::string &category,
	    const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	log(logclass, category, fmt, args);
	va_end(args);
}

void
Logger::log(const std::string &logclass,
	    const std::string &category,
	    const char *fmt, va_list va_args)
{
	class dyn_buf buf(100);
	buf.vprintf(fmt, va_args);

	log(logclass, category, buf.str());
}

void
Logger::log(const std::string &logclass,
	    const std::string &category,
	    const std::string &message)
{
	struct entry entry;

	entry.recorded = time(NULL);
	entry.logclass = logclasses.find(logclass);
	entry.category = categories.find(category);
	entry.message = message;

	assert(entry.logclass != logclasses.end());

	if (entry.category == categories.end())
	{
		categories.insert(infos_t::value_type(category, info_t(category)));
		entry.category = categories.find(category);
	}

	if (entry.logclass->second.enabled && entry.category->second.enabled)
	{
		push_entry_back(entry);

		while (memory > memory_limit)
			pop_entry_front();

		if (out)
			out->put(pretty_print(entry));
	}
}

std::string
Logger::pretty_print(const struct entry &entry)
{
	std::string ret;

	/* time */
	{
		struct tm tm;

#if _MSC_VER >= 1400
		localtime_r(&entry.recorded, &tm);
#else
		struct tm *tm0 = localtime(&entry.recorded);
		assert(tm0);
		tm = *tm0;
#endif

		char buf[64];
		strftime(buf, sizeof(buf), "%H:%M:%S ", &tm);
		ret += buf;
	}

	ret += '[';
	ret += entry.logclass->first;
	ret += "] ";
	ret += entry.category->first;
	ret += ": ";
	ret += entry.message;

	if (ret[ret.size() - 1] != '\n')
		ret += '\n';

	return ret;
}

void
Logger::push_entry_back(const struct entry &entry)
{
	entries.push_back(entry);

	memory += sizeof(entry);
	memory += entry.message.capacity();
}

void
Logger::pop_entry_front(void)
{
	struct entry entry(entries.front());
	entries.pop_front();

	memory -= sizeof(entry);
	memory -= entry.message.capacity();
}

} /* namespace lib */

