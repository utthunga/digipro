
/* 
 * $Id: address_comparator.h,v 1.2 2010/02/25 17:18:38 rsc Exp $
 *
 * Copyright (C) 2010 Robert Schneckenhaus <trobt@web.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_address_comparator_h
#define _lib_address_comparator_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

/**
 * address comparator.
 */
class WINDLL_LIB address_comparator
{
public:
	~address_comparator(void);

	/** Default constructor. */
	address_comparator(void);

	/**
	 * Constructor.
	 * Auto-detects the meaning of the given
	 * address string (fqdn, hostname or address).
	 */
	address_comparator(const std::string &addr);

private:
	std::string fqdn_;
	std::string name_;
	std::string ipv4_;

	/* helper */
	bool is_fqdn(const std::string &name) const;

public:
	/**
	 * String presentation.
	 * String contains the fqdn if possible.
	 * Else string contains the hostname if possible.
	 * Else string contains the ip address.
	 * String is empty if nothing is defined.
	 */
	std::string to_string(void) const;

	/**
	 * Sets the fully qualified domain name,
	 * hostname or ipv4 address.
	 */
	void set(const std::string &str);

	/**
	 * Set the fully qualified domain name.
	 */
	inline void set_fqdn(const std::string &fqdn) { fqdn_ = fqdn; }

	/**
	 * Set the hostname.
	 */
	inline void set_hostname(const std::string &name) { name_ = name; }

	/**
	 * Set the ipv4 address.
	 */
	inline void set_address(const std::string &addr) { ipv4_ = addr; }

	/**
	 * Get the fully qualified domain name.
	 * May be empty.
	 */
	inline const std::string &fqdn() const { return fqdn_; }

	/**
	 * Get the hostname.
	 * May be empty.
	 */
	inline const std::string &hostname() const { return name_; }

	/**
	 * Get the ipv4 address.
	 * May be empty.
	 */
	inline const std::string &address() const { return ipv4_; }

	/**
	 * Compares the comparator with another comparators.
	 */
	inline bool equal(const address_comparator &c) const
	{
		return ((!fqdn_.empty() && fqdn_ == c.fqdn_) ||
			(!name_.empty() && name_ == c.name_) ||
			ipv4_ == c.ipv4_);
	}

	/**
	 * Compares the comparator with a fqdn, hostname or address string.
	 */
	inline bool equal(const std::string &s) const
	{
		return (!s.empty() && (fqdn_ == s || name_ == s || ipv4_ == s));
	}

	/**
	 * Static comparison function.
	 */
	static bool equal(const std::string &s1, const std::string &s2)
	{
		address_comparator c1(s1);
		address_comparator c2(s2);

		return c1.equal(c2);
	}

public:
	/* operators */

	/**
	 * Compares the comparator with another comparators.
	 */
	inline bool operator==(const address_comparator &c) const
	{
		return equal(c);
	}

	/**
	 * Compares the comparator with another comparators.
	 */
	inline bool operator!=(const address_comparator &c) const
	{
		return !equal(c);
	}

	/**
	 * Compares the comparator with a fqdn, hostname or address string.
	 */
	inline bool operator==(const std::string &s) const
	{
		return equal(s);
	}

	/**
	 * Compares the comparator with a fqdn, hostname or address string.
	 */
	inline bool operator!=(const std::string &s) const
	{
		return !equal(s);
	}

	/**
	 * Compares an address comparator with a fqdn, hostname or address string.
	 */
	inline friend bool operator==(const std::string &s, const address_comparator &c)
	{
		return c.equal(s);
	}

	/**
	 * Compares an address comparator with a fqdn, hostname or address string.
	 */
	inline friend bool operator!=(const std::string &s, const address_comparator &c)
	{
		return !c.equal(s);
	}
};

} /* namespace lib */

#endif /* _lib_address_comparator_h */
