
/* 
 * $Id: file_exist.cxx,v 1.3 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * Check if file exist.
 * Return true if the file with path exist and is readable, false otherwise.
 */
WINDLL_LIB bool
file_exist(const std::string &path)
{
	FILE *f = fopen(path.c_str(), "r");
	bool result = false;

	if (f)
	{
		result = true;
		fclose(f);
	}

	return result;
}

} /* namespace lib */
