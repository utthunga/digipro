
/* 
 * $Id: reuse.cxx,v 1.7 2006/02/01 09:30:18 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <algorithm>

// own libs

// own header
#include "reuse.h"


namespace reuse
{

#if defined(_MSC_VER) && (_MSC_VER < 1300)
WINDLL_LIB struct pos NoPosition;
namespace
{
	class init
	{
	public:
		init(void)
		{
			NoPosition.Line = 0;
			NoPosition.Column = 0;
			NoPosition.File = 0;
			NoPosition.EndColumn = 0;
		}
	};
	static class init init;
}
#endif

/**
 * Fast \f$log_2(x)\f$ implementation.
 * This implement \f$log_2(x)\f$ with fast integer shift operators.
 * \param x with \f$x>=0\f$ and \f$x<2^x\f$
 * \return \f$log_2(x)\f$
 */
WINDLL_LIB int
Log2(unsigned long x) throw()
{
	int y = 0;
	
	if (x >= 65536) { y += 16; x >>= 16; }
	if (x >=   256) { y +=  8; x >>=  8; }
	if (x >=    16) { y +=  4; x >>=  4; }
	if (x >=     4) { y +=  2; x >>=  2; }
	if (x >=     2) { y +=  1; x >>=  1; }
	
	return y;
}

/**
 * Fast \f$2^x\f$ implementation.
 * This implement \f$2^x\f$ with fast integer shift operators. Due to the use of
 * integer this function only work for small x correctly.
 * \param x with \f$x>=0\f$ and \f$x<32\f$
 * \return \f$2^x\f$
 */
WINDLL_LIB unsigned long
Exp2(int x) throw()
{
	unsigned long y = 1;
	
	if (x >= 16) { x -= 16; y <<= 16; }
	if (x >=  8) { x -=  8; y <<=  8; }
	if (x >=  4) { x -=  4; y <<=  4; }
	if (x >=  2) { x -=  2; y <<=  2; }
	if (x >=  1) { x -=  1; y <<=  1; }
	
	return y;
}

const int Set::BitsPerBitset = sizeof(long) * 8;
const int Set::MaskBitsPerBitset = BitsPerBitset - 1;

Set::Set(int MaxSize)
{
	long ElmtCount ;
	
	ElmtCount = MaxSize + BitsPerBitset;
	ElmtCount -= (MaxSize & MaskBitsPerBitset);
	ElmtCount /=  BitsPerBitset;
	
	BitsetPtr = new long[ElmtCount];
	
	MaxElmt = MaxSize;
	LastBitset = ElmtCount - 1;
	
	AssignEmpty();
}

Set::~Set(void)
{
	delete [] BitsetPtr;
}

Set::Set(const Set &) { }
class Set& Set::operator=(const Set &s) { return *this; }

void
Set::Union(const Set &s) throw()
{
	long *b1 = BitsetPtr;
	const long *b2 = s.BitsetPtr;
	int i = LastBitset + 1;
	
	do { *b1++ |= *b2++;} while (--i);
	
	Card = -1;
	FirstElmt = std::min(FirstElmt, s.FirstElmt);
	LastElmt = std::max(LastElmt, s.LastElmt);
}

void
Set::Include(int Elmt) throw()
{
	BitsetPtr [Elmt / BitsPerBitset] |= 1L << (Elmt & MaskBitsPerBitset);
	Card = -1;
	FirstElmt = std::min(FirstElmt, Elmt);
	LastElmt  = std::max(LastElmt , Elmt);
}

void
Set::Exclude(int Elmt) throw()
{
	BitsetPtr [Elmt / BitsPerBitset] &= ~(1L << (Elmt & MaskBitsPerBitset));
	Card = -1;
	
	if (Elmt == FirstElmt && Elmt < MaxElmt)
		FirstElmt++;
	
	if (Elmt == LastElmt && Elmt > 0)
		LastElmt--;
}

int
Set::Minimum(void) throw()
{
	int last = LastElmt;
	int i;
	
	for (i = FirstElmt; i <= last; ++i)
	{
		if (IsElement(i))
		{
			FirstElmt = i;
			return i;
		}
	}
	
	return 0;
}

int
Set::Extract(void) throw()
{
	int i = Minimum();
	Exclude(i);
	return i;
}

bool
Set::IsElement(int Elmt) const throw()
{
	return ((BitsetPtr[(Elmt) / BitsPerBitset] >> ((Elmt) & MaskBitsPerBitset)) & 1);
}

bool
Set::IsEmpty(void) const throw()
{
	if (FirstElmt <= LastElmt)
	{
		int i = LastBitset + 1;
		const long *b = BitsetPtr;
		
		do {
			if (*b++ != 0)
				return false;
		}
		while (--i);
	}
	
	return true;
}

void
Set::AssignEmpty(void) throw()
{
	long *b = BitsetPtr;
	int i = LastBitset + 1;
	
	do { *b++ = 0; } while (--i);
	
	Card = 0;
	FirstElmt = MaxElmt;
	LastElmt  = 0;
}

} /* namespace reuse */
