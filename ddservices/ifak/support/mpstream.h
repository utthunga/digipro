
/*
 * $Id: mpstream.h,v 1.2 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2007-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_mpstream_h
#define _lib_mpstream_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <vector>

// own libs

// own
#include "stream.h"


namespace lib
{

/**
 * Stream multiplexer.
 * This stream dispatch the output operations to the attached stream objects.
 * Input operations are not supported by this stream.
 */
class WINDLL_LIB mpstream : public stream
{
public:
	void attach(stream *);
	void remove(stream *);
	void remove_all(void);

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);
	virtual void flush(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

private:
	std::vector<stream *> streams;
};

} /* namespace lib */

#endif /* _lib_mpstream_h */
