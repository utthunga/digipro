
/* 
 * $Id: IniParser.cxx,v 1.14 2009/07/31 11:21:39 mmeier Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <limits.h>

// C++ stdlib
#include <algorithm>
#include <functional>

// other libs

// my lib

// own header
#include "IniParser.h"
#include "port.h"
#include "string.h"


namespace lib
{

IniParser::IniParser(void)
: modified_(false)
{
}

IniParser::~IniParser(void)
{
}

void
IniParser::set_modified() throw()
{
	modified_ = true;
}

bool
IniParser::check_section(const std::string &section) const
{
	return find_section(section);
}

bool
IniParser::check_key(const std::string &section, const std::string &key) const
{
	sections_ptr sec(find_section(section));

	if (sec)
		return (find_entry(key, sec->entries) != sec->entries.end());

	return false;
}

std::vector<std::string>
IniParser::get_section(const std::string &section) const
{
	sections_ptr sec(find_section(section));
	std::vector<std::string> result;

	if (sec)
	{
		result.resize(sec->entries.size());

		std::transform(sec->entries.begin(), sec->entries.end(),
			       result.begin(),
			       std::mem_fun_ref(&entry::get_key));
	}

	return result;
}

void
IniParser::clear_section(const std::string &section)
{
	sections_ptr sec(find_section(section));

	if (sec)
	{
		if (sec->entries.size())
		{
			sec->entries.clear();
			modified_ = true;

			on_modified(section, std::string());
		}
	}
}

std::string
IniParser::get_key(const std::string &section, const std::string &key, const std::string &def) const
{
	const sections_ptr sec(find_section(section));
	std::string result(def);

	if (sec)
	{
		entries_t::const_iterator i(find_entry(key, sec->entries));

		if (i != sec->entries.end())
			result = i->value;
	}

	return result;
}

void
IniParser::del_key(const std::string &section, const std::string &key)
{
	sections_ptr sec(find_section(section));

	if (sec)
	{
		entries_t::iterator i(sec->entries.begin());

		while (i != sec->entries.end())
		{
			if (*i == key)
			{
				sec->entries.erase(i);
				modified_ = true;

				on_modified(section, key);
				break;
			}
			++i;
		}
	}
}

void
IniParser::set_key(const std::string &section, const std::string &key, const std::string &value, bool quote)
{
	sections_ptr sec(find_section(section));
	bool exist = false;

	if (sec)
	{
		entries_t::iterator i(find_entry(key, sec->entries));

		if (i != sec->entries.end())
		{
			if (i->value != value)
			{
				i->value = value;
				i->quote = quote;
				modified_ = true;

				on_modified(section, key);
			}
			exist = true;
		}
	}

	if (!exist)
	{
		class entry entry;

		entry.key = key;
		entry.value = value;
		entry.quote = quote;

		new_entry(section, entry);
		modified_ = true;

		on_modified(section, key);
	}
}

void
IniParser::set_key(const std::string &section, const std::string &key, int value, const char *fmt)
{
	char buf[32];
	snprintf(buf, sizeof(buf), fmt, value);

	set_key(section, key, std::string(buf));
}

void
IniParser::set_key(const std::string &section, const std::string &key, bool value)
{
	set_key(section, key, std::string(value ? "yes" : "no"));
}

bool
IniParser::get_bool_key(const std::string &section, const std::string &key, bool def) const
{
	std::string str(get_key(section, key));
	bool result = def;

	if (!str.empty())
	{
		const char *c_str = str.c_str();

		if (stricmp(c_str, "true") == 0 || stricmp(c_str, "yes") == 0 || stricmp(c_str, "1") == 0)
			result = true;
		else if (stricmp(c_str, "false") == 0 || stricmp(c_str, "no") == 0 || stricmp(c_str, "0") == 0)
			result = false;
	}

	return result;
}

int
IniParser::get_int_key(const std::string &section, const std::string &key, int def) const
{
	std::string str(get_key(section, key));
	int result = def;

	if (!str.empty())
	{
		const char *c_str = str.c_str();
		char *end;

		errno = 0;
		long lval = strtol(c_str, &end, 0);
		bool error = false;

		if (c_str[0] == '\0' || *end != '\0')
			/* not a number */
			error = true;

		if (errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN))
			/* out of range */
			error = true;

		if (!error)
			result = lval;
	}

	return result;
}

void
IniParser::load(const class stream &f, class stream *log)
{
	/* empty existing things */
	sections_pos.clear();
	sections.clear();
	last_comments.clear();
	modified_ = false;

	parse(f, log);
}

void
IniParser::save(class stream &f)
{
	std::for_each(sections_pos.begin(), sections_pos.end(), save_section(f));

	f.put(last_comments);

	/* after save we are not modified */
	modified_ = false;
}

void
IniParser::on_modified(const std::string &section, const std::string &key)
{
}

void
IniParser::save_section::operator()(const sections_ptr &sec) const
{
	const class section &section = *sec;

	if (section.comments.empty())
		f.put('\n');
	else
		f.put(section.comments);

	f.put('['); f.put(section.name); f.put(']'); f.put('\n');

	std::for_each(section.entries.begin(), section.entries.end(), save_entry(f));
}

void
IniParser::save_entry::operator()(const class entry &entry) const
{
	f.put(entry.comments);
	f.put(entry.key);

	if (entry.value.length())
	{
		f.put(" = ");

		if (entry.quote)
		{
			f.put('"');

			for (size_t i = 0, i_max = entry.value.size(); i < i_max; ++i)
			{
				char c = entry.value[i];

				if (c == '\\' || c == '"')
					f.put('\\');

				f.put(c);
			}

			f.put('"');
		}
		else
			f.put(entry.value);
	}

	f.put(';');
	f.put(entry.comment);
	f.put('\n');
}

IniParser::sections_ptr
IniParser::find_section(const std::string &name)
{
	sections_t::iterator sec(sections.find(name));

	if (sec != sections.end())
		return sec->second;

	return sections_ptr();
}

const IniParser::sections_ptr
IniParser::find_section(const std::string &name) const
{
	sections_t::const_iterator sec(sections.find(name));

	if (sec != sections.end())
		return sec->second;

	return sections_ptr();
}

IniParser::entries_t::iterator
IniParser::find_entry(const std::string &key, entries_t &entries)
{
	return std::find(entries.begin(), entries.end(), key);
}

IniParser::entries_t::const_iterator
IniParser::find_entry(const std::string &key, const entries_t &entries) const
{
	return std::find(entries.begin(), entries.end(), key);
}

void
IniParser::new_section(const std::string &name, const std::string &comments)
{
	sections_ptr section(new class section);

	section->comments = comments;
	section->name = name;

	sections_pos.push_back(section);
	sections[name] = section;
}

void
IniParser::new_entry(const std::string &section, const entry &entry)
{
	sections_ptr sec(find_section(section));

	if (!sec)
	{
		new_section(section);
		sec = find_section(section);
	}
	assert(sec);

	sec->entries.push_back(entry);
}

void
IniParser::parse(const class stream &f, class stream *log)
{
	std::string section;

	c = f.get();

	for (;;)
	{
		std::string line, skipped;

		next_valid_line(f, line, skipped);

		if (!line.length())
		{
			last_comments = skipped;
			break;
		}

		if (line[0] == '[')
		{
			std::string::size_type pos;

			pos = line.find(']');

			if (pos != std::string::npos)
			{
				section = line.substr(1, pos - 1);

				if (!find_section(section))
					new_section(section, skipped);
				else if (log)
					log->printf("IniParser: section [%s] already defined!\n", section.c_str());
			}
			else if (log)
				log->printf("IniParser: invalid section name: %s\n", line.c_str());
		}
		else
		{
			std::string::size_type pos;
			bool error = true;

			pos = line.find('=');

			if (pos != std::string::npos)
			{
				const std::string::size_type length = line.length();
				std::string::size_type i;

				i = pos - 1;
				while (isspace(line[i]))
					--i;

				class entry entry;

				entry.key = line.substr(0, i+1);
				entry.quote = false;

				i = pos + 1;
				while (i < length && isspace(line[i]))
					++i;

				std::string::size_type start = i;

				if (i < length)
				{
					char c = line[i];

					if (c == '"')
					{
						/* read string */
						std::string value;
						bool quote = false;

						entry.quote = true;
						i = start + 1;

						while (i < length)
						{
							c = line[i];

							if (c == '\\')
							{
								if (quote)
								{
									value += c;
									quote = false;
								}
								else
									quote = true;
							}
							else if (c == '"')
							{
								if (quote)
								{
									value += c;
									quote = false;
								}
								else
									break;
							}
							else
							{
								if (quote)
								{
									value += '\\';
									quote = false;
								}
								value += c;
							}

							++i;
						}

						i = line.find(';', i);

						if (i != std::string::npos)
						{
							entry.comments = skipped;
							entry.value = value;
							if (i+1 < line.length())
								entry.comment = line.substr(i+1);

							new_entry(section, entry);
							error = false;
						}
					}
					else
					{
						i = line.find(';', start);

						if (i != std::string::npos)
						{
							entry.comments = skipped;
							entry.value = line.substr(start, i - start);
							if (i+1 < line.length())
								entry.comment = line.substr(i+1);

							new_entry(section, entry);
							error = false;
						}
					}
				}
			}
			else
			{
				std::string::size_type i;

				i = line.find(';');

				if (i != std::string::npos)
				{
					class entry entry;

					entry.comments = skipped;
					entry.key = line.substr(0, i);
					entry.quote = false;
					if (i+1 < line.length())
						entry.comment = line.substr(i+1);

					new_entry(section, entry);
					error = false;
				}
			}

			if (error && log)
				log->printf("IniParser: invalid key/value line (%s)\n", line.c_str());
		}
	}
}

void
IniParser::next_valid_line(const class stream &f, std::string &line, std::string &skipped)
{
	line.clear(); line.reserve(100);
	skipped.clear(); skipped.reserve(100);

	skip_space(f, skipped);

	while (c != EOF)
	{
		if (check_eol(f))
			break;

		line.push_back(c);
		c = f.get();
	}
}

void
IniParser::skip_space(const class stream &f, std::string &skipped)
{
	while (c != EOF)
	{
		while (c == '#' || c == ';')
			skip_line(f, skipped);

		if (!isspace(c))
			break;

		skipped.push_back(c);
		c = f.get();
	}
}

void
IniParser::skip_line(const class stream &f, std::string &skipped)
{
	while (c != EOF)
	{
		if (check_eol(f))
		{
			skipped.push_back('\n');
			break;
		}

		skipped.push_back(c);
		c = f.get();
	}
}

bool
IniParser::check_eol(const class stream &f)
{
	if (c == '\r' || c == '\n')
	{
		int c1 = c;

		c = f.get();

		if (c1 == '\r' && c == '\n')
			c = f.get();

		return true;
	}

	return false;
}

} /* namespace lib */
