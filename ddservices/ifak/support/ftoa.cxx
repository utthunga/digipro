
/* 
 * $Id: ftoa.cxx,v 1.3 2008/10/21 15:37:49 fna Exp $
 *
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <float.h>
#include <math.h>

// C++ stdlib

// own header
#include "port.h"
#include "string.h"


namespace lib
{

WINDLL_LIB std::string
ftoa(const char *fmt, double value)
{
	assert(fmt);

#ifdef WIN32
	switch (_fpclass(value))
	{
		case _FPCLASS_SNAN:
		case _FPCLASS_QNAN:
			return std::string("nan");
		case _FPCLASS_NINF:
			return std::string("-inf");
		case _FPCLASS_PINF:
			return std::string("inf");
	}
#else
	if (isnan(value))
		return std::string("nan");

	int inf = isinf(value);

	if (inf < 0)
		return std::string("-inf");
	if (inf > 0)
		return std::string("inf");
#endif

	char buf[128];
	snprintf(buf, sizeof(buf), fmt, value);

	return std::string(buf);
}

} /* namespace lib */
