
/* 
 * $Id: IniFile.h,v 1.3 2006/11/06 14:25:17 fna Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_IniFile_h
#define _lib_IniFile_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// other libs

// own header
#include "IniParser.h"


namespace lib
{

class WINDLL_LIB IniFile : public IniParser
{
public:
	IniFile(void);
	IniFile(const std::string &);

	void set_path(const std::string &p) { path = p; set_modified(); }
	std::string get_path(void) const { return path; }

	void load(void);
	void save(void);

private:
	std::string path;
};

} /* namespace lib */

#endif /* _lib_IniFile_h */
