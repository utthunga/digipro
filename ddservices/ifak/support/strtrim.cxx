
/* 
 * $Id: strtrim.cxx,v 1.2 2008/10/21 15:36:50 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

WINDLL_LIB std::string
strtrim(const std::string &str, const std::string &what)
{
	std::string::size_type pos1 = str.find_first_not_of(what);
	std::string::size_type pos2 = str.find_last_not_of(what);
	std::string ret;

	if (pos1 != std::string::npos)
		ret = str.substr(pos1, pos2 - pos1 + 1);

	return ret;
}

} /* namespace lib */
