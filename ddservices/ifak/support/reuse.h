
/* 
 * $Id: reuse.h,v 1.6 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */
 
/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the cocktail reuse compatibility lib.
 */

#ifndef _lib_reuse_h
#define _lib_reuse_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header


/**
 * Namespace for the cocktail reuse library.
 * This namespace include all things that are directly or indirectly derived
 * or rewritten from the cocktail reuse library. These things are currently
 * used by the cocktail generated scanner and parser.
 * \note In the meantime I removed and rewritten most of the stuff. Most
 * things are gone into the parser and scanner skeletons due to better
 * integration and handling.
 */
namespace reuse
{

/**
 * Position type.
 * This is the cocktail idea about a position marker. This is
 * necessary for compatibility as the scanner and parser generator
 * depend hardcoded on this type.
 */
struct WINDLL_LIB pos
{
	unsigned long Line;
	unsigned long Column;
	unsigned long File;
	unsigned long EndColumn;
};

/**
 * The NoPosition marker.
 * This position object represent the invalid or unknown position.
 * It's used for the error case or as default initialization.
 */
#if defined(_MSC_VER) && (_MSC_VER < 1300)
WINDLL_LIB extern struct pos NoPosition;
#else
const struct pos NoPosition = { 0, 0, 0, 0 };
#endif

/**
 * The tPosition typedef.
 * This exist only to make the parser generator lark and the lark
 * preprocessor happy. They aren't able to handle 'struct name' in
 * various places of the input language.
 */
typedef struct pos tPosition;

/**
 * \name Helper functions.
 * Some nice helpers used by the scanner generator in buffer
 * calculation.
 */
//! \{
WINDLL_LIB int Log2(unsigned long x) throw();
WINDLL_LIB unsigned long Exp2(int x) throw();
//! \}

/**
 * Set support class.
 * This class implements abstract Set handling. It's used by
 * generated parser for error recovering.
 */
class WINDLL_LIB Set
{
public:
	Set(int MaxSize);
	~Set(void);
	
	void	AssignEmpty	(void) throw();
	bool	IsEmpty		(void) const throw();
	bool	IsElement	(int Elmt) const throw();
	void	Include		(int Elmt) throw();
	void	Exclude		(int Elmt) throw();
	void	Union		(const Set &s) throw();
	int	Minimum		(void) throw();
	int	Extract		(void) throw();
	
private:
	/* not implemented and don't implicitly define them */
	Set(const Set &);
	class Set& operator=(const Set &);
	
	static const int BitsPerBitset;
	static const int MaskBitsPerBitset;
	
	int	MaxElmt;
	int	LastBitset;
	long	*BitsetPtr;
	int	Card;
	int	FirstElmt;
	int	LastElmt;
};

} /* namespace reuse */

#endif /* _lib_reuse_h */
