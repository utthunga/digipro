
/* 
 * $Id: check_fit_float.cxx,v 1.3 2008/10/14 15:30:54 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <float.h>
#include <math.h>

// C++ stdlib

// own header
#include "check_fit_float.h"
#include "port.h"


namespace lib
{

/**
 * Check if a double fit into a float.
 * Check if a double fit into a float without loss of precision
 * and so on.
 * \param value the double value to be checked
 * \return 0 if the value fits, 1 if the value fits with loss
 *         of precision, -1 otherwise.
 */
WINDLL_LIB int
check_fit_float(double value)
{
	if (value == 0 || isinf(value) || isnan(value))
		return 0;

	double absval = fabs(value);

	if (absval > FLT_MAX)
		return -1;

	if (absval < FLT_MIN)
		return -1;

	double precision = absval - static_cast<float>(absval);
	precision = fabs(precision);

	if (precision > FLT_EPSILON)
		return 1;

	return 0;
}

} /* namespace lib */
