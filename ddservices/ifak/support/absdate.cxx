
/* 
 * Copyright (C) 2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdlib.h>

// C++ stdlib
#include <cmath>
#include <iomanip>
#include <sstream>
#include <stdexcept>

// own header
#include "absdate.h"


namespace lib
{
absdate::absdate(const absdate& other)
{
	value = other.value;
	cached = other.cached;
	cached_display_value = other.cached_display_value;
}

absdate::absdate(unsigned int year, unsigned int month, unsigned int day,
		unsigned int hour, unsigned int minute, unsigned int second)
: value(0.0)
, cached(false)
{
	assign_date(year, month, day, hour, minute, second);
}

absdate::absdate(const struct tm &t)
{
	assign_date(1900+t.tm_year, t.tm_mon+1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
}

int
absdate::sum_days(unsigned int month, unsigned int year)
{
	if (month > 11)
		throw std::invalid_argument("month > 11");

	unsigned int length = 0;

	for (size_t i = 0; i < month; ++i)
		length += mlength[i];

	if (month > 1 && is_leap_year(year))
		length += 1; // leap day in feb

	return length;
}

unsigned int
absdate::get_month(unsigned int days, unsigned int year)
{
	if (days > real_ylength(year))
		throw std::invalid_argument("too many days");

	unsigned int month = 0;
	unsigned int sdays = 0;

	if (days == real_ylength(year))
		return 11;

	while (month < 12)
	{
		sdays += mlength[month];

		if (is_leap_year(year) && month == 1)
			++sdays;

		if (sdays > days)
			return month;

		++month;
	}

	throw std::invalid_argument("...");
}

bool
absdate::is_leap_year(unsigned int year)
{
	if (!(year % 4))
	{
		if (year <= 1582)
			return true;

		if (year % 100)
			return true;
		else if (!(year % 400))
			return true;
	}

	return false;
}

unsigned int
absdate::real_ylength(unsigned int year)
{
	if (year == 1582)
		return 365-10; // 5.10. - 14.10. are missing

	if (is_leap_year(year))
		return ylength+1;
	else
		return ylength;
}

std::string
absdate::get_month_name(unsigned int nr)
{
	if (month_names.empty())
	{
		month_names[0]  = "Jan";
		month_names[1]  = "Feb";
		month_names[2]  = "Mar";
		month_names[3]  = "Apr";
		month_names[4]  = "May";
		month_names[5]  = "Jun";
		month_names[6]  = "Jul";
		month_names[7]  = "Aug";
		month_names[8]  = "Sep";
		month_names[9]  = "Oct";
		month_names[10] = "Nov";
		month_names[11] = "Dec";
	}

	return month_names[nr];
}

void
absdate::assign_date(unsigned int year, unsigned int month, unsigned int day,
		unsigned int hour, unsigned int minute, unsigned int second)
{
	unsigned int i = 1;

	/* sum up days for specified years */
	while (i < year)
	{
		value += real_ylength(i);
		++i;
	}

	i = 0;

	while (month && i < month-1)
	{
		value += mlength[i];

		if (i == 1 && is_leap_year(year))
			++value;

		++i;
	}

	value += day ? day-1 : 0;

	double t = (double) second + (double) minute * 60.0 + (double) hour * 3600.0;

	value += t / 86400.0;
}

void
absdate::compute(void)
{
	double val = value;
	std::ostringstream out;
	unsigned long days = 0;
	year = 1;

	if (value > year_1990)
	{
		days = year_1990;
		year = 1990;
	}

	while (days < value)
	{
		if (days + real_ylength(year) <= value)
			days += real_ylength(year++);
		else
			break;
	}

	val -= days;
	
	month = get_month((unsigned int) val, year);

	val -= sum_days(month, year);

	day = floor(val);

	val -= day;

	if (year == 1582)
		if (day > 3 && day < 14)
			day += 10;

	seconds = floor(86400.0 * val + 0.5);

	hours = floor(seconds / 3600.0);
	seconds -= hours * 3600.0;

	minutes = floor(seconds / 60.0);
	seconds -= minutes * 60.0;

	out << std::setw(2) << day+1 << ". " << get_month_name(month) << " " << std::setw(4) << year;
	out << " " << std::setw(2) << std::setfill('0') << hours << ":";
	out << std::setw(2) << std::setfill('0') << minutes << ":";
	out << std::setw(2) << std::setfill('0') << seconds;

	cached_display_value = out.str();
	cached = true;
}

std::ostream& operator<<(std::ostream &str, absdate &d)
{
	/* update cached value */
	if (!d.cached)
		d.compute();

	str << d.cached_display_value;
	return str;
}

unsigned int absdate::ylength = 365;
unsigned int absdate::mlength[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
std::map<unsigned int, std::string> absdate::month_names;
unsigned int absdate::year_1990 = 726469;

} /* namespace lib */
