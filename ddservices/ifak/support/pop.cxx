
/* 
 * $Id: pop.cxx,v 1.3 2009/11/19 14:37:17 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "pop.h"


namespace lib
{

/**
 * Population count.
 * This function count the number of 1-bits of the 32bit argument.
 * Branch-free and should compile to 21 basic RISC instructions.
 * \note Taken from Hacker's Delight; Henry S. Warren; Addison-Wesley
 */
WINDLL_LIB sys::int32
pop(sys::int32 x)
{
	x = x - ((x >> 1) & 0x55555555);
	x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x = (x + (x >> 4)) & 0x0f0f0f0f;
	x = x + (x >> 8);
	x = x + (x >> 16);
	
	return x & 0x0000003f;
}

} /* namespace lib */
