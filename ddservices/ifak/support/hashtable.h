
/* 
 * $Id: hashtable.h,v 1.8 2008/05/15 11:30:51 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a general purpose hash template class.
 */

#ifndef _lib_hashtable_h
#define _lib_hashtable_h

#include "lib_config.h"

#ifdef _MSC_VER
# pragma warning (disable: 4786)
#endif

// C stdlib
#include <string_edd.h>

// C++ stdlib
#include <vector>
#include <list>
#include <memory>

// own lib
#include "stream.h"


namespace lib
{

/** Compare function template used by the hashtable template class. */
template<typename Key> struct WINDLL_LIB equalfunc { };

/** Hash function template used by the hashtable template class. */
template<typename Key> struct WINDLL_LIB hashfunc { };

/** Dump function template used by the hashtable template class. */
template<typename Key> struct WINDLL_LIB dumpfunc { };

/* some defaults
 */

/* C string keys */

/**
 * Compare function template specialization used by the hashtable template
 * class.
 * This is a specialization for C style strings of the Compare function
 * template equalfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB equalfunc<const char *>
{
	bool operator()(const char *__x, const char *__y) const
	{ return !strcmp(__x, __y); }
};
/**
 * Hash function template specialization used by the hashtable template class.
 * This is a specialization for C style strings of the Hash function template
 * hashfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB hashfunc<const char *>
{
	unsigned long operator()(const char *__s, const unsigned long __HASHBITS) const
	{
		register unsigned long hashval = 0;
		
		while (*__s)
		{
			hashval = ((hashval << 5) - hashval) + *__s;
			__s++;
		}
		
		hashval ^= (hashval >> __HASHBITS) ^ (hashval >> (__HASHBITS << 1));
		
		return hashval;
	}
};
/**
 * Dump function template specialization used by the hastable template class.
 * This is a specialization for C style strings of the Dump function template
 * dumpfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB dumpfunc<const char *>
{
	void operator()(class stream &f, const char *__s) const
	{ f.printf("%s", __s); }
};

/* skalar type keys */

#define proto(type) \
template<> \
struct WINDLL_LIB equalfunc<type> \
{ \
	bool operator()(const type __x, const type __y) const \
	{ return (__x == __y); } \
}

proto(char);
proto(unsigned char);
proto(short);
proto(unsigned short);
proto(int);
proto(unsigned int);
proto(long);
proto(unsigned long);

#undef proto

#define proto(type) \
template<> \
struct WINDLL_LIB hashfunc<type> \
{ \
	unsigned long operator()(type __x, const unsigned long) const \
	{ unsigned long __y(__x); return __y; } \
}

proto(char);
proto(unsigned char);
proto(short);
proto(unsigned short);
proto(int);
proto(unsigned int);
proto(long);
proto(unsigned long);

#undef proto

#define proto(type) \
template<> \
struct WINDLL_LIB dumpfunc<type> \
{ \
	void operator()(class stream &f, type __x) const \
	{ long __y(__x); f.printf("%li", __y); } \
}

proto(char);
proto(unsigned char);
proto(short);
proto(unsigned short);
proto(int);
proto(unsigned int);
proto(long);
proto(unsigned long);

#undef proto


/**
 * The hashtable template class.
 * This template implements an efficient general purpose hashing
 * algorithm. It work with a fixed size hash table and linked lists
 * for collision handling (open hashing).
 */
template <
	typename Key,
	typename T,
	typename EqualFunc = equalfunc<Key>,
	typename HashFunc = hashfunc<Key>,
	typename DumpFunc = dumpfunc<Key>
>
class WINDLL_LIB hashtable
{
private:
	const unsigned long HASHBITS;
	const unsigned long HASHSIZE;
	const unsigned long HASHMASK;
	
	/**
	 * Internal data holder for an hash object.
	 * This struct save the hash object and the corresponding key. It
	 * realize the linked list for collision handling too.
	 */
	struct map
	{
		map *next;
		const Key key;
		T value;
		
		map(map *n, const Key k, T v) : next(n), key(k), value(v) { }
	};
	
	/** The main hash table. */
	std::vector<struct map *> table;
	
	/** The concrete compare function. */
	EqualFunc equal;
	/** The concrete hash function. */
	HashFunc hasher;
	/** The concrete dumping function. */
	DumpFunc dumper;
	
#ifdef STATISTIC
	/** Statistic data. */
	//!{
	mutable unsigned long entries;
	mutable unsigned long collisions;
	mutable unsigned long mem;
	//!}
# define HASHTABLE_STATISTIC_CONSTRUCT , entries(0), collisions(0), mem(0)
#else
# define HASHTABLE_STATISTIC_CONSTRUCT
#endif

	template <
		typename ValueType,
		typename IteratorType
	>
	class iterator_base
	{
	public:
		typedef iterator_base<ValueType, IteratorType> Self;

		iterator_base(const Self &i)
		{
			it = i.it;
			v_it = i.v_it;
			end = i.end;
		}

		bool operator==(const Self &i)
		{
			return i.it == it;
		}

		bool operator!=(const Self &i)
		{
			return i.it != it;
		}

		Self operator++(void)
		{
			if (!it)
			{
				return Self();
			}

			if (it->next)
			{
				it = it->next;
			}
			else
			{
				set_next();	
			}
			return *this;

		}

		ValueType operator*(void) const
		{
			return it->key;
		}

		ValueType *operator->(void) const
		{
			return &it->key;
		}
		

	private:
		friend class hashtable<Key, T,  EqualFunc, HashFunc, DumpFunc>;

		iterator_base(void)
			: it(NULL)
		{
		}

		iterator_base(IteratorType start, IteratorType end)
		{
			v_it = start;
			this->end = end;

			if (!*v_it)
				set_next();
			else
				it = *v_it;
		}

		void set_next(void)
		{
			while (true)
			{
				++v_it;
				if (v_it != end) 
				{
					if (!*v_it)
						continue;

					it = *v_it;
					break;
				}
				else
				{
					it = NULL;
					break;
				}
			}
		}

		IteratorType v_it;
		IteratorType end;
		struct map *it;
	};


public:

	typedef iterator_base<const Key, typename std::vector<struct map *>::const_iterator> const_iterator;
	typedef iterator_base<const Key, typename std::vector<struct map *>::iterator> iterator;

	/** default constructor with a 10bit sized hash table. */
	hashtable(void)
	: HASHBITS(10UL), HASHSIZE(1UL << HASHBITS), HASHMASK(HASHSIZE - 1UL),
	  table(HASHSIZE) HASHTABLE_STATISTIC_CONSTRUCT { }
	/**
	 * special constructor with a user defined sized hash table.
	 * \note The argument is the hash table size in bits. The value cannot
	 * be greater than 32.
	 */
	explicit hashtable(unsigned long bits)
	: HASHBITS(bits), HASHSIZE(1UL << HASHBITS), HASHMASK(HASHSIZE - 1UL),
	  table(HASHSIZE) HASHTABLE_STATISTIC_CONSTRUCT { }
	
	~hashtable(void) { clear(); }
	
	void clear(void)
	{
		for (size_t i = 0, i_max = table.size(); i < i_max; ++i)
		{
			map *m = table[i];
			table[i] = NULL;
			
			while (m)
			{
				map *next = m->next;
				delete m;
				m = next;
			}
		}
	}
	
	bool empty(void) const
	{
		for (size_t i = 0, i_max = table.size(); i < i_max; ++i)
		{
			if (table[i])
				return false;
		}
		return true;
	}
	
	void stats(unsigned long data[5]) const throw()
	{
		data[0] = HASHBITS;
		data[1] = HASHSIZE;
#ifdef STATISTIC
		data[2] = entries;
		data[3] = collisions;
		data[4] = mem;
#else
		data[2] = 0;
		data[3] = 0;
		data[4] = 0;
#endif
	}
	
	/** Calculate the hash value for the key. */
	inline unsigned long hash(const Key key) const
	{
		return (hasher(key, HASHBITS) & HASHMASK);
	}
	
	/**
	 * Lookup the hashtable for an hash object corresponding to this key.
	 * \param key The key where the hashtable is searched for.
	 * \return The corresponding hash object or 0.
	 */
	T lookup(const Key key) const
	{
		const unsigned long hashval = hash(key);
		map *m;
		
		for (m = table[hashval]; m != 0; m = m->next)
			if (equal(m->key, key))
				return m->value;
		
		return 0;
	}
	
	/** Same as lookup(). \see lookup */
	T operator[](const Key key) const { return lookup(key); }
	
	/**
	 * Install a new hash object for the specified key.
	 * \param key The key for the hash object.
	 * \param value The hash object.
	 * \note It's assumed that there isn't anything already installed for
	 * the specified key. The caller is responsible to verify this
	 * condition. The behaviour of this class is undefined in case that
	 * these condition is not true.
	 */
	void install(const Key key, const T value)
	{
		const unsigned long hashval = hash(key);
		map *m;
		
		m = new map(table[hashval], key, value);
		
#ifdef STATISTIC
		entries++;
		if (table[hashval])
			collisions++;
		
		mem += sizeof(map);
#endif
		
		table[hashval] = m;
	}
	
	/**
	 * Delete the hash object for the specified key.
	 * \param key The key of the hash object that should be deleted. If
	 * there is no such key with an assoziated hash object nothing is
	 * done.
	 */
	void remove(const Key key)
	{
		const unsigned long hashval = hash(key);
		map **temp = &table[hashval];
		
		while (*temp)
		{
			if (equal((*temp)->key, key))
			{
				map *m = *temp;
				
				*temp = m->next;
				m->next = 0;
				
				delete m;
				
#ifdef STATISTIC
				entries--;
				if (table[hashval])
					collisions--;
				
				mem -= sizeof(map);
#endif
				break;
			}
			
			/* next */
			temp = &(*temp)->next;
		}
	}

	/**
	 * Iterator methods for iterator and const_iterator.
	 */
	iterator 
	begin(void)
	{ 
		return iterator(table.begin(), table.end());
	}

	iterator 
	end(void)
	{ 
		return iterator(); 
	} 
	const_iterator 
	begin(void) const
	{ 
		return const_iterator(table.begin(), table.end());
	}

	const_iterator 
	end(void) const
	{ 
		return const_iterator(); 
	} 

	/**
	 * Dump out the hash table to specified stream.
	 * This go over the complete array and print out the Keys of all hash
	 * table entries (through the dumpfunc helper class).
	 */
	void dump(class stream &f) const
	{
		f.printf("Dumping hashtable:\n");
		
		for (size_t i = 0, i_max = table.size(); i < i_max; ++i)
		{
			map *m = table[i];
			
			if (m)
			{
				f.printf("%u: ", (unsigned int) i);
				while (m)
				{
					dumper(f, m->key);
					if (m->next)
						f.printf(" -> ");
					
					m = m->next;
				}
				f.printf("\n");
			}
		}
		
		f.printf("... done\n");
	}
};

} /* namespace lib */

#endif /* _lib_hashtable_h */
