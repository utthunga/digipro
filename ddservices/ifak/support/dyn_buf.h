
/* 
 * $Id: dyn_buf.h,v 1.16 2009/05/07 12:00:04 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a dynamic buffer.
 */

#ifndef _lib_dyn_buf_h
#define _lib_dyn_buf_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>
#include <stdarg.h>
#include <string_edd.h>

// C++ stdlib
#include <string>

// own header
#include "vsnprintf.h"


namespace lib
{

/**
 * dynamic buffer.
 */
class WINDLL_LIB dyn_buf
{
public:
	~dyn_buf(void);

	/** Default constructor. */
	dyn_buf(void);

	/** Normal constructor. */
	dyn_buf(size_t s);

	/** Copy constructor. */
	dyn_buf(const dyn_buf &t);

private:
	void resize(size_t min = 0);

public:
	/**
	 * Empty the buffer.
	 */
	void clear(void) throw() { ptr = buf; space = size - 1; }

	/**
	 * Format print.
	 * The same as libc vprintf.
	 * \note The string is appended to the existing content of the buffer.
	 */
	void vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);
	/**
	 * Format print.
	 * The same as libc printf.
	 * \note The string is appended to the existing content of the buffer.
	 */
	void printf(const char *fmt, ...) CHECK_PRINTF(2, 3);
	/**
	 * Append buffer.
	 * Append len characters from buffer str.
	 */
	void put(const char *str, size_t len);
	/**
	 * Append C string.
	 * Append the C string str.
	 */
	void put(const char *str) { put(str, strlen(str)); }
	/**
	 * Append C++ string.
	 * Append the C++ string str.
	 */
	void put(const std::string &str) { put(str.data(), str.length()); }
	/**
	 * Append character.
	 */
	void put(const char c) { if (!space) resize(); *ptr++ = c; space--; }
	/**
	 * Remove characters.
	 * This remove up to len characters from this buffer (but only
	 * length() characters maximum).
	 */
	void del(size_t len);

	/**
	 * Get C string.
	 * Return C style string (readonly) of the encapsuled string buffer.
	 */
	const char *c_str(void) const throw() { *ptr = '\0'; return buf; }
	/**
	 * Get std::string.
	 * Return a std::string object if the encapsuled string buffer.
	 */
	std::string str(void) const;
	/**
	 * Get pointer to the data.
	 * \note It's not a C string, there is no terminating 0.
	 */
	const char *data(void) const throw() { return buf; }
	/**
	 * Get data length.
	 * Return the number of used bytes.
	 */
	size_t length(void) const throw() { return (ptr - buf); }
	/**
	 * Check if the buffer is empty.
	 * Return 'true' if the buffer is empty.
	 */
	bool empty(void) const throw() { return (ptr == buf); }

	     operator const char *() const { return c_str(); }

	/** Assignment. */
        class dyn_buf& operator=(const dyn_buf &t);

	void operator= (const char *str) { clear(); put(str); }
	void operator+=(const char *str) { put(str); }
	void operator+=(const char c) { put(c); }

	void operator= (const std::string &str) { clear(); put(str.data(), str.length()); }
	void operator+=(const std::string &str) { put(str.data(), str.length()); }

	/** Comparison. */
	bool operator!=(const dyn_buf &) const;

private:
	size_t size, space;
	char *buf, *ptr;
};

} /* namespace lib */

#endif /* _lib_dyn_buf_h */
