
/* 
 * $Id: auto_array.h,v 1.7 2009/07/15 16:30:28 fna Exp $
 *
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for an automatic allocating/deallocating array.
 */

#ifndef _lib_auto_array_h
#define _lib_auto_array_h

#include "lib_config.h"

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "port.h"

// own header


namespace lib
{

template<typename T>
class auto_array
{
public:
	auto_array(void) : mysize(0), buf(NULL) { }

	explicit auto_array(size_t s) { init(s); }

	explicit auto_array(const T *buf, size_t buflen) { assert((buf && buflen) || (!buf && !buflen)); copy(buf, buflen); }

	/* copy constructor */
	auto_array(const auto_array &a) { copy(a.buf, a.mysize); }

	/* assignment construtor */
	class auto_array& operator=(const auto_array &a)
	{
		if (this != &a)
		{
			delete [] buf;
			copy(a.buf, a.mysize);
		}
		return *this;
	}

	~auto_array(void) { delete [] buf; }

	void reset(size_t s)
	{
		delete [] buf;
		init(s);
	}

	void resize(size_t s)
	{
		if (s > mysize)
		{
			T *new_buf = new T[s];

			if (mysize)
				memcpy(new_buf, buf, sizeof(*buf) * mysize);

			bzero(new_buf+mysize, sizeof(*buf) * (s - mysize));

			delete [] buf;

			mysize = s;
			buf = new_buf;
		}
		else if (s < mysize)
		{
			T *old_buf = buf;

			mysize = s;

			if (mysize)
			{
				buf = new T[mysize];
				memcpy(buf, old_buf, sizeof(*buf) * mysize);
			}
			else
				buf = NULL;

			delete [] old_buf;
		}
	}

	T *data(void) const throw() { return buf; }
	size_t size(void) const throw() { return mysize; }

	T& operator[](size_t index) const
	{
		if (index < mysize)
			return buf[index];

		throw std::out_of_range("invalid index");
	}

protected:
	void init(size_t s)
	{
		mysize = s;

		if (mysize)
		{
			buf = new T[mysize];
			bzero(buf, sizeof(*buf) * mysize);
		}
		else
			buf = NULL;
	}
	void copy(const T *buffer, size_t s)
	{
		mysize = s;

		if (mysize)
		{
			buf = new T[mysize];
			memcpy(buf, buffer, sizeof(*buf) * mysize);
		}
		else
			buf = NULL;
	}

	size_t mysize;
	T *buf;
};

} /* namespace lib */

#endif /* _lib_auto_array_h */
