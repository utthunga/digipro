
/* 
 * $Id: file_exist.cxx,v 1.3 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2010 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>
#include <stdarg.h>

// C++ stdlib

// own header
#include "vsnprintf.h"


namespace lib
{

/**
 * Implement UNIX like behavior for vsnprintf
 * Returns the number of bytes which would be written, or -1 on error.
 */
#if defined(__MINGW32__)
int
_vsnprintf_s(char * buffer, size_t sizeOfBuffer, size_t count, const char *format, va_list argptr)
{
	return _vsnprintf(buffer, sizeOfBuffer, format, argptr);
}

#define _TRUNCATE 0
#endif

WINDLL_LIB int
vsnprintf(char *ptr, size_t count, const char *fmt, va_list args)
{
	if (!fmt)
		return -1;

	size_t size = _vscprintf(fmt, args);

	if (size + 1 >= count)
		return size + 1;

	return _vsnprintf_s(ptr, count, _TRUNCATE, fmt, args);
}

} /* namespace lib */
