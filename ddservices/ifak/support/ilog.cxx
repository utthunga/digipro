
/* 
 * $Id: ilog.cxx,v 1.5 2009/11/19 14:37:16 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <math.h>

// C++ stdlib

// own header
#include "ilog.h"
#include "pop.h"


namespace lib
{

/**
 * Integer logarithm base 2.
 * \note Taken from Hacker's Delight; Henry S. Warren; Addison-Wesley
 */
WINDLL_LIB int
ilog2(sys::uint32 x)
{
	x = x | (x >>  1);
	x = x | (x >>  2);
	x = x | (x >>  4);
	x = x | (x >>  8);
	x = x | (x >> 16);
	
	return pop(x) - 1;
}

WINDLL_LIB int
ilog2(sys::uint64 value)
{
	int exponent;

	double d_value = (double) value;
	frexp(d_value, &exponent);

	return exponent - 1;
} 

/**
 * Integer logarithm base 10.
 * A table search is quite reasonable todo the job. This could be a binary
 * search but because the table is small and in many applications x is
 * usually small, a simple linear search is probably best.
 * Should be compiled to execute in about 9+4[log10x] instructions. Thus,
 * it executes in 5 to 45 instructions, with perhaps 13 (for 10 <= x <= 99)
 * being typical.
 * \note Taken from Hacker's Delight; Henry S. Warren; Addison-Wesley
 */
WINDLL_LIB int
ilog10(sys::uint32 x)
{
	static sys::uint32 table[11] =
	{ 0, 9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999,
	  0xffffffff
	};
	
	int i;
	
	for (i = -1; ; ++i)
		if (x <= table[i+1])
			break;
	
	return i;
}

} /* namespace lib */
