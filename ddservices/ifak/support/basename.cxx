
/* 
 * $Id: basename.cxx,v 1.6 2008/03/24 09:27:35 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * The C++ version of basename(3).
 * 
 * The basename() function takes the pathname pointed to by path and returns
 * a string to the final component of the pathname, deleting any trailing
 * `/' characters.
 * 
 * If path consists entirely of `/' characters, basename() returns the
 * string ``/''.
 * 
 * If path is an empty string, basename() returns the string ``.''.
 */
WINDLL_LIB std::string
basename(const std::string &path)
{
	if (path.empty())
		/* handle empty string */
		return std::string(".");

	std::string::size_type end = path.find_last_not_of(PATHSEPARATORS);

	if (end == std::string::npos)
		/* we consist entirely of '/' */
		return std::string(DEF_PATHSEPARATOR);

	std::string::size_type pos = path.find_last_of(PATHSEPARATORS, end);

	/* calculate start */
	if (pos == std::string::npos)
		pos = 0;
	else
		pos++;

	/* and copy result */
	return path.substr(pos);
}

} /* namespace lib */
