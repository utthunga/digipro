
/*
 * $Id: mpstream.cxx,v 1.1 2008/10/03 22:32:58 fna Exp $
 * 
 * Copyright (C) 2007-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <stdexcept>

// own libs

// own
#include "mpstream.h"

#undef max
#undef min


namespace lib
{

void
mpstream::attach(stream *f)
{
	if (f)
	{
		bool found = false;

		for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		{
			if (streams[i] == f)
			{
				found = true;
				break;
			}
		}

		if (!found)
			streams.push_back(f);
	}
}

void
mpstream::remove(stream *f)
{
	if (f)
	{
		std::vector<stream *>::iterator iter(streams.begin());
		std::vector<stream *>::const_iterator end(streams.end());

		while (iter != end)
		{
			if (f == *iter)
			{
				streams.erase(iter);
				break;
			}

			++iter;
		}
	}
}

void
mpstream::remove_all(void)
{
	streams.clear();
}

size_t
mpstream::read(void *ptr, size_t size, size_t nmemb) const
{
	throw std::logic_error("multiplex stream: read operations not supported!");
}

size_t
mpstream::write(const void *ptr, size_t size, size_t nmemb)
{
	size_t ret = 0;

	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		ret = std::max(streams[i]->write(ptr, size, nmemb), ret);

	return ret;
}

bool
mpstream::eof(void) const
{
	throw std::logic_error("multiplex stream: read operations not supported!");
}

void
mpstream::clear(void)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->clear();
}

void
mpstream::flush(void)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->flush();
}

int
mpstream::vprintf(const char *fmt, va_list args)
{
	int ret = 0;

	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		ret = std::max(streams[i]->vprintf(fmt, args), ret);

	return ret;
}

void
mpstream::put(const std::string &str)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->put(str);
}

void
mpstream::put(const char *str, size_t len)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->put(str, len);
}

void
mpstream::put(const char *str)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->put(str);
}

void
mpstream::put(const char c)
{
	for (size_t i = 0, i_max = streams.size(); i < i_max; ++i)
		streams[i]->put(c);
}

} /* namespace dome */
