
/* 
 * $Id: pca_conv.h,v 1.5 2006/02/13 16:09:52 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Packed ASCII conversion routines.
 */

#ifndef _lib_pca_conv_h
#define _lib_pca_conv_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB std::string pca2str(const char *src, size_t length);
WINDLL_LIB std::string str2pca(const char *src, size_t length);

inline std::string pca2str(const std::string &src) { return pca2str(src.c_str(), src.length()); }
inline std::string str2pca(const std::string &src) { return str2pca(src.c_str(), src.length()); }

} /* namespace lib */

#endif /* _lib_pca_conv_h */
