
/* 
 * $Id: resolve_hostname.h,v 1.3 2010/02/09 17:52:52 rsc Exp $
 *
 * Copyright (C) 2010 Robert Schneckenhaus <trobt@web.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_resolve_hostname_h
#define _lib_resolve_hostname_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB std::string resolve_hostname(const std::string &);

} /* namespace lib */

#endif /* _lib_resolve_hostname_h */
