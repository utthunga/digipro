
/* 
 * $Id: tmpfile.h,v 1.3 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_tmpfile_h
#define _lib_tmpfile_h

#include "lib_config.h"

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header


namespace lib
{

WINDLL_LIB FILE *tmpfile(void);

} /* namespace lib */

#endif /* _lib_tmpfile_h */
