
/* 
 * $Id: smart_ptr.h,v 1.10 2006/05/21 16:01:08 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a smart pointer template class.
 */

#ifndef _lib_smart_ptr_h
#define _lib_smart_ptr_h

#include "lib_config.h"

// C stdlib
#include <assert.h>
#include <stdlib.h>

// C++ stdlib
#include <stdexcept>
#include <memory>

// own libs

// own header


namespace lib
{

/**
 * smart pointer.
 */
template<typename T>
class smart_ptr
{
public:
	~smart_ptr() { unref(); }

	explicit
	smart_ptr(T *p = NULL)
	: ptr(p), refs(ptr ? new size_t(1) : NULL)
	{
	}

	explicit
	smart_ptr(std::auto_ptr<T> p)
	: ptr(p.get()), refs(ptr ? new size_t(1) : NULL)
	{
		p.release();
	}

	/* Copy constructor. */
	smart_ptr(const smart_ptr<T> &p)
	: ptr(p.ptr), refs(p.refs)
	{
		if (refs)
			(*refs)++;
	}

	/* Assignment constructor. */
	class smart_ptr<T>& operator=(const smart_ptr<T> &p)
	{
		if (p.refs)
			(*p.refs)++;

		unref();

		ptr = p.ptr;
		refs = p.refs;

		return *this;
	}

	T& operator*()
	{
		if (!ptr)
			throw std::logic_error("dereferencing NULL");

		return *ptr;
	}
	const T& operator*() const
	{
		if (!ptr)
			throw std::logic_error("dereferencing NULL");

		return *ptr;
	}

	T* operator->()
	{
		if (!ptr)
			throw std::logic_error("dereferencing NULL");

		return ptr;
	}
	const T* operator->() const
	{
		if (!ptr)
			throw std::logic_error("dereferencing NULL");

		return ptr;
	}

	T* get() const { return ptr; }

	operator bool() const throw() { return ptr != NULL; }

	bool operator==(const smart_ptr<T> &p) { return (ptr == p.ptr); }
	bool operator!=(const smart_ptr<T> &p) { return (ptr != p.ptr); }

	/* fulfill strict weak ordering */
	bool operator< (const smart_ptr<T> &p) const throw() { return (ptr <  p.ptr); }

protected:
	T *ptr;
	size_t *refs;

	void unref()
	{
		if (refs && --(*refs) == 0)
		{
			delete refs;
			delete ptr;
		}
	}
};

} /* namespace lib */

#endif /* _lib_smart_ptr_h */
