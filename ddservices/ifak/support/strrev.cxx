
/* 
 * $Id: strrev.cxx,v 1.2 2005/09/19 15:20:58 fna Exp $
 *
 * Copyright (C) 2003-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <algorithm>
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

WINDLL_LIB std::string
strrev(std::string &str)
{
	std::reverse(str.begin(), str.end());
	return str;
}

} /* namespace lib */
