
/* 
 * $Id: strtoxx.cxx,v 1.7 2009/11/19 14:59:55 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <limits.h>

// C++ stdlib

// own header
#include "strtoxx.h"


namespace lib
{

WINDLL_LIB int
bin_ascii_to_int(sys::int32 &v, const char *s, size_t len)
{
	const sys::int32 cutoff = UINT_MAX / 2;
//	const sys::int32 cutlim = UINT_MAX % 2;
	
	const char *end = s + len;
	
	assert(s[0] == '0' && (s[1] == 'b' || s[1] == 'B'));
	s += 2;
	
	v = 0;
	while (s < end)
	{
		if (v > cutoff || v == cutoff)
		{
			v = INT_MAX;
			return ERANGE;
		}
		
		v <<= 1; /* i *= 2; */
		
		if (*s++ == '1')
			v++;
	}
	
	return 0;
}

WINDLL_LIB int
hex_ascii_to_int(sys::int32 &v, const char *s, size_t len)
{
	const sys::int32 cutoff = UINT_MAX / 16;
	const sys::int32 cutlim = UINT_MAX % 16;
	
	const char *end = s + len;
	
	assert(s[0] == '0' && (s[1] == 'x' || s[1] == 'X'));
	s += 2;
	
	v = 0;
	while (s < end)
	{
		int c = *s++;
		
		if (c > 64)
		{
			if (c > 96) c -= 87;
			else c -= 55;
		}
		else c -= 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = INT_MAX;
			return ERANGE;
		}
		
		v <<= 4; /* i *= 16; */
		v += c;
	}
	
	return 0;
}

WINDLL_LIB int
dec_ascii_to_int(sys::int32 &v, const char *s, size_t len)
{
	const sys::int32 cutoff = UINT_MAX / 10;
	const sys::int32 cutlim = UINT_MAX % 10;
	
	const char *end = s + len;
	
	v = *s++ - 48;
	while (s < end)
	{
		int c = *s++ - 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = INT_MAX;
			return ERANGE;
		}
		
		v *= 10;
		v += c;
	}
	
	return 0;
}

WINDLL_LIB int
oct_ascii_to_int(sys::int32 &v, const char *s, size_t len)
{
	const sys::int32 cutoff = UINT_MAX / 8;
	const sys::int32 cutlim = UINT_MAX % 8;
	
	const char *end = s + len;
	
	assert(s[0] == '0');
	s++;
	
	v = 0;
	while (s < end)
	{
		int c = *s++ - 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = INT_MAX;
			return ERANGE;
		}
		
		v <<= 3; /* i *= 8; */
		v += c;
	}
	
	return 0;
}


WINDLL_LIB int
bin_ascii_to_int64(sys::int64 &v, const char *s, size_t len)
{
	const sys::int64 cutoff = ULLONG_MAX / 2;
//	const sys::int64 cutlim = ULLONG_MAX % 2;
	
	const char *end = s + len;
	
	assert(s[0] == '0' && (s[1] == 'b' || s[1] == 'B'));
	s += 2;
	
	v = 0;
	while (s < end)
	{
		if (v > cutoff || v == cutoff)
		{
			v = LLONG_MAX;
			return ERANGE;
		}
		
		v <<= 1; /* i *= 2; */
		
		if (*s++ == '1')
			v++;
	}
	
	return 0;
}

WINDLL_LIB int
hex_ascii_to_int64(sys::int64 &v, const char *s, size_t len)
{
	const sys::int64 cutoff = ULLONG_MAX / 16;
	const sys::int64 cutlim = ULLONG_MAX % 16;
	
	const char *end = s + len;
	
	assert(s[0] == '0' && (s[1] == 'x' || s[1] == 'X'));
	s += 2;
	
	v = 0;
	while (s < end)
	{
		int c = *s++;
		
		if (c > 64)
		{
			if (c > 96) c -= 87;
			else c -= 55;
		}
		else c -= 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = LLONG_MAX;
			return ERANGE;
		}
		
		v <<= 4; /* i *= 16; */
		v += c;
	}
	
	return 0;
}

WINDLL_LIB int
dec_ascii_to_int64(sys::int64 &v, const char *s, size_t len)
{
	const sys::int64 cutoff = ULLONG_MAX / 10;
	const sys::int64 cutlim = ULLONG_MAX % 10;
	
	const char *end = s + len;
	
	v = *s++ - 48;
	while (s < end)
	{
		int c = *s++ - 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = LLONG_MAX;
			return ERANGE;
		}
		
		v *= 10;
		v += c;
	}
	
	return 0;
}

WINDLL_LIB int
oct_ascii_to_int64(sys::int64 &v, const char *s, size_t len)
{
	const sys::int64 cutoff = ULLONG_MAX / 8;
	const sys::int64 cutlim = ULLONG_MAX % 8;
	
	const char *end = s + len;
	
	assert(s[0] == '0');
	s++;
	
	v = 0;
	while (s < end)
	{
		int c = *s++ - 48;
		
		if (v > cutoff || (v == cutoff && c > cutlim))
		{
			v = LLONG_MAX;
			return ERANGE;
		}
		
		v <<= 3; /* i *= 8; */
		v += c;
	}
	
	return 0;
}


WINDLL_LIB int
bin_ascii_to_vbitset(class vbitset &v, const char *s, size_t len)
{
	const char *end = s + len;
	
	assert(s[0] == '0' && (s[1] == 'b' || s[1] == 'B'));
	s += 2;
	
	v = vbitset(1);
	if (*s++ == '1')
		v |= 1;
	
	while (s < end)
	{
		v <<= 1; /* i *= 2; */
		
		if (*s++ == '1')
			v |= 1;
	}
	
	return 0;
}

WINDLL_LIB int
hex_ascii_to_vbitset(class vbitset &v, const char *s, size_t len)
{
	const char *end = s + len;
	int c;
	
	assert(s[0] == '0' && (s[1] == 'x' || s[1] == 'X'));
	s += 2;
	
	c = *s++;
	if (c > 64)
	{
		if (c > 96) c -= 87;
		else c -= 55;
	}
	else c -= 48;
	
	v = vbitset(4);
	v |= c;
	
	while (s < end)
	{
		c = *s++;
		
		if (c > 64)
		{
			if (c > 96) c -= 87;
			else c -= 55;
		}
		else c -= 48;
		
		v <<= 4; /* i *= 16; */
		v |= c;
	}
	
	return 0;
}

static char *
strrev(char *s)
{
	char *q = s;
 
	if (*q)
	{
		char *p = q;

		while (*++q)
			;

		while (--q > p)
		{
			char c;

			c = *q;
			*q = *p;
			*p++ = c;
		}
	}

	return s;
}

WINDLL_LIB int
dec_ascii_to_vbitset(class vbitset &v, const char *s, size_t len)
{
	char *final; int final_index;
	const char *end;
	char *buf, *src, *dst;
	bool run = true;
	
	final = new char[len * 8];
	assert(final);
	
	src = buf = new char[len];
	assert(src);
	
	end = s + len;
	while (s < end)
		*src++ = *s++ - 48;
	
	src = dst = buf;
	end = buf + len;
	
	final_index = 0;
	while (run)
	{
		bool first = true;
		int rest;
		
		rest = 0;
		while (src < end)
		{
			int c;
			
			c = rest * 10;
			c += *src++;
			
			if (c > 1)
			{
				*dst++ = c / 2;
				rest = c % 2;
				first = false;
			}
			else
			{
				rest = c;
				
				if (!first)
					*dst++ = 0;
			}
		}
		
		if (rest)
			final[final_index++] = '1';
		else
			final[final_index++] = '0';
		
		end = dst;
		src = dst = buf;
		
		if (end == buf)
			run = false;
	}
	
	final[final_index] = '\0';
	strrev(final);
	
	v.reset();
	src = final;
	for (;;)
	{
		if (*src++ == '1')
			v |= 1;
		else
			v |= 0;
		
		if (!*src)
			break;
		
		v <<= 1;
	}
	
	delete [] buf;
	delete [] final;
	
	return 0;
}

WINDLL_LIB int
oct_ascii_to_vbitset(class vbitset &v, const char *s, size_t len)
{
	const char *end = s + len;
	
	assert(s[0] == '0');
	s++;
	
	v = vbitset(3);
	v |= (*s++ - 48);
	
	while (s < end)
	{
		int c = *s++ - 48;
		
		v <<= 3; /* i *= 8; */
		v |= c;
	}
	
	return 0;
}

} /* namespace lib */
