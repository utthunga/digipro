
/*
 * $Id: lib_config.h,v 1.9 2009/05/07 21:16:15 fna Exp $
 * 
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_config_h
#define _lib_config_h

#ifdef _MSC_VER

/* export support lib as DLL */
# ifdef LIB_AS_DLL
/* DLL import/export defines */
#  ifdef LIB_DLL
#   define WINDLL_LIB __declspec(dllexport)
#  else
#   define WINDLL_LIB __declspec(dllimport)
#  endif
# endif

/* ignore stupid non-dll interface warnings on it's own STL */
# pragma warning (disable: 4251)
/* ignore name for debugger to long warnings on it's own STL */
# pragma warning (disable: 4786)

# ifdef __cplusplus
/* force throwing exceptions if out of memory */
#  include <new>
# endif

#endif /* _MSC_VER */

#ifndef WINDLL_LIB
# define WINDLL_LIB
#endif

#ifdef __GNUC__
# define CHECK_PRINTF(index, start) __attribute__ ((format (printf, index, start)))
# define CHECK_STRFTIME(index) __attribute__ ((format (strftime, index, 0)))
#endif

#ifndef CHECK_PRINTF
# define CHECK_PRINTF(index, start)
#endif

#ifndef CHECK_STRFTIME
# define CHECK_STRFTIME(index)
#endif

#endif /* _lib_config_h */
