

/* 
 * Copyright (C) 2011 Robert Schneckenhaus <robert.schneckenhaus@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "find_end.h"


namespace lib
{

/* finds the end character 'endc' while regarding counts of the beginning character which
 * is the character at the given offset 'off'. escaped versions of endc are ignored (e.g. \\").
 */
WINDLL_LIB size_t
find_end(const std::string &str, size_t off, char endc)
{
	size_t n = 1;
	char begc = str[off];

	if (begc == endc)
	{
		for (;;)
		{
			size_t pos = str.find_first_of(endc, off+1);

			if (pos == std::string::npos || pos == 0)
				return pos;

			if (str[pos-1] != '\\')
				return pos;

			off = pos;
		}
	}

	for (size_t i=off+1; i<str.length(); ++i)
	{
	if (str[i] == begc && (i>0 && str[i-1] != '\\'))++n;
		else if (str[i] == endc && (i>0 && str[i-1] != '\\'))
		{
			if((--n) == 0)
				return i;
		}
	}

	return std::string::npos;
}

WINDLL_LIB size_t
find_closing_round_bracket(const std::string &str, size_t off)
{
	return find_end(str, off, ')');
}

WINDLL_LIB size_t
find_closing_curly_bracket(const std::string &str, size_t off)
{
	return find_end(str, off, '}');
}

WINDLL_LIB size_t
find_closing_square_bracket(const std::string &str, size_t off)
{
	return find_end(str, off, ']');
}

} /* namespace lib */
