
/* 
 * $Id: ilog.h,v 1.5 2009/11/19 14:37:16 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_ilog_h
#define _lib_ilog_h

#include "lib_config.h"

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "sys_types.h"


namespace lib
{

WINDLL_LIB int ilog2(sys::uint32);
WINDLL_LIB int ilog2(sys::uint64);

WINDLL_LIB int ilog10(sys::uint32);

} /* namespace lib */

#endif /* _lib_ilog_h */
