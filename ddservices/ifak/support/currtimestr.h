
/* 
 * $Id: currtimestr.h,v 1.2 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2006-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Time helper.
 */

#ifndef _lib_currtimestr_h
#define _lib_currtimestr_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own libs

// own header


namespace lib
{

WINDLL_LIB std::string currtimestr(const char *fmt = "%c") CHECK_STRFTIME(1);

} /* namespace lib */

#endif /* _lib_currtimestr_h */
