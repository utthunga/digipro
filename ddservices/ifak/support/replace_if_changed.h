
/* 
 * $Id: replace_if_changed.h,v 1.1 2008/03/24 11:57:34 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_replace_if_changed_h
#define _lib_replace_if_changed_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB bool replace_if_changed(const std::string &, const std::string &, int);

} /* namespace lib */

#endif /* _lib_replace_if_changed_h */
