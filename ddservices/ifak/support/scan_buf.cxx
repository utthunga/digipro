
/* 
 * $Id: scan_buf.cxx,v 1.9 2008/10/08 10:15:55 fna Exp $
 *
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "scan_buf.h"


namespace lib
{

void
scan_buf_pool::clear(void)
{
	for (std::list<char *>::iterator it = cache.begin(); it != cache.end(); ++it)
	{
		delete [] *it; 
	}

	cache.clear();
}

void
scan_buf::resize(size_t wanted)
{

	if (pool)
		pool->remove(ptr);

	const unsigned long clen = len();
	unsigned long nplen = plen << 1;
	char *nptr;
	
	while (plen+wanted >= nplen)
		nplen <<= 1;
	
	nptr = new char[nplen];
	
	memcpy(nptr, ptr, clen);
	xptr = nptr + clen;
	
	delete [] ptr;
	ptr = nptr;
	
	freespace = nplen - clen;
	plen = nplen;

	if (pool)
		pool->insert(ptr);
	
	assert(freespace > wanted);
}

#if defined(_MSC_VER) && (_MSC_VER < 1300)
WINDLL_LIB struct scan_buf NoScanBuf;
namespace
{
	class init
	{
	public:
		init(void)
		{
			NoScanBuf.release();
		}
	};
	static class init init;
}
#endif

} /* namespace lib */
