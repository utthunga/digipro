
/* 
 * $Id: nstream.cxx,v 1.8 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2003 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib

// own header
#include "nstream.h"


namespace lib
{

size_t
nstream::read(void *ptr, size_t size, size_t nmemb) const
{
	off_t todo = size * nmemb;

	memset(ptr, 0, todo);

	return todo;
}
size_t
nstream::write(const void *ptr, size_t size, size_t nmemb)
{
	off_t todo = size * nmemb;

	return todo;
}

bool
nstream::eof(void) const
{
	return false;
}
void
nstream::clear(void)
{
}

int
nstream::vprintf(const char *fmt, va_list args)
{
	return 0;
}

void
nstream::put(const std::string &)
{
}
void
nstream::put(const char *str, size_t len)
{
}
void
nstream::put(const char *str)
{
}
void
nstream::put(const char c)
{
}

int
nstream::get(void) const
{
	return 0;
}

} /* namespace lib */
