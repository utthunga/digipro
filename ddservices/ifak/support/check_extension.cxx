
/* 
 * $Id: check_extension.cxx,v 1.3 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * Compare extension.
 * Check if the file given by path have the same extension
 * as the ext argument. Upper/lowercase don't matter (case insensitive
 * compare).
 */
WINDLL_LIB bool
check_extension(const std::string &path, const std::string &ext)
{
	bool ret = false;

	std::string name(lib::basename(path));
	std::string::size_type pos = name.rfind('.');

	if (pos != std::string::npos)
	{
		std::string::size_type size = name.size();
		const char *pext = ext.c_str();

		while (++pos < size && *pext)
		{
			if (tolower(name[pos]) != tolower(*pext++))
				break;
		}

		if (pos == size && !*pext)
			ret = true;
	}

	return ret;
}

} /* namespace lib */
