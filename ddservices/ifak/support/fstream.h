
/* 
 * $Id: fstream.h,v 1.10 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for my idea of I/O streams.
 */

#ifndef _lib_fstream_h
#define _lib_fstream_h

#include "lib_config.h"

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "stream.h"


namespace lib
{

/**
 * file stream.
 */
class WINDLL_LIB fstream : public stream
{
public:
	enum close_on_destruct { auto_close, dont_close };

	fstream(FILE *f, enum close_on_destruct = auto_close);
	virtual ~fstream(void);

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);
	virtual void flush(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &str);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

	virtual int get(void) const;

private:
	FILE *file;
	const enum close_on_destruct close;
};

WINDLL_LIB class fstream * fstream_open(const char *path, const char *mode);
WINDLL_LIB class fstream * fstream_open(const std::string &path, const char *mode);

} /* namespace lib */

#endif /* _lib_fstream_h */
