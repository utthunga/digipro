
/* 
 * $Id: check_date.cxx,v 1.6 2008/05/15 11:30:51 fna Exp $
 *
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>
#include <time.h>

// C++ stdlib
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

/**
 * Check date.
 * Check if the specified day/month/year combination is a valid date.
 * \param day 1..31
 * \param month 1..12
 * \param year Year in Gregorian calendar.
 * \return true if the date is valid, false otherwise.
 */
WINDLL_LIB bool
check_date(int day, int month, int year)
{
	if (day < 1 || day > 31 || month < 1 || month > 12)
		return false;

	if (year < 1970 || year > 2037)
		return true;

#ifdef WIN32
	/* special check
	 * mktime under WIN32 assume localtime and try to convert to
	 * UTC; this won't work with dates before and exactly 1.1.1970.
	 */
	if (year == 1970 && month == 1 && day == 1)
		return true;
#endif

	struct tm src_tm;
	memset(&src_tm, 0, sizeof(src_tm));

	src_tm.tm_mday = day;
	src_tm.tm_mon = (month - 1);
	src_tm.tm_year = (year - 1900);

	struct tm dst_tm;
	memcpy(&dst_tm, &src_tm, sizeof(dst_tm));

	if (mktime(&dst_tm) == (time_t)-1) 
		 return false;

	return ((src_tm.tm_mday == dst_tm.tm_mday) &&
		(src_tm.tm_mon  == dst_tm.tm_mon)  &&
		(src_tm.tm_year == dst_tm.tm_year));
}

} /* namespace lib */
