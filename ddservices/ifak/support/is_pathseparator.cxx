
/* 
 * $Id: is_pathseparator.cxx,v 1.1 2008/10/11 23:46:49 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

WINDLL_LIB bool
is_pathseparator(char c)
{
	std::string seps;
	seps = PATHSEPARATORS;

	return (seps.find(c) != std::string::npos);
}

} /* namespace lib */
