
/* 
 * $Id: tmpfile.cxx,v 1.4 2006/06/02 19:20:47 fna Exp $
 *
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdlib.h>

// C++ stdlib

// own header
#include "tmpfile.h"

#ifdef WIN32
# include <windows.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <io.h>
# include <stdio.h>
# include <share.h>
#endif


namespace lib
{

#ifdef WIN32
WINDLL_LIB FILE *
tmpfile(void)
{
	FILE *out = NULL;
	char tmpDir[_MAX_PATH+1];
	long r;

	r = GetTempPath(sizeof(tmpDir), tmpDir);
	if (r && r < sizeof(tmpDir))
	{
		char *name;

		name = _tempnam(tmpDir, "tmp");
		if (name)
		{
			int fd;

#if _MSC_VER < 1400
			fd = _sopen(name, _O_CREAT|_O_EXCL|_O_RDWR|_O_BINARY|_O_TEMPORARY,
				    _SH_DENYNO, _S_IREAD | _S_IWRITE);

			if (fd >= 0)
#else
			if (_sopen_s(&fd, name, _O_CREAT|_O_EXCL|_O_RDWR|_O_BINARY|_O_TEMPORARY,
				    _SH_DENYNO, _S_IREAD | _S_IWRITE) == 0)
#endif
				out = _fdopen(fd, "w+b");

			free(name);
		}
	}

	if (!out)
	{
#if _MSC_VER < 1400
		out = ::tmpfile();
#else
		if (::tmpfile_s(&out) != 0)
			out = NULL;
#endif
	}

	return out;
}
#else
WINDLL_LIB FILE *
tmpfile(void)
{
	return ::tmpfile();
}
#endif

} /* namespace lib */
