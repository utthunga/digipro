
/* 
 * $Id: mem_fun.h,v 1.8 2007/01/22 12:14:31 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Method callback slots.
 */

#ifndef _lib_mem_fun_h
#define _lib_mem_fun_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header


namespace lib
{

template<typename t_ret, class t_obj>
class WINDLL_LIB bounded_mem_fun0
{
public:
	typedef t_ret (t_obj::*t_method)(void);

	bounded_mem_fun0(t_obj &obj, t_method method)
	: myobj(&obj), mymethod(method) {}

	t_ret operator()(void)
	{ return (myobj->*mymethod)(); }

private:
	t_obj *myobj;
	t_method mymethod;
};

WINDLL_LIB
template<typename t_ret, class t_obj, class t_obj2>
inline bounded_mem_fun0<t_ret, t_obj>
mem_fun(t_obj &obj, t_ret (t_obj2::*method)(void))
{
	return bounded_mem_fun0<t_ret, t_obj>(obj, method);
}

template<typename t_ret, class t_obj, class t_arg1>
class WINDLL_LIB const_bounded_mem_fun1
{
public:
	typedef t_ret (t_obj::*t_method)(t_arg1) const;

	const_bounded_mem_fun1(const t_obj &obj, t_method method)
	: myobj(&obj), mymethod(method) {}

	t_ret operator()(const t_arg1 &a1) const
	{ return (myobj->*mymethod)(a1); }

private:
	const t_obj *myobj;
	t_method mymethod;
};

WINDLL_LIB
template<typename t_ret, class t_obj, class t_obj2, class t_arg1>
inline const_bounded_mem_fun1<t_ret, t_obj, t_arg1>
mem_fun(t_obj &obj, t_ret (t_obj2::*method)(t_arg1) const)
{
	return const_bounded_mem_fun1<t_ret, t_obj, t_arg1>(obj, method);
}

} /* namespace lib */

#endif /* _lib_mem_fun_h */
