
/* 
 * $Id: scratch_buf.cxx,v 1.8 2006/06/01 21:44:04 fna Exp $
 *
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib

// own libs

// own header
#include "port.h"
#include "scratch_buf.h"


namespace lib
{

scratch_buf::scratch_buf(size_t s)
: size(s)
{
	buf = new char[size];
	clear();
}

scratch_buf::scratch_buf(const scratch_buf &b)
: size(b.size), space(b.space)
{
	long len = size - space;
	
	buf = new char[size];
	ptr = buf + len;
	
	assert(len == (b.ptr - b.buf));
	
	memcpy(buf, b.buf, len);
}

class scratch_buf &
scratch_buf::operator=(const scratch_buf &b)
{
	if (this != &b)
	{
		long len;
		
		delete [] buf;

		size = b.size;
		space = b.space;
		len = size - space;
		
		buf = new char[size];
		ptr = buf + len;
		
		assert(len == (b.ptr - b.buf));
		
		memcpy(buf, b.buf, len);
	}
	
	return *this;
}

void
scratch_buf::printf(const char *fmt, ...) throw()
{
	va_list args;
	int ret;
	
	va_start(args, fmt);
	ret = lib::vsnprintf(ptr, space, fmt, args);
	va_end(args);
	
	if (ret < 0 || static_cast<size_t>(ret) > space)
		ret = space;
	
	ptr += ret;
	space -= ret;
}

void
scratch_buf::put(const char *str, size_t len) throw()
{
	len = std::min(len, space);
	
	memcpy(ptr, str, len);
	
	ptr += len;
	space -= len;
}

void
scratch_buf::del(size_t len) throw()
{
	size_t used = ptr - buf;
	
	len = std::min(len, used);
	
	space += len;
	ptr -= len;
}

} /* namespace lib */
