
/* 
 * $Id: lib_internal.h,v 1.1 2006/01/30 09:27:00 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_internal_h
#define _lib_internal_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header


#ifdef WIN32
# define PATHSEPARATORS "/\\"
# define DEF_PATHSEPARATOR "\\"
#else
# define PATHSEPARATORS '/'
# define DEF_PATHSEPARATOR "/"
#endif

namespace lib
{

} /* namespace lib */

#endif /* _lib_internal_h */
