
/* 
 * Copyright (C) 2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_absdate_h
#define _lib_absdate_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <map>

// own header


namespace lib
{

class absdate
{
public:
	/* constructors */
	absdate(void) : value(0.0), cached(false) { }
	absdate(double v) : value(v), cached(false) { }
	absdate(const absdate& other);
	absdate(unsigned int year, unsigned int month, unsigned int day,
		unsigned int hour = 0, unsigned int minute = 0, unsigned int second = 0);
	absdate(const struct tm &t);


	/* stream operator */

	friend std::ostream& operator<<(std::ostream &str, absdate &d);

	/* operators */

	absdate&	operator+(const absdate& other) { cached = false; value += other.value; return *this; }
	absdate&	operator-(const absdate& other) { cached = false; value -= other.value; if (value < 0) value = 0.0; return *this; }

	void	operator+=(const absdate& other) { operator+(other); }
	void	operator-=(const absdate& other) { operator-(other); }

	void	operator=(const absdate& other) { cached = false; value = other.value; }
	
	bool	operator<(const absdate& other) { return (value < other.value); }
	bool	operator>(const absdate& other) { return (value > other.value); }
	bool	operator==(const absdate& other)
	{
		static const float eps = 1.0/86400.0;

		return (value > (other.value - eps) && value < (other.value + eps));
	}

	bool	operator!=(const absdate& other) { return !operator==(other); }

	/* access functions */

	static int sum_days(unsigned int month, unsigned int year);
	static unsigned int get_month(unsigned int days, unsigned int year);
	static bool is_leap_year(unsigned int year);
	static unsigned int real_ylength(unsigned int year);
	static std::string get_month_name(unsigned int nr);

	unsigned int get_year(void) { if (!cached) compute(); return year; }
	unsigned int get_mon(void) { if (!cached) compute(); return month; }
	unsigned int get_day(void) { if (!cached) compute(); return day+1; }
	unsigned int get_hours(void) { if (!cached) compute(); return hours; }
	unsigned int get_minutes(void) { if (!cached) compute(); return minutes; }
	unsigned int get_seconds(void) { if (!cached) compute(); return seconds; }


private:
	void assign_date(unsigned int year, unsigned int month, unsigned int day,
		unsigned int hour, unsigned int minute, unsigned int second);
	void compute(void);

	double value;

	static unsigned int ylength;
	static unsigned int mlength[12];
	static std::map<unsigned int, std::string> month_names;
	static unsigned int year_1990;

	std::string cached_display_value;
	bool cached;

	double seconds, minutes, hours;
	unsigned int day, month, year;
};


} /* namespace lib */

#endif /* _lib_absdate_h */
