
/* 
 * $Id: dirname.cxx,v 1.7 2008/03/24 09:27:35 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * The C++ version of dirname(3).
 * 
 * The dirname() function takes a pathname, path, and returns a string that is
 * a pathname of the parent directory of path.  Trailing `/' characters in
 * path are not counted as part of the path.
 * 
 * If path does not contain a `/', then dirname() returns the string ``.''.
 * 
 * If path is an empty string, dirname() returns the string ``.''.
 */
WINDLL_LIB std::string
dirname(const std::string &path)
{
	if (path.empty())
		/* handle empty string */
		goto return_dot;

	std::string::size_type end;

	/* Strip trailing slashes, if any. */
	end = path.find_last_not_of(PATHSEPARATORS);

	if (end == std::string::npos)
		/* we consist entirely of '/' */
		goto return_dot;

	/* find last '/' */
	end = path.find_last_of(PATHSEPARATORS, end);

	/* No '/' found, return the string "." */
	if (end == std::string::npos)
		goto return_dot;

	/* Strip trailing slashes, if any. */
	end = path.find_last_not_of(PATHSEPARATORS, end);

	return path.substr(0, end+1);

return_dot:
	return std::string(".");
}

} /* namespace lib */
