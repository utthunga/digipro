
/* 
 * $Id: Logger.h,v 1.4 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2006-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_Logger_h
#define _lib_Logger_h

#include "lib_config.h"

// C stdlib
#include <stdarg.h>
#include <time.h>

// C++ stdlib
#include <list>
#include <map>
#include <string>
#include <vector>

// own header
#include "stream.h"


namespace lib
{

/**
 * Sophisticated logging services.
 */
class WINDLL_LIB Logger
{
public:
	Logger(void);

	void set_stream(class stream *o = NULL) { out = o; }

	void create_logclass(const std::string &, const std::string &, bool = true);
	void create_category(const std::string &, const std::string &, bool = true);

	size_t get_mem_limit(void) const throw() { return memory_limit; }
	void set_mem_limit(size_t limit);

	/**
	 * Logclass/Category Description.
	 */
	struct info
	{
		std::string descr;
		bool enabled;
		const bool def_enabled;

		info(const std::string &d, bool e = true)
		: descr(d), enabled(e), def_enabled(e) { }
	};
	typedef struct info info_t;

	enum { LOGCLASS, CATEGORY };

	/* information/configuration */
	std::vector<std::string> get_names(int type);
	info_t get_info(int type, const std::string &);
	void set_active(int type, const std::string &, bool = true);

	/* generic log */
	void log(const std::string &logclass,
		 const std::string &category,
		 const std::string &message);

	void log(const std::string &logclass,
		 const std::string &category,
		 const char *fmt, ...) CHECK_PRINTF(4, 5);

	void log(const std::string &logclass,
		 const std::string &category,
		 const char *fmt, va_list va_args) CHECK_PRINTF(4, 0);

	/* predefined logclasses */
	void log_info(const std::string &cat, const std::string &msg)     { log(CLASS_INFO,     cat, msg); }
	void log_warning(const std::string &cat, const std::string &msg)  { log(CLASS_WARNING,  cat, msg); }
	void log_error(const std::string &cat, const std::string &msg)    { log(CLASS_ERROR,    cat, msg); }
	void log_trace(const std::string &cat, const std::string &msg)    { log(CLASS_TRACE,    cat, msg); }
	void log_lowtrace(const std::string &cat, const std::string &msg) { log(CLASS_LOWTRACE, cat, msg); }

	void log_info(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void log_warning(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void log_error(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void log_trace(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void log_lowtrace(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);

	static const std::string CLASS_INFO;
	static const std::string CLASS_WARNING;
	static const std::string CLASS_ERROR;
	static const std::string CLASS_TRACE;
	static const std::string CLASS_LOWTRACE;

private:
	class stream *out;

	typedef std::map<std::string, info_t> infos_t;
	infos_t logclasses;
	infos_t categories;

	infos_t &get_infos(int type);

	struct entry
	{
		time_t recorded;
		infos_t::const_iterator logclass;
		infos_t::const_iterator category;
		std::string message;
	};

	typedef std::list<struct entry> entries_t;
	entries_t entries;

	size_t memory, memory_limit;

	std::string pretty_print(const struct entry &);
	void push_entry_back(const struct entry &);
	void pop_entry_front(void);
};

} /* namespace lib */

#endif /* _lib_Logger_h */
