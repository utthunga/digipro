
/* 
 * $Id: pathseparator.cxx,v 1.1 2006/02/01 16:24:52 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

WINDLL_LIB const char *
pathseparator(void) throw()
{
	return DEF_PATHSEPARATOR;
}

} /* namespace lib */
