
/* 
 * $Id: strip_extension.cxx,v 1.4 2006/01/30 09:27:00 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * Strip extension.
 * If pathname contain an extension (that is a '.' in the filename)
 * it's stripped and the result returned. If there is no extension
 * path is returned without modifications.
 */
WINDLL_LIB std::string
strip_extension(const std::string &path)
{
	std::string::size_type pos = path.rfind('.');

	if (pos != std::string::npos)
	{
		std::string::size_type separator = path.find_last_of(PATHSEPARATORS);

		if (separator == std::string::npos || separator < pos)
			return path.substr(0, pos);
	}

	return path;
}

} /* namespace lib */
