
/* 
 * $Id: ident.cxx,v 1.15 2009/04/07 11:23:45 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <ctype.h>
#include <string.h>

// C++ stdlib

// own header
#include "ident.h"
#include "port.h"


namespace lib
{

IDENTIFIER::id::id(class IDENT *p, char *s)
: links(0), parent(p), str(s)
{
#ifdef STATISTIC
	parent->entries++;
	parent->mem += strlen(str)+1;
	parent->mem += sizeof(*this);
#endif

	parent->hash.install(str, this);
}

IDENTIFIER::id::~id(void)
{
#ifdef STATISTIC
	parent->entries--;
	parent->mem -= strlen(str)+1;
	parent->mem -= sizeof(*this);
#endif

	if (parent)
		parent->hash.remove(str);

	delete [] str;
}

void
IDENTIFIER::release(void) throw()
{
	if (myid)
	{
		myid->links--;

		if (myid->links == 0)
			delete myid;
	}
}

IDENTIFIER &
IDENTIFIER::operator=(const IDENTIFIER &id) throw()
{
	if (this != &id)
	{
		release();
		myid = id.myid;
		if (myid) myid->links++;
	}
	return *this;
}

size_t
IDENTIFIER::length(void) const throw()
{
	size_t len = 0;

	if (myid)
		len = strlen(myid->c_str());

	return len;
}

const char *
IDENTIFIER::c_str(void) const throw()
{
	if (myid)
		return myid->c_str();

	return "";
}

std::string
IDENTIFIER::str(void) const
{
	if (myid)
		return std::string(myid->c_str());

	return std::string();
}

bool
IDENTIFIER::operator<(const IDENTIFIER &id) const throw()
{
	return (myid < id.myid);
}

bool
IDENTIFIER::operator>(const IDENTIFIER &id) const throw()
{
	return (myid > id.myid);
}

void *
IDENTIFIER::hash(void) const throw()
{
	return myid;
}


const IDENTIFIER IDENT::NoIdent;

IDENT::IDENT(void)
: hash(14)
#ifdef STATISTIC
  , entries(0), mem(0)
#endif
{
	cache.reserve(4096);
}

IDENT::~IDENT(void)
{
	for (std::vector<IDENTIFIER>::iterator it = cache.begin(); it != cache.end(); ++it)
		if (it->myid)
			it->myid->parent = NULL;
}

/**
 * IDENTIFIER creation.
 * Create an identifier from a std string.
 * \param s The std string. Empty strings are allowed.
 * \return The IDENTIFIER for this string or NoIdent for null pointers or empty
 * strings.
 */
IDENTIFIER
IDENT::create(const std::string &s)
{
	return create(s.c_str());
}

/**
 * IDENTIFIER creation.
 * Create an identifier from a C string.
 * \param s The C string pointer. Null pointers and empty strings are allowed.
 * \return The IDENTIFIER for this string or NoIdent for null pointers or empty
 * strings.
 */
IDENTIFIER
IDENT::create(const char *s)
{
	IDENTIFIER::id *t;

	if (!s || *s == '\0')
		return NoIdent;

	t = hash.lookup(s);
	if (!t)
	{
		size_t len = strlen(s) + 1;
		char *str = new char[len];

		strcpy_s(str, len, s);

		t = new IDENTIFIER::id(this, str);
		cache.push_back(IDENTIFIER(t));
	}

	return IDENTIFIER(t);
}

/**
 * IDENTIFIER creation.
 * Create an identifier from a new[] allocated C string.
 * \param s The C string pointer. Null pointers and empty strings are allowed.
 * \return The IDENTIFIER for this string or NoIdent for null pointers or empty
 * strings.
 * \warning The memory is owned by the IDENTIFIER after calling this method.
 */
IDENTIFIER
IDENT::create_from_string(char *&s)
{
	IDENTIFIER::id *t;
	char *str;

	if (!s || *s == '\0')
		return NoIdent;

	/* move ownership */
	str = s;
	s = NULL;

	t = hash.lookup(str);
	if (!t)
	{
		t = new IDENTIFIER::id(this, str);
		cache.push_back(IDENTIFIER(t));
	}
	else
		delete [] str;

	return IDENTIFIER(t);
}

/**
 * IDENTIFIER creation.
 * Create an identifier from a buffer. The buffer doesn't need to be
 * terminated with 0 like for C strings.
 * \param s The buffer pointer. Null pointers and empty strings are allowed.
 * \param len The length of the buffer.
 * \return The IDENTIFIER for this string or NoIdent for null pointers or empty
 * strings.
 */
IDENTIFIER
IDENT::create_from_buf(const char *s, unsigned long len)
{
	IDENTIFIER::id *t;
	char *str;

	if (!len || !s || *s == '\0')
		return NoIdent;

	str = new char[len+1];

	memcpy(str, s, len);
	str[len] = '\0';

	t = hash.lookup(str);
	if (!t)
	{
		t = new IDENTIFIER::id(this, str);
		cache.push_back(IDENTIFIER(t));
	}
	else
		delete [] str;

	return IDENTIFIER(t);
}

void
IDENT::clear(void)
{
	cache.clear();
	assert(hash.empty());
}

void
IDENT::dump_statistic(class stream &f) const
{
#ifdef STATISTIC
	unsigned long data[5];

	hash.stats(data);

	f.printf("IDENT (%lu entries, %lu bytes, %lu cache entries)\n", entries, mem, cache.size());
	f.printf(" - HASHTABLE (%lu bits, %lu entries)\n", data[0], data[1]);
	f.printf("   %5lu entries\n", data[2]);
	f.printf("   %5lu collisions\n", data[3]);
	f.printf("   %5lu bytes\n", data[4]);
#endif
}

} /* namespace lib */
