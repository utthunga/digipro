
/* 
 * Copyright (C) 2011 Robert Schneckenhaus <robert.schneckenhaus@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_find_end_h
#define _lib_find_end_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB size_t find_end(const std::string &, size_t, char);

WINDLL_LIB size_t find_closing_round_bracket(const std::string &, size_t);
WINDLL_LIB size_t find_closing_curly_bracket(const std::string &, size_t);
WINDLL_LIB size_t find_closing_square_bracket(const std::string &, size_t);

} /* namespace lib */

#endif /* _lib_find_end_h */
