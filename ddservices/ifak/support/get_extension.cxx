
/* 
 * $Id: get_extension.cxx,v 1.1 2007/07/13 11:30:13 fna Exp $
 *
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * Get extension.
 * Return the extension of the file as specified by the path argument.
 */
WINDLL_LIB std::string
get_extension(const std::string &path)
{
	std::string name(lib::basename(path));
	std::string::size_type pos = name.rfind('.');
	std::string ret;

	if (pos != std::string::npos)
		ret = name.substr(pos, std::string::npos);

	return ret;
}

} /* namespace lib */
