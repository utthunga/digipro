
/* 
 * $Id: auto_buf.cxx,v 1.9 2007/06/22 09:34:44 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib

// own libs

// own header
#include "auto_buf.h"
#include "port.h"


namespace lib
{

auto_buf::auto_buf(size_t s)
: size_(s)
, buf_(new char[size_])
{
	bzero(buf_, size_);
}

auto_buf::~auto_buf(void)
{
	delete [] buf_;
}

auto_buf::auto_buf(const auto_buf &b)
: size_(b.size_)
, buf_(new char[size_])
{
	memcpy(buf_, b.buf_, size_);
}

class auto_buf &
auto_buf::operator=(const auto_buf &b)
{
	if (this != &b)
	{
		delete [] buf_;
		
		size_ = b.size_;
		
		buf_ = new char[size_];
		memcpy(buf_, b.buf_, size_);
	}
	
	return *this;
}

#if defined(_MSC_VER) && defined(_DEBUG)
char *auto_buf::data(void) const throw() { return buf_; }
#endif

} /* namespace lib */
