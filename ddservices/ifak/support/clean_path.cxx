
/* 
 * $Id: clean_path.cxx,v 1.2 2008/10/11 23:46:49 fna Exp $
 *
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

WINDLL_LIB std::string
clean_path(const std::string &path)
{
	std::string ret(path);

	if (ret.size() > 1)
	{
		std::string::size_type pos;

		/* remove heading ./ or .\ and following pathseparators
		 */
		if (ret[0] == '.' && is_pathseparator(ret[1]))
		{
			pos = 2;

			while (is_pathseparator(ret[pos]))
				++pos;

			ret.erase(0, pos);
		}

		/* filter // to / or \\ to \
		 */
		pos = ret.find_first_of(PATHSEPARATORS);
		while (pos != std::string::npos)
		{
			std::string::size_type seps = 0;

			++pos;
			while (pos+seps < ret.size() && is_pathseparator(ret[pos+seps]))
				++seps;

			if (seps)
			{
				ret.erase(pos, seps);
				++pos;
			}

			pos = ret.find_first_of(PATHSEPARATORS, pos);
		}

		/* filter /./ to / or \.\ to \
		 */
		pos = ret.find_first_of(PATHSEPARATORS);
		while (pos != std::string::npos && pos+2 < ret.size())
		{
			if (ret[pos+1] == '.' && is_pathseparator(ret[pos+2]))
				ret.erase(pos+1, 2);
			else
				pos = ret.find_first_of(PATHSEPARATORS, pos+1);
		}

		/* filter trailing pathseparator
		 */
		pos = ret.size();
		if (pos && is_pathseparator(ret[pos-1]))
			ret.erase(pos-1);
	}

	return ret;
}

} /* namespace lib */
