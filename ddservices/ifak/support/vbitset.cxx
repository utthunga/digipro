
/* 
 * $Id: vbitset.cxx,v 1.10 2009/11/19 16:04:04 fna Exp $
 *
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own header
#include "ilog.h"
#include "vbitset.h"


namespace lib
{

#define __BITS_PER_WORDT \
	(8 * sizeof(unsigned long))

#define __BITSET_WORDS(__n) \
	((__n) < 1 ? 1 : ((__n) + __BITS_PER_WORDT - 1) / __BITS_PER_WORDT)

#define _S_whichword(__pos) \
	(__pos / __BITS_PER_WORDT)

#define _S_whichbit(__pos) \
	(__pos % __BITS_PER_WORDT)

#define _S_maskbit(__pos) \
	(1UL << _S_whichbit(__pos))

vbitset::~vbitset(void)
{
	delete [] data;
}

vbitset::vbitset(void)
: bits(0), words(1)
{
	data = new unsigned long[words];
	assert(data);
	
	data[0] = 0;
}

vbitset::vbitset(unsigned long val)
: bits(0), words(1)
{
	data = new unsigned long[words];
	assert(data);
	
	data[0] = val;
	bits = ilog2((sys::uint64)val) + 1;
}

vbitset::vbitset(const vbitset &t)
: bits(t.bits), words(t.words)
{
	size_t i;
	
	data = new unsigned long[words];
	assert(data);
	
	for (i = 0; i < words; ++i)
		data[i] = t.data[i];
}

vbitset& vbitset::operator=(const vbitset &x)
{
	if (this != &x)
	{
		size_t i;
		
		bits = x.bits;
		
		if (words != x.words)
		{
			words = x.words;
			
			delete [] data;
			
			data = new unsigned long[words];
			assert(data);
		}
		
		for (i = 0; i < words; ++i)
			data[i] = x.data[i];
	}
	
	return *this;
}


unsigned long &vbitset::getword(size_t pos)       { return data[_S_whichword(pos)]; }
unsigned long  vbitset::getword(size_t pos) const { return data[_S_whichword(pos)]; }

unsigned long &vbitset::hiword(void)       { return data[words - 1]; }
unsigned long  vbitset::hiword(void) const { return data[words - 1]; }


void
vbitset::do_extend(size_t nbits)
{
	do_extend(nbits, __BITSET_WORDS(nbits));
}

void
vbitset::do_extend(size_t nbits, size_t nwords)
{
	if (bits < nbits)
	{
		bits = nbits;
		
		if (words < nwords)
		{
			unsigned long *ndata;
			size_t i;
			
			ndata = new unsigned long[nwords];
			assert(ndata);
			
			for (i = 0; i < words; ++i)
				ndata[i] = data[i];
			
			delete [] data;
			data = ndata;
			
			words = nwords;
			
			/* zero out the newly allocated space */
			for (; i < words; ++i)
				data[i] = 0;
		}
	}
}

void
vbitset::do_align(const vbitset &x)
{
	do_extend(x.bits, x.words);
}

// Helper class to zero out the unused high-order bits in the highest word.
void
vbitset::do_sanitize(void)
{
	if (bits % __BITS_PER_WORDT)
		hiword() &= ~((~0UL) << (bits % __BITS_PER_WORDT));
}

void
vbitset::do_and(const vbitset &x)
{
	size_t i;
	
	for (i = 0; i < x.words; ++i)
		data[i] &= x.data[i];
	
	for (; i < words; ++i)
		data[i] &= 0;
}

void
vbitset::do_or(const vbitset &x)
{
	size_t i;
	
	for (i = 0; i < x.words; ++i)
		data[i] |= x.data[i];
}

void
vbitset::do_xor(const vbitset &x)
{
	size_t i;
	
	for (i = 0; i < x.words; ++i)
		data[i] ^= x.data[i];
	
	for (; i < words; ++i)
		data[i] ^= 0;
}

void
vbitset::do_left_shift(size_t shift)
{
	if (shift != 0)
		do_extend(bits + shift);
	
	if (shift != 0)
	{
		const size_t wshift = shift / __BITS_PER_WORDT;
		const size_t offset = shift % __BITS_PER_WORDT;
		const size_t sub_offset = __BITS_PER_WORDT - offset;
		size_t n = words - 1, n1;
		
		for (; n > wshift; --n)
			data[n] = (data[n - wshift] << offset) |
					(data[n - wshift - 1] >> sub_offset);
		
		if (n == wshift)
			data[n] = data[0] << offset;
		
		for (n1 = 0; n1 < n; ++n1)
			data[n1] = 0UL;
	}
}

void
vbitset::do_right_shift(size_t shift)
{
	if (shift != 0)
	{
		const size_t wshift = shift / __BITS_PER_WORDT;
		const size_t offset = shift % __BITS_PER_WORDT;
		const size_t sub_offset = __BITS_PER_WORDT - offset;
		const size_t limit = words - wshift - 1;
		size_t n = 0, n1;
		
		for ( ; n < limit; ++n)
			data[n] = (data[n + wshift] >> offset) |
					(data[n + wshift + 1] << sub_offset);
		
		data[limit] = data[words-1] >> offset;
		
		for (n1 = limit + 1; n1 < words; ++n1)
			data[n1] = 0UL;
	}
}

void
vbitset::do_set(void)
{
	size_t i;
	
	for (i = 0; i < words; ++i)
		data[i] = ~0UL;
}

void
vbitset::do_reset(void)
{
	delete [] data;
	
	bits = 0;
	words = 1;
	
	data = new unsigned long[words];
	assert(data);
	
	data[0] = 0;
}

void
vbitset::do_flip(void)
{
	size_t i;
	
	for (i = 0; i < words; ++i)
		data[i] = ~data[i];
}

bool
vbitset::is_equal(const vbitset &x) const
{
	size_t i;
	
	if (bits != x.bits)
		return false;
	
	for (i = 0; i < words; ++i)
		if (data[i] != x.data[i])
			return false;
	
	return true;
}

bool
vbitset::is_any(void) const
{
	size_t i;
	
	for (i = 0; i < words; ++i)
		if (data[i] != 0)
			return true;
	
	return false;
}

unsigned long
vbitset::to_ulong(void) const
{
	size_t i;
	
	for (i = 1; i < words; ++i)
		if (data[i])
			throw std::overflow_error("vbitset");
	
	return data[0];
}

static inline void
put_next(unsigned char *buf, size_t len, size_t i, unsigned char c, bool msb_first)
{
	buf[msb_first ? (len - 1 - i) : i] = c;
}

void
vbitset::to_buf(unsigned char *buf, size_t len, bool msb_first) const
{
	size_t j = 0;
	
	for (size_t i = 0; i < words && j < len; ++i)
	{
		unsigned int word = data[i];
		
		put_next(buf, len, j++, (word      ) & 0xff, msb_first); if (j == len) break;
		put_next(buf, len, j++, (word >>  8) & 0xff, msb_first); if (j == len) break;
		put_next(buf, len, j++, (word >> 16) & 0xff, msb_first); if (j == len) break;
		put_next(buf, len, j++, (word >> 24) & 0xff, msb_first);
	}
	
	while (j < len)
		put_next(buf, len, j++, 0, msb_first);
}

static inline unsigned long
get_next(const unsigned char *buf, size_t len, size_t i, bool msb_first)
{
	return buf[msb_first ? (len - 1 - i) : i];
}

void
vbitset::from_buf(const unsigned char *buf, size_t len, bool msb_first)
{
	do_extend(len * 8);
	
	for (size_t i = 0, j = 0; j < len; ++i)
	{
		unsigned long word;
		
		word = get_next(buf, len, j++, msb_first);              data[i]  = word; if (j == len) break;
		word = get_next(buf, len, j++, msb_first); word <<=  8; data[i] |= word; if (j == len) break;
		word = get_next(buf, len, j++, msb_first); word <<= 16; data[i] |= word; if (j == len) break;
		word = get_next(buf, len, j++, msb_first); word <<= 24; data[i] |= word;
	}
}


vbitset& vbitset::operator&=(const vbitset &x)
{
	do_align(x);
	do_and(x);
	return *this;
}

vbitset& vbitset::operator|=(const vbitset &x)
{
	do_align(x);
	do_or(x);
	return *this;
}

vbitset& vbitset::operator|=(sys::uint64 x)
{
	size_t highest = ilog2(x) + 1;
	
	if (highest > bits)
		bits = highest;
	
	data[0] |= x;
	
	if (sizeof(data[0]) == 4)
		data[1] |= x >> 32;
	
	return *this;
}

vbitset& vbitset::operator^=(const vbitset &x)
{
	do_align(x);
	do_xor(x);
	return *this;
}

vbitset& vbitset::operator<<=(size_t pos)
{
	do_left_shift(pos);
	do_sanitize();
	return *this;
}

vbitset& vbitset::operator>>=(size_t pos)
{
	do_right_shift(pos);
	do_sanitize();
	return *this;
}

vbitset vbitset::operator~ (void)       const { return vbitset(*this).flip();  }
vbitset vbitset::operator<<(size_t pos) const { return vbitset(*this) <<= pos; }
vbitset vbitset::operator>>(size_t pos) const { return vbitset(*this) >>= pos; }

bool vbitset::operator==(const vbitset &x) const { return  is_equal(x); }
bool vbitset::operator!=(const vbitset &x) const { return !is_equal(x); }	


bool vbitset::test(size_t pos) const
{
	if (pos >= bits)
		return false;
	
	return ((getword(pos) & _S_maskbit(pos)) != 0UL);
}

bool vbitset::operator[](size_t pos) const
{
	return test(pos);
}


vbitset& vbitset::set(void)
{
	do_set();
	do_sanitize();
	return *this;
}

vbitset& vbitset::set(size_t pos, int val)
{
	if (pos >= bits)
		do_extend(pos + 1);
	
	if (val) getword(pos) |= _S_maskbit(pos);
	else getword(pos) &= ~_S_maskbit(pos);
	
	return *this;
}


vbitset& vbitset::reset(void)
{
	do_reset();
	return *this;
}

vbitset& vbitset::reset(size_t pos)
{
	if (pos >= bits)
		do_extend(pos + 1);
	
	getword(pos) &= ~_S_maskbit(pos);
	return *this;
}


vbitset& vbitset::flip(void)
{
	do_flip();
	do_sanitize();
	return *this;
}

vbitset& vbitset::flip(size_t pos)
{
	if (pos >= bits)
		do_extend(pos + 1);
	
	getword(pos) ^= _S_maskbit(pos);
	return *this;
}


bool vbitset::any(void) const { return is_any(); }
bool vbitset::none(void) const { return !is_any(); }

size_t vbitset::size(void) const { return bits; }

void
vbitset::dump(class stream &f) const
{
	for (size_t i = bits; i > 0; i--)
	{
		if (test(i - 1))
			f.put('1');
		else
			f.put('0');
	}
}


WINDLL_LIB vbitset operator&(const vbitset &x, const vbitset &y)
{
	vbitset result(x);
	result &= y;
	return result;
}

WINDLL_LIB vbitset operator|(const vbitset &x, const vbitset &y)
{
	vbitset result(x);
	result |= y;
	return result;
}

WINDLL_LIB vbitset operator^(const vbitset &x, const vbitset &y)
{
	vbitset result(x);
	result ^= y;
	return result;
}

} /* namespace lib */
