
/* 
 * $Id: basename_with_sep.cxx,v 1.3 2008/03/24 09:27:35 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_internal.h"

// C stdlib

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

WINDLL_LIB std::string
basename_with_sep(const std::string &path)
{
	if (path.empty())
		/* handle empty string */
		return std::string(DEF_PATHSEPARATOR ".");

	std::string::size_type end = path.find_last_not_of(PATHSEPARATORS);

	if (end == std::string::npos)
		/* we consist entirely of '/' */
		return std::string(DEF_PATHSEPARATOR);

	std::string::size_type pos = path.find_last_of(PATHSEPARATORS, end);

	if (pos == std::string::npos)
	{
		std::string ret(DEF_PATHSEPARATOR);
		ret += path;

		return ret;
	}

	return path.substr(pos);
}

} /* namespace lib */
