
/* 
 * Copyright (C) 2011 Robert Schneckenhaus <robert.schneckenhaus@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <algorithm>

// own header
#ifdef CPM_64_BIT_OS 
#include "string.h"
#else
#include <string>
#endif

#ifdef min
#undef min /* avoid WinAPI min macro */
#endif

namespace lib
{

WINDLL_LIB void
replace_all(std::string &str, char old, char ch, size_t pos, size_t n)
{
	size_t end = (n == std::string::npos) ? str.length() : std::min(pos + n, str.length());

	for (size_t i=pos; i<end; ++i)
		if (str[i] == old)
			str[i] = ch;
}

WINDLL_LIB void
replace_all(std::string &str, const std::string &old, const std::string &s, size_t pos, size_t n)
{
	size_t end = (n == std::string::npos) ? str.length() : std::min(pos + n, str.length());
	size_t off = pos;

	const int diff = s.length() - old.length();

	for (;;)
	{
		off = str.find(old, off);

		if (off == std::string::npos || off >= end || off + old.length() > end)
			return;

		str.replace(off, old.length(), s);

		off += s.length();
		end += diff;
	}
}

} /* namespace lib */
