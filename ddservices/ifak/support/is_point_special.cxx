
/* 
 * $Id: is_point_special.cxx,v 1.1 2006/01/20 10:24:55 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "paths.h"


namespace lib
{

/**
 * Check if file is '.' or '..'.
 * Return true if the name match '.' or '..', false otherwise.
 */
WINDLL_LIB bool
is_point_special(const std::string &name)
{
	const size_t len = name.length();

	if (len && len < 3 && name[0] == '.')
	{
		if (len == 1 || name[1] == '.')
			return true;
	}

	return false;
}

} /* namespace lib */
