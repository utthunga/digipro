
/* 
 * Copyright (C) 2011 Robert Schneckenhaus <robert.schneckenhaus@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

WINDLL_LIB std::string
strtok(const std::string &str, size_t &off, const std::string &dividers)
{
	if (off >= str.length())
	{
		off = std::string::npos;
		return std::string();
	}

	size_t pos = str.find_first_of(dividers, off);

	if (pos == std::string::npos)
	{
		std::string rval = str.substr(off);
		off = str.length();

		return rval;
	}

	std::string rval = str.substr(off, pos-off);
	off = pos+1;
	
	return rval;
}

} /* namespace lib */
