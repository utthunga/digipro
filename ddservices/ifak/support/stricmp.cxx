
/* 
 * $Id: stricmp.cxx,v 1.2 2005/09/19 15:20:58 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <ctype.h>

// C++ stdlib
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

/*
 * Case insensitive string comparison. Note that this only returns
 * 0 (match) or nonzero (no match), and that the returned value
 * is not a reliable indicator of any "order".
 */
WINDLL_LIB int
stricmp(const char *str1, const char *str2)
{
	register char c1, c2;
	
	do {
		c1 = *str1++; if (isupper(c1)) c1 = tolower(c1);
		c2 = *str2++; if (isupper(c2)) c2 = tolower(c2);
	}
	while (c1 && c1 == c2);
	
	return (int)(c1 - c2);
}

WINDLL_LIB int
stricmp(const std::string &str1, const std::string &str2)
{
	return stricmp(str1.c_str(), str2.c_str());
}

} /* namespace lib */
