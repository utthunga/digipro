
/* 
 * $Id: currtimestr.cxx,v 1.3 2009/08/03 10:17:58 fna Exp $
 *
 * Copyright (C) 2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <time.h>

// C++ stdlib

// own header
#include "currtimestr.h"
#include "port.h"


namespace lib
{

WINDLL_LIB std::string
currtimestr(const char *fmt)
{
	time_t t = time(NULL);
	struct tm tm;
	char buf[128];

#if _MSC_VER >= 1400
	localtime_r(&t, &tm);
#else
	struct tm *tm0 = localtime(&t);
	assert(tm0);
	tm = *tm0;
#endif

	strftime(buf, sizeof(buf), fmt, &tm);

	return std::string(buf);
}

} /* namespace lib */
