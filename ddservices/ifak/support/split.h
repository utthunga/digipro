
/* 
 * Copyright (C) 2011 Robert Schneckenhaus <robert.schneckenhaus@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_split_h
#define _lib_split_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <list>
#include <memory>
#include <string>
#include <vector>

// own header


namespace lib
{

template <typename Container>
std::auto_ptr<Container> split(const std::string &str, const std::string &dividers)
{
	std::auto_ptr<Container> c(new Container);
	std::string::size_type off = 0;
	std::string::size_type pos;

	while (off < str.length())
	{
		pos = str.find_first_of(dividers, off);

		if (pos == std::string::npos)
		{
			c->push_back(str.substr(off));
			break;
		}

		if (pos == off)
			c->push_back(std::string()); // empty string
		else
			c->push_back(str.substr(off, pos-off));

		if (pos == str.length()-1)
			c->push_back(std::string());

		off = pos+1;
	}

	return std::auto_ptr<Container>(c.release());
}

std::auto_ptr<std::vector<std::string> > splitv(const std::string &str, const std::string &dividers)
{
	return split<std::vector<std::string > >(str, dividers);
}

std::auto_ptr<std::list<std::string> > splitl(const std::string &str, const std::string &dividers)
{
	return split<std::list<std::string > >(str, dividers);
}

} /* namespace lib */

#endif /* _lib_split_h */
