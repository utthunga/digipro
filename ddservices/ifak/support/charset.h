
/* 
 * $Id: charset.h,v 1.5 2009/09/22 13:28:27 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Support routines for ISO Latin-1 and UTF-8 handling.
 */

#ifndef _lib_charset_h
#define _lib_charset_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

/* forward declarations */
class dyn_buf;
class scratch_buf;

/**
 * Check if a character is in the ISO Latin-1 set.
 * \note It's assumed that the given argument is really a character, e.g.
 * not greater than 255 or smaller than 0. Otherwise the behaviour is
 * undefined.
 */
inline bool
is_iso_latin_1(int c) throw()
{
	extern WINDLL_LIB char iso_latin_1[256];
	
	return (iso_latin_1[c] ? true : false);
}

/**
 * Convert an ISO Latin-1 into an UTF-8 string.
 * This routine convert an ISO Latin-1 (also known as
 * ISO-8859-1) encoded string into an UTF-8 encoded string.
 * It's designed for maximal speed. The resulting UTF-8 string
 * can be larger than the original string.
 */
WINDLL_LIB std::string iso_latin1_to_utf8(const std::string &);
WINDLL_LIB std::string iso_latin1_to_utf8(const dyn_buf &);
WINDLL_LIB std::string iso_latin1_to_utf8(const scratch_buf &);
WINDLL_LIB std::string iso_latin1_to_utf8(const char *);
WINDLL_LIB std::string iso_latin1_to_utf8(const char *, size_t);

} /* namespace lib */

#endif /* _lib_charset_h */
