
/* 
 * $Id: paths.h,v 1.8 2008/10/11 23:46:49 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_paths_h
#define _lib_paths_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB std::string basename(const std::string &);
WINDLL_LIB std::string dirname(const std::string &);

WINDLL_LIB std::string basename_with_sep(const std::string &);

WINDLL_LIB bool check_extension(const std::string &path, const std::string &ext);
WINDLL_LIB std::string get_extension(const std::string &path);
WINDLL_LIB std::string strip_extension(const std::string &path);

WINDLL_LIB bool file_exist(const std::string &path);
WINDLL_LIB bool is_point_special(const std::string &name);

WINDLL_LIB std::string rel_path(const std::string &path, const std::string &base);
WINDLL_LIB std::string abs_path(const std::string &path, const std::string &base);
WINDLL_LIB bool is_abs_path(const std::string &path);

WINDLL_LIB std::string clean_path(const std::string &path);

WINDLL_LIB const char *pathseparator(void) throw();
WINDLL_LIB bool is_pathseparator(char c);

WINDLL_LIB void rrm(const std::string &path);

} /* namespace lib */

#endif /* _lib_paths_h */
