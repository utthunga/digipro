
/* 
 * $Id: supp_smart_ptr.cxx,v 1.2 2006/04/10 13:42:46 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "supp_smart_ptr.h"


namespace lib
{

supp_smart_ptr_base::supp_smart_ptr_base(void)
: refs(0)
{
}

supp_smart_ptr_base::~supp_smart_ptr_base(void)
{
}

} /* namespace lib */
