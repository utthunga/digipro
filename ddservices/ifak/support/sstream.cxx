
/* 
 * $Id: sstream.cxx,v 1.5 2009/07/01 12:50:42 fna Exp $
 *
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "dyn_buf.h"
#include "sstream.h"


namespace lib
{

sstream::sstream(void)
: pos(0)
{
	buf.reserve(60);
}

sstream::sstream(size_t start_size)
: pos(0)
{
	buf.reserve(start_size);
}

sstream::sstream(const std::string &src)
: buf(src)
, pos(0)
{
}

size_t
sstream::read(void *ptr, size_t size, size_t nmemb) const
{
	const char *str = buf.data();
	size_t length = buf.length();
	off_t todo = size * nmemb;
	off_t avail = length - pos;

	todo = std::min(avail, todo);
	memcpy(ptr, str + pos, todo);

	pos += todo;

	return todo;
}
size_t
sstream::write(const void *ptr, size_t size, size_t nmemb)
{
	off_t todo = size * nmemb;

	buf.append(static_cast<const char *>(ptr), todo);

	return todo;
}

bool
sstream::eof(void) const
{
	size_t length = buf.length();
	off_t avail = length - pos;

	return avail ? true : false;
}
void
sstream::clear(void)
{
	buf.clear();
	pos = 0;
}

int
sstream::vprintf(const char *fmt, va_list args)
{
	class dyn_buf tmp(120);

	tmp.vprintf(fmt, args);
	put(tmp.data(), tmp.length());

	return tmp.length();
}

void sstream::put(const std::string &str)      { buf.append(str); }
void sstream::put(const char *str, size_t len) { buf.append(str, len); }
void sstream::put(const char *str)             { buf.append(str); }
void sstream::put(const char c)                { buf += c; }

int
sstream::get(void) const
{
	const char *str = buf.data();
	size_t length = buf.length();
	off_t avail = length - pos;
	int c = EOF;

	if (avail)
		c = *(str + pos++);

	return c;
}

} /* namespace lib */

