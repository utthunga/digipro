
/* 
 * $Id: position.cxx,v 1.5 2007/09/17 11:44:05 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own header
#include "position.h"


namespace lib
{

bool pos::operator== (const struct pos &p) const throw()
{
	return ((line == p.line) && (column == p.column) && (file == p.file));
}

bool pos::operator!= (const struct pos &p) const throw()
{
	return ((line != p.line) || (column != p.column) || (file != p.file));
}

#if defined(_MSC_VER) && (_MSC_VER < 1300)
WINDLL_LIB struct pos NoPosition;	

namespace
{
	class init
	{
	public:
		init(void) throw()
		{
			NoPosition.line = 0;
			NoPosition.column = 0;
			NoPosition.file = IDENT::NoIdent;
			NoPosition.end_column = 0;
			NoPosition.end_line = 0;
		}
	};
	
	static class init init;
}
#else
WINDLL_LIB struct pos NoPosition = { 0, 0, IDENT::NoIdent, 0, 0};	
#endif

WINDLL_LIB bool
pos_inside(const pos &check, const pos &start, const pos &end)
{
	if (start.file == end.file)
	{
		if (check.file == start.file)
		{
			if (check.line > start.line && check.line < end.line)
				return true;

			if (check.line == start.line && check.column >= start.column)
				return true;

			if (check.line == end.line && check.column <= end.column)
				return true;
		}
	}
	else
	{
		if (check.file == start.file)
		{
			if (check.line > start.line)
				return true;

			if (check.line == start.line && check.column >= start.column)
				return true;
		}
		else if (check.file == end.file)
		{
			if (check.line < end.line)
				return true;

			if (check.line == end.line && check.column <= end.column)
				return true;
		}
	}

	return false;
}

} /* namespace lib */
