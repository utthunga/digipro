
/* 
 * $Id: maptable.h,v 1.10 2008/09/09 22:32:03 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for a general purpose mapping table.
 */

#ifndef _lib_maptable_h
#define _lib_maptable_h

#include "lib_config.h"
#include <iostream>
#ifdef _MSC_VER
# pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <algorithm>
#include <vector>
#include <stdexcept>

// own header


namespace lib
{

/**
 * The maptable template class.
 * This class uniquely map objects from type T to an integer number.
 * The pointer can be accessed through this handle by the overloaded
 * [] operator or the lookup method member. Designed to handle pointers or
 * native types efficently.
 */
template<typename T>
class maptable
{
	typedef std::vector<T> table_t;
public:
	typedef typename table_t::size_type size_type;

#if defined(_MSC_VER) && (_MSC_VER < 1300)
	/* VisualC++ is really a buggy compiler */
	enum { NoHandle = 0 };
	
	maptable(void)
	: tablelookup(0), table(256) { }
	explicit maptable(size_type startsize)
	: tablelookup(0), table(startsize) { }
#else
	/**
	 * The invalid handle.
	 * There exist never a mapping for this handle.
	 * It's for the error condition and initialization.
	 */
	const size_type NoHandle;
	
	maptable(void)
	: NoHandle(0), tablelookup(0), table(256) { }
	explicit maptable(unsigned int startsize)
	: NoHandle(0), tablelookup(0), table(startsize) { }
#endif
	
	void clear(void)
	{
		tablelookup = 0;
		std::fill(table.begin(), table.end(), T());
	}
	
	/** same as lookup(). \see lookup */
	T operator[](size_type handle) const throw() { return lookup(handle); }
	
	/** lookup the object that correspond to handle */
	T lookup(size_type handle) const throw()
	{
		T t = T();
		
		if (handle)
		{
			register size_type index = (handle - 1);
			
			if (index < table.size())
				t = table[index];
		}
		
		return t;
	}
	
	/** create a new mapping for the object t */
	size_type install(T t)
	{
		size_type size;
		
	restart:
		for (size = table.size(); tablelookup < size; ++tablelookup)
		{
			if (table[tablelookup] == T())
			{
				table[tablelookup] = t;
				return ++tablelookup;
			}
		}
		
		table.resize(size * 2);
		goto restart;
	}
	
	/** delete the object that correspond to handle */
        T remove(size_type handle)
	{
		T t = T();
		
		if (handle)
		{
			register size_type index = (handle - 1);
			
			if (index < table.size())
			{
				t = table[index];
				table[index] = T();
				
				if (tablelookup > index)
					tablelookup = index;
			}
			else
				throw std::out_of_range("maptable.remove()");
		}
		
		return t;
	}
	
	/** overwrite the object that correspond to handle with nt */
	T replace(size_type handle, T nt)
	{
		T t = T();
		
		if (handle)
		{
			register size_type index = (handle - 1);
			

			if (index < table.size())
			{

				t = table[index];
				table[index] = nt;
			}
			else
				throw std::out_of_range("maptable.replace()");
		}
		
		return t;
	}

private:	
	size_type tablelookup;
	table_t table;
};

} /* namespace lib */

#endif /* _lib_maptable_h */
