
/* 
 * $Id: net_buf.cxx,v 1.7 2007/04/20 09:40:27 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdio.h>

// C++ stdlib

// own libs

// own header
#include "net_buf.h"
#include "port.h"


namespace lib
{

#define ROUND_UP(size) ((size + 3) & ~3)

net_buf::buffer::buffer(size_t s, size_t hint)
: refcount(1)
, size(s)
, leading_space_hint(hint)
, pos(0)
{
	buf = new char[size];
	clear();
}

net_buf::buffer::buffer(const class buffer &b)
: refcount(1)
, size(b.size)
, leading_space_hint(b.leading_space_hint)
, pos(b.pos)
{
	buf = new char[size];

	start = buf + (b.start - b.buf);
	end = buf + (b.end - b.buf);

	memcpy(start, b.data(), b.length());
}

net_buf::buffer::~buffer(void)
{
	delete [] buf;
}

void
net_buf::buffer::resize(size_t offset)
{
	size = ROUND_UP(size);

	char *nbuf = new char[size];
	char *nstart = nbuf + offset;

	const size_t len = length();

	if (len)
		memcpy(nstart, start, len);

	delete [] buf;

	buf = nbuf;
	start = nstart;
	end = nstart + len;
}

void
net_buf::buffer::remove_from_start(size_t len)
{
	start += len;
	if (start > end)
		start = end;
}

void
net_buf::buffer::remove_from_end(size_t len)
{
	end -= len;
	if (end < start)
		end = start;
}

void *
net_buf::buffer::add_at_start(size_t len)
{
	size_t space = start - buf;

	if (len > space)
	{
		size += (len - space);
		size += leading_space_hint;

		resize(leading_space_hint + len);
	}

	start -= len;
	return start;
}

void *
net_buf::buffer::add_at_end(size_t len)
{
	size_t space = buf + size - end;

	if (len > space)
	{
		while (len > space)
		{
			size <<= 1;
			space = buf + size - end;
		}

		resize(start - buf);
	}

	end += len;
	return (end - len);
}

void
net_buf::buffer::vprintf(const char *fmt, va_list args)
{
restart:
	int space = buf + size - end;
	int ret;
#ifdef va_copy
	va_list copied_args;

	va_copy(copied_args, args);
	ret = lib::vsnprintf(end, space, fmt, copied_args);
	va_end(copied_args);
#else
	ret = lib::vsnprintf(end, space, fmt, args);
#endif
	if (ret >= space)
	{
		size <<= 1;
		resize(start - buf);

		goto restart;
	}
	else if (ret < 0)
	{
		/* failed ??? -> exception ??? */
		ret = 0;
	}

	end += ret;
}


net_buf::net_buf(void)
: buffer(NULL)
{
}

net_buf::net_buf(size_t size, size_t leading_space_hint)
{
	size = std::max(size + leading_space_hint, leading_space_hint << 1);
	size = ROUND_UP(size);

	buffer = new class buffer(size, leading_space_hint);
}

net_buf::net_buf(const net_buf &t)
: buffer(t.buffer)
{
	if (buffer)
		buffer->refcount++;
}

class net_buf &
net_buf::operator=(const net_buf &t)
{
        if (this != &t)
        {
		unref();

		buffer = t.buffer;
		buffer->refcount++;
        }

        return *this;
}

net_buf::~net_buf(void)
{
	unref();
}

void
net_buf::unref(void)
{
	if (buffer)
	{
		if (!--buffer->refcount)
			delete buffer;
	}
}

void
net_buf::modify(void)
{
	if (!buffer)
		buffer = new class buffer(36, 12);

	if (buffer->refcount > 1)
	{
		/* copy-on-write */

		class buffer *new_buffer = new class buffer(*buffer);

		unref();

		buffer = new_buffer;
	}
}

void
net_buf::reset(size_t minsize, size_t hint)
{
	modify();

	buffer->leading_space_hint = hint;
	buffer->clear();

	if ((minsize + hint) > buffer->size)
	{
		buffer->size = std::max(minsize + hint, hint << 1);
		buffer->resize(hint);
	}
}

void
net_buf::append(const void *data, size_t len)
{
	modify();
	memcpy(buffer->add_at_end(len), data, len);
}

void
net_buf::prepend(const void *data, size_t len)
{
	modify();
	memcpy(buffer->add_at_start(len), data, len);
}

void *
net_buf::add_at_start(size_t len)
{
	modify();
	return buffer->add_at_start(len);
}

void *
net_buf::add_at_end(size_t len)
{
	modify();
	return buffer->add_at_end(len);
}

void
net_buf::remove_from_start(size_t len)
{
	modify();
	buffer->remove_from_start(len);
}

void
net_buf::remove_from_end(size_t len)
{
	modify();
	buffer->remove_from_end(len);
}


size_t
net_buf::read(void *ptr, size_t size, size_t nmemb) const
{
	size_t todo = size * nmemb;
	size_t avail = length() - buffer->pos;

	todo = std::min(avail, todo);
	memcpy(ptr, buffer->start + buffer->pos, todo);

	buffer->pos += todo;

	return todo;
}

size_t
net_buf::write(const void *ptr, size_t size, size_t nmemb)
{
	size_t todo = size * nmemb;

	append(ptr, todo);

	return todo;
}

bool
net_buf::eof(void) const
{
	return (length() - buffer->pos) ? true : false;
}

void
net_buf::clear(void)
{
	modify();
	buffer->clear();
}

int
net_buf::vprintf(const char *fmt, va_list args)
{
	size_t oldlength = length();

	modify();
	buffer->vprintf(fmt, args);

	return (length() - oldlength);
}

int
net_buf::get(void) const
{
	size_t avail = length() - buffer->pos;
	int c = EOF;

	if (avail)
		c = *(buffer->start + buffer->pos++);

	return c;
}

} /* namespace lib */
