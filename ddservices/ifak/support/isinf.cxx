
/* 
 * $Id: isinf.cxx,v 1.1 2008/10/09 13:56:48 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <float.h>

// C++ stdlib

// own header
#include "port.h"


namespace lib
{

#ifdef WIN32
WINDLL_LIB int
isinf(double x)
{
	int ret = 0;

	switch (_fpclass(x))
	{
		case _FPCLASS_NINF: ret = -1; break;
		case _FPCLASS_PINF: ret =  1; break;
	}

	return ret;
}
#endif

} /* namespace lib */
