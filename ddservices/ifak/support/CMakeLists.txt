#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    Compiler Options #######################################################
#############################################################################
add_compile_options(${FLUKE_IGNORE_COMPILE_WARNINGS})

#############################################################################
#    include the source files ###############################################
#############################################################################
set (SUPPORT_SRC    ${CMAKE_CURRENT_SOURCE_DIR}/absdate.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/clean_path.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/ilog.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/nlz.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/resolve_hostname.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strtok.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/address_comparator.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/currtimestr.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/IniFile.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/nstream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/reuse.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strtoxx.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/AddressString.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/dirname.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/IniParser.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/paths.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/scan_buf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strtrim.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/auto_buf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/dyn_buf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/isinf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/pathseparator.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/scratch_buf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strupr.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/basename.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/file_exist.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/is_pathseparator.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/pca_conv.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/sstream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/supp_smart_ptr.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/basename_with_sep.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/find_end.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/is_point_special.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/pop.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/stream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/tmpfile.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/charset.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/fstream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/Logger.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/position.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/stricmp.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/vbitset.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/check_date.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/ftoa.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/mpstream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/replace_all.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strip_extension.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/check_extension.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/get_extension.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/mstream.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/replace_if_changed.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strlwr.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/check_fit_float.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/ident.cxx net_buf.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/resolve_address.cxx
                    ${CMAKE_CURRENT_SOURCE_DIR}/strrev.cxx)

#############################################################################
#    include the header files ###############################################
#############################################################################
include_directories (${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/edd
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/osservice
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/support
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/sys
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/profibus
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/stackmachine
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/ucpp
                     ${CMAKE_PROSTAR_SRC_DIR}/clogger)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(support OBJECT ${SUPPORT_SRC})
