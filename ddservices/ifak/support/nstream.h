
/* 
 * $Id: nstream.h,v 1.10 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for my idea of I/O streams.
 */

#ifndef _lib_nstream_h
#define _lib_nstream_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib

// own header
#include "stream.h"


namespace lib
{

/**
 * null stream.
 */
class WINDLL_LIB nstream : public stream
{
public:
	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

	virtual int get(void) const;
};

} /* namespace lib */

#endif /* _lib_nstream_h */
