
/* 
 * $Id: supp_smart_ptr.h,v 1.4 2006/04/10 13:42:46 fna Exp $
 *
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a smart pointer template class.
 */

#ifndef _lib_supp_smart_ptr_h
#define _lib_supp_smart_ptr_h

#include "lib_config.h"

// C stdlib
#include <assert.h>
#include <stdlib.h>

// C++ stdlib
#include <stdexcept>
#include <memory>

// own libs

// own header


namespace lib
{

class WINDLL_LIB supp_smart_ptr_base;

class supp_smart_ptr_help
{
protected:
	static inline void ref(class supp_smart_ptr_base *);
	static inline void unref(class supp_smart_ptr_base *);
};

/**
 * supported smart pointer.
 */
template<typename T>
class supp_smart_ptr : public supp_smart_ptr_help
{
public:
	~supp_smart_ptr() { unref(ptr); }

	supp_smart_ptr(T *p = NULL) : ptr(p) { ref(ptr); }
	explicit supp_smart_ptr(std::auto_ptr<T> p) : ptr(p.get()) { p.release(); ref(ptr); }

	/* Copy constructor. */
	supp_smart_ptr(const supp_smart_ptr<T> &p) : ptr(p.ptr) { ref(ptr); }

	/* Assignment constructor. */
	supp_smart_ptr<T>& operator=(const supp_smart_ptr<T> &p) { reset(p.ptr); return *this; }
	supp_smart_ptr<T>& operator=(T *p) { reset(p); return *this; }

	void reset(T *p = NULL) { ref(p); unref(ptr); ptr = p; }

	T& operator*()
	{
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return *ptr;
	}
	const T& operator*() const
	{
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return *ptr;
	}

	T* operator->()
	{
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return ptr;
	}
	const T* operator->() const
	{
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return ptr;
	}

	T* get() const { return ptr; }

	operator bool() const throw() { return ptr != NULL; }

	bool operator==(const supp_smart_ptr<T> &p) const throw() { return (ptr == p.ptr); }
	bool operator!=(const supp_smart_ptr<T> &p) const throw() { return (ptr != p.ptr); }

	/* fulfill strict weak ordering */
	bool operator< (const supp_smart_ptr<T> &p) const throw() { return (ptr <  p.ptr); }

protected:
	T *ptr;
};

class WINDLL_LIB supp_smart_ptr_base
{
public:
	supp_smart_ptr_base(void);
	virtual ~supp_smart_ptr_base(void);

protected:
	friend class supp_smart_ptr_help;
	size_t refs;
};

inline void
supp_smart_ptr_help::ref(class supp_smart_ptr_base *ptr)
{
	if (ptr)
		ptr->refs++;
}

inline void
supp_smart_ptr_help::unref(class supp_smart_ptr_base *ptr)
{
	if (ptr && --ptr->refs == 0)
		delete ptr;
}

} /* namespace lib */

#endif /* _lib_supp_smart_ptr_h */
