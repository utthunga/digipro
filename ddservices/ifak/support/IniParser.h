
/* 
 * $Id: IniParser.h,v 1.11 2009/07/31 11:07:21 mmeier Exp $
 * 
 * Copyright (C) 2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_IniParser_h
#define _lib_IniParser_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <list>
#include <map>
#include <string>
#include <vector>

// other libs

// own header
#include "stream.h"
#include "supp_smart_ptr.h"


namespace lib
{

class WINDLL_LIB IniParser
{
private:
	typedef IniParser Self;

	/* no copy/assignment constructor semantic defined */
	IniParser(const Self &);
	Self& operator=(const Self &);

public:
	virtual ~IniParser(void);

	bool modified(void) const throw() { return modified_; }
	void set_modified(void) throw();

	bool check_section(const std::string &section) const;
	bool check_key(const std::string &section, const std::string &key) const;

	std::vector<std::string> get_section(const std::string &section) const;
	void clear_section(const std::string &section);

	std::string get_key(const std::string &section, const std::string &key, const std::string &def = std::string()) const;
	void del_key(const std::string &section, const std::string &key);
	void set_key(const std::string &section, const std::string &key, const std::string &value, bool qoute = false);
	void set_key(const std::string &section, const std::string &key, int value, const char *fmt = "%i");
	void set_key(const std::string &section, const std::string &key, bool value);

	/* helper */
	bool get_bool_key(const std::string &section, const std::string &key, bool def = false) const;
	int get_int_key(const std::string &section, const std::string &key, int def = 0) const;

protected:
	IniParser(void);

	void load(const class stream &, class stream *log);
	void save(class stream &);

	virtual void on_modified(const std::string &section, const std::string &key);

private:
	/** the next input character */
	int c;

	class entry
	{
	public:
		std::string comments;
		std::string key;
		std::string value;
		std::string comment;
		bool quote;

		std::string get_key(void) const { return key; }

		bool operator==(const std::string &v) const { return (v == key); }
	};
	typedef std::list<class entry> entries_t;

	class save_entry
	{
	public:	
		save_entry(class stream &_f) : f(_f) { }
		void operator()(const class entry &) const;

	private:
		class stream &f;
	};

	class section : public supp_smart_ptr_base
	{
	public:
		std::string comments;
		std::string name;
		entries_t entries;

		bool operator==(const std::string &v) const { return (v == name); }
	};
	typedef supp_smart_ptr<class section> sections_ptr;
	typedef std::list<sections_ptr> sections_pos_t;
	typedef std::map<std::string, sections_ptr> sections_t;

	class save_section
	{
	public:	
		save_section(class stream &_f) : f(_f) { }
		void operator()(const sections_ptr &) const;

	private:
		class stream &f;
	};

	/** The parsed ini file. */
	sections_pos_t sections_pos;
	sections_t sections;
	/** The last commentlines. */
	std::string last_comments;

	/* modified flag */
	bool modified_;

	sections_ptr find_section(const std::string &name);
	const sections_ptr find_section(const std::string &name) const;

	entries_t::iterator find_entry(const std::string &, entries_t &);
	entries_t::const_iterator find_entry(const std::string &, const entries_t &) const;

	void new_section(const std::string &name, const std::string &comments = std::string());
	void new_entry(const std::string &section, const entry &entry);

	/* main parser */
	void parse(const class stream &f, class stream *log);

	/* helper */
	void next_valid_line(const class stream &f, std::string &str, std::string &skipped);
	void skip_space(const class stream &f, std::string &skipped);
	void skip_line(const class stream &f, std::string &skipped);
	bool check_eol(const class stream &f);
};

} /* namespace lib */

#endif /* _lib_IniParser_h */
