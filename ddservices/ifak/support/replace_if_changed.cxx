
/* 
 * $Id: replace_if_changed.cxx,v 1.2 2008/10/03 22:13:07 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <memory>

// own libs
#include "sys.h"

// own header
#include "replace_if_changed.h"
#include "fstream.h"


namespace lib
{

namespace
{

void
skip_line(class stream &f)
{
	int c;

	c = f.get();
	while (c != EOF && c != '\n')
		c = f.get();
}

bool
equal(const std::string &name1, const std::string &name2, int headskip)
{
	std::auto_ptr<class stream> f1(fstream_open(name1, "r"));
	std::auto_ptr<class stream> f2(fstream_open(name2, "r"));
	bool ret = false;

	if (f1.get() && f2.get())
	{
		int i;

		for (i = 0; i < headskip; i++)
		{
			skip_line(*f1);
			skip_line(*f2);
		}

		int c1 = f1->get();
		int c2 = f2->get();

		while (c1 != EOF && c2 != EOF)
		{
			if (c1 != c2)
				break;

			c1 = f1->get();
			c2 = f2->get();
		}

		if (c1 == c2)
			ret = true;
	}

	return ret;
}

} /* namespace anonymous */

WINDLL_LIB bool
replace_if_changed(const std::string &newfile, const std::string &oldfile, int headskip)
{
	bool ret = !equal(newfile, oldfile, headskip);

	if (ret)
		sys::rename(newfile, oldfile);
	else
		sys::unlink(newfile);

	return ret;
}

} /* namespace lib */
