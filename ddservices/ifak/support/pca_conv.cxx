
/* 
 * $Id: pca_conv.cxx,v 1.4 2006/02/01 09:30:18 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own header
#include "pca_conv.h"


namespace lib
{

static void
pca2str1(std::string &dst, const char *src, size_t length)
{
	unsigned int i = 0;
	
	/* step 1: Unpack the four 6bit ASCII characters
	 */
	while (length >= 3)
	{
		dst[i++] = ((src[0] & 0xfc) >> 2)                         ;
		dst[i++] = ((src[0] & 0x03) << 4) | ((src[1] & 0xf0) >> 4);
		dst[i++] = ((src[1] & 0x0f) << 2) | ((src[2] & 0xc0) >> 6);
		dst[i++] = ((src[2] & 0x3f)     )                         ;
		
		src += 3;
		length -= 3;
	}
	
	/* or alternativly reject this as invalid octet string */
	switch (length)
	{
		case 1:
			dst[i++] = ((src[0] & 0xfc) >> 2)                         ;
			break;
		case 2:
			dst[i++] = ((src[0] & 0xfc) >> 2)                         ;
			dst[i++] = ((src[0] & 0x03) << 4) | ((src[1] & 0xf0) >> 4);
			break;
	}
	
	/* step 2: place the complement of bit 5 into bit 6 for each
	 *         unpacked ASCII character
	 */
	{
		const unsigned int max = i;
		
		for (i = 0; i < max; ++i)
		{
			if (dst[i] < 0x20)
				dst[i] |= 0x40;
		}
	}
	
	/* step 3: zero bit 7 of each unpacked ASCII character
	 */
}

WINDLL_LIB std::string
pca2str(const char *src, size_t length)
{
	const unsigned long dst_length = ((length + 2) / 3) * 4;
	std::string dst;

	dst.resize(dst_length);
	pca2str1(dst, src, length);

	return dst;
}

static void
str2pca1(std::string &dst, const char *src, size_t length)
{
	unsigned int i = 0;
	
	/* step 1: truncate bit 6 and 7 of each ASCII character
	 * step 2: pack four 6bit ASCII character into free octects
	 */
	
	while (length >= 4)
	{
		assert(src[0] && src[1] && src[2] && src[3]);
		
		dst[i++] = ((src[0] & 0x3f) << 2) | ((src[1] & 0x30) >> 4);
		dst[i++] = ((src[1] & 0x0f) << 4) | ((src[2] & 0x3c) >> 2);
		dst[i++] = ((src[2] & 0x03) << 6) | ((src[3] & 0x3f)     );
		
		src += 4;
		length -= 4;
	}
	
	/* pad with spaces */
	switch (length)
	{
		case 1:
			assert(src[0]);
			
			dst[i++] = ((src[0] & 0x3f) << 2) | ((' '    & 0x30) >> 4);
			dst[i++] = ((' '    & 0x0f) << 4) | ((' '    & 0x3c) >> 2);
			dst[i]   = ((' '    & 0x03) << 6) | ((' '    & 0x3f)     );
			break;
		case 2:
			assert(src[0] && src[1]);
			
			dst[i++] = ((src[0] & 0x3f) << 2) | ((src[1] & 0x30) >> 4);
			dst[i++] = ((src[1] & 0x0f) << 4) | ((' '    & 0x3c) >> 2);
			dst[i]   = ((' '    & 0x03) << 6) | ((' '    & 0x3f)     );
			break;
		case 3:
			assert(src[0] && src[1] && src[2]);
			
			dst[i++] = ((src[0] & 0x3f) << 2) | ((src[1] & 0x30) >> 4);
			dst[i++] = ((src[1] & 0x0f) << 4) | ((src[2] & 0x3c) >> 2);
			dst[i]   = ((src[2] & 0x03) << 6) | ((' '    & 0x3f)     );
			break;
	}
}

WINDLL_LIB std::string
str2pca(const char *src, size_t length)
{
	const unsigned long dst_length = ((length + 3) / 4) * 3;
	std::string dst;

	dst.resize(dst_length);
	str2pca1(dst, src, length);

	return dst;
}

} /* namespace lib */
