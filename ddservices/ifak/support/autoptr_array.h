
/* 
 * $Id: autoptr_array.h,v 1.7 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a small auto allocating/deallocating buffer.
 */

#ifndef _lib_autoptr_array_h
#define _lib_autoptr_array_h

#include "lib_config.h"

// C stdlib
#include <stdlib.h>

// C++ stdlib
#include <memory>
#include <stdexcept>

// own libs

// own header
#include "auto_array.h"


namespace lib
{

template<typename T>
class autoptr_array
{
public:
	explicit autoptr_array(size_t s)
	: mysize(s)
	, buf(mysize ? new std::auto_ptr<T>[mysize] : NULL)
	{ }

	explicit autoptr_array(const auto_array<T *> &a)
	: mysize(a.size())
	, buf(mysize ? new std::auto_ptr<T>[mysize] : NULL)
	{
		T **data = a.data();

		for (size_t i = 0; i < mysize; i++)
			buf[i].reset(data[i]);
	}

	~autoptr_array(void) { delete [] buf; }

	std::auto_ptr<T>& operator[](size_t index) const
	{
		if (index < mysize)
			return buf[index];

		throw std::out_of_range("invalid index");
	}
	size_t size(void) const throw() { return mysize; }

protected:
	const size_t mysize;
	std::auto_ptr<T> *buf;

private:
	/* disallow */
	autoptr_array(const autoptr_array &);
	class autoptr_array& operator=(const autoptr_array &);
};

template<typename T>
class auto_array_from_autoptr_array : public auto_array<T *>
{
public:
	explicit auto_array_from_autoptr_array(const class autoptr_array<T> &a)
	: auto_array<T *>(a.size())
	{
		for (size_t i = 0; i < this->mysize; i++)
			buf[i] = a[i].get();
	}

protected:
	using auto_array<T *>::buf;
};

} /* namespace lib */

#endif /* _lib_autoptr_array_h */
