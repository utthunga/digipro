
/* 
 * $Id: strlwr.cxx,v 1.3 2006/02/01 09:30:18 fna Exp $
 *
 * Copyright (C) 2002-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <ctype.h>

// C++ stdlib
#include <string.h>

// own header
#include "string_edd.h"


namespace lib
{

WINDLL_LIB std::string
strlwr(std::string &str)
{
	for (size_t i = 0, i_max = str.size(); i < i_max; ++i)
	{
		int c = str[i];

		if (isupper(c))
			str[i] = tolower(c);
	}
	return str;
}

} /* namespace lib */
