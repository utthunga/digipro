
/* 
 * $Id: auto_buf.h,v 1.9 2007/06/22 09:34:44 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a small auto allocating/deallocating buffer.
 */

#ifndef _lib_auto_buf_h
#define _lib_auto_buf_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib

// own libs

// own header


namespace lib
{

/**
 * scratch buffer.
 */
class WINDLL_LIB auto_buf
{
public:
	auto_buf(size_t s);
	~auto_buf(void);
	
	/* Copy constructor. */
	auto_buf(const auto_buf &);
	
	/* Assignment constructor. */
	class auto_buf& operator=(const auto_buf &);
	
#if defined(_MSC_VER) && defined(_DEBUG)
	/**
	 * Get attached buffer.
	 * Return the pointer to the managed buffer.
	 */
	char *data(void) const throw(); /* don't ask me why */
#else
	/**
	 * Get attached buffer.
	 * Return the pointer to the managed buffer.
	 */
	inline char *data(void) const throw() { return buf_; }
#endif
	/**
	 * Get data length.
	 * Return the auto_buf size.
	 */
	inline size_t length(void) const throw() { return size_; }
	/**
	 * Get data length.
	 * Return the auto_buf size.
	 */
	inline size_t size(void) const throw() { return size_; }
	
private:
	size_t size_;
	char *buf_;
};

} /* namespace lib */

#endif /* _lib_auto_buf_h */
