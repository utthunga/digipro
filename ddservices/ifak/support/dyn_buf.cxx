
/* 
 * $Id: dyn_buf.cxx,v 1.12 2009/02/20 21:14:35 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib

// own libs

// own header
#include "dyn_buf.h"
#include "port.h"


namespace lib
{

dyn_buf::dyn_buf(void)
: size(1024)
, space(size - 1)
, buf(new char[size])
, ptr(buf)
{
}

dyn_buf::dyn_buf(size_t s)
: size(s)
, space(size - 1)
, buf(new char[size])
, ptr(buf)
{
}

dyn_buf::dyn_buf(const dyn_buf &t)
: size(t.size)
, space(size - 1)
, buf(new char[size])
, ptr(buf)
{
	put(t.data(), t.length());
}

dyn_buf::~dyn_buf(void)
{
	delete [] buf;
}

void
dyn_buf::resize(size_t min)
{
	const size_t len = length();

	size_t newsize = (size << 1);
	char *nbuf;

	while ((newsize - size) < min)
		newsize <<= 1;

	// allocate
	nbuf = new char[newsize];
	assert(nbuf);

	// copy over
	memcpy(nbuf, buf, len);

	// adjust current ptr
	ptr = nbuf + len;

	// delete old buffer
	delete [] buf;
	buf = nbuf;

	// adjust free space and size
	space += (newsize - size);
	size = newsize;
}

void
dyn_buf::vprintf(const char *fmt, va_list args)
{
#ifdef va_copy
	va_list copied_args;
	int ret;

restart:
	va_copy(copied_args, args);
	ret = lib::vsnprintf(ptr, space, fmt, copied_args);
	va_end(copied_args);
#else
	int ret;

restart:
	ret = lib::vsnprintf(ptr, space, fmt, args);
#endif

	if (static_cast<size_t>(ret) >= space)
	{
		/* overflow */
		resize();
		goto restart;
	}
	else if (ret < 0)
	{
		/* failed ??? -> exception ??? */
		ret = 0;
	}

	ptr += ret;
	space -= ret;
}

void
dyn_buf::printf(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
}

void
dyn_buf::put(const char *str, size_t len)
{
	if (len > space)
	{
		resize(len);
		assert(len <= space);
	}

	memcpy(ptr, str, len);

	ptr += len;
	space -= len;
}

void
dyn_buf::del(size_t len)
{
	size_t used = ptr - buf;

	len = std::min(len, used);

	space += len;
	ptr -= len;
}

std::string
dyn_buf::str(void) const
{
	return std::string(buf, length());
}

class dyn_buf& dyn_buf::operator=(const dyn_buf &t)
{
        if (this != &t)
        {
                clear();
		put(t.data(), t.length());
        }
        
        return *this;
}

bool
dyn_buf::operator!=(const dyn_buf &t) const
{
	size_t len = length();

	if (len == t.length())
		return (memcmp(buf, t.buf, len) != 0);

	return true;
}

} /* namespace lib */
