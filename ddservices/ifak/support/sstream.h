
/* 
 * $Id: sstream.h,v 1.4 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for my idea of I/O streams.
 */

#ifndef _lib_sstream_h
#define _lib_sstream_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>

// C++ stdlib

// own header
#include "stream.h"


namespace lib
{

/**
 * string stream.
 */
class WINDLL_LIB sstream : public stream
{
public:
	sstream(void);
	explicit sstream(size_t start_size);
	explicit sstream(const std::string &);

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &str);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

	virtual int get(void) const;

	const std::string &data(void) const throw() { return buf; }

private:
	std::string buf;
	mutable off_t pos;
};

} /* namespace lib */

#endif /* _lib_sstream_h */
