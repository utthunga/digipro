
/* 
 * $Id: stream.h,v 1.11 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for my idea of I/O streams.
 */

#ifndef _lib_stream_h
#define _lib_stream_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>
#include <stdarg.h>

// C++ stdlib
#include <string>

// own header


namespace lib
{

/**
 * Stream abstraction.
 * This is the generic interface for uni- or bidirectional streams.
 * It's used by scanners, I/O routines, debug routines and so on. There exist
 * specializations that map to the C FILE interface and to memory at the
 * moment.
 */
class WINDLL_LIB stream
{
private:
	typedef stream Self;

	/* no copy/assignment constructor semantic defined */
	stream(const Self &);
	Self& operator=(const Self &);

public:
	stream(void) { }
	virtual ~stream(void);

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const = 0;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb) = 0;

	virtual bool eof(void) const = 0;
	virtual void clear(void) = 0;
	virtual void flush(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0) = 0;
	virtual int printf(const char *fmt, ...) CHECK_PRINTF(2, 3);

	virtual void put(const std::string &);
	virtual void put(const char *str, size_t len);
	virtual void put(const char *str);
	virtual void put(const char c);

	virtual int get(void) const;
};

} /* namespace lib */

#endif /* _lib_stream_h */
