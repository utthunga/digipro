
/* 
 * $Id: charset.cxx,v 1.5 2009/09/22 13:28:27 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own header
#include "charset.h"

#include "dyn_buf.h"
#include "scratch_buf.h"


namespace lib
{

WINDLL_LIB char iso_latin_1[256] =
{
	/* dec | oct | hex */
	/* ----+-----+---- */
	/*   0 |   0 |   0 */	false,
	/*   1 |   1 |   1 */	false,
	/*   2 |   2 |   2 */	false,
	/*   3 |   3 |   3 */	false,
	/*   4 |   4 |   4 */	false,
	/*   5 |   5 |   5 */	false,
	/*   6 |   6 |   6 */	false,
	/*   7 |   7 |   7 */	false,
	/*   8 |  10 |   8 */	false,
	/*   9 |  11 |   9 */	false,
	/*  10 |  12 |   a */	false,
	/*  11 |  13 |   b */	false,
	/*  12 |  14 |   c */	false,
	/*  13 |  15 |   d */	false,
	/*  14 |  16 |   e */	false,
	/*  15 |  17 |   f */	false,
	/*  16 |  20 |  10 */	false,
	/*  17 |  21 |  11 */	false,
	/*  18 |  22 |  12 */	false,
	/*  19 |  23 |  13 */	false,
	/*  20 |  24 |  14 */	false,
	/*  21 |  25 |  15 */	false,
	/*  22 |  26 |  16 */	false,
	/*  23 |  27 |  17 */	false,
	/*  24 |  30 |  18 */	false,
	/*  25 |  31 |  19 */	false,
	/*  26 |  32 |  1a */	false,
	/*  27 |  33 |  1b */	false,
	/*  28 |  34 |  1c */	false,
	/*  29 |  35 |  1d */	false,
	/*  30 |  36 |  1e */	false,
	/*  31 |  37 |  1f */	false,
	/*  32 |  40 |  20 */	true,
	/*  33 |  41 |  21 */	true,
	/*  34 |  42 |  22 */	true,
	/*  35 |  43 |  23 */	true,
	/*  36 |  44 |  24 */	true,
	/*  37 |  45 |  25 */	true,
	/*  38 |  46 |  26 */	true,
	/*  39 |  47 |  27 */	true,
	/*  40 |  50 |  28 */	true,
	/*  41 |  51 |  29 */	true,
	/*  42 |  52 |  2a */	true,
	/*  43 |  53 |  2b */	true,
	/*  44 |  54 |  2c */	true,
	/*  45 |  55 |  2d */	true,
	/*  46 |  56 |  2e */	true,
	/*  47 |  57 |  2f */	true,
	/*  48 |  60 |  30 */	true,
	/*  49 |  61 |  31 */	true,
	/*  50 |  62 |  32 */	true,
	/*  51 |  63 |  33 */	true,
	/*  52 |  64 |  34 */	true,
	/*  53 |  65 |  35 */	true,
	/*  54 |  66 |  36 */	true,
	/*  55 |  67 |  37 */	true,
	/*  56 |  70 |  38 */	true,
	/*  57 |  71 |  39 */	true,
	/*  58 |  72 |  3a */	true,
	/*  59 |  73 |  3b */	true,
	/*  60 |  74 |  3c */	true,
	/*  61 |  75 |  3d */	true,
	/*  62 |  76 |  3e */	true,
	/*  63 |  77 |  3f */	true,
	/*  64 | 100 |  40 */	true,
	/*  65 | 101 |  41 */	true,
	/*  66 | 102 |  42 */	true,
	/*  67 | 103 |  43 */	true,
	/*  68 | 104 |  44 */	true,
	/*  69 | 105 |  45 */	true,
	/*  70 | 106 |  46 */	true,
	/*  71 | 107 |  47 */	true,
	/*  72 | 110 |  48 */	true,
	/*  73 | 111 |  49 */	true,
	/*  74 | 112 |  4a */	true,
	/*  75 | 113 |  4b */	true,
	/*  76 | 114 |  4c */	true,
	/*  77 | 115 |  4d */	true,
	/*  78 | 116 |  4e */	true,
	/*  79 | 117 |  4f */	true,
	/*  80 | 120 |  50 */	true,
	/*  81 | 121 |  51 */	true,
	/*  82 | 122 |  52 */	true,
	/*  83 | 123 |  53 */	true,
	/*  84 | 124 |  54 */	true,
	/*  85 | 125 |  55 */	true,
	/*  86 | 126 |  56 */	true,
	/*  87 | 127 |  57 */	true,
	/*  88 | 130 |  58 */	true,
	/*  89 | 131 |  59 */	true,
	/*  90 | 132 |  5a */	true,
	/*  91 | 133 |  5b */	true,
	/*  92 | 134 |  5c */	true,
	/*  93 | 135 |  5d */	true,
	/*  94 | 136 |  5e */	true,
	/*  95 | 137 |  5f */	true,
	/*  96 | 140 |  60 */	true,
	/*  97 | 141 |  61 */	true,
	/*  98 | 142 |  62 */	true,
	/*  99 | 143 |  63 */	true,
	/* 100 | 144 |  64 */	true,
	/* 101 | 145 |  65 */	true,
	/* 102 | 146 |  66 */	true,
	/* 103 | 147 |  67 */	true,
	/* 104 | 150 |  68 */	true,
	/* 105 | 151 |  69 */	true,
	/* 106 | 152 |  6a */	true,
	/* 107 | 153 |  6b */	true,
	/* 108 | 154 |  6c */	true,
	/* 109 | 155 |  6d */	true,
	/* 110 | 156 |  6e */	true,
	/* 111 | 157 |  6f */	true,
	/* 112 | 160 |  70 */	true,
	/* 113 | 161 |  71 */	true,
	/* 114 | 162 |  72 */	true,
	/* 115 | 163 |  73 */	true,
	/* 116 | 164 |  74 */	true,
	/* 117 | 165 |  75 */	true,
	/* 118 | 166 |  76 */	true,
	/* 119 | 167 |  77 */	true,
	/* 120 | 170 |  78 */	true,
	/* 121 | 171 |  79 */	true,
	/* 122 | 172 |  7a */	true,
	/* 123 | 173 |  7b */	true,
	/* 124 | 174 |  7c */	true,
	/* 125 | 175 |  7d */	true,
	/* 126 | 176 |  7e */	true,
	/* 127 | 177 |  7f */	false,
	/* 128 | 200 |  80 */	false,
	/* 129 | 201 |  81 */	false,
	/* 130 | 202 |  82 */	false,
	/* 131 | 203 |  83 */	false,
	/* 132 | 204 |  84 */	false,
	/* 133 | 205 |  85 */	false,
	/* 134 | 206 |  86 */	false,
	/* 135 | 207 |  87 */	false,
	/* 136 | 210 |  88 */	false,
	/* 137 | 211 |  89 */	false,
	/* 138 | 212 |  8a */	false,
	/* 139 | 213 |  8b */	false,
	/* 140 | 214 |  8c */	false,
	/* 141 | 215 |  8d */	false,
	/* 142 | 216 |  8e */	false,
	/* 143 | 217 |  8f */	false,
	/* 144 | 220 |  90 */	false,
	/* 145 | 221 |  91 */	false,
	/* 146 | 222 |  92 */	false,
	/* 147 | 223 |  93 */	false,
	/* 148 | 224 |  94 */	false,
	/* 149 | 225 |  95 */	false,
	/* 150 | 226 |  96 */	false,
	/* 151 | 227 |  97 */	false,
	/* 152 | 230 |  98 */	false,
	/* 153 | 231 |  99 */	false,
	/* 154 | 232 |  9a */	false,
	/* 155 | 233 |  9b */	false,
	/* 156 | 234 |  9c */	false,
	/* 157 | 235 |  9d */	false,
	/* 158 | 236 |  9e */	false,
	/* 159 | 237 |  9f */	false,
	/* 160 | 240 |  a0 */	true,
	/* 161 | 241 |  a1 */	true,
	/* 162 | 242 |  a2 */	true,
	/* 163 | 243 |  a3 */	true,
	/* 164 | 244 |  a4 */	true,
	/* 165 | 245 |  a5 */	true,
	/* 166 | 246 |  a6 */	true,
	/* 167 | 247 |  a7 */	true,
	/* 168 | 250 |  a8 */	true,
	/* 169 | 251 |  a9 */	true,
	/* 170 | 252 |  aa */	true,
	/* 171 | 253 |  ab */	true,
	/* 172 | 254 |  ac */	true,
	/* 173 | 255 |  ad */	true,
	/* 174 | 256 |  ae */	true,
	/* 175 | 257 |  af */	true,
	/* 176 | 260 |  b0 */	true,
	/* 177 | 261 |  b1 */	true,
	/* 178 | 262 |  b2 */	true,
	/* 179 | 263 |  b3 */	true,
	/* 180 | 264 |  b4 */	true,
	/* 181 | 265 |  b5 */	true,
	/* 182 | 266 |  b6 */	true,
	/* 183 | 267 |  b7 */	true,
	/* 184 | 270 |  b8 */	true,
	/* 185 | 271 |  b9 */	true,
	/* 186 | 272 |  ba */	true,
	/* 187 | 273 |  bb */	true,
	/* 188 | 274 |  bc */	true,
	/* 189 | 275 |  bd */	true,
	/* 190 | 276 |  be */	true,
	/* 191 | 277 |  bf */	true,
	/* 192 | 300 |  c0 */	true,
	/* 193 | 301 |  c1 */	true,
	/* 194 | 302 |  c2 */	true,
	/* 195 | 303 |  c3 */	true,
	/* 196 | 304 |  c4 */	true,
	/* 197 | 305 |  c5 */	true,
	/* 198 | 306 |  c6 */	true,
	/* 199 | 307 |  c7 */	true,
	/* 200 | 310 |  c8 */	true,
	/* 201 | 311 |  c9 */	true,
	/* 202 | 312 |  ca */	true,
	/* 203 | 313 |  cb */	true,
	/* 204 | 314 |  cc */	true,
	/* 205 | 315 |  cd */	true,
	/* 206 | 316 |  ce */	true,
	/* 207 | 317 |  cf */	true,
	/* 208 | 320 |  d0 */	true,
	/* 209 | 321 |  d1 */	true,
	/* 210 | 322 |  d2 */	true,
	/* 211 | 323 |  d3 */	true,
	/* 212 | 324 |  d4 */	true,
	/* 213 | 325 |  d5 */	true,
	/* 214 | 326 |  d6 */	true,
	/* 215 | 327 |  d7 */	true,
	/* 216 | 330 |  d8 */	true,
	/* 217 | 331 |  d9 */	true,
	/* 218 | 332 |  da */	true,
	/* 219 | 333 |  db */	true,
	/* 220 | 334 |  dc */	true,
	/* 221 | 335 |  dd */	true,
	/* 222 | 336 |  de */	true,
	/* 223 | 337 |  df */	true,
	/* 224 | 340 |  e0 */	true,
	/* 225 | 341 |  e1 */	true,
	/* 226 | 342 |  e2 */	true,
	/* 227 | 343 |  e3 */	true,
	/* 228 | 344 |  e4 */	true,
	/* 229 | 345 |  e5 */	true,
	/* 230 | 346 |  e6 */	true,
	/* 231 | 347 |  e7 */	true,
	/* 232 | 350 |  e8 */	true,
	/* 233 | 351 |  e9 */	true,
	/* 234 | 352 |  ea */	true,
	/* 235 | 353 |  eb */	true,
	/* 236 | 354 |  ec */	true,
	/* 237 | 355 |  ed */	true,
	/* 238 | 356 |  ee */	true,
	/* 239 | 357 |  ef */	true,
	/* 240 | 360 |  f0 */	true,
	/* 241 | 361 |  f1 */	true,
	/* 242 | 362 |  f2 */	true,
	/* 243 | 363 |  f3 */	true,
	/* 244 | 364 |  f4 */	true,
	/* 245 | 365 |  f5 */	true,
	/* 246 | 366 |  f6 */	true,
	/* 247 | 367 |  f7 */	true,
	/* 248 | 370 |  f8 */	true,
	/* 249 | 371 |  f9 */	true,
	/* 250 | 372 |  fa */	true,
	/* 251 | 373 |  fb */	true,
	/* 252 | 374 |  fc */	true,
	/* 253 | 375 |  fd */	true,
	/* 254 | 376 |  fe */	true,
	/* 255 | 377 |  ff */	true
};

static struct { int major; int minor; } latin1_to_utf8[] =
{
	/* 160 | 240 |  a0 */	{ 0xc2, 0xa0 },
	/* 161 | 241 |  a1 */	{ 0xc2, 0xa1 },
	/* 162 | 242 |  a2 */	{ 0xc2, 0xa2 },
	/* 163 | 243 |  a3 */	{ 0xc2, 0xa3 },
	/* 164 | 244 |  a4 */	{ 0xc2, 0xa4 },
	/* 165 | 245 |  a5 */	{ 0xc2, 0xa5 },
	/* 166 | 246 |  a6 */	{ 0xc2, 0xa6 },
	/* 167 | 247 |  a7 */	{ 0xc2, 0xa7 },
	/* 168 | 250 |  a8 */	{ 0xc2, 0xa8 },
	/* 169 | 251 |  a9 */	{ 0xc2, 0xa9 },
	/* 170 | 252 |  aa */	{ 0xc2, 0xaa },
	/* 171 | 253 |  ab */	{ 0xc2, 0xab },
	/* 172 | 254 |  ac */	{ 0xc2, 0xac },
	/* 173 | 255 |  ad */	{ 0xc2, 0xad },
	/* 174 | 256 |  ae */	{ 0xc2, 0xae },
	/* 175 | 257 |  af */	{ 0xc2, 0xaf },
	/* 176 | 260 |  b0 */	{ 0xc2, 0xb0 },
	/* 177 | 261 |  b1 */	{ 0xc2, 0xb1 },
	/* 178 | 262 |  b2 */	{ 0xc2, 0xb2 },
	/* 179 | 263 |  b3 */	{ 0xc2, 0xb3 },
	/* 180 | 264 |  b4 */	{ 0xc2, 0xb4 },
	/* 181 | 265 |  b5 */	{ 0xc2, 0xb5 },
	/* 182 | 266 |  b6 */	{ 0xc2, 0xb6 },
	/* 183 | 267 |  b7 */	{ 0xc2, 0xb7 },
	/* 184 | 270 |  b8 */	{ 0xc2, 0xb8 },
	/* 185 | 271 |  b9 */	{ 0xc2, 0xb9 },
	/* 186 | 272 |  ba */	{ 0xc2, 0xba },
	/* 187 | 273 |  bb */	{ 0xc2, 0xbb },
	/* 188 | 274 |  bc */	{ 0xc2, 0xbc },
	/* 189 | 275 |  bd */	{ 0xc2, 0xbd },
	/* 190 | 276 |  be */	{ 0xc2, 0xbe },
	/* 191 | 277 |  bf */	{ 0xc2, 0xbf },
	/* 192 | 300 |  c0 */	{ 0xc3, 0x80 },
	/* 193 | 301 |  c1 */	{ 0xc3, 0x81 },
	/* 194 | 302 |  c2 */	{ 0xc3, 0x82 },
	/* 195 | 303 |  c3 */	{ 0xc3, 0x83 },
	/* 196 | 304 |  c4 */	{ 0xc3, 0x84 },
	/* 197 | 305 |  c5 */	{ 0xc3, 0x85 },
	/* 198 | 306 |  c6 */	{ 0xc3, 0x86 },
	/* 199 | 307 |  c7 */	{ 0xc3, 0x87 },
	/* 200 | 310 |  c8 */	{ 0xc3, 0x88 },
	/* 201 | 311 |  c9 */	{ 0xc3, 0x89 },
	/* 202 | 312 |  ca */	{ 0xc3, 0x8a },
	/* 203 | 313 |  cb */	{ 0xc3, 0x8b },
	/* 204 | 314 |  cc */	{ 0xc3, 0x8c },
	/* 205 | 315 |  cd */	{ 0xc3, 0x8d },
	/* 206 | 316 |  ce */	{ 0xc3, 0x8e },
	/* 207 | 317 |  cf */	{ 0xc3, 0x8f },
	/* 208 | 320 |  d0 */	{ 0xc3, 0x90 },
	/* 209 | 321 |  d1 */	{ 0xc3, 0x91 },
	/* 210 | 322 |  d2 */	{ 0xc3, 0x92 },
	/* 211 | 323 |  d3 */	{ 0xc3, 0x93 },
	/* 212 | 324 |  d4 */	{ 0xc3, 0x94 },
	/* 213 | 325 |  d5 */	{ 0xc3, 0x95 },
	/* 214 | 326 |  d6 */	{ 0xc3, 0x96 },
	/* 215 | 327 |  d7 */	{ 0xc3, 0x97 },
	/* 216 | 330 |  d8 */	{ 0xc3, 0x98 },
	/* 217 | 331 |  d9 */	{ 0xc3, 0x99 },
	/* 218 | 332 |  da */	{ 0xc3, 0x9a },
	/* 219 | 333 |  db */	{ 0xc3, 0x9b },
	/* 220 | 334 |  dc */	{ 0xc3, 0x9c },
	/* 221 | 335 |  dd */	{ 0xc3, 0x9d },
	/* 222 | 336 |  de */	{ 0xc3, 0x9e },
	/* 223 | 337 |  df */	{ 0xc3, 0x9f },
	/* 224 | 340 |  e0 */	{ 0xc3, 0xa0 },
	/* 225 | 341 |  e1 */	{ 0xc3, 0xa1 },
	/* 226 | 342 |  e2 */	{ 0xc3, 0xa2 },
	/* 227 | 343 |  e3 */	{ 0xc3, 0xa3 },
	/* 228 | 344 |  e4 */	{ 0xc3, 0xa4 },
	/* 229 | 345 |  e5 */	{ 0xc3, 0xa5 },
	/* 230 | 346 |  e6 */	{ 0xc3, 0xa6 },
	/* 231 | 347 |  e7 */	{ 0xc3, 0xa7 },
	/* 232 | 350 |  e8 */	{ 0xc3, 0xa8 },
	/* 233 | 351 |  e9 */	{ 0xc3, 0xa9 },
	/* 234 | 352 |  ea */	{ 0xc3, 0xaa },
	/* 235 | 353 |  eb */	{ 0xc3, 0xab },
	/* 236 | 354 |  ec */	{ 0xc3, 0xac },
	/* 237 | 355 |  ed */	{ 0xc3, 0xad },
	/* 238 | 356 |  ee */	{ 0xc3, 0xae },
	/* 239 | 357 |  ef */	{ 0xc3, 0xaf },
	/* 240 | 360 |  f0 */	{ 0xc3, 0xb0 },
	/* 241 | 361 |  f1 */	{ 0xc3, 0xb1 },
	/* 242 | 362 |  f2 */	{ 0xc3, 0xb2 },
	/* 243 | 363 |  f3 */	{ 0xc3, 0xb3 },
	/* 244 | 364 |  f4 */	{ 0xc3, 0xb4 },
	/* 245 | 365 |  f5 */	{ 0xc3, 0xb5 },
	/* 246 | 366 |  f6 */	{ 0xc3, 0xb6 },
	/* 247 | 367 |  f7 */	{ 0xc3, 0xb7 },
	/* 248 | 370 |  f8 */	{ 0xc3, 0xb8 },
	/* 249 | 371 |  f9 */	{ 0xc3, 0xb9 },
	/* 250 | 372 |  fa */	{ 0xc3, 0xba },
	/* 251 | 373 |  fb */	{ 0xc3, 0xbb },
	/* 252 | 374 |  fc */	{ 0xc3, 0xbc },
	/* 253 | 375 |  fd */	{ 0xc3, 0xbd },
	/* 254 | 376 |  fe */	{ 0xc3, 0xbe },
	/* 255 | 377 |  ff */	{ 0xc3, 0xbf }
};

WINDLL_LIB std::string
iso_latin1_to_utf8(const char *src, size_t len)
{
	std::string ret;
	size_t i;

	ret.reserve(len + 16);

	for (i = 0; i < len; ++i)
	{
		int c = src[i] & 0xff;
		
		if (c > 0x7f)
		{
			if (c >= 160)
			{
				c -= 160;
				
				ret += latin1_to_utf8[c].major;
				ret += latin1_to_utf8[c].minor;
			}
			else
				ret += '?';
		}
		else
			ret += c;
	}

	return ret;
}

WINDLL_LIB std::string
iso_latin1_to_utf8(const char *src)
{
	return iso_latin1_to_utf8(src, strlen(src));
}

WINDLL_LIB std::string
iso_latin1_to_utf8(const std::string &src)
{
	return iso_latin1_to_utf8(src.c_str(), src.length());
}

WINDLL_LIB std::string
iso_latin1_to_utf8(const dyn_buf &src)
{
	return iso_latin1_to_utf8(src.c_str(), src.length());
}

WINDLL_LIB std::string
iso_latin1_to_utf8(const scratch_buf &src)
{
	return iso_latin1_to_utf8(src.c_str(), src.length());
}

} /* namespace lib */
