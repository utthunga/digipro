/* 
 * $Id: resolve_hostname.cxx,v 1.6 2010/02/25 10:07:08 rsc Exp $
 *
 * Copyright (C) 2010 Robert Schneckenhaus <trobt@web.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib
#include <stdexcept>

// own libs
#include "bsdsocket.h"

// own
#include "auto_buf.h"
#include "resolve_hostname.h"

#define NI_MAXIP_LEN	46


namespace lib
{

std::string
resolve_hostname(const std::string &name)
{
#ifdef __CYGWIN__
	assert(0);
#else
#ifdef SYSTEM_WIN32
	WSADATA wsaData;

	int wsaret = WSAStartup(MAKEWORD(1,1), &wsaData);
	assert(wsaret == 0);
#endif

	struct addrinfo *ai = NULL;
	std::string ret;

	sys::getaddrinfo(name.c_str(), NULL, NULL, &ai);

	if (ai)
	{
		lib::auto_buf buf(NI_MAXIP_LEN);

		sys::getnameinfo(ai->ai_addr, ai->ai_addrlen,
		 		buf.data(), buf.size(),
				NULL, 0,
				NI_NUMERICHOST);

		ret = buf.data();

		sys::freeaddrinfo(ai);
	}

#ifdef SYSTEM_WIN32
	WSACleanup();
#endif

	return ret;

#endif /* ifndef __CYGWIN__ */
}

} /* namespace sys */
