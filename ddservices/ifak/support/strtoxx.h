
/* 
 * $Id: strtoxx.h,v 1.6 2009/11/19 14:59:55 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_strtoxx_h
#define _lib_strtoxx_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header
#include "sys_types.h"
#include "vbitset.h"


namespace lib
{

WINDLL_LIB int bin_ascii_to_int(sys::int32 &, const char *, size_t);
WINDLL_LIB int hex_ascii_to_int(sys::int32 &, const char *, size_t);
WINDLL_LIB int dec_ascii_to_int(sys::int32 &, const char *, size_t);
WINDLL_LIB int oct_ascii_to_int(sys::int32 &, const char *, size_t);

WINDLL_LIB int bin_ascii_to_int64(sys::int64 &, const char *, size_t);
WINDLL_LIB int hex_ascii_to_int64(sys::int64 &, const char *, size_t);
WINDLL_LIB int dec_ascii_to_int64(sys::int64 &, const char *, size_t);
WINDLL_LIB int oct_ascii_to_int64(sys::int64 &, const char *, size_t);

WINDLL_LIB int bin_ascii_to_vbitset(class vbitset &, const char *, size_t);
WINDLL_LIB int hex_ascii_to_vbitset(class vbitset &, const char *, size_t);
WINDLL_LIB int dec_ascii_to_vbitset(class vbitset &, const char *, size_t);
WINDLL_LIB int oct_ascii_to_vbitset(class vbitset &, const char *, size_t);

} /* namespace lib */

#endif /* _lib_strtoxx_h */
