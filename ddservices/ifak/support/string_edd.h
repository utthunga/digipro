
/* 
 * $Id: string_edd.h,v 1.6 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_string_h
#define _lib_string_h

#include "lib_config.h"

// C stdlib
#include <string.h>

// C++ stdlib
#include <string>

// own header


namespace lib
{

#ifndef STR
# define STR(x) _STRINGIFY(x)
# define _STRINGIFY(x) #x
#endif

WINDLL_LIB std::string ftoa(const char *fmt, double) CHECK_PRINTF(1, 0);

WINDLL_LIB std::string strlwr(std::string &);
WINDLL_LIB std::string strrev(std::string &);
WINDLL_LIB std::string strtrim(const std::string &, const std::string &what = " \t\n\r");
WINDLL_LIB std::string strupr(std::string &);

WINDLL_LIB int stricmp(const char *, const char *);
WINDLL_LIB int stricmp(const std::string &, const std::string &);

WINDLL_LIB bool check_date(int day, int month, int year);

WINDLL_LIB std::string strtok(const std::string &, size_t &, const std::string &);

WINDLL_LIB void replace_all(std::string &, char, char, size_t pos = 0, size_t n = std::string::npos);
WINDLL_LIB void replace_all(std::string &, const std::string &, const std::string &, size_t pos = 0, size_t n = std::string::npos);

} /* namespace lib */

#endif /* _lib_string_h */
