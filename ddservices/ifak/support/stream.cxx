
/* 
 * $Id: stream.cxx,v 1.7 2005/06/14 13:36:18 fna Exp $
 *
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <string.h>

// C++ stdlib

// own header
#include "stream.h"


namespace lib
{

stream::~stream(void) { }

void stream::flush(void) { }

int
stream::printf(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = vprintf(fmt, args);
	va_end(args);

	return ret;
}

void stream::put(const std::string &str) { write(str.data(), 1, str.length()); }
void stream::put(const char *str, size_t len) { write(str, 1, len); }
void stream::put(const char *str) { write(str, 1, strlen(str)); }
void stream::put(const char c) { write(&c, 1, 1); }

int stream::get(void) const { char c; read(&c, 1, 1); return c; }

} /* namespace lib */
