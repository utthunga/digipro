
/* 
 * $Id: pop.h,v 1.5 2009/11/19 14:37:17 fna Exp $
 *
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_pop_h
#define _lib_pop_h

#include "lib_config.h"

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "sys_types.h"


namespace lib
{

WINDLL_LIB sys::int32 pop(sys::int32);

} /* namespace lib */

#endif /* _lib_pop_h */
