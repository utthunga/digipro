
/* 
 * $Id: ident.h,v 1.14 2008/03/17 20:22:48 fna Exp $
 *
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for an efficient string abstraction class.
 */

#ifndef _lib_ident_h
#define _lib_ident_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header
#include "hashtable.h"
#include "stream.h"


namespace lib
{

/**
 * The identifier class.
 * This class abstract strings used by the concrete syntax parser to a
 * uniquely mapped object. It behave like a type object. Additionaly there are
 * support routines to extract the encapsuled string. It have an automatic
 * storage management. This means that if there are no references left all
 * resources of this IDENTIFIER will be automatically freed. For that the copy
 * constructor and assignment operator are self defined. The real string is
 * encapsuled in a helper class id that is unique for a string. To ensure
 * the unique mapping an IDENTIFIER can only be created by the IDENT manager
 * class that include an hashtable over all IDENTIFIERs.
 */
class WINDLL_LIB IDENTIFIER
{
friend class IDENT;
protected:
	/**
	 * Helper class for identifier management.
	 * This hold the final string and use a reference counter to
	 * remember how often it's used.
	 */
	class id
	{
	public:
		id(class IDENT *p, char *s);
		~id(void);

		const char *c_str(void) const throw() { return str; }

	friend class IDENT;
	friend class IDENTIFIER;
	protected:
		unsigned long links;

	private:
		class IDENT *parent;
		char *str;
	};

	class id *myid;
	void release(void) throw();

public:
	/**
	 * NoIdent constructor (default).
	 * This constructor create the NoIdent identifier, an invalid identifier
	 * for error checking and for default initialization. It doesn't
	 * reference an id object.
	 */
	IDENTIFIER(void) throw() : myid(NULL)
	{ }

	/**
	 * normal constructor.
	 * Increase reference counter.
	 */
	IDENTIFIER(class id *id) throw() : myid(id)
	{ if (myid) myid->links++; }

	/**
	 * copy constructor.
	 * Increase reference counter.
	 */
	IDENTIFIER(const IDENTIFIER &id) throw() : myid(id.myid)
	{ if (myid) myid->links++; }

	/**
	 * assignment operator.
	 * Release current assigned id object and
	 * increase reference counter for the new id object.
	 */
	IDENTIFIER& operator=(const IDENTIFIER &id) throw();

	/**
	 * Release this IDENTIFIER.
	 * The destructor decrease the reference counter. If there are no
	 * references left it destruct the encapsuled id object.
	 */
	~IDENTIFIER(void) throw() { release(); }

	/**
	 * \name Helper methods.
	 * Get the C or C++ string of the encapsuled id object.
	 */
	//! \{
	size_t size(void) const throw() { return length(); }
	size_t length(void) const throw();
	const char *c_str(void) const throw();
	std::string str(void) const;
	//! \}

	/**
	 * \name Operator methods.
	 * Overloaded equal and not equal operators for nice handling and
	 * comparing. Though IDENTIFIERs can be used like builtin types.
	 */
	//! \{
	bool operator==(const IDENTIFIER &id) const throw()
	{
		return (myid == id.myid);
	}
	bool operator!=(const IDENTIFIER &id) const throw()
	{
		return (myid != id.myid);
	}
	bool operator<(const IDENTIFIER &id) const throw();
	bool operator>(const IDENTIFIER &id) const throw();
	//! \}

	/* for hash functions */
	void *hash(void) const throw();
};

/**
 * Compare function template specialization used by the hashtable template
 * class.
 * This is a specialization for the IDENTIFIER class of the Compare function
 * template equalfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB equalfunc<IDENTIFIER>
{
	bool operator()(const IDENTIFIER &__x, const IDENTIFIER &__y) const
	{ return __x == __y; }
};
/**
 * Hash function template specialization used by the hashtable template class.
 * This is a specialization for the IDENTIFIER class of the Hash function
 * template hashfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB hashfunc<IDENTIFIER>
{
	unsigned long operator()(const IDENTIFIER &__x, const unsigned long) const
	{ return (reinterpret_cast<unsigned long>(__x.hash()) >> 4); }
};
/**
 * Dump function template specialization used by the hashtable template class.
 * This is a specialization for the IDENTIFIER class of the Dump function
 * template dumpfunc used by the hashtable template class.
 */
template<>
struct WINDLL_LIB dumpfunc<IDENTIFIER>
{
	void operator()(class stream &f, const IDENTIFIER &__x) const
	{ f.printf("%s", __x.c_str()); }
};

/**
 * The identifier manager class.
 * This class controls the identifier creation. It remember all created
 * IDENTIFIERs inside a hashtable that is lookup up before a new IDENTIFIER is
 * created. If an IDENTIFIER already exist it's used and the reference counter
 * is increased.
 */
class WINDLL_LIB IDENT
{
public:
	/** The NoIdent identifier for comparison. */
	static const IDENTIFIER NoIdent;

	IDENT(void);
	~IDENT(void);

	IDENTIFIER create(const std::string &);
	IDENTIFIER create(const char *);
	IDENTIFIER create_from_string(char *&);
	IDENTIFIER create_from_buf(const char *, unsigned long);

	void clear(void);
	void dump_statistic(class stream &f) const;

friend class IDENTIFIER::id;
protected:
	hashtable<const char *, IDENTIFIER::id *> hash;

#ifdef STATISTIC
	/** Statistic data. */
	//!{
	unsigned long entries;
	unsigned long mem;
	//!}
#endif

private:
	std::vector<IDENTIFIER> cache;
};

} /* namespace lib */

#endif /* _lib_ident_h */
