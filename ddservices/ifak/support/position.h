
/* 
 * $Id: position.h,v 1.6 2007/09/17 11:44:05 fna Exp $
 *
 * Copyright (C) 2001 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for my idea of a position type.
 */

#ifndef _lib_position_h
#define _lib_position_h

#include "lib_config.h"

// C stdlib

// C++ stdlib

// own header
#include "ident.h"


namespace lib
{

/**
 * File position abstraction.
 * This is my idea of a file position. Contrary to the reuse::pos it
 * additionally include the file name. This class is used in the abstract
 * syntax tree and will be automatically created from the reuse::pos at tree
 * generation time. The useful equal and not equal operators are overloaded.
 */
struct WINDLL_LIB pos
{
	unsigned long	line;
	unsigned long	column;
	IDENTIFIER	file;
	unsigned long end_column;
	unsigned long end_line;
	
	bool operator== (const struct pos &p) const throw();
	bool operator!= (const struct pos &p) const throw();
};

/**
 * NoPosition constant.
 * This is the invalid or unknown position constant that is used in error case
 * and for default initialization or for comparing.
 */
extern WINDLL_LIB struct pos NoPosition;

/**
 * Check if the pos is inside start and end.
 */
WINDLL_LIB bool pos_inside(const pos &, const pos &start, const pos &end);

} /* namespace lib */

#endif /* _lib_position_h */
