
/* 
 * $Id: net_buf.h,v 1.5 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2004-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a network buffer.
 */

#ifndef _lib_net_buf_h
#define _lib_net_buf_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>
#include <string_edd.h>

// C++ stdlib
#include <string>

// own header
#include "stream.h"
#include "vsnprintf.h"


namespace lib
{

/**
 * network buffer.
 */
class WINDLL_LIB net_buf : public stream
{
public:
	~net_buf(void);

	/** Default constructor. */
	net_buf(void);

	/** Constructor. */
	net_buf(size_t size, size_t leading_space_hint = 12);

	/** Copy constructor. */
	net_buf(const net_buf &t);

	/** Assignment. */
        class net_buf& operator=(const net_buf &t);

private:
	class buffer
	{
	public:
		buffer(size_t size, size_t hint);
		buffer(const class buffer &);
		~buffer(void);

		inline void clear(void) throw() { start = end = buf + leading_space_hint; pos = 0; }
		void resize(size_t offset);

		inline void remove_from_start(size_t);
		inline void remove_from_end(size_t);

		void *add_at_start(size_t);
		void *add_at_end(size_t);

		void vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

		inline const char *data(void) const throw() { return start; }
		inline size_t length(void) const throw() { return (end - start); }

		size_t refcount;
		char *buf, *start, *end;
		size_t size, leading_space_hint;
		size_t pos; // stream
	};

	class buffer *buffer;

	void unref(void);
	void modify(void);

public:
	/**
	 * Reset buffer.
	 * The buffer is emptied and reallocated to fulfill
	 * the expected_size request.
	 */
	void reset(size_t expected_size, size_t leading_space_hint = 12);

	/**
	 * Append data.
	 */
	inline void append(const std::string &str) { append(str.data(), str.length()); }
	inline void append(const char *str) { append(str, strlen(str)); }
	void append(const void *, size_t);

	/**
	 * Prepend data.
	 */
	inline void prepend(const std::string &str) { prepend(str.data(), str.length()); }
	inline void prepend(const char *str) { prepend(str, strlen(str)); }
	void prepend(const void *, size_t);

	/**
	 * Add data at the start of the buf.
	 * Reserve space to place size_t data the the start
	 * of the net_buf. Return pointer to the reserved space.
	 */
	void *add_at_start(size_t);
	/**
	 * Add data at the end of the buf.
	 * Reserve space to place size_t data the the end
	 * of the net_buf. Return pointer to the reserved space.
	 */
	void *add_at_end(size_t);

	/**
	 * Remove data from the start of the buf.
	 */
	void remove_from_start(size_t);
	/**
	 * Remove data from the end of the buf.
	 */
	void remove_from_end(size_t);

	/**
	 * Get pointer to the data.
	 * \note It's not a C string, there is no terminating 0.
	 */
	inline const char *data(void) const throw() { return buffer ? buffer->data() : NULL; }

	/**
	 * Get data length.
	 * Return the number of used bytes.
	 */
	inline size_t length(void) const throw() { return buffer ? buffer->length() : 0; }

	inline operator const void *() const { return data(); }

	inline void operator+=(const char *str) { prepend(str); }
	inline void operator+=(const std::string &str) { prepend(str); }

	inline void operator-=(size_t len) { remove_from_end(len); }


	/*
	 * the stream interface
	 */

	virtual size_t read(void *ptr, size_t size, size_t nmemb) const;
	virtual size_t write(const void *ptr, size_t size, size_t nmemb);

	virtual bool eof(void) const;
	virtual void clear(void);

	virtual int vprintf(const char *fmt, va_list args) CHECK_PRINTF(2, 0);

	virtual void put(const std::string &str) { append(str); }
	virtual void put(const char *str, size_t len) { append(str, len); }
	virtual void put(const char *str) { append(str); }
	virtual void put(const char c) { append(&c, 1); }

	virtual int get(void) const;

	/* rewind stream to the start */
	inline void rewind(void) { buffer->pos = 0; }
};

} /* namespace lib */

#endif /* _lib_net_buf_h */
