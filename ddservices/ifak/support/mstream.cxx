
/* 
 * $Id: mstream.cxx,v 1.14 2009/07/01 12:50:42 fna Exp $
 *
 * Copyright (C) 2002-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>

// C++ stdlib

// own header
#include "mstream.h"


namespace lib
{

mstream::mstream(void)
: mybuf(new class dyn_buf)
, buf(*mybuf.get())
, pos(0)
{
}

mstream::mstream(class dyn_buf &b)
: buf(b)
, pos(0)
{
}

size_t
mstream::read(void *ptr, size_t size, size_t nmemb) const
{
	const char *str = buf.data();
	size_t length = buf.length();
	off_t todo = size * nmemb;
	off_t avail = length - pos;

	todo = std::min(avail, todo);
	memcpy(ptr, str + pos, todo);

	pos += todo;

	return todo;
}
size_t
mstream::write(const void *ptr, size_t size, size_t nmemb)
{
	off_t todo = size * nmemb;

	buf.put(static_cast<const char *>(ptr), todo);

	return todo;
}

bool
mstream::eof(void) const
{
	size_t length = buf.length();
	off_t avail = length - pos;

	return avail ? true : false;
}
void
mstream::clear(void)
{
	buf.clear();
	pos = 0;
}

int
mstream::vprintf(const char *fmt, va_list args)
{
	size_t length = buf.length();

	buf.vprintf(fmt, args);

	return (buf.length() - length);
}

void
mstream::put(const std::string &str)
{
	buf.put(str);
}

void
mstream::put(const char *str, size_t len)
{
	buf.put(str, len);
}
void
mstream::put(const char *str)
{
	buf.put(str);
}
void
mstream::put(const char c)
{
	buf.put(c);
}

int
mstream::get(void) const
{
	const char *str = buf.data();
	size_t length = buf.length();
	off_t avail = length - pos;
	int c = EOF;

	if (avail)
		c = *(str + pos++);

	return c;
}

} /* namespace lib */
