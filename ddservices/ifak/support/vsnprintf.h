
/* 
 * $Id: vsnprintf.h,v 1.11 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2010 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author 
 * \brief 
 */

#ifndef _lib_vsnprintf_h
#define _lib_vsnprintf_h

#include "lib_config.h"

// C stdlib
#include <sys/types.h>
#include <stdio.h>

// C++ stdlib
#include <string>

// own header
#include "sys_types.h"


namespace lib
{

#ifndef SYSTEM_WIN32
using ::vsnprintf;
#else
WINDLL_LIB int vsnprintf(char *, size_t, const char *, va_list);
#endif


} /* namespace lib */

#endif /* _lib_vsnprintf_h */
