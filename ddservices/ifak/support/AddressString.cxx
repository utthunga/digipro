/*
 * $Id: AddressString.cxx,v 1.1 2008/09/24 12:41:12 mmeier Exp $
 *
 * Copyright (C) 2008 Tomasz Linkowski
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Tomasz Linkowski
 * \remarks Copyright &copy; ifak e.V. Magdeburg
 */

// C stdlib
#include <stdlib.h>

// C++ stdlib

// other libs

// own libs

// my lib

// own header
#include "AddressString.h"

namespace lib
{

AddressString& AddressString::operator=(const std::string& address)
{
	_address = address;
	return *this;
}
	
std::string AddressString::get_protocol() const
{
	int end = _address.find_first_of(':');
	return _address.substr(0, end);
}

std::string AddressString::get_hostname() const
{
	int begin = _address.find_last_of('/') + 1;
	int end = _address.find_last_of(':');
	return _address.substr(begin, end-begin);
}

int AddressString::get_port() const
{
	int begin = _address.find_last_of(':') + 1;
	return atoi( _address.substr(begin).c_str() );
}

} /* namespace lib */
