
/* 
 * $Id: scan_buf.h,v 1.9 2006/06/01 21:44:04 fna Exp $
 *
 * Copyright (C) 2004 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for a scanner buffer.
 */

#ifndef _lib_scan_buf_h
#define _lib_scan_buf_h

#include "lib_config.h"

// C stdlib
#include <assert.h>
#include <string.h>
#include <sys/types.h>

// C++ stdlib
#include <list>

// own header


namespace lib
{

/*
 * scan_buf_pool should be used in combination with 
 * scan_buf if it cannot be ensured that scan_buf::release 
 * will be called (can happen with certain grammars).
 * Saves pointers to string. Strings can be inserted to
 * the cache or removed from the cache without modifying
 * or deleting the pointers or the contents.
 * The clear method and the destructor will free all 
 * saved pointers. They should be called _after_ the 
 * scanner and parsers are deleted.
 */
class scan_buf_pool
{
public:
	void insert(char *ptr)
	{
		cache.push_back(ptr);
	}

	void remove(char *ptr)
	{
		cache.remove(ptr);
	}

	void clear(void);

	~scan_buf_pool(void)
	{
		clear();
	}
private:

	std::list<char *> cache;
};

/**
 * Scanner buffer.
 * No constructor/destructor so it can be used in a union
 * as required by the cocktail generated parser/scanner.
 */
struct WINDLL_LIB scan_buf
{
	char *ptr, *xptr;
	size_t plen, freespace;
	class scan_buf_pool *pool;
	
	void init(void) { init(511); }

	/**
	 * Pass a reference to a scan_buf_pool if it is uncertain 
	 * whether release will be called. 
	 */
	void init(scan_buf_pool &pool, int deflen = 511)
	{
		plen = freespace = deflen + 1;
		ptr = xptr = new char[plen];

		this->pool = &pool;
		pool.insert(ptr);
	}

	void init(int deflen)
	{
		plen = freespace = deflen + 1;
		ptr = xptr = new char[plen];
		pool = NULL;
	}
	
	char *release(void)
	{
		char *ret = ptr;

		if (pool)
			pool->remove(ptr);
		
		ptr = xptr = NULL;
		plen = freespace = 0;
		
		return ret;
	}
	
	size_t len(void) const { return (plen - freespace); }
	
	void put(char c)
	{
		assert(ptr && xptr && freespace);
		
		*xptr++ = c;
		freespace--;
		
		if (freespace == 0)
			resize();
	}
	void put(const char *src, size_t nlen)
	{
		assert(ptr && xptr && freespace);
		
		if (nlen >= freespace)
			resize(nlen);
		
		memcpy(xptr, src, nlen);
		xptr += nlen;
		freespace -= nlen;
	}
	void finish(void)
	{
		assert(ptr && xptr && freespace);
		*xptr = '\0';
	}
	
private:
	void resize(size_t wanted = 0);
};

#if defined(_MSC_VER) && (_MSC_VER < 1300)
WINDLL_LIB extern struct scan_buf NoScanBuf;
#else
const struct scan_buf NoScanBuf = { NULL, NULL, 0, 0 };
#endif

} /* namespace lib */

#endif /* _lib_scan_buf_h */
