
/* 
 * $Id: resolve_address.h,v 1.2 2009/04/06 23:59:50 fna Exp $
 *
 * Copyright (C) 2008 Marco Meier <mmeier@badeio.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _lib_resolve_address_h
#define _lib_resolve_address_h

#include "lib_config.h"

// C stdlib

// C++ stdlib
#include <string>

// own header


namespace lib
{

WINDLL_LIB std::string resolve_address(const std::string &);

} /* namespace lib */

#endif /* _lib_resolve_address_h */
