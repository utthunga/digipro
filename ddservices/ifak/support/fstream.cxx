
/* 
 * $Id: fstream.cxx,v 1.10 2005/06/14 13:36:17 fna Exp $
 *
 * Copyright (C) 2002 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <stdarg.h>

// C++ stdlib
#include<iostream>
// own header
#include "fstream.h"


namespace lib
{

fstream::fstream(FILE *f, enum close_on_destruct c)
: file(f), close(c)
{
	assert(file);
}

fstream::~fstream(void)
{
	if (close == auto_close)
		fclose(file);
}

size_t
fstream::read(void *ptr, size_t size, size_t nmemb) const
{
	//std::cout<<"YOGA:calling inside fstream::read"<<"__LINE__ ::"<<__LINE__<<"__FILE__ ::"<<__FILE__<<std::endl;
	//std::cout<<"\n"<<std::endl;
	//std::cout<<(char *)file<<std::endl;
	return fread(ptr, size, nmemb, file);
}
size_t
fstream::write(const void *ptr, size_t size, size_t nmemb)
{
	return fwrite(ptr, size, nmemb, file);
}

bool
fstream::eof(void) const
{
	return feof(file) ? true : false;
}
void
fstream::clear(void)
{
	clearerr(file);
}
void
fstream::flush(void)
{
	fflush(file);
}

int
fstream::vprintf(const char *fmt, va_list args)
{
	int ret;
#ifdef va_copy
	va_list copied_args;
	va_copy(copied_args, args);

	ret = vfprintf(file, fmt, copied_args);
	va_end(copied_args);
#else
	ret = vfprintf(file, fmt, args);
#endif

	return ret;
}

void
fstream::put(const std::string &str)
{
	fwrite(str.data(), 1, str.length(), file);
}

void
fstream::put(const char *str, size_t len)
{
	fwrite(str, 1, len, file);
}
void
fstream::put(const char *str)
{
	fputs(str, file);
}
void
fstream::put(const char c)
{
	putc(c, file);
}

int
fstream::get(void) const
{
	return getc(file);
}


WINDLL_LIB class fstream *
fstream_open(const char *path, const char *mode)
{
	class fstream *ret = NULL;
	FILE *f;

	f = fopen(path, mode);
	if (f)
	{
		try { ret = new class fstream(f, fstream::auto_close); }
		catch (...) { fclose(f); throw; }

		if (!ret)
			fclose(f);
	}

	return ret;
}

WINDLL_LIB class fstream *
fstream_open(const std::string &path, const char *mode)
{
	return fstream_open(path.c_str(), mode);
}

} /* namespace lib */
