
/* 
 * $Id: edd_values_arithmetic.cxx,v 1.16 2009/02/04 14:37:13 fna Exp $
 * 
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <math.h>

// C++ stdlib

// my lib
#include "check_fit_float.h"
#include "port.h"
#include "string.h"
#include "BasicType.h"

// own
#include "edd_ast.h"
#include "edd_values_arithmetic.h"

using namespace std;

namespace edd
{

/*
 * arithmetic_value
 */

template<typename T>
operand_ptr
arithmetic_value<T>::clone(void) const
{
	return new arithmetic_value<T>(value);
}

template<typename T>
arithmetic_value<T>::operator stackmachine::Type *() const { return stackmachine::BasicType<T>::create(value); }

/* Cast operators. */

#define CAST_OPERATOR(classname, type, value) \
	template<typename T> \
	classname<T>::operator type() const { return type(value); }

CAST_OPERATOR(arithmetic_value, bool,    value ? true : false)
CAST_OPERATOR(arithmetic_value, int8,    value)
CAST_OPERATOR(arithmetic_value, int16,   value)
CAST_OPERATOR(arithmetic_value, int32,   value)
CAST_OPERATOR(arithmetic_value, int64,   value)
CAST_OPERATOR(arithmetic_value, uint8,   value)
CAST_OPERATOR(arithmetic_value, uint16,  value)
CAST_OPERATOR(arithmetic_value, uint32,  value)
CAST_OPERATOR(arithmetic_value, uint64,  value)
CAST_OPERATOR(arithmetic_value, float32, value)
CAST_OPERATOR(arithmetic_value, float64, value)

/* Unary operators. */

#define UNARY_OPERATOR(classname, op) \
	template<typename T> \
	operand_ptr classname<T>::operator op(void) const { return new classname<T>(op value); }

UNARY_OPERATOR(arithmetic_value, +)
UNARY_OPERATOR(arithmetic_value, -)
UNARY_OPERATOR(arithmetic_value, ~)
UNARY_OPERATOR(arithmetic_value, !)

/* Binary operators. */

#define BINARY_OPERATOR(classname, op) \
	template<typename T> \
	operand_ptr classname<T>::operator op(const operand &o) const { return new classname<T>(value op (T)o); }

BINARY_OPERATOR(arithmetic_value, *)
template<typename T>
operand_ptr arithmetic_value<T>::operator /(const operand &o) const
{
	/* catch division by zero */
	if (nl::is_integer && (T)o == 0)
		throw RuntimeError(xxDivisionByZero);

	return new arithmetic_value<T>(value / (T)o);
}
BINARY_OPERATOR(arithmetic_value, +)
BINARY_OPERATOR(arithmetic_value, -)
BINARY_OPERATOR(arithmetic_value, %)
BINARY_OPERATOR(arithmetic_value, ^)
BINARY_OPERATOR(arithmetic_value, &)
BINARY_OPERATOR(arithmetic_value, |)
BINARY_OPERATOR(arithmetic_value, <<)
BINARY_OPERATOR(arithmetic_value, >>)

#define BINARY_OPERATOR_CMP(classname, op) \
	template<typename T> \
	bool classname<T>::operator op(const operand &o) const { return (value op (T)o); }

BINARY_OPERATOR_CMP(arithmetic_value, <)
BINARY_OPERATOR_CMP(arithmetic_value, >)
BINARY_OPERATOR_CMP(arithmetic_value, ==)
BINARY_OPERATOR_CMP(arithmetic_value, !=)
BINARY_OPERATOR_CMP(arithmetic_value, <=)
BINARY_OPERATOR_CMP(arithmetic_value, >=)

/* arithmetic_value dump specializations */

#define DUMP0(T, fmt) \
	template<> \
	void arithmetic_value<T>::dump0(lib::stream &f) const { f.printf("$$ " #T " { %" fmt " }", value); }

DUMP0(int8,    PRId8 )
DUMP0(int16,   PRId16)
DUMP0(int32,   PRId32)
DUMP0(int64,   PRId64)
DUMP0(uint8,   PRIu8 )
DUMP0(uint16,  PRIu16)
DUMP0(uint32,  PRIu32)
DUMP0(uint64,  PRIu64)
DUMP0(float32, "f"   )
DUMP0(float64, "f"   )

#undef DUMP0

/* arithmetic_value print specializations */

#define ROUND_8(bytes) (((bytes) + 7) & ~7)

#define PRINT(classname, T, deffmt) \
	template<> \
	std::string \
	classname<T>::print(const char *fmt) const \
	{ \
		if (!fmt) \
		{ \
			char buf[ROUND_8((nl::digits10)+4)]; \
			snprintf(buf, sizeof(buf), "%" deffmt, value); \
			return std::string(buf); \
		} \
		\
		char buf[128]; \
		snprintf(buf, sizeof(buf), fmt, value); \
		return std::string(buf); \
	} \
	\
	template<> \
	const char * \
	classname<T>::printf_fmt(void) throw() { return "%" deffmt; }

PRINT(arithmetic_value, int8,   PRId8 )
PRINT(arithmetic_value, int16,  PRId16)
PRINT(arithmetic_value, int32,  PRId32)
PRINT(arithmetic_value, int64,  PRId64)
PRINT(arithmetic_value, uint8,  PRIu8 )
PRINT(arithmetic_value, uint16, PRIu16)
PRINT(arithmetic_value, uint32, PRIu32)
PRINT(arithmetic_value, uint64, PRIu64)

#undef PRINT

template<>
std::string
arithmetic_value<float32>::print(const char *fmt) const
{
	return lib::ftoa(fmt ? fmt : "%.6g", value);
}
template<>
const char *
arithmetic_value<float32>::printf_fmt(void) throw() { return "%.6g"; }

template<>
std::string
arithmetic_value<float64>::print(const char *fmt) const
{
	return lib::ftoa(fmt ? fmt : "%.8g", value);
}
template<>
const char *
arithmetic_value<float64>::printf_fmt(void) throw() { return "%.8g"; }

/* float32/64 specializations */

template<>
operand_ptr arithmetic_value<float32>::operator~(void) const { return operand::operator~(); }
template<>
operand_ptr arithmetic_value<float64>::operator~(void) const { return operand::operator~(); }

#define BINARY_OPERATOR_SPECIALIZATION(classname, T, op) \
	template<> \
	operand_ptr classname<T>::operator op(const operand &o) const { return operand::operator op(o); }

BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, %)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, ^)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, &)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, |)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, <<)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float32, >>)

BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, %)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, ^)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, &)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, |)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, <<)
BINARY_OPERATOR_SPECIALIZATION(arithmetic_value, float64, >>)


/*
 * arithmetic_lvalue
 */

template<typename T>
void
arithmetic_lvalue<T>::assign(const T v, bool strict)
{
	value = v;
	mystatus.changed();
}

template<typename T>
operand_ptr
arithmetic_lvalue<T>::clone(void) const
{
	return new arithmetic_lvalue<T>(value, mystatus.reference());
}

template<typename T>
void arithmetic_lvalue<T>::operator= (const stackmachine::Type &v) { assign((T) v); }

template<typename T>
operand::value_status_t& arithmetic_lvalue<T>::status(void) { return mystatus.reference(); }
template<typename T>
const operand::value_status_t& arithmetic_lvalue<T>::status(void) const { return mystatus.reference(); }

/* Postfix operators. */

template<typename T>
operand_ptr arithmetic_lvalue<T>::operator++  (int)  // post
{
	const T ret = value;
	assign(value + 1);
	return new arithmetic_value<T>(ret);
}
template<typename T>
operand_ptr arithmetic_lvalue<T>::operator--  (int)  // post
{
	const T ret = value;
	assign(value - 1);
	return new arithmetic_value<T>(ret);
}

/* Unary operators. */

template<typename T>
operand_ptr arithmetic_lvalue<T>::operator++  (void) // pre
{
	assign(value + 1);
	return new arithmetic_value<T>(value);
}
template<typename T>
operand_ptr arithmetic_lvalue<T>::operator--  (void) // pre
{
	assign(value - 1);
	return new arithmetic_value<T>(value);
}

/* Assignment operators. */

#define ASSIGNMENT_OPERATOR(classname, op) \
	template<typename T> \
	void classname<T>::operator op ## =(const operand &o) { assign(value op ((T)o)); }

template<typename T>
void arithmetic_lvalue<T>::operator=(const operand &o) { assign((T)o); }

ASSIGNMENT_OPERATOR(arithmetic_lvalue, +)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, -)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, *)
template<typename T>
void arithmetic_lvalue<T>::operator /=(const operand &o)
{
	/* catch division by zero */
	if (arithmetic_value<T>::nl::is_integer && (T)o == 0)
		throw RuntimeError(xxDivisionByZero);

	assign(value / ((T)o));
}
ASSIGNMENT_OPERATOR(arithmetic_lvalue, %)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, ^)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, &)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, |)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, >>)
ASSIGNMENT_OPERATOR(arithmetic_lvalue, <<)

/* arithmetic_lvalue dump specializations */

#define DUMP0(T, fmt) \
	template<> \
	void arithmetic_lvalue<T>::dump0(lib::stream &f) const \
	{ \
		f.put("$$ " #T " lval"); \
		mystatus.dump(f); \
		f.printf(" { %" fmt " }", value); \
	}

DUMP0(int8,    PRId8 )
DUMP0(int16,   PRId16)
DUMP0(int32,   PRId32)
DUMP0(int64,   PRId64)
DUMP0(uint8,   PRIu8 )
DUMP0(uint16,  PRIu16)
DUMP0(uint32,  PRIu32)
DUMP0(uint64,  PRIu64)
DUMP0(float32, "f"  )
DUMP0(float64, "f"  )

#undef DUMP0

/* arithmetic_lvalue scan for integer types */

template<typename T>
template<typename T1>
T arithmetic_lvalue<T>::scan0<0, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(!nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef unsigned long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoul(s, &check, 0);

	/* strtoul accept negative numbers and
	 * return the negated value as result [no error];
	 * so explicitly check for a negation sign
	 */
	if (*s == '-')
		throw RuntimeError(xxOverUnderFlow);

	if ((errno == ERANGE && (tmp == ULONG_MAX))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp > nl::max())
			throw RuntimeError(xxOverUnderFlow);
	}

	return T(tmp);
}
template<typename T>
template<typename T1>
T arithmetic_lvalue<T>::scan0<1, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtol(s, &check, 0);

	if ((errno == ERANGE && (tmp == LONG_MAX || tmp == LONG_MIN))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp < nl::min() || tmp > nl::max())
			throw RuntimeError(xxOverUnderFlow);
	}

	return T(tmp);
}
template<typename T>
template<typename T1>
T arithmetic_lvalue<T>::scan0<2, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(!nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef unsigned long long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoull(s, &check, 0);

	/* strtoull accept negative numbers and
	 * return the negated value as result [no error];
	 * so explicitly check for a negation sign
	 */
	if (*s == '-')
		throw RuntimeError(xxOverUnderFlow);

	if ((errno == ERANGE && (tmp == ULLONG_MAX))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp > nl::max())
			throw RuntimeError(xxOverUnderFlow);
	}

	return T(tmp);
}
template<typename T>
template<typename T1>
T arithmetic_lvalue<T>::scan0<3, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef long long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoll(s, &check, 0);

	if ((errno == ERANGE && (tmp == LLONG_MAX || tmp == LLONG_MIN))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp < nl::min() || tmp > nl::max())
			throw RuntimeError(xxOverUnderFlow);
	}

	return T(tmp);
}
template<typename T>
void
arithmetic_lvalue<T>::scan(const std::string &str, bool strict)
{
	typedef std::numeric_limits<T> nl;

	assert(nl::is_specialized);
	assert(nl::is_bounded);
	assert(nl::is_integer);

	const int val =
		  (nl::is_signed   ? 0x1 : 0x0)
		| (nl::digits > 32 ? 0x2 : 0x0);

	/* 
	 * 0 | strtoul   (unsigned long)
	 * 1 | strtol    (long)
	 * 2 | strtoull  (unsigned long long)
	 * 3 | strtoll   (long long)
	 */
	T result = scan0<val, T>::go(str);

	assign(result, strict);
}

/* arithmetic_lvalue scan specializations */

/*
 * WIN32 use:
 * nan  -> 1.#QNAN
 * +inf -> 1.#INF / +1.#INF
 * -inf -> -1.#INF
 */

template<>
void arithmetic_lvalue<float32>::scan(const std::string &str0, bool strict)
{
	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;
	double tmp;
#ifdef CPM_64_BIT_OS
	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(isnan(tmp));     goto done; }
#else
	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(__builtin_isnan(static_cast<double>(tmp)));     goto done; }
#endif
	if (lib::stricmp(s, "inf")  == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "+inf") == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "-inf") == 0) { tmp = -nl::infinity();  assert(isinf(tmp) < 0); goto done; }

	errno = 0;
	tmp = strtod(s, &check);

	if ((errno == ERANGE && (tmp == HUGE_VAL || tmp == 0))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

	if (lib::check_fit_float(tmp) == -1)
		throw RuntimeError(xxOverUnderFlow);

done:
	assign(float32(tmp), strict);
}

template<>
void arithmetic_lvalue<float64>::scan(const std::string &str0, bool strict)
{
	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;
	double tmp;

#ifdef CPM_64_BIT_OS
	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(isnan(tmp));     goto done; }
#else
	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(__builtin_isnan(static_cast<double>(tmp)));     goto done; }
#endif
	if (lib::stricmp(s, "inf")  == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "+inf") == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "-inf") == 0) { tmp = -nl::infinity();  assert(isinf(tmp) < 0); goto done; }

	errno = 0;
	tmp = strtod(s, &check);

	if ((errno == ERANGE && (tmp == HUGE_VAL || tmp == 0))
	    || (errno != 0 && tmp == 0))
		throw RuntimeError(xxOverUnderFlow);

	if (check == s || *check != '\0')
		throw RuntimeError(xxIllegalChar);

done:
	assign(tmp, strict);
}

/* float32/64 specializations */

#define ASSIGNMENT_OPERATOR_SPECIALIZATION(classname, T, op) \
	template<> \
	void classname<T>::operator op ## =(const operand &o) { operand::operator op(o); }

ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, %)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, ^)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, &)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, |)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, >>)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float32, <<)

ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, %)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, ^)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, &)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, |)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, >>)
ASSIGNMENT_OPERATOR_SPECIALIZATION(arithmetic_lvalue, float64, <<)


/*
 * checked_arithmetic_lvalue
 */

template<typename T>
void
checked_arithmetic_lvalue<T>::assign(const T v, bool strict)
{
	const T bak = value;

	value = v;

	if (!type->check_value(*this, env))
	{
		if (strict)
		{
			value = bak;

#ifdef CPM_64_BIT_OS
			if (!isnan(v))
#else
			if (!__builtin_isnan(static_cast<double>(v)))
#endif
				throw RuntimeError(xxRange, type->pos);
			else
				throw RuntimeError(xxInvalidInput, type->pos);
		}

		mystatus.invalid();
	}
	else
		mystatus.changed();
}

template<typename T>
operand_ptr
checked_arithmetic_lvalue<T>::clone(void) const
{
	return new checked_arithmetic_lvalue<T>(value, type, mystatus.reference());
}

/* checked_arithmetic_lvalue dump specializations */

#define DUMP0(T, fmt) \
	template<> \
	void checked_arithmetic_lvalue<T>::dump0(lib::stream &f) const \
	{ \
		f.put("$$ checked " #T " lval"); \
		mystatus.dump(f); \
		f.printf(" { %" fmt " }", value); \
	}

DUMP0(int8,    PRId8 )
DUMP0(int16,   PRId16)
DUMP0(int32,   PRId32)
DUMP0(int64,   PRId64)
DUMP0(uint8,   PRIu8 )
DUMP0(uint16,  PRIu16)
DUMP0(uint32,  PRIu32)
DUMP0(uint64,  PRIu64)
DUMP0(float32, "f")
DUMP0(float64, "f")

#undef DUMP0


/*
 * code instantiation
 */

namespace
{

#define INSTANCES(name) \
	INSTANCE(name, int8) \
	INSTANCE(name, int16) \
	INSTANCE(name, int32) \
	INSTANCE(name, int64) \
	INSTANCE(name, uint8) \
	INSTANCE(name, uint16) \
	INSTANCE(name, uint32) \
	INSTANCE(name, uint64) \
	INSTANCE(name, float32) \
	INSTANCE(name, float64)

#define INSTANCE(name, type) \
	name<type> dummy_ ## name ## type(0);

INSTANCES(arithmetic_value)
INSTANCES(arithmetic_lvalue)

#undef INSTANCE

#define INSTANCE(name, type) \
	name<type> dummy_ ## name ## type(0, NULL);

INSTANCES(checked_arithmetic_lvalue)

#undef INSTANCE

}

/*
 * boolean type
 */

operand_ptr
boolean_value::clone(void) const
{
	return new boolean_value(value);
}

void
boolean_value::dump0(lib::stream &f) const
{
	std::string v = print();

	f.printf("$$ boolean { %s }", (value > 0) ? "TRUE" : "FALSE");
}

std::string
boolean_value::print(const char *fmt) const
{
	return std::string(value ? "TRUE" : "FALSE");
}

    	boolean_value::operator stackmachine::Type*() const
	{ return new stackmachine::uint8Type(value); }

	boolean_value::operator bool(void) const
	{ return (value > 0); }
	boolean_value::operator uint8(void) const
	{ return value; }

bool
boolean_value::operator==(const operand &o) const
{
	if (value == 0)
	{
		if ((uint8) o == 0)
			return true;
		else
			return false;
	}
	else
	{
		if ((uint8) o != 0)
			return true;
		else
			return false;
	}
}

bool
boolean_value::operator!=(const operand &o) const
{
	return !operator==(o);
}

void
boolean_lvalue::assign(const uint8 v, bool strict)
{
	value = v;
	mystatus.changed();
}

operand_ptr
boolean_lvalue::clone(void) const
{
	return new boolean_lvalue(value, mystatus.reference());
}

void
boolean_lvalue::dump0(lib::stream &f) const
{
	f.printf("$$ boolean lval");
	mystatus.dump(f);
	f.printf(" { %s }", (value > 0) ? "TRUE" : "FALSE");
}

void
boolean_lvalue::scan(const std::string &str, bool strict)
{
	std::string v(str);
	lib::strupr(v);

	if (v == "TRUE")
		assign(1);
	else if (v == "FALSE")
		assign(0);
	else
		throw RuntimeError(xxInvalidInput);
		
}

void	boolean_lvalue::operator=(const stackmachine::Type &v) { pretty_scan((std::string) v); }

operand::value_status_t& boolean_lvalue::status(void) { return mystatus.reference(); }
const operand::value_status_t& boolean_lvalue::status(void) const { return mystatus.reference(); }

void	boolean_lvalue::operator=(const operand &op) { assign((uint8) op); }

} /* namespace edd */
