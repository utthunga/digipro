
/* 
 * $Id: config.h,v 1.26 2006/11/07 08:44:28 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _config_h
#define _config_h

#define DEMOLIMIT 650

#define PROJECT_FILE_EXT "EDDy"

#endif /* _config_h */
