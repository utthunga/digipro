
/* 
 * $Id: edd_tree_map.cxx,v 1.102 2010/03/01 15:00:09 mmeier Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_tree.h"

// C stdlib
#include <assert.h>
#include <string.h>
#include <stdio.h>

// C++ stdlib

// my lib

// own header
#include "edd_ast.h"


namespace edd
{

/**
 * \internal
 */
template<typename T>
T *
edd_tree::m_node(
		const lib::pos &pos,
		class tree *attribute_list)
{
	T *t = new T(this, pos);
	assert(t);

	m_node(t, attribute_list);

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_like(
		class tree *ident,
		const lib::pos &pos,
		const lib::pos &start,
		const lib::pos &end,
		unsigned long type,
		class tree *add_type,
		class tree *likename,
		class tree *redefinitions,
		class tree *anon_redefinitions)
{
	return m_edd_object<LIKE_tree>(
		ident,
		pos, start, end,
		tree_concat(
		 m_pseudo(EDD_LIKE, EDD_LIKE_ATTR_TYPE, tree_concat(m_integer(type, pos), add_type)),
		 m_pseudo(EDD_LIKE, EDD_LIKE_ATTR_LIKENAME, likename),
		 m_pseudo(EDD_LIKE, EDD_LIKE_ATTR_REDEFINITIONS, redefinitions),
		 m_pseudo(EDD_LIKE, EDD_LIKE_ATTR_ANON_REDEFINITIONS, anon_redefinitions)
		));
}

/**
 * \internal
 */
class ROOT_tree *
edd_tree::m_root(
		const lib::pos &pos,
		class tree *identification,
		class tree *definition_list)
{
	return m_node<ROOT_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_ROOT, EDD_ROOT_ATTR_IDENTIFICATION, identification),
		 m_pseudo(EDD_ROOT, EDD_ROOT_ATTR_DEFINITION_LIST, definition_list)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_identification(
		const lib::pos &pos,
		class tree *manufacturer,
		class tree *device_type,
		class tree *device_revision,
		class tree *DD_revision,
		class tree *edd_version,
		class tree *edd_profile,
		class tree *manufacturer_ext)
{
	return m_node<IDENTIFICATION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_MANUFACTURER, manufacturer),
		 m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_DEVICE_TYPE, device_type),
		 m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_DEVICE_REVISION, device_revision),
		 m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_DD_REVISION, DD_revision),
		 tree_concat(
		  m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_EDD_VERSION, edd_version),
		  m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_EDD_PROFILE, edd_profile),
		  m_pseudo(EDD_IDENTIFICATION, EDD_IDENTIFICATION_ATTR_MANUFACTURER_EXT, manufacturer_ext)
		)));
}

/**
 * \internal
 */
class tree *
edd_tree::m_imported_dd(
		const lib::pos &pos,
		class tree *identification,
		class tree *imports,
		class tree *redefinitions)
{
	return m_node<IMPORTED_DD_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_IMPORTED_DD, EDD_IMPORTED_DD_ATTR_IDENTIFICATION, identification),
		 m_pseudo(EDD_IMPORTED_DD, EDD_IMPORTED_DD_ATTR_IMPORTS, imports),
		 m_pseudo(EDD_IMPORTED_DD, EDD_IMPORTED_DD_ATTR_REDEFINITIONS, redefinitions)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_arithmetic(
		const lib::pos &pos,
		class tree *type,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_ARITHMETIC_tree>(pos,
		tree_concat(
		 type,
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_boolean(
		const lib::pos &pos)
{
	return m_node<VARIABLE_TYPE_BOOLEAN_tree>(pos, NULL);
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_enumerated(
		const lib::pos &pos,
		class tree *type,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_ENUMERATED_tree>(pos,
		tree_concat(
		 type,
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_index(
		const lib::pos &pos,
		class tree *reference_array_reference,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_INDEX_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_VARIABLE_TYPE_INDEX, EDD_VARIABLE_TYPE_INDEX_ATTR_REFERENCE, reference_array_reference),
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_string(
		const lib::pos &pos,
		class tree *type,
		class tree *size,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_STRING_tree>(pos,
		tree_concat(
		 type,
		 size,
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_date_time(
		const lib::pos &pos,
		class tree *type,
		class tree *size,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_DATE_TIME_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_VARIABLE_TYPE_DATE_TIME, EDD_VARIABLE_TYPE_DATE_TIME_ATTR_TYPE, type),
		 m_pseudo(EDD_VARIABLE_TYPE_DATE_TIME, EDD_VARIABLE_TYPE_DATE_TIME_ATTR_SIZE, size),
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_type_object_reference(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<VARIABLE_TYPE_OBJECT_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_minmax_value(
		const lib::pos &pos,
		class tree *type,
		class tree *expr,
		class tree *nr)
{
	return m_node<MINMAX_VALUE_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_MINMAX_VALUE, EDD_MINMAX_VALUE_ATTR_TYPE, type),
		 m_pseudo(EDD_MINMAX_VALUE, EDD_MINMAX_VALUE_ATTR_EXPR, expr),
		 m_pseudo(EDD_MINMAX_VALUE, EDD_MINMAX_VALUE_ATTR_NR, nr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_enumerator_enum(
		const lib::pos &pos,
		class tree *value,
		class tree *description,
		class tree *help)
{
	return m_node<VARIABLE_ENUMERATOR_ENUM_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_ENUM, EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_VALUE, value),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_ENUM, EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_DESCRIPTION, description),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_ENUM, EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_HELP, help)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::add_variable_enumerator_enum_comma(
		class tree *t,
		class tree *comma)
{
	class node_tree *node;

	assert(t && t->kind == EDD_VARIABLE_ENUMERATOR_ENUM);

	node = (class node_tree *) t;
	node->assign_attr(EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_COMMA, comma);

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_variable_enumerator_bitenum(
		const lib::pos &pos,
		class tree *value,
		class tree *description,
		class tree *help,
		class tree *variable_class,
		class tree *status_class,
		class tree *method_ref,
		class tree *attribute_list)
{
	return m_node<VARIABLE_ENUMERATOR_BITENUM_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_INTEGER, value),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_DESCRIPTION, description),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_HELP, help),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_VARIABLE_CLASS, variable_class),
		 m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_STATUS_CLASS, status_class),
		 tree_concat(
		  m_pseudo(EDD_VARIABLE_ENUMERATOR_BITENUM, EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_METHOD_REFERENCE, method_ref),
		  attribute_list
		)));
}

/**
 * \internal
 */
class tree *
edd_tree::add_variable_enumerator_bitenum_comma(
		class tree *t,
		class tree *comma)
{
	class node_tree *node;

	assert(t && t->kind == EDD_VARIABLE_ENUMERATOR_BITENUM);

	node = (class node_tree *) t;
	node->assign_attr(EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_COMMA, comma);

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_transaction(
		const lib::pos &pos,
		class tree *integer,
		class tree *attribute_list)
{
	return m_node<TRANSACTION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_TRANSACTION, EDD_TRANSACTION_ATTR_INTEGER, integer),
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_data_item(
		const lib::pos &pos,
		class tree *reference,
		class tree *mask,
		class tree *qualifier)
{
	return m_node<DATA_ITEM_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_DATA_ITEM, EDD_DATA_ITEM_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_DATA_ITEM, EDD_DATA_ITEM_ATTR_MASK, mask),
		 m_pseudo(EDD_DATA_ITEM, EDD_DATA_ITEM_ATTR_QUALIFIER, qualifier)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::add_data_item_comma(
		class tree *t,
		class tree *comma)
{
	assert(t);

	if (t->kind == EDD_DATA_ITEM)
	{
		class node_tree *node;

		node = (class node_tree *) t;
		node->assign_attr(EDD_DATA_ITEM_ATTR_COMMA, comma);
	}
	else
		delete comma;

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_array_element(
		const lib::pos &pos,
		class tree *integer,
		class tree *reference,
		class tree *description,
		class tree *help)
{
	return m_node<REFERENCE_ARRAY_ELEMENT_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_REFERENCE_ARRAY_ELEMENT, EDD_REFERENCE_ARRAY_ELEMENT_ATTR_INTEGER, integer),
		 m_pseudo(EDD_REFERENCE_ARRAY_ELEMENT, EDD_REFERENCE_ARRAY_ELEMENT_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_REFERENCE_ARRAY_ELEMENT, EDD_REFERENCE_ARRAY_ELEMENT_ATTR_DESCRIPTION, description),
		 m_pseudo(EDD_REFERENCE_ARRAY_ELEMENT, EDD_REFERENCE_ARRAY_ELEMENT_ATTR_HELP, help)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_menu_item(
		const lib::pos &pos,
		class tree *item,
		class tree *attribute_list)
{
	return m_node<MENU_ITEM_tree>(pos,
		tree_concat(
		 /* make sure we don't create an empty MENU_ITEM_tree until
		  * further semantic checks are established
		  */
		 (item) ? m_pseudo(EDD_MENU_ITEM, EDD_MENU_ITEM_ATTR_ITEM, item) : NULL, 
		 attribute_list
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_menu_item_string(
		const lib::pos &pos,
		class tree *string)
{
	return m_node<MENU_ITEM_STRING_tree>(pos,
		 m_pseudo(EDD_MENU_ITEM_STRING, EDD_MENU_ITEM_STRING_ATTR_STRING, string)
		);
}

/**
 * \internal
 */
class tree *
edd_tree::m_menu_item_format(
		const lib::pos &pos,
		class tree *type)
{
	return m_node<MENU_ITEM_FORMAT_tree>(pos,
		 m_pseudo(EDD_MENU_ITEM_FORMAT, EDD_MENU_ITEM_FORMAT_ATTR_TYPE, type)
		);
}

/**
 * \internal
 */
class tree *
edd_tree::add_comma(
		class tree *t,
		class tree *comma,
		int kind,
		unsigned long attr)
{
	if (comma)
	{
		class tree *child = t;

		if (child && child->kind == EDD_pseudo)
			child = ((class pseudo_tree *) child)->get();

		if (child && child->kind == kind)
		{
			class node_tree *node;

			node = (class node_tree *) child;
			node->assign_attr(attr, comma);
		}
		else
		{
			if (relaxed_mode())
			{
				err_->warning(xxSyntaxTrailing,
					      comma->pos,
					      ident_->create(","),
					      lib::NoPosition,
					      ident_->create(child ? child->descr() : "<unknown>"));
			}
			else
			{
				err_->error(xxSyntaxTrailing,
					    comma->pos,
					    ident_->create(","),
					    lib::NoPosition,
					    ident_->create(child ? child->descr() : "<unknown>"));
			}

			delete comma;
		}
	}

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_method_parameter(
		const lib::pos &pos,
		class tree *type,
		class tree *identifier,
		class tree *byref)
{
	return m_node<METHOD_PARAMETER_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_METHOD_PARAMETER, EDD_METHOD_PARAMETER_ATTR_TYPE, type),
		 m_pseudo(EDD_METHOD_PARAMETER, EDD_METHOD_PARAMETER_ATTR_IDENTIFIER, identifier),
		 m_pseudo(EDD_METHOD_PARAMETER, EDD_METHOD_PARAMETER_ATTR_BYREF, byref)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_method_parameter_type_unsigned(
		unsigned long value,
		const lib::pos &pos)
{
	switch (value)
	{
		default:
			/* not possible */ assert(0);
			break;

		case EDD_METHOD_PARAMETER_TYPE_t_char:
			value = EDD_METHOD_PARAMETER_TYPE_t_uchar;
			break;
		case EDD_METHOD_PARAMETER_TYPE_t_short:
			value = EDD_METHOD_PARAMETER_TYPE_t_ushort;
			break;
		case EDD_METHOD_PARAMETER_TYPE_t_int:
			value = EDD_METHOD_PARAMETER_TYPE_t_uint;
			break;
		case EDD_METHOD_PARAMETER_TYPE_t_long:
			value = EDD_METHOD_PARAMETER_TYPE_t_ulong;
			break;
		case EDD_METHOD_PARAMETER_TYPE_t_llong:
			value = EDD_METHOD_PARAMETER_TYPE_t_ullong;
			break;
	}

	return m_enum(value, pos);
}

/**
 * \internal
 */
class tree *
edd_tree::m_method_definition(
		const lib::pos &pos,
		class tree *definition)
{
	return m_node<METHOD_DEFINITION_tree>(pos,
		m_pseudo(EDD_METHOD_DEFINITION, EDD_METHOD_DEFINITION_ATTR_DEFINITION, definition));
}

/**
 * \internal
 */
class tree *
edd_tree::m_response_code(
		const lib::pos &pos,
		class tree *integer,
		class tree *type,
		class tree *description,
		class tree *help)
{
	return m_node<RESPONSE_CODE_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_RESPONSE_CODE, EDD_RESPONSE_CODE_ATTR_INTEGER, integer),
		 m_pseudo(EDD_RESPONSE_CODE, EDD_RESPONSE_CODE_ATTR_TYPE, type),
		 m_pseudo(EDD_RESPONSE_CODE, EDD_RESPONSE_CODE_ATTR_DESCRIPTION, description),
		 m_pseudo(EDD_RESPONSE_CODE, EDD_RESPONSE_CODE_ATTR_HELP, help)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_member(
		const lib::pos &pos,
		class tree *identifier,
		class tree *reference,
		class tree *description,
		class tree *help)
{
	return m_node<MEMBER_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_MEMBER, EDD_MEMBER_ATTR_IDENTIFIER, identifier),
		 m_pseudo(EDD_MEMBER, EDD_MEMBER_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_MEMBER, EDD_MEMBER_ATTR_DESCRIPTION, description),
		 m_pseudo(EDD_MEMBER, EDD_MEMBER_ATTR_HELP, help)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_vector(
		const lib::pos &pos,
		class tree *string,
		class tree *value)
{
	return m_node<VECTOR_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_VECTOR, EDD_VECTOR_ATTR_STRING, string),
		 m_pseudo(EDD_VECTOR, EDD_VECTOR_ATTR_VALUE, value)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_edit_display_item(
		const lib::pos &pos,
		class tree *reference)
{
	return m_node<EDIT_DISPLAY_ITEM_tree>(pos,
		 m_pseudo(EDD_EDIT_DISPLAY_ITEM, EDD_EDIT_DISPLAY_ITEM_ATTR_REFERENCE, reference)
		);
}

/**
 * \internal
 */
class tree *
edd_tree::m_key_points(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<KEY_POINTS_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_waveform_type_yt(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<WAVEFORM_TYPE_YT_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_waveform_type_xy(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<WAVEFORM_TYPE_XY_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_waveform_type_horizontal(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<WAVEFORM_TYPE_HORIZONTAL_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_waveform_type_vertical(
		const lib::pos &pos,
		class tree *attribute_list)
{
	return m_node<WAVEFORM_TYPE_VERTICAL_tree>(pos,
		attribute_list);
}

/**
 * \internal
 */
class tree *
edd_tree::m_import_action(
		const lib::pos &pos,
		class tree *action,
		class tree *type,
		class tree *identifier_type,
		class tree *identifier)
{
	return m_node<IMPORT_ACTION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_IMPORT_ACTION, EDD_IMPORT_ACTION_ATTR_ACTION, action),
		 m_pseudo(EDD_IMPORT_ACTION, EDD_IMPORT_ACTION_ATTR_TYPE, type),
		 m_pseudo(EDD_IMPORT_ACTION, EDD_IMPORT_ACTION_ATTR_IDENTIFIER_TYPE, identifier_type),
		 m_pseudo(EDD_IMPORT_ACTION, EDD_IMPORT_ACTION_ATTR_IDENTIFIER, identifier)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_import_action_redefinition(
		const lib::pos &pos,
		class tree *action,
		class tree *type,
		class tree *ident,
		class tree *list)
{
	return m_node<IMPORT_ACTION_REDEFINITION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_IMPORT_ACTION_REDEFINITION, EDD_IMPORT_ACTION_REDEFINITION_ATTR_ACTION, action),
		 m_pseudo(EDD_IMPORT_ACTION_REDEFINITION, EDD_IMPORT_ACTION_REDEFINITION_ATTR_TYPE, type),
		 m_pseudo(EDD_IMPORT_ACTION_REDEFINITION, EDD_IMPORT_ACTION_REDEFINITION_ATTR_IDENTIFIER, ident),
		 m_pseudo(EDD_IMPORT_ACTION_REDEFINITION, EDD_IMPORT_ACTION_REDEFINITION_ATTR_LIST, list)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_like_action(
		const lib::pos &pos,
		class tree *action,
		class tree *t,
		class tree *info)
{
	return m_node<LIKE_ACTION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_LIKE_ACTION, EDD_LIKE_ACTION_ATTR_ACTION, action),
		 m_pseudo(EDD_LIKE_ACTION, EDD_LIKE_ACTION_ATTR_TREE, t),
		 m_pseudo(EDD_LIKE_ACTION, EDD_LIKE_ACTION_ATTR_INFO, info)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_anon_like_action(
		const lib::pos &pos,
		class tree *action,
		class tree *info)
{
	return m_node<ANON_LIKE_ACTION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_ANON_LIKE_ACTION, EDD_ANON_LIKE_ACTION_ATTR_ACTION, action),
		 m_pseudo(EDD_ANON_LIKE_ACTION, EDD_ANON_LIKE_ACTION_ATTR_INFO, info)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_open_close(
		const lib::pos &pos,
		int type,
		class tree *identifier)
{
	return m_node<OPEN_CLOSE_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_OPEN_CLOSE, EDD_OPEN_CLOSE_ATTR_TYPE, m_integer(type, pos)),
		 m_pseudo(EDD_OPEN_CLOSE, EDD_OPEN_CLOSE_ATTR_IDENTIFIER, identifier)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_constraint_if(
		const lib::pos &pos,
		class tree *expr,
		class tree *if_stmt,
		class tree *else_stmt)
{
	return m_node<CONSTRAINT_IF_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_CONSTRAINT_IF, EDD_CONSTRAINT_IF_ATTR_IF, expr),
		 m_pseudo(EDD_CONSTRAINT_IF, EDD_CONSTRAINT_IF_ATTR_THEN, if_stmt),
		 m_pseudo(EDD_CONSTRAINT_IF, EDD_CONSTRAINT_IF_ATTR_ELSE, else_stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_constraint_select(
		const lib::pos &pos,
		class tree *expr,
		class tree *selections)
{
	return m_node<CONSTRAINT_SELECT_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_CONSTRAINT_SELECT, EDD_CONSTRAINT_SELECT_ATTR_SWITCH, expr),
		 m_pseudo(EDD_CONSTRAINT_SELECT, EDD_CONSTRAINT_SELECT_ATTR_STMTS, selections)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_constraint_selection(
		const lib::pos &pos,
		class tree *expr,
		class tree *stmt)
{
	return m_node<CONSTRAINT_SELECTION_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_CONSTRAINT_SELECTION, EDD_CONSTRAINT_SELECTION_ATTR_CASE, expr),
		 m_pseudo(EDD_CONSTRAINT_SELECTION, EDD_CONSTRAINT_SELECTION_ATTR_STMT, stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_primary(
		const lib::pos &pos,
		class tree *value)
{
	return m_node<EXPR_PRIMARY_tree>(pos,
		m_pseudo(EDD_EXPR_PRIMARY, EDD_EXPR_PRIMARY_ATTR_VALUE, value));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_postfix(
		const lib::pos &pos,
		class tree *op,
		class tree *expr)
{
	return m_node<EXPR_POSTFIX_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_POSTFIX, EDD_EXPR_POSTFIX_ATTR_OP, op),
		 m_pseudo(EDD_EXPR_POSTFIX, EDD_EXPR_POSTFIX_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_unary(
		const lib::pos &pos,
		class tree *op,
		class tree *expr)
{
	return m_node<EXPR_UNARY_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_UNARY, EDD_EXPR_UNARY_ATTR_OP, op),
		 m_pseudo(EDD_EXPR_UNARY, EDD_EXPR_UNARY_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_binary(
		const lib::pos &pos,
		class tree *op,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<EXPR_BINARY_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_BINARY, EDD_EXPR_BINARY_ATTR_OP, op),
		 m_pseudo(EDD_EXPR_BINARY, EDD_EXPR_BINARY_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_EXPR_BINARY, EDD_EXPR_BINARY_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_conditional(
		const lib::pos &pos,
		class tree *expr,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<EXPR_CONDITIONAL_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_CONDITIONAL, EDD_EXPR_CONDITIONAL_ATTR_EXPR, expr),
		 m_pseudo(EDD_EXPR_CONDITIONAL, EDD_EXPR_CONDITIONAL_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_EXPR_CONDITIONAL, EDD_EXPR_CONDITIONAL_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_assignment(
		const lib::pos &pos,
		class tree *op,
		class tree *unary,
		class tree *expr)
{
	return m_node<EXPR_ASSIGNMENT_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_ASSIGNMENT, EDD_EXPR_ASSIGNMENT_ATTR_OP, op),
		 m_pseudo(EDD_EXPR_ASSIGNMENT, EDD_EXPR_ASSIGNMENT_ATTR_UNARY, unary),
		 m_pseudo(EDD_EXPR_ASSIGNMENT, EDD_EXPR_ASSIGNMENT_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_expr_comma(
		const lib::pos &pos,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<EXPR_COMMA_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_EXPR_COMMA, EDD_EXPR_COMMA_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_EXPR_COMMA, EDD_EXPR_COMMA_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_string_node(
		const lib::pos &pos,
		class tree *list)
{
	return m_node<STRING_tree>(pos,
		m_pseudo(EDD_STRING, EDD_STRING_ATTR_LIST, list));
}

/**
 * \internal
 */
class tree *
edd_tree::m_string_terminal(
		const lib::pos &pos,
		class tree *ref,
		class tree *integer)
{
	return m_node<STRING_TERMINAL_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_STRING_TERMINAL, EDD_STRING_TERMINAL_ATTR_REF, ref),
		 m_pseudo(EDD_STRING_TERMINAL, EDD_STRING_TERMINAL_ATTR_INTEGER, integer)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_string_literal(
		class tree *list)
{
	class string_tree *lx;
	class tree *l;

	assert(list);

	size_t len = 0;
	l = list;
	while (l)
	{
		assert(l->kind == EDD_string);

		lx = (class string_tree *)(l);
		len += lx->len();

		l = l->next;
	}

	std::string str;
	str.reserve(len + 1);

	/* copy it over */
	l = list;
	while (l)
	{
		assert(l->kind == EDD_string);

		lx = (class string_tree *)(l);
		str += lx->data;

		l = l->next;
	}

	/* create new terminal */
	class tree *t = new string_tree(str, this, list->pos);
	assert(t);

	/* free the string list */
	delete list;

	return t;
}

/**
 * \internal
 */
class tree *
edd_tree::m_dictionary_reference(
		const lib::pos &pos,
		class tree *identifier)
{
	return m_node<DICTIONARY_REFERENCE_tree>(pos,
		m_pseudo(EDD_DICTIONARY_REFERENCE, EDD_DICTIONARY_REFERENCE_ATTR_IDENTIFIER, identifier));
}

/**
 * \internal
 */
class tree *
edd_tree::m_typed_reference(
		const lib::pos &pos,
		unsigned long type,
		class tree *reference)
{
	return m_node<TYPED_REFERENCE_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_TYPED_REFERENCE, EDD_TYPED_REFERENCE_ATTR_TYPE, m_integer(type, pos)),
		 m_pseudo(EDD_TYPED_REFERENCE, EDD_TYPED_REFERENCE_ATTR_REFERENCE, reference)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_method_reference(
		const lib::pos &pos,
		class tree *reference)
{
	return m_node<METHOD_REFERENCE_tree>(pos,
		m_pseudo(EDD_METHOD_REFERENCE, EDD_METHOD_REFERENCE_ATTR_REFERENCE, reference));
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_ident(
		const lib::pos &pos,
		class tree *identifier)
{
	return m_node<REFERENCE_IDENT_tree>(pos,
		m_pseudo(EDD_REFERENCE_IDENT, EDD_REFERENCE_IDENT_ATTR_IDENTIFIER, identifier));
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_array_ref(
		const lib::pos &pos,
		class tree *reference,
		class tree *expr)
{
	return m_node<REFERENCE_ARRAY_REF_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_REFERENCE_ARRAY_REF, EDD_REFERENCE_ARRAY_REF_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_REFERENCE_ARRAY_REF, EDD_REFERENCE_ARRAY_REF_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_call(
		const lib::pos &pos,
		class tree *reference,
		class tree *args)
{
	return m_node<REFERENCE_CALL_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_REFERENCE_CALL, EDD_REFERENCE_CALL_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_REFERENCE_CALL, EDD_REFERENCE_CALL_ATTR_ARGS, args)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_record(
		const lib::pos &pos,
		class tree *reference,
		class tree *identifier)
{
	return m_node<REFERENCE_RECORD_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_REFERENCE_RECORD, EDD_REFERENCE_RECORD_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_REFERENCE_RECORD, EDD_REFERENCE_RECORD_ATTR_IDENTIFIER, identifier)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_reference_block(
		const lib::pos &pos,
		class tree *identifier)
{
	return m_node<REFERENCE_BLOCK_tree>(pos,
		m_pseudo(EDD_REFERENCE_BLOCK, EDD_REFERENCE_BLOCK_ATTR_IDENTIFIER, identifier));
}

/**
 * \internal
 */
class tree *
edd_tree::attach_comma(
		class tree *t,
		class tree *comma)
{
	if (comma)
	{
		class tree *child = t;

		if (child && child->kind == EDD_pseudo)
			child = ((class pseudo_tree *) child)->get();

		class node_tree *node = NULL;
		bool check = false;

		if (child)
			node = child->is_node();

		if (node)
			check = node->is_comma_element();

		if (!check)
		{
			if (relaxed_mode())
			{
				err_->warning(xxSyntaxTrailing,
					      comma->pos,
					      ident_->create(","),
					      lib::NoPosition,
					      ident_->create(child ? child->descr() : "<unknown>"));
			}
			else
			{
				err_->error(xxSyntaxTrailing,
					    comma->pos,
					    ident_->create(","),
					    lib::NoPosition,
					    ident_->create(child ? child->descr() : "<unknown>"));
			}

			delete comma;
		}
		else
			node->add_comma(comma);
	}

	return t;
}

/*
 * components
 */

/**
 * \internal
 */
class tree *
edd_tree::m_component_initial_value(
		const lib::pos &pos,
		class tree *reference,
		class tree *expr)
{
	return m_node<COMPONENT_INITIAL_VALUES_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_COMPONENT_INITIAL_VALUES, EDD_COMPONENT_INITIAL_VALUES_ATTR_REFERENCE, reference),
		 m_pseudo(EDD_COMPONENT_INITIAL_VALUES, EDD_COMPONENT_INITIAL_VALUES_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_components_reference(
		const lib::pos &pos,
		class tree *ref,
		class tree *attribute_list)
{
	return m_node<COMPONENTS_REFERENCE_tree>(pos,
			tree_concat(
				ref,
				attribute_list
				)
			);
}

/**
 * \internal
 */
class tree *
edd_tree::m_required_range(
		const lib::pos &pos,
		class tree *reference,
		class tree *argument_list)
{
	return m_node<COMPONENT_REQUIRED_RANGE_tree>(pos,
			tree_concat(
				m_pseudo(EDD_COMPONENT_REQUIRED_RANGE, EDD_COMPONENT_REQUIRED_RANGE_ATTR_REF, reference),
				argument_list
				)
			);
}

/*
 * C gramar
 */

/**
 * \internal
 */
class tree *
edd_tree::m_c_char(
		const lib::pos &pos,
		class tree *integer)
{
	return m_node<c_char_tree>(pos,
		m_pseudo(EDD_c_char, EDD_c_char_ATTR_INTEGER, integer));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_dictionary(
		const lib::pos &pos,
		class tree *identifier)
{
	return m_node<c_dictionary_tree>(pos,
		m_pseudo(EDD_c_dictionary, EDD_c_dictionary_ATTR_IDENTIFIER, identifier));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_identifier(
		const lib::pos &pos,
		class tree *identifier)
{
	return m_node<c_identifier_tree>(pos,
		m_pseudo(EDD_c_identifier, EDD_c_identifier_ATTR_IDENTIFIER, identifier));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_integer(
		const lib::pos &pos,
		class tree *integer)
{
	return m_node<c_integer_tree>(pos,
		m_pseudo(EDD_c_integer, EDD_c_integer_ATTR_INTEGER, integer));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_real(
		const lib::pos &pos,
		class tree *real)
{
	return m_node<c_real_tree>(pos,
		m_pseudo(EDD_c_real, EDD_c_real_ATTR_REAL, real));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_string(
		const lib::pos &pos,
		class tree *string)
{
	return m_node<c_string_tree>(pos,
		m_pseudo(EDD_c_string, EDD_c_string_ATTR_STRING, string));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_edd_terminal(
		const lib::pos &pos,
		class tree *edd_terminal)
{
	return m_node<c_edd_terminal_tree>(pos,
		m_pseudo(EDD_c_edd_terminal, EDD_c_edd_terminal_ATTR_TERMINAL, edd_terminal));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_primary_expr(
		const lib::pos &pos,
		class tree *value)
{
	return m_node<c_primary_expr_tree>(pos,
		m_pseudo(EDD_c_primary_expr, EDD_c_primary_expr_ATTR_VALUE, value));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_postfix_expr_array(
		const lib::pos &pos,
		class tree *expr,
		class tree *index)
{
	return m_node<c_postfix_expr_array_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_postfix_expr_array, EDD_c_postfix_expr_array_ATTR_EXPR, expr),
		 m_pseudo(EDD_c_postfix_expr_array, EDD_c_postfix_expr_array_ATTR_INDEX, index)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_postfix_expr_call(
		const lib::pos &pos,
		class tree *identifier,
		class tree *args)
{
	return m_node<c_postfix_expr_call_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_postfix_expr_call, EDD_c_postfix_expr_call_ATTR_IDENTIFIER, identifier),
		 m_pseudo(EDD_c_postfix_expr_call, EDD_c_postfix_expr_call_ATTR_ARGS, args)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_postfix_expr_struct(
		const lib::pos &pos,
		class tree *expr,
		class tree *identifier)
{
	return m_node<c_postfix_expr_struct_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_postfix_expr_struct, EDD_c_postfix_expr_struct_ATTR_EXPR, expr),
		 m_pseudo(EDD_c_postfix_expr_struct, EDD_c_postfix_expr_struct_ATTR_IDENTIFIER, identifier)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_postfix_expr_op(
		const lib::pos &pos,
		class tree *op,
		class tree *expr)
{
	return m_node<c_postfix_expr_op_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_postfix_expr_op, EDD_c_postfix_expr_op_ATTR_OP, op),
		 m_pseudo(EDD_c_postfix_expr_op, EDD_c_postfix_expr_op_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_unary_expr(
		const lib::pos &pos,
		class tree *op,
		class tree *expr)
{
	return m_node<c_unary_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_unary_expr, EDD_c_unary_expr_ATTR_OP, op),
		 m_pseudo(EDD_c_unary_expr, EDD_c_unary_expr_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_cast_expr(
		const lib::pos &pos,
		class tree *type_name,
		class tree *expr)
{
	return m_node<c_cast_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_cast_expr, EDD_c_cast_expr_ATTR_SPECIFIER, type_name),
		 m_pseudo(EDD_c_cast_expr, EDD_c_cast_expr_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_binary_expr(
		const lib::pos &pos,
		class tree *op,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<c_binary_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_binary_expr, EDD_c_binary_expr_ATTR_OP, op),
		 m_pseudo(EDD_c_binary_expr, EDD_c_binary_expr_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_c_binary_expr, EDD_c_binary_expr_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_conditional_expr(
		const lib::pos &pos,
		class tree *expr,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<c_conditional_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_conditional_expr, EDD_c_conditional_expr_ATTR_EXPR, expr),
		 m_pseudo(EDD_c_conditional_expr, EDD_c_conditional_expr_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_c_conditional_expr, EDD_c_conditional_expr_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_assignment_expr(
		const lib::pos &pos,
		class tree *op,
		class tree *unary,
		class tree *expr)
{
	return m_node<c_assignment_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_assignment_expr, EDD_c_assignment_expr_ATTR_OP, op),
		 m_pseudo(EDD_c_assignment_expr, EDD_c_assignment_expr_ATTR_UNARY, unary),
		 m_pseudo(EDD_c_assignment_expr, EDD_c_assignment_expr_ATTR_EXPR, expr)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_comma_expr(
		const lib::pos &pos,
		class tree *expr1,
		class tree *expr2)
{
	return m_node<c_comma_expr_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_comma_expr, EDD_c_comma_expr_ATTR_EXPR1, expr1),
		 m_pseudo(EDD_c_comma_expr, EDD_c_comma_expr_ATTR_EXPR2, expr2)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_declaration(
		const lib::pos &pos,
		class tree *specifier,
		class tree *declarator)
{
	return m_node<c_declaration_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_declaration, EDD_c_declaration_ATTR_SPECIFIER, specifier),
		 m_pseudo(EDD_c_declaration, EDD_c_declaration_ATTR_DECLARATOR, declarator)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_declarator(
		const lib::pos &pos,
		class tree *identifier,
		class tree *array)
{
	return m_node<c_declarator_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_declarator, EDD_c_declarator_ATTR_IDENTIFIER, identifier),
		 m_pseudo(EDD_c_declarator, EDD_c_declarator_ATTR_ARRAY, array)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_declarator_array_specifier(
		const lib::pos &pos,
		class tree *expr)
{
	return m_node<c_declarator_array_specifier_tree>(pos,
		m_pseudo(EDD_c_declarator_array_specifier, EDD_c_declarator_array_specifier_ATTR_EXPR, expr));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_labeled_statement(
		const lib::pos &pos,
		class tree *case_expr,
		class tree *stmt)
{
	return m_node<c_labeled_statement_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_labeled_statement, EDD_c_labeled_statement_ATTR_CASE, case_expr),
		 m_pseudo(EDD_c_labeled_statement, EDD_c_labeled_statement_ATTR_STMT, stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_compound_statement(
		const lib::pos &pos,
		class tree *decls,
		class tree *stmts,
		class tree *end)
{
	return m_node<c_compound_statement_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_compound_statement, EDD_c_compound_statement_ATTR_DECLS, decls),
		 m_pseudo(EDD_c_compound_statement, EDD_c_compound_statement_ATTR_STMTS, stmts),
		 m_pseudo(EDD_c_compound_statement, EDD_c_compound_statement_ATTR_END, end)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_expr_statement(
		const lib::pos &pos,
		class tree *expr)
{
	return m_node<c_expr_statement_tree>(pos,
		m_pseudo(EDD_c_expr_statement, EDD_c_expr_statement_ATTR_EXPR, expr));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_selection_statement_if(
		const lib::pos &pos,
		class tree *if_expr,
		class tree *then_stmt,
		class tree *else_stmt)
{
	return m_node<c_selection_statement_if_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_selection_statement_if, EDD_c_selection_statement_if_ATTR_IF, if_expr),
		 m_pseudo(EDD_c_selection_statement_if, EDD_c_selection_statement_if_ATTR_THEN, then_stmt),
		 m_pseudo(EDD_c_selection_statement_if, EDD_c_selection_statement_if_ATTR_ELSE, else_stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_selection_statement_switch(
		const lib::pos &pos,
		class tree *switch_expr,
		class tree *stmt)
{
	return m_node<c_selection_statement_switch_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_selection_statement_switch, EDD_c_selection_statement_switch_ATTR_SWITCH, switch_expr),
		 m_pseudo(EDD_c_selection_statement_switch, EDD_c_selection_statement_switch_ATTR_STMT, stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_iteration_statement_do(
		const lib::pos &pos,
		class tree *do_stmt,
		class tree *stmt)
{
	return m_node<c_iteration_statement_do_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_iteration_statement_do, EDD_c_iteration_statement_do_ATTR_DO, do_stmt),
		 m_pseudo(EDD_c_iteration_statement_do, EDD_c_iteration_statement_do_ATTR_STMT, stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_iteration_statement_for(
		const lib::pos &pos,
		class tree *start_stmt,
		class tree *while_stmt,
		class tree *expr_stmt,
		class tree *stmt)
{
	return m_node<c_iteration_statement_for_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_iteration_statement_for, EDD_c_iteration_statement_for_ATTR_START, start_stmt),
		 m_pseudo(EDD_c_iteration_statement_for, EDD_c_iteration_statement_for_ATTR_WHILE, while_stmt),
		 m_pseudo(EDD_c_iteration_statement_for, EDD_c_iteration_statement_for_ATTR_EXPR, expr_stmt),
		 m_pseudo(EDD_c_iteration_statement_for, EDD_c_iteration_statement_for_ATTR_STMT, stmt)
		));
}

/**
 * \internal
 */
class tree *
edd_tree::m_c_jump_statement(
		const lib::pos &pos,
		class tree *op,
		class tree *expr)
{
	return m_node<c_jump_statement_tree>(pos,
		tree_concat(
		 m_pseudo(EDD_c_jump_statement, EDD_c_jump_statement_ATTR_OP, op),
		 m_pseudo(EDD_c_jump_statement, EDD_c_jump_statement_ATTR_EXPR, expr)
		));
}

} /* namespace edd */
