
/* 
 * $Id: edd_values_ast.h,v 1.7 2009/09/21 11:01:36 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions of AST object values.
 */

#ifndef _edd_values_ast_h
#define _edd_values_ast_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
# pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <map>
#include <string>
#include <vector>

// my lib

// own
#include "edd_operand_interface.h"


namespace edd
{

/**
 * EDD no value.
 * Used as default object for non-representative EDD objects or in error
 * case.
 */
class no_value : public operand
{
public:
	/** \name Constructor/destructor. */
	//! \{
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//! \}
};

/**
 * EDD IDENTIFIER value.
 */
class identifier_value : public operand
{
protected:
	const lib::IDENTIFIER myid;

public:
	/** \name Constructor/destructor. */
	//! \{
	identifier_value(const lib::IDENTIFIER &id) : myid(id) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator lib::IDENTIFIER() const;
	//! \}
};

/**
 * EDD tree value.
 */
class tree_value : public operand
{
protected:
	class node_tree * const mytree;

public:
	/** \name Constructor/destructor. */
	//! \{
	tree_value(class node_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	operator node_tree *() const;
	operator lib::IDENTIFIER() const;
	//! \}
};
typedef operand_ptr_T<tree_value> tree_value_ptr;

/**
 * EDD object value.
 */
class edd_object_value : public tree_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	edd_object_value(class EDD_OBJECT_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void env_assert(class method_env *env) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Cast operators. */
	//! \{
	operator EDD_OBJECT_tree *() const;
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}
};
typedef operand_ptr_T<edd_object_value> edd_object_value_ptr;

/**
 * EDD object adt type.
 */
class value_adt : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	value_adt(class EDD_OBJECT_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool operator==(const operand &op) const;
	bool operator!=(const operand &op) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	virtual operand_ptr_T<value_adt> value_adt_clone(void) const = 0;
	virtual size_t size(void) const = 0;
	virtual void save_type(lib::stream &f) const = 0;
	virtual void check_type(const lib::stream &f) const = 0;
	virtual void save_value(lib::stream &f) const = 0;
	virtual void load_value(const lib::stream &f) = 0;
	//! \}

protected:
	static void expect(const lib::stream &f, char c);
	static void expect(const lib::stream &f, const char *str);
	static void expect_long(const lib::stream &f, long val);

	static std::string read_string(const lib::stream &f);
	static long read_long(const lib::stream &f);

	static void write_string(lib::stream &f, const std::string &str);
	static void write_long(lib::stream &f, long val);
};
typedef operand_ptr_T<value_adt> value_adt_ptr;

/**
 * EDD adt collection/record base value.
 */
class struct_value_adt : public value_adt
{
public:
	/** \name Constructor/destructor. */
	//! \{
	struct_value_adt(class EDD_OBJECT_tree *t, class tree *members);
protected:
	struct element
	{
		class MEMBER_tree *member;
		value_adt_ptr value;
	};

	typedef struct element element_t;
	typedef std::vector<element_t> elements_t;

	elements_t elements;

	struct_value_adt(class EDD_OBJECT_tree *t, const elements_t &);
	//! \}

public:
	/** \name Mandatory helper methods. */
	//! \{
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	size_t size(void) const;
	void save_type(lib::stream &f) const;
	void check_type(const lib::stream &f) const;
	void save_value(lib::stream &f) const;
	void load_value(const lib::stream &f);
	//! \}
};

/**
 * EDD adt collection value.
 */
class collection_value_adt : public struct_value_adt
{
public:
	/** \name Constructor/destructor. */
	//! \{
	collection_value_adt(class COLLECTION_tree *t, class tree *members);
	collection_value_adt(class COLLECTION_tree *t, const elements_t &);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	//! \}
};
typedef operand_ptr_T<collection_value_adt> collection_value_adt_ptr;

/**
 * EDD collection value.
 */
class collection_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	collection_value(class COLLECTION_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}
};
typedef operand_ptr_T<collection_value> collection_value_ptr;

/**
 * EDD adt list value.
 */
class list_value_adt : public value_adt
{
public:
	typedef value_adt_ptr element_t;
	typedef std::vector<element_t> elements_t;

	/** \name Constructor/destructor. */
	//! \{
	list_value_adt(class LIST_tree *t, const element_t,
		       long capacity, long count, bool fixed);

private:
	const size_t capacity_;
	const bool fixed_;

	const element_t def_element;
	mutable elements_t elements;

	list_value_adt(class LIST_tree *t, const element_t,
		       long capacity, bool fixed, const elements_t &);
	//! \}

public:
	/** \name Mandatory helper methods. */
	//! \{
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	size_t size(void) const;
	void save_type(lib::stream &f) const;
	void check_type(const lib::stream &f) const;
	void save_value(lib::stream &f) const;
	void load_value(const lib::stream &f);
	//! \}

	/** \name Special routines. */
	//! \{
	element_t def_value(void) { return def_element; }
	const element_t def_value(void) const { return def_element; }

	elements_t &values(void) { return elements; }
	const elements_t &values(void) const { return elements; }

	size_t capacity(void) const;
	size_t count(void) const;

	operand_ptr first(void) const;
	operand_ptr last(void) const;

	void delete_at(size_t index);
	void insert_at(size_t index, operand_ptr);

	void runtime_error_index(long index) const;
	//! \}
};
typedef operand_ptr_T<list_value_adt> list_value_adt_ptr;

/**
 * EDD list value.
 * Identicial with the adt list value.
 */
typedef class list_value_adt list_value;
typedef operand_ptr_T<list_value> list_value_ptr;

/**
 * EDD method value.
 */
class method_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	method_value(class METHOD_BASE_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator()(operand_ptr args) const;
	//! \}
};
typedef operand_ptr_T<method_value> method_value_ptr;

/**
 * EDD adt record value.
 */
class record_value_adt : public struct_value_adt
{
public:
	/** \name Constructor/destructor. */
	//! \{
	record_value_adt(class RECORD_tree *t, class tree *members);
	record_value_adt(class RECORD_tree *t, const elements_t &);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	//! \}
};
typedef operand_ptr_T<record_value_adt> record_value_adt_ptr;

/**
 * EDD record value.
 */
class record_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	record_value(class RECORD_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}
};
typedef operand_ptr_T<record_value> record_value_ptr;

/**
 * EDD component value.
 */
class component_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	component_value(class COMPONENT_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	//void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	//void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	//! \}

};
typedef operand_ptr_T<component_value> component_value_ptr;
/**
 * EDD component relation value
 */
class component_relation_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	component_relation_value(class COMPONENT_RELATION_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	//void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	//void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	//! \}

};
typedef operand_ptr_T<component_relation_value> component_relation_value_ptr;

/**
 * EDD reference array value.
 */
class reference_array_value_adt : public value_adt
{
public:
	/** \name Constructor/destructor. */
	//! \{
	reference_array_value_adt(class REFERENCE_ARRAY_tree *t, class tree *elements);
private:
	struct element
	{
		class REFERENCE_ARRAY_ELEMENT_tree *ref;
		value_adt_ptr value;
	};

	typedef struct element element_t;
	typedef std::map<long, element_t> elements_t;

	elements_t elements;

	reference_array_value_adt(class REFERENCE_ARRAY_tree *t, const elements_t &);
	//! \}

public:
	/** \name Mandatory helper methods. */
	//! \{
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	size_t size(void) const;
	void save_type(lib::stream &f) const;
	void check_type(const lib::stream &f) const;
	void save_value(lib::stream &f) const;
	void load_value(const lib::stream &f);
	//! \}
};
typedef operand_ptr_T<reference_array_value_adt> reference_array_value_adt_ptr;

/**
 * EDD reference array value.
 */
class reference_array_value : public edd_object_value
{
public:
	/** \name Constructor/destructor. */
	//! \{
	reference_array_value(class REFERENCE_ARRAY_tree *t);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	//! \}
};
typedef operand_ptr_T<reference_array_value> reference_array_value_ptr;

/**
 * EDD adt value array value.
 */
class value_array_value_adt : public value_adt
{
public:
	typedef value_adt_ptr element_t;
	typedef std::vector<element_t> elements_t;

	/** \name Constructor/destructor. */
	//! \{
	value_array_value_adt(class VALUE_ARRAY_tree *t, const element_t);
private:
	const element_t def_element;
	mutable elements_t elements;

	value_array_value_adt(class VALUE_ARRAY_tree *t, const element_t, const elements_t &);
	//! \}

public:
	/** \name Mandatory helper methods. */
	//! \{
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	void eval_values(std::vector<operand_ptr> &);
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=(const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[](long index) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	size_t size(void) const;
	void save_type(lib::stream &f) const;
	void check_type(const lib::stream &f) const;
	void save_value(lib::stream &f) const;
	void load_value(const lib::stream &f);
	//! \}

	/** \name Special routines. */
	//! \{
	operand_ptr at(long index) const;

	element_t def_value(void) { return def_element; }
	const element_t def_value(void) const { return def_element; }

	elements_t &values(void) { return elements; }
	const elements_t &values(void) const { return elements; }
	//! \}
};
typedef operand_ptr_T<value_array_value_adt> value_array_value_adt_ptr;

/**
 * EDD value array value.
 * Identicial with the adt value array value.
 */
typedef class value_array_value_adt value_array_value;
typedef operand_ptr_T<value_array_value> value_array_value_ptr;

/**
 * EDD adt variable value.
 */
class variable_value_adt : public value_adt
{
protected:
	mutable class VARIABLE_ENUMERATOR_tree *enumerator_cache;
	void post_assign_actions(void) throw();

	class VARIABLE_TYPE_tree * const type;
	operand_ptr value;
	age_stamp_t age_stamp;

public:
	/** \name Constructor/destructor. */
	//! \{
	variable_value_adt(class VARIABLE_tree *, operand_ptr);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	std::string pretty_print(void) const;
	void scan(const std::string &str, bool strict = true);
	void pretty_scan(const std::string &str);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status (void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator int8() const;
	            operator int16() const;
	            operator int32() const;
	            operator int64() const;
	            operator uint8() const;
	            operator uint16() const;
	            operator uint32() const;
	            operator uint64() const;
	            operator float32() const;
	            operator float64() const;
	            operator std::string() const;
	//! \}

	/** \name Postfix operators. */
	//! \{
	operand_ptr operator++  (int); // post
	operand_ptr operator--  (int); // post
	//! \}

	/** \name Unary operators. */
	//! \{
	operand_ptr operator++  (void); // pre
	operand_ptr operator--  (void); // pre

	operand_ptr operator+   (void) const;
	operand_ptr operator-   (void) const;
	operand_ptr operator~   (void) const;
	operand_ptr operator!   (void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	operand_ptr operator*   (const operand &op) const;
	operand_ptr operator/   (const operand &op) const;
	operand_ptr operator+   (const operand &op) const;
	operand_ptr operator-   (const operand &op) const;
	operand_ptr operator%   (const operand &op) const;
	operand_ptr operator^   (const operand &op) const;
	operand_ptr operator&   (const operand &op) const;
	operand_ptr operator|   (const operand &op) const;
	operand_ptr operator<<  (const operand &op) const;
	operand_ptr operator>>  (const operand &op) const;

	bool        operator<   (const operand &op) const;
	bool        operator>   (const operand &op) const;
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	bool        operator<=  (const operand &op) const;
	bool        operator>=  (const operand &op) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	void        operator+=  (const operand &op);
	void        operator-=  (const operand &op);
	void        operator*=  (const operand &op);
	void        operator/=  (const operand &op);
	void        operator%=  (const operand &op);
	void        operator^=  (const operand &op);
	void        operator&=  (const operand &op);
	void        operator|=  (const operand &op);
	void        operator>>= (const operand &op);
	void        operator<<= (const operand &op);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[]  (long) const;
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	size_t size(void) const;
	void save_type(lib::stream &f) const;
	void check_type(const lib::stream &f) const;
	void save_value(lib::stream &f) const;
	void load_value(const lib::stream &f);
	//! \}

	/** \name Typesafe clone. */
	//! \{
	virtual operand_ptr_T<variable_value_adt> type_clone(void) const;
	//! \}

	/** \name Special routines. */
	//! \{
	class VARIABLE_ENUMERATOR_tree *enumerator(void) const;
	age_stamp_t get_age_stamp(void) const throw() { return age_stamp; }
	void set_age_stamp(age_stamp_t as) throw() { age_stamp = as; }
	//! \}
};
typedef operand_ptr_T<variable_value_adt> variable_value_adt_ptr;

/**
 * EDD variable value.
 */
class variable_value : public edd_object_value
{
private:
	inline void pre_assign_actions(void) const;
	inline void post_assign_actions(void) const;

	variable_value_adt_ptr value;

public:
	/** \name Constructor/destructor. */
	//! \{
	variable_value(class VARIABLE_tree *t, variable_value_adt_ptr v);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	std::string pretty_print(void) const;
	void scan(const std::string &str, bool strict = true);
	void pretty_scan(const std::string &str);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status (void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator int8() const;
	            operator int16() const;
	            operator int32() const;
	            operator int64() const;
	            operator uint8() const;
	            operator uint16() const;
	            operator uint32() const;
	            operator uint64() const;
	            operator float32() const;
	            operator float64() const;
	            operator std::string() const;
	//! \}

	/** \name Postfix operators. */
	//! \{
	operand_ptr operator++  (int); // post
	operand_ptr operator--  (int); // post
	//! \}

	/** \name Unary operators. */
	//! \{
	operand_ptr operator++  (void); // pre
	operand_ptr operator--  (void); // pre

	operand_ptr operator+   (void) const;
	operand_ptr operator-   (void) const;
	operand_ptr operator~   (void) const;
	operand_ptr operator!   (void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	operand_ptr operator*   (const operand &op) const;
	operand_ptr operator/   (const operand &op) const;
	operand_ptr operator+   (const operand &op) const;
	operand_ptr operator-   (const operand &op) const;
	operand_ptr operator%   (const operand &op) const;
	operand_ptr operator^   (const operand &op) const;
	operand_ptr operator&   (const operand &op) const;
	operand_ptr operator|   (const operand &op) const;
	operand_ptr operator<<  (const operand &op) const;
	operand_ptr operator>>  (const operand &op) const;

	bool        operator<   (const operand &op) const;
	bool        operator>   (const operand &op) const;
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	bool        operator<=  (const operand &op) const;
	bool        operator>=  (const operand &op) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	void        operator+=  (const operand &op);
	void        operator-=  (const operand &op);
	void        operator*=  (const operand &op);
	void        operator/=  (const operand &op);
	void        operator%=  (const operand &op);
	void        operator^=  (const operand &op);
	void        operator&=  (const operand &op);
	void        operator|=  (const operand &op);
	void        operator>>= (const operand &op);
	void        operator<<= (const operand &op);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[]  (long) const;
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}

	/** \name Special routines. */
	//! \{
	age_stamp_t get_age_stamp(void) const throw() { return value->get_age_stamp(); }
	void set_age_stamp(age_stamp_t as) { value->set_age_stamp(as); }
	//! \}
};
typedef operand_ptr_T<variable_value> variable_value_ptr;

/*
 * Special values
 */

/**
 * EDD bit value.
 */
class bit_lvalue : public edd_object_value
{
private:
	class VARIABLE_ENUMERATOR_BITENUM_tree * const enumerator;
	operand_ptr value;

	uint32 get_value(void) const { return *value; }
	bool bit(void) const;
	void set(bool flag);

public:
	/** \name Constructor/destructor. */
	//! \{
	bit_lvalue(class VARIABLE_tree *,
		   class VARIABLE_ENUMERATOR_BITENUM_tree *,
		   operand_ptr);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void setenv0(class method_env *env) throw();
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator int8() const;
	            operator int16() const;
	            operator int32() const;
	            operator int64() const;
	            operator uint8() const;
	            operator uint16() const;
	            operator uint32() const;
	            operator uint64() const;
	            operator float32() const;
	            operator float64() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	operand_ptr operator~   (void) const;
	operand_ptr operator!   (void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	operand_ptr operator^   (const operand &op) const;
	operand_ptr operator&   (const operand &op) const;
	operand_ptr operator|   (const operand &op) const;

	bool        operator<   (const operand &op) const;
	bool        operator>   (const operand &op) const;
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	bool        operator<=  (const operand &op) const;
	bool        operator>=  (const operand &op) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr member(const lib::IDENTIFIER &id) const;
	//! \}

	/** \name Special routines. */
	//! \{
	long bitposition(void) const;
	class VARIABLE_ENUMERATOR_BITENUM_tree *get_bitenum(void) const throw() { return enumerator; }
	//! \}
};
typedef operand_ptr_T<bit_lvalue> bit_lvalue_ptr;

/**
 * EDD bitenum adt variable value.
 */
class bitenum_variable_value_adt : public variable_value_adt
{
public:
	/** \name Constructor/destructor. */
	//! \{
	bitenum_variable_value_adt(class VARIABLE_tree *, operand_ptr);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[]  (long) const;
	//! \}

	/** \name Mandatory adt helper methods. */
	//! \{
	value_adt_ptr value_adt_clone(void) const;
	//! \}

	/** \name Typesafe clone. */
	//! \{
	virtual operand_ptr_T<variable_value_adt> type_clone(void) const;
	//! \}
};

/**
 * EDD status lvalue wrapper
 */
class status_lvalue : public operand
{
private:
	value_status_t &value;
	void assign(long);

public:
	/** \name Constructor/destructor. */
	//! \{
	status_lvalue(value_status_t &v) : value(v) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator int8() const;
	            operator int16() const;
	            operator int32() const;
	            operator int64() const;
	            operator uint8() const;
	            operator uint16() const;
	            operator uint32() const;
	            operator uint64() const;
	            operator float32() const;
	            operator float64() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}
};

} /* namespace edd */

#endif /* _edd_values_ast_h */
