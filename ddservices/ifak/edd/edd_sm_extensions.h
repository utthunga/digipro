
/* 
 * $Id: edd_sm_extensions.h,v 1.44 2009/09/14 11:25:34 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for edd specific stackmachine extensions.
 *
 * Special stackmachine instructions for handling edd objects directly on the
 * stackmachine and special stackmachine data types and AST operands for
 * easy interaction between the stackmachine and AST.
 */

#ifndef _edd_sm_extensions_h
#define _edd_sm_extensions_h

// C stdlib
#include <assert.h>

// C++ stdlib

// my lib
#include "ident.h"
#include "position.h"
#include "Context.h"
#include "InstrInterface.h"

// own header
#include "edd_operand_interface.h"


namespace edd
{

/**
 * \internal
 * m_stringchar
 */
class m_stringchar : public stackmachine::Type
{
public:
	/** \name Constructor/Destructor. */
	//! \{
	m_stringchar(stackmachine::Context &, unsigned int addr, unsigned long off);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	stackmachine::Type *clone(void) const;
	std::string type_descr(void) const;
	void dump(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	std::string print(void) const;
	void scan(const std::string &);
	//! \}

	/** \name Cast operators. */
	//! \{
	     operator               bool() const;
	     operator stackmachine::int8() const;
	     operator stackmachine::int16() const;
	     operator stackmachine::int32() const;
	     operator stackmachine::int64() const;
	     operator stackmachine::uint8() const;
	     operator stackmachine::uint16() const;
	     operator stackmachine::uint32() const;
	     operator stackmachine::uint64() const;
	     operator stackmachine::float32() const;
	     operator stackmachine::float64() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	void operator+   (void);
	void operator-   (void);
	void operator~   (void);
	void operator!   (void);
	//! \}

	/** \name Binary operators. */
	//! \{
	bool operator<   (const stackmachine::Type &) const;
	bool operator>   (const stackmachine::Type &) const;
	bool operator==  (const stackmachine::Type &) const;
	bool operator!=  (const stackmachine::Type &) const;
	bool operator<=  (const stackmachine::Type &) const;
	bool operator>=  (const stackmachine::Type &) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void operator=   (const stackmachine::Type &);
	void operator+=  (const stackmachine::Type &);
	void operator-=  (const stackmachine::Type &);
	void operator*=  (const stackmachine::Type &);
	void operator/=  (const stackmachine::Type &);
	void operator%=  (const stackmachine::Type &);
	void operator^=  (const stackmachine::Type &);
	void operator&=  (const stackmachine::Type &);
	void operator|=  (const stackmachine::Type &);
	void operator>>= (const stackmachine::Type &);
	void operator<<= (const stackmachine::Type &);
	//! \}

	/** Special things. */
	//! \{
	stackmachine::int8 get(void) const;
	void assign(stackmachine::int8);
	//! \}

private:
	stackmachine::Context &c;
	unsigned int addr;
	unsigned long off;
};

/**
 * \internal
 * instr_LDI_stringchar
 */
class instr_LDI_stringchar : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * instr_STI_stringchar
 */
class instr_STI_stringchar : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * instr_IXA_stringchar
 */
class instr_IXA_stringchar : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * m_eddoperand.
 */
class m_eddoperand : public stackmachine::Type
{
public:
	/** \name Constructor/Destructor. */
	//! \{
	m_eddoperand(operand_ptr op);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	stackmachine::Type *clone(void) const;
	std::string type_descr(void) const;
	void dump(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	std::string print(void) const;
	void scan(const std::string &str, bool strict);
	//! \}

	/** Special things. */
	//! \{
	operand_ptr get(void) const { return myop; }
	operand_ptr dup(void) const { return myop->clone(); }
	//! \}

private:
	operand_ptr myop;
};

/**
 * \internal
 * instr_LDA_operand.
 */
class instr_LDA_operand : public stackmachine::InstrInterface
{
public:
	instr_LDA_operand(operand_ptr o)
	: op(o) { assert(op); }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	operand_ptr op;
};

/**
 * \internal
 * instr_LDA_operand.
 */
class instr_LDA_operand_tree : public stackmachine::InstrInterface
{
public:
	instr_LDA_operand_tree(class tree *l)
	: link(l) { assert(link); }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	class tree *link;
};

/**
 * \internal
 * instr_LDA_operand_stack.
 */
class instr_LDA_operand_stack : public stackmachine::InstrInterface
{
public:
	instr_LDA_operand_stack(unsigned int f, unsigned int o)
	: frame(f), offset(o) { }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int frame;
	unsigned int offset;
};

/**
 * \internal
 * instr_LDI_operand.
 */
class instr_LDI_operand : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * instr_STI_operand.
 */
class instr_STI_operand : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * instr_IXA_operand.
 */
class instr_IXA_operand : public stackmachine::InstrInterface
{
public:
	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;
};

/**
 * \internal
 * instr_STRUCT.
 */
class instr_STRUCT : public stackmachine::InstrInterface
{
public:
	instr_STRUCT(const lib::IDENTIFIER &i, bool l)
	: ident(i), lval(l) { }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	const lib::IDENTIFIER ident;
	const bool lval;
};

/**
 * \internal
 * instr_CALL.
 */
class instr_CALL : public stackmachine::InstrInterface
{
public:
	instr_CALL(const class c_method *p, unsigned int c);

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	const class c_method *c_method;
	const unsigned int argcount;
};

/**
 * \internal
 * instr_LDC_identifier.
 */
class instr_LDC_identifier : public stackmachine::InstrInterface
{
public:
	instr_LDC_identifier(const lib::IDENTIFIER &id)
	: myid(id) { }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	const lib::IDENTIFIER myid;
};

/**
 * \internal
 * instr_LDC_dictionary.
 */
class instr_LDC_dictionary : public stackmachine::InstrInterface
{
public:
	instr_LDC_dictionary(class edd_tree *p, const lib::IDENTIFIER &id)
	: parent(p), myid(id) { }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	class edd_tree *parent;
	const lib::IDENTIFIER myid;
};

/**
 * \internal
 * instr_LDC_enumstring.
 */
class instr_LDC_enumstring : public stackmachine::InstrInterface
{
public:
	instr_LDC_enumstring(class VARIABLE_tree *_vt)
	: vt(_vt) { }

	void execute(stackmachine::Context &c);
	void dump(lib::stream &f) const;

private:
	class VARIABLE_tree *vt;
};

} /* namespace edd */

#endif /* _edd_sm_extensions_h */
