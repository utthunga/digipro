
/* 
 * $Id: edd_builtins_interface.h,v 1.13 2006/07/13 12:52:04 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the abstract builtin interface.
 */

#ifndef _edd_builtins_interface_h
#define _edd_builtins_interface_h

// C stdlib

// C++ stdlib
#include <string>

// my lib

// own header
#include "cocktail_types.h"


namespace edd
{

/**
 * Builtin abstraction.
 * This is the abstract interface for handling EDD specific builtin
 * functions. It only feature the pure virtual method lookup that search the
 * builtin functions for the specified name. If a builtin function is found
 * a pointer to a c_method object is returned.
 */
class edd_builtins_interface
{
public:
	virtual ~edd_builtins_interface(void) { }
	
	virtual const class c_method *lookup(const std::string &name) = 0;
};

} /* namespace edd */

#endif /* _edd_builtins_interface_h */
