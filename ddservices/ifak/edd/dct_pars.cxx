
/* $Id: Parser.cxx,v 1.8 2007/07/17 15:06:17 fna Exp $ */

#include <stdio.h>
#include <string.h>
#include "port.h"

#include "dct_pars.h"

#ifdef _MSC_VER
# pragma warning (disable: 4102)
#endif


# line 36 "dct_pars.lalr"


/* GLOBAL BEGIN */

#include "edd_ast.h"
#include "edd_tree.h"

#define NullTree				0
#define NoTree					0
#define ABSTRACT				parent->

#define MyPos(a1)				mypos(a1)

#define mCons_string_list(a1,a2)		(a1->next = a2, a1)
#define mNil_string_list()			NoTree

#define mIdentifier(a1,a2)			a1.release()
#define mInteger(a1,a2)				ABSTRACT m_integer(a1,a2)
#define mString(a1,a2)				ABSTRACT m_str_list(a1,a2)

/* GLOBAL END */


# line 40 "dct_pars.cxx"

namespace CT_NAMESPACE
{

#ifndef DOXYGEN_IGNORE

#define const const int
/* C++ forbid implicit int */
static const yyFirstTerminal	= 0;
static const yyLastTerminal	= 6;
static const yySetSize	= 7;
static const yyFirstSymbol	= 0;
static const yyLastSymbol	= 15;
static const yyTTableMax	= 14;
static const yyNTableMax	= 20;
static const yyStartState	= 1;
static const yyFirstReadState	= 1;
static const yyLastReadState	= 10;
static const yyFirstReadReduceState	= 11;
static const yyLastReadReduceState	= 14;
static const yyFirstReduceState	= 15;
static const yyLastReduceState	= 25;
static const yyLastState	= 25;
static const yyLastStopState	= 15;
static const yyFirstFinalState	= yyFirstReadReduceState;
#undef const

#ifndef yyGetAttribute
# define yyGetAttribute(yyAttrStackPtr, a) (yyAttrStackPtr)->Scan = a
#endif

#define ErrorMessages(Messages) yyControl.yyMessages = Messages
#define SemActions(Actions)	 yyControl.yyActions = Actions

#ifdef YYGetLook
# define GetLookahead(k)	yyGetLookahead((k) - 1, yyTerminal)
# define GetAttribute(k, a)	xxGetAttribute((k) - 1, yyTerminal, a)
#endif

#ifndef yyInitStackSize
# define yyInitStackSize	256
#endif
static const int yyNoState	= 0;
static const int yystandard	= 1;
static const int yytrial	= 2;
static const int yybuffer	= 4;
static const int yyreparse	= 8;
#define yyS			yySynAttribute
#define yyA			yyAttrStackPtr
#define YYACCEPT		goto yyAccept
#define YYABORT			goto yyAbort

#ifdef YYDEC_TABLE
# define yyDecrement(x)
# define yySetNT(x)
#else
# define yyDecrement(x)		yyStateStackPtr -= x; yyAttrStackPtr -= x;
# define yySetNT(x)		yyNonterminal = x;
#endif

#ifdef YYNDefault
# define yytNComb yytComb
#else
# define yytNComb yyStateRange
#endif

typedef struct { yyStateRange Check, Next; } yytComb;
typedef enum {
yyNT0_intern	= 7,
yyNTdescription	= 8,
yyNTentry_list	= 9,
yyNTentry	= 10,
yyNTDictionaryKey	= 11,
yyNTstring_listR	= 12,
yyNTIdentifier	= 13,
yyNTInteger	= 14,
yyNTString	= 15
} yytNonterminal;

#ifdef YYDEBUG
static const char * const yyRule[] = { 0,
"0_intern : description _EOF_ ",
"description : entry_list ",
"entry_list : ",
"entry_list : entry_list entry ",
"entry : DictionaryKey Identifier string_listR ",
"DictionaryKey : '[' Integer ',' Integer ']' ",
"string_listR : String ",
"string_listR : string_listR String ",
"Identifier : ident ",
"Integer : IntConst ",
"String : StringConst ",
""
};
#endif
static const char * const yyTokenName[yyLastTerminal + 2] = {
"_EOF_",
"ident",
"IntConst",
"StringConst",
"[",
",",
"]",
""
};
static const yytComb yyTComb[yyTTableMax + 1] = {
{   9,   19}, {   1,   17}, {   3,   16}, {   9,   14}, {   9,   19}, 
{   1,   17}, {   3,    4}, {   2,   15}, {   5,   12}, {   4,   13}, 
{   8,   13}, {   7,   14}, {   6,    8}, {  10,   11}, {   0,    0}, 
};
static const yytNComb yyNComb[yyNTableMax - yyLastTerminal]={
    0,     2,     3,    18,     5,     9,     0,     0,    21,    22, 
    7,     6,    10,     0, 
};
static const yytComb * const yyTBasePtr[yyLastReadState + 1] = { 0,
& yyTComb [   1], & yyTComb [   7], & yyTComb [   2], & yyTComb [   7], 
& yyTComb [   7], & yyTComb [   7], & yyTComb [   8], & yyTComb [   8], 
& yyTComb [   0], & yyTComb [   7], 
};
static const yytNComb * const yyNBasePtr[yyLastReadState + 1] = { 0,
& yyNComb [  -7], & yyNComb [  -7], & yyNComb [  -7], & yyNComb [  -3], 
& yyNComb [  -3], & yyNComb [  -7], & yyNComb [  -7], & yyNComb [  -2], 
& yyNComb [  -6], & yyNComb [  -7], 
};
#ifdef YYTDefault
static const unsigned short yyTDefault[yyLastReadState + 1] = { 0,
};
#endif
#ifdef YYNDefault
static const unsigned short yyNDefault[yyLastReadState + 1] = { 0,
};
#endif
#if !defined NO_RECOVER | defined YYDEC_TABLE
static const unsigned char yyLength[yyLastReduceState - yyFirstReduceState + 1] = {
    2,     1,     0,     2,     3,     5,     1,     2,     1,     1, 
    1, 
};
static const yytNonterminal yyLeftHandSide[yyLastReduceState - yyFirstReduceState + 1] = {
yyNT0_intern,
yyNTdescription,
yyNTentry_list,
yyNTentry_list,
yyNTentry,
yyNTDictionaryKey,
yyNTstring_listR,
yyNTstring_listR,
yyNTIdentifier,
yyNTInteger,
yyNTString,
};
#endif
#ifndef NO_RECOVER
static const yySymbolRange yyContinuation[yyLastReadState + 1] = { 0,
    0,     0,     0,     2,     1,     5,     3,     2,     0,     6, 
};
static const unsigned short yyCondition[yyLastState - yyLastReduceState + 1] = { 0,
};
#endif
static const unsigned short yyFinalToProd[yyLastReadReduceState - yyFirstReadReduceState + 2] = {
   20,    23,    24,    25, 
0
};
static const unsigned short yyStartLine[yyLastStopState - yyFirstReduceState + 2] = { 0,
95,
};
#ifdef YYaccDefault

static const unsigned long * const yyDefaultLook[yyLastReadState + 1] = { 0,
};
#endif

#endif /* DOXYGEN_IGNORE */

#ifdef IDENT_IS_AUTO_PTR
# define PARENT_ERROR			parent->err().error
# define PARENT_REPAIR			parent->err().repair
# define PARENT_INFORMATION		parent->err().information
# define PARENT_IDENT_CREATE		parent->ident().create
# define PARENT_IDENT_CREATE_FROM_BUF	parent->ident().create_from_buf
# define PARENT_IDENT_NoIdent		parent->ident().NoIdent
#else
# define PARENT_ERROR			parent->err->error
# define PARENT_REPAIR			parent->err->repair
# define PARENT_INFORMATION		parent->err->information
# define PARENT_IDENT_CREATE		parent->ident->create
# define PARENT_IDENT_CREATE_FROM_BUF	parent->ident->create_from_buf
# define PARENT_IDENT_NoIdent		parent->ident->NoIdent
#endif

#if defined YYTrialParse | defined YYReParse | defined YYGetLook

#ifndef yyInitBufferSize
# define yyInitBufferSize 256
#endif

#endif /* YYTrialParse | YYReParse | YYGetLook */

dct_pars::dct_pars (PARENT *p)
: parent(p)
{
	Debug = false;
	TokenName = yyTokenName;
	yyStateStackSize = yyInitStackSize;
	yyAttrStackSize = yyInitStackSize;
	yyIsContStackSize = yyInitStackSize;
	yyCompResStackSize = yyInitStackSize;
	
#ifdef YYTrialParse
	yyBufferSize = yyInitBufferSize;
	yyBuffer = new yytBuffer[yyBufferSize];
	yyBufferNext = 1;
	yyBufferLast = 1;
	yyBufferClear = true;
	yyParseLevel = 0;
#endif
#ifdef YYDEBUG
	yyCount = 0;
	yyTrace = stdout;
#endif
{
	/* BEGIN section is inserted here */
# line 67 "dct_pars.lalr"


/* BEGIN BEGIN */
/* BEGIN END */


# line 269 "dct_pars.cxx"
}

}

dct_pars::~dct_pars (void)
{
	/* CLOSE section is inserted here */
# line 74 "dct_pars.lalr"


/* CLOSE BEGIN */
/* CLOSE END */


# line 284 "dct_pars.cxx"
	
#ifdef YYTrialParse
	delete [] yyBuffer;
#endif
}

#if defined YYTrialParse | defined YYReParse | defined YYGetLook

#ifndef TOKENOP
# define TOKENOP
#endif
#ifndef BEFORE_TRIAL
# define BEFORE_TRIAL
#endif
#ifndef AFTER_TRIAL
# define AFTER_TRIAL
#endif

void
dct_pars::yyBufferExtend (void)
{
	yytBuffer *tmp;
	unsigned long i;
	
	tmp = new yytBuffer[yyBufferSize * 2];
	assert(tmp);
	
	for (i = 0; i < yyBufferSize; i++)
		tmp[i] = yyBuffer[i];
	
	delete [] yyBuffer;
	
	yyBuffer = tmp;
	yyBufferSize *= 2;
}

int
dct_pars::yyGetToken (void)
{
	yySymbolRange yyToken;
	
	if (yyBufferNext < yyBufferLast)
	{
		yyToken = yyBuffer[++yyBufferNext].yyToken;
		scanner->Attribute = yyBuffer[yyBufferNext].yyAttribute;
	}
	else
	{
		yyToken = scanner->GetToken();
		if ((yytrial | yybuffer) & yyControl.yyMode)
		{
			if (++yyBufferLast >= yyBufferSize)
			{
				yyBufferExtend();
#ifdef YYDEBUG
				if (Debug)
				{
					yyPrintState(0);
					fprintf(yyTrace, "extend  token buffer from %ld to %ld",
						yyBufferSize / 2, yyBufferSize); yyNl ();
				}
#endif
			}
			yyBuffer[yyBufferLast].yyToken = yyToken;
			yyBuffer[yyBufferLast].yyAttribute = scanner->Attribute;
#ifdef YYMemoParse
			yyBuffer[yyBufferLast].yyStart = 0;
#endif
			yyBufferNext = yyBufferLast;
		}
	}
	
	TOKENOP
	
	return yyToken;
}

void
dct_pars::yyBufferSet (yySymbolRange yyToken)
{
	if (yyBufferNext == yyBufferLast)
	{
		if (yyBufferClear)
			yyBufferLast = 0;
		
		if (++yyBufferLast >= yyBufferSize)
		{
			yyBufferExtend();
#ifdef YYDEBUG
			if (Debug)
			{
				yyPrintState (0);
				fprintf(yyTrace, "extend  token buffer from %ld to %ld",
					yyBufferSize / 2, yyBufferSize);
				yyNl();
			}
#endif
		}
		
		yyBuffer[yyBufferLast].yyToken	= yyToken;
		yyBuffer[yyBufferLast].yyAttribute= scanner->Attribute;
#ifdef YYMemoParse
		yyBuffer[yyBufferLast].yyStart	= 0;
#endif
		yyBufferNext = yyBufferLast;
	}
}

#else /* YYTrialParse | YYReParse | YYGetLook */
# define yyGetToken scanner->GetToken
#endif /* YYTrialParse | YYReParse | YYGetLook */

#ifdef YYGetLook

int
dct_pars::yyGetLookahead (int yyk, yySymbolRange yyToken)
{
	if (yyk == 0)
		return yyToken;
	
	if (yyControl.yyMode == yystandard)
		yyBufferSet(yyToken);
	
	while (yyBufferNext + yyk > yyBufferLast)
	{
		if (yyBuffer[yyBufferLast].yyToken == scanner->EofToken)
			return scanner->EofToken;
		
		if (++yyBufferLast >= yyBufferSize)
		{
			yyBufferExtend();
#ifdef YYDEBUG
			if (Debug)
			{
				yyPrintState(0);
				fprintf(yyTrace, "extend  token buffer from %ld to %ld",
					yyBufferSize / 2, yyBufferSize);
				yyNl();
			}
#endif
		}
		yyBuffer[yyBufferLast].yyToken = scanner->GetToken();
		yyBuffer[yyBufferLast].yyAttribute = scanner->Attribute;
#ifdef YYMemoParse
		yyBuffer[yyBufferLast].yyStart = 0;
#endif
	}
	
	scanner->Attribute = yyBuffer[yyBufferNext].yyAttribute;
	return yyBuffer[yyBufferNext + yyk].yyToken;
}

void
dct_pars::xxGetAttribute (int yyk, yySymbolRange yyToken, tScanAttribute * yyAttribute)
{
	if (yyk == 0)
		*yyAttribute = scanner->Attribute;
	else
	{
		yyGetLookahead(yyk, yyToken);
		*yyAttribute = yyBuffer[Min(yyBufferNext + yyk, yyBufferLast)].yyAttribute;
	}
}

#endif /* YYGetLook */

#ifdef YYReParse

#define BufferOn(Actions, Messages) yyBufferOn(Actions, Messages, yyTerminal)
#define BufferPosition	yyBufferNext

long
dct_pars::yyBufferOn (bool yyActions, bool yyMessages, yySymbolRange yyToken)
{
	if (yyControl.yyMode == yystandard)
	{
		yyPrevControl		= yyControl;
		yyControl.yyMode	= yybuffer;
		yyControl.yyActions	= yyActions;
		yyControl.yyMessages	= yyMessages;
		yyBufferSet(yyToken);
		yyBufferClear		= false;
	}
	return yyBufferNext;
}

long
dct_pars::BufferOff (void)
{
	if (yyControl.yyMode == yybuffer)
		yyControl = yyPrevControl;
	
	return yyBufferNext;
}

void
dct_pars::BufferClear (void)
{
	yyBufferClear = true;
}

#endif /* YYReParse */

#ifdef YYDEBUG

void
dct_pars::yyNl (void) const
{
	putc('\n', yyTrace);
	fflush(yyTrace);
}

void
dct_pars::yyPrintState (yyStateRange yyState)
{
	fprintf(yyTrace, "%4ld:", ++yyCount);
	fprintf(yyTrace, "%4ld,%3ld",
		scanner->Attribute.Position.Line,
		scanner->Attribute.Position.Column);
	fprintf(yyTrace, ":%5d  %c  ",
		yyState,
		" ST-B---R"[yyControl.yyMode]);
#if defined YYTrialParse | defined YYReParse
	if (yyParseLevel > 0)
	{
		int yyi = yyParseLevel;
		fprintf(yyTrace, "%2d  ", yyi);
		do fputs("  ", yyTrace);
		while (--yyi);
	}
	else
#endif
		fputs("    ", yyTrace);
}

bool
dct_pars::yyPrintResult (yyStateRange yyState, int yyLine, bool yyCondition)
{
	if (Debug)
	{
		yyPrintState(yyState);
		fprintf(yyTrace, "check   predicate in line %d, result = %d",
			yyLine, yyCondition);
		yyNl();
	}

	return yyCondition;
}

#else /* YYDEBUG */
# define yyPrintResult(State, Line, Condition) Condition
#endif /* YYDEBUG */

#if defined YYDEBUG | defined YYDEC_TABLE
# define yyGotoReduce(State, Rule)	{ yyState = State; goto yyReduce; }
# define yyGotoRead(State)		{ yyState = State; goto yyRead; }
#else
# define yyGotoReduce(State, Rule)	goto Rule;
# define yyGotoRead(State)		{ yyState = State; goto yyRead; }
#endif

lib::pos
dct_pars::mypos(const reuse::tPosition &pos) const
{
	return scanner->mypos(pos);
}

lib::pos
dct_pars::mypos(const reuse::tPosition &pos1, const reuse::tPosition &pos2) const
{
	return scanner->mypos(pos1, pos2);
}

int
dct_pars::Parse (dct_pars_ret **t, int mode)
{
	return Parse(t, mode, yyStartState);
}

int
dct_pars::Parse (dct_pars_ret **t, int mode, yyStateRange yyStartSymbol)
{
	int yyErrorCount;
	
	yyStateStack = new yyStateRange[yyStateStackSize];
	yyAttributeStack = new tParsAttribute[yyAttrStackSize];
	yyIsContStackPtr = new yyStateRange[yyIsContStackSize];
	yyCompResStackPtr = new yyStateRange[yyCompResStackSize];
	
	yyEndOfStack	= &yyStateStack[yyStateStackSize];
#if defined YYTrialParse | defined YYReParse
	yyStateStackPtr	= yyStateStack;
	yyAttrStackPtr	= yyAttributeStack;
	yyBufferNext	= 1;
	yyBufferLast	= 1;
	yyParseLevel	= 0;
#endif
#ifdef YYDEBUG
	if (Debug)
	{
		fprintf(yyTrace, "  # |Position|State|Mod|Lev|Action |Terminal and Lookahead or Rule\n");
		yyNl();
	}
#endif
	yyControl.yyMode	= yystandard;
	yyControl.yyActions	= true;
	yyControl.yyMessages	= true;
	
	yyErrorCount = yyParse(t, mode,
			(yyStateRange) yyStartSymbol,
			(yySymbolRange) yyGetToken(),
			yyStartLine[yyStartSymbol]);
	
	delete [] yyCompResStackPtr;
	delete [] yyIsContStackPtr;
	delete [] yyAttributeStack;
	delete [] yyStateStack;
	
	return yyErrorCount;
}

void
dct_pars::yyStateRangeArrayExtend(yyStateRange *&ptr, long &size)
{
	yyStateRange *tmp;
	long i;
	
	tmp = new yyStateRange[size * 2];
	assert(tmp);
	
	for (i = 0; i < size; i++)
		tmp[i] = ptr[i];
	
	delete [] ptr;
	
	ptr = tmp;
	size *= 2;
}

#ifdef YYTrialParse

#ifdef YYMemoParse
# define MemoryClear(Position) yyBuffer [Position].yyStart = 0
#endif

int
dct_pars::yyTrialParse (yyStateRange yyStartSymbol, yySymbolRange yyToken, int yyLine)
{
	int yyErrorCount;
	unsigned long yyPrevStateStackPtr = yyStateStackPtr - yyStateStack;
	unsigned long yyPrevAttrStackPtr = yyAttrStackPtr - yyAttributeStack;
	long yyPrevBufferNext;
	yytControl yyPrevControl;
	
	BEFORE_TRIAL;
	
#ifdef YYMemoParse
	if (yyBuffer[yyBufferNext].yyStart ==  yyStartSymbol) return 0;
	if (yyBuffer[yyBufferNext].yyStart == -yyStartSymbol) return 1;
#endif
	yyPrevControl = yyControl;
	yyStateStackPtr++;
	yyAttrStackPtr++;
	yyParseLevel++;
	if (yyControl.yyMode == yystandard)
		yyBufferSet(yyToken);
	yyPrevBufferNext = yyBufferNext;
	yyControl.yyMode = yytrial;
	yyControl.yyActions = false;
	yyControl.yyMessages = false;
	yyErrorCount = yyParse(0, 0, yyStartSymbol, yyToken, yyLine);
#ifdef YYMemoParse
	yyBuffer[yyPrevBufferNext].yyStart = yyErrorCount ? -yyStartSymbol : yyStartSymbol;
#endif
	yyStateStackPtr = yyStateStack + yyPrevStateStackPtr;
	yyAttrStackPtr = yyAttributeStack + yyPrevAttrStackPtr;
	yyBufferNext = yyPrevBufferNext;
	yyControl = yyPrevControl;
	yyParseLevel--;
	scanner->Attribute = yyBuffer[yyBufferNext].yyAttribute;
	
	AFTER_TRIAL;
	
	return yyErrorCount;
}
#endif /* YYTrialParse */

#ifdef YYReParse
int
dct_pars::ReParse (yyStateRange yyStartSymbol,
	       int yyFrom,
	       int yyTo,
	       bool yyActions,
	       bool yyMessages)
{
	int yyErrorCount = 1;
	
	if (1 <= yyFrom && yyFrom <= yyTo && yyTo <= yyBufferLast)
	{
		unsigned long yyPrevStateStackPtr = yyStateStackPtr - yyStateStack;
		unsigned long yyPrevAttrStackPtr = yyAttrStackPtr - yyAttributeStack;
		long yyPrevBufferNext = yyBufferNext;
		int yyToToken = yyBuffer[yyTo].yyToken;
		yytControl yyPrevControl;
		
		yyPrevControl = yyControl;
		yyStateStackPtr++;
		yyAttrStackPtr++;
		yyParseLevel++;
		yyBufferNext= yyFrom - 1;
		yyBuffer[yyTo].yyToken = scanner->EofToken;
		yyControl.yyMode = yyreparse;
		yyControl.yyActions = yyActions;
		yyControl.yyMessages = yyMessages;
		
		yyErrorCount = yyParse(0, yyStartSymbol,
				(yySymbolRange) yyGetToken(),
				yyStartLine[yyStartSymbol]);
		
		yyStateStackPtr	= yyStateStack + yyPrevStateStackPtr;
		yyAttrStackPtr = yyAttributeStack + yyPrevAttrStackPtr;
		yyBufferNext = yyPrevBufferNext;
		yyControl = yyPrevControl;
		yyParseLevel--;
		yyBuffer[yyTo].yyToken = yyToToken;
		scanner->Attribute = yyBuffer[yyBufferNext].yyAttribute;
	}
	else
	{
		PARENT_ERROR(xxInternalFailure,
			     mypos(scanner->Attribute.Position),
			     PARENT_IDENT_CREATE("invalid call of ReParse"));
	}
	
	return yyErrorCount;
}
#endif /* YYReParse */

void
dct_pars::yySyntaxError (void) const
{
	PARENT_ERROR(xxSyntaxError,
		     mypos(scanner->Attribute.Position),
		     PARENT_IDENT_CREATE_FROM_BUF(scanner->TokenPtr, scanner->TokenLength));
}

int
dct_pars::yyParse (dct_pars_ret **t, int mode, yyStateRange yyStartSymbol, yySymbolRange yyToken, int yyLine)
{
	yyStateRange yyState = yyStartSymbol;
	yySymbolRange yyTerminal = yyToken;
	bool yyIsRepairing = false;
 	tParsAttribute yySynAttribute;   /* synthesized attribute */
	int yyErrorCount = 0;
	
	/* zero out yySynAttribute */
	memset(&yySynAttribute, 0, sizeof(yySynAttribute));
	
#if !(defined YYTrialParse | defined YYReParse)
	yyStateRange *yyStateStackPtr = yyStateStack;
	tParsAttribute *yyAttrStackPtr = yyAttributeStack;
#endif
#ifdef YYDEBUG
	long yyStartCount = yyCount + 1;
	yySymbolRange yyPrevTerminal = yyToken;
#endif
#ifdef YYGetLook
	yySymbolRange yy2;
#endif
	
dct_pars_ret *root = NULL;
	if (t) *t = NULL;
	
# line 60 "dct_pars.lalr"


/* LOCAL BEGIN */
/* LOCAL END */


# line 765 "dct_pars.cxx"

#ifdef YYDEBUG
	if (Debug)
	{
		yyPrintState(yyStartSymbol);
		fprintf(yyTrace,
	 		"parse   for predicate in line %d, lookahead: %s",
			yyLine,
			yyTokenName[yyTerminal]);
		yyNl();
	}
#endif

yyParseLoop:
	for (;;)
	{
		if (yyStateStackPtr >= yyEndOfStack)
		{
			/* extend StateStack array */
			{
				unsigned long yyyStateStackPtr = yyStateStackPtr - yyStateStack;
				yyStateRange *tmp;
				unsigned long i;
				
				tmp = new yyStateRange[yyStateStackSize*2];
				for (i = 0; i < yyStateStackSize; i++)
					tmp[i] = yyStateStack[i];
				
				delete [] yyStateStack;
				
				yyStateStack = tmp;
				yyStateStackSize *= 2;
				
				/* adjust internal pointer */
				yyStateStackPtr = yyStateStack + yyyStateStackPtr;
				yyEndOfStack = &yyStateStack[yyStateStackSize];
			}
			
			/* extend AttributeStack array */
			{
				unsigned long yyyAttrStackPtr = yyAttrStackPtr - yyAttributeStack;
				tParsAttribute *tmp;
				unsigned long i;
				
				tmp = new tParsAttribute[yyAttrStackSize*2];
				for (i = 0; i < yyAttrStackSize; i++)
					tmp[i] = yyAttributeStack[i];
				
				delete [] yyAttributeStack;
				
				yyAttributeStack = tmp;
				yyAttrStackSize *= 2;
				
				/* adjust internal pointer */
				yyAttrStackPtr = yyAttributeStack + yyyAttrStackPtr;
			}
			
#ifdef YYDEBUG
			if (Debug)
			{
				yyPrintState(yyState);
				fprintf(yyTrace, "extend  stack from %ld to %ld",
					yyStateStackSize / 2, yyStateStackSize); yyNl ();
			}
#endif
		}
		
		*yyStateStackPtr = yyState;

#ifdef YYTDefault
# ifndef YYaccDefault
yyTermTrans:
# endif
#endif
		for (;;) /* SPEC State = Next (State, Terminal); terminal transit */
		{
			const yytComb *yyTCombPtr;
			
			yyTCombPtr = yyTBasePtr[yyState] + yyTerminal;
			if (yyTCombPtr->Check == yyState)
			{
				yyState = yyTCombPtr->Next;
				break;
			}
#ifdef YYTDefault
# ifdef YYaccDefault
			unsigned long *yylp;
			
			yylp = yyDefaultLook[yyState]
			if (yylp && (yylp[yyTerminal >> 5] >> (yyTerminal & 0x1f)) & 1)
			{
				yyState = yyTDefault[yyState];
				break;
			}
# else
			if ((yyState = yyTDefault[yyState]) != yyNoState)
				goto yyTermTrans;
# endif
#endif

			/* syntax error */
			
			if (!yyIsRepairing)			/* report and recover */
			{
				yySymbolRange yyyTerminal = (yySymbolRange) yyTerminal;

#ifdef YYTrialParse
				if (yyControl.yyMode == yytrial)
					YYABORT;
#endif
#ifndef NO_RECOVER
				yyErrorCount++;
				yyErrorRecovery(&yyyTerminal,
						yyStateStack,
						(yyStateStackPtr - yyStateStack));
				yyTerminal = yyyTerminal;
				yyIsRepairing = true;
#else
				yySyntaxError();
				YYABORT;
#endif
			}
#ifndef NO_RECOVER
			yyState = *yyStateStackPtr;
			for (;;)
			{
				if (yyNext(yyState, (yySymbolRange) yyTerminal) == yyNoState)
				{
					yySymbolRange yyRepairToken;		/* repair */
 					tScanAttribute yyRepairAttribute;

					yyRepairToken = yyContinuation[yyState];
					yyState = yyNext(yyState, yyRepairToken);
					if (yyState <= yyLastReadReduceState)
					{
						/* read or read reduce ? */
						scanner->ErrorAttribute((int) yyRepairToken, &yyRepairAttribute);
						if (yyControl.yyMessages)
						{
							PARENT_REPAIR(xxTokenInserted,
								      mypos(scanner->Attribute.Position),
								      PARENT_IDENT_CREATE(yyTokenName[yyRepairToken]));
						}
#ifdef YYDEBUG
						if (Debug)
						{
							yyPrintState(*yyStateStackPtr);
							fprintf(yyTrace, "insert  %s", yyTokenName[yyRepairToken]);
							yyNl();
							yyPrintState(*yyStateStackPtr);
							fprintf(yyTrace, "shift   %s, lookahead: %s",
							yyTokenName[yyRepairToken], yyTokenName[yyTerminal]);
							yyNl();
                				}
#endif
						if (yyState >= yyFirstFinalState)
						{
							/* avoid second push */
							yyState = yyFinalToProd[yyState - yyFirstReadReduceState];
						}
					
						yyGetAttribute(yyAttrStackPtr++, yyRepairAttribute);
						*++yyStateStackPtr = yyState;
					}

					/* final state? */
					if (yyState >= yyFirstFinalState)
						goto yyFinal;
				}
				else
				{
					yyState = yyNext(yyState, (yySymbolRange) yyTerminal);
					goto yyFinal;
				}
			}
#endif
		}

yyFinal:
		/* final state? */
		if (yyState >= yyFirstFinalState)
		{
			/* read reduce? */
			if (yyState <= yyLastReadReduceState)
			{
				yyStateStackPtr++;
				yyGetAttribute(yyAttrStackPtr++, scanner->Attribute);
				yyTerminal = yyGetToken();
#ifdef YYDEBUG
				if (Debug)
				{
					yyStateStackPtr[0] = yyStateStackPtr[-1];
					yyPrintState(*yyStateStackPtr);
					fprintf(yyTrace, "shift   %s, lookahead: %s",
						yyTokenName[yyPrevTerminal], yyTokenName[yyTerminal]);
					yyNl();
					yyPrevTerminal = yyTerminal;
				}
#endif
				yyIsRepairing = false;
			}

			for (;;)
			{
				// XXX for yyParse2 changed to int
				// yytNonterminal yyNonterminal; /* left-hand side */
				int yyNonterminal; /* left-hand side */

yyReduce:
				yyNonterminal = yyNT0_intern;
#ifdef YYDEBUG
				if (Debug)
				{
					/* read reduce? */
					if (yyState <= yyLastReadReduceState)
						yyState = yyFinalToProd[yyState - yyFirstReadReduceState];

					yyPrintState(*yyStateStackPtr);
					if (yyState <= yyLastReduceState)
					{
						fprintf(yyTrace, "reduce  %s",
							yyRule[yyState - yyLastReadReduceState]);
						yyNl();
					}
					else
					{
						fprintf(yyTrace, "dynamic decision %d",
							yyState - yyLastReduceState);
						yyNl();
					}
				}
#endif
#ifdef YYDEC_TABLE
				if (yyLastStopState < yyState && yyState <= yyLastReduceState)
				{
					int yyd = yyLength[yyState - yyFirstReduceState];
					yyStateStackPtr -= yyd;
					yyAttrStackPtr -= yyd;
					yyNonterminal = yyLeftHandSide[yyState - yyFirstReduceState];
				}
#endif
				
#ifndef DOXYGEN_IGNORE
				
switch (yyState) {
case 15:
YYACCEPT;
case 16: /* description : entry_list */
yyDecrement (1) yySetNT (yyNTdescription) {
} break;
case 17: /* entry_list : */
yySetNT (yyNTentry_list) {
} break;
case 18: /* entry_list : entry_list entry */
yyDecrement (2) yySetNT (yyNTentry_list) {
} break;
case 19: /* entry : DictionaryKey Identifier string_listR */
yyDecrement (3) yySetNT (yyNTentry) {
# line 98 "dct_pars.lalr"
 ;
{  ABSTRACT m_dictionary_entry(
				MyPos(yyA [1].Identifier.Pos),
				yyA [1].Identifier.Tree,
				yyA [0].DictionaryKey.Major,
				yyA [0].DictionaryKey.Minor,
				yyA [2].string_listR.Tree); }
	;

# line 1034 "dct_pars.cxx"
} break;
case 20:
case 11: /* DictionaryKey : '[' Integer ',' Integer ']' */
yyDecrement (5) yySetNT (yyNTDictionaryKey) {
# line 107 "dct_pars.lalr"
 yyS.DictionaryKey.Major	 =  yyA [1].Integer.Value;

	  yyS.DictionaryKey.Minor	 =  yyA [3].Integer.Value;

	;

# line 1046 "dct_pars.cxx"
} break;
case 21: /* string_listR : String */
yyDecrement (1) yySetNT (yyNTstring_listR) {
# line 113 "dct_pars.lalr"
 yyS.string_listR.Tree	= mCons_string_list(
			yyA [0].String.Tree,
			mNil_string_list());

	;

# line 1057 "dct_pars.cxx"
} break;
case 22: /* string_listR : string_listR String */
yyDecrement (2) yySetNT (yyNTstring_listR) {
# line 119 "dct_pars.lalr"
 yyS.string_listR.Tree	= mCons_string_list(
			yyA [1].String.Tree,
			yyA [0].string_listR.Tree);

	;

# line 1068 "dct_pars.cxx"
} break;
case 23:
case 12: /* Identifier : ident */
yyDecrement (1) yySetNT (yyNTIdentifier) {
# line 125 "dct_pars.lalr"
 yyS.Identifier.Tree	= mIdentifier(yyA [0].Scan.ident.ident, MyPos(yyA [0].Scan.Position));

	  yyS.Identifier.Pos	 =  yyA [0].Scan.Position;

	;

# line 1080 "dct_pars.cxx"
} break;
case 24:
case 13: /* Integer : IntConst */
yyDecrement (1) yySetNT (yyNTInteger) {
# line 131 "dct_pars.lalr"
 yyS.Integer.Value	 =  yyA [0].Scan.IntConst.value;

	;

# line 1090 "dct_pars.cxx"
} break;
case 25:
case 14: /* String : StringConst */
yyDecrement (1) yySetNT (yyNTString) {
# line 135 "dct_pars.lalr"
 yyS.String.Tree	= mString(MyPos(yyA [0].Scan.Position), yyA [0].Scan.StringConst.string);

	;

# line 1100 "dct_pars.cxx"
} break;
}
				
#endif /* DOXGEN_IGNORE */
				
/* SPEC State = Next(Top(), Nonterminal); nonterminal transition */
#ifdef YYNDefault
				yyState = *yyStateStackPtr++;
				for (;;)
				{
					const yytComb *yyNCombPtr;

					yyNCombPtr = yyNBasePtr[yyState] + (int) yyNonterminal;
					if (yyNCombPtr->Check == yyState)
					{
						yyState = yyNCombPtr->Next;
						break;
					}
					yyState = yyNDefault[yyState];
				}
#else
				yyState = yyNBasePtr[*yyStateStackPtr++][yyNonterminal];
#endif
				*yyAttrStackPtr++ = yySynAttribute;
				if (yyState < yyFirstFinalState)
					goto yyParseLoop;
			
				/* read reduce? */
#ifdef YYDEBUG
				yyStateStackPtr[0] = yyStateStackPtr[-1];
#endif
			}

		}
		else
		{
			/* read */
	yyRead:
			yyStateStackPtr++;
			yyGetAttribute(yyAttrStackPtr++, scanner->Attribute);
			yyTerminal = yyGetToken();
#ifdef YYDEBUG
			if (Debug)
			{
				yyPrintState(yyStateStackPtr[-1]);
				fprintf(yyTrace, "shift   %s, lookahead: %s",
					yyTokenName[yyPrevTerminal], yyTokenName[yyTerminal]);
				yyNl();
				yyPrevTerminal = yyTerminal;
			}
#endif
			yyIsRepairing = false;
		}
	}

yyAbort:
#ifdef YYDEBUG
	if (Debug)
	{
		yyPrintState(*yyStateStackPtr);
		fprintf(yyTrace, "fail    parse started at %ld", yyStartCount);
		yyNl();
	}
#endif
	if (root) delete root;
	return ++yyErrorCount;

yyAccept:
#ifdef YYDEBUG
	if (Debug)
	{
		yyPrintState(*yyStateStackPtr);
		fprintf(yyTrace, "accept  parse started at %ld", yyStartCount);
		yyNl();
	}
#endif
	if (t) *t = root;
	return yyErrorCount;

	/* never reached
	 * avoid compiler warnings about unreferenced labels
	 */
	goto yyReduce;
	goto yyRead;
	goto yyAbort;
}

void
dct_pars::yyParse2 (yyStateRange &yyState, yyStateRange *&yyStateStackPtr,
		tParsAttribute *&yyAttrStackPtr, int &yyNonterminal,
		tParsAttribute &yySynAttribute)
{
#ifndef DOXYGEN_IGNORE

/* YYPARSE2 */

#endif /* DOXGEN_IGNORE */
}

#ifndef NO_RECOVER
void
dct_pars::yyErrorRecovery (yySymbolRange *yyTerminal, yyStateRange *yyStack, long yyStackPtr)
{
#define yyContinueSize 5000
	char yyContinueString[yyContinueSize + 2];
	reuse::Set yyContinueSet(yyLastTerminal);
	reuse::Set yyRestartSet(yyLastTerminal);
	bool yyTokensSkipped;
	int yyLength;
	
	if (yyControl.yyMessages)
	{
		/* 1. report syntax error */
		yySyntaxError();
		
		/* 2. report the offending token */
		strcpy_s(yyContinueString, sizeof(yyContinueString), yyTokenName[*yyTerminal]);
#ifdef SPELLING
		if (strncmp(yyContinueString, scanner->TokenPtr, scanner->TokenLength))
		{
			yyContinueString[yyLength = strlen(yyContinueString)] = ' ';
			scanner->GetWord(&yyContinueString[++yyLength]);
		}
#endif
		PARENT_INFORMATION(xxTokenFound,
				   mypos(scanner->Attribute.Position),
				   PARENT_IDENT_CREATE(yyContinueString),
				   true);
		
		/* 3. report the set of expected terminal symbols */
		
		yyComputeContinuation(yyStack, yyStackPtr, yyContinueSet);
		yyLength = 0;
		yyContinueString[0] = '\0';
		
		while (!yyContinueSet.IsEmpty())
		{
			const char *yyTokenString = yyTokenName[yyContinueSet.Extract()];
			const int yyl = strlen(yyTokenString);
			
			if (yyLength + yyl >= yyContinueSize)
				break;
			
			strcpy_s(&yyContinueString[yyLength],
				 sizeof(yyContinueString) - yyLength,
				 yyTokenString);
			
			yyLength += yyl;
			yyContinueString[yyLength++] = ' ';
		}
		yyContinueString[--yyLength] = '\0';
		
		PARENT_INFORMATION(xxExpectedTokens,
				   mypos(scanner->Attribute.Position),
				   PARENT_IDENT_CREATE(yyContinueString),
				   true);
	}
	
	/* 4. compute the set of terminal symbols for restart of the parse */
	
	yyComputeRestartPoints(yyStack, yyStackPtr, yyRestartSet);
	
	/* 5. skip terminal symbols until a restart point is reached */
	
	yyTokensSkipped = false;
	while (!yyRestartSet.IsElement(*yyTerminal))
	{
#ifdef YYDEBUG
		yySymbolRange yyPrevTerminal = *yyTerminal;
#endif
		*yyTerminal = yyGetToken();
		yyTokensSkipped = true;
#ifdef YYDEBUG
		if (Debug)
		{
			yyPrintState(yyStack[yyStackPtr]);
			fprintf(yyTrace, "skip    %s, lookahead: %s",
				yyTokenName[yyPrevTerminal], yyTokenName[* yyTerminal]);
			yyNl();
		}
#endif
	}
	
	/* 6. report the restart point */
	if (yyTokensSkipped & yyControl.yyMessages)
	{
		PARENT_INFORMATION(xxRestartPoint,
				   mypos(scanner->Attribute.Position),
				   PARENT_IDENT_NoIdent,
				   true);
	}
}

/**
 * Check all terminals for read condition.
 * Compute the set of terminal symbols that can be accepted (read)
 * in a given stack configuration (eventually after reduce actions).
 */
void
dct_pars::yyComputeContinuation (yyStateRange *yyStack,
			     long yyStackPtr,
			     reuse::Set &yyContinueSet)
{
	yySymbolRange yyTerminal;
	yyStateRange yyState = yyStack[yyStackPtr];
	
	yyContinueSet.AssignEmpty();
	for (yyTerminal = yyFirstTerminal; yyTerminal <= yyLastTerminal; yyTerminal++)
	{
		if (yyNext(yyState, yyTerminal) != yyNoState
		    && yyIsContinuation(yyTerminal, yyStack, yyStackPtr))
		{
			yyContinueSet.Include(yyTerminal);
		}
	}
}

/**
 * Terminal read check.
 * Check whether a given terminal symbol can be accepted (read)
 * in a certain stack configuration (eventually after reduce actions).
 */
bool
dct_pars::yyIsContinuation (yySymbolRange yyTerminal, yyStateRange *yyStack, long yyStackPtr)
{
	yyStateRange yState;
	yytNonterminal yyNonterminal;
	
	while (yyStackPtr >= yyIsContStackSize)  /* pass Stack by value */
		yyStateRangeArrayExtend(yyIsContStackPtr, yyIsContStackSize);
	
	memcpy((char *) yyIsContStackPtr, (char *) yyStack,
	       sizeof(yyStateRange) * (yyStackPtr + 1));
	
	yState = yyIsContStackPtr[yyStackPtr];
	for (;;)
	{
		yyIsContStackPtr[yyStackPtr] = yState;
		yState = yyNext(yState, yyTerminal);
		if (yState == yyNoState)
			return false;
		
		do {
			/* reduce */
			if (yState > yyLastReduceState)
			{
				/* dynamic ? */
				yState = yyCondition[yState - yyLastReduceState];
			}
			
			if (yState <= yyLastStopState)
			{
				/* read, read reduce, or accept? */
				return true;
			}
			else
			{
				/* reduce */
				yyStackPtr -= yyLength[yState - yyFirstReduceState];
				yyNonterminal = yyLeftHandSide[yState - yyFirstReduceState];
			}
			
			yState = yyNext(yyIsContStackPtr[yyStackPtr], (yySymbolRange) yyNonterminal);
			
			if (yyStackPtr >= yyIsContStackSize)
				yyStateRangeArrayExtend(yyIsContStackPtr, yyIsContStackSize);
			
			yyStackPtr++;
		}
		while (yState >= yyFirstFinalState);
	}
}

/**
 * Compute restart points.
 * Compute a set of terminal symbols that can be used to restart
 * parsing in a given stack configuration. We simulate parsing until
 * end of file using a suffix program synthesized by the function (array)
 * yyContinuation. All symbols acceptable in the states reached during
 * the simulation can be used to restart parsing.
 */
void
dct_pars::yyComputeRestartPoints (yyStateRange *yyStack,
			      long yyStackPtr,
			      reuse::Set &yyRestartSet)
{
	yyStateRange yState;
	yytNonterminal yyNonterminal;
	reuse::Set yyContinueSet(yyLastTerminal);
	
	while (yyStackPtr >= yyCompResStackSize) /* pass Stack by value */
		yyStateRangeArrayExtend(yyCompResStackPtr, yyCompResStackSize);
	
	memcpy((char *) yyCompResStackPtr, (char *) yyStack,
	       sizeof(yyStateRange) * (yyStackPtr + 1));
	
	yyRestartSet.AssignEmpty();
	yState = yyCompResStackPtr[yyStackPtr];
	
	for (;;)
	{
		if (yyStackPtr >= yyCompResStackSize)
			yyStateRangeArrayExtend(yyCompResStackPtr, yyCompResStackSize);
		
		yyCompResStackPtr[yyStackPtr] = yState;
		yyComputeContinuation(yyCompResStackPtr, yyStackPtr, yyContinueSet);
		yyRestartSet.Union(yyContinueSet);
		yState = yyNext(yState, yyContinuation[yState]);
		
		if (yState >= yyFirstFinalState) /* final state ? */
		{
			if (yState <= yyLastReadReduceState) /* read reduce ? */
			{
				yyStackPtr++;
				yState = yyFinalToProd[yState - yyFirstReadReduceState];
			}
			
			do {
				/* reduce */
				
				if (yState > yyLastReduceState)
				{
					/* dynamic */
					yState = yyCondition[yState - yyLastReduceState];
				}
				
				if (yyFirstReduceState <= yState && yState <= yyLastStopState)
				{
					/* accept */
					return;
				}
				else if (yState < yyFirstFinalState)
				{
					/* read */
					yyStackPtr++;
					break;
				}
				else
				{
					/* reduce */
					yyStackPtr -= yyLength[yState - yyFirstReduceState];
					yyNonterminal = yyLeftHandSide[yState - yyFirstReduceState];
				}
				
				yState = yyNext(yyCompResStackPtr[yyStackPtr],
							(yySymbolRange) yyNonterminal);
				yyStackPtr++;
			}
			while (yState >= yyFirstFinalState);
		}
		else
		{
			/* read */
			yyStackPtr++;
		}
	}
}

/**
 * Access the parse table.
 * Next : State x Symbol -> Action
 */
yyStateRange dct_pars::yyNext (yyStateRange yyState, yySymbolRange yySymbol) const
{
	if (yySymbol <= yyLastTerminal)
	{
		for (;;)
		{
			const yytComb *yyTCombPtr;
			
			yyTCombPtr = yyTBasePtr[yyState] + yySymbol;
			if (yyTCombPtr->Check == yyState)
				return yyTCombPtr->Next;
#ifdef YYTDefault
# ifdef YYaccDefault
			{
				unsigned long *yylp;
				return (yylp = yyDefaultLook[yyState]) &&
					(yylp[yySymbol >> 5] >> (yySymbol & 0x1f)) & 1 ?
					yyTDefault[yyState] : yyNoState;
			}
# else
			if ((yyState = yyTDefault[yyState]) == yyNoState)
				return yyNoState;
# endif
#else
			return yyNoState;
#endif
		}
	}
#ifdef YYNDefault
	for (;;)
	{
		const yytComb *yyNCombPtr;
		
		yyNCombPtr = yyNBasePtr[yyState] + yySymbol;
		if (yyNCombPtr->Check == yyState)
			return yyNCombPtr->Next;
		
		yyState = yyNDefault[yyState];
	}
#else
	return yyNBasePtr[yyState][yySymbol];
#endif
}
#endif /* NO_RECOVER */

} /* namespace CT_NAMESPACE */
