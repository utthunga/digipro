
/* 
 * DO NOT EDIT!
 * 
 * This file is automatically created during the build process.
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 *               2009-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _version_h
#define _version_h

/*
 * version number of the eddlib & applications
 */
#define VERSION	"2.0.2"

#endif /* _version_h */
