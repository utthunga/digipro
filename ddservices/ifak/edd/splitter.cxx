
/* 
 * $Id: splitter.cxx,v 1.3 2005/10/03 10:35:32 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#include <string>


int
main(void)
{
	char buf[8192];
	
	FILE *in = stdin;
	FILE *out = stdout;
	
	std::string split;
	size_t level = 0;
	bool done = false;
	
	char *line;
	
	line = fgets(buf, sizeof(buf), in);
	while (line)
	{
		std::string str(line);
		
		if (level)
		{
			const char *_s = str.c_str();
			const char *s = _s;
			
			while (*s)
			{
				int c = *s++;
				
				if (c == '{')
					level++;
				else if (c == '}')
					level--;
				
				if (level == 0)
				{
					done = true;
					
					split += std::string(_s, s - _s);
					fputs("yyParse2(yyState, yyStateStackPtr, yyAttrStackPtr, yyNonterminal, yySynAttribute);", out);
					fputs(s, out);
					break;
				}
			}
			
			if (level)
				split += str;
		}
		else
		{
			if (!done && str.find(std::string("default: switch (yyState) {")) != std::string::npos)
			{
				fputs("default: ", out);
				split = "switch (yyState) {\n";
				level++;
			}
			else if (str.find(std::string("/* YYPARSE2 */")) != std::string::npos)
			{
				fputs(split.c_str(), out);
			}
			else
			{
				fwrite(str.data(), 1, str.length(), out);
			}
		}
		
		line = fgets(buf, sizeof(buf), in);
	}
	
	return 0;
}
