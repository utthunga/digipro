
/* 
 * $Id: device_io_hart.cxx,v 1.54 2010/01/26 11:29:49 mmeier Exp $
 * 
 * Copyright (C) 2006-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io_hart.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// other libs

// my lib
#include "port.h"

// own header
#include "edd_ast.h"
#include "edd_builtins_impl.h"
#include "pbInterpreter.h"

#ifndef _
# define _(x) x
#endif


namespace edd
{

device_io_hart::device_io_hart(class Interpreter &parent)
: device_io(parent)
{
}

int
device_io_hart::do_command(class method_env *env,
			   const class COMMAND_tree *cmd, int trans_nr, enum action action,
			   int &warnings)
{
	assert(cmd);

	bool abort;
	int ret;

	if (env)
	{
		ret = do_command1(env,
				  cmd, trans_nr, action,
				  NULL,
				  false,
				  NULL, NULL, NULL,
				  abort, warnings);
	}
	else
	{
		ret = do_command0(env,
				  cmd, trans_nr, action, warnings);
	}

	return ret;
}

int
device_io_hart::send(class method_env *env, const lib::pos &caller_pos,
		     int cmd_nr, int trans_nr, enum action action, bool do_cmd48,
		     std::string *cmd_status_str,
		     std::string *more_data_status_str,
		     std::string *more_data_info_str,
		     int &warnings)
{
	assert(env);

	const class COMMAND_tree *cmd = cmd_by_number(cmd_nr, trans_nr, caller_pos);
	const class COMMAND_tree *cmd48 = do_cmd48 ? cmd_by_number(48, 0, caller_pos) : NULL;
	bool abort;
	int ret;

	ret = do_command1(env,
			  cmd, trans_nr, action,
			  cmd48,
			  true,
			  cmd_status_str, more_data_status_str, more_data_info_str,
			  abort, warnings);

	if (abort)
		env->abort();

	return ret;
}

const class COMMAND_tree *
device_io_hart::cmd_by_number(int cmd_nr, int trans_nr, const lib::pos &caller_pos)
{
	const class COMMAND_tree *cmd;

	cmd = parent.lookup_cmd_by_number(cmd_nr);
	if (!cmd || !cmd->check_transaction_nr(trans_nr))
	{
		char buf[60];

		if (cmd)
			snprintf(buf, sizeof(buf), "TRANSACTION %i", trans_nr);
		else
			snprintf(buf, sizeof(buf), "COMMAND %i", cmd_nr);

		RuntimeError xx(xxNoSuch, caller_pos, parent.ident().create(buf));

		error(parent.errmsg(xx).c_str());

		throw xx;
	}

	return cmd;
}

static void
str_assign(std::string &str, unsigned char *buf, size_t buflen)
{
	str.resize(buflen);

	for (size_t i = 0; i < buflen; i++)
		str[i] = buf[i];
}

int
device_io_hart::do_command1(class method_env *env,
			    const class COMMAND_tree *cmd, int trans_nr, enum action action,
			    const class COMMAND_tree *cmd48,
				const bool check_device_status,
			    std::string *cmd_status_str,
			    std::string *more_data_status_str,
			    std::string *more_data_info_str,
			    bool &abort, int &warnings)
{
	assert(env && cmd);

	unsigned char cmd_status[3] = { 0, 0, 0 }, more_data_status[3] = { 0, 0, 0 };
	class RESPONSE_CODE_tree *edd_response = NULL, *edd_response48 = NULL;
	const char *reason_str = NULL;
	int retries = env->retries();
	int ret, ret48 = 0;

	abort = false;

retry:
	ret = do_command0(env, cmd, trans_nr, action, warnings);
	switch (ret)
	{
		default:
			/* not possible */ assert(0);
			break;

		case BI_SUCCESS:
			/* anything is ok */
			break;

		case BI_ERROR:
			/* nothing is ok */
			goto abort_error;

		case BI_COMM_ERROR:
			/* communication is not ok */
			if (env->hart_masks.comm_error_abort)
			{
				trace(_("communication error abort condition, aborting command '%s'"),
					cmd->name());

				goto comm_error;
			}

			if (env->hart_masks.comm_error_retry)
			{
				reason_str = _("communication error");
				goto handle_retry;
			}

			/* ignore */
			break;

		case BI_NO_DEVICE:
			/* no such device */
			if (env->hart_masks.no_device_abort)
			{
				trace(_("no device error abort condition, aborting command '%s'"),
					cmd->name());

				goto no_device_error;
			}

			if (env->hart_masks.no_device_retry)
			{
				reason_str = _("no device error");
				goto handle_retry;
			}

			/* ignore */
			break;
	}

	cmd_status[0] = response_code;
	cmd_status[1] = comm_status;
	cmd_status[2] = device_status;
	if (cmd_status_str)
		str_assign(*cmd_status_str, cmd_status, 3);

	if (!cmd48 && more_data_info_str)
	{
		/* get_more_status */

		if (ret == 0 && datalen > 2)
			str_assign(*more_data_info_str, data+2, datalen-2);
	}

	edd_response = edd_response_code;

	if (cmd48)
	{
		/* check if COMMAND 48 must be dispatched */

		if (cmd_status[2] & env->hart_masks.MORE_STATUS_AVAILABLE)
			trace(_("more status available, issue COMMAND 48"));
		else
			cmd48 = NULL;
	}

	if (cmd48)
	{
retry48:
		ret48 = do_command0(env, cmd48, 0, action, warnings);
		switch (ret48)
		{
			default:
				/* not possible */ assert(0);
				break;

			case BI_SUCCESS:
				/* anything is ok */
				break;

			case BI_ERROR:
				/* nothing is ok */
				goto abort_error;

			case BI_COMM_ERROR:
				/* communication is not ok */
				if (env->hart_masks.cmd48_comm_error_abort)
				{
					trace(_("communication error abort condition, aborting command 48 '%s'"),
						cmd->name());

					goto comm_error;
				}

				if (env->hart_masks.cmd48_comm_error_retry)
				{
					reason_str = _("communication error");
					goto handle_retry48;
				}

				/* ignore */
				break;

			case BI_NO_DEVICE:
				/* no such device */
				if (env->hart_masks.cmd48_no_device_abort)
				{
					trace(_("no device error abort condition, aborting command 48 '%s'"),
						cmd->name());

					goto no_device_error;
				}

				if (env->hart_masks.cmd48_no_device_retry)
				{
					reason_str = _("no device error");
					goto handle_retry48;
				}

				/* ignore */
				break;
		}

		more_data_status[0] = response_code;
		more_data_status[1] = comm_status;
		more_data_status[2] = device_status;
		if (more_data_status_str)
			str_assign(*more_data_status_str, more_data_status, 3);

		if (datalen > 2 && more_data_info_str)
			str_assign(*more_data_info_str, data+2, datalen-2);

		edd_response48 = edd_response_code;
	}

	/* unexpected response code result in unexpected response code abort
	 * except one of the *_ALL_RESPONSE_CODES builtins was called
	 */
	if (!edd_response && env->hart_masks.response_all == 0)
	{
		reason_str = _("unexpected response code");
		goto abort_error_msg;
	}

	/* check for abort condition */

	if (cmd_status[0] & env->hart_masks.response_code_abort)
	{
		reason_str = _("response code");
		goto abort_error_msg;
	}
	if (cmd_status[1] & env->hart_masks.comm_status_abort)
	{
		reason_str = _("communication status");
		goto abort_error_msg;
	}
	if ((cmd_status[2] & env->hart_masks.device_status_abort) && check_device_status)
	{
		reason_str = _("device status");
		goto abort_error_msg;
	}

	if (cmd48)
	{
		/* unexpected response code result in unexpected response code abort
		 * except one of the XMTR_*_ALL_RESPONSE_CODES builtins was called
		 */
		if (!edd_response48 && env->hart_masks.cmd48_response_all == 0)
		{
			reason_str = _("unexpected response code");
			goto abort_error_msg48;
		}

		if (more_data_status[0] & env->hart_masks.cmd48_response_code_abort)
		{
			reason_str = _("response code");
			goto abort_error_msg48;
		}
		if (more_data_status[1] & env->hart_masks.cmd48_comm_status_abort)
		{
			reason_str = _("communication status");
			goto abort_error_msg48;
		}
		if (more_data_status[2] & env->hart_masks.cmd48_device_status_abort)
		{
			reason_str = _("device status");
			goto abort_error_msg48;
		}

		if (datalen > 2)
		{
			size_t i_max = datalen - 2;

			if (i_max > env->hart_masks.cmd48_data_maxlen)
				i_max = env->hart_masks.cmd48_data_maxlen;

			for (size_t i = 0; i < i_max; ++i)
			{
				if (data[2+i] & env->hart_masks.cmd48_data_abort[i])
				{
					reason_str = _("xmtr data");
					goto abort_error_msg48;
				}
			}
		}
	}

	/* check for retry condition */

	if (cmd_status[0] & env->hart_masks.response_code_retry)
	{
		reason_str = _("response code");
		goto handle_retry;
	}
	if (cmd_status[1] & env->hart_masks.comm_status_retry)
	{
		reason_str = _("communication status");
		goto handle_retry;
	}
	if (cmd_status[2] & env->hart_masks.device_status_retry)
	{
		reason_str = _("device status");
		goto handle_retry;
	}

	if (cmd48)
	{
		if (more_data_status[0] & env->hart_masks.cmd48_response_code_retry)
		{
			reason_str = _("response code");
			goto handle_retry48;
		}
		if (more_data_status[1] & env->hart_masks.cmd48_comm_status_retry)
		{
			reason_str = _("communication status");
			goto handle_retry48;
		}
		if (more_data_status[2] & env->hart_masks.cmd48_device_status_retry)
		{
			reason_str = _("device status");
			goto handle_retry48;
		}

		if (datalen > 2)
		{
			size_t i_max = datalen - 2;

			if (i_max > env->hart_masks.cmd48_data_maxlen)
				i_max = env->hart_masks.cmd48_data_maxlen;

			for (size_t i = 0; i < i_max; ++i)
			{
				if (data[2+i] & env->hart_masks.cmd48_data_retry[i])
				{
					reason_str = _("xmtr data");
					goto handle_retry48;
				}
			}
		}
	}

	if (ret) return ret;
	if (ret48) return ret48;

	return BI_SUCCESS;

abort_error_msg:
	assert(reason_str);

	error(_("%s abort condition reached, abort command '%s'"),
		reason_str, cmd->name());

	goto abort_error;

abort_error_msg48:
	assert(reason_str);

	error(_("%s abort condition reached, abort command 48"),
		reason_str);

abort_error:
	abort = true;
	return BI_ERROR;

comm_error:
	abort = true;
	return BI_COMM_ERROR;

no_device_error:
	abort = true;
	return BI_NO_DEVICE;

handle_retry:
	assert(reason_str);

	if (retries)
	{
		trace(_("%s retry condition, retry command '%s'"),
			reason_str, cmd->name());

		--retries;
		goto retry;
	}

	/* retry count exceeded, abort with BI_RETRY */

	error(_("%s retry condition, retries exceeded, aborting command '%s'"),
		reason_str, cmd->name());

	goto retry_error;

handle_retry48:
	assert(reason_str);

	if (retries)
	{
		trace(_("%s retry condition, retry command 48"),
			reason_str);

		--retries;
		goto retry48;
	}

	/* retry count exceeded, abort with BI_RETRY */

	error(_("%s retry condition, retries exceeded, aborting command 48"),
		reason_str);

retry_error:
	abort = true;
	return BI_RETRY;
}

int
device_io_hart::do_command0(class method_env *env,
			    const class COMMAND_tree *cmd, int trans_nr, enum action action,
			    int &warnings)
{
	assert(cmd);

	datalen = 0;
	response_code = 0;
	comm_status = 0;
	device_status = 0;
	edd_response_code = NULL;

	int number;

	try
	{
		number = cmd->number(env);
	}
	catch (const RuntimeError &)
	{
		/* ignore */
		number = -1;
	}

	if (number == -1)
	{
		error(_("COMMAND %s: invalid command number, aborting"), cmd->name());
		return BI_ERROR;
	}

	if (!is_online())
	{
		error(_("COMMAND %i [%i] (%s): not online!"), number, trans_nr, cmd->name());
		return BI_COMM_ERROR;
	}

	std::vector<class DATA_ITEM_tree *> read_vars, write_vars;

	try
	{
		write_vars = cmd->request_items(trans_nr, env);
		read_vars = cmd->reply_items_safe(trans_nr, env);

		if (read_vars.size() < 2)
		{
			error(_("COMMAND %i [%i] (%s): at least 2 bytes in REPLY list are required, aborting"),
				number, trans_nr, cmd->name());

			return BI_ERROR;
		}
	}
	catch (const RuntimeError &xx)
	{
		error(_("COMMAND %i [%i] (%s): RuntimeError while obtaining request/reply items, aborting"),
			number, trans_nr, cmd->name());

		std::string msg(cmd->__parent->errmsg(xx));

		error(_("COMMAND %i [%i] (%s): RuntimeError is: %s"),
			number, trans_nr, cmd->name(), msg.c_str());

		return BI_ERROR;
	}

	/* check if we can optimize the command dispatch away:
	 * 1) for READ: checking the read_vars is enough
	 * 2) for WRITE: checking the read_vars and write_vars
	 * 3) for COMMAND: no optimization at all
	 */
	{
		bool optimize_flag = false;

		if (action == action_read)
			optimize_flag = can_optimize(env, read_vars);
		else if (action == action_write)
			optimize_flag = can_optimize(env, read_vars) && can_optimize(env, write_vars);

		if (optimize_flag)
		{
			if (can_optimize(env, read_vars))
			{
				/* all variables are newer than the last age stamp */

				info(_("COMMAND %i [%i] (%s): not executed, all items are up-to-date"),
					number, trans_nr, cmd->name());

				/* the SUCCESS response code must be defined too */
				edd_response_code = cmd->lookup_response_code(0, env, trans_nr);

				return BI_SUCCESS;
			}
		}
	}

	/* for anything other than READ increase the age stamp;
	 * a WRITE or COMMAND may modify device variables
	 */
	if (action != action_read)
		optimize_age_stamp_tick(env);

	size_t expect, dynamic_expect;
	int32 r_timeout, w_timeout;

	if (action == action_write)
	{
		/* don't execute PRE_READ_ACTIONS */

		if (read_encode0(cmd, trans_nr, env, parent.logger, read_vars,
				 expect, dynamic_expect, r_timeout, warnings))
			return BI_ERROR;
	}
	else
	{
		/* execute PRE_READ_ACTIONS */

		if (read_encode(cmd, trans_nr, env, parent.logger, read_vars,
				expect, dynamic_expect, r_timeout, warnings))
			return BI_ERROR;
	}

	if (expect+dynamic_expect > 240)
	{
		error(_("COMMAND %i [%i] (%s): size of variables to large (%lu > 240)!"),
			number, trans_nr, cmd->name(), (unsigned long)expect);

		return BI_ERROR;
	}

	if (dynamic_expect > 0)
	{
		error(_("COMMAND %i [%i] (%s): variable-size elements are not allowed!"),
			number, trans_nr, cmd->name());

		return BI_ERROR;
	}

	if (action == action_read)
	{
		/* don't execute PRE_WRITE_ACTIONS */

		if (write_encode0(cmd, trans_nr, env, parent.logger, write_vars, data, sizeof(data), datalen, w_timeout))
			return BI_ERROR;
	}
	else
	{
		/* execute PRE_WRITE_ACTIONS */

		if (write_encode(cmd, trans_nr, env, parent.logger, write_vars, data, sizeof(data), datalen, w_timeout))
			return BI_ERROR;
	}

	struct request request;

	request.number = number;
	request.datalen = datalen;
	request.data = data;

	/* trace messages */
	{
		lib::dyn_buf msg(100);

		msg.printf(_("COMMAND %i [%i] (%s): sent %lu bytes"),
			     number, trans_nr, cmd->name(), (unsigned long)datalen);

		if (datalen > 0)
		{
			msg.put(" (hex): ");
			for (size_t i = 0; i < datalen; i++)
				msg.printf("%02x ", request.data[i]);
		}

		trace(msg.c_str());
	}

	long rc = hart_send_cmd(request);

	if (rc == 0)
	{
		datalen = request.datalen;

		/* trace messages */
		{
			lib::dyn_buf msg(100);

			msg.printf(_("COMMAND %i [%i] (%s): received %lu bytes"),
				     number, trans_nr, cmd->name(), (unsigned long)datalen);

			if (datalen > 0)
			{
				msg.put(" (hex): ");
				for (size_t i = 0; i < datalen; i++)
					msg.printf("%02x ", request.data[i]);
			}

			trace(msg.c_str());
		}

		if (datalen < 2)
		{
			error(_("COMMAND %i [%i] (%s): no response code and device status, aborting"),
				number, trans_nr, cmd->name());

			return BI_ERROR;
		}

		response_code = request.data[0];
		device_status = request.data[1];

		if (response_code)
		{
			if (response_code & 0x80)
			{
				comm_status = response_code;
				response_code = 0;
				device_status = 0;

				error(_("COMMAND %i [%i] (%s): comm status: %s"),
					number, trans_nr, cmd->name(), commstatus2str0(comm_status).c_str());
			}
			else
			{
				edd_response_code = cmd->lookup_response_code(response_code, env, trans_nr);
				if (edd_response_code && edd_response_code->have_description())
				{
					info(_("COMMAND %i [%i] (%s): response: %s"),
					       number, trans_nr, cmd->name(), edd_response_code->description(env).c_str());

					if (edd_response_code->have_help())
					{
						info(_("COMMAND %i [%i] (%s): response help: %s"),
						       number, trans_nr, cmd->name(), edd_response_code->help(env).c_str());
					}
				}
				else
				{
					error(_("COMMAND %i [%i] (%s): response: %s"),
						number, trans_nr, cmd->name(), responsecode2str0(response_code));
				}
			}
		}
		else
		{
			/* the SUCCESS response code must be defined too */
			edd_response_code = cmd->lookup_response_code(response_code, env, trans_nr);
		}

		if (device_status)
		{
			trace(_("COMMAND %i [%i] (%s): device status: %s"),
				number, trans_nr, cmd->name(), devicestatus2str0(device_status).c_str());
		}

		if (expect > datalen)
		{
			assert(read_vars.size() > 1);

			/* assign at least response_code and device_status */

			read_vars.resize(2);

			read_decode(cmd, trans_nr, env, parent.logger, read_vars,
				    request.data, 2);

			if (comm_status)
				return BI_COMM_ERROR;

			if (response_code)
			{
				if (edd_response_code)
				{
					info(_("COMMAND %i [%i] (%s) finished with expected response"),
					       number, trans_nr, cmd->name());
				}
				else
				{
					warning(_("COMMAND %i [%i] (%s) finished with unexpected response"),
						  number, trans_nr, cmd->name());

					++warnings;
				}

				return BI_SUCCESS;
			}

			error(_("COMMAND %i [%i] (%s): failed, response ok,  "
				"expected %lu bytes, got %lu, aborting"),
				number, trans_nr, cmd->name(),
				(unsigned long)expect, (unsigned long)datalen);

			return BI_ERROR;
		}

		if (expect < datalen)
		{
			warning(_("COMMAND %i [%i] (%s) returned %lu bytes "
				  "but only %lu bytes are expected!"),
				  number, trans_nr, cmd->name(),
				(unsigned long)datalen, (unsigned long)expect);

			++warnings;
		}

		if (action == action_write)
		{
			/* don't execute POST_READ_ACTIONS */

			if (read_decode0(cmd, trans_nr, env, parent.logger, read_vars,
					 request.data, datalen))
			{
				return BI_ERROR;
			}
		}
		else
		{
			/* execute POST_READ_ACTIONS */

			if (read_decode(cmd, trans_nr, env, parent.logger, read_vars,
					request.data, datalen))
			{
				return BI_ERROR;
			}
		}

		if (action != action_read)
		{
			/* execute POST_WRITE_ACTIONS */

			write_decode(cmd, trans_nr, env, parent.logger, write_vars, 0);
		}

		info(_("COMMAND %i [%i] (%s) finished ok"),
		       number, trans_nr, cmd->name());

		return BI_SUCCESS;
	}

	datalen = 0;

	trace(_("COMMAND %i [%i] (%s) failed"),
		number, trans_nr, cmd->name());

	return BI_COMM_ERROR;
}

const char *
device_io_hart::responsecode2str0(int response_code)
{
	const char *str = NULL;

	switch (response_code & 0x7f)
	{
		case 0:
			str = _("no command-specific error");
			break;
		case 1:
			str = _("(undefined)");
			break;
		case 2:
			str = _("invalid selection");
			break;
		case 3:
			str = _("passed parameter too large");
			break;
		case 4:
			str = _("passed parameter too small");
			break;
		case 5:
			str = _("too few data bytes received");
			break;
		case 6:
			str = _("device-specific command error");
			break;
		case 7:
			str = _("in write-protect mode");
			break;
		case 16:
			str = _("access restricted");
			break;
		case 32:
			str = _("device is busy");
			break;
		case 64:
			str = _("command not implemented");
			break;
		default:
			/* 8-15 multiple meanings */
			/* 28 multiple meanings */
			str = _("command specific meaning");
			break;
	}

	assert(str);
	return str;
}

std::string
device_io_hart::responsecode2str(int response_code)
{
	std::string str(_("Response status:\n\n"));

	str += responsecode2str0(response_code);
	str += '\n';

	return str;
}

std::string
device_io_hart::devicestatus2str0(int device_status)
{
	std::string str;

	if (device_status & 0x80) { str += _("field device malfunction");           str += '|'; }
	if (device_status & 0x40) { str += _("configuration changed");              str += '|'; }
	if (device_status & 0x20) { str += _("cold start");                         str += '|'; }
	if (device_status & 0x10) { str += _("more status available");              str += '|'; }
	if (device_status & 0x08) { str += _("analoge output current fixed");       str += '|'; }
	if (device_status & 0x04) { str += _("analoge output saturated");           str += '|'; }
	if (device_status & 0x02) { str += _("non-primary variable out of limits"); str += '|'; }
	if (device_status & 0x01) { str += _("primary variable out of limits");     str += '|'; }

	if (str.size())
		str.erase(str.size() - 1);

	return str;
}

std::string
device_io_hart::devicestatus2str(int device_status)
{
	std::string str(_("Device status:\n\n"));

	if (device_status & 0x80) { str += "- "; str += _("field device malfunction");           str += '\n'; }
	if (device_status & 0x40) { str += "- "; str += _("configuration changed");              str += '\n'; }
	if (device_status & 0x20) { str += "- "; str += _("cold start");                         str += '\n'; }
	if (device_status & 0x10) { str += "- "; str += _("more status available");              str += '\n'; }
	if (device_status & 0x08) { str += "- "; str += _("analoge output current fixed");       str += '\n'; }
	if (device_status & 0x04) { str += "- "; str += _("analoge output saturated");           str += '\n'; }
	if (device_status & 0x02) { str += "- "; str += _("non-primary variable out of limits"); str += '\n'; }
	if (device_status & 0x01) { str += "- "; str += _("primary variable out of limits");     str += '\n'; }

	return str;
}

std::string
device_io_hart::commstatus2str0(int comm_status)
{
	std::string str;

	comm_status &= 0x7f;

	if (comm_status & 0x40) { str += _("parity error");        str += '|'; }
	if (comm_status & 0x20) { str += _("overrun error");       str += '|'; }
	if (comm_status & 0x10) { str += _("framing error");       str += '|'; }
	if (comm_status & 0x08) { str += _("checksum error");      str += '|'; }
	if (comm_status & 0x04) { str += _("(reserved)");          str += '|'; }
	if (comm_status & 0x02) { str += _("rx buffer overflow");  str += '|'; }
	if (comm_status & 0x01) { str += _("(undefined)");         str += '|'; }

	if (str.size())
		str.erase(str.size() - 1);

	return str;
}

std::string
device_io_hart::commstatus2str(int comm_status)
{
	std::string str(_("Communication status:\n\n"));

	comm_status &= 0x7f;

	if (comm_status & 0x40) { str += "- "; str += _("parity error");       str += '\n'; }
	if (comm_status & 0x20) { str += "- "; str += _("overrun error");      str += '\n'; }
	if (comm_status & 0x10) { str += "- "; str += _("framing error");      str += '\n'; }
	if (comm_status & 0x08) { str += "- "; str += _("checksum error");     str += '\n'; }
	if (comm_status & 0x04) { str += "- "; str += _("(reserved)");         str += '\n'; }
	if (comm_status & 0x02) { str += "- "; str += _("rx buffer overflow"); str += '\n'; }
	if (comm_status & 0x01) { str += "- "; str += _("(undefined)");        str += '\n'; }

	return str;
}

} /* namespace edd */
