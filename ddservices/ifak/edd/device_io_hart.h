
/* 
 * $Id: device_io_hart.h,v 1.25 2010/01/26 11:29:49 mmeier Exp $
 * 
 * Copyright (C) 2006-2008 Frank Naumann <frank.naumann@ifak-md.de>
 *               2010 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _device_io_hart_h
#define _device_io_hart_h

// C stdlib
#include <stdlib.h>

// C++ stdlib
#include <set>

// other libs

// my lib
#include "position.h"

// own header
#include "device_io.h"


namespace edd
{

/**
 * Profibus communication base.
 * This communication specialization support the profibus protocol.
 * It include all generic things like executing COMMANDs, reading
 * the profibus directory and do the relative to absolute address mapping.
 */
class device_io_hart : public device_io
{
public:
	device_io_hart(class Interpreter &);

	/* implement device_io_interface */
	virtual int do_command(class method_env *,
			       const class COMMAND_tree *, int, enum action, int &warnings);

	/* helper for HART communication builtins */
	int send(class method_env *env, const lib::pos &caller_pos,
		 int cmd_number, int transaction, enum action, bool do_cmd48,
		 std::string *cmd_status,
		 std::string *more_data_status,
		 std::string *more_data_info,
		 int &warnings);

	/* helper */
	static const char *responsecode2str0(int response_code);
	static std::string responsecode2str(int response_code);
	static std::string devicestatus2str0(int device_status);
	static std::string devicestatus2str(int device_status);
	static std::string commstatus2str0(int comm_status);
	static std::string commstatus2str(int comm_status);

protected:
	/* low-level I/O interface */
	struct request
	{
		int number;
		size_t datalen;
		unsigned char *data;
	};
	virtual long hart_send_cmd(struct request &) = 0;

private:
	/* helper */
	const class COMMAND_tree *cmd_by_number(int, int, const lib::pos &);

	int do_command1(class method_env *,
			const class COMMAND_tree *, int, enum action,
			const class COMMAND_tree *cmd48,
			const bool check_device_status,
			std::string *, std::string *, std::string *,
			bool &abort, int &warnings);

	int do_command0(class method_env *,
			const class COMMAND_tree *, int, enum action, int &warnings);

	unsigned char data[256]; size_t datalen;
	unsigned char response_code, device_status, comm_status;
	class RESPONSE_CODE_tree *edd_response_code;
};

} /* namespace edd */

#endif /* _device_io_hart_h */
