
/* 
 * $Id: edd_values_string.cxx,v 1.16 2009/11/19 15:51:23 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>

// C++ stdlib

// my lib
#include "port.h"
#include "strtoxx.h"
#include "BasicType.h"

// own
#include "edd_values_string.h"
#include "edd_ast.h"


namespace edd
{

operand_ptr string_value::clone(void) const { return new string_value(value); }

void
string_value::dump0(lib::stream &f) const
{
	f.printf("$$ string { \"%s\" }", value.c_str());
}

std::string
string_value::print(const char *fmt) const
{
	return value;
}

            string_value::operator stackmachine::Type *() const { return new stackmachine::stringType(value); }

            string_value::operator bool() const { return value.length() ? true : false; }
            string_value::operator std::string() const { return value; }

operand_ptr string_value::operator+   (const operand &op) const { return new string_value(value +  (std::string) op); }

bool        string_value::operator==  (const operand &op) const { return (value == (std::string) op); }
bool        string_value::operator!=  (const operand &op) const { return (value != (std::string) op); }


string_lvalue::string_lvalue(value_status_t status)
: mystatus(status)
{ }

string_lvalue::string_lvalue(const char *v, value_status_t status)
: string_value(v)
, mystatus(status)
{ }

string_lvalue::string_lvalue(const std::string &v, value_status_t status)
: string_value(v)
, mystatus(status)
{ }

void
string_lvalue::assign(const std::string &v, bool strict)
{
	value = v;
	mystatus.changed();
}

operand_ptr string_lvalue::clone(void) const
{ return new string_lvalue(value, mystatus.reference()); }

void
string_lvalue::dump0(lib::stream &f) const
{
	f.printf("$$ string lval");
	mystatus.dump(f);
	f.printf(" { \"%s\" }", value.c_str());
}

void
string_lvalue::scan(const std::string &str, bool strict)
{
	assign(str, strict);
}

void        string_lvalue::operator= (const stackmachine::Type &v) { assign((std::string) v); }

operand::value_status_t& string_lvalue::status(void) { return mystatus.reference(); }
const operand::value_status_t& string_lvalue::status(void) const { return mystatus.reference(); }

void        string_lvalue::operator=   (const operand &op) { assign(        (std::string) op); }
void        string_lvalue::operator+=  (const operand &op) { assign(value + (std::string) op); }


checked_string_lvalue::checked_string_lvalue(const char *v,
					     class VARIABLE_TYPE_STRING_tree *t,
					     value_status_t status)
: string_lvalue(v, status)
, type(t)
{
}

checked_string_lvalue::checked_string_lvalue(const std::string &v,
					     class VARIABLE_TYPE_STRING_tree *t,
					     value_status_t status)
: string_lvalue(v, status)
, type(t)
{
}

void
checked_string_lvalue::assign(const std::string &v, bool strict)
{
	const std::string bak = value;

	value = v;

	if (type->getsubtype() == EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII)
	{
		/* additional PACKED_ASCII handling:
		 * - convert lowercase to uppercase
		 * - pad with spaces
		 */
		std::string::size_type str_len = value.size();
		size_t len = type->length();

		if (str_len <= len)
		{
			for (std::string::size_type i = 0, i_max = str_len; i < i_max; ++i)
			{
				char c = value[i];

				if (islower(c))
					value[i] = toupper(c);
			}

			value.resize(len, ' ');
		}
	}

	if (!type->check_value(*this, env))
	{
		if (strict)
		{
			value = bak;
			throw RuntimeError(xxRange, type->pos);
		}

		mystatus.invalid();

		if (value.size() > type->get_io_size())
			value.resize(type->get_io_size());
	}
	else
		mystatus.changed();
}

operand_ptr checked_string_lvalue::clone(void) const
{ return new checked_string_lvalue(value, type, mystatus.reference()); }

void
checked_string_lvalue::dump0(lib::stream &f) const
{
	f.printf("$$ checked string lval");
	mystatus.dump(f);
	f.printf(" { \"%s\" }", value.c_str());
}

std::string
checked_string_lvalue::pretty_print(void) const
{
	if (type->getsubtype() == EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII)
	{
		/* special PACKED_ASCII pretty printing:
		 * - strip trailing spaces
		 */
		std::string result;
		int space = 0;

		for (std::string::size_type i = 0, i_max = value.size(); i < i_max; ++i)
		{
			char c = value[i];

			if (c != ' ')
			{
				while (space--)
					result += ' ';

				space = 0;
				result += c;
			}
			else
				++space;
		}

		return result;
	}

	return print(NULL);
}


octet_string_lvalue::octet_string_lvalue(const char *v, value_status_t status)
: string_lvalue(v, status)
{ }

octet_string_lvalue::octet_string_lvalue(const std::string &v, value_status_t status)
: string_lvalue(v, status)
{ }

operand_ptr octet_string_lvalue::clone(void) const
{ return new octet_string_lvalue(value, mystatus.reference()); }

void
octet_string_lvalue::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.put("$$ octet string lval");
	mystatus.dump(f);
	f.printf(" { \"%s\" }", visible.c_str());
}

std::string
octet_string_lvalue::print(const char *fmt) const
{
	std::string visible;
	size_t i, max = value.length();

	visible.reserve(max * 5 + 1);

	for (i = 0; i < max; i++)
	{
		if (i != 0)
			visible += ' ';

		unsigned char v = value[i];

		char buf[8];
		snprintf(buf, sizeof(buf), "0x%x", v);

		visible += buf;
	}

	return visible;
}

static bool
is_hexdigit(char c)
{
	if (isdigit(c))
		return true;

	if (c >= 'a' && c <= 'f')
		return true;

	if (c >= 'A' && c <= 'F')
		return true;

	return false;
}

void
octet_string_lvalue::scan(const std::string &str, bool strict)
{
	const char *v = str.c_str();
	std::string octets;

	octets.reserve(str.length() / 5 + 1);

	for (;;)
	{
		/* first skip any leading space */
		while (isspace(*v))
			v++;

		if (!*v)
			break;

		bool error = true;

		if (v[0] == '0' && (v[1] == 'x' || v[1] == 'X'))
		{
			if (is_hexdigit(v[2]))
			{
				size_t len = 3;
				int32 i;

				if (is_hexdigit(v[3]))
					++len;

				lib::hex_ascii_to_int(i, v, len);
				v += len;

				octets += (char) i;
				error = false;
			}
		}

		if (error)
			throw RuntimeError(xxInvalidInput);
	}

	assign(octets, strict);
}


checked_octet_string_lvalue::checked_octet_string_lvalue(const char *v,
							 class VARIABLE_TYPE_STRING_tree *t,
							 value_status_t status)
: octet_string_lvalue(v, status)
, type(t)
{
}

checked_octet_string_lvalue::checked_octet_string_lvalue(const std::string &v,
							 class VARIABLE_TYPE_STRING_tree *t,
							 value_status_t status)
: octet_string_lvalue(v, status)
, type(t)
{
}

void
checked_octet_string_lvalue::assign(const std::string &v, bool strict)
{
	const std::string bak = value;

	value = v;

	if (!type->check_value(*this, env))
	{
		if (strict)
		{
			value = bak;
			throw RuntimeError(xxRange, type->pos);
		}

		mystatus.invalid();

		if (value.size() > type->get_io_size())
			value.resize(type->get_io_size());
	}
	else
		mystatus.changed();
}

operand_ptr checked_octet_string_lvalue::clone(void) const
{ return new checked_octet_string_lvalue(value, type, mystatus.reference()); }

void
checked_octet_string_lvalue::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.put("$$ checked octet string lval");
	mystatus.dump(f);
	f.printf(" { \"%s\" }", visible.c_str());
}

} /* namespace edd */
