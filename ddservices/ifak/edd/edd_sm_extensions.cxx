
/* 
 * $Id: edd_sm_extensions.cxx,v 1.44 2009/07/18 09:19:50 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// my lib
#include "port.h"
#include "BasicType.h"

// own header
#include "edd_sm_extensions.h"

#include "edd_ast.h"
#include "edd_tree.h"
#include "edd_values.h"

using namespace stackmachine;


namespace edd
{

m_stringchar::m_stringchar(stackmachine::Context &c_, unsigned int a, unsigned long o)
: c(c_)
, addr(a)
, off(o)
{
}

stackmachine::Type *
m_stringchar::clone(void) const
{
	return new m_stringchar(c, addr, off);
}

std::string m_stringchar::type_descr(void) const { return "stringchar"; }

void
m_stringchar::dump(lib::stream &f) const
{
	f.printf("stringchar [%u, %li]", addr, off);
}

std::string
m_stringchar::print(void) const
{
	stackmachine::int8Type data(get());
	return data.print();
}

void
m_stringchar::scan(const std::string &str)
{
	stackmachine::int8Type data('\0');

	data.scan(str);
	assign(data);
}

     m_stringchar::operator               bool   () const { return get() ? true : false; }
     m_stringchar::operator stackmachine::int8   () const { return get(); }
     m_stringchar::operator stackmachine::int16  () const { return get(); }
     m_stringchar::operator stackmachine::int32  () const { return get(); }
     m_stringchar::operator stackmachine::int64  () const { return get(); }
     m_stringchar::operator stackmachine::uint8  () const { return get(); }
     m_stringchar::operator stackmachine::uint16 () const { return get(); }
     m_stringchar::operator stackmachine::uint32 () const { return get(); }
     m_stringchar::operator stackmachine::uint64 () const { return get(); }
     m_stringchar::operator stackmachine::float32() const { return get(); }
     m_stringchar::operator stackmachine::float64() const { return get(); }

void m_stringchar::operator+   (void) { assign(+get()); }
void m_stringchar::operator-   (void) { assign(-get()); }
void m_stringchar::operator~   (void) { assign(~get()); }
void m_stringchar::operator!   (void) { assign(!get()); }

bool m_stringchar::operator<   (const stackmachine::Type &v) const { return (get() <  ((stackmachine::int8) v)); }
bool m_stringchar::operator>   (const stackmachine::Type &v) const { return (get() >  ((stackmachine::int8) v)); }
bool m_stringchar::operator==  (const stackmachine::Type &v) const { return (get() == ((stackmachine::int8) v)); }
bool m_stringchar::operator!=  (const stackmachine::Type &v) const { return (get() != ((stackmachine::int8) v)); }
bool m_stringchar::operator<=  (const stackmachine::Type &v) const { return (get() <= ((stackmachine::int8) v)); }
bool m_stringchar::operator>=  (const stackmachine::Type &v) const { return (get() >= ((stackmachine::int8) v)); }

void m_stringchar::operator=   (const stackmachine::Type &v) { assign(         ((stackmachine::int8) v)); }
void m_stringchar::operator+=  (const stackmachine::Type &v) { assign(get() +  ((stackmachine::int8) v)); }
void m_stringchar::operator-=  (const stackmachine::Type &v) { assign(get() -  ((stackmachine::int8) v)); }
void m_stringchar::operator*=  (const stackmachine::Type &v) { assign(get() *  ((stackmachine::int8) v)); }
void m_stringchar::operator/=  (const stackmachine::Type &v) { assign(get() /  ((stackmachine::int8) v)); }
void m_stringchar::operator%=  (const stackmachine::Type &v) { assign(get() %  ((stackmachine::int8) v)); }
void m_stringchar::operator^=  (const stackmachine::Type &v) { assign(get() ^  ((stackmachine::int8) v)); }
void m_stringchar::operator&=  (const stackmachine::Type &v) { assign(get() &  ((stackmachine::int8) v)); }
void m_stringchar::operator|=  (const stackmachine::Type &v) { assign(get() |  ((stackmachine::int8) v)); }
void m_stringchar::operator>>= (const stackmachine::Type &v) { assign(get() >> ((stackmachine::int8) v)); }
void m_stringchar::operator<<= (const stackmachine::Type &v) { assign(get() << ((stackmachine::int8) v)); }

stackmachine::int8
m_stringchar::get(void) const
{
	std::string str = c.stack[addr];
	stackmachine::int8 c = '\0';

	if (str.size() > off)
		c = str[off];

	return c;
}

void
m_stringchar::assign(stackmachine::int8 v)
{
	std::string str = c.stack[addr];

	if (str.size() <= off)
		str.resize(off+1);

	str[off] = v;

	c.stack[addr] = new stackmachine::stringType(str);
}


void
instr_LDI_stringchar::execute(stackmachine::Context &c)
{
	class m_stringchar *addr;

	addr = dynamic_cast<class m_stringchar *>(&((stackmachine::Type &) c.stack[c.SP]));
	assert(addr);

	c.stack[c.SP] = new stackmachine::int8Type(addr->get());
}
void
instr_LDI_stringchar::dump(lib::stream &f) const
{
	f.put("\tLDI (stringchar)");
	dump_pos(f);
}


void
instr_STI_stringchar::execute(stackmachine::Context &c)
{
	class m_stringchar *addr;

	addr = dynamic_cast<class m_stringchar *>(&((stackmachine::Type &) c.stack[c.SP - 1]));
	assert(addr);

	addr->assign(c.stack[c.SP--]);
}
void
instr_STI_stringchar::dump(lib::stream &f) const
{
	f.put("\tSTI (stringchar)");
	dump_pos(f);
}


void
instr_IXA_stringchar::execute(stackmachine::Context &c)
{
	stackmachine::uint32 off = c.stack[c.SP--];
	stackmachine::uint32 addr = c.stack[c.SP];

	if (off > 65535)
		throw RuntimeError(xxRange, pos);

	c.stack[c.SP] = new m_stringchar(c, addr, off);
}
void
instr_IXA_stringchar::dump(lib::stream &f) const
{
	f.put("\tIXA (stringchar)");
	dump_pos(f);
}


m_eddoperand::m_eddoperand(operand_ptr op)
: myop(op)
{
	assert(myop);

	if (dynamic_cast<const class edd_object_value *>(myop.get()))
		assert(myop->getenv() != NULL);
}

stackmachine::Type *
m_eddoperand::clone(void) const
{
	stackmachine::Type *ret = new m_eddoperand(myop);
	assert(ret);

	return ret;
}

std::string m_eddoperand::type_descr(void) const { return "edd operand"; }

void
m_eddoperand::dump(lib::stream &f) const
{
	f.put("edd operand [");
	myop->dump(f);
	f.put(']');
}

std::string m_eddoperand::print(void) const { return myop->print(); }
void m_eddoperand::scan(const std::string &str, bool strict) { myop->scan(str, strict); }


void
instr_LDA_operand::execute(stackmachine::Context &c)
{
	c.stack[++c.SP] = new m_eddoperand(op);
}
void
instr_LDA_operand::dump(lib::stream &f) const
{
	f.put("\tLDA (edd operand) [");
	op->dump(f);
	f.put(']');
	dump_pos(f);
}


void
instr_LDA_operand_tree::execute(stackmachine::Context &c)
{
	c.stack[++c.SP] = new m_eddoperand(link->value((class method_env *) c.env_data));
}
void
instr_LDA_operand_tree::dump(lib::stream &f) const
{
	f.printf("\tLDA (edd operand from tree) [%s]", link->name());
	dump_pos(f);
}


void
instr_LDA_operand_stack::execute(stackmachine::Context &c)
{
	uint32 fp = c.FP;
	int level = frame;

	/* first search the right frame pointer */
	while (level > 0)
	{
		fp = c.stack[fp-1];
		level--;
	}

	c.stack[++c.SP] = c.stack[fp + offset];
}
void
instr_LDA_operand_stack::dump(lib::stream &f) const
{
	f.printf("\tLDA (edd operand from stack) %i %i", frame, offset);
	dump_pos(f);
}


void
instr_LDI_operand::execute(stackmachine::Context &c)
{
	class m_eddoperand *addr;
	operand_ptr op;

	addr = dynamic_cast<class m_eddoperand *>(&((stackmachine::Type &) c.stack[c.SP]));
	assert(addr);

	op = addr->get();
	assert(op); op->env_assert((class method_env *) c.env_data);

	c.stack[c.SP] = (*op);
}
void
instr_LDI_operand::dump(lib::stream &f) const
{
	f.put("\tLDI (edd operand)");
	dump_pos(f);
}


void
instr_STI_operand::execute(stackmachine::Context &c)
{
	class m_eddoperand *addr;
	operand_ptr op;

	addr = dynamic_cast<class m_eddoperand *>(&((stackmachine::Type &) c.stack[c.SP - 1]));
	assert(addr);

	op = addr->get();
	assert(op); op->env_assert((class method_env *) c.env_data);

	(*op) = c.stack[c.SP--];
}
void
instr_STI_operand::dump(lib::stream &f) const
{
	f.put("\tSTI (edd operand)");
	dump_pos(f);
}


void
instr_IXA_operand::execute(stackmachine::Context &c)
{
	stackmachine::int32 index = c.stack[c.SP--];
	class m_eddoperand *addr;
	operand_ptr op;

	addr = dynamic_cast<class m_eddoperand *>(&((stackmachine::Type &) c.stack[c.SP]));
	assert(addr);

	op = addr->get();
	assert(op); op->env_assert((class method_env *) c.env_data);

	((class method_env *) c.env_data)->set_array_index(new int32_value(index));

	c.stack[c.SP] = new m_eddoperand((*op)[(long)index]);
}
void
instr_IXA_operand::dump(lib::stream &f) const
{
	f.put("\tIXA (edd operand)");
	dump_pos(f);
}


void
instr_STRUCT::execute(stackmachine::Context &c)
{
	class m_eddoperand *addr;
	operand_ptr op;

	addr = dynamic_cast<class m_eddoperand *>(&((stackmachine::Type &) c.stack[c.SP]));
	assert(addr);

	op = addr->get();
	assert(op); op->env_assert((class method_env *) c.env_data);

	op = op->member(ident);
	assert(op);

	if (!lval)
	{
		// dereference, LDI
		c.stack[c.SP] = (*op);
	}
	else
	{
		// dereference, LDA
		c.stack[c.SP] = new m_eddoperand(op);
	}
}
void
instr_STRUCT::dump(lib::stream &f) const
{
	f.printf("\tSTRUCT (edd operand) [identifier %s] (by %s)",
		  ident.c_str(),
		  lval ? "address" : "value");
	dump_pos(f);
}


/**
 * \internal
 * stackdata_byvalue.
 */
class stackdata_byvalue : public operand
{
public:
	/** \name Constructor/destructor. */
	//! \{
	stackdata_byvalue(stackmachine::Context &_c, uint32 sp)
	: c(_c), SP(sp) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone (void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	std::string print(const char *fmt = 0) const;
	void scan(const std::string &str, bool strict);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

private:
	stackmachine::Context &c;
	const uint32 SP;
};

operand_ptr
stackdata_byvalue::clone (void) const { return new stackdata_byvalue(c, SP); }

void
stackdata_byvalue::dump0(lib::stream &f) const
{
	f.printf("stackdata_byvalue: SP %" PRIu32 " ", SP);
	c.stack[SP].dump(f);
}

std::string stackdata_byvalue::print(const char *) const { return c.stack[SP].print(); }
void stackdata_byvalue::scan(const std::string &str, bool strict) { c.stack[SP].scan(str); }

         stackdata_byvalue::operator stackmachine::Type *() const { return c.stack[SP].clone(); }


/**
 * \internal
 * stackdata_byref.
 */
class stackdata_byref : public operand
{
public:
	/** \name Constructor/destructor. */
	//! \{
	stackdata_byref(stackmachine::Context &_c, uint32 sp)
	: c(_c), SP(sp) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone (void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	std::string print(const char *fmt = 0) const;
	void scan(const std::string &str, bool strict);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Other operators. */
	//! \{
	operand_ptr operator[]  (long index) const;
	//! \}

private:
	stackmachine::Context &c;
	const uint32 SP;
};

operand_ptr
stackdata_byref::clone (void) const { return new stackdata_byref(c, SP); }

void
stackdata_byref::dump0(lib::stream &f) const
{
	f.printf("stackdata_byref: SP %" PRIu32 " ", SP);
	c.stack[SP].dump(f);
}

std::string stackdata_byref::print(const char *) const { return c.stack[SP].print(); }
void stackdata_byref::scan(const std::string &str, bool strict) { c.stack[SP].scan(str); }

            stackdata_byref::operator stackmachine::Type *() const { return c.stack[SP].clone(); }
void        stackdata_byref::operator= (const stackmachine::Type &d) { c.stack[SP] = d.clone(); }

operand_ptr stackdata_byref::operator[] (long index) const { return new stackdata_byref(c, SP + index); }


instr_CALL::instr_CALL(const class c_method *p, unsigned int c)
: c_method(p), argcount(c)
{
	assert(c_method);
	assert(c_method->argcount() == argcount);
}
void
instr_CALL::execute(stackmachine::Context &c)
{
	const uint32 SP = c.SP;
	operand_ptr args;

	for (unsigned int i = 0; i < argcount; i++)
	{
		const uint32 argSP = SP - i;
		class m_eddoperand *addr;
		operand_ptr tmp;

		addr = dynamic_cast<class m_eddoperand *>(&((stackmachine::Type &) c.stack[argSP]));
		if (addr)
		{
			if (c_method->argmethod(i) == c_method::byvalue)
				tmp = addr->dup(); // XXX ???
			else /* byref */
				tmp = addr->get();
		}
		else
		{
			if (c_method->argmethod(i) == c_method::byvalue)
				tmp = new stackdata_byvalue(c, argSP);
			else
				tmp = new stackdata_byref(c, c.stack[argSP]);
		}

		assert(tmp);
		args.concat(tmp);
	}

	operand_ptr result(c_method->dispatch((class method_env *) c.env_data, args, pos));
	assert(result);

	if (c_method->returntype() != c_type::tvoid)
		c.stack[++c.SP] = (*result);
}
void
instr_CALL::dump(lib::stream &f) const
{
	f.printf("\tCALL %s", c_method->ident());
	dump_pos(f);
}


void
instr_LDC_identifier::execute(stackmachine::Context &c)
{
	operand_ptr op(new class identifier_value(myid));
	c.stack[++c.SP] = new m_eddoperand(op);
}
void
instr_LDC_identifier::dump(lib::stream &f) const
{
	f.printf("\tLDC (identifier) [%s]", myid.c_str());
	dump_pos(f);
}


void
instr_LDC_dictionary::execute(stackmachine::Context &c)
{
	const std::string value = parent->dictionary_lookup(myid);
	c.stack[++c.SP] = new stackmachine::stringType(value);
}
void
instr_LDC_dictionary::dump(lib::stream &f) const
{
	f.printf("\tLDC (dictionary) [%s]", myid.c_str());
	dump_pos(f);
}


void
instr_LDC_enumstring::execute(stackmachine::Context &c)
{
	class method_env *env = (class method_env *) c.env_data;
	std::vector<class VARIABLE_ENUMERATOR_tree *> enums(vt->get_enumerations(env));

	stackmachine::int32 value = c.stack[c.SP];
	bool found = false;

	for (size_t i = 0, i_max = enums.size(); i < i_max; i++)
	{
		operand_ptr op(enums[i]->value(env));
		int32 v = *op;

		if (v == value)
		{
			c.stack[c.SP] = new stackmachine::stringType(enums[i]->description(env));
			found = true;

			break;
		}
	}

	if (!found)
	{
		char buf[32];
		snprintf(buf, sizeof(buf), "%" PRId32 , value);

		c.stack[c.SP] = new stackmachine::stringType(buf);
	}
}
void
instr_LDC_enumstring::dump(lib::stream &f) const
{
	f.printf("\tLDC (enumstring) [%s]", vt->name());
	dump_pos(f);
}

} /* namespace edd */
