
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the units generator tool,
 * written for the EDDL project.
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_units_si_table.h"
#include "edd_err.h"
#include "edd_values.h"

#include "BasicType.h"
#include "Context.h"
#include "Instr.h"
#include "SMachine.h"

using namespace stackmachine;



namespace edd
{

static const double si_factors_1_kg_per_m_s2[30] =
{
/*  0 */ 2.4885884520e+02, /* inH2O_68degF */
/*  1 */ 3.3863800000e+03, /* inHg_0degC */
/*  2 */ 1.3332200000e+02, /* mmHg_0degC */
/*  3 */ 6.8947547895e+03, /* psi */
/*  4 */ 1.0000000000e+05, /* bar */
/*  5 */ 1.0000000000e+02, /* mbar */
/*  6 */ 1.0000000000e+00, /* Pa */
/*  7 */ 1.0000000000e+03, /* kPa */
/*  8 */ 1.3332200000e+02, /* torr */
/*  9 */ 1.0132472000e+05, /* atm */
/* 10 */ 1.0000000000e+06, /* MPa */
/* 11 */ 2.4908200000e+02, /* inH2O_4degC */
/* 12 */ 9.8063800000e+00, /* mmH2O_4degC */
/* 13 */ 1.0000000000e+00, /* Pa */
/* 14 */ 1.0000000000e+09, /* GPa */
/* 15 */ 1.0000000000e+06, /* MPa */
/* 16 */ 1.0000000000e+03, /* kPa */
/* 17 */ 1.0000000000e-03, /* mPa */
/* 18 */ 1.0000000000e-06, /* microPa */
/* 19 */ 1.0000000000e+02, /* hPa */
/* 20 */ 1.0000000000e+05, /* bar */
/* 21 */ 1.0000000000e+02, /* mbar */
/* 22 */ 1.3332200000e+02, /* torr */
/* 23 */ 1.0132472000e+05, /* atm */
/* 24 */ 6.8947547895e+03, /* psi */
/* 25 */ 2.4908200000e+02, /* inH2O_4degC */
/* 26 */ 2.4885884520e+02, /* inH2O_68degF */
/* 27 */ 9.8063800000e+00, /* mmH2O_4degC */
/* 28 */ 3.3863800000e+03, /* inHg_0degC */
/* 29 */ 1.3332200000e+02, /* mmHg_0degC */
};

static const double si_factors_1_kg_per_m2[4] =
{
/*  0 */ 1.0000000000e+01, /* gm/cm2 */
/*  1 */ 1.0000000000e+04, /* kg/cm2 */
/*  2 */ 1.0000000000e+01, /* gm/cm2 */
/*  3 */ 1.0000000000e+04, /* kg/cm2 */
};

static const double si_factors_1_m3_per_s[147] =
{
/*  0 */ 4.7194744320e-04, /* feet3/min */
/*  1 */ 6.3090196400e-05, /* gal_US_liq/min */
/*  2 */ 1.6666666667e-05, /* liter/min */
/*  3 */ 7.5768321581e-05, /* impGal/min */
/*  4 */ 2.7777777778e-04, /* m3/hr */
/*  5 */ 3.7854117840e-03, /* gal_US_liq/s */
/*  6 */ 4.3812636389e-02, /* megagal_US_liq/day */
/*  7 */ 1.0000000000e-03, /* liter/s */
/*  8 */ 1.1574074074e-02, /* megaliter/day */
/*  9 */ 2.8316846592e-02, /* feet3/s */
/* 10 */ 3.2774128000e-07, /* feet3/day */
/* 11 */ 1.0000000000e+00, /* m3/s */
/* 12 */ 1.1574074074e-05, /* m3/day */
/* 13 */ 1.2628053597e-06, /* impGal/hr */
/* 14 */ 5.2616889987e-08, /* impGal/day */
/* 15 */ 2.7777777778e-04, /* m3/hr */
/* 16 */ 2.7777777778e-07, /* liter/hr */
/* 17 */ 4.7194744320e-04, /* feet3/min */
/* 18 */ 7.8657907200e-06, /* feet3/hr */
/* 19 */ 1.6666666667e-02, /* m3/min */
/* 20 */ 1.5898729493e-01, /* bbl_US_petrol/s */
/* 21 */ 2.6497882488e-03, /* bbl_US_petrol/min */
/* 22 */ 4.4163137480e-05, /* bbl_US_petrol/hr */
/* 23 */ 1.8401307283e-06, /* bbl_US_petrol/day */
/* 24 */ 1.0515032733e-06, /* gal_US_liq/hr */
/* 25 */ 4.5460992949e-03, /* impGal/s */
/* 26 */ 2.7777777778e-07, /* liter/hr */
/* 27 */ 4.3812636389e-08, /* gal_US_liq/day */
/* 28 */ 1.0000000000e+00, /* m3/s */
/* 29 */ 1.6666666667e-02, /* m3/min */
/* 30 */ 2.7777777778e-04, /* m3/hr */
/* 31 */ 1.1574074074e-05, /* m3/day */
/* 32 */ 1.0000000000e-03, /* liter/s */
/* 33 */ 1.6666666667e-05, /* liter/min */
/* 34 */ 2.7777777778e-07, /* liter/hr */
/* 35 */ 1.1574074074e-08, /* liter/day */
/* 36 */ 1.1574074074e-02, /* Mliter/day */
/* 37 */ 2.8316846592e-02, /* ft3/s */
/* 38 */ 7.8657907200e-06, /* ft3/hr */
/* 39 */ 3.2774128000e-07, /* ft3/day */
/* 40 */ 3.7854117840e-03, /* gal_US_liq/s */
/* 41 */ 6.3090196400e-05, /* gal_US_liq/min */
/* 42 */ 1.0515032733e-06, /* gal_US_liq/hr */
/* 43 */ 4.3812636389e-08, /* gal_US_liq/day */
/* 44 */ 4.3812636389e-02, /* Mgal_US_liq/day */
/* 45 */ 4.5460992949e-03, /* impGal/s */
/* 46 */ 7.5768321581e-05, /* impGal/min */
/* 47 */ 1.2628053597e-06, /* impGal/hr */
/* 48 */ 5.2616889987e-08, /* impGal/day */
/* 49 */ 1.5898729493e-01, /* bbl_US_petrol/s */
/* 50 */ 2.6497882488e-03, /* bbl_US_petrol/min */
/* 51 */ 4.4163137480e-05, /* bbl_US_petrol/hr */
/* 52 */ 1.8401307283e-06, /* bbl_US_petrol/day */
/* 53 */ 3.7854117840e-06, /* mgal_US_liq/s */
/* 54 */ 3.7854117840e-09, /* microgal_US_liq/s */
/* 55 */ 3.7854117840e+00, /* kgal_US_liq/s */
/* 56 */ 3.7854117840e+03, /* Mgal_US_liq/s */
/* 57 */ 6.3090196400e-08, /* mgal_US_liq/min */
/* 58 */ 6.3090196400e-11, /* microgal_US_liq/min */
/* 59 */ 6.3090196400e-02, /* kgal_US_liq/min */
/* 60 */ 6.3090196400e+01, /* Mgal_US_liq/min */
/* 61 */ 1.0515032733e-12, /* microgal_US_liq/hr */
/* 62 */ 1.0515032733e-09, /* mgal_US_liq/hr */
/* 63 */ 1.0515032733e-03, /* kgal_US_liq/hr */
/* 64 */ 1.0515032733e+00, /* Mgal_US_liq/hr */
/* 65 */ 4.3812636389e-11, /* mgal_US_liq/day */
/* 66 */ 4.3812636389e-14, /* microgal_US_liq/day */
/* 67 */ 4.3812636389e-05, /* kgal_US_liq/day */
/* 68 */ 4.5460992949e-09, /* microimpGal/s */
/* 69 */ 4.5460992949e-06, /* mimpGal/s */
/* 70 */ 4.5460992949e+00, /* kimpGal/s */
/* 71 */ 4.5460992949e+03, /* MimpGal/s */
/* 72 */ 7.5768321581e-11, /* microimpGal/min */
/* 73 */ 7.5768321581e-08, /* mimpGal/min */
/* 74 */ 7.5768321581e-02, /* kimpGal/min */
/* 75 */ 7.5768321581e+01, /* MimpGal/min */
/* 76 */ 1.2628053597e-12, /* microimpGal/hr */
/* 77 */ 1.2628053597e-09, /* mimpGal/hr */
/* 78 */ 1.2628053597e-03, /* kimpGal/hr */
/* 79 */ 1.2628053597e+00, /* MimpGal/hr */
/* 80 */ 5.2616889987e-14, /* microimpGal/day */
/* 81 */ 5.2616889987e-11, /* mimpGal/day */
/* 82 */ 5.2616889987e-05, /* kimpGal/day */
/* 83 */ 5.2616889987e-02, /* MimpGal/day */
/* 84 */ 1.5898729493e-07, /* microbbl_US_petrol/s */
/* 85 */ 1.5898729493e-04, /* mbbl_US_petrol/s */
/* 86 */ 1.5898729493e+02, /* kbbl_US_petrol/s */
/* 87 */ 1.5898729493e+05, /* Mbbl_US_petrol/s */
/* 88 */ 2.6497882488e-09, /* microbbl_US_petrol/min */
/* 89 */ 2.6497882488e-06, /* mbbl_US_petrol/min */
/* 90 */ 2.6497882488e+00, /* kbbl_US_petrol/min */
/* 91 */ 2.6497882488e+03, /* Mbbl_US_petrol/min */
/* 92 */ 4.4163137480e-11, /* microbbl_US_petrol/hr */
/* 93 */ 4.4163137480e-08, /* mbbl_US_petrol/hr */
/* 94 */ 4.4163137480e-02, /* kbbl_US_petrol/hr */
/* 95 */ 4.4163137480e+01, /* Mbbl_US_petrol/hr */
/* 96 */ 1.8401307283e-12, /* microbbl_US_petrol/day */
/* 97 */ 1.8401307283e-09, /* mbbl_US_petrol/day */
/* 98 */ 1.8401307283e-03, /* kbbl_US_petrol/day */
/* 99 */ 1.8401307283e+00, /* Mbbl_US_petrol/day */
/* 100 */ 1.0000000000e-18, /* microm3/s */
/* 101 */ 1.0000000000e-09, /* mm3/s */
/* 102 */ 1.0000000000e+09, /* km3/s */
/* 103 */ 1.0000000000e+18, /* Mm3/s */
/* 104 */ 1.6666666667e-20, /* microm3/min */
/* 105 */ 1.6666666667e-11, /* mm3/min */
/* 106 */ 1.6666666667e+07, /* km3/min */
/* 107 */ 1.6666666667e+16, /* Mm3/min */
/* 108 */ 2.7777777778e-22, /* microm3/hr */
/* 109 */ 2.7777777778e-13, /* mm3/hr */
/* 110 */ 2.7777777778e+05, /* km3/hr */
/* 111 */ 2.7777777778e+14, /* Mm3/hr */
/* 112 */ 1.1574074074e-23, /* microm3/day */
/* 113 */ 1.1574074074e-14, /* mm3/day */
/* 114 */ 1.1574074074e+04, /* km3/day */
/* 115 */ 1.1574074074e+13, /* Mm3/day */
/* 116 */ 1.0000000000e-06, /* cm3/s */
/* 117 */ 1.6666666667e-08, /* cm3/min */
/* 118 */ 2.7777777778e-10, /* cm3/hr */
/* 119 */ 1.1574074074e-11, /* cm3/day */
/* 120 */ 1.6666666667e-02, /* kliter/min */
/* 121 */ 2.7777777778e-04, /* kliter/hr */
/* 122 */ 1.1574074074e-05, /* kliter/day */
/* 123 */ 1.6666666667e-08, /* ml/min */
/* 124 */ 1.0000000000e-06, /* ml/s */
/* 125 */ 2.7777777778e-10, /* ml/hr */
/* 126 */ 1.1574074074e-11, /* ml/day */
/* 127 */ 1.2334818375e+00, /* af/s */
/* 128 */ 2.0558030626e-02, /* af/min */
/* 129 */ 3.4263384376e-04, /* af/hr */
/* 130 */ 1.4276410157e-05, /* af/day */
/* 131 */ 2.9562263456e-05, /* fl_oz_US/s */
/* 132 */ 4.9270439093e-07, /* fl_oz_US/min */
/* 133 */ 8.2117398489e-09, /* fl_oz_US/hr */
/* 134 */ 3.4215582704e-10, /* fl_oz_US/day */
/* 135 */ 1.0000000000e-01, /* hl/s */
/* 136 */ 1.6666666667e-03, /* hl/min */
/* 137 */ 2.7777777778e-05, /* hl/hr */
/* 138 */ 1.1574074074e-06, /* hl/day */
/* 139 */ 1.1924047120e-01, /* bbl_US_liq/s */
/* 140 */ 1.9873411866e-03, /* bbl_US_liq/min */
/* 141 */ 3.3122353110e-05, /* bbl_US_liq/hr */
/* 142 */ 1.3800980463e-06, /* bbl_US_liq/day */
/* 143 */ 1.1734776530e-01, /* bbl_US_fed/s */
/* 144 */ 1.9557960884e-03, /* bbl_US_fed/min */
/* 145 */ 3.2596601473e-05, /* bbl_US_fed/hr */
/* 146 */ 1.3581917281e-06, /* bbl_US_fed/day */
};

static const double si_factors_1_m_per_s[21] =
{
/*  0 */ 3.0480000000e-01, /* feet/s */
/*  1 */ 1.0000000000e+00, /* m/s */
/*  2 */ 2.5400000000e-02, /* in/s */
/*  3 */ 4.2333333333e-04, /* in/min */
/*  4 */ 5.0800000000e-03, /* feet/min */
/*  5 */ 2.7777777778e-04, /* m/hr */
/*  6 */ 1.0000000000e+00, /* m/s */
/*  7 */ 1.0000000000e-03, /* mm/s */
/*  8 */ 2.7777777778e-04, /* m/hr */
/*  9 */ 2.7777777778e-01, /* km/hr */
/* 10 */ 5.1444444444e-01, /* knot */
/* 11 */ 2.5400000000e-02, /* in/s */
/* 12 */ 3.0480000000e-01, /* ft/s */
/* 13 */ 9.1440000000e-01, /* yd/s */
/* 14 */ 4.2333333333e-04, /* in/min */
/* 15 */ 5.0800000000e-03, /* ft/min */
/* 16 */ 1.5240000000e-02, /* yd/min */
/* 17 */ 7.0555555556e-06, /* in/hr */
/* 18 */ 8.4666666667e-05, /* ft/hr */
/* 19 */ 2.5400000000e-04, /* yd/hr */
/* 20 */ 4.4704000000e-01, /* mph */
};

static void
si_rule_normalize_1_K_0(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(273.150000));
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_ADD);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_0(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(273.150000));
	sm.emit(new instr_SUB);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_1(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(255.372000));
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_MUL);
	sm.emit(new instr_ADD);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_1(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(255.372000));
	sm.emit(new instr_SUB);
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_DIV);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_2(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_MUL);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_2(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_DIV);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_3(class SMachine &sm)
{
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_3(class SMachine &sm)
{
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_4(class SMachine &sm)
{
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_4(class SMachine &sm)
{
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_5(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(273.150000));
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_ADD);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_5(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(273.150000));
	sm.emit(new instr_SUB);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_6(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(255.372000));
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_MUL);
	sm.emit(new instr_ADD);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_6(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(255.372000));
	sm.emit(new instr_SUB);
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_DIV);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_normalize_1_K_7(class SMachine &sm)
{
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_MUL);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void
si_rule_denormalize_1_K_7(class SMachine &sm)
{
	sm.emit(new instr_LOAD_RET);
	sm.emit(new instr_LDC_float64(5));
	sm.emit(new instr_LDC_float64(9));
	sm.emit(new instr_DIV);
	sm.emit(new instr_DIV);
	sm.emit(new instr_STORE_RET);
	sm.emit(new instr_HLT);
}
static void (*const si_rules_normalize_1_K[8])(class SMachine &) =
{
	si_rule_normalize_1_K_0,
	si_rule_normalize_1_K_1,
	si_rule_normalize_1_K_2,
	si_rule_normalize_1_K_3,
	si_rule_normalize_1_K_4,
	si_rule_normalize_1_K_5,
	si_rule_normalize_1_K_6,
	si_rule_normalize_1_K_7,
};
static void (*const si_rules_denormalize_1_K[8])(class SMachine &) =
{
	si_rule_denormalize_1_K_0,
	si_rule_denormalize_1_K_1,
	si_rule_denormalize_1_K_2,
	si_rule_denormalize_1_K_3,
	si_rule_denormalize_1_K_4,
	si_rule_denormalize_1_K_5,
	si_rule_denormalize_1_K_6,
	si_rule_denormalize_1_K_7,
};

static const double si_factors_1_m2_kg_per_s3_A[7] =
{
/*  0 */ 1.0000000000e-03, /* mV */
/*  1 */ 1.0000000000e+00, /* V */
/*  2 */ 1.0000000000e+00, /* V */
/*  3 */ 1.0000000000e+06, /* MV */
/*  4 */ 1.0000000000e+03, /* kV */
/*  5 */ 1.0000000000e-03, /* mV */
/*  6 */ 1.0000000000e-06, /* microV */
};

static const double si_factors_1_m2_kg_per_s3_A2[17] =
{
/*  0 */ 1.0000000000e+00, /* Ohm */
/*  1 */ 1.0000000000e+03, /* kOhm */
/*  2 */ 1.0000000000e+00, /* W */
/*  3 */ 1.0000000000e+12, /* TW */
/*  4 */ 1.0000000000e+09, /* GW */
/*  5 */ 1.0000000000e+06, /* MW */
/*  6 */ 1.0000000000e+03, /* kW */
/*  7 */ 1.0000000000e-03, /* mW */
/*  8 */ 1.0000000000e-06, /* microW */
/*  9 */ 1.0000000000e-09, /* nW */
/* 10 */ 1.0000000000e-12, /* pW */
/* 11 */ 1.0000000000e+00, /* Ohm */
/* 12 */ 1.0000000000e+09, /* GOhm */
/* 13 */ 1.0000000000e+06, /* MOhm */
/* 14 */ 1.0000000000e+03, /* kOhm */
/* 15 */ 1.0000000000e-03, /* mOhm */
/* 16 */ 1.0000000000e-06, /* microOhm */
};

static const double si_factors_1_per_s[11] =
{
/*  0 */ 1.0000000000e+00, /* Hz */
/*  1 */ 1.0000000000e+00, /* Hz */
/*  2 */ 1.0000000000e+12, /* THz */
/*  3 */ 1.0000000000e+09, /* GHz */
/*  4 */ 1.0000000000e+06, /* MHz */
/*  5 */ 1.0000000000e+03, /* kHz */
/*  6 */ 1.0000000000e+00, /* 1/s */
/*  7 */ 1.6666666667e-02, /* 1/min */
/*  8 */ 1.0000000000e+00, /* Bq */
/*  9 */ 1.0000000000e+06, /* MBq */
/* 10 */ 1.0000000000e+03, /* kBq */
};

static const double si_factors_1_A[7] =
{
/*  0 */ 1.0000000000e-03, /* mA */
/*  1 */ 1.0000000000e+00, /* A */
/*  2 */ 1.0000000000e+03, /* kA */
/*  3 */ 1.0000000000e-03, /* mA */
/*  4 */ 1.0000000000e-06, /* microA */
/*  5 */ 1.0000000000e-09, /* nA */
/*  6 */ 1.0000000000e-12, /* pA */
};

static const double si_factors_1_m3[35] =
{
/*  0 */ 3.7854117840e-03, /* gal_US_liq */
/*  1 */ 1.0000000000e-03, /* liter */
/*  2 */ 4.5460992949e-03, /* impGal */
/*  3 */ 1.0000000000e+00, /* m3 */
/*  4 */ 1.5898729493e-01, /* bbl_US_petrol */
/*  5 */ 3.5239135715e-02, /* bu_US_dry */
/*  6 */ 7.6455485798e-01, /* yard3 */
/*  7 */ 2.8316846592e-02, /* feet3 */
/*  8 */ 1.6387064000e-05, /* inch3 */
/*  9 */ 1.1924047120e-01, /* bbl_US_liq */
/* 10 */ 1.0000000000e-01, /* hectoliter */
/* 11 */ 1.0000000000e+00, /* m3 */
/* 12 */ 1.0000000000e-03, /* dm3 */
/* 13 */ 1.0000000000e-06, /* cm3 */
/* 14 */ 1.0000000000e-09, /* mm3 */
/* 15 */ 1.0000000000e-03, /* liter */
/* 16 */ 1.0000000000e-05, /* cl */
/* 17 */ 1.0000000000e-06, /* ml */
/* 18 */ 1.0000000000e-01, /* hl */
/* 19 */ 1.6387064000e-05, /* in3 */
/* 20 */ 2.8316846592e-02, /* ft3 */
/* 21 */ 7.6455485798e-01, /* yd3 */
/* 22 */ 4.1681818254e+09, /* mile3 */
/* 23 */ 4.7317647300e-04, /* pt_US_liq */
/* 24 */ 9.4635294600e-04, /* qt_US_liq */
/* 25 */ 3.7854117840e-03, /* gal_US_liq */
/* 26 */ 4.5460992949e-03, /* impGal */
/* 27 */ 3.5239135715e-02, /* bu_US_dry */
/* 28 */ 1.5898729493e-01, /* bbl_US_petrol */
/* 29 */ 1.1924047120e-01, /* bbl_US_liq */
/* 30 */ 1.0000000000e+00, /* kliter */
/* 31 */ 2.9562263456e-05, /* fl_oz_US */
/* 32 */ 1.0000000000e-06, /* cm3 */
/* 33 */ 1.2334818375e+00, /* af */
/* 34 */ 1.1734776530e-01, /* bbl_US_fed */
};

static const double si_factors_1_m[18] =
{
/*  0 */ 3.0480000000e-01, /* feet */
/*  1 */ 1.0000000000e+00, /* m */
/*  2 */ 2.5400000000e-02, /* inch */
/*  3 */ 1.0000000000e-02, /* cm */
/*  4 */ 1.0000000000e-03, /* mm */
/*  5 */ 1.0000000000e+00, /* m */
/*  6 */ 1.0000000000e+03, /* km */
/*  7 */ 1.0000000000e-02, /* cm */
/*  8 */ 1.0000000000e-03, /* mm */
/*  9 */ 1.0000000000e-06, /* microm */
/* 10 */ 1.0000000000e-09, /* nm */
/* 11 */ 1.0000000000e-12, /* pm */
/* 12 */ 1.0000000000e-10, /* angstrom */
/* 13 */ 3.0480000000e-01, /* ft */
/* 14 */ 2.5400000000e-02, /* in */
/* 15 */ 9.1440000000e-01, /* yd */
/* 16 */ 1.6093440000e+03, /* mile */
/* 17 */ 1.8520000000e+03, /* nmile */
};

static const double si_factors_1_s[11] =
{
/*  0 */ 6.0000000000e+01, /* min */
/*  1 */ 1.0000000000e+00, /* s */
/*  2 */ 3.6000000000e+03, /* hr */
/*  3 */ 8.6400000000e+04, /* day */
/*  4 */ 1.0000000000e-06, /* us */
/*  5 */ 1.0000000000e+00, /* s */
/*  6 */ 1.0000000000e-03, /* ms */
/*  7 */ 1.0000000000e-06, /* us */
/*  8 */ 6.0000000000e+01, /* min */
/*  9 */ 3.6000000000e+03, /* hr */
/* 10 */ 8.6400000000e+04, /* day */
};

static const double si_factors_1_m2_per_s[4] =
{
/*  0 */ 1.0000000000e-06, /* cSt */
/*  1 */ 1.0000000000e+00, /* m2/s */
/*  2 */ 1.0000000000e-04, /* St */
/*  3 */ 1.0000000000e-06, /* cSt */
};

static const double si_factors_1_kg_per_m_s[4] =
{
/*  0 */ 1.0000000000e-02, /* centiPo */
/*  1 */ 1.0000000000e+00, /* Pa-s */
/*  2 */ 1.0000000000e+00, /* Po */
/*  3 */ 1.0000000000e-02, /* cPo */
};

static const double si_factors_1_kg[19] =
{
/*  0 */ 1.0000000000e-03, /* gm */
/*  1 */ 1.0000000000e+00, /* kg */
/*  2 */ 1.0000000000e+03, /* metricton */
/*  3 */ 4.5359237000e-01, /* pound */
/*  4 */ 9.0718474000e+02, /* shortton */
/*  5 */ 1.0160469088e+03, /* longton */
/*  6 */ 2.8349523125e-02, /* ounce */
/*  7 */ 1.0000000000e+00, /* kg */
/*  8 */ 1.0000000000e-03, /* gm */
/*  9 */ 1.0000000000e-06, /* mg */
/* 10 */ 1.0000000000e+03, /* Mgm */
/* 11 */ 1.0000000000e+03, /* metricton */
/* 12 */ 2.8349523125e-02, /* oz */
/* 13 */ 4.5359237000e-01, /* lb */
/* 14 */ 9.0718474000e+02, /* shortton */
/* 15 */ 1.0160469088e+03, /* longton */
/* 16 */ 2.0000000000e-04, /* ct */
/* 17 */ 3.7324172160e-01, /* lb_tr */
/* 18 */ 3.1103476800e-02, /* oz_tr */
};

static const double si_factors_1_s3_A2_per_m3_kg[11] =
{
/*  0 */ 1.0000000000e-01, /* mS/cm */
/*  1 */ 1.0000000000e-04, /* microS/cm */
/*  2 */ 1.0000000000e+00, /* S/m */
/*  3 */ 1.0000000000e+06, /* MS/m */
/*  4 */ 1.0000000000e+03, /* kS/m */
/*  5 */ 1.0000000000e-01, /* mS/cm */
/*  6 */ 1.0000000000e-03, /* microS/mm */
/*  7 */ 1.0000000000e+02, /* S/cm */
/*  8 */ 1.0000000000e-04, /* microS/cm */
/*  9 */ 1.0000000000e-03, /* mS/m */
/* 10 */ 1.0000000000e-06, /* microS/m */
};

static const double si_factors_1_m_kg_per_s2[6] =
{
/*  0 */ 1.0000000000e+00, /* N */
/*  1 */ 1.0000000000e+00, /* N */
/*  2 */ 1.0000000000e+06, /* MN */
/*  3 */ 1.0000000000e+03, /* kN */
/*  4 */ 1.0000000000e-03, /* mN */
/*  5 */ 1.0000000000e-06, /* microN */
};

static const double si_factors_1_m2_kg_per_s2[29] =
{
/*  0 */ 1.0000000000e+00, /* N-m */
/*  1 */ 1.0543512640e+09, /* decatherm */
/*  2 */ 1.3558174560e+00, /* ft-lbf */
/*  3 */ 3.6000000000e+06, /* kWh */
/*  4 */ 4.1840000000e+06, /* Mcal_therm */
/*  5 */ 1.0000000000e+06, /* MJ */
/*  6 */ 1.0543512640e+03, /* Btu_therm */
/*  7 */ 1.0000000000e+00, /* N-m */
/*  8 */ 1.0000000000e+06, /* MN-m */
/*  9 */ 1.0000000000e+03, /* kN-m */
/* 10 */ 1.0000000000e-03, /* mN-m */
/* 11 */ 1.0000000000e+00, /* J */
/* 12 */ 1.0000000000e+18, /* EJ */
/* 13 */ 1.0000000000e+15, /* PJ */
/* 14 */ 1.0000000000e+12, /* TJ */
/* 15 */ 1.0000000000e+09, /* GJ */
/* 16 */ 1.0000000000e+06, /* MJ */
/* 17 */ 1.0000000000e+03, /* kJ */
/* 18 */ 1.0000000000e-03, /* mJ */
/* 19 */ 3.6000000000e+03, /* Wh */
/* 20 */ 3.6000000000e+15, /* TWh */
/* 21 */ 3.6000000000e+12, /* GWh */
/* 22 */ 3.6000000000e+09, /* MWh */
/* 23 */ 3.6000000000e+06, /* kWh */
/* 24 */ 4.1840000000e+00, /* cal_therm */
/* 25 */ 4.1840000000e+03, /* kcal_therm */
/* 26 */ 4.1840000000e+06, /* Mcal_therm */
/* 27 */ 1.0543512640e+03, /* Btu_therm */
/* 28 */ 1.0543512640e+09, /* decatherm */
};

static const double si_factors_1_kg_per_s[48] =
{
/*  0 */ 1.0000000000e-03, /* gm/s */
/*  1 */ 1.6666666667e-05, /* gm/min */
/*  2 */ 2.7777777778e-07, /* gm/hr */
/*  3 */ 1.0000000000e+00, /* kg/s */
/*  4 */ 1.6666666667e-02, /* kg/min */
/*  5 */ 2.7777777778e-04, /* kg/hr */
/*  6 */ 1.1574074074e-05, /* kg/day */
/*  7 */ 1.6666666667e+01, /* metricton/min */
/*  8 */ 2.7777777778e-01, /* metricton/hr */
/*  9 */ 1.1574074074e-02, /* metricton/day */
/* 10 */ 4.5359237000e-01, /* pound/s */
/* 11 */ 7.5598728333e-03, /* pound/min */
/* 12 */ 1.2599788056e-04, /* pound/hr */
/* 13 */ 5.2499116898e-06, /* pound/day */
/* 14 */ 1.5119745667e+01, /* shortton/min */
/* 15 */ 2.5199576111e-01, /* shortton/hr */
/* 16 */ 1.0499823380e-02, /* shortton/day */
/* 17 */ 2.8223525244e-01, /* longton/hr */
/* 18 */ 1.1759802185e-02, /* longton/day */
/* 19 */ 1.0000000000e-03, /* gm/s */
/* 20 */ 1.6666666667e-05, /* gm/min */
/* 21 */ 2.7777777778e-07, /* gm/hr */
/* 22 */ 1.1574074074e-08, /* gm/day */
/* 23 */ 1.0000000000e+00, /* kg/s */
/* 24 */ 1.6666666667e-02, /* kg/min */
/* 25 */ 2.7777777778e-04, /* kg/hr */
/* 26 */ 1.1574074074e-05, /* kg/day */
/* 27 */ 1.0000000000e+03, /* metricton/s */
/* 28 */ 1.6666666667e+01, /* metricton/min */
/* 29 */ 2.7777777778e-01, /* metricton/hr */
/* 30 */ 1.1574074074e-02, /* metricton/day */
/* 31 */ 4.5359237000e-01, /* lb/s */
/* 32 */ 7.5598728333e-03, /* lb/min */
/* 33 */ 1.2599788056e-04, /* lb/hr */
/* 34 */ 5.2499116898e-06, /* lb/day */
/* 35 */ 9.0718474000e+02, /* shortton/s */
/* 36 */ 1.5119745667e+01, /* shortton/min */
/* 37 */ 2.5199576111e-01, /* shortton/hr */
/* 38 */ 1.0499823380e-02, /* shortton/day */
/* 39 */ 1.0160469088e+03, /* longton/s */
/* 40 */ 1.6934115147e+01, /* longton/min */
/* 41 */ 2.8223525244e-01, /* longton/hr */
/* 42 */ 1.1759802185e-02, /* longton/day */
/* 43 */ 1.0000000000e+00, /* N-s/m */
/* 44 */ 2.8349523125e-02, /* oz/s */
/* 45 */ 4.7249205208e-04, /* oz/min */
/* 46 */ 7.8748675347e-06, /* oz/hr */
/* 47 */ 3.2811948061e-07, /* oz/day */
};

static const double si_factors_1_kg_per_m3[28] =
{
/*  0 */ 1.0000000000e+03, /* gm/cm3 */
/*  1 */ 1.0000000000e+00, /* kg/m3 */
/*  2 */ 1.1982642732e+02, /* pound/gal_US_liq */
/*  3 */ 1.6018463374e+01, /* pound/feet3 */
/*  4 */ 1.0000000000e+03, /* gm/ml */
/*  5 */ 1.0000000000e+03, /* kg/liter */
/*  6 */ 1.0000000000e+00, /* gm/liter */
/*  7 */ 2.7679904710e+04, /* pound/inch3 */
/*  8 */ 1.1865528425e+03, /* shortton/yd3 */
/*  9 */ 1.0000000000e+00, /* kg/m3 */
/* 10 */ 1.0000000000e+03, /* Mgm/m3 */
/* 11 */ 1.0000000000e+03, /* kg/dm3 */
/* 12 */ 1.0000000000e+03, /* gm/cm3 */
/* 13 */ 1.0000000000e-03, /* gm/m3 */
/* 14 */ 1.0000000000e+03, /* metricton/m3 */
/* 15 */ 1.0000000000e+03, /* kg/liter */
/* 16 */ 1.0000000000e+03, /* gm/ml */
/* 17 */ 1.0000000000e+00, /* gm/liter */
/* 18 */ 2.7679904710e+04, /* lb/in3 */
/* 19 */ 1.6018463374e+01, /* lb/ft3 */
/* 20 */ 1.1982642732e+02, /* lb/gal_US_liq */
/* 21 */ 1.1865528425e+03, /* shortton/yd3 */
/* 22 */ 9.9776168662e+01, /* lb/impGal */
/* 23 */ 1.0000000000e-03, /* mg/liter */
/* 24 */ 1.0000000000e-06, /* microgm/liter */
/* 25 */ 1.0000000000e-03, /* mg/dm3 */
/* 26 */ 1.0000000000e-03, /* mg/liter */
/* 27 */ 1.0000000000e-06, /* mg/m3 */
};

static const double si_factors_1_rad_per_s[4] =
{
/*  0 */ 1.7453293000e-02, /* degree/s */
/*  1 */ 6.2831853000e+00, /* rev/s */
/*  2 */ 6.2831853000e+00, /* rev/s */
/*  3 */ 1.0000000000e+00, /* rad/s */
};

static const double si_factors_1_rad_per_m[2] =
{
/*  0 */ 6.2831853000e+00, /* rpm */
/*  1 */ 6.2831853000e+00, /* rpm */
};

static const double si_factors_1_m2_kg_per_s3[26] =
{
/*  0 */ 1.0000000000e+03, /* kw */
/*  1 */ 7.4570000000e+02, /* HP_550_ftlbf */
/*  2 */ 1.1622222222e+03, /* Mcal_therm/hr */
/*  3 */ 2.7777777778e+02, /* MJ/hr */
/*  4 */ 2.9287535111e-01, /* Btu_therm/hr */
/*  5 */ 1.1622222222e+03, /* Mcal_therm/hr */
/*  6 */ 2.7777777778e+02, /* MJ/hr */
/*  7 */ 2.9287535111e-01, /* Btu_therm/hr */
/*  8 */ 7.4570000000e+02, /* HP_550_ftlbf */
/*  9 */ 4.1840000000e+03, /* kcal_therm/s */
/* 10 */ 6.9733333333e+01, /* kcal_therm/min */
/* 11 */ 1.1622222222e+00, /* kcal_therm/hr */
/* 12 */ 4.8425925926e-02, /* kcal_therm/day */
/* 13 */ 4.1840000000e+06, /* Mcal_therm/s */
/* 14 */ 6.9733333333e+04, /* Mcal_therm/min */
/* 15 */ 4.8425925926e+01, /* Mcal_therm/day */
/* 16 */ 1.0000000000e+03, /* kJ/s */
/* 17 */ 1.6666666667e+01, /* kJ/min */
/* 18 */ 2.7777777778e-01, /* kJ/hr */
/* 19 */ 1.1574074074e-02, /* kJ/day */
/* 20 */ 1.0000000000e+06, /* MJ/s */
/* 21 */ 1.6666666667e+04, /* MJ/min */
/* 22 */ 1.1574074074e+01, /* MJ/day */
/* 23 */ 1.0543512640e+03, /* Btu_therm/s */
/* 24 */ 1.7572521067e+01, /* Btu_therm/min */
/* 25 */ 1.2203139630e-02, /* Btu_therm/day */
};

static const double si_factors_1_ppm[5] =
{
/*  0 */ 1.0000000000e+00, /* ppm */
/*  1 */ 1.0000000000e+00, /* ppm */
/*  2 */ 1.0000000000e-03, /* ppb */
/*  3 */ 1.0000000000e+03, /* ppth */
/*  4 */ 1.0000000000e-06, /* ppt */
};

static const double si_factors_1_rad[7] =
{
/*  0 */ 1.7453293000e-02, /* degree */
/*  1 */ 1.0000000000e+00, /* rad */
/*  2 */ 1.7453293000e-02, /* degree */
/*  3 */ 2.9088821667e-04, /* arcmin */
/*  4 */ 4.8481369444e-06, /* arcsec */
/*  5 */ 1.5707963700e-02, /* gon */
/*  6 */ 6.2831853000e+00, /* rev */
};

static const double si_factors_1_s4_A2_per_m2_kg[6] =
{
/*  0 */ 1.0000000000e-12, /* pF */
/*  1 */ 1.0000000000e+00, /* F */
/*  2 */ 1.0000000000e-03, /* mF */
/*  3 */ 1.0000000000e-06, /* microF */
/*  4 */ 1.0000000000e-09, /* nF */
/*  5 */ 1.0000000000e-12, /* pF */
};

static const double si_factors_1_mol[8] =
{
/*  0 */ 4.4615900000e+01, /* SCM */
/*  1 */ 4.4615900000e-03, /* SCL */
/*  2 */ 1.1953100000e+00, /* SCF */
/*  3 */ 1.1953100000e+00, /* SCF */
/*  4 */ 1.0000000000e+00, /* mol */
/*  5 */ 1.0000000000e+03, /* kmol */
/*  6 */ 1.0000000000e-03, /* mmol */
/*  7 */ 1.0000000000e-06, /* micromol */
};

static const double si_factors_1_m2[12] =
{
/*  0 */ 1.0000000000e+00, /* m2 */
/*  1 */ 1.0000000000e+06, /* km2 */
/*  2 */ 1.0000000000e-04, /* cm2 */
/*  3 */ 1.0000000000e-02, /* dm2 */
/*  4 */ 1.0000000000e-06, /* mm2 */
/*  5 */ 1.0000000000e+02, /* are */
/*  6 */ 1.0000000000e+04, /* ha */
/*  7 */ 6.4516000000e-04, /* in2 */
/*  8 */ 9.2903040000e-02, /* ft2 */
/*  9 */ 8.3612736000e-01, /* yd2 */
/* 10 */ 2.5899881103e+06, /* mile2 */
/* 11 */ 2.8316846592e-02, /* ft3/m */
};

static const double si_factors_1_kg_per_m[3] =
{
/*  0 */ 1.0000000000e+00, /* kg/m */
/*  1 */ 1.0000000000e-06, /* mg/m */
/*  2 */ 1.0000000000e-06, /* tex */
};

static const double si_factors_1_kg_per_s2[2] =
{
/*  0 */ 1.0000000000e+00, /* N/m */
/*  1 */ 1.0000000000e-03, /* mN/m */
};

static const double si_factors_1_m2_kg_per_s2_K[2] =
{
/*  0 */ 1.0000000000e+00, /* J/K */
/*  1 */ 1.0000000000e+03, /* kJ/K */
};

static const double si_factors_1_m2_per_s2_K[2] =
{
/*  0 */ 1.0000000000e+00, /* J/kg-K */
/*  1 */ 1.0000000000e+03, /* kJ/kg-K */
};

static const double si_factors_1_m2_per_s2[11] =
{
/*  0 */ 1.0000000000e+00, /* J/kg */
/*  1 */ 1.0000000000e+06, /* MJ/kg */
/*  2 */ 1.0000000000e+03, /* kJ/kg */
/*  3 */ 1.0000000000e+00, /* Gy */
/*  4 */ 1.0000000000e-03, /* mGy */
/*  5 */ 1.0000000000e-02, /* cGy */
/*  6 */ 1.0000000000e+00, /* Sv */
/*  7 */ 1.0000000000e-03, /* mSv */
/*  8 */ 1.0000000000e-02, /* rem */
/*  9 */ 4.1840000000e+03, /* kcal_therm/kg */
/* 10 */ 2.3244466480e+03, /* Btu_therm/lb */
};

static const double si_factors_1_s_A[7] =
{
/*  0 */ 1.0000000000e+00, /* C */
/*  1 */ 1.0000000000e+06, /* MC */
/*  2 */ 1.0000000000e+03, /* kC */
/*  3 */ 1.0000000000e-06, /* microC */
/*  4 */ 1.0000000000e-09, /* nC */
/*  5 */ 1.0000000000e-12, /* pC */
/*  6 */ 3.6000000000e+03, /* A-hr */
};

static const double si_factors_1_s_A_per_m3[6] =
{
/*  0 */ 1.0000000000e+00, /* C/m3 */
/*  1 */ 1.0000000000e+09, /* C/mm3 */
/*  2 */ 1.0000000000e+06, /* C/cm3 */
/*  3 */ 1.0000000000e+03, /* kC/m3 */
/*  4 */ 1.0000000000e-03, /* mC/m3 */
/*  5 */ 1.0000000000e-06, /* microC/m3 */
};

static const double si_factors_1_s_A_per_m2[6] =
{
/*  0 */ 1.0000000000e+00, /* C/m2 */
/*  1 */ 1.0000000000e+06, /* C/mm2 */
/*  2 */ 1.0000000000e+04, /* C/cm2 */
/*  3 */ 1.0000000000e+03, /* kC/m2 */
/*  4 */ 1.0000000000e-03, /* mC/m2 */
/*  5 */ 1.0000000000e-06, /* microC/m2 */
};

static const double si_factors_1_m_kg_per_s3_A[6] =
{
/*  0 */ 1.0000000000e+00, /* V/m */
/*  1 */ 1.0000000000e+06, /* MV/m */
/*  2 */ 1.0000000000e+03, /* kV/m */
/*  3 */ 1.0000000000e+02, /* V/cm */
/*  4 */ 1.0000000000e-03, /* mV/m */
/*  5 */ 1.0000000000e-06, /* microV/m */
};

static const double si_factors_1_s4_A2_per_m3_kg[4] =
{
/*  0 */ 1.0000000000e+00, /* F/m */
/*  1 */ 1.0000000000e-06, /* microF/m */
/*  2 */ 1.0000000000e-09, /* nF/m */
/*  3 */ 1.0000000000e-12, /* pF/m */
};

static const double si_factors_1_A_per_m2[4] =
{
/*  0 */ 1.0000000000e+00, /* A/m2 */
/*  1 */ 1.0000000000e+06, /* MA/m2 */
/*  2 */ 1.0000000000e+04, /* A/cm2 */
/*  3 */ 1.0000000000e+03, /* kA/m2 */
};

static const double si_factors_1_A_per_m[3] =
{
/*  0 */ 1.0000000000e+00, /* A/m */
/*  1 */ 1.0000000000e+03, /* kA/m */
/*  2 */ 1.0000000000e+02, /* A/cm */
};

static const double si_factors_1_kg_per_s2_A[4] =
{
/*  0 */ 1.0000000000e+00, /* T */
/*  1 */ 1.0000000000e-03, /* mT */
/*  2 */ 1.0000000000e-06, /* microT */
/*  3 */ 1.0000000000e-09, /* nT */
};

static const double si_factors_1_m2_kg_per_s2_A[2] =
{
/*  0 */ 1.0000000000e+00, /* Wb */
/*  1 */ 1.0000000000e-03, /* mWb */
};

static const double si_factors_1_m_kg_per_s2_A[2] =
{
/*  0 */ 1.0000000000e+00, /* Wb/m */
/*  1 */ 1.0000000000e+03, /* kWb/m */
};

static const double si_factors_1_m2_kg_per_s2_A2[5] =
{
/*  0 */ 1.0000000000e+00, /* H */
/*  1 */ 1.0000000000e-03, /* mH */
/*  2 */ 1.0000000000e-06, /* microH */
/*  3 */ 1.0000000000e-09, /* nH */
/*  4 */ 1.0000000000e-12, /* pH */
};

static const double si_factors_1_m_kg_per_s2_A2[3] =
{
/*  0 */ 1.0000000000e+00, /* H/m */
/*  1 */ 1.0000000000e-06, /* microH/m */
/*  2 */ 1.0000000000e-09, /* nH/m */
};

static const double si_factors_1_m3_kg_per_s2_A[2] =
{
/*  0 */ 1.0000000000e+00, /* N-m2/A */
/*  1 */ 1.0000000000e+00, /* Wb-m */
};

static const double si_factors_1_s3_A2_per_m2_kg[5] =
{
/*  0 */ 1.0000000000e+00, /* S */
/*  1 */ 1.0000000000e+03, /* kS */
/*  2 */ 1.0000000000e-03, /* mS */
/*  3 */ 1.0000000000e-06, /* microS */
/*  4 */ 1.0000000000e+15, /* PS */
};

static const double si_factors_1_m3_kg_per_s3_A2[8] =
{
/*  0 */ 1.0000000000e+00, /* Ohm-m */
/*  1 */ 1.0000000000e+09, /* GOhm-m */
/*  2 */ 1.0000000000e+06, /* MOhm-m */
/*  3 */ 1.0000000000e+03, /* kOhm-m */
/*  4 */ 1.0000000000e-02, /* Ohm-cm */
/*  5 */ 1.0000000000e-03, /* mOhm-m */
/*  6 */ 1.0000000000e-06, /* microOhm-m */
/*  7 */ 1.0000000000e-09, /* nOhm-m */
};

static const double si_factors_1_kg_per_s3_A2[5] =
{
/*  0 */ 1.0000000000e+00, /* W/m2 */
/*  1 */ 1.0000000000e+00, /* W/m2 */
/*  2 */ 1.0000000000e-03, /* mW/m2 */
/*  3 */ 1.0000000000e-06, /* microW/m2 */
/*  4 */ 1.0000000000e-12, /* pW/m2 */
};

static const double si_factors_1_s_cd_sr[2] =
{
/*  0 */ 1.0000000000e+00, /* lm-s */
/*  1 */ 3.6000000000e+03, /* lm-hr */
};

static const double si_factors_1_cd_sr_per_m2[2] =
{
/*  0 */ 1.0000000000e+00, /* lm/m2 */
/*  1 */ 1.0000000000e+00, /* lx */
};

static const double si_factors_1_mol_per_s[2] =
{
/*  0 */ 1.9921833333e-02, /* SCF/min */
/*  1 */ 3.3203055556e-04, /* SCF/hr */
};

static const double si_factors_1_B[2] =
{
/*  0 */ 1.0000000000e+00, /* B */
/*  1 */ 1.0000000000e-01, /* dB */
};

static const double si_factors_1_kg_per_mol[2] =
{
/*  0 */ 1.0000000000e+00, /* kg/mol */
/*  1 */ 1.0000000000e-03, /* gm/mol */
};

static const double si_factors_1_m3_per_mol[4] =
{
/*  0 */ 1.0000000000e+00, /* m3/mol */
/*  1 */ 1.0000000000e-03, /* dm3/mol */
/*  2 */ 1.0000000000e-06, /* cm3/mol */
/*  3 */ 1.0000000000e-03, /* liter/mol */
};

static const double si_factors_1_m2_kg_per_s2_mol[2] =
{
/*  0 */ 1.0000000000e+00, /* J/mol */
/*  1 */ 1.0000000000e+03, /* kJ/mol */
};

static const double si_factors_1_mol_per_m3[3] =
{
/*  0 */ 1.0000000000e+00, /* mol/m3 */
/*  1 */ 1.0000000000e+03, /* mol/dm3 */
/*  2 */ 1.0000000000e+03, /* mol/liter */
};

static const double si_factors_1_mol_per_kg[2] =
{
/*  0 */ 1.0000000000e+00, /* mol/kg */
/*  1 */ 1.0000000000e-03, /* mmol/kg */
};

static const double si_factors_1_per_kg_s[3] =
{
/*  0 */ 1.0000000000e+00, /* Bq/kg */
/*  1 */ 1.0000000000e+03, /* kBq/kg */
/*  2 */ 1.0000000000e+06, /* MBq/kg */
};

static const double si_factors_1_s_A_per_kg[3] =
{
/*  0 */ 1.0000000000e+00, /* C/kg */
/*  1 */ 1.0000000000e-03, /* mC/kg */
/*  2 */ 2.5800000000e-04, /* R */
};

static const double si_factors_1_m2_kg_per_s3_K_A[2] =
{
/*  0 */ 1.0000000000e+00, /* V/K */
/*  1 */ 1.0000000000e-03, /* mV/K */
};

static const double *si_factors[60] =
{
/*  0 */ si_factors_1_kg_per_m_s2,
/*  1 */ si_factors_1_kg_per_m2,
/*  2 */ si_factors_1_m3_per_s,
/*  3 */ si_factors_1_m_per_s,
/*  4 */ si_factors_1_m2_kg_per_s3_A,
/*  5 */ si_factors_1_m2_kg_per_s3_A2,
/*  6 */ si_factors_1_per_s,
/*  7 */ si_factors_1_A,
/*  8 */ si_factors_1_m3,
/*  9 */ si_factors_1_m,
/* 10 */ si_factors_1_s,
/* 11 */ si_factors_1_m2_per_s,
/* 12 */ si_factors_1_kg_per_m_s,
/* 13 */ si_factors_1_kg,
/* 14 */ si_factors_1_s3_A2_per_m3_kg,
/* 15 */ si_factors_1_m_kg_per_s2,
/* 16 */ si_factors_1_m2_kg_per_s2,
/* 17 */ si_factors_1_kg_per_s,
/* 18 */ si_factors_1_kg_per_m3,
/* 19 */ si_factors_1_rad_per_s,
/* 20 */ si_factors_1_rad_per_m,
/* 21 */ si_factors_1_m2_kg_per_s3,
/* 22 */ si_factors_1_ppm,
/* 23 */ si_factors_1_rad,
/* 24 */ si_factors_1_s4_A2_per_m2_kg,
/* 25 */ si_factors_1_mol,
/* 26 */ si_factors_1_m2,
/* 27 */ si_factors_1_kg_per_m,
/* 28 */ si_factors_1_kg_per_s2,
/* 29 */ si_factors_1_m2_kg_per_s2_K,
/* 30 */ si_factors_1_m2_per_s2_K,
/* 31 */ si_factors_1_m2_per_s2,
/* 32 */ si_factors_1_s_A,
/* 33 */ si_factors_1_s_A_per_m3,
/* 34 */ si_factors_1_s_A_per_m2,
/* 35 */ si_factors_1_m_kg_per_s3_A,
/* 36 */ si_factors_1_s4_A2_per_m3_kg,
/* 37 */ si_factors_1_A_per_m2,
/* 38 */ si_factors_1_A_per_m,
/* 39 */ si_factors_1_kg_per_s2_A,
/* 40 */ si_factors_1_m2_kg_per_s2_A,
/* 41 */ si_factors_1_m_kg_per_s2_A,
/* 42 */ si_factors_1_m2_kg_per_s2_A2,
/* 43 */ si_factors_1_m_kg_per_s2_A2,
/* 44 */ si_factors_1_m3_kg_per_s2_A,
/* 45 */ si_factors_1_s3_A2_per_m2_kg,
/* 46 */ si_factors_1_m3_kg_per_s3_A2,
/* 47 */ si_factors_1_kg_per_s3_A2,
/* 48 */ si_factors_1_s_cd_sr,
/* 49 */ si_factors_1_cd_sr_per_m2,
/* 50 */ si_factors_1_mol_per_s,
/* 51 */ si_factors_1_B,
/* 52 */ si_factors_1_kg_per_mol,
/* 53 */ si_factors_1_m3_per_mol,
/* 54 */ si_factors_1_m2_kg_per_s2_mol,
/* 55 */ si_factors_1_mol_per_m3,
/* 56 */ si_factors_1_mol_per_kg,
/* 57 */ si_factors_1_per_kg_s,
/* 58 */ si_factors_1_s_A_per_kg,
/* 59 */ si_factors_1_m2_kg_per_s3_K_A,
};

static const struct
{
	void (*const *normalize)(class SMachine &);
	void (*const *denormalize)(class SMachine &);
}
si_rules[1] =
{
	{ si_rules_normalize_1_K, si_rules_denormalize_1_K },
};

struct unit_entry
{
	short key;
	unsigned char major;
	unsigned char minor;
};
static const struct unit_entry unit_lookup_table[656] =
{
	{     1,   0,   0 },
	{     2,   0,   1 },
	{     5,   0,   2 },
	{     6,   0,   3 },
	{     7,   0,   4 },
	{     8,   0,   5 },
	{     9,   1,   0 },
	{    10,   1,   1 },
	{    11,   0,   6 },
	{    12,   0,   7 },
	{    13,   0,   8 },
	{    14,   0,   9 },
	{    15,   2,   0 },
	{    16,   2,   1 },
	{    17,   2,   2 },
	{    18,   2,   3 },
	{    19,   2,   4 },
	{    20,   3,   0 },
	{    21,   3,   1 },
	{    22,   2,   5 },
	{    23,   2,   6 },
	{    24,   2,   7 },
	{    25,   2,   8 },
	{    26,   2,   9 },
	{    27,   2,  10 },
	{    28,   2,  11 },
	{    29,   2,  12 },
	{    30,   2,  13 },
	{    31,   2,  14 },
	{    32, 128,   0 },
	{    33, 128,   1 },
	{    34, 128,   2 },
	{    35, 128,   3 },
	{    36,   4,   0 },
	{    37,   5,   0 },
	{    38,   6,   0 },
	{    39,   7,   0 },
	{    40,   8,   0 },
	{    41,   8,   1 },
	{    42,   8,   2 },
	{    43,   8,   3 },
	{    44,   9,   0 },
	{    45,   9,   1 },
	{    46,   8,   4 },
	{    47,   9,   2 },
	{    48,   9,   3 },
	{    49,   9,   4 },
	{    50,  10,   0 },
	{    51,  10,   1 },
	{    52,  10,   2 },
	{    53,  10,   3 },
	{    54,  11,   0 },
	{    55,  12,   0 },
	{    56,  10,   4 },
	{    58,   4,   1 },
	{    60,  13,   0 },
	{    61,  13,   1 },
	{    62,  13,   2 },
	{    63,  13,   3 },
	{    64,  13,   4 },
	{    65,  13,   5 },
	{    66,  14,   0 },
	{    67,  14,   1 },
	{    68,  15,   0 },
	{    69,  16,   0 },
	{    70,  17,   0 },
	{    71,  17,   1 },
	{    72,  17,   2 },
	{    73,  17,   3 },
	{    74,  17,   4 },
	{    75,  17,   5 },
	{    76,  17,   6 },
	{    77,  17,   7 },
	{    78,  17,   8 },
	{    79,  17,   9 },
	{    80,  17,  10 },
	{    81,  17,  11 },
	{    82,  17,  12 },
	{    83,  17,  13 },
	{    84,  17,  14 },
	{    85,  17,  15 },
	{    86,  17,  16 },
	{    87,  17,  17 },
	{    88,  17,  18 },
	{    89,  16,   1 },
	{    91,  18,   0 },
	{    92,  18,   1 },
	{    93,  18,   2 },
	{    94,  18,   3 },
	{    95,  18,   4 },
	{    96,  18,   5 },
	{    97,  18,   6 },
	{    98,  18,   7 },
	{    99,  18,   8 },
	{   110,   8,   5 },
	{   111,   8,   6 },
	{   112,   8,   7 },
	{   113,   8,   8 },
	{   114,   3,   2 },
	{   115,   3,   3 },
	{   116,   3,   4 },
	{   117,  19,   0 },
	{   118,  19,   1 },
	{   119,  20,   0 },
	{   120,   3,   5 },
	{   121,   2,  15 },
	{   122,   2,  16 },
	{   123,   2,  17 },
	{   124,   8,   9 },
	{   125,  13,   6 },
	{   126,  16,   2 },
	{   127,  21,   0 },
	{   128,  16,   3 },
	{   129,  21,   1 },
	{   130,   2,  18 },
	{   131,   2,  19 },
	{   132,   2,  20 },
	{   133,   2,  21 },
	{   134,   2,  22 },
	{   135,   2,  23 },
	{   136,   2,  24 },
	{   137,   2,  25 },
	{   138,   2,  26 },
	{   139,  22,   0 },
	{   140,  21,   2 },
	{   141,  21,   3 },
	{   142,  21,   4 },
	{   143,  23,   0 },
	{   153,  24,   0 },
	{   162,  16,   4 },
	{   163,   5,   1 },
	{   164,  16,   5 },
	{   165,  16,   6 },
	{   166,  25,   0 },
	{   167,  25,   1 },
	{   168,  25,   2 },
	{   235,   2,  27 },
	{   236,   8,  10 },
	{   237,   0,  10 },
	{   238,   0,  11 },
	{   239,   0,  12 },
	{  1000, 128,   4 },
	{  1001, 128,   5 },
	{  1002, 128,   6 },
	{  1003, 128,   7 },
	{  1004,  23,   1 },
	{  1005,  23,   2 },
	{  1006,  23,   3 },
	{  1007,  23,   4 },
	{  1008,  23,   5 },
	{  1009,  23,   6 },
	{  1010,   9,   5 },
	{  1011,   9,   6 },
	{  1012,   9,   7 },
	{  1013,   9,   8 },
	{  1014,   9,   9 },
	{  1015,   9,  10 },
	{  1016,   9,  11 },
	{  1017,   9,  12 },
	{  1018,   9,  13 },
	{  1019,   9,  14 },
	{  1020,   9,  15 },
	{  1021,   9,  16 },
	{  1022,   9,  17 },
	{  1023,  26,   0 },
	{  1024,  26,   1 },
	{  1025,  26,   2 },
	{  1026,  26,   3 },
	{  1027,  26,   4 },
	{  1028,  26,   5 },
	{  1029,  26,   6 },
	{  1030,  26,   7 },
	{  1031,  26,   8 },
	{  1032,  26,   9 },
	{  1033,  26,  10 },
	{  1034,   8,  11 },
	{  1035,   8,  12 },
	{  1036,   8,  13 },
	{  1037,   8,  14 },
	{  1038,   8,  15 },
	{  1039,   8,  16 },
	{  1040,   8,  17 },
	{  1041,   8,  18 },
	{  1042,   8,  19 },
	{  1043,   8,  20 },
	{  1044,   8,  21 },
	{  1045,   8,  22 },
	{  1046,   8,  23 },
	{  1047,   8,  24 },
	{  1048,   8,  25 },
	{  1049,   8,  26 },
	{  1050,   8,  27 },
	{  1051,   8,  28 },
	{  1052,   8,  29 },
	{  1053,  25,   3 },
	{  1054,  10,   5 },
	{  1056,  10,   6 },
	{  1057,  10,   7 },
	{  1058,  10,   8 },
	{  1059,  10,   9 },
	{  1060,  10,  10 },
	{  1061,   3,   6 },
	{  1062,   3,   7 },
	{  1063,   3,   8 },
	{  1064,   3,   9 },
	{  1065,   3,  10 },
	{  1066,   3,  11 },
	{  1067,   3,  12 },
	{  1068,   3,  13 },
	{  1069,   3,  14 },
	{  1070,   3,  15 },
	{  1071,   3,  16 },
	{  1072,   3,  17 },
	{  1073,   3,  18 },
	{  1074,   3,  19 },
	{  1075,   3,  20 },
	{  1077,   6,   1 },
	{  1078,   6,   2 },
	{  1079,   6,   3 },
	{  1080,   6,   4 },
	{  1081,   6,   5 },
	{  1082,   6,   6 },
	{  1083,   6,   7 },
	{  1084,  19,   2 },
	{  1085,  20,   1 },
	{  1086,  19,   3 },
	{  1088,  13,   7 },
	{  1089,  13,   8 },
	{  1090,  13,   9 },
	{  1091,  13,  10 },
	{  1092,  13,  11 },
	{  1093,  13,  12 },
	{  1094,  13,  13 },
	{  1095,  13,  14 },
	{  1096,  13,  15 },
	{  1097,  18,   9 },
	{  1098,  18,  10 },
	{  1099,  18,  11 },
	{  1100,  18,  12 },
	{  1101,  18,  13 },
	{  1102,  18,  14 },
	{  1103,  18,  15 },
	{  1104,  18,  16 },
	{  1105,  18,  17 },
	{  1106,  18,  18 },
	{  1107,  18,  19 },
	{  1108,  18,  20 },
	{  1109,  18,  21 },
	{  1115,  27,   0 },
	{  1116,  27,   1 },
	{  1117,  27,   2 },
	{  1120,  15,   1 },
	{  1121,  15,   2 },
	{  1122,  15,   3 },
	{  1123,  15,   4 },
	{  1124,  15,   5 },
	{  1126,  16,   7 },
	{  1127,  16,   8 },
	{  1128,  16,   9 },
	{  1129,  16,  10 },
	{  1130,   0,  13 },
	{  1131,   0,  14 },
	{  1132,   0,  15 },
	{  1133,   0,  16 },
	{  1134,   0,  17 },
	{  1135,   0,  18 },
	{  1136,   0,  19 },
	{  1137,   0,  20 },
	{  1138,   0,  21 },
	{  1139,   0,  22 },
	{  1140,   0,  23 },
	{  1141,   0,  24 },
	{  1144,   1,   2 },
	{  1145,   1,   3 },
	{  1147,   0,  25 },
	{  1148,   0,  26 },
	{  1150,   0,  27 },
	{  1156,   0,  28 },
	{  1158,   0,  29 },
	{  1159,  12,   1 },
	{  1160,  11,   1 },
	{  1161,  12,   2 },
	{  1162,  12,   3 },
	{  1163,  11,   2 },
	{  1164,  11,   3 },
	{  1165,  28,   0 },
	{  1166,  28,   1 },
	{  1167,  16,  11 },
	{  1168,  16,  12 },
	{  1169,  16,  13 },
	{  1170,  16,  14 },
	{  1171,  16,  15 },
	{  1172,  16,  16 },
	{  1173,  16,  17 },
	{  1174,  16,  18 },
	{  1175,  16,  19 },
	{  1176,  16,  20 },
	{  1177,  16,  21 },
	{  1178,  16,  22 },
	{  1179,  16,  23 },
	{  1180,  16,  24 },
	{  1181,  16,  25 },
	{  1182,  16,  26 },
	{  1183,  16,  27 },
	{  1184,  16,  28 },
	{  1186,   5,   2 },
	{  1187,   5,   3 },
	{  1188,   5,   4 },
	{  1189,   5,   5 },
	{  1190,   5,   6 },
	{  1191,   5,   7 },
	{  1192,   5,   8 },
	{  1193,   5,   9 },
	{  1194,   5,  10 },
	{  1195,  21,   5 },
	{  1196,  21,   6 },
	{  1197,  21,   7 },
	{  1198,  21,   8 },
	{  1202,  29,   0 },
	{  1203,  29,   1 },
	{  1204,  30,   0 },
	{  1205,  30,   1 },
	{  1206,  31,   0 },
	{  1207,  31,   1 },
	{  1208,  31,   2 },
	{  1209,   7,   1 },
	{  1210,   7,   2 },
	{  1211,   7,   3 },
	{  1212,   7,   4 },
	{  1213,   7,   5 },
	{  1214,   7,   6 },
	{  1215,  32,   0 },
	{  1216,  32,   1 },
	{  1217,  32,   2 },
	{  1218,  32,   3 },
	{  1219,  32,   4 },
	{  1220,  32,   5 },
	{  1221,  32,   6 },
	{  1222,  33,   0 },
	{  1223,  33,   1 },
	{  1224,  33,   2 },
	{  1225,  33,   3 },
	{  1226,  33,   4 },
	{  1227,  33,   5 },
	{  1228,  34,   0 },
	{  1229,  34,   1 },
	{  1230,  34,   2 },
	{  1231,  34,   3 },
	{  1232,  34,   4 },
	{  1233,  34,   5 },
	{  1234,  35,   0 },
	{  1235,  35,   1 },
	{  1236,  35,   2 },
	{  1237,  35,   3 },
	{  1238,  35,   4 },
	{  1239,  35,   5 },
	{  1240,   4,   2 },
	{  1241,   4,   3 },
	{  1242,   4,   4 },
	{  1243,   4,   5 },
	{  1244,   4,   6 },
	{  1245,  24,   1 },
	{  1246,  24,   2 },
	{  1247,  24,   3 },
	{  1248,  24,   4 },
	{  1249,  24,   5 },
	{  1250,  36,   0 },
	{  1251,  36,   1 },
	{  1252,  36,   2 },
	{  1253,  36,   3 },
	{  1255,  37,   0 },
	{  1256,  37,   1 },
	{  1257,  37,   2 },
	{  1258,  37,   3 },
	{  1259,  38,   0 },
	{  1260,  38,   1 },
	{  1261,  38,   2 },
	{  1262,  39,   0 },
	{  1263,  39,   1 },
	{  1264,  39,   2 },
	{  1265,  39,   3 },
	{  1266,  40,   0 },
	{  1267,  40,   1 },
	{  1268,  41,   0 },
	{  1269,  41,   1 },
	{  1270,  42,   0 },
	{  1271,  42,   1 },
	{  1272,  42,   2 },
	{  1273,  42,   3 },
	{  1274,  42,   4 },
	{  1275,  43,   0 },
	{  1276,  43,   1 },
	{  1277,  43,   2 },
	{  1279,  44,   0 },
	{  1280,  44,   1 },
	{  1281,   5,  11 },
	{  1282,   5,  12 },
	{  1283,   5,  13 },
	{  1284,   5,  14 },
	{  1285,   5,  15 },
	{  1286,   5,  16 },
	{  1287,  45,   0 },
	{  1288,  45,   1 },
	{  1289,  45,   2 },
	{  1290,  45,   3 },
	{  1291,  46,   0 },
	{  1292,  46,   1 },
	{  1293,  46,   2 },
	{  1294,  46,   3 },
	{  1295,  46,   4 },
	{  1296,  46,   5 },
	{  1297,  46,   6 },
	{  1298,  46,   7 },
	{  1299,  14,   2 },
	{  1300,  14,   3 },
	{  1301,  14,   4 },
	{  1302,  14,   5 },
	{  1303,  14,   6 },
	{  1308,  47,   0 },
	{  1310,  48,   0 },
	{  1311,  48,   1 },
	{  1312,  49,   0 },
	{  1314,  49,   1 },
	{  1318,  17,  19 },
	{  1319,  17,  20 },
	{  1320,  17,  21 },
	{  1321,  17,  22 },
	{  1322,  17,  23 },
	{  1323,  17,  24 },
	{  1324,  17,  25 },
	{  1325,  17,  26 },
	{  1326,  17,  27 },
	{  1327,  17,  28 },
	{  1328,  17,  29 },
	{  1329,  17,  30 },
	{  1330,  17,  31 },
	{  1331,  17,  32 },
	{  1332,  17,  33 },
	{  1333,  17,  34 },
	{  1334,  17,  35 },
	{  1335,  17,  36 },
	{  1336,  17,  37 },
	{  1337,  17,  38 },
	{  1338,  17,  39 },
	{  1339,  17,  40 },
	{  1340,  17,  41 },
	{  1341,  17,  42 },
	{  1347,   2,  28 },
	{  1348,   2,  29 },
	{  1349,   2,  30 },
	{  1350,   2,  31 },
	{  1351,   2,  32 },
	{  1352,   2,  33 },
	{  1353,   2,  34 },
	{  1354,   2,  35 },
	{  1355,   2,  36 },
	{  1356,   2,  37 },
	{  1357,  26,  11 },
	{  1358,   2,  38 },
	{  1359,   2,  39 },
	{  1360,  50,   0 },
	{  1361,  50,   1 },
	{  1362,   2,  40 },
	{  1363,   2,  41 },
	{  1364,   2,  42 },
	{  1365,   2,  43 },
	{  1366,   2,  44 },
	{  1367,   2,  45 },
	{  1368,   2,  46 },
	{  1369,   2,  47 },
	{  1370,   2,  48 },
	{  1371,   2,  49 },
	{  1372,   2,  50 },
	{  1373,   2,  51 },
	{  1374,   2,  52 },
	{  1375,  47,   1 },
	{  1376,  47,   2 },
	{  1377,  47,   3 },
	{  1378,  47,   4 },
	{  1380,  17,  43 },
	{  1382,  51,   0 },
	{  1383,  51,   1 },
	{  1384,  25,   4 },
	{  1385,  25,   5 },
	{  1386,  25,   6 },
	{  1387,  25,   7 },
	{  1388,  52,   0 },
	{  1389,  52,   1 },
	{  1390,  53,   0 },
	{  1391,  53,   1 },
	{  1392,  53,   2 },
	{  1393,  53,   3 },
	{  1394,  54,   0 },
	{  1395,  54,   1 },
	{  1397,  55,   0 },
	{  1398,  55,   1 },
	{  1399,  55,   2 },
	{  1400,  56,   0 },
	{  1401,  56,   1 },
	{  1402,   6,   8 },
	{  1403,   6,   9 },
	{  1404,   6,  10 },
	{  1405,  57,   0 },
	{  1406,  57,   1 },
	{  1407,  57,   2 },
	{  1408,  31,   3 },
	{  1409,  31,   4 },
	{  1410,  31,   5 },
	{  1411,  31,   6 },
	{  1412,  31,   7 },
	{  1413,  31,   8 },
	{  1414,  58,   0 },
	{  1415,  58,   1 },
	{  1416,  58,   2 },
	{  1420,  59,   0 },
	{  1421,  59,   1 },
	{  1423,  22,   1 },
	{  1424,  22,   2 },
	{  1425,  22,   3 },
	{  1430,  18,  22 },
	{  1431,  21,   9 },
	{  1432,  21,  10 },
	{  1433,  21,  11 },
	{  1434,  21,  12 },
	{  1435,  21,  13 },
	{  1436,  21,  14 },
	{  1437,  21,  15 },
	{  1438,  21,  16 },
	{  1439,  21,  17 },
	{  1440,  21,  18 },
	{  1441,  21,  19 },
	{  1442,  21,  20 },
	{  1443,  21,  21 },
	{  1444,  21,  22 },
	{  1445,  21,  23 },
	{  1446,  21,  24 },
	{  1447,  21,  25 },
	{  1448,   2,  53 },
	{  1449,   2,  54 },
	{  1450,   2,  55 },
	{  1451,   2,  56 },
	{  1452,   2,  57 },
	{  1453,   2,  58 },
	{  1454,   2,  59 },
	{  1455,   2,  60 },
	{  1456,   2,  61 },
	{  1457,   2,  62 },
	{  1458,   2,  63 },
	{  1459,   2,  64 },
	{  1460,   2,  65 },
	{  1461,   2,  66 },
	{  1462,   2,  67 },
	{  1463,   2,  68 },
	{  1464,   2,  69 },
	{  1465,   2,  70 },
	{  1466,   2,  71 },
	{  1467,   2,  72 },
	{  1468,   2,  73 },
	{  1469,   2,  74 },
	{  1470,   2,  75 },
	{  1471,   2,  76 },
	{  1472,   2,  77 },
	{  1473,   2,  78 },
	{  1474,   2,  79 },
	{  1475,   2,  80 },
	{  1476,   2,  81 },
	{  1477,   2,  82 },
	{  1478,   2,  83 },
	{  1479,   2,  84 },
	{  1480,   2,  85 },
	{  1481,   2,  86 },
	{  1482,   2,  87 },
	{  1483,   2,  88 },
	{  1484,   2,  89 },
	{  1485,   2,  90 },
	{  1486,   2,  91 },
	{  1487,   2,  92 },
	{  1488,   2,  93 },
	{  1489,   2,  94 },
	{  1490,   2,  95 },
	{  1491,   2,  96 },
	{  1492,   2,  97 },
	{  1493,   2,  98 },
	{  1494,   2,  99 },
	{  1495,   2, 100 },
	{  1496,   2, 101 },
	{  1497,   2, 102 },
	{  1498,   2, 103 },
	{  1499,   2, 104 },
	{  1500,   2, 105 },
	{  1501,   2, 106 },
	{  1502,   2, 107 },
	{  1503,   2, 108 },
	{  1504,   2, 109 },
	{  1505,   2, 110 },
	{  1506,   2, 111 },
	{  1507,   2, 112 },
	{  1508,   2, 113 },
	{  1509,   2, 114 },
	{  1510,   2, 115 },
	{  1511,   2, 116 },
	{  1512,   2, 117 },
	{  1513,   2, 118 },
	{  1514,   2, 119 },
	{  1515,  31,   9 },
	{  1516,  31,  10 },
	{  1517,   8,  30 },
	{  1518,   2, 120 },
	{  1519,   2, 121 },
	{  1520,   2, 122 },
	{  1551,  14,   7 },
	{  1552,  14,   8 },
	{  1553,  14,   9 },
	{  1554,  14,  10 },
	{  1558,  18,  23 },
	{  1559,  18,  24 },
	{  1563,   2, 123 },
	{  1564,  18,  25 },
	{  1565,  18,  26 },
	{  1566,  18,  27 },
	{  1567,  13,  16 },
	{  1568,  13,  17 },
	{  1569,  13,  18 },
	{  1570,   8,  31 },
	{  1571,   8,  32 },
	{  1572,   8,  33 },
	{  1577,   2, 124 },
	{  1578,   2, 125 },
	{  1579,   2, 126 },
	{  1580,   2, 127 },
	{  1581,   2, 128 },
	{  1582,   2, 129 },
	{  1583,   2, 130 },
	{  1584,   2, 131 },
	{  1585,   2, 132 },
	{  1586,   2, 133 },
	{  1587,   2, 134 },
	{  1606,  17,  44 },
	{  1607,  17,  45 },
	{  1608,  17,  46 },
	{  1609,  17,  47 },
	{  1631,  45,   4 },
	{  1632,  22,   4 },
	{  1633,   2, 135 },
	{  1634,   2, 136 },
	{  1635,   2, 137 },
	{  1636,   2, 138 },
	{  1637,   2, 139 },
	{  1638,   2, 140 },
	{  1639,   2, 141 },
	{  1640,   2, 142 },
	{  1641,   8,  34 },
	{  1642,   2, 143 },
	{  1643,   2, 144 },
	{  1644,   2, 145 },
	{  1645,   2, 146 },
};

/* conversion tables: 60 */
/* total number of unit conversions: 656 */
/* number of double entries: 174 */

static const struct unit_entry *
lookup_unit(int key)
{
	const struct unit_entry *low = unit_lookup_table;
	const struct unit_entry *high = unit_lookup_table - 1
					+ sizeof(unit_lookup_table) / sizeof(unit_lookup_table[0]);
	
	while (low <= high)
	{
		const struct unit_entry *mid = low + (high - low) / 2;
		
		if (mid->key == key)
			return mid;
		
		if (mid->key < key)
			low = mid + 1;
		else
			high = mid - 1;
	}
	
	return 0;
}

static void
unit_conversion_factor(operand_ptr vars, bool valid, double factor)
{
	while (vars)
	{
		if (valid)
		{
			try
			{
				double v;
				
				v = *vars;
				v *= factor;
				
				float64_value v_op(v);
				(*vars) = v_op;
			}
			catch (const RuntimeError &)
			{
				vars->status() = operand::VALUE_STATUS_NOT_CONVERTED;
			}
		}
		else
		{
			/* unit conversion not
			 * possible
			 */
			vars->status() = operand::VALUE_STATUS_NOT_CONVERTED;
		}
		
		vars = vars.next();
	}
}

static void
unit_conversion_rule(operand_ptr vars,
		      void (*normalize)(class SMachine &),
		      void (*denormalize)(class SMachine &))
{
	assert(normalize && denormalize);
	
	class SMachine src_sm;
	class SMachine dst_sm;
	
	(*normalize)(src_sm);
	(*denormalize)(dst_sm);
	
	while (vars)
	{
		try
		{
			double v = *vars;
			
			/* normalize */
			{
				class Context c(src_sm.size());
				
				c.ret = new float64Type(v);
				src_sm.execute(c);
				v = c.ret;
			}
			
			/* denormalize */
			{
				class Context c(dst_sm.size());
				
				c.ret = new float64Type(v);
				dst_sm.execute(c);
				v = c.ret;
			}
			
			float64_value v_op(v);
			*vars = v_op;
		}
		catch (const std::exception &)
		{
			vars->status() = operand::VALUE_STATUS_NOT_CONVERTED;
		}
		
		vars = vars.next();
	}
}

void
unit_conversion(int src, int dst, operand_ptr vars)
{
	const struct unit_entry *entry_src;
	const struct unit_entry *entry_dst;
	bool done = false;
	
	entry_src = lookup_unit(src);
	if (entry_src)
	{
		entry_dst = lookup_unit(dst);
		if (entry_dst)
		{
			if (entry_src->major == entry_dst->major)
			{
				if (entry_src->major > 127)
				{
					const int index = entry_src->major - 128;
					void (*normalize)(class SMachine &);
					void (*denormalize)(class SMachine &);
					
					normalize = si_rules[index].normalize[entry_src->minor];
					denormalize = si_rules[index].denormalize[entry_dst->minor];
					
					unit_conversion_rule(vars, normalize, denormalize);
					done = true;
				}
				else
				{
					const int index = entry_src->major;
					const double *table = si_factors[index];
					double factor;
					
					factor = table[entry_src->minor];
					factor /= table[entry_dst->minor];
					
					unit_conversion_factor(vars, true, factor);
					done = true;
				}
			}
		}
	}
	
	if (!done)
		unit_conversion_factor(vars, false, 1.0);
}


} /* namespace edd */
