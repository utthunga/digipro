
/* 
 * $Id: edd_values_time.h,v 1.13 2008/09/29 21:41:49 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 *               2011-2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the date and time values.
 */

#ifndef _edd_values_time_h
#define _edd_values_time_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
# pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <string>

// my lib

// own
#include "edd_operand_interface.h"


namespace edd
{

/**
 * EDD date_and_time value.
 */
class date_and_time_value : public operand
{
protected:
	const size_t with_time;
	std::string value;

public:
	/** \name Constructor/destructor. */
	//! \{
	date_and_time_value(bool with_time);
	date_and_time_value(bool with_time, const std::string &v);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator std::string() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}
};

/**
 * EDD date_and_time variable.
 */
class date_and_time_lvalue : public date_and_time_value
{
protected:
	class status mystatus;
	virtual void assign(const std::string &v, bool strict = false);

public:
	/** \name Constructor/destructor. */
	//! \{
	date_and_time_lvalue(bool with_time, value_status_t = VALUE_STATUS_NONE);
	date_and_time_lvalue(bool with_time, const std::string &v, value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}
};

/**
 * EDD duration value.
 */
class duration_time_value : public operand
{
protected:
	const bool is_time;
	std::string value;

	sys::uint32 get_ms(void) const;

public:
	/** \name Constructor/destructor. */
	//! \{
	duration_time_value(bool time);
	duration_time_value(bool time, const std::string &v);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//std::string pretty_print(void) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator std::string() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}
};

/**
 * EDD duration variable.
 */
class duration_time_lvalue : public duration_time_value
{
protected:
	class status mystatus;
	virtual void assign(const std::string &v, bool strict = false);

public:
	/** \name Constructor/destructor. */
	//! \{
	duration_time_lvalue(bool time, value_status_t = VALUE_STATUS_NONE);
	duration_time_lvalue(bool time, const std::string &v, value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}
};

/**
 * EDD time value.
 */
class time_value_value : public operand
{
protected:
	const size_t with_days;
	std::string value;
	class VARIABLE_TYPE_DATE_TIME_tree *type;

	sys::int64 get_value(void) const;


public:
	/** \name Constructor/destructor. */
	//! \{
	time_value_value(bool with_days, class VARIABLE_TYPE_DATE_TIME_tree *type);
	time_value_value(bool with_days, class VARIABLE_TYPE_DATE_TIME_tree *type, const std::string &v);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//std::string pretty_print(void) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator uint32() const;
	            operator std::string() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}
};

/**
 * EDD duration variable.
 */
class time_value_lvalue : public time_value_value
{
protected:
	class status mystatus;
	virtual void assign(const std::string &v, bool strict = false);

public:
	/** \name Constructor/destructor. */
	//! \{
	time_value_lvalue(bool with_days, class VARIABLE_TYPE_DATE_TIME_tree *type, value_status_t = VALUE_STATUS_NONE);
	time_value_lvalue(bool with_days, class VARIABLE_TYPE_DATE_TIME_tree *type, const std::string &v, value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}
};

} /* namespace edd */

#endif /* _edd_values_time_h */
