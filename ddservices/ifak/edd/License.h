
/* 
 * $Id: License.h,v 1.5 2009/05/19 12:35:33 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _License_h
#define _License_h

// C stdlib

// C++ stdlib
#include <string>

// my lib

// own


/**
 * Definition of the abstract interface of the license checker.
 * If no license is available, the tool runs in DEMO mode.
 */
class License
{
public:
	/** \name Construct/Destructor */
	//! \{
	virtual ~License() { }
	//! \}

	/**
	 * Asks for a license for the checker command line tool.
	 * \retval  true - checker has a license
	 */
	virtual bool have_checker_license() = 0;

	/**
	 * Asks for a license for the workbench tool.
	 * \retval  true - workbench has a license
	 */
	virtual bool have_workbench_license() = 0;

	/**
	 * Asks for a license for communication.
	 * The communication type is specified as argument.
	 * Currently defined profiles are:
	 * - FIELDBUS
	 * - HART
	 * - PROFIBUS
	 * - PROFINET
	 * .
	 * \param profile  The communication profile.
	 * \retval  true - communication has a license
	 */
	virtual bool have_comm_license(int profile) = 0;

	/**
	 * Provides an error message of the last action with the license
	 * manager. Should only be called after have_checker_license() or
	 * have_workbench_license() returned false.
	 * \retval std::string - error message in 7bit ASCII
	 */
	virtual std::string get_error_msg() const = 0;

	/**
	 * Provides an error code of the last action with the license
	 * manager. Should only be called after have_checker_license() or
	 * have_workbench_license() returned false.
	 * \retval long - error code
	 */
	virtual long get_error_code() const = 0;

	/**
	 * provides a message about the expiration info of the license.
	 * Should only be called after have_checker_license() or
	 * have_workbench_license() returned true.
	 * \retval std::string - expiration info in 7bit ASCII
	 */
	virtual std::string get_expiration_info() const = 0;
};

#endif /* _License_h */
