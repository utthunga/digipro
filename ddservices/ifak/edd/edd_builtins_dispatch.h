
/* 
 * $Id: edd_builtins_dispatch.h,v 1.15 2008/09/29 21:41:49 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_builtins_dispatch_h
#define _edd_builtins_dispatch_h

// C stdlib

// C++ stdlib
#include <string>

// my lib

// own header
#include "cocktail_types.h"
#include "edd_types.h"
#include "edd_operand_interface.h"


namespace edd
{

typedef int8		edd_int08;
typedef int16		edd_int16;
typedef int32		edd_int32;

typedef uint8		edd_uint08;
typedef uint16		edd_uint16;
typedef uint32		edd_uint32;

typedef float32		edd_real32;
typedef float64		edd_real64;

typedef std::string	edd_string;


class edd_int08_opmap
{
public:
	edd_int08_opmap(operand_ptr o);
	
	     operator edd_int08() const;
	void operator=(edd_int08 d);
	
private:
	operand_ptr op;
};

class edd_int16_opmap
{
public:
	edd_int16_opmap(operand_ptr o);
	
	     operator edd_int16() const;
	void operator=(edd_int16 d);
	
private:
	operand_ptr op;
};

class edd_int32_opmap
{
public:
	edd_int32_opmap(operand_ptr o);
	
	     operator edd_int32() const;
	void operator=(edd_int32 d);
	
private:
	operand_ptr op;
};


class edd_uint08_opmap
{
public:
	edd_uint08_opmap(operand_ptr o);
	
	     operator edd_uint08() const;
	void operator=(edd_uint08 d);
	
private:
	operand_ptr op;
};

class edd_uint16_opmap
{
public:
	edd_uint16_opmap(operand_ptr o);
	
	     operator edd_uint16() const;
	void operator=(edd_uint16 d);
	
private:
	operand_ptr op;
};

class edd_uint32_opmap
{
public:
	edd_uint32_opmap(operand_ptr o);
	
	     operator edd_uint32() const;
	void operator=(edd_uint32 d);
	
private:
	operand_ptr op;
};


class edd_real32_opmap
{
public:
	edd_real32_opmap(operand_ptr o);
	
	     operator edd_real32() const;
	void operator=(edd_real32 d);
	
private:
	operand_ptr op;
};

class edd_real64_opmap
{
public:
	edd_real64_opmap(operand_ptr o);
	
	     operator edd_real64() const;
	void operator=(edd_real64 d);
	
private:
	operand_ptr op;
};

class edd_string_opmap
{
public:
	edd_string_opmap(operand_ptr o);
	
	     operator edd_string() const;
	void operator=(const edd_string &d);
	
private:
	operand_ptr op;
};

typedef edd_int08_opmap		edd_int08_ref;
typedef edd_int16_opmap		edd_int16_ref;
typedef edd_int32_opmap		edd_int32_ref;

typedef edd_uint08_opmap	edd_uint08_ref;
typedef edd_uint16_opmap	edd_uint16_ref;
typedef edd_uint32_opmap	edd_uint32_ref;

typedef edd_real32_opmap	edd_real32_ref;
typedef edd_real64_opmap	edd_real64_ref;

typedef edd_string_opmap	edd_string_ref;


class edd_int32_array
{
public:
	edd_int32_array(operand_ptr o);

	edd_int32 operator[](int index);
	void assign(int index, edd_int32 d);

	operand_ptr get_op(void) const throw() { return op; }

private:
	operand_ptr op;
};

class edd_uint32_array
{
public:
	edd_uint32_array(operand_ptr o);

	edd_uint32 operator[](int index);
	void assign(int index, edd_uint32 d);

	operand_ptr get_op(void) const throw() { return op; }

private:
	operand_ptr op;
};

} /* namespace edd */

#endif /* _edd_builtins_dispatch_h */
