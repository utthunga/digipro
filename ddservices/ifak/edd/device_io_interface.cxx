
/* 
 * $Id: device_io_interface.cxx,v 1.5 2009/02/10 15:17:39 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io_interface.h"

// C stdlib

// C++ stdlib

// my lib

// own header
#include "edd_ast.h"
#include "edd_operand_interface.h"


namespace edd
{

void
device_io_interface::begin_transfer_sequence(class method_env *env)
{
	if (env)
	{
		env->set_optimize(true);
		env->age_stamp_tick();
	}
}

void
device_io_interface::end_transfer_sequence(class method_env *env)
{
	if (env)
	{
		env->set_optimize(false);
		env->age_stamp_tick();
	}
}

} /* namespace edd */
