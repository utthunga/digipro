
/* 
 * $Id: device_io.h,v 1.39 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2003-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _device_io_h
#define _device_io_h

// C stdlib

// C++ stdlib
#include <memory>

// other libs

// my lib
#include "Logger.h"
#include "vbitset.h"

// own header
#include "device_io_interface.h"


namespace edd
{

/**
 * Generic device I/O.
 * Implements generic functions as needed by special device I/O
 * classes. Supports VARIABLE encoding/decoding helper, updownload,
 * state machine, debug helper and so on.
 */
class device_io : public device_io_interface
{
public:
	device_io(class Interpreter &);

	/* implement device_io_interface */
	virtual bool is_online(void) const throw();

	/* implement device_io_interface */
	virtual int read(class method_env *, class EDD_OBJECT_tree *);
	virtual int write(class method_env *, class EDD_OBJECT_tree *);

	/* connection state */
	enum state
	{
		disconnected = 0,
		connected,
		disconnecting,
		connecting,
		connect_error
	};
	enum state get_state(void) const throw() { return mystate; }
	const char *get_state_descr(void) const throw();

	/* helper */
	int updownload_old(bool flag, class method_env * = NULL);
	int updownload_all(bool flag, class method_env * = NULL);
	int updownload_pdm(bool flag, class method_env * = NULL);
	int updownload_iec(bool flag, class method_env * = NULL);

protected:
	virtual void state_change(enum state);

	/* notifications */
	virtual void on_state_change(void);
	virtual void on_runtime_error(const class RuntimeError &);

	virtual void on_progress_start(const std::string &, bool);
	virtual void on_progress_end(void);
	virtual bool on_progress_activity(void);
	virtual bool on_progress_update(double);

	/* helper */
	int read_write_object(class method_env *, class EDD_OBJECT_tree *, unsigned long op, int &warnings);
	int updownload_menu(class method_env *, class MENU_tree *, bool &, bool, bool, int &warnings);
	int updownload_object(class method_env *, operand_ptr, bool &, bool, bool, int &warnings);
	void updownload_finalize(class method_env *env, const char *msg,
			         int errors, int warnings, bool cancel);
	void set_hart_masks(class method_env *);

	void info(const char *fmt, ...) CHECK_PRINTF(2, 3);
	void warning(const char *fmt, ...) CHECK_PRINTF(2, 3);
	void error(const char *fmt, ...) CHECK_PRINTF(2, 3);
	void trace(const char *fmt, ...) CHECK_PRINTF(2, 3);

	/* references */
	class Interpreter &parent;

	/* connection state */
	enum state mystate;

	/*
	 * generic internal helper
	 */

	static void warning(lib::Logger &, const char *fmt, ...) CHECK_PRINTF(2, 3);
	static void error(lib::Logger &, const char *fmt, ...) CHECK_PRINTF(2, 3);

	static void runtime_error(lib::Logger &, const class COMMAND_tree *,
				  const RuntimeError &, const char *);

	static bool check_variable(class VARIABLE_tree *, bool flag, class method_env *);

	static bool can_optimize(class method_env *, const std::vector<class DATA_ITEM_tree *> &);

	static void optimize_age_stamp_tick(class method_env *);

	static int read_encode(const class COMMAND_tree *, int trans_nr, class method_env *,
			       lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
			       size_t &datalen, size_t &dynamic_datalen, int32 &timeout, int &warnings);

	static int read_decode(const class COMMAND_tree *, int trans_nr, class method_env *,
			       lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
			       const unsigned char *buf, const size_t buflen);

	static int write_encode(const class COMMAND_tree *, int trans_nr, class method_env *,
			        lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
				unsigned char *buf, const size_t buflen, size_t &datalen,
				int32 &timeout);

	static int write_decode(const class COMMAND_tree *, int trans_nr, class method_env *,
			        lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
				int success);

	static int do_pre_read_actions(const class COMMAND_tree *, int trans_nr, class method_env *,
				       lib::Logger &, const std::vector<class DATA_ITEM_tree *> &);

	static int do_pre_write_actions(const class COMMAND_tree *, int trans_nr, class method_env *,
					lib::Logger &, const std::vector<class DATA_ITEM_tree *> &);

	static void do_post_read_actions(const class COMMAND_tree *, int trans_nr, class method_env *,
					 lib::Logger &, const std::vector<class DATA_ITEM_tree *> &);

	static bool do_post_write_actions0(class method_env *, lib::Logger &,
					   operand_ptr, int success, bool, bool);

	static void do_post_write_actions(const class COMMAND_tree *, int trans_nr, class method_env *,
					  lib::Logger &, const std::vector<class DATA_ITEM_tree *> &, int success);

	/*
	 * encode/decode helper
	 */

	struct read_encode_info
	{
		read_encode_info(class method_env *, lib::Logger &,
				 const class COMMAND_tree *, int,
				 size_t &, size_t &, int32 &);

		class method_env *env;
		lib::Logger &logger;
		const class COMMAND_tree *cmd;
		int trans_nr;

		std::auto_ptr<lib::vbitset> mask;

		size_t &datalen;
		size_t &dynamic_datalen;
		int32 &timeout;
	};

	static int read_encode0_var(struct read_encode_info &,
				    class VARIABLE_tree *, const lib::vbitset *);

	static int read_encode0_op(struct read_encode_info &,
				   operand_ptr, const lib::vbitset *);

	struct read_decode_info
	{
		read_decode_info(class method_env *, lib::Logger &,
				 const class COMMAND_tree *, int,
				 const unsigned char *, const size_t);

		class method_env *env;
		lib::Logger &logger;
		const class COMMAND_tree *cmd;
		int trans_nr;

		lib::vbitset data_item;
		bool mask;
		size_t masklen;

		const unsigned char *data;
		const size_t datalen;

		size_t pos;
	};

	static int read_decode0_var(struct read_decode_info &,
				    class VARIABLE_tree *, operand_ptr, const lib::vbitset *);

	static int read_decode0_op(struct read_decode_info &,
				   operand_ptr, const lib::vbitset *);

	struct write_encode_info
	{
		write_encode_info(class method_env *, lib::Logger &,
				  const class COMMAND_tree *, int,
				  unsigned char *, const size_t, size_t &, int32 &);

		class method_env *env;
		lib::Logger &logger;
		const class COMMAND_tree *cmd;
		int trans_nr;

		std::auto_ptr<lib::vbitset> mask;
		size_t masklen;

		lib::vbitset data_item;
		size_t pos;

		unsigned char *data;
		const size_t datalen;
		size_t &datagramlen;

		int32 &timeout;
	};

	static int write_encode0_var(struct write_encode_info &,
				     class VARIABLE_tree *, operand_ptr, const lib::vbitset *);

	static int write_encode0_op(struct write_encode_info &,
				    operand_ptr, const lib::vbitset *);

public:
	/*
	 * generic public helper
	 */

	static int read_encode0(const class COMMAND_tree *, int trans_nr, class method_env *,
			        lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
			        size_t &datalen, size_t &dynamic_datalen, int32 &timeout, int &warnings);

	static int read_decode0(const class COMMAND_tree *, int trans_nr, class method_env *,
			        lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
			        const unsigned char *buf, const size_t buflen);

	static int write_encode0(const class COMMAND_tree *, int trans_nr, class method_env *,
			         lib::Logger &, const std::vector<class DATA_ITEM_tree *> &,
				 unsigned char *buf, const size_t buflen, size_t &datalen,
				 int32 &timeout);

	static void variable_encode(class method_env *env, class VARIABLE_tree *v, operand_ptr op,
				    unsigned char *buf, size_t buflen);

	static void variable_decode(class method_env *env, class VARIABLE_tree *v, operand_ptr op,
				    const unsigned char *buf, const size_t buflen);
};

} /* namespace edd */

#endif /* _device_io_h */
