
/* 
 * $Id: main.cxx,v 1.104 2009/05/19 12:35:34 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifdef _MSC_VER
# pragma warning (disable: 4786)
#endif

// C stdlib
#include <errno.h>
#include <limits.h>
#include <stdio.h>

// C++ stdlib
#include <vector>
#include <string>

// my lib
#include "lib/fstream.h"
#include "lib/getopt.h"
#include "lib/paths.h"
#include "lib/string.h"
#include "sys/dirent.h"

// own header
#include "Checker.h"
#include "InternalEvaluationLicense.h"
#include "NullLicense.h"
#include "config.h"
#include "version.h"

#if !defined(FREEWARE) && !defined(TESTSYSTEM)
# define NORMAL
#endif


/* external debugging and testing hook */
int main1(edd::Checker &, bool);

/* version specific helper */
static void check_license(edd::Checker &);
static void greetings_license(void);


#ifdef NORMAL

#if defined WIN32 && !defined INTERNAL_EVALUATION
# include "visual-studio-share/license-lib/WorkbenchDongleLicense.h"
#endif

static std::auto_ptr<class License> license;

static void
lookup_license(void)
{
	if (!license.get())
	{
		try
		{
#if defined(INTERNAL_EVALUATION)
			license.reset(new class InternalEvaluationLicense);
#elif defined(WIN32)
			license.reset(new class WorkbenchDongleLicense);
#endif
		}
		catch (const std::exception &)
		{
			/* ignore */
		}

		if (!license.get())
			license.reset(new class NullLicense);

		assert(license.get());
	}
}

static void
check_license(edd::Checker &checker)
{
	lookup_license();

	if (!license->have_checker_license())
		checker.set_demo_limit(DEMOLIMIT, true);
}

static void
greetings_license(void)
{
	lookup_license();

	if (!license->have_checker_license())
		printf("demo [limited to " STR(DEMOLIMIT) " EDD-Objects]");
	else
		printf("licensed [unrestricted]");
}
#endif /* NORMAL */

#ifdef FREEWARE
static void
check_license(edd::Checker &checker)
{
	/* only restricted to PROFIBUS
	 * checked while handling the arguments
	 */
}
static void
greetings_license(void)
{
	printf("Freeware [only PROFIBUS supported]");
}
#endif /* FREEWARE */

#ifdef TESTSYSTEM

extern "C" __declspec(dllimport) int fEdddll(int n);
extern "C" __declspec(dllimport) int isEDDCheckerLicensed(void);

static int
dllValidate(void)
{
	srand((unsigned)time(NULL));
	int prime = 9689;
	long int la = rand();
	int rem;
	float af = la * prime / RAND_MAX;
	int n = (int)af;
	ldiv_t div_result;

	div_result = ldiv(n, prime);
	rem = div_result.rem;	

	int result = fEdddll(n);
	if (rem == result)
		return 1;

	return 0;
}

static void
check_license(edd::Checker &checker)
{
	if (!dllValidate())
	{
		printf("Incorrect iflEDDChecker.dll used!\n");
		printf("Correct the problem and try again.\n");
		exit(1);
	}

	if (!isEDDCheckerLicensed())
	{
		printf("No Testsystem license available!\n");
		printf("Correct the problem and try again.\n");
		exit(1);
	}
}

static void
greetings_license(void)
{
	printf("Testsystem [only PROFIBUS supported]");
}
#endif /* TESTSYSTEM */

static void
greetings(void)
{
	printf(CHECKER_DESCR " v" VERSION " (");
	greetings_license();
	printf( ")\n");

	printf(
		"Copyright (C) 1999-2009 ifak e.V. Magdeburg (http://www.ifak.eu/)\n"
		"Author Frank Naumann <frank.naumann@ifak.eu>\n"
		"CPP preprocessor from Thomas Pornin <pornin@bolet.org>\n"
		"Compiled on " __DATE__ ".\n"
	);
}

static void
usage(const std::string &myname)
{
	printf(
"Usage:\n"
"        %s [options] <EDD or Devices file or isEDDWorkbench file>\n"
"\n"
"edd file            : the .edd/.ddl file that should be checked\n"
"Devices file        : the .Devices files of PDM\n"
"isEDDWorkbench file : the .EDDy file of an isEDDWorkbench project\n"
"\n"
"available options:\n"
"   -h          [or --help]:               print this message\n"
"   -V          [or --version]:            print version\n"
"   -I<path>    [or --include=<path>]:     add path as preprocessor include\n"
"                                          search dir\n"
"   -R<path>    [or --rinclude=<path>]:    recursively add all subdirectories of\n"
"                                          path as preprocessor include search\n"
"                                          dir\n"
"   -H<path>    [or --hcf-path=<path>]:    specifiy path to the HART source\n"
"                                          device catalog\n"
"   -A<path>    [or --pb-path=<path>]:     specifiy path to the PROFIBUS/PROFINET\n"
"                                          PA catalog\n"
"   -D<macro>   [or --define=<macro>]:     predefine macro for preprocessor\n"
"   -d<file>    [or --dictionary=<file>]:  add file as dictionary\n"
"   -w          [or --no-warn]:            do not display warnings\n"
"   -p<profile> [or --profile=<profile>]:  parse specific EDDL dialect;\n"
"                                          profile can be: HART, FIELDBUS,\n"
"                                          PROFIBUS, PROFINET; default is PROFIBUS\n"
"   -P          [or --pdm-mode]:           try to behave like PDM; more relaxed\n"
"                                          syntax and semantic checks\n"
"   --max-errors=<number>                  report only the first <number> of\n"
"                                          errors; default is 50, minimum is 10\n"
"   --pedantic                             pedantic mode\n"
"   --no-enh-1                             create error messages if EDD\n"
"                                          enhancements part 1 are used\n"
"\n", lib::basename(myname).c_str());

	exit(1);
}

static void
std_unexpected(void)
{
	fputs("Unexpected critical error, bailing out...\n", stderr);
	std::terminate();
}

int
main(int argc, char **argv)
{
	std::vector<std::string> includes;
	std::vector<std::string> defines;
	std::vector<std::string> dictionaries;
	std::string hcf_path, pb_path;
	std::string file;

	edd::edd_tree::profile profile = edd::edd_tree::PROFIBUS;
	size_t options = 0;
	bool quiet = true;
	bool warn = true;
	int err_limit = -1;
	int i = 1;


	/*
	 * initialization
	 */

	std::set_unexpected(std_unexpected);


	try
	{
		/*
		 * Defaults
		 */

		includes.push_back(".");


		/*
		 * parse arguments
		 */

		for (;;)
		{
			static struct option long_options[] =
			{
				{ "help",		no_argument,		0, 'h'	},
				{ "version",		no_argument,		0, 'V'	},
				{ "include",		required_argument,	0, 'I'	},
				{ "rinclude",		required_argument,	0, 'R'	},
				{ "define",		required_argument,	0, 'D'	},
				{ "dictionary",		required_argument,	0, 'd'	},
				{ "hcf-path",		required_argument,	0, 'H'	},
				{ "pb-path",		required_argument,	0, 'A'	},
				{ "no-warn",		no_argument,		0, 'w'	},
				{ "profile",		required_argument,	0, 'p'  },
				{ "pdm-mode",		no_argument,		0, 'P'  },
				{ "max-errors",		required_argument,	0,  1   },
				{ "pedantic",		no_argument,		0,  2   },
				{ "no-enh-1",		no_argument,		0,  3   },
				{ 0,			0,			0,  0	}
			};

			int option_index = 0;
			int c;


			c = getopt_long(argc, argv, "hVvI:R:D:d:H:A:wp:P",
					long_options, &option_index);

			/* end of the options */
			if (c == -1)
				break;

			switch (c)
			{
				case 0:
				{
					/* If this option set a flag, do nothing else now.  */
					if (long_options[option_index].flag != 0)
						break;

					usage(argv[0]);
					exit(1);

					break;
				}
				case 'h':
				{
					greetings();
					putchar('\n');
					usage(argv[0]);
					exit(0);
				}
				case 'V':
				{
					greetings();
					exit(0);
				}
				case 'I':
				{
					std::string str(optarg);
					sys::DIR *dir = sys::opendir(str);

					if (dir)
					{
						includes.push_back(optarg);
						sys::closedir(dir);
					}
					else
						printf("Warning, no such directory: \"%s\"\n", str.c_str());

					break;
				}
				case 'R':
				{
					std::string str(optarg);
					sys::DIR *dir = sys::opendir(str);

					if (dir)
					{
						edd::Interpreter::lookup_dirs(str, includes);
						sys::closedir(dir);
					}
					else
						printf("Warning, no such directory: \"%s\"\n", str.c_str());

					break;
				}
				case 'D':
				{
					defines.push_back(optarg);
					break;
				}
				case 'd':
				{
					std::string str(optarg);

					if (lib::file_exist(str))
						dictionaries.push_back(str);
					else
						printf("Warning, no such file: \"%s\"\n", str.c_str());

					break;
				}
				case 'H':
				case 'A':
				{
					std::string str(optarg);
					sys::DIR *dir = sys::opendir(str);

					if (dir)
					{
						if (c == 'H')
							hcf_path = str;
						else
							pb_path = str;

						sys::closedir(dir);
					}
					else
						printf("Warning, no such directory: \"%s\"\n", str.c_str());

					break;
				}
				case 'w':
				{
					warn = false;
					break;
				}
				case 'p':
				{
#if defined(FREEWARE) || defined(TESTSYSTEM)
					std::string str(optarg);

					if (str != edd::edd_tree::profile_descr(edd::edd_tree::PROFIBUS))
					{
						printf("Profile `%s' not licensed!\n", optarg);
						printf("Please purchase the unrestricted version from http://www.ifak-system.com/.\n\n");

						exit(1);
					}

					profile = edd::edd_tree::PROFIBUS;
#else
					std::vector<edd::edd_tree::profile> profiles(edd::edd_tree::allprofiles());
					std::string str(optarg);
					bool found = false;

					for (size_t i = 0; i < profiles.size(); i++)
					{
						if (str == edd::edd_tree::profile_descr(profiles[i]))
						{
							profile = profiles[i];
							found = true;

							break;
						}
					}

					if (!found)
					{
						printf("unknown profile `%s'\n\n", optarg);

						usage(argv[0]);
						exit(1);
					}
#endif
					break;
				}
				case 'P':
				{
					options |= edd::edd_tree::PDM_MODE;
					break;
				}
				case 1:
				{
					char *ep;
					long lval;

					errno = 0;
					lval = strtol(optarg, &ep, 10);

					if (optarg[0] == '\0' || *ep != '\0')
					{
						usage(argv[0]);
						exit(1);
					}

					if ((errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN))
					     || (lval > INT_MAX || lval < INT_MIN))
					{
						usage(argv[0]);
						exit(1);
					}

					err_limit = lval;
					break;
				}
				case 2:
				{
					options |= edd::edd_tree::PEDANTIC;
					break;
				}
				case 3:
				{
					options |= edd::edd_tree::NO_EDDx1;
					break;
				}

				case '?':
					printf("unknown option `%s'\n\n", optarg);
					/* fall back */

				default:
					usage(argv[0]);
					exit(1);
			}
		}


		/*
		 * lookup file argument
		 */

		if (!(optind < argc))
		{
			usage(argv[0]);
			exit(1);
		}

		file = argv[optind];


		/*
		 * run the parser/checker
		 */

		lib::fstream f(stdout, lib::fstream::dont_close);
		edd::Checker checker;

		check_license(checker);

		if (lib::check_extension(file, "Devices"))
		{
			i = edd::Checker::process_Devices(checker,
							  file,
							  includes,
							  defines,
							  dictionaries,
							  hcf_path,
							  pb_path,
							  quiet, warn, err_limit,
							  profile, options,
							  f);
		}
		else if (lib::check_extension(file, "EDDy"))
		{
			i = edd::Checker::process_EDDY(checker,
						       file,
						       includes,
						       defines,
						       dictionaries,
						       hcf_path,
						       pb_path,
						       quiet, warn, err_limit,
						       profile, options,
						       f);
		}
		else
		{
			i = edd::Checker::run_check(checker,
						    file,
						    includes,
						    defines,
						    dictionaries,
						    hcf_path,
						    pb_path,
						    quiet, warn, err_limit,
						    profile, options,
						    f);
		}

		/* if correct dispatch the debugging and testing hook */
		if (i == 0)
			i = main1(checker, !quiet);
	}
	catch (const std::exception &e)
	{
		fputs("Error: ", stderr);
		fputs(e.what(), stderr);
		fputs("!\n", stderr);
	}
	catch (...)
	{
		fputs("Global exception handler:\n", stderr);
		fputs(" - Unknown uncaught exception!\n", stderr);
	}

	return i;
}
