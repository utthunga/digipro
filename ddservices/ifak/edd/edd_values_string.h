
/* 
 * $Id: edd_values_string.h,v 1.10 2008/11/12 11:29:53 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions of the string values.
 */

#ifndef _edd_values_string_h
#define _edd_values_string_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
# pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <string>

// my lib

// own
#include "edd_operand_interface.h"


namespace edd
{

/**
 * EDD string value.
 */
class string_value : public operand
{
protected:
	std::string value;

public:
	/** \name Constructor/destructor. */
	//! \{
	string_value(void) { }
	string_value(const char *v) : value(v) { }
	string_value(const std::string &v) : value(v) { }
	//! \}

	long len(void) const { return value.length(); }
	const std::string &get(void) const { return value; }

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator std::string() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	operand_ptr operator+   (const operand &op) const;

	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}
};

/**
 * EDD string variable.
 */
class string_lvalue : public string_value
{
protected:
	class status mystatus;
	virtual void assign(const std::string &v, bool strict = false);

public:
	/** \name Constructor/destructor. */
	//! \{
	string_lvalue(value_status_t = VALUE_STATUS_NONE);
	string_lvalue(const char *v, value_status_t = VALUE_STATUS_NONE);
	string_lvalue(const std::string &v, value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	void        operator+=  (const operand &op);
	//! \}
};

/**
 * EDD checked string variable.
 */
class checked_string_lvalue : public string_lvalue
{
protected:
	class VARIABLE_TYPE_STRING_tree * const type;

	void assign(const std::string &v, bool strict);

public:
	/** \name Constructor/destructor. */
	//! \{
	checked_string_lvalue(const char *v,
			      class VARIABLE_TYPE_STRING_tree *t,
			      value_status_t = VALUE_STATUS_NONE);

	checked_string_lvalue(const std::string &v,
			      class VARIABLE_TYPE_STRING_tree *t,
			      value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string pretty_print(void) const;
	//! \}
};

/**
 * EDD octect string variable
 */
class octet_string_lvalue : public string_lvalue
{
public:
	/** \name Constructor/destructor. */
	//! \{
	octet_string_lvalue(const char *v, value_status_t = VALUE_STATUS_NONE);
	octet_string_lvalue(const std::string &v, value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	void scan(const std::string &str, bool strict = true);
	//! \}
};

/**
 * EDD checked octet string value.
 */
class checked_octet_string_lvalue : public octet_string_lvalue
{
protected:
	class VARIABLE_TYPE_STRING_tree * const type;

	void assign(const std::string &v, bool strict);

public:
	/** \name Constructor/destructor. */
	//! \{
	checked_octet_string_lvalue(const char *v,
				    class VARIABLE_TYPE_STRING_tree *t,
				    value_status_t = VALUE_STATUS_NONE);

	checked_octet_string_lvalue(const std::string &v,
				    class VARIABLE_TYPE_STRING_tree *t,
				    value_status_t = VALUE_STATUS_NONE);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}
};

} /* namespace edd */

#endif /* _edd_values_string_h */
