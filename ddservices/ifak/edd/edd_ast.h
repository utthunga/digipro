
/* 
 * Wed Nov 20 09:30:47 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the ast generator tool,
 * written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_ast_h
#define _edd_ast_h


/* BEGIN IMPORT */

/**
 * \file
 * \author Frank Naumann, Marco Meier
 * \brief Definitions for the abstract syntax tree.
 * \note This is a generated file. Any changes you made are lost on the next
 * rebuild.
 */

/**
 * \page compilation_steps Overview of the compilation steps
 * 
 * Short overview about the main compilation steps and actions that are done
 * during parsing and checking an EDD specification.
 * 
 * parse (builtin)
 * - check syntax
 * - check unique top level identifiers
 * - check for multiple defined attributes
 * - synthesize the AST
 * .
 * 
 * pass0 (builtin)
 * - verify AST consistence:
 *   -# check mandatory attributes
 *   -# check AST type consistence
 *   -# check lists specifications
 *   .
 * .
 * 
 * pass1
 * - check references
 * - check referenced types
 * - synthesize C type informations
 * - typecheck the C methods
 * .
 * 
 * pass2
 * - initialize variable/arrays with DEFAULT/INITIAL values
 * - compile the C methods, free C methods AST subtrees
 * .
 * 
 */

#ifdef _MSC_VER
# pragma warning (disable: 4786)
#endif

// C stdlib
#include <assert.h>

// C++ stdlib
#include <list>
#include <map>
#include <memory>
#include <string>
#include <typeinfo>

// my lib
#include "IniParser.h"
#include "dyn_buf.h"
#include "hashtable.h"
#include "ident.h"
#include "maptable.h"
#include "position.h"
#include "scratch_buf.h"
#include "smart_ptr.h"
#include "vbitset.h"
#include "Context.h"
#include "FrameInfo.h"
#include "SMachine.h"

// own header
#include "cocktail_types.h"
#include "edd_values_ast.h"
#include "edd_builtins_dispatch.h"
#include "edd_err.h"
#include "edd_operand_interface.h"
#include "edd_tree.h"

/* END IMPORT */
namespace edd
{


/* BEGIN EXTERN */

/*
 * forward declarations
 */

class c_variable;
class c_type;
class c_method;

class edd_type;
class method_env;


/**
 * \internal
 * C variable representation.
 * This is the abstract interface to C style variables. The main
 * purpose is to generically handle all required things for
 * semantic checking and code generation.
 */
class c_variable : public lib::supp_smart_ptr_base
{
private:
	typedef class c_variable Self;

	/* no copy/assignment constructor semantic defined */
	c_variable(const Self &);
	Self& operator=(const Self &);

public:
	c_variable(void);
	virtual ~c_variable(void);

	virtual void dump(lib::stream &) const = 0;

	virtual const lib::IDENTIFIER &ident(void) const throw() = 0;
	virtual const lib::pos &pos(void) const throw() = 0;
	virtual class c_type type(void) throw() = 0;

	virtual bool initialized(void) const throw() = 0;
	virtual void setinit(void) throw() = 0;

	virtual void lda(stackmachine::SMachine &sm, int frame, const lib::pos &) const = 0;
	virtual void ldi(stackmachine::SMachine &sm) const = 0;
	virtual void sti(stackmachine::SMachine &sm) const = 0;
	virtual void ixa(stackmachine::SMachine &sm, int subsize) const = 0;

	virtual class c_type deref(bool const_index, int index, const lib::pos &);
	virtual class c_type lookup(const lib::IDENTIFIER &ident, const lib::pos &);

	virtual bool is_eddobject(void) const throw();
	virtual class EDD_OBJECT_tree *edd_entry(void) const;
	virtual const class edd_type *edd_type(void) const;
};
typedef lib::supp_smart_ptr<class c_variable> c_variable_ptr;

/**
 * \internal
 * C type representation.
 * This is the central class that represent any C type. It's used
 * for semantic checking and transformations mostly inside the C method
 * compiler.
 */
class c_type
{
public:
	enum typespec
	{
		none = 0,
		tbool,
		tint8, tint16, tint32, tint64,
		tuint8, tuint16, tuint32, tuint64,
		tfloat32, tfloat64,
		tstring,
		tarray, tstruct,
		tvoid,
		tident, teddobject /* pseudo types; special handling */
	};
	enum convert { no, warn, yes };

	/** \name Constructor/Destructor. */
	//! \{

	/** Default constructor. */
	c_type(void);

	/** Normal constructor. */
	c_type(typespec t);

	/** Array constructor. */
	c_type(c_type *a, long s);

	/** Constant constructor. */
	c_type(typespec t, stackmachine::Type *);

	/** Copy constructor. */
	c_type(const c_type &t);

	/** Assignment. */
	class c_type& operator=(const c_type &t);

	/** Desctructor. */
	~c_type(void);

	//! \}

	/** Debugging support. */
	void dump(lib::stream &) const;

	/** Valid operator. */
	operator bool() const throw() { return (mytype != none); }

	/** \name General methods. */
	//! \{
	typespec type(void) const throw() { return mytype; }
	bool is_bool(void) const throw() { return (mytype == tbool); }
	bool is_integer(void) const throw() { return ((mytype >= tbool) && (mytype <= tuint64)); }
	bool is_arithmetic(void) const throw() { return ((mytype >= tbool) && (mytype <= tfloat64)); }
	bool is_string(void) const throw() { return (mytype == tstring); }
	bool is_signed(void) const throw();
	bool is_lval(void) const throw();
	bool is_constant(void) const throw();
	bool is_array(void) const;
	bool is_struct(void) const throw();
	bool is_eddobject(void) const throw();
	bool is_ident(void) const throw();
	//! \}

	/** \name Constant value methods. */
	//! \{
	void setdata(stackmachine::Type *);
	stackmachine::Type &getdata(void) const;
	std::string get_ident(void) const;
	int min_digits(bool &need_sign) const;
	//! \}

	/** \name Variable methods. */
	//! \{
	void setvar(c_variable_ptr v) throw();
	void forcevar(c_variable_ptr v) throw();
	c_variable_ptr getvar(void) const throw();
	lib::IDENTIFIER	ident(void) const throw();
	bool initialized(void) const throw();
	void setinit(void) throw();
	void ldi(stackmachine::SMachine &) const;
	void sti(stackmachine::SMachine &) const;
	//! \}

	/** \name Array methods. */
	//! \{
	long size(void) const;
	class c_type deref(bool const_index, int index, const lib::pos &);
	void ixa(stackmachine::SMachine &, int) const;
	//! \}

	/** \name Struct methods. */
	//! \{
	class c_type lookup(const lib::IDENTIFIER &ident, const lib::pos &);
	//! \}

	/** \name Code generation helpers. */
	//! \{
	void load_default(stackmachine::SMachine &sm,
			  class c_compound_statement_tree *env) const;
	void load_address(class c_node_tree *t, stackmachine::SMachine &sm,
			  class c_compound_statement_tree *env) const;
	void load_value(class c_node_tree *t, c_type::typespec wanted,
			stackmachine::SMachine &sm, class c_compound_statement_tree *env) const;
	//! \}

	/** \name Helper. */
	//! \{
	const char *descr(void) const throw() { return t_descr[mytype]; }

	typespec result(const c_type &target) const throw();
	convert cast(const c_type &target) const;

	const char *printf_fmt(void) const throw();
	int digits(void) const throw() { return digits(mytype); }
	stackmachine::Type *min_value(void) const { return min_value(mytype); }
	stackmachine::Type *max_value(void) const { return max_value(mytype); }
	stackmachine::Type *createdata(void) const { return createdata(mytype); }
	//! \}

	/** \name Static helper. */
	//! \{
	static bool is_bool(typespec mytype) throw() { return (mytype == tbool); }
	static bool is_integer(typespec mytype) throw() { return ((mytype >= tbool) && (mytype <= tuint64)); }
	static bool is_arithmetic(typespec mytype) throw() { return ((mytype >= tbool) && (mytype <= tfloat64)); }
	static bool is_string(typespec mytype) throw() { return (mytype == tstring); }
	static int digits(typespec) throw();
	static stackmachine::Type *min_value(typespec);
	static stackmachine::Type *max_value(typespec);
	static stackmachine::Type *createdata(typespec);
	//! \}

private:
	typespec mytype;
	c_variable_ptr var; /* lval variable reference */
	class c_type *array; /* array typespec chain */
	stackmachine::Type *data; /* constant value */

	/* static data */
	static const char *t_descr[18];
	static const typespec t_result[18][18];
	static const convert t_cast[18][18];
};

/**
 * \internal
 * EDD specific stackmachine.
 * Do the breakpoint check and dispatch to the Interpreter debug hook.
 */
class SMachine : public stackmachine::SMachine
{
public:
	SMachine(class METHOD_BASE_tree *);

	lib::pos get_last_pos(void) const { return last_pos; }
	void set_source_step(bool flag) { source_step = flag; }

	unsigned long add_breakpoint(unsigned long line);
	void del_breakpoint(unsigned long line);
	void del_breakpoints(void) { breakpoints.clear(); }

	typedef std::set<unsigned int> breakpoints_t;
	breakpoints_t breakpoints;

protected:
	virtual void on_pre_execute(stackmachine::Context &c);
	virtual void on_post_execute(stackmachine::Context &c);
	virtual void on_pre_step(stackmachine::Context &c);

private:
	class METHOD_BASE_tree *link;
	lib::pos last_pos;
	bool source_step;
};

/**
 * \internal
 * RuntimeError specialization
 * Indicates that a non-existant module instance was referenced
 * via a operator[]
 */

class ComponentRangeError : public RuntimeError
{
public:
	ComponentRangeError(err_code e, const lib::pos &p, const lib::IDENTIFIER &id)
	: RuntimeError(e, p, id) { }

	virtual ~ComponentRangeError(void) throw() { }
};

/* END EXTERN */
enum kinds
{
	EDD_NONE = 0,
	
	EDD_ROOT = 1,
	EDD_IDENTIFICATION = 2,
	EDD_AXIS = 3,
	EDD_BLOCK = 4,
	EDD_CHART = 5,
	EDD_COLLECTION = 6,
	EDD_MEMBER = 7,
	EDD_COMMAND = 8,
	EDD_TRANSACTION = 9,
	EDD_DATA_ITEM = 10,
	EDD_CONNECTION = 11,
	EDD_DOMAIN = 12,
	EDD_EDIT_DISPLAY = 13,
	EDD_EDIT_DISPLAY_ITEM = 14,
	EDD_COMPONENT = 15,
	EDD_COMPONENT_INITIAL_VALUES = 16,
	EDD_COMPONENT_FOLDER = 17,
	EDD_COMPONENT_REFERENCE = 18,
	EDD_INTERFACE = 19,
	EDD_COMPONENT_RELATION = 20,
	EDD_COMPONENTS_REFERENCE = 21,
	EDD_COMPONENT_REQUIRED_RANGE = 22,
	EDD_FILE = 23,
	EDD_GRAPH = 24,
	EDD_GRID = 25,
	EDD_VECTOR = 26,
	EDD_IMAGE = 27,
	EDD_LIST = 28,
	EDD_IMPORTED_DD = 29,
	EDD_IMPORT_ACTION = 30,
	EDD_IMPORT_ACTION_REDEFINITION = 31,
	EDD_LIKE = 32,
	EDD_LIKE_ACTION = 33,
	EDD_ANON_LIKE_ACTION = 34,
	EDD_MENU = 35,
	EDD_MENU_ITEM = 36,
	EDD_MENU_ITEM_FORMAT = 37,
	EDD_MENU_ITEM_STRING = 38,
	EDD_MENU_ITEM_ENUM_REF = 39,
	EDD_METHOD = 40,
	EDD_METHOD_DEFINITION = 41,
	EDD_METHOD_PARAMETER = 42,
	EDD_PLUGIN = 43,
	EDD_OPEN_CLOSE = 44,
	EDD_PROGRAM = 45,
	EDD_RECORD = 46,
	EDD_REFERENCE_ARRAY = 47,
	EDD_VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT = 48,
	EDD_REFERENCE_ARRAY_ELEMENT = 49,
	EDD_REFRESH_RELATION = 50,
	EDD_UNIT_RELATION = 51,
	EDD_WAO_RELATION = 52,
	EDD_RESPONSE_CODES_DEFINITION = 53,
	EDD_RESPONSE_CODE = 54,
	EDD_SOURCE = 55,
	EDD_VALUE_ARRAY = 56,
	EDD_VARIABLE = 57,
	EDD_VARIABLE_TYPE_ARITHMETIC = 58,
	EDD_MINMAX_VALUE = 59,
	EDD_VARIABLE_TYPE_BOOLEAN = 60,
	EDD_VARIABLE_TYPE_ENUMERATED = 61,
	EDD_VARIABLE_TYPE_STRING = 62,
	EDD_VARIABLE_TYPE_INDEX = 63,
	EDD_VARIABLE_TYPE_DATE_TIME = 64,
	EDD_VARIABLE_TYPE_OBJECT = 65,
	EDD_VARIABLE_ENUMERATOR_ENUM = 66,
	EDD_VARIABLE_ENUMERATOR_BITENUM = 67,
	EDD_VARIABLE_LIST = 68,
	EDD_WAVEFORM = 69,
	EDD_KEY_POINTS = 70,
	EDD_WAVEFORM_TYPE_YT = 71,
	EDD_WAVEFORM_TYPE_XY = 72,
	EDD_WAVEFORM_TYPE_HORIZONTAL = 73,
	EDD_WAVEFORM_TYPE_VERTICAL = 74,
	EDD_CONSTRAINT_IF = 75,
	EDD_CONSTRAINT_SELECT = 76,
	EDD_CONSTRAINT_SELECTION = 77,
	EDD_EXPR_PRIMARY = 78,
	EDD_EXPR_POSTFIX = 79,
	EDD_EXPR_UNARY = 80,
	EDD_EXPR_BINARY = 81,
	EDD_EXPR_CONDITIONAL = 82,
	EDD_EXPR_ASSIGNMENT = 83,
	EDD_EXPR_COMMA = 84,
	EDD_DICTIONARY_REFERENCE = 85,
	EDD_TYPED_REFERENCE = 86,
	EDD_METHOD_REFERENCE = 87,
	EDD_REFERENCE_IDENT = 88,
	EDD_REFERENCE_ARRAY_REF = 89,
	EDD_REFERENCE_CALL = 90,
	EDD_REFERENCE_RECORD = 91,
	EDD_REFERENCE_BLOCK = 92,
	EDD_STRING = 93,
	EDD_STRING_TERMINAL = 94,
	EDD_c_char = 95,
	EDD_c_dictionary = 96,
	EDD_c_identifier = 97,
	EDD_c_integer = 98,
	EDD_c_real = 99,
	EDD_c_string = 100,
	EDD_c_edd_terminal = 101,
	EDD_c_primary_expr = 102,
	EDD_c_postfix_expr_array = 103,
	EDD_c_postfix_expr_struct = 104,
	EDD_c_postfix_expr_call = 105,
	EDD_c_postfix_expr_op = 106,
	EDD_c_unary_expr = 107,
	EDD_c_cast_expr = 108,
	EDD_c_binary_expr = 109,
	EDD_c_conditional_expr = 110,
	EDD_c_assignment_expr = 111,
	EDD_c_comma_expr = 112,
	EDD_c_declaration = 113,
	EDD_c_declarator = 114,
	EDD_c_declarator_array_specifier = 115,
	EDD_c_compound_statement = 116,
	EDD_c_expr_statement = 117,
	EDD_c_selection_statement_if = 118,
	EDD_c_selection_statement_switch = 119,
	EDD_c_labeled_statement = 120,
	EDD_c_iteration_statement_do = 121,
	EDD_c_iteration_statement_for = 122,
	EDD_c_jump_statement = 123,
	
	EDD_NODE_MAX = 124,
	
	EDD_TERMINAL = 124,
	EDD_pseudo = EDD_TERMINAL + 0,
	EDD_boolean = EDD_TERMINAL + 1,
	EDD_integer = EDD_TERMINAL + 2,
	EDD_real = EDD_TERMINAL + 3,
	EDD_enum = EDD_TERMINAL + 4,
	EDD_set = EDD_TERMINAL + 5,
	EDD_bitset = EDD_TERMINAL + 6,
	EDD_identifier = EDD_TERMINAL + 7,
	EDD_string = EDD_TERMINAL + 8,
	EDD_current_role = EDD_TERMINAL + 9,
	EDD_uuid = EDD_TERMINAL + 10,
	EDD_array_index = EDD_TERMINAL + 11,
	EDD_TERMINAL_MAX = 136,
};


class node_tree;

/**
 * The base tree node class.
 * This is the abstract base class of all tree node classes.
 * It implement the generic data and features of an AST node.
 */
class tree
{
private:
	static const char *_descr[136];
	static const char **_attr_descr[124];
	static const size_t _attr_count[124];
	
public:
	static const char *get_descr(unsigned long kind);
	static const char *get_attr_descr(unsigned long kind, unsigned long attr);
	
private:
private:
 friend class edd_tree; 

	
public:
	/**
	 * The concrete node type.
	 * This uniquely identify the concrete class type of this node.
	 * Can be used to cast to the concrete class type in case of using
	 * additional features that are type specific.
	 */
	const enum kinds kind;
	
	class tree *next;
	class edd_tree * const __parent;
	
	/**
	 * The position information.
	 * This struct hold the source information of the tree node.
	 * This include the source file, line number and column there this
	 * tree node is constructed from.
	 */
	lib::pos pos;
	
private:
	/* no copy/assignment constructor semantic defined */
	tree(const class tree &);
	class tree& operator=(const class tree &);
	
public:
	tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: kind(k), next(0), __parent(_p),
	  pos(p) { }
	virtual ~tree (void);
	
	/**
	 * \internal
	 * Clone itself.
	 * This method create an exact copy of the %tree object.
	 * This must be implemented, it's needed in several places
	 * for automatic storage management and easy abstract
	 * handling of the %tree objects.
	 * \return Newly allocated, exact copy of *this.
	 * \exception RuntimeError(xxOutOfMemory) - not enough memory to
	 * complete the operation.
	 */
	virtual tree *clone(void) const = 0;
	
	/**
	 * Check if the tree is a node_tree.
	 * Return the node_tree pointer, if the tree is an node_tree.
	 * Return NULL if the tree isn't a node_tree.
	 * \return node_tree pointer or NULL.
	 */
	virtual node_tree *is_node(void) throw();
	/**
	 * Check if the tree is a node_tree.
	 * Return the node_tree pointer, if the tree is an node_tree.
	 * Return NULL if the tree isn't a node_tree.
	 * \return node_tree pointer or NULL.
	 */
	virtual const node_tree *is_node(void) const throw();
	
	/**
	 * \internal
	 * Statistic/debugger helper.
	 * Return the real size of the tree.
	 */
	virtual unsigned long _sizeof(void) const throw();
	
	/**
	 * Tree description.
	 * This return the short tree name description
	 * of this tree object. Useful for debugging
	 * and error message creation.
	 */
	const char *descr(void) const throw() { return _descr[kind]; }
	
	void concat(class tree *t) throw();
	
public:
	virtual void dump(lib::stream &f) const;
public:
	virtual const lib::IDENTIFIER & ident(void) const throw();
public:
	virtual class edd_type * synth(void);
public:
	virtual class c_type c_synth_flat(void);
protected:
	virtual void pass1(void);
protected:
	virtual void pass2(const edd_tree::pass2info &info);
public:
	virtual bool have_label(void) const throw();
public:
	virtual std::string label(class method_env *env) const;
public:
	virtual bool have_help(void) const throw();
public:
	virtual std::string help(class method_env *env) const;
public:
	virtual operand_ptr value(class method_env *env);
public:
	virtual operand_ptr eval(class method_env *env);
public:
	virtual class EDD_OBJECT_tree * is_edd_object(void) throw();
	
public:
	const char * name(void) const throw();
protected:
	bool profile_HART(void) const throw();
protected:
	bool profile_PROFIxxx(void) const throw();
protected:
	lib::IDENTIFIER profile_descr(void) const;
protected:
	bool support_long_long(void) const throw();
};

/**
 * The abstract node specialization.
 * This tree specialization features an abstract
 * interface to the AST non terminal objects. It
 * can be used to generically handle the AST tree
 * of an EDD specification. Attribute access,
 * the typechecking and the compiler pass0
 * intensivly use this interface.
 */
class node_tree : public tree
{
public:
	node_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: tree(k, _p, p), _attr_mask(0)
	  , deadlock(false), pass1_done(false), component_parent(NULL) { }
	
	virtual node_tree *is_node(void) throw();
	virtual const node_tree *is_node(void) const throw();
	
	virtual unsigned long _sizeof(void) const throw();
	
	unsigned long attr_mask(void) const throw() { return _attr_mask; }
	void attr_mask_set(unsigned long attr) throw() { _attr_mask |= attr; }
	void attr_mask_clear(unsigned long attr) throw() { _attr_mask &= ~attr; }
	
	virtual class tree *attr(unsigned long i) const = 0;
	virtual const char *attr_descr(unsigned long i) const = 0;
	virtual unsigned long attr_max(void) const = 0;
	virtual unsigned long merge(void) const = 0;
	virtual unsigned long required(void) const = 0;
	virtual const unsigned long type_flags(unsigned long i) const = 0;
	virtual const unsigned long *types_flat(unsigned long i) const = 0;
	virtual const unsigned long *types_recursiv(unsigned long i) const = 0;
	
	void assign_attr(unsigned long attr, class tree *t);
	void add_attr(unsigned long attr, class tree *t);
	void remove_attr(unsigned long attr, class tree *t);
	void replace_attr(unsigned long attr, class tree *old, class tree *n);
	
protected:
	unsigned long _attr_mask;
	
	void clone_node(class node_tree *t) const;
	
private:
	virtual class tree **attr_ptr(unsigned long i) = 0;
	
private:
	public:
		typedef std::pair<int, std::string> label_help_t;
		typedef std::vector<label_help_t> label_help_stack_t;

	protected:
		/**
		 * \internal
		 * Deadlock flag.
		 * This flag is used to detect circular dependencies
		 * between nodes. This avoid deadlock problems with
		 * endless loops in the tree evaluation routines.
		 * In the case of a deadlock the RuntimeError(xxDeadlock)
		 * is thrown.
		 */
		bool deadlock;

		/**
		 * \internal
		 * pass1 done flag
		 */
		bool pass1_done;

	  	class COMPONENT_tree *component_parent;
	
private:
	protected:
		struct selector_lookup_result
		{
			class tree *t;
			lib::pos pos;
			bool always;

			selector_lookup_result(void)
			: t(NULL), pos(lib::NoPosition), always(false) { }
		};
		typedef struct selector_lookup_result selector_lookup_result_t;

		struct selector_info
		{
			const unsigned long *selectors;
			const c_type::typespec *selector_c_types;
			const bool *selector_always;
			size_t size;
		};
		static struct selector_info empty_selector_info;
	

	
public:
	virtual void clone_local(class node_tree *t) const;
public:
	virtual void dump(lib::stream &f) const;
public:
	virtual operand_ptr value(class method_env *env);
public:
	virtual operand_ptr eval(class method_env *env);
protected:
	virtual const node_tree::selector_info & get_selector_info(void) const;
public:
	virtual void selector_ids(std::vector<std::string> &result) const;
public:
	virtual bool selector_lookup1(const lib::IDENTIFIER &ident, class c_type *type, selector_lookup_result_t &result);
public:
	virtual class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	virtual operand_ptr selector_default_value(c_type::typespec type, const lib::IDENTIFIER &ident);
public:
	virtual operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	virtual class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	virtual operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	virtual operand_ptr element_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, long index);
public:
	virtual bool is_comma_element(void) const throw();
protected:
	virtual class identifier_tree * get_comma(void) const throw();
public:
	virtual void add_comma(class tree *comma);
protected:
	virtual void pass1(void);
protected:
	virtual void profile_check(void) const;
protected:
	virtual void profile_check_FIELDBUS(void) const;
protected:
	virtual void profile_check_HART(void) const;
protected:
	virtual void profile_check_PROFIBUS(void) const;
protected:
	virtual void profile_check_PROFINET(void) const;
public:
	virtual void set_component_parent(class COMPONENT_tree *parent);
	
protected:
	class edd_type * synthattr(unsigned long nr) const;
protected:
	operand_ptr evalattr(unsigned long nr, class method_env *env) const;
protected:
	template<typename T> T evalT(unsigned long attr, class method_env *env) const;
protected:
	template<typename T> T evalT(unsigned long attr, class method_env *env, const T &def) const;
protected:
	void selector_ids_generic(std::vector<std::string> &ids) const;
protected:
	bool selector_lookup0(const lib::IDENTIFIER &ident, const struct selector_info &info, class c_type *type, selector_lookup_result_t &result) const;
protected:
	class edd_type * selector_lookup2(const lib::IDENTIFIER &selector_ident, const lib::pos &selector_pos);
protected:
	std::string member_label_help0(class method_env *env, bool labelflag, lib::IDENTIFIER id);
public:
	static unsigned long tree_count(const class tree *t) throw();
protected:
	std::vector<std::string> evalstringlist(unsigned long nr, class method_env *env) const;
protected:
	static void put_space(lib::dyn_buf &buf, int chars);
protected:
	static std::string xml_quote(const std::string &str);
protected:
	void xml_eval0_op(operand_ptr op, const std::string &access_path, class EDD_OBJECT_tree *value_object, lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	void xml_eval0_value(operand_ptr op, const std::string &access_path, class EDD_OBJECT_tree *object, class EDD_OBJECT_tree *value_object, lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	void xml_eval0_struct(operand_ptr op, const std::string &access_path, class EDD_OBJECT_tree *object, class EDD_OBJECT_tree *value_object, lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, const std::vector<MEMBER_tree *> &members);
protected:
	void xml_eval0_value_array(operand_ptr op, const std::string &access_path, class EDD_OBJECT_tree *object, class EDD_OBJECT_tree *value_object, lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, long size);
protected:
	void xml_eval0_ref_array(operand_ptr op, const std::string &access_path, class EDD_OBJECT_tree *object, class EDD_OBJECT_tree *value_object, lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, const std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> &elements);
protected:
	void xml_eval0_values(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, unsigned long attr_nr);
protected:
	void xml_print_runtime_error(lib::dyn_buf &buf, size_t level, const RuntimeError &e) const;
protected:
	void check_comma(class identifier_tree *comma) const;
protected:
	class identifier_tree * check_comma_syntax_get_comma(class tree *t) const;
protected:
	bool check_comma_syntax_check_kind(class tree *t) const;
protected:
	void check_comma_syntax(class tree *t) const;
protected:
	void check_c_uninitialized(const lib::pos &pos, const class c_type &type) const throw();
protected:
	bool check_c_constant(const class c_type &dst, const class c_type &val, const lib::pos &pos, bool no_sign) const;
protected:
	bool check_c_type(const class c_type &src, const lib::pos &pos, const class c_type &dst, bool no_sign) const;
protected:
	bool check_c_type(const class c_type &src, const lib::pos &pos, const class c_type &dst) const;
protected:
	bool check_edd_type(unsigned long nr, const class c_type &mytype) const;
protected:
	class c_type check_constant_expr0(unsigned long nr, lib::pos &error_pos, bool &error) const;
protected:
	bool check_constant_expr(unsigned long nr, bool warn) const;
protected:
	bool check_integer_expr(unsigned long nr) const;
protected:
	bool check_integer_lists(const class integer_tree *t1, const class integer_tree *t2) const throw();
protected:
	void print_type_description(lib::scratch_buf &buf, const class integer_tree *type) const;
protected:
	bool checktypes_complete(const class tree * const t, const class integer_tree *type, const lib::IDENTIFIER &ident, const lib::pos &pos, const lib::pos &refpos) const throw();
protected:
	bool check_members(unsigned long attr, const unsigned long *expected, std::vector<std::string> &selector_names) const;
protected:
	void check_method_args(class edd_type *l, const class c_method *c_method, class tree *args, class c_type &rtype) const;
protected:
	void check_method_reference(unsigned long attr_nr, unsigned long args_attr_nr, class c_type &rtype) const;
public:
	static operand_ptr c_constant2op(const c_type &constant);
protected:
	static void op2tree(std::vector<class tree *> &ret, operand_ptr list);
protected:
	template<typename T> void op2tree(std::vector<T *> &ret, operand_ptr list, int kind) const;
protected:
	static void op2vector(std::vector<operand_ptr> &ret, operand_ptr op);
protected:
	void eval_values(std::vector<operand_ptr> &ret, unsigned long attr, class method_env *env) const;
protected:
	void check_values(unsigned long attr) const;
protected:
	void find_all(unsigned long nr, enum kinds kind, std::list<class tree *> &ret) const;
protected:
	template<typename T, int T_kind> void find_all_tree(class tree *t, std::vector<T *> &ret) const;
protected:
	template<typename T, int T_kind> void find_all(unsigned long nr, std::vector<T *> &ret) const;
protected:
	unsigned long synth_set(const unsigned long attr_nr) const;
public:
	static void variable_class_descr(unsigned long mask, lib::dyn_buf &buf);
public:
	static std::string variable_class_descr_all(void);
protected:
	void check_variable_class0(const unsigned long data, const unsigned long allowed, const unsigned long exclusive, const lib::pos &pos, bool warning) const;
protected:
	void check_variable_class(const unsigned long attr_nr, const unsigned long allowed, const unsigned long exclusive, bool warning) const;
protected:
	void syntax_check_variable_class(const unsigned long attr_nr);
protected:
	bool check_no_constraints(unsigned long nr, bool warn) const;
protected:
	void profile_check_dispatch(void) const;
protected:
	void profile_check_find_all(unsigned long nr, enum kinds kind, const char *add_msg, bool warning) const;
protected:
	void profile_this_not_allowed(void) const;
protected:
	void profile_this_enhancement1_error(void) const;
protected:
	void profile_attr_enhancement1_error(unsigned long nr) const;
protected:
	void profile_attr_not_allowed(unsigned long nr, bool warning) const;
protected:
	void profile_attr_not_allowed(unsigned long nr) const;
protected:
	void profile_attr_missing(unsigned long nr, bool warning) const;
protected:
	void profile_attr_missing(unsigned long nr) const;
protected:
	void profile_check_item_types_FIELDBUS(class integer_tree *t) const;
protected:
	void profile_check_item_types_HART(class integer_tree *t) const;
protected:
	void profile_check_ACTIONS(unsigned long nr, bool warning) const;
protected:
	bool profile_check_no_constraints(unsigned long nr, bool warn) const;
protected:
	void attr_non_conformant(unsigned long nr, bool warning) const;
public:
	class COMPONENT_tree * is_component_child(void);
public:
	class COMPONENT_tree * get_parent_component(void) const;
protected:
	class EDD_OBJECT_tree * ident_lookup(const lib::IDENTIFIER key) const;
};


class EDD_OBJECT_tree;
class UNIT_REF_tree;
class DISPLAY_OBJECT_tree;
class LINE_OBJECT_tree;
class METHOD_BASE_tree;
class VARIABLE_TYPE_tree;
class VARIABLE_ENUMERATOR_tree;
class WAVEFORM_TYPE_tree;
class COMMA_CHECK_BASE_tree;
class REFERENCE_BASE_tree;
class c_node_tree;
class c_expr_node_tree;
class c_string_node_tree;
class c_unary_expr_base_tree;
class c_binary_expr_base_tree;

class ROOT_tree;
class IDENTIFICATION_tree;
class AXIS_tree;
class BLOCK_tree;
class CHART_tree;
class COLLECTION_tree;
class MEMBER_tree;
class COMMAND_tree;
class TRANSACTION_tree;
class DATA_ITEM_tree;
class CONNECTION_tree;
class DOMAIN_tree;
class EDIT_DISPLAY_tree;
class EDIT_DISPLAY_ITEM_tree;
class COMPONENT_tree;
class COMPONENT_INITIAL_VALUES_tree;
class COMPONENT_FOLDER_tree;
class COMPONENT_REFERENCE_tree;
class INTERFACE_tree;
class COMPONENT_RELATION_tree;
class COMPONENTS_REFERENCE_tree;
class COMPONENT_REQUIRED_RANGE_tree;
class FILE_tree;
class GRAPH_tree;
class GRID_tree;
class VECTOR_tree;
class IMAGE_tree;
class LIST_tree;
class IMPORTED_DD_tree;
class IMPORT_ACTION_tree;
class IMPORT_ACTION_REDEFINITION_tree;
class LIKE_tree;
class LIKE_ACTION_tree;
class ANON_LIKE_ACTION_tree;
class MENU_tree;
class MENU_ITEM_tree;
class MENU_ITEM_FORMAT_tree;
class MENU_ITEM_STRING_tree;
class MENU_ITEM_ENUM_REF_tree;
class METHOD_tree;
class METHOD_DEFINITION_tree;
class METHOD_PARAMETER_tree;
class PLUGIN_tree;
class OPEN_CLOSE_tree;
class PROGRAM_tree;
class RECORD_tree;
class REFERENCE_ARRAY_tree;
class VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT_tree;
class REFERENCE_ARRAY_ELEMENT_tree;
class REFRESH_RELATION_tree;
class UNIT_RELATION_tree;
class WAO_RELATION_tree;
class RESPONSE_CODES_DEFINITION_tree;
class RESPONSE_CODE_tree;
class SOURCE_tree;
class VALUE_ARRAY_tree;
class VARIABLE_tree;
class VARIABLE_TYPE_ARITHMETIC_tree;
class MINMAX_VALUE_tree;
class VARIABLE_TYPE_BOOLEAN_tree;
class VARIABLE_TYPE_ENUMERATED_tree;
class VARIABLE_TYPE_STRING_tree;
class VARIABLE_TYPE_INDEX_tree;
class VARIABLE_TYPE_DATE_TIME_tree;
class VARIABLE_TYPE_OBJECT_tree;
class VARIABLE_ENUMERATOR_ENUM_tree;
class VARIABLE_ENUMERATOR_BITENUM_tree;
class VARIABLE_LIST_tree;
class WAVEFORM_tree;
class KEY_POINTS_tree;
class WAVEFORM_TYPE_YT_tree;
class WAVEFORM_TYPE_XY_tree;
class WAVEFORM_TYPE_HORIZONTAL_tree;
class WAVEFORM_TYPE_VERTICAL_tree;
class CONSTRAINT_IF_tree;
class CONSTRAINT_SELECT_tree;
class CONSTRAINT_SELECTION_tree;
class EXPR_PRIMARY_tree;
class EXPR_POSTFIX_tree;
class EXPR_UNARY_tree;
class EXPR_BINARY_tree;
class EXPR_CONDITIONAL_tree;
class EXPR_ASSIGNMENT_tree;
class EXPR_COMMA_tree;
class DICTIONARY_REFERENCE_tree;
class TYPED_REFERENCE_tree;
class METHOD_REFERENCE_tree;
class REFERENCE_IDENT_tree;
class REFERENCE_ARRAY_REF_tree;
class REFERENCE_CALL_tree;
class REFERENCE_RECORD_tree;
class REFERENCE_BLOCK_tree;
class STRING_tree;
class STRING_TERMINAL_tree;
class c_char_tree;
class c_dictionary_tree;
class c_identifier_tree;
class c_integer_tree;
class c_real_tree;
class c_string_tree;
class c_edd_terminal_tree;
class c_primary_expr_tree;
class c_postfix_expr_array_tree;
class c_postfix_expr_struct_tree;
class c_postfix_expr_call_tree;
class c_postfix_expr_op_tree;
class c_unary_expr_tree;
class c_cast_expr_tree;
class c_binary_expr_tree;
class c_conditional_expr_tree;
class c_assignment_expr_tree;
class c_comma_expr_tree;
class c_declaration_tree;
class c_declarator_tree;
class c_declarator_array_specifier_tree;
class c_compound_statement_tree;
class c_expr_statement_tree;
class c_selection_statement_if_tree;
class c_selection_statement_switch_tree;
class c_labeled_statement_tree;
class c_iteration_statement_do_tree;
class c_iteration_statement_for_tree;
class c_jump_statement_tree;

class pseudo_tree;
class boolean_tree;
class integer_tree;
class real_tree;
class enum_tree;
class set_tree;
class bitset_tree;
class identifier_tree;
class string_tree;
class current_role_tree;
class uuid_tree;
class array_index_tree;


/**
 * The edd object specialization.
 * This is the general edd object tree specialization. It features
 * the id mapping and identifier handling. Only the edd objects need
 * to inherit this.
 */
class EDD_OBJECT_tree : public node_tree
{
private:
 public: lib::pos start, end; 
private:
	friend class edd_tree;
	friend class IMPORT_ACTION_tree;
	friend class LIKE_tree;
	friend class COMPONENT_tree;

	protected:
		lib::IDENTIFIER myident;
	private:
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *myhash;
		unsigned int mapid;
	
private:
 bool like_instance; 
private:
	public:
		enum actions
		{
			PRE_EDIT_ACTIONS = 0, POST_EDIT_ACTIONS,
			PRE_READ_ACTIONS, POST_READ_ACTIONS,
			PRE_WRITE_ACTIONS, POST_WRITE_ACTIONS,
			INIT_ACTIONS, REFRESH_ACTIONS, EXIT_ACTIONS
		};
		typedef enum actions actions_t;
	
private:
 protected: bool used; 
private:
 protected: typedef std::map<lib::IDENTIFIER, class MEMBER_tree *> check_members_t; 
private:
	protected:
		template<typename T>
		struct member_T
		{
			member_T(MEMBER_tree *m, T *r)
			: member(m), ref(r) { }

			MEMBER_tree *member;
			T *ref;
		};

		template<typename T>
		struct members_T
		{
			typedef std::vector<struct member_T<T> > Type;
		};
	

	
public:
	EDD_OBJECT_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  , myident(lib::IDENT::NoIdent), \
	    myhash(NULL), \
	    mapid(__parent->treemap().NoHandle), like_instance(false), used(false) { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual void clone_local(class node_tree *t) const;
public:
	virtual const lib::IDENTIFIER & ident(void) const throw();
public:
	virtual class c_type c_synth_flat(void);
protected:
	virtual void post_construction(bool from_like);
public:
	virtual bool have_validity(void) const throw();
public:
	virtual bool is_valid(class method_env *env) const throw();
public:
	virtual bool have_visibility(void) const throw();
public:
	virtual bool is_visible(class method_env *env) const throw();
public:
	virtual bool adt_capable(void);
public:
	virtual bool is_value_object(void) const throw();
public:
	virtual bool is_io_object(void);
public:
	virtual size_t get_io_size(void);
public:
	virtual void save(lib::IniParser &db) const;
public:
	virtual void load(const lib::IniParser &db);
public:
	virtual value_adt_ptr value_adt(void);
protected:
	virtual long actions2attr(actions_t type) const throw();
public:
	virtual bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
public:
	virtual void callgraph(const edd_tree::pass2info &info_ref);
protected:
	virtual void xml_print_name(lib::dyn_buf &buf, class method_env *env);
protected:
	virtual void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	virtual void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
	
public:
	class EDD_OBJECT_tree * is_edd_object(void) throw();
public:
	void set_additional_pos(const lib::pos &s, const lib::pos &e);
protected:
	void assign_ident(const lib::IDENTIFIER &id);
protected:
	void assign_hash(lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *newhash);
public:
	unsigned int id(void);
public:
	 ~EDD_OBJECT_tree(void);
public:
	class EDD_OBJECT_tree * next_object(void) const throw();
public:
	bool is_like_instance(void) const throw();
public:
	void set_like_instance(bool flag) throw();
public:
	int read(class method_env *env);
public:
	int write(class method_env *env);
protected:
	void save0(lib::IniParser &db, value_adt_ptr v) const;
protected:
	void load0(const lib::IniParser &db, value_adt_ptr v);
protected:
	void save_object0(lib::IniParser &db, class method_env *env, class EDD_OBJECT_tree *object) const;
protected:
	void load_object0(const lib::IniParser &db, class method_env *env, class EDD_OBJECT_tree *object);
protected:
	void recurse_types(void);
public:
	bool check_for_value_types_only(void);
protected:
	bool run_actions0(actions_t type, unsigned long attr_nr, class method_env *env, operand_ptr op);
public:
	static const char * actions_descr(actions_t type) throw();
public:
	bool have_actions(actions_t type) const throw();
public:
	bool run_actions(actions_t type, class method_env *env, operand_ptr op);
public:
	bool run_actions(actions_t type, class method_env *env);
public:
	bool is_used(void) const throw();
public:
	void set_referenced(void) throw();
public:
	void set_dependency(void) throw();
protected:
	void xml_print_basic_attr(lib::dyn_buf &buf, class method_env *env);
protected:
	void xml_print_head(lib::dyn_buf &buf, class method_env *env);
public:
	std::string xml_eval(class method_env *env, bool recurse);
protected:
	void check_unique_member_list_ids(class tree *t0, check_members_t members) const;
protected:
	void members0(std::vector<MEMBER_tree *> &ret, unsigned long attr, class method_env *env) const;
protected:
	template<typename T> void members0(typename members_T<T>::Type &ret, unsigned long attr, class method_env *env) const;
protected:
	void xml_eval0_members(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, unsigned long attr);
};

/**
 * Unit reference base object.
 * This is the base for unit reference objects. Supports generic
 * methods for registering and handling unit references.
 */
class UNIT_REF_tree : public EDD_OBJECT_tree
{
private:
 class UNIT_RELATION_tree *unit_ref; 

	
public:
	UNIT_REF_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: EDD_OBJECT_tree(k, _p, p)
	  , unit_ref(NULL) { }
	
	unsigned long _sizeof(void) const throw();
	
protected:
	virtual class tree * have_constant_unit(void) const throw() = 0;
protected:
	virtual std::string constant_unit(class method_env *env) const = 0;
protected:
	virtual void pass2(const edd_tree::pass2info &info);
	
public:
	bool have_unit(void) const throw();
public:
	std::string get_unit(class method_env *env) const;
public:
	void register_unit_ref(class UNIT_RELATION_tree *t);
public:
	class UNIT_RELATION_tree * have_unit_ref(void) const throw();
public:
	class VARIABLE_tree * get_unit_ref(class method_env *env) const;
};

/**
 * Display base object.
 * Generic routines for display objects (#edd::CHART_tree, #edd::GRAPH_tree).
 */
class DISPLAY_OBJECT_tree : public EDD_OBJECT_tree
{
private:
 public: enum { XX_SMALL, X_SMALL, SMALL, MEDIUM, LARGE, X_LARGE, XX_LARGE }; 

	
public:
	DISPLAY_OBJECT_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: EDD_OBJECT_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual unsigned int display_height(class method_env *env) const = 0;
public:
	virtual unsigned int display_width(class method_env *env) const = 0;
	
protected:
	void xml_print_display_size(lib::dyn_buf &buf, unsigned int size, const char *name) const;
protected:
	void xml_print_display_attr(lib::dyn_buf &buf, class method_env *env) const;
};

/**
 * GUI Line base object.
 * Generic routines for GUI objects that support line attributes
 * (#edd::SOURCE_tree, #edd::WAVEFORM_tree).
 */
class LINE_OBJECT_tree : public EDD_OBJECT_tree
{
private:
 public: enum { EDD_DATA = 0, EDD_DATA1, EDD_DATA2, EDD_DATA3, EDD_DATA4, EDD_DATA5,
			 EDD_DATA6, EDD_DATA7, EDD_DATA8, EDD_DATA9,
			 EDD_LOW_LOW_LIMIT, EDD_LOW_LIMIT,
			 EDD_HIGH_LIMIT, EDD_HIGH_HIGH_LIMIT,
			 EDD_TRANSPARENT }; 

	
public:
	LINE_OBJECT_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: EDD_OBJECT_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual bool emphasis(class method_env *env) const = 0;
public:
	virtual unsigned long line_type(class method_env *env) const = 0;
public:
	virtual bool have_line_color(void) const throw() = 0;
public:
	virtual unsigned long line_color(class method_env *env) const = 0;
	
public:
	static size_t nr_line_types(void);
public:
	static const char * line_type_descr(unsigned long type) throw();
protected:
	void xml_print_line_attr(lib::dyn_buf &buf, class method_env *env) const;
};

/**
 * Method base object.
 * The interface for all METHOD type EDD objects.
 */
class METHOD_BASE_tree : public EDD_OBJECT_tree
{
private:
 friend class SMachine; 
private:
 protected: lib::pos pos_start, pos_end; 
private:
	public:
		class task
		{
		public:
			virtual ~task(void) { }
			virtual void operator() (void) = 0;
		};

	private:
		std::list<class task *> pass2_tasks;
	
private:
	public:
		enum builtin_type
		{
			fgetval, igetval, lgetval, sgetval,
			fsetval, isetval, lsetval, ssetval
		};

	private:
		struct builtin
		{
			lib::pos pos;
			int type;
		};
		typedef struct builtin builtin_t;
		typedef std::vector<builtin_t> builtins_t;

		struct child_descr
		{
			lib::pos pos;
			class METHOD_tree *child;
		};
		typedef struct child_descr child_t;
		typedef std::vector<child_t> childs_t;

		struct semantic_info
		{
			builtins_t builtins;
			childs_t childs;
		};
		typedef struct semantic_info semantic_info_t;
		typedef lib::smart_ptr<semantic_info_t> semantic_info_ptr;

		semantic_info_ptr semantic_info;
	

	
public:
	METHOD_BASE_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: EDD_OBJECT_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual c_variable_ptr parameter_lookup(const lib::IDENTIFIER &id) const = 0;
public:
	virtual const class c_method * getmethod(void) const = 0;
public:
	virtual operand_ptr execute(class method_env *env, operand_ptr args, const lib::pos &caller_pos) = 0;
public:
	virtual void dump_sm(lib::stream &f) const = 0;
public:
	virtual class SMachine * get_sm(void) = 0;
	
protected:
	void on_method_start(stackmachine::Context &c);
protected:
	void on_method_stop(stackmachine::Context &c);
protected:
	void on_method_debug(stackmachine::Context &c);
protected:
	void on_method_notify(stackmachine::Context &c);
public:
	lib::pos get_start_pos(void) const throw();
public:
	lib::pos get_end_pos(void) const throw();
public:
	unsigned long add_breakpoint(unsigned long line);
public:
	void del_breakpoint(unsigned long line);
public:
	void del_breakpoints(void);
protected:
	void log_exception(const stackmachine::SMachine &sm, const stackmachine::Context &context, const char *name, const std::string &msg) const;
public:
	void register_pass2_task(class task *task);
protected:
	void process_pass2_tasks(void);
public:
	void register_builtin(const lib::pos &pos, int type);
public:
	void register_child(const lib::pos &pos, class METHOD_tree *t);
protected:
	const edd_tree::pass2info * search_variable(const edd_tree::pass2info *info) const;
protected:
	void information_callgraph(const edd_tree::pass2info *info) const;
protected:
	static const char * builtin_descr(int type) throw();
protected:
	void check_builtin(const edd_tree::pass2info *info, const builtin_t &builtin) const;
protected:
	bool check_recursion(const edd_tree::pass2info &info_ref, const child_t &child);
public:
	void callgraph(const edd_tree::pass2info &info_ref);
};

class VARIABLE_TYPE_tree : public node_tree
{
	
public:
	VARIABLE_TYPE_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual enum kinds gettype(void) const throw() = 0;
public:
	virtual unsigned long getsubtype(void) const throw();
public:
	virtual const char * realdescr(void) const throw() = 0;
public:
	virtual std::string typedescr(void) const throw();
public:
	virtual const char * typestring(void) const throw() = 0;
public:
	virtual size_t get_io_size(void) const throw() = 0;
public:
	virtual bool check_value(const class operand &op, class method_env *env) const;
public:
	virtual void check_enum_dependency(void) const;
public:
	virtual operand_ptr default_lvalue(void) = 0;
public:
	virtual operand_ptr initial_lvalue(void) = 0;
public:
	virtual operand_ptr blank_lvalue(void) = 0;
public:
	virtual operand_ptr unchecked_lvalue(void) = 0;
public:
	virtual const class tree * get_default_value(void) const throw();
public:
	virtual const class tree * get_initial_value(void) const throw();
public:
	virtual bool have_display_format(void) const throw();
public:
	virtual std::string get_display_format(class method_env *env);
public:
	virtual bool have_edit_format(void) const throw();
public:
	virtual std::string get_edit_format(class method_env *env);
public:
	virtual bool have_enumerations(void) const throw();
public:
	virtual void get_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class method_env *env) const;
public:
	virtual class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
public:
	virtual std::vector<class MINMAX_VALUE_tree *> get_minmaxvalues(bool flag) const;
public:
	virtual bool have_scaling(void) const throw();
public:
	virtual operand_ptr scaling_factor(class method_env *env) const;
public:
	virtual bool have_read_write_hook(void) const throw();
public:
	virtual operand_ptr pre_write_hook(class method_env *env, operand_ptr op);
public:
	virtual operand_ptr post_read_hook(class method_env *env, operand_ptr op);
public:
	virtual class edd_type * synth_index(class VARIABLE_tree *parent, bool const_index, long index);
public:
	virtual bool selector_lookup1(const lib::IDENTIFIER &ident, class c_type *type, selector_lookup_result_t &result);
public:
	virtual operand_ptr selector_default_value(c_type::typespec type, const lib::IDENTIFIER &ident);
public:
	virtual void check_default_initial_child(class tree *t, unsigned long nr) const;
protected:
	virtual void find_all_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums) const;
	
protected:
	class VARIABLE_ENUMERATOR_tree * get_enumeration_simple(const operand &value, class method_env *env, class VARIABLE_ENUMERATOR_tree *list) const;
protected:
	void check_default_initial(unsigned long nr) const;
protected:
	void check_default_initial_all(unsigned long nr) const;
protected:
	void check_default_initial_all0(class tree *t, unsigned long nr) const;
private:
	template<typename T> void check_enum_value(class VARIABLE_ENUMERATOR_tree *t, std::map<T, class tree *> &values, std::map<T, class tree *> *result_values) const;
private:
	void check_enum_description(class VARIABLE_ENUMERATOR_tree *t, std::map<std::string, class tree *> &descrs, std::map<std::string, class tree *> *result_descrs) const;
private:
	template<typename T> void check_enums(class tree *t, void (VARIABLE_TYPE_tree::*check)(class VARIABLE_ENUMERATOR_tree *, T &, T*) const, T data, T *result_data) const;
protected:
	template<typename T> void check_enums_values(class tree *t) const;
protected:
	void check_enums_description(class tree *t) const;
public:
	bool check_enumval_exist(operand_ptr value) const;
protected:
	void find_all_enumerations_simple(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class VARIABLE_ENUMERATOR_tree *list) const;
};

/**
 * Enumerator base class.
 * This is the base class for ENUMERATOR and BITENUMERATOR
 * objects. It features an abstract interface for accessing
 * the type, description and the value.
 */
class VARIABLE_ENUMERATOR_tree : public node_tree
{
	
public:
	VARIABLE_ENUMERATOR_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual int enumtype(void) const = 0;
public:
	virtual operand_ptr enum_value(void) const = 0;
public:
	virtual std::string description(class method_env *env) const = 0;
public:
	virtual void check_description_dependency(void) const = 0;
public:
	virtual class identifier_tree * comma(void) const throw();
	
public:
	operand_ptr value(class method_env *env);
public:
	void xml_eval(lib::dyn_buf &buf, class method_env *env);
};

class WAVEFORM_TYPE_tree : public node_tree
{
	
public:
	WAVEFORM_TYPE_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual double x_initial(class method_env *env) const;
public:
	virtual double x_increment(class method_env *env) const;
public:
	virtual std::vector<operand_ptr> x_values(class method_env *env) const;
public:
	virtual std::vector<operand_ptr> y_values(class method_env *env) const;
public:
	virtual long number_of_points(class method_env *env) const;
public:
	virtual std::string x_unit(class method_env *env) const;
public:
	virtual std::string y_unit(class method_env *env) const;
protected:
	virtual void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse) = 0;
	
public:
	void xml_eval(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	std::string get_unit(unsigned long attr, class method_env *env) const;
protected:
	void xml_print_x_initial(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse) const;
protected:
	void xml_print_x_increment(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse) const;
protected:
	void xml_print_values(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, unsigned long attr_nr, const char *name);
protected:
	void xml_print_number_of_points(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse) const;
};

class COMMA_CHECK_BASE_tree : public node_tree
{
	
public:
	COMMA_CHECK_BASE_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
protected:
	virtual void pass1(void);
	
};

class REFERENCE_BASE_tree : public COMMA_CHECK_BASE_tree
{
private:
	protected:
		struct index_op
		{
			std::vector<int> index;
			operand_ptr op;
		};
		typedef struct index_op index_op_t;
		typedef std::vector<index_op_t> index_ops_t;
	

	
public:
	REFERENCE_BASE_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: COMMA_CHECK_BASE_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual REFERENCE_BASE_tree::index_ops_t determine_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars);
public:
	virtual operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res) = 0;
public:
	virtual class EDD_OBJECT_tree * value_object(class method_env *env) = 0;
public:
	virtual operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path) = 0;
	
public:
	bool lookup_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars, std::vector<int> &index);
};

class c_node_tree : public node_tree
{
	
public:
	c_node_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual bool search_call_expr(const lib::IDENTIFIER &id) const;
public:
	virtual class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	virtual void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
	
protected:
	bool search_call_expr0(unsigned long nr, const lib::IDENTIFIER &id) const;
protected:
	class c_type c_synth_attr(unsigned long nr, class c_compound_statement_tree *env, class c_control *control);
protected:
	void code_attr(unsigned long nr, stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};

class c_expr_node_tree : public c_node_tree
{
	
public:
	c_expr_node_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: c_node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
public:
	virtual bool no_effect(void) const = 0;
	
public:
	void check_no_effect(void);
protected:
	bool check_div_by_zero(const class c_type &dst, const class c_type &val, const lib::pos &pos) const;
};

class c_string_node_tree : public c_expr_node_tree
{
	
public:
	c_string_node_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: c_expr_node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
	
public:
	bool no_effect(void) const;
protected:
	void check_str(const std::string &str, class c_compound_statement_tree *env) const;
};

class c_unary_expr_base_tree : public c_expr_node_tree
{
private:
	protected:
		enum ops { OP_NONE, OP_INCR, OP_DECR, OP_POS, OP_NEG, OP_INV, OP_NOT };
	

	
public:
	c_unary_expr_base_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: c_expr_node_tree(k, _p, p)
	  { }
	
	unsigned long _sizeof(void) const throw();
	
protected:
	virtual enum c_unary_expr_base_tree::ops get_op(void) const = 0;
protected:
	virtual lib::pos expr_pos(void) const = 0;
	
protected:
	lib::IDENTIFIER opmsg(void) const;
protected:
	void c_compute_constant(class c_type &ret, const class c_type &op) const;
protected:
	bool check_integer(const class c_type &type) const;
protected:
	bool check_arithmetic(const class c_type &type) const;
protected:
	bool check_not_bool(const class c_type &type) const;
protected:
	class c_type check_type(const class c_type &rtype) const;
public:
	bool no_effect(void) const;
};

class c_binary_expr_base_tree : public c_expr_node_tree
{
private:
	protected:
		enum ops
		{
			OP_NONE,
			OP_MUL,
			OP_DIV,
			OP_MODULO,
			OP_ADD,
			OP_SUB,
			OP_L_SHIFT,
			OP_R_SHIFT,
			OP_LESS,
			OP_GREATER,
			OP_LESS_EQUAL,
			OP_GREATER_EQUAL,
			OP_EQUAL,
			OP_NOT_EQUAL,
			OP_AND,
			OP_XOR,
			OP_OR,
			OP_L_AND,
			OP_L_OR
		};

		c_type::typespec result;
	

	
public:
	c_binary_expr_base_tree(enum kinds k, class edd_tree * const _p, const lib::pos &p)
	: c_expr_node_tree(k, _p, p)
	  , result(c_type::none) { }
	
	unsigned long _sizeof(void) const throw();
	
protected:
	virtual enum c_binary_expr_base_tree::ops get_op(void) const = 0;
protected:
	virtual lib::pos expr1_pos(void) const = 0;
protected:
	virtual lib::pos expr2_pos(void) const = 0;
	
protected:
	lib::IDENTIFIER opmsg(void) const;
private:
	stackmachine::Type * c_convert(c_type::typespec wanted, const stackmachine::Type &data) const;
private:
	void c_compute_constant(c_type::typespec result, class c_type &ret, const class c_type &op1, const class c_type &op2) const;
protected:
	class c_type check_type(const class c_type &rtype1, const class c_type &rtype2);
public:
	bool no_effect(void) const;
};


/**
 * The root node.
 * This is the entry node for every EDD AST representation. It hold the
 * EDD identification and all EDD objects.
 */
class ROOT_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
	friend class edd_tree;
	friend class IMPORT_ACTION_tree;
	

private:
		class meta_data0
		{
		public:
			meta_data0(const lib::IDENTIFIER &_id,
				   class EDD_OBJECT_tree *_t)
			: id(_id), t(_t) { }

			lib::IDENTIFIER id;
			class EDD_OBJECT_tree *t;

			bool operator< (const class meta_data0 &m) const
			{ return (strcmp(id.c_str(), m.id.c_str()) < 0); }
		};

	public:
		class meta_data
		{
		public:
			meta_data(const class meta_data0 &meta)
			: id(meta.id.str())
			, kind(meta.t->kind)
			, line(meta.t->pos.line)
			, column(meta.t->pos.column)
			, end_line(meta.t->end.line)
			, end_column(meta.t->end.column)
			, file(meta.t->pos.file.str())
			{
				if (meta.t->pos.file != meta.t->end.file)
					end_line = end_column = 0;
			}

			meta_data(class EDD_OBJECT_tree *t)
			: id(t->ident().str())
			, kind(t->kind)
			, line(t->pos.line)
			, column(t->pos.column)
			, end_line(0)
			, end_column(0)
			, file(t->pos.file.str())
			{
				if (t->pos.file != t->end.file)
					end_line = end_column = 0;
			}

			meta_data(const std::string &s)
			: id(s)
			, kind(EDD_NONE)
			, line(0), column(0)
			, end_line(0), end_column(0)
			{ }

			std::string id;
			int kind;

			unsigned long line, column;
			unsigned long end_line, end_column;
			std::string file;

			bool operator< (const class meta_data &m) const
			{ return (id < m.id); }
		};

		typedef std::vector<class meta_data> identifiers_t;
		typedef std::map<std::string, std::vector<std::string> > selectors_t;
	

private:
	public:
		class import_info
		{
		public:
			/* import directive  */
			unsigned long line, column;
			std::string file;

			/* imported edd */
			std::string abspath;
			int manufacturer, device_type, device_revision, dd_revision;

			/* imported things */
			identifiers_t objects;
			std::list<class import_info> import_infos;
		};
		typedef std::list<class import_info> import_info_t;

		import_info_t import_infos;
	

public:
	ROOT_tree(class edd_tree * const _p, const lib::pos &p);
	~ROOT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	const class IDENTIFICATION_tree * get_identification(void) const throw();
public:
	const class IDENTIFICATION_tree * identification(void) const throw();
public:
	class IDENTIFICATION_tree * clone_identification(void) const;
public:
	int edd_profile(void) const throw();
public:
	class EDD_OBJECT_tree * definition_list(void) const throw();
protected:
	void replace_definition(class EDD_OBJECT_tree *old, class EDD_OBJECT_tree *t);
protected:
	void remove_definition(class EDD_OBJECT_tree *t);
protected:
	void add_definition(class EDD_OBJECT_tree *t);
private:
	void check_unique_block_numbers(void) const;
private:
	void simple_reference_check(void);
private:
	void pic_sizes_check(void);
private:
	void iec_updownload_check_menu(class EDD_OBJECT_tree *menu, bool read_flag) const;
private:
	void iec_updownload_check(void) const;
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void check_special_menus(int which) const;
private:
	void check_PDM_special_methods(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
private:
	void check_object_type(class EDD_OBJECT_tree *check, enum kinds kind) const;
private:
	void check_object_type(const char *name, enum kinds kind) const;
protected:
	void profile_check_HART(void) const;
public:
	void syntax_meta_data(identifiers_t &identifiers, selectors_t &selectors) const;
public:
	ROOT_tree::import_info_t & get_import_infos(void);
public:
	const ROOT_tree::import_info_t & get_import_infos(void) const;
public:
	void push_import(const lib::pos &pos, const std::string &abspath, class ROOT_tree *imported_dd, class EDD_OBJECT_tree *objects);
};


/**
 * The identification node.
 * This hold the EDD identification informations.
 */
class IDENTIFICATION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[7];
	static const unsigned long *_types_flat[7];
	static const unsigned long *_types_recursiv[7];
	
	class tree *_attr[7];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		mutable int manufacturer_id, device_type_id;
		mutable bool defines_checked;
	

public:
	IDENTIFICATION_tree(class edd_tree * const _p, const lib::pos &p);
	~IDENTIFICATION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	int manufacturer(void) const throw();
public:
	int device_type(void) const throw();
public:
	int device_revision(void) const throw();
public:
	int dd_revision(void) const throw();
public:
	int edd_version(void) const throw();
public:
	int edd_profile(void) const throw();
public:
	std::string manufacturer_ext(void) const throw();
public:
	std::string manufacturer_name(void) const throw();
public:
	std::string device_type_name(void) const throw();
private:
	void finalize_ids(void) const;
private:
	bool manufacturer_is_ident(void) const throw();
private:
	bool device_type_is_ident(void) const throw();
private:
	bool check_ushort(class tree *t, int data, bool is_ident) const;
public:
	bool check(void) const;
protected:
	void pass1(void);
public:
	bool equal(const class IDENTIFICATION_tree *t) const throw();
public:
	int distance(class IDENTIFICATION_tree *t) const throw();
private:
	void save_item(lib::IniParser &db, int data, unsigned long nr) const;
public:
	void save(lib::IniParser &db) const;
private:
	bool check_item(const lib::IniParser &db, int data, unsigned long nr) const;
public:
	bool check_version(const lib::IniParser &db) const;
};


/**
 * The AXIS node.
 * AXIS describes an axis of a CHART or GRAPH.
 * 
 * \note AXIS is an EDD 1.2 enhancement.
 */
class AXIS_tree : public UNIT_REF_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 operand_ptr view_min_, view_max_; 

public:
	AXIS_tree(class edd_tree * const _p, const lib::pos &p);
	~AXIS_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr min_value(class method_env *env) const;
public:
	operand_ptr max_value(class method_env *env) const;
public:
	bool linear(class method_env *env) const;
public:
	bool logarithmic(class method_env *env) const;
protected:
	class tree * have_constant_unit(void) const throw();
protected:
	std::string constant_unit(class method_env *env) const;
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	double get_view_min(void) const;
public:
	double get_view_max(void) const;
private:
	void set_view_min_max(operand_ptr op, double value);
public:
	void set_view_min(double v);
public:
	void set_view_max(double v);
public:
	bool view_min_set(void) const;
public:
	bool view_max_set(void) const;
public:
	void unset_view_min(void);
public:
	void unset_view_max(void);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_HART(void) const;
};


/**
 * The BLOCK node.
 * 
 * \par BLOCK_A
 * A device may be organised in logical blocks. Each BLOCK_A is a logical
 * grouping of PARAMETERS and additional information for BLOCK_A applications.
 * Types of blocks are described by the CHARACTERISTICS attribute. More than
 * one BLOCK_A from the same type can be instantiated.
 * 
 * \note This BLOCK_A approach is optimised for access efficiency.
 * 
 * \par BLOCK_B
 * The variables of a device or module respectively are structured in blocks
 * corresponding to device components or functional parts. Three types of
 * blocks are defined: PHYSICAL, TRANSDUCER and FUNCTION. More than one
 * BLOCK_B from the same TYPE can be instantiated. For efficient access, each
 * instance has its own NUMBER. For each of the block types there are
 * different threads of NUMBER sequences, starting with 1. When accessing a
 * VARIABLE within a device, the BLOCK_B construct is used in conjunction with
 * the COMMAND to provide relative addressing.
 * 
 * \note This BLOCK_B approach is optimised for memory efficiency.
 * 
 * \note The storage of a variable in a device is manufacturer specific and is
 * represented by a device directory. A device directory contains a summary of
 * the available BLOCK_B entries in a device. Furthermore the device directory
 * contains for each BLOCK_B a pointer to its physical address. An EDD
 * application finds a single object by adding an offset to the BLOCK_B
 * physical address.
 * 
 * \note Within a BLOCK_B instance, relative indexing is used. This relative
 * index is defined in the device profile.
 */
class BLOCK_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[27];
	static const unsigned long *_types_flat[27];
	static const unsigned long *_types_recursiv[27];
	
	class tree *_attr[27];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	BLOCK_tree(class edd_tree * const _p, const lib::pos &p);
	~BLOCK_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	int type(void) const throw();
public:
	const char * type_descr(void) const throw();
public:
	long number(class method_env *env) const;
public:
	class RECORD_tree * characteristics(class method_env *env) const;
public:
	std::vector<class MEMBER_tree *> parameters(void) const;
public:
	bool is_block_b(void) const throw();
public:
	bool is_block_a(void) const throw();
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The CHART node.
 * CHART describes a chart used to display data from a device in the EDD
 * application. A CHART is used to display continuous data values from the
 * device, as opposed to a GRAPH that is used to display a data set that is
 * stored in a device. The data from the device may be adjusted prior to
 * being displayed via the use of INIT_ACTIONS and RERESH_ACTIONS on the
 * SOURCE members of the CHART.
 * 
 * \note CHART is an EDD 1.2 enhancement.
 */
class CHART_tree : public DISPLAY_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[10];
	static const unsigned long *_types_flat[10];
	static const unsigned long *_types_recursiv[10];
	
	class tree *_attr[10];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 public: typedef members_T<class SOURCE_tree>::Type members_t; 

private:
 public: enum { GAUGE, HORIZONTAL_BAR, SCOPE, STRIP, SWEEP, VERTICAL_BAR }; 

private:
 std::vector<std::string> selector_names; 

public:
	CHART_tree(class edd_tree * const _p, const lib::pos &p);
	~CHART_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	CHART_tree::members_t members(class method_env *env) const;
public:
	unsigned int display_height(class method_env *env) const;
public:
	unsigned int display_width(class method_env *env) const;
public:
	unsigned long type(class method_env *env) const;
public:
	static const char * type_descr(unsigned long type) throw();
public:
	bool have_cycle_time(void) const throw();
public:
	long cycle_time(class method_env *env) const;
public:
	long length(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The COLLECTION node.
 * A COLLECTION is a logical group of items, such as VARIABLEs or MENUs. An
 * identifier is assigned to each item. The items may be referenced within the
 * device description by using the COLLECTION identifier and the item name.
 * 
 * \note COLLECTION is intended to be used by a tool, e.g. to identify
 * parameters that should be processed together.
 * 
 * \note The recommended supported nesting level is two (e.g. COLLECTION of
 * COLLECTION).
 */
class COLLECTION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 std::vector<std::string> selector_names; 

public:
	COLLECTION_tree(class edd_tree * const _p, const lib::pos &p);
	~COLLECTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	std::vector<MEMBER_tree *> members(class method_env *env) const;
public:
	bool lookup_member(class method_env *env, class EDD_OBJECT_tree *object) const;
public:
	bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
public:
	void save_members(lib::IniParser &db, class method_env *env) const;
public:
	void load_members(const lib::IniParser &db, class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	bool checktypes(const class integer_tree *t) const throw();
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


class MEMBER_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	MEMBER_tree(class edd_tree * const _p, const lib::pos &p);
	~MEMBER_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	const lib::IDENTIFIER & ident(void) const throw();
public:
	class REFERENCE_BASE_tree * getref(void) const;
public:
	std::string ref_label(class method_env *env);
public:
	std::string ref_help(class method_env *env);
public:
	bool have_description(void) const throw();
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res) const;
public:
	class EDD_OBJECT_tree * value_object(class method_env *env) const;
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	class edd_type * synth_ref(void);
public:
	class EDD_OBJECT_tree * adt_object(void) const;
public:
	bool adt_capable(void) const;
public:
	value_adt_ptr value_adt(void) const;
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


/**
 * The COMMAND node.
 * The COMMAND is a construct to support mapping of the EDD communication
 * elements to a chosen communication system. It specifies various elements
 * required to build a communication frame. If this construct is used by an
 * EDDL profile, every data item needing to be transmitted shall be referenced
 * inside a COMMAND and also in the upload -/download MENU (see 5.11.2.10).
 * 
 * \note The address field of a communication frame is specified using the
 * selectable constructs BLOCK / SLOT / NUMBER together with INDEX and MODULE
 * as applicable. If multiple EDD application process instances or one EDD
 * application process instance which needs an explicit reference exist in a
 * device, these are specified by the additional construct CONNECTION. The
 * type of the communication frame or its control field i s specified by the
 * OPERATION construct. The data content of the communication frame is
 * specified by the TRANSACTION construct.
 * 
 * \note The scope of BLOCK / SLOT / NUMBER is limited to a given EDD
 * application process instance, i.e. the same BLOCK / SLOT / NUMBER value may
 * be used in conjunction with different CONNECTION values to identify
 * different data items within different EDD application process instances.
 */
class COMMAND_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[11];
	static const unsigned long *_types_flat[11];
	static const unsigned long *_types_recursiv[11];
	
	class tree *_attr[11];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
	public:
		struct command_info
		{
			class COMMAND_tree *command;
			int transaction;

			/* if INDEX is used; empty if not needed */
			std::vector<class VARIABLE_tree *> index_vars;
			std::vector<int> index_values;
		};
		typedef struct command_info command_info_t;

		struct search_info
		{
			std::vector<command_info_t> optimal;
			std::vector<command_info_t> suboptimal;

			bool optimal_found(void) const throw() { return !optimal.empty(); }
		};
		typedef struct search_info search_info_t;
	

public:
	COMMAND_tree(class edd_tree * const _p, const lib::pos &p);
	~COMMAND_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void check_range(unsigned long nr, long v, long min, long max, bool out_hex) const;
private:
	void check_slot(unsigned long nr, long v) const;
private:
	void check_subslot(unsigned long nr, long v) const;
private:
	void check_index(unsigned long nr, long v) const;
public:
	long slot(class method_env *env) const;
public:
	long subslot(class method_env *env) const;
public:
	long index(class method_env *env) const;
public:
	class BLOCK_tree * block(class method_env *env) const;
public:
	long number(class method_env *env) const;
private:
	class integer_tree * get_number(void) const;
public:
	bool check_number(int nr) const;
public:
	unsigned long operation(class method_env *env) const;
public:
	bool check_transaction_nr(int transaction_nr) const;
private:
	class TRANSACTION_tree * lookup_transaction(int transaction_nr) const;
public:
	std::vector<class DATA_ITEM_tree *> request_items(int transaction_nr, class method_env *env) const;
public:
	std::vector<class DATA_ITEM_tree *> reply_items(int transaction_nr, class method_env *env) const;
public:
	std::vector<class DATA_ITEM_tree *> reply_items_safe(int transaction_nr, class method_env *env) const;
public:
	size_t reply_item_size(int transaction_nr, int data_item_nr) const;
public:
	size_t transaction_io_size(int transaction_nr) const throw();
public:
	bool no_explicit_transactions(void) const throw();
private:
	int search_trans_nr0(const unsigned char *data, size_t request_size, int trans_nr) const;
public:
	int search_trans_nr(const unsigned char *data, size_t request_size) const;
public:
	class CONNECTION_tree * connection(class method_env *env) const;
public:
	std::string module(class method_env *env) const;
public:
	std::string header(class method_env *env) const;
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
public:
	class RESPONSE_CODE_tree * lookup_response_code(long number, class method_env *env, long trans_nr) const;
private:
	void check_read_write0(class enum_tree *t, bool read, bool &ret) const;
public:
	bool check_read_write(bool read) const;
public:
	void search_io_object(class tree *object, unsigned long op, class method_env *env, search_info_t &search_info);
public:
	bool search_io_variable(class tree *var, unsigned long op, class method_env *env, search_info_t &search_info);
public:
	operand_ptr value(class method_env *env);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void profile_check_PROFIxxx_transaction_read(class enum_tree *t) const;
private:
	void profile_check_PROFIxxx_transaction_write(class enum_tree *t) const;
private:
	void profile_check_PROFIxxx_transaction(class enum_tree *t) const;
private:
	void profile_check_PROFIxxx(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


class TRANSACTION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 size_t request_io_size; 

private:
 size_t reply_io_size; 

public:
	TRANSACTION_tree(class edd_tree * const _p, const lib::pos &p);
	~TRANSACTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	long number(void) const throw();
public:
	bool have_request(void) const throw();
public:
	lib::pos request_pos(void) const throw();
public:
	operand_ptr request(class method_env *env) const;
public:
	bool have_reply(void) const throw();
public:
	lib::pos reply_pos(void) const throw();
public:
	operand_ptr reply(class method_env *env) const;
public:
	void reply_safe(class method_env *env, std::vector<class DATA_ITEM_tree *> &ret) const;
public:
	size_t reply_item_size(int data_item_nr) const;
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
private:
	bool lookup_1(class tree *object, operand_ptr list, class method_env *env) const;
public:
	bool lookup_1(class tree *object, unsigned long op, class method_env *env) const;
private:
	std::vector<class VARIABLE_tree *> get_index_vars(class method_env *env) const;
private:
	bool search_var(class tree *var, class method_env *env, unsigned long nr, const std::vector<class VARIABLE_tree *> &index_vars, std::vector<int> &index) const;
public:
	bool lookup_2(class tree *var, unsigned long op, class method_env *env, std::vector<class VARIABLE_tree *> &index_vars, std::vector<int> &index) const;
public:
	size_t get_request_io_size(void) const throw();
public:
	size_t get_reply_io_size(void) const throw();
public:
	size_t get_io_size(void) const throw();
private:
	void check_io_size(unsigned long attr_nr, size_t &io_size);
public:
	operand_ptr eval(class method_env *env);
private:
	bool check_mask(std::auto_ptr<lib::vbitset> &mask, const lib::vbitset *item_mask, const lib::pos &data_item_pos) const;
private:
	void check_masks(unsigned long attr_nr) const;
private:
	void iterate_items(unsigned long attr, int mask);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


class DATA_ITEM_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 size_t io_size; 

public:
	DATA_ITEM_tree(class edd_tree * const _p, const lib::pos &p);
	~DATA_ITEM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
public:
	bool is_constant(void) const throw();
public:
	long get_constant(void) const throw();
public:
	operand_ptr get_value(class method_env *env) const;
public:
	class EDD_OBJECT_tree * get_object(class method_env *env) const;
public:
	bool lookup(class tree *t, class method_env *env) const;
public:
	bool search_var(class tree *var, class method_env *env, const std::vector<class VARIABLE_tree *> &index_vars, std::vector<int> &index) const;
public:
	const lib::vbitset * mask(void) const throw();
public:
	bool is_index(void) const throw();
public:
	bool is_info(void) const throw();
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	size_t get_io_size(void) const throw();
private:
	void check_type_var_mask(class edd_type *l) const;
private:
	void check_size(class edd_type *l);
private:
	void check_type(void);
private:
	void error_mask(void) const;
private:
	void error_qualifier(void) const;
protected:
	void pass1(void);
};


/**
 * The CONNECTION node.
 * A device may include multiple EDD applications. Each EDD application is
 * represented as an EDD application process instance. The basic construct
 * CONNECTION provides a reference to an individual EDD application process
 * instance within a device.
 * \par
 * The name associated with a CONNECTION construct may be used within the
 * COMMAND attribute CONNECTION.
 */
class CONNECTION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	CONNECTION_tree(class edd_tree * const _p, const lib::pos &p);
	~CONNECTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr value(class method_env *env);
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


/**
 * The DOMAIN node.
 * A DOMAIN represents a memory space in a device. It may contain programs or
 * data. The identifier specifies the corresponding DOMAIN in the device. The
 * data, programs (which are allocated in the DOMAIN), size, access, etc. are
 * specified outside of the EDD.
 * 
 * \note DOMAIN can be used to transfer large amounts of data to and from a
 * device.
 * 
 * \note To allow DOMAIN upload or download the device needs to support DOMAIN
 * specific services.
 */
class DOMAIN_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	DOMAIN_tree(class edd_tree * const _p, const lib::pos &p);
	~DOMAIN_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
public:
	bool have_handling(void) const throw();
public:
	bool have_handling_expr(void) const throw();
protected:
	unsigned long handling(class method_env *env) const;
public:
	bool is_readable(class method_env *env) const;
public:
	bool is_writeable(class method_env *env) const;
protected:
	void xml_print_handling(lib::dyn_buf &buf, class method_env *env) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
};


/**
 * The EDIT_DISPLAY node.
 * An EDIT_DISPLAY defines how data will be managed for display and editing
 * purposes. It is used to group items together for editing.
 * 
 * \note This construct is included for backwards compatibility. For future
 * implementation the more general basic construct MENU is recommended.
 */
class EDIT_DISPLAY_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[5];
	static const unsigned long *_types_flat[5];
	static const unsigned long *_types_recursiv[5];
	
	class tree *_attr[5];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	EDIT_DISPLAY_tree(class edd_tree * const _p, const lib::pos &p);
	~EDIT_DISPLAY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void eval_items(class method_env *env, std::vector<class EDIT_DISPLAY_ITEM_tree *> &items, unsigned long attr_nr) const;
public:
	std::vector<class EDIT_DISPLAY_ITEM_tree *> edit_items(class method_env *env) const;
public:
	std::vector<class EDIT_DISPLAY_ITEM_tree *> display_items(class method_env *env) const;
private:
	void check_items(unsigned long i, size_t profilemask) const;
protected:
	long actions2attr(actions_t type) const throw();
protected:
	void pass1(void);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
private:
	void xml_print_items(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse, unsigned long attr_nr, const char *name);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_FIELDBUS(void) const;
};


/**
 * The EDIT_DISPLAY_ITEM node.
 */
class EDIT_DISPLAY_ITEM_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	EDIT_DISPLAY_ITEM_tree(class edd_tree * const _p, const lib::pos &p);
	~EDIT_DISPLAY_ITEM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class REFERENCE_BASE_tree * getref(void) const;
private:
	std::string label_help(class method_env *env, bool labelflag) const;
public:
	std::string label(class method_env *env) const;
public:
	std::string help(class method_env *env) const;
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


class COMPONENT_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[18];
	static const unsigned long *_types_flat[18];
	static const unsigned long *_types_recursiv[18];
	
	class tree *_attr[18];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		std::auto_ptr<lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> > hash_;
		COMPONENT_tree *modules;
		bool found_relation;
		COMPONENT_tree *parent;
		uint32 slot;
		operand_ptr addressing;

	

public:
	COMPONENT_tree(class edd_tree * const _p, const lib::pos &p);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void check_supported_protocol(void);
private:
	std::string protocol_str(class method_env *env);
private:
	void check_supported_classification(void);
public:
	 ~COMPONENT_tree(void);
public:
	void add_addressing(operand_ptr addr);
private:
	void store_addressing(void);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
public:
	bool has_scan_method(void);
public:
	void run_scan_method(void);
public:
	void build_module_list(void);
private:
	operand_ptr get_module_list(const lib::IDENTIFIER &id);
private:
	template<typename T> void get_module_minmax(T t, int32 &min_value, int32 &max_value);
private:
	static class COMPONENTS_REFERENCE_tree * get_module_from_ref_list(operand_ptr op, const lib::IDENTIFIER &id);
private:
	static bool not_in_list(operand_ptr op, const lib::IDENTIFIER &id);
private:
	void check_relation_type_child(class COMPONENT_RELATION_tree *t);
private:
	void check_relation_type_next_prev(class COMPONENT_RELATION_tree *t, int inc);
private:
	void check_relation_type_sibling(class COMPONENT_RELATION_tree *t);
private:
	void check_relation_type_parent(class COMPONENT_RELATION_tree *t);
public:
	void check_modules(void);
public:
	class COMPONENT_tree * get_module(long index) const;
public:
	lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> * hash(void);
public:
	void build_hashtable(void);
public:
	void set_component_parent(class COMPONENT_tree *parent);
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr value(class method_env *env);
public:
	class EDD_OBJECT_tree * item_list(void) const throw();
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool check_index(long index) const;
public:
	class edd_type * synth(void);
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
};


class COMPONENT_INITIAL_VALUES_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	COMPONENT_INITIAL_VALUES_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENT_INITIAL_VALUES_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
};


class COMPONENT_FOLDER_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	COMPONENT_FOLDER_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENT_FOLDER_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void check_supported_classification(void);
private:
	void check_supported_protocol(void);
private:
	std::string protocol_str(class method_env *env);
protected:
	void pass1(void);
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
};


class COMPONENT_REFERENCE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[7];
	static const unsigned long *_types_flat[7];
	static const unsigned long *_types_recursiv[7];
	
	class tree *_attr[7];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	COMPONENT_REFERENCE_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENT_REFERENCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void check_supported_protocol(void);
private:
	std::string protocol_str(class method_env *env);
private:
	void check_supported_classification(void);
protected:
	void pass1(void);
};


class INTERFACE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	INTERFACE_tree(class edd_tree * const _p, const lib::pos &p);
	~INTERFACE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
};


class COMPONENT_RELATION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[9];
	static const unsigned long *_types_flat[9];
	static const unsigned long *_types_recursiv[9];
	
	class tree *_attr[9];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		lib::hashtable<lib::IDENTIFIER, int> *module_count;
	

public:
	COMPONENT_RELATION_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENT_RELATION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr get_minmaxvalue(bool max);
private:
	void find_component(lib::IDENTIFIER &id, uint32 slot);
public:
	operand_ptr get_addressing_variables(void);
protected:
	void pass1(void);
public:
	operand_ptr value(class method_env *env);
public:
	unsigned int gettype(void);
public:
	operand_ptr get_components(void);
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
};


class COMPONENTS_REFERENCE_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	COMPONENTS_REFERENCE_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENTS_REFERENCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_ref(void);
public:
	operand_ptr get_minmaxvalue(bool max);
protected:
	void pass1(void);
public:
	void lookup_required_range_vars(std::list<class identifier_tree *> &vars) const;
};


class COMPONENT_REQUIRED_RANGE_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[5];
	static const unsigned long *_types_flat[5];
	static const unsigned long *_types_recursiv[5];
	
	class tree *_attr[5];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	COMPONENT_REQUIRED_RANGE_tree(class edd_tree * const _p, const lib::pos &p);
	~COMPONENT_REQUIRED_RANGE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
public:
	std::vector<class MINMAX_VALUE_tree *> get_minmaxvalues(bool flag) const;
private:
	class MINMAX_VALUE_tree * lookup_minmax(class MINMAX_VALUE_tree *t, long nr) const throw();
private:
	void check_minmax(class MINMAX_VALUE_tree *t, const char *t_pref, class MINMAX_VALUE_tree *other, const char *other_pref) const;
protected:
	void pass1(void);
};


/**
 * The FILE node.
 * FILE describes an area of persistent storage that is maintained by the EDD
 * application. Each physical device has it's own copy of the store, i.e., a
 * FILE associated with an instance of a device, not with the type of a
 * device. The MEMBERS attribute defines the structure of the store.
 * 
 * \note FILE is an EDD 1.2 enhancement.
 */
class FILE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 public: typedef members_T<class EDD_OBJECT_tree>::Type members_t; 

private:
 std::vector<std::string> selector_names; 

public:
	FILE_tree(class edd_tree * const _p, const lib::pos &p);
	~FILE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	FILE_tree::members_t members(class method_env *env) const;
public:
	bool lookup_member(class method_env *env, class EDD_OBJECT_tree *object) const;
public:
	void save_members(lib::IniParser &db, class method_env *env) const;
public:
	void load_members(const lib::IniParser &db, class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The GRAPH node.
 * GRAPH describes a graph used to display data from a device in the EDD
 * application. A GRAPH is used to display a data set that is stored in a
 * device, as opposed to a CHART that is used to display continuously
 * collected data values from a device.
 * 
 * \note GRAPH is an EDD 1.2 enhancement.
 */
class GRAPH_tree : public DISPLAY_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[9];
	static const unsigned long *_types_flat[9];
	static const unsigned long *_types_recursiv[9];
	
	class tree *_attr[9];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 public: typedef members_T<class WAVEFORM_tree>::Type members_t; 

private:
 std::vector<std::string> selector_names; 

public:
	GRAPH_tree(class edd_tree * const _p, const lib::pos &p);
	~GRAPH_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	GRAPH_tree::members_t members(class method_env *env) const;
public:
	unsigned int display_height(class method_env *env) const;
public:
	unsigned int display_width(class method_env *env) const;
public:
	bool have_cycle_time(void) const throw();
public:
	long cycle_time(class method_env *env) const;
public:
	class AXIS_tree * x_axis(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The GRID node.
 * GRID describes a matrix of data from a device to be displayed by the EDD
 * application. A GRID is used to display vectors of data along with the
 * heading or description of the data in that vector.  The vectors are
 * displayed horizontally (rows) or vertically (columns) as specified by the
 * ORIENTATION attribute
 * 
 * \note GRID is an EDD 1.2 enhancement.
 */
class GRID_tree : public DISPLAY_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[9];
	static const unsigned long *_types_flat[9];
	static const unsigned long *_types_recursiv[9];
	
	class tree *_attr[9];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	GRID_tree(class edd_tree * const _p, const lib::pos &p);
	~GRID_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	bool have_handling(void) const throw();
public:
	bool have_handling_expr(void) const throw();
protected:
	unsigned long handling(class method_env *env) const;
public:
	bool is_readable(class method_env *env) const;
public:
	bool is_writeable(class method_env *env) const;
protected:
	void xml_print_handling(lib::dyn_buf &buf, class method_env *env) const;
public:
	unsigned int display_height(class method_env *env) const;
public:
	unsigned int display_width(class method_env *env) const;
public:
	bool orientation(void) const;
public:
	std::vector<class VECTOR_tree *> vectors(class method_env *env);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The VECTOR node.
 * The VECTORS attribute specifies the contents of the GRID. Each VECTOR is
 * displayed in order. For a vertical GRID, the vectors are displayed left
 * to right.  For a horizontal GRID, the vectors are displayed top to bottom.
 * Each vector consists of a description that provides a label for the vector
 * followed by the data to be displayed.
 */
class VECTOR_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VECTOR_tree(class edd_tree * const _p, const lib::pos &p);
	~VECTOR_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::string label(class method_env *env) const;
public:
	std::vector<operand_ptr> values(class method_env *env) const;
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	void xml_eval(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


/**
 * The IMAGE node.
 * IMAGE describes a visual image.
 * 
 * \note IMAGE is an EDD 1.2 enhancement.
 */
class IMAGE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 unsigned long pic_sizes; 

public:
	IMAGE_tree(class edd_tree * const _p, const lib::pos &p);
	~IMAGE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	bool have_link(void) const throw();
public:
	class EDD_OBJECT_tree * link(class method_env *env);
public:
	std::string path(class method_env *env);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void check_format(const std::string &path, const lib::pos &pos);
public:
	unsigned long get_pic_sizes(void) const throw();
private:
	void check_size(const std::string &path, const lib::pos &pos);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The LIST node.
 * LIST describes a sequence of elements where each element of the sequence
 * is defined by a EDDL construct instance.
 * 
 * The CAPACITY attribute specifies the maximum number of
 * elements that may be stored in the list. When a LIST is
 * part of a FILE, the CAPACITY shall not be specified since
 * the capacity is only limited by the resources of the EDD
 * application.
 * 
 * The COUNT attribute specifies the numbers of elements
 * currently stored in the list. When a LIST is part of a
 * FILE, the COUNT shall not be specified since only the EDD
 * application knows how many elements are currently in the
 * list.
 * 
 * If COUNT is specified, it defines the size that it has at
 * creation time of the LIST instance. If it is not defined,
 * a LIST instance with zero elements is created.
 * 
 * \note LIST is an EDD 1.2 enhancement.
 */
class LIST_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[7];
	static const unsigned long *_types_flat[7];
	static const unsigned long *_types_recursiv[7];
	
	class tree *_attr[7];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class VARIABLE_tree *count_ref;
		class EDD_OBJECT_tree *type;
		bool type_done;
		long fixed_length_;

		list_value_ptr myvalue;
	

public:
	LIST_tree(class edd_tree * const _p, const lib::pos &p);
	~LIST_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	long capacity(void) const;
public:
	long count(void) const;
public:
	void dump(lib::stream &f) const;
public:
	class EDD_OBJECT_tree * get_type(void) const throw();
public:
	void check_compatibility(class EDD_OBJECT_tree *object, const lib::pos &refpos);
private:
	void check_count_ref(class REFERENCE_IDENT_tree *t);
private:
	void lookup_type(void);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	bool check_index(long index) const;
public:
	bool check_index_runtime(class method_env *env, long index);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
public:
	void update_count(class method_env *env, long size);
public:
	long fixed_length(void) const throw();
public:
	long lookup_length(operand_ptr op);
public:
	long length(class method_env *env);
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_value_object(void) const throw();
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
public:
	bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
public:
	void delete_at(operand_ptr op, long index);
public:
	void delete_at(class method_env *env, long index);
public:
	void insert_at(operand_ptr op, long index, operand_ptr val);
public:
	void insert_at(class method_env *env, long index, operand_ptr val);
public:
	void insert_end(operand_ptr op, operand_ptr val);
public:
	void insert_end(class method_env *env, operand_ptr val);
public:
	operand_ptr selector_eval_list(class method_env *env, const lib::IDENTIFIER &id, const list_value &value);
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	void save(lib::IniParser &db) const;
public:
	void load(const lib::IniParser &db);
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The IMPORTED node.
 * The EDDL constructs previously specified in this clause are sufficient for
 * describing any single device. However, additional mechanisms are required
 * to describe multiple revisions of a single device or to describe parts of a
 * generic device. To provide this mechanisms, one EDD can import another EDD.
 * The imported EDD itself can also import other EDDs.
 * 
 * \note With these mechanisms the EDD of a new device revision can be
 * specified by simply importing the EDD of the old device revision and
 * specifying the changes of the items. This type of EDD is called delta
 * description, because the entire EDD is specified as changes to an existing
 * EDD.
 * 
 * Three import types shall be supported:
 * - importing all items, where all items of the external EDD are imported.
 * - importing items of a specified type, where only the items of the
 * specified types are imported. If a specified type is imported, further
 * imports of specific items of the same type are not allowed.
 * - importing a specific item. The identifier of an imported EDD element
 * shall not be changed.
 * .
 */
class IMPORTED_DD_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	IMPORTED_DD_tree(class edd_tree * const _p, const lib::pos &p);
	~IMPORTED_DD_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class EDD_OBJECT_tree * run(class ROOT_tree *self, const std::vector<std::string> &includes, const std::vector<std::string> &defines) const;
private:
	void check_profile(class ROOT_tree *root, const std::string &name) const;
private:
	class EDD_OBJECT_tree * run1(class ROOT_tree *self, class IDENTIFICATION_tree *identification, const std::vector<std::string> &includes, const std::vector<std::string> &defines) const;
private:
	class EDD_OBJECT_tree * run1(class ROOT_tree *self, const std::string &identification, const std::vector<std::string> &includes, const std::vector<std::string> &defines) const;
private:
	class EDD_OBJECT_tree * run_imports(class ROOT_tree *root, lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash) const;
private:
	void run_redefinitions(class tree **head) const;
protected:
	void pass1(void);
};


class IMPORT_ACTION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	IMPORT_ACTION_tree(class edd_tree * const _p, const lib::pos &p);
	~IMPORT_ACTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	int type_match(class tree *t) const throw();
private:
	void move_object(class EDD_OBJECT_tree **head, class ROOT_tree *root, class EDD_OBJECT_tree *object, std::map<lib::IDENTIFIER, lib::pos> &imported_objects) const;
public:
	class EDD_OBJECT_tree * run(class ROOT_tree *root, lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash, std::map<lib::IDENTIFIER, lib::pos> &imported_objects) const;
protected:
	void pass1(void);
};


class IMPORT_ACTION_REDEFINITION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	IMPORT_ACTION_REDEFINITION_tree(class edd_tree * const _p, const lib::pos &p);
	~IMPORT_ACTION_REDEFINITION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class pseudo_tree * release_list(void) throw();
public:
	void dump(lib::stream &f) const;
private:
	class tree * search(class tree *t) const;
public:
	void run(class tree **head);
};


/**
 * The LIKE node.
 * LIKE is used to create a new EDD instance based on an existing item.
 * LIKE makes a copy of the existing item type with all attributes.
 */
class LIKE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	LIKE_tree(class edd_tree * const _p, const lib::pos &p);
	~LIKE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class EDD_OBJECT_tree * run(lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash, bool &push_back);
public:
	void run_redefinitions(class node_tree *t, class pseudo_tree *l) const;
private:
	void run_anon_redefinitions(class node_tree *t, class ANON_LIKE_ACTION_tree *l) const;
protected:
	void pass1(void);
private:
	void check(void);
private:
	void check_type_redefinition(void);
};


class LIKE_ACTION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 typedef enum { search, remove } action_t; 

public:
	LIKE_ACTION_tree(class edd_tree * const _p, const lib::pos &p);
	~LIKE_ACTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	unsigned long type(void) const throw();
public:
	class tree * get(void) const;
private:
	class tree * release(void);
public:
	void dump(lib::stream &f) const;
private:
	bool equal(class tree *t1, class tree *t2) const;
private:
	int recurse(class node_tree *parent, unsigned long attr, action_t action, const class pseudo_tree *info, class tree *x) const;
public:
	void run(const class LIKE_tree * const parent, class node_tree * const t, const class pseudo_tree * const l);
};


class ANON_LIKE_ACTION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	ANON_LIKE_ACTION_tree(class edd_tree * const _p, const lib::pos &p);
	~ANON_LIKE_ACTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	void run(const class LIKE_tree * const parent, class node_tree * const t);
public:
	void check_type_redefinition(void) const;
};


/**
 * The MENU node.
 * MENU organises variables, methods, and other items specified in the EDDL
 * into a hierarchical structure. A user application may use the MENU ITEMS
 * to display and enter information in an organised and consistent fashion.
 */
class MENU_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[16];
	static const unsigned long *_types_flat[16];
	static const unsigned long *_types_recursiv[16];
	
	class tree *_attr[16];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
	public:
		enum gui_type
		{
			GUI_unknown = 0,
			GUI_none,
			GUI_OnlineWindow,
			GUI_OfflineWindow,
			GUI_OnlineDialog,
			GUI_OfflineDialog,
			GUI_Page,
			GUI_Group,
			GUI_Menu,		// EDD 1.2
			GUI_Table,		// EDD 1.2

			/* PDM specific */
			GUI_BarVal,
			GUI_VertBar,
			GUI_Val,
			GUI_YT_Diagram
		};
	

public:
	MENU_tree(class edd_tree * const _p, const lib::pos &p);
	~MENU_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
private:
	class edd_type * synth_items(void) const;
private:
	operand_ptr eval_items(class method_env *env) const;
public:
	bool have_access(void) const throw();
public:
	bool online(class method_env *env) const;
public:
	bool offline(class method_env *env) const;
public:
	bool have_role(void) const throw();
public:
	std::vector<std::string> entry(class method_env *env) const;
public:
	std::vector<std::string> purpose(class method_env *env) const;
public:
	std::vector<std::string> role(class method_env *env) const;
private:
	unsigned long style(class method_env *env) const;
private:
	std::string style_str(class method_env *env) const;
public:
	static const char * style_descr(unsigned long style) throw();
public:
	int all_styles_mask(void) const throw();
protected:
	long actions2attr(actions_t type) const throw();
protected:
	void concatenate_item_strings(class tree *t) const;
private:
	void check_review0(const lib::pos &review_pos);
public:
	void check_review(const lib::pos &review_pos);
private:
	void check_circular_references0(class MENU_tree *check);
public:
	void check_circular_references(class MENU_tree *check);
private:
	bool check_kind(const unsigned long *ptr, unsigned long check) const;
private:
	void check_item(class MENU_ITEM_tree *item, int &item_styles, bool style_menu);
private:
	void check_item_style0(class edd_type *l, int styles_mask, int child_styles_mask);
private:
	void check_item_style(class MENU_ITEM_tree *item, int styles_mask, int child_styles_mask);
private:
	void check_items(void);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
private:
	void check_PDM_elements(void);
private:
	void check_PDM_element_BarGraph(void);
private:
	void check_PDM_element_Val(void);
private:
	void check_PDM_element_Page(void);
private:
	void check_PDM_element_Group(void);
private:
	bool check_role(class method_env *env) const;
private:
	void check_items_simple0(const unsigned long *expected, bool error, bool recursive);
public:
	void check_items_simple(const unsigned long *expected, bool error, bool recursive);
private:
	void check_item_iec_updownload(class MENU_ITEM_tree *item, bool read_flag);
private:
	void check_items_iec_updownload0(bool read_flag);
public:
	void check_items_iec_updownload(bool read_flag);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
protected:
	void xml_print_name(lib::dyn_buf &buf, class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	static const char * get_gui_type_descr(enum gui_type style) throw();
public:
	const char * get_gui_type_descr(class method_env *env);
public:
	enum MENU_tree::gui_type get_gui_type(class method_env *env);
private:
	enum MENU_tree::gui_type gui_type_edd12_style(class method_env *env);
private:
	enum MENU_tree::gui_type gui_type_pdm_style(class method_env *env);
private:
	int get_pdm_ident_style(void) const;
private:
	bool check_deprecated_ident_style(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


/**
 * The MENU_ITEM node.
 * The ITEMS attribute specifies selected elements (VARIABLE, BLOCK_A
 * PARAMETERS, elements of BLOCK_A PARAMETERS, EDIT_DISPLAY, METHOD and
 * other MENUs) of the EDD which shall be displayed to the user plus optional
 * qualifiers for each element. These qualifiers (DISPLAY_VALUE, HIDDEN,
 * READ_ONLY, REVIEW) are used to control the item processing.
 * 
 * \note The MENU items are presented to the user in the order they appear.
 * 
 * \note For vertical menus, the first item is displayed on top and the last
 * item on the bottom. For horizontal menus, the first item is displayed on
 * the left and the last item on the right.
 */
class MENU_ITEM_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class tree *bit_index; 

public:
	MENU_ITEM_tree(class edd_tree * const _p, const lib::pos &p);
	~MENU_ITEM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool is_reference(void) const throw();
public:
	bool is_format(void) const throw();
public:
	bool is_string(void) const throw();
public:
	bool is_enum_ref(void) const throw();
public:
	class MENU_ITEM_STRING_tree * get_string_node(void) const;
public:
	static std::string label_help_concatenation(const label_help_stack_t &str);
private:
	std::string label_help(class method_env *env, bool labelflag) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	class node_tree * deref(class method_env *env);
private:
	class REFERENCE_BASE_tree * getref(class method_env *env) const;
public:
	class tree * get_args(void) const throw();
public:
	operand_ptr args(class method_env *env) const;
public:
	operand_ptr run(class tree *t, class method_env *env);
public:
	int qualifier(void) const throw();
public:
	bool is_review(void) const throw();
public:
	bool is_display_value(void) const throw();
public:
	bool is_read_only(void) const throw();
public:
	bool is_hidden(void) const throw();
public:
	bool no_label(void) const throw();
public:
	bool no_unit(void) const throw();
public:
	bool is_inline(void) const throw();
private:
	static std::string qualifier_descr(unsigned long mask);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
private:
	void check_qualifiers(void) const;
private:
	void check_method_or_enum_ref(void);
protected:
	void pass1(void);
public:
	void xml_eval(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
protected:
	void profile_check(void) const;
protected:
	void profile_check_HART(void) const;
};


class MENU_ITEM_FORMAT_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 mutable lib::IDENTIFIER myid; 

public:
	MENU_ITEM_FORMAT_tree(class edd_tree * const _p, const lib::pos &p);
	~MENU_ITEM_FORMAT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool is_separator(void) const;
public:
	bool is_columnbreak(void) const;
public:
	bool is_rowbreak(void) const;
public:
	const lib::IDENTIFIER & ident(void) const throw();
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr eval(class method_env *env);
};


class MENU_ITEM_STRING_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 mutable lib::IDENTIFIER myid; 

public:
	MENU_ITEM_STRING_tree(class edd_tree * const _p, const lib::pos &p);
	~MENU_ITEM_STRING_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::string label(class method_env *env) const;
public:
	std::string get(class method_env *env) const;
public:
	bool concat(class MENU_ITEM_STRING_tree *str);
public:
	const lib::IDENTIFIER & ident(void) const throw();
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
public:
	operand_ptr eval(class method_env *env);
};


class MENU_ITEM_ENUM_REF_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 mutable lib::IDENTIFIER myid; 

public:
	MENU_ITEM_ENUM_REF_tree(class edd_tree * const _p, const lib::pos &p);
	~MENU_ITEM_ENUM_REF_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::string label(class method_env *env) const;
public:
	std::string help(class method_env *env) const;
public:
	std::string get(class method_env *env) const;
private:
	void get0(class method_env *env, std::string &label, std::string &help) const;
public:
	const lib::IDENTIFIER & ident(void) const throw();
protected:
	void profile_check(void) const;
public:
	operand_ptr eval(class method_env *env);
};


/**
 * The METHOD node.
 * A METHOD is used to define a subroutine which is executed in the EDD
 * application.
 * 
 * \note A METHOD is used to specify consistency checks, conversion
 * algorithms between EDD variables, user acknowledges or specific device
 * interactions.
 */
class METHOD_tree : public METHOD_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[10];
	static const unsigned long *_types_flat[10];
	static const unsigned long *_types_recursiv[10];
	
	class tree *_attr[10];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class SMachine sm;
		class c_method *mymethod;
		const class c_method *special;
		bool empty_;
		bool cloned;
	

public:
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	unsigned long variable_class(class method_env *env) const;
public:
	bool is_hidden(class method_env *env) const;
public:
	bool have_access(void) const throw();
public:
	bool online(class method_env *env) const;
public:
	bool offline(class method_env *env) const;
public:
	bool have_handling(void) const throw();
public:
	bool have_handling_expr(void) const throw();
protected:
	unsigned long handling(class method_env *env) const;
public:
	bool is_readable(class method_env *env) const;
public:
	bool is_writeable(class method_env *env) const;
protected:
	void xml_print_handling(lib::dyn_buf &buf, class method_env *env) const;
public:
	 METHOD_tree(class edd_tree *_p, const lib::pos &p);
public:
	 ~METHOD_tree(void);
public:
	void dump_sm(lib::stream &f) const;
private:
	class SMachine * get_sm(void);
public:
	bool empty(void) const throw();
private:
	void clone_local(class node_tree *t) const;
public:
	bool check_dd_item_parameter(void) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	operand_ptr execute(class method_env *env, operand_ptr args, const lib::pos &caller_pos);
public:
	c_variable_ptr parameter_lookup(const lib::IDENTIFIER &id) const;
public:
	c_type::typespec returntype(void) const;
public:
	unsigned int parameter_count(void) const;
public:
	class METHOD_PARAMETER_tree * parameter(unsigned int index) const;
public:
	const class c_method * getmethod(void) const;
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
};


class METHOD_DEFINITION_tree : public METHOD_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class SMachine sm;
		class c_method *mymethod;
	

public:
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	 METHOD_DEFINITION_tree(class edd_tree *_p, const lib::pos &p);
public:
	 ~METHOD_DEFINITION_tree(void);
public:
	void dump_sm(lib::stream &f) const;
private:
	class SMachine * get_sm(void);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr execute(class method_env *env, operand_ptr args, const lib::pos &caller_pos);
public:
	c_variable_ptr parameter_lookup(const lib::IDENTIFIER &id) const;
public:
	const class c_method * getmethod(void) const;
};


class METHOD_PARAMETER_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 c_variable_ptr v; 

public:
	METHOD_PARAMETER_tree(class edd_tree * const _p, const lib::pos &p);
	~METHOD_PARAMETER_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	lib::IDENTIFIER getident(void) const throw();
public:
	bool is_dd_item(void) const throw();
public:
	bool byref(void) const throw();
public:
	c_variable_ptr deref(unsigned int index);
private:
	const char * type_descr(void) const;
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	class c_type c_synth_flat(void);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


/**
 * The PLUGIN node.
 */
class PLUGIN_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[5];
	static const unsigned long *_types_flat[5];
	static const unsigned long *_types_recursiv[5];
	
	class tree *_attr[5];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	PLUGIN_tree(class edd_tree * const _p, const lib::pos &p);
	~PLUGIN_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
};


/**
 * The OPEN_CLOSE node.
 * The output of the EDDL processor can be written to output files. The OPEN
 * keyword instructs the EDDL processor to create a separate output file for
 * the EDD constructs and the CLOSE keyword to close the output file. An
 * attempt to open an output file that is already open results in an error.
 * Opening and closing the same output file more than once in an EDD causes
 * the processor to completely overwrite the contents of the file each time
 * it is opened.
 * - The OPEN keyword opens an output file in overwrite mode. Each time an
 * OPEN keyword is processed, an output file is opened, and created if
 * necessary, unless the file is already open, in which case an error results.
 * All objects generated are written to all open files.
 * - The CLOSE keyword prevents further output to an open file. If a file is
 * reopened after closing, then any following objects overwrite the file.
 * .
 * 
 * \note Not supported, ignored for now.
 */
class OPEN_CLOSE_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	OPEN_CLOSE_tree(class edd_tree * const _p, const lib::pos &p);
	~OPEN_CLOSE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
};


/**
 * The PROGRAM node.
 * PROGRAM allows remote control for execution of a program.
 * \par
 * EXAMPLE: A device could download a PROGRAM into a domain of
 * another device using a download service and then remotely
 * operate the PROGRAM by issuing a service request. PROGRAMs
 * should be created, deleted, started. stopped, etc.
 */
class PROGRAM_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	PROGRAM_tree(class edd_tree * const _p, const lib::pos &p);
	~PROGRAM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
protected:
	void pass1(void);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
public:
	operand_ptr value(class method_env *env);
};


/**
 * The RECORD node.
 * A RECORD is a logical group of VARIABLEs. Each item in the RECORD is a
 * reference to a VARIABLE and may be of different data types.
 * 
 * \note A RECORD may be transfered as one communication object.
 */
class RECORD_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[6];
	static const unsigned long *_types_flat[6];
	static const unsigned long *_types_recursiv[6];
	
	class tree *_attr[6];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 std::vector<std::string> selector_names; 

public:
	RECORD_tree(class edd_tree * const _p, const lib::pos &p);
	~RECORD_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	std::vector<MEMBER_tree *> members(class method_env *env) const;
public:
	bool lookup_member(class method_env *env, class EDD_OBJECT_tree *object) const;
public:
	bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
public:
	void save_members(lib::IniParser &db, class method_env *env) const;
public:
	void load_members(const lib::IniParser &db, class method_env *env);
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
protected:
	void pass1(void);
protected:
	void profile_check_HART(void) const;
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_FIELDBUS(void) const;
};


/**
 * The REFERENCE_ARRAY node.
 * A REFERENCE_ARRAY is a set of items of the specified item type (e.g.
 * VARIABLE or MENU).
 * 
 * \note REFERENCE_ARRAYs are not related to communication arrays (item
 * type "VALUE_ARRAY"). Communication arrays are arrays of values.
 * 
 * \note Specified as REFERENCE_ARRAY in the CENELEC specification.
 */
class REFERENCE_ARRAY_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[7];
	static const unsigned long *_types_flat[7];
	static const unsigned long *_types_recursiv[7];
	
	class tree *_attr[7];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 typedef std::map<long, class REFERENCE_ARRAY_ELEMENT_tree *> check_elements_t; 

public:
	REFERENCE_ARRAY_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_ARRAY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval_elements(class method_env *env) const;
public:
	std::vector<REFERENCE_ARRAY_ELEMENT_tree *> elements(class method_env *env) const;
protected:
	std::string element_label_help0(class method_env *env, bool labelflag, long index);
public:
	std::string element_label(class method_env *env, long index);
public:
	std::string element_help(class method_env *env, long index);
public:
	operand_ptr element_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, long index);
public:
	bool lookup_element(class method_env *env, class EDD_OBJECT_tree *object) const;
public:
	bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
public:
	void save_elements(lib::IniParser &db, class method_env *env) const;
public:
	void load_elements(const lib::IniParser &db, class method_env *env);
public:
	class edd_type * synth(void);
public:
	class edd_type * synth_index(long index);
public:
	class c_type c_synth_flat(void);
public:
	bool checktypes(const class integer_tree *t) const throw();
public:
	bool check_index_runtime(class method_env *env, long index) const;
private:
	void check_unique_element_list_index(class tree *t0, check_elements_t elements) const;
protected:
	void pass1(void);
private:
	void check_keyword(const char *name, bool warning) const;
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


class VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT_tree : public VARIABLE_ENUMERATOR_tree
{
private:
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class REFERENCE_ARRAY_ELEMENT_tree *ref; 

public:
	VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	int enumtype(void) const;
public:
	void set_parent(class REFERENCE_ARRAY_ELEMENT_tree *r);
public:
	operand_ptr enum_value(void) const;
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	void check_description_dependency(void) const;
public:
	operand_ptr eval(class method_env *env);
};


class REFERENCE_ARRAY_ELEMENT_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class VARIABLE_ENUMERATOR_REFERENCE_ARRAY_ELEMENT_tree enumerator; 

public:
	~REFERENCE_ARRAY_ELEMENT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	long index(void) const throw();
public:
	class REFERENCE_BASE_tree * getref(void) const;
public:
	std::string ref_label(class method_env *env);
public:
	std::string ref_help(class method_env *env);
public:
	bool have_description(void) const throw();
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res) const;
public:
	class EDD_OBJECT_tree * value_object(class method_env *env) const;
public:
	class edd_type * synth(void);
public:
	 REFERENCE_ARRAY_ELEMENT_tree(class edd_tree *_p, const lib::pos &p);
public:
	class VARIABLE_ENUMERATOR_tree * get_enumerator(void);
protected:
	void pass1(void);
public:
	class EDD_OBJECT_tree * adt_object(void) const;
public:
	bool adt_capable(void) const;
public:
	value_adt_ptr value_adt(void) const;
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


/**
 * The REFRESH_RELATION node.
 * A REFRESH relation is used during online access and specifies a set of
 * VARIABLEs which should be refreshed (read from the device) whenever a
 * VARIABLE from a specified set is modified.
 * 
 * \note A VARIABLE may have a refresh relationship with itself, implying
 * that the VARIABLE is to be read after writing.
 */
class REFRESH_RELATION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	REFRESH_RELATION_tree(class edd_tree * const _p, const lib::pos &p);
	~REFRESH_RELATION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	bool lookup_variable(const class tree *var, class method_env *env) const;
private:
	std::vector<operand_ptr> deps(class method_env *env) const;
public:
	void run(const class tree *var, class method_env *env);
private:
	bool check_collection(class tree *t);
protected:
	void pass1(void);
public:
	operand_ptr value(class method_env *env);
};


/**
 * The UNIT_RELATION node.
 * A UNIT relation specifies a reference to a VARIABLE holding a unit code and
 * a list of dependent VARIABLEs. When the VARIABLE holding the unit code is
 * modified, the list of effected VARIABLEs using that unit code shall be
 * refreshed. When an effected VARIABLE is displayed, the value of its unit
 * code shall also be displayed.
 */
class UNIT_RELATION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	UNIT_RELATION_tree(class edd_tree * const _p, const lib::pos &p);
	~UNIT_RELATION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class VARIABLE_tree * getref(class method_env *env) const;
public:
	bool lookup_variable(const class tree *var, class method_env *env) const;
public:
	bool search_unit_ref(class tree *search, class method_env *env) const;
public:
	operand_ptr deps(class method_env *env) const;
public:
	operand_ptr var_deps(class method_env *env) const;
public:
	void run(const class tree *var, class method_env *env);
protected:
	void pass1(void);
public:
	operand_ptr value(class method_env *env);
};


/**
 * The WAO_RELATION node.
 * A WRITE_AS_ONE relation specifies a list of VARIABLEs that shall be
 * modified as a group by the EDD application. This relation is used when a
 * device requires specific VARIABLEs to be examined and modified at the same
 * time for proper operation.
 * 
 * \note This relation does not necessarily mean the VARIABLEs are written to
 * the device at the same time. Not all VARIABLEs sent to the device at the
 * same time are necessarily part of a WRITE_AS_ONE relation. It is up to the
 * EDD application to determine how the updates to the parameters of this
 * relation are passed to the device.
 */
class WAO_RELATION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAO_RELATION_tree(class edd_tree * const _p, const lib::pos &p);
	~WAO_RELATION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::vector<class VARIABLE_tree *> refs(class method_env *env) const;
protected:
	void pass1(void);
public:
	operand_ptr value(class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


/**
 * The RESPONSE_CODES_DEFINITION node.
 * RESPONSE_CODES specify values a device may return as error information.
 * Each VARIABLE, RECORD, VALUE_ARRAY, VARIABLE_LIST, PROGRAM, COMMAND or
 * DOMAIN can have its own set of RESPONSE_CODES.
 */
class RESPONSE_CODES_DEFINITION_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	RESPONSE_CODES_DEFINITION_tree(class edd_tree * const _p, const lib::pos &p);
	~RESPONSE_CODES_DEFINITION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
};


class RESPONSE_CODE_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	RESPONSE_CODE_tree(class edd_tree * const _p, const lib::pos &p);
	~RESPONSE_CODE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	long number(void) const;
public:
	unsigned long type(void) const;
public:
	std::string type_descr(void) const;
public:
	bool have_description(void) const throw();
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	operand_ptr eval(class method_env *env);
};


/**
 * The SOURCE node.
 * A SOURCE describes a source of data values for a CHART.
 * 
 * \note SOURCE is an EDD 1.2 enhancement.
 */
class SOURCE_tree : public LINE_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[12];
	static const unsigned long *_types_flat[12];
	static const unsigned long *_types_recursiv[12];
	
	class tree *_attr[12];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 public: typedef members_T<class EDD_OBJECT_tree>::Type members_t; 

private:
 std::vector<std::string> selector_names; 

public:
	SOURCE_tree(class edd_tree * const _p, const lib::pos &p);
	~SOURCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool emphasis(class method_env *env) const;
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	unsigned long line_type(class method_env *env) const;
public:
	bool have_line_color(void) const throw();
public:
	unsigned long line_color(class method_env *env) const;
public:
	class AXIS_tree * y_axis(class method_env *env) const;
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	SOURCE_tree::members_t members(class method_env *env) const;
protected:
	long actions2attr(actions_t type) const throw();
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


/**
 * The VAUE_ARRAY node.
 * A VALUE_ARRAY is a logical group of values. Each element of a VALUE_ARRAY
 * shall be of the same data type and shall correspond to an EDDL construct.
 * An element is referenced from elsewhere in the EDD via the VALUE_ARRAY
 * identifier and the element index.
 * 
 * \note From a communication perspective the individual elements of the
 * VALUE_ARRAY are not treated as individual variables, but simply as a
 * grouped values.
 * 
 * \note Specified as VALUE_ARRAY in the CENELEC specification.
 */
class VALUE_ARRAY_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[7];
	static const unsigned long *_types_flat[7];
	static const unsigned long *_types_recursiv[7];
	
	class tree *_attr[7];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class EDD_OBJECT_tree *type;
		bool type_done;

		value_array_value_ptr myvalue;
	

public:
	VALUE_ARRAY_tree(class edd_tree * const _p, const lib::pos &p);
	~VALUE_ARRAY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	long length(void) const throw();
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
public:
	void dump(lib::stream &f) const;
public:
	class EDD_OBJECT_tree * get_type(void) const throw();
private:
	void lookup_type(bool exception);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	bool check_index(long index) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
public:
	operand_ptr default_lvalue(void);
public:
	operand_ptr initial_lvalue(void);
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_value_object(void) const throw();
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
public:
	bool run_actions_all(actions_t type, class method_env *env, operand_ptr op);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void save(lib::IniParser &db) const;
public:
	void load(const lib::IniParser &db);
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
protected:
	void profile_check_HART(void) const;
};


/**
 * The VARIABLE node.
 * VARIABLE describes parameters contained in a device or in the EDD
 * application.
 * \par
 * This is the abstraction of an EDD VARIABLE. It features all attributes
 * that are specified inside the EDD specification. Most of the methods
 * can be easily accessed with specialized methods (this include the validity
 * check, class, style, constant_unit, handling and so on calculation).
 */
class VARIABLE_tree : public UNIT_REF_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[21];
	static const unsigned long *_types_flat[21];
	static const unsigned long *_types_recursiv[21];
	
	class tree *_attr[21];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		variable_value_adt_ptr myvalue_adt;
		variable_value_ptr myvalue;

		/* relation stuff */
		std::list<class REFRESH_RELATION_tree *> refresh_list;
		std::list<class UNIT_RELATION_tree *> unit_list;
		std::list<class WAO_RELATION_tree *> wao_list;

		long shadow;
		int flags; /* 0x01 - referenced by a command reply
			    * 0x02 - referenced by a command request
			    * 0x10 - unit transition block
			    * 0x20 - force unit transition if unset
			    */
		friend class method_env;
		friend class UNIT_RELATION_tree;
	

public:
	VARIABLE_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	unsigned long variable_class(class method_env *env) const;
public:
	bool is_dynamic(class method_env *env) const;
public:
	bool synth_is_dynamic(void) const;
public:
	bool is_local(class method_env *env) const;
public:
	bool is_hidden(class method_env *env) const;
public:
	bool synth_is_local(void) const;
public:
	std::string style(class method_env *env) const;
protected:
	class tree * have_constant_unit(void) const throw();
protected:
	std::string constant_unit(class method_env *env) const;
public:
	bool have_handling(void) const throw();
public:
	bool have_handling_expr(void) const throw();
protected:
	unsigned long handling(class method_env *env) const;
public:
	bool is_readable(class method_env *env) const;
public:
	bool is_writeable(class method_env *env) const;
protected:
	void xml_print_handling(lib::dyn_buf &buf, class method_env *env) const;
public:
	bool synth_have_HANDLING_READ(void) const;
public:
	bool synth_have_HANDLING_WRITE(void) const;
protected:
	long actions2attr(actions_t type) const throw();
public:
	bool have_read_timeout(void) const throw();
public:
	bool have_write_timeout(void) const throw();
public:
	operand_ptr read_timeout(class method_env *env) const;
public:
	operand_ptr write_timeout(class method_env *env) const;
public:
	class VARIABLE_TYPE_tree * type(void) const throw();
public:
	std::vector<std::string> purpose(class method_env *env) const;
private:
	bool arithmetic_option_convert(class pseudo_tree *pseudo, enum kinds kind, const unsigned long *old_attr, const unsigned long *new_attr) const;
protected:
	void post_construction(bool from_like);
public:
	void dump(lib::stream &f) const;
public:
	void set_cmd_ref(int mask) throw();
public:
	bool can_be_read(void) const throw();
public:
	bool can_be_written(void) const throw();
public:
	class edd_type * synth_index(bool const_index, long index);
public:
	class c_type c_synth_flat(void);
private:
	bool check_c_typespec(c_type::typespec typespec);
private:
	void cmd_unref_warning(int mask, const char *type) const;
private:
	void cmd_ref_warning(int mask, const char *type) const;
private:
	void check_cmd_ref(void) const;
public:
	void check_enum_dependency(void);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	size_t getsize(void) const throw();
public:
	operand_ptr default_lvalue(void);
public:
	operand_ptr initial_lvalue(void);
public:
	operand_ptr blank_lvalue(void);
public:
	operand_ptr unchecked_lvalue(void);
private:
	variable_value_adt * new_value_adt(void);
public:
	bool adt_capable(void);
public:
	value_adt_ptr value_adt(void);
public:
	operand_ptr value(class method_env *env);
public:
	bool is_value_object(void) const throw();
public:
	bool is_io_object(void);
public:
	size_t get_io_size(void);
private:
	void xml_print_type_attr(lib::dyn_buf &buf, class method_env *env) const;
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
private:
	void xml_eval_enums(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
public:
	void xml_type(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool selector_lookup1(const lib::IDENTIFIER &ident, class c_type *type, selector_lookup_result_t &result);
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_default_value(c_type::typespec type, const lib::IDENTIFIER &ident);
public:
	operand_ptr selector_eval_variable(class method_env *env, const lib::IDENTIFIER &id, const variable_value_adt &value);
public:
	bool have_enumerations(void) const throw();
public:
	bool check_enumval_exist(operand_ptr value) const;
public:
	std::vector<class VARIABLE_ENUMERATOR_tree *> get_enumerations(class method_env *env) const;
public:
	class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
public:
	std::vector<class MINMAX_VALUE_tree *> get_minmaxvalues(bool flag) const;
protected:
	bool have_read_write_hook(void) const throw();
protected:
	operand_ptr pre_write_hook(class method_env *env, operand_ptr op);
protected:
	operand_ptr post_read_hook(class method_env *env, operand_ptr op);
public:
	void register_refresh(class REFRESH_RELATION_tree *t);
public:
	void run_refresh(class method_env *env);
private:
	void run_refresh0(class method_env *env);
public:
	void register_unit(class UNIT_RELATION_tree *t);
private:
	void run_unit_refresh(class method_env *env);
private:
	void run_unit_refresh0(class method_env *env);
private:
	void run_unit_transition(class method_env *env);
public:
	void register_wao(class WAO_RELATION_tree *t);
private:
	bool is_arithmetic(void) const;
public:
	void pre_assign_actions(class method_env *env);
public:
	void post_assign_actions(class method_env *env);
public:
	operand_ptr get_device_value(class method_env *env, operand_ptr op);
public:
	void assign_device_value(class method_env *env, operand_ptr op, operand_ptr val);
public:
	void save(lib::IniParser &db) const;
public:
	void load(const lib::IniParser &db);
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
};


class VARIABLE_TYPE_ARITHMETIC_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[12];
	static const unsigned long *_types_flat[12];
	static const unsigned long *_types_recursiv[12];
	
	class tree *_attr[12];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 int flags; 

private:
 bool iec_scaling; 

public:
	VARIABLE_TYPE_ARITHMETIC_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_ARITHMETIC_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	std::string typedescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
private:
	operand_ptr get_upper(void);
private:
	operand_ptr get_lower(void);
private:
	bool check_upper(const operand &op) const;
private:
	bool check_lower(const operand &op) const;
public:
	bool have_display_format(void) const throw();
public:
	std::string get_display_format(class method_env *env);
public:
	bool have_edit_format(void) const throw();
public:
	std::string get_edit_format(class method_env *env);
private:
	std::string eval_format_checked(unsigned long attr_nr, class method_env *env, int flag);
private:
	void check_format(unsigned long attr);
public:
	bool have_enumerations(void) const throw();
public:
	void get_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class method_env *env) const;
public:
	class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
public:
	void check_enum_dependency(void) const;
protected:
	void find_all_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums) const;
public:
	bool have_scaling(void) const throw();
public:
	operand_ptr scaling_factor(class method_env *env) const;
public:
	unsigned long getfinalsubtype(void) const throw();
public:
	bool have_read_write_hook(void) const throw();
public:
	operand_ptr pre_write_hook(class method_env *env, operand_ptr op);
public:
	operand_ptr post_read_hook(class method_env *env, operand_ptr op);
public:
	const class tree * get_default_value(void) const throw();
public:
	const class tree * get_initial_value(void) const throw();
public:
	std::vector<class MINMAX_VALUE_tree *> get_minmaxvalues(bool flag) const;
private:
	class MINMAX_VALUE_tree * lookup_minmax(class MINMAX_VALUE_tree *t, long nr) const throw();
private:
	void check_minmax(class MINMAX_VALUE_tree *t, const char *t_pref, class MINMAX_VALUE_tree *other, const char *other_pref) const;
public:
	bool check_value(const class operand &op, class method_env *env) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void check_0_float_constant0(class edd_type *list) const;
private:
	void check_0_float_constant(class MINMAX_VALUE_tree *values) const;
protected:
	void profile_check_HART(void) const;
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr lvalue(unsigned int first, unsigned int second);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
private:
	bool separate_min_max_nr(const char *s, int &index) const;
public:
	bool selector_lookup1(const lib::IDENTIFIER &ident, class c_type *type, selector_lookup_result_t &result);
public:
	operand_ptr selector_default_value(c_type::typespec type, const lib::IDENTIFIER &ident);
};


class MINMAX_VALUE_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	MINMAX_VALUE_tree(class edd_tree * const _p, const lib::pos &p);
	~MINMAX_VALUE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
public:
	long attr_nr(void) const throw();
protected:
	void pass1(void);
private:
	operand_ptr value0(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


class VARIABLE_TYPE_BOOLEAN_tree : public VARIABLE_TYPE_tree
{
private:
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_TYPE_BOOLEAN_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_BOOLEAN_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
private:
	operand_ptr unchecked_lvalue(void);
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
};


/**
 * \internal
 * Variable enumerated type.
 */
class VARIABLE_TYPE_ENUMERATED_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[5];
	static const unsigned long *_types_flat[5];
	static const unsigned long *_types_recursiv[5];
	
	class tree *_attr[5];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_TYPE_ENUMERATED_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_ENUMERATED_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	std::string typedescr(void) const throw();
public:
	const char * typestring(void) const throw();
private:
	enum kinds enumerator_type(void) const throw();
public:
	size_t get_io_size(void) const throw();
public:
	const class tree * get_default_value(void) const throw();
public:
	const class tree * get_initial_value(void) const throw();
public:
	bool have_enumerations(void) const throw();
public:
	void get_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class method_env *env) const;
public:
	class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
protected:
	void find_all_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums) const;
public:
	bool check_enumval_exist_exact(unsigned long val) const;
public:
	void check_enum_dependency(void) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
public:
	class edd_type * synth_index(class VARIABLE_tree *parent, bool const_index, long index);
public:
	class c_type c_synth_flat(void);
public:
	bool check_value(const class operand &op, class method_env *env) const;
private:
	operand_ptr lvalue(unsigned int first, unsigned int second);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
};


class VARIABLE_TYPE_STRING_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[5];
	static const unsigned long *_types_flat[5];
	static const unsigned long *_types_recursiv[5];
	
	class tree *_attr[5];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_TYPE_STRING_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_STRING_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	std::string typedescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
public:
	size_t length(void) const throw();
public:
	const class tree * get_default_value(void) const throw();
public:
	const class tree * get_initial_value(void) const throw();
private:
	void check_value_string(const std::string &str, class tree *t, unsigned long nr) const;
private:
	void check_default_initial_child(class tree *t, unsigned long nr) const;
public:
	bool have_enumerations(void) const throw();
public:
	void get_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class method_env *env) const;
public:
	class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
public:
	void check_enum_dependency(void) const;
protected:
	void find_all_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums) const;
public:
	static bool check_packed_ascii(const std::string &str);
private:
	void check_and_warn_packed_ascii(class tree *t, const operand_ptr op) const;
public:
	bool check_value(const class operand &op, class method_env *env) const;
private:
	void my_check_edd_type(unsigned long nr, const class c_type &mytype) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void profile_check_FF(void);
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr lvalue(unsigned int first, unsigned int second);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
};


/**
 * \internal
 * \todo pretty_print must display the description of the indices of
 * the REFERENCE_ARRAY this INDEX VARIABLE reference instead the value (if
 * applicable)
 */
class VARIABLE_TYPE_INDEX_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_TYPE_INDEX_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_INDEX_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	std::string typedescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
public:
	const class tree * get_default_value(void) const throw();
public:
	const class tree * get_initial_value(void) const throw();
public:
	bool check_value(const class operand &op, class method_env *env) const;
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
protected:
	void profile_check_HART(void) const;
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr guess_default(class tree *t);
private:
	operand_ptr lvalue(unsigned int first, unsigned int second);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
public:
	bool have_enumerations(void) const throw();
public:
	void get_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums, class method_env *env) const;
public:
	class VARIABLE_ENUMERATOR_tree * get_enumeration(const operand &value, class method_env *env) const;
protected:
	void find_all_enumerations(std::vector<class VARIABLE_ENUMERATOR_tree *> &enums) const;
protected:
	const node_tree::selector_info & get_selector_info(void) const;
};


class VARIABLE_TYPE_DATE_TIME_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[8];
	static const unsigned long *_types_flat[8];
	static const unsigned long *_types_recursiv[8];
	
	class tree *_attr[8];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 int flags; 

public:
	VARIABLE_TYPE_DATE_TIME_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_DATE_TIME_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	unsigned long getsubtype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
private:
	size_t get_io_size_time_value(void) const;
public:
	const class tree * get_default_value(void) const throw();
public:
	const class tree * get_initial_value(void) const throw();
public:
	bool check_value(const class operand &op, class method_env *env) const;
public:
	bool has_scale(void);
public:
	float32 get_scale(void);
public:
	bool have_display_format(void) const throw();
public:
	std::string get_display_format(class method_env *env);
private:
	std::string eval_format_checked(unsigned long attr_nr, class method_env *env, int flag);
public:
	bool have_time_format(void);
public:
	std::string get_time_format(class method_env *env);
private:
	std::string eval_format_checked_time(unsigned long attr_nr, class method_env *env, int flag);
private:
	std::string fmtcheck_time(const std::string &suspect, const std::string &def);
protected:
	void pass1(void);
protected:
	void profile_check_HART(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr lvalue(unsigned int first, unsigned int second);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
};


class VARIABLE_TYPE_OBJECT_tree : public VARIABLE_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_TYPE_OBJECT_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_TYPE_OBJECT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	enum kinds gettype(void) const throw();
public:
	const char * realdescr(void) const throw();
public:
	const char * typestring(void) const throw();
public:
	size_t get_io_size(void) const throw();
protected:
	void pass1(void);
public:
	class c_type c_synth_flat(void);
private:
	operand_ptr default_lvalue(void);
private:
	operand_ptr initial_lvalue(void);
private:
	operand_ptr blank_lvalue(void);
private:
	operand_ptr unchecked_lvalue(void);
};


/**
 * ENUMERATOR specialization.
 * This is the ENUMERATOR object class for simple enumerators.
 * It features only the value, description and help information.
 */
class VARIABLE_ENUMERATOR_ENUM_tree : public VARIABLE_ENUMERATOR_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_ENUMERATOR_ENUM_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_ENUMERATOR_ENUM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	int enumtype(void) const;
public:
	operand_ptr enum_value(void) const;
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	void check_description_dependency(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	operand_ptr eval(class method_env *env);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
protected:
	void pass1(void);
};


/**
 * BITENUMERATOR specialization.
 * This is the BITENUMERATOR object class for bit enumerators.
 * It features the value, description, help, variable class and
 * status class information.
 */
class VARIABLE_ENUMERATOR_BITENUM_tree : public VARIABLE_ENUMERATOR_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[10];
	static const unsigned long *_types_flat[10];
	static const unsigned long *_types_recursiv[10];
	
	class tree *_attr[10];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	VARIABLE_ENUMERATOR_BITENUM_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_ENUMERATOR_BITENUM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	int enumtype(void) const;
public:
	operand_ptr enum_value(void) const;
public:
	long bitposition(void) const throw();
public:
	std::string description(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
public:
	void check_description_dependency(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	unsigned long variable_class(class method_env *env) const;
public:
	long status_class(void) const;
public:
	void run_method(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
protected:
	void pass1(void);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


/**
 * The VARIABLE_LIST node.
 * A VARIABLE_LIST is a group of EDD communication objects (VARIABLE,
 * VALUE_ARRAY or RECORDS). VARIABLE_LISTS are used to group objects for
 * application convenience.
 * 
 * \note A VARIABLE_LIST may be transfered as one communication object.
 */
class VARIABLE_LIST_tree : public EDD_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 std::vector<std::string> selector_names; 

public:
	VARIABLE_LIST_tree(class edd_tree * const _p, const lib::pos &p);
	~VARIABLE_LIST_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * selector_lookup(const lib::IDENTIFIER &ident, const lib::pos &pos);
public:
	operand_ptr selector_eval(class method_env *env, const lib::IDENTIFIER &ident);
public:
	class MEMBER_tree * member_lookup(class method_env *env, const lib::IDENTIFIER &ident);
public:
	operand_ptr member_label_help(class method_env *env, bool labelflag, label_help_stack_t &res, lib::IDENTIFIER id);
public:
	std::string member_label(class method_env *env, lib::IDENTIFIER id);
public:
	std::string member_help(class method_env *env, lib::IDENTIFIER id);
public:
	class RESPONSE_CODE_tree * lookup_response_code0(long number, class method_env *env) const;
protected:
	void pass1(void);
protected:
	void profile_check_PROFIBUS(void) const;
protected:
	void profile_check_PROFINET(void) const;
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	void selector_ids(std::vector<std::string> &result) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_FIELDBUS(void) const;
};


/**
 * The WAVEFORM node.
 * A WAVEFORM describes a data set that may be displayed by a GRAPH.
 * 
 * \note WAVEFORM is an EDD 1.2 enhancement.
 */
class WAVEFORM_tree : public LINE_OBJECT_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[14];
	static const unsigned long *_types_flat[14];
	static const unsigned long *_types_recursiv[14];
	
	class tree *_attr[14];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAVEFORM_tree(class edd_tree * const _p, const lib::pos &p);
	~WAVEFORM_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool have_handling(void) const throw();
public:
	bool have_handling_expr(void) const throw();
protected:
	unsigned long handling(class method_env *env) const;
public:
	bool is_readable(class method_env *env) const;
public:
	bool is_writeable(class method_env *env) const;
protected:
	void xml_print_handling(lib::dyn_buf &buf, class method_env *env) const;
protected:
	long actions2attr(actions_t type) const throw();
public:
	class KEY_POINTS_tree * key_points(void) const throw();
public:
	bool emphasis(class method_env *env) const;
public:
	bool have_validity(void) const throw();
public:
	bool is_valid(class method_env *env) const throw();
public:
	bool have_visibility(void) const throw();
public:
	bool is_visible(class method_env *env) const throw();
public:
	unsigned long line_type(class method_env *env) const;
public:
	bool have_line_color(void) const throw();
public:
	unsigned long line_color(class method_env *env) const;
public:
	class AXIS_tree * y_axis(class method_env *env) const;
public:
	class WAVEFORM_TYPE_tree * type(void) const;
protected:
	void pass1(void);
protected:
	void profile_check(void) const;
public:
	operand_ptr value(class method_env *env);
protected:
	void xml_print_attr(lib::dyn_buf &buf, class method_env *env);
public:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
protected:
	const node_tree::selector_info & get_selector_info(void) const;
public:
	bool have_help(void) const throw();
public:
	std::string help(class method_env *env) const;
public:
	bool have_label(void) const throw();
public:
	std::string label(class method_env *env) const;
protected:
	void profile_check_FIELDBUS(void) const;
protected:
	void profile_check_HART(void) const;
};


class KEY_POINTS_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	KEY_POINTS_tree(class edd_tree * const _p, const lib::pos &p);
	~KEY_POINTS_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::vector<operand_ptr> x_values(class method_env *env) const;
public:
	std::vector<operand_ptr> y_values(class method_env *env) const;
protected:
	void pass1(void);
public:
	void xml_eval(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse) const;
};


class WAVEFORM_TYPE_YT_tree : public WAVEFORM_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAVEFORM_TYPE_YT_tree(class edd_tree * const _p, const lib::pos &p);
	~WAVEFORM_TYPE_YT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	double x_initial(class method_env *env) const;
public:
	double x_increment(class method_env *env) const;
public:
	std::vector<operand_ptr> y_values(class method_env *env) const;
public:
	long number_of_points(class method_env *env) const;
public:
	std::string y_unit(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


class WAVEFORM_TYPE_XY_tree : public WAVEFORM_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAVEFORM_TYPE_XY_tree(class edd_tree * const _p, const lib::pos &p);
	~WAVEFORM_TYPE_XY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::vector<operand_ptr> x_values(class method_env *env) const;
public:
	std::vector<operand_ptr> y_values(class method_env *env) const;
public:
	long number_of_points(class method_env *env) const;
public:
	std::string x_unit(class method_env *env) const;
public:
	std::string y_unit(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


class WAVEFORM_TYPE_HORIZONTAL_tree : public WAVEFORM_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAVEFORM_TYPE_HORIZONTAL_tree(class edd_tree * const _p, const lib::pos &p);
	~WAVEFORM_TYPE_HORIZONTAL_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::vector<operand_ptr> y_values(class method_env *env) const;
public:
	std::string y_unit(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


class WAVEFORM_TYPE_VERTICAL_tree : public WAVEFORM_TYPE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	WAVEFORM_TYPE_VERTICAL_tree(class edd_tree * const _p, const lib::pos &p);
	~WAVEFORM_TYPE_VERTICAL_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	std::vector<operand_ptr> x_values(class method_env *env) const;
public:
	std::string x_unit(class method_env *env) const;
protected:
	void pass1(void);
protected:
	void xml_eval0(lib::dyn_buf &buf, class method_env *env, size_t level, bool recurse);
};


class CONSTRAINT_IF_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	CONSTRAINT_IF_tree(class edd_tree * const _p, const lib::pos &p);
	~CONSTRAINT_IF_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class tree * then_list(void) const throw();
public:
	class tree * else_list(void) const throw();
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
};


class CONSTRAINT_SELECT_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 typedef std::list<class c_type> case_values_t; 

public:
	CONSTRAINT_SELECT_tree(class edd_tree * const _p, const lib::pos &p);
	~CONSTRAINT_SELECT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class CONSTRAINT_SELECTION_tree * select_list(void) const throw();
public:
	class edd_type * synth(void);
private:
	void check_duplicate(case_values_t &case_values, const class c_type &case_value, const lib::pos &case_pos) const;
protected:
	void pass1(void);
protected:
	void profile_check_HART(void) const;
public:
	operand_ptr eval(class method_env *env);
};


class CONSTRAINT_SELECTION_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	CONSTRAINT_SELECTION_tree(class edd_tree * const _p, const lib::pos &p);
	~CONSTRAINT_SELECTION_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class tree * stmt_list(void) const throw();
public:
	bool default_case(void) const;
public:
	operand_ptr case_expr(class method_env *env) const;
public:
	class edd_type * synth(void);
public:
	class c_type check_case(void) const;
public:
	operand_ptr eval(class method_env *env);
};


class EXPR_PRIMARY_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	EXPR_PRIMARY_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_PRIMARY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class tree * get(void) const throw();
public:
	class edd_type * synth(void);
public:
	operand_ptr eval(class method_env *env);
public:
	bool no_effect(void) const;
};


class EXPR_POSTFIX_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype;
		bool done;
	

public:
	EXPR_POSTFIX_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_POSTFIX_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	bool no_effect(void) const;
};


class EXPR_UNARY_tree : public c_unary_expr_base_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		operand_ptr constant;
		class c_type rtype;
		bool done;
	

public:
	EXPR_UNARY_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_UNARY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	enum c_unary_expr_base_tree::ops get_op(void) const;
protected:
	lib::pos expr_pos(void) const;
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
private:
	operand_ptr eval0(operand_ptr op);
public:
	operand_ptr eval(class method_env *env);
};


class EXPR_BINARY_tree : public c_binary_expr_base_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		operand_ptr constant;
		class c_type rtype;
		bool done;
	

public:
	EXPR_BINARY_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_BINARY_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	enum c_binary_expr_base_tree::ops get_op(void) const;
protected:
	lib::pos expr1_pos(void) const;
protected:
	lib::pos expr2_pos(void) const;
public:
	void dump(lib::stream &f) const;
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
private:
	operand_ptr convert(operand_ptr in) const;
private:
	operand_ptr eval0(operand_ptr left_op, operand_ptr right_op);
public:
	operand_ptr eval(class method_env *env);
};


class EXPR_CONDITIONAL_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type result;
		bool done;
	

public:
	EXPR_CONDITIONAL_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_CONDITIONAL_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	bool no_effect(void) const;
};


class EXPR_ASSIGNMENT_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	EXPR_ASSIGNMENT_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_ASSIGNMENT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	lib::IDENTIFIER opmsg(void) const;
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	bool no_effect(void) const;
};


class EXPR_COMMA_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	EXPR_COMMA_tree(class edd_tree * const _p, const lib::pos &p);
	~EXPR_COMMA_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	bool no_effect(void) const;
};


class DICTIONARY_REFERENCE_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 lib::IDENTIFIER id; 

public:
	DICTIONARY_REFERENCE_tree(class edd_tree * const _p, const lib::pos &p);
	~DICTIONARY_REFERENCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth(void);
public:
	void dump(lib::stream &f) const;
private:
	void clone_local(class node_tree *t) const;
protected:
	void pass1(void);
public:
	operand_ptr value(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
};


class TYPED_REFERENCE_tree : public COMMA_CHECK_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	TYPED_REFERENCE_tree(class edd_tree * const _p, const lib::pos &p);
	~TYPED_REFERENCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class REFERENCE_BASE_tree * get(void) const throw();
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
};


class METHOD_REFERENCE_tree : public COMMA_CHECK_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class REFERENCE_CALL_tree *call_reference;
		bool done;
	

public:
	METHOD_REFERENCE_tree(class edd_tree * const _p, const lib::pos &p);
	~METHOD_REFERENCE_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
};


class REFERENCE_IDENT_tree : public REFERENCE_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class EDD_OBJECT_tree *ref; 

public:
	REFERENCE_IDENT_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_IDENT_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class EDD_OBJECT_tree * getref(void);
public:
	void dump(lib::stream &f) const;
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
public:
	REFERENCE_BASE_tree::index_ops_t determine_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars);
private:
	void ensure_ref(void);
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res);
public:
	class EDD_OBJECT_tree * value_object(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path);
};


class REFERENCE_ARRAY_REF_tree : public REFERENCE_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	REFERENCE_ARRAY_REF_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_ARRAY_REF_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class edd_type * synth_reference(void) const;
private:
	class tree * release_reference(void);
private:
	class tree * release_expr(void);
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
private:
	void check_index(class edd_type *l, long index, const lib::pos &pos) const;
protected:
	void pass1(void);
private:
	int search_index_var(const std::vector<class VARIABLE_tree *> &index_vars) const;
private:
	void expand_reference_array(class method_env *env, index_ops_t &ops, index_op_t &element, long var_index, class REFERENCE_ARRAY_tree *array) const;
public:
	REFERENCE_BASE_tree::index_ops_t determine_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars);
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res);
public:
	class EDD_OBJECT_tree * value_object(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path);
};


class REFERENCE_CALL_tree : public REFERENCE_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype;
		bool done;
	

public:
	REFERENCE_CALL_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_CALL_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
private:
	void synth_me(void);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
public:
	REFERENCE_BASE_tree::index_ops_t determine_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars);
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res);
public:
	class EDD_OBJECT_tree * value_object(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path);
};


class REFERENCE_RECORD_tree : public REFERENCE_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	REFERENCE_RECORD_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_RECORD_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
private:
	std::auto_ptr<class edd_type> lookup(bool do_check) const;
protected:
	void pass1(void);
public:
	REFERENCE_BASE_tree::index_ops_t determine_index_vars(class method_env *env, class tree *var, const std::vector<class VARIABLE_tree *> &index_vars);
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res);
public:
	class EDD_OBJECT_tree * value_object(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path);
};


class REFERENCE_BLOCK_tree : public REFERENCE_BASE_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class EDD_OBJECT_tree *ref; 

public:
	REFERENCE_BLOCK_tree(class edd_tree * const _p, const lib::pos &p);
	~REFERENCE_BLOCK_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class EDD_OBJECT_tree * getref(void);
public:
	void dump(lib::stream &f) const;
public:
	bool is_comma_element(void) const throw();
protected:
	class identifier_tree * get_comma(void) const throw();
public:
	void add_comma(class tree *comma);
public:
	class edd_type * synth(void);
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
private:
	void ensure_ref(void);
public:
	operand_ptr label_help(class method_env *env, bool labelflag, label_help_stack_t &res);
public:
	class EDD_OBJECT_tree * value_object(class method_env *env);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr eval_with_access_path(class method_env *env, lib::dyn_buf &access_path);
};


class STRING_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	STRING_tree(class edd_tree * const _p, const lib::pos &p);
	~STRING_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	void check_enum_dependency(void) const;
public:
	class edd_type * synth(void);
public:
	operand_ptr eval(class method_env *env);
};


class STRING_TERMINAL_tree : public node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	STRING_TERMINAL_tree(class edd_tree * const _p, const lib::pos &p);
	~STRING_TERMINAL_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	void pass1(void);
protected:
	void pass2(const edd_tree::pass2info &info);
public:
	void check_enum_dependency(void) const;
public:
	class edd_type * synth(void);
public:
	operand_ptr eval(class method_env *env);
};


class c_char_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_char_tree(class edd_tree * const _p, const lib::pos &p);
	~c_char_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_dictionary_tree : public c_string_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_dictionary_tree(class edd_tree * const _p, const lib::pos &p);
	~c_dictionary_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_identifier_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 c_variable_ptr var; 

public:
	c_identifier_tree(class edd_tree * const _p, const lib::pos &p);
	~c_identifier_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	const lib::IDENTIFIER & ident(void) const throw();
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	class c_type c_synth_safe(class c_compound_statement_tree *env, class c_control *control);
public:
	class c_type c_synth_flat_arg(class c_compound_statement_tree *env, class c_control *control, bool global_first, bool final);
protected:
	void pass2(const edd_tree::pass2info &info);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_integer_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_integer_tree(class edd_tree * const _p, const lib::pos &p);
	~c_integer_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_real_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_real_tree(class edd_tree * const _p, const lib::pos &p);
	~c_real_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_string_tree : public c_string_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_string_tree(class edd_tree * const _p, const lib::pos &p);
	~c_string_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_edd_terminal_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type mytype; 

public:
	c_edd_terminal_tree(class edd_tree * const _p, const lib::pos &p);
	~c_edd_terminal_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_primary_expr_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_primary_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_primary_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	class c_node_tree * deref(void) const throw();
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_postfix_expr_array_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_expr, rtype_index;
		c_variable_ptr ls;
	

public:
	c_postfix_expr_array_tree(class edd_tree * const _p, const lib::pos &p);
	~c_postfix_expr_array_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_postfix_expr_struct_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_expr;
		bool lval;
	

public:
	c_postfix_expr_struct_tree(class edd_tree * const _p, const lib::pos &p);
	~c_postfix_expr_struct_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_postfix_expr_call_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class VARIABLE_tree *enum_string_ref;
		const class c_method *c_method;
		std::vector<class c_node_tree *> args;
		std::vector<class c_type> targs;
		class c_type rtype;
	

public:
	c_postfix_expr_call_tree(class edd_tree * const _p, const lib::pos &p);
	~c_postfix_expr_call_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	const lib::IDENTIFIER & ident(void) const throw();
private:
	bool search_call_expr(const lib::IDENTIFIER &id) const;
private:
	void print_this_description(lib::scratch_buf &buf, class c_compound_statement_tree *env, class c_control *control) const;
private:
	void print_method_description(const class c_method *m, lib::scratch_buf &buf) const;
private:
	void information_candidates(const std::vector<const class c_method *> &overloads) const;
private:
	bool check_arg_type(const class c_type &src, class c_node_tree *arg, const class c_type &dst) const;
private:
	bool check_args(const class c_method *c_method, class c_compound_statement_tree *env, class c_control *control, bool builtin, bool simple);
private:
	bool check_args_overloaded(const std::vector<const class c_method *> &overloads, class c_compound_statement_tree *env, class c_control *control, const class c_method **method, bool builtin);
private:
	void c_synth_args(class c_compound_statement_tree *env, class c_control *control);
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_postfix_expr_op_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype; 

public:
	c_postfix_expr_op_tree(class edd_tree * const _p, const lib::pos &p);
	~c_postfix_expr_op_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_unary_expr_tree : public c_unary_expr_base_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype; 

public:
	c_unary_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_unary_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	enum c_unary_expr_base_tree::ops get_op(void) const;
protected:
	lib::pos expr_pos(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_cast_expr_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype;
		c_type::typespec result;
	

public:
	c_cast_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_cast_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_binary_expr_tree : public c_binary_expr_base_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype1, rtype2; 

public:
	c_binary_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_binary_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	enum c_binary_expr_base_tree::ops get_op(void) const;
protected:
	lib::pos expr1_pos(void) const;
protected:
	lib::pos expr2_pos(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_conditional_expr_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype, rtype1, rtype2;
		c_type::typespec result;
	

public:
	c_conditional_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_conditional_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_assignment_expr_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype1, rtype2; 

public:
	c_assignment_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_assignment_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	void check_VARIABLE(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
private:
	lib::IDENTIFIER opmsg(void) const;
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_comma_expr_tree : public c_expr_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype1, rtype2; 

public:
	c_comma_expr_tree(class edd_tree * const _p, const lib::pos &p);
	~c_comma_expr_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	bool no_effect(void) const;
};


class c_declaration_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_declaration_tree(class edd_tree * const _p, const lib::pos &p);
	~c_declaration_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
protected:
	void profile_check(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


/**
 * c_declarator
 * \todo Pass through array size for method local data into stackmachine!
 * Enhance the stackmachine <-> call interface to pass through array size and
 * use the array size to avoid invalid array access.
 */
class c_declarator_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
public:
	c_declarator_tree(class edd_tree * const _p, const lib::pos &p);
	~c_declarator_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	lib::IDENTIFIER getident(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	class c_type c_synth_declarator(const class c_type &base) const;
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_declarator_array_specifier_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 long size; 

public:
	c_declarator_array_specifier_tree(class edd_tree * const _p, const lib::pos &p);
	~c_declarator_array_specifier_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	int getsize(void) const;
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_compound_statement_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class METHOD_BASE_tree *link;
		const class c_method *c_method;

		class c_compound_statement_tree *up;
		int parsize;

		/* environment data */
		typedef std::map<lib::IDENTIFIER, c_variable_ptr> myenv_t;
		myenv_t myenv;

		typedef std::map<c_variable_ptr, bool> unused_t;
		unused_t unused;

		/* frame informations */
		std::auto_ptr<stackmachine::FrameInfo> frameinfo;

		struct var_info
		{
			var_info(c_variable_ptr v, int o, int s)
			: var(v), off(o), size(s) { }

			c_variable_ptr var;
			int off;
			int size;
		};
		std::vector<struct var_info> vars;

		unsigned int outlabel;
		bool return_stmt;
	

public:
	c_compound_statement_tree(class edd_tree * const _p, const lib::pos &p);
	~c_compound_statement_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	bool empty(void) const throw();
public:
	lib::pos pos_start(void) const throw();
public:
	lib::pos pos_end(void) const throw();
public:
	bool search_call(const lib::IDENTIFIER &id) const;
public:
	void synthesize(class METHOD_BASE_tree *l);
public:
	const class c_method * getmethod(void) const;
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	int frame(void) const;
private:
	void check_shadow(const lib::IDENTIFIER &ident, const lib::pos &idpos) const;
public:
	void add(const lib::IDENTIFIER &ident, const lib::pos &idpos, int size, const class c_type &type);
public:
	c_variable_ptr lookup(const lib::IDENTIFIER &ident, const lib::pos &idpos);
private:
	c_variable_ptr lookup_global_namespace(const lib::IDENTIFIER &ident, const lib::pos &idpos);
public:
	c_variable_ptr global_lookup(const lib::IDENTIFIER &ident);
public:
	class c_compound_statement_tree * getouterframe(void);
public:
	class METHOD_BASE_tree * getlink(void);
public:
	unsigned int getoutlabel(stackmachine::SMachine &sm);
public:
	void register_return_stmt(void);
};


class c_expr_statement_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[1];
	static const unsigned long *_types_flat[1];
	static const unsigned long *_types_recursiv[1];
	
	class tree *_attr[1];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype; 

public:
	c_expr_statement_tree(class edd_tree * const _p, const lib::pos &p);
	~c_expr_statement_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_selection_statement_if_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[3];
	static const unsigned long *_types_flat[3];
	static const unsigned long *_types_recursiv[3];
	
	class tree *_attr[3];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
 class c_type rtype; 

public:
	c_selection_statement_if_tree(class edd_tree * const _p, const lib::pos &p);
	~c_selection_statement_if_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};


class c_selection_statement_switch_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_switch;
		class c_type rtype_stmt;

		class c_control *mycontrol;
		unsigned int myframe;

		unsigned int outlabel;

		/* list of case stmts that have to be handled */
		typedef std::list<class c_labeled_statement_tree *> cases_t;
		cases_t cases;
	

public:
	c_selection_statement_switch_tree(class edd_tree * const _p, const lib::pos &p);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	 ~c_selection_statement_switch_tree(void);
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	unsigned int frame(void) const;
public:
	unsigned int getoutlabel(stackmachine::SMachine &sm);
private:
	void check_dups(class c_labeled_statement_tree *t);
public:
	void register_case(class c_labeled_statement_tree *t);
};


class c_labeled_statement_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_case;
		class c_type rtype_stmt;

		unsigned int inlabel;
	

public:
	c_labeled_statement_tree(class edd_tree * const _p, const lib::pos &p);
	~c_labeled_statement_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	void emit_case(c_type::typespec wanted, stackmachine::SMachine &sm, class c_compound_statement_tree *env) const;
public:
	c_type case_value(void) const;
public:
	bool is_default(void) const;
public:
	unsigned int getinlabel(stackmachine::SMachine &sm);
};


class c_iteration_statement_do_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_stmt;
		class c_type rtype_do;

		class c_control *mycontrol;
		unsigned int myframe;

		unsigned int outlabel;
		unsigned int contlabel;
	

public:
	c_iteration_statement_do_tree(class edd_tree * const _p, const lib::pos &p);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	 ~c_iteration_statement_do_tree(void);
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	unsigned int frame(void) const;
public:
	unsigned int getoutlabel(stackmachine::SMachine &sm);
public:
	unsigned int getcontlabel(stackmachine::SMachine &sm);
};


class c_iteration_statement_for_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[4];
	static const unsigned long *_types_flat[4];
	static const unsigned long *_types_recursiv[4];
	
	class tree *_attr[4];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype_start;
		class c_type rtype_while;
		class c_type rtype_expr;
		class c_type rtype_stmt;

		class c_control *mycontrol;
		unsigned int myframe;

		unsigned int outlabel;
		unsigned int contlabel;
	

public:
	c_iteration_statement_for_tree(class edd_tree * const _p, const lib::pos &p);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	 ~c_iteration_statement_for_tree(void);
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
public:
	unsigned int frame(void) const;
public:
	unsigned int getoutlabel(stackmachine::SMachine &sm);
public:
	unsigned int getcontlabel(stackmachine::SMachine &sm);
};


class c_jump_statement_tree : public c_node_tree
{
private:
	static const char **_attr_descr;
	static const unsigned long _attr_max;
	static const unsigned long _merge;
	static const unsigned long _required;
	static const unsigned long _type_flags[2];
	static const unsigned long *_types_flat[2];
	static const unsigned long *_types_recursiv[2];
	
	class tree *_attr[2];
	
	void check(unsigned long i) const;
	class tree ** attr_ptr(unsigned long i);
	
private:
		class c_type rtype;
		class c_control *up;
	

public:
	c_jump_statement_tree(class edd_tree * const _p, const lib::pos &p);
	~c_jump_statement_tree(void);
	
	class tree *clone(void) const;
	
	class tree * attr(unsigned long i) const;
	const char * attr_descr(unsigned long i) const;
	unsigned long attr_max(void) const;
	unsigned long merge(void) const;
	unsigned long required(void) const;
	const unsigned long type_flags(unsigned long i) const;
	const unsigned long * types_flat(unsigned long i) const;
	const unsigned long * types_recursiv(unsigned long i) const;
	
	unsigned long _sizeof(void) const throw();
	
private:
	class c_type c_synth(class c_compound_statement_tree *env, class c_control *control);
public:
	void code(stackmachine::SMachine &sm, class c_compound_statement_tree *env);
};



class pseudo_tree : public tree
{
private:
		friend class LIKE_tree;
		friend class VARIABLE_tree;

		class tree *mytree;
		enum kinds mytkind;
		unsigned long mytattr;
	

public:
	unsigned long _sizeof(void) const throw();
	
protected:
	void set_tattr(unsigned long attr);
protected:
	void set_tkind(enum kinds kind);
public:
	class tree * release(void);
public:
	class tree * get(void) const;
public:
	unsigned long tattr(void) const;
public:
	enum kinds tkind(void) const;
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 ~pseudo_tree(void);
public:
	 pseudo_tree(class tree *t, enum kinds k, unsigned long a, class edd_tree * const _p, const lib::pos &p);
};

class boolean_tree : public tree
{
private:
public:
	const bool data;
	
	boolean_tree(bool v, class edd_tree * const _p, const lib::pos &p);
	class tree *clone(void) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
};

class integer_tree : public tree
{
private:
 int32 data32; int64 data64; bool signed_flag; 

public:
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void) const;
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 integer_tree(bool usf, uint64 uv, class edd_tree * const _p, const lib::pos &p);
public:
	 integer_tree(bool usf, uint32 uv, class edd_tree * const _p, const lib::pos &p);
public:
	 integer_tree(int64 v, bool sf, class edd_tree * const _p, const lib::pos &p);
public:
	 integer_tree(int32 v, bool sf, class edd_tree * const _p, const lib::pos &p);
public:
	 integer_tree(int64 v, class edd_tree * const _p, const lib::pos &p);
public:
	 integer_tree(int32 v, class edd_tree * const _p, const lib::pos &p);
public:
	std::string get_string(void) const throw();
public:
	int32 get_int32(void) const throw();
public:
	uint64 get_unsigned64(void) const throw();
public:
	uint32 get_unsigned32(void) const throw();
public:
	bool is_negative64(void) const throw();
public:
	bool is_negative32(void) const throw();
public:
	bool is_signed(void) const throw();
public:
	bool is_64bit(void) const throw();
};

class real_tree : public tree
{
private:
public:
	const float64 data;
	
	real_tree(float64 v, class edd_tree * const _p, const lib::pos &p);
	class tree *clone(void) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
};

class enum_tree : public tree
{
private:
public:
	const uint32 data;
	
	enum_tree(uint32 v, class edd_tree * const _p, const lib::pos &p);
	class tree *clone(void) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
};

class set_tree : public tree
{
private:
public:
	const uint32 data;
	
	set_tree(uint32 v, class edd_tree * const _p, const lib::pos &p);
	class tree *clone(void) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
};

class bitset_tree : public tree
{
private:
public:
	lib::vbitset data;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 bitset_tree(const lib::vbitset &v, class edd_tree * const _p, const lib::pos &p);
public:
	 bitset_tree(lib::vbitset *&v, class edd_tree * const _p, const lib::pos &p);
public:
	 bitset_tree(uint64 v, class edd_tree * const _p, const lib::pos &p);
};

class identifier_tree : public tree
{
private:
public:
	const lib::IDENTIFIER data;
	
	identifier_tree(lib::IDENTIFIER v, class edd_tree * const _p, const lib::pos &p);
	class tree *clone(void) const;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	const char * c_str(void) const;
public:
	void dump(lib::stream &f) const;
};

class string_tree : public tree
{
private:
public:
	std::string data;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
public:
	const char * c_str(void) const;
public:
	size_t len(void) const;
public:
	tree * clone(void) const;
public:
	 string_tree(const char *v, unsigned long len, class edd_tree * const _p, const lib::pos &p);
public:
	 string_tree(const char *v, class edd_tree * const _p, const lib::pos &p);
public:
	 string_tree(const std::string &v, class edd_tree * const _p, const lib::pos &p);
};

class current_role_tree : public tree
{
private:
public:
	unsigned long _sizeof(void) const throw();
	
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 current_role_tree(class edd_tree *_p, const lib::pos &p);
};

class uuid_tree : public tree
{
private:
public:
	std::string data;
	
	unsigned long _sizeof(void) const throw();
	
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	stackmachine::Type * type(void);
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 uuid_tree(const char *v, unsigned long len, class edd_tree * const _p, const lib::pos &p);
public:
	 uuid_tree(const std::string &v, class edd_tree * const _p, const lib::pos &p);
};

class array_index_tree : public tree
{
private:
public:
	unsigned long _sizeof(void) const throw();
	
protected:
	void pass1(void);
public:
	operand_ptr eval(class method_env *env);
public:
	operand_ptr value(class method_env *env);
public:
	class edd_type * synth(void);
public:
	class c_type c_synth_flat(void);
public:
	void dump(lib::stream &f) const;
public:
	tree * clone(void) const;
public:
	 array_index_tree(class edd_tree *_p, const lib::pos &p);
};



#ifndef DOXYGEN_IGNORE

const unsigned long EDD_ROOT_ATTR_IDENTIFICATION = 0;
const unsigned long EDD_ROOT_ATTR_DEFINITION_LIST = 1;

const unsigned long EDD_IDENTIFICATION_ATTR_MANUFACTURER = 0;
const unsigned long EDD_IDENTIFICATION_ATTR_DEVICE_TYPE = 1;
const unsigned long EDD_IDENTIFICATION_ATTR_DEVICE_REVISION = 2;
const unsigned long EDD_IDENTIFICATION_ATTR_DD_REVISION = 3;
const unsigned long EDD_IDENTIFICATION_ATTR_EDD_VERSION = 4;
const unsigned long EDD_IDENTIFICATION_ATTR_EDD_PROFILE = 5;
const unsigned long EDD_IDENTIFICATION_ATTR_MANUFACTURER_EXT = 6;

const unsigned long EDD_AXIS_ATTR_HELP = 0;
const unsigned long EDD_AXIS_ATTR_LABEL = 1;
const unsigned long EDD_AXIS_ATTR_MIN_VALUE = 2;
const unsigned long EDD_AXIS_ATTR_MAX_VALUE = 3;
const unsigned long EDD_AXIS_ATTR_SCALING = 4;
   const unsigned long EDD_AXIS_SCALING_LINEAR = 0;
   const unsigned long EDD_AXIS_SCALING_LOGARITHMIC = 1;
const unsigned long EDD_AXIS_ATTR_CONSTANT_UNIT = 5;

const unsigned long EDD_BLOCK_ATTR_HELP = 0;
const unsigned long EDD_BLOCK_ATTR_LABEL = 1;
const unsigned long EDD_BLOCK_ATTR_TYPE = 2;
   const unsigned long EDD_BLOCK_TYPE_PHYSICAL = 0;
   const unsigned long EDD_BLOCK_TYPE_TRANSDUCER = 1;
   const unsigned long EDD_BLOCK_TYPE_FUNCTION = 2;
const unsigned long EDD_BLOCK_ATTR_NUMBER = 3;
const unsigned long EDD_BLOCK_ATTR_PLUGINS = 4;
const unsigned long EDD_BLOCK_ATTR_CHARACTERISTICS = 5;
const unsigned long EDD_BLOCK_ATTR_PARAMETERS = 6;
const unsigned long EDD_BLOCK_ATTR_PARAMETER_LISTS = 7;
const unsigned long EDD_BLOCK_ATTR_LOCAL_PARAMETERS = 8;
const unsigned long EDD_BLOCK_ATTR_AXIS_ITEMS = 9;
const unsigned long EDD_BLOCK_ATTR_CHART_ITEMS = 10;
const unsigned long EDD_BLOCK_ATTR_COLLECTION_ITEMS = 11;
const unsigned long EDD_BLOCK_ATTR_EDIT_DISPLAY_ITEMS = 12;
const unsigned long EDD_BLOCK_ATTR_FILE_ITEMS = 13;
const unsigned long EDD_BLOCK_ATTR_GRAPH_ITEMS = 14;
const unsigned long EDD_BLOCK_ATTR_GRID_ITEMS = 15;
const unsigned long EDD_BLOCK_ATTR_IMAGE_ITEMS = 16;
const unsigned long EDD_BLOCK_ATTR_LIST_ITEMS = 17;
const unsigned long EDD_BLOCK_ATTR_MENU_ITEMS = 18;
const unsigned long EDD_BLOCK_ATTR_METHOD_ITEMS = 19;
const unsigned long EDD_BLOCK_ATTR_PLUGIN_ITEMS = 20;
const unsigned long EDD_BLOCK_ATTR_UNIT_ITEMS = 21;
const unsigned long EDD_BLOCK_ATTR_REFERENCE_ARRAY_ITEMS = 22;
const unsigned long EDD_BLOCK_ATTR_REFRESH_ITEMS = 23;
const unsigned long EDD_BLOCK_ATTR_SOURCE_ITEMS = 24;
const unsigned long EDD_BLOCK_ATTR_WAO_ITEMS = 25;
const unsigned long EDD_BLOCK_ATTR_WAVEFORM_ITEMS = 26;

const unsigned long EDD_CHART_ATTR_HELP = 0;
const unsigned long EDD_CHART_ATTR_LABEL = 1;
const unsigned long EDD_CHART_ATTR_VALIDITY = 2;
const unsigned long EDD_CHART_ATTR_VISIBILITY = 3;
const unsigned long EDD_CHART_ATTR_MEMBERS = 4;
const unsigned long EDD_CHART_ATTR_DISPLAY_HEIGHT = 5;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_XX_SMALL = 0;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_X_SMALL = 1;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_SMALL = 2;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_MEDIUM = 3;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_LARGE = 4;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_X_LARGE = 5;
   const unsigned long EDD_CHART_DISPLAY_HEIGHT_XX_LARGE = 6;
const unsigned long EDD_CHART_ATTR_DISPLAY_WIDTH = 6;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_XX_SMALL = 0;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_X_SMALL = 1;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_SMALL = 2;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_MEDIUM = 3;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_LARGE = 4;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_X_LARGE = 5;
   const unsigned long EDD_CHART_DISPLAY_WIDTH_XX_LARGE = 6;
const unsigned long EDD_CHART_ATTR_TYPE = 7;
   const unsigned long EDD_CHART_TYPE_GAUGE = 0;
   const unsigned long EDD_CHART_TYPE_HORIZONTAL_BAR = 1;
   const unsigned long EDD_CHART_TYPE_SCOPE = 2;
   const unsigned long EDD_CHART_TYPE_STRIP = 3;
   const unsigned long EDD_CHART_TYPE_SWEEP = 4;
   const unsigned long EDD_CHART_TYPE_VERTICAL_BAR = 5;
const unsigned long EDD_CHART_ATTR_CYCLE_TIME = 8;
const unsigned long EDD_CHART_ATTR_LENGTH = 9;

const unsigned long EDD_COLLECTION_ATTR_TYPE = 0;
const unsigned long EDD_COLLECTION_ATTR_HELP = 1;
const unsigned long EDD_COLLECTION_ATTR_LABEL = 2;
const unsigned long EDD_COLLECTION_ATTR_MEMBERS = 3;
const unsigned long EDD_COLLECTION_ATTR_VALIDITY = 4;
const unsigned long EDD_COLLECTION_ATTR_VISIBILITY = 5;

const unsigned long EDD_MEMBER_ATTR_IDENTIFIER = 0;
const unsigned long EDD_MEMBER_ATTR_REFERENCE = 1;
const unsigned long EDD_MEMBER_ATTR_DESCRIPTION = 2;
const unsigned long EDD_MEMBER_ATTR_HELP = 3;

const unsigned long EDD_COMMAND_ATTR_SLOT = 0;
const unsigned long EDD_COMMAND_ATTR_SUBSLOT = 1;
const unsigned long EDD_COMMAND_ATTR_INDEX = 2;
const unsigned long EDD_COMMAND_ATTR_BLOCK = 3;
const unsigned long EDD_COMMAND_ATTR_NUMBER = 4;
const unsigned long EDD_COMMAND_ATTR_OPERATION = 5;
   const unsigned long EDD_COMMAND_OPERATION_READ = 0;
   const unsigned long EDD_COMMAND_OPERATION_WRITE = 1;
   const unsigned long EDD_COMMAND_OPERATION_COMMAND = 2;
   const unsigned long EDD_COMMAND_OPERATION_DATA_EXCHANGE = 3;
const unsigned long EDD_COMMAND_ATTR_TRANSACTION = 6;
const unsigned long EDD_COMMAND_ATTR_CONNECTION = 7;
const unsigned long EDD_COMMAND_ATTR_MODULE = 8;
const unsigned long EDD_COMMAND_ATTR_HEADER = 9;
const unsigned long EDD_COMMAND_ATTR_RESPONSE_CODES = 10;

const unsigned long EDD_TRANSACTION_ATTR_INTEGER = 0;
const unsigned long EDD_TRANSACTION_ATTR_REQUEST = 1;
const unsigned long EDD_TRANSACTION_ATTR_REPLY = 2;
const unsigned long EDD_TRANSACTION_ATTR_RESPONSE_CODES = 3;

const unsigned long EDD_DATA_ITEM_ATTR_REFERENCE = 0;
const unsigned long EDD_DATA_ITEM_ATTR_MASK = 1;
const unsigned long EDD_DATA_ITEM_ATTR_QUALIFIER = 2;
   const unsigned long EDD_DATA_ITEM_QUALIFIER_INDEX = 0x1;
   const unsigned long EDD_DATA_ITEM_QUALIFIER_INFO = 0x2;
const unsigned long EDD_DATA_ITEM_ATTR_COMMA = 3;

const unsigned long EDD_CONNECTION_ATTR_APPINSTANCE = 0;

const unsigned long EDD_DOMAIN_ATTR_RESPONSE_CODES = 0;
const unsigned long EDD_DOMAIN_ATTR_HANDLING = 1;
   const unsigned long EDD_DOMAIN_HANDLING_READ = 0x1;
   const unsigned long EDD_DOMAIN_HANDLING_WRITE = 0x2;

const unsigned long EDD_EDIT_DISPLAY_ATTR_LABEL = 0;
const unsigned long EDD_EDIT_DISPLAY_ATTR_EDIT_ITEMS = 1;
const unsigned long EDD_EDIT_DISPLAY_ATTR_DISPLAY_ITEMS = 2;
const unsigned long EDD_EDIT_DISPLAY_ATTR_PRE_EDIT_ACTIONS = 3;
const unsigned long EDD_EDIT_DISPLAY_ATTR_POST_EDIT_ACTIONS = 4;

const unsigned long EDD_EDIT_DISPLAY_ITEM_ATTR_REFERENCE = 0;

const unsigned long EDD_COMPONENT_ATTR_LABEL = 0;
const unsigned long EDD_COMPONENT_ATTR_HELP = 1;
const unsigned long EDD_COMPONENT_ATTR_COMPONENT_RELATIONS = 2;
const unsigned long EDD_COMPONENT_ATTR_PROTOCOL = 3;
   const unsigned long EDD_COMPONENT_PROTOCOL_UNKNOWN = 0;
   const unsigned long EDD_COMPONENT_PROTOCOL_PROFIBUS_DP = 1;
   const unsigned long EDD_COMPONENT_PROTOCOL_PROFIBUS_PA = 2;
   const unsigned long EDD_COMPONENT_PROTOCOL_PROFINET = 3;
   const unsigned long EDD_COMPONENT_PROTOCOL_HART = 4;
   const unsigned long EDD_COMPONENT_PROTOCOL_FF = 5;
const unsigned long EDD_COMPONENT_ATTR_CLASSIFICATION = 4;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_ACTUATOR = 0;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_ACTUATOR_ELECTRO_PNEUMATIC = 1;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_ACTUATOR_ELECTRIC = 2;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_ACTUATOR_HYDRAULIC = 3;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_CONVERTER = 4;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_CONTROLLER = 5;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_DISCRETE_IN = 6;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_DISCRETE_OUT = 7;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_FREQUENCY_CONVERTER = 8;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_INDICATOR = 9;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_NETWORK_COMPONENT = 10;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_REMOTEIO = 11;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_RECORDER = 12;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR = 13;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_ACOUSTIC = 14;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_ANALYTIC_GAS = 15;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_ANALYTIC = 16;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_ANALYTIC_LIQUID = 17;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_CONCENTRATION = 18;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_DENSITY = 19;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW = 20;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_CORIOLIS = 21;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_ELECTRO_MAGNETIC = 22;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_MECHANICAL = 23;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_RADIOMETRIC = 24;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_THERMAL = 25;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_ULTRASONIC = 26;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_FLOW_VORTEX_COUNTER = 27;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL = 28;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL_BUOYANCY = 29;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL_CAPACITIVE = 30;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL_ECHO = 31;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL_HYDROSTATIC = 32;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_LEVEL_RADIOMETRIC = 33;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_MULTIVARIABLE = 34;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_POSITION = 35;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_PRESSURE = 36;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SENSOR_TEMPERATURE = 37;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SEMICONDUCTOR = 38;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_SWITCHGEAR = 39;
   const unsigned long EDD_COMPONENT_CLASSIFICATION_UNIVERSAL = 40;
const unsigned long EDD_COMPONENT_ATTR_COMPONENT_PATH = 5;
const unsigned long EDD_COMPONENT_ATTR_COMPONENT_PARENT = 6;
const unsigned long EDD_COMPONENT_ATTR_EDD = 7;
const unsigned long EDD_COMPONENT_ATTR_CAN_DELETE = 8;
const unsigned long EDD_COMPONENT_ATTR_REDUNDANCY = 9;
const unsigned long EDD_COMPONENT_ATTR_CHECK_CONFIGURATION = 10;
const unsigned long EDD_COMPONENT_ATTR_SUPPLIED_INTERFACE = 11;
const unsigned long EDD_COMPONENT_ATTR_INITIAL_VALUES = 12;
const unsigned long EDD_COMPONENT_ATTR_DECLARATION = 13;
const unsigned long EDD_COMPONENT_ATTR_SCAN_METHOD = 14;
const unsigned long EDD_COMPONENT_ATTR_SCAN_LIST = 15;
const unsigned long EDD_COMPONENT_ATTR_DETECT_METHOD = 16;
const unsigned long EDD_COMPONENT_ATTR_COMPONENTS = 17;

const unsigned long EDD_COMPONENT_INITIAL_VALUES_ATTR_REFERENCE = 0;
const unsigned long EDD_COMPONENT_INITIAL_VALUES_ATTR_EXPR = 1;

const unsigned long EDD_COMPONENT_FOLDER_ATTR_LABEL = 0;
const unsigned long EDD_COMPONENT_FOLDER_ATTR_HELP = 1;
const unsigned long EDD_COMPONENT_FOLDER_ATTR_CLASSIFICATION = 2;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_ACTUATOR = 0;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_ACTUATOR_ELECTRO_PNEUMATIC = 1;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_ACTUATOR_ELECTRIC = 2;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_ACTUATOR_HYDRAULIC = 3;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_CONVERTER = 4;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_CONTROLLER = 5;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_DISCRETE_IN = 6;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_DISCRETE_OUT = 7;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_FREQUENCY_CONVERTER = 8;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_INDICATOR = 9;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_NETWORK_COMPONENT = 10;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_REMOTEIO = 11;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_RECORDER = 12;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR = 13;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_ACOUSTIC = 14;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_ANALYTIC_GAS = 15;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_ANALYTIC = 16;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_ANALYTIC_LIQUID = 17;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_CONCENTRATION = 18;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_DENSITY = 19;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW = 20;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_CORIOLIS = 21;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_ELECTRO_MAGNETIC = 22;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_MECHANICAL = 23;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_RADIOMETRIC = 24;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_THERMAL = 25;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_ULTRASONIC = 26;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_FLOW_VORTEX_COUNTER = 27;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL = 28;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL_BUOYANCY = 29;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL_CAPACITIVE = 30;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL_ECHO = 31;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL_HYDROSTATIC = 32;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_LEVEL_RADIOMETRIC = 33;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_MULTIVARIABLE = 34;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_POSITION = 35;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_PRESSURE = 36;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SENSOR_TEMPERATURE = 37;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SEMICONDUCTOR = 38;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_SWITCHGEAR = 39;
   const unsigned long EDD_COMPONENT_FOLDER_CLASSIFICATION_UNIVERSAL = 40;
const unsigned long EDD_COMPONENT_FOLDER_ATTR_COMPONENT_PARENT = 3;
const unsigned long EDD_COMPONENT_FOLDER_ATTR_COMPONENT_PATH = 4;
const unsigned long EDD_COMPONENT_FOLDER_ATTR_PROTOCOL = 5;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_UNKNOWN = 0;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_PROFIBUS_DP = 1;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_PROFIBUS_PA = 2;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_PROFINET = 3;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_HART = 4;
   const unsigned long EDD_COMPONENT_FOLDER_PROTOCOL_FF = 5;

const unsigned long EDD_COMPONENT_REFERENCE_ATTR_PROTOCOL = 0;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_UNKNOWN = 0;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_PROFIBUS_DP = 1;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_PROFIBUS_PA = 2;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_PROFINET = 3;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_HART = 4;
   const unsigned long EDD_COMPONENT_REFERENCE_PROTOCOL_FF = 5;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_CLASSIFICATION = 1;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_ACTUATOR = 0;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_ACTUATOR_ELECTRO_PNEUMATIC = 1;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_ACTUATOR_ELECTRIC = 2;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_ACTUATOR_HYDRAULIC = 3;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_CONVERTER = 4;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_CONTROLLER = 5;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_DISCRETE_IN = 6;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_DISCRETE_OUT = 7;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_FREQUENCY_CONVERTER = 8;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_INDICATOR = 9;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_NETWORK_COMPONENT = 10;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_REMOTEIO = 11;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_RECORDER = 12;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR = 13;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_ACOUSTIC = 14;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_ANALYTIC_GAS = 15;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_ANALYTIC = 16;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_ANALYTIC_LIQUID = 17;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_CONCENTRATION = 18;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_DENSITY = 19;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW = 20;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_CORIOLIS = 21;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_ELECTRO_MAGNETIC = 22;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_MECHANICAL = 23;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_RADIOMETRIC = 24;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_THERMAL = 25;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_ULTRASONIC = 26;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_FLOW_VORTEX_COUNTER = 27;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL = 28;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL_BUOYANCY = 29;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL_CAPACITIVE = 30;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL_ECHO = 31;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL_HYDROSTATIC = 32;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_LEVEL_RADIOMETRIC = 33;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_MULTIVARIABLE = 34;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_POSITION = 35;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_PRESSURE = 36;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SENSOR_TEMPERATURE = 37;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SEMICONDUCTOR = 38;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_SWITCHGEAR = 39;
   const unsigned long EDD_COMPONENT_REFERENCE_CLASSIFICATION_UNIVERSAL = 40;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_COMPONENT_PARENT = 2;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_COMPONENT_PATH = 3;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_MANUFACTURER = 4;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_DEVICE_TYPE = 5;
const unsigned long EDD_COMPONENT_REFERENCE_ATTR_DEVICE_REVISION = 6;

const unsigned long EDD_INTERFACE_ATTR_LABEL = 0;
const unsigned long EDD_INTERFACE_ATTR_HELP = 1;
const unsigned long EDD_INTERFACE_ATTR_DECLARATION = 2;

const unsigned long EDD_COMPONENT_RELATION_ATTR_LABEL = 0;
const unsigned long EDD_COMPONENT_RELATION_ATTR_HELP = 1;
const unsigned long EDD_COMPONENT_RELATION_ATTR_TYPE = 2;
   const unsigned long EDD_COMPONENT_RELATION_TYPE_PARENT_COMPONENT = 0;
   const unsigned long EDD_COMPONENT_RELATION_TYPE_CHILD_COMPONENT = 1;
   const unsigned long EDD_COMPONENT_RELATION_TYPE_SIBLING_COMPONENT = 2;
   const unsigned long EDD_COMPONENT_RELATION_TYPE_NEXT_COMPONENT = 3;
   const unsigned long EDD_COMPONENT_RELATION_TYPE_PREV_COMPONENT = 4;
const unsigned long EDD_COMPONENT_RELATION_ATTR_MAXIMUM_NUMBER = 3;
const unsigned long EDD_COMPONENT_RELATION_ATTR_MINIMUM_NUMBER = 4;
const unsigned long EDD_COMPONENT_RELATION_ATTR_REQUIRED_INTERFACE = 5;
const unsigned long EDD_COMPONENT_RELATION_ATTR_SUPPLIED_INTERFACE = 6;
const unsigned long EDD_COMPONENT_RELATION_ATTR_ADDRESSING = 7;
const unsigned long EDD_COMPONENT_RELATION_ATTR_COMPONENTS = 8;

const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_REF = 0;
const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_MINIMUM_NUMBER = 1;
const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_MAXIMUM_NUMBER = 2;
const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_AUTO_CREATE = 3;
const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_FILTER = 4;
const unsigned long EDD_COMPONENTS_REFERENCE_ATTR_REQUIRED_RANGES = 5;

const unsigned long EDD_COMPONENT_REQUIRED_RANGE_ATTR_REF = 0;
const unsigned long EDD_COMPONENT_REQUIRED_RANGE_ATTR_MAX_VALUE = 1;
const unsigned long EDD_COMPONENT_REQUIRED_RANGE_ATTR_MIN_VALUE = 2;
const unsigned long EDD_COMPONENT_REQUIRED_RANGE_ATTR_MAX_VALUES = 3;
const unsigned long EDD_COMPONENT_REQUIRED_RANGE_ATTR_MIN_VALUES = 4;

const unsigned long EDD_FILE_ATTR_HELP = 0;
const unsigned long EDD_FILE_ATTR_LABEL = 1;
const unsigned long EDD_FILE_ATTR_MEMBERS = 2;

const unsigned long EDD_GRAPH_ATTR_HELP = 0;
const unsigned long EDD_GRAPH_ATTR_LABEL = 1;
const unsigned long EDD_GRAPH_ATTR_VALIDITY = 2;
const unsigned long EDD_GRAPH_ATTR_VISIBILITY = 3;
const unsigned long EDD_GRAPH_ATTR_MEMBERS = 4;
const unsigned long EDD_GRAPH_ATTR_DISPLAY_HEIGHT = 5;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_XX_SMALL = 0;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_X_SMALL = 1;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_SMALL = 2;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_MEDIUM = 3;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_LARGE = 4;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_X_LARGE = 5;
   const unsigned long EDD_GRAPH_DISPLAY_HEIGHT_XX_LARGE = 6;
const unsigned long EDD_GRAPH_ATTR_DISPLAY_WIDTH = 6;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_XX_SMALL = 0;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_X_SMALL = 1;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_SMALL = 2;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_MEDIUM = 3;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_LARGE = 4;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_X_LARGE = 5;
   const unsigned long EDD_GRAPH_DISPLAY_WIDTH_XX_LARGE = 6;
const unsigned long EDD_GRAPH_ATTR_CYCLE_TIME = 7;
const unsigned long EDD_GRAPH_ATTR_X_AXIS = 8;

const unsigned long EDD_GRID_ATTR_HELP = 0;
const unsigned long EDD_GRID_ATTR_LABEL = 1;
const unsigned long EDD_GRID_ATTR_VALIDITY = 2;
const unsigned long EDD_GRID_ATTR_VISIBILITY = 3;
const unsigned long EDD_GRID_ATTR_HANDLING = 4;
   const unsigned long EDD_GRID_HANDLING_READ = 0x1;
   const unsigned long EDD_GRID_HANDLING_WRITE = 0x2;
const unsigned long EDD_GRID_ATTR_DISPLAY_HEIGHT = 5;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_XX_SMALL = 0;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_X_SMALL = 1;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_SMALL = 2;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_MEDIUM = 3;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_LARGE = 4;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_X_LARGE = 5;
   const unsigned long EDD_GRID_DISPLAY_HEIGHT_XX_LARGE = 6;
const unsigned long EDD_GRID_ATTR_DISPLAY_WIDTH = 6;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_XX_SMALL = 0;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_X_SMALL = 1;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_SMALL = 2;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_MEDIUM = 3;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_LARGE = 4;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_X_LARGE = 5;
   const unsigned long EDD_GRID_DISPLAY_WIDTH_XX_LARGE = 6;
const unsigned long EDD_GRID_ATTR_ORIENTATION = 7;
   const unsigned long EDD_GRID_ORIENTATION_HORIZONTAL = 0;
   const unsigned long EDD_GRID_ORIENTATION_VERTICAL = 1;
const unsigned long EDD_GRID_ATTR_VECTORS = 8;

const unsigned long EDD_VECTOR_ATTR_STRING = 0;
const unsigned long EDD_VECTOR_ATTR_VALUE = 1;

const unsigned long EDD_IMAGE_ATTR_HELP = 0;
const unsigned long EDD_IMAGE_ATTR_LABEL = 1;
const unsigned long EDD_IMAGE_ATTR_VALIDITY = 2;
const unsigned long EDD_IMAGE_ATTR_VISIBILITY = 3;
const unsigned long EDD_IMAGE_ATTR_LINK = 4;
const unsigned long EDD_IMAGE_ATTR_PATH = 5;

const unsigned long EDD_LIST_ATTR_HELP = 0;
const unsigned long EDD_LIST_ATTR_LABEL = 1;
const unsigned long EDD_LIST_ATTR_VALIDITY = 2;
const unsigned long EDD_LIST_ATTR_VISIBILITY = 3;
const unsigned long EDD_LIST_ATTR_CAPACITY = 4;
const unsigned long EDD_LIST_ATTR_COUNT = 5;
const unsigned long EDD_LIST_ATTR_TYPE = 6;

const unsigned long EDD_IMPORTED_DD_ATTR_IDENTIFICATION = 0;
const unsigned long EDD_IMPORTED_DD_ATTR_IMPORTS = 1;
const unsigned long EDD_IMPORTED_DD_ATTR_REDEFINITIONS = 2;

const unsigned long EDD_IMPORT_ACTION_ATTR_ACTION = 0;
   const unsigned long EDD_IMPORT_ACTION_ACTION_EVERYTHING = 0x1;
   const unsigned long EDD_IMPORT_ACTION_ACTION_BY_NAME = 0x2;
   const unsigned long EDD_IMPORT_ACTION_ACTION_BY_TYPE = 0x4;
const unsigned long EDD_IMPORT_ACTION_ATTR_TYPE = 1;
   const unsigned long EDD_IMPORT_ACTION_TYPE_AXES = 0x1;
   const unsigned long EDD_IMPORT_ACTION_TYPE_BLOCKS = 0x2;
   const unsigned long EDD_IMPORT_ACTION_TYPE_CHARTS = 0x4;
   const unsigned long EDD_IMPORT_ACTION_TYPE_COLLECTIONS = 0x8;
   const unsigned long EDD_IMPORT_ACTION_TYPE_COMMANDS = 0x10;
   const unsigned long EDD_IMPORT_ACTION_TYPE_CONNECTIONS = 0x20;
   const unsigned long EDD_IMPORT_ACTION_TYPE_DOMAINS = 0x40;
   const unsigned long EDD_IMPORT_ACTION_TYPE_EDIT_DISPLAYS = 0x80;
   const unsigned long EDD_IMPORT_ACTION_TYPE_FILES = 0x100;
   const unsigned long EDD_IMPORT_ACTION_TYPE_GRAPHS = 0x200;
   const unsigned long EDD_IMPORT_ACTION_TYPE_GRIDS = 0x400;
   const unsigned long EDD_IMPORT_ACTION_TYPE_IMAGES = 0x800;
   const unsigned long EDD_IMPORT_ACTION_TYPE_LISTS = 0x1000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_MENUS = 0x2000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_METHODS = 0x4000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_PLUGINS = 0x8000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_PROGRAMS = 0x10000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_RECORDS = 0x20000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_REFERENCE_ARRAYS = 0x40000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_REFRESHS = 0x80000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_RESPONSE_CODES = 0x100000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_SOURCES = 0x200000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_UNITS = 0x400000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_VALUE_ARRAYS = 0x800000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_VARIABLES = 0x1000000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_VARIABLE_LISTS = 0x2000000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_WAVEFORMS = 0x4000000;
   const unsigned long EDD_IMPORT_ACTION_TYPE_WRITE_AS_ONES = 0x8000000;
const unsigned long EDD_IMPORT_ACTION_ATTR_IDENTIFIER_TYPE = 2;
const unsigned long EDD_IMPORT_ACTION_ATTR_IDENTIFIER = 3;

const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ATTR_ACTION = 0;
   const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ACTION_DELETE = 0;
   const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ACTION_REPLACE = 1;
   const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ACTION_REDEFINE = 2;
const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ATTR_TYPE = 1;
const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ATTR_IDENTIFIER = 2;
const unsigned long EDD_IMPORT_ACTION_REDEFINITION_ATTR_LIST = 3;

const unsigned long EDD_LIKE_ATTR_TYPE = 0;
const unsigned long EDD_LIKE_ATTR_LIKENAME = 1;
const unsigned long EDD_LIKE_ATTR_REDEFINITIONS = 2;
const unsigned long EDD_LIKE_ATTR_ANON_REDEFINITIONS = 3;

const unsigned long EDD_LIKE_ACTION_ATTR_ACTION = 0;
   const unsigned long EDD_LIKE_ACTION_ACTION_COMPARE = 0;
   const unsigned long EDD_LIKE_ACTION_ACTION_DELETE = 1;
   const unsigned long EDD_LIKE_ACTION_ACTION_REDEFINE = 2;
   const unsigned long EDD_LIKE_ACTION_ACTION_DOWN = 3;
   const unsigned long EDD_LIKE_ACTION_ACTION_LIST_DELETE = 4;
   const unsigned long EDD_LIKE_ACTION_ACTION_LIST_REDEFINE = 5;
   const unsigned long EDD_LIKE_ACTION_ACTION_LIST_ADD = 6;
const unsigned long EDD_LIKE_ACTION_ATTR_INFO = 1;
const unsigned long EDD_LIKE_ACTION_ATTR_TREE = 2;

const unsigned long EDD_ANON_LIKE_ACTION_ATTR_ACTION = 0;
const unsigned long EDD_ANON_LIKE_ACTION_ATTR_INFO = 1;

const unsigned long EDD_MENU_ATTR_HELP = 0;
const unsigned long EDD_MENU_ATTR_LABEL = 1;
const unsigned long EDD_MENU_ATTR_VALIDITY = 2;
const unsigned long EDD_MENU_ATTR_VISIBILITY = 3;
const unsigned long EDD_MENU_ATTR_ITEMS = 4;
const unsigned long EDD_MENU_ATTR_ACCESS = 5;
   const unsigned long EDD_MENU_ACCESS_ONLINE = 0;
   const unsigned long EDD_MENU_ACCESS_OFFLINE = 1;
const unsigned long EDD_MENU_ATTR_ENTRY = 6;
const unsigned long EDD_MENU_ATTR_PURPOSE = 7;
const unsigned long EDD_MENU_ATTR_ROLE = 8;
const unsigned long EDD_MENU_ATTR_STYLE = 9;
   const unsigned long EDD_MENU_STYLE_UNKNOWN = 0;
   const unsigned long EDD_MENU_STYLE_WINDOW = 1;
   const unsigned long EDD_MENU_STYLE_DIALOG = 2;
   const unsigned long EDD_MENU_STYLE_GROUP = 3;
   const unsigned long EDD_MENU_STYLE_MENU = 4;
   const unsigned long EDD_MENU_STYLE_PAGE = 5;
   const unsigned long EDD_MENU_STYLE_TABLE = 6;
const unsigned long EDD_MENU_ATTR_PRE_EDIT_ACTIONS = 10;
const unsigned long EDD_MENU_ATTR_POST_EDIT_ACTIONS = 11;
const unsigned long EDD_MENU_ATTR_PRE_READ_ACTIONS = 12;
const unsigned long EDD_MENU_ATTR_POST_READ_ACTIONS = 13;
const unsigned long EDD_MENU_ATTR_PRE_WRITE_ACTIONS = 14;
const unsigned long EDD_MENU_ATTR_POST_WRITE_ACTIONS = 15;

const unsigned long EDD_MENU_ITEM_ATTR_ITEM = 0;
const unsigned long EDD_MENU_ITEM_ATTR_ARGS = 1;
const unsigned long EDD_MENU_ITEM_ATTR_QUALIFIER = 2;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_REVIEW = 0x1;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_DISPLAY_VALUE = 0x2;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_READ_ONLY = 0x4;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_HIDDEN = 0x8;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_NO_LABEL = 0x10;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_NO_UNIT = 0x20;
   const unsigned long EDD_MENU_ITEM_QUALIFIER_INLINE = 0x40;
const unsigned long EDD_MENU_ITEM_ATTR_COMMA = 3;

const unsigned long EDD_MENU_ITEM_FORMAT_ATTR_TYPE = 0;
   const unsigned long EDD_MENU_ITEM_FORMAT_TYPE_SEPARATOR = 0;
   const unsigned long EDD_MENU_ITEM_FORMAT_TYPE_COLUMNBREAK = 1;
   const unsigned long EDD_MENU_ITEM_FORMAT_TYPE_ROWBREAK = 2;

const unsigned long EDD_MENU_ITEM_STRING_ATTR_STRING = 0;

const unsigned long EDD_MENU_ITEM_ENUM_REF_ATTR_REF = 0;
const unsigned long EDD_MENU_ITEM_ENUM_REF_ATTR_ARG = 1;

const unsigned long EDD_METHOD_ATTR_PARAMETERS = 0;
const unsigned long EDD_METHOD_ATTR_TYPE = 1;
const unsigned long EDD_METHOD_ATTR_HELP = 2;
const unsigned long EDD_METHOD_ATTR_LABEL = 3;
const unsigned long EDD_METHOD_ATTR_VALIDITY = 4;
const unsigned long EDD_METHOD_ATTR_VISIBILITY = 5;
const unsigned long EDD_METHOD_ATTR_VARIABLE_CLASS = 6;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_ALARM = 0x1;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_ANALOG_INPUT = 0x2;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_ANALOG_OUTPUT = 0x4;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_COMPUTATION = 0x8;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_CONTAINED = 0x10;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_CORRECTION = 0x20;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DEVICE = 0x40;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DIAGNOSTIC = 0x80;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DIGITAL_INPUT = 0x100;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DIGITAL_OUTPUT = 0x200;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DISCRETE = 0x400;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DISCRETE_INPUT = 0x800;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DISCRETE_OUTPUT = 0x1000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_DYNAMIC = 0x2000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_FREQUENCY = 0x4000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_FREQUENCY_INPUT = 0x8000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_FREQUENCY_OUTPUT = 0x10000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_HART = 0x20000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_HIDDEN = 0x40000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_INPUT = 0x80000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_LOCAL = 0x100000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_LOCAL_DISPLAY = 0x200000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_OPERATE = 0x400000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_OUTPUT = 0x800000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_SERVICE = 0x1000000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_TUNE = 0x2000000;
   const unsigned long EDD_METHOD_VARIABLE_CLASS_USER_INTERFACE = 0x4000000;
const unsigned long EDD_METHOD_ATTR_ACCESS = 7;
   const unsigned long EDD_METHOD_ACCESS_ONLINE = 0;
   const unsigned long EDD_METHOD_ACCESS_OFFLINE = 1;
const unsigned long EDD_METHOD_ATTR_HANDLING = 8;
   const unsigned long EDD_METHOD_HANDLING_READ = 0x1;
   const unsigned long EDD_METHOD_HANDLING_WRITE = 0x2;
const unsigned long EDD_METHOD_ATTR_DEFINITION = 9;

const unsigned long EDD_METHOD_DEFINITION_ATTR_DEFINITION = 0;
const unsigned long EDD_METHOD_DEFINITION_ATTR_COMMA = 1;

const unsigned long EDD_METHOD_PARAMETER_ATTR_TYPE = 0;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_char = 0;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_uchar = 1;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_short = 2;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_ushort = 3;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_int = 4;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_uint = 5;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_long = 6;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_ulong = 7;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_llong = 8;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_ullong = 9;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_float = 10;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_double = 11;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_string = 12;
   const unsigned long EDD_METHOD_PARAMETER_TYPE_t_item = 13;
const unsigned long EDD_METHOD_PARAMETER_ATTR_IDENTIFIER = 1;
const unsigned long EDD_METHOD_PARAMETER_ATTR_BYREF = 2;

const unsigned long EDD_PLUGIN_ATTR_LABEL = 0;
const unsigned long EDD_PLUGIN_ATTR_HELP = 1;
const unsigned long EDD_PLUGIN_ATTR_UUID = 2;
const unsigned long EDD_PLUGIN_ATTR_VALIDITY = 3;
const unsigned long EDD_PLUGIN_ATTR_VISIBILITY = 4;

const unsigned long EDD_OPEN_CLOSE_ATTR_TYPE = 0;
const unsigned long EDD_OPEN_CLOSE_ATTR_IDENTIFIER = 1;

const unsigned long EDD_PROGRAM_ATTR_RESPONSE_CODES = 0;
const unsigned long EDD_PROGRAM_ATTR_ARGUMENTS = 1;

const unsigned long EDD_RECORD_ATTR_HELP = 0;
const unsigned long EDD_RECORD_ATTR_LABEL = 1;
const unsigned long EDD_RECORD_ATTR_VALIDITY = 2;
const unsigned long EDD_RECORD_ATTR_VISIBILITY = 3;
const unsigned long EDD_RECORD_ATTR_MEMBERS = 4;
const unsigned long EDD_RECORD_ATTR_RESPONSE_CODES = 5;

const unsigned long EDD_REFERENCE_ARRAY_ATTR_HELP = 0;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_LABEL = 1;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_TYPE = 2;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_ELEMENTS = 3;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_KEYWORD = 4;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_VALIDITY = 5;
const unsigned long EDD_REFERENCE_ARRAY_ATTR_VISIBILITY = 6;


const unsigned long EDD_REFERENCE_ARRAY_ELEMENT_ATTR_INTEGER = 0;
const unsigned long EDD_REFERENCE_ARRAY_ELEMENT_ATTR_REFERENCE = 1;
const unsigned long EDD_REFERENCE_ARRAY_ELEMENT_ATTR_DESCRIPTION = 2;
const unsigned long EDD_REFERENCE_ARRAY_ELEMENT_ATTR_HELP = 3;

const unsigned long EDD_REFRESH_RELATION_ATTR_LEFT = 0;
const unsigned long EDD_REFRESH_RELATION_ATTR_RIGHT = 1;

const unsigned long EDD_UNIT_RELATION_ATTR_REF = 0;
const unsigned long EDD_UNIT_RELATION_ATTR_REFS = 1;

const unsigned long EDD_WAO_RELATION_ATTR_SPECIFIER = 0;

const unsigned long EDD_RESPONSE_CODES_DEFINITION_ATTR_TYPE = 0;

const unsigned long EDD_RESPONSE_CODE_ATTR_INTEGER = 0;
const unsigned long EDD_RESPONSE_CODE_ATTR_TYPE = 1;
   const unsigned long EDD_RESPONSE_CODE_TYPE_SUCCESS = 0;
   const unsigned long EDD_RESPONSE_CODE_TYPE_MISC_WARNING = 1;
   const unsigned long EDD_RESPONSE_CODE_TYPE_DATA_ENTRY_WARNING = 2;
   const unsigned long EDD_RESPONSE_CODE_TYPE_DATA_ENTRY_ERROR = 3;
   const unsigned long EDD_RESPONSE_CODE_TYPE_MODE_ERROR = 4;
   const unsigned long EDD_RESPONSE_CODE_TYPE_PROCESS_ERROR = 5;
   const unsigned long EDD_RESPONSE_CODE_TYPE_MISC_ERROR = 6;
const unsigned long EDD_RESPONSE_CODE_ATTR_DESCRIPTION = 2;
const unsigned long EDD_RESPONSE_CODE_ATTR_HELP = 3;

const unsigned long EDD_SOURCE_ATTR_HELP = 0;
const unsigned long EDD_SOURCE_ATTR_LABEL = 1;
const unsigned long EDD_SOURCE_ATTR_EMPHASIS = 2;
const unsigned long EDD_SOURCE_ATTR_VALIDITY = 3;
const unsigned long EDD_SOURCE_ATTR_VISIBILITY = 4;
const unsigned long EDD_SOURCE_ATTR_LINE_TYPE = 5;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA = 0;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA1 = 1;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA2 = 2;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA3 = 3;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA4 = 4;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA5 = 5;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA6 = 6;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA7 = 7;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA8 = 8;
   const unsigned long EDD_SOURCE_LINE_TYPE_DATA9 = 9;
   const unsigned long EDD_SOURCE_LINE_TYPE_LOW_LOW_LIMIT = 10;
   const unsigned long EDD_SOURCE_LINE_TYPE_LOW_LIMIT = 11;
   const unsigned long EDD_SOURCE_LINE_TYPE_HIGH_LIMIT = 12;
   const unsigned long EDD_SOURCE_LINE_TYPE_HIGH_HIGH_LIMIT = 13;
   const unsigned long EDD_SOURCE_LINE_TYPE_TRANSPARENT = 14;
const unsigned long EDD_SOURCE_ATTR_LINE_COLOR = 6;
const unsigned long EDD_SOURCE_ATTR_Y_AXIS = 7;
const unsigned long EDD_SOURCE_ATTR_MEMBERS = 8;
const unsigned long EDD_SOURCE_ATTR_INIT_ACTIONS = 9;
const unsigned long EDD_SOURCE_ATTR_REFRESH_ACTIONS = 10;
const unsigned long EDD_SOURCE_ATTR_EXIT_ACTIONS = 11;

const unsigned long EDD_VALUE_ARRAY_ATTR_TYPE = 0;
const unsigned long EDD_VALUE_ARRAY_ATTR_SIZE = 1;
const unsigned long EDD_VALUE_ARRAY_ATTR_LABEL = 2;
const unsigned long EDD_VALUE_ARRAY_ATTR_HELP = 3;
const unsigned long EDD_VALUE_ARRAY_ATTR_VALIDITY = 4;
const unsigned long EDD_VALUE_ARRAY_ATTR_VISIBILITY = 5;
const unsigned long EDD_VALUE_ARRAY_ATTR_RESPONSE_CODES = 6;

const unsigned long EDD_VARIABLE_ATTR_HELP = 0;
const unsigned long EDD_VARIABLE_ATTR_LABEL = 1;
const unsigned long EDD_VARIABLE_ATTR_RESPONSE_CODES = 2;
const unsigned long EDD_VARIABLE_ATTR_VALIDITY = 3;
const unsigned long EDD_VARIABLE_ATTR_VISIBILITY = 4;
const unsigned long EDD_VARIABLE_ATTR_VARIABLE_CLASS = 5;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_ALARM = 0x1;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_ANALOG_INPUT = 0x2;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_ANALOG_OUTPUT = 0x4;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_COMPUTATION = 0x8;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_CONTAINED = 0x10;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_CORRECTION = 0x20;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DEVICE = 0x40;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DIAGNOSTIC = 0x80;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DIGITAL_INPUT = 0x100;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DIGITAL_OUTPUT = 0x200;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DISCRETE = 0x400;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DISCRETE_INPUT = 0x800;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DISCRETE_OUTPUT = 0x1000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_DYNAMIC = 0x2000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_FREQUENCY = 0x4000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_FREQUENCY_INPUT = 0x8000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_FREQUENCY_OUTPUT = 0x10000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_HART = 0x20000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_HIDDEN = 0x40000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_INPUT = 0x80000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_LOCAL = 0x100000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_LOCAL_DISPLAY = 0x200000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_OPERATE = 0x400000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_OUTPUT = 0x800000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_SERVICE = 0x1000000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_TUNE = 0x2000000;
   const unsigned long EDD_VARIABLE_VARIABLE_CLASS_USER_INTERFACE = 0x4000000;
const unsigned long EDD_VARIABLE_ATTR_STYLE = 6;
const unsigned long EDD_VARIABLE_ATTR_CONSTANT_UNIT = 7;
const unsigned long EDD_VARIABLE_ATTR_HANDLING = 8;
   const unsigned long EDD_VARIABLE_HANDLING_READ = 0x1;
   const unsigned long EDD_VARIABLE_HANDLING_WRITE = 0x2;
const unsigned long EDD_VARIABLE_ATTR_PRE_EDIT_ACTIONS = 9;
const unsigned long EDD_VARIABLE_ATTR_POST_EDIT_ACTIONS = 10;
const unsigned long EDD_VARIABLE_ATTR_PRE_READ_ACTIONS = 11;
const unsigned long EDD_VARIABLE_ATTR_POST_READ_ACTIONS = 12;
const unsigned long EDD_VARIABLE_ATTR_PRE_WRITE_ACTIONS = 13;
const unsigned long EDD_VARIABLE_ATTR_POST_WRITE_ACTIONS = 14;
const unsigned long EDD_VARIABLE_ATTR_REFRESH_ACTIONS = 15;
const unsigned long EDD_VARIABLE_ATTR_READ_TIMEOUT = 16;
const unsigned long EDD_VARIABLE_ATTR_WRITE_TIMEOUT = 17;
const unsigned long EDD_VARIABLE_ATTR_TYPE = 18;
const unsigned long EDD_VARIABLE_ATTR_PURPOSE = 19;
const unsigned long EDD_VARIABLE_ATTR_ARITHMETIC_OPTION = 20;

const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_TYPE = 0;
   const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_INTEGER = 0;
   const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_UNSIGNED = 1;
   const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_DOUBLE = 2;
   const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_FLOAT = 3;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_SIZE = 1;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_DISPLAY_FORMAT = 2;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_EDIT_FORMAT = 3;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_ENUMERATOR_LIST = 4;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_SCALING_FACTOR = 5;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_DEFAULT_VALUE = 6;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_INITIAL_VALUE = 7;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_MAX_VALUE = 8;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_MIN_VALUE = 9;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_MAX_VALUES = 10;
const unsigned long EDD_VARIABLE_TYPE_ARITHMETIC_ATTR_MIN_VALUES = 11;

const unsigned long EDD_MINMAX_VALUE_ATTR_TYPE = 0;
   const unsigned long EDD_MINMAX_VALUE_TYPE_MAXIMUM = 0;
   const unsigned long EDD_MINMAX_VALUE_TYPE_MINIMUM = 1;
const unsigned long EDD_MINMAX_VALUE_ATTR_EXPR = 1;
const unsigned long EDD_MINMAX_VALUE_ATTR_NR = 2;


const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_ATTR_TYPE = 0;
   const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_TYPE_ENUMERATED = 0;
   const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_TYPE_BITENUMERATED = 1;
const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_ATTR_SIZE = 1;
const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_ATTR_DEFAULT_VALUE = 2;
const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_ATTR_INITIAL_VALUE = 3;
const unsigned long EDD_VARIABLE_TYPE_ENUMERATED_ATTR_ENUMERATOR_LIST = 4;

const unsigned long EDD_VARIABLE_TYPE_STRING_ATTR_TYPE = 0;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_ASCII = 0;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_BITSTRING = 1;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_EUC = 2;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII = 3;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_PASSWORD = 4;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_VISIBLE = 5;
   const unsigned long EDD_VARIABLE_TYPE_STRING_TYPE_OCTET = 6;
const unsigned long EDD_VARIABLE_TYPE_STRING_ATTR_SIZE = 1;
const unsigned long EDD_VARIABLE_TYPE_STRING_ATTR_DEFAULT_VALUE = 2;
const unsigned long EDD_VARIABLE_TYPE_STRING_ATTR_INITIAL_VALUE = 3;
const unsigned long EDD_VARIABLE_TYPE_STRING_ATTR_ENUMERATOR_LIST = 4;

const unsigned long EDD_VARIABLE_TYPE_INDEX_ATTR_SIZE = 0;
const unsigned long EDD_VARIABLE_TYPE_INDEX_ATTR_REFERENCE = 1;
const unsigned long EDD_VARIABLE_TYPE_INDEX_ATTR_DEFAULT_VALUE = 2;
const unsigned long EDD_VARIABLE_TYPE_INDEX_ATTR_INITIAL_VALUE = 3;

const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_TYPE = 0;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DATE = 0;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DATE_AND_TIME = 1;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_TYPE_DURATION = 2;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_TYPE_TIME = 3;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_TYPE_TIME_VALUE = 4;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_SIZE = 1;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_DEFAULT_VALUE = 2;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_INITIAL_VALUE = 3;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_SCALE = 4;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_SCALE_SECONDS = 0x1;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_SCALE_MINUTES = 0x2;
   const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_SCALE_HOURS = 0x4;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_DISPLAY_FORMAT = 5;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_EDIT_FORMAT = 6;
const unsigned long EDD_VARIABLE_TYPE_DATE_TIME_ATTR_TIME_FORMAT = 7;

const unsigned long EDD_VARIABLE_TYPE_OBJECT_ATTR_DEFAULT_REFERENCE = 0;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_PARENT = 0;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_CHILD = 1;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_SELF = 2;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_NEXT = 3;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_PREV = 4;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_FIRST = 5;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_LAST = 6;
   const unsigned long EDD_VARIABLE_TYPE_OBJECT_DEFAULT_REFERENCE_NULL = 7;

const unsigned long EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_VALUE = 0;
const unsigned long EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_DESCRIPTION = 1;
const unsigned long EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_HELP = 2;
const unsigned long EDD_VARIABLE_ENUMERATOR_ENUM_ATTR_COMMA = 3;

const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_INTEGER = 0;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_DESCRIPTION = 1;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_HELP = 2;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_VARIABLE_CLASS = 3;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_ALARM = 0x1;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_ANALOG_INPUT = 0x2;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_ANALOG_OUTPUT = 0x4;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_COMPUTATION = 0x8;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_CONTAINED = 0x10;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_CORRECTION = 0x20;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DEVICE = 0x40;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DIAGNOSTIC = 0x80;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DIGITAL_INPUT = 0x100;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DIGITAL_OUTPUT = 0x200;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DISCRETE = 0x400;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DISCRETE_INPUT = 0x800;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DISCRETE_OUTPUT = 0x1000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_DYNAMIC = 0x2000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_FREQUENCY = 0x4000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_FREQUENCY_INPUT = 0x8000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_FREQUENCY_OUTPUT = 0x10000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_HART = 0x20000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_HIDDEN = 0x40000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_INPUT = 0x80000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_LOCAL = 0x100000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_LOCAL_DISPLAY = 0x200000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_OPERATE = 0x400000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_OUTPUT = 0x800000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_SERVICE = 0x1000000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_TUNE = 0x2000000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_VARIABLE_CLASS_USER_INTERFACE = 0x4000000;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_STATUS_CLASS = 4;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_DATA = 0x1;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_HARDWARE = 0x2;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_MISC = 0x4;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_MODE = 0x8;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_PROCESS = 0x10;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_SOFTWARE = 0x20;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_CORRECTABLE = 0x40;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_SELF_CORRECTING = 0x80;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_UNCORRECTABLE = 0x100;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_EVENT = 0x200;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_STATE = 0x400;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_COMM_ERROR = 0x800;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_IGNORE_IN_HANDHELD = 0x1000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_IGNORE_IN_TEMPORARY_MASTER = 0x2000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_MORE = 0x4000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_BAD = 0x8000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_DETAIL = 0x10000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_SUMMARY = 0x20000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_ALL = 0x40000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_AO = 0x80000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_DV = 0x100000;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_TV = 0x200000;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_STATUS_CLASS_DEVICE = 5;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_STATUS_CLASS_RELIABILITY = 6;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_RELIABILITY_MANUAL = 0;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_RELIABILITY_AUTO = 1;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_STATUS_CLASS_MODE = 7;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_MODE_GOOD = 0;
   const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_STATUS_CLASS_MODE_BAD = 1;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_METHOD_REFERENCE = 8;
const unsigned long EDD_VARIABLE_ENUMERATOR_BITENUM_ATTR_COMMA = 9;

const unsigned long EDD_VARIABLE_LIST_ATTR_HELP = 0;
const unsigned long EDD_VARIABLE_LIST_ATTR_LABEL = 1;
const unsigned long EDD_VARIABLE_LIST_ATTR_MEMBERS = 2;
const unsigned long EDD_VARIABLE_LIST_ATTR_RESPONSE_CODES = 3;

const unsigned long EDD_WAVEFORM_ATTR_HELP = 0;
const unsigned long EDD_WAVEFORM_ATTR_LABEL = 1;
const unsigned long EDD_WAVEFORM_ATTR_HANDLING = 2;
   const unsigned long EDD_WAVEFORM_HANDLING_READ = 0x1;
   const unsigned long EDD_WAVEFORM_HANDLING_WRITE = 0x2;
const unsigned long EDD_WAVEFORM_ATTR_INIT_ACTIONS = 3;
const unsigned long EDD_WAVEFORM_ATTR_REFRESH_ACTIONS = 4;
const unsigned long EDD_WAVEFORM_ATTR_EXIT_ACTIONS = 5;
const unsigned long EDD_WAVEFORM_ATTR_KEY_POINTS = 6;
const unsigned long EDD_WAVEFORM_ATTR_EMPHASIS = 7;
const unsigned long EDD_WAVEFORM_ATTR_VALIDITY = 8;
const unsigned long EDD_WAVEFORM_ATTR_VISIBILITY = 9;
const unsigned long EDD_WAVEFORM_ATTR_LINE_TYPE = 10;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA = 0;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA1 = 1;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA2 = 2;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA3 = 3;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA4 = 4;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA5 = 5;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA6 = 6;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA7 = 7;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA8 = 8;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_DATA9 = 9;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_LOW_LOW_LIMIT = 10;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_LOW_LIMIT = 11;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_HIGH_LIMIT = 12;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_HIGH_HIGH_LIMIT = 13;
   const unsigned long EDD_WAVEFORM_LINE_TYPE_TRANSPARENT = 14;
const unsigned long EDD_WAVEFORM_ATTR_LINE_COLOR = 11;
const unsigned long EDD_WAVEFORM_ATTR_Y_AXIS = 12;
const unsigned long EDD_WAVEFORM_ATTR_TYPE = 13;

const unsigned long EDD_KEY_POINTS_ATTR_X_VALUES = 0;
const unsigned long EDD_KEY_POINTS_ATTR_Y_VALUES = 1;

const unsigned long EDD_WAVEFORM_TYPE_YT_ATTR_X_INITIAL = 0;
const unsigned long EDD_WAVEFORM_TYPE_YT_ATTR_X_INCREMENT = 1;
const unsigned long EDD_WAVEFORM_TYPE_YT_ATTR_Y_VALUES = 2;
const unsigned long EDD_WAVEFORM_TYPE_YT_ATTR_NUMBER_OF_POINTS = 3;

const unsigned long EDD_WAVEFORM_TYPE_XY_ATTR_X_VALUES = 0;
const unsigned long EDD_WAVEFORM_TYPE_XY_ATTR_Y_VALUES = 1;
const unsigned long EDD_WAVEFORM_TYPE_XY_ATTR_NUMBER_OF_POINTS = 2;

const unsigned long EDD_WAVEFORM_TYPE_HORIZONTAL_ATTR_Y_VALUES = 0;

const unsigned long EDD_WAVEFORM_TYPE_VERTICAL_ATTR_X_VALUES = 0;

const unsigned long EDD_CONSTRAINT_IF_ATTR_IF = 0;
const unsigned long EDD_CONSTRAINT_IF_ATTR_THEN = 1;
const unsigned long EDD_CONSTRAINT_IF_ATTR_ELSE = 2;

const unsigned long EDD_CONSTRAINT_SELECT_ATTR_SWITCH = 0;
const unsigned long EDD_CONSTRAINT_SELECT_ATTR_STMTS = 1;

const unsigned long EDD_CONSTRAINT_SELECTION_ATTR_CASE = 0;
const unsigned long EDD_CONSTRAINT_SELECTION_ATTR_STMT = 1;

const unsigned long EDD_EXPR_PRIMARY_ATTR_VALUE = 0;

const unsigned long EDD_EXPR_POSTFIX_ATTR_OP = 0;
   const unsigned long EDD_EXPR_POSTFIX_OP_INCR = 0;
   const unsigned long EDD_EXPR_POSTFIX_OP_DECR = 1;
const unsigned long EDD_EXPR_POSTFIX_ATTR_EXPR = 1;

const unsigned long EDD_EXPR_UNARY_ATTR_OP = 0;
   const unsigned long EDD_EXPR_UNARY_OP_INCR = 0;
   const unsigned long EDD_EXPR_UNARY_OP_DECR = 1;
   const unsigned long EDD_EXPR_UNARY_OP_POS = 2;
   const unsigned long EDD_EXPR_UNARY_OP_NEG = 3;
   const unsigned long EDD_EXPR_UNARY_OP_INV = 4;
   const unsigned long EDD_EXPR_UNARY_OP_NOT = 5;
const unsigned long EDD_EXPR_UNARY_ATTR_EXPR = 1;

const unsigned long EDD_EXPR_BINARY_ATTR_OP = 0;
   const unsigned long EDD_EXPR_BINARY_OP_MUL = 0;
   const unsigned long EDD_EXPR_BINARY_OP_DIV = 1;
   const unsigned long EDD_EXPR_BINARY_OP_MODULO = 2;
   const unsigned long EDD_EXPR_BINARY_OP_ADD = 3;
   const unsigned long EDD_EXPR_BINARY_OP_SUB = 4;
   const unsigned long EDD_EXPR_BINARY_OP_L_SHIFT = 5;
   const unsigned long EDD_EXPR_BINARY_OP_R_SHIFT = 6;
   const unsigned long EDD_EXPR_BINARY_OP_LESS = 7;
   const unsigned long EDD_EXPR_BINARY_OP_GREATER = 8;
   const unsigned long EDD_EXPR_BINARY_OP_LESS_EQUAL = 9;
   const unsigned long EDD_EXPR_BINARY_OP_GREATER_EQUAL = 10;
   const unsigned long EDD_EXPR_BINARY_OP_EQUAL = 11;
   const unsigned long EDD_EXPR_BINARY_OP_NOT_EQUAL = 12;
   const unsigned long EDD_EXPR_BINARY_OP_AND = 13;
   const unsigned long EDD_EXPR_BINARY_OP_XOR = 14;
   const unsigned long EDD_EXPR_BINARY_OP_OR = 15;
   const unsigned long EDD_EXPR_BINARY_OP_L_AND = 16;
   const unsigned long EDD_EXPR_BINARY_OP_L_OR = 17;
const unsigned long EDD_EXPR_BINARY_ATTR_EXPR1 = 1;
const unsigned long EDD_EXPR_BINARY_ATTR_EXPR2 = 2;

const unsigned long EDD_EXPR_CONDITIONAL_ATTR_EXPR = 0;
const unsigned long EDD_EXPR_CONDITIONAL_ATTR_EXPR1 = 1;
const unsigned long EDD_EXPR_CONDITIONAL_ATTR_EXPR2 = 2;

const unsigned long EDD_EXPR_ASSIGNMENT_ATTR_OP = 0;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_SIMPLE = 0;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_MUL = 1;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_DIV = 2;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_MODULO = 3;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_ADD = 4;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_SUB = 5;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_R_SHIFT = 6;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_L_SHIFT = 7;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_AND = 8;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_XOR = 9;
   const unsigned long EDD_EXPR_ASSIGNMENT_OP_OR = 10;
const unsigned long EDD_EXPR_ASSIGNMENT_ATTR_UNARY = 1;
const unsigned long EDD_EXPR_ASSIGNMENT_ATTR_EXPR = 2;

const unsigned long EDD_EXPR_COMMA_ATTR_EXPR1 = 0;
const unsigned long EDD_EXPR_COMMA_ATTR_EXPR2 = 1;

const unsigned long EDD_DICTIONARY_REFERENCE_ATTR_IDENTIFIER = 0;

const unsigned long EDD_TYPED_REFERENCE_ATTR_TYPE = 0;
const unsigned long EDD_TYPED_REFERENCE_ATTR_REFERENCE = 1;
const unsigned long EDD_TYPED_REFERENCE_ATTR_COMMA = 2;

const unsigned long EDD_METHOD_REFERENCE_ATTR_REFERENCE = 0;
const unsigned long EDD_METHOD_REFERENCE_ATTR_COMMA = 1;

const unsigned long EDD_REFERENCE_IDENT_ATTR_IDENTIFIER = 0;
const unsigned long EDD_REFERENCE_IDENT_ATTR_COMMA = 1;

const unsigned long EDD_REFERENCE_ARRAY_REF_ATTR_REFERENCE = 0;
const unsigned long EDD_REFERENCE_ARRAY_REF_ATTR_EXPR = 1;
const unsigned long EDD_REFERENCE_ARRAY_REF_ATTR_COMMA = 2;

const unsigned long EDD_REFERENCE_CALL_ATTR_REFERENCE = 0;
const unsigned long EDD_REFERENCE_CALL_ATTR_ARGS = 1;
const unsigned long EDD_REFERENCE_CALL_ATTR_COMMA = 2;

const unsigned long EDD_REFERENCE_RECORD_ATTR_REFERENCE = 0;
const unsigned long EDD_REFERENCE_RECORD_ATTR_IDENTIFIER = 1;
const unsigned long EDD_REFERENCE_RECORD_ATTR_COMMA = 2;

const unsigned long EDD_REFERENCE_BLOCK_ATTR_IDENTIFIER = 0;
const unsigned long EDD_REFERENCE_BLOCK_ATTR_COMMA = 1;

const unsigned long EDD_STRING_ATTR_LIST = 0;

const unsigned long EDD_STRING_TERMINAL_ATTR_REF = 0;
const unsigned long EDD_STRING_TERMINAL_ATTR_INTEGER = 1;

const unsigned long EDD_c_char_ATTR_INTEGER = 0;

const unsigned long EDD_c_dictionary_ATTR_IDENTIFIER = 0;

const unsigned long EDD_c_identifier_ATTR_IDENTIFIER = 0;

const unsigned long EDD_c_integer_ATTR_INTEGER = 0;

const unsigned long EDD_c_real_ATTR_REAL = 0;

const unsigned long EDD_c_string_ATTR_STRING = 0;

const unsigned long EDD_c_edd_terminal_ATTR_TERMINAL = 0;

const unsigned long EDD_c_primary_expr_ATTR_VALUE = 0;

const unsigned long EDD_c_postfix_expr_array_ATTR_EXPR = 0;
const unsigned long EDD_c_postfix_expr_array_ATTR_INDEX = 1;

const unsigned long EDD_c_postfix_expr_struct_ATTR_EXPR = 0;
const unsigned long EDD_c_postfix_expr_struct_ATTR_IDENTIFIER = 1;

const unsigned long EDD_c_postfix_expr_call_ATTR_IDENTIFIER = 0;
const unsigned long EDD_c_postfix_expr_call_ATTR_ARGS = 1;

const unsigned long EDD_c_postfix_expr_op_ATTR_OP = 0;
   const unsigned long EDD_c_postfix_expr_op_OP_INCR = 0;
   const unsigned long EDD_c_postfix_expr_op_OP_DECR = 1;
const unsigned long EDD_c_postfix_expr_op_ATTR_EXPR = 1;

const unsigned long EDD_c_unary_expr_ATTR_OP = 0;
   const unsigned long EDD_c_unary_expr_OP_INCR = 0;
   const unsigned long EDD_c_unary_expr_OP_DECR = 1;
   const unsigned long EDD_c_unary_expr_OP_POS = 2;
   const unsigned long EDD_c_unary_expr_OP_NEG = 3;
   const unsigned long EDD_c_unary_expr_OP_INV = 4;
   const unsigned long EDD_c_unary_expr_OP_NOT = 5;
const unsigned long EDD_c_unary_expr_ATTR_EXPR = 1;

const unsigned long EDD_c_cast_expr_ATTR_SPECIFIER = 0;
   const unsigned long EDD_c_cast_expr_SPECIFIER_SIGNED = 0;
   const unsigned long EDD_c_cast_expr_SPECIFIER_UNSIGNED = 1;
   const unsigned long EDD_c_cast_expr_SPECIFIER_CHAR = 2;
   const unsigned long EDD_c_cast_expr_SPECIFIER_SHORT = 3;
   const unsigned long EDD_c_cast_expr_SPECIFIER_INT = 4;
   const unsigned long EDD_c_cast_expr_SPECIFIER_LONG = 5;
   const unsigned long EDD_c_cast_expr_SPECIFIER_FLOAT = 6;
   const unsigned long EDD_c_cast_expr_SPECIFIER_DOUBLE = 7;
const unsigned long EDD_c_cast_expr_ATTR_EXPR = 1;

const unsigned long EDD_c_binary_expr_ATTR_OP = 0;
   const unsigned long EDD_c_binary_expr_OP_MUL = 0;
   const unsigned long EDD_c_binary_expr_OP_DIV = 1;
   const unsigned long EDD_c_binary_expr_OP_MODULO = 2;
   const unsigned long EDD_c_binary_expr_OP_ADD = 3;
   const unsigned long EDD_c_binary_expr_OP_SUB = 4;
   const unsigned long EDD_c_binary_expr_OP_L_SHIFT = 5;
   const unsigned long EDD_c_binary_expr_OP_R_SHIFT = 6;
   const unsigned long EDD_c_binary_expr_OP_LESS = 7;
   const unsigned long EDD_c_binary_expr_OP_GREATER = 8;
   const unsigned long EDD_c_binary_expr_OP_LESS_EQUAL = 9;
   const unsigned long EDD_c_binary_expr_OP_GREATER_EQUAL = 10;
   const unsigned long EDD_c_binary_expr_OP_EQUAL = 11;
   const unsigned long EDD_c_binary_expr_OP_NOT_EQUAL = 12;
   const unsigned long EDD_c_binary_expr_OP_AND = 13;
   const unsigned long EDD_c_binary_expr_OP_XOR = 14;
   const unsigned long EDD_c_binary_expr_OP_OR = 15;
   const unsigned long EDD_c_binary_expr_OP_L_AND = 16;
   const unsigned long EDD_c_binary_expr_OP_L_OR = 17;
const unsigned long EDD_c_binary_expr_ATTR_EXPR1 = 1;
const unsigned long EDD_c_binary_expr_ATTR_EXPR2 = 2;

const unsigned long EDD_c_conditional_expr_ATTR_EXPR = 0;
const unsigned long EDD_c_conditional_expr_ATTR_EXPR1 = 1;
const unsigned long EDD_c_conditional_expr_ATTR_EXPR2 = 2;

const unsigned long EDD_c_assignment_expr_ATTR_OP = 0;
   const unsigned long EDD_c_assignment_expr_OP_SIMPLE = 0;
   const unsigned long EDD_c_assignment_expr_OP_MUL = 1;
   const unsigned long EDD_c_assignment_expr_OP_DIV = 2;
   const unsigned long EDD_c_assignment_expr_OP_MODULO = 3;
   const unsigned long EDD_c_assignment_expr_OP_ADD = 4;
   const unsigned long EDD_c_assignment_expr_OP_SUB = 5;
   const unsigned long EDD_c_assignment_expr_OP_R_SHIFT = 6;
   const unsigned long EDD_c_assignment_expr_OP_L_SHIFT = 7;
   const unsigned long EDD_c_assignment_expr_OP_AND = 8;
   const unsigned long EDD_c_assignment_expr_OP_XOR = 9;
   const unsigned long EDD_c_assignment_expr_OP_OR = 10;
const unsigned long EDD_c_assignment_expr_ATTR_UNARY = 1;
const unsigned long EDD_c_assignment_expr_ATTR_EXPR = 2;

const unsigned long EDD_c_comma_expr_ATTR_EXPR1 = 0;
const unsigned long EDD_c_comma_expr_ATTR_EXPR2 = 1;

const unsigned long EDD_c_declaration_ATTR_SPECIFIER = 0;
   const unsigned long EDD_c_declaration_SPECIFIER_SIGNED = 0;
   const unsigned long EDD_c_declaration_SPECIFIER_UNSIGNED = 1;
   const unsigned long EDD_c_declaration_SPECIFIER_CHAR = 2;
   const unsigned long EDD_c_declaration_SPECIFIER_SHORT = 3;
   const unsigned long EDD_c_declaration_SPECIFIER_INT = 4;
   const unsigned long EDD_c_declaration_SPECIFIER_LONG = 5;
   const unsigned long EDD_c_declaration_SPECIFIER_FLOAT = 6;
   const unsigned long EDD_c_declaration_SPECIFIER_DOUBLE = 7;
   const unsigned long EDD_c_declaration_SPECIFIER_STRING = 8;
const unsigned long EDD_c_declaration_ATTR_DECLARATOR = 1;

const unsigned long EDD_c_declarator_ATTR_IDENTIFIER = 0;
const unsigned long EDD_c_declarator_ATTR_ARRAY = 1;

const unsigned long EDD_c_declarator_array_specifier_ATTR_EXPR = 0;

const unsigned long EDD_c_compound_statement_ATTR_DECLS = 0;
const unsigned long EDD_c_compound_statement_ATTR_STMTS = 1;
const unsigned long EDD_c_compound_statement_ATTR_END = 2;

const unsigned long EDD_c_expr_statement_ATTR_EXPR = 0;

const unsigned long EDD_c_selection_statement_if_ATTR_IF = 0;
const unsigned long EDD_c_selection_statement_if_ATTR_THEN = 1;
const unsigned long EDD_c_selection_statement_if_ATTR_ELSE = 2;

const unsigned long EDD_c_selection_statement_switch_ATTR_SWITCH = 0;
const unsigned long EDD_c_selection_statement_switch_ATTR_STMT = 1;

const unsigned long EDD_c_labeled_statement_ATTR_CASE = 0;
const unsigned long EDD_c_labeled_statement_ATTR_STMT = 1;

const unsigned long EDD_c_iteration_statement_do_ATTR_STMT = 0;
const unsigned long EDD_c_iteration_statement_do_ATTR_DO = 1;

const unsigned long EDD_c_iteration_statement_for_ATTR_START = 0;
const unsigned long EDD_c_iteration_statement_for_ATTR_WHILE = 1;
const unsigned long EDD_c_iteration_statement_for_ATTR_EXPR = 2;
const unsigned long EDD_c_iteration_statement_for_ATTR_STMT = 3;

const unsigned long EDD_c_jump_statement_ATTR_OP = 0;
   const unsigned long EDD_c_jump_statement_OP_CONTINUE = 0;
   const unsigned long EDD_c_jump_statement_OP_BREAK = 1;
   const unsigned long EDD_c_jump_statement_OP_RETURN = 2;
const unsigned long EDD_c_jump_statement_ATTR_EXPR = 1;

#endif /* DOXYGEN_IGNORE */


const unsigned long EDD_TYPE_FLAGS_LIST = 0x1;

/* BEGIN EXPORT */

/**
 * \internal
 * EDD type representation.
 * This is the central class that represent any EDD specific type.
 * It's used by the semantic checks inside the EDD compiler.
 */
class edd_type
{
private:
	typedef class edd_type Self;

	/* no copy/assignment constructor semantic defined */
	edd_type(const Self &);
	Self& operator=(const Self &);

public:
	/* normal constructor */
	edd_type(class EDD_OBJECT_tree *t);
	edd_type(class EDD_OBJECT_tree *t, const lib::pos &pos);
	edd_type(class EDD_OBJECT_tree *t, const lib::pos &pos, const lib::IDENTIFIER &ident);
	edd_type(class c_type c, const lib::pos &pos, bool lval = false);
	edd_type(class c_type c, const lib::pos &pos, class EDD_OBJECT_tree *t);

	/* destructor */
	~edd_type(void);

	/** Debugging support. */
	void dump(lib::stream &) const;

	/* general methods */
	class edd_type *next(void) const throw() { return nxt; }
	void concat(class edd_type *next) throw();
	int len(void) const throw();

	/* invalid type mean we already encountered errors */
	bool valid(void) const throw() { return ((link && link->kind != EDD_NONE) || myctype); }

	enum kinds kind(void) const throw() { return (link ? link->kind : EDD_NONE); }
	lib::pos linkpos(void) const throw() { return mylinkpos; }

	const lib::pos &pos(void) const throw() { return mypos; }
	const lib::IDENTIFIER &ident(void) const throw() { return myident; }
	const class c_type &ctype(void) const throw() { return myctype; }

	class EDD_OBJECT_tree *getlink(void) const throw() { return link; }
	const char *descr(void) const throw();

	/** array deref */
	class edd_type *deref(void) const;
	class edd_type *deref0(bool const_index, int index) const;

	/** structure lookup */
	class edd_type *lookup(const lib::IDENTIFIER &ident, const lib::pos &pos) const;
	class edd_type *lookup0(const lib::IDENTIFIER &ident, const lib::pos &pos) const;

	bool is_lval(void) const throw();
	bool is_constant(void) const throw();
	bool is_array(void) const throw();

	const unsigned long *types_array(void) const throw() { return types_array_; }

private:
	class edd_type *nxt;
	class EDD_OBJECT_tree *link;
	lib::pos mypos;
	lib::pos mylinkpos;
	lib::IDENTIFIER myident;
	class c_type myctype;
	bool lval;

	void setpos(const lib::pos &pos) throw() { mypos = pos; }
	void setlinkpos(const lib::pos &pos) throw() { mylinkpos = pos; }
	void setident(const lib::IDENTIFIER &ident) throw() { myident = ident; }

	static const unsigned long types_array_[];
};

/**
 * Method environment.
 * The environment is used to run dialogs/windows and methods. This
 * environment class hold all cache and context stacks of executed
 * dialogs/windows and methods. Allow cache and context stacking in
 * order to support recursivly stacked dialogs/windows and methods.
 * 
 * The cache hold modified operand values. The cache can be stacked
 * in frames as needed. If a frame is popped it can be either discarded
 * or applied to the next lower frame.
 * 
 * The context hold:
 * - the stackmachine context
 * - the abortlist
 * - the node object that pushed the context
 * .
 * 
 * Before a method is executed a new context is pushed. Called builtins
 * use this context to handle abort methods, resolving variable references,
 * pretty print strings, lookup implicit context variables and so on.
 * 
 * If the environment is destructed the remaining operand cache is
 * discarded, written back into the offline parameter set or written back
 * into the offline parameter set and sent to the device depending on the
 * cache state. The cache state can be set with
 * #set_op_cache_state. The default is
 * #save.
 * 
 * \note All interpreter actions that get an environment argument can also
 * be called with NULL. In this case, no caching is used and all data are
 * modified in the offline parameter set. For methods a default environment
 * is created that push all modified values back into the offline parameter
 * set after the method terminated.
 */
class method_env
{
private:
	typedef class method_env Self;

	/* no copy/assignment constructor semantic defined */
	method_env(const Self &);
	Self& operator=(const Self &);

public:
	/** \name Constructor/Destructor. */
	//! \{

	method_env(class edd_tree &, bool online = false);

	/* make class polymorphic */
	virtual ~method_env(void);

	//! \}

	/** The edd::edd_tree base class of this context. */
	class edd_tree &parent;

	/* shortcuts for logging services */
	void info(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void warning(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void error(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);
	void trace(const std::string &cat, const char *fmt, ...) CHECK_PRINTF(3, 4);

	/* BUILTINS category constant */
	static const std::string BUILTINS;

	/* context stack data & manipulation */
	class context
	{
	public:
		context(class node_tree *, operand_ptr, stackmachine::Context *);
		void clear(class node_tree *, operand_ptr, stackmachine::Context *);

		class node_tree *node;
		operand_ptr node_op;
		stackmachine::Context *c;
	};
	typedef class context context_t;
	typedef std::list<context_t> context_stack_t;

	/* access helper */
	class node_tree *outer_node(void) const;
	class node_tree *context_node(void) const throw() { assert(context.node); return context.node; }
	class VARIABLE_tree *context_variable(void) const throw();
	operand_ptr context_variable_value(void) const throw();

	void push_context(class node_tree *, operand_ptr, stackmachine::Context *);
	void pop_context(void);

	/* return topmost context */
	const context_t &get_context(void) const { return context; }

	/* context access */
	size_t nr_contexts(void) const;
	const context_t &get_context(size_t) const;

	class auto_context
	{
	public:
		auto_context(class method_env &,
			     class node_tree *, operand_ptr,
			     stackmachine::Context *);

		~auto_context(void);

	private:
		class method_env &env;
	};

	/* array index stack */
	operand_ptr get_array_index(void) const;
	void set_array_index(operand_ptr);

	/* operand cache */
	void push_cache(void);
	void discard_cache(void);
	void apply_cache(void);
	void dump(lib::stream &) const;

	class tree *id2tree(unsigned int id) const;
	void get_locals(stackmachine::Context *,
			std::map<lib::IDENTIFIER, stackmachine::Type *> &) const;

	stackmachine::Type *frame_lookup(const lib::IDENTIFIER &, stackmachine::Context *) const;
	stackmachine::Type *frame_lookup(const lib::IDENTIFIER &) const;
	stackmachine::Type *frame_lookup(const std::string &, stackmachine::Context *) const;

	std::string builtin_printf(const std::string &fmt,
				   edd_int32_array *ids,
				   std::list<class VARIABLE_tree *> *objects = NULL);

	void add_abort(class METHOD_tree *t);
	void remove_abort(class METHOD_tree *t);
	void push_abort(class METHOD_tree *t);
	void pop_abort(void);
	void clear_abortlist(void);

	void abort(void);
	void clear_abort(void);
	bool aborted(void) const throw();
	bool aborting(void) const throw();

	int retries(void) const throw();
	void set_retries(int n) throw();

	bool optimize(void) const throw();
	void set_optimize(bool) throw();

	bool do_relations(void) const throw();
	void set_do_relations(bool) throw();

	bool online(void) const throw() { return online_; }

	void age_stamp_tick(void) throw();
	void update_age_stamp(operand_ptr) const throw();
	bool check_age(operand_ptr) const throw();

	/* runtime method observation */
	void set_steps_notify(unsigned long = 0) throw();
	unsigned long get_steps_notify(void) const throw();
	
	/**
	 * Operand cache states.
	 * This are the possible operand cache states.
	 */
	typedef enum
	{
		discard, /**< Discard the cache. */
		save,    /**< Save the cache into the offline parameter set. */
		send     /**< Like save but also send to the device. */
	} op_cache_state_t;

	void set_op_cache_state(op_cache_state_t new_state) throw();
	op_cache_state_t get_op_cache_state(void) throw();
	void op_cache_flush(op_cache_state_t mode);

	operand_ptr op_cache_lookup(operand_ptr op) const;
	operand_ptr op_cache_insert(operand_ptr op);
	void op_cache_remove(operand_ptr op);
	void op_cache_remove_all(operand_ptr op);

	operand_ptr value(class EDD_OBJECT_tree *);
	operand_ptr value(operand_ptr);
	operand_ptr uncached_value(operand_ptr);

	static operand_ptr value(class method_env *, operand_ptr);

private:
	void fmt_printf(lib::dyn_buf &buf, size_t len, const char *fmt,
			edd_int32_array *ids, long index,
			std::list<class VARIABLE_tree *> *objects);
	void fmt_printf(lib::dyn_buf &buf, size_t len, const char *fmt,
			const lib::IDENTIFIER &ident,
			std::list<class VARIABLE_tree *> *objects);

	context_stack_t context_stack;
	context_t context;

	operand_ptr last_array_index;

	typedef std::list<class METHOD_tree *> abortlist_t;
	typedef std::list<abortlist_t> abortlist_stack_t;

	abortlist_stack_t abortlist_stack;
	abortlist_t abortlist;

	bool aborted_state;
	bool aborting_state;

	/* helper */
	void pop_context0(void);

	/** The retry count. */
	int my_retries;

	/** The handle relations flag. */
	bool optimize_;

	/** The handle relations flag. */
	bool do_relations_;

	/** Whether this is an online environment. */
	bool online_;

	/** age stamp counter */
	age_stamp_t age_stamp;

	/** number of notify steps */
	unsigned long steps_notify_;

	/**
	 * The operand cache of this method context.
	 * This hold from all locally modified EDD object lvalues a
	 * reference to the original lvalue inside the AST and a copy of the
	 * modified lvalue. If save_on_exit is called the modified lvalue is
	 * written back to the AST on normal context termination.
	 */
	typedef std::map<operand_ptr, operand_ptr> op_cache_t;
	typedef std::list<op_cache_t> op_cache_stack_t;

	op_cache_stack_t op_cache_stack;
	op_cache_t &op_cache(void) { return op_cache_stack.front(); }

	/**
	 * The operand cache state of this method context.
	 * Used at destruction time to finalize the operand cache
	 * automatically.
	 */
	op_cache_state_t op_cache_state;

	operand_ptr op_cache_lookup1(operand_ptr op) const;
	operand_ptr op_cache_insert1(operand_ptr op);
	void op_cache_remove1(operand_ptr op);
	void op_cache_remove_all1(operand_ptr op);

public:
	/*
	 * HART communication masks
	 */
	struct hart_masks
	{
		unsigned char comm_status_abort;
		unsigned char comm_status_retry;
		unsigned char device_status_abort;
		unsigned char device_status_retry;
		unsigned char response_code_abort;
		unsigned char response_code_retry;

		unsigned char cmd48_comm_status_abort;
		unsigned char cmd48_comm_status_retry;
		unsigned char cmd48_device_status_abort;
		unsigned char cmd48_device_status_retry;
		unsigned char cmd48_response_code_abort;
		unsigned char cmd48_response_code_retry;

		std::vector<unsigned char> cmd48_data_abort;
		std::vector<unsigned char> cmd48_data_retry;

		bool          comm_error_abort;
		bool          comm_error_retry;
		bool          cmd48_comm_error_abort;
		bool          cmd48_comm_error_retry;

		bool          no_device_abort;
		bool          no_device_retry;
		bool          cmd48_no_device_abort;
		bool          cmd48_no_device_retry;

		bool          response_all;
		bool          cmd48_response_all;

		/*
		 * helper constants
		 */
		static const unsigned int cmd48_data_maxlen      = 25;

		static const unsigned char ALL_BITS              = 0xff;
		static const unsigned char NO_BITS               = 0x00;

		/* response codes */
		static const unsigned char CMD_NOT_IMPLEMENTED   = 0x40;
		static const unsigned char DEVICE_IS_BUSY        = 0x20;
		static const unsigned char ACCESS_RESTRICTED     = 0x10;

		/* device status */
		static const unsigned char DEVICE_MALFUNCTION    = 0x80;
		static const unsigned char CONFIGURATION_CHANGED = 0x40;
		static const unsigned char COLD_START            = 0x20;
		static const unsigned char MORE_STATUS_AVAILABLE = 0x10;

		/* comm status */
        static const unsigned char PARITY_PERROR         = 0xC0;
        static const unsigned char OVERRUN_PERROR        = 0xA0;
        static const unsigned char FRAMING_PERROR        = 0x90;
        static const unsigned char CHECKSUM_PERROR       = 0x88;
		/* reserved                                      = 0x84; */
		static const unsigned char RX_BUFFER_OVERFLOW    = 0x82;
		/* undefined                                     = 0x81; */
	};
	typedef struct hart_masks hart_masks_t;
	hart_masks_t hart_masks;

private:
	typedef std::list<hart_masks_t> hart_masks_stack_t;
	hart_masks_stack_t hart_masks_stack;

	void hart_masks_init(hart_masks_t &masks);
};

/**
 * \internal
 * C function prototype representation.
 * This is the abstract interface that represents a C or a builtin method.
 * It's used in the semantic check and by the call dispatcher.
 * There are at least two specializations, one for EDD methods and one
 * for builtin functions.
 */
class c_method
{
private:
	typedef class c_method Self;

	/* no copy/assignment constructor semantic defined */
	c_method(const Self &);
	Self& operator=(const Self &);

public:
	c_method(void) { }
	virtual ~c_method(void);

	/** The name of this method. */
	virtual const char *ident(void) const = 0;

	enum calling { byvalue, byref, byref_array };

	/** The return type of the method. */
	virtual c_type::typespec returntype(void) const = 0;

	/* arguments */
	virtual unsigned int argcount(void) const = 0;
	virtual c_type::typespec argtype(unsigned int index) const = 0;
	virtual calling argmethod(unsigned int index) const = 0;

	/* special method */
	virtual bool special(void) const { return false; }

	/** Next overloaded method or 0. */
	virtual const c_method *next(void) const throw();

	/** Method specific additional check. */
	virtual void check(class edd_tree * const parent,
			   class METHOD_BASE_tree * const method,
		   	   const lib::pos &pos,
			   const std::vector<class c_node_tree *> &args,
			   std::vector<class c_type> &targs) const { }

	/** Method name used as local variable name. */
	virtual void name_conflict(class edd_tree * const parent,
				   class METHOD_BASE_tree * const method,
		   		   const lib::pos &pos) const { }

	/** The calling dispatch. */
	virtual operand_ptr dispatch(class method_env *,
				     operand_ptr args, const lib::pos &) const = 0;

	/**
	 * Method have own code generation.
	 * Return true if edd::c_method::code should be called instead the
	 * default code generation.
	 */
	virtual bool self_code(void) const throw();

	/**
	 * Method specific code generation.
	 * This overwrite the default code generation of a calling dispatch to
	 * this method. Can be used to inline short things like simple
	 * builtins. Can only be called if edd::c_method::self_code return
	 * true.
	 */
	virtual void code(class edd_tree * const __parent,
			  class c_node_tree * const caller,
			  stackmachine::SMachine &sm,
			  class c_compound_statement_tree *env,
			  const std::vector<class c_node_tree *> &args,
			  const std::vector<class c_type> &targs) const;
};

/* END EXPORT */
} /* namespace edd */
#endif /* _edd_ast_h */
