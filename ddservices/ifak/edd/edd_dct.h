
/* 
 * $Id: edd_dct.h,v 1.24 2009/05/11 15:32:50 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the edd generic dictionary.
 */

#ifndef _edd_dct_h
#define _edd_dct_h

// C stdlib

// C++ stdlib
#include <map>
#include <string>

// my lib
#include "ident.h"
#include "position.h"

// own header


namespace edd
{

/**
 * The dictionary implementation.
 * This is the dictionary implementation used by the eddlib.
 */
class edd_dictionary
{
public:
	std::string lookup(const lib::IDENTIFIER &) const;
	std::string lookup(const lib::IDENTIFIER &, bool &) const;
	lib::pos get_pos(const lib::IDENTIFIER &) const;

	bool exist(const lib::IDENTIFIER &id)
	{ return map.find(id) != map.end(); }

	void add(const lib::IDENTIFIER &id, const std::string &str, const lib::pos &pos)
	{ map[id] = entry_t(str, pos); }

	void remove(const lib::IDENTIFIER &id)
	{ map.erase(id); }

	void clear(void)
	{ map.clear(); }

	void dump(lib::stream &) const;

	typedef std::pair<std::string, lib::pos> entry_t;
	typedef std::map<lib::IDENTIFIER, entry_t> map_t;

	const map_t &data(void) const throw() { return map; }

private:
	map_t map;
};

} /* namespace edd */

#endif /* _edd_dct_h */
