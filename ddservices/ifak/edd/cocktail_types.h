
/* 
 * $Id: cocktail_types.h,v 1.8 2009/07/18 09:49:33 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the scanner/parser edd types.
 */

#ifndef _cocktail_types_h
#define _cocktail_types_h

// C stdlib

// C++ stdlib

// my lib
#include "sys_types.h"

/**
 * Namespace for system independant EDD interpreter component.
 * All edd related or specific classes, enums, constants etc. are defined in
 * this namespace. This is useful to distingush shortly if something is
 * related directly to the EDD library or is part of one of the
 * subsystems.
 */
namespace edd
{

/* macros for the cocktail scanner and parser */
#define CT_NAMESPACE edd
#define CT_ERR edd_err

/**
 * PARENT type definition for the cocktail scanner and parser.
 */
typedef class edd_tree PARENT;

/**
 * Increasing age stamp data type for remembering the age of a variable value.
 */
typedef sys::uint32 age_stamp_t;
#define PRI_AGE_STAMP_T PRIu32

/* forward declarations */
class edd_tree;

class c_type;
class c_method;
class method_env;
class operand;

class tree;
class node_tree;
class EDD_OBJECT_tree;
class COMMAND_tree;
class MENU_tree;
class METHOD_BASE_tree;
class VALUE_ARRAY_tree;
class VARIABLE_tree;

class c_node_tree;
class c_compound_statement_tree;

} /* namespace edd */

namespace stackmachine
{

/* forward declarations */
class smachine;

} /* namespace stackmachine */

#endif /* _cocktail_types_h */
