
/* 
 * $Id: fmtcheck.cxx,v 1.4 2009/02/04 09:50:47 fna Exp $
 * 
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// C++ stdlib

// my lib

// own header
#include "fmtcheck.h"


namespace edd
{

#ifndef __NetBSD__

/*
 * Import the NetBSD fmtcheck(3) routine.
 */
#ifndef DOXYGEN_IGNORE
# include "fmtcheck.c"
#endif

#endif

} /* namespace edd */
