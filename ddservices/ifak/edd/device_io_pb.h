
/* 
 * $Id: device_io_pb.h,v 1.21 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2003-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _device_io_pb_h
#define _device_io_pb_h

// C stdlib
#include <stdlib.h>

// C++ stdlib
#include <set>
#include <vector>

// other libs

// my lib
#include "Timeval.h"

// own header
#include "device_io.h"


namespace edd
{

/**
 * Profibus communication base.
 * This communication specialization support the profibus protocol.
 * It include all generic things like executing COMMANDs, reading
 * the profibus directory and do the relative to absolute address mapping.
 */
class device_io_pb : public device_io
{
public:
    device_io_pb(class Interpreter &);

    /* low-level I/O interface */
    struct request
    {
        unsigned char slot;
        unsigned char index;
        size_t datalen;
        unsigned char *data;
    };

	/* implement device_io_interface */
	virtual int do_command(class method_env *,
			       const class COMMAND_tree *, int, enum action,
			       int &warnings);

	/* enable/disable directory caching */
	void cache_directory(bool flag) { cache_directory_flag = flag; }

protected:
    /* overwrite device_io */
    virtual void state_change(enum state);
	virtual long pb_read(struct request &) = 0;
	virtual long pb_write(const struct request &) = 0;

private:
	void dump_data(const class COMMAND_tree *cmd, const char *op,
		       const unsigned char *buf, size_t buflen, const char *descr);
	void do_timeout(const class COMMAND_tree *cmd, const char *op,
			long timeout);
	int  do_read_command(const class COMMAND_tree *, int, class method_env *, int &warnings);
	int  do_write_command(const class COMMAND_tree *, int, class method_env *, int &warnings);

	/* profibus dictionary data
	 * for relative to absolute command mapping
	 */
	unsigned short m_Num_PB; /* Physical Block */
	unsigned short m_Num_TB; /* Transducer Block */
	unsigned short m_Num_FB; /* Function Block */
	unsigned short m_Num_LO; /* LinkObject */

	struct DevBlockParType
	{
		unsigned char Slot;
		unsigned char Index;
		unsigned short Num_Par;
	};
	typedef std::vector<struct DevBlockParType> m_Parameter_t;

	m_Parameter_t m_PB_Parameter;
	m_Parameter_t m_TB_Parameter;
	m_Parameter_t m_FB_Parameter;
	m_Parameter_t m_LO_Parameter;

	bool directory_done_flag;
	bool cache_directory_flag;

	bool pb_readdir(void);
	bool finalize_parameter(const unsigned char *dir_data, int dir_data_len,
				m_Parameter_t &m_Parameter, unsigned short m_Num,
				unsigned char Ofs, const char *descr);

	/**
	 * Calculate slot and index for the specified command.
	 * This method always return slot and index as absolute values.
	 * - absolut: SLOT and INDEX
	 * - relative: BLOCK und INDEX ==> from directory: slot 1, index 1
	 */
	bool pb_resolve_cmd(const class COMMAND_tree *, const char *op,
			    class method_env *,
			    unsigned char &slot, unsigned char &index);


	/* timeout handling */
	long required_timeout;
	os_service::Timeval timestamp;
};

} /* namespace edd */

#endif /* _device_io_pb_h */
