
/* 
 * $Id: InternalEvaluationLicense.cxx,v 1.6 2009/05/19 12:35:33 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InternalEvaluationLicense.h"


bool
InternalEvaluationLicense::have_checker_license()
{
	return true;
}

bool
InternalEvaluationLicense::have_workbench_license()
{
	return true;
}

bool
InternalEvaluationLicense::have_comm_license(int profile)
{
	return true;
}

std::string
InternalEvaluationLicense::get_error_msg() const
{
	return std::string();
}

long
InternalEvaluationLicense::get_error_code() const
{
	return 0;
}

std::string
InternalEvaluationLicense::get_expiration_info() const
{
	return std::string("Internal Test Version");
}
