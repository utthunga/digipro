
/* 
 * $Id: DevicesParser.cxx,v 1.8 2009/07/01 12:50:41 fna Exp $
 * 
 * Copyright (C) 2007-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <stdio.h>
#include <stdlib.h>

// C++ stdlib
#include <stdexcept>

// other libs

// my lib
#include "scratch_buf.h"

// own header
#include "DevicesParser.h"


namespace edd
{

DevicesParser::DevicesParser(void)
: strict_checking(false)
{
}

int
DevicesParser::lookup_manufacturer_name(const std::string &name) const
{
	manufacturers_t::const_iterator iter(entries.begin());
	manufacturers_t::const_iterator end(entries.end());

	while (iter != end)
	{
		if (iter->second.ident == name)
			return iter->first;

		++iter;
	}

	return -1;
}

int
DevicesParser::lookup_device_type_name(int manufacturer_id, const std::string &name) const
{
	manufacturers_t::const_iterator iter(entries.find(manufacturer_id));

	if (valid(iter))
	{
		devices_t::const_iterator jter(iter->second.devices.begin());
		devices_t::const_iterator end(iter->second.devices.end());

		while (jter != end)
		{
			if (jter->second == name)
				return jter->first;

			++jter;
		}
	}

	return -1;
}

std::string
DevicesParser::lookup_manufacturer_id(int manufacturer_id) const
{
	manufacturers_t::const_iterator iter(entries.find(manufacturer_id));
	std::string ret;

	if (valid(iter))
		ret = iter->second.ident;

	return ret;
}

std::string
DevicesParser::lookup_device_type_id(int manufacturer_id, int device_type_id) const
{
	manufacturers_t::const_iterator iter(entries.find(manufacturer_id));
	std::string ret;

	if (valid(iter))
	{
		devices_t::const_iterator jter(iter->second.devices.find(device_type_id));

		if (jter != iter->second.devices.end())
			ret = jter->second;
	}

	return ret;
}

void
DevicesParser::load(const lib::stream &f)
{
	clear();
	parse(f);
}

void
DevicesParser::clear(void)
{
	entries.clear();
	line = 1;
	bytes = 0;
}

void
DevicesParser::get_next_char(const lib::stream &f)
{
	c = f.get();

	if (c != EOF)
		++bytes;
}

void
DevicesParser::parse(const lib::stream &f)
{
	std::string dummy;
	size_t curr_manufacturer = 0;
	bool first = true;

	get_next_char(f);

	skip_space(f, dummy);

	for (;;)
	{
		std::string key, str;
		int val;

		skip_space(f, dummy);

		if (c == EOF)
			break;

		expect_ident(f, key);
		expect_integer(f, val);
		expect(f, '=');
		expect_ident(f, str);

		if (key == "MANUFACTURER")
		{
			first = false;

			if (strict_checking && entries.find(val) != entries.end())
			{
				lib::scratch_buf buf(100);
				buf.printf("line %lu: syntax error, MANUFACTURER 0x%x double defined",
					   line, val);

				throw std::runtime_error(buf.str());
			}

			/* remember */
			curr_manufacturer = val;
			entries[curr_manufacturer].ident = str;
		}
		else if (key == "DEVICE_TYPE")
		{
			if (first)
			{
				lib::scratch_buf buf(100);
				buf.printf("line %lu: syntax error, expected 'MANUFACTURER'", line);

				throw std::runtime_error(buf.str());
			}

			devices_t &devices = entries[curr_manufacturer].devices;

			if (strict_checking && devices.find(val) != devices.end())
			{
				lib::scratch_buf buf(100);
				buf.printf("line %lu: syntax error, MANUFACTURER 0x%lx DEVICE_TYPE 0x%x double defined",
					   line, (unsigned long)curr_manufacturer, val);

				throw std::runtime_error(buf.str());
			}

			devices[val] = str;
		}
		else
		{
			lib::scratch_buf buf(100);
			buf.printf("line %lu: syntax error, expected 'MANUFACTURER' or 'DEVICE_TYPE'", line);

			throw std::runtime_error(buf.str());
		}
	}
}

void
DevicesParser::expect(const lib::stream &f, char expected)
{
	std::string skipped;

	skip_space(f, skipped);

	if (c != expected)
	{
		lib::scratch_buf buf(100);
		buf.printf("line %lu: syntax error, expected '%c'", line, expected);

		throw std::runtime_error(buf.str());
	}

	get_next_char(f);
}

void
DevicesParser::expect_ident(const lib::stream &f, std::string &ident)
{
	read_ident(f, ident);

	if (c == EOF || !ident.length())
	{
		lib::scratch_buf buf(100);
		buf.printf("line %lu: syntax error, expected identifier", line);

		throw std::runtime_error(buf.str());
	}
}

void
DevicesParser::expect_integer(const lib::stream &f, int &val)
{
	read_integer(f, val);

	if (c == EOF)
	{
		lib::scratch_buf buf(100);
		buf.printf("line %lu: syntax error, expected integer", line);

		throw std::runtime_error(buf.str());
	}
}

void
DevicesParser::read_ident(const lib::stream &f, std::string &ident)
{
	std::string skipped;

	/* first skip any space */
	skip_space(f, skipped);

	ident.reserve(16);

	while (c != EOF)
	{
		if (!isalnum(c) && c != '_')
			break;

		ident += c;
		get_next_char(f);
	}
}

void
DevicesParser::read_integer(const lib::stream &f, int &val)
{
	std::string skipped, str;
	val = 0;

	str.reserve(10);

	/* first skip any space */
	skip_space(f, skipped);

	while (c != EOF)
	{
		if (!isxdigit(c) && c != 'x' && c != 'X')
			break;

		str += c;
		get_next_char(f);
	}

	if (!str.empty())
		val = strtol(str.c_str(), NULL, 0);
}

void
DevicesParser::skip_space(const lib::stream &f, std::string &skipped)
{
	while (c != EOF)
	{
		if (c == '/')
		{
			skipped.push_back(c);
			get_next_char(f);

			if (c == '*')
				skip_c_comment(f, skipped);
			else if (c == '/')
				skip_line(f, skipped);
		}
		if (c == '#')
		{
			skip_line(f, skipped);
		}
		else if (check_eol(f))
		{
			skipped.push_back('\n');
			++line;
		}
		else
		{
			if (!isspace(c))
				break;

			skipped.push_back(c);
			get_next_char(f);
		}
	}
}

void
DevicesParser::skip_c_comment(const lib::stream &f, std::string &skipped)
{
	skipped.push_back(c);
	get_next_char(f);

	while (c != EOF)
	{
		if (c == '*')
		{
			skipped.push_back(c);
			get_next_char(f);

			if (c == '/')
			{
				skipped.push_back(c);
				get_next_char(f);

				break;
			}
		}
		else
		{
			if (check_eol(f))
			{
				skipped.push_back('\n');
				++line;
			}
			else
			{
				skipped.push_back(c);
				get_next_char(f);
			}
		}
	}
}

void
DevicesParser::skip_line(const lib::stream &f, std::string &skipped)
{
	while (c != EOF)
	{
		if (check_eol(f))
		{
			skipped.push_back('\n');
			++line;

			break;
		}

		skipped.push_back(c);
		get_next_char(f);
	}
}

bool
DevicesParser::check_eol(const lib::stream &f)
{
	if (c == '\r' || c == '\n')
	{
		int c1 = c;

		get_next_char(f);

		if (c1 == '\r' && c == '\n')
			get_next_char(f);

		return true;
	}

	return false;
}

} /* namespace edd */
