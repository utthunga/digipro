
/* $Id: Scanner.cxx,v 1.12 2008/05/15 11:30:50 fna Exp $ */

#ifdef _MSC_VER
# pragma warning (disable: 4102)
#endif

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "dct_scan.h"

namespace CT_NAMESPACE
{

const int dct_scan::EofToken = 0;

#ifndef DOXYGEN_IGNORE

typedef struct { char yychar; double yydouble; } yyForAlign;
#define yyMaxAlign (sizeof(yyForAlign) - sizeof(double))


#define const const int
/* C++ disallow implict int */
static const yyDNoState	= 0;
static const yyFirstCh	= (unsigned char) '\0';
static const yyLastCh	= (unsigned char) '\377';
static const yyEolCh	= (unsigned char) '\12';
static const yyEobCh	= (unsigned char) '\177';
static const yyDStateCount	= 99;
static const yyTableSize	= 1126;
static const yyEobState	= 15;
static const yyDefaultState	= 16;
static const STD	= 1;
static const COMMENT	= 3;
static const CPPCOMMENT	= 5;
static const STR	= 7;
#undef const


typedef struct { yyStateRange yyCheck, yyNext; } yyCombType;

static const yyCombType yyComb[yyTableSize + 1] = {
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   1,   19}, 
{   1,   17}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   1,   23}, {   0,    0}, {   1,   89}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {  97,   98}, {   0,    0}, {   1,   28}, 
{   0,    0}, {   0,    0}, {   1,   92}, {   1,   90}, {   1,   12}, 
{   1,   12}, {   1,   12}, {   1,   12}, {   1,   12}, {   1,   12}, 
{   1,   12}, {   1,   12}, {   1,   12}, {  12,   12}, {  12,   12}, 
{  12,   12}, {  12,   12}, {  12,   12}, {  12,   12}, {  12,   12}, 
{  12,   12}, {  12,   12}, {  12,   12}, {  92,   99}, {  95,   96}, 
{   0,    0}, {   0,    0}, {   0,    0}, {  92,   93}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   1,   29}, {   0,    0}, {   1,   27}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   20}, {   3,   94}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   24}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   95}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   97}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {  14,   15}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, {   3,   11}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   21}, 
{   5,   91}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   25}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {  43,   44}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, {   5,   10}, 
{   5,   10}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,   22}, {   7,   18}, {   7,    9}, {   7,    9}, {   7,   31}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,   26}, {   7,    9}, 
{   7,   30}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,   32}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {  44,   45}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  45,   46}, {  46,   47}, {  47,   48}, 
{  48,   49}, {  49,   50}, {  50,   51}, {  51,   52}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  32,   41}, {  52,   53}, {  53,   54}, {  54,   55}, {  13,   13}, 
{  32,   42}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  55,   56}, {  56,   57}, {  57,   58}, 
{  58,   59}, {  59,   60}, {  60,   61}, {  61,   62}, {  62,   63}, 
{  63,   64}, {  64,   65}, {  65,   66}, {  66,   67}, {  67,   68}, 
{  68,   69}, {  70,   71}, {  71,   72}, {  72,   73}, {  73,   74}, 
{  74,   75}, {  75,   76}, {  76,   77}, {  77,   78}, {  78,   79}, 
{  79,   80}, {  80,   81}, {  81,   82}, {  32,   40}, {  82,   83}, 
{  83,   84}, {  84,   85}, {  85,   86}, {  32,   38}, {  86,   87}, 
{  87,   88}, {   0,    0}, {   0,    0}, {  32,   37}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {  32,   36}, {  32,   43}, {   0,    0}, {   0,    0}, 
{  32,   35}, {   0,    0}, {  32,   34}, {   0,    0}, {  32,   33}, 
{   0,    0}, {  32,   70}, {   0,    0}, {   0,    0}, {   0,    0}, 
{  32,   39}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, 
};

static const yyCombType * const yyBasePtr[yyDStateCount + 1] = { 0,
& yyComb [   0], & yyComb [   0], & yyComb [  94], & yyComb [   0], 
& yyComb [ 350], & yyComb [   0], & yyComb [ 606], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [  10], 
& yyComb [ 814], & yyComb [  94], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [ 871], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [ 378], & yyComb [ 617], 
& yyComb [ 777], & yyComb [ 773], & yyComb [ 769], & yyComb [ 772], 
& yyComb [ 771], & yyComb [ 761], & yyComb [ 767], & yyComb [ 807], 
& yyComb [ 791], & yyComb [ 813], & yyComb [ 837], & yyComb [ 833], 
& yyComb [ 836], & yyComb [ 835], & yyComb [ 825], & yyComb [ 831], 
& yyComb [ 844], & yyComb [ 828], & yyComb [ 850], & yyComb [ 846], 
& yyComb [ 842], & yyComb [ 845], & yyComb [ 844], & yyComb [ 834], 
& yyComb [   0], & yyComb [ 847], & yyComb [ 851], & yyComb [ 833], 
& yyComb [ 859], & yyComb [ 855], & yyComb [ 851], & yyComb [ 854], 
& yyComb [ 853], & yyComb [ 843], & yyComb [ 856], & yyComb [ 860], 
& yyComb [ 842], & yyComb [ 869], & yyComb [ 865], & yyComb [ 861], 
& yyComb [ 864], & yyComb [ 864], & yyComb [ 854], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [  26], 
& yyComb [   0], & yyComb [   0], & yyComb [  22], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], 
};

static const yyStateRange yyDefault[yyDStateCount + 1] = { 0,
   13,     1,    14,     3,    14,     5,    14,     7,    14,    14, 
   14,    14,    14,     0,     0,     0,    14,     9,     0,    11, 
   10,     9,     0,    11,    10,     9,     0,     0,     0,     0, 
    9,     9,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,    14,    14,    14,    14,    14,    14,    14,    14, 
   14,    14,    14,    14,    14,    14,    14,    14,    14,    14, 
   14,    14,    14,    14,    14,    14,    14,    14,     0,    14, 
   14,    14,    14,    14,    14,    14,    14,    14,    14,    14, 
   14,    14,    14,    14,    14,    14,    14,     0,     0,    12, 
   17,    14,     0,    17,    11,     0,    11,     0,     0, 
};

static const yyStateRange yyEobTrans[yyDStateCount + 1] = { 0,
    0,     0,    11,    11,    10,    10,     9,     9,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0, 
};


#ifdef xxGetLower
static const unsigned char yyToLower[] =
{
'\0', '\1', '\2', '\3', '\4', '\5', '\6', '\7',
'\10', '\11', '\12', '\13', '\14', '\15', '\16', '\17',
'\20', '\21', '\22', '\23', '\24', '\25', '\26', '\27',
'\30', '\31', '\32', '\33', '\34', '\35', '\36', '\37',
' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
'@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']', '^', '_',
'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}','~','\177',
'\200', '\201', '\202', '\203', '\204', '\205', '\206', '\207',
'\210', '\211', '\212', '\213', '\214', '\215', '\216', '\217',
'\220', '\221', '\222', '\223', '\224', '\225', '\226', '\227',
'\230', '\231', '\232', '\233', '\234', '\235', '\236', '\237',
'\240', '\241', '\242', '\243', '\244', '\245', '\246', '\247',
'\250', '\251', '\252', '\253', '\254', '\255', '\256', '\257',
'\260', '\261', '\262', '\263', '\264', '\265', '\266', '\267',
'\270', '\271', '\272', '\273', '\274', '\275', '\276', '\277',
'\300', '\301', '\302', '\303', '\304', '\305', '\306', '\307',
'\310', '\311', '\312', '\313', '\314', '\315', '\316', '\317',
'\320', '\321', '\322', '\323', '\324', '\325', '\326', '\327',
'\330', '\331', '\332', '\333', '\334', '\335', '\336', '\337',
'\340', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
'\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
'\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
'\370', '\371', '\372', '\373', '\374', '\375', '\376', '\377',
};
# endif

# ifdef xxGetUpper
static const unsigned char yyToUpper[] =
{
'\0', '\1', '\2', '\3', '\4', '\5', '\6', '\7',
'\10', '\11', '\12', '\13', '\14', '\15', '\16', '\17',
'\20', '\21', '\22', '\23', '\24', '\25', '\26', '\27',
'\30', '\31', '\32', '\33', '\34', '\35', '\36', '\37',
' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
'`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}','~','\177',
'\200', '\201', '\202', '\203', '\204', '\205', '\206', '\207',
'\210', '\211', '\212', '\213', '\214', '\215', '\216', '\217',
'\220', '\221', '\222', '\223', '\224', '\225', '\226', '\227',
'\230', '\231', '\232', '\233', '\234', '\235', '\236', '\237',
'\240', '\241', '\242', '\243', '\244', '\245', '\246', '\247',
'\250', '\251', '\252', '\253', '\254', '\255', '\256', '\257',
'\260', '\261', '\262', '\263', '\264', '\265', '\266', '\267',
'\270', '\271', '\272', '\273', '\274', '\275', '\276', '\277',
'\300', '\301', '\302', '\303', '\304', '\305', '\306', '\307',
'\310', '\311', '\312', '\313', '\314', '\315', '\316', '\317',
'\320', '\321', '\322', '\323', '\324', '\325', '\326', '\327',
'\330', '\331', '\332', '\333', '\334', '\335', '\336', '\337',
'\340', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
'\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
'\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
'\370', '\371', '\372', '\373', '\374', '\375', '\376', '\377',
};
#endif

#endif /* DOXYGEN_IGNORE */

#ifdef xxyyPush
void dct_scan::yyPush(yyStateRange yyState)
{
	if (yyStStStackIdx == yyStStStackSize)
	{
		yyStateRange *tmp;
		unsigned long i;
		
		tmp = new yyStateRange[yyStStStackSize * 2];
		if (!tmp)
			yyErrorOutOfMemory();
		
		for (i = 0; i < yyStStStackSize; i++)
			tmp[i] = yyStStStackPtr[i];
		
		delete [] yyStStStackPtr;
		
		yyStStStackPtr = tmp;
		yyStStStackSize *= 2;
	}
	yyStStStackPtr[yyStStStackIdx++] = yyStartState;
	yyStart(yyState);
}
#endif

#ifdef xxyyPop
void dct_scan::yyPop(void)
{
	yyPreviousStart = yyStartState;
	if (yyStStStackIdx > 0)
		yyStartState = yyStStStackPtr[--yyStStStackIdx];
	else
		yyErrorStartStackUnderflow();
}
#endif

void dct_scan::yyStart(yyStateRange yyState)
{
	yyPreviousStart = yyStartState;
	yyStartState = yyState;
}

void dct_scan::yyPrevious(void)
{
	yyStateRange yys = yyStartState;
	yyStartState = yyPreviousStart;
	yyPreviousStart = yys;
}

void dct_scan::yyLess(int yyn)
{
	yyChBufferIndex -= TokenLength - yyn;
	TokenLength = yyn;
}

void dct_scan::yyTab1(int yya)
{
	yyLineStart -= (yyTabSpace - 1 - ((unsigned char *) TokenPtr - yyLineStart + yya - 1)) & (yyTabSpace - 1);
}

void dct_scan::yyEol(int Column)
{
	yyLineCount++;
	yyLineStart = yyChBufferIndex - 1 - Column;
}

void dct_scan::unput(char yyc)
{
	*(--yyChBufferIndex) = yyc;
}

#define yyPrevious	yyPrevious()
#define yyTab		yyTab1(0)
#define yyTab2(a,b)	yyTab1(a)

# line 70 "dct_scan.rex"

/* BEGIN GLOBAL */

void dct_scan::ErrorAttribute (int Token, dct_scan_tScanAttribute * pAttribute)
{
 pAttribute->Position = Attribute.Position;
 switch (Token) {
 case /* ident */ 1: 
 pAttribute->ident.ident= lib::NoScanBuf;
 ;
 break;
 case /* IntConst */ 2: 
 pAttribute->IntConst.value= 0;
 ;
 break;
 case /* StringConst */ 3: 
 pAttribute->StringConst.string= lib::NoScanBuf;
 ;
 break;
 }
}


/* END GLOBAL */

# line 498 "dct_scan.cxx"

#ifndef yyInitBufferSize
# define yyInitBufferSize	1024 * 8 + 256
#endif
#ifndef yyInitStStStackSize
# define yyInitStStStackSize	16
#endif
#ifndef yyInitFileStackSize
# define yyInitFileStackSize	8
#endif
#ifndef yyDefaultTabSpace
# define yyDefaultTabSpace	8
#endif


dct_scan::dct_scan(PARENT *p)
#ifdef IDENT_IS_AUTO_PTR
: parent(p), ident(&parent->ident()), err(&parent->err())
#else
: parent(p), ident(parent->ident), err(parent->err)
#endif
{
#ifdef FILELIMIT
	filesize = 0;
#endif
	Attribute.Position.Line = 0;
	Attribute.Position.Column = 0;
	Attribute.Position.EndColumn = 0;
	Attribute.Position.File = ULONG_MAX;
	
	yyTabSpace = yyDefaultTabSpace;
	
	yyStateStackSize = yyInitBufferSize;
	yyStateStack = new yyStateRange[yyStateStackSize];
	if (!yyStateStack)
		yyErrorOutOfMemory();
	yyStateStack[0] = yyDefaultState;
	yyStartState = STD;
	
	yyPreviousStart = STD;
	yySourceFile = NULL;
	yyEof = false;
	
	yyChBufferSize = yyInitBufferSize;
	yyChBufferPtr = new unsigned char[yyChBufferSize];
	if (!yyChBufferPtr)
		yyErrorOutOfMemory();
	yyChBufferStart = &yyChBufferPtr[yyMaxAlign];
	yyChBufferStart[-1] = yyEolCh;		/* begin of line indicator */
	yyChBufferStart[ 0] = yyEobCh;		/* end of buffer sentinel */
	yyChBufferStart[ 1] = '\0';
	yyChBufferIndex = yyChBufferStart;
	
	yyBytesRead = 0;
	yyLineCount = 1;
	yyLineStart = &yyChBufferStart[-1];
	
	yyFileStackSize	= yyInitFileStackSize;
	yyFileStack = new yytFileStack[yyFileStackSize];
	if (!yyFileStack)
		yyErrorOutOfMemory();
	yyFileStackPtr = &yyFileStack[0];
	
#if defined xxyyPush | defined xxyyPop
	yyStStStackSize = yyInitStStStackSize;
	yyStStStackPtr = new yyStateRange[yyStStStackSize];
	yyStStStackIdx = 0;
#endif

{
# line 97 "dct_scan.rex"

/* BEGIN BEGIN */
/* END BEGIN */

# line 574 "dct_scan.cxx"
}
}

dct_scan::~dct_scan(void)
{
# line 103 "dct_scan.rex"

/* BEGIN CLOSE */
/* END CLOSE */

# line 585 "dct_scan.cxx"
	
	while (yyFileStackPtr != &yyFileStack[0])
		CloseFile();
	
	if (yyStateStack)
		delete [] yyStateStack;
	
	if (yyChBufferPtr)
		delete [] yyChBufferPtr;
	
	if (yyFileStack)
		delete [] yyFileStack;
	
#if defined xxyyPush | defined xxyyPop
	if (yyStStStackPtr)
		delete [] yyStStStackPtr;
#endif
}

void
dct_scan::setfilename(const std::string &name)
{
	bool todo = true;
	
	for (size_t i = 0, max = yyfiles.size(); i < max; i++)
	{
		if (yyfiles[i] == name)
		{
			Attribute.Position.File = i;
			todo = false;
			break;
		}
	}
	
	if (todo)
	{
		yyfiles.push_back(name);
		Attribute.Position.File = yyfiles.size() - 1;
	}
}

std::string
dct_scan::getfilename(unsigned long file) const
{
	if (file < yyfiles.size())
		return yyfiles[file];
	
	return std::string();
}

lib::pos
dct_scan::mypos(const reuse::tPosition &pos) const
{
	lib::pos p;
	
	p.line = pos.Line;
	p.column = pos.Column;
	p.end_column = pos.EndColumn;
	p.end_line = pos.Line;
	p.file = ident->create(getfilename(pos.File));
	
	return p;
}

lib::pos
dct_scan::mypos(const reuse::tPosition &pos1, const reuse::tPosition &pos2) const
{
	lib::pos p;

	p.line = pos1.Line;
	p.column = pos1.Column;
	p.end_column = pos2.EndColumn;
	p.end_line = pos2.Line;
	p.file = ident->create(getfilename(pos1.File));

	return p;
}

int
dct_scan::GetToken(void)
{
	yyStateRange yyState;
	yyStateRange *yyStatePtr;
	unsigned char *yyChBufferIndexReg;
	const yyCombType * const * const yyBasePtrReg = yyBasePtr;
	
# line 109 "dct_scan.rex"

/* BEGIN LOCAL */

/* number of errors in string */
int string_errors = 0;

/* start position of the string */
struct reuse::pos string_pos = reuse::NoPosition;

/* END LOCAL */

# line 684 "dct_scan.cxx"
	
yyBegin:
	yyState = yyStartState; /* initialize */
	yyStatePtr = & yyStateStack[1];
	yyChBufferIndexReg = yyChBufferIndex;
	TokenPtr = (char *) yyChBufferIndexReg;
	
	/* ASSERT yyChBuffer[yyChBufferIndex] == first character */
	
yyContinue:
	/* continue after sentinel or skipping blanks
	 * execute as many state transitions as possible
	 * determine next state and get next character
	 */
	for (;;)
	{
		const yyCombType *yyTablePtr;
		
		yyTablePtr = (yyBasePtrReg[yyState] + *yyChBufferIndexReg++);
		if (yyTablePtr->yyCheck == yyState)
		{
			yyState = yyTablePtr->yyNext;
			/* push state */
			*yyStatePtr++ = yyState;
			goto yyContinue;
		}
		
		/* reconsider character */
		yyChBufferIndexReg--;
		
		if ((yyState = yyDefault[yyState]) == yyDNoState)
			break;
	}
	
	for (;;)
	{
		/* search for last final state */
		TokenLength = (int)(yyChBufferIndexReg - (unsigned char *) TokenPtr);
		yyChBufferIndex = yyChBufferIndexReg;
switch (* -- yyStatePtr) {
case 99:;
# line 154 "dct_scan.rex"
{ yyStart(COMMENT); 
} goto yyBegin;
# line 729 "dct_scan.cxx"
case 98:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 155 "dct_scan.rex"
{ err->warning(xxNestedComment,
					  mypos(Attribute.Position),
					  ident->NoIdent); 
} goto yyBegin;
# line 738 "dct_scan.cxx"
case 96:;
# line 158 "dct_scan.rex"
{ yyStart(STD); 
} goto yyBegin;
# line 743 "dct_scan.cxx"
case 94:;
# line 159 "dct_scan.rex"
{ yyEol(0); 
} goto yyBegin;
# line 748 "dct_scan.cxx"
case 11:;
case 20:;
case 24:;
case 95:;
case 97:;
# line 160 "dct_scan.rex"
{
} goto yyBegin;
# line 757 "dct_scan.cxx"
case 93:;
# line 165 "dct_scan.rex"
{ yyStart(CPPCOMMENT); 
} goto yyBegin;
# line 762 "dct_scan.cxx"
case 91:;
# line 166 "dct_scan.rex"
{ yyStart(STD); yyEol(0); 
} goto yyBegin;
# line 767 "dct_scan.cxx"
case 10:;
case 21:;
case 25:;
# line 167 "dct_scan.rex"
{
} goto yyBegin;
# line 774 "dct_scan.cxx"
case 90:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 173 "dct_scan.rex"
{
		Attribute.IntConst.value = 0;
		return 2;
	
} goto yyBegin;
# line 784 "dct_scan.cxx"
case 12:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 181 "dct_scan.rex"
{
		const char *s = TokenPtr;
		int slen = TokenLength;
		
		if (*TokenPtr == '0')
		{
			s++;
			slen--;
		}
		
		if (lib::dec_ascii_to_int(Attribute.IntConst.value, s, slen) == 0)
			return 2;
		
		/* overflow, error */
		err->error(xxConstantOverflow,
			   mypos(Attribute.Position),
			   ident->NoIdent);
		
		/* truncate to INT_MAX */
		Attribute.IntConst.value = INT_MAX;
		return 2;
	
} goto yyBegin;
# line 812 "dct_scan.cxx"
case 89:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 208 "dct_scan.rex"
{
		yyStart(STR);
		Attribute.StringConst.string.init();
		
		string_errors = 0;
		string_pos = Attribute.Position;
	
} goto yyBegin;
# line 825 "dct_scan.cxx"
case 88:;
# line 221 "dct_scan.rex"
{
		int c, i;
		
#define HEX_CORRECTION(c) \
	if (c > 64)			\
	{				\
		if (c > 96) c -= 87;	\
		else c -= 55;		\
	}				\
	else c -= 48
		
		c = *(TokenPtr + 2); HEX_CORRECTION(c);
		i = c << 4;
		
		c = *(TokenPtr + 3); HEX_CORRECTION(c);
		i += c;
		
		Attribute.StringConst.string.put(i);
		
#undef HEX_CORRECTION
	
} goto yyBegin;
# line 850 "dct_scan.cxx"
case 69:;
# line 248 "dct_scan.rex"
{
		char *s = TokenPtr + 1;
		char *end = TokenPtr + TokenLength;
		int i;
		
		i = 0;
		while (s < end)
		{
			i <<= 3; /* i *= 8; */
			i += *s++ - 48;
		}
		
		Attribute.StringConst.string.put(i);
	
} goto yyBegin;
# line 868 "dct_scan.cxx"
case 42:;
# line 264 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\'');
	
} goto yyBegin;
# line 875 "dct_scan.cxx"
case 41:;
# line 269 "dct_scan.rex"
{
		Attribute.StringConst.string.put('"');
	
} goto yyBegin;
# line 882 "dct_scan.cxx"
case 40:;
# line 274 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\\');
	
} goto yyBegin;
# line 889 "dct_scan.cxx"
case 39:;
# line 279 "dct_scan.rex"
{
		Attribute.StringConst.string.put('|');
	
} goto yyBegin;
# line 896 "dct_scan.cxx"
case 38:;
# line 284 "dct_scan.rex"
{
		/* ignore */
	
} goto yyBegin;
# line 903 "dct_scan.cxx"
case 37:;
# line 289 "dct_scan.rex"
{
		/* ignore */
	
} goto yyBegin;
# line 910 "dct_scan.cxx"
case 36:;
# line 294 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\n');
	
} goto yyBegin;
# line 917 "dct_scan.cxx"
case 35:;
# line 299 "dct_scan.rex"
{
		/* ignore */
	
} goto yyBegin;
# line 924 "dct_scan.cxx"
case 34:;
# line 304 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\t');
	
} goto yyBegin;
# line 931 "dct_scan.cxx"
case 33:;
# line 309 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\n');
		Attribute.StringConst.string.put('\n');
		Attribute.StringConst.string.put('\n');
	
} goto yyBegin;
# line 940 "dct_scan.cxx"
case 18:;
# line 316 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\n');
		yyEol(0);
	
} goto yyBegin;
# line 948 "dct_scan.cxx"
case 31:;
# line 322 "dct_scan.rex"
{
		/* ignore */
	
} goto yyBegin;
# line 955 "dct_scan.cxx"
case 22:;
# line 327 "dct_scan.rex"
{
		Attribute.StringConst.string.put('\t');
	
} goto yyBegin;
# line 962 "dct_scan.cxx"
case 9:;
case 26:;
case 32:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 332 "dct_scan.rex"
{
		int c = *TokenPtr & 0xff;
		
		if (!lib::is_iso_latin_1(c))
		{
			if (string_errors++ < 3)
			{
				char buf[8];
				
				snprintf(buf, sizeof(buf), "%u", c);
				
				err->warning(xxNoISOLatin1Char,
					     mypos(Attribute.Position),
					     ident->create(buf));
			}
			
			c = '?';
		}
		
		Attribute.StringConst.string.put(c);
	
} goto yyBegin;
# line 991 "dct_scan.cxx"
case 30:;
# line 355 "dct_scan.rex"
{
		yyStart(STD);
		
		Attribute.Position = string_pos;
		Attribute.StringConst.string.finish();
		
		return 3;
	
} goto yyBegin;
# line 1003 "dct_scan.cxx"
case 29:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 367 "dct_scan.rex"
{ return 4; 
} goto yyBegin;
# line 1010 "dct_scan.cxx"
case 28:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 368 "dct_scan.rex"
{ return 5; 
} goto yyBegin;
# line 1017 "dct_scan.cxx"
case 27:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 369 "dct_scan.rex"
{ return 6; 
} goto yyBegin;
# line 1024 "dct_scan.cxx"
case 13:;
Attribute.Position.Line   = yyLineCount;
Attribute.Position.Column = (int) ((unsigned char *) TokenPtr - yyLineStart);Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength - 1;
# line 376 "dct_scan.rex"
{
		if (TokenLength > 255)
		{
			err->warning(xxIdentifierToLong,
				     mypos(Attribute.Position),
				     ident->NoIdent);
		}
		
		Attribute.ident.ident.init(TokenLength);
		Attribute.ident.ident.put(TokenPtr, TokenLength);
		Attribute.ident.ident.finish();
		
		return 1;
	
} goto yyBegin;
# line 1044 "dct_scan.cxx"
case 23:;
{/* BlankAction */
while (* yyChBufferIndexReg ++ == ' ') ;
TokenPtr = (char *) -- yyChBufferIndexReg;
yyState = yyStartState;
yyStatePtr = & yyStateStack [1];
goto yyContinue;
} goto yyBegin;
case 19:;
{/* TabAction */
yyTab;
} goto yyBegin;
case 17:;
{/* EolAction */
yyEol (0);
} goto yyBegin;
case 1:;
case 2:;
case 3:;
case 4:;
case 5:;
case 6:;
case 7:;
case 8:;
case 14:;
case 43:;
case 44:;
case 45:;
case 46:;
case 47:;
case 48:;
case 49:;
case 50:;
case 51:;
case 52:;
case 53:;
case 54:;
case 55:;
case 56:;
case 57:;
case 58:;
case 59:;
case 60:;
case 61:;
case 62:;
case 63:;
case 64:;
case 65:;
case 66:;
case 67:;
case 68:;
case 70:;
case 71:;
case 72:;
case 73:;
case 74:;
case 75:;
case 76:;
case 77:;
case 78:;
case 79:;
case 80:;
case 81:;
case 82:;
case 83:;
case 84:;
case 85:;
case 86:;
case 87:;
case 92:;
		/* non final states */
		yyChBufferIndexReg--;			/* return character */
		break;
 
case 16:
			Attribute.Position.Line = yyLineCount;
			Attribute.Position.Column = (int)(yyChBufferIndexReg - yyLineStart);
			Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength;
			/* TokenLength = 1; */
			yyChBufferIndex = ++yyChBufferIndexReg;
	{
# line 122 "dct_scan.rex"

/* BEGIN DEFAULT */

	if (*TokenPtr != '\r')
		err->error(xxIllegalChar,
			   mypos(Attribute.Position),
			   ident->create_from_buf(TokenPtr, TokenLength));

/* END DEFAULT */

# line 1137 "dct_scan.cxx"
	}
	goto yyBegin;
	
		case yyDNoState:
			goto yyBegin;

case 15:
			yyChBufferIndex = --yyChBufferIndexReg; /* undo last state transit */
			if (--TokenLength == 0)
			{
				/* get previous state */
				yyState = yyStartState;
					yyState++;
			}
			else
			{
				yyState = *(yyStatePtr - 1);
			}
			
			if (yyChBufferIndex != &yyChBufferStart[yyBytesRead])
			{
				/* end of buffer sentinel in buffer */
				if ((yyState = yyEobTrans [yyState]) == yyDNoState)
					continue;
				yyChBufferIndexReg++;
				/* push state */
				*yyStatePtr++ = yyState;
				goto yyContinue;
			}

			/* end of buffer reached */
			if (!yyEof)
			{
				char *yySource;
				char *yyTarget;
				unsigned long yyChBufferFree;
				
				yySource = TokenPtr - 1;
				yyTarget = (char *) &yyChBufferPtr[(yyMaxAlign - 1 - TokenLength) & (yyMaxAlign - 1)];
				yyChBufferFree = reuse::Exp2(reuse::Log2(yyChBufferSize - 4 - yyMaxAlign - TokenLength));
				
				/* copy initial part of token in front of the input buffer */
				if (yySource > yyTarget)
				{
					TokenPtr = yyTarget + 1;
					do *yyTarget++ = *yySource++;
					while (yySource < (char *) yyChBufferIndexReg)
						;
					yyLineStart += (unsigned char *) yyTarget - yyChBufferStart - yyBytesRead;
					yyChBufferStart = (unsigned char *) yyTarget;
				}
				else
				{
					yyChBufferStart = yyChBufferIndexReg;
				}
				
				/* extend buffer if necessary */
				if (yyChBufferFree < yyChBufferSize >> 3 /* / 8 */ )
				{
					unsigned long yyDelta;
					unsigned char *yyOldChBufferPtr = yyChBufferPtr;
					
					/* resize yyChBuffer array */
					{
						unsigned char *tmp;
						
						tmp = new unsigned char[yyChBufferSize * 2];
						if (!tmp)
							yyErrorOutOfMemory();
						
						memcpy(tmp, yyChBufferPtr, yyChBufferSize);
						delete [] yyChBufferPtr;
						
						yyChBufferPtr = tmp;
						yyChBufferSize *= 2;
					}
					
					yyDelta = yyChBufferPtr - yyOldChBufferPtr;
					yyChBufferStart	+= yyDelta;
					yyLineStart	+= yyDelta;
					TokenPtr	+= yyDelta;
					yyChBufferFree = reuse::Exp2(reuse::Log2(yyChBufferSize - 4 - yyMaxAlign - TokenLength));
					if (yyStateStackSize < yyChBufferSize)
					{
						yyStateRange *yyOldStateStack = yyStateStack;
						yyStateRange *tmp;
						unsigned long i;
						
						tmp = new yyStateRange[yyStateStackSize * 2];
						if (!tmp)
							yyErrorOutOfMemory();
						
						for (i = 0; i < yyStateStackSize; i++)
							tmp[i] = yyStateStack[i];
						
						delete [] yyStateStack;
						
						yyStateStack = tmp;
						yyStateStackSize *= 2;
						
						yyStatePtr += yyStateStack - yyOldStateStack;
					}
				}
				
				/* read buffer and restart */
				yyChBufferIndex = yyChBufferIndexReg = yyChBufferStart;
				yyBytesRead = GetLine(yySourceFile, (char *) yyChBufferIndex, (int) yyChBufferFree);
				if (yyBytesRead <= 0)
				{
					yyBytesRead = 0;
					yyEof = true;
				}
				yyChBufferStart[yyBytesRead    ] = yyEobCh;
				yyChBufferStart[yyBytesRead + 1] = '\0';
				
				goto yyContinue;
			}
			
			if (TokenLength == 0)
			{
				/* end of file reached */
				Attribute.Position.Line   = yyLineCount;
				Attribute.Position.Column = (int)((unsigned char *) TokenPtr - yyLineStart);
				Attribute.Position.EndColumn = Attribute.Position.Column + TokenLength;
				
				CloseFile();
				
				if (yyFileStackPtr == &yyFileStack[0])
				{
# line 134 "dct_scan.rex"

/* BEGIN EOF */
/* END EOF */

# line 1272 "dct_scan.cxx"
					return EofToken;
				}
				
				goto yyBegin;
			}
			break;
		
		default:
			yyErrorInternal();
		}
	}
}
 
void dct_scan::yyInitialize(void)
{
	/* increase FileStack array */
	if (yyFileStackPtr >= yyFileStack + yyFileStackSize - 1)
	{
		unsigned long yyyFileStackPtr = yyFileStackPtr - yyFileStack;
		yytFileStack *tmp;
		unsigned long i;
		
		tmp = new yytFileStack[yyFileStackSize * 2];
		if (!tmp)
			yyErrorOutOfMemory();
		
		for (i = 0; i < yyFileStackSize; i++)
			tmp[i] = yyFileStack[i];
		
		delete [] yyFileStack;
		
		yyFileStack = tmp;
		yyFileStackSize *= 2;
		
		yyFileStackPtr = yyFileStack + yyyFileStackPtr;
	}
	
	/* push file */
	yyFileStackPtr++;
	yyFileStackPtr->yySourceFile	= yySourceFile;
	yyFileStackPtr->yyEof		= yyEof;
	yyFileStackPtr->yyChBufferPtr	= yyChBufferPtr;
	yyFileStackPtr->yyChBufferStart	= yyChBufferStart;
	yyFileStackPtr->yyChBufferSize	= yyChBufferSize;
	yyFileStackPtr->yyChBufferIndex	= yyChBufferIndex;
	yyFileStackPtr->yyBytesRead	= yyBytesRead;
	yyFileStackPtr->yyLineCount	= yyLineCount;
	yyFileStackPtr->yyLineStart	= yyLineStart;
	
	/* initialize file state */
	yyChBufferSize = yyInitBufferSize;
	yyChBufferPtr = new unsigned char[yyChBufferSize];
	if (!yyChBufferPtr)
		yyErrorOutOfMemory();
	
	yyChBufferStart = &yyChBufferPtr[yyMaxAlign];
	yyChBufferStart[-1] = yyEolCh;		/* begin of line indicator */
	yyChBufferStart[ 0] = yyEobCh;		/* end of buffer sentinel */
	yyChBufferStart[ 1] = '\0';
	yyChBufferIndex	    = yyChBufferStart;
	yyEof		    = false;
	yyBytesRead	    = 0;
	yyLineCount	    = 1;
	yyLineStart	    = &yyChBufferStart[-1];
}

void dct_scan::BeginFile(std::auto_ptr<lib::stream> stream, const std::string &yyFileName)
{
	yyInitialize();
	
	yySourceFile = stream.release();
	assert(yySourceFile);
	
	setfilename(yyFileName);
}

void dct_scan::CloseFile(void)
{
	if (yyFileStackPtr == &yyFileStack[0])
		yyErrorFileStackUnderflow();
	
	if (yySourceFile)
		delete yySourceFile;
	
	delete [] yyChBufferPtr;
	yyChBufferPtr = NULL;
	
	/* pop file */
	yySourceFile	= yyFileStackPtr->yySourceFile;
	yyEof		= yyFileStackPtr->yyEof;
	yyChBufferPtr	= yyFileStackPtr->yyChBufferPtr;
	yyChBufferStart	= yyFileStackPtr->yyChBufferStart;
	yyChBufferSize	= yyFileStackPtr->yyChBufferSize;
	yyChBufferIndex	= yyFileStackPtr->yyChBufferIndex;
	yyBytesRead	= yyFileStackPtr->yyBytesRead;
	yyLineCount	= yyFileStackPtr->yyLineCount;
	yyLineStart	= yyFileStackPtr->yyLineStart;
	yyFileStackPtr--;		
}
 
#ifdef xxGetWord
int dct_scan::GetWord(char *yyWord)
{
	char *yySource = TokenPtr;
	char *yyTarget = yyWord;
	char *yyChBufferIndexReg = yyChBufferIndex;
	
	do {
		/* ASSERT word is not empty */
		*yyTarget++ = *yySource++;
	}
	while (yySource < yyChBufferIndexReg);
	
	*yyTarget = '\0';
	
	return (int)(yyChBufferIndexReg - TokenPtr);
}
#endif
 
#ifdef xxGetLower
int dct_scan::GetLower(char *yyWord)
{
	unsigned char *yySource = TokenPtr;
	unsigned char *yyTarget = yyWord;
	unsigned char *yyChBufferIndexReg = yyChBufferIndex;
	
	do {
		/* ASSERT word is not empty */
		*yyTarget++ = yyToLower[*yySource++];
	}
	while (yySource < yyChBufferIndexReg);
	
	*yyTarget = '\0';
	
	return (int)(yyChBufferIndexReg - TokenPtr);
}
#endif
 
#ifdef xxGetUpper
int dct_scan::GetUpper(char *yyWord)
{
	unsigned char *yySource = TokenPtr;
	unsigned char *yyTarget = yyWord;
	unsigned char *yyChBufferIndexReg = yyChBufferIndex;
	
	do {
		/* ASSERT word is not empty */
		*yyTarget++ = yyToUpper[*yySource++];
	}
	while (yySource < yyChBufferIndexReg);
	
	*yyTarget = '\0';
	
	return (int)(yyChBufferIndexReg - TokenPtr);
}
#endif
 
#ifdef xxinput
char dct_scan::input(void)
{
	if (yyChBufferIndex == &yyChBufferStart[yyBytesRead])
	{
		if (!yyEof)
		{
			yyLineStart -= yyBytesRead;
			yyChBufferIndex = yyChBufferStart = yyChBufferPtr;
 			yyBytesRead = GetLine(yySourceFile, yyChBufferIndex,
					      (int) reuse::Exp2(reuse::Log2(yyChBufferSize)));
			if (yyBytesRead <= 0)
			{
				yyBytesRead = 0;
				yyEof = true;
			}
			yyChBufferStart[yyBytesRead    ] = yyEobCh;
			yyChBufferStart[yyBytesRead + 1] = '\0';
		}
	}
	
	if (yyChBufferIndex == &yyChBufferStart[yyBytesRead])
		return '\0';
	else
		return *yyChBufferIndex++;
}
#endif

void dct_scan::yyErrorInternal(void)
{
	// internal error
	throw RuntimeError(xxInternal,
			   mypos(Attribute.Position),
		   ident->create("dct_scan"));
}

void dct_scan::yyErrorOutOfMemory(void)
{
	// out of memory
	throw RuntimeError(xxOutOfMemory,
			   mypos(Attribute.Position),
		   ident->create("dct_scan"));
}

void dct_scan::yyErrorFileStackUnderflow(void)
{
	// file stack underflow (too many calls of CloseFile)
	throw RuntimeError(xxStackUnderflow,
			   mypos(Attribute.Position),
		   ident->create("dct_scan, file stack"));
}

void dct_scan::yyErrorStartStackUnderflow(void)
{
	// start stack underflow (too many calls of yyPop)
	throw RuntimeError(xxStackUnderflow,
			   mypos(Attribute.Position),
		   ident->create("dct_scan, start stack"));
}

int dct_scan::GetLine(lib::stream *stream, char *buf, int bufsize)
{
	int n = 0;
	
	n = stream->read(buf, 1, bufsize);
	
#ifdef FILELIMIT
	if (n > 0)
	{
		filesize += n;
		if (filesize > FILELIMIT)
			throw RuntimeError(xxDemoversion);
	}
#endif
	
	return n;
}

} /* namespace CT_NAMESPACE */
