
/* 
 * $Id: device_io.cxx,v 1.75 2010/01/14 19:00:33 mmeier Exp $
 * 
 * Copyright (C) 2003-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// other libs

// my lib
#include "auto_array.h"
#include "pca_conv.h"

// own header
#include "edd_ast.h"
#include "edd_values.h"
#include "pbInterpreter.h"

#ifndef _
# define _(x) x
#endif

#ifndef N_
# define N_(x) x
#endif


namespace edd
{

device_io::device_io(class Interpreter &p)
: parent(p)
, mystate(disconnected)
{
}

bool
device_io::is_online(void) const throw()
{
	return (mystate == connected);
}

int
device_io::read(class method_env *env, class EDD_OBJECT_tree *object)
{
	assert(object && object->is_io_object());

	int warnings = 0;
	int rc = -1;

	trace(_("Read of %s %s started ..."),
		object->descr(), object->name());

	if (is_online())
		rc = read_write_object(env, object, EDD_COMMAND_OPERATION_READ, warnings);
	else
		error(_("Not online!"));

	trace(_("Read of %s %s finished!"),
		object->descr(), object->name());

	return rc;
}

int
device_io::write(class method_env *env, class EDD_OBJECT_tree *object)
{
	assert(object && object->is_io_object());

	int warnings = 0;
	int rc = -1;

	trace(_("Write of %s %s started ..."),
		object->descr(), object->name());

	if (is_online())
		rc = read_write_object(env, object, EDD_COMMAND_OPERATION_WRITE, warnings);
	else
		error(_("Not online!"));

	trace(_("Write of %s %s finished!"),
		object->descr(), object->name());

	return rc;
}

const char *
device_io::get_state_descr(void) const throw()
{
	static const char * const descr[] =
	{
		N_("disconnected"),
		N_("connected"),
		N_("disconnecting"),
		N_("connecting"),
		N_("connect error")
	};

	return descr[mystate];
}

void
device_io::state_change(enum state newstate)
{
	if (newstate != mystate)
	{
		mystate = newstate;
		on_state_change();
	}
}

void device_io::on_state_change(void) { }

void
device_io::on_runtime_error(const class RuntimeError &xx)
{
	error(parent.err().errmsg(parent.get_language(), xx).c_str());
}

void device_io::on_progress_start(const std::string &, bool) { }
void device_io::on_progress_end(void) { }
bool device_io::on_progress_activity(void) { return false; }
bool device_io::on_progress_update(double) { return false; }

int
device_io::read_write_object(class method_env *env, class EDD_OBJECT_tree *object,
			     unsigned long op, int &warnings)
{
	assert(object && object->is_io_object());
	assert(op == EDD_COMMAND_OPERATION_READ || op == EDD_COMMAND_OPERATION_WRITE);

	Interpreter::command_t result;
	int rc = -1;

	result.command = NULL;
	result.transaction = 0;

	result = parent.search_command(object, op, env);
	if (result.command)
	{
		info(_("Using COMMAND %s for %s operation of %s %s"),
			result.command->name(),
			(op == EDD_COMMAND_OPERATION_READ) ? _("read") : _("write"),
			object->descr(),
			object->name());

		rc = do_command(env, result.command, result.transaction,
				op == EDD_COMMAND_OPERATION_READ ? action_read : action_write,
				warnings);
	}
	else
		error(_("No COMMAND found for %s, ignoring it"), object->name());

	return rc;
}

int
device_io::updownload_old(bool flag, class method_env *env)
{
	const char *menu_name = flag ? "download_variables" : "upload_variables";
	class EDD_OBJECT_tree *node = parent.node_lookup(menu_name);

	if (node && node->kind == EDD_MENU)
		return updownload_pdm(flag, env);

	return updownload_all(flag, env);
}

int
device_io::updownload_all(bool flag, class method_env *env)
{
	// flag == true  -> upload   ==> read from device
	// flag == false -> download ==> write to device

	std::auto_ptr<class method_env> own_env;

	const int operation = flag ? EDD_COMMAND_OPERATION_READ : EDD_COMMAND_OPERATION_WRITE;
	const char *msg = flag ? _("Read from device") : _("Write to device");
	bool cancel = false;
	int warnings = 0;
	int errors = 0;

	on_progress_start(msg, false);
	info(_("%s started ..."), msg);

	if (is_online())
	{
		std::vector<class EDD_OBJECT_tree *> allvars(parent.allobjects(EDD_VARIABLE));

		/* if we don't have an environment create one
		 */
		if (!env)
		{
			own_env.reset(new method_env(parent, true));

			env = own_env.get();
			env->set_op_cache_state(method_env::save);
		}

		set_hart_masks(env);
		env->set_do_relations(false);
		begin_transfer_sequence(env);

		for (size_t i = 0, i_max = allvars.size(); i < i_max; ++i)
		{
			class VARIABLE_tree *vt = (class VARIABLE_tree *) allvars[i];

			if (check_variable(vt, flag, env))
			{
				if (parent.search_command(vt, operation, env).command)
					errors += updownload_object(env, vt->value(env), cancel, flag, true, warnings);

				if (cancel)
					break;

				cancel = on_progress_update((double)(i)/(double)(i_max));
			}
		}

		end_transfer_sequence(env);
		env->set_do_relations(true);
	}
	else
	{
		error(_("Not online!"));
		++errors;
	}

	updownload_finalize(env, msg, errors, warnings, cancel);

	on_progress_end();

	return errors;
}

int
device_io::updownload_pdm(bool flag, class method_env *env)
{
	// flag == true  -> upload   ==> read from device
	// flag == false -> download ==> write to device

	std::auto_ptr<class method_env> own_env;

	const char *menu_name = flag ? "download_variables" : "upload_variables";
	const char *msg = flag ? _("Read from device") : _("Write to device");
	bool cancel = false;
	int warnings = 0;
	int errors = 0;

	on_progress_start(msg, false);
	info(_("%s started ..."), msg);

	if (is_online())
	{
		/* lookup the menu
		 */
		class EDD_OBJECT_tree *node = parent.node_lookup(menu_name);

		if (node)
		{
			if (node->kind == EDD_MENU)
			{
				class MENU_tree *menu = (class MENU_tree *)node;

				info(_("Using %s at %s: %lu"),
				     menu->name(),
				     menu->pos.file.c_str(),
				     menu->pos.line);

				/* if we don't have an environment create one
				 */
				if (!env)
				{
					own_env.reset(new method_env(parent, true));

					env = own_env.get();
					env->set_op_cache_state(method_env::save);
				}

				set_hart_masks(env);
				env->set_do_relations(false);
				begin_transfer_sequence(env);

				errors = updownload_menu(env, menu, cancel, flag, true, warnings);

				end_transfer_sequence(env);
				env->set_do_relations(true);
			}
			else
			{
				error(_("Object %s isn't a MENU, abort!"), menu_name);
				++errors;
			}
		}
		else
		{
			error(_("MENU %s don't exist, abort!"), menu_name);
			++errors;
		}
	}
	else
	{
		error(_("Not online!"));
		++errors;
	}

	updownload_finalize(env, msg, errors, warnings, cancel);

	on_progress_end();

	return errors;
}

int
device_io::updownload_iec(bool flag, class method_env *env)
{
	// flag == true  -> upload   ==> read from device
	// flag == false -> download ==> write to device

	std::auto_ptr<class method_env> own_env;

	const char *menu_name = flag ? "upload_from_device_root_menu" : "download_to_device_root_menu";
	const char *msg = flag ? _("Read from device") : _("Write to device");
	bool cancel = false;
	int warnings = 0;
	int errors = 0;

	on_progress_start(msg, true);
	info(_("%s started ..."), msg);

	if (is_online())
	{
		/* lookup the menu
		 */
		class EDD_OBJECT_tree *node = parent.node_lookup(menu_name);

		if (node)
		{
			if (node->kind == EDD_MENU)
			{
				class MENU_tree *menu = (class MENU_tree *)node;

				info(_("Using %s at %s: %lu"),
				     menu->name(),
				     menu->pos.file.c_str(),
				     menu->pos.line);

				/* if we don't have an environment create one
				 */
				if (!env)
				{
					own_env.reset(new method_env(parent, true));

					env = own_env.get();
					env->set_op_cache_state(method_env::save);
				}

				set_hart_masks(env);
				env->set_do_relations(false);
				begin_transfer_sequence(env);

				errors = updownload_menu(env, menu, cancel, flag, false, warnings);

				end_transfer_sequence(env);
				env->set_do_relations(true);
			}
			else
			{
				error(_("Object %s isn't a MENU, abort!"), menu_name);
				++errors;
			}
		}
		else
		{
			error(_("MENU %s don't exist, abort!"), menu_name);
			++errors;
		}
	}
	else
	{
		error(_("Not online!"));
		++errors;
	}

	updownload_finalize(env, msg, errors, warnings, cancel);

	on_progress_end();

	return errors;
}

void
device_io::updownload_finalize(class method_env *env, const char *msg,
			       int errors, int warnings, bool cancel)
{
	if (cancel)
	{
		info(_("%s cancelled by user"), msg);
		env->set_op_cache_state(method_env::discard);
	}

	if (errors)
	{
		if (warnings)
			info(_("%s finished [with %i error%s, %i warnings]!"),
				msg, errors, (errors > 1) ? "s" : "", warnings);
		else
			info(_("%s finished [with %i error%s]!"),
				msg, errors, (errors > 1) ? "s" : "");
	}
	else
	{
		if (warnings)
			info(_("%s finished [without any error, %i warnings]!"), msg, warnings);
		else
			info(_("%s finished [without any error]!"), msg);
	}
}

int
device_io::updownload_menu(class method_env *env, class MENU_tree *menu,
			   bool &cancel, bool flag, bool simple, int &warnings)
{
	assert(env && menu);

	/* run pre read/write actions */
	{
		bool ok = true;

		try
		{
			if (flag)
				ok = menu->run_actions(EDD_OBJECT_tree::PRE_READ_ACTIONS, env);
			else
				ok = menu->run_actions(EDD_OBJECT_tree::PRE_WRITE_ACTIONS, env);
		}
		catch (const RuntimeError &xx)
		{
			on_runtime_error(xx);
			ok = false;
		}

		if (!ok)
		{
			error(_("PRE_%s_ACTIONS of %s failed!"),
			      flag ? "READ" : "WRITE",
			      menu->name());

			return 1;
		}
	}

	Interpreter::menuitems_t items;

	try
	{
		items = parent.evalmenu(menu, env);
	}
	catch (const RuntimeError &xx)
	{
		on_runtime_error(xx);

		error(_("ITEM evaluation of %s failed!"),
		      menu->name());

		return 1;
	}

	int errors = 0;

	for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
	{
		operand_ptr op(items[i].item->value(env));
		class tree *t = items[i].ref;

		if (simple && t->kind != EDD_VARIABLE)
			info(_("ignoring non-VARIABLE %s"), t->name());
		else
			errors += updownload_object(env, op, cancel, flag, simple, warnings);

		if (cancel)
			break;

		if (simple)
			cancel = on_progress_update((double)(i)/(double)(i_max));
		else
			cancel = on_progress_activity();
	}

	/* run post read/write actions */
	{
		bool ok = true;

		try
		{
			if (flag)
				ok = menu->run_actions(EDD_OBJECT_tree::POST_READ_ACTIONS, env);
			else
				ok = menu->run_actions(EDD_OBJECT_tree::POST_WRITE_ACTIONS, env);
		}
		catch (const RuntimeError &xx)
		{
			on_runtime_error(xx);
			ok = false;
		}

		if (!ok)
		{
			error(_("POST_%s_ACTIONS of %s failed!"),
			      flag ? "READ" : "WRITE",
			      menu->name());

			++errors;
		}
	}

	return errors;
}

int
device_io::updownload_object(class method_env *env, operand_ptr op,
			     bool &cancel, bool flag, bool no_validity, int &warnings)
{
	const int operation = flag ? EDD_COMMAND_OPERATION_READ : EDD_COMMAND_OPERATION_WRITE;
	class tree *t = *op;
	int errors = 0;

	switch (t->kind)
	{
		default:
		{
			info(_("ignoring Object %s"), t->name());
			break;
		}

		/* recursion */
		case EDD_MENU:
		{
			class MENU_tree *menu = (class MENU_tree *) t;

			if (no_validity || menu->is_valid(env))
				/* recurse down */
				errors += updownload_menu(env, menu, cancel, flag, false, warnings);

			break;
		}

		/* terminating value object */
		case EDD_VARIABLE:
		{
			class VARIABLE_tree *vt = (class VARIABLE_tree *) t;

			if (check_variable(vt, flag, env) && (no_validity || vt->is_valid(env)))
			{
				try
				{
					if (read_write_object(env, vt, operation, warnings) != 0)
						++errors;

					env->clear_abort();
					env->set_retries(3);
				}
				catch (const RuntimeError &xx)
				{
					on_runtime_error(xx);
					++errors;
				}
			}
			else
				info(_("ignoring incorrect VARIABLE %s"), vt->name());

			break;
		}

		/* arrays of value objects */
		case EDD_LIST:
		case EDD_VALUE_ARRAY:
		{
			class EDD_OBJECT_tree *array = (class EDD_OBJECT_tree *) t;

			if (no_validity || array->is_valid(env))
			{
				try
				{
					if (read_write_object(env, array, operation, warnings) != 0)
						++errors;

					env->clear_abort();
					env->set_retries(3);
				}
				catch (const RuntimeError &xx)
				{
					on_runtime_error(xx);
					++errors;
				}
			}

			break;
		}
		case EDD_REFERENCE_ARRAY:
		{
			class REFERENCE_ARRAY_tree *reference_array = (class REFERENCE_ARRAY_tree *) t;

			if (no_validity || reference_array->is_valid(env))
			{
				std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> elements;
				elements = reference_array->elements(env);

				for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
				{
					long index = elements[i]->index();

					errors += updownload_object(env, (*op)[index],
								    cancel, flag, no_validity, warnings);

					if (cancel)
						break;

					cancel = on_progress_activity();
				}
			}

			break;
		}

		/* structs of value objects */
		case EDD_COLLECTION:
		case EDD_RECORD:
		{
			class EDD_OBJECT_tree *edd_object = (class EDD_OBJECT_tree *) t;

			if (no_validity || edd_object->is_valid(env))
			{
				std::vector<class MEMBER_tree *> members;

				if (t->kind == EDD_COLLECTION)
					members = ((class COLLECTION_tree *)t)->members(env);
				else
					members = ((class RECORD_tree *)t)->members(env);

				for (size_t i = 0, i_max = members.size(); i < i_max; ++i)
				{
					lib::IDENTIFIER ident(members[i]->ident());

					errors += updownload_object(env, op->member(ident),
								    cancel, flag, no_validity, warnings);

					if (cancel)
						break;

					cancel = on_progress_activity();
				}
			}

			break;
		}
	}

	return errors;
}

void
device_io::set_hart_masks(class method_env *env)
{
	assert(env);

	env->set_retries(3);

	/* ignore any comm status */
	env->hart_masks.comm_status_abort = env->hart_masks.NO_BITS;
	env->hart_masks.comm_status_retry = env->hart_masks.NO_BITS;

	/* ignore any device status */
	env->hart_masks.device_status_abort = env->hart_masks.NO_BITS;
	env->hart_masks.device_status_retry = env->hart_masks.NO_BITS;

	/* ignore any response code ... */
	env->hart_masks.response_code_abort = env->hart_masks.NO_BITS;
	env->hart_masks.response_code_retry = env->hart_masks.NO_BITS;

	/* ... except DEVICE_IS_BUSY ... */
	env->hart_masks.response_code_retry |= env->hart_masks.DEVICE_IS_BUSY;

	/* ... and undefined response codes */
	env->hart_masks.response_all = false;
}

void
device_io::info(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	parent.logger.log(lib::Logger::CLASS_INFO, "I/O", fmt, args);
	va_end(args);
}

void
device_io::warning(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	parent.logger.log(lib::Logger::CLASS_WARNING, "I/O", fmt, args);
	va_end(args);
}

void
device_io::error(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	parent.logger.log(lib::Logger::CLASS_ERROR, "I/O", fmt, args);
	va_end(args);
}

void
device_io::trace(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	parent.logger.log(lib::Logger::CLASS_TRACE, "I/O", fmt, args);
	va_end(args);
}

void
device_io::warning(lib::Logger &logger, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	logger.log(lib::Logger::CLASS_WARNING, "I/O", fmt, args);
	va_end(args);
}

void
device_io::error(lib::Logger &logger, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	logger.log(lib::Logger::CLASS_ERROR, "I/O", fmt, args);
	va_end(args);
}

void
device_io::runtime_error(lib::Logger &logger, const class COMMAND_tree *cmd,
			 const RuntimeError &xx, const char *msg)
{
	error(logger, _("COMMAND %s: RuntimeError while %s"),
	      cmd->name(), msg);

	std::string str(cmd->__parent->errmsg(xx));

	error(logger, _("COMMAND %s: RuntimeError is: %s"),
	      cmd->name(), str.c_str());
}

bool
device_io::check_variable(class VARIABLE_tree *vt, bool flag, class method_env *env)
{
	bool rc = false;

	if (flag)
		rc = vt->is_readable(env);
	else
		rc = vt->is_writeable(env);

	return rc;
}

bool
device_io::can_optimize(class method_env *env,
			const std::vector<class DATA_ITEM_tree *> &items)
{
	if (!env || !env->optimize())
		return false;

	for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
	{
		class DATA_ITEM_tree *item = items[i];

		if (item && !item->is_constant())
		{
			operand_ptr op(item->value(env));

			if (env->check_age(op))
				/* this one is out of date */
				return false;
		}
	}

	return true;
}

void
device_io::optimize_age_stamp_tick(class method_env *env)
{
	if (env && env->optimize())
		env->age_stamp_tick();
}

int
device_io::read_encode(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
		       lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		       size_t &datalen, size_t &dynamic_datalen, int32 &timeout, int &warnings)
{
	int rc;

	/* first execute all PRE_READ_ACTIONS */
	rc = do_pre_read_actions(cmd, trans_nr, env, logger, items);
	if (rc == 0)
		/* actions executed ok, now encode the datagram */
		rc = read_encode0(cmd, trans_nr, env, logger, items, datalen, dynamic_datalen, timeout, warnings);

	return rc;
}

int
device_io::read_decode(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
		       lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		       const unsigned char *buf, const size_t buflen)
{
	int rc;

	/* decode the datagram */
	rc = read_decode0(cmd, trans_nr, env, logger, items, buf, buflen);
	if (rc == 0)
		/* decode ok, run all POST_READ_ACTIONS */
		do_post_read_actions(cmd, trans_nr, env, logger, items);

	return rc;
}

int
device_io::write_encode(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
		        lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		        unsigned char *buf, const size_t buflen, size_t &datalen,
		        int32 &timeout)
{
	int rc;

	/* first execute all PRE_WRITE_ACTIONS */
	rc = do_pre_write_actions(cmd, trans_nr, env, logger, items);
	if (rc == 0)
		/* actions executed ok, now encode the datagram */
		rc = write_encode0(cmd, trans_nr, env, logger, items, buf, buflen, datalen, timeout);

	return rc;
}

int
device_io::write_decode(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
		        lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
			int success)
{
	/* only need to run all POST_WRITE_ACTIONS */
	do_post_write_actions(cmd, trans_nr, env, logger, items, success);
	return 0;
}

int
device_io::do_pre_read_actions(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
			       lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items)
{
	int rc = 0;

	try
	{
		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];

			if (item && !item->is_constant())
			{
				operand_ptr op(item->get_value(env));
				class EDD_OBJECT_tree *object = *op;

				if (!object->run_actions_all(EDD_OBJECT_tree::PRE_READ_ACTIONS, env, op))
				{
					error(logger, _("PRE_READ_ACTIONS of %s failed!"), object->name());
					rc = -1;

					break;
				}
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("running PRE_READ_ACTIONS"));
		rc = -1;
	}

	return rc;
}

int
device_io::do_pre_write_actions(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
				lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items)
{
	int rc = 0;

	try
	{
		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];
			assert(item);

			if (!item->is_constant())
			{
				operand_ptr op(item->get_value(env));
				class EDD_OBJECT_tree *object = *op;

				if (!object->run_actions_all(EDD_OBJECT_tree::PRE_WRITE_ACTIONS, env, op))
				{
					error(logger, _("PRE_WRITE_ACTIONS of %s failed!"), object->name());
					rc = -1;

					break;
				}
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("running PRE_WRITE_ACTIONS"));
		rc = -1;
	}

	return rc;
}

void
device_io::do_post_read_actions(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
				lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items)
{
	try
	{
		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];

			if (item && !item->is_constant())
			{
				operand_ptr op(item->get_value(env));
				class EDD_OBJECT_tree *object = *op;

				if (!object->run_actions_all(EDD_OBJECT_tree::POST_READ_ACTIONS, env, op))
					error(logger, _("POST_READ_ACTIONS of %s failed!"), object->name());
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("running POST_READ_ACTIONS"));
	}
}

bool
device_io::do_post_write_actions0(class method_env *env, lib::Logger &logger, 
				  operand_ptr op, int success, bool do_relations, bool do_actions)
{
	class EDD_OBJECT_tree *object = *op;
	bool rc = true;

	switch (object->kind)
	{
		default:
			/* not possible */ assert(0);
			break;

		case EDD_VARIABLE:
		{
			if (success == 0)
			{
				op->status() = operand::VALUE_STATUS_NONE;

				if (do_actions)
				{
					class VARIABLE_tree *v = (class VARIABLE_tree *)object;

					if (!v->run_actions(EDD_OBJECT_tree::POST_WRITE_ACTIONS, env, op))
						rc = false;

				/* don't dispatch the refresh; we follow PDM
				 * here as we work like PDM with an offline
				 * data set too.
				 *
				 *	if (do_relations)
				 *		v->run_refresh(env);
				 */

					if (env) env->update_age_stamp(op);
				}
			}
			else
				op->status() = operand::VALUE_STATUS_NOT_ACCEPTED;

			break;
		}
		case EDD_VALUE_ARRAY:
		case EDD_LIST:
		{
			long i_max;

			if (object->kind == EDD_VALUE_ARRAY)
				i_max = ((class VALUE_ARRAY_tree *)object)->length();
			else
				i_max = ((class LIST_tree *)object)->length(env);

			assert(i_max >= 0);

			for (long i = 0; i < i_max; ++i)
			{
				if (!do_post_write_actions0(env, logger,
							    (*op)[i], success, do_relations, false))
					rc = false;
			}

			break;
		}
		case EDD_REFERENCE_ARRAY:
		{
			class REFERENCE_ARRAY_tree *reference_array = (class REFERENCE_ARRAY_tree *)object;
			std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> elements;

			elements = reference_array->elements(env);

			for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
			{
				long index = elements[i]->index();

				if (!do_post_write_actions0(env, logger,
							    (*op)[index], success, do_relations, do_actions))
					rc = false;
			}

			break;
		}
		case EDD_COLLECTION:
		case EDD_RECORD:
		{
			std::vector<class MEMBER_tree *> members;

			if (object->kind == EDD_COLLECTION)
				members = ((class COLLECTION_tree *)object)->members(env);
			else
				members = ((class RECORD_tree *)object)->members(env);

			for (size_t i = 0, i_max = members.size(); i < i_max; ++i)
			{
				lib::IDENTIFIER ident(members[i]->ident());

				if (!do_post_write_actions0(env, logger,
							    op->member(ident), success, do_relations, do_actions))
					rc = false;
			}

			break;
		}
	}

	return rc;
}

void
device_io::do_post_write_actions(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
				 lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items, int success)
{
	try
	{
		bool do_relations = env ? env->do_relations() : true;

		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];
			assert(item);

			if (!item->is_constant())
			{
				operand_ptr op(item->get_value(env));
				class EDD_OBJECT_tree *object = *op;

				if (!do_post_write_actions0(env, logger, op, success, do_relations, true))
					error(logger, _("POST_WRITE_ACTIONS of %s failed!"), object->name());
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("running POST_WRITE_ACTIONS"));
	}
}

int
device_io::read_encode0_var(struct read_encode_info &info,
			    class VARIABLE_tree *v, const lib::vbitset *item_mask)
{
	size_t varlen = v->get_io_size();
	assert(varlen > 0);

	if (item_mask)
	{
		if (info.mask.get())
		{
			/* check for overlapping bits */
			if ((*info.mask & *item_mask).any())
			{
				error(info.logger, _("Item masks overlap in COMMAND '%s'!"),
				      info.cmd->name());

				return -1;
			}

			/* check that item mask is not larger than
			 * prev mask
			 */
			if (item_mask->size() > info.mask->size())
			{
				error(info.logger, _("Item masks overflow in COMMAND '%s'!"),
				      info.cmd->name());

				return -1;
			}

			*info.mask |= *item_mask;
		}
		else
		{
			/* MASK open */
			info.mask.reset(new lib::vbitset(*item_mask));
		}

		if (item_mask->test(0))
		{
			/* MASK close */

			varlen = (info.mask->size() + 7) / 8;
			info.mask.reset(NULL);
		}
		else
			varlen = 0;
	}
	else if (info.mask.get())
	{
		/* warning, unterminated MASK close */
		warning(info.logger, _("Unterminated MASK close in COMMAND '%s'!"),
			info.cmd->name());

		varlen += (info.mask->size() + 7) / 8;
		info.mask.reset(NULL);
	}

	info.datalen += varlen;

	if (v->have_read_timeout())
	{
		operand_ptr op(v->read_timeout(info.env));

		if (op)
		{	int32 rt = *op;
			info.timeout = std::max(info.timeout, rt);
		}
	}

	return 0;
}

int
device_io::read_encode0_op(struct read_encode_info &info,
			   operand_ptr op, const lib::vbitset *item_mask)
{
	class EDD_OBJECT_tree *object = *op;
	assert(object);

	int rc = 0;

	switch (object->kind)
	{
		default:
			/* not possible */ assert(0);
			break;

		case EDD_VARIABLE:
		{
			class VARIABLE_tree *v = (class VARIABLE_tree *)object;

			rc = read_encode0_var(info, v, item_mask);

			break;
		}
		case EDD_VALUE_ARRAY:
		{
			long i_max = ((class VALUE_ARRAY_tree *)object)->length();
			assert(i_max >= 0);

			for (long i = 0; i < i_max; ++i)
			{
				rc = read_encode0_op(info, (*op)[i], NULL);
				if (rc)
					break;

				if (info.dynamic_datalen)
				{
					error(info.logger,
					      _("ARRAY %s have elements with ambiguous size"
						" [while processing COMMAND '%s']!"),
					     object->name(),
					     info.cmd->name());

					rc = -1;
					break;
				}
			}

			break;
		}
		case EDD_LIST:
		{
			long fixed_length = ((class LIST_tree *)object)->fixed_length();

			if (fixed_length)
			{
				assert(fixed_length >= 0);

				for (long i = 0; i < fixed_length; ++i)
				{
					rc = read_encode0_op(info, (*op)[i], NULL);
					if (rc)
						break;

					if (info.dynamic_datalen)
					{
						error(info.logger,
						      _("LIST %s have elements with ambiguous size"
							" [while processing COMMAND '%s']!"),
						     object->name(),
						     info.cmd->name());

						rc = -1;
						break;
					}
				}
			}
			else
			{
				class EDD_OBJECT_tree *type = ((class LIST_tree *)object)->get_type();
				operand_ptr type_op(type->value_adt());

				size_t datalen = info.datalen;
				info.datalen = 0;

				rc = read_encode0_op(info, type_op, NULL);
				if (rc == 0)
				{
					if (info.dynamic_datalen)
					{
						error(info.logger,
						      _("LIST %s have elements with ambiguous size"
							" [while processing COMMAND '%s']!"),
						     object->name(),
						     info.cmd->name());

						rc = -1;
					}
					else
					{
						info.dynamic_datalen = info.datalen;
						info.datalen = datalen;
					}
				}
			}

			break;
		}
		case EDD_REFERENCE_ARRAY:
		{
			class REFERENCE_ARRAY_tree *reference_array = (class REFERENCE_ARRAY_tree *)object;
			std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> elements;

			elements = reference_array->elements(info.env);

			for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
			{
				long index = elements[i]->index();

				rc = read_encode0_op(info, (*op)[index], NULL);
				if (rc)
					break;
				
				if (info.dynamic_datalen)
				{
					error(info.logger,
					      _("ITEM_ARRAY %s have elements with ambiguous size"
						" [while processing COMMAND '%s']!"),
					     object->name(),
					     info.cmd->name());

					rc = -1;
					break;
				}
			}

			break;
		}
		case EDD_COLLECTION:
		case EDD_RECORD:
		{
			std::vector<class MEMBER_tree *> members;

			if (object->kind == EDD_COLLECTION)
				members = ((class COLLECTION_tree *)object)->members(info.env);
			else
				members = ((class RECORD_tree *)object)->members(info.env);

			for (size_t i = 0, i_max = members.size(); i < i_max; ++i)
			{
				lib::IDENTIFIER ident(members[i]->ident());

				rc = read_encode0_op(info, op->member(ident), NULL);
				if (rc)
					break;

				if (info.dynamic_datalen)
				{
					error(info.logger,
					      _("%s %s have members with ambiguous size"
						" [while processing COMMAND '%s']!"),
					     (object->kind == EDD_COLLECTION) ? "COLLECTION" : "RECORD",
					     object->name(),
					     info.cmd->name());

					rc = -1;
					break;
				}
			}

			break;
		}
	}

	return rc;
}

device_io::read_encode_info::read_encode_info(class method_env *e, lib::Logger &l,
					      const class COMMAND_tree *c, int tnr,
					      size_t &dlen, size_t &ddlen, int32 &t)
: env(e), logger(l)
, cmd(c), trans_nr(tnr)
, datalen(dlen)
, dynamic_datalen(ddlen)
, timeout(t)
{
	datalen = 0;
	dynamic_datalen = 0;
	timeout = 0;
}

int
device_io::read_encode0(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
			lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		        size_t &datalen, size_t &dynamic_datalen, int32 &timeout, int &warnings)
{
	int rc = 0;

	try
	{
		struct read_encode_info info(env, logger, cmd, trans_nr, datalen, dynamic_datalen, timeout);

		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];

			if (info.dynamic_datalen)
			{
				error(info.logger,
				      _("A dynamic element must be the last"
					" [while processing COMMAND '%s']!"),
				     info.cmd->name());

				rc = -1;
				break;
			}

			if (!item)
			{
				datalen += cmd->reply_item_size(trans_nr, i);
			}
			else if (item->is_constant())
			{
				++datalen;
			}
			else
			{
				operand_ptr op(item->value(env));
				assert(op);

				rc = read_encode0_op(info, op, item->mask());
				if (rc)
					break;
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("encode read items, aborting"));
		rc = -1;
	}

	return rc;
}

int
device_io::read_decode0_var(struct read_decode_info &info,
			    class VARIABLE_tree *v, operand_ptr op, const lib::vbitset *item_mask)
{
	const unsigned char *buf = info.data + info.pos;
	size_t buflen = info.datalen - info.pos;

	const size_t varlen = v->get_io_size();
	assert(varlen > 0);

	if (item_mask)
	{
		if (!info.mask)
		{
			/* MASK open */
			info.mask = true;
			info.masklen = (item_mask->size() + 7) / 8;

			if (info.masklen > buflen)
			{
				error(info.logger, _("not enough bytes received"));
				return -1;
			}

			info.data_item.reset();
			info.data_item.from_buf(buf, info.masklen);

			info.pos += info.masklen;
		}

		if (item_mask->test(0))
		{
			/* MASK close */
			info.mask = false;
		}

		size_t trailing = 0;

		while (!item_mask->test(trailing))
			++trailing;

		lib::vbitset data(info.data_item);
		data &= *item_mask;
		data >>= trailing;

		if (varlen <= 16)
		{
			unsigned char tmpbuf[16];

			data.to_buf(tmpbuf, varlen);
			variable_decode(info.env, v, op, tmpbuf, varlen);
		}
		else
		{
			lib::auto_array<unsigned char> tmpbuf(varlen);

			data.to_buf(tmpbuf.data(), varlen);
			variable_decode(info.env, v, op, tmpbuf.data(), varlen);
		}
	}
	else
	{
		if (info.mask)
			/* warning, unterminated MASK close */
			info.mask = false;

		if (varlen > buflen)
		{
			error(info.logger, _("not enough bytes received"));
			return -1;
		}

		variable_decode(info.env, v, op, buf, varlen);
		info.pos += varlen;
	}

	return 0;
}

int
device_io::read_decode0_op(struct read_decode_info &info,
			   operand_ptr op, const lib::vbitset *item_mask)
{
	class EDD_OBJECT_tree *object = *op;
	assert(object);

	int rc = 0;

	switch (object->kind)
	{
		default:
			/* not possible */ assert(0);
			break;

		case EDD_VARIABLE:
		{
			class VARIABLE_tree *v = (class VARIABLE_tree *)object;

			rc = read_decode0_var(info, v, op, item_mask);

			break;
		}
		case EDD_VALUE_ARRAY:
		{
			long i_max = ((class VALUE_ARRAY_tree *)object)->length();
			assert(i_max >= 0);

			for (long i = 0; i < i_max; ++i)
			{
				rc = read_decode0_op(info, (*op)[i], NULL);
				if (rc)
					break;
			}

			break;
		}
		case EDD_LIST:
		{
			class LIST_tree *list = (class LIST_tree *)object;
			long fixed_length = list->fixed_length();

			if (fixed_length)
			{
				for (long i = 0; i < fixed_length; ++i)
				{
					rc = read_decode0_op(info, (*op)[i], NULL);
					if (rc)
						break;
				}
			}
			else
			{
				/* clear LIST */
				while (list->lookup_length(op))
					list->delete_at(op, 0);

				class EDD_OBJECT_tree *type = list->get_type();
				operand_ptr type_op(type->value_adt());

				/* add received elements */
				while (info.pos < info.datalen)
				{
					rc = read_decode0_op(info, type_op, NULL);
					if (rc)
						break;

					list->insert_end(op, type_op);
				}
			}

			break;
		}
		case EDD_REFERENCE_ARRAY:
		{
			class REFERENCE_ARRAY_tree *reference_array = (class REFERENCE_ARRAY_tree *)object;
			std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> elements;

			elements = reference_array->elements(info.env);

			for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
			{
				long index = elements[i]->index();

				rc = read_decode0_op(info, (*op)[index], NULL);
				if (rc)
					break;
			}

			break;
		}
		case EDD_COLLECTION:
		case EDD_RECORD:
		{
			std::vector<class MEMBER_tree *> members;

			if (object->kind == EDD_COLLECTION)
				members = ((class COLLECTION_tree *)object)->members(info.env);
			else
				members = ((class RECORD_tree *)object)->members(info.env);

			for (size_t i = 0, i_max = members.size(); i < i_max; ++i)
			{
				lib::IDENTIFIER ident(members[i]->ident());

				rc = read_decode0_op(info, op->member(ident), NULL);
				if (rc)
					break;
			}

			break;
		}
	}

	return rc;
}

device_io::read_decode_info::read_decode_info(class method_env *e, lib::Logger &l,
					      const class COMMAND_tree *c, int tnr,
					      const unsigned char *d, const size_t dlen)
: env(e), logger(l)
, cmd(c), trans_nr(tnr)
, mask(false)
, data(d), datalen(dlen)
, pos(0)
{
}

int
device_io::read_decode0(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
			lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		        const unsigned char *data, const size_t datalen)
{
	int rc = 0;

	try
	{
		struct read_decode_info info(env, logger, cmd, trans_nr, data, datalen);

		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			assert(info.pos <= datalen);

			class DATA_ITEM_tree *item = items[i];

			if (!item)
			{
				size_t expected = cmd->reply_item_size(trans_nr, i);

				if (expected)
				{
					error(logger, _("skipping %lu bytes for invalid reply item %lu"),
					      (unsigned long)expected, (unsigned long)(i+1));

					info.pos += expected;
				}
				else
				{
					error(logger, _("invalid reply item with nondeterministic size; "
						        "skip the last %lu variables"),
					      (unsigned long)(i_max - i));

					break;
				}
			}
			else if (item->is_constant())
			{
				const unsigned char *buf = data + info.pos;
				size_t buflen = datalen - info.pos;

				if (!buflen)
				{
					error(logger, _("not enough bytes received"));
					rc = -1;

					break;
				}

				if (buf[0] != item->get_constant())
				{
					error(logger, _("constant mismatch in reply (got %u, expected %li)"),
					      buf[0], item->get_constant());
					rc = -1;

					break;
				}

				++info.pos;
			}
			else
			{
				operand_ptr op(item->value(env));
				assert(op);

				rc = read_decode0_op(info, op, item->mask());
				if (rc)
					break;
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("decode read items, aborting"));
		rc = -1;
	}

	return rc;
}

int
device_io::write_encode0_var(struct write_encode_info &info,
			     class VARIABLE_tree *v, operand_ptr op, const lib::vbitset *item_mask)
{
	lib::auto_array<unsigned char> DataBufAlloc;
	unsigned char DataBufStack[64];
	unsigned char *DataBuf = NULL;
	size_t DataBufLen = 0;

	size_t varlen = v->get_io_size();
	assert(varlen > 0);

	if (varlen > sizeof(DataBufStack))
	{
		DataBufAlloc.reset(varlen);

		DataBuf = DataBufAlloc.data();
		DataBufLen = DataBufAlloc.size();
	}
	else
	{
		DataBuf = DataBufStack;
		DataBufLen = sizeof(DataBufStack);

		memset(DataBuf, 0, sizeof(DataBufLen));
	}
	assert(DataBuf && DataBufLen);

	variable_encode(info.env, v, op, DataBuf, varlen);

	const unsigned char *buf = DataBuf;
	size_t buflen = varlen;

	if (item_mask)
	{
		size_t trailing = 0;

		while (!item_mask->test(trailing))
			++trailing;

		lib::vbitset tmp;

		tmp.from_buf(buf, buflen);
		tmp <<= trailing;
		tmp &= *item_mask;

		if (info.mask.get())
		{
			info.datagramlen -= info.masklen;
			info.pos -= info.masklen;

			/* check for overlapping bits */
			if ((*info.mask & *item_mask).any())
			{
				error(info.logger, _("Item masks overlap in COMMAND '%s'!"),
				      info.cmd->name());
				return -1;
			}

			/* check that item mask is not larger than prev mask
			 */
			if (item_mask->size() > info.mask->size())
			{
				error(info.logger, _("Item masks overflow in COMMAND '%s'!"),
				      info.cmd->name());
				return -1;
			}

			*info.mask |= *item_mask;
		}
		else
		{
			/* MASK open */
			info.mask.reset(new lib::vbitset(*item_mask));
			info.masklen = (item_mask->size() + 7) / 8;

			info.data_item.reset();
		}

		if (item_mask->test(0))
		{
			/* MASK close */
			info.mask.reset(NULL);
		}

		if (info.masklen > DataBufLen)
		{
			DataBufAlloc.resize(info.masklen);

			if (DataBuf == DataBufStack)
				memcpy(DataBufAlloc.data(), DataBuf, DataBufLen);

			DataBuf = DataBufAlloc.data();
			DataBufLen = DataBufAlloc.size();
		}
		assert(info.masklen <= DataBufLen);

		info.data_item |= tmp;
		info.data_item.to_buf(DataBuf, info.masklen);

		buf = DataBuf;
		buflen = info.masklen;
	}
	else if (info.mask.get())
	{
		/* unterminated MASK close */
		warning(info.logger, _("Unterminated MASK close in COMMAND '%s'!"),
			info.cmd->name());

		info.mask.reset(NULL);
	}

	info.datagramlen += buflen;
	if (info.datagramlen > info.datalen)
	{
		error(info.logger, _("Size of variables to large (%lu > %lu) in COMMAND '%s'!"),
		      (unsigned long)info.datagramlen, (unsigned long)info.datalen,
		      info.cmd->name());

		return -1;
	}

	for (size_t k = 0; k < buflen; ++k)
		info.data[info.pos+k] |= buf[k];

	info.pos += buflen;

	if (v->have_write_timeout())
	{
		operand_ptr op(v->write_timeout(info.env));

		if (op)
		{	int32 rt = *op;
			info.timeout = std::max(info.timeout, rt);
		}
	}

	return 0;
}

int
device_io::write_encode0_op(struct write_encode_info &info,
			    operand_ptr op, const lib::vbitset *item_mask)
{
	class EDD_OBJECT_tree *object = *op;
	assert(object);

	int rc = 0;

	switch (object->kind)
	{
		default:
			/* not possible */ assert(0);
			break;

		case EDD_VARIABLE:
		{
			class VARIABLE_tree *v = (class VARIABLE_tree *)object;

			rc = write_encode0_var(info, v, op, item_mask);

			break;
		}
		case EDD_VALUE_ARRAY:
		case EDD_LIST:
		{
			long i_max;

			if (object->kind == EDD_VALUE_ARRAY)
				i_max = ((class VALUE_ARRAY_tree *)object)->length();
			else
				i_max = ((class LIST_tree *)object)->length(info.env);

			assert(i_max >= 0);

			for (long i = 0; i < i_max; ++i)
			{
				rc = write_encode0_op(info, (*op)[i], NULL);
				if (rc)
					break;
			}

			break;
		}
		case EDD_REFERENCE_ARRAY:
		{
			class REFERENCE_ARRAY_tree *reference_array = (class REFERENCE_ARRAY_tree *)object;
			std::vector<class REFERENCE_ARRAY_ELEMENT_tree *> elements;

			elements = reference_array->elements(info.env);

			for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
			{
				long index = elements[i]->index();

				rc = write_encode0_op(info, (*op)[index], NULL);
				if (rc)
					break;
			}

			break;
		}
		case EDD_COLLECTION:
		case EDD_RECORD:
		{
			std::vector<class MEMBER_tree *> members;

			if (object->kind == EDD_COLLECTION)
				members = ((class COLLECTION_tree *)object)->members(info.env);
			else
				members = ((class RECORD_tree *)object)->members(info.env);

			for (size_t i = 0, i_max = members.size(); i < i_max; ++i)
			{
				lib::IDENTIFIER ident(members[i]->ident());

				rc = write_encode0_op(info, op->member(ident), NULL);
				if (rc)
					break;
			}

			break;
		}
	}

	return rc;
}

device_io::write_encode_info::write_encode_info(class method_env *e, lib::Logger &l,
						const class COMMAND_tree *c, int tnr,
						unsigned char *d, const size_t dlen,
						size_t &dglen, int32 &t)
: env(e), logger(l)
, cmd(c), trans_nr(tnr)
, masklen(0)
, pos(0)
, data(d), datalen(dlen)
, datagramlen(dglen)
, timeout(t)
{
	memset(data, 0, datalen);
	datagramlen = 0;
	timeout = 0;
}

int
device_io::write_encode0(const class COMMAND_tree *cmd, int trans_nr, class method_env *env,
			 lib::Logger &logger, const std::vector<class DATA_ITEM_tree *> &items,
		         unsigned char *data, const size_t datalen, size_t &datagramlen,
		         int32 &timeout)
{
	int rc = 0;

	try
	{
		struct write_encode_info info(env, logger, cmd, trans_nr, data, datalen, datagramlen, timeout);

		for (size_t i = 0, i_max = items.size(); i < i_max; ++i)
		{
			class DATA_ITEM_tree *item = items[i];
			assert(item);

			if (item->is_constant())
			{
				data[info.pos++] = (unsigned char)(item->get_constant());
				++datagramlen;
			}
			else
			{
				operand_ptr op(item->value(env));
				assert(op);

				rc = write_encode0_op(info, op, item->mask());
				if (rc)
					break;
			}
		}
	}
	catch (const RuntimeError &xx)
	{
		runtime_error(logger, cmd, xx, _("decode read items, aborting"));
		rc = -1;
	}

	return rc;
}

void
device_io::variable_encode(class method_env *env, class VARIABLE_tree *v, operand_ptr op,
			   unsigned char *buf, size_t buflen)
{
	assert(buflen <= v->get_io_size());
	assert(op);

	op = v->get_device_value(env, op);
	assert(op);

	switch (v->gettype())
	{
		case EDD_VARIABLE_TYPE_ARITHMETIC:
		case EDD_VARIABLE_TYPE_INDEX:
		{
			switch (v->getsubtype())
			{
				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_INTEGER:
				{
					assert(buflen >= 1 && buflen <= 4);

					switch (buflen)
					{
						case 1:
						{
							union { int8 val; unsigned char bytes[1]; } v;

							v.val = *op;
							buf[0] = v.bytes[0];

							break;
						}

						case 2:
						{
							union { int16 val; unsigned char bytes[2]; } v;

							v.val = *op;
							buf[1] = v.bytes[0];
							buf[0] = v.bytes[1];

							break;
						}

						case 3:
						{
							union { int32 val; unsigned char bytes[4]; } v;

							v.val = *op;
							buf[0] = v.bytes[2];
							buf[1] = v.bytes[1];
							buf[2] = v.bytes[0];

							break;
						}

						case 4:
						{
							union { int32 val; unsigned char bytes[4]; } v;

							v.val = *op;
							buf[0] = v.bytes[3];
							buf[1] = v.bytes[2];
							buf[2] = v.bytes[1];
							buf[3] = v.bytes[0];

							break;
						}
					}

					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_UNSIGNED:
				{
					assert(buflen >= 1 && buflen <= 4);

					switch (buflen)
					{
						case 1:
						{
							union { uint8 val; unsigned char bytes[1]; } v;

							v.val = *op;
							buf[0] = v.bytes[0];

							break;
						}

						case 2:
						{
							union { uint16 val; unsigned char bytes[2]; } v;

							v.val = *op;
							buf[0] = v.bytes[1];
							buf[1] = v.bytes[0];

							break;
						}

						case 3:
						{
							union { uint32 val; unsigned char bytes[4]; } v;

							v.val = *op;
							buf[0] = v.bytes[2];
							buf[1] = v.bytes[1];
							buf[2] = v.bytes[0];

							break;
						}

						case 4:
						{
							union { uint32 val; unsigned char bytes[4]; } v;

							v.val = *op;
							buf[0] = v.bytes[3];
							buf[1] = v.bytes[2];
							buf[2] = v.bytes[1];
							buf[3] = v.bytes[0];

							break;
						}
					}

					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_FLOAT:
				{
					union { float32 val; unsigned char bytes[4]; } v;
					v.val = *op;

					assert(buflen == 4);

					for (size_t k = 0; k < buflen; ++k)
						buf[buflen - 1 - k] = v.bytes[k];

					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_DOUBLE:
				{
					union { float64 val; unsigned char bytes[8]; } v;
					v.val = *op;

					assert(buflen == 8);

					for (size_t k = 0; k < buflen; ++k)
						buf[buflen - 1 - k] = v.bytes[k];

					break;
				}

				default:
					/* never reached */ assert(0);
					break;
			}
			break;
		}

		case EDD_VARIABLE_TYPE_BOOLEAN:
		{
			assert(buflen == 1);

			buf[0] = *op;
			break;
		}

		case EDD_VARIABLE_TYPE_ENUMERATED:
		{
			assert(buflen >= 1 && buflen <= 4);

			switch (buflen)
			{
				case 1:
				{
					union { uint8 val; unsigned char bytes[1]; } v;

					v.val = *op;
					buf[0] = v.bytes[0];

					break;
				}

				case 2:
				{
					union { uint16 val; unsigned char bytes[2]; } v;

					v.val = *op;
					buf[0] = v.bytes[1];
					buf[1] = v.bytes[0];

					break;
				}

				case 3:
				{
					union { uint32 val; unsigned char bytes[4]; } v;

					v.val = *op;
					buf[0] = v.bytes[2];
					buf[1] = v.bytes[1];
					buf[2] = v.bytes[0];

					break;
				}

				case 4:
				{
					union { uint32 val; unsigned char bytes[4]; } v;

					v.val = *op;
					buf[0] = v.bytes[3];
					buf[1] = v.bytes[2];
					buf[2] = v.bytes[1];
					buf[3] = v.bytes[0];

					break;
				}
			}

			break;
		}

		case EDD_VARIABLE_TYPE_STRING:
		{
			std::string str = *op;
			size_t k;

			if (v->getsubtype() == EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII)
				str = lib::str2pca(str);

			assert(str.length() <= buflen);

			for (k = 0; k < str.length(); ++k)
				buf[k] = str[k];

			for (; k < buflen; ++k)
				buf[k] = '\0';

			break;
		}

		case EDD_VARIABLE_TYPE_DATE_TIME:
		{
			std::string str = *op;
			size_t k;

			assert(str.length() <= buflen);

			for (k = 0; k < str.length(); ++k)
				buf[k] = str[k];

			for (; k < buflen; ++k)
				buf[k] = '\0';

			break;
		}

		case EDD_VARIABLE_TYPE_OBJECT:
		{
			for (size_t k = 0; k < buflen; ++k)
				buf[k] = '\0';

			break;
		}

		default:
			/* never reached */ assert(0);
			break;
	}
}

void
device_io::variable_decode(class method_env *env, class VARIABLE_tree *v, operand_ptr op,
			   const unsigned char *buf, const size_t buflen)
{
	operand_ptr value;

	assert(buflen == v->get_io_size());

	switch (v->gettype())
	{
		case EDD_VARIABLE_TYPE_ARITHMETIC:
		case EDD_VARIABLE_TYPE_INDEX:
		{
			switch (v->getsubtype())
			{
				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_INTEGER:
				{
					assert(buflen >= 1 && buflen <= 4);

					switch (buflen)
					{
						case 1:
						{
							union { int8 val; unsigned char bytes[1]; } v;

							v.bytes[0] = buf[0];

							value = new int8_value(v.val);
							break;
						}

						case 2:
						{
							union { int16 val; unsigned char bytes[2]; } v;

							v.bytes[0] = buf[1];
							v.bytes[1] = buf[0];

							value = new int16_value(v.val);
							break;
						}

						case 3:
						{
							union { int32 val; unsigned char bytes[4]; } v;

							v.bytes[0] = buf[2];
							v.bytes[1] = buf[1];
							v.bytes[2] = buf[0];
							v.bytes[3] = 0;

							value = new int32_value(v.val);
							break;
						}

						case 4:
						{
							union { int32 val; unsigned char bytes[4]; } v;

							v.bytes[0] = buf[3];
							v.bytes[1] = buf[2];
							v.bytes[2] = buf[1];
							v.bytes[3] = buf[0];

							value = new int32_value(v.val);
							break;
						}
					}

					assert(value);
					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_UNSIGNED:
				{
					assert(buflen >= 1 && buflen <= 4);

					switch (buflen)
					{
						case 1:
						{
							union { uint8 val; unsigned char bytes[1]; } v;

							v.bytes[0] = buf[0];

							value = new uint8_value(v.val);
							break;
						}

						case 2:
						{
							union { uint16 val; unsigned char bytes[2]; } v;

							v.bytes[0] = buf[1];
							v.bytes[1] = buf[0];

							value = new uint16_value(v.val);
							break;
						}

						case 3:
						{
							union { uint32 val; unsigned char bytes[4]; } v;

							v.bytes[0] = buf[2];
							v.bytes[1] = buf[1];
							v.bytes[2] = buf[0];
							v.bytes[3] = 0;

							value = new uint32_value(v.val);
							break;
						}

						case 4:
						{
							union { uint32 val; unsigned char bytes[4]; } v;

							v.bytes[0] = buf[3];
							v.bytes[1] = buf[2];
							v.bytes[2] = buf[1];
							v.bytes[3] = buf[0];

							value = new uint32_value(v.val);
							break;
						}
					}

					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_FLOAT:
				{
					union { float32 val; unsigned char bytes[4]; } v;

					assert(buflen == 4);

					for (size_t k = 0; k < buflen; ++k)
						v.bytes[k] = buf[buflen - 1 - k];

					value = new float32_value(v.val);
					break;
				}

				case EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_DOUBLE:
				{
					union { float64 val; unsigned char bytes[8]; } v;

					assert(buflen == 8);

					for (size_t k = 0; k < buflen; ++k)
						v.bytes[k] = buf[buflen - 1 - k];

					value = new float64_value(v.val);
					break;
				}

				default:
					/* never reached */ assert(0);
					break;
			}
			break;
		}

		case EDD_VARIABLE_TYPE_BOOLEAN:
		{
			assert(buflen == 1);

			value = new boolean_value(buf[0]);

			break;
		}

		case EDD_VARIABLE_TYPE_ENUMERATED:
		{
			assert(buflen >= 1 && buflen <= 4);

			switch (buflen)
			{
				case 1:
				{
					union { uint8 val; unsigned char bytes[1]; } v;

					v.bytes[0] = buf[0];

					value = new uint8_value(v.val);
					break;
				}

				case 2:
				{
					union { uint16 val; unsigned char bytes[2]; } v;

					v.bytes[0] = buf[1];
					v.bytes[1] = buf[0];

					value = new uint16_value(v.val);
					break;
				}

				case 3:
				{
					union { uint32 val; unsigned char bytes[4]; } v;

					v.bytes[0] = buf[2];
					v.bytes[1] = buf[1];
					v.bytes[2] = buf[0];
					v.bytes[3] = 0;

					value = new uint32_value(v.val);
					break;
				}

				case 4:
				{
					union { uint32 val; unsigned char bytes[4]; } v;

					v.bytes[0] = buf[3];
					v.bytes[1] = buf[2];
					v.bytes[2] = buf[1];
					v.bytes[3] = buf[0];

					value = new uint32_value(v.val);
					break;
				}
			}

			break;
		}

		case EDD_VARIABLE_TYPE_STRING:
		{
			lib::dyn_buf tmpbuf(buflen+4);

			switch (v->getsubtype())
			{
				case EDD_VARIABLE_TYPE_STRING_TYPE_PACKED_ASCII:
				{
					tmpbuf.put(lib::pca2str((const char *)(buf), buflen));
					break;
				}
				default:
				{
					tmpbuf.put((const char *)(buf), buflen);
					break;
				}
			}

			value = new string_value(tmpbuf);
			break;
		}

		case EDD_VARIABLE_TYPE_DATE_TIME:
		{
			lib::scratch_buf tmpbuf(buflen+4);
			tmpbuf.put((const char *)(buf), buflen);

			value = new string_value(tmpbuf);
			break;
		}

		case EDD_VARIABLE_TYPE_OBJECT:
		{
			value = v->default_lvalue();
			break;
		}

		default:
			/* never reached */ assert(0);
			break;
	}
	assert(value);

	v->assign_device_value(env, op, value);
}

} /* namespace edd */
