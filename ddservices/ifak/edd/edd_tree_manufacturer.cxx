
/* 
 * $Id: edd_tree_manufacturer.cxx,v 1.13 2008/10/21 00:20:03 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_tree.h"

// C stdlib

// C++ stdlib

// my lib

// own header
#include "DevicesParser.h"


namespace edd
{

#include "devices_data.cxx"

int
edd_tree::lookup_manufacturer_name(const char *name) const
{
	/* first lookup in the dynamically loaded devices data */
	int ret;

	ret = devices_->lookup_manufacturer_name(name);
	if (ret < 0)
		ret = static_lookup_manufacturer_name(name);

	return ret;
}

int
edd_tree::lookup_device_type_name(int manufacturer_id, const char *name) const
{
	/* first lookup in the dynamically loaded devices data */
	int ret;

	ret = devices_->lookup_device_type_name(manufacturer_id, name);
	if (ret < 0)
		ret = static_lookup_device_type_name(manufacturer_id, name);

	return ret;
}

std::string
edd_tree::lookup_manufacturer_id(int id) const
{
	std::string str;

	str = static_lookup_manufacturer_id(id);
	if (str.empty())
		str = devices_->lookup_manufacturer_id(id);

	return str;
}

std::string
edd_tree::lookup_device_type_id(int manufacturer_id, int id) const
{
	/* first lookup in the dynamically loaded devices data */
	std::string str;

	str = devices_->lookup_device_type_id(manufacturer_id, id);
	if (str.empty())
		str = static_lookup_device_type_id(manufacturer_id, id);

	return str;
}

} /* namespace edd */
