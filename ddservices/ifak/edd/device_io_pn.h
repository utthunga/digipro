
/* 
 * $Id: device_io_pn.h,v 1.3 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _device_io_pn_h
#define _device_io_pn_h

// C stdlib
#include <stdlib.h>

// C++ stdlib
#include <set>
#include <vector>

// other libs

// my lib
#include "Timeval.h"

// own header
#include "device_io.h"


namespace edd
{

/**
 * Profibus communication base.
 * This communication specialization support the profibus protocol.
 * It include all generic things like executing COMMANDs, reading
 * the profibus directory and do the relative to absolute address mapping.
 */
class device_io_pn : public device_io
{
public:
	device_io_pn(class Interpreter &);

	/* implement device_io_interface */
	virtual int do_command(class method_env *,
			       const class COMMAND_tree *, int, enum action,
			       int &warnings);

protected:
	/* low-level I/O interface */
	struct request
	{
		unsigned short slot;
		unsigned short subslot;
		unsigned short index;
		unsigned long datalen;
		unsigned char *data;
	};
	virtual long pn_read(struct request &) = 0;
	virtual long pn_write(const struct request &) = 0;

private:
	void dump_data(const class COMMAND_tree *cmd, const char *op,
		       const unsigned char *buf, size_t buflen, const char *descr);

	void do_timeout(const class COMMAND_tree *cmd, const char *op,
			long timeout);

	int  do_read_command(const class COMMAND_tree *, int, class method_env *, int &warnings);
	int  do_write_command(const class COMMAND_tree *, int, class method_env *, int &warnings);

	bool resolve_cmd(const class COMMAND_tree *, const char *op,
			 class method_env *, struct request &);

	/* timeout handling */
	long required_timeout;
	os_service::Timeval timestamp;
};

} /* namespace edd */

#endif /* _device_io_pn_h */
