
/* 
 * $Id: device_io_null.cxx,v 1.5 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io_null.h"

// C stdlib

// C++ stdlib

// my lib

// own header


namespace edd
{

bool
device_io_null::is_online(void) const
{
	return false;
}

int
device_io_null::read(class method_env *, class EDD_OBJECT_tree *)
{
	return 0;
}

int
device_io_null::write(class method_env *, class EDD_OBJECT_tree *)
{
	return 0;
}

int
device_io_null::do_command(class edd::method_env *,
			   const class COMMAND_tree *, int, enum action,
			   int &warnings)
{
	return 0;
}

} /* namespace edd */
