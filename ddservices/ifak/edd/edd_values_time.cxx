
/* 
 * $Id: edd_values_time.cxx,v 1.24 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>

// C++ stdlib

// my lib
#include "port.h"
#include "string.h"
#include "strtoxx.h"
#include "BasicType.h"
#include "sys_types.h"

// own
#include "edd_values_time.h"
#include "edd_ast.h"


namespace edd
{

#if 0
/* helper
 */
static std::string
print_uint64(sys::uint64 v)
{
	lib::scratch_buf buf(50);

	do {
		buf.put("0123456789"[v % 10]);
		v /= 10;
	}       
	while (v > 0);

	std::string ret(buf.str());
	return lib::strrev(ret);
}
#endif

date_and_time_value::date_and_time_value(bool flag)
: with_time(flag ? 4 : 0)
{
	if (with_time)
	{
		value.resize(7);

		for (int i = 0; i < 4; ++i)
			value[i] = '\0';
	}
	else
		value.resize(3);

	value[with_time  ] =  1;
	value[with_time+1] =  1;
	value[with_time+2] = 80;
}

date_and_time_value::date_and_time_value(bool flag, const std::string &v)
: with_time(flag ? 4 : 0)
, value(v)
{
	assert(with_time ? (value.size() == 7) : (value.size() == 3));
}

operand_ptr
date_and_time_value::clone(void) const
{
	return new date_and_time_value(with_time > 0, value);
}

void
date_and_time_value::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ date%s { \"%s\" }",
		 with_time ? "_and_time" : "", visible.c_str());
}

/*
 * DATE
 * 
 * Octet
 *   
 *   1 - bit0-7 - 1..31 day of month
 *   2 - bit0-7 - 1..12 month
 *   3 - bit0-7 - 1..255 years (year minus 1900)
 */

/*
 * DATE_AND_TIME
 * 
 * Octet
 *   
 *   1 
 *   2 - bit0-15 - 0..59999ms
 *   3 - bit0-5  - 0..59min
 *   4 - bit0-4  - 0..23h
 *     - bit5-6  - reserved
 *     - bit7    - 0 = standard time, 1 = summer time
 *   5 - bit0-4  - 1..31 day of month
 *     - bit5-7  - 1..7 day of week (1=Monday, 7=Sunday)
 *   6 - bit0-5  - 1..12 month
 *     - bit6-7  - reserved
 *   7 - bit0-7  - 1..255 years (year minus 1900)
 */

std::string
date_and_time_value::print(const char *fmt) const
{
	unsigned int day   = value[with_time  ] & 0x1f;
	unsigned int month = value[with_time+1] & 0x3f;
	unsigned int year;
	unsigned char c;

	c = value[with_time+2];
	year = 1900 + c;

	lib::scratch_buf buf(60);

	buf.printf("%02u.%02u.%4u", day, month, year);

	if (with_time)
	{
		unsigned int min   = value[2] & 0x3f;
		unsigned int hour  = value[3] & 0x1f;
		unsigned int sec;

		c = value[0];
		sec = c;
		sec <<= 8;

		c = value[1];
		sec |= c;
		sec /= 1000;

		buf.printf(" %2u:%02u:%02u", hour, min, sec);
	}

	return buf.str();
}

            date_and_time_value::operator stackmachine::Type *() const
	    { return new stackmachine::stringType(pretty_print()); }

            date_and_time_value::operator bool() const { return value.length() ? true : false; }
            date_and_time_value::operator std::string() const { return value; }

bool        date_and_time_value::operator==  (const operand &op) const { return (value == (std::string) op); }
bool        date_and_time_value::operator!=  (const operand &op) const { return (value != (std::string) op); }


date_and_time_lvalue::date_and_time_lvalue(bool with_time, value_status_t status)
: date_and_time_value(with_time)
, mystatus(status)
{
}

date_and_time_lvalue::date_and_time_lvalue(bool with_time, const std::string &v, value_status_t status)
: date_and_time_value(with_time, v)
, mystatus(status)
{
}

void
date_and_time_lvalue::assign(const std::string &v, bool strict)
{
	if (!(with_time ? (v.size() == 7) : (v.size() == 3)))
		throw RuntimeError(xxRange);

	value = v;
	mystatus.changed();
}

operand_ptr
date_and_time_lvalue::clone(void) const
{
	return new date_and_time_lvalue(with_time > 0, value, mystatus.reference());
}

void
date_and_time_lvalue::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ date%s lval", with_time ? "_and_time" : "");
	mystatus.dump(f);
	f.printf(" { \"%s\" }", visible.c_str());
}

void
date_and_time_lvalue::scan(const std::string &str, bool strict)
{
	std::string newvalue(value);
	const char *s = str.c_str();
	unsigned int day, month, year;

	if (with_time)
	{
		unsigned int hour, min, sec;
		int ret;

		ret = sscanf(s, "%2u.%2u.%4u %2u:%2u:%2u",
			     &day, &month, &year,
			     &hour, &min, &sec);

		if (ret != 6)
			throw RuntimeError(xxInvalidInput);

		if (hour > 23 || min > 59 || sec > 59)
			throw RuntimeError(xxInvalidInput);

		unsigned int ms = sec * 1000;

		newvalue[0] = (ms >> 8) & 0xff;
		newvalue[1] = (ms & 0xff);
		newvalue[2] = min;
		newvalue[3] = hour;
	}
	else
	{
		int ret;

		ret = sscanf(s, "%2u.%2u.%4u", &day, &month, &year);

		if (ret != 3)
			throw RuntimeError(xxInvalidInput);
	}

	if (year < 80)
		year += 100;

	if (year >= 1900)
		year -= 1900;

	if (!lib::check_date(day, month, year + 1900))
		throw RuntimeError(xxInvalidInput);

	if (year > 255)
		throw RuntimeError(xxInvalidInput);

	newvalue[with_time  ] = day;
	newvalue[with_time+1] = month;
	unsigned char _year = year;
	newvalue[with_time+2] = _year;

	assign(newvalue, strict);
}

void        date_and_time_lvalue::operator= (const stackmachine::Type &v) { pretty_scan((std::string) v); }

operand::value_status_t& date_and_time_lvalue::status(void) { return mystatus.reference(); }
const operand::value_status_t& date_and_time_lvalue::status(void) const { return mystatus.reference(); }

void        date_and_time_lvalue::operator=   (const operand &op) { assign((std::string) op); }


duration_time_value::duration_time_value(bool time)
: is_time(time)
{
	value.resize(6);

	for (size_t i = 0, i_max = value.size(); i < i_max; ++i)
		value[i] = '\0';
}

duration_time_value::duration_time_value(bool time, const std::string &v)
: is_time(time)
, value(v)
{
	assert((value.size() == 6) || (value.size() == 0));
}

operand_ptr
duration_time_value::clone(void) const
{
	return new duration_time_value(is_time, value);
}

void
duration_time_value::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ %s { \"%s\" }", (is_time) ? "time" : "duration", visible.c_str());
}

/*
 * DURATION
 * 
 * Octet
 *   
 *   1
 *   2
 *   3
 *   4 - bit0-27  - Number of milliseconds since midnight
 *     - bit28-31 - set to zero
 *   5
 *   6 - bit 0-15 Number of days
 */

/*
 * TIME
 *
 * Octet
 *   
 *   1
 *   2
 *   3
 *   4 - bit0-27  - Number of milliseconds since midnight
 *     - bit28-31 - set to zero
 *   5
 *   6 - bit 0-15 Number of days since 01.01.1984
 */

sys::uint32
duration_time_value::get_ms(void) const
{
	sys::uint32 ms;

	ms = (unsigned char) (value[0] & 0xf);
	ms <<= 8;
	ms |= (unsigned char) value[1];
	ms <<= 8;
	ms |= (unsigned char) value[2];
	ms <<= 8;
	ms |= (unsigned char) value[3];

	return ms;
}

std::string
duration_time_value::print(const char *fmt) const
{
	sys::uint32 ms = get_ms();
	size_t days = (unsigned char) value[4];
	days <<= 8;
	days |= (unsigned char) value[5];

	unsigned int hour, min, sec;


	lib::scratch_buf buf(40);

	if (is_time)
	{
		struct tm tv = {0,0,0,0,0,0,0,0,0};
		tv.tm_year = 84;
		tv.tm_mon = 0;
		tv.tm_mday = 1;

		time_t base = mktime(&tv);

		if (days)
			base += days*24*60*60 + ms/1000;

		struct tm * time_ = localtime(&base);

		buf.printf("%02u.%02u.%4u", time_->tm_mday, time_->tm_mon+1, time_->tm_year + 1900);
		buf.printf(" %2u:%02u:%02u", time_->tm_hour, time_->tm_min, time_->tm_sec);

	}
	else
	{
		if (days)
			buf.printf("%lud ", (unsigned long) days);

		hour = ms / 3600000;
		ms -= hour * 3600000;

		min = ms / 60000;
		ms -= min * 60000;

		sec = ms / 1000;
		ms -= sec * 1000;
		buf.printf("%uh %02um %02us %03ums", hour, min, sec, (unsigned int)ms);
	}


	return buf.str();
}

            duration_time_value::operator stackmachine::Type *() const
	    { return new stackmachine::stringType(pretty_print()); }

            duration_time_value::operator bool() const { return value.length() ? true : false; }
            duration_time_value::operator std::string() const { return value; }

bool        duration_time_value::operator==  (const operand &op) const { return (value == (std::string) op); }
bool        duration_time_value::operator!=  (const operand &op) const { return (value != (std::string) op); }


duration_time_lvalue::duration_time_lvalue(bool time, value_status_t status)
: duration_time_value(time)
, mystatus(status)
{
}

duration_time_lvalue::duration_time_lvalue(bool time, const std::string &v, value_status_t status)
: duration_time_value(time, v)
, mystatus(status)
{
}

void
duration_time_lvalue::assign(const std::string &v, bool strict)
{
	if (!(v.size() == 6))
		throw RuntimeError(xxRange);

	value = v;
	mystatus.changed();
}

operand_ptr
duration_time_lvalue::clone(void) const
{
	return new duration_time_lvalue(is_time, value, mystatus.reference());
}

void
duration_time_lvalue::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ %s lval", (is_time) ? "time" : "duration");

	mystatus.dump(f);
	f.printf(" { \"%s\" }", visible.c_str());
}

void
duration_time_lvalue::scan(const std::string &str, bool strict)
{
	std::string newvalue;
	newvalue.resize(6);
	const char *s = str.c_str();

	if (is_time)
	{
		int ret, year, month, day, hour, min, sec;

		struct tm tv = {0,0,0,0,0,0,0,0,0};
		tv.tm_year = 84;
		tv.tm_mon = 0;
		tv.tm_mday = 1;

		time_t base = mktime(&tv);

		ret = sscanf(s, "%u.%u.%u %u:%u:%u", &day, &month, &year,
						&hour, &min, &sec);

		if (hour < 0 || hour > 23 || min < 0 || min > 59)
			throw RuntimeError(xxInvalidInput);

		unsigned int newms = (((hour*60)+min)*60+sec)*1000;

		if ((newms & 0x0fffffff) != newms)
			throw RuntimeError(xxRange);

		newvalue[0] = (newms >> 24) & 0xff;
		newvalue[1] = (newms >> 16) & 0xff;
		newvalue[2] = (newms >> 8) & 0xff;
		newvalue[3] = (newms) & 0xff;


		if (year > 1900)
			year -= 1900;

		if (!lib::check_date(day, month, year+1900))
			throw RuntimeError(xxInvalidInput);

		if (year < 84)
			throw RuntimeError(xxRange);

		tv.tm_year = year;
		tv.tm_mon = month-1;
		tv.tm_mday = day;
		tv.tm_hour = hour;
		tv.tm_min = min;
		tv.tm_sec = sec;

		time_t newdate = mktime(&tv);
		newdate -= base;

		unsigned int days = newdate / 86400;

		newvalue[4] = (days >> 8) & 0xff;
		newvalue[5] = (days & 0xff);

	}
	else
	{
		int ret;
		int hour, min, sec, ms, day= 0;

		ret = sscanf(s, "%uh %um %us %ums", &hour, &min, &sec, &ms);

		if (ret != 4)
		{
			ret = sscanf(s, "%ud %uh %um %us %ums", &day, &hour, &min, &sec, &ms);
			if (ret != 5)
				throw RuntimeError(xxInvalidInput);

			if (day < 0)
				throw RuntimeError(xxInvalidInput);
		}

		if (hour > 23 || min < 0 || min > 59)
			throw RuntimeError(xxInvalidInput);

		if (day > 65535)
			throw RuntimeError(xxRange);

		unsigned int newms = hour*3600000 + min*60000 + sec*1000 + ms;

		if ((newms & 0x0fffffff) != newms)
			throw RuntimeError(xxRange);

		newvalue[0] = (newms >> 24) & 0xff;
		newvalue[1] = (newms >> 16) & 0xff;
		newvalue[2] = (newms >> 8) & 0xff;
		newvalue[3] = (newms) & 0xff;

		newvalue[4] = (day >> 8) & 0xff;
		newvalue[5] = (day & 0xff);
	}

	assign(newvalue, false);
}

void        duration_time_lvalue::operator= (const stackmachine::Type &v) { pretty_scan((std::string) v); }

operand::value_status_t& duration_time_lvalue::status(void) { return mystatus.reference(); }
const operand::value_status_t& duration_time_lvalue::status(void) const { return mystatus.reference(); }

void        duration_time_lvalue::operator=   (const operand &op) { assign((std::string) op); }


time_value_value::time_value_value(bool flag, class VARIABLE_TYPE_DATE_TIME_tree *type)
: with_days((flag) ? 8 : 4)
, type(type)
{
	value.resize(with_days);

	for (size_t i = 0, i_max = value.size(); i < i_max; ++i)
		value[i] = '\0';
}

time_value_value::time_value_value(bool flag, class VARIABLE_TYPE_DATE_TIME_tree *type, const std::string &v)
: with_days((flag) ? 8 : 4)
, value(v)
, type(type)
{
	assert(value.size() == with_days);
}

operand_ptr
time_value_value::clone(void) const
{
	if (with_days == 4)
		return new time_value_value(false, type, value);
	else
		return new time_value_value(true, type, value);
}

void
time_value_value::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ time_value(%lu) { \"%s\" }",
		 with_days, visible.c_str());
}

/*
 * TIME_VALUE
 * 
 * Octet
 *   
 *   1
 *   2
 *   3
 *   4
 *   5
 *   6
 *   7
 *   8 - bit0-62 - Number of 1/32 ms.
 *     - bit63   - SN: 0 = data is positive, 1 = data is negative and in two's
 *                                           complement representation
 *
 * Octet
 *
 *   1
 *   2
 *   3
 *   4 - bit 0-31 - Number of 1/32 ms.
 */

sys::int64
time_value_value::get_value(void) const
{
	sys::int64 ms = 0;

	for (size_t i = 0; i < with_days - 1; ++i)
	{
		ms |= (unsigned char) value[i];
		ms <<= 8;
	}

	ms |= (unsigned char) value[with_days - 1];

	return ms;
}

std::string
time_value_value::print(const char *fmt) const
{
	lib::scratch_buf buf(30);

	if (!type->has_scale())
	{
		std::string fmt;
		const std::string default_fmt("%H:%M:%S");

		time_t sec = get_value()/32/1000;
		struct tm *tm = gmtime(&sec);

		if (type->have_time_format())
		{
			fmt = type->get_time_format(env);

			// convert %T to %H:%M:%S to work around missing %T in msvc strftime
			std::string::size_type pos = 0;
			const std::string T_fmt("%T");

			while ((pos = fmt.find(T_fmt, pos)) != std::string::npos)
			{
				fmt.replace(pos, T_fmt.length(), default_fmt);
				pos += default_fmt.length();
			}
		}
		else
			fmt = default_fmt;

		char str[1024];
		size_t ret = strftime(str, 1024, fmt.c_str(), tm);

		return std::string(str, ret);
	}
	else
	{
		float32 scale = type->get_scale();
		std::string fmt;

		if (type->have_display_format())
		{
			fmt = type->get_display_format(env);
			fmt.insert(fmt.begin(), '%');
		}
		else
			fmt = "%.2f";

		buf.printf(fmt.c_str(), get_value() * scale);
	}

	return buf.str();
}

            time_value_value::operator stackmachine::Type *() const
	    { return new stackmachine::int64Type(get_value()); }

            time_value_value::operator bool() const { return value.length() ? true : false; }
            time_value_value::operator uint32() const { return get_value(); }
            time_value_value::operator std::string() const { return value; }

bool        time_value_value::operator==  (const operand &op) const { return (value == (std::string) op); }
bool        time_value_value::operator!=  (const operand &op) const { return (value != (std::string) op); }


time_value_lvalue::time_value_lvalue(bool flag, class VARIABLE_TYPE_DATE_TIME_tree *type, value_status_t status)
: time_value_value(flag, type)
, mystatus(status)
{
}

time_value_lvalue::time_value_lvalue(bool flag, class VARIABLE_TYPE_DATE_TIME_tree *type, const std::string &v, value_status_t status)
: time_value_value(flag, type, v)
, mystatus(status)
{
}

void
time_value_lvalue::assign(const std::string &v, bool strict)
{
	if (v.size() != with_days)
		throw RuntimeError(xxRange);

	value = v;
	mystatus.changed();
}

operand_ptr
time_value_lvalue::clone(void) const
{
	return new time_value_lvalue((with_days == 8) ? true : false, type, value, mystatus.reference());
}

void
time_value_lvalue::dump0(lib::stream &f) const
{
	std::string visible = print();

	f.printf("$$ time_value(%lu) lval", with_days);
	mystatus.dump(f);
	f.printf(" { \"%s\" }", visible.c_str());
}

void
time_value_lvalue::scan(const std::string &str, bool strict)
{
	std::string newvalue;
	newvalue.resize(with_days);

	float32 v;
	float32 scale = 1.0;

	if (type->has_scale())
		scale = type->get_scale();

	const char *s = str.c_str();

	int ret = sscanf(s, "%f", &v);

	if (ret != 1)
		throw RuntimeError(xxInvalidInput);

	if (type->has_scale())
		v /= scale;

	uint64 ms32 = v;


	for (unsigned char i = 0; i < with_days; ++i)
	{
		unsigned char shift = (with_days * 8) - ((i+1) * 8);
		newvalue[i] = (ms32 >> shift) & 0xff;
	}

	assign(newvalue, false);
}

void        time_value_lvalue::operator= (const stackmachine::Type &v) { pretty_scan((std::string) v); }

operand::value_status_t& time_value_lvalue::status(void) { return mystatus.reference(); }
const operand::value_status_t& time_value_lvalue::status(void) const { return mystatus.reference(); }

void        time_value_lvalue::operator=   (const operand &op) { assign((std::string) op); }

} /* namespace edd */
