
/* 
 * $Id: fmtcheck.h,v 1.2 2008/11/06 17:50:21 fna Exp $
 * 
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_fmtcheck_h
#define _edd_fmtcheck_h

// C stdlib

// C++ stdlib

// my lib

// own header


namespace edd
{

#ifndef __NetBSD__

/**
 * \internal
 * Sanitizes user-supplied printf(3)-style format string
 *
 * \param fmt_suspect The format string that need to be validated.
 * \param fmt_default The reference format string.
 *
 * \return fmt_default if the fmt_suspect argument don't match the
 * format types as defined by fmt_default.
 */
const char *fmtcheck(const char *fmt_suspect, const char *fmt_default);

#endif

} /* namespace edd */

#endif /* _edd_fmtcheck_h */
