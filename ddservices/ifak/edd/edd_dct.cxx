
/* 
 * $Id: edd_dct.cxx,v 1.18 2006/03/14 13:54:11 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_dct.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// my lib

// own header


namespace edd
{

std::string
edd_dictionary::lookup(const lib::IDENTIFIER &id) const
{
	map_t::const_iterator iter(map.find(id));

	if (iter != map.end())
		return iter->second.first;

	return id.str();
}

std::string
edd_dictionary::lookup(const lib::IDENTIFIER &id, bool &found) const
{
	map_t::const_iterator iter(map.find(id));

	if (iter != map.end())
	{
		found = true;
		return iter->second.first;
	}

	found = false;
	return id.str();
}

lib::pos
edd_dictionary::get_pos(const lib::IDENTIFIER &id) const
{
	map_t::const_iterator iter(map.find(id));

	if (iter != map.end())
		return iter->second.second;

	return lib::NoPosition;
}

void
edd_dictionary::dump(lib::stream &f) const
{
	map_t::const_iterator end(map.end());
	map_t::const_iterator iter(map.begin());

	while (iter != end)
	{
		f.put('[');
		f.put(iter->first.str());
		f.put("] \"");
		f.put(iter->second.first);
		f.put("\"\n");

		++iter;
	}
}

} /* namespace edd */
