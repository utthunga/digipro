
/* 
 * $Id: Checker.cxx,v 1.33 2009/07/20 19:48:29 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "Checker.h"

// C stdlib
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// C++ stdlib

// my lib
#include "IniFile.h"
#include "fstream.h"
#include "paths.h"
#include "port.h"
#include "sys.h"

// own header
#include "edd_ast.h"


namespace edd
{

/**
 * Parse dictionary files.
 * This method do all neccessary steps to parse and check EDD dictionary
 * files. It's designed for the command line checker and use stdout/stderr for
 * all output. If the quiet flag is false it output some usefule statistic
 * information too (this include parse time, the number of dictionary
 * entries and the replaced entries). The quiet flag default to true.
 * \param dictionaries Vector of dictionary files that should be parsed and
 * checked.
 * \param quiet Optional, default to true. Output additional statistic
 * information if false.
 * \param warnings Enable/disable warnings.
 * \param f Log stream for error and information messages.
 */
int
Checker::load_dictionaries(const std::vector<std::string> &dictionaries,
			   bool quiet, bool warnings,
			   lib::stream &f)
{
	clock_t time1, time2;
	bool force_nl = false;
	int ret = 0;

	err_config(!warnings);

	/* get start time */
	time1 = clock();

	for (size_t i = 0, i_max = dictionaries.size(); i < i_max; i++)
		ret += load_dictionary(dictionaries[i]);

	/* get end time */
	time2 = clock();

	if (ret || err().get_msgCount())
	{
		errmsg_flush(f);
		force_nl = true;
	}

	/* print statistics */
	if (!quiet)
	{
		long double duration;

		if (force_nl)
			f.put('\n');

		/* calculate used time */
		duration = time2 - time1;
		duration /= CLOCKS_PER_SEC;

		f.printf("Parsed dictionaries in %.2Lf seconds.\n", duration);
		f.flush();
	}

	return ret;
}

/**
 * Parse and check EDD file.
 * This method do all neccessary steps to parse and check an EDD file.
 * It's designed for the command line checker and use stdout/stderr for all
 * output. If the quiet flag is false it output some useful statistic
 * information too (this include parse time and tree statistic). The quiet
 * flag default to true.
 * \param file The input file name. Should be a valid pathname to a file.
 * \param includes Vector of include paths there the builtin preprocessor
 * should search for included files.
 * \param defines Vector of predefined macros for the preprocessor.
 * \param quiet If not true output additional statistic information.
 * \param warnings Enable/disable the warnings.
 * \param err_limit The maximum number of errors that shall be reported.
 * Set this to 0 to disable.
 * \param profile Which profile should be used for parsing and checking.
 * \param options The options for the syntax and semantic check. See
 * edd::edd_tree::PDM_MODE, edd::edd_tree::PEDANTIC and
 * edd::edd_tree::NO_EDDx1.
 * \param f The stream where the error and warning messages are printed out.
 */
int
Checker::check_file(const std::string &file,
		    const std::vector<std::string> &includes,
		    const std::vector<std::string> &defines,
		    bool quiet, bool warnings, int err_limit,
		    profile_t profile, size_t options,
		    lib::stream &f)
{
	clock_t time1, time2;
	bool force_nl = false;
	int ret;

	if (err_limit > 0)
		err_threshold(err_limit);

	err_config(!warnings);

	/* get start time */
	time1 = clock();

	ret = parse(file.c_str(), includes, defines, profile, options);

	/* get end time */
	time2 = clock();

	if (ret || err().get_msgCount())
	{
		force_nl = true;

		errmsg_flush(f);

		if (ret)
		{
			f.put("\nChecking aborted due to fatal errors, ");
			f.put("remaining checks not executed.\n");
		}
	}

	/* print statistics */
	if (!quiet)
	{
		unsigned long size, terminal, nonterminal, eddobjects;
		long double duration;

		if (force_nl)
			f.put('\n');

		/* calculate used time */
		duration = time2 - time1;
		duration /= CLOCKS_PER_SEC;

		f.printf("Parsed and checked in %.2Lf seconds.\n", duration);

		size = stat_tree(terminal, nonterminal, eddobjects);
		f.printf("Nodes: %lu (%lu non-terminal, %lu terminal;"
			 " %lu kb).\n",
			 terminal+nonterminal, nonterminal, terminal, size / 1024);

		if (root)
		{
			size = tree_count(root->definition_list());
			f.printf("EDD objects: %lu\n", size);
		}

		f.put('\n');
		f.flush();
	}

	return ret;
}

void
Checker::dump_statistic(lib::stream &f)
{
	/* first the edd objects summary */
	{
		unsigned long dummy, eddobjects;
		stat_tree(dummy, dummy, eddobjects);

		f.printf("EDD_OBJECTS: %lu\n\n", (unsigned long)eddobjects);
	}

	/* detailed statistic */
	{
		tree_info_t info[EDD_TERMINAL_MAX];

		statistic(info);

		for (size_t i = 0; i < EDD_TERMINAL_MAX; ++i)
		{
			f.printf("%s: %lu\n",
				 tree::get_descr(i),
				 (unsigned long)info[i].count);

			for (size_t j = 0; j < sizeof(info[0].attr_count)/sizeof(info[0].attr_count[0]); ++j)
			{
				const char *descr = tree::get_attr_descr(i, j);

				if (!descr)
					break;

				f.printf("- %s: %lu\n",
					 descr,
					 (unsigned long)info[i].attr_count[j]);
			}

			f.put('\n');
		}
	}
}

void
Checker::statistic(tree_info_t *info) const
{
	bzero(info, sizeof(*info) * EDD_TERMINAL_MAX);

	statistic(root, info);
}

void
Checker::statistic(class tree *t, tree_info_t *infos) const
{
	while (t)
	{
		class node_tree *node;

		tree_info_t &info = infos[t->kind];
		info.count++;

		node = t->is_node();
		if (node)
		{
			for (size_t i = 0; i < node->attr_max(); ++i)
			{
				if (node->attr(i))
				{
					statistic(node->attr(i), infos);
					info.attr_count[i]++;
				}
			}

			if (!info.object)
				info.object = (t->is_edd_object() != NULL);
		}

		t = t->next;
	}
}

int
Checker::misc(void)
{
	if (root)
	{
		lib::fstream out(stdout, lib::fstream::dont_close);

		misc1(out, root, 0);
	}

	return 0;
}

void
Checker::misc1(lib::stream &f, class tree *t, unsigned long level)
{
	if (t)
	{
		while (t)
		{
			node_tree *node;
			unsigned long i;

			for (i = 0; i < level; i++)
				f.put("   ");

			f.printf("-- node type %i, line %lu",
				 t->kind, t->pos.line);

			node = t->is_node();
			if (node)
			{
				f.printf(", %s", node->descr());
				if (node->ident() != ident().NoIdent)
					f.printf(" [%s] ", node->name());
			}

			t->dump(f);

			if (node)
			{
				for (i = 0; i < node->attr_max(); i++)
				{
					if (node->attr(i))
					{
						unsigned long j;

						for (j = 0; j < level+1; j++)
							f.put("   ");

						f.printf("-- %s\n",
							 node->attr_descr(i));

						static const char *look[] =
						{
							"IDENTIFIER",
							"NUMBER",
							"REFERENCE",
							"CLASS",
							"DEFAULT_VALUE",
							"INITIAL_VALUE",
							"HELP",
							"HANDLING",
							"LABEL",
							"VALUE",
							"TRANSACTION",
							"DEFINITION",
							NULL
						};
						const char **s = look;

						while (*s)
						{
							if (!strcmp(*s, node->attr_descr(i)))
							{
								eval(f, node->attr(i), level+2);
								break;
							}

							s++;
						}

						if (!*s)
							misc1(f, node->attr(i), level+2);
					}
				}
			}

			t = t->next;
		}
	}
	else
	{
		unsigned long i;

		for (i = 0; i < level; i++)
			f.put("   ");

		f.put("-- NULL\n");
	}
}

void
Checker::dump_tree(void)
{
	lib::fstream out(stdout, lib::fstream::dont_close);

	edd_tree::dump_tree(out, root);
}

int
Checker::run_check(class Checker &checker,
		   const std::string &file,
		   std::vector<std::string> &includes,
		   std::vector<std::string> &defines,
		   std::vector<std::string> &dictionaries,
		   std::string &hcf_path,
		   std::string &pb_path,
		   bool quiet, bool warn, int err_limit,
		   profile_t profile, size_t options,
		   lib::stream &f)
{
	int i;

	std::string dir(lib::dirname(file));
	if (dir != ".")
		includes.insert(includes.begin(), dir);

	try
	{
		checker.set_hcf_path(hcf_path);
		checker.set_pb_path(pb_path);

		/* add HCF/PROFIBUS path to the include dirs */
		if (!hcf_path.empty())
			includes.push_back(hcf_path);
		if (!pb_path.empty())
			includes.push_back(pb_path), includes.push_back(pb_path + lib::pathseparator() + "0000");

		if (dictionaries.size() > 0)
		{
			bool flag = false;

			if (warn && (options & edd::edd_tree::PEDANTIC))
				flag = true;

			checker.load_dictionaries(dictionaries,
						  quiet, flag,
						  f);
		}

		checker.load_devices();

		i = checker.check_file(file,
				       includes, defines,
				       quiet, warn, err_limit,
				       profile, options,
				       f);
	}
	catch (const RuntimeError &xx)
	{
		f.put("RuntimeError:\n\"");
		f.put(checker.err().errmsg(checker.get_language(), xx).c_str());
		f.put("\"\n");

		i = 1;
	}

	return i;
}

int
Checker::process_Devices(class Checker &checker,
			 const std::string &file,
			 std::vector<std::string> &includes,
			 std::vector<std::string> &defines,
			 std::vector<std::string> &dictionaries,
			 std::string &hcf_path,
			 std::string &pb_path,
			 bool quiet, bool warn, int err_limit,
			 profile_t profile, size_t options,
			 lib::stream &f)
{
	std::string edd_file("(unknown)");
	std::string gsd_file("(unknown)");
	int i;

	i = parse_Devices(file,
			  includes,
			  dictionaries,
			  edd_file,
			  gsd_file,
			  &f);

	/* run the check only if there are no errors */
	if (i == 0)
	{
		std::string dir(lib::dirname(file));

		if (dir != ".")
			sys::chdir(dir);

		i = run_check(checker,
			      edd_file,
			      includes,
			      defines,
			      dictionaries,
			      hcf_path,
			      pb_path,
			      quiet, warn, err_limit,
			      profile, options,
			      f);
	}

	return i;
}

int
Checker::process_EDDY(class Checker &checker,
		      const std::string &file,
		      std::vector<std::string> &includes,
		      std::vector<std::string> &defines,
		      std::vector<std::string> &dictionaries,
		      std::string &hcf_path,
		      std::string &pb_path,
		      bool quiet, bool warn, int err_limit,
		      profile_t profile, size_t options,
		      lib::stream &out)
{
	std::string project_dir(lib::dirname(file));
	std::string project_file(lib::basename(file));

	/* change to the project dir */
	sys::chdir(project_dir);

	/* load the config file */
	lib::IniFile config(project_file);

	std::vector<std::string> data;

	/* append include dirs */
	data = config.get_section("Includes");
	includes.insert(includes.end(), data.begin(), data.end());

	/* append defines */
	data = config.get_section("Defines");
	for (size_t i = 0, i_max = data.size(); i < i_max; ++i)
	{
		std::string define = data[i];
		std::string key(config.get_key("Defines", define));

		if (!key.empty())
		{
			define += '=';
			define += key;
		}

		defines.push_back(define);
	}

	/* append dictionaries */
	data = config.get_section("Files");
	for (size_t i = 0, i_max = data.size(); i < i_max; ++i)
	{
		if (lib::check_extension(data[i], "dct"))
			dictionaries.push_back(data[i]);
	}

	/* if no HCF path is set get the one from the config */
	if (hcf_path.empty())
		hcf_path = config.get_key("HCF", "Path");

	/* add HCF path to the include dirs */
	if (!hcf_path.empty())
		includes.push_back(hcf_path);

	/* if no PROFIBUS path is set get the one from the config */
	if (pb_path.empty())
		pb_path = config.get_key("PROFIBUS", "Path");

	/* add PROFIBUS path to the include dirs */
	if (!pb_path.empty())
		includes.push_back(pb_path);

	/* lookup the primary edd file */
	std::string edd_file(config.get_key("Settings", "Primary"));

	/* get profile setting */
	{
		std::vector<profile_t> profiles(edd::edd_tree::allprofiles());
		std::string str(config.get_key("Settings", "Profile"));

		for (size_t i = 0; i < profiles.size(); i++)
		{
			if (str == profile_descr(profiles[i]))
			{
				profile = profiles[i];
				break;
			}
		}
	}

	/* add profile options */
	options |= config.get_int_key("Settings", "ProfileOptions");

	return run_check(checker,
			 edd_file,
			 includes,
			 defines,
			 dictionaries,
			 hcf_path,
			 pb_path,
			 quiet, warn, err_limit,
			 profile, options,
			 out);
}

} /* namespace edd */
