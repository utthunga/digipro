
/* 
 * $Id: edd_builtins_dispatch.cxx,v 1.8 2008/09/25 12:50:56 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_builtins_dispatch.h"

// C stdlib

// C++ stdlib
#include <memory>

// my lib
#include "BasicType.h"

// own header
#include "edd_operand_interface.h"

using namespace stackmachine;


namespace edd
{

edd_int08_opmap::edd_int08_opmap(operand_ptr o) : op(o) { }

     edd_int08_opmap::operator edd_int08() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_int08)(*data));
}
void edd_int08_opmap::operator=(edd_int08 d)
{
	const stackmachine::int8Type data(d);
	(*op) = data;
}


edd_int16_opmap::edd_int16_opmap(operand_ptr o) : op(o) { }

     edd_int16_opmap::operator edd_int16() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_int16)(*data));
}
void edd_int16_opmap::operator=(edd_int16 d)
{
	const stackmachine::int16Type data(d);
	(*op) = data;
}


edd_int32_opmap::edd_int32_opmap(operand_ptr o) : op(o) { }

     edd_int32_opmap::operator edd_int32() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_int32)(*data));
}
void edd_int32_opmap::operator=(edd_int32 d)
{
	const stackmachine::int32Type data(d);
	(*op) = data;
}


edd_uint08_opmap::edd_uint08_opmap(operand_ptr o) : op(o) { }

     edd_uint08_opmap::operator edd_uint08() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_uint08)(*data));
}
void edd_uint08_opmap::operator=(edd_uint08 d)
{
	const stackmachine::uint8Type data(d);
	(*op) = data;
}


edd_uint16_opmap::edd_uint16_opmap(operand_ptr o) : op(o) { }

     edd_uint16_opmap::operator edd_uint16() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_uint16)(*data));
}
void edd_uint16_opmap::operator=(edd_uint16 d)
{
	const stackmachine::uint16Type data(d);
	(*op) = data;
}


edd_uint32_opmap::edd_uint32_opmap(operand_ptr o) : op(o) { }

     edd_uint32_opmap::operator edd_uint32() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_uint32)(*data));
}
void edd_uint32_opmap::operator=(edd_uint32 d)
{
	const stackmachine::uint32Type data(d);
	(*op) = data;
}


edd_real32_opmap::edd_real32_opmap(operand_ptr o) : op(o) { }

     edd_real32_opmap::operator edd_real32() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_real32)(*data));
}
void edd_real32_opmap::operator=(edd_real32 d)
{
	const stackmachine::float32Type data(d);
	(*op) = data;
}


edd_real64_opmap::edd_real64_opmap(operand_ptr o) : op(o) { }

     edd_real64_opmap::operator edd_real64() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_real64)(*data));
}
void edd_real64_opmap::operator=(edd_real64 d)
{
	const stackmachine::float64Type data(d);
	(*op) = data;
}


edd_string_opmap::edd_string_opmap(operand_ptr o) : op(o) { }

     edd_string_opmap::operator edd_string() const
{
	std::auto_ptr<stackmachine::Type> data(*op);
	return ((edd_string)(*data));
}
void edd_string_opmap::operator=(const edd_string &d)
{
	const stackmachine::stringType data(d);
	(*op) = data;
}


edd_int32_array::edd_int32_array(operand_ptr o) : op(o) { }

edd_int32 edd_int32_array::operator[](int index)
{
	operand_ptr element(op->operator[](index));
	std::auto_ptr<stackmachine::Type> data(*element);
	return (edd_int32)(*data);
}
void edd_int32_array::assign(int index, edd_int32 d)
{
	operand_ptr element(op->operator[](index));
	const stackmachine::int32Type data(d);
	(*element) = data;
}

edd_uint32_array::edd_uint32_array(operand_ptr o) : op(o) { }

edd_uint32 edd_uint32_array::operator[](int index)
{
	operand_ptr element(op->operator[](index));
	std::auto_ptr<stackmachine::Type> data(*element);
	return (edd_uint32)(*data);
}
void edd_uint32_array::assign(int index, edd_uint32 d)
{
	operand_ptr element(op->operator[](index));
	const stackmachine::uint32Type data(d);
	(*element) = data;
}

} /* namespace edd */
