
/* 
 * $Id: device_io_interface.h,v 1.9 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the communication interface.
 */

#ifndef _device_io_interface_h
#define _device_io_interface_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "cocktail_types.h"
#include "edd_operand_interface.h"


namespace edd
{

/**
 * I/O abstraction.
 * This is the communication I/O abstraction used by the EDD library.
 */
class device_io_interface
{
public:
	virtual ~device_io_interface(void) { }

	virtual bool is_online(void) const = 0;

	virtual int read(class method_env *, class EDD_OBJECT_tree *) = 0;
	virtual int write(class method_env *, class EDD_OBJECT_tree *) = 0;

	enum action { action_read, action_write, action_command };

	virtual int do_command(class method_env *,
			       const class COMMAND_tree *, int trans_nr, enum action,
			       int &warnings) = 0;

	void begin_transfer_sequence(class method_env *);
	void end_transfer_sequence(class method_env *);
};

} /* namespace edd */

#endif /* _device_io_interface_h */
