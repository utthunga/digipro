
/* 
 * $Id: NullLicense.h,v 1.4 2009/05/19 12:35:33 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _NullLicense_h
#define _NullLicense_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "License.h"


class NullLicense : public License
{
public:
	virtual bool have_checker_license();
	virtual bool have_workbench_license();
	virtual bool have_comm_license(int profile);
	virtual std::string get_error_msg() const;
	virtual long get_error_code() const;
	virtual std::string get_expiration_info() const;
};

#endif /* _NullLicense_h */
