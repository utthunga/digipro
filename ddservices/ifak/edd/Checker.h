
/* 
 * $Id: Checker.h,v 1.14 2008/03/16 16:45:10 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the command line EDD checker.
 */

#ifndef _Checker_h
#define _Checker_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
# pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <vector>
#include <string>

// my lib

// own header
#include "pbInterpreter.h"


namespace edd
{

/**
 * Simple command line parser.
 * This is the EDD library specialization for the command line checker. It
 * features an enhanced parser method with some statistic counting, flushing
 * error output and such things.
 */
class Checker : public Interpreter
{
public:
	int load_dictionaries(const std::vector<std::string> &dictionaries,
			      bool quiet, bool warnings,
			      lib::stream &);

	int check_file(const std::string &file,
		       const std::vector<std::string> &includes,
		       const std::vector<std::string> &defines,
		       bool quiet, bool warnings, int err_limit,
		       profile_t profile, size_t options,
		       lib::stream &f);

	/** \name Debugging routines. */
	//! \{
	void		dump_statistic(lib::stream &f);
	void		dump_tree(void);

	int		misc(void);
	void		misc1(lib::stream &f, class tree *t, unsigned long level);
	//! \}

protected:
	struct tree_info
	{
		size_t count;
		size_t attr_count[32];
		bool object;
	};
	typedef struct tree_info tree_info_t;

	void statistic(tree_info_t *) const;
	void statistic(class tree *t, tree_info_t *) const;

public:
	static int run_check(class Checker &checker,
			     const std::string &file,
			     std::vector<std::string> &includes,
			     std::vector<std::string> &defines,
			     std::vector<std::string> &dictionaries,
			     std::string &hcf_path,
			     std::string &pb_path,
			     bool quiet, bool warn, int err_limit,
			     profile_t profile, size_t options,
			     lib::stream &f);

	static int process_Devices(class Checker &checker,
				   const std::string &file,
				   std::vector<std::string> &includes,
				   std::vector<std::string> &defines,
				   std::vector<std::string> &dictionaries,
				   std::string &hcf_path,
				   std::string &pb_path,
				   bool quiet, bool warn, int err_limit,
				   profile_t profile, size_t options,
				   lib::stream &f);

	static int process_EDDY(class Checker &checker,
				const std::string &file,
				std::vector<std::string> &includes,
				std::vector<std::string> &defines,
				std::vector<std::string> &dictionaries,
				std::string &hcf_path,
				std::string &pb_path,
				bool quiet, bool warn, int err_limit,
				profile_t profile, size_t options,
				lib::stream &f);
};

} /* namespace edd */

#endif /* _Checker_h */
