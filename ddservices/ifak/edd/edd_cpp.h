
/* 
 * $Id: edd_cpp.h,v 1.12 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions for the edd specific preprocessor.
 */

#ifndef _edd_cpp_h
#define _edd_cpp_h

// C stdlib

// C++ stdlib
#include <set>
#include <string>
#include <vector>

// my lib
#include "ident.h"
#include "cpp.h"

// own header


namespace edd
{

class edd_cpp : protected ucpp::cpp
{
public:
	edd_cpp(lib::IDENT *, class edd_err *, bool);

	FILE *run(FILE *file,
		  const char *filename,
		  const std::vector<std::string> &includes,
		  const std::vector<std::string> &defines,
		  std::set<std::string> &included_files);

protected:
	void fatal(const char *, ...) CHECK_PRINTF(2, 3);
	void error(long, const char *, ...) CHECK_PRINTF(3, 4);
	void warning(long, const char *, ...) CHECK_PRINTF(3, 4);

	lib::IDENT * const ident;
	class edd_err * const err;
	const bool pdm_mode;
};

} /* namespace edd */

#endif /* _edd_cpp_h */
