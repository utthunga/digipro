
/* 
 * $Id: device_io_pn.cxx,v 1.6 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io_pn.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// other libs

// my lib
#include "sys.h"

// own header
#include "edd_ast.h"
#include "pbInterpreter.h"

#ifndef _
# define _(x) x
#endif


namespace edd
{

device_io_pn::device_io_pn(class Interpreter &parent)
: device_io(parent)
, required_timeout(0)
{
}

int
device_io_pn::do_command(class method_env *env,
			 const class COMMAND_tree *cmd, int trans_nr, enum action action,
			 int &warnings)
{
	assert(cmd);

	if (!is_online())
	{
		error("Not online!");
		return -1;
	}

	int ret = 0;

	switch (action)
	{
		case action_read:
		{
			ret = do_read_command(cmd, trans_nr, env, warnings);
			break;
		}
		case action_write:
		{
			ret = do_write_command(cmd, trans_nr, env, warnings);
			break;
		}
		default:
		{
			switch (cmd->operation(env))
			{
				case EDD_COMMAND_OPERATION_READ:
					ret = do_read_command(cmd, trans_nr, env, warnings);
					break;
				case EDD_COMMAND_OPERATION_WRITE:
					ret = do_write_command(cmd, trans_nr, env, warnings);
					break;
				default:
					assert(0);
			}
			break;
		}
	}

	return ret;
}

void
device_io_pn::dump_data(const class COMMAND_tree *cmd, const char *op,
			const unsigned char *buf, size_t buflen, const char *descr)
{
	lib::dyn_buf tmp(100);

	if (cmd && op)
		tmp.printf("COMMAND '%s' (%s): %s (hex): ", cmd->name(), op, descr);
	else
		tmp.printf("%s (hex): ", descr);

	for (size_t i = 0; i < buflen; i++)
		tmp.printf("%02x ", buf[i]);

	trace(tmp.c_str());
}

void 
device_io_pn::do_timeout(const class COMMAND_tree *cmd, const char *op,
			 long new_timeout)
{
	if (required_timeout > 0)
	{
		os_service::Timeval tmp(timestamp);
		os_service::Timeval waittime;

		timestamp.set_to_now();
		timestamp -= tmp;

		waittime.set(required_timeout / 1000, (required_timeout % 1000) * 1000);

		if (timestamp < waittime)
		{
			sys::uint32 ms;

			waittime -= timestamp;

			ms = waittime.to_seconds();
			ms *= 1000;
			ms += waittime.get_useconds() / 1000;

			trace(_("COMMAND '%s' (%s): Handling timeout, waiting %" PRIu32 " ms!"),
				cmd->name(), op, ms);

			if (waittime.to_seconds())
				sys::sleep(waittime.to_seconds());

			if (waittime.get_useconds())
				sys::usleep(waittime.get_useconds());
		}
	}

	required_timeout = new_timeout;
}

int
device_io_pn::do_read_command(const class COMMAND_tree *cmd, int trans_nr,
			      class method_env *env, int &warnings)
{
	struct request req;

	if (cmd->operation(env) != EDD_COMMAND_OPERATION_READ)
	{
		error(_("COMMAND '%s' is not of OPERATION READ, can't execute it as read COMMAND!"),
			cmd->name());
		return -1;
	}

	if (!resolve_cmd(cmd, "read", env, req))
	{
		error(_("COMMAND '%s' (read): resolve_cmd failed, aborted"), cmd->name());
		return -1;
	}

	std::vector<class DATA_ITEM_tree *> items;

	try
	{
		items = cmd->reply_items(trans_nr, env);
	}
	catch (const RuntimeError &e)
	{
		error(_("COMMAND '%s' (read): RuntimeError while obtaining reply items, aborted"),
		      cmd->name());

		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (read): RuntimeError is: %s"), cmd->name(), msg.c_str());

		return -1;
	}

	if (can_optimize(env, items))
	{
		/* all variables are newer than the last age stamp */

		info(_("COMMAND '%s' (read) not executed, all items are up-to-date"), cmd->name());
		return 0;
	}

	unsigned char data[256];
	size_t expect, dynamic_expect;
	int32 timeout;

	if (read_encode(cmd, trans_nr, env, parent.logger, items,
			expect, dynamic_expect, timeout, warnings))
	{
		return -1;
	}

	if (expect+dynamic_expect > 240)
	{
		error(_("COMMAND '%s' (read): size of variables to large (%lu > 240)!"),
		      cmd->name(), (unsigned long)expect);

		return -1;
	}

	do_timeout(cmd, "read", timeout);

	if (dynamic_expect)
	{
		trace(_("COMMAND '%s' (read): slot %u, subslot %u, index 0x%x, len 240 [variable-size: %lu + n * %lu]"),
			cmd->name(), req.slot, req.subslot, req.index,
			(unsigned long)expect, (unsigned long)dynamic_expect);
	}
	else
		trace(_("COMMAND '%s' (read): slot %u, subslot %u, index 0x%x, len %lu"),
			cmd->name(), req.slot, req.subslot, req.index,
			(unsigned long)expect);

	req.datalen = dynamic_expect ? 240 : expect;
	req.data = data;

	long rc = pn_read(req);

	if (required_timeout > 0)
		/* timeout required */
		timestamp.set_to_now();

	if (rc == 0)
	{
		dump_data(cmd, "read", req.data, req.datalen, _("Received"));

		if (dynamic_expect)
		{
			if (req.datalen < expect)
			{
				error(_("COMMAND '%s' (read) failed, expected %lu + n * %lu bytes, got %lu"),
					cmd->name(),
					(unsigned long)expect, (unsigned long)dynamic_expect,
					req.datalen);

				rc = -1;
			}
			else if ((req.datalen - expect) % dynamic_expect)
			{
				error(_("COMMAND '%s' (read) failed, expected %lu + n * %lu bytes, got %lu [not aligned]"),
					cmd->name(),
					(unsigned long)expect, (unsigned long)dynamic_expect,
					req.datalen);

				rc = -1;
			}
		}
		else if (expect != req.datalen)
		{
			error(_("COMMAND '%s' (read) failed, expected %lu bytes, got %lu"),
				cmd->name(),
				(unsigned long)expect,
				req.datalen);

			rc = -1;
		}

		if (rc == 0)
		{
			if (read_decode(cmd, trans_nr, env, parent.logger, items,
					req.data, req.datalen))
			{
				rc = -1;
			}
		}
	}
	else
		error(_("COMMAND '%s' (read) failed (%li)"), cmd->name(), rc);

	return rc;
}

int
device_io_pn::do_write_command(const class COMMAND_tree *cmd, int trans_nr,
			       class method_env *env, int &warnings)
{
	struct request req;

	if (cmd->operation(env) != EDD_COMMAND_OPERATION_WRITE)
	{
		error(_("COMMAND '%s' is not of OPERATION WRITE, can't execute it as write COMMAND!"),
			cmd->name());
		return -1;
	}

	if (!resolve_cmd(cmd, "write", env, req))
	{
		error(_("COMMAND '%s' (write): resolve_cmd failed, aborted"), cmd->name());
		return -1;
	}

	std::vector<class DATA_ITEM_tree *> items;

	try
	{
		items = cmd->request_items(trans_nr, env);
	}
	catch (const RuntimeError &e)
	{
		error(_("COMMAND '%s' (write): RuntimeError while obtaining request items, aborted"),
		      cmd->name());

		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (write): RuntimeError is: %s"), cmd->name(), msg.c_str());

		return -1;
	}

	if (can_optimize(env, items))
	{
		/* all variables are newer than the last age stamp */

		info(_("COMMAND '%s' (write) not executed, all items are up-to-date"), cmd->name());
		return 0;
	}

	optimize_age_stamp_tick(env);

	unsigned char data[256];
	size_t datalen;
	int32 timeout;

	if (write_encode(cmd, trans_nr, env, parent.logger, items, data, sizeof(data), datalen, timeout))
		return -1;

	do_timeout(cmd, "write", timeout);

	trace(_("COMMAND '%s' (write): slot %i, subslot %u, index 0x%x, len %lu"),
		cmd->name(), req.slot, req.subslot, req.index, (unsigned long)datalen);

	dump_data(cmd, "write", data, datalen, _("Send"));

	req.datalen = datalen;
	req.data = data;

	long rc = pn_write(req);

	if (required_timeout > 0)
		/* timeout required */
		timestamp.set_to_now();

	if (rc != 0)
		error(_("COMMAND '%s' (write) failed (%li)"), cmd->name(), rc);

	write_decode(cmd, trans_nr, env, parent.logger, items, rc);

	return rc;
}

bool
device_io_pn::resolve_cmd(const class COMMAND_tree *cmd, const char *op,
			  class method_env *env, struct request &req)
{
	bool ret = false;

	if (cmd) try
	{
		long slot = cmd->slot(env);
		long subslot = cmd->subslot(env);
		long index = cmd->index(env);

		if (slot != -1 && subslot != -1 && index != -1)
		{
			assert(slot >= 0 && slot <= 0x7fff);
			assert(subslot >= 1 && subslot <= 0x7fff);
			assert(index >= 0 && index <= 0xffff);

			req.slot = (unsigned short)slot;
			req.subslot = (unsigned short)subslot;
			req.index = (unsigned short)index;

			ret = true;
		}
	}
	catch (const RuntimeError &e)
	{
		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (%s): %s"), cmd->name(), op, msg.c_str());
	}

	return ret;
}

} /* namespace edd */
