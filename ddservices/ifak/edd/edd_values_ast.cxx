
/* 
 * $Id: edd_values_ast.cxx,v 1.16 2010/02/03 14:26:26 mmeier Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_values_ast.h"

// C stdlib

// C++ stdlib

// my lib
#include "mstream.h"
#include "port.h"
#include "string.h"
#include "BasicType.h"

// own
#include "edd_ast.h"
#include "edd_sm_extensions.h"
#include "edd_values.h"

using namespace stackmachine;


namespace edd
{

// helper macros
#define CAST_OPERATOR(classname, type) \
	classname::operator type() const { return CAST_OPERATOR_ACCESS(type); }

#define VALUE_CAST_OPERATORS(classname) \
	CAST_OPERATOR(classname, bool) \
	CAST_OPERATOR(classname, int8) \
	CAST_OPERATOR(classname, int16) \
	CAST_OPERATOR(classname, int32) \
	CAST_OPERATOR(classname, int64) \
	CAST_OPERATOR(classname, uint8) \
	CAST_OPERATOR(classname, uint16) \
	CAST_OPERATOR(classname, uint32) \
	CAST_OPERATOR(classname, uint64) \
	CAST_OPERATOR(classname, float32) \
	CAST_OPERATOR(classname, float64)


operand_ptr no_value::clone(void) const { return new no_value; }

void no_value::dump0(lib::stream &f) const
{
	f.printf("$$ no value");
}

std::string
no_value::print(const char *fmt) const
{
	return std::string("not implemented");
}


operand_ptr identifier_value::clone(void) const { return new identifier_value(myid); }

void
identifier_value::dump0(lib::stream &f) const
{
	f.printf("$$ identifier { %s }", myid.c_str());
}

         identifier_value::operator lib::IDENTIFIER() const { return myid; }


tree_value::tree_value(class node_tree *t)
: mytree(t)
{
	assert(mytree);
}

operand_ptr
tree_value::clone(void) const
{
	return new tree_value(mytree);
}

void
tree_value::dump0(lib::stream &f) const
{
	f.put("$$ tree_value ");
	f.put(mytree->name());
}

         tree_value::operator node_tree *() const { return mytree; }
         tree_value::operator lib::IDENTIFIER() const { return mytree->ident(); }


edd_object_value::edd_object_value(class EDD_OBJECT_tree *t)
: tree_value(t)
{
}

operand_ptr
edd_object_value::clone(void) const
{
	return new edd_object_value((class EDD_OBJECT_tree *)(mytree));
}

void
edd_object_value::env_assert(class method_env *check) const
{
	assert(env == check);

	if (env != check)
		throw RuntimeError(xxInternal,
				   mytree->pos,
				   mytree->__parent->ident().create("edd_object_value::env_assert"));
}

void
edd_object_value::dump0(lib::stream &f) const
{
	f.put("$$ edd_object_value ");
	f.put(mytree->name());
}

edd_object_value::operator stackmachine::Type *() const
{
	operand *unconstThis = const_cast<edd_object_value *>(this);
	return new m_eddoperand(unconstThis);
}

void
edd_object_value::operator= (const stackmachine::Type &v)
{
	const class m_eddoperand *addr;
	operand_ptr op;

	addr = dynamic_cast<const class m_eddoperand *>(&v);
	assert(addr);

	op = addr->get();

	operand *me = this;
	me->operator=(*op);
}

edd_object_value::operator EDD_OBJECT_tree *() const
{
	return (class EDD_OBJECT_tree *)(mytree);
}

operand_ptr
edd_object_value::member(const lib::IDENTIFIER &id) const
{
	return mytree->selector_eval(env, id);
}


value_adt::value_adt(class EDD_OBJECT_tree *t)
: edd_object_value(t)
{
}

operand_ptr
value_adt::clone(void) const
{
	return value_adt_clone();
}

bool
value_adt::operator==(const operand &op) const
{
	return !(this->operator!=(op));
}

bool
value_adt::operator!=(const operand &op) const
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	const class value_adt *list = dynamic_cast<const class value_adt *>(&op);
	assert(list);

	lib::mstream values_self, values_list;

	save_value(values_self);
	list->save_value(values_list);

	return (values_self.get_buf() != values_list.get_buf());
}

void
value_adt::expect(const lib::stream &f, char c)
{
	int i = f.get();

	if (i == EOF || i != c)
		throw RuntimeError(xxIllegalChar);
}

void
value_adt::expect(const lib::stream &f, const char *str)
{
	char buf[128];
	size_t len = strlen(str);

	assert(len < sizeof(buf));

	if (len != f.read(buf, 1, len) || strncmp(str, buf, len) != 0)
		throw RuntimeError(xxIllegalChar);
}

void
value_adt::expect_long(const lib::stream &f, long val)
{
	char str[64];
	snprintf(str, sizeof(str), "{%li}", val);

	expect(f, str);
}

std::string
value_adt::read_string(const lib::stream &f)
{
	std::string str;

	expect(f, '"');
	str.reserve(100);

	bool quote = false;

	for (;;)
	{
		char c = f.get();

		if (c == EOF)
			throw RuntimeError(xxSyntaxError);

		if (c == '\\')
		{
			if (quote)
			{
				str += c;
				quote = false;
			}
			else
				quote = true;
		}
		else if (c == '"')
		{
			if (quote)
			{
				str += c;
				quote = false;
			}
			else
				break;
		}
		else
		{
			if (quote)
			{
				str += '\\';
				quote = false;
			}
			str += c;
		}
	}

	return str;
}

long
value_adt::read_long(const lib::stream &f)
{
	std::string str;
	int c;

	expect(f, '{');
	str.reserve(16);

	c = f.get();
	while (c != '}')
	{
		if (c == EOF)
			throw RuntimeError(xxSyntaxError);

		str += c;
		c = f.get();
	}

	return strtol(str.c_str(), NULL, 0);
}

void
value_adt::write_string(lib::stream &f, const std::string &str)
{
	f.put('"');

	for (size_t i = 0, i_max = str.size(); i < i_max; i++)
	{
		char c = str[i];

		if (c == '\\' || c == '"')
			f.put('\\');

		f.put(c);
	}

	f.put('"');
}

void
value_adt::write_long(lib::stream &f, long val)
{
	f.printf("{%li}", val);
}


struct_value_adt::struct_value_adt(class EDD_OBJECT_tree *t, class tree *members)
: value_adt(t)
{
	elements.reserve(t->tree_count(members));

	while (members)
	{
		assert(members->kind == EDD_MEMBER);

		element_t element;

		element.member = (class MEMBER_tree *) members;
		element.value = element.member->value_adt();

		elements.push_back(element);

		members = members->next;
	}
}

struct_value_adt::struct_value_adt(class EDD_OBJECT_tree *t, const elements_t &e)
: value_adt(t)
, elements(e)
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		element_t &element = elements[i];

		element.value = element.value->value_adt_clone();
	}
}

void
struct_value_adt::setenv0(class method_env *env) throw()
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
		elements[i].value->setenv(env);
}

void
struct_value_adt::dump0(lib::stream &f) const
{
	f.put(mytree->name());
	f.put(" { ");
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		f.put(element.member->name());
		f.put(" { ");
		element.value->dump0(f);
		f.put(" } ");
	}
	f.put(" }");
}

void
struct_value_adt::eval_values(std::vector<operand_ptr> &ops)
{
	ops.reserve(ops.size() + elements.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
		elements[i].value->eval_values(ops);
}

void
struct_value_adt::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		operand_ptr val(op.member(element.member->ident()));
		operand_ptr value(element.value);

		*value = *val;
	}
}

operand_ptr
struct_value_adt::member(const lib::IDENTIFIER &id) const
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		if (element.member->ident() == id)
			return element.value;
	}

	return mytree->selector_eval(env, id);
}

size_t
struct_value_adt::size(void) const
{
	size_t size = 0;

	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		size += element.value->size();
	}

	return size;
}

void
struct_value_adt::save_type(lib::stream &f) const
{
	f.put(mytree->descr());
	f.put('{');
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		f.put(element.member->name());
		f.put('{');
		element.value->save_type(f);
		f.put('}');
	}
	f.put('}');
}

void
struct_value_adt::check_type(const lib::stream &f) const
{
	expect(f, mytree->descr());
	expect(f, '{');
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		expect(f, element.member->name());
		expect(f, '{');
		element.value->check_type(f);
		expect(f, '}');
	}
	expect(f, '}');
}

void
struct_value_adt::save_value(lib::stream &f) const
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		const element_t &element = elements[i];

		f.put(element.member->name());
		f.put('{');
		element.value->save_value(f);
		f.put('}');
	}
}

void
struct_value_adt::load_value(const lib::stream &f)
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		element_t &element = elements[i];

		expect(f, element.member->name());
		expect(f, '{');
		element.value->load_value(f);
		expect(f, '}');
	}
}


collection_value_adt::collection_value_adt(class COLLECTION_tree *t, class tree *members)
: struct_value_adt(t, members)
{
}

collection_value_adt::collection_value_adt(class COLLECTION_tree *t, const elements_t &e)
: struct_value_adt(t, e)
{
}

value_adt_ptr
collection_value_adt::value_adt_clone(void) const
{
	return new collection_value_adt((class COLLECTION_tree *)mytree, elements);
}

void
collection_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ collection_value_adt ");
	struct_value_adt::dump0(f);
}


collection_value::collection_value(class COLLECTION_tree *t)
: edd_object_value(t)
{
}

operand_ptr
collection_value::clone(void) const
{
	return new collection_value((class COLLECTION_tree *)mytree);
}

void
collection_value::dump0(lib::stream &f) const
{
	f.put("$$ collection_value ");
	f.put(mytree->name());
}

void
collection_value::eval_values(std::vector<operand_ptr> &ops)
{
	std::vector<MEMBER_tree *> members(((class COLLECTION_tree *) mytree)->members(env));

	ops.reserve(ops.size() + members.size());

	for (size_t i = 0, i_max = members.size(); i < i_max; i++)
		members[i]->value(env)->eval_values(ops);
}

void
collection_value::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	std::vector<MEMBER_tree *> members(((class COLLECTION_tree *) mytree)->members(env));

	for (size_t i = 0, i_max = members.size(); i < i_max; i++)
	{
		operand_ptr val(op.member(members[i]->ident()));
		operand_ptr value(members[i]->value(env));

		*value = *val;
	}
}


list_value_adt::list_value_adt(class LIST_tree *t, const element_t de,
			       long capacity, long count, bool fixed)
: value_adt(t)
, capacity_(capacity)
, fixed_(fixed)
, def_element(de)
{
	assert(def_element);

	if (capacity_ > 0)
		elements.reserve(capacity_);

	if (count > 0)
		elements.resize(count);
}

list_value_adt::list_value_adt(class LIST_tree *t, const element_t de,
			       long capacity, bool fixed, const elements_t &e)
: value_adt(t)
, capacity_(capacity)
, fixed_(fixed)
, def_element(de)
{
	assert(def_element);

	if (capacity_ > 0)
		elements.reserve(capacity_);

	elements.resize(e.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		if (e[i])
			elements[i] = e[i]->value_adt_clone();
	}
}

value_adt_ptr
list_value_adt::value_adt_clone(void) const
{
	return new list_value_adt((class LIST_tree *)mytree, def_element,
				  capacity_, fixed_, elements);
}

void
list_value_adt::setenv0(class method_env *env) throw()
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; ++i)
	{
		if (elements[i])
			elements[i]->setenv(env);
	}
}

void
list_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ list_value_adt ");
	f.put(mytree->name());
	f.put(" { ");

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		f.printf(" [%lu] ", (unsigned long)i);

		if (elements[i])
			elements[i]->dump(f);
		else
			def_element->dump(f);
	}

	f.put(" }");
}

void
list_value_adt::eval_values(std::vector<operand_ptr> &ops)
{
	ops.reserve(ops.size() + elements.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
		this->operator[](i)->eval_values(ops);
}

void
list_value_adt::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	const list_value_adt *list = dynamic_cast<const list_value_adt *>(&op);
	assert(list);

	elements.clear();
	elements.reserve(list->capacity());
	elements.resize(list->count());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		elements[i] = def_element->value_adt_clone();

		operand_ptr val(op[long(i)]);
		operand_ptr value(elements[i]);

		*value = *val;
	}
}

operand_ptr
list_value_adt::operator[] (long index) const
{
	if (index < 0 || (unsigned long)index >= elements.size())
		runtime_error_index(index);

	if (!elements[index])
	{
		elements[index] = def_element->value_adt_clone();
		elements[index]->setenv(env);
	}

	return elements[index];
}

operand_ptr
list_value_adt::member(const lib::IDENTIFIER &id) const
{
	return ((class LIST_tree *)mytree)->selector_eval_list(env, id, *this);
}

size_t
list_value_adt::size(void) const
{
	size_t size = def_element->size();

	return size * elements.size();
}

void
list_value_adt::save_type(lib::stream &f) const
{
	f.put(mytree->descr());
	f.put('{');
	def_element->save_type(f);
	f.put('}');
}

void
list_value_adt::check_type(const lib::stream &f) const
{
	expect(f, mytree->descr());
	expect(f, '{');
	def_element->check_type(f);
	expect(f, '}');
}

void
list_value_adt::save_value(lib::stream &f) const
{
	f.put("CAPACITY");
	write_long(f, capacity());

	f.put("COUNT");
	write_long(f, count());

	f.put("DEFAULT{");
	def_element->save_value(f);
	f.put('}');

	size_t entries = 0;

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (elements[i])
			entries++;
	}

	f.put("ENTRIES");
	write_long(f, entries);

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (elements[i])
		{
			write_long(f, i);
			f.put('{');
			elements[i]->save_value(f);
			f.put('}');
		}
	}
}

void
list_value_adt::load_value(const lib::stream &f)
{
	elements.clear();

	expect(f, "CAPACITY");
	long capacity_val = read_long(f);
	if (capacity_val > 0)
		elements.reserve(capacity_val);

	expect(f, "COUNT");
	long count_val = read_long(f);
	if (count_val > 0)
		elements.resize(count_val);

	expect(f, "DEFAULT");
	expect(f, '{');
	def_element->value_adt_clone()->load_value(f); // XXX
	expect(f, '}');

	expect(f, "ENTRIES");
	size_t entries = read_long(f);

	for (size_t i = 0; i < entries; i++)
	{
		long index = read_long(f);

		if (index < 0 || size_t(index) >= elements.size())
			runtime_error_index(index);

		expect(f, '{');
		elements[index] = def_element->value_adt_clone();
		elements[index]->setenv(env);
		elements[index]->load_value(f);
		expect(f, '}');
	}
}

size_t
list_value_adt::capacity(void) const
{
	if (!capacity_)
		return elements.capacity();

	return capacity_;
}

size_t
list_value_adt::count(void) const
{
	return elements.size();
}

operand_ptr
list_value_adt::first(void) const
{
	operand_ptr ret;

	if (elements.size())
	{
		if (!elements.front())
		{
			elements.front() = def_element->value_adt_clone();
			elements.front()->setenv(env);
		}

		ret = elements.front();
	}

	return ret;
}

operand_ptr
list_value_adt::last(void) const
{
	operand_ptr ret;

	if (elements.size())
	{
		if (!elements.back())
		{
			elements.back() = def_element->value_adt_clone();
			elements.back()->setenv(env);
		}

		ret = elements.back();
	}

	return ret;
}

void
list_value_adt::delete_at(size_t index)
{
	if (fixed_)
		throw RuntimeError(xxFixedList, mytree->pos, mytree->ident());

	if (index < elements.size())
	{
		elements.erase(elements.begin() + index);
		((class LIST_tree *)mytree)->update_count(env, elements.size());
	}
	else
		runtime_error_index(index);
}

void
list_value_adt::insert_at(size_t index, operand_ptr val)
{
	if (fixed_)
		throw RuntimeError(xxFixedList, mytree->pos, mytree->ident());

	if (capacity_ && elements.size() >= capacity_)
		throw RuntimeError(xxInsertExceedsCapacity, mytree->pos, mytree->ident());

	if (index <= elements.size())
	{
		elements_t::iterator iter(elements.insert(elements.begin() + index,
					  def_element->value_adt_clone()));

		operand_ptr op(*iter);
		op->setenv(env);
		*op = *val;

		((class LIST_tree *)mytree)->update_count(env, elements.size());
	}
	else
		runtime_error_index(index);
}

void
list_value_adt::runtime_error_index(long index) const
{
	char buf[32];
	snprintf(buf, sizeof(buf), "%li", index);

	throw RuntimeError(xxArrayIndex,
			   mytree->pos,
			   mytree->__parent->ident().create(buf));
}


method_value::method_value(class METHOD_BASE_tree *t)
: edd_object_value(t)
{
}

operand_ptr
method_value::clone(void) const
{
	return new method_value((class METHOD_BASE_tree *)mytree);
}

void
method_value::dump0(lib::stream &f) const
{
	f.put("$$ method_value ");
	f.put(mytree->name());
}

operand_ptr
method_value::operator() (operand_ptr args) const
{
	class METHOD_BASE_tree *parent = (class METHOD_BASE_tree *)mytree;

	// XXX I think lib::NoPosition should be replaced by
	//     an argument and the caller shall provide the calling position 
	return parent->execute(env, args, lib::NoPosition);
}


record_value_adt::record_value_adt(class RECORD_tree *t, class tree *members)
: struct_value_adt(t, members)
{
}

record_value_adt::record_value_adt(class RECORD_tree *t, const elements_t &e)
: struct_value_adt(t, e)
{
}

value_adt_ptr
record_value_adt::value_adt_clone(void) const
{
	return new record_value_adt((class RECORD_tree *)mytree, elements);
}

void
record_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ record_value_adt ");
	struct_value_adt::dump0(f);
}


record_value::record_value(class RECORD_tree *t)
: edd_object_value(t)
{
}

operand_ptr
record_value::clone(void) const
{
	return new record_value((class RECORD_tree *)mytree);
}

void
record_value::dump0(lib::stream &f) const
{
	f.put("$$ record_value ");
	f.put(mytree->name());
}

void
record_value::eval_values(std::vector<operand_ptr> &ops)
{
	std::vector<MEMBER_tree *> members(((class RECORD_tree *) mytree)->members(env));

	ops.reserve(ops.size() + members.size());

	for (size_t i = 0, i_max = members.size(); i < i_max; i++)
		members[i]->value(env)->eval_values(ops);
}

void
record_value::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	std::vector<MEMBER_tree *> members(((class RECORD_tree *) mytree)->members(env));

	for (size_t i = 0, i_max = members.size(); i < i_max; i++)
	{
		operand_ptr val(op.member(members[i]->ident()));
		operand_ptr value(members[i]->value(env));

		*value = *val;
	}
}

component_value::component_value(class COMPONENT_tree *t)
: edd_object_value(t)
{
}

operand_ptr
component_value::clone(void) const
{
	return new component_value((class COMPONENT_tree *)mytree);
}

void
component_value::dump0(lib::stream &f) const
{
	f.put("$$ component_value ");
	f.put(mytree->name());
}

operand_ptr
component_value::operator[] (long index) const
{

	class tree *t = ((class COMPONENT_tree *)mytree)->get_module(index);

	return t->value(env);
}

component_relation_value::component_relation_value(class COMPONENT_RELATION_tree *t)
: edd_object_value(t)
{
}

operand_ptr
component_relation_value::clone(void) const
{
	return new component_relation_value((class COMPONENT_RELATION_tree *)mytree);
}

void
component_relation_value::dump0(lib::stream &f) const
{
	f.put("$$ component_relation_value ");
	f.put(mytree->name());
}

operand_ptr
component_relation_value::operator[] (long index) const
{

	throw RuntimeError(xxInternal,
			   mytree->pos,
			   mytree->__parent->ident().create("component_relation_value::operator[] unimplemented"));
}

reference_array_value_adt::reference_array_value_adt(class REFERENCE_ARRAY_tree *t, class tree *list)
: value_adt(t)
{
	while (list)
	{
		assert(list->kind == EDD_REFERENCE_ARRAY_ELEMENT);

		element_t element;

		element.ref = (class REFERENCE_ARRAY_ELEMENT_tree *) list;
		element.value = element.ref->value_adt();

		elements[element.ref->index()] = element;

		list = list->next;
	}
}

reference_array_value_adt::reference_array_value_adt(class REFERENCE_ARRAY_tree *t, const elements_t &e)
: value_adt(t)
, elements(e)
{
	for (elements_t::iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
		iter->second.value = iter->second.value->value_adt_clone();
}

value_adt_ptr
reference_array_value_adt::value_adt_clone(void) const
{
	return new reference_array_value_adt((class REFERENCE_ARRAY_tree *)mytree, elements);
}

void
reference_array_value_adt::setenv0(class method_env *env) throw()
{
	for (elements_t::iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
		iter->second.value->setenv(env);
}

void
reference_array_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ reference_array_value_adt ");
	f.put(mytree->name());
	f.put(" { ");
	for (elements_t::const_iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		f.printf("%li", iter->first);
		f.put(" { ");
		iter->second.value->dump0(f);
		f.put(" } ");
	}
	f.put(" }");
}

void
reference_array_value_adt::eval_values(std::vector<operand_ptr> &ops)
{
	ops.reserve(ops.size() + elements.size());

	for (elements_t::iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
		iter->second.value->eval_values(ops);
}

void
reference_array_value_adt::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	for (elements_t::iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		operand_ptr val(op[iter->first]);
		operand_ptr value(iter->second.value);

		*value = *val;
	}
}

operand_ptr
reference_array_value_adt::operator[] (long index) const
{
	elements_t::const_iterator iter(elements.find(index));

	if (iter != elements.end())
		return iter->second.value;

	char buf[32];
	snprintf(buf, sizeof(buf), "%li", index);

	throw RuntimeError(xxArrayIndex,
			   mytree->pos,
			   mytree->__parent->ident().create(buf));
}

size_t
reference_array_value_adt::size(void) const
{
	size_t size = 0;

	for (elements_t::const_iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
		size += iter->second.value->size();

	return size;
}

void
reference_array_value_adt::save_type(lib::stream &f) const
{
	f.put(mytree->descr());
	f.put('{');
	for (elements_t::const_iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		write_long(f, iter->first);
		f.put('{');
		iter->second.value->save_type(f);
		f.put('}');
	}
	f.put('}');
}

void
reference_array_value_adt::check_type(const lib::stream &f) const
{
	expect(f, mytree->descr());
	expect(f, '{');
	for (elements_t::const_iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		expect_long(f, iter->first);
		expect(f, '{');
		iter->second.value->check_type(f);
		expect(f, '}');
	}
	expect(f, '}');
}

void
reference_array_value_adt::save_value(lib::stream &f) const
{
	for (elements_t::const_iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		write_long(f, iter->first);
		f.put('{');
		iter->second.value->save_value(f);
		f.put('}');
	}
}

void
reference_array_value_adt::load_value(const lib::stream &f)
{
	for (elements_t::iterator iter = elements.begin(), end = elements.end(); iter != end; ++iter)
	{
		expect_long(f, iter->first);
		expect(f, '{');
		iter->second.value->load_value(f);
		expect(f, '}');
	}
}


reference_array_value::reference_array_value(class REFERENCE_ARRAY_tree *t)
: edd_object_value(t)
{
}

operand_ptr
reference_array_value::clone(void) const
{
	return new reference_array_value((class REFERENCE_ARRAY_tree *)mytree);
}

void
reference_array_value::dump0(lib::stream &f) const
{
	f.put("$$ reference_array_value ");
	f.put(mytree->name());
}

void
reference_array_value::eval_values(std::vector<operand_ptr> &ops)
{
	std::vector<REFERENCE_ARRAY_ELEMENT_tree *> elements(((class REFERENCE_ARRAY_tree *) mytree)->elements(env));

	ops.reserve(ops.size() + elements.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
		elements[i]->value(env)->eval_values(ops);
}

void
reference_array_value::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	std::vector<REFERENCE_ARRAY_ELEMENT_tree *> elements(((class REFERENCE_ARRAY_tree *) mytree)->elements(env));

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		REFERENCE_ARRAY_ELEMENT_tree *element = elements[i];
		operand_ptr val(op[element->index()]);
		operand_ptr value(element->value(env));

		*value = *val;
	}
}

operand_ptr
reference_array_value::operator[] (long index) const
{
	std::vector<REFERENCE_ARRAY_ELEMENT_tree *> elements(((class REFERENCE_ARRAY_tree *) mytree)->elements(env));

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		REFERENCE_ARRAY_ELEMENT_tree *element = elements[i];

		if (element->index() == index)
			return element->value(env);
	}

	char buf[32];
	snprintf(buf, sizeof(buf), "%li", index);

	throw RuntimeError(xxArrayIndex,
			   mytree->pos,
			   mytree->__parent->ident().create(buf));
}


value_array_value_adt::value_array_value_adt(class VALUE_ARRAY_tree *t,
					     const element_t de)
: value_adt(t)
, def_element(de)
{
	assert(def_element);

	long nr_elements = t->length();

	if (nr_elements > 0)
		elements.resize(nr_elements);
}

value_array_value_adt::value_array_value_adt(class VALUE_ARRAY_tree *t,
					     const element_t de,
					     const elements_t &e)
: value_adt(t)
, def_element(de)
{
	assert(def_element);
	assert(t->length() == long(e.size()));

	elements.resize(e.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (e[i])
			elements[i] = e[i]->value_adt_clone();
	}
}

value_adt_ptr
value_array_value_adt::value_adt_clone(void) const
{
	return new value_array_value_adt((class VALUE_ARRAY_tree *)mytree, def_element, elements);
}

void
value_array_value_adt::setenv0(class method_env *env) throw()
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (elements[i])
			elements[i]->setenv(env);
	}
}

void
value_array_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ value_array_value_adt ");
	f.put(mytree->name());
	f.put(" { ");
	for (size_t i = 0, max = elements.size(); i < max; i++)
	{
		if (elements[i])
			elements[i]->dump(f);
		else
			def_element->dump(f);
	}
	f.put(" }");
}

void
value_array_value_adt::eval_values(std::vector<operand_ptr> &ops)
{
	ops.reserve(ops.size() + elements.size());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
		at(i)->eval_values(ops);
}

void
value_array_value_adt::operator=(const operand &op)
{
	if (mytree != op)
		throw RuntimeError(xxTypeMismatch, mytree->pos, mytree->ident());

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		operand_ptr val(op[long(i)]);
		operand_ptr value(at(i));

		*value = *val;
	}
}

operand_ptr
value_array_value_adt::operator[] (long index) const
{
	return at(index);
}

operand_ptr
value_array_value_adt::at(long index) const
{
	if (index < 0 || (size_t)index >= elements.size())
	{
		char buf[32];
		snprintf(buf, sizeof(buf), "%li", index);

		throw RuntimeError(xxArrayIndex,
				   mytree->pos,
				   mytree->__parent->ident().create(buf));
	}

	if (!elements[index])
	{
		elements[index] = def_element->value_adt_clone();
		elements[index]->setenv(env);
	}

	return elements[index];
}

size_t
value_array_value_adt::size(void) const
{
	size_t size = def_element->size();

	return size * elements.size();
}

void
value_array_value_adt::save_type(lib::stream &f) const
{
	f.put(mytree->descr());
	f.put('{');

	f.put("SIZE");
	write_long(f, elements.size());

	f.put('{');
	def_element->save_type(f);
	f.put('}');

	f.put('}');
}

void
value_array_value_adt::check_type(const lib::stream &f) const
{
	expect(f, mytree->descr());
	expect(f, '{');

	expect(f, "SIZE");
	if (read_long(f) != long(elements.size()))
		throw RuntimeError(xxRuntime, mytree->pos);

	expect(f, '{');
	def_element->check_type(f);
	expect(f, '}');

	expect(f, '}');
}

void
value_array_value_adt::save_value(lib::stream &f) const
{
	f.put("DEFAULT{");
	def_element->save_value(f);
	f.put('}');

	size_t entries = 0;

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (elements[i])
			entries++;
	}

	f.put("ENTRIES");
	write_long(f, entries);

	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
	{
		if (elements[i])
		{
			write_long(f, i);
			f.put('{');
			elements[i]->save_value(f);
			f.put('}');
		}
	}
}

void
value_array_value_adt::load_value(const lib::stream &f)
{
	for (size_t i = 0, i_max = elements.size(); i < i_max; i++)
		elements[i].reset();

	expect(f, "DEFAULT");
	expect(f, '{');
	def_element->value_adt_clone()->load_value(f); // XXX
	expect(f, '}');

	expect(f, "ENTRIES");
	size_t entries = read_long(f);

	for (size_t i = 0; i < entries; i++)
	{
		int index = read_long(f);

		if (index < 0 || size_t(index) >= elements.size())
			throw RuntimeError(xxRuntime, mytree->pos);

		expect(f, '{');
		elements[index] = def_element->value_adt_clone();
		elements[index]->setenv(env);
		elements[index]->load_value(f);
		expect(f, '}');
	}
}


variable_value_adt::variable_value_adt(class VARIABLE_tree *t, operand_ptr v)
: value_adt(t)
, enumerator_cache(NULL)
, type(t->type())
, value(v)
, age_stamp(0)
{
	assert(type);
	assert(value);
}

value_adt_ptr
variable_value_adt::value_adt_clone(void) const
{
	return new variable_value_adt((class VARIABLE_tree *)mytree, value->clone());
}

operand_ptr_T<variable_value_adt>
variable_value_adt::type_clone(void) const
{
	return new variable_value_adt((class VARIABLE_tree *)mytree, value->clone());
}

void
variable_value_adt::setenv0(class method_env *env) throw()
{
	value->setenv(env);
}

void
variable_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ variable_value_adt ");
	f.put(mytree->name());
	f.printf(" (#%" PRI_AGE_STAMP_T ")", age_stamp);
	value->dump(f);
}

void
variable_value_adt::post_assign_actions(void) throw()
{
	enumerator_cache = NULL;
}

std::string
variable_value_adt::print(const char *fmt) const
{
	operand_ptr v = value;

	if (type->have_scaling())
	{
		operand_ptr scaling(type->scaling_factor(env));

		v = new float64_lvalue(*value);
		*v *= (*scaling);
	}

	return v->print(fmt);
}
std::string
variable_value_adt::pretty_print(void) const
{
	operand_ptr v = value;

	if (type->have_scaling())
	{
		operand_ptr scaling(type->scaling_factor(env));

		v = new float64_lvalue(*value);
		*v *= (*scaling);
	}

	if (type->have_display_format())
	{
		std::string fmt(type->get_display_format(env));

		if (!fmt.empty())
		{
			int c = fmt[fmt.length() - 1];

			if (c == 'x' || c == 'X' || c == 'o')
			{
				int pad = (c == 'o') ? 1 : 2;

				for (;;)
				{
					c = fmt[0];

					if (c == ' ' || c == '+' || c == '-' || c == '#' || c == '0')
					{
						fmt.erase(fmt.begin());
					}
					else
						break;
				}

				int prec = 0;

				while (isdigit(c))
				{
					prec *= 10;
					prec += c - '0';

					fmt.erase(fmt.begin());
					c = fmt[0];
				}

				if (prec)
				{
					prec += pad;

					char buf[16];
					snprintf(buf, sizeof(buf), "%%#0%i", prec);

					fmt.insert(0, buf);
				}
				else
					fmt.insert(0, "%#0");
			}
			else
				fmt.insert(fmt.begin(), '%');

			return v->print(fmt.c_str());
		}
	}

	/* if there is an enumeration value use the enumeration description */
	if (enumerator())
		return enumerator()->description(env);

	/* otherwise print the value normally */
	return v->pretty_print();
}
void
variable_value_adt::scan(const std::string &str, bool strict)
{
	if (type->have_scaling())
	{
		float64_lvalue val(0.0);
		val.scan(str, strict);

		operand_ptr scaling(type->scaling_factor(env));

		val /= (*scaling);

		if (type->gettype() == EDD_VARIABLE_TYPE_ARITHMETIC &&
		   (type->getsubtype() == EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_INTEGER ||
		    type->getsubtype() == EDD_VARIABLE_TYPE_ARITHMETIC_TYPE_UNSIGNED))
		{
			// round integer variables (add 0.5 and truncate)

			val += float64_value(0.5);

			std::string rounded_val = val.print();
			std::string::size_type pos = rounded_val.find(".");

			if (pos != std::string::npos)
				rounded_val = rounded_val.substr(0, pos);

			value->scan(rounded_val, strict);

		}
		else
			value->scan(val.print(), strict);
	}
	else
		value->scan(str, strict);

	post_assign_actions();
}
void
variable_value_adt::pretty_scan(const std::string &str)
{
	std::vector<class VARIABLE_ENUMERATOR_tree *> enums;
	bool done = false;

	type->get_enumerations(enums, env);

	for (size_t i = 0, i_max = enums.size(); i < i_max; i++)
	{
		if (str == enums[i]->description(env))
		{
			operand_ptr op(enums[i]->value(env));

			/* assign the enumeration value */
			(*value) = (*op);
			post_assign_actions();

			done = true;
			break;
		}
	}

	if (!done && type->have_display_format())
	{
		std::string fmt(type->get_display_format(env));

		if (!fmt.empty())
		{
			int c = fmt[fmt.length() - 1];

			if (c == 'd' || c == 'i' || c == 'u')
			{
				bool strip = false;

				for (;;)
				{
					c = fmt[0];

					if (c == '0')
					{
						strip = true;
						break;
					}
					else if (c == ' ' || c == '+' || c == '-' || c == '#')
					{
						fmt.erase(fmt.begin());
					}
					else
						break;
				}

				if (strip)
				{
					std::string str1(str);
					int c;

					/* strip leading zeros */
					c = str1[0];
					while (isspace(c) || c == '0')
					{
						str1.erase(str1.begin());
						c = str1[0];
					}

					if (str1.empty())
						str1 = str;

					scan(str1);

					done = true;
				}
			}
		}
	}

	if (!done)
		scan(str);
}

            variable_value_adt::operator stackmachine::Type *() const { return (Type *)(*value); }
void        variable_value_adt::operator= (const stackmachine::Type &v) { *value = v; post_assign_actions(); }

operand::value_status_t& variable_value_adt::status (void) { return value->status(); }
const operand::value_status_t& variable_value_adt::status(void) const { return value->status(); }

#define CAST_OPERATOR_ACCESS(type) (type)(*value)
VALUE_CAST_OPERATORS(variable_value_adt)
CAST_OPERATOR(variable_value_adt, std::string)
#undef CAST_OPERATOR_ACCESS

operand_ptr variable_value_adt::operator++  (int)  // post
{
	operand_ptr ret((*value)++);
	post_assign_actions();

	return ret;
}
operand_ptr variable_value_adt::operator--  (int)  // post
{
	operand_ptr ret((*value)--);
	post_assign_actions();

	return ret;
}

operand_ptr variable_value_adt::operator++  (void) // pre
{
	operand_ptr ret(++(*value));
	post_assign_actions();

	return ret;
}
operand_ptr variable_value_adt::operator--  (void) // pre
{
	operand_ptr ret(--(*value));
	post_assign_actions();

	return ret;
}

operand_ptr variable_value_adt::operator+   (void) const { return +(*value); }
operand_ptr variable_value_adt::operator-   (void) const { return -(*value); }
operand_ptr variable_value_adt::operator~   (void) const { return ~(*value); }
operand_ptr variable_value_adt::operator!   (void) const { return !(*value); }

operand_ptr variable_value_adt::operator*   (const operand &op) const { return ((*value) *  op); }
operand_ptr variable_value_adt::operator/   (const operand &op) const { return ((*value) /  op); }
operand_ptr variable_value_adt::operator+   (const operand &op) const { return ((*value) +  op); }
operand_ptr variable_value_adt::operator-   (const operand &op) const { return ((*value) -  op); }
operand_ptr variable_value_adt::operator%   (const operand &op) const { return ((*value) %  op); }
operand_ptr variable_value_adt::operator^   (const operand &op) const { return ((*value) ^  op); }
operand_ptr variable_value_adt::operator&   (const operand &op) const { return ((*value) &  op); }
operand_ptr variable_value_adt::operator|   (const operand &op) const { return ((*value) |  op); }
operand_ptr variable_value_adt::operator<<  (const operand &op) const { return ((*value) << op); }
operand_ptr variable_value_adt::operator>>  (const operand &op) const { return ((*value) >> op); }

bool        variable_value_adt::operator<   (const operand &op) const { return ((*value) <  op); }
bool        variable_value_adt::operator>   (const operand &op) const { return ((*value) >  op); }
bool        variable_value_adt::operator==  (const operand &op) const { return ((*value) == op); }
bool        variable_value_adt::operator!=  (const operand &op) const { return ((*value) != op); }
bool        variable_value_adt::operator<=  (const operand &op) const { return ((*value) <= op); }
bool        variable_value_adt::operator>=  (const operand &op) const { return ((*value) >= op); }

void        variable_value_adt::operator=   (const operand &op) { (*value) =   op; post_assign_actions(); }
void        variable_value_adt::operator+=  (const operand &op) { (*value) +=  op; post_assign_actions(); }
void        variable_value_adt::operator-=  (const operand &op) { (*value) -=  op; post_assign_actions(); }
void        variable_value_adt::operator*=  (const operand &op) { (*value) *=  op; post_assign_actions(); }
void        variable_value_adt::operator/=  (const operand &op) { (*value) /=  op; post_assign_actions(); }
void        variable_value_adt::operator%=  (const operand &op) { (*value) %=  op; post_assign_actions(); }
void        variable_value_adt::operator^=  (const operand &op) { (*value) ^=  op; post_assign_actions(); }
void        variable_value_adt::operator&=  (const operand &op) { (*value) &=  op; post_assign_actions(); }
void        variable_value_adt::operator|=  (const operand &op) { (*value) |=  op; post_assign_actions(); }
void        variable_value_adt::operator>>= (const operand &op) { (*value) >>= op; post_assign_actions(); }
void        variable_value_adt::operator<<= (const operand &op) { (*value) <<= op; post_assign_actions(); }

operand_ptr variable_value_adt::operator[]  (long index) const { return (*value)[index]; }

operand_ptr
variable_value_adt::member(const lib::IDENTIFIER &id) const
{
	return ((class VARIABLE_tree *)mytree)->selector_eval_variable(env, id, *this);
}

size_t
variable_value_adt::size(void) const
{
	return ((class VARIABLE_tree *)mytree)->get_io_size();
}

void
variable_value_adt::save_type(lib::stream &f) const
{
	f.put(mytree->descr());
	f.put('{');
	f.put(type->realdescr());
	f.put(',');
	f.put(type->typestring());
	f.put(',');
	write_long(f, type->get_io_size());
	f.put('}');
}

void
variable_value_adt::check_type(const lib::stream &f) const
{
	expect(f, mytree->descr());
	expect(f, '{');
	expect(f, type->realdescr());
	expect(f, ',');
	expect(f, type->typestring());
	expect(f, ',');
	expect_long(f, type->get_io_size());
	expect(f, '}');
}

void
variable_value_adt::save_value(lib::stream &f) const
{
	std::string str(value->print(NULL));

	write_string(f, str);
}

void
variable_value_adt::load_value(const lib::stream &f)
{
	std::string str(read_string(f));

	value->scan(str, false);
}

class VARIABLE_ENUMERATOR_tree *
variable_value_adt::enumerator(void) const
{
	if (!enumerator_cache && type->have_enumerations())
		enumerator_cache = type->get_enumeration(*value, env);

	return enumerator_cache;
}


variable_value::variable_value(class VARIABLE_tree *t, variable_value_adt_ptr v)
: edd_object_value(t), value(v)
{
}

inline void
variable_value::pre_assign_actions(void) const
{
	((class VARIABLE_tree *)mytree)->pre_assign_actions(env);
}

inline void
variable_value::post_assign_actions(void) const
{
	((class VARIABLE_tree *)mytree)->post_assign_actions(env);
}

operand_ptr
variable_value::clone(void) const
{
	return new variable_value((class VARIABLE_tree *)mytree, value->type_clone());
}

void
variable_value::setenv0(class method_env *env) throw()
{
	value->setenv(env);
}

void
variable_value::dump0(lib::stream &f) const
{
	f.put("$$ variable_value ");
	f.put(mytree->name());
	f.put(' ');
	value->dump(f);
}

std::string
variable_value::print(const char *fmt) const
{
	return value->print(fmt);
}
std::string
variable_value::pretty_print(void) const
{
	return value->pretty_print();
}

void
variable_value::scan(const std::string &str, bool strict)
{
	pre_assign_actions();

	value->scan(str, strict);

	post_assign_actions();
}
void
variable_value::pretty_scan(const std::string &str)
{
	pre_assign_actions();

	value->pretty_scan(str);

	post_assign_actions();
}

            variable_value::operator stackmachine::Type *() const { return (Type *)(*value); }
void        variable_value::operator= (const stackmachine::Type &v) { pre_assign_actions(); *value = v; post_assign_actions(); }

operand::value_status_t& variable_value::status (void) { return value->status(); }
const operand::value_status_t& variable_value::status(void) const { return value->status(); }

#define CAST_OPERATOR_ACCESS(type) (type)(*value)
VALUE_CAST_OPERATORS(variable_value)
CAST_OPERATOR(variable_value, std::string)
#undef CAST_OPERATOR_ACCESS

operand_ptr variable_value::operator++  (int)  // post
{
	pre_assign_actions();
	operand_ptr ret((*value)++);
	post_assign_actions();

	return ret;
}
operand_ptr variable_value::operator--  (int)  // post
{
	pre_assign_actions();
	operand_ptr ret((*value)--);
	post_assign_actions();

	return ret;
}

operand_ptr variable_value::operator++  (void) // pre
{
	pre_assign_actions();
	operand_ptr ret(++(*value));
	post_assign_actions();

	return ret;
}
operand_ptr variable_value::operator--  (void) // pre
{
	pre_assign_actions();
	operand_ptr ret(--(*value));
	post_assign_actions();

	return ret;
}

operand_ptr variable_value::operator+   (void) const { return +(*value); }
operand_ptr variable_value::operator-   (void) const { return -(*value); }
operand_ptr variable_value::operator~   (void) const { return ~(*value); }
operand_ptr variable_value::operator!   (void) const { return !(*value); }

operand_ptr variable_value::operator*   (const operand &op) const { return ((*value) *  op); }
operand_ptr variable_value::operator/   (const operand &op) const { return ((*value) /  op); }
operand_ptr variable_value::operator+   (const operand &op) const { return ((*value) +  op); }
operand_ptr variable_value::operator-   (const operand &op) const { return ((*value) -  op); }
operand_ptr variable_value::operator%   (const operand &op) const { return ((*value) %  op); }
operand_ptr variable_value::operator^   (const operand &op) const { return ((*value) ^  op); }
operand_ptr variable_value::operator&   (const operand &op) const { return ((*value) &  op); }
operand_ptr variable_value::operator|   (const operand &op) const { return ((*value) |  op); }
operand_ptr variable_value::operator<<  (const operand &op) const { return ((*value) << op); }
operand_ptr variable_value::operator>>  (const operand &op) const { return ((*value) >> op); }

bool        variable_value::operator<   (const operand &op) const { return ((*value) <  op); }
bool        variable_value::operator>   (const operand &op) const { return ((*value) >  op); }
bool        variable_value::operator==  (const operand &op) const { return ((*value) == op); }
bool        variable_value::operator!=  (const operand &op) const { return ((*value) != op); }
bool        variable_value::operator<=  (const operand &op) const { return ((*value) <= op); }
bool        variable_value::operator>=  (const operand &op) const { return ((*value) >= op); }

void        variable_value::operator=   (const operand &op) { pre_assign_actions(); (*value) =   op; post_assign_actions(); }
void        variable_value::operator+=  (const operand &op) { pre_assign_actions(); (*value) +=  op; post_assign_actions(); }
void        variable_value::operator-=  (const operand &op) { pre_assign_actions(); (*value) -=  op; post_assign_actions(); }
void        variable_value::operator*=  (const operand &op) { pre_assign_actions(); (*value) *=  op; post_assign_actions(); }
void        variable_value::operator/=  (const operand &op) { pre_assign_actions(); (*value) /=  op; post_assign_actions(); }
void        variable_value::operator%=  (const operand &op) { pre_assign_actions(); (*value) %=  op; post_assign_actions(); }
void        variable_value::operator^=  (const operand &op) { pre_assign_actions(); (*value) ^=  op; post_assign_actions(); }
void        variable_value::operator&=  (const operand &op) { pre_assign_actions(); (*value) &=  op; post_assign_actions(); }
void        variable_value::operator|=  (const operand &op) { pre_assign_actions(); (*value) |=  op; post_assign_actions(); }
void        variable_value::operator>>= (const operand &op) { pre_assign_actions(); (*value) >>= op; post_assign_actions(); }
void        variable_value::operator<<= (const operand &op) { pre_assign_actions(); (*value) <<= op; post_assign_actions(); }

operand_ptr variable_value::operator[]  (long index) const { return (*value)[index]; }
operand_ptr variable_value::member(const lib::IDENTIFIER &id) const { return value->member(id); }


bool
bit_lvalue::bit(void) const
{
	return (get_value() & enumerator->bitposition()) ? true : false;
}
void
bit_lvalue::set(bool flag)
{
	operand_ptr v(enumerator->value(env));

	if (flag)
	{
		*value |= *v;
	}
	else
	{
		v = ~(*v);
		*value &= *v;
	}
}

bit_lvalue::bit_lvalue(class VARIABLE_tree *t,
		       class VARIABLE_ENUMERATOR_BITENUM_tree *e,
		       operand_ptr v)
: edd_object_value(t)
, enumerator(e)
, value(v)
{
	assert(enumerator);
}

operand_ptr
bit_lvalue::clone(void) const
{
	return new bit_lvalue((class VARIABLE_tree *)mytree,
			      enumerator,
			      env ? env->uncached_value(value) : value);
}

void
bit_lvalue::setenv0(class method_env *env) throw()
{
	if (env && value->getenv() == NULL)
		value = env->value(value);

	assert(env == value->getenv());
}

void
bit_lvalue::dump0(lib::stream &f) const
{
	f.put("$$ bit_lvalue ");
	f.put(mytree->ident().c_str());
	f.printf("[0x%lx] ", (unsigned long)enumerator->bitposition());
	value->dump(f);
}

std::string
bit_lvalue::print(const char *fmt) const
{
	if (fmt)
		return enumerator->description(env);

	if (bit())
		return std::string("true");

	return std::string("false");
}

void
bit_lvalue::scan(const std::string &str, bool strict)
{
	bool flag = false;

	if (str == "true")
		flag = true;

	set(flag);
}

            bit_lvalue::operator stackmachine::Type *() const { return new stackmachine::boolType(bit()); }
void        bit_lvalue::operator= (const stackmachine::Type &v) { set((bool)v); }

operand::value_status_t& bit_lvalue::status(void) { return value->status(); }
const operand::value_status_t& bit_lvalue::status(void) const { return value->status(); }

#define CAST_OPERATOR_ACCESS(type) bit()
VALUE_CAST_OPERATORS(bit_lvalue)
#undef CAST_OPERATOR_ACCESS

operand_ptr bit_lvalue::operator~   (void) const { return new uint32_value(~bit()); }
operand_ptr bit_lvalue::operator!   (void) const { return new uint32_value(!bit()); }

operand_ptr bit_lvalue::operator^   (const operand &op) const { return new uint32_value(bit() ^ (uint32) op); }
operand_ptr bit_lvalue::operator&   (const operand &op) const { return new uint32_value(bit() & (uint32) op); }
operand_ptr bit_lvalue::operator|   (const operand &op) const { return new uint32_value(bit() | (uint32) op); }

bool        bit_lvalue::operator<   (const operand &op) const { return (bit() <  (bool) op); }
bool        bit_lvalue::operator>   (const operand &op) const { return (bit() >  (bool) op); }
bool        bit_lvalue::operator==  (const operand &op) const { return (bit() == (bool) op); }
bool        bit_lvalue::operator!=  (const operand &op) const { return (bit() != (bool) op); }
bool        bit_lvalue::operator<=  (const operand &op) const { return (bit() <= (bool) op); }
bool        bit_lvalue::operator>=  (const operand &op) const { return (bit() >= (bool) op); }

void        bit_lvalue::operator=   (const operand &op) { set((bool) op); }

operand_ptr bit_lvalue::member(const lib::IDENTIFIER &id) const
{
	return enumerator->selector_eval(env, id);
}

long
bit_lvalue::bitposition(void) const
{
	return enumerator->bitposition();
}


bitenum_variable_value_adt::bitenum_variable_value_adt(class VARIABLE_tree *t, operand_ptr op)
: variable_value_adt(t, op)
{
}

value_adt_ptr
bitenum_variable_value_adt::value_adt_clone(void) const
{
	return new bitenum_variable_value_adt((class VARIABLE_tree *)mytree, value->clone());
}

operand_ptr_T<variable_value_adt>
bitenum_variable_value_adt::type_clone(void) const
{
	return new bitenum_variable_value_adt((class VARIABLE_tree *)mytree, value->clone());
}

void
bitenum_variable_value_adt::dump0(lib::stream &f) const
{
	f.put("$$ bitenum_variable_value_adt ");
	f.put(mytree->ident().c_str());
	f.put(' ');
	value->dump(f);
}

operand_ptr
bitenum_variable_value_adt::operator[](long index) const
{
	class VARIABLE_tree *vt = (class VARIABLE_tree *)mytree;
	assert(vt->gettype() == EDD_VARIABLE_TYPE_ENUMERATED
	       && vt->getsubtype() == EDD_VARIABLE_TYPE_ENUMERATED_TYPE_BITENUMERATED);

	std::vector<class VARIABLE_ENUMERATOR_tree *> enums(vt->get_enumerations(env));

	for (size_t i = 0, i_max = enums.size(); i < i_max; ++i)
	{
		class VARIABLE_ENUMERATOR_BITENUM_tree *enumerator;

		enumerator = (class VARIABLE_ENUMERATOR_BITENUM_tree *)enums[i];
		assert(enumerator->kind == EDD_VARIABLE_ENUMERATOR_BITENUM);

		if (enumerator->bitposition() == index)
		{
			operand *unconstThis = const_cast<bitenum_variable_value_adt *>(this);
			operand_ptr op = new bit_lvalue(vt, enumerator, unconstThis);
			op->setenv(env);

			return op;
		}
	}

	return variable_value_adt::operator[](index);
}


void
status_lvalue::assign(long v)
{
	switch (v)
	{
		default:
		case VALUE_STATUS_NONE:          value = VALUE_STATUS_NONE;          break;
		case VALUE_STATUS_INVALID:       value = VALUE_STATUS_INVALID;       break;
		case VALUE_STATUS_NOT_ACCEPTED:  value = VALUE_STATUS_NOT_ACCEPTED;  break;
		case VALUE_STATUS_NOT_SUPPORTED: value = VALUE_STATUS_NOT_SUPPORTED; break;
		case VALUE_STATUS_CHANGED:       value = VALUE_STATUS_CHANGED;       break;
		case VALUE_STATUS_LOADED:        value = VALUE_STATUS_LOADED;        break;
		case VALUE_STATUS_INITIAL:       value = VALUE_STATUS_INITIAL;       break;
		case VALUE_STATUS_NOT_CONVERTED: value = VALUE_STATUS_NOT_CONVERTED; break;
	}
}

operand_ptr status_lvalue::clone(void) const { return new status_lvalue(value); }

void
status_lvalue::dump0(lib::stream &f) const
{
	f.printf("$$ status lval { %s }", value_status_descr(value));
}

std::string
status_lvalue::print(const char *fmt) const
{
	return value_status_descr(value);
}

void
status_lvalue::scan(const std::string &str, bool strict)
{
	throw RuntimeError(xxNotSupportedBy);
}

            status_lvalue::operator stackmachine::Type *() const { return new stackmachine::int32Type(value); }
void        status_lvalue::operator= (const stackmachine::Type &v) { assign((int32) v); }

#define CAST_OPERATOR_ACCESS(type) type(value)
VALUE_CAST_OPERATORS(status_lvalue)
#undef CAST_OPERATOR_ACCESS

bool        status_lvalue::operator==  (const operand &op) const { return (value == (int32) op); }
bool        status_lvalue::operator!=  (const operand &op) const { return (value != (int32) op); }

void        status_lvalue::operator=   (const operand &op) { assign((int32) op); }

} /* namespace edd */

