
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_builtins_null_h
#define _edd_builtins_null_h

#include "ident.h"
#include "position.h"

#include "cocktail_types.h"
#include "edd_builtins_dispatch.h"

namespace builtins
{

using namespace edd;

#define BUILTINS_STDARGS_DEF class method_env *env, const lib::pos &caller_pos
#define BUILTINS_STDARGS env, caller_pos

enum box_type
{
	EDIT_BOX_TYPE_INTEGER,
	EDIT_BOX_TYPE_FLOAT,
	EDIT_BOX_TYPE_STRING,
	EDIT_BOX_TYPE_DATE_AND_TIME,
	EDIT_BOX_TYPE_DATE,
	EDIT_BOX_TYPE_TIME,
    COMBO_BOX_SINGLE_SELECT,
	COMBO_BOX_MULTI_SELECT
};

const long SetNewValue( class tree* device_var_name, class method_env* env);

void eval_menu(edd::method_env *env, edd::tree *t, unsigned char lev);

void level(unsigned char ind);

/**
 * The builtin ACKNOWLEDGE.
 * The Builtin ACKNOWLEDGE will display the prompt and wait for the enter key
 * to be pressed. Unlike Builtin acknowledge, the prompt may NOT contain
 * embedded device VARIABLEs.
 */
edd_int32 ACKNOWLEDGE(
		BUILTINS_STDARGS_DEF,
		edd_string prompt);
/**
 * The builtin DELAY.
 * The Builtin DELAY will display a prompt message and pause for the specified
 * number of seconds. The prompt may contain local variable values,
 * (see Builtin put_message for lexical structure). The prompt may NOT contain
 * device variable values. The delay time shall be a positive number.
 * \note Previously displayed messages are not guaranteed to remain on
 * the screen.
 * \note Some EDD applications will clear the display upon each call to
 * this function.
 */
void DELAY(
		BUILTINS_STDARGS_DEF,
		edd_int32 delay_time,
		edd_string prompt);
/**
 * The builtin DELAY_TIME.
 * The Builtin DELAY_TIME will pause for the specified number of seconds.
 * The delay time must be a positive number.
 */
void DELAY_TIME(
		BUILTINS_STDARGS_DEF,
		edd_int32 delay_time);
/**
 * The builtin DISPLAY.
 * The Builtin DISPLAY will display the specified message on the screen,
 * continuously updating the dynamic VARIABLE values used in the string (see
 * PUT_MESSAGE for Lexical Structure). The VARIABLE value will continue to be
 * updated until operator acknowledgement is made.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param prompt specifies a message to be displayed
 */
void DISPLAY(
		BUILTINS_STDARGS_DEF,
		edd_string prompt);
/**
 * The builtin GET_DEV_VAR_VALUE.
 * The Builtin GET_DEV_VAR_VALUE will display the specified prompt message,
 * and allow the user to edit the value of a device VARIABLE. The edited copy
 * of the device VARIABLE value will be updated when the new value is entered,
 * but will not be sent to the device. This shall be done explicitly using one
 * of the send command functions.
 * 
 * The prompt may NOT contain embedded local and/or device VARIABLE values.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 GET_DEV_VAR_VALUE(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		class tree * device_var_name);
/**
 * The builtin GET_LOCAL_VAR_VALUE.
 * The Builtin GET_LOCAL_VAR_VALUE will display the specified prompt message,
 * and allow the user to edit the value of a local VARIABLE.
 * 
 * The prompt may NOT contain embedded local and/or device VARIABLE values.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 GET_LOCAL_VAR_VALUE(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		lib::IDENTIFIER local_var_name);
/**
 * The builtin LOG_MESSAGE.
 * Builtin  LOG_MESSAGE inserts a message into a logging protocol.
 * 
 * \note Not mentioned in any profile in the standard.
 * \todo Builtin LOG_MESSAGE not in any profile.
 */
void LOG_MESSAGE(
		BUILTINS_STDARGS_DEF,
		edd_int32 priority,
		edd_string message);
/**
 * The builtin MenuDisplay.
 * Builtin MenuDisplay will display a MENU instance to the user and a supplied
 * list of options for the user to select from. Once a selection is made, the
 * option position in the option list is returned. The position in the list is
 * zero based. For example, if provided list contains two items, if the first
 * item is selected this functiton will return a 0. If second item is
 * selected, a 1 is returned. The options are passed to the function as a
 * single string with semicolon separators between the options.
 * 
 * \note MenuDisplay is an EDD 1.2 enhancement.
 */
edd_int32 MenuDisplay(
		BUILTINS_STDARGS_DEF,
		class tree * menu,
		edd_string options,
		edd_int32_ref &selection);
/**
 * The builtin PUT_MESSAGE.
 * The Builtin PUT_MESSAGE will display the specified message on the screen.
 * Any embedded local variable references will be expanded to the variable's
 * value (see Builtin put_message for Lexical Structure).
 * 
 * Embedded device variables are NOT supported in this function.
 * \note Previously displayed messages are not guaranteed to remain on
 * the screen.
 * \note Some EDD applications will clear the display upon each call to
 * this function.
 */
void PUT_MESSAGE(
		BUILTINS_STDARGS_DEF,
		edd_string message);
/**
 * The builtin SELECT_FROM_LIST.
 * The Builtin SELECT_FROM_LIST has the same functionality as Builtin
 * select_from_list, except that device variables are not allowed in the
 * prompt string. There shall be at least two options in the select list.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 SELECT_FROM_LIST(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_string option_list);
/**
 * The builtin ShellExecute.
 * ShellExecute takes a string argument and opens the specified file.
 */
void ShellExecute(
		BUILTINS_STDARGS_DEF,
		edd_string arg);
/**
 * The builtin _ERROR.
 * The Builtin _ERROR is an undocumented SIMATIC PDM extension for profile HART.
 */
void _ERROR(
		BUILTINS_STDARGS_DEF,
		edd_int32 value);
void _ERROR_1(
		BUILTINS_STDARGS_DEF,
		edd_string message);
void _ERROR_2(
		BUILTINS_STDARGS_DEF,
		edd_real32 value);
void _ERROR_3(
		BUILTINS_STDARGS_DEF,
		edd_uint32 value);
/**
 * The builtin _TRACE.
 * The Builtin _TRACE is an undocumented SIMATIC PDM extension for profile HART.
 */
void _TRACE(
		BUILTINS_STDARGS_DEF,
		edd_int32 value);
void _TRACE_1(
		BUILTINS_STDARGS_DEF,
		edd_string message);
void _TRACE_2(
		BUILTINS_STDARGS_DEF,
		edd_real32 value);
void _TRACE_3(
		BUILTINS_STDARGS_DEF,
		edd_uint32 value);
/**
 * The builtin _WARNING.
 * The Builtin _WARNING is an undocumented SIMATIC PDM extension for profile HART.
 */
void _WARNING(
		BUILTINS_STDARGS_DEF,
		edd_int32 value);
void _WARNING_1(
		BUILTINS_STDARGS_DEF,
		edd_string message);
void _WARNING_2(
		BUILTINS_STDARGS_DEF,
		edd_real32 value);
void _WARNING_3(
		BUILTINS_STDARGS_DEF,
		edd_uint32 value);
/**
 * The builtin abort.
 * The Builtin abort will display a message indicating that the method has
 * been aborted and wait for acknowledgement from the user. Once
 * acknowledgement has been made, the system will execute any abort methods
 * in the abort method list and will exit the method.
 */
void abort(
		BUILTINS_STDARGS_DEF);
/**
 * The builtin acknowledge.
 * The Builtin acknowledge will display the prompt and wait for
 * acknowledgment from the user. The prompt may contain local and/or device
 * variable values.
 */
edd_int32 acknowledge(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_int32_array &global_var_ids);
/**
 * The builtin delay.
 * The Builtin delay will display a prompt message and pause for the specified
 * number of seconds. The prompt may contain local and/or device VARIABLE
 * values (see put_message for Lexical Structure). The delay time shall be a
 * positive number.
 * \note Previously displayed messages are not guaranteed to remain on
 * the screen.
 * \note Some EDD applications will clear the display upon each call to
 * this function.
 */
void delay(
		BUILTINS_STDARGS_DEF,
		edd_int32 delay_time,
		edd_string prompt,
		edd_int32_array &global_var_ids);
/**
 * The builtin delayfor.
 * The Builtin delayfor prints a string on the application display and waits
 * for the allotted amount of time to expire. VARIABLE values may be embedded
 * in the message string with formatting information.
 */
edd_int32 delayfor(
		BUILTINS_STDARGS_DEF,
		edd_int32 duration,
		edd_string prompt,
		edd_uint32_array &ids,
		edd_uint32_array &indices,
		edd_int32 id_count);
/**
 * The builtin display.
 * The Builtin display will display the specified message on the screen,
 * continuously updating the dynamic VARIABLE values used in the string (see
 * put_message for Lexical Structure). The VARIABLE value will continue to be
 * updated until operator acknowledgement is made.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param prompt specifies a message to be displayed
 * \param global_var_ids array of int I m specifies an array which contains
 * unique identifiers of VARIABLE instance
 */
void display(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_int32_array &global_var_ids);
/**
 * The builtin edit_device_value.
 * The Builtin edit_device_value displays a prompt with a device value
 * specified by the param_id and param_member_id. The user can edit the
 * displayed value.
 * 
 * \note A device value may reside in the VARIABLE table of a caching EDD
 * application, and the value may not be written to the device until a method
 * terminates or the value is explicitly sent to the device by the method.
 * 
 * VARIABLE values may be embedded in the message string with formatting
 * information. The application edits and verifies the values entered for
 * the device value. This editing is based on VARIABLE arithmetic types
 * defined in the original EDD for each VARIABLE. The EDDL attributes
 * associated with VARIABLEs may include:
 * - HANDLING
 * - DISPLAY_FORMAT
 * - EDIT_FORMAT
 * - MIN_VALUE
 * - MAX_VALUE
 * .
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param prompt specifies a message to be displayed
 * \param ids specifies the item IDs of the VARIABLEs used in the prompt
 * message
 * \param indices specifies the member IDs or array index of the VARIABLEs
 * used in the prompt message. Set to zero if there is no member ID or array
 * index
 * \param id_count specifies the number of array elements in the arrays for
 * ids and indices. Set to zero if the arrays for ids and indices are NULL
 * \param param_id specifies the item ID of the VARIABLE to be modified
 * \param param_member_id specifies the member ID of the VARIBLE to be
 * modified
 * \return specifies the return value of the Builtin
 */
edd_int32 edit_device_value(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_uint32_array &ids,
		edd_uint32_array &indices,
		edd_int32 id_count,
		edd_uint32 param_id,
		edd_uint32 param_member_id);
/**
 * The builtin edit_local_value.
 * The Builtin edit_local_value displays a prompt with the local method
 * VARIABLE name specified by the parameter localvar. The localvar parameter
 * is a null-terminated C string of characters. The user can edit the
 * displayed value of the local method VARIABLE.
 * 
 * Variable values may be embedded in the message string with formatting
 * information. The EDD application is responsible for preventing invalid
 * values from being returned by performing checks based on VARIABLE type
 * only. The method shall perform range checking on returned values.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param prompt specifies a message to be displayed
 * \param ids specifies the item IDs of the VARIABLEs used in the prompt
 * message
 * \param indices specifies the member IDs or array index of the VARIABLEs
 * used in the prompt message. Set to zero if there is no member ID or array
 * index
 * \param id_count specifies the number of array elements in the
 * arrays for ids and indices. Set to zero if the arrays for ids and indices
 * are NULL
 * \param local_var specifies the name of the local VARIABLE that may be
 * modified
 * \return specifies the return value of the Builtin
 */
edd_int32 edit_local_value(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_uint32_array &ids,
		edd_uint32_array &indices,
		edd_int32 id_count,
		edd_string local_var);
/**
 * The builtin get_dev_var_value.
 * The Builtin get_dev_var_value will display the specified prompt message,
 * and allow the user to edit the value of a device VARIABLE. The edited copy
 * of the device VARIABLE value will be updated when the new value is entered,
 * but will not be sent to the device. This shall be done explicitly using one
 * of the send command functions.
 * 
 * The prompt may contain embedded local and/or device VARIABLE values (see
 * Builtin PUT_MESSAGE).
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 get_dev_var_value(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_int32_array &ids,
		class tree * device_var_name);
/**
 * The builtin get_local_var_value.
 * The Builtin get_local_var_value will display the specified prompt message,
 * and allow the user to edit the value of a local VARIABLE.
 * 
 * The prompt may contain embedded local and/or device VARIABLE values.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 get_local_var_value(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_int32_array &global_var_ids,
		lib::IDENTIFIER local_var_name);
/**
 * The builtin method_abort.
 * The Builtin method_abort displays a message indicating that the method is
 * aborting and waits for acknowledgment from the user.
 * 
 * After the acknowledgment, the abort methods in the abort method list are
 * executed one after another from the front of the list (where the first
 * abort method was added to the list) to the back of the list. The abort
 * method list is maintained only for the execution of a single invocation
 * of the method.
 * 
 * After the abort methods are executed, the method terminates and control
 * is returned to the application.
 * 
 * If an abort method calls method_abort, the abort method list processing
 * is immediately terminated.
 */
void method_abort(
		BUILTINS_STDARGS_DEF,
		edd_string prompt);
void method_set(
		BUILTINS_STDARGS_DEF);
/**
 * The builtin put_message.
 * The Builtin put_message will display the specified message on the screen.
 * Any embedded variable references will be expanded to the variable's current
 * value.
 * 
 * Any number of eddobjects may be accessed by the string, but the array
 * entries accessed shall contain valid variable eddobjects. Local and device
 * variables may be mixed in the display message.
 * 
 * Multiple languages are also supported in the put_message string. This is
 * done by separating the different language strings by their country code
 * eddobject. The country code eddobject is the vertical bar character,
 * '|', followed immediately by that country's three digit phone code. If
 * the phone code is less than three digits in length, it should be padded on
 * the left with zeros.
 */
void put_message(
		BUILTINS_STDARGS_DEF,
		edd_string message,
		edd_int32_array &global_var_ids);
/**
 * The builtin refresh.
 * The Builtin refresh set the \b CHANGED flag on attributes in SIMATIC PDM.
 * It is to be used in PRE/POST_EDIT_ACTIONS to refresh all depending
 * variables in the gridview. If a variable x is changed and y and z should be
 * refreshed in the view too, you have to write a POST_EDIT_ method for x like
 * that:
 * \code
 * METHOD refresh_on_x
 * {
 * 	LABEL "refresh_on_x";
 * 	DEFINITION
 *	{
 * 		long var_ids[2]; 
 * 		var_ids[0] = VARID(y);
 * 		var_ids[1] = VARID(z);
 * 		refresh(var_ids);
 * 	}
 * }
 * \endcode
 */
void refresh(
		BUILTINS_STDARGS_DEF,
		edd_int32_array &var_ids);
/**
 * The builtin select_from_list.
 * The Builtin select_from_list will display a prompt and a supplied list of
 * options for the user to select from. Once a selection is made, that
 * option's position in the option list is returned. The position in the list
 * is zero based. For example, if provided list contains two items, if the
 * first item is selected this function will return a 0. If second item is
 * selected, a 1 is returned.
 * 
 * The prompt and/or list may also contain embedded local and/or device
 * variable values (see put_message for the Lexical Structure).
 * 
 * The options are passed to the function as a single string with semicolon
 * separators between the options. There shall be at least two options in the
 * select list.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 * \note The user has the opportunity to abort the method while method
 * execution is halted waiting for this function to return.
 */
edd_int32 select_from_list(
		BUILTINS_STDARGS_DEF,
		edd_string prompt,
		edd_int32_array &global_var_ids,
		edd_string option_list);
void std_Funktionsleiste(
		BUILTINS_STDARGS_DEF);
void std_LoadinDevice(
		BUILTINS_STDARGS_DEF);
void std_LoadinPC(
		BUILTINS_STDARGS_DEF);
void std_NodeAddress(
		BUILTINS_STDARGS_DEF);
void std_Statuszeile(
		BUILTINS_STDARGS_DEF);
void std_Update(
		BUILTINS_STDARGS_DEF);

} /* namespace builtins */

#endif /* _edd_builtins_null_h */

