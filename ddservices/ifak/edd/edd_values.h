
/* 
 * $Id: edd_values.h,v 1.2 2008/09/29 21:41:49 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Just include all the edd value types.
 */

#ifndef _edd_values_h
#define _edd_values_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "edd_values_arithmetic.h"
#include "edd_values_string.h"
#include "edd_values_time.h"


namespace edd
{

} /* namespace edd */

#endif /* _edd_values_h */
