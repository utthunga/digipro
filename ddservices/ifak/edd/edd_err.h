
/* 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the errmsg generator tool,
 * written for the EDDL project.
 * 
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/* 
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the error manager class.
 * \note This is a generated file. Any changes you made are lost on the next
 * rebuild.
 */

#ifndef _edd_err_h
#define _edd_err_h

#ifdef _MSC_VER
#pragma warning (disable: 4200)
#pragma warning (disable: 4786)
#endif

// C stdlib

// C++ stdlib
#include <bitset>
#include <exception>
#include <memory>
#include <string>
#include <vector>

// my lib
#include "ident.h"
#include "position.h"
#include "scratch_buf.h"

// own header


namespace edd
{

/**
 * Error languages.
 * This are the available error language classes. They are used to
 * transparently map between different supported languages.
 */
enum err_lang
{
	xxLang_en = 0,
	xxLang_de = 1,
	xxLang_fr = 2,
	xxLang_it = 3,
	xxLang_es = 4,
};

/**
 * Error types.
 * This are the available error type classes. They are used to determine the
 * the specific action and behaviour of the corresponding error code err_code.
 */
enum err_type
{
	xxError = 0,
	xxWarning = 1,
	xxInformation = 2,
	xxRepair = 3,
};

/**
 * Error codes.
 * This are the available error code classes. They encode the specific
 * happened error in an abstract way.
 */
enum err_code
{
	xxUnknown = 0, /**< unknown error%{ (%m0)%} */
	xxMessage = 1, /**< %m0 */
	xxSystem = 2, /**< system: '%m0' */
	xxCpp = 3, /**< preprocessor: %m0 */
	xxOutOfMemory = 4, /**< out of memory */
	xxInternal = 5, /**< internal failure%{ [module %m0]%} */
	xxInternalFailure = 6, /**< internal failure%{ (%m0)%} */
	xxStackUnderflow = 7, /**< stack underflow%{ [module %m0]%} */
	xxNoSuch = 8, /**< no such %m0 */
	xxNoSuchFile = 9, /**< no such file%{ \"%m0\"%} */
	xxTooManyErrors = 10, /**< found more than %m0 errors, abort */
	xxDemoversion = 11, /**< file to long - this is only a demo version */
	xxDemoversionObjects = 12, /**< more than %m0 EDD Objects - this is only a demo version */
	xxDemoNotSupported = 13, /**< this is a demo version, operation not supported */
	xxCannotContinue = 14, /**< cannot continue due to previous errors, abort */
	xxIllegalChar = 15, /**< illegal character%{ %m0%} */
	xxSyntaxError = 16, /**< wrong syntax%{ near token %m0%} */
	xxSyntaxAttributeMissing = 17, /**< missing attribute '%m0'%{ before '%m1'%} is non-conformant */
	xxSyntaxMissing = 18, /**< missing '%m0'%{ before '%m1'%} is non-conformant */
	xxSyntaxMissingAfter = 19, /**< missing '%m0'%{ after '%m1'%} is non-conformant */
	xxSyntaxTrailing = 20, /**< trailing '%m0'%{ at the end of '%m1'%} is non-conformant */
	xxSyntaxLeading = 21, /**< leading '%m0'%{ at the start of '%m1'%} is non-conformant */
	xxSyntaxNonConformant = 22, /**< syntax construct '%m0' is non-conformant */
	xxUsageNonConformant = 23, /**< usage of '%m0' is non-conformant in this context */
	xxRefNonConformant = 24, /**< reference to '%m0' is non-conformant in this context%{, expected %m1 reference%} */
	xxKeywordUsage = 25, /**< using keyword '%m0' as identifier is non-conformant */
	xxMissing = 26, /**< missing %m0 */
	xxAlreadyDefined = 27, /**< %m0 already defined%{ at line %p1%}%{ for %m1%} */
	xxIdentifierAlreadyDefined = 28, /**< identifier '%m0' already defined%{ at line %p1%} */
	xxAttributeAlreadyDefined = 29, /**< attribute '%m0' already defined%{ at line %p1%} */
	xxAttributeMissing = 30, /**< attribute '%m0' missing */
	xxAttributeNotSupported = 31, /**< attribute '%m0' not supported%{ for %m1%} */
	xxAttributeNonConformant = 32, /**< attribute '%m0' non-conformant%{, must be in '%m1' definition%} */
	xxAttributeDependency = 33, /**< attribute '%m0' requires definition of '%m1' */
	xxEmptyAttribute = 34, /**< empty attribute '%m0' is not allowed */
	xxMutuallyExclusiveAttr = 35, /**< attribute '%m0' is mutually exclusive%{ with '%m1'%} */
	xxTypeMismatch = 36, /**< type mismatch%{ for definition of '%m0'%}%{, expected %m1%} */
	xxInvalidList = 37, /**< lists%{ of %m1%} are not allowed for definition of '%m0' */
	xxIdentifierUnknown = 38, /**< identifier '%m0' not declared */
	xxWrongReference = 39, /**< type mismatch in reference%{ to '%m0'%}%{ defined at line %p1%}%{, expected %m1 reference%} */
	xxMixedReferences = 40, /**< mixing references%{ of %m0%} not allowed */
	xxRuntime = 41, /**< runtime error%{ %m0%} */
	xxInvalidInput = 42, /**< invalid input%{ %m0%} */
	xxNotImplemented = 43, /**< function%{ %m0%} not implemented */
	xxDeadlock = 44, /**< circular dependencies%{ for evaluation of %m0%{ from %p1%}%} */
	xxNonLvalue = 45, /**< non-lvalue in%{ %m0%} expr */
	xxNoValue = 46, /**< reference%{ to '%m0'%} isn't a value */
	xxRange = 47, /**< expr out of range%{ in %m0 %} */
	xxNoConstantExpr = 48, /**< no constant expression */
	xxToManyTypes = 49, /**< two or more data types%{ in declaration of '%m0'%} */
	xxInvalidOperand = 50, /**< invalid operand%{ to %m0%} */
	xxInvalidSpecifier = 51, /**< %m0 specifier invalid%{ for '%m1'%} */
	xxConflictingSpecifiers = 52, /**< %m0 are mutually exclusive%{ in specification of '%m1'%} */
	xxTypeCast = 53, /**< cannot convert from '%m0' to '%m1' */
	xxIncompatibleOperands = 54, /**< incompatible operands in%{ %m0%} expression */
	xxNoStruct = 55, /**< request for member '%m0' in something not a structure */
	xxNoSuchMember = 56, /**< %m1 has no member named '%m0' */
	xxNoSuchStructMember = 57, /**< structure has no member named '%m0' */
	xxNoArray = 58, /**< subscripted value%{ '%m0'%} isn't an array */
	xxArraySubscript = 59, /**< array subscript is not an integer */
	xxNegativeArraySize = 60, /**< size of array%{ '%m0'%} is negative */
	xxNullArraySize = 61, /**< size of array%{ '%m0'%} is null */
	xxNoArraySize = 62, /**< size of array%{ '%m0'%} is not specified */
	xxArrayIndex = 63, /**< index%{ %m0%} exceeds range of array%{ '%m1'%{ at line %p1%}%} */
	xxToFewArguments = 64, /**< too few arguments to method%{ '%m0'%{ at line %p1%}%} */
	xxToManyArguments = 65, /**< too many arguments to method%{ '%m0'%{ at line %p1%}%} */
	xxToFewArgumentsBuiltin = 66, /**< too few arguments to builtin function%{ '%m0'%} */
	xxToManyArgumentsBuiltin = 67, /**< too many arguments to builtin function%{ '%m0'%} */
	xxNoMatchingCall = 68, /**< no matching function call to %m0 */
	xxNeedLvalue = 69, /**< need lvalue%{ for argument %m1%} on call by reference to function '%m0' */
	xxNeedArrayRef = 70, /**< need array reference%{ for argument %m1%} on call to function '%m0' */
	xxLocalArgConflict = 71, /**< referencing '%m0' conflicts with local argument%{ defined at line %p1%} */
	xxNoReturn = 72, /**< control reaches end of non-void function */
	xxNoReturnValue = 73, /**< `return' with no value in function returning non-void */
	xxReturnValue = 74, /**< `return' with a value in function returning void */
	xxContinue = 75, /**< continue statement not within a loop */
	xxBreak = 76, /**< break statement not within loop or switch */
	xxCaseLabel = 77, /**< case label not within a switch statement */
	xxCaseLabelExpr = 78, /**< case label does not reduce to an integer constant */
	xxCaseDup = 79, /**< duplicate case value */
	xxCaseDupDefault = 80, /**< multiple default labels in one switch */
	xxSwitchType = 81, /**< switch quantity not an integer */
	xxUninitialized = 82, /**< use of uninitalized variable%{ '%m0'%} */
	xxConstantOverflow = 83, /**< overflow in constant definition */
	xxConstantUnderflow = 84, /**< underflow in constant definition */
	xxNoIntegerConstant = 85, /**< %m0 does not reduce to an integer constant */
	xxNoIntegerExpr = 86, /**< expression does not reduce to an integer */
	xxRefreshReference = 87, /**< refresh relation references local variable%{ '%m0'%} */
	xxUnitReference = 88, /**< Variable '%m0' already in unit relation '%m1'%{ at line %p1%} */
	xxUnitUnitReference = 89, /**< Unit variable '%m0' cannot be part of dependency list in unit relation '%m1'%{ at line %p1%} */
	xxWAOReference = 90, /**< write as one relation references local variable%{ '%m0'%} */
	xxRecordMember = 91, /**< record member '%m0' must reference the same type as at line %p1 */
	xxUnimplementedBuiltin = 92, /**< Builtin '%m0' not implemented */
	xxNonConformantBuiltin = 93, /**< Builtin '%m0' is non-conformant%{, undocumented %m1 extension%} */
	xxUnknownLikeMember = 94, /**< No such%{ %m0%} member in redefinition%{ of '%m1'%} */
	xxNoSuchImport = 95, /**< No such import%{ %m0%} found */
	xxImportMismatch = 96, /**< Identification mismatch in found import %m0 */
	xxObjectAlreadyImported = 97, /**< '%m0' already imported%{ at line %p1%} */
	xxProfileMismatch = 98, /**< Profile of '%m0' don't match the current profile%{ (%m1)%} */
	xxUnsupportedEDDLVersion = 99, /**< EDDL version %m0 not supported */
	xxUnsupportedEDDLProfile = 100, /**< EDDL profile %m0 not supported */
	xxForProfile = 101, /**< %m0 for EDDL profile%{ %m1%} */
	xxNotAllowedForProfile = 102, /**< %m0 not allowed for EDDL profile%{ %m1%} */
	xxOnlyAllowedForProfile = 103, /**< Only %m0 allowed for EDDL profile%{ %m1%} */
	xxNotAllowed = 104, /**< %m0 not allowed%{ for %m1%{ at line %p1%}%} */
	xxEDDx1Enhancement = 105, /**< %m0 is an EDD part 1 enhancement */
	xxEDDx1AttrEnhancement = 106, /**< Attribute '%m0' is an EDD part 1 enhancement */
	xxConstraintEvalNothing = 107, /**< constraint evaluates to nothing */
	xxWrongVersion = 108, /**< wrong version */
	xxWrongIdentification = 109, /**< wrong identification */
	xxKeyPointsValues = 110, /**< Missing X/Y_VALUES in KEY_POINTS declaration */
	xxNoADTRef = 111, /**< EDDL object '%m0'%{ defined at line %p1%} can't be used as type reference due to constraints and/or expressions */
	xxNoValueRef = 112, /**< EDDL object '%m0'%{ defined at line %p1%} can't be used here; it includes references to von-value objects */
	xxEnumStringRefExpression = 113, /**< exactly one integer expression is required when referencing an enumeration string */
	xxNoSuchEnum = 114, /**< no such enumeration value%{ '%m0'%}%{ in VARIABLE '%m1'%{ at line %p1%}%} */
	xxIdentificationUnknown = 115, /**< identification%{ '%m0'%} unknown */
	xxNoArrayIndex = 116, /**< No ARRAY_INDEX available in this context */
	xxUnsupportedImageType = 117, /**< Image type '%m0' is not allowed (only png, jpg, gif) */
	xxImageToLarge = 118, /**< Image file '%m0' is to large%{ (%m1 bytes)%}, HART picture limit is 150.000 bytes */
	xxImagesToLarge = 119, /**< The summary of all picture files exceeds 1.000.000 bytes%{ (currently %m0 bytes)%} */
	xxIllegalDefaultRedef = 120, /**< HART don't allow the redefinition of DEFAULT_VALUE */
	xxNoFloatConstant375 = 121, /**< HART-375 requires floating point constants for FLOAT/DOUBLE VARIABLEs */
	xxMethodUsedAsValue = 122, /**< METHOD%{ '%m0'%} can't be used as value */
	xxNotSupportedBy = 123, /**< %{%m0 %}not supported%{ by %m1%} */
	xxCantResolved = 124, /**< %m0 can not be resolved%{ to %m1%} */
	xxBuiltinKeyword = 125, /**< Keyword '%m0' is a builtin name%{ for %m1; it's not recommended to use it%} */
	xxDivisionByZero = 126, /**< division by zero */
	xxFixedList = 127, /**< Can't insert/delete on fixed-size LIST%{ '%m0'%} */
	xxInsertExceedsCapacity = 128, /**< Insert operation would exceed the capacity%{ of '%m0'%} */
	xxNoIOObject = 129, /**< Object%{ %m0%} can't be used as communication object%{ [%m1]%} */
	xxMaskStart = 130, /**< A new item mask must start on a byte boundary (highest bit(s) set) */
	xxMaskOverlap = 131, /**< Item masks shall not overlap (two or more masks reference the same bit(s)) */
	xxMaskOverflow = 132, /**< Item masks shall not overflow (reference higher bits than the previous mask(s)) */
	xxMaskBitStreamGap = 133, /**< Item mask(s) leave bit-stream gap%{: <%m1>%} */
	xxUnterminatedMask = 134, /**< Unterminated item mask(s)%{: <%m1>%} */
	xxUnknownProtocol = 135, /**< Protocol '%m0' unknown */
	xxNoAddrVar = 136, /**< non-addressing variable '%m0' specified */
	xxRelationType = 137, /**< '%m0' not allowed when ADDRESSING is not specified */
	xxInvalidSize = 138, /**< Invalid size of '%m0'%{ (expected %m1)%} */
	xxUnknownModule = 139, /**< Unknown module '%m0' referenced */
	xxModuleAddrMissing = 140, /**< Variable '%m0' missing%{ in module '%m1'%} */
	xxRelationCount = 141, /**< Module count error in COMPONENT_RELATION '%m0' */
	xxModuleCount = 142, /**< Module count error for '%m0' in COMPONENT_RELATION '%m1' */
	xxNoRelation = 143, /**< No COMPONENT_RELATION found for module '%m0' */
	xxWrongParent = 144, /**< Wrong parent module for '%m0' in relation '%m1' */
	xxParentRelation = 145, /**< Relation '%m0' of COMPONENT '%m1' ignored */
	xxMinMax = 146, /**< MINUMUM_NUMBER is greater than MAXIMUM_NUMBER in COMPONENT_RELATION '%m0' */
	xxDoubleDefined = 147, /**< identifier '%m0' already defined at line %p1 is non-conformant */
	xxNoISOLatin1Char = 148, /**< character%{ '%m0'%} is not an ISO Latin-1 (8859-1) character */
	xxDuplicate = 149, /**< duplicate '%m0' */
	xxImplicitCast = 150, /**< implicit conversion from '%m0' to '%m1' */
	xxShadowLocal = 151, /**< declaration of '%m0' shadows previous local%{ at line %p1%} */
	xxShadowGlobal = 152, /**< declaration of '%m0' shadows global declaration%{ at %p1%} */
	xxShadowBuiltin = 153, /**< declaration of '%m0' shadows builtin method */
	xxShadowParameter = 154, /**< declaration of '%m0' shadows a parameter */
	xxMissingDictionaryEntry = 155, /**< no such dictionary entry '%m0' */
	xxDoubleDictionaryEntry = 156, /**< dictionary entry '%m0' already defined%{ at line %p1%} */
	xxNestedComment = 157,
	xxIdentifierToLong = 158, /**< Identifier to long, only 255 characters are allowed */
	xxUnrepresentableChars = 159, /**< string contain unrepresentable characters%{ for %m0%} */
	xxUnterminatedLanguageTag = 160, /**< unterminated language tag */
	xxUnknownLanguageTag = 161, /**< unknown language tag%{ '%m0'%} */
	xxTelephonLanguageTag = 162, /**< telephone language tags are deprecated%{, please use '%m0' instead '%m1'%} */
	xxDoubleLanguageTag = 163, /**< Language tag '%m0' already defined */
	xxImplicitLanguageTag = 164, /**< Language tag 'en' conflicts with implicit default */
	xxUnterminatedFormat = 165, /**< unterminated%{ '%m0'%} format specifier in string */
	xxUnknownFormatIdentifier = 166, /**< unknown identifier%{ '%m0'%} in format specifier */
	xxAssignment = 167, /**< error in assignment%{ of '%m0'%} */
	xxConstantExpr = 168, /**< constant expr%{ in %m0%}%{; %m1 never reached!%} */
	xxStatementNoEffect = 169, /**< statement with no effect */
	xxCommaNoEffect = 170, /**< left-hand operand of comma expression has no effect */
	xxUnusedVariable = 171, /**< unused variable '%m0' */
	xxUnsupportedEnumSize = 172, /**< (Bit)Enumerations with size greater than 4 are not supported */
	xxEnumValueAlreadyDefined = 173, /**< Enumeration value%{ '%m0'%} already defined%{ at line %p1%}, ignored */
	xxNotReferenced = 174, /**< %m0 not referenced%{ in any %m1%} */
	xxReadableNotReferenced = 175, /**< readable %m0 not referenced%{ in any %m1%} */
	xxWriteableNotReferenced = 176, /**< writeable %m0 not referenced%{ in any %m1%} */
	xxReadonlyReferenced = 177, /**< readonly %m0 referenced%{ in a %m1%} */
	xxWriteonlyReferenced = 178, /**< writeonly %m0 referenced%{ in a %m1%} */
	xxNestedStructAccess = 179, /**< member '%m0' not defined for all possible code paths%{ following '%m1'%} */
	xxNestedArrayAccess = 180, /**< index %m0 not defined for all possible code paths%{ following '%m1'%} */
	xxReferenceArrayElements = 181, /**< in ARRAY '%m0' not all elements have exactly the same arithmetic type%{ following '%m1'%} */
	xxSingleTransBuiltin = 182, /**< Builtin '%m0' used with a multi-transaction COMMAND%{ (%m1%} */
	xxMultiTransBuiltin = 183, /**< Builtin '%m0' used with a single-transaction COMMAND%{ (%m1%} */
	xxEmptyMenu = 184, /**< empty MENU not recommended */
	xxIncorrectlyUsedOnly = 185, /**< %m0 incorrectly used%{, only allowed for %m1%} */
	xxIncorrectlyUsedNot = 186, /**< %m0 incorrectly used%{, not allowed for %m1%} */
	xxDeprecated = 187, /**< %m0 deprecated%{, please use %m1 instead%} */
	xxMixingElements = 188, /**< Mixing %m0 elements%{ with %m1%} not allowed */
	xxUnreferencedObject = 189, /**< Unreferenced object %m0 */
	xxIdentifierShallBe = 190, /**< Special identifier \"%m0\" shall be a %m1 */
	xxMissingNotRecommended = 191, /**< missing %m0 is not recommended */
	xxStringLenBuiltins = 192, /**< string len shall be the maximum len of the string, not zero or negative */
	xxInvalidFormat = 193, /**< invalid %m0 in VARIABLE%{ %m1%} */
	xxInvalid = 194, /**< invalid %m0%{ %m1%} */
	xxNegativeUsed = 195, /**< negative value is incorrect for unsigned type%{ %m0%} */
	xxOverUnderFlow = 196, /**< overflow/underflow error%{ for %m0%} */
	xxTruncated64to32 = 197, /**< Truncated 64bit to 32bit */
	xxAssign = 198, /**< Assigning %m0 is not recommended */
	xxOpenFileRead = 199, /**< Can't open '%m0' for reading%{ [%m1]%} */
	xxOpenFileWrite = 200, /**< Can't open '%m0' for writing%{ [%m1]%} */
	xxOpenDirectory = 201, /**< Can't open directory '%m0' */
	xxStatFile = 202, /**< Can't access file '%m0' */
	xxReadIdentification = 203, /**< Can't read identification of '%m0' */
	xxReadValue = 204, /**< Can't read the value of '%m0' from the saved values */
	xxExpectedTokens = 205, /**< expected %m0 */
	xxTokenFound = 206, /**< found %m0 */
	xxRestartPoint = 207, /**< restart */
	xxCaseFirstDefault = 208, /**< this is the first default label */
	xxWhileDereferencing = 209, /**< While dereferencing '%m0'%{ defined at line %p1%} */
	xxWhileExecuting = 210, /**< While executing %m0%{ from %m1%}  */
	xxWhileSearching = 211, /**< While searching %m0: */
	xxTruncated = 212, /**< Truncated to %m0 */
	xxCandidate = 213, /**< Candidates are: %m0 */
	xxOverwrittenObject = 214, /**< Object '%m0' overwritten by definition at line %p1 */
	xxIgnored = 215, /**< ignored '%m0' */
	xxIgnoredRedefinition = 216, /**< ignored redefinition of '%m0' */
	xxUnexepectedBehaviour = 217, /**< This may result in unexpected behaviour during runtime */
	xxOnlyUsedFrom = 218, /**< %m0 shall only be used from %m1 */
	xxTokenInserted = 219, /**< token inserted */
};

/**
 * Runtime execption class.
 * This exception is thrown in case of a runtime error. A runtime error can
 * happen all the time on evaluating or accessing the AST objects. It's
 * indicates an logical failure inside the EDD specification (circular
 * dependencies, out of range, ...) or failure on AST access (invalid
 * variable, unimplemented builtin and so on).
 * The runtime error can also be converted to a human readable string by the
 * edd::edd_err error manager class.
 * \see edd::edd_err::errmsg
 */
class RuntimeError : public std::exception
{
public:
	~RuntimeError(void) throw() { }

	RuntimeError(void)
	: error(xxRuntime), pos(lib::NoPosition), ident(lib::IDENT::NoIdent) { }

	RuntimeError(err_code e)
	: error(e), pos(lib::NoPosition), ident(lib::IDENT::NoIdent) { }

	RuntimeError(err_code e, const lib::pos &p)
	: error(e), pos(p), ident(lib::IDENT::NoIdent) { }

	RuntimeError(err_code e, const lib::pos &p, const lib::IDENTIFIER &id)
	: error(e), pos(p), ident(id) { }

	const char * what(void) const throw() { return "RuntimeError"; }

	err_code error;
	lib::pos pos;
	lib::IDENTIFIER ident;
};

struct err_msg
{
public:
	std::string msg;
	std::string file;
	unsigned int line;
	unsigned int column;
	unsigned int end_line;
	unsigned int end_column;

	err_msg()
		: msg(""), file(""), line(0), column(0), end_line(line), end_column(column)
	{
	}
};

/**
 * The error manager class.
 * It's used to manage all detected errors in an easy way. The error object
 * can enqueue error messages for a later review. It only store the type
 * class, code class and error specific data like the pos and corresponding
 * lib::IDENTIFIER. The error class also know all human encoded error messages and
 * the available translations for it.
 */
class edd_err
{
public:
	edd_err(lib::IDENT *);
	~edd_err(void);

	/**
	 * Clear the message chain.
	 * This delete all enqueued error messages and release the
	 * corresponding ressources.
	 */
	void clear(void) throw();

	/**
	 * Flush the message chain.
	 * This print out all error messages in the message chain to the File
	 * stream f. The messages are printed in the specified language lang.
	 * Every printed error message is removed from the message chain. The
	 * corresponding ressources are released too.
	 */
	void fflush(lib::stream &f, err_lang lang);

	/**
	 * Flushes the message chain. 
	 * This prints out all error messages in the message chain to a 
	 * std::vector. The messages are printed in the specified language
	 * lang. Every printed error message is removed from the message chain.
	 * The corresponding ressources are releases, too. 
	*/
	std::auto_ptr<std::vector<err_msg> > vflush(err_lang lang, err_type type = xxError);

	/**
	 * Get message counter.
	 * Return the number of all messages that are enqueued into the
	 * message chain.
	 */
	size_t get_msgCount(void) const throw() { return msgCount; }

	/**
	 * Get hidden message counter.
	 * Return the number of all messages that are supressed (supress
	 * all warnings, supress specific warning).
	 */
	size_t get_msgHidden(void) const throw() { return msgHidden; }

	/**
	 * Get error counter.
	 * Return the number of the error or fatal messages that are enqueued
	 * into the message chain. This value can be less than the message
	 * number as not all messages are real error messages.
	 * \see err_type for the available error type classes.
	 */
	size_t get_errCount(void) const throw() { return errCount; }

	/**
	 * Get unreported error counter.
	 * Return the number of all errors that are discarded due to
	 * errThreshold limit reached.
	 */
	size_t get_errUnreported(void) const throw() { return errUnreported; }

	/**
	 * Warning filter interface class.
	 */
	class warning_filter
	{
	public:
		virtual ~warning_filter(void);
		virtual bool operator()(err_code, const lib::pos &, const lib::IDENTIFIER &) = 0;
	};
	typedef class warning_filter warning_filter_t;

	/**
	 * Set custom warning filter.
	 * The filter functor shall return false if a warning shall be
	 * filtered.
	 */
	void set_warning_filter(std::auto_ptr<warning_filter_t>);

	/**
	 * \internal
	 * \name Message enqueue routines.
	 * These routines enqueues the message and related informations into
	 * the message chain.
	 */
	//! \{
	void error		(err_code, const lib::pos &, const lib::IDENTIFIER &);
	void error		(err_code, const lib::pos &, const lib::IDENTIFIER &,
					   const lib::pos &, const lib::IDENTIFIER &);

	void warning		(err_code, const lib::pos &, const lib::IDENTIFIER &, bool = false);
	void warning		(err_code, const lib::pos &, const lib::IDENTIFIER &,
					   const lib::pos &, const lib::IDENTIFIER &, bool = false);

	void information	(err_code, const lib::pos &, const lib::IDENTIFIER &, bool = false);
	void information	(err_code, const lib::pos &, const lib::IDENTIFIER &,
					   const lib::pos &, const lib::IDENTIFIER &, bool = false);

	void repair		(err_code, const lib::pos &, const lib::IDENTIFIER &, bool = false);
	void repair		(err_code, const lib::pos &, const lib::IDENTIFIER &,
					   const lib::pos &, const lib::IDENTIFIER &, bool = false);

	void error		(const RuntimeError &);
	void error		(const std::exception &);

	void warning		(const RuntimeError &);
	void warning		(const std::exception &);
	//! \}

	/**
	 * \name Support routines.
	 * Routines to directly print a message without clobbering the
	 * internal buffering and chaining.
	 */
	//! \{
	std::string errmsg(err_lang, const RuntimeError &) const;
	std::string errmsg(err_lang, err_code, const lib::pos &, const lib::IDENTIFIER &) const;
	std::string errmsg(err_lang, err_code, const lib::pos &, const lib::IDENTIFIER &,
					       const lib::pos &, const lib::IDENTIFIER &) const;
	//! \}

	/**
	 * \name Helper routines.
	 * Convert language id or telephone encoding into the abstract
	 * language representation err_lang.
	 */
	//! \{
	static bool id_to_lang(const char *id, enum err_lang &lang) throw();
	static bool tel_to_lang(const char *tel, enum err_lang &lang) throw();
	//! \}

	void config_errthreshold(size_t threshold = 50) throw() { errThreshold = threshold; }
	void config_warn(bool onoff = true) throw() { warn = onoff; }
	void config_code(bool onoff, err_code);

	static size_t languages_size(void) throw() { return lang_size; }
	static std::vector<enum err_lang> languages(void);
	static std::vector<enum err_code> suppressible_codes(void);
	static const char *language_descr(enum err_lang) throw();
	static const char *message_descr(enum err_code) throw();
	static const char *code_to_id(enum err_code) throw();
	static enum err_lang descr_to_lang(const std::string &);

private:
	void init(void);

	static int           lang_size;		/* number of languages */
	static int           lang_types_size;
	static int           lang_codes_size;
	static const char  **lang_descr;	/* language in readable form */
	static const char ***lang_types;	/* error types */
	static const char ***lang_codes;	/* error codes */
	static const char  **lang_codes_descr;	/* error codes descr */
	static const char   *lang_codes_flags;	/* error codes flags */

	/**
	 * \internal
	 * The identifier manager.
	 * The lib::IDENT object there all given lib::IDENTIFIER objects belong too.
	 */
	lib::IDENT *ident;

	size_t msgCount, msgHidden;
	size_t errCount, errUnreported;
	size_t errThreshold;
	bool skip_append;

	struct msg
	{
		struct msg *next;
		struct msg *chain;

		err_type type;
		err_code code;

		size_t size;
		lib::pos info_pos[2];
		lib::IDENTIFIER info_msg[2];
	};

	void free_element(struct msg *);

	struct msg *root;
	struct msg *last;

	bool warn;
	std::bitset<220> disabled;
	std::auto_ptr<warning_filter_t> warning_filter;

	void put_checked(err_type, err_code, size_t, lib::pos *, lib::IDENTIFIER *, bool);
	void put(err_type, err_code, size_t, const lib::pos *, const lib::IDENTIFIER *, bool);

	std::string getmsg(const struct msg &, err_lang) const;
	void flush_msg(const struct msg *, err_lang, lib::scratch_buf &, lib::stream &) const;
	void vflush_msg(const struct msg *, err_lang, std::vector<err_msg> *) const;
	void getmsg(const struct msg &, err_lang, lib::scratch_buf &) const;
	void err_sprintf(lib::scratch_buf &, const char *fmt, const struct msg &) const;
};

} /* namespace edd */

#endif /* _edd_err_h */
