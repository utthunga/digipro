
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the units generator tool,
 * written for the EDDL project.
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_units_si_table_h
#define _edd_units_si_table_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "edd_operand_interface.h"


namespace edd
{

void unit_conversion(int src, int dst, operand_ptr vars);

/**
 * \page unit_conversions Supported unit conversions
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{m} \mbox{s}^2}}\f$ [factor conversion]
 *   - inH2O_68degF (1)
 *   - inHg_0degC (2)
 *   - mmHg_0degC (5)
 *   - psi (6)
 *   - bar (7)
 *   - mbar (8)
 *   - Pa (11)
 *   - kPa (12)
 *   - torr (13)
 *   - atm (14)
 *   - MPa (237)
 *   - inH2O_4degC (238)
 *   - mmH2O_4degC (239)
 *   - Pa (1130)
 *   - GPa (1131)
 *   - MPa (1132)
 *   - kPa (1133)
 *   - mPa (1134)
 *   - microPa (1135)
 *   - hPa (1136)
 *   - bar (1137)
 *   - mbar (1138)
 *   - torr (1139)
 *   - atm (1140)
 *   - psi (1141)
 *   - inH2O_4degC (1147)
 *   - inH2O_68degF (1148)
 *   - mmH2O_4degC (1150)
 *   - inHg_0degC (1156)
 *   - mmHg_0degC (1158)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{m}^2}}\f$ [factor conversion]
 *   - gm/cm2 (9)
 *   - kg/cm2 (10)
 *   - gm/cm2 (1144)
 *   - kg/cm2 (1145)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^3}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - feet3/min (15)
 *   - gal_US_liq/min (16)
 *   - liter/min (17)
 *   - impGal/min (18)
 *   - m3/hr (19)
 *   - gal_US_liq/s (22)
 *   - megagal_US_liq/day (23)
 *   - liter/s (24)
 *   - megaliter/day (25)
 *   - feet3/s (26)
 *   - feet3/day (27)
 *   - m3/s (28)
 *   - m3/day (29)
 *   - impGal/hr (30)
 *   - impGal/day (31)
 *   - m3/hr (121)
 *   - liter/hr (122)
 *   - feet3/min (123)
 *   - feet3/hr (130)
 *   - m3/min (131)
 *   - bbl_US_petrol/s (132)
 *   - bbl_US_petrol/min (133)
 *   - bbl_US_petrol/hr (134)
 *   - bbl_US_petrol/day (135)
 *   - gal_US_liq/hr (136)
 *   - impGal/s (137)
 *   - liter/hr (138)
 *   - gal_US_liq/day (235)
 *   - m3/s (1347)
 *   - m3/min (1348)
 *   - m3/hr (1349)
 *   - m3/day (1350)
 *   - liter/s (1351)
 *   - liter/min (1352)
 *   - liter/hr (1353)
 *   - liter/day (1354)
 *   - Mliter/day (1355)
 *   - ft3/s (1356)
 *   - ft3/hr (1358)
 *   - ft3/day (1359)
 *   - gal_US_liq/s (1362)
 *   - gal_US_liq/min (1363)
 *   - gal_US_liq/hr (1364)
 *   - gal_US_liq/day (1365)
 *   - Mgal_US_liq/day (1366)
 *   - impGal/s (1367)
 *   - impGal/min (1368)
 *   - impGal/hr (1369)
 *   - impGal/day (1370)
 *   - bbl_US_petrol/s (1371)
 *   - bbl_US_petrol/min (1372)
 *   - bbl_US_petrol/hr (1373)
 *   - bbl_US_petrol/day (1374)
 *   - mgal_US_liq/s (1448)
 *   - microgal_US_liq/s (1449)
 *   - kgal_US_liq/s (1450)
 *   - Mgal_US_liq/s (1451)
 *   - mgal_US_liq/min (1452)
 *   - microgal_US_liq/min (1453)
 *   - kgal_US_liq/min (1454)
 *   - Mgal_US_liq/min (1455)
 *   - microgal_US_liq/hr (1456)
 *   - mgal_US_liq/hr (1457)
 *   - kgal_US_liq/hr (1458)
 *   - Mgal_US_liq/hr (1459)
 *   - mgal_US_liq/day (1460)
 *   - microgal_US_liq/day (1461)
 *   - kgal_US_liq/day (1462)
 *   - microimpGal/s (1463)
 *   - mimpGal/s (1464)
 *   - kimpGal/s (1465)
 *   - MimpGal/s (1466)
 *   - microimpGal/min (1467)
 *   - mimpGal/min (1468)
 *   - kimpGal/min (1469)
 *   - MimpGal/min (1470)
 *   - microimpGal/hr (1471)
 *   - mimpGal/hr (1472)
 *   - kimpGal/hr (1473)
 *   - MimpGal/hr (1474)
 *   - microimpGal/day (1475)
 *   - mimpGal/day (1476)
 *   - kimpGal/day (1477)
 *   - MimpGal/day (1478)
 *   - microbbl_US_petrol/s (1479)
 *   - mbbl_US_petrol/s (1480)
 *   - kbbl_US_petrol/s (1481)
 *   - Mbbl_US_petrol/s (1482)
 *   - microbbl_US_petrol/min (1483)
 *   - mbbl_US_petrol/min (1484)
 *   - kbbl_US_petrol/min (1485)
 *   - Mbbl_US_petrol/min (1486)
 *   - microbbl_US_petrol/hr (1487)
 *   - mbbl_US_petrol/hr (1488)
 *   - kbbl_US_petrol/hr (1489)
 *   - Mbbl_US_petrol/hr (1490)
 *   - microbbl_US_petrol/day (1491)
 *   - mbbl_US_petrol/day (1492)
 *   - kbbl_US_petrol/day (1493)
 *   - Mbbl_US_petrol/day (1494)
 *   - microm3/s (1495)
 *   - mm3/s (1496)
 *   - km3/s (1497)
 *   - Mm3/s (1498)
 *   - microm3/min (1499)
 *   - mm3/min (1500)
 *   - km3/min (1501)
 *   - Mm3/min (1502)
 *   - microm3/hr (1503)
 *   - mm3/hr (1504)
 *   - km3/hr (1505)
 *   - Mm3/hr (1506)
 *   - microm3/day (1507)
 *   - mm3/day (1508)
 *   - km3/day (1509)
 *   - Mm3/day (1510)
 *   - cm3/s (1511)
 *   - cm3/min (1512)
 *   - cm3/hr (1513)
 *   - cm3/day (1514)
 *   - kliter/min (1518)
 *   - kliter/hr (1519)
 *   - kliter/day (1520)
 *   - ml/min (1563)
 *   - ml/s (1577)
 *   - ml/hr (1578)
 *   - ml/day (1579)
 *   - af/s (1580)
 *   - af/min (1581)
 *   - af/hr (1582)
 *   - af/day (1583)
 *   - fl_oz_US/s (1584)
 *   - fl_oz_US/min (1585)
 *   - fl_oz_US/hr (1586)
 *   - fl_oz_US/day (1587)
 *   - hl/s (1633)
 *   - hl/min (1634)
 *   - hl/hr (1635)
 *   - hl/day (1636)
 *   - bbl_US_liq/s (1637)
 *   - bbl_US_liq/min (1638)
 *   - bbl_US_liq/hr (1639)
 *   - bbl_US_liq/day (1640)
 *   - bbl_US_fed/s (1642)
 *   - bbl_US_fed/min (1643)
 *   - bbl_US_fed/hr (1644)
 *   - bbl_US_fed/day (1645)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - feet/s (20)
 *   - m/s (21)
 *   - in/s (114)
 *   - in/min (115)
 *   - feet/min (116)
 *   - m/hr (120)
 *   - m/s (1061)
 *   - mm/s (1062)
 *   - m/hr (1063)
 *   - km/hr (1064)
 *   - knot (1065)
 *   - in/s (1066)
 *   - ft/s (1067)
 *   - yd/s (1068)
 *   - in/min (1069)
 *   - ft/min (1070)
 *   - yd/min (1071)
 *   - in/hr (1072)
 *   - ft/hr (1073)
 *   - yd/hr (1074)
 *   - mph (1075)
 * 
 * - SI normalized: \f${\displaystyle\mbox{K}}\f$ [rule conversion]
 *   - degC (32)
 *   - degF (33)
 *   - degR (34)
 *   - K (35)
 *   - K (1000)
 *   - degC (1001)
 *   - degF (1002)
 *   - degR (1003)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{A}}}\f$ [factor conversion]
 *   - mV (36)
 *   - V (58)
 *   - V (1240)
 *   - MV (1241)
 *   - kV (1242)
 *   - mV (1243)
 *   - microV (1244)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{A}^2}}\f$ [factor conversion]
 *   - Ohm (37)
 *   - kOhm (163)
 *   - W (1186)
 *   - TW (1187)
 *   - GW (1188)
 *   - MW (1189)
 *   - kW (1190)
 *   - mW (1191)
 *   - microW (1192)
 *   - nW (1193)
 *   - pW (1194)
 *   - Ohm (1281)
 *   - GOhm (1282)
 *   - MOhm (1283)
 *   - kOhm (1284)
 *   - mOhm (1285)
 *   - microOhm (1286)
 * 
 * - SI normalized: \f$\frac{{\displaystyle1}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - Hz (38)
 *   - Hz (1077)
 *   - THz (1078)
 *   - GHz (1079)
 *   - MHz (1080)
 *   - kHz (1081)
 *   - 1/s (1082)
 *   - 1/min (1083)
 *   - Bq (1402)
 *   - MBq (1403)
 *   - kBq (1404)
 * 
 * - SI normalized: \f${\displaystyle\mbox{A}}\f$ [factor conversion]
 *   - mA (39)
 *   - A (1209)
 *   - kA (1210)
 *   - mA (1211)
 *   - microA (1212)
 *   - nA (1213)
 *   - pA (1214)
 * 
 * - SI normalized: \f${\displaystyle\mbox{m}^3}\f$ [factor conversion]
 *   - gal_US_liq (40)
 *   - liter (41)
 *   - impGal (42)
 *   - m3 (43)
 *   - bbl_US_petrol (46)
 *   - bu_US_dry (110)
 *   - yard3 (111)
 *   - feet3 (112)
 *   - inch3 (113)
 *   - bbl_US_liq (124)
 *   - hectoliter (236)
 *   - m3 (1034)
 *   - dm3 (1035)
 *   - cm3 (1036)
 *   - mm3 (1037)
 *   - liter (1038)
 *   - cl (1039)
 *   - ml (1040)
 *   - hl (1041)
 *   - in3 (1042)
 *   - ft3 (1043)
 *   - yd3 (1044)
 *   - mile3 (1045)
 *   - pt_US_liq (1046)
 *   - qt_US_liq (1047)
 *   - gal_US_liq (1048)
 *   - impGal (1049)
 *   - bu_US_dry (1050)
 *   - bbl_US_petrol (1051)
 *   - bbl_US_liq (1052)
 *   - kliter (1517)
 *   - fl_oz_US (1570)
 *   - cm3 (1571)
 *   - af (1572)
 *   - bbl_US_fed (1641)
 * 
 * - SI normalized: \f${\displaystyle\mbox{m}}\f$ [factor conversion]
 *   - feet (44)
 *   - m (45)
 *   - inch (47)
 *   - cm (48)
 *   - mm (49)
 *   - m (1010)
 *   - km (1011)
 *   - cm (1012)
 *   - mm (1013)
 *   - microm (1014)
 *   - nm (1015)
 *   - pm (1016)
 *   - angstrom (1017)
 *   - ft (1018)
 *   - in (1019)
 *   - yd (1020)
 *   - mile (1021)
 *   - nmile (1022)
 * 
 * - SI normalized: \f${\displaystyle\mbox{s}}\f$ [factor conversion]
 *   - min (50)
 *   - s (51)
 *   - hr (52)
 *   - day (53)
 *   - us (56)
 *   - s (1054)
 *   - ms (1056)
 *   - us (1057)
 *   - min (1058)
 *   - hr (1059)
 *   - day (1060)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - cSt (54)
 *   - m2/s (1160)
 *   - St (1163)
 *   - cSt (1164)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{m} \mbox{s}}}\f$ [factor conversion]
 *   - centiPo (55)
 *   - Pa-s (1159)
 *   - Po (1161)
 *   - cPo (1162)
 * 
 * - SI normalized: \f${\displaystyle\mbox{kg}}\f$ [factor conversion]
 *   - gm (60)
 *   - kg (61)
 *   - metricton (62)
 *   - pound (63)
 *   - shortton (64)
 *   - longton (65)
 *   - ounce (125)
 *   - kg (1088)
 *   - gm (1089)
 *   - mg (1090)
 *   - Mgm (1091)
 *   - metricton (1092)
 *   - oz (1093)
 *   - lb (1094)
 *   - shortton (1095)
 *   - longton (1096)
 *   - ct (1567)
 *   - lb_tr (1568)
 *   - oz_tr (1569)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s}^3 \mbox{A}^2}}{{\displaystyle\mbox{m}^3 \mbox{kg}}}\f$ [factor conversion]
 *   - mS/cm (66)
 *   - microS/cm (67)
 *   - S/m (1299)
 *   - MS/m (1300)
 *   - kS/m (1301)
 *   - mS/cm (1302)
 *   - microS/mm (1303)
 *   - S/cm (1551)
 *   - microS/cm (1552)
 *   - mS/m (1553)
 *   - microS/m (1554)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m} \mbox{kg}}}{{\displaystyle\mbox{s}^2}}\f$ [factor conversion]
 *   - N (68)
 *   - N (1120)
 *   - MN (1121)
 *   - kN (1122)
 *   - mN (1123)
 *   - microN (1124)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^2}}\f$ [factor conversion]
 *   - N-m (69)
 *   - decatherm (89)
 *   - ft-lbf (126)
 *   - kWh (128)
 *   - Mcal_therm (162)
 *   - MJ (164)
 *   - Btu_therm (165)
 *   - N-m (1126)
 *   - MN-m (1127)
 *   - kN-m (1128)
 *   - mN-m (1129)
 *   - J (1167)
 *   - EJ (1168)
 *   - PJ (1169)
 *   - TJ (1170)
 *   - GJ (1171)
 *   - MJ (1172)
 *   - kJ (1173)
 *   - mJ (1174)
 *   - Wh (1175)
 *   - TWh (1176)
 *   - GWh (1177)
 *   - MWh (1178)
 *   - kWh (1179)
 *   - cal_therm (1180)
 *   - kcal_therm (1181)
 *   - Mcal_therm (1182)
 *   - Btu_therm (1183)
 *   - decatherm (1184)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - gm/s (70)
 *   - gm/min (71)
 *   - gm/hr (72)
 *   - kg/s (73)
 *   - kg/min (74)
 *   - kg/hr (75)
 *   - kg/day (76)
 *   - metricton/min (77)
 *   - metricton/hr (78)
 *   - metricton/day (79)
 *   - pound/s (80)
 *   - pound/min (81)
 *   - pound/hr (82)
 *   - pound/day (83)
 *   - shortton/min (84)
 *   - shortton/hr (85)
 *   - shortton/day (86)
 *   - longton/hr (87)
 *   - longton/day (88)
 *   - gm/s (1318)
 *   - gm/min (1319)
 *   - gm/hr (1320)
 *   - gm/day (1321)
 *   - kg/s (1322)
 *   - kg/min (1323)
 *   - kg/hr (1324)
 *   - kg/day (1325)
 *   - metricton/s (1326)
 *   - metricton/min (1327)
 *   - metricton/hr (1328)
 *   - metricton/day (1329)
 *   - lb/s (1330)
 *   - lb/min (1331)
 *   - lb/hr (1332)
 *   - lb/day (1333)
 *   - shortton/s (1334)
 *   - shortton/min (1335)
 *   - shortton/hr (1336)
 *   - shortton/day (1337)
 *   - longton/s (1338)
 *   - longton/min (1339)
 *   - longton/hr (1340)
 *   - longton/day (1341)
 *   - N-s/m (1380)
 *   - oz/s (1606)
 *   - oz/min (1607)
 *   - oz/hr (1608)
 *   - oz/day (1609)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{m}^3}}\f$ [factor conversion]
 *   - gm/cm3 (91)
 *   - kg/m3 (92)
 *   - pound/gal_US_liq (93)
 *   - pound/feet3 (94)
 *   - gm/ml (95)
 *   - kg/liter (96)
 *   - gm/liter (97)
 *   - pound/inch3 (98)
 *   - shortton/yd3 (99)
 *   - kg/m3 (1097)
 *   - Mgm/m3 (1098)
 *   - kg/dm3 (1099)
 *   - gm/cm3 (1100)
 *   - gm/m3 (1101)
 *   - metricton/m3 (1102)
 *   - kg/liter (1103)
 *   - gm/ml (1104)
 *   - gm/liter (1105)
 *   - lb/in3 (1106)
 *   - lb/ft3 (1107)
 *   - lb/gal_US_liq (1108)
 *   - shortton/yd3 (1109)
 *   - lb/impGal (1430)
 *   - mg/liter (1558)
 *   - microgm/liter (1559)
 *   - mg/dm3 (1564)
 *   - mg/liter (1565)
 *   - mg/m3 (1566)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{rad}}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - degree/s (117)
 *   - rev/s (118)
 *   - rev/s (1084)
 *   - rad/s (1086)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{rad}}}{{\displaystyle\mbox{m}}}\f$ [factor conversion]
 *   - rpm (119)
 *   - rpm (1085)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^3}}\f$ [factor conversion]
 *   - kw (127)
 *   - HP_550_ftlbf (129)
 *   - Mcal_therm/hr (140)
 *   - MJ/hr (141)
 *   - Btu_therm/hr (142)
 *   - Mcal_therm/hr (1195)
 *   - MJ/hr (1196)
 *   - Btu_therm/hr (1197)
 *   - HP_550_ftlbf (1198)
 *   - kcal_therm/s (1431)
 *   - kcal_therm/min (1432)
 *   - kcal_therm/hr (1433)
 *   - kcal_therm/day (1434)
 *   - Mcal_therm/s (1435)
 *   - Mcal_therm/min (1436)
 *   - Mcal_therm/day (1437)
 *   - kJ/s (1438)
 *   - kJ/min (1439)
 *   - kJ/hr (1440)
 *   - kJ/day (1441)
 *   - MJ/s (1442)
 *   - MJ/min (1443)
 *   - MJ/day (1444)
 *   - Btu_therm/s (1445)
 *   - Btu_therm/min (1446)
 *   - Btu_therm/day (1447)
 * 
 * - SI normalized: \f${\displaystyle\mbox{ppm}}\f$ [factor conversion]
 *   - ppm (139)
 *   - ppm (1423)
 *   - ppb (1424)
 *   - ppth (1425)
 *   - ppt (1632)
 * 
 * - SI normalized: \f${\displaystyle\mbox{rad}}\f$ [factor conversion]
 *   - degree (143)
 *   - rad (1004)
 *   - degree (1005)
 *   - arcmin (1006)
 *   - arcsec (1007)
 *   - gon (1008)
 *   - rev (1009)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s}^4 \mbox{A}^2}}{{\displaystyle\mbox{m}^2 \mbox{kg}}}\f$ [factor conversion]
 *   - pF (153)
 *   - F (1245)
 *   - mF (1246)
 *   - microF (1247)
 *   - nF (1248)
 *   - pF (1249)
 * 
 * - SI normalized: \f${\displaystyle\mbox{mol}}\f$ [factor conversion]
 *   - SCM (166)
 *   - SCL (167)
 *   - SCF (168)
 *   - SCF (1053)
 *   - mol (1384)
 *   - kmol (1385)
 *   - mmol (1386)
 *   - micromol (1387)
 * 
 * - SI normalized: \f${\displaystyle\mbox{m}^2}\f$ [factor conversion]
 *   - m2 (1023)
 *   - km2 (1024)
 *   - cm2 (1025)
 *   - dm2 (1026)
 *   - mm2 (1027)
 *   - are (1028)
 *   - ha (1029)
 *   - in2 (1030)
 *   - ft2 (1031)
 *   - yd2 (1032)
 *   - mile2 (1033)
 *   - ft3/m (1357)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{m}}}\f$ [factor conversion]
 *   - kg/m (1115)
 *   - mg/m (1116)
 *   - tex (1117)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{s}^2}}\f$ [factor conversion]
 *   - N/m (1165)
 *   - mN/m (1166)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{K}}}\f$ [factor conversion]
 *   - J/K (1202)
 *   - kJ/K (1203)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2}}{{\displaystyle\mbox{s}^2 \mbox{K}}}\f$ [factor conversion]
 *   - J/kg-K (1204)
 *   - kJ/kg-K (1205)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2}}{{\displaystyle\mbox{s}^2}}\f$ [factor conversion]
 *   - J/kg (1206)
 *   - MJ/kg (1207)
 *   - kJ/kg (1208)
 *   - Gy (1408)
 *   - mGy (1409)
 *   - cGy (1410)
 *   - Sv (1411)
 *   - mSv (1412)
 *   - rem (1413)
 *   - kcal_therm/kg (1515)
 *   - Btu_therm/lb (1516)
 * 
 * - SI normalized: \f${\displaystyle\mbox{s} \mbox{A}}\f$ [factor conversion]
 *   - C (1215)
 *   - MC (1216)
 *   - kC (1217)
 *   - microC (1218)
 *   - nC (1219)
 *   - pC (1220)
 *   - A-hr (1221)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s} \mbox{A}}}{{\displaystyle\mbox{m}^3}}\f$ [factor conversion]
 *   - C/m3 (1222)
 *   - C/mm3 (1223)
 *   - C/cm3 (1224)
 *   - kC/m3 (1225)
 *   - mC/m3 (1226)
 *   - microC/m3 (1227)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s} \mbox{A}}}{{\displaystyle\mbox{m}^2}}\f$ [factor conversion]
 *   - C/m2 (1228)
 *   - C/mm2 (1229)
 *   - C/cm2 (1230)
 *   - kC/m2 (1231)
 *   - mC/m2 (1232)
 *   - microC/m2 (1233)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m} \mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{A}}}\f$ [factor conversion]
 *   - V/m (1234)
 *   - MV/m (1235)
 *   - kV/m (1236)
 *   - V/cm (1237)
 *   - mV/m (1238)
 *   - microV/m (1239)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s}^4 \mbox{A}^2}}{{\displaystyle\mbox{m}^3 \mbox{kg}}}\f$ [factor conversion]
 *   - F/m (1250)
 *   - microF/m (1251)
 *   - nF/m (1252)
 *   - pF/m (1253)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{A}}}{{\displaystyle\mbox{m}^2}}\f$ [factor conversion]
 *   - A/m2 (1255)
 *   - MA/m2 (1256)
 *   - A/cm2 (1257)
 *   - kA/m2 (1258)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{A}}}{{\displaystyle\mbox{m}}}\f$ [factor conversion]
 *   - A/m (1259)
 *   - kA/m (1260)
 *   - A/cm (1261)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}}}\f$ [factor conversion]
 *   - T (1262)
 *   - mT (1263)
 *   - microT (1264)
 *   - nT (1265)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}}}\f$ [factor conversion]
 *   - Wb (1266)
 *   - mWb (1267)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m} \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}}}\f$ [factor conversion]
 *   - Wb/m (1268)
 *   - kWb/m (1269)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}^2}}\f$ [factor conversion]
 *   - H (1270)
 *   - mH (1271)
 *   - microH (1272)
 *   - nH (1273)
 *   - pH (1274)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m} \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}^2}}\f$ [factor conversion]
 *   - H/m (1275)
 *   - microH/m (1276)
 *   - nH/m (1277)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^3 \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{A}}}\f$ [factor conversion]
 *   - N-m2/A (1279)
 *   - Wb-m (1280)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s}^3 \mbox{A}^2}}{{\displaystyle\mbox{m}^2 \mbox{kg}}}\f$ [factor conversion]
 *   - S (1287)
 *   - kS (1288)
 *   - mS (1289)
 *   - microS (1290)
 *   - PS (1631)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^3 \mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{A}^2}}\f$ [factor conversion]
 *   - Ohm-m (1291)
 *   - GOhm-m (1292)
 *   - MOhm-m (1293)
 *   - kOhm-m (1294)
 *   - Ohm-cm (1295)
 *   - mOhm-m (1296)
 *   - microOhm-m (1297)
 *   - nOhm-m (1298)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{A}^2}}\f$ [factor conversion]
 *   - W/m2 (1308)
 *   - W/m2 (1375)
 *   - mW/m2 (1376)
 *   - microW/m2 (1377)
 *   - pW/m2 (1378)
 * 
 * - SI normalized: \f${\displaystyle\mbox{s} \mbox{cd} \mbox{sr}}\f$ [factor conversion]
 *   - lm-s (1310)
 *   - lm-hr (1311)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{cd} \mbox{sr}}}{{\displaystyle\mbox{m}^2}}\f$ [factor conversion]
 *   - lm/m2 (1312)
 *   - lx (1314)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{mol}}}{{\displaystyle\mbox{s}}}\f$ [factor conversion]
 *   - SCF/min (1360)
 *   - SCF/hr (1361)
 * 
 * - SI normalized: \f${\displaystyle\mbox{B}}\f$ [factor conversion]
 *   - B (1382)
 *   - dB (1383)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{kg}}}{{\displaystyle\mbox{mol}}}\f$ [factor conversion]
 *   - kg/mol (1388)
 *   - gm/mol (1389)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^3}}{{\displaystyle\mbox{mol}}}\f$ [factor conversion]
 *   - m3/mol (1390)
 *   - dm3/mol (1391)
 *   - cm3/mol (1392)
 *   - liter/mol (1393)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^2 \mbox{mol}}}\f$ [factor conversion]
 *   - J/mol (1394)
 *   - kJ/mol (1395)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{mol}}}{{\displaystyle\mbox{m}^3}}\f$ [factor conversion]
 *   - mol/m3 (1397)
 *   - mol/dm3 (1398)
 *   - mol/liter (1399)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{mol}}}{{\displaystyle\mbox{kg}}}\f$ [factor conversion]
 *   - mol/kg (1400)
 *   - mmol/kg (1401)
 * 
 * - SI normalized: \f$\frac{{\displaystyle1}}{{\displaystyle\mbox{kg} \mbox{s}}}\f$ [factor conversion]
 *   - Bq/kg (1405)
 *   - kBq/kg (1406)
 *   - MBq/kg (1407)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{s} \mbox{A}}}{{\displaystyle\mbox{kg}}}\f$ [factor conversion]
 *   - C/kg (1414)
 *   - mC/kg (1415)
 *   - R (1416)
 * 
 * - SI normalized: \f$\frac{{\displaystyle\mbox{m}^2 \mbox{kg}}}{{\displaystyle\mbox{s}^3 \mbox{K} \mbox{A}}}\f$ [factor conversion]
 *   - V/K (1420)
 *   - mV/K (1421)
 * 
 */

} /* namespace edd */

#endif /* _edd_units_si_table_h */

