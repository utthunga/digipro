
/* 
 * $Id: edd_cpp.cxx,v 1.22 2006/09/30 17:56:13 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_cpp.h"

// C stdlib
#include <stdarg.h>

// C++ stdlib

// my lib
#include "ident.h"
#include "paths.h"
#include "port.h"
#include "tmpfile.h"
#include "vsnprintf.h"
#include "sys.h"

// own header
#include "edd_err.h"

using namespace ucpp;


namespace edd
{

edd_cpp::edd_cpp(lib::IDENT *i, class edd_err *e, bool pdm)
: ident(i), err(e), pdm_mode(pdm)
{
}

FILE *
edd_cpp::run(FILE *file,
	     const char *filename,
	     const std::vector<std::string> &includes,
	     const std::vector<std::string> &defines,
	     std::set<std::string> &included_files)
{
	struct cpp::lexer_state _ls;
	struct cpp::lexer_state *ls = &_ls;
	FILE *tmp;
	size_t i;
	int r, fr = 0;

	init_lexer_state(ls);

	ls->flags = cpp::DEFAULT_CPP_FLAGS;
	ls->flags |= cpp::WARN_TRIGRAPHS;
	ls->flags |= cpp::WARN_TRIGRAPHS_MORE;
	ls->flags |= cpp::WARN_ANNOYING;

	/* don't discard comments */
	ls->flags &= ~cpp::DISCARD_COMMENTS;

	if (pdm_mode)
	{
		ls->flags |= cpp::PDM_MODE;
		ls->flags |= cpp::APPEND_NEWLINE;
	}

	define_macro(ls, "_EDDY_=1");

	/* additional initializations */
	for (i = 0; i < includes.size(); i++)
		add_incpath(includes[i].c_str());
	for (i = 0; i < defines.size(); i++)
		define_macro(ls, defines[i].c_str());

	/* input */
	ls->input = file;
	set_init_filename(filename, 1);

	/* output stream */
#if 0
	char *tmpname = tmpnam(NULL);
	printf("preprocessing %s (-> %s)\n", filename, tmpname);
	tmp = fopen(tmpname, "w+");
#else
	tmp = lib::tmpfile();
#endif
	if (tmp)
	{
		try
		{
			emit_output = ls->output = tmp;

			enter_file(ls, ls->flags);
			while ((r = run_cpp(ls)) < cpp::CPPERR_EOF)
				fr = fr || (r > 0);

			fr = fr || check_cpp_errors(ls);

			if (ls->flags & cpp::KEEP_OUTPUT)
				put_char(ls, '\n');

			if (emit_dependencies)
				fputc('\n', emit_output);

#ifndef NO_UCPP_BUF
			if (!(ls->flags & cpp::LEXER))
				flush_output(ls);
#endif
		}
		catch (...)
		{
			fclose(tmp);
			tmp = NULL;
		}
	}

	if (ls->flags & cpp::WARN_TRIGRAPHS && ls->count_trigraphs)
		warning(0, "%ld trigraphs encountered", ls->count_trigraphs);

	/* release the lexer state */
	free_lexer_state(ls);

	if (tmp)
	{
		if (fr)
		{
			fclose(tmp);
			tmp = NULL;
		}
		else
			rewind(tmp);
	}

	/* update included_files */
	const std::set<std::string> &files(get_included_files());
	const std::string cwd(sys::getcwd());

	std::set<std::string>::const_iterator end(files.end());
	std::set<std::string>::const_iterator iter(files.begin());

	while (iter != end)
	{
		included_files.insert(lib::abs_path(*iter, cwd));
		++iter;
	}

	return tmp;
}

void
edd_cpp::fatal(const char *fmt, ...)
{
	char buf0[255];
	va_list ap;

	va_start(ap, fmt);
	lib::vsnprintf(buf0, sizeof(buf0), fmt, ap);
	va_end(ap);

	char buf[255];
	snprintf(buf, sizeof(buf), "cpp [%s]", buf0);

	lib::pos pos = lib::NoPosition;
	pos.file = ident->create(current_filename);

	throw RuntimeError(xxInternal,
			   pos,
			   ident->create(buf));
}

void
edd_cpp::error(long line, const char *fmt, ...)
{
	char buf[255];
	va_list ap;

	va_start(ap, fmt);
	lib::vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	lib::pos pos = lib::NoPosition;

	if (line > 0)
		pos.line = line;

	pos.file = ident->create(current_filename);

	err->error(xxCpp,
		   pos,
		   ident->create(buf));
}

void
edd_cpp::warning(long line, const char *fmt, ...)
{
	char buf[255];
	va_list ap;

	va_start(ap, fmt);
	lib::vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	lib::pos pos = lib::NoPosition;

	if (line > 0)
		pos.line = line;

	pos.file = ident->create(current_filename);

	err->warning(xxCpp,
		     pos,
		     ident->create(buf));
}

} /* namespace edd */
