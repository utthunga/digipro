
/* 
 * $Id: edd_tree.h,v 1.247 2009/11/25 09:43:36 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 *               2010-2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the basic EDD library.
 */

#ifndef _edd_tree_h
#define _edd_tree_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
#endif

// C stdlib

// C++ stdlib
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

// my lib
#include "IniParser.h"
#include "Logger.h"
#include "dyn_buf.h"
#include "hashtable.h"
#include "maptable.h"
#include "scan_buf.h"
#include "smart_ptr.h"
#include "vbitset.h"

// own header
#include "cocktail_types.h"
#include "edd_err.h"
#include "edd_operand_interface.h"


namespace edd
{

/**
 * The main EDD library object.
 * This class mainly implements all neccessary things for the abstract syntax
 * tree creation and dispatching the several compiler passes that recurse
 * over the abstract syntax tree and perform various checks and
 * transformations. It include and hold all neccessary helper objects that are
 * used to managing the abstract syntax tree. This means mainly the identifier
 * management through the lib::IDENT class, the error message management through
 * the edd::edd_err class and other things.
 * 
 * \nosubgrouping
 */
class edd_tree
{
	friend class dct_pars;
	friend class edd_pars;
	friend class METHOD_BASE_tree;
	friend class VARIABLE_tree;
	friend class COMPONENT_tree;

public:
	/** \name Constructor/Destructor. */
	//! \{
	edd_tree(void);

	/* make class polymorphic */
	virtual ~edd_tree(void);
	//! \}

	/** \name Helper things. */
	//! \{
	/**
	 * Assign language preference.
	 * This method assign a new language preference to this EDD
	 * object. This influence mainly the evaluation of language strings
	 * inside the EDD specification and the message output from the
	 * error subclass.
	 */
	void set_language(enum err_lang lang) { xxLang = lang; }
	/**
	 * Return language preference.
	 * This return the current language preference that is used by the
	 * EDD library.
	 */
	enum err_lang get_language(void) const { return xxLang; }
	/**
	 * Set tabulator space.
	 * The tabulatorspace influence the scanner column position calculation.
	 * Should be set to the value that is configured to display the
	 * EDD/DLL source. The Default value is 8.
	 */
	void set_tab_size(int space = 8) { yyTabSpace = space; }
	/**
	 * Set error threshold.
	 * Set how many errors are reported. If the threshold is reached
	 * new errors are discarded.
	 */
	void err_threshold(int threshold = 50);
	/**
	 * Disable/enable warnings.
	 * If called with true warnings are from now on suppressed. Call with
	 * false to switch warnings on.
	 */
	void err_config(bool nowarn);
	/**
	 * The EDD language profiles.
	 * Depending on this setting semantic checking is done differently for
	 * specific attributes of the EDD file.
	 * 
	 * \note Don't change the numbers, this are the standardized numbers
	 * for the profile (cross dependencies with AST and IDE config
	 * save/load).
	 */
	enum profile
	{
		HART     = 1, /**< Profile for HART Communication Foundation */
		FIELDBUS = 2, /**< Profile for Fieldbus Foundation */
		PROFIBUS = 3, /**< Profile for PROFIBUS */
		PROFINET = 4, /**< Profile for PROFINET */
	};
	typedef enum profile profile_t;
	/**
	 * EDD profile options.
	 * Options that can be additionally set for the parsing.
	 * 
	 * \note Never change the enum values, they are used by the IDE
	 * to save/load the configuration.
	 */
	enum /* profile options */
	{
		PDM_MODE           = 0x001, /**< Enable PDM compatbility */
		PDM_SCALING_FACTOR = 0x004, /**< Enable PDM SCALING_FACTOR handling */
		PEDANTIC           = 0x002, /**< Pedantic mode, more warnings */
		NO_EDDx1           = 0x100, /**< Disable EDD part 1 enhancements */
	};
	/**
	 * Return profile number.
	 * This return the currently assigned profile number.
	 */
	profile_t get_profile(void) const throw() { return myprofile; }
	/**
	 * Return profile options.
	 * This return the profile options.
	 */
	size_t get_options(void) const throw() { return myoptions; }
	/**
	 * Return current profile description.
	 * This return a human readable C string of the currently assigned
	 * EDD profile.
	 */
	const char *profile_descr(void) throw() { return profile_descr(myprofile); }
	/**
	 * Return profile description.
	 * This return a human readable C string of the specified
	 * EDD profile.
	 */
	static const char *profile_descr(profile_t) throw();
	/**
	 * Return option description.
	 * This return a human readable C string of the specified
	 * profile option.
	 */
	static const char *option_descr(size_t option) throw();
	/**
	 * Return all available profiles.
	 */
	static std::vector<profile_t> allprofiles(void);
	/**
	 * Return all available options.
	 */
	static std::vector<size_t> alloptions(void);
	/**
	 * Flag if the syntax should be parsed relaxed.
	 * On relaxed syntax parsing some of the syntax checks are disabled
	 * that are a common problem with original PDM EDD
	 * specifications.
	 */
	bool relaxed_mode(void) const throw() { return (myoptions & PDM_MODE) != 0; }
	bool pdm_mode(void) const throw() { return (myoptions & PDM_MODE) != 0; }
	bool pedantic(void) const throw() { return (myoptions & PEDANTIC) != 0; }
	bool no_eddx1(void) const throw() { return (myoptions & NO_EDDx1) != 0; }
	/**
	 * Set new device I/O mapper.
	 * This release the current used I/O object and assign the new I/O
	 * object.
	 */
	void assign_device_io(std::auto_ptr<class device_io_interface>) throw();
	/**
	 * Set demo limitation.
	 * For a demo version the number of EDD objects and the value
	 * load functionality can be limited.
	 */
	void set_demo_limit(size_t objects = 0, bool no_value_load = false);
	/**
	 * Lookup human readable string of the manufacturer id.
	 * Search the static manufacturer description table for
	 * the specified id and return the description if found.
	 * If no matching id is found NULL is returned instead.
	 * \param id The manufacturer id.
	 * \return string of the description.
	 */
	std::string lookup_manufacturer_id(int id) const;
	/**
	 * Lookup manufacturer id of a manufacturer like macro define.
	 * PDM accept Identifiers for MANFACTURER and DEVICE_TYPE and
	 * automatically convert them to the right ids. We have a similiar
	 * conversion table and this method lookup the id of such a
	 * MANUFACTURER define (used in the AST to auto convert define to id).
	 * \param define The manufacturer define.
	 * \return The manufacturer id or -1 if the define is not found.
	 */
	int lookup_manufacturer_name(const char *define) const;
	/**
	 * Lookup device type name.
	 * Lookup the devices.cfg data and return the name of the devices name
	 * if such name exist for the specified manufacturer and the device
	 * type id.
	 * \param manufacturer_id The manufacturer id the device type id
	 * belongs too.
	 * \param id The device type id.
	 * \return string of the device type name.
	 */
	std::string lookup_device_type_id(int manufacturer_id, int id) const;
	/**
	 * Lookup device type of a device type define given a manufacturer id.
	 * \param manufacturer_id The manufacturer id the define belongs too.
	 * \param define The device type define.
	 * \return device type id or -1 if the define is not found.
	 */
	int lookup_device_type_name(int manufacturer_id, const char *define) const;
	//! \}

	/** \name Role configuration. */
	//! \{
	const std::set<std::string> &get_roles(void) const throw() { return myroles; }
	void add_role(const std::string &);
	void remove_role(const std::string &);
	//! \}

private:
	int		parse0(class ROOT_tree **root,
			       lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
			       int mode,
			       const std::string &file,
			       const std::vector<std::string> &includes,
			       const std::vector<std::string> &defines);

	int		parse_ident(const std::string &file,
				    const std::vector<std::string> &includes,
				    const std::vector<std::string> &defines,
				    class IDENTIFICATION_tree **);

	static bool	is_edd_file(const std::string &file);

	void		information_import(class IDENTIFICATION_tree *import, bool &informed);

	bool		search_import_hart(class IDENTIFICATION_tree *import,
					   const std::vector<std::string> &includes,
					   const std::vector<std::string> &defines,
					   const std::string &hcf_path,
					   std::string &result);

	bool		search_import_pb(class IDENTIFICATION_tree *import,
					 const std::vector<std::string> &includes,
					 const std::vector<std::string> &defines,
					 const std::string &pb_path,
					 std::string &result);

	void		search_import_self(class IDENTIFICATION_tree *import,
					   const std::vector<std::string> &includes,
					   const std::vector<std::string> &defines,
					   const std::string &path,
					   bool &informed);

public:
	int		parse_import(const std::string &file,
				     const std::vector<std::string> &includes,
				     const std::vector<std::string> &defines,
				     class ROOT_tree **root,
		       		     lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> **hash);

	bool		search_import(class IDENTIFICATION_tree *import,
				      const std::vector<std::string> &includes,
				      const std::vector<std::string> &defines,
				      std::string &result);

	bool		search_file(const std::string &file,
				    const std::vector<std::string> &includes,
		 		    std::string &result);

	int		parse(const std::string &file,
			      const std::vector<std::string> &includes,
			      const std::vector<std::string> &defines,
			      profile_t profile = PROFIBUS, size_t options = 0);

	int		load_dictionary(const std::string &file, size_t options = 0);

	int		load_devices(void);
	int		load_devices(const std::string &path);

private:
	int		load_values0(const lib::IniParser &);
	int		save_values0(lib::IniParser &) const;

public:
	int		load_values(lib::dyn_buf &buf);
	int		load_values(const std::string &path);

	int		save_values(lib::dyn_buf &buf) const;
	int		save_values(const std::string &path) const;

	/**
	 * Format a edd::RuntimeError as human readable message.
	 * This formats a edd::RuntimeError as human readable string. It take
	 * the current language mapping into account.
	 * See edd::edd_err::errmsg.
	 */
	std::string	errmsg(const edd::RuntimeError &xx) { return err_->errmsg(xxLang, xx); }
	/**
	 * Flush error messages into lib::stream.
	 * See edd::edd_err::fflush.
	 */
	void		errmsg_flush(lib::stream &f) const { err_->fflush(f, xxLang); }
	/**
	 * Clear error messages.
	 * See edd::edd_err::clear.
	 */
	void		errmsg_clear(void) const { err_->clear(); }


	/* -----------------
	 * semantic checking
	 * -----------------
	 */

private:
	/**
	 * \internal
	 * Data type for the semantic check recursion.
	 * Used by the pass0 recursion routines to hold several informations
	 * that are need for the generic type check.
	 */
	struct pass0info
	{
		class node_tree *parent;
		unsigned long attr;
		const unsigned long *types;
	};

public:
	/**
	 * \internal
	 * Data type for pass2.
	 */
	struct pass2info
	{
		const struct pass2info *upper;
		class node_tree *parent;
		unsigned long attr;
	};

	/** \name Generic compiler check routines. */
	//! \{
	lib::IDENTIFIER	build_attr_names(const class node_tree *node, const unsigned long mask) const;
	lib::IDENTIFIER	build_type_names(const unsigned long *types) const;
	void		generate_required_attr_errs(const class node_tree *t, const unsigned long required) const;

private:
	void		check_type(class tree *t, const struct pass0info &check) const;

	void		pass0(class tree *t,
			      const struct pass0info &flat,
			      const struct pass0info &recursiv);
	void		pass0(void);

	void		pass1(class tree *t);
	void		pass1(void);

	void		pass2(class tree *t, const struct pass2info &);
	void		pass2(void);

	void		build_hashtable(lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
					class ROOT_tree *root) const;

	void		run_import(class ROOT_tree *root,
				   lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
				   const std::vector<std::string> &includes,
				   const std::vector<std::string> &defines) const;

	void		run_like(class ROOT_tree *root,
				 lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash) const;
	//! \}


	/* ----------
	 * class data
	 * ----------
	 */

public:

	void Display_Lnt_Size(){
	std::cout<<"$$$$$$$$$$$$$$$$$$$$$ "<<std::endl;
	std::cout<<"maptable size is YOGA : "<< sizeof(treemap_)<<std::endl;
	std::cout<<"hashtable size is YOGA : "<< sizeof(hash_)<<std::endl;
	}

	/** EDDLIB version information. */
	static const char *version;
	/** EDDLIB compile date information. */
	static const char *date;

	/** Return reference to the identifier management object. */
	inline lib::IDENT &ident(void) const { return *ident_.get(); }
	/** Return reference to the dictionary management object. */
	inline class edd_dictionary &dictionary(void) const { return *dictionary_.get(); }
	/** Return reference to the currently assigned communication object. */
	inline class device_io_interface &device_io(void) const { return *device_io_.get(); }
	/** Return reference to the error message management object. */
	inline class edd_err &err(void) const { return *err_.get(); }
	/** Return reference to the hash table object. */
	inline lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> &hash(void) const { return *hash_.get(); }
	/** Return reference to the id mapping object. */
	inline lib::maptable<class EDD_OBJECT_tree *> &treemap(void) const { return *treemap_.get(); }
	/** Return reference to the builtin management object. */
	inline class edd_builtins_interface &builtins(void) const { return *builtins_.get(); }

	/** Return the AST ROOT_tree pointer if the loaded DD is valid */
	inline const class ROOT_tree *get_root(void) const { return valid ? root : NULL; }
	/**
	 * Return plain pointer to the AST.
	 * The loaded DD is maybe not valid.
	 * \note This method is for internal use only.
	 */
	inline const class ROOT_tree *raw_root(void) const { return root; }

	const class IDENTIFICATION_tree *identification(void) const;
	std::vector<std::string> ddl_files(void) const;

	/* global variables */
	lib::Logger logger;
	operand_ptr bi_rc;

protected:
	void clear(void);

	/** Identifier management. */
	std::auto_ptr<lib::IDENT> ident_;

	/** Dictionary object. */
	std::auto_ptr<class edd_dictionary> dictionary_;

	/** Communication object. */
	std::auto_ptr<class device_io_interface> device_io_;

	/** Error object. */
	std::auto_ptr<class edd_err> err_;

	/** Crossreference identifier hashtable. */
	std::auto_ptr<lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> > hash_;

	/** Unique integer mapping of tree nodes */
	std::auto_ptr<lib::maptable<class EDD_OBJECT_tree *> > treemap_;

	/** Builtins abstraction and dispatcher object. */
	std::auto_ptr<class edd_builtins_interface> builtins_;

	/** Devices.cfg information object. */
	std::auto_ptr<class DevicesParser> devices_;

#define IDENT_IS_AUTO_PTR

	/** The root of all evil. */
	class ROOT_tree *root;

private:
	/** Preferred language. */
	enum err_lang xxLang;

	/** Assigned profile. */
	profile_t myprofile;

	/** Assigned parsing options. */
	size_t myoptions;

	/** Assigned roles. */
	std::set<std::string> myroles;

	/* limits (demoversion) */
	size_t object_limit;
	bool deny_value_load;

	/* import cache */
	struct import_cache_entry
	{
		std::string fullname;
		lib::smart_ptr<class IDENTIFICATION_tree> id;
	};
	typedef std::vector<struct import_cache_entry> import_cache_dir_t;
	typedef std::map<std::string, import_cache_dir_t> import_cache_t;
	import_cache_t import_cache;

	/* included files */
	typedef std::set<std::string> included_files_t;
	included_files_t included_files;

	/* runtime relevant data */
	std::string hcf_path, pb_path;
	bool use_rel_cmds_;
	int yyTabSpace;
	bool collect_import_infos_;
	bool valid;

	typedef std::map<int, const class COMMAND_tree *> cmd_by_number_t;
	cmd_by_number_t cmd_by_number;

public:
	/* --------------
	 * generic helper
	 * --------------
	 */
	class EDD_OBJECT_tree *node_lookup(const std::string &name) const;
	class EDD_OBJECT_tree *node_lookup(const char *name) const;
	class EDD_OBJECT_tree *node_lookup(const lib::IDENTIFIER &name) const;

	std::string dictionary_lookup(const std::string &name) const;
	std::string dictionary_lookup(const char *name) const;
	std::string dictionary_lookup(const lib::IDENTIFIER &name) const;
	std::string dictionary_lookup(const lib::IDENTIFIER &, bool &) const;

	operand_ptr eval(class tree *t, class method_env * = NULL) const;
	void eval(lib::stream &f, class tree *t, unsigned long level) const;

	class EDD_OBJECT_tree *id2tree(unsigned int id) const throw();

	void check_langstring(const std::string &, const lib::pos &);

	static std::string langmap(const std::string &, enum err_lang);
	std::string langmap(const std::string &str) const { return langmap(str, xxLang); }

	/**
	 * Set HCF catalog path.
	 * Set the HCF catalog path that is used to resolve IMPORT
	 * directives inside the EDD. The HCF catalog path is only used
	 * if parsing EDDs under profile HART.
	 */
	void set_hcf_path(const std::string &path) { hcf_path = path; }
	/**
	 * Set PROFIBUS catalog path.
	 * Set the PROFIBUS catalog path that is used to resolve IMPORT
	 * directives inside the EDD. The PROFIBUS catalog path is only used
	 * if parsing EDDs under profile PROFIBUS.
	 */
	void set_pb_path(const std::string &path) { pb_path = path; }

	void set_use_rel_cmds(bool flag = true) throw() { use_rel_cmds_ = flag; }
	bool use_rel_cmds(void) const throw() { return use_rel_cmds_; }

	void set_collect_import_infos(bool flag = true) throw() { collect_import_infos_ = flag; }
	bool collect_import_infos(void) const throw() { return collect_import_infos_; }

	void register_cmd_by_number(int number, const class COMMAND_tree *);
	const class COMMAND_tree *lookup_cmd_by_number(int number) const;
	const class COMMAND_tree *lookup_cmd_by_number_pass1(int number) const;

	lib::IDENTIFIER search_FILE(class method_env *, class EDD_OBJECT_tree *) const;

	/* debugging helper */
	void dump_tree(lib::stream &, const class tree *, bool single = false) const;
private:
	void dump_tree0(lib::stream &, const class tree *, unsigned long, bool) const;

	void search_DD_ITEM_METHOD_refs0(class tree *);
	void search_DD_ITEM_METHOD_refs(void);


protected:
	/* ---------
	 * notifiers
	 * ---------
	 */
	virtual void on_value_changed(class EDD_OBJECT_tree *, class method_env *);
	virtual void on_method_start(class EDD_OBJECT_tree *, class method_env *);
	virtual void on_method_stop(class EDD_OBJECT_tree *, class method_env *);
	virtual void on_method_debug(class EDD_OBJECT_tree *, class method_env *);
	virtual void on_method_notify(class EDD_OBJECT_tree *, class method_env *);

	/* --------------
	 * generic things
	 * --------------
	 */
	struct str_list *m_str_list(
				const lib::pos &,
				lib::scan_buf &);

	void		m_dictionary_entry(
				const lib::pos &,
				char *idstr,
				int major, int minor,
				struct str_list	*list);

	/* -----------------
	 * tree manipulation
	 * -----------------
	 */

	/** \name Tree manipulation methods. */
	//! \{
public:
	static void tree_delete(
				class tree	**head,
				class tree	*t) throw();

	static void tree_replace(
				class tree	**head,
				class tree	*old,
				class tree	*n) throw();

protected:
	static class tree *tree_reverse(
				class tree	*tree) throw();

	static class tree *tree_by_index(
				class tree	*head,
				unsigned long	index) throw();

	static class tree *tree_extract(
				class tree	**head,
				unsigned long	kind) throw();
	static class tree *tree_extract(
				class tree	**head,
				unsigned long	kind,
				unsigned long	max) throw();

	static unsigned long tree_count(
				class tree	*t) throw();

	/**
	 * \internal
	 * Forward tree object.
	 * This just return the given tree object pointer. Needed by the
	 * syntax parser for executing syntatic actions there just a tree is
	 * passed through. Inline to speedup things.
	 */
	static class tree *tree_forward(
				class tree	*t) throw() { return t; };

	/* yeah, overloaded :-) */
	static class tree *tree_concat(
				class tree	*t,
				class tree	*t1) throw();
	static class tree *tree_concat(
				class tree	*t,
				class tree	*t1,
				class tree	*t2) throw();
	static class tree *tree_concat(
				class tree	*t,
				class tree	*t1,
				class tree	*t2,
				class tree	*t3) throw();
	static class tree *tree_concat(
				class tree	*t,
				class tree	*t1,
				class tree	*t2,
				class tree	*t3,
				class tree	*t4) throw();
	static class tree *tree_concat(
				class tree	*t,
				class tree	*t1,
				class tree	*t2,
				class tree	*t3,
				class tree	*t4,
				class tree	*t5) throw();
	//! \}


	/* -------------
	 * tree creation
	 * -------------
	 */

	/** \name Generic tree creation routines. */
	//! \{
public:
	void		m_node(
				class node_tree *node,
				class tree *attribute_list) const;

protected:
	void		m_edd_object(
				class EDD_OBJECT_tree *object,
				class tree *identifier,
				class tree *attribute_list) const;
	//! \}

	/** \name Terminal tree creation routines. */
	//! \{
	class tree *	m_pseudo(int kind, unsigned long attr, class tree *);
	class tree *	m_pseudo(int kind, unsigned long attr, class tree *, const lib::pos &);
	class tree *	m_boolean(bool, const lib::pos &);
	class tree *	m_character(char, const lib::pos &);
	class tree *	m_integer(int32, const lib::pos &, bool negative = false);
	class tree *	m_integer64(int64, const lib::pos &, bool negative = false);
	class tree *	m_real(double, const lib::pos &);
	class tree *	m_enum(uint32, const lib::pos &);
	class tree *	m_set(uint32, const lib::pos &);
	class tree *	m_bitset(lib::vbitset *&, const lib::pos &);
	class tree *	m_bitset(uint64, const lib::pos &);
	class tree *	m_identifier(lib::scan_buf &, const lib::pos &);
	class tree *	m_identifier(const char *, const lib::pos &);
	class tree *	m_string(const char *, const lib::pos &);
	class tree *	m_string(lib::scan_buf &, const lib::pos &);
	class tree *	m_string(class tree *, const lib::pos &);
	class tree *	m_current_role(const lib::pos &);
	class tree *	m_array_index(const lib::pos &);
	class tree *	m_uuid(const char *, const lib::pos &);
	class tree *	m_uuid(lib::scan_buf &, const lib::pos &);
	//! \}


	/* -------------
	 * node creating
	 * -------------
	 */

	/** \name Node tree creation routines. */
	//! \{
	template<typename T>
	T *		m_node(
				const lib::pos &pos,
				class tree *attribute_list);
	template<typename T>
	T *		m_edd_object(
				class tree *ident,
				const lib::pos &pos,
				const lib::pos &start,
				const lib::pos &end,
				class tree *attribute_list)
	{
		T *t = new T(this, pos);
		assert(t);

		/* additional pos information */
		t->set_additional_pos(start, end);

		m_edd_object(t, ident, attribute_list);

		return t;
	}

	class tree *	m_like(
				class tree *ident,
				const lib::pos &pos,
				const lib::pos &start,
				const lib::pos &end,
				unsigned long type,
				class tree *add_type,
				class tree *likename,
				class tree *redefinitions,
				class tree *anon_redefinitions);
	class ROOT_tree *
			m_root(
				const lib::pos &pos,
				class tree *identification,
				class tree *definition_list);
	class tree *	m_identification(
				const lib::pos &pos,
				class tree *manufacturer,
				class tree *device_type,
				class tree *device_revision,
				class tree *DD_revision,
				class tree *edd_version,
				class tree *edd_profile,
				class tree *manufacturer_ext);

	class tree *	m_imported_dd(
				const lib::pos &pos,
				class tree *identification,
				class tree *imports,
				class tree *redefinitions);

	class tree *	m_variable_type_arithmetic(
				const lib::pos &pos,
				class tree *type,
				class tree *attribute_list);
	class tree *	m_variable_type_boolean(
				const lib::pos &pos);
	class tree *	m_variable_type_enumerated(
				const lib::pos &pos,
				class tree *type,
				class tree *attribute_list);
	class tree *	m_variable_type_index(
				const lib::pos &pos,
				class tree *reference_array_reference,
				class tree *attribute_list);
	class tree *	m_variable_type_string(
				const lib::pos &pos,
				class tree *type,
				class tree *size,
				class tree *attribute_list);
	class tree *	m_variable_type_date_time(
				const lib::pos &pos,
				class tree *type,
				class tree *size,
				class tree *attribute_list);
	class tree *	m_variable_type_object_reference(
				const lib::pos &pos,
				class tree *attribute_list);

	class tree *	m_minmax_value(
				const lib::pos &pos,
				class tree *type,
				class tree *expr,
				class tree *nr);

	class tree *	m_variable_enumerator_enum(
				const lib::pos &pos,
				class tree *value,
				class tree *description,
				class tree *help);
	class tree *	m_variable_enumerator_bitenum(
				const lib::pos &pos,
				class tree *value,
				class tree *description,
				class tree *help,
				class tree *variable_class,
				class tree *status_class,
				class tree *method_ref,
				class tree *attribute_list);

	class tree *	add_variable_enumerator_enum_comma(
				class tree *t,
				class tree *comma);
	class tree *	add_variable_enumerator_bitenum_comma(
				class tree *t,
				class tree *comma);

	class tree *	m_transaction(
				const lib::pos &pos,
				class tree *integer,
				class tree *attribute_list);
	class tree *	m_data_item(
				const lib::pos &pos,
				class tree *reference,
				class tree *mask,
				class tree *qualifier);
	class tree *	add_data_item_comma(
				class tree *t,
				class tree *comma);

	class tree *	m_reference_array_element(
				const lib::pos &pos,
				class tree *integer,
				class tree *reference,
				class tree *description,
				class tree *help);

	class tree *	m_menu_item(
				const lib::pos &pos,
				class tree *item,
				class tree *attribute_list);
	class tree *	m_menu_item_string(
				const lib::pos &pos,
				class tree *string);
	class tree *	m_menu_item_format(
				const lib::pos &pos,
				class tree *type);

	class tree *	add_comma(
				class tree *t,
				class tree *comma,
				int kind,
				unsigned long attr);

	class tree *	m_method_parameter(
				const lib::pos &pos,
				class tree *type,
				class tree *identifier,
				class tree *byref);

	class tree *	m_method_parameter_type_unsigned(
				unsigned long value,
				const lib::pos &pos);

	class tree *	m_method_definition(
				const lib::pos &pos,
				class tree *definition);

	class tree *	m_response_code(
				const lib::pos &pos,
				class tree *integer,
				class tree *type,
				class tree *description,
				class tree *help);

	class tree *	m_member(
				const lib::pos &pos,
				class tree *identifier,
				class tree *reference,
				class tree *description,
				class tree *help);

	class tree *	m_vector(
				const lib::pos &pos,
				class tree *string,
				class tree *value);

	class tree *	m_edit_display_item(
				const lib::pos &pos,
				class tree *reference);

	class tree *	m_key_points(
				const lib::pos &pos,
				class tree *attribute_list);

	class tree *	m_waveform_type_yt(
				const lib::pos &pos,
				class tree *attribute_list);
	class tree *	m_waveform_type_xy(
				const lib::pos &pos,
				class tree *attribute_list);
	class tree *	m_waveform_type_horizontal(
				const lib::pos &pos,
				class tree *attribute_list);
	class tree *	m_waveform_type_vertical(
				const lib::pos &pos,
				class tree *attribute_list);

	class tree *	m_import_action(
				const lib::pos &pos,
				class tree *action,
				class tree *type,
				class tree *identifier_type,
				class tree *identifier);
	class tree *	m_import_action_redefinition(
				const lib::pos &pos,
				class tree *action,
				class tree *type,
				class tree *ident,
				class tree *list);
	class tree *	m_like_action(
				const lib::pos &pos,
				class tree *action,
				class tree *t,
				class tree *info);
	class tree *	m_anon_like_action(
				const lib::pos &pos,
				class tree *action,
				class tree *info);

	class tree *	m_open_close(
				const lib::pos &pos,
				int type,
				class tree *identifier);


	/* constraints */

	class tree *	m_constraint_if(
				const lib::pos &pos,
				class tree *expr,
				class tree *if_stmt,
				class tree *else_stmt);
	class tree *	m_constraint_select(
				const lib::pos &pos,
				class tree *expr,
				class tree *selections);
	class tree *	m_constraint_selection(
				const lib::pos &pos,
				class tree *expr,
				class tree *stmt);

	/* expressions */

	class tree *	m_expr_primary(
				const lib::pos &pos,
				class tree *value);
	class tree *	m_expr_postfix(
				const lib::pos &pos,
				class tree *op,
				class tree *expr);
	class tree *	m_expr_unary(
				const lib::pos &pos,
				class tree *op,
				class tree *expr);
	class tree *	m_expr_binary(
				const lib::pos &pos,
				class tree *op,
				class tree *expr1,
				class tree *expr2);
	class tree *	m_expr_conditional(
				const lib::pos &pos,
				class tree *expr,
				class tree *expr1,
				class tree *expr2);
	class tree *	m_expr_assignment(
				const lib::pos &pos,
				class tree *op,
				class tree *unary,
				class tree *expr);
	class tree *	m_expr_comma(
				const lib::pos &pos,
				class tree *expr1,
				class tree *expr2);

	/* special */

	class tree *	m_string_node(
				const lib::pos &pos,
				class tree *list);

	class tree *	m_string_terminal(
				const lib::pos &pos,
				class tree *ref,
				class tree *integer);

	class tree *	m_string_literal(
				class tree *list);

	/* references */

	class tree *	m_dictionary_reference(
				const lib::pos &pos,
				class tree *identifier);
	class tree *	m_typed_reference(
				const lib::pos &pos,
				unsigned long	 type,
				class tree *reference);
	class tree *	m_method_reference(
				const lib::pos &pos,
				class tree *reference);
	class tree *	m_reference_ident(
				const lib::pos &pos,
				class tree *identifier);
	class tree *	m_reference_array_ref(
				const lib::pos &pos,
				class tree *reference,
				class tree *expr);
	class tree *	m_reference_call(
				const lib::pos &pos,
				class tree *reference,
				class tree *args);
	class tree *	m_reference_record(
				const lib::pos &pos,
				class tree *reference,
				class tree *identifier);
	class tree *	m_reference_block(
				const lib::pos &pos,
				class tree *identifier);

	class tree *	attach_comma(
				class tree *t,
				class tree *comma);

	/* components */

	class tree *	m_component_initial_value(
				const lib::pos &pos,
				class tree *reference,
				class tree *expr);

	class tree *	m_components_reference(
				const lib::pos &pos,
				class tree *ref,
				class tree *attribute_list);

	class tree *	m_required_range(
				const lib::pos &pos,
				class tree *reference,
				class tree *argument_list);

	/* C gramar */

	class tree *	m_c_char(
				const lib::pos &pos,
				class tree *integer);
	class tree *	m_c_dictionary(
				const lib::pos &pos,
				class tree *identifier);
	class tree *	m_c_identifier(
				const lib::pos &pos,
				class tree *identifier);
	class tree *	m_c_integer(
				const lib::pos &pos,
				class tree *integer);
	class tree *	m_c_real(
				const lib::pos &pos,
				class tree *real);
	class tree *	m_c_string(
				const lib::pos &pos,
				class tree *string);
	class tree *	m_c_edd_terminal(
				const lib::pos &pos,
				class tree *edd_terminal);
	class tree *	m_c_primary_expr(
				const lib::pos &pos,
				class tree *value);
	class tree *	m_c_postfix_expr_array(
				const lib::pos &pos,
				class tree *expr,
				class tree *index);
	class tree *	m_c_postfix_expr_call(
				const lib::pos &pos,
				class tree *identifier,
				class tree *args);
	class tree *	m_c_postfix_expr_struct(
				const lib::pos &pos,
				class tree *expr,
				class tree *identifier);
	class tree *	m_c_postfix_expr_op(
				const lib::pos &pos,
				class tree *op,
				class tree *expr);
	class tree *	m_c_unary_expr(
				const lib::pos &pos,
				class tree *op,
				class tree *expr);
	class tree *	m_c_cast_expr(
				const lib::pos &pos,
				class tree *type_name,
				class tree *expr);
	class tree *	m_c_binary_expr(
				const lib::pos &pos,
				class tree *op,
				class tree *expr1,
				class tree *expr2);
	class tree *	m_c_conditional_expr(
				const lib::pos &pos,
				class tree *expr,
				class tree *expr1,
				class tree *expr2);
	class tree *	m_c_assignment_expr(
				const lib::pos &pos,
				class tree *op,
				class tree *unary,
				class tree *expr);
	class tree *	m_c_comma_expr(
				const lib::pos &pos,
				class tree *expr1,
				class tree *expr2);
	class tree *	m_c_declaration(
				const lib::pos &pos,
				class tree *specifier,
				class tree *declarator);
	class tree *	m_c_declarator(
				const lib::pos &pos,
				class tree *identifier,
				class tree *array);
	class tree *	m_c_declarator_array_specifier(
				const lib::pos &pos,
				class tree *expr);
	class tree *	m_c_labeled_statement(
				const lib::pos &pos,
				class tree *case_expr,
				class tree *stmt);
	class tree *	m_c_compound_statement(
				const lib::pos &pos,
				class tree *decls,
				class tree *stmts,
				class tree *end);
	class tree *	m_c_expr_statement(
				const lib::pos &pos,
				class tree *expr);
	class tree *	m_c_selection_statement_if(
				const lib::pos &pos,
				class tree *if_expr,
				class tree *then_stmt,
				class tree *else_stmt);
	class tree *	m_c_selection_statement_switch(
				const lib::pos &pos,
				class tree *switch_expr,
				class tree *stmt);
	class tree *	m_c_iteration_statement_do(
				const lib::pos &pos,
				class tree *do_stmt,
				class tree *stmt);
	class tree *	m_c_iteration_statement_for(
				const lib::pos &pos,
				class tree *start_stmt,
				class tree *while_stmt,
				class tree *expr_stmt,
				class tree *stmt);
	class tree *	m_c_jump_statement(
				const lib::pos &pos,
				class tree *op,
				class tree *expr);
	//! \}
};

} /* namespace edd */

#endif /* _edd_tree_h */
