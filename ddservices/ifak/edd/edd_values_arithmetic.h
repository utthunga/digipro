
/* 
 * $Id: edd_values_arithmetic.h,v 1.5 2009/02/04 14:37:13 fna Exp $
 * 
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann
 * \brief Definitions of various concrete operand classes.
 */

#ifndef _edd_values_arithmetic_h
#define _edd_values_arithmetic_h

// C stdlib

// C++ stdlib
#include <limits>
#include <string>

// my lib

// own
#include "edd_operand_interface.h"


namespace edd
{

/**
 * EDD arithmetic value.
 * Abstracts arithmetic constant values.
 */
template<typename T>
class arithmetic_value : public operand
{
protected:
	T value;

public:
	typedef std::numeric_limits<T> nl;

	/** \name Constructor/destructor. */
	//! \{
	arithmetic_value(T v) : value(v) { }
	arithmetic_value(const operand &v) : value(v) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator int8() const;
	            operator int16() const;
	            operator int32() const;
	            operator int64() const;
	            operator uint8() const;
	            operator uint16() const;
	            operator uint32() const;
	            operator uint64() const;
	            operator float32() const;
	            operator float64() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	operand_ptr operator+   (void) const;
	operand_ptr operator-   (void) const;
	operand_ptr operator~   (void) const;
	operand_ptr operator!   (void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	operand_ptr operator*   (const operand &op) const;
	operand_ptr operator/   (const operand &op) const;
	operand_ptr operator+   (const operand &op) const;
	operand_ptr operator-   (const operand &op) const;
	operand_ptr operator%   (const operand &op) const;
	operand_ptr operator^   (const operand &op) const;
	operand_ptr operator&   (const operand &op) const;
	operand_ptr operator|   (const operand &op) const;
	operand_ptr operator<<  (const operand &op) const;
	operand_ptr operator>>  (const operand &op) const;

	bool        operator<   (const operand &op) const;
	bool        operator>   (const operand &op) const;
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	bool        operator<=  (const operand &op) const;
	bool        operator>=  (const operand &op) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	static const char *printf_fmt(void) throw();
	//! \}
};

/**
 * EDD arithmetic variable.
 * Specialization of an arithmetic constant value, support the lvalue operators
 * too.
 */
template<typename T>
class arithmetic_lvalue : public arithmetic_value<T>
{
protected:
	using arithmetic_value<T>::env;
	using arithmetic_value<T>::value;

	class operand::status mystatus;
	virtual void assign(const T v, bool strict = false);

public:
	using arithmetic_value<T>::nl;

	/** \name Constructor/destructor. */
	//! \{
	arithmetic_lvalue(T v,
			  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: arithmetic_value<T>(v), mystatus(status) { }
	arithmetic_lvalue(const operand &v,
			  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: arithmetic_value<T>(v), mystatus(status) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Postfix operators. */
	//! \{
	operand_ptr operator++  (int);  // post
	operand_ptr operator--  (int);  // post
	//! \}

	/** \name Unary operators. */
	//! \{
	operand_ptr operator++  (void); // pre
	operand_ptr operator--  (void); // pre
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	void        operator+=  (const operand &op);
	void        operator-=  (const operand &op);
	void        operator*=  (const operand &op);
	void        operator/=  (const operand &op);
	void        operator%=  (const operand &op);
	void        operator^=  (const operand &op);
	void        operator&=  (const operand &op);
	void        operator|=  (const operand &op);
	void        operator>>= (const operand &op);
	void        operator<<= (const operand &op);
	//! \}

private:
	/** Helper for scan */
	template<int, typename T1> class scan0 { };

	/* the specializations */
	template<typename T1> class scan0<0, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<1, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<2, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<3, T1> { public: static T go(const std::string &); };
};

/**
 * EDD special checked arithmetic variable.
 * Specialization of the arithmetic variable class. This class implements strong
 * value checking for all the modifying operators. The value checks are done
 * by calling back the AST node of the underlying EDD variable there the
 * check routines are implemented. 
 */
template<typename T>
class checked_arithmetic_lvalue : public arithmetic_lvalue<T>
{
protected:
	using arithmetic_lvalue<T>::env;
	using arithmetic_lvalue<T>::value;
	using arithmetic_lvalue<T>::mystatus;
	class VARIABLE_TYPE_tree * const type;

	void assign(const T v, bool strict);

public:
	using arithmetic_lvalue<T>::nl;

	/** \name Constructor/destructor. */
	//! \{
	checked_arithmetic_lvalue(T v, class VARIABLE_TYPE_tree *t,
				  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: arithmetic_lvalue<T>(v, status), type(t) { }
	checked_arithmetic_lvalue(const operand &v, class VARIABLE_TYPE_tree *t,
				  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: arithmetic_lvalue<T>(v, status), type(t) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}
};

typedef arithmetic_value<int8>             bool_value;

typedef arithmetic_value<int8>             int8_value;
typedef arithmetic_value<int16>            int16_value;
typedef arithmetic_value<int32>            int32_value;
typedef arithmetic_value<int64>            int64_value;

typedef arithmetic_value<uint8>            uint8_value;
typedef arithmetic_value<uint16>           uint16_value;
typedef arithmetic_value<uint32>           uint32_value;
typedef arithmetic_value<uint64>           uint64_value;

typedef arithmetic_value<float32>          float32_value;
typedef arithmetic_value<float64>          float64_value;


typedef arithmetic_lvalue<int8>            int8_lvalue;
typedef arithmetic_lvalue<int16>           int16_lvalue;
typedef arithmetic_lvalue<int32>           int32_lvalue;
typedef arithmetic_lvalue<int64>           int64_lvalue;

typedef arithmetic_lvalue<uint8>           uint8_lvalue;
typedef arithmetic_lvalue<uint16>          uint16_lvalue;
typedef arithmetic_lvalue<uint32>          uint32_lvalue;
typedef arithmetic_lvalue<uint64>          uint64_lvalue;

typedef arithmetic_lvalue<float32>         float32_lvalue;
typedef arithmetic_lvalue<float64>         float64_lvalue;


typedef checked_arithmetic_lvalue<int8>    checked_int8_lvalue;
typedef checked_arithmetic_lvalue<int16>   checked_int16_lvalue;
typedef checked_arithmetic_lvalue<int32>   checked_int32_lvalue;
typedef checked_arithmetic_lvalue<int64>   checked_int64_lvalue;

typedef checked_arithmetic_lvalue<uint8>   checked_uint8_lvalue;
typedef checked_arithmetic_lvalue<uint16>  checked_uint16_lvalue;
typedef checked_arithmetic_lvalue<uint32>  checked_uint32_lvalue;
typedef checked_arithmetic_lvalue<uint64>  checked_uint64_lvalue;

typedef checked_arithmetic_lvalue<float32> checked_float32_lvalue;
typedef checked_arithmetic_lvalue<float64> checked_float64_lvalue;

/**
 * BOOLEAN data type
 */
class boolean_value : public operand
{
protected:
	uint8 value;

public:

	/** \name Constructor/destructor. */
	//! \{
	boolean_value(uint8 v) : value(v) { }
	boolean_value(const operand &v) : value(v) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	std::string print(const char *fmt = NULL) const;
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	            operator stackmachine::Type *() const;
	//! \}

	/** \name Cast operators. */
	//! \{
	            operator bool() const;
	            operator uint8() const;
	//! \}

	/** \name Binary operators. */
	//! \{
	bool        operator==  (const operand &op) const;
	bool        operator!=  (const operand &op) const;
	//! \}
};

class boolean_lvalue : public boolean_value
{
protected:
	class operand::status mystatus;
	virtual void assign(const uint8 v, bool strict = false);
public:
	/** \name Constructor/destructor. */
	//! \{
	boolean_lvalue(bool v,
			  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: boolean_value(v), mystatus(status) { }
	boolean_lvalue(const operand &v,
			  operand::value_status_t status = operand::VALUE_STATUS_NONE)
	: boolean_value(v), mystatus(status) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	operand_ptr clone(void) const;
	void dump0(lib::stream &f) const;
	//! \}

	/** \name String I/O methods (optional). */
	//! \{
	void scan(const std::string &str, bool strict = true);
	//! \}

	/** \name Stackmachine data I/O. */
	//! \{
	void        operator= (const stackmachine::Type &);
	//! \}

	/** \name Helper methods (optional). */
	//! \{
	operand::value_status_t& status(void);
	const operand::value_status_t& status(void) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	void        operator=   (const operand &op);
	//! \}
};

} /* namespace edd */

#endif /* _edd_values_arithmetic_h */
