
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _edd_builtins_impl_h
#define _edd_builtins_impl_h

#include <vector>

#include "ident.h"
#include "position.h"

#include "cocktail_types.h"
#include "edd_builtins_dispatch.h"

/* BEGIN EXPORT */

#include <math.h>

/* avoid macro conflicts */
#ifdef log2
# undef log2
# undef log2f
#endif

/* following IEC spec */
const long BLTIN_SUCCESS		=    0;	/** Success */
const long BLTIN_NO_MEMORY		= 1801;	/** Out of memory */
const long BLTIN_VAR_NOT_FOUND		= 1802;	/** Cannot find variable */
const long BLTIN_BAD_ID			= 1803;	/** Specified item or member ID does not exist */
const long BLTIN_NO_DATA_TO_SEND	= 1804;	/** No value to send to device */
const long BLTIN_WRONG_DATA_TYPE	= 1805;	/** Wrong variable type for Builtin */
const long BLTIN_NO_RESP_CODES		= 1806;	/** Item does not have response codes */
const long BLTIN_BAD_METHOD_ID		= 1807;	/** Invalid method ID */
const long BLTIN_BUFFER_TOO_SMALL	= 1808;	/** Supplied buffer for starting string is too small */
const long BLTIN_CANNOT_READ_VARIABLE	= 1809;	/** Cannot read the specified variable */
const long BLTIN_INVALID_PROMPT		= 1810;	/** Bad prompt string */
const long BLTIN_NO_LANGUAGE_STRING	= 1811;	/** No valid string for current language */
const long BLTIN_DDS_ERROR		= 1812;	/** A DDS error occurred */
const long BLTIN_FAIL_RESPONSE_CODE	= 1813;	/** Response code received from the device caused Builtin to fail */
const long BLTIN_FAIL_COMM_ERROR	= 1814;	/** Communication error caused Builtin to fail */
const long BLTIN_NOT_IMPLEMENTED	= 1815;	/** Builtin function not yet implemented */
const long BLTIN_BAD_ITEM_TYPE		= 1816;	/** Wrong item type for Builtin */
const long BLTIN_VALUE_NOT_SET		= 1817;	/** A CLASS LOCAL variable has not been initialized before being used */

/* following HART spec */
const long BI_SUCCESS			=    0; /** task succeeded in intended task */
const long BI_ERROR			=   -1; /** error occured in task */
const long BI_ABORT			=   -2; /** user aborted task */
const long BI_NO_DEVICE			=   -3; /** no device found on comm request */
const long BI_COMM_ERROR		=   -4; /** communications error */
const long BI_CONTINUE			=   -5; /** continue */
const long BI_RETRY			=   -6; /** retry */

#include <math.h>

#ifdef _MSC_VER
#include <float.h> // needed for _fpclass
#endif

#pragma push_macro("fpclassify")
#undef fpclassify

/* END EXPORT */


/**
 * Namespace for the EDD builtin library component.
 */
namespace builtins
{

using namespace edd;

/**
 * The builtin ABORT_ON_ALL_COMM_STATUS.
 * The Builtin ABORT_ON_ALL_COMM_STATUS will set all of the bits in the comm
 * status abort mask. This will cause the system to abort the current method
 * if the device returns any comm status value. Comm status is defined to be
 * the first data octet returned in a transaction, when bit 7 of this octet
 * is set.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See Builtin ABORT_ON_RESPONSE_CODES for a list of the available
 * masks, and their default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin ABORT_ON_ALL_DEVICE_STATUS.
 * Builtin ABORT_ON_ALL_DEVICE_STATUS will set all of the bits in the device
 * status abort mask. This will cause the system to abort the current method
 * if the device returns any device status value. Device status is defined to
 * be the second data octet returned in a  transaction.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See 
 * ABORT_ON_RESPONSE_CODE for a list of the available masks, and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin ABORT_ON_ALL_RESPONSE_CODES.
 * Builtin ABORT_ON_ALL_RESPONSE_CODES will set all of the bits in the
 * response code abort mask. This will cause the system to abort the current
 * method if the device returns any response code value.
 * 
 * The response code is defined to be the first data octet returned in a
 * transaction, when bit 7 of this octet is 0.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send command function for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks, and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin ABORT_ON_COMM_ERROR.
 * Builtin ABORT_ON_COMM_ERROR will set the no comm error mask such that the
 * method will be aborted if a comm error is found while sending a command.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODES for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command,  send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin ABORT_ON_COMM_STATUS.
 * The Builtin ABORT_ON_COMM_STATUS will set the correct bit(s) in the comm
 * status abort mask such that the specified comm_status_value will cause the
 * method to abort. Comm status is defined to be the first data octet returned
 * in a transaction, when bit 7 of this octet is l. 
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send command function for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command,  send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_ABORT_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ABORT_ON_DEVICE_STATUS.
 * The Builtin ABORT_ON_DEVICE_STATUS will set the correct bit(s) in the
 * device status abort mask such that the specified device status value will
 * cause the method to abort. Device status is defined to be the second data
 * octet returned in a  transaction. 
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send command function for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks and their default
 * values.
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command,  send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_ABORT_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ABORT_ON_NO_DEVICE.
 * The Builtin ABORT_ON_NO_DEVICE will set the no devices mask such that the
 * method will be aborted if no device is found while sending a command.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send command function for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin ABORT_ON_RESPONSE_CODE.
 * The Builtin ABORT_ON_RESPONSE_CODE will set the correct bit(s) in the
 * response code abort mask such that the specified response code value will
 * cause the method to abort. The response code is defined to be the first
 * data octet returned in a  transaction, when bit 7 of this octet is 0.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method.
 * 
 * The following is a list of all the available masks. The default values of
 * the masks are as follows:
 * - response_code_abort_mask = ACCESS_RESTRICTED | CMD_NOT_IMPLEMENTED
 * - response_code_retry_mask = NO BITS SET
 * - cmd48_response_code_abort_mask = ACCESS_RESTRICTED | CMD_NOT_IMPLEMENTED
 * - cmd48_response_code_retry_mask = NO BITS SET
 * - device_status_abort_mask = DEVICE_MALFUNCTION
 * - device_status_retry_mask = NO BITS SET
 * - cmd48_device_status_abort_mask = DEVICE_MALFUNCTION
 * - cmd48_device_status_retry_mask = NO BITS SET
 * - comm_status_abort_mask = NO BITS SET
 * - comm_status_retry_mask = ALL BITS SET
 * - cmd48_comm_status_abort_mask = NO BITS SET
 * - cmd48_comm_status_retry_mask = ALL BITS SET
 * - no_device_abort_mask = ALL BITS SET
 * - comm_error_abort_mask = ALL BITS SET
 * - comm_error_retry_mask = NO BITS SET
 * - cmd48_no_device_abort_mask = ALL BITS SET
 * - cmd48_comm_error_abort_mask = ALL BITS SET
 * - cmd48_comm_error_retry_mask = NO BITS SET
 * - cmd48_data_abort_mask = NO BITS SET
 * - cmd48_data_retry_mask = NO BITS SET
 * .
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status, and display.
 */
void ABORT_ON_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_ABORT_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin AddTime.
  * The Builtin AddTime adds a number of seconds to a given value.
  *
  * \param t0 the initial value to be used
  * \param seconds the number of seconds to be added
  *
  * \return the resulting time

  * \note TODO: AddTime is defined using time_t in the IEC spec.
  */
edd_int32 AddTime(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 t0,
		edd_real64 seconds);

edd_string BUILD_MESSAGE(
		class method_env *env, const lib::pos &caller_pos,
		edd_string text);

/**
 * The builtin ByteToDouble.
 * Builtin ByteToDouble will return a double value from a vector of four octets.
 */
edd_real64 ByteToDouble(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint08 in0,
		edd_uint08 in1,
		edd_uint08 in2,
		edd_uint08 in3,
		edd_uint08 in4,
		edd_uint08 in5,
		edd_uint08 in6,
		edd_uint08 in7);

/**
 * The builtin ByteToFloat.
 * Builtin ByteToFloat will return a floating value from a vector of four octets.
 */
edd_real32 ByteToFloat(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint08 in0,
		edd_uint08 in1,
		edd_uint08 in2,
		edd_uint08 in3);

/**
 * The builtin ByteToLong.
 * Builtin ByteToLong will return a long value from a vector of four octets.
 */
edd_int32 ByteToLong(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint08 in0,
		edd_uint08 in1,
		edd_uint08 in2,
		edd_uint08 in3);

/**
 * The builtin ByteToShort.
 * Builtin ByteToShort will return a short value from a vector of two octets.
 */
edd_int16 ByteToShort(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint08 in0,
		edd_uint08 in1);

#ifndef DOXYGEN_IGNORE
void check_Date_to_DayOfMonth_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_Date_to_Month_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_Date_to_Month_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_Date_to_Year_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin DiffTime.
 * The Builtin DiffTime calculates the difference between two calender 
 * times.
 *
 * \param t1 The more recent time
 * \param t0 The less recent time
 *
 * \return The difference between t1 and t0 in seconds
 *
 * \note TODO t0, t1 are specified as time_t in the IEC spec.
 */
edd_real64 DiffTime(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 t1,
		edd_int32 t0);

/**
 * The builtin DoubleToByte.
 * Builtin DoubleToByte will provide a vector of unsigned1 values representing
 * the particular elements of a double value.
 */
void DoubleToByte(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 in,
		edd_uint08_ref &out0,
		edd_uint08_ref &out1,
		edd_uint08_ref &out2,
		edd_uint08_ref &out3,
		edd_uint08_ref &out4,
		edd_uint08_ref &out5,
		edd_uint08_ref &out6,
		edd_uint08_ref &out7);

/**
 * The builtin FloatToByte.
 * Builtin FloatToByte will provide a vector of unsigned1 values representing
 * the particular elements of a float value.
 */
void FloatToByte(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 in,
		edd_uint08_ref &out0,
		edd_uint08_ref &out1,
		edd_uint08_ref &out2,
		edd_uint08_ref &out3);

/**
 * The builtin GET_DD_REVISION.
 * The Builtin GET_DD_REVISION gets the DD_REVISION from the currently
 * used device description as stated in the identification of the devic
 * description with the keyword DD_REVISION.
 *
 * \return The specified DD_REVISION
 */
edd_int32 GET_DD_REVISION(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin GET_DEVICE_REVISION.
 * The Builtin GET_DEVICE_REVISION gets the DEVICE_REVISION from the
 * currently used device description as stated in the identification of the
 * device description with the keyword DEVICE_REVISION.
 *
 * \return The specified DEVICE_REVISION
 */
edd_int32 GET_DEVICE_REVISION(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin GET_DEVICE_TYPE.
 * The Builtin GET_DEVICE_TYPE gets the numerical equivalent of the DEVICE_TYPE
 * from the currently used device description as stated in the identification of
 * the device description with the keyword DEVICE_TYPE.
 *
 * \return The specified DEVICE_TYPE
 */
edd_int32 GET_DEVICE_TYPE(
		class method_env *env, const lib::pos &caller_pos);

#ifndef DOXYGEN_IGNORE
void check_GET_DEV_VAR_VALUE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_GET_LOCAL_VAR_VALUE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin GET_MANUFACTURER.
 * The Builtin GET_MANUFACTURER gets the numerical equivalent of the MANUFACTURER
 * from the currently used device description as stated in the identification of
 * the device description with the keyword MANUFACTURER.
 *
 * \return The specified MANUFACTURER
 */
edd_int32 GET_MANUFACTURER(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin GET_TICK_COUNT.
 * The Builtin GET_TICK_COUNT returns the time in milliseconds since the last
 * system boot. It can be used for timestamps.
 * 
 * \note In order to the Datatype long, the returnvalue of GET_TICK_COUNT will
 * wrap around and start from zero after a period of 49,71026961806 days.
 * 
 * \note Not mentioned in any profile in the standard.
 * \todo Sync with standard.
 */
edd_int32 GET_TICK_COUNT(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_ALL_COMM_STATUS.
 * The Builtin IGNORE_ALL_COMM_STATUS will clear all of the bits in the comm
 * status retry and abort masks. This will cause the system to ignore all bits
 * in the comm status value. Comm status is defined to be the first data octet
 * returned in a  transaction, when bit 7 of this octet is set.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the Builtin send_command for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_ALL_DEVICE_STATUS.
 * The Builtin IGNORE_ALL_DEVICE_STATUS will clear all of the bits in the
 * device status retry and abort masks. This will cause the system to ignore
 * all bits in the device status octet. Device status is defined to be the
 * second data octet returned in a  transaction.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the Builtin send_command for implementation of the masks. See
 * ABORT_ON_RESPONSE_CODE for a list of the available masks and their default
 * values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_ALL_RESPONSE_CODES.
 * The Builtin IGNORE_ALL_RESPONSE_CODES will clear all of the bits in the
 * response code retry and abort masks. This will cause the system to ignore
 * all response code values returned from the device.
 * 
 * The response code is defined to be the first data octet returned in a
 * transaction, when bit 7 of this octet is 0.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the Builtin send_command for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_COMM_ERROR.
 * The Builtin IGNORE_COMM_ERROR will set the comm error mask such that a
 * communications error condition will be ignored while sending a command.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_COMM_STATUS.
 * The Builtin IGNORE_COMM_STATUS will clear the correct bit(s) in the comm
 * status abort and retry mask such that the specified bits in the comm_status
 * value will be ignored. comm_status is defined to be the first data octet
 * returned in a  transaction, when bit 7 of this octet is 1.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_IGNORE_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin IGNORE_DEVICE_STATUS.
 * The Builtin IGNORE_DEVICE_STATUS will clear the correct bit(s) in the
 * device status abort and retry masks such that the specified bits in the
 * device status octet are ignored. Device status is defined to be the second
 * data octet returned in a  transaction.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_IGNORE_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin IGNORE_NO_DEVICE.
 * The Builtin IGNORE_NO_DEVICE will set the no device mask to show that the
 * no device condition should be ignored while sending a command.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin IGNORE_RESPONSE_CODE.
 * The Builtin IGNORE_RESPONSE_CODE will clear the correct bit(s) in the
 * response code masks such that the specified response code value will be
 * ignored. The response code is defined to be the first data octet returned
 * in a  transaction, when bit 7 of this octet is 0.
 * 
 * The retry and abort masks are reset to their default values at the start of
 * each method, so the new mask value will only be valid during the current
 * method. See the send_command function for implementation of the masks. See
 * Builtin ABORT_ON_RESPONSE_CODE for a list of the available masks and their
 * default values.
 * 
 * The mask affected by this function is used by the Builtins send,
 * send_trans, send_command, send_command_trans, ext_send_command,
 * ext_send_command_trans, get_more_status and display.
 */
void IGNORE_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_IGNORE_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ITEM_ID.
 * The Builtin ITEM_ID can be used in methods to specify the item ID of a
 * variable for calls to other.
 * 
 * \todo BUILTIN ITEM_ID unclear
 */
edd_uint32 ITEM_ID(
		class method_env *env, const lib::pos &caller_pos,
		class tree * name);

/**
 * The builtin ListDeleteElementAt.
 * Builtin ListDeleteElementAt will delete an element from a list. If the list
 * is a member of a FILE, the specified element will be removed from the
 * persistent store.
 * 
 * If a COUNT are explicitly declared on the specified LIST, any variables
 * referenced by the COUNT attribute will not be updated. The method is
 * responsible for updating the COUNT when elements are deleted from the list.
 * If a COUNT are not explicitly declared, the COUNT attribute will be
 * automatically updated.
 * 
 * If the index parameter is less than zero or greater than the number of
 * elements in the list, an error will be returned. If the list parameter
 * does not refer to a LIST instance, an error will be returned.
 * 
 * \note ListDeleteElementAt is an EDD 1.2 enhancement.
 */
edd_int32 ListDeleteElementAt(
		class method_env *env, const lib::pos &caller_pos,
		class tree * listname,
		edd_int32 index);
#ifndef DOXYGEN_IGNORE
void check_ListDeleteElementAt_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ListInsert.
 * Builtin ListInsert will insert an element to a list. If the list is a
 * member of a FILE, the specified element will be added to the persistent
 * store.
 * 
 * If COUNT and/or CAPACITY attributes are explicitly declared on the
 * specified LIST, any variables referenced by these attributes will not be
 * updated. The method is responsible for updating the COUNT and CAPACITY when
 * elements are inserted in the list. If a COUNT and/or CAPACITY attributes
 * are not explicitly declared, these attributes will be updated
 * automatically.
 * 
 * If the index parameter is less than zero or greater than the capacity of
 * the list, an error will be returned. If the list parameter does not refer
 * to a LIST instance, an error will be returned.
 * 
 * \note ListInsert is an EDD 1.2 enhancement.
 */
edd_int32 ListInsert(
		class method_env *env, const lib::pos &caller_pos,
		class tree * listname,
		edd_int32 index,
		class tree * object);
#ifndef DOXYGEN_IGNORE
void check_ListInsert_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin LongToByte.
 * Builtin LongToByte will provide a vector of unsigned1 values representing
 * the particular elements of a long value.
 */
void LongToByte(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 in,
		edd_uint08_ref &out0,
		edd_uint08_ref &out1,
		edd_uint08_ref &out2,
		edd_uint08_ref &out3);

/**
 * The builtin METHODID.
 * Same like VARID (following the usage in PDM).
 */
edd_int32 METHODID(
		class method_env *env, const lib::pos &caller_pos,
		class tree * variable_name);
#ifndef DOXYGEN_IGNORE
void check_METHODID_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_MenuDisplay_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin NaN_value.
 * The Builtin NaN_value returns the value of "not a number".
 *
 * \param value A pointer through which the value is returned
 *
 * \return BI_SUCCESS
 * \note IEC says returns the value - looked up in FF spec
 */
edd_int32 NaN_value(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64_ref &value);

/**
 * The builtin READ_COMMAND.
 * Builtin READ_COMMAND allows reading variables with a given command.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param command_name specifies the Identifier of the command to execute.
 */
edd_int32 READ_COMMAND(
		class method_env *env, const lib::pos &caller_pos,
		class tree * command_name);
#ifndef DOXYGEN_IGNORE
void check_READ_COMMAND_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void RETRY_ON_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void RETRY_ON_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void RETRY_ON_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

void RETRY_ON_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

void RETRY_ON_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_RETRY_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void RETRY_ON_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_RETRY_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void RETRY_ON_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

void RETRY_ON_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_RETRY_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 ReadCommand(
		class method_env *env, const lib::pos &caller_pos,
		class tree * command_name);
#ifndef DOXYGEN_IGNORE
void check_ReadCommand_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin SET_NUMBER_OF_RETRIES.
 * The Builtin SET_NUMBER_OF_RETRIES sets up the number of times a command
 * will be retried due to the appropriate retry masks. This value is defaulted
 * to 0 at the beginning of each method.
 */
void SET_NUMBER_OF_RETRIES(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 n);

/**
 * The builtin ShortToByte.
 * Builtin ShortToByte will provide a vector of unsigned1 values representing
 * the particular elements of a short value.
 */
void ShortToByte(
		class method_env *env, const lib::pos &caller_pos,
		edd_int16 in,
		edd_uint08_ref &out0,
		edd_uint08_ref &out1);

/**
 * The builtin TIME_VALUE_to_Hour.
 * The Builtin TIME_VALUE_to_Hour extracts the hour component from a TIME_VALUE.
 *
 * \param time_value The TIME_VALUE
 *
 * \return The hour component
 */
edd_int32 TIME_VALUE_to_Hour(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 time_value);

/**
 * The builtin TIME_VALUE_to_Minute.
 * The Builtin TIME_VALUE_to_Minute extracts the minute component from a TIME_VALUE.
 *
 * \param time_value The TIME_VALUE
 *
 * \return The minute component
 */
edd_int32 TIME_VALUE_to_Minute(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 time_value);

/**
 * The builtin TIME_VALUE_to_Second.
 * The Builtin TIME_VALUE_to_Second extracts the seconds component from a TIME_VALUE.
 *
 * \param time_value The TIME_VALUE
 *
 * \return The seconds component
 */
edd_int32 TIME_VALUE_to_Second(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 time_value);

/**
 * The builtin TIME_VALUE_to_seconds.
 * The Builtin TIME_VALUE_to_seconds converts a TIME_VALUE to a number of seconds.
 *
 * \param time_value The TIME_VALUE
 *
 * \return The number of seconds
 */
edd_real64 TIME_VALUE_to_seconds(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 time_value);

/**
 * The builtin TIME_VALUE_to_string.
 * The Builtin TIME_VALUE_to_string creates a string representation of a
 * TIME_VALUE. The ASI C strftime function defines the structure of the format string.
 *
 * \param string The string into which the formatted TIME_VALUE is copied.
 * \param format How the TIME_VALUE is to be formatted.
 * \param time_value The TIME_VALUE to be formatted
 *
 * \return The nu,mber of cahracters actually copied or -1 if an error occurs.
 */
edd_int32 TIME_VALUE_to_string(
		class method_env *env, const lib::pos &caller_pos,
		edd_string_ref &str,
		edd_string format,
		edd_uint32 time_value);

/**
 * The builtin To_TIME_VALUE.
 * The Builtin To_TIME_VALUE creates a TIME_VALUE from its input arguments.
 *
 * \param hours The number of hours.
 * \param minutes The number of minutes.
 * \param seconds The number of seconds.
 *
 * \return the TIME_VALUE
 */
edd_real64 To_TIME_VALUE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 hours,
		edd_int32 minutes,
		edd_int32 seconds);

/**
 * The builtin VARID.
 * The Builtin VARID will return the numeric eddobject for the variable
 * specified. A valid variable name must be provided. Each variable in the
 * device description is assigned a unique numeric eddobject. This function
 * is to be used when the eddobject of a variable either needs to be stored
 * in a temporary buffer, or needs to be sent as a parameter to another
 * Builtin.
 */
edd_int32 VARID(
		class method_env *env, const lib::pos &caller_pos,
		class tree * variable_name);
#ifndef DOXYGEN_IGNORE
void check_VARID_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin WRITE_COMMAND.
 * Builtin WRITE_COMMAND allows writing variables with a given command.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param command_name specifies the Identifier of the command to execute.
 */
edd_int32 WRITE_COMMAND(
		class method_env *env, const lib::pos &caller_pos,
		class tree * command_name);
#ifndef DOXYGEN_IGNORE
void check_WRITE_COMMAND_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 WriteCommand(
		class method_env *env, const lib::pos &caller_pos,
		class tree * command_name);
#ifndef DOXYGEN_IGNORE
void check_WriteCommand_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_ABORT_ON_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_ALL_DATA(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_ABORT_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_ABORT_ON_DATA(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 byte,
		edd_int32 bit);
#ifndef DOXYGEN_IGNORE
void check_XMTR_ABORT_ON_DATA_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_ABORT_ON_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_ABORT_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_ABORT_ON_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_ABORT_ON_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_ABORT_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_IGNORE_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_ALL_DATA(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_IGNORE_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_IGNORE_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_IGNORE_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_IGNORE_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_IGNORE_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_IGNORE_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_RETRY_ON_ALL_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_ALL_DATA(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_ALL_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_ALL_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_ALL_RESPONSE_CODES(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_COMM_ERROR(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_COMM_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_RETRY_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_RETRY_ON_DATA(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 byte,
		edd_int32 bit);
#ifndef DOXYGEN_IGNORE
void check_XMTR_RETRY_ON_DATA_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_RETRY_ON_DEVICE_STATUS(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_RETRY_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void XMTR_RETRY_ON_NO_DEVICE(
		class method_env *env, const lib::pos &caller_pos);

void XMTR_RETRY_ON_RESPONSE_CODE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_XMTR_RETRY_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_real32 abs(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 value);

edd_real64 abs_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

edd_real32 acos(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 value);

edd_real64 acos_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

/**
 * The builtin add_abort_method.
 * The Builtin add_abort_method will add a method to the abort method list,
 * which is the list of methods to be executed if the current method is
 * aborted. The abort method list can hold up to twenty methods at any one
 * time. The methods are run in the order they are added to the list, and the
 * same method may be added to the list more than once. The list is cleared
 * after each non_abort method is executed.
 * \note It is important to note that the abort methods are only executed when
 * the method is aborted, and not when you exit the method under normal
 * operating conditions. Methods can be aborted due to an abort mask condition
 * when sending a command, or when the Builtin abort is called.
 */
edd_int32 add_abort_method(
		class method_env *env, const lib::pos &caller_pos,
		class tree * abort_method_name);
#ifndef DOXYGEN_IGNORE
void check_add_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin add_abort_method_1.
 * The Builtin add_abort_method adds an abort method to the calling method's
 * abort method list. The abort method list is the list of methods, which will
 * be executed if the method aborts. The abort method list applies only to the
 * current execution of the method.
 * 
 * Methods abort when response codes or communications errors occur which have
 * been set to trigger an abort via other Builtin calls; for example,
 * abort_on_all_comm_error and abort_on_all_response_codes. Methods can also
 * be explicitly aborted by a direct call to Builtin method _abort, or when an
 * error occurs. The normal completion of a method's execution is not
 * considered an abort.
 * 
 * The abort method list is initially empty when a method begins execution.
 * The abort methods are added to the back of the list, and are removed and
 * run from the front of the list in the order they appear, i.e. first in
 * first out. An abort method may appear more than once in the list.
 * 
 * Abort methods may be shared between methods, meaning two methods may have
 * the same abort method in their respective abort method lists.
 * 
 * The method_id parameter is specified by the method writer using the
 * Builtin ITEM_ID and the name of the method.
 */
edd_int32 add_abort_method_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 method_id);

edd_real32 asin(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 value);

edd_real64 asin_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

/**
 * The builtin assign_double.
 * The Builtin assign_double will assign the specified value to the device
 * variable. The variable must be valid, and must reference a variable of type
 * double.
 */
edd_int32 assign_double(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_real64 new_value);
#ifndef DOXYGEN_IGNORE
void check_assign_double_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin assign_float.
 * The Builtin assign_float will assign the specified value to the device
 * variable. The variable must be valid, and must reference a variable of type
 * float.
 */
edd_int32 assign_float(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_real64 new_value);
#ifndef DOXYGEN_IGNORE
void check_assign_float_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin assign_int.
 * The Builtin assign_int will assign the specified value to the device
 * variable. The variable must be valid, and must reference a variable of type
 * int.
 */
edd_int32 assign_int(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_int32 new_value);
#ifndef DOXYGEN_IGNORE
void check_assign_int_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 assign_long(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_int32 new_value);
#ifndef DOXYGEN_IGNORE
void check_assign_long_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin assign_str.
 * The Builtin assign_str will assign the specified value to the device
 * variable. The variable must be valid. If neccessary, the value is casted
 * to the type of the referenced variable. See assign_float for an example
 * of the syntax of this function 
 */
edd_int32 assign_str(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_string new_value);
#ifndef DOXYGEN_IGNORE
void check_assign_str_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin assign_var.
 * The Builtin assign_var will assign the value of the source variable to the
 * destination variable. Both variables must be valid, and they must have the
 * same type.
 */
edd_int32 assign_var(
		class method_env *env, const lib::pos &caller_pos,
		class tree * destination_var,
		class tree * source_var);
#ifndef DOXYGEN_IGNORE
void check_assign_var_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_real32 atan(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 value);

edd_real64 atan_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

edd_real32 atof(
		class method_env *env, const lib::pos &caller_pos,
		edd_string text);

edd_int32 atoi(
		class method_env *env, const lib::pos &caller_pos,
		edd_string text);

edd_int32 atol(
		class method_env *env, const lib::pos &caller_pos,
		edd_string text);

edd_real32 cbrt(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 cbrt_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 ceil(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 ceil_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 cos(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 cos_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 cosh(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 cosh_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin dassign.
 * The Builtin dassign will assign the specified value to the device
 * variable. The variable must be valid, and must reference a variable of type
 * double.
 */
edd_int32 dassign(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_real64 new_value);
#ifndef DOXYGEN_IGNORE
void check_dassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin dictionary_string.
 * The Builtin dictionary_string will retrieve and return the dictionary
 * string associated with the given name in the current language. If the
 * string is not available in the current language, the English string will
 * be returned. If the string is not found in the dictionary the function will
 * return "ERROR: Dict String Not Found" which is a standard string defined in
 * the dictionary ([400,0] dict_str_not_found).
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param dict_string_name is the handle of the string as it appears in the
 * standard dictionary.
 * 
 * \return The dictionary string referenced by dict_string_name if successful,
 * “ERROR: Dict String Not Found” if string could not be found in the
 * dictionary.
 */
edd_string dictionary_string(
		class method_env *env, const lib::pos &caller_pos,
		lib::IDENTIFIER dict_string_name);
#ifndef DOXYGEN_IGNORE
void check_dictionary_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin discard_on_exit.
 * The Builtin discard_on_exit sets the termination action to discard any
 * changes made by the method to VARIABLE values stored in a caching EDD
 * application VARIABLE table, which have not already been sent to the device.
 * It causes the method to discard any changes to VARIABLE values in the
 * VARIABLE table when the method ends.
 * 
 * When a method modifies a VARIABLE value in a caching EDD application, it
 * is changing a resident copy of that value. This change is effective only
 * for the life of the method. To change the value(s) in the device, the
 * method shall send the value(s) to the device or call send_on_exit. To save
 * the values only in the VARIABLE table (rather than send them to the
 * device), call the Builtin save_on_exit.
 * 
 * After method termination, the changed values in the VARIABLE table return
 * to the device values that existed before they were changed by the method.
 * 
 * All values that have been changed but not sent to the device are handled at
 * the termination of the method by either discarding them, sending them to
 * the device, or saving the changes in a way that survives the end of the
 * method (but does not affect the values in the device).
 * 
 * This Builtin takes no action in a non-caching EDD application because all
 * parameter values are written directly to the device.
 * 
 * If a method does not call discard_on_exit, save_on_exit, or send_on_exit
 * before exiting, any values changed by the Builtin are discarded as if the
 * Builtin discard_on_exit had been called.
 */
void discard_on_exit(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin display_comm_status.
 * The Builtin display_comm_status will display the string associated with
 * the specified value of the comm_status_value octet. The displayed string
 * will remain on the screen until operator acknowledgement is made.
 * 
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * 
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 */
void display_comm_status(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 comm_status_value);
#ifndef DOXYGEN_IGNORE
void check_display_comm_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin display_device_status.
 * The Builtin display_device_status will display the string associated with
 * the specified value of the device_status octet. The displayed string will
 * remain on the screen until operator acknowledgement is made.
 * 
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * 
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 */
void display_device_status(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 device_status_value);
#ifndef DOXYGEN_IGNORE
void check_display_device_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin display_response_status.
 * The Builtin display_response_status will display the string associated with
 * the specified value of the response_code octet. The displayed string will
 * remain on the screen until operator acknowledgement is made.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 */
void display_response_status(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd,
		edd_int32 response_code_value);
#ifndef DOXYGEN_IGNORE
void check_display_response_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin display_xmtr_status.
 * The Builtin display_xmtr_status will display the string associated with the
 * specified value of the indicated transmitter VARIABLE. The displayed string
 * will remain on the screen until operator acknowledgement is made.
 * \note Previously displayed messages are not guaranteed to remain on the
 * screen.
 * \note Some EDD applications will clear the display upon each call to this
 * function.
 */
void display_xmtr_status(
		class method_env *env, const lib::pos &caller_pos,
		class tree * xmtr_variable,
		edd_int32 response_code);
#ifndef DOXYGEN_IGNORE
void check_display_xmtr_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_real32 exp(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 exp_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin ext_send_command.
 * ext_send_command has the same functionality as the send_command function
 * except that the command status and data bytes returned from the device
 * with command 48 are also returned.
 * 
 * The calling function is responsible for allocating arrays for the data to
 * be returned. The cmd_status and more_data_status variables are arrays of
 * size STATUS_SIZE, and the more_data_info variable is an array whose size
 * is equal to the number of data bytes that can be returned by the device in
 * command 48 (maximum of MAX_XMTR_STATUS_LEN). If the status bit indicating
 * that more data is available is returned (status class MORE), command 48 is
 * automatically issued. If command 48 was not issued, the more_data_status
 * and more_data_info bytes are set to zero.
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found.
 */
edd_int32 ext_send_command(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_string_ref &cmd_status,
		edd_string_ref &more_data_status,
		edd_string_ref &more_data_info);
#ifndef DOXYGEN_IGNORE
void check_ext_send_command_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ext_send_command_trans.
 * Send the specified transaction of a HART command.
 * 
 * ext_send_command_trans sends the HART command with the specified
 * transaction to the device. This function is to be used to send commands
 * that have been defined with multiple transactions. ext_send_command_trans
 * has the same functionality as the send_command function except that the
 * command status and data bytes returned from the device with command 48 are
 * also returned.
 * 
 * The calling function is responsible for allocating arrays for the data to
 * be returned. The cmd_status and more_data_status variables are arrays of
 * size STATUS_SIZE, and the more_data_info variable is an array whose size
 * is equal to the number of data bytes that can be returned by the device in
 * command 48 (maximum of MAX_XMTR_STATUS_LEN). If the status bit indicating
 * that more data is available is returned (status class MORE), command 48 is
 * automatically issued. If command 48 was not issued, the more_data_status
 * and more_data_info bytes are set to zero.
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found
 */
edd_int32 ext_send_command_trans(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_int32 transaction,
		edd_string_ref &cmd_status,
		edd_string_ref &more_data_status,
		edd_string_ref &more_data_info);
#ifndef DOXYGEN_IGNORE
void check_ext_send_command_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin fassign.
 * The Builtin fassign will assign the specified value to the device variable.
 * The variable must be valid, and must reference a variable of type float.
 */
edd_int32 fassign(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_real64 new_value);
#ifndef DOXYGEN_IGNORE
void check_fassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin fgetval.
 * The Builtin fgetval is specifically designed to be used in pre-read/write
 * and post-read/write methods. This function will return the value of a
 * device VARIABLE as it was received from the connected device. Scaling
 * operations may then be performed on the VARIABLE value before it is stored
 * in the EDD application.
 */
edd_real64 fgetval(
		class method_env *env, const lib::pos &caller_pos);
#ifndef DOXYGEN_IGNORE
void check_fgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin float_value.
 * The Builtin float_value will return the value of the specified device
 * variable. The variable must be valid and of type float.
 */
edd_real64 float_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_float_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_real32 floor(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 floor_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 fmod(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x,
		edd_real32 y);

edd_real64 fmod_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x,
		edd_real64 y);

/**
 * The builtin fpclassify.
 * The Builtin fpclassify classifies a floating point value as either not a
 * number, infinite, zero, subnormal or normal.
 *
 * \param value The floating point value to be classified.
 */
edd_int32 fpclassify(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

/**
 * The builtin fsetval.
 * The Builtin fsetval sets the value of a VARIABLE of type float for which
 * a pre/post action has been designated to the specified value.
 * 
 * The function fsetval is specifically designed to be used in pre-read/write
 * and post-read/write methods. This function will set the value of a device
 * VARIABLE to the specified value.
 */
edd_real64 fsetval(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);
#ifndef DOXYGEN_IGNORE
void check_fsetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin ftoa.
 * This Builtin converts a floating point value into a text string.
 * 
 * XXX missing in HART and IEC spec?
 */
edd_string ftoa(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 value);

/**
 * The builtin fvar_value.
 * The Builtin fvar_value will return the value of the specified device
 * variable. The variable must be valid and of type float.
 */
edd_real64 fvar_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_fvar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_get_dev_var_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_dictionary_string.
 * The Builtin get_dictionary_string will retrieve the dictionary string
 * associated with the given name in the current language. If the string
 * is not available in the current language, the English string will be
 * retrieved. If the string is not defined in either language, an error
 * condition occurs, and the function will return BI_ERROR. If the string
 * is longer than the max_str_len, the string will be truncated.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param dict_string_name is the name of the string as it appears in the
 * standard dictionary.
 * \param str The string there the dictionary entry is placed.
 * \param maxlen The maximum len of the str parameter. If the dictionary entry
 * is longer than maxlen it will be truncated.
 * 
 * \return BI_SUCCESS if successful, BI_ERROR if string could not be found.
 */
edd_int32 get_dictionary_string(
		class method_env *env, const lib::pos &caller_pos,
		lib::IDENTIFIER dict_string_name,
		edd_string_ref &str,
		edd_int32 maxlen);
#ifndef DOXYGEN_IGNORE
void check_get_dictionary_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_dictionary_string_1.
 * The Builtin get_dictionary_string will retrieve the dictionary string
 * associated with the given name in the current language. If the string
 * is not available in the current language, the English string will be
 * retrieved. If the string is not defined in either language, an error
 * condition occurs, and the function will return BI_ERROR. If the string
 * is longer than the max_str_len, the string will be truncated.
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param dict_string_name is the name of the string as it appears in the
 * standard dictionary.
 * \param str The string there the dictionary entry is placed.
 * 
 * \return BI_SUCCESS if successful, BI_ERROR if string could not be found.
 * 
 * \note Undocumented SIMATIC PDM builtin without max_str_len parameter.
 */
edd_int32 get_dictionary_string_1(
		class method_env *env, const lib::pos &caller_pos,
		lib::IDENTIFIER dict_string_name,
		edd_string_ref &str);
#ifndef DOXYGEN_IGNORE
void check_get_dictionary_string_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_enum_string.
 * The Builtin get_enum_string get a string associated with the value of an
 * enumerated or bit enumerated variable (for bit-enums, only provide
 * a single bit value).
 * 
 * \param env The environment of the caller.
 * \param caller_pos The position of the caller.
 * \param variable The VARIABLE object.
 * \param value The value of the enumeration. 
 * \param status_string contains the string value for the specified enumerated or
 * bit enumerated variable and value. The value must be valid for the
 * specified DD Variable.
 * 
 * \return BI_SUCCESS if successful, BI_ERROR if string could not be found.
 */
edd_int32 get_enum_string(
		class method_env *env, const lib::pos &caller_pos,
		class tree * variable,
		edd_int32 value,
		edd_string_ref &status_string);
#ifndef DOXYGEN_IGNORE
void check_get_enum_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

#ifndef DOXYGEN_IGNORE
void check_get_local_var_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_more_status.
 * Get results of command 48.
 * 
 * get_more_status will issue a command 48 to the device and return the
 * response code, communications status, and command status bytes in the
 * status array provided. It will also return any data bytes returned in
 * the more_data_info array. This function will process the status/data
 * bytes in the same manner as send_command.
 * 
 * The calling function is responsible for allocating arrays for the data to
 * be returned. The more_data_status variable is an array of size STATUS_SIZE,
 * and the more_data_info variable is an array whose size is equal to the
 * number of data bytes that can be returned by the device in command 48
 * (maximum of MAX_XMTR_STATUS_LEN).
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found.
 */
edd_int32 get_more_status(
		class method_env *env, const lib::pos &caller_pos,
		edd_string_ref &more_data_status,
		edd_string_ref &more_data_info);
#ifndef DOXYGEN_IGNORE
void check_get_more_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_rspcode_string.
 * Get a response code string.
 * 
 * response_string will contain the response code string for the specified
 * command and response code. If the string is longer than the maximum length
 * defined in response_string_length, the string is truncated. The response
 * code specified must be valid for the indicated command.
 * 
 * \return BI_SUCCESS if successful, BI_ERROR if string could not be found.
 */
edd_int32 get_rspcode_string(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_int32 response_code_value,
		edd_string_ref &response_string,
		edd_int32 response_string_length);
#ifndef DOXYGEN_IGNORE
void check_get_rspcode_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 get_rspcode_string_by_id(
		class method_env *env, const lib::pos &caller_pos,
		class tree * command_name,
		edd_int32 response_code_value,
		edd_string_ref &response_string);
#ifndef DOXYGEN_IGNORE
void check_get_rspcode_string_by_id_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin get_status_code_string.
 * The Builtin get_status_code_string will return status code string for the
 * specified device VARIABLE and status code value. If the string is longer
 * than the maximum length defined in status_string_length, the string is
 * truncated. The status code value shall be valid for the specified device
 * VARIABLE.
 */
edd_int32 get_status_code_string(
		class method_env *env, const lib::pos &caller_pos,
		class tree * variable_name,
		edd_int32 status_code,
		edd_string_ref &status_string,
		edd_int32 status_string_length);
#ifndef DOXYGEN_IGNORE
void check_get_status_code_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void get_variable_string(
		class method_env *env, const lib::pos &caller_pos,
		class tree * variable,
		edd_string_ref &out);
#ifndef DOXYGEN_IGNORE
void check_get_variable_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin iassign.
 * The Builtin iassign will assign the specified value to the device variable.
 * The variable must be valid, and must reference a variable of type int.
 */
edd_int32 iassign(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_int32 new_value);
#ifndef DOXYGEN_IGNORE
void check_iassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 igetval(
		class method_env *env, const lib::pos &caller_pos);
#ifndef DOXYGEN_IGNORE
void check_igetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin int_value.
 * The Builtin int_value will return the value of the specified device
 * variable. The variable eddobject must be valid and of type integer.
 */
edd_int32 int_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_int_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin is_NaN.
 * The Builtin is_NaN checks whether a double floating-point value is not a
 * number. Not a number is specified by the IEEE 754 specification as an
 * invalid double value. The value for NaN for a double value is:
 * 0x7FF8000000000000.
 */
edd_int32 is_NaN(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 dvalue);

/**
 * The builtin isetval.
 * The Builtin isetval will set the value of the device variable of type
 * int for which a pre/post action has been designated to the specified
 * value. isetval is specifically designed to be used in pre-read/write and
 * post-read/write methods. This function will set the value of a device
 * variable to the specified value.
 */
edd_int32 isetval(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 value);
#ifndef DOXYGEN_IGNORE
void check_isetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_string itoa(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 value);

/**
 * The builtin ivar_value.
 * The Builtin ivar_value will return the value of the specified variable. The
 * variable source_var_name must be valid and of type integer.
 */
edd_int32 ivar_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_ivar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin lassign.
 * The Builtin lassign will assign the specified value to the device variable.
 * The variable must be valid, and must reference a variable of type long.
 */
edd_int32 lassign(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		edd_int32 new_value);
#ifndef DOXYGEN_IGNORE
void check_lassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin lgetval.
 * The Builtin lgetval is specifically designed to be used in pre-read/write
 * and post-read/write methods. This function returns the value of a VARIABLE
 * as it was received from the connected device. Scaling operations may then
 * be performed on the VARIABLE value before it is stored in the EDD
 * application.
 */
edd_int32 lgetval(
		class method_env *env, const lib::pos &caller_pos);
#ifndef DOXYGEN_IGNORE
void check_lgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_real32 log(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 log_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 log10(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 log10_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 log2(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 log2_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin long_value.
 * The Builtin long_value will return the value of the specified device
 * variable. The variable eddobject shall be valid and of type long.
 */
edd_int32 long_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_long_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin lsetval.
 * The Builtin lsetval will set the value of the device variable of type long
 * for which a pre/post action has been designated to the specified value. It
 * is specifically designed to be used in pre-read/write and post-read/write
 * methods. This function will set the value of a device variable to the
 * specified value.
 */
edd_int32 lsetval(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 value);
#ifndef DOXYGEN_IGNORE
void check_lsetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin lvar_value.
 * The Builtin lvar_value will return the value of the specified device
 * variable. The variable eddobject must be valid and of type long.
 */
edd_int32 lvar_value(
		class method_env *env, const lib::pos &caller_pos,
		class tree * source_var_name);
#ifndef DOXYGEN_IGNORE
void check_lvar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin nan.
 * The Builtin nan returns a "not a number" value as a double with a 
 * fractional bit pattern based on a string. If the character sequence begins
 * with a quiet or signaling NaN prefix letter, then any following hex digits
 * in the character sequence must be valid for that type of NaN.
 *
 * \param value The "not a number" value to be returned.
 *
 * \return The specified value.
 */
edd_real64 nan(
		class method_env *env, const lib::pos &caller_pos,
		edd_string value);

/**
 * The builtin nanf.
 * The Builtin nanf returns a "not a number" value as a float with a 
 * fractional bit pattern based on a string. If the character sequence begins
 * with a quiet or signaling NaN prefix letter, then any following hex digits
 * in the character sequence must be valid for that type of NaN.
 *
 * \param value The "not a number" value to be returned.
 *
 * \return The specified value.
 */
edd_real32 nanf(
		class method_env *env, const lib::pos &caller_pos,
		edd_string value);

/**
 * The builtin p_abort.
 * p_abort will display a message indicating that the method has been aborted
 * and wait for acknowledgement from the user. Once acknowledgement has been
 * made, the system will execute any abort methods in the abort method list,
 * and will exit the method.
 * 
 * \todo Not implemented, examples in PDM use incorrect syntax:
 * if (choice == -1) p_abort;
 */
void p_abort(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin pop_abort_method.
 * Removes an abort method from the top of the abort list. pop_abort_method
 * will remove a method from the top of abort method list, which is the list
 * of methods to be executed if the current method is aborted. Abort methods
 * may not be removed during an abort method.
 * 
 * \return BI_SUCCESS if the method was successfully removed from the list,
 * and BI_ERROR if either no methods are in the list or if this function was
 * run during an abort method.
 * 
 * \note pop_abort_method is an EDD 1.2 enhancement.
 */
edd_int32 pop_abort_method(
		class method_env *env, const lib::pos &caller_pos);

edd_real32 pow(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x,
		edd_real32 y);

edd_real64 pow_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x,
		edd_real64 y);

/**
 * The builtin process_abort.
 * The Builtin process_abort will abort the current method, running any abort
 * methods, which are in the abort method list. Unlike the Builtin abort, no
 * message will be displayed when this function is executed. This Builtin
 * function may not be run from inside an abort method.
 */
void process_abort(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin push_abort_method.
 * Add a method to the top of the abort list. push_abort_method will add a
 * method to the abort method list, which is the list of methods to be
 * executed if the current method is aborted. The abort method list can hold
 * up to twenty methods at any one time. Pushing an abort method puts the
 * method at the top of the abort method list. The abort method list for the
 * current method is cleared after that method has completed execution.
 * 
 * It is important to note that the abort methods are only executed when the
 * method is aborted, and not when you exit the method under normal operating
 * conditions. Methods can be aborted due to an abort mask condition when
 * sending a HART command, or when the abort function is called.
 * 
 * \return BI_SUCCESS if the method was successfully added to the list,
 * and BI_ERROR if the list was full.
 * 
 * \note push_abort_method is an EDD 1.2 enhancement.
 */
edd_int32 push_abort_method(
		class method_env *env, const lib::pos &caller_pos,
		class tree * abort_method_name);
#ifndef DOXYGEN_IGNORE
void check_push_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin remove_abort_method.
 * The Builtin remove_abort_method will remove a method from the abort method
 * list, which is the list of methods to be executed if the current method is
 * aborted. This Builtin will remove the first occurrence of the specified
 * method in the list, starting with the first method added. If there are
 * multiple occurrences of a specific method, only the first one is removed.
 * Abort methods may not be removed during an abort method.
 */
edd_int32 remove_abort_method(
		class method_env *env, const lib::pos &caller_pos,
		class tree * abort_method_name);
#ifndef DOXYGEN_IGNORE
void check_remove_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin remove_abort_method_1.
 * The Builtin remove_abort_method moves a method from the abort method list.
 * The abort method list contains the methods that are to be executed if the
 * primary method aborts.
 * 
 * This Builtin removes the first occurrence of the method that it finds in
 * the list, starting at the front of the list and moving to the back. The
 * front of the list has the abort methods that were added first (FIFO). If
 * the method occurs more than once in the list, the subsequent occurrences
 * are not removed.
 * 
 * The method_id parameter is specified by the method writer using the
 * Builtin ITEM_ID and the name of the method. Abort methods cannot call
 * remove_abort_method.
 */
edd_int32 remove_abort_method_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_uint32 method_id);

/**
 * The builtin remove_all_abort_methods.
 * The Builtin remove_all_abort_methods removes all methods from the abort
 * method list. This Builtin is similar to the Builtin remove_abort_method or
 * remove_abort_method except it removes all of the abort methods. An abort
 * method cannot call remove_all_abort_methods.
 */
void remove_all_abort_methods(
		class method_env *env, const lib::pos &caller_pos);

edd_real32 round(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 round_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin save_on_exit.
 * The Builtin save_on_exit sets the termination action to save any changes
 * made by the method to variables in the variable table not already sent to
 * the device. The values in the device are not affected. As long as the
 * application has not changed the values, the values are saved as if written
 * by an application.
 * 
 * When a method modifies a variable's value, it is really just changing a
 * resident copy of that value in the EDD application. This change is
 * effective only for the life of the method. To make the values permanent in
 * the variable table, the method shall save the values before exiting.
 * 
 * All values that have been changed, but not sent to the device, are handled
 * at the termination of the method by either sending them to the device,
 * discarding them, or saving the value changes in a way that survives the end
 * of the method (but doesn't affect the device). Saved values can be used by
 * other methods.
 * 
 * If a method does not call Builtin discard_on_exit, save_on_exit, or
 * send_on_exit prior to exiting, any values in the Builtin are discarded as
 * if discard_on_exit was called.
 * 
 * If the method aborts, then the Builtin discard_on_exit option is enforced
 * even if Builtin save_on_exit were called before the method aborted.
 */
void save_on_exit(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin save_values.
 * The Builtin save_values will cause the values of all device variables
 * modified during a method session to remain permanent after the method
 * has been exited.
 * 
 * Device variables that are modified, but are not sent to the connected
 * device, will be restored to their previous value if this function is not
 * called.
 */
edd_int32 save_values(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin seconds_to_TIME_VALUE.
 * The Builtin seconds_to_TIME_VALUE converts a seconds value to a TIME_VALUE.
 *
 * \param seconds The number of seconds.
 *
 * \return The TIME_VALUE
 */
edd_uint32 seconds_to_TIME_VALUE(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 seconds);

/**
 * The builtin send.
 * send will send the specified HART command and return the response code,
 * communications status and command status, respectively, in the cmd_status
 * array. If the device status bit indicating that more status information is
 * available is returned (bit 4), command 48 is automatically issued.
 * 
 * The bytes in the cmd_status array and the status bytes from command 48 (if
 * it was sent) are logically ANDed with the appropriate abort masks to
 * determine if the method should be aborted. If there is a match, the method
 * is aborted and the abort methods (if any were designated) are run. If
 * there is no match for the abort masks, the same data is ANDed with the
 * appropriate retry masks. If there is a match, the appropriate command will
 * be retried (either the specified command, or command 48). This continues
 * until either the command succeeds, an abort occurs, or the maximum number
 * of retries is reached.
 * 
 * The status bytes for the command are returned in the cmd_status parameter,
 * an array of size STATUS_SIZE (= 3 bytes), allocated in the method making
 * the call.
 * 
 * The following is an example of this function:
 * \code
 * int result;
 * char cmd_status[STATUS_SIZE];
 * result = send(40, cmd_status);
 * \endcode
 * 
 * In the above example, if cmd_status[STATUS_DEVICE_STATUS] & 0x10 is set,
 * command 48 will automatically be dispatched before the send function
 * returns.
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found.
 */
edd_int32 send(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_string_ref &cmd_status);
#ifndef DOXYGEN_IGNORE
void check_send_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin send_all_values.
 * The Builtin send_all_values sends all of the values in the variable table
 * that the method modified but has not previously sent to the device. This
 * action changes the device values to match the values set by the method.
 * 
 * This Builtin does nothing for a non-caching EDD application because it
 * does not have a variable table. Updates the device with all off the values
 * in the variable table that the method has changed.
 */
edd_int32 send_all_values(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin send_command.
 * send_command will send the specified HART command. The status/data bytes
 * from the command are then logically ANDed with the appropriate abort masks
 * to determine if the method should be aborted. If there is a match, the
 * method is aborted and the abort methods (if any were designated) are run.
 * If there is no match for the abort masks, the same data is ANDed with the
 * appropriate retry masks. If there is a match, the command will be retried.
 * This continues until either the command succeeds, an abort occurs, or the
 * maximum number of retries is reached. No status or data bytes are returned
 * with this function.
 * 
 * \return BI_SUCESS if the function was successful, BI_COMM_ERROR if an error
 * occurred sending the HART command, or BI_NO_DEVICE if no device was found.
 */
edd_int32 send_command(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number);
#ifndef DOXYGEN_IGNORE
void check_send_command_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin send_command_trans.
 * send_command_trans sends the HART command with the specified transaction
 * to the device. This function is to be used to send commands that have been
 * defined with multiple transactions.
 * 
 * send_command_trans will send the specified HART command. The status/data
 * bytes from the command are then logically ANDed with the appropriate abort
 * masks to determine if the method should be aborted. If there is a match,
 * the method is aborted and the abort methods (if any were designated) are
 * run. If there is no match for the abort masks, the same data is ANDed with
 * the appropriate retry masks. If there is a match, the command will be
 * retried. This continues until either the command succeeds, an abort occurs,
 * or the maximum number of retries is reached. No status or data bytes are
 * returned with this command.
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found.
 */
edd_int32 send_command_trans(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_int32 transaction);
#ifndef DOXYGEN_IGNORE
void check_send_command_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

/**
 * The builtin send_on_exit.
 * The Builtin send_on_exit sets the termination action to send any changes
 * made by the method to variables stored in the variable table of the EDD
 * application, which have not already been sent to the device. This Builtin
 * does nothing in a non-caching EDD application, because a non-caching EDD
 * application does not have a variable table.
 * 
 * When a method modifies a variable value, it changes a copy of that value.
 * This change is effective only for the life of the method. In order to make
 * the change in the device, the method shall send the value(s) to the device
 * or call this Builtin before the method exits.
 * 
 * All values that are changed, but not sent to the device, are handled at the
 * termination of the method by either sending them to the device, discarding
 * them, or saving the value changes in a way that survives the end of the
 * method. The changed values in the variable table are sent after the method
 * terminates.
 * 
 * If a method does not call Builtin save_on_exit, or Builtin send_on_exit
 * prior to the exiting, any values changed by the Builtin are discarded as
 * if Builtin discard_on_exit was called. If the method aborts then the
 * Builtin discard_on_exit option is enforced, even if Builtin save_on_exit,
 * or Builtin send_on_exit were called before the method aborted.
 */
void send_on_exit(
		class method_env *env, const lib::pos &caller_pos);

/**
 * The builtin send_trans.
 * send_trans sends a HART command with the specified transaction to the
 * device. This function is to be used to send commands that have been defined
 * with multiple transactions.
 * 
 * send_trans will send the specified HART command and return the response
 * code, communications status, and command status respectively in the
 * cmd_status array. If the status bit indicating that more data is available
 * is returned (status class MORE), command 48 is automatically issued. The
 * bytes in the cmd_status array and the status bytes from command 48 (if it
 * was sent) are logically ANDed with the appropriate abort masks to determine
 * if the method should be aborted. If there is a match, the method is aborted
 * and the abort methods (if any were designated) are run. If there is no
 * match for the abort masks, the same data is ANDed with the appropriate
 * retry masks. If there is a match, the appropriate command will be retried
 * (either the specified command, or command 48). This continues until either
 * the command succeeds, an abort occurs, or the maximum number of retries is
 * reached.
 * 
 * The status bytes for the command are returned in the cmd_status parameter,
 * an array of size STATUS_SIZE (= 3 bytes), allocated in the method making
 * the call.
 * 
 * \return BI_SUCESS if the command was successful, BI_COMM_ERROR if an error
 * occurred sending the command, or BI_NO_DEVICE if no device was found
 */
edd_int32 send_trans(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 cmd_number,
		edd_int32 transaction,
		edd_string_ref &cmd_status);
#ifndef DOXYGEN_IGNORE
void check_send_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 sgetval(
		class method_env *env, const lib::pos &caller_pos,
		edd_string_ref &str);
#ifndef DOXYGEN_IGNORE
void check_sgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

void sie_set(
		class method_env *env, const lib::pos &caller_pos);

edd_real32 sin(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 sin_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 sinh(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 sinh_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 sqrt(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 sqrt_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_string ssetval(
		class method_env *env, const lib::pos &caller_pos,
		edd_string value);
#ifndef DOXYGEN_IGNORE
void check_ssetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */

edd_int32 strcmp(
		class method_env *env, const lib::pos &caller_pos,
		edd_string s1,
		edd_string s2);

edd_string strleft(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str,
		edd_int32 length);

edd_int32 strlen(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str);

edd_string strlwr(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str);

edd_string strmid(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str,
		edd_int32 pos,
		edd_int32 len);

edd_string strright(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str,
		edd_int32 length);

edd_string strstr(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str,
		edd_string substr);

edd_string strtrim(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str);

edd_string strupr(
		class method_env *env, const lib::pos &caller_pos,
		edd_string str);

edd_real32 tan(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 tan_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

edd_real32 tanh(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 tanh_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin timet_to_TIME_VALUE.
 * The Builtin timet_to_TIME_VALUE converts the time of day part of a time_t to a TIME_VALUE(4).
 */
edd_uint32 timet_to_TIME_VALUE(
		class method_env *env, const lib::pos &caller_pos,
		edd_int32 timet);

/**
 * The builtin timet_to_string.
 * The Builtin timet_to_string creates a string representation of a time_t.
 * The ANSI C strftime function defines the structure of the format string.
 *
 * \param str The string into which the formatted time_t is copied.
 * \param format How the time_t is to be formatted.
 * \param timet The time_t to be formatted.
 *
 * \return The number of characters actually copied or -1 if an error occurs.
 */
edd_int32 timet_to_string(
		class method_env *env, const lib::pos &caller_pos,
		edd_string_ref &str,
		edd_string format,
		edd_int32 timet);

edd_real32 trunc(
		class method_env *env, const lib::pos &caller_pos,
		edd_real32 x);

edd_real64 trunc_1(
		class method_env *env, const lib::pos &caller_pos,
		edd_real64 x);

/**
 * The builtin vassign.
 * The Builtin vassign will assign the value of the source variable to the
 * destination variable. Both variables must be valid, and they must have the
 * same type.
 */
edd_int32 vassign(
		class method_env *env, const lib::pos &caller_pos,
		class tree * device_var,
		class tree * source_var);
#ifndef DOXYGEN_IGNORE
void check_vassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
#endif /* DOXYGEN_IGNORE */


} /* namespace builtins */

#endif /* _edd_builtins_impl_h */
