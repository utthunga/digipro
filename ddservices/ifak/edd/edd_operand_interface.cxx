
/* 
 * $Id: edd_operand_interface.cxx,v 1.47 2009/05/07 20:48:30 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_operand_interface.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// my lib

// own
#include "edd_err.h"

#ifndef N_
# define N_(x) x
#endif


namespace edd
{

operand::~operand(void) { }

void operand::setenv(class method_env *e) throw() { env = e; setenv0(env); }
void operand::setenv0(class method_env *env) throw() { }

void operand::env_assert(class method_env *env) const { }

void operand::dump(lib::stream &f) const { f.printf("[env 0x%lx] ", (unsigned long)env); dump0(f); }

std::string operand::print(const char *fmt) const { throw RuntimeError(xxNoValue); }
std::string operand::pretty_print(void) const { return print(NULL); }
void operand::scan(const std::string &, bool) { throw RuntimeError(xxNonLvalue); }
void operand::pretty_scan(const std::string &str) { scan(str); }

            operand::operator stackmachine::Type *() const { throw RuntimeError(xxNoValue); }
void        operand::operator= (const stackmachine::Type &) { throw RuntimeError(xxNonLvalue); }

void
operand::status::dump(lib::stream &f) const throw()
{
	f.printf(" status [%i]", mystatus);
}

const char *
operand::value_status_descr(value_status_t status)
{
	const char *ret = NULL;

	switch (status)
	{
		case VALUE_STATUS_NONE:
			ret = "";
			break;
		case VALUE_STATUS_INVALID:
			ret = N_("invalid");
			break;
		case VALUE_STATUS_NOT_ACCEPTED:
			ret = N_("not accepted");
			break;
		case VALUE_STATUS_NOT_SUPPORTED:
			ret = N_("not supported");
			break;
		case VALUE_STATUS_CHANGED:
			ret = N_("changed");
			break;
		case VALUE_STATUS_LOADED:
			ret = N_("loaded");
			break;
		case VALUE_STATUS_INITIAL:
			ret = N_("initial");
			break;
		case VALUE_STATUS_NOT_CONVERTED:
			ret = N_("not converted");
			break;
	}

	assert(ret);
	return ret;
}

operand::value_status_t& operand::status(void) { throw RuntimeError(xxNonLvalue); }
const operand::value_status_t& operand::status(void) const { throw RuntimeError(xxNonLvalue); }
void operand::eval_values(std::vector<operand_ptr> &ops) { ops.push_back(this); }

#define CAST_OPERATOR(T) \
	operand::operator T() const { throw RuntimeError(xxTypeMismatch); }

CAST_OPERATOR(bool)
CAST_OPERATOR(int8)
CAST_OPERATOR(int16)
CAST_OPERATOR(int32)
CAST_OPERATOR(int64)
CAST_OPERATOR(uint8)
CAST_OPERATOR(uint16)
CAST_OPERATOR(uint32)
CAST_OPERATOR(uint64)
CAST_OPERATOR(float32)
CAST_OPERATOR(float64)
CAST_OPERATOR(std::string)

CAST_OPERATOR(lib::IDENTIFIER)
CAST_OPERATOR(node_tree *)
CAST_OPERATOR(EDD_OBJECT_tree *)

operand_ptr operand::operator++  (int)  { throw RuntimeError(xxNonLvalue); } // post
operand_ptr operand::operator--  (int)  { throw RuntimeError(xxNonLvalue); } // post

operand_ptr operand::operator++  (void) { throw RuntimeError(xxNonLvalue); } // pre
operand_ptr operand::operator--  (void) { throw RuntimeError(xxNonLvalue); } // pre
operand_ptr operand::operator+   (void) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator-   (void) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator~   (void) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator!   (void) const { throw RuntimeError(xxNoValue); }

operand_ptr operand::operator*   (const operand &) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator/   (const operand &) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator+   (const operand &) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator-   (const operand &) const { throw RuntimeError(xxNoValue); }
operand_ptr operand::operator%   (const operand &) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator^   (const operand &) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator&   (const operand &) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator|   (const operand &) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator<<  (const operand &) const { throw RuntimeError(xxTypeMismatch); }
operand_ptr operand::operator>>  (const operand &) const { throw RuntimeError(xxTypeMismatch); }
bool        operand::operator<   (const operand &) const { throw RuntimeError(xxNoValue); }
bool        operand::operator>   (const operand &) const { throw RuntimeError(xxNoValue); }
bool        operand::operator==  (const operand &) const { throw RuntimeError(xxNoValue); }
bool        operand::operator!=  (const operand &) const { throw RuntimeError(xxNoValue); }
bool        operand::operator<=  (const operand &) const { throw RuntimeError(xxNoValue); }
bool        operand::operator>=  (const operand &) const { throw RuntimeError(xxNoValue); }

void        operand::operator=   (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator+=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator-=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator*=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator/=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator%=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator^=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator&=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator|=  (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator>>= (const operand &) { throw RuntimeError(xxNonLvalue); }
void        operand::operator<<= (const operand &) { throw RuntimeError(xxNonLvalue); }

operand_ptr operand::operator[]  (long) const { throw RuntimeError(xxNonLvalue); }
operand_ptr operand::operator()  (operand_ptr) const { throw RuntimeError(xxNonLvalue); }
operand_ptr operand::member      (const lib::IDENTIFIER &i) const { throw RuntimeError(xxNonLvalue); }

bool operand::operator&& (const operand &op) const { return (((bool)(*this)) && ((bool) op)); }
bool operand::operator|| (const operand &op) const { return (((bool)(*this)) || ((bool) op)); }

} /* namespace edd */
