/* 
 * $Id: edd_tree.cxx,v 1.296 2009/11/25 09:43:36 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_tree.h"

// C stdlib
#include <assert.h>
#include <limits.h>
#include <locale.h>
#include <time.h>

// C++ stdlib
#include <list>
#include <memory>
#include <dirent.h>

// my lib
#include "IniFile.h"
#include "fstream.h"
#include "mstream.h"
#include "paths.h"
#include "port.h"
#include "string.h"
#include "dirent.h"
#include "statxx.h"
#include "dirent_edd.h"

// own header
#include "DevicesParser.h"
#include "dct_pars.h"
#include "device_io_null.h"
#include "edd_ast.h"
#include "edd_builtins.h"
#include "edd_cpp.h"
#include "edd_dct.h"
#include "edd_pars.h"
#include "edd_values.h"
#include "version.h"

#ifndef N_
# define N_(x) x
#endif

namespace edd {

const char *edd_tree::version = VERSION;
const char *edd_tree::date = __DATE__;

/**
 * Initialize the new EDD manager.
 * Allocate and assign all neccessary helpers like the dictionary object, the
 * identifier manager, message manager, hashtable, maptable and so on.
 * \exception std::bad_alloc() in case of not enough memory.
 */
edd_tree::edd_tree(void) :
		bi_rc(new int32_lvalue(0)), ident_(new lib::IDENT), dictionary_(
				new edd_dictionary), device_io_(new device_io_null), err_(
				new edd_err(ident_.get())), hash_(
				new lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *>(
						12)), treemap_(
				new lib::maptable<class EDD_OBJECT_tree *>), builtins_(
				new edd_builtins), devices_(new DevicesParser), root(NULL), xxLang(
				xxLang_en), myprofile(PROFIBUS), myoptions(0), object_limit(0), deny_value_load(
				false), use_rel_cmds_(false), yyTabSpace(8), collect_import_infos_(
				false), valid(false) {
    setlocale(LC_NUMERIC, "C");
	logger.create_category("ACTIONS", "Messages about EDD object ACTIONS");
	logger.create_category("BUILTINS", "Messages about execution of builtins");
	logger.create_category("METHODS", "Messages about execution of METHODs");
	logger.create_category("I/O", "Messages about the communication");
	logger.create_category("VARIABLES", "Messages about VARIABLES");
}

/**
 * Cleanup the EDD manager.
 * Release and freeup all memory hold by the various helper objects.
 */
edd_tree::~edd_tree(void) {
/** CPMHACK: (TODO)Bharath: Commented beloew 3 lines of code to avaoid
 *  segmentation fault in clearing the memory, Once menu navigation is
 *  implemented then this should be handled for clearing the memory
 */

//	clear();
//	err().clear();
//	dictionary().clear();

// XXX there is a memory leak in the parser
// this have the effect that ident_ is not empty and thus
// clear() fails
//	ident_->clear();
}

/**
 * Clear all associated data of a loaded DD.
 * This remove any data associated with the currently
 * loaded DD. This include the import cache, the included files history, the
 * cmd to number mapper, the hash table, the id mapping and finally the AST
 * itself.
 * \note This method don't clear the dictionary, the error messages and
 * the loaded devices.cfg data!
 * \note This method is implicitly called on parse.
 */
void edd_tree::clear(void) {
	import_cache.clear();
	included_files.clear();
	cmd_by_number.clear();

	if (root) {
		delete root;
		root = NULL;
	}
	assert(hash_->empty());

	treemap_->clear();
}

void edd_tree::on_value_changed(class EDD_OBJECT_tree *, class method_env *) {
}

void edd_tree::on_method_start(class EDD_OBJECT_tree *, class method_env *) {
}

void edd_tree::on_method_stop(class EDD_OBJECT_tree *, class method_env *) {
}

void edd_tree::on_method_debug(class EDD_OBJECT_tree *, class method_env *) {
}

void edd_tree::on_method_notify(class EDD_OBJECT_tree *, class method_env *) {
}

/*
 * Documented in header.
 */
void edd_tree::err_threshold(int threshold) {
	--threshold;

	if (threshold < 10)
		threshold = 10;
	else if (threshold > 9999)
		threshold = 9999;

	err().config_errthreshold(threshold);
}

/*
 * Documented in header.
 */
void edd_tree::err_config(bool nowarn) {
	err().config_warn(!nowarn);
}

/*
 * Documented in header.
 */
const char *
edd_tree::profile_descr(profile_t profile) throw () {
	const char *ret = NULL;

	switch (profile) {
	case HART:
		ret = "HART";
		break;
	case FIELDBUS:
		ret = "FIELDBUS";
		break;
	case PROFIBUS:
		ret = "PROFIBUS";
		break;
	case PROFINET:
		ret = "PROFINET";
		break;
	default:
		/* this can't happen */
		assert(0);
	}

	assert(ret);
	return ret;
}

/*
 * Documented in header.
 */
const char *
edd_tree::option_descr(size_t option) throw () {
	const char *ret = NULL;

	switch (option) {
	case PDM_MODE:
		ret = N_("PDM compatible");
		break;
	case PDM_SCALING_FACTOR:
		ret = N_("PDM compatible SCALING_FACTOR");
		break;
	case PEDANTIC:
		ret = N_("Pedantic mode");
		break;
	case NO_EDDx1:
		ret = N_("Disable EDD enhancements");
		break;
	default:
		/* this can't happen */
		assert(0);
	}

	assert(ret);
	return ret;
}

/*
 * Documented in header.
 */
std::vector<edd_tree::profile_t> edd_tree::allprofiles(void) {
	std::vector<edd_tree::profile_t> ret;

	ret.reserve(3);
	ret.push_back(PROFIBUS);
	ret.push_back(PROFINET);
	ret.push_back(HART);
	ret.push_back(FIELDBUS);

	return ret;
}

/*
 * Documented in header.
 */
std::vector<size_t> edd_tree::alloptions(void) {
	std::vector<size_t> ret;

	ret.reserve(4);
	ret.push_back(PDM_MODE);
	ret.push_back(PDM_SCALING_FACTOR);
	ret.push_back(PEDANTIC);
	ret.push_back(NO_EDDx1);

	return ret;
}

void edd_tree::add_role(const std::string &role) {
	myroles.insert(role);
}

void edd_tree::remove_role(const std::string &role) {
	myroles.erase(role);
}

/*
 * Documented in header.
 */
void edd_tree::assign_device_io(
		std::auto_ptr<class device_io_interface> n_io) throw () {
	if (!n_io.get())
		n_io.reset(new device_io_null);

	assert(n_io.get());
	device_io_ = n_io;
}

/*
 * Documented in header.
 */
void edd_tree::set_demo_limit(size_t objects, bool no_value_load) {
	object_limit = objects;
	deny_value_load = no_value_load;
}

/**
 * \internal
 * Internal parse routine.
 * This method do all basic things of parsing an EDD specification. It's used
 * internally by several other methods.
 * \par
 * It run the preprocessor, invoke the EDD parser, run the pass0
 * typecheck, build the cross reference hashtable and finally handle the
 * import and like directives.
 * \param root The root pointer of the parsed specification is stored here.
 * \param hash The hastable that should be filled in.
 * \param mode The parser mode that should be use (short parse, normal parse).
 * \param file The filename of the specification.
 * \param includes The include vector for C preprocessing and handling
 * imports.
 * \param defines The vector of default defines for C preprocessing.
 * \return The number of errors or 0 if succeeded.
 */
int edd_tree::parse0(class ROOT_tree **root,
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
		int mode, const std::string &file0,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines) {
	const char *file = file0.c_str();
	class edd_pars *parser;
	FILE *src = NULL;
	int ret;

	parser = new class edd_pars(this);
	assert(parser);

	parser->scanner = new class edd_scan(this);
	assert(parser->scanner);

	/* set tabulator space for column calucation */
	parser->scanner->yyTabSpace = yyTabSpace;

	//std::cout << "YOGA: edd::parse->edd_tree::parse0:" << "__File:" << __FILE__	<< "__Line No:" << __LINE__ << std::endl;
	try {
		std::auto_ptr<lib::stream> stream;
		FILE *tmp = NULL;

		if (file) {
			src = fopen(file, "r");
			if (!src)
				throw RuntimeError(xxNoSuchFile, lib::NoPosition,
						ident_->create(file));

			edd_cpp cpp(ident_.get(), err_.get(), relaxed_mode());
			tmp = cpp.run(src, file, includes, defines, included_files);

			// XXX closed by cpp.run :-( fclose(src);
			src = NULL;

			if (!tmp)
				throw RuntimeError(xxCannotContinue, lib::NoPosition,
						ident_->NoIdent);
		}

		if (tmp)
			stream.reset(new lib::fstream(tmp));
		else
			stream.reset(new lib::fstream(stdin, lib::fstream::dont_close));

		assert(stream.get());
		parser->scanner->BeginFile(stream, file ? file : "<stdin>");

		/* parse in */
		std::cout << " AST is getting created here" << std::endl;
		ret = parser->Parse(root, mode);
		if (ret) {
			assert(err().get_errCount() != 0);

			if (*root) {
				delete *root;
				*root = NULL;
			}
		} else
			assert(*root);

		/* always run the typecheck */
		pass0();

		ret = err().get_errCount();
		if (ret == 0) {
			assert(*root);

			/* convert IMPORTED nodes */
			run_import(*root, hash, includes, defines);

			/* construct hashtable */
			build_hashtable(hash, *root);

			ret = err().get_errCount();
			if (ret == 0) {
				/* convert LIKE nodes */
				run_like(*root, hash);

				ret = err().get_errCount();
				if (ret == 0) {
					/* rerun typecheck to detect
					 * wrong imports/redefinitions
					 */
					pass0();

					ret = err().get_errCount();
				}
			}
		}
	} catch (const RuntimeError &xx) {
		err().error(xx);
		ret = err().get_errCount();
	} catch (const std::exception &e) {
		err().error(xxUnknown, lib::NoPosition, ident_->create(e.what()));
		ret = err().get_errCount();
	} catch (...) {
		err().error(xxUnknown, lib::NoPosition,
				ident_->create(__FILE__ ":" STR(__LINE__)));
		ret = err().get_errCount();
	}

	/* release scanner and parser */
	delete parser->scanner;
	delete parser;

	/* close the src file stream if still open */
	if (src)
		fclose(src);

	return ret;
}

/**
 * \internal
 * Parse EDD identification only.
 * Invoke all steps to only parse the identification of an EDD
 * specification.
 * \param file The file name of the specification.
 * \param includes The include vector for preprocessing.
 * \param defines The default defines for preprocessing.
 * \param t The parsed identification is stored here. Ownership of the
 * identification subtree move to the parent.
 * \return The number of encountered errors during parsing.
 * \note If errors are encountered \b NO error messages are created and
 * enqueeed into the message queue.
 * \note The C proprocessor must run completly :-/.
 */
int edd_tree::parse_ident(const std::string &file,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines,
		class IDENTIFICATION_tree **t) {
	assert(t);
	*t = NULL;

	std::auto_ptr<class edd_err> save_err(err_);
	err_.reset(new edd_err(ident_.get()));
	assert(err_.get());

	class ROOT_tree *root = NULL;
	int ret;

	ret = parse0(&root, NULL, 1, file, includes, defines);
	if (ret == 0) {
		assert(root);

		if (root->get_identification())
			*t = root->clone_identification();
		else
			ret = 1;

		delete root;
	}

	//lib::fstream out(stdout, lib::fstream::dont_close);
	//err().fflush(&out, xxLang);
	err_ = save_err;

	assert(ret ? (*t == NULL) : (*t != NULL));
	return ret;
}

/**
 * \internal
 * Helper that start parsing of imported EDD specifications.
 * \param file The EDD file.
 * \param includes The include vector for the preprocessor.
 * \param defines The default defines for preprocessing.
 * \param root There to store the AST root on success.
 * \param hash There to store the hash table for this AST.
 * \return The number of errors encountered (0 for no error).
 * root and hash are only valid if there are no errors, otherwise
 * NULL pointer are stored there.
 */
int edd_tree::parse_import(const std::string &file,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, class ROOT_tree **root,
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> **hash) {
	assert(root && hash);

	*root = NULL;
	*hash = new lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *>(12);
	assert(*hash);

	int ret;

	/* run the ucpp and the parser */
	ret = parse0(root, *hash, 2, file, includes, defines);
	if (ret) {
		if (*root)
			delete *root;
		delete *hash;

		*root = NULL;
		*hash = NULL;
	}

	return ret;
}

/**
 * \internal
 * Helper to check if a file is an EDD specification.
 * Is used during directory scanning during handling the IMPORT directives to
 * determine if a file is an EDD specification or not.
 */
bool edd_tree::is_edd_file(const std::string &name) {
	return lib::check_extension(name, "ddl")
			|| lib::check_extension(name, "edd");
}

/**
 * \internal
 * Report 'while searching' message information.
 * Create and enqueue a 'while searching' information into the message queue.
 * Also check and set a flag to only report the message once.
 * \param import The corresponding identification tree.
 * \param informed The lock flag.
 */
void edd_tree::information_import(class IDENTIFICATION_tree *import,
		bool &informed) {
	if (!informed) {
		char buf[128];

		snprintf(buf, sizeof(buf), "IMPORT %i,%i,%i,%i", import->manufacturer(),
				import->device_type(), import->device_revision(),
				import->dd_revision());

		err().information(xxWhileSearching, import->pos, ident_->create(buf));

		informed = true;
	}
}

bool edd_tree::search_import_hart(class IDENTIFICATION_tree *import,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, const std::string &hcf_path,
		std::string &result) {
	const char *sep = lib::pathseparator();
	lib::dyn_buf buf(1024);
	bool ret = false;

	buf.printf("%s%s%02x%s%02x%s%02x%02x", hcf_path.c_str(), sep,
			import->manufacturer(), sep, import->device_type(), sep,
			import->device_revision(), import->dd_revision());

	sys::DIR *dirp;

	dirp = sys::opendir(buf.str());
	if (dirp) {
		std::string entry;

		while (!(entry = sys::readdir(dirp)).empty()) {
			if (is_edd_file(entry)) {
				std::string fullname;

				fullname += buf.str();
				fullname += sep;
				fullname += entry;

				class IDENTIFICATION_tree *t;
				int i;

				i = parse_ident(fullname, includes, defines, &t);

				if (i == 0) {
					if (import->equal(t)) {
						result = fullname;
						ret = true;
					}

					delete t;

					if (ret)
						break;
				}
			}
		}

		closedir(dirp);
	} else {
		err().warning(xxOpenDirectory, import->pos, ident_->create(buf));

		bool informed = false;
		information_import(import, informed);
	}

	return ret;
}

bool edd_tree::search_import_pb(class IDENTIFICATION_tree *import,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, const std::string &pb_path,
		std::string &result) {
	const char *sep = lib::pathseparator();
	lib::dyn_buf buf(1024);
	bool ret = false;

	buf.printf("%s%s%04x%s%04x%s%04x%s%04x_%04x_%04x_%04x.ddl", pb_path.c_str(),
			sep, import->manufacturer(), sep, import->device_type(), sep,
			import->device_revision(), sep, import->manufacturer(),
			import->device_type(), import->device_revision(),
			import->dd_revision());

	if (lib::file_exist(buf.c_str())) {
		class IDENTIFICATION_tree *t;
		int i;

		i = parse_ident(buf.c_str(), includes, defines, &t);

		if (i == 0) {
			if (import->equal(t)) {
				result = buf.str();
				ret = true;
			}

			delete t;
		}
	}

	return ret;
}

/**
 * \internal
 * Helper method for search_import.
 * This method iterate over the specified directory looking for
 * EDD specifications. If they can be parsed the IDENTIFICATION
 * information is obtained and stored.
 * \param import The searched IDENTIFICATION, only for error reporting.
 * \param includes The vector of include directories for the preprocessor.
 * \param defines The default defines for preprocessing.
 * \param path The directory that should be searched.
 * \param idents Vector for storing found identifications.
 * \param files Vector for storing the corresponding files.
 * \param informed Flag if information message is already reported.
 */
void edd_tree::search_import_self(class IDENTIFICATION_tree *import,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, const std::string &path,
		bool &informed) {
	sys::DIR *dirp;

	dirp = sys::opendir(path);
	if (dirp) {
		import_cache_dir_t &import_cache_dir = import_cache[path];
		std::string entry;

		import_cache_dir.clear();

		while (!(entry = sys::readdir(dirp)).empty()) {
			/* check if it's an edd/ddl filename */
			if (!is_edd_file(entry))
				continue;

			std::string fullname(
					lib::clean_path(path + lib::pathseparator() + entry));
			sys::statxx st;
			int ret;

			ret = sys::stat(fullname, st);
			if (ret == 0) {
				if (st.isreg()) {
					class IDENTIFICATION_tree *t;

					ret = parse_ident(fullname, includes, defines, &t);
					if (ret == 0) {
						struct import_cache_entry entry = { fullname,
								lib::smart_ptr<class IDENTIFICATION_tree>(t) };
						import_cache_dir.push_back(entry);
					} else {
						information_import(import, informed);

						err().warning(xxReadIdentification, import->pos,
								ident_->create(fullname), true);
					}
				}
			} else {
				information_import(import, informed);

				err().warning(xxStatFile, import->pos, ident_->create(fullname),
						true);
			}
		}

		closedir(dirp);
	} else {
		information_import(import, informed);

		err().warning(xxOpenDirectory, import->pos, ident_->create(path), true);
	}
}

/**
 * Search EDD file matching given IDENTIFICATION.
 * This method iterate over the given vector of include paths and
 * look for valid EDD specifications that match the given IDENTIFICATION.
 * If there are several matches the best one is returned.
 * \param import The searched IDENTIFICATION.
 * \param includes The vector of directories to lookup.
 * \param defines The default defines for preprocessing.
 * \param result If a match is found the filename is stored here.
 * \return true if a EDD specification is found, false otherwise.
 */
bool edd_tree::search_import(class IDENTIFICATION_tree *import,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, std::string &result) {
	if (get_profile() == HART && !hcf_path.empty())
		return search_import_hart(import, includes, defines, hcf_path, result);

	if (get_profile() == PROFIBUS && !pb_path.empty())
		return search_import_pb(import, includes, defines, pb_path, result);

	if (get_profile() == PROFINET && !pb_path.empty())
		return search_import_pb(import, includes, defines, pb_path, result);

	bool informed = false;

	for (size_t i = 0; i < includes.size(); ++i) {
		import_cache_t::const_iterator iter;

		iter = import_cache.find(includes[i]);
		if (iter == import_cache.end()) {
			/* not yet looked up */

			search_import_self(import, includes, defines, includes[i],
					informed);

			iter = import_cache.find(includes[i]);
			if (iter == import_cache.end())
				continue;
		}

		const import_cache_dir_t &import_cache_dir = iter->second;

		for (size_t j = 0; j < import_cache_dir.size(); ++j) {
			if (import->equal(import_cache_dir[j].id.get())) {
				result = import_cache_dir[j].fullname;
				return true;
			}
		}
	}

	return false;
}

/**
 * \internal
 */
bool edd_tree::search_file(const std::string &file,
		const std::vector<std::string> &includes, std::string &result) {
	for (size_t i = 0, i_max = includes.size(); i < i_max; ++i) {
		result = lib::clean_path(includes[i] + lib::pathseparator() + file);

		if (lib::file_exist(result))
			return true;
	}

	result.clear();
	return false;
}

/**
 * The main compiler entry.
 * This is the entry point to create an abstract syntax tree from an EDD
 * specification. It do all neccessary steps by running the C preprocessor,
 * starting the syntax parser and invoking the compiler check passes that do
 * the semantic checking and verification, compiling the C methods,
 * initializing the internal data and value classes and so on.
 * \param file The relativ or absolute pathname to the EDD file that has
 * to be parsed and checked.
 * \param includes A vector of include paths for the C preprocessor. An empty
 * vector indicate no include paths. Default include path is the current
 * working directory.
 * \param defines The default defines for preprocessing.
 * \param profile The profile type of the EDDL that should be parsed. This
 * override the default setting PROFIBUS.
 * \param options The profile options for the syntax and semantic checks.
 * \return The number of encountered errors. The errors are recorded by the
 * error manager class. A value of 0 means no error (but non-error messages
 * can be still in the message queue). The abstract syntax tree representation
 * only exist and is valid if no error is encountered.
 */
int edd_tree::parse(const std::string &file,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines, profile_t profile,
		size_t options) {
	std::string filename(lib::clean_path(file));
	int ret;

	myprofile = profile; // My profile is profibus
	myoptions = options; //My options for syntax is and semantic checks

	//std::cout << "YOGA: edd_tree::parse function" << "__File:" << __FILE__	<< "__Line No:" << __LINE__ << std::endl;
	clear();

	try {
		/* run the ucpp and the parser */
		ret = parse0(&root, hash_.get(), 0, filename, includes, defines);

		/* run pass1 and pass2; but only if there are no errors
		 * encountered
		 */
		if (ret == 0) {
			int edd_profile = root->edd_profile();

			/* look for a given EDD profile
			 */
			switch (edd_profile) {
			case HART:
			case FIELDBUS:
			case PROFIBUS:
			case PROFINET:
				myprofile = (profile_t) edd_profile;
				break;
			}

			if (object_limit
					&& tree_count(root->definition_list()) > object_limit) {
				lib::scratch_buf buf(16);
				buf.printf("%lu", (unsigned long) object_limit);

				/* clear pending warnings */
				err().clear();

				err().error(xxDemoversionObjects, lib::NoPosition,
						ident_->create(buf.c_str()));

				ret = err().get_errCount();
			}

			if (ret == 0) {
				pass1();

				ret = err().get_errCount();
				if (ret == 0) {
					pass2();

					ret = err().get_errCount();
				}
			}
		}
	} catch (const RuntimeError &xx) {
		err().error(xx);
		ret = err().get_errCount();
	} catch (const std::exception &e) {
		err().error(xxUnknown, lib::NoPosition, ident_->create(e.what()));
		ret = err().get_errCount();
	} catch (...) {
		err().error(xxUnknown, lib::NoPosition,
				ident_->create(__FILE__ ":" STR(__LINE__)));
		ret = err().get_errCount();
	}

	valid = ret ? false : true;

	return ret;
}

/**
 * Load a dictionary file.
 * This method invoke the dictionary parser and add the content of the
 * specified file to the runtime dictionary. Existing dictionary entries
 * will be silently replaced by the new ones.
 * \note If the dictionary file contain errors all entries up to the first
 * deteced error are valid and loaded.
 */
int edd_tree::load_dictionary(const std::string &file0, size_t options) {
	std::string file(lib::clean_path(file0.c_str()));

	myoptions = options;

	class dct_pars *parser;
	int ret = 0;

	parser = new class dct_pars(this);
	assert(parser);

	parser->scanner = new class dct_scan(this);
	assert(parser->scanner);

	try {
		FILE *src;

		src = fopen(file.c_str(), "r");
		if (src) {
			std::auto_ptr<lib::stream> stream(new lib::fstream(src));
			assert(stream.get());

			parser->scanner->BeginFile(stream, file);

			/* parse in */
			parser->Parse(0, 0);

			ret = err().get_errCount();
		} else
			throw RuntimeError(xxNoSuchFile, lib::NoPosition,
					ident_->create(file));
	} catch (const RuntimeError &xx) {
		err().error(xx);
		ret = err().get_errCount();
	} catch (const std::exception &e) {
		err().error(xxUnknown, lib::NoPosition, ident_->create(e.what()));
		ret = err().get_errCount();
	} catch (...) {
		err().error(xxUnknown, lib::NoPosition,
				ident_->create(__FILE__ ":" STR(__LINE__)));
		ret = err().get_errCount();
	}

	/* release scanner and parser */
	delete parser->scanner;
	delete parser;

	return ret;
}

int edd_tree::load_devices(void) {
	int ret = 0;

	if (!hcf_path.empty())
		ret += load_devices(hcf_path + lib::pathseparator() + "devices.cfg");

	ret += load_devices("devices.cfg");

	return ret;
}

int edd_tree::load_devices(const std::string &path) {
	int ret = 0;
	std::cout<<"YOGA: edd_tree::load_devices"<<"LINE no"<<__LINE__<<"FILE"<<__FILE__<<std::endl;
	std::cout<<path<<std::endl;
	try {
		lib::stream *f;

		f = lib::fstream_open(path, "r");
		if (f) {
			devices_->load(*f);
			delete f;
		}
	} catch (const std::exception &e) {
		err().error(xxMessage, lib::NoPosition, ident_->create(e.what()));
		++ret;
	}

	return ret;
}

/**
 * Load a value dump (internal helper).
 * This method load a previously save value dump and assign it to the
 * currently running AST. It do all the dirty things that are neccessary.
 * \param stream The stream to read from.
 * \param file The name associated with the stream.
 * \return The number of encountered errors. Pending messages are in the
 * message queue if there are errors.
 */
int edd_tree::load_values0(const lib::IniParser &db) {
	int ret = 0;

	if (db.get_int_key("Version-Info", "version", INT_MAX) > 1) {
		err().error(xxWrongVersion, lib::NoPosition, ident_->NoIdent);
		++ret;
	} else if (root) {
		/* check the identification */
		if (!root->identification()->check_version(db)) {
			err().error(xxWrongVersion, lib::NoPosition, ident_->NoIdent);
			++ret;
		} else {
			/* now iterate over all EDD objects */
			class EDD_OBJECT_tree *object;

			object = root->definition_list();
			while (object) {
				try {
					object->load(db);
				} catch (const RuntimeError &) {
					err().warning(xxReadValue, object->pos, object->ident());
					++ret;
				}

				object = object->next_object();
			}
		}
	}

	return ret;
}

/**
 * Load a value dump.
 * This method load a previously save value dump and assign it to the
 * currently running AST. The EDD identification, version number and all saved
 * type informations are verified before any value is assigned. If there is a
 * mismatch (unknown references, wrong types, wrong sizes, wrong indexes)
 * error messages will be enqueued into the the message queue and the number
 * of errors will be returned.
 * \param buf A buffer of the value dump.
 * \return The number of encountered errors. Pending messages are in the
 * message queue if there are errors.
 */
int edd_tree::load_values(lib::dyn_buf &buf) {
	if (deny_value_load) {
		err().error(xxDemoNotSupported, lib::NoPosition, ident_->NoIdent);
		return 1;
	}

	int ret = 0;

	try {
		class IniParser: public lib::IniParser {
		public:
			IniParser(void) {
			}

			void load(const lib::stream &f) {
				lib::IniParser::load(f, NULL);
			}
		} db;

		lib::mstream f(buf);
		db.load(f);

		ret = load_values0(db);
	} catch (const std::exception &e) {
		err().warning(e);
		++ret;
	}

	return ret;
}

/**
 * Load a value dump.
 * This method load a previously save value dump and assign it to the
 * currently running AST. The EDD identification, version number and all saved
 * type informations are verified before any value is assigned. If there is a
 * mismatch (unknown references, wrong types, wrong sizes, wrong indexes)
 * error messages will be enqueued into the the message queue and the number
 * of errors will be returned.
 * \param path The filepath of the value dump.
 * \return The number of encountered errors. Pending messages are in the
 * message queue if there are errors.
 */
int edd_tree::load_values(const std::string &path) {
	if (deny_value_load) {
		err().error(xxDemoNotSupported, lib::NoPosition, ident_->NoIdent);
		return 1;
	}

	int ret = 0;

	try {
		lib::IniFile db(path);

		ret = load_values0(db);
	} catch (const std::exception &e) {
		err().warning(e);
		++ret;
	}

	return ret;
}

int edd_tree::save_values0(lib::IniParser &db) const {
	int ret = 0;

	db.set_key("Version-Info", "version", 1);

	if (root) {
		class EDD_OBJECT_tree *object;

		/* save the identification */
		root->identification()->save(db);

		/* now iterate over all EDD objects */
		object = root->definition_list();
		while (object) {
			try {
				object->save(db);
			} catch (const RuntimeError &xx) {
				err().error(xx);
				++ret;
			}

			object = object->next_object();
		}
	}

	return ret;
}

/**
 * Save cached values.
 * This method save all VARIABLEs and ARRAYs of the currently running AST to
 * the specified buffer. The value dump is done in a human readable form.
 * \param buf The buffer for the value dump.
 * \return The number of encountered errors. Pending messages are in the
 * message queue if there are errors.
 */
int edd_tree::save_values(lib::dyn_buf &buf) const {
	int ret = 0;

	try {
		class IniParser: public lib::IniParser {
		public:
			IniParser(void) {
			}

			void save(lib::stream &f) {
				lib::IniParser::save(f);
			}
		} db;

		ret = save_values0(db);
		if (ret == 0) {
			/* no errors, save into buf */
			lib::mstream f(buf);
			db.save(f);
		}
	} catch (const std::exception &e) {
		err().error(e);
		++ret;
	}

	return ret;
}

/**
 * Save cached values.
 * This method save all VARIABLEs and ARRAYs of the currently running AST to
 * the specified file. The value dump is done in a human readable form.
 * \param path The filepath for the value dump.
 * \return The number of encountered errors. Pending messages are in the
 * message queue if there are errors.
 * \warning If the file exist it will be deleted.
 */
int edd_tree::save_values(const std::string &path) const {
	int ret = 0;

	try {
		lib::IniFile db;

		ret = save_values0(db);
		if (ret == 0) {
			/* no errors, save file */
			try {
				db.set_path(path);
				db.save();
			} catch (const std::exception &) {
				err().error(xxOpenFileWrite, lib::NoPosition,
						ident_->create(path));
				ret = 1;
			}
		}
	} catch (const std::exception &e) {
		err().error(e);
		++ret;
	}

	return ret;
}

/**
 * \internal
 * Build attribute names.
 * This create a readable string of the attribute names of a node object.
 * The mask argument specifiy which attribute names are wanted.
 * \param node Tree pointer to the node object.
 * \param mask Bitmask of the wanted attributes.
 * \return An identifier object of the name string.
 * \note As this helper method is mainly used to create error messages the
 * string length is restricted to 64 characters (including th terminating
 * 0) to avoid to long name strings.
 */
lib::IDENTIFIER edd_tree::build_attr_names(const class node_tree *node,
		const unsigned long mask) const {
	lib::IDENTIFIER id = ident_->NoIdent;
	char buf[64];

	char *s = buf;
	const int reserved = 4;
	int left = sizeof(buf) - reserved;

	unsigned long i;

	/* initialize as empty string */
	buf[0] = '\0';

	for (i = 0; i < node->attr_max(); ++i) {
		unsigned long attr = 1UL << i;

		if (mask & attr) {
			int len;

			len = snprintf(s, left, "%s ", node->attr_descr(i));
			if (len >= left) {
				len = snprintf(s, left + reserved, "...");
				s += len;

				break;
			}

			s += len;
			left -= len;
		}
	}

	if ((s > buf) && s[-1] == ' ')
		s[-1] = '\0';

	if (strlen(buf))
		id = ident_->create(buf);

	return id;
}

/**
 * \internal
 * Build type names.
 * This method create a readable string of the tree object types specified by
 * the argument vector of type numbers.
 * \param types Vector of the type numbers that should be printed to the
 * string. The vector must be terminated by the EDD_NONE type number.
 * \return An identifier object of the name string.
 * \note As this helper method is mainly used to create error messages the
 * string length is restricted to 64 characters (including th terminating
 * 0) to avoid to long name strings.
 */
lib::IDENTIFIER edd_tree::build_type_names(const unsigned long *types) const {
	lib::IDENTIFIER id = ident_->NoIdent;
	char buf[64];

	char *s = buf;
	const int reserved = 4;
	int left = sizeof(buf) - reserved;

	/* initialize as empty string */
	buf[0] = '\0';

	if (types) {
		while (*types != EDD_NONE) {
			// XXX ??? if (*types && *types < EDD_NODE_MAX)
			{
				int i;

				i = snprintf(s, left, "%s ", tree::get_descr(*types));
				if (i >= left) {
					i = snprintf(s, left + reserved, "...");
					s += i;

					break;
				}

				s += i;
				left -= i;
			}

			++types;
		}

		if ((s > buf) && s[-1] == ' ')
			s[-1] = '\0';
	}

	if (strlen(buf))
		id = ident_->create(buf);

	return id;
}

/**
 * \internal
 * Generate missing attribute error messages.
 * Helper method that create for missing attributes of a node object the
 * error messages. The messages are enqueued by the error manager object.
 * \param t Pointer to the node object.
 * \param required Bitmask of the required attributes of the node object.
 */
void edd_tree::generate_required_attr_errs(const class node_tree *t,
		const unsigned long required) const {
	const unsigned long attr_mask = t->attr_mask();
	const unsigned long attr_max = t->attr_max();
	unsigned long i;

	for (i = 0; i < attr_max; ++i) {
		const unsigned long attr = 1UL << i;

		if ((required & attr) && !(attr_mask & attr)) {
			err().error(xxAttributeMissing, t->pos,
					ident_->create(t->attr_descr(i)));
		}
	}
}

/**
 * \internal
 * Check tree object against type list.
 * Helper method that check an tree object against the given type list. If the
 * tree object is not of one of the types in the type list a type mismatch
 * error message is created end enqueued.
 * \param t Pointer to the tree object.
 * \param check The type informations that are used. This include the type
 * list and the parent node object + attribute number. That later
 * informations are required to generate useful error messages.
 */
void edd_tree::check_type(class tree *t, const struct pass0info &check) const {
	const unsigned long kind = t->kind;
	const unsigned long *types;

	assert(kind != EDD_NONE);

	types = check.types;
	while (*types != EDD_NONE) {
		if (*types == kind)
			return;

		++types;
	}

	const lib::IDENTIFIER ref_ident(
			ident_->create(check.parent->attr_descr(check.attr)));
	assert(ref_ident != ident_->NoIdent);

	err().error(xxTypeMismatch, t->pos, ref_ident, t->pos,
			build_type_names(check.types));
}

/**
 * \internal
 * The main pass0 loop.
 * This recurse over a complete subtree and do the generic attribute and
 * typecheck of the abstract syntax tree.
 * The attribute and type informations are part of the specialized node
 * objects and are available through the node interface (all the tree objects
 * are generated from a higher abstraction level with the AST tool). In case
 * of missing attributes or attributes with non-specifed type error messages
 * are created and enqueued.
 * \param t Pointer to the start object of the subtree.
 * \param flat Type informations for this tree level.
 * \param recursiv Type informations that are passed down to the subtree.
 */
void edd_tree::pass0(class tree *t, const struct pass0info &flat,
		const struct pass0info &recursiv) {
	while (t) {
		class node_tree *node = t->is_node();

		/* basic attribute check */
		if (node) {
			/* required attribute check */
			if ((node->attr_mask() & node->required()) != node->required())
				generate_required_attr_errs(node, node->required());
		}

		/* basic typecheck */
		if (flat.types) {
			check_type(t, flat);
		} else if (recursiv.types) {
			check_type(t, recursiv);
		}

		/* list check */
		if (t->next
				&& !(flat.parent->type_flags(flat.attr) & EDD_TYPE_FLAGS_LIST)) {
			err().error(xxInvalidList, t->pos,
					ident_->create(flat.parent->attr_descr(flat.attr)), t->pos,
					build_type_names(flat.types));
		}

		/* handle attributes */
		if (node) {
			unsigned long i;

			for (i = 0; i < node->attr_max(); ++i) {
				if (node->attr(i)) {
					struct pass0info down_flat;
					struct pass0info down_recursiv;

					down_flat.parent = node;
					down_flat.attr = i;
					down_flat.types = node->types_flat(i);

					down_recursiv.parent = node;
					down_recursiv.attr = i;
					down_recursiv.types = node->types_recursiv(i);

					/* overwrite if neccessary */
					if (recursiv.types && !down_recursiv.types)
						down_recursiv = recursiv;

					/* recurse over the tree */
					pass0(node->attr(i), down_flat, down_recursiv);
				}
			}
		}

		t = t->next;
	}
}

/**
 * \internal
 * Pass0 entry point.
 * This call the recursive pass0 check with the root pointer as start
 * condition.
 */
void edd_tree::pass0(void) {
	if (root) {
		unsigned long rootnode[2] = { EDD_ROOT, 0 };

		struct pass0info flat = { 0, 0, rootnode };
		struct pass0info recursiv = { 0, 0, 0 };

		/* then recurse over the tree */
		pass0(root, flat, recursiv);
	}
}

/**
 * \internal
 * The main pass1 loop.
 * This recurse over a complete subtree and dispatch the tree specific pass1()
 * methods for every tree object.
 * \param t Pointer to the start object of the subtree.
 */
void edd_tree::pass1(class tree *t) {
	while (t) {
		class node_tree *node = t->is_node();

		/* handle attributes */
		if (node) {
			unsigned long i;

			for (i = 0; i < node->attr_max(); ++i) {
				class tree *attr;

				attr = node->attr(i);
				if (attr)
					pass1(attr);
			}
		}

		t->pass1();

		t = t->next;
	}
}

/**
 * \internal
 * Pass1 entry point.
 * This call the recursive pass1 check with the root pointer as start
 * condition.
 */
void edd_tree::pass1(void) {
	pass1(root);
}

/**
 * \internal
 * The main pass2 loop.
 * This recurse over a complete subtree and dispatch the tree specific pass2()
 * methods for every tree object.
 * \param t Pointer to the start object of the subtree.
 */
void edd_tree::pass2(class tree *t, const struct pass2info &info) {
	while (t) {
		class node_tree *node = t->is_node();

		/* handle attributes */
		if (node) {
			struct pass2info down_info = { &info, node };
			unsigned long i;

			for (i = 0; i < node->attr_max(); ++i) {
				class tree *attr;

				attr = node->attr(i);
				if (attr) {
					down_info.attr = i;
					pass2(attr, down_info);
				}
			}
		}

		t->pass2(info);

		t = t->next;
	}
}

/**
 * \internal
 * Pass2 entry point.
 * This call the recursive pass2 check with the root pointer as start
 * condition.
 */
void edd_tree::pass2(void) {
	struct pass2info info = { NULL, NULL, 0 };

	pass2(root, info);
}

/**
 * \internal
 * Construct cross reference hash table.
 * This method fill out the given hashtable with the mappings to the
 * edd::EDD_OBJECT_tree (the EDD objects). For duplicate mappings
 * an error message is created and enqueued.
 */
void edd_tree::build_hashtable(
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
		class ROOT_tree *root) const {
	class EDD_OBJECT_tree *object;

	assert(root);

	object = root->definition_list();
	while (object) {
		if (object->ident() != ident_->NoIdent) {
			class EDD_OBJECT_tree *check;

			/* check for duplicates */
			check = hash->lookup(object->ident());
			if (check) {
				assert(check != object);

				if (relaxed_mode()) {
					err().warning(xxDoubleDefined, object->pos, object->ident(),
							check->pos, ident_->NoIdent);

					err().information(xxOverwrittenObject, check->pos,
							check->ident(), object->pos, ident_->NoIdent, true);

					/* update hashtable */
					check->assign_hash(NULL);
					object->assign_hash(hash);

					/* remove overwritten object */
					root->remove_definition(check);
					delete check;
				} else {
					err().error(xxIdentifierAlreadyDefined, object->pos,
							object->ident(), check->pos, ident_->NoIdent);
				}
			} else {
				const class c_method *c_method;

				/* warn if builtin identifier are shadowed */
				c_method = builtins_->lookup(object->ident().str());
				if (c_method && !c_method->special()) // XXX
					err().warning(xxShadowBuiltin, object->pos,
							object->ident());

				object->assign_hash(hash);

			}

			if (object->kind == EDD_COMPONENT) {
				object->set_component_parent(NULL);

				class COMPONENT_tree *t = (class COMPONENT_tree *) object;

				t->build_hashtable();
			}
		}

		object = object->next_object();
	}
}

/**
 * \internal
 * Interprete import object nodes.
 * \note This work recursivly on the imported device descriptions.
 */
void edd_tree::run_import(class ROOT_tree *root,
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash,
		const std::vector<std::string> &includes,
		const std::vector<std::string> &defines) const {
	std::list<class IMPORTED_DD_tree *> todo;
	class tree *t;

	assert(root);

	t = root->definition_list();
	while (t) {
		if (t->kind == EDD_IMPORTED_DD)
			todo.push_back((class IMPORTED_DD_tree *) t);

		t = t->next;
	}

	while (!todo.empty()) {
		class EDD_OBJECT_tree *object_list;
		class IMPORTED_DD_tree *import;

		import = todo.front();
		todo.pop_front();

		object_list = import->run(root, includes, defines);
		if (object_list)
			root->replace_definition(import, object_list);
		else
			root->remove_definition(import);

		delete import;
	}
}

/**
 * \internal
 * Interprete like object nodes.
 * This execute the EDD like top level objects. They are replaced in the
 * abstract syntax tree by cloned objects they point to. After cloning is done
 * the redefintions are applied. Linked like references are delayed until they
 * terminate on a non-like object. In case of circular dependencies like
 * execution is stopped and an error message is created and enqueued. Also the
 * like objects are removed and deleted to avoid future confusion. If there
 * are no errors the abstract syntax tree doesn't include any like object node
 * anymore.
 */
void edd_tree::run_like(class ROOT_tree *root,
		lib::hashtable<lib::IDENTIFIER, class EDD_OBJECT_tree *> *hash) const {
	std::list<class LIKE_tree *> todo;
	class tree *t;

	assert(root);

	t = root->definition_list();
	while (t) {
		if (t->kind == EDD_LIKE)
			todo.push_back((class LIKE_tree *) t);

		t = t->next;
	}

	while (!todo.empty()) {
		class EDD_OBJECT_tree *object;
		class LIKE_tree *like;
		bool push_back = false;

		like = todo.front();
		todo.pop_front();

		object = like->run(hash, push_back);
		if (object) {
			/* install redefined object into hashtable */
			object->assign_hash(hash);

			/* replace object in definition list */
			root->replace_definition(like, object);

			/* and delete the like subtree */
			delete like;
		} else {
			if (!push_back) {
				root->remove_definition(like);
				delete like;
			} else
				todo.push_back(like);
		}
	}
}

/**
 * Return the identification object.
 * Lookup the identification object of the parsed EDD specification and
 * return a pointer to it. The object can be used to obtain informations about
 * manufacturer, device_id, device_revision and so on.
 * \return Pointer to the edd::IDENTIFICATION_tree object.
 */
const class IDENTIFICATION_tree *
edd_tree::identification(void) const {
	const class IDENTIFICATION_tree *t = NULL;

	if (root)
		t = root->identification();

	return t;
}

/**
 * Return a vector of all files included by the ddl.
 * \return Vector of pathnames.
 */
std::vector<std::string> edd_tree::ddl_files(void) const {
	std::vector<std::string> ret;
	ret.reserve(included_files.size());

	included_files_t::const_iterator iter, end;

	for (iter = included_files.begin(), end = included_files.end(); iter != end;
			++iter)
		ret.push_back(*iter);

	return ret;
}

/**
 * Tree node lookup.
 * This method lookup in the abstract syntax tree if a tree node with the
 * specified name exist. E.g. the tree is searched for toplevel nodes with
 * the identifier name.
 * \param name std::string of the identifier that should be searched for.
 * \return The tree node object pointer or NULL if there doesn't exist a node
 * with such identifier.
 */
class EDD_OBJECT_tree *
edd_tree::node_lookup(const std::string &name) const {
	return node_lookup(ident_->create(name));
}

/**
 * Tree node lookup.
 * This method lookup in the abstract syntax tree if a tree node with the
 * specified name exist. E.g. the tree is searched for toplevel nodes with
 * the identifier name.
 * \param name C style string of the identifier that should be searched for.
 * \return The tree node object pointer or NULL if there doesn't exist a node
 * with such identifier.
 */
class EDD_OBJECT_tree *
edd_tree::node_lookup(const char *name) const {
	return node_lookup(ident_->create(name));
}

/**
 * Tree node lookup.
 * This method lookup in the abstract syntax tree if a tree node with the
 * specified name exist. E.g. the tree is searched for toplevel nodes with
 * the identifier name.
 * \param name lib::IDENTIFIER that should be searched for.
 * \return The tree node object pointer or NULL if there doesn't exist a node
 * with such identifier.
 */
class EDD_OBJECT_tree *
edd_tree::node_lookup(const lib::IDENTIFIER &name) const {
	return hash_->lookup(name);
}

/**
 * Dictionary lookup.
 * This method lookup the dictionary if an entry with the specified name
 * exist. It return the dictionary entry or if no dictionary entry exist the
 * search name itself.
 * \param name std::string of the identifier that should be searched for.
 * \return The dictionary entry or the search name itself.
 */
std::string edd_tree::dictionary_lookup(const std::string &name) const {
	return dictionary_lookup(ident_->create(name));
}

/**
 * Dictionary lookup.
 * This method lookup the dictionary if an entry with the specified name
 * exist. It return the dictionary entry or if no dictionary entry exist the
 * search name itself.
 * \param name C style string of the identifier that should be searched for.
 * \return The dictionary entry or the search name itself.
 */
std::string edd_tree::dictionary_lookup(const char *name) const {
	return dictionary_lookup(ident_->create(name));
}

/**
 * Dictionary lookup.
 * This method lookup the dictionary if an entry with the specified name
 * exist. It return the dictionary entry or if no dictionary entry exist the
 * search name itself.
 * \param name lib::IDENTIFIER that should be searched for.
 * \return The dictionary entry or the search name itself.
 */
std::string edd_tree::dictionary_lookup(const lib::IDENTIFIER &name) const {
	bool found;

	return dictionary_lookup(name, found);
}

std::string edd_tree::dictionary_lookup(const lib::IDENTIFIER &name,
		bool &found) const {
	std::string ret(dictionary().lookup(name, found));

	return found ? langmap(ret) : ret;
}

/**
 * Evaluation dispatcher.
 * Dispatch method that evaluate the tree object at runtime to it's current
 * value. It just call the tree specific eval() method. This operation can
 * throw an exception in case of errors: tree object isn't evaluable, circular
 * dependencies, invalid references, data value leave it's range and so on.
 * \param t The tree object that should be evaluated.
 * \param env The environment under which the interpreter should operate.
 * \return The resulting %operand list of the operation. The specific %operand
 * type depend on the tree object. Note, the result can also be an %operand
 * list! Also note that the ownership of the %operand move to the caller. That
 * means that the caller must explicitly delete the %operand list after usage.
 * \exception RuntimeError(...) evaluation error encountered
 */
operand_ptr edd_tree::eval(class tree *t, class method_env *env) const {
	return t ? t->eval(env) : operand_ptr();
}

/**
 * Debug evaluation dispatcher.
 * This is for debugging only. Dispatch the evaluation method of the tree
 * object and dump out the resulting %operand list. Exceptions of the
 * evaluation call are catched and displayed. Storage management is
 * automatically done too.
 * \param t The tree object that should be evaluated.
 * \param f File stream there the descriptions are written.
 * \param level indentation level; the line will be headed by level * 3
 * spaces.
 */
void edd_tree::eval(lib::stream &f, class tree *t, unsigned long level) const {
	std::string prefix("");

	/* create prefix string */
	{
		char buf[32];
		unsigned long i;

		for (i = 0; i < level; ++i)
			prefix += "   ";

		snprintf(buf, sizeof(buf), "-- line %lu ", t->pos.line);
		prefix += buf;
	}

	try {
		operand_ptr list(t->eval(NULL));

		if (list) {
			while (list) {
				f.put(prefix.c_str());
				list->dump(f);
				f.put('\n');

				list = list.next();
			}
		} else {
			f.put(prefix.c_str());
			f.printf("$$$$ eval -> NULL\n");
		}
	} catch (const RuntimeError &xx) {
		f.put(prefix.c_str());
		f.printf("$$$$ eval -> exception (%i, line %ld, column %ld)\n",
				xx.error, xx.pos.line, xx.pos.column);
	} catch (...) {
		f.put(prefix.c_str());
		f.printf("$$$$ eval -> unknown exception\n");
	}
}

/**
 * Tree ID mapping.
 * This method convert an ID to the corresponding edd::EDD_OBJECT_tree
 * pointer. If no such mapping exist a NULL pointer is returned instead.
 * \param id The ID of the searched edd::EDD_OBJECT_tree.
 * \return The corresponding edd::EDD_OBJECT_tree pointer or NULL.
 */
class EDD_OBJECT_tree *
edd_tree::id2tree(unsigned int id) const throw () {
	return treemap_->lookup(id);
}

void edd_tree::register_cmd_by_number(int number,
		const class COMMAND_tree *cmd) {
	if (cmd)
		cmd_by_number[number] = cmd;
	else
		cmd_by_number.erase(number);
}

const class COMMAND_tree *
edd_tree::lookup_cmd_by_number(int number) const {
	cmd_by_number_t::const_iterator iter(cmd_by_number.find(number));

	if (iter != cmd_by_number.end())
		return iter->second;

	return NULL;
}

const class COMMAND_tree *
edd_tree::lookup_cmd_by_number_pass1(int number) const {
	const class COMMAND_tree *command;

	command = lookup_cmd_by_number(number);
	if (!command) {
		/* not found in lookup map;
		 * we need to verify if the command number don't exist because
		 * the register_cmd_by_number() is called from pass1 too
		 */

		class tree *t;

		assert(root);

		t = root->definition_list();
		while (t) {
			if (t->kind == EDD_COMMAND) {
				class COMMAND_tree *c = (class COMMAND_tree *) t;

				if (c->check_number(number)) {
					command = c;
					break;
				}
			}

			t = t->next;
		}
	}

	return command;
}

void edd_tree::search_DD_ITEM_METHOD_refs0(class tree *t) {
	while (t) {
		class node_tree *node = t->is_node();

		if (node) {
			if (node->kind != EDD_METHOD
					&& node->kind != EDD_METHOD_DEFINITION) {
				unsigned long i;

				for (i = 0; i < node->attr_max(); ++i) {
					class tree *attr;

					attr = node->attr(i);
					if (attr)
						search_DD_ITEM_METHOD_refs0(attr);
				}
			} else {
			}
		}

		t = t->next;
	}
}

void edd_tree::search_DD_ITEM_METHOD_refs(void) {
	typedef std::list<class tree *> refs_t;
	typedef std::map<class METHOD_tree *, refs_t> DD_ITEM_METHOD_refs_t;

	DD_ITEM_METHOD_refs_t DD_ITEM_METHOD_refs;

	if (DD_ITEM_METHOD_refs.empty()) {
		class EDD_OBJECT_tree *object = root->definition_list();

		while (object) {
			if (object->kind == EDD_METHOD) {
				class METHOD_tree *method = (class METHOD_tree *) object;

				if (method->check_dd_item_parameter()) {
					DD_ITEM_METHOD_refs_t::value_type value(method, refs_t());
					DD_ITEM_METHOD_refs.insert(value);
				}
			}

			object = object->next_object();
		}

		search_DD_ITEM_METHOD_refs0(root->definition_list());
	}
}

/**
 * Lookup the FILE objects for an specific object.
 * This helper method lookup all FILE objects for the specified object.
 * If the object is found in a FILE object, the lib::IDENTIFIER of this
 * FILE object is returned. Otherwise lib::IDENT::NoIdent is returned.
 */
lib::IDENTIFIER edd_tree::search_FILE(class method_env *env,
		class EDD_OBJECT_tree *object) const {
	lib::IDENTIFIER ret;
	class tree *list;

	list = root->definition_list();
	while (list) {
		if (list->kind == EDD_FILE) {
			class FILE_tree *file = (class FILE_tree *) list;

			if (file->lookup_member(env, object)) {
				ret = file->ident();
				break;
			}
		}

		list = list->next;
	}

	return ret;
}

/**
 * \internal
 */
void edd_tree::dump_tree0(lib::stream &f, const class tree *t,
		unsigned long level, bool single) const {
	if (t) {
		while (t) {
			const class node_tree *node = t->is_node();
			unsigned long i;

			for (i = 0; i < level; ++i)
				f.put("   ");

			f.printf("-- %s type %i, line %lu", node ? "node" : "terminal",
					t->kind, t->pos.line);

			t->dump(f);

			if (node) {
				for (i = 0; i < node->attr_max(); ++i) {
					if (node->attr(i)) {
						unsigned long j;

						for (j = 0; j < level + 1; ++j)
							f.put("   ");

						f.printf("-- %s\n", node->attr_descr(i));

						dump_tree0(f, node->attr(i), level + 2, false);
					}
				}
			}

			if (single)
				break;

			t = t->next;
		}
	} else {
		unsigned long i;

		for (i = 0; i < level; ++i)
			f.put("   ");

		f.put("-- NULL\n");
	}
}

/**
 * \internal
 */
void edd_tree::dump_tree(lib::stream &f, const class tree *t,
		bool single) const {
	dump_tree0(f, t, 0, single);
}

/**
 * \internal
 * Delete tree object from tree list.
 * This delete a tree object from the tree list. The element that have to be
 * removed must be part of the list. Otherwise an exception is thrown.
 * It's the callers responsibility to verify this condition.
 * \param head Pointer to the pointer of the first list element.
 * \param t The list element that have to be removed.
 * \exception edd::RuntimeError() t was not part of the tree list.
 */
void edd_tree::tree_delete(class tree **head, class tree *t) throw () {
	while (*head) {
		if (*head == t) {
			*head = t->next;
			t->next = NULL;

			return;
		}

		head = &(*head)->next;
	}

	assert(0);
}

/**
 * \internal
 * Replace tree object inside tree list.
 * This replace a tree object in a tree list by another tree object. The
 * element that have to be replaced must be part of the list. Otherwise an
 * exception is thrown. It's the callers responsibility to verify this
 * condition.
 * \param head Pointer to the pointer of the first list element.
 * \param old The list element that have to be replaced.
 * \param n The new list element.
 * \exception edd::RuntimeError() old was not part of the tree list.
 */
void edd_tree::tree_replace(class tree **head, class tree *old,
		class tree *n) throw () {
	while (*head) {
		if (*head == old) {
			n->next = old->next;
			old->next = NULL;

			*head = n;

			return;
		}

		head = &(*head)->next;
	}

	assert(0);
}

/**
 * \internal
 * Reverse a tree list.
 * This reverse the elements of a tree list and return the new start element of
 * the list.
 * \param tree The tree list.
 * \return The new head of the reversed list.
 */
class tree *
edd_tree::tree_reverse(class tree *tree) throw () {
	register class tree *t = tree;

	if (t) {
		class tree *next = tree->next;

		t->next = NULL;
		while (next) {
			tree = next;
			next = tree->next;
			tree->next = t;
			t = tree;
		}
	}

	return t;
}

/**
 * \internal
 * Get tree element by index.
 * Looks up a tree list like an array for the n-th element and return it. The
 * element is not removed from the list. If the index is greater as there are
 * list elements the last list element is returned.
 * \param head Pointer of the start element of the list.
 * \param index The index of the element that have to be returned.
 * \return The list element that is virtually numbered with index or the last
 * list element if index exceeds the available list elements.
 */
class tree *
edd_tree::tree_by_index(class tree *head, unsigned long index) throw () {
	while (head && index--)
		head = head->next;

	return head;
}

/**
 * \internal
 * Extract elements from tree list.
 * This remove all elements of type kind from the tree list starting with the
 * head argument. The removed elements are concatenated to a new list and the
 * pointer to the start argument of the newly created list is returned.
 * \param head Pointer to the pointer of the start element of the tree list.
 * \param kind The type of the elements that have to be extracted.
 * \return A new list of the removed list elements.
 */
class tree *
edd_tree::tree_extract(class tree **head, unsigned long kind) throw () {
	return tree_extract(head, kind, 1);
}
;

/**
 * \internal
 * Extract elements from tree list.
 * This remove all elements of type kind from the tree list starting with the
 * head argument. The removed elements are concatenated to a new list and the
 * pointer to the start argument of the newly created list is returned.
 * \param head Pointer to the pointer of the start element of the tree list.
 * \param kind The type of the elements that have to be extracted.
 * \param max The type range of elemnst that have to be extracted.
 * \return A new list of the removed list elements.
 */
class tree *
edd_tree::tree_extract(class tree **head, unsigned long kind,
		unsigned long max) throw () {
	class tree *t; /* return list head */
	class tree **thead = &t; /* return list head forward ptr */

	while (head && *head) {
		unsigned long current = (*head)->kind;

		if ((current >= kind) && (current < (kind + max))) {
			*thead = *head;
			thead = &((*head)->next);
			if (thead)
				*head = *thead;
		} else
			head = &((*head)->next);
	}

	*thead = NULL;
	return t;
}

/**
 * Count tree elements.
 * This count the total number of concatenated tree elements and return this
 * value.
 * \param t Pointer of the start element of the list.
 * \return The total number of tree elements.
 */
unsigned long edd_tree::tree_count(class tree *t) throw () {
	unsigned long count = 0;

	while (t) {
		++count;
		t = t->next;
	}

	return count;
}

/**
 * \internal
 * Concat two tree lists.
 * This concat two tree lists by appending the second list on the tail of the
 * first list.
 */
class tree *
edd_tree::tree_concat(class tree *t, class tree *t1) throw () {
	class tree *head = t;

	if (head) {
		if (t1) {
			while (t->next)
				t = t->next;

			t->next = t1;
		}
	} else
		head = t1;

	return head;
}

/**
 * \internal
 * Concat three tree lists.
 * Work like concatenating two tree lists.
 */
class tree *
edd_tree::tree_concat(class tree *t, class tree *t1, class tree *t2) throw () {
	class tree *tx;

	tx = tree_concat(t, t1);
	tx = tree_concat(tx, t2);

	return tx;
}

/**
 * \internal
 * Concat four tree lists.
 * Work like concatenating two tree lists.
 */
class tree *
edd_tree::tree_concat(class tree *t, class tree *t1, class tree *t2,
		class tree *t3) throw () {
	class tree *tx;

	tx = tree_concat(t, t1, t2);
	tx = tree_concat(tx, t3);

	return tx;
}

/**
 * \internal
 * Concat five tree lists.
 * Work like concatenating two tree lists.
 */
class tree *
edd_tree::tree_concat(class tree *t, class tree *t1, class tree *t2,
		class tree *t3, class tree *t4) throw () {
	class tree *tx;

	tx = tree_concat(t, t1, t2, t3);
	tx = tree_concat(tx, t4);

	return tx;
}

/**
 * \internal
 * Concat six tree lists.
 * Work like concatenating two tree lists.
 */
class tree *
edd_tree::tree_concat(class tree *t, class tree *t1, class tree *t2,
		class tree *t3, class tree *t4, class tree *t5) throw () {
	class tree *tx;

	tx = tree_concat(t, t1, t2, t3, t4);
	tx = tree_concat(tx, t5);

	return tx;
}

/**
 * \internal
 * Generic node object initialization.
 * This initalize a newly allocated node object. It implement the attribute
 * list splitup for all node objects. It use the node interface for getting
 * all required informations. For collisions an attribute already defined
 * message is created and enqueued. If an attribute doesn't belong to this
 * node object a fatal error is created.
 * \note At the moment only 32 attributes per node are supported (long data
 * types is used as bitmask).
 * \param node Pointer of the node object.
 * \param attribute_list The pseudo object tree list that hold the attribute
 * information and the attribute pointer itself.
 */
void edd_tree::m_node(class node_tree *node, class tree *attribute_list) const {
	class tree *t;

	/* internal error */
	assert(node);

	t = attribute_list;
	while (t) {
		class pseudo_tree *pseudo = NULL;

		if (t->kind == EDD_pseudo)
			pseudo = (class pseudo_tree *) (t);

		if (pseudo && (pseudo->tkind() == node->kind)
				&& (pseudo->tattr() < node->attr_max())) {
			unsigned long index = pseudo->tattr();
			unsigned long attr = 1UL << index;

			if (!(node->attr_mask() & attr)) {
				node->assign_attr(index, pseudo->release());
				node->attr_mask_set(attr);
			} else {
				if (node->merge() & attr) {
					node->add_attr(index, pseudo->release());
				} else {
					lib::pos pos = t->pos;

					if (pseudo->get())
						pos = pseudo->get()->pos;

					/* attribute already defined */
					err().error(xxAttributeAlreadyDefined, pos,
							ident_->create(node->attr_descr(index)));
				}
			}
		} else {
			/* internal failure (unknown attribute) */
			throw RuntimeError(xxInternal, t->pos,
					ident_->create("edd_tree::node_splitup"));
		}

		t = t->next;
	}

	delete attribute_list;
}

/**
 * \internal
 * Generic edd object node initialization.
 * This initialize a newly allocated edd object. It check the identifier
 * tree and apply the identifier object to the edd object. Then the attribute
 * list splitup is dispatched.
 * \param object Pointer of the edd object.
 * \param identifier Pointer of the identifier for this edd object.
 * \param attribute_list The attribute list for the edd object.
 * \return The initialized edd object object.
 * \exception RuntimeError(xxOutOfMemory) in case that the t argument pointer
 * is NULL.
 */
void edd_tree::m_edd_object(class EDD_OBJECT_tree *object,
		class tree *identifier, class tree *attribute_list) const {
	assert(object);
	assert(identifier && identifier->kind == EDD_identifier);

	/* initialize identifier for this node */
	object->assign_ident(((class identifier_tree *) identifier)->data);

	/* and free up the tree */
	delete identifier;

	/* splitup attribute list */
	m_node(object, attribute_list);

	/* dispatch special things */
	object->post_construction(false);
}

/**
 * \internal
 * Create pseudo terminal object.
 * This create a pseudo tree object that is used to describe attribute lists
 * in a generic way. It hold the attribute itself, the attribute number and
 * the type of the node this attribute belongs to.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_pseudo(int kind, // should be enum kinds
		unsigned long attr, class tree *node) {
	return m_pseudo(kind, attr, node, node ? node->pos : lib::NoPosition);
}

/**
 * \internal
 * Create pseudo terminal object.
 * This create a pseudo tree object that is used to describe attribute lists
 * in a generic way. It hold the attribute itself, the attribute number and
 * the type of the node this attribute belongs to.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_pseudo(int kind, // should be enum kinds
		unsigned long attr, class tree *node, const lib::pos &pos) {
	class tree *t;

	t = new pseudo_tree(node, kinds(kind), attr, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create boolean terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_boolean(bool i, const lib::pos &pos) {
	class tree *t;

	t = new boolean_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create character terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_character(char i0, const lib::pos &pos) {
	class tree *t;
	int32 i = i0;

	t = new integer_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create integer terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_integer(int32 i, const lib::pos &pos, bool negative) {
	class tree *t;

	if (negative || i >= 0)
		t = new integer_tree(i, this, pos);
	else
		t = new integer_tree(true, (uint32) i, this, pos);

	assert(t);
	return t;
}

/**
 * \internal
 * Create integer64 terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_integer64(int64 i, const lib::pos &pos, bool negative) {
	class tree *t;

	if (negative || i >= 0)
		t = new integer_tree(i, this, pos);
	else
		t = new integer_tree(true, (uint64) i, this, pos);

	assert(t);
	return t;
}

/**
 * \internal
 * Create real terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_real(double i, const lib::pos &pos) {
	class tree *t;

	t = new real_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create enum terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_enum(uint32 i, const lib::pos &pos) {
	class tree *t;

	t = new enum_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create set terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_set(uint32 i, const lib::pos &pos) {
	class tree *t;

	t = new set_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create bitset terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_bitset(lib::vbitset *&i, const lib::pos &pos) {
	class tree *t;

	t = new bitset_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create bitset terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_bitset(uint64 i, const lib::pos &pos) {
	class tree *t;

	t = new bitset_tree(i, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create identifier terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_identifier(lib::scan_buf &buf, const lib::pos &pos) {
	class tree *t;
	size_t len = buf.len();
	char *str = buf.release();

	if (len) {
		assert(str);

		t = new identifier_tree(ident_->create_from_string(str), this, pos);

		assert(str == NULL);
	} else {
		t = new identifier_tree(ident_->NoIdent, this, pos);

		if (str)
			delete[] str;
	}

	assert(t);
	return t;
}

/**
 * \internal
 * Create identifier terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_identifier(const char *str, const lib::pos &pos) {
	class tree *t;

	assert(str);

	t = new identifier_tree(ident_->create(str), this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create string terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_string(const char *str, const lib::pos &pos) {
	class tree *t;

	assert(str);

	t = new string_tree(str, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create string terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_string(lib::scan_buf &buf, const lib::pos &pos) {
	class string_tree *t;
	size_t len = buf.len();
	char *str = buf.release();

	if (len) {
		assert(str);
		t = new string_tree(str, len, this, pos);
	} else
		t = new string_tree(NULL, this, pos);

	if (str)
		delete[] str;

	assert(t);
	return t;
}

/**
 * \internal
 * Create string terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_string(class tree *t, const lib::pos &pos) {
	class tree *str = NULL;
	lib::scratch_buf buf(100);

	switch (t->kind) {
	case EDD_integer:
		buf += ((integer_tree *) t)->get_string();
		break;

	case EDD_enum:
		buf.printf("%" PRIu32 , ((enum_tree *) t)->data);
		break;

	case EDD_set:
		buf.printf("%" PRIu32 , ((set_tree *) t)->data);
		break;

	case EDD_real:
		buf.printf("%f", ((real_tree *) t)->data);
		break;

	case EDD_identifier:
		buf.printf("%s", ((identifier_tree *) t)->data.c_str());
		break;

	case EDD_string:
		str = t;
		t = NULL;
		break;

	default:
		/* create empty string */
		break;
	}

	if (t)
		delete t;

	if (!str) {
		str = new string_tree(buf.data(), buf.length(), this, pos);
		assert(str);
	}

	return str;
}

class tree *
edd_tree::m_current_role(const lib::pos &pos) {
	class tree *t;

	t = new current_role_tree(this, pos);
	assert(t);

	return t;
}

class tree *
edd_tree::m_array_index(const lib::pos &pos) {
	class tree *t;

	t = new array_index_tree(this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create uuid terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_uuid(const char *str, const lib::pos &pos) {
	class tree *t;

	assert(str);

	t = new uuid_tree(str, this, pos);
	assert(t);

	return t;
}

/**
 * \internal
 * Create uuid terminal object.
 * \exception RuntimeError(xxOutOfMemory) in case that the new operator fails.
 */
class tree *
edd_tree::m_uuid(lib::scan_buf &buf, const lib::pos &pos) {
	class uuid_tree *t;
	size_t len = buf.len();
	char *str = buf.release();

	if (len) {
		assert(str);
		t = new uuid_tree(str, len, this, pos);
	} else
		t = new uuid_tree(NULL, len, this, pos);

	if (str)
		delete[] str;

	assert(t);
	return t;
}

/**
 * \internal
 */
struct str_list *
edd_tree::m_str_list(const lib::pos &pos, lib::scan_buf &buf) {
	struct str_list *t;

	t = new str_list;
	assert(t);

	t->next = NULL;
	t->len = buf.len();
	t->str = buf.release();

	if (!t->str) {
		assert(!t->len);

		t->str = new char[1];
		t->str[0] = '\0';
	}

	return t;
}

/**
 * \internal
 */
void edd_tree::m_dictionary_entry(const lib::pos &pos, char *idstr, int major,
		int minor, struct str_list *list) {
	struct str_list *l;
	assert(list);

	/* reverse the list */
	l = list;
	if (l) {
		struct str_list *next = l->next;

		l->next = NULL;
		while (next) {
			list = next;
			next = list->next;
			list->next = l;
			l = list;
		}
	}
	list = l;

	/* calculate complete string len */
	size_t len = 0;

	l = list;
	while (l) {
		len += l->len;
		l = l->next;
	}

	std::string str;
	str.reserve(len + 1);

	/* copy it over and free it */
	l = list;
	while (l) {
		list = l->next;

		str.append(l->str, l->len);

		delete[] l->str;
		delete l;

		l = list;
	}

	bool check_duplicate = true;

	if (major && minor) {
		if (strcmp(idstr, "xxx_highestoffset") == 0)
			check_duplicate = false;
	}

	lib::IDENTIFIER id(ident_->create_from_string(idstr));
	assert(idstr == NULL);

	if (check_duplicate && dictionary().exist(id)) {
		err().warning(xxDoubleDictionaryEntry, pos, id,
				dictionary().get_pos(id), ident_->NoIdent);
	}

	check_langstring(str, pos);

	dictionary().add(id, str, pos);
}

} /* namespace edd */
