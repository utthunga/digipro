
/* 
 * $Id: devices_data.cxx,v 1.15 2008/10/21 00:20:03 fna Exp $
 * 
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

struct sub_table_entry
{
	unsigned short id;
	const char *define;
};

static const struct sub_table_entry STANDARD_device_types[] =
{
	{ 0x01, "_TABLES" },
	{ 0x02, "_UNIVERSAL" },
	{ 0x03, "_COMMON_PRACTICE" },
	{ 0x05, "_TEMPERATURE_UNIVERSAL_5" },
	{ 0x06, "_TEMPERATURE_COMMON_5" },
	{ 0x07, "_PRESSURE_UNIVERSAL_5" },
	{ 0x08, "_PRESSURE_COMMON_5" },
	{ 0x09, "_FLOW_UNIVERSAL_5" },
	{ 0x0a, "_FLOW_COMMON_5" },
	{ 0x0c, "_LEVEL_UNIVERSAL_5" },
	{ 0x0d, "_LEVEL_COMMON_5" },
	{ 0x0e, "_MASSFLOW_UNIVERSAL_5" },
	{ 0x0f, "_MASSFLOW_COMMON_5" },
	{ 0x10, "_MENU" },
	{ 0x11, "_RMTFLOW" },
	{ 0x12, "_RMTPRES" },
	{ 0x13, "_MOORE_STANDARD" },
	{ 0x14, "_SIEPRES" },
	{ 0x15, "_P2_STD" },
	{ 0x16, "_PV" },
	{ 0x17, "_TEMPERATURE_FAMILY" },
	{ 0x63, "_MUNGE" },
	{    0, NULL }
};

static const struct sub_table_entry AMETEK_device_types[] =
{
	{ 0x04, "_NEWTHERMOX" },
	{    0, NULL }
};

static const struct sub_table_entry BROOKS_INSTRUMENT_device_types[] =
{
	{ 0x01, "_TRI20" },
	{ 0x02, "_38XXVA" },
	{ 0x03, "_99XX_OVAL" },
	{ 0x04, "_QUANTIM" },
	{    0, NULL }
};

static const struct sub_table_entry DELTA_device_types[] =
{
	{ 0x01, "_HT" },
	{    0, NULL }
};

static const struct sub_table_entry ENDRESS_HAUSER_device_types[] =
{
	{ 0x03, "_FMU860" },
	{ 0x04, "_FMU861" },
	{ 0x05, "_FMU862" },
	{ 0x06, "_FMR130" },
	{ 0x07, "_CERABS" },
	{ 0x08, "_FEC12" },
	{ 0x09, "_DELTBS" },
	{ 0x0A, "_FMU231" },
	{ 0x0B, "_DELTAPS" },
	{ 0x0C, "_FMR23X" },
	{ 0x0D, "_FMP200" },
	{ 0x0E, "_CERABM" },
	{ 0x0F, "_FMR2xx" },
	{ 0x10, "_FMR53x" },
	{ 0x11, "_FMU4x" },
	{ 0x12, "_FMP4x" },
	{ 0x13, "_FMG60" },
	{ 0x17, "_PMD7_FMD7" },
	{ 0x18, "_PMC7_PMP7" },
	{ 0x1b, "_FMU9x" },
	{ 0x1e, "_FMR25x" },
	{ 0x32, "_PROMAG33" },
	{ 0x33, "_PROWIRL70" },
	{ 0x34, "_PROMASS63" },
	{ 0x35, "_PROMAG39" },
	{ 0x36, "_PROMAG35S" },
	{ 0x37, "_PROWIRL77" },
	{ 0x39, "_PROMASS60" },
	{ 0x40, "_PROSON_F" },
	{ 0x41, "_PROMAG50" },
	{ 0x42, "_PROMAG53" },
	{ 0x43, "_PROMAG51" },
	{ 0x45, "_PROMAG10" },
	{ 0x46, "_PROMAG23" },
	{ 0x50, "_PROMASS80" },
	{ 0x51, "_PROMASS83" },
	{ 0x52, "_PROMASS84" },
	{ 0x53, "_PROMASS40" },
	{ 0x56, "_PROWIRL72" },
	{ 0x57, "_PROWIRL73" },
	{ 0x58, "_PROSON_F90" },
	{ 0x59, "_PROSON_F93" },
	{ 0x64, "_AT70" },
	{ 0x65, "_TMASS65" },
	{ 0x78, "_TMD832" },
	{ 0x79, "_TMD833" },
	{ 0x7c, "_TMD842" },
	{ 0x8c, "_MYPRO_PH" },
	{ 0x8d, "_MYPRO_LFC" },
	{ 0x8e, "_MYPRO_LFI" },
	{ 0xb5, "_NMT530" },
	{ 0xb6, "_NMS530" },
	{ 0xc8, "_TMT182" },
	{ 0xc9, "_TMT122" },
	{ 0xcb, "_TMT142" },
	{ 0xca, "_TMT162" },
	{ 0xff, "_FIELDTEST" },
	{    0, NULL }
};

static const struct sub_table_entry BAILEY_FISCHER_AND_PORTER_device_types[] =
{
	{ 0x07, "_50XM2000" },
	{ 0x08, "_50XE4000" },
	{ 0x0e, "_50VT1000" },
	{ 0x0f, "_50VM1000" },
	{ 0x19, "_50XM1000" },
	{ 0x1A, "_50SM1000" },
	{ 0x42, "_PTH" },
	{ 0x50, "_TB82PH_pH" },
	{ 0x51, "_TB82PH_ORP" },
	{ 0x52, "_TB82PH_pION" },
	{ 0x53, "_TB82PH_IConc" },
	{ 0x54, "_TB82EC_COND" },
	{ 0x55, "_TB82EC_CONC" },
	{ 0x56, "_TB82TE_COND" },
	{ 0x57, "_TB82TE_CONC" },
	{ 0x58, "_TB82TC_COND" },
	{ 0x59, "_TB82TC_CONC" },
	{    0, NULL }
};

static const struct sub_table_entry FISHER_CONTROLS_device_types[] =
{
	{ 0x01, "_DT4000" },
	{ 0x02, "_DVC5000" },
	{ 0x05, "_DVC2000" },
	{    0, NULL }
};

static const struct sub_table_entry FOXBORO_device_types[] =
{
	{ 0x29, "_IMT25" },
	{ 0x2E, "_IA_PRESSURE" },
	{ 0x2f, "_IMV_25_30" },
	{ 0x30, "_875PH" },
	{ 0x33, "_ITVORTEX" },
	{ 0x34, "_CFT50" },
	{ 0x35, "_875CR" },
	{ 0x36, "_875EC" },
	{ 0x38, "_RTT15" },
	{ 0x7d, "_CFT30" },
	{    0, NULL }
};

static const struct sub_table_entry FUJI_device_types[] =
{
	{ 0x01, "_FCX" },
	{ 0x02, "_FCXA2" },
	{ 0x81, "_FRC" },
	{    0, NULL }
};

static const struct sub_table_entry HARTMANN_BRAUN_device_types[] =
{
	{ 0x03, "_TEU471" },
	{ 0x04, "_TEU421" },
	{ 0x05, "_TEU211" },
	{ 0x06, "_TS11" },
	{ 0x08, "_TH02" },
	{ 0x40, "_TZID" },
	{ 0x41, "_TZIDC" },
	{ 0x85, "_AS800" },
	{ 0x87, "_CONTRAC" },
	{    0, NULL }
};

static const struct sub_table_entry HONEYWELL_device_types[] =
{
	{ 0x01, "_ST3000" },
	{ 0x02, "_STT25T" },
	{ 0x03, "_HWFLOW" },
	{ 0x04, "_STT250" },
	{ 0x05, "_HERCULINE" },
	{ 0x06, "_SMARTCET" },
	{ 0x07, "_STT17H" },
	{    0, NULL }
};

static const struct sub_table_entry KAY_RAY_SENSALL_device_types[] =
{
	{ 0x08, "_4050" },
	{ 0x09, "_3680" },
	{ 0x0A, "_4790" },
	{ 0x0b, "_ACCU_PULSE_PRO" },
	{ 0x0f, "_3280" },
	{    0, NULL }
};

static const struct sub_table_entry ABB_device_types[] =
{
	{ 0x01, "_KSX" },
	{ 0x02, "_600T" },
	{ 0x03, "_2600T_268" },
	{ 0x04, "_2600T_262_264" },
	{ 0x08, "_KST" },
	{ 0x0a, "_658T" },
	{ 0x0c, "_653S" },
	{ 0x10, "_VALVE" },
	{ 0x18, "_FLOW" },
	{ 0x1d, "_FSM4000" },
	{ 0x20, "_LEVEL" },
	{ 0x23, "_TB82PH" },
	{ 0x24, "_TB82EC" },
	{ 0x25, "_TB82TE" },
	{ 0x26, "_TB82TC" },
	{ 0x89, "_2000T" },
	{ 0x8c, "_261" },
	{    0, NULL }
};

static const struct sub_table_entry M_SYSTEM_device_types[] =
{
	{ 0x01, "_B6U" },
	{ 0x02, "_B3HU" },
	{ 0x03, "_27HU" },
	{    0, NULL }
};

static const struct sub_table_entry MICRO_MOTION_device_types[] =
{
	{ 0x07, "_9712" },
	{ 0x14, "_9720" },
	{ 0x15, "_9739" },
	{ 0x24, "_2000_CONFIG" },
	{ 0x1e, "_9701" },
	{ 0x26, "_2000_IS" },
	{ 0x27, "_1000_IS" },
	{ 0x29, "_1000" },
	{ 0x2a, "_2000" },
	{ 0x34, "_2400S_ANALOG" },
	{ 0x41, "_3000" },
	{ 0x25, "_1500" },
	{    0, NULL }
};

static const struct sub_table_entry MOORE_INDUSTRIES_device_types[] =
{
	{ 0x01, "_TRZ" },
	{ 0x03, "_THZ" },
	{    0, NULL }
};

static const struct sub_table_entry MOORE_PRODUCTS_device_types[] =
{
	{ 0x01, "_340_A" },
	{ 0x02, "_340_B" },
	{ 0x03, "_344" },
	{ 0x05, "_341" },
	{ 0x06, "_340_A2" },
	{ 0x08, "_340_B2" },
	{ 0x09, "_344_2" },
	{ 0x0a, "_340_S" },
	{ 0x0b, "_343" },
	{ 0x0c, "_760_D" },
	{ 0x0d, "_345" },
	{    0, NULL }
};

static const struct sub_table_entry RONAN_device_types[] =
{
	{ 0x06, "_X99_MD" },
	{ 0x05, "_X96_LD" },
	{ 0x04, "_X96_W" },
	{ 0x03, "_X96_L" },
	{ 0x02, "_X96_D" },
	{    0, NULL }
};

static const struct sub_table_entry ROSEMOUNT_device_types[] =
{
	{ 0x01, "_3051" },
	{ 0x02, "_3044" },
	{ 0x03, "_1151s" },
	{ 0x04, "_8712" },
	{ 0x05, "_2001" },
	{ 0x06, "_3051C" },
	{ 0x0b, "_3001L" },
	{ 0x0c, "_8712H" },
	{ 0x0d, "_3044C" },
	{ 0x0e, "_3001C" },
	{ 0x0f, "_3051CLP" },
	{ 0x10, "_8800" },
	{ 0x11, "_3201" },
	{ 0x12, "_1152" },
	{ 0x13, "_3202" },
	{ 0x14, "_3001S" },
	{ 0x16, "_3095MV" },
	{ 0x18, "_644" },
	{ 0x19, "_3144" },
	{ 0x1a, "_3244" },
	{ 0x1c, "_APEX" },
	{ 0x1d, "_TRILOOP" },
	{ 0x1e, "_3095C" },
	{ 0x21, "_3300" },
	{ 0x23, "_2088" },
	{ 0x24, "_PROBAR" },
	{ 0x27, "_2090" },
	{ 0x28, "_PROBAR_UC" },
	{ 0x2f, "_PROPLATE_UC" },
	{ 0x36, "_1810" },
	{ 0x37, "_8712D" },
	{ 0x38, "_4600" },
	{ 0x3a, "_8800D" },
	{ 0x3b, "_248" },
	{ 0x3c, "_8732D" },
	{ 0x40, "_2088lp" },
	{ 0x42, "_951" },
	{ 0x43, "_5400" },
	{ 0x44, "_3051S_SIS" },
	{ 0x45, "_3144PSIS" },
	{ 0x47, "_4500" },
	{ 0x7f, "_HART_COMMUNICATOR" },
	{ 0x80, "_UNIVERSAL_XMTR" },
	{    0, NULL }
};

static const struct sub_table_entry PEEK_MEASUREMENT_device_types[] =
{
	{ 0x00, "_FD900" },
	{    0, NULL }
};

static const struct sub_table_entry SCHLUMBERGER_device_types[] =
{
	{ 0x01, "_NEXGEN" },
	{    0, NULL }
};

static const struct sub_table_entry SIEMENS_device_types[] =
{
	{ 0x01, "_SIEMENS_MICRO_K" },
	{ 0x02, "_SITRANS_L" },
	{ 0x03, "_SIPAN_PH" },
	{ 0x04, "_SITRANS_F" },
	{ 0x05, "_SIPAN_LF" },
	{ 0x06, "_SIPAN_O2" },
	{ 0x07, "_SITRANS_LR" },
	{ 0x10, "_SITRANS_TW" },
	{ 0x0a, "_SITRANS_P_HS" },
	{ 0x0b, "_SITRANS_P_DS" },
	{ 0x0c, "_SITRANS_P_ES" },
	{ 0x0d, "_SITRANS_P_MS" },
	{ 0x0f, "_SITRANS_T_2K" },
	{ 0x12, "_SITRANS_TK_H" },
	{ 0x13, "_SITRANS_TH300" },
	{ 0x15, "_SIPART_PS2" },
	{ 0x16, "_SITRANS_FM" },
	{ 0x18, "_MAG6000" },
	{ 0x19, "_SITRANS_F_C_MASSFLO" },
	{ 0x1a, "_IM2_TM2" },
	{ 0x1d, "_SITRANS_P300" },
	{    0, NULL }
};

static const struct sub_table_entry TOSHIBA_device_types[] =
{
	{ 0x02, "_AP3100" },
	{ 0x03, "_AP3110" },
	{ 0x04, "_AP3120" },
	{ 0x05, "_AP3140" },
	{ 0x06, "_AP3150" },
	{ 0x07, "_AP3170" },
	{ 0x08, "_AP3180" },
	{ 0x09, "_AP3190" },
	{ 0x0a, "_LF220" },
	{    0, NULL }
};

static const struct sub_table_entry ROSEMOUNT_ANALYTICAL_device_types[] =
{
	{ 0x06, "_1181pH" },
	{ 0x07, "_1181Cond" },
	{ 0x0a, "_1054ApH" },
	{ 0x0b, "_1054ACond" },
	{ 0x0c, "_OXT4000" },
	{ 0x0d, "_WC_3000" },
	{ 0x0e, "_3081FG" },
	{ 0x0f, "_OPM_2000R" },
	{ 0x10, "_OCX_4400" },
	{ 0x14, "_3081pH" },
	{ 0x15, "_3081Cond" },
	{ 0x16, "_3081_81C" },
	{ 0x17, "_3081_81T" },
	{ 0x18, "_5081A" },
	{ 0x19, "_5081C" },
	{ 0x1a, "_5081pH" },
	{ 0x1b, "_Xmt_A" },
	{ 0x1c, "_Xmt_C" },
	{ 0x1d, "_Xmt_pH" },
	{ 0x1e, "_Model_5081_FG" },
	{ 0x50, "_Model_54pH" },
	{ 0x51, "_Model_54eC" },
	{ 0x51, "_Model_54Cond" },
	{ 0x52, "_Model_54e_pH" },
	{ 0x53, "_Model_54e_Amp" },
	{    0, NULL }
};

static const struct sub_table_entry VALMET_device_types[] =
{
	{ 0x01, "_PSMART" },
	{ 0x3c, "_SMARTPULP" },
	{ 0x3d, "_SMARTLX" },
	{ 0x3e, "_MCAi" },
	{ 0x3f, "_SMARTLC" },
	{    0, NULL }
};

static const struct sub_table_entry FLOWSERVE_device_types[] =
{
	{ 0x01, "_LGX1200" },
	{ 0x02, "_KAMMMER_C2100" },
	{ 0x03, "_LGX500" },
	{ 0x03, "_LGX520" },
	{ 0x04, "_LGX3200IQ" },
	{    0, NULL }
};

static const struct sub_table_entry VIATRAN_device_types[] =
{
	{ 0x01, "_970" },
	{ 0x2E, "_IA_PRESSURE" },
	{    0, NULL }
};

static const struct sub_table_entry WEED_device_types[] =
{
	{ 0x01, "_9100H" },
	{ 0x02, "_9100D" },
	{    0, NULL }
};

static const struct sub_table_entry YAMATAKE_device_types[] =
{
	{ 0x01, "_YHWFLOW" },
	{ 0x02, "_YPRESS" },
	{ 0x03, "_YSVP" },
	{ 0x04, "_YCTEMP" },
	{ 0x05, "_YPTG" },
	{ 0x08, "_MAGNEW2W" },
	{    0, NULL }
};

static const struct sub_table_entry YOKOGAWA_device_types[] =
{
	{ 0x01, "_YEWFLO" },
	{ 0x02, "_YT200" },
	{ 0x03, "_UNICOM" },
	{ 0x04, "_EJA" },
	{ 0x05, "_ADMAG_AE" },
	{ 0x06, "_AM11" },
	{ 0x08, "_ADMAG_SE" },
	{ 0x09, "_YTA" },
	{ 0x0a, "_YTA70E" },
	{ 0x0b, "_DYF" },
	{ 0x0c, "_ZR202" },
	{ 0x0d, "_ZR402" },
	{ 0x14, "_ISC202" },
	{ 0x15, "_PH202" },
	{ 0x16, "_SC202" },
	{ 0x18, "_DO202" },
	{ 0x27, "_PH150" },
	{ 0x28, "_SC150" },
	{ 0x2b, "_PH450" },
	{ 0x2c, "_SC450" },
	{ 0x2d, "_ISC450" },
	{ 0x40, "_ROTAMASS" },
	{ 0x41, "_RAMC" },
	{ 0x42, "_RCCT_F3" },
	{ 0x50, "_AXFA11" },
	{ 0x51, "_EJX" },
	{ 0x52, "_AXFA14" },
	{ 0x53, "_AV550G" },
	{ 0x54, "_EJX_MV" },
	{    0, NULL }
};

static const struct sub_table_entry EXAC_device_types[] =
{
	{ 0xd3, "_8320" },
	{ 0xd4, "_SX5100" },
	{    0, NULL }
};

static const struct sub_table_entry KDG_MOBREY_device_types[] =
{
	{ 0x13, "_4301" },
	{ 0x14, "_3301" },
	{ 0x15, "_MSP100" },
	{ 0x29, "_MLT100" },
	{ 0x2f, "_MRL700" },
	{    0, NULL }
};

static const struct sub_table_entry PRINCO_device_types[] =
{
	{ 0x01, "_L4610" },
	{    0, NULL }
};

static const struct sub_table_entry SMAR_device_types[] =
{
	{ 0x01, "_LD301" },
	{ 0x02, "_TT301" },
	{ 0x03, "_FY301" },
	{ 0x04, "_LD291" },
	{ 0x05, "_TP301" },
	{ 0x06, "_DT301" },
	{    0, NULL }
};

static const struct sub_table_entry ECKARDT_device_types[] =
{
	{ 0x01, "_TSV175" },
	{ 0x02, "_DMU_130" },
	{ 0x03, "_TI20" },
	{ 0x04, "_SRD991" },
	{ 0x05, "_DMU_140" },
	{ 0x06, "_SRD960" },
	{    0, NULL }
};

static const struct sub_table_entry SAMSON_device_types[] =
{
	{ 0xef, "_3730" },
	{ 0xf9, "_3780" },
	{    0, NULL }
};

static const struct sub_table_entry SPARLING_INSTRUMENTS_device_types[] =
{
	{ 0xee, "_626" },
	{    0, NULL }
};

static const struct sub_table_entry KROHNE_device_types[] =
{

	{ 0xe3, "_IFC300" },
	{ 0xe4, "_OPTIFLEX1300C" },
	{ 0xe5, "_OPTIWAVE7300C" },
	{ 0xe6, "_M8E" },
	{ 0xe7, "_UFC030" },
	{ 0xe8, "_MFC050" },
	{ 0xe9, "_IFC040" },
	{ 0xea, "_M10" },
	{ 0xec, "_BM102" },
	{ 0xed, "_VFM31" },
	{ 0xee, "_BM100" },
	{ 0xf2, "_ESKII" },
	{ 0xf3, "_IFC110" },
	{ 0xf4, "_IFC090" },
	{ 0xf5, "_UFC500" },
	{ 0xf6, "_IFC010" },
	{ 0xf7, "_MFC08x" },
	{ 0xf8, "_IFC080" },
	{ 0xf9, "_BM70" },
	{    0, NULL }
};

static const struct sub_table_entry DRUCK_device_types[] =
{
	{ 0xef, "_RTX1000H_A" },
	{ 0xec, "_RTX1000H_SG" },
	{ 0xe9, "_RTX1000H_G" },
	{    0, NULL }
};

static const struct sub_table_entry WESTLOCK_CONTROLS_device_types[] =
{
	{ 0x01, "_ICOT" },
	{ 0x02, "_SMARTCAL" },
	{ 0x03, "_DEPIC" },
	{ 0x04, "_DEPIC_ESD" },
	{    0, NULL }
};

static const struct sub_table_entry DREXELBROOK_device_types[] =
{
	{ 0x02, "_UNIVERSAL_III" },
	{    0, NULL }
};

static const struct sub_table_entry SAAB_TANK_CONTROL_device_types[] =
{
	{ 0xed, "_5600" },
	{ 0xEF, "_TR_PRO" },
	{    0, NULL }
};

static const struct sub_table_entry DRAEGER_device_types[] =
{
	{ 0x01, "_TEST" },
	{ 0xf7, "_POLYTRON2" },
	{ 0xed, "_POLYTRON2_IR" },
	{ 0xeb, "_POLYTRON3" },
	{ 0xea, "_POLYTRON_PULSAR" },
	{    0, NULL }
};

static const struct sub_table_entry SIEMENS_MILLTRONICS_P_I_device_types[] =
{
	{ 0xf8, "_MST2002" },
	{ 0xf9, "_MST9500" },
	{    0, NULL }
};

static const struct sub_table_entry MAGNETROL_device_types[] =
{
	{ 0xe5, "_MODEL_705_3X" },
	{ 0xe6, "_MODEL_RX5" },
	{ 0xe7, "_MODEL_704" },
	{ 0xe8, "_JUPITER" },
	{ 0xe9, "_MODEL_TA2" },
	{ 0xea, "_MODEL_707" },
	{ 0xeb, "_MODEL_705_2X" },
	{ 0xec, "_MODEL_708" },
	{ 0xed, "_MODEL_805" },
	{ 0xee, "_MODEL_705" },
	{ 0xef, "_SMARTEZ" },
	{    0, NULL }
};

static const struct sub_table_entry NELES_JAMESBURY_device_types[] =
{
	{ 0xeb, "_ValvGuard" },
	{ 0xee, "_ND800_PT" },
	{ 0xef, "_ND800" },
	{    0, NULL }
};

static const struct sub_table_entry MILLTRONICS_device_types[] =
{
	{ 0xc8, "_86x_PROBE" },
	{ 0xc9, "_IQ300_M" },
	{ 0xca, "_LR200" },
	{ 0xcc, "_SITRANS_LR_400" },
	{ 0xcd, "_PRBLU6" },
	{ 0xce, "_PRBLU12" },
	{ 0xcf, "_PRBLR" },
	{    0, NULL }
};

static const struct sub_table_entry ANDERSON_device_types[] =
{
 	{ 0xc8, "_ANDRSN1" },
	{    0, NULL }
};

static const struct sub_table_entry INOR_device_types[] =
{
	{ 0xef, "_MESO" },
	{    0, NULL }
};

static const struct sub_table_entry ROBERTSHAW_device_types[] =
{
	{ 0xC8, "_EXCALIBUR_7000" },
	{    0, NULL }
};

static const struct sub_table_entry PEPPERL_FUCHS_device_types[] =
{
	{ 0xe8, "_CorrTran" },
	{    0, NULL }
};

static const struct sub_table_entry ACCUTECH_device_types[] =
{
	{ 0xee, "_VR1500" },
	{ 0xef, "_AI1500" },
	{    0, NULL }
};

static const struct sub_table_entry KAMSTRUP_device_types[] =
{
	{ 0xed, "_FLEXBAR_CER" },
	{ 0xee, "_FLEXBAR_HRT" },
	{ 0xef, "_FLEXTOP_HRT" },
	{    0, NULL }
};

static const struct sub_table_entry KNICK_device_types[] =
{
	{ 0xe8, "_2211CondI" },
	{ 0xe9, "_2211_OXY" },
	{ 0xea, "_2211Cond" },
	{ 0xeb, "_2211PH" },
	{    0, NULL }
};

static const struct sub_table_entry MTS_SYSTEMS_device_types[] =
{
	{ 0xed, "_M_Series" },
	{    0, NULL }
};

static const struct sub_table_entry OVAL_device_types[] =
{
	{ 0x64, "_100" },
	{ 0x65, "_101" },
	{ 0x6e, "_CT9401" },
	{ 0x6f, "_ST9801" },
	{ 0x70, "_EV9201" },
	{ 0x7b, "_MT9431" },
	{    0, NULL }
};

static const struct sub_table_entry MASONEILAN_device_types[] =
{
	{ 0x64, "_HDLT" },
	{ 0xc8, "_SVI" },
	{ 0xc9, "_SVI2" },
	{    0, NULL }
};

static const struct sub_table_entry BESTA_device_types[] =
{
	{ 0xef, "_CLS420" },
	{    0, NULL }
};

static const struct sub_table_entry OHMART_device_types[] =
{
	{ 0xE0, "_XMTR" },
	{ 0xE1, "_XMTR_COMP" },
	{    0, NULL }
};

static const struct sub_table_entry HAROLD_BECK_AND_SONS_device_types[] =
{
	{ 0x01, "_ESR_D" },
	{ 0x0a, "_DCM" },
	{ 0xef, "_239" },
	{    0, NULL }
};

static const struct sub_table_entry WIKA_device_types[] =
{
	{ 0xef, "_T32H" },
	{ 0xee, "_UNITRANS" },
	{    0, NULL }
};

static const struct sub_table_entry BOPP_AND_REUTHER_HEINRICHS_device_types[] =
{
	{ 0xeb, "_UMC2" },
	{ 0xec, "_ES" },
	{ 0xed, "_VTX" },
	{ 0xee, "_DIMF" },
	{ 0xef, "_UST_01" },
	{    0, NULL }
};

static const struct sub_table_entry PR_ELECTRONICS_device_types[] =
{
	{ 0xee, "_PR6335" },
	{ 0xef, "_PR5335" },
	{    0, NULL }
};

static const struct sub_table_entry APPARATEBAU_HUNDSBACH_device_types[] =
{
	{ 0xee, "_MT115" },
	{    0, NULL }
};

static const struct sub_table_entry DYNISCO_device_types[] =
{
	{ 0xde, "_SPX" },
	{ 0xdf, "_IPXII" },
	{ 0xe0, "_IPX" },
	{    0, NULL }
};

static const struct sub_table_entry SPRIANO_device_types[] =
{
	{ 0xec, "_SST70" },
	{    0, NULL }
};

static const struct sub_table_entry DMC_device_types[] =
{
	{ 0xEF, "_RMSA" },
	{    0, NULL }
};

static const struct sub_table_entry KLAY_INSTRUMENTS_device_types[] =
{
	{ 0xEF, "_SERIES_2000" },
	{    0, NULL }
};

static const struct sub_table_entry BURKERT_device_types[] =
{
	{ 0xef, "_8635" },
	{ 0xef, "_8630" },
	{    0, NULL }
};

static const struct sub_table_entry GLI_device_types[] =
{
	{ 0xEE, "_M33_53P" },
	{ 0xEF, "_M33_53C" },
	{    0, NULL }
};

static const struct sub_table_entry PMC_device_types[] =
{
	{ 0xe0, "_SMT_EL" },
	{    0, NULL }
};

static const struct sub_table_entry LABOM_device_types[] =
{
	{ 0x3c, "_PASCAL_CI" },
	{ 0xe0, "_IPX" },
	{    0, NULL }
};

static const struct sub_table_entry DANFOSS_device_types[] =
{
	{ 0x03, "_MAGFLO" },
	{    0, NULL }
};

static const struct sub_table_entry TURBO_device_types[] =
{
	{ 0x7f, "_INTERMAG_TRANSMAG" },
	{    0, NULL }
};

static const struct sub_table_entry TOKYO_KEISO_device_types[] =
{
	{ 0x7f, "_FST3" },
	{ 0x7b, "_FW9" },
	{    0, NULL }
};

static const struct sub_table_entry SMC_device_types[] =
{
	{ 0x7e, "_IP8101" },
	{ 0xef, "_F793_E701" },
	{    0, NULL }
};

static const struct sub_table_entry STATUS_INSTRUMENTS_device_types[] =
{
	{ 0xef, "_SEM300" },
	{    0, NULL }
};

static const struct sub_table_entry HUAKONG_device_types[] =
{
	{ 0x7f, "_HK_TT01" },
	{    0, NULL }
};

static const struct sub_table_entry VORTEK_device_types[] =
{
	{ 0x01, "_INNOVAMASS" },
	{    0, NULL }
};

static const struct sub_table_entry ACTION_INSTRUMENTS_device_types[] =
{
	{ 0xef, "_T798" },
	{    0, NULL }
};

static const struct sub_table_entry MAGTECH_device_types[] =
{
	{ 0x88, "_LTM_100" },
	{ 0x89, "_LTM_300" },
	{    0, NULL }
};

static const struct sub_table_entry RUEGER_device_types[] =
{
	{ 0x01, "_S95_HRT" },
	{ 0x7f, "_S95_3U02" },
	{    0, NULL }
};

static const struct sub_table_entry METTLER_TOLEDO_device_types[] =
{
	{ 0x78, "_CI7100" },
	{ 0x79, "_O2_4100" },
	{ 0x7a, "_C7100" },
	{ 0x7b, "_PH2100" },
	{    0, NULL }
};

static const struct sub_table_entry DETECTOR_ELECTRONICS_device_types[] =
{
	{ 0x7c, "_X3301" },
	{ 0x7e, "_OPECL_RX" },
	{ 0x7f, "_ECLIPSE" },
	{    0, NULL }
};

static const struct sub_table_entry THERMO_MEASURE_TECH_device_types[] =
{
	{ 0x7D, "_NDMi" },
	{ 0x7E, "_RCMi" },
	{ 0x7f, "_NCMi" },
	{    0, NULL }
};

static const struct sub_table_entry DEZURIK_device_types[] =
{
	{ 0xEF, "_POSITIONER" },
	{    0, NULL }
};

static const struct sub_table_entry PHASE_DYNAMICS_device_types[] =
{
	{ 0x7f, "_ANALYZER" },
	{    0, NULL }
};

static const struct sub_table_entry WELLTECH_device_types[] =
{
	{ 0x7C, "_WT_2000" },
	{    0, NULL }
};

static const struct sub_table_entry MILTON_ROY_device_types[] =
{
  	{ 0x7f, "_VPOS" },
	{    0, NULL }
};

static const struct sub_table_entry PMV_device_types[] =
{
	{ 0xd3, "_D3" },
	{    0, NULL }
};

static const struct sub_table_entry TURCK_device_types[] =
{
	{ 0x01, "_KMU_HLI" },
	{    0, NULL }
};

static const struct sub_table_entry PANAMETRICS_device_types[] =
{
	{ 0x70, "_UFLOW" },
	{    0, NULL }
};

static const struct sub_table_entry ATI_device_types[] =
{
	{ 0x7f, "_D12" },
	{    0, NULL } 
};

static const struct sub_table_entry FINT_device_types[] =
{
	{ 0x02, "_PIR2000" },
	{ 0xed, "_OXITEC5000" },
	{    0, NULL }
};

static const struct sub_table_entry BERTHOLD_device_types[] =
{
	{ 0x7E, "_LB460" },
	{ 0x7F, "_LB490" },
	{ 0x7d, "_LB491" },
	{    0, NULL }
};

static const struct sub_table_entry INTERCORR_device_types[] =
{
	{ 0xEF, "_SMARTCET18" },
	{    0, NULL }
};

static const struct sub_table_entry FLUID_COMPONENTS_device_types[] =
{
	{ 0x78, "_FCI_FLOW" },
	{    0, NULL }
};

static const struct sub_table_entry INVENSYS_device_types[] =
{
	{ 0x2E, "_IA_PRESSURE" },
	{    0, NULL }
};

static const struct sub_table_entry TYCO_VALVES_CONTROLS_device_types[] =
{
	{ 0x01, "_SMARTCAL" },
	{    0, NULL }
};

static const struct sub_table_entry J_Tec_device_types[] =
{
	{ 0x00, "_SERIES_0" },
	{    0, NULL }
};
static const struct sub_table_entry SYNETIX_device_types[] =
{
	{ 0x7f, "_PRI150" },
	{    0, NULL }
};
static const struct sub_table_entry PHOENIX_CONTACT_device_types[] =
{
	{ 0x01, "_MCR_FL_TS_LP_I_Ex" },
	{ 0x02, "_MCR_FL_HT_TS_I_Ex" },
	{    0, NULL }
};

static const struct sub_table_entry American_Level_Instruments_device_types[] =
{
	{ 0x7f, "_Model_32E" },
	{    0, NULL }
};

static const struct sub_table_entry PYROMATION_INC_device_types[] =
{
	{ 0x7f, "_SERIES442" },
	{ 0x80, "_SERIES642" },
	{    0, NULL }
};

static const struct sub_table_entry SATRON_device_types[] =
{
	{ 0x7f, "_VSERIES" },
	{    0, NULL }
};

static const struct sub_table_entry BADGERMETER_device_types[] =
{
	{ 0x01, "_PRIMO" },
	{    0, NULL }
};

static const struct sub_table_entry GP50_device_types[] =
{
	{ 0x7c, "_GP50400SERIES" },
	{    0, NULL }
};

static const struct sub_table_entry KONGSBERG_MARITIME_device_types[] =
{
	{ 0xef, "_GT400" },
	{    0, NULL }
};

static const struct sub_table_entry HENGESBACH_device_types[] =
{
	{ 0x21, "_TE52" },
	{    0, NULL }
};

static const struct sub_table_entry LANLIAN_INSTRUMENTS_device_types[] =
{
	{ 0x74, "_LSIII" },
	{    0, NULL }
};

static const struct sub_table_entry KPM_device_types[] =
{
	{ 0x81, "_KC5" },
	{ 0x80, "_KC3" },
	{    0, NULL }
};      

static const struct sub_table_entry SICK_MAIHAK_device_types[] =
{
	{ 0x80, "_FL600" },
	{    0, NULL }
};

static const struct sub_table_entry SPIRAX_device_types[] =
{
	{ 0x80, "_SP301" },
	{    0, NULL }
};

static const struct sub_table_entry ROOST_device_types[] =
{
	{ 0x80, "_EPP300" },
	{    0, NULL } 
};      

static const struct sub_table_entry KOSO_device_types[] =
{
	{ 0x80, "_POS_0" },
	{    0, NULL }
};

static const struct sub_table_entry HCF_device_types[] =
{
	{ 0xea, "_XMTR_MV" },
	{ 0xeb, "_STD" },
	{ 0xec, "_SAMPLE4" },
	{ 0xed, "_SAMPLE3" },
	{ 0xee, "_SAMPLE2" },
	{ 0xef, "_SAMPLE1" },
	{    0, NULL }
};


struct table_entry
{
	unsigned short id;
	const char *define;
	const char *name;
	const struct sub_table_entry *device_defines;
};

static const struct table_entry manufacturer_ids[] =
{
/* HCF assigned */
{     0, "STANDARD",			"Standard",			STANDARD_device_types			},
{     1, NULL,				"Acromag",			NULL					},
{     2, NULL,				"Allen Bradley",		NULL					},
{     3, "AMETEK",			"Ametek",			AMETEK_device_types			},
{     4, NULL,				"Analog Devices",		NULL					},
{     5, NULL,				"Elsag Bailey",			NULL					},
{     6, NULL,				"Beckman",			NULL					},
{     7, NULL,				"Bell Microsensor",		NULL					},
{     8, NULL,				"Bourns",			NULL					},
{     9, NULL,				"Bristol Babcock",		NULL					},
{    10, "BROOKS_INSTRUMENT",		"Brooks Instrument",		BROOKS_INSTRUMENT_device_types		},
{    11, NULL,				"Chessell",			NULL					},
{    12, NULL,				"Combustion Engineering",	NULL					},
{    13, NULL,				"Daniel Industries",		NULL					},
{    14, "DELTA",			"Delta",			DELTA_device_types			},
{    15, NULL,				"Dieterich Standard",		NULL					},
{    16, NULL,				"Dohrmann",			NULL					},
{    17, "ENDRESS_HAUSER",		"Endress & Hauser",		ENDRESS_HAUSER_device_types		},
{    18, "BAILEY_FISCHER_AND_PORTER",	"Fisher & Porter",		BAILEY_FISCHER_AND_PORTER_device_types	},
{    19, "FISHER_CONTROLS",		"Fisher Controls",		FISHER_CONTROLS_device_types		},
{    20, "FOXBORO",			"Foxboro Eckardt",		FOXBORO_device_types			},
{    21, "FUJI",			"Fuji",				FUJI_device_types			},
{    22, "HARTMANN_BRAUN",		"Hartmann & Braun",		HARTMANN_BRAUN_device_types		},
{    23, "HONEYWELL",			"Honeywell",			HONEYWELL_device_types			},
{    24, NULL,				"ITT Barton",			NULL					},
{    25, "KAY_RAY_SENSALL",		"Kay Ray/Sensall",		KAY_RAY_SENSALL_device_types		},
{    26, "ABB",				"ABB Automation",		ABB_device_types			},
{    27, NULL,				"Leeds & Northrup",		NULL					},
{    28, NULL,				"Leslie",			NULL					},
{    29, "M_SYSTEM",			"M-System Co.",			M_SYSTEM_device_types			},
{    30, NULL,				"Measurex",			NULL					},
{    31, "MICRO_MOTION",		"Micro Motion",			MICRO_MOTION_device_types		},
{    32, "MOORE_INDUSTRIES",		"Moore Industries",		MOORE_INDUSTRIES_device_types		},
{    33, "MOORE_PRODUCTS",		"Moore Products",		MOORE_PRODUCTS_device_types		},
{    34, NULL,				"Ohkura Electric",		NULL					},
{    35, NULL,				"Paine",			NULL					},
{    36, NULL,				"Rochester Instrument Systems",	NULL					},
{    37, "RONAN",			"Ronan",			RONAN_device_types			},
{    38, "ROSEMOUNT",			"Rosemount",			ROSEMOUNT_device_types			},
{    39, "PEEK_MEASUREMENT",		"Peek Measurement",		PEEK_MEASUREMENT_device_types		},
{    40, "SCHLUMBERGER",		"Schlumberger",			SCHLUMBERGER_device_types		},
{    41, NULL,				"Sensall",			NULL					},
{    42, "SIEMENS",			"SIEMENS AG",			SIEMENS_device_types			},
{    43, NULL,				"Camille Bauer Weed",		NULL					},
{    44, "TOSHIBA",			"Toshiba Corp.",		TOSHIBA_device_types			},
{    45, NULL,				"Transmation",			NULL					},
{    46, "ROSEMOUNT_ANALYTICAL",	"Rosemount Analytic",		ROSEMOUNT_ANALYTICAL_device_types	},
{    47, "VALMET",			"Valmet",			VALMET_device_types			},
{    48, "FLOWSERVE",			"Valtek Flowserve",		FLOWSERVE_device_types			},
{    49, NULL,				"Varec",			NULL					},
{    50, "VIATRAN",			"Viatran",			VIATRAN_device_types			},
{    51, "WEED",			"Weed",				WEED_device_types			},
{    52, NULL,				"Westinghouse",			NULL					},
{    53, NULL,				"Xomox",			NULL					},
{    54, "YAMATAKE",			"Yamatake",			YAMATAKE_device_types			},
{    55, "YOKOGAWA",			"Yokogawa",			YOKOGAWA_device_types			},
{    56, NULL,				"Nuovo Pignone",		NULL					},
{    57, NULL,				"Promac",			NULL					},
{    58, "EXAC",			"Exac Corporation",		EXAC_device_types			},
{    59, "KDG_MOBREY",			"KDG Mobrey",			KDG_MOBREY_device_types			},
{    60, NULL,				"Arcom Control Systems",	NULL					},
{    61, "PRINCO",			"Princo",			PRINCO_device_types			},
{    62, "SMAR",			"SMAR Me�- und Regeltechnik GmbH", SMAR_device_types			},
{    63, "ECKARDT",			"Foxboro Eckardt",		ECKARDT_device_types			},
{    64, NULL,				"Measurement Technology",	NULL					},
{    65, NULL,				"Applied System Technologies",	NULL					},
{    66, "SAMSON",			"SAMSON AG",			SAMSON_device_types			},
{    67, "SPARLING_INSTRUMENTS",	"Sparling Instruments",		SPARLING_INSTRUMENTS_device_types	},
{    68, NULL,				"Fireye",			NULL					},
{    69, "KROHNE",			"KROHNE Messtechnik GmbH & Co.KG", KROHNE_device_types			},
{    70, NULL,				"Betz",				NULL					},
{    71, "DRUCK",			"Druck",			DRUCK_device_types			},
{    72, NULL,				"SOR",				NULL					},
{    73, NULL,				"Elcon Instruments",		NULL					},
{    74, NULL,				"EMCO",				NULL					},
{    75, NULL,				"Termiflex Corporation",	NULL					},
{    76, NULL,				"VAF Instruments",		NULL					},
{    77, "WESTLOCK_CONTROLS",		"Westlock Controls",		WESTLOCK_CONTROLS_device_types		},
{    78, "DREXELBROOK",			"Drexelbrook",			DREXELBROOK_device_types		},
{    79, "SAAB_TANK_CONTROL",		"Saab Tank Control",		SAAB_TANK_CONTROL_device_types		},
{    80, NULL,				"K-TEK",			NULL					},
{    81, NULL,				"Flowdata",			NULL					},
{    82, "DRAEGER",			"Draeger",			DRAEGER_device_types			},
{    83, NULL,				"Raytek",			NULL					},
{    84, "SIEMENS_MILLTRONICS_P_I",	"SIEMENS Milltronics",		SIEMENS_MILLTRONICS_P_I_device_types	},
{    85, NULL,				"BTG Engineering B.V.",		NULL					},
{    86, "MAGNETROL",			"Magnetrol",			MAGNETROL_device_types			},
{    87, "NELES_JAMESBURY",		"Neles Controls",		NELES_JAMESBURY_device_types		},
{    88, "MILLTRONICS",			"SIEMENS Milltronics",		MILLTRONICS_device_types		},
{    89, NULL,				"HELIOS",			NULL					},
{    90, "ANDERSON",			"Anderson Instrument Company",	ANDERSON_device_types			},
{    91, "INOR",			"INOR AB",			INOR_device_types			},
{    92, "ROBERTSHAW",			"ROBERTSHAW",			ROBERTSHAW_device_types			},
{    93, "PEPPERL_FUCHS",		"PEPPERL+FUCHS GmbH",		PEPPERL_FUCHS_device_types		},
{    94, "ACCUTECH",			"ACCUTECH",			ACCUTECH_device_types			},
{    95, NULL,				"Flow Measurement",		NULL					},
{    96, "KAMSTRUP",			"KAMSTRUP",			KAMSTRUP_device_types			},
{    97, "KNICK",			"Knick",			KNICK_device_types			},
{    98, NULL,				"VEGA Grieshaber KG",		NULL					},
{    99, "MTS_SYSTEMS",			"MTS Systems Corp.",		MTS_SYSTEMS_device_types		},
{   100, "OVAL",			"Oval",				OVAL_device_types			},
{   101, "MASONEILAN",			"Masoneilan-Dresser",		MASONEILAN_device_types			},
{   102, "BESTA",			"BESTA",			BESTA_device_types			},
{   103, "OHMART",			"Ohmart",			OHMART_device_types			},
{   104, "HAROLD_BECK_AND_SONS",	"Harold Beck and Sons",		HAROLD_BECK_AND_SONS_device_types	},
{   105, NULL,				"rittmeyer instrumentation",	NULL					},
{   106, "ROESSEL",			"R�ssel Messtechnik",		NULL					},
{   107, "WIKA",			"WIKA",				WIKA_device_types			},
{   108, "BOPP_AND_REUTHER_HEINRICHS",	"Bopp & Reuther Heinrichs",	BOPP_AND_REUTHER_HEINRICHS_device_types	},
{   109, "PR_ELECTRONICS",		"PR Electronics",		PR_ELECTRONICS_device_types		},
{   110, NULL,				"Jordan Controls",		NULL					},
{   111, NULL,				"Valcom s.r.l.",		NULL					},
{   112, NULL,				"US ELECTRIC MOTORS",		NULL					},
{   113, "APPARATEBAU_HUNDSBACH",	"Apparatebau Hundsbach",	APPARATEBAU_HUNDSBACH_device_types	},
{   114, "DYNISCO",			"Dynisco",			DYNISCO_device_types			},
{   115, "SPRIANO",			"Spriano",			SPRIANO_device_types			},
{   116, "DMC",				"DMC",				DMC_device_types			},
{   117, "KLAY_INSTRUMENTS",		"Klay Instruments",		KLAY_INSTRUMENTS_device_types		},
{   118, NULL,				"Action Instruments",		NULL					},
{   119, NULL,				"MMG Automatiky DTR",		NULL					},
{   120, "BURKERT",			"B�rkert",			BURKERT_device_types			},
{   121, NULL,				"Hersey Measurement",		NULL					},
{   122, NULL,				"PONDUS INSTRUMENTS",		NULL					},
{   123, NULL,				"ZAP S.A. Ostrow Wielkopolski",	NULL					},
{   124, "GLI",				"GLI",				GLI_device_types			},
{   125, NULL,				"Fisher-Rosemount Performance Technologies", NULL			},
{   126, "PMC",				"Paper Machine Components",	PMC_device_types			},
{   127, "LABOM",			"Labom Mess- und Regeltechnik GmbH", LABOM_device_types			},
{   128, "DANFOSS",			"Danfoss Antriebs-und Regeltechnik GmbH", DANFOSS_device_types		},
{   129, "TURBO", 			"TURBO-WERK",			TURBO_device_types			},
{   130, "TOKYO_KEISO",			"TOKYO KEISO CO., LTD.",	TOKYO_KEISO_device_types		},
{   131, "SMC",				"SMC Pneumatik GmbH",		SMC_device_types			},
{   132, "STATUS_INSTRUMENTS",		"Status Instruments",		STATUS_INSTRUMENTS_device_types		},
{   133, "HUAKONG",			"HUAKONG",			HUAKONG_device_types			},
{   134, NULL,				"Duon System",			NULL					},
{   135, "VORTEK",			"Vortek Instruments LLC",	VORTEK_device_types			},
{   136, NULL,				"AG Crosby",			NULL					},
{   137, "ACTION_INSTRUMENTS",		"Action Instruments",		ACTION_INSTRUMENTS_device_types		},
{   138, NULL,				"Keystone Controls",		NULL					},
{   139, NULL,				"Thermo Electric Co.",		NULL					},
{   140, "MAGTECH",			"ISE-Magtech",			MAGTECH_device_types			},
{   141, "RUEGER",			"Rueger",			RUEGER_device_types			},
{   142, "METTLER_TOLEDO",		"Mettler Toledo GmbH",		METTLER_TOLEDO_device_types		},
{   143, "DETECTOR_ELECTRONICS",	"Det-Tronics",			DETECTOR_ELECTRONICS_device_types	},
{   144, "THERMO_MEASURE_TECH",		"TN Technologies",		THERMO_MEASURE_TECH_device_types	},
{   145, "DEZURIK",			"DeZURIK",			DEZURIK_device_types			},
{   146, "PHASE_DYNAMICS",		"Phase Dynamics",		PHASE_DYNAMICS_device_types		},
{   147, "WELLTECH",			"WELLTECH SHANGHAI",		WELLTECH_device_types			},
{   148, NULL,				"ENRAF",			NULL					},
{   149, NULL,				"4tech ASA",			NULL					},
{   150, NULL,				"Brandt Instruments",		NULL					},
{   151, NULL,				"Nivelco",			NULL					},
{   152, NULL,				"Camille Bauer",		NULL					},
{   153, NULL,				"Metran",			NULL					},
{   154, "MILTON_ROY",			"Milton Roy Co.",		MILTON_ROY_device_types			},
{   155, "PMV",				"PMV",				PMV_device_types			},
{   156, "TURCK",			"Turck (Tianjin) Sensor Co., Ltd.", TURCK_device_types			},
{   157, "PANAMETRICS",			"Panametrics",			PANAMETRICS_device_types		},
{   158, "STAHL",			"Stahl",			NULL					},
{   159, "ATI",				"Analytical Technology, Inc",	ATI_device_types			},
{   160, "FINT",			"Fieldbus International",	FINT_device_types			},
{   161, "BERTHOLD",			"Berthold Technologies GmbH & Co.KG", BERTHOLD_device_types		},
{   162, "INTERCORR",			"INTERCORR",			INTERCORR_device_types			},
{   166, "FLUID_COMPONENTS",		"FLUID_COMPONENTS",		FLUID_COMPONENTS_device_types		},
{   169, "INVENSYS",			"Invensys",			INVENSYS_device_types			},
{   171, "TYCO_VALVES_CONTROLS",	"TycoValveControls",		TYCO_VALVES_CONTROLS_device_types	},
{   173, "J_Tec",			"J_Tec",			J_Tec_device_types			},
{   174, "SYNETIX",			"SYNETIX",			SYNETIX_device_types			},
{   176, "PHOENIX_CONTACT",		"Phoenix Contact GmbH & Co. KG", PHOENIX_CONTACT_device_types		},
{   178, "American_Level_Instruments",	"American_Level_Instruments",	American_Level_Instruments_device_types	},
{   181, "PYROMATION_INC",		"PYROMATION_INC",		PYROMATION_INC_device_types		},
{   182, "SATRON",			"SATRON",			SATRON_device_types			},
{   188, NULL,				"APLISENS SP. Z O.O.",		NULL					},
{   189, "BADGERMETER",			"BADGERMETER",			BADGERMETER_device_types		},
{   191, "GP50",			"GP50",				GP50_device_types			},
{   192, "KONGSBERG_MARITIME",		"KONGSBERG_MARITIME",		KONGSBERG_MARITIME_device_types		},
{   194, "HENGESBACH",			"HENGESBACH",			HENGESBACH_device_types			},
{   195, "LANLIAN_INSTRUMENTS",		"Lanlian Instruments",		LANLIAN_INSTRUMENTS_device_types	},
{   197, "KPM",				"KPM",				KPM_device_types			},
{   199, "SICK_MAIHAK",			"SICK_MAIHAK",			SICK_MAIHAK_device_types		},
{   202, "SPIRAX",			"SPIRAX",			SPIRAX_device_types			},
{   204, "TECFLUID",			"Tecfluid S.A.",		NULL					},
{   206, "ROOST",			"ROOST",			ROOST_device_types			},
{   207, "KOSO",			"KOSO",				KOSO_device_types			},
{   212, "SIC",				"ChongQing Sichuan Instruments Co., Ltd.", NULL				},
{   213, "HACH_LANGE",			"HACH LANGE united for water quality", NULL				},
{   239, "HCF",				"HCF",				HCF_device_types			},
{   250, "NOT_USED",			"not used",			NULL					},
{   251, "NONE",			"none",				NULL					},
{   252, "UNKNOWN",			"unknown",			NULL					},
{   253, "SPECIAL",			"special",			NULL					},
{   254, NULL,				"reserved",			NULL					},
{   255, NULL,				"reserved",			NULL					},
/* PNO assigned */
{   256, NULL,				"R�eger",							NULL	},
{   257, NULL,				"Sick AG",							NULL	},
{   258, NULL,				"ASCO JOUCOMATIC GmbH & Co.",					NULL	},
{   259, "BOSCHREXROTH",		"Bosch Rexroth AG",						NULL	},
{   260, NULL,				"ESR Pollmeier",						NULL	},
{   261, NULL,				"Kollmorgen Seidel",						NULL	},
{   262, NULL,				"Lenze",							NULL	},
{   263, NULL,				"Loher",							NULL	},
{   264, "MITSUBISHI",			"Mitsubishi Electric Europe B.V. Niederlassung Deutschland",	NULL	},
{   265, "MOELLER",			"Moeller GmbH",							NULL	},
{   266, "SEW_EURODRIVE",		"SEW-EURODRIVE GmbH & Co.",					NULL	},
{   267, NULL,				"St�ber",							NULL	},
{   268, NULL,				"HMS Industrial Networks GmbH",					NULL	},
{   269, NULL,				"Z�llig",							NULL	},
{   270, "BARTEC",			"BARTEC GmbH",							NULL	},
{   271, NULL,				"Parker Hannifin Compumotor",					NULL	},
{   272, NULL,				"Fraba Posital",						NULL	},
{   273, NULL,				"YASKAWA Electric GmbH",					NULL	},
{   274, NULL,				"Baum�ller N�rnberg El.GmbhCo",					NULL	},
{   279, NULL,				"SOFTING AG",							NULL	},
{   281, NULL,				"Elektro-Elektronik Pranjic",					NULL	},
{   282, NULL,				"Eurotherm Automation",						NULL	},
{   283, NULL,				"Dr. Bruno Lange GmbH & Co. KG",				NULL	},
{   284, NULL,				"JH Group",							NULL	},
{   285, NULL,				"WAGO Kontakttechnik",						NULL	},
{   286, NULL,				"Hilscher Gesellschaft f�r Systemautomation mbH",		NULL	},
{   287, NULL,				"Indramat",							NULL	},
{   288, NULL,				"Beckhoff Industrie Elektronik",				NULL	},
{   289, NULL,				"Bihl+Wiedemann GmbH",						NULL	},
{   291, NULL,				"Swan Analytical Instruments",					NULL	},
{   292, NULL,				"Deutschmann Automation GmbH & Co. KG",				NULL	},
{   293, "ROTORK",			"Rotork Controls Ltd.",						NULL	},
{   294, NULL,				"Leuze Lumiflex GmbH & Co. KG",					NULL	},
{   295, NULL,				"Woodhead Software & Electronics",				NULL	},
{   296, NULL,				"Berger Lahr",							NULL	},
{   297, NULL,				"Schneider Electric",						NULL	},
{   298, NULL,				"IVO GmbH & Co. KG",						NULL	},
{   299, "TURCK",			"Werner Turck GmbH & Co. KG",					NULL	},
{   300, NULL,				"G. Bachmann Electronic GmbH",					NULL	},
{   301, NULL,				"HORIBA STEC, Co. Ltd.",					NULL	},
{   302, NULL,				"Maschinenfabrik Spaichingen",					NULL	},
{   303, NULL,				"Murrelektronik GmbH",						NULL	},
{   304, NULL,				"Hirschmann Automation and Control GmbH",			NULL	},
{   305, "DREHMO",			"Drehmo GmbH",							NULL	},
{   306, NULL,				"AGILiCOM",							NULL	},
{   307, NULL,				"L. Bernard AS",						NULL	},
{   310, "IFM",				"ifm electronic GmbH",						NULL	},
{   333, NULL,				"Festo AG &Co. KG",						NULL	},
{   444, NULL,				"profichip GmbH",						NULL	},
{   555, NULL,				"VIPA GmbH",							NULL	},
{   777, NULL,				"ifak e.V. Magdeburg",						NULL	},
{  1488, "BEKA",			"BEKA associates Ltd.",						NULL	},
{  2403, NULL,				"Prozesskommunikation TU Dresden",				NULL	},
{  7556, "ELOMATIC",			"Elomatic",							NULL	},
{  8888, NULL,				"ifak system",							NULL	},
{ 65535, NULL,				"PNO",								NULL	}
/* end */
};
const size_t manufacturer_ids_max = sizeof(manufacturer_ids) / sizeof(manufacturer_ids[0]);

struct manufacturer_www_entry
{
	unsigned short id;
	const char *www;
};

static const struct manufacturer_www_entry manufacturer_wwws[] =
{
/* www addresses */
{    42, "www.automation.siemens.com"		},
{    84, "www.automation.siemens.com"		},
{    88, "www.automation.siemens.com"		},
{   109, "www.prelectronics.com"		},
{   127, "www.labom.de"				},
{   239, "www.hartcomm.org"			},
{   310, "www.ifm-electronic.com"		},
{   777, "www.ifak.eu"				},
{  8888, "www.ifak-system.com"			},
{ 65535, "www.profibus.com"			}
};
const size_t manufacturer_wwws_max = sizeof(manufacturer_wwws) / sizeof(manufacturer_wwws[0]);

static const struct table_entry *
static_lookup_manufacturer_entry(unsigned int id) throw()
{
	const struct table_entry *lower = manufacturer_ids;
	const struct table_entry *upper = manufacturer_ids + (manufacturer_ids_max - 1);
	
	while (lower <= upper)
	{
		const struct table_entry *middle = lower + (upper - lower) / 2;
		
		if (middle->id == id)
			return middle;
		
		if (middle->id < id)
			lower = middle + 1;
		else
			upper = middle - 1;
	}
	
	return NULL;
}

static std::string
static_lookup_manufacturer_id(int id) throw()
{
	const struct table_entry *entry;
	
	entry = static_lookup_manufacturer_entry(id);
	if (entry)
		return entry->name;
	
	return std::string();
}

static int
static_lookup_manufacturer_name(const char *define) throw()
{
	size_t i;
	
	for (i = 0; i < manufacturer_ids_max; i++)
	{
		if (manufacturer_ids[i].define)
		{
			if (strcmp(define, manufacturer_ids[i].define) == 0)
				return manufacturer_ids[i].id;
		}
	}
	
	return -1;
}

static std::string
static_lookup_device_type_id(int manufacturer_id, int id) throw()
{
	const struct table_entry *entry;
	
	entry = static_lookup_manufacturer_entry(manufacturer_id);
	if (entry && entry->device_defines)
	{
		const struct sub_table_entry *device_defines = entry->device_defines;

		while (device_defines->define)
		{
			if (device_defines->id == id)
				return device_defines->define;
			
			device_defines++;
		}
	}
	
	return std::string();
}

static int
static_lookup_device_type_name(int manufacturer_id, const char *define) throw()
{
	const struct table_entry *entry;
	
	entry = static_lookup_manufacturer_entry(manufacturer_id);
	if (entry && entry->device_defines)
	{
		const struct sub_table_entry *device_defines = entry->device_defines;
		
		while (device_defines->define)
		{
			if (strcmp(define, device_defines->define) == 0)
				return device_defines->id;
			
			device_defines++;
		}
	}
	
	return -1;
}
