
/* 
 * $Id: main1.cxx,v 1.58 2009/02/19 22:58:07 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include <stdio.h>
#include <string.h>

#include <memory>

#include "lib/fstream.h"

#include "Checker.h"
#include "device_io.h"
#include "edd_ast.h"
#include "edd_dct.h"
#include "edd_operand_interface.h"

using namespace edd;

int
main1(class Checker &edd, bool verbose)
{
	lib::fstream _out(stdout, lib::fstream::dont_close);
	lib::stream &out = _out;

	if (0)
	{
		printf("sizeof(int) = %lu\n", (unsigned long)sizeof(int));
		printf("sizeof(long) = %lu\n", (unsigned long)sizeof(long));
		printf("sizeof(void *) = %lu\n", (unsigned long)sizeof(void *));

		printf("sizeof(IDENTIFIER) = %lu\n", (unsigned long)sizeof(lib::IDENTIFIER));
		printf("sizeof(struct pos) = %lu\n", (unsigned long)sizeof(lib::pos));
		printf("sizeof(IDENT) = %lu\n", (unsigned long)sizeof(lib::IDENT));
		printf("sizeof(tree) = %lu\n", (unsigned long)sizeof(tree));
		printf("sizeof(node_tree) = %lu\n", (unsigned long)sizeof(node_tree));
		printf("sizeof(ROOT_tree) = %lu\n", (unsigned long)sizeof(ROOT_tree));
		printf("sizeof(VARIABLE_tree) = %lu\n", (unsigned long)sizeof(VARIABLE_tree));
		printf("sizeof(identifier_tree) = %lu\n", (unsigned long)sizeof(identifier_tree));
		printf("sizeof(boolean_tree) = %lu\n", (unsigned long)sizeof(boolean_tree));
		printf("sizeof(integer_tree) = %lu\n", (unsigned long)sizeof(integer_tree));
		printf("sizeof(real_tree) = %lu\n", (unsigned long)sizeof(real_tree));
		printf("sizeof(string_tree) = %lu\n", (unsigned long)sizeof(string_tree));
		printf("sizeof(DICTIONARY_REFERENCE_tree) = %lu\n", (unsigned long)sizeof(DICTIONARY_REFERENCE_tree));
	}

	if (0)
	{
		printf("#################################################\n");
		edd.misc();
		printf("#################################################\n");
	}

	if (0)
	{
		printf("#################################################\n");
		edd.dump_tree();
		printf("#################################################\n");
	}

	if (0)
	{
		class EDD_OBJECT_tree *menu;

		menu = edd.node_lookup("Menu_Main_Specialist");
		if (menu)
		{
			out.put(edd.xml_eval(NULL, menu->id(), true));
			out.put('\n');
		}
	}

	fflush(stdout);
	fflush(stderr);
	out.flush();

	return 0;
}
