
/* 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the errmsg generator tool,
 * written for the EDDL project.
 * 
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/* 
 * Copyright (C) 2001-2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 */

// C stdlib
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// C++ stdlib
#include <stack>

// my lib
#include "port.h"
#include "stream.h"

// own header
#include "edd_ast.h"
#include "edd_tree.h"

#include "edd_err.h"


namespace edd
{

static const char *edd_languages[] =
{
	"en",
	"de",
	"fr",
	"it",
	"es",
};


static const char *edd_en_types[] =
{
	"Error",
	"Warning",
	"Information",
	"Repair",
};

static const char **edd_types[] =
{
	edd_en_types,
	edd_en_types,
	edd_en_types,
	edd_en_types,
	edd_en_types,
};


static const char *edd_en_codes[] =
{
	"unknown error%{ (%m0)%}",
	"%m0",
	"system: '%m0'",
	"preprocessor: %m0",
	"out of memory",
	"internal failure%{ [module %m0]%}",
	"internal failure%{ (%m0)%}",
	"stack underflow%{ [module %m0]%}",
	"no such %m0",
	"no such file%{ \"%m0\"%}",
	"found more than %m0 errors, abort",
	"file to long - this is only a demo version",
	"more than %m0 EDD Objects - this is only a demo version",
	"this is a demo version, operation not supported",
	"cannot continue due to previous errors, abort",
	"illegal character%{ %m0%}",
	"wrong syntax%{ near token %m0%}",
	"missing attribute '%m0'%{ before '%m1'%} is non-conformant",
	"missing '%m0'%{ before '%m1'%} is non-conformant",
	"missing '%m0'%{ after '%m1'%} is non-conformant",
	"trailing '%m0'%{ at the end of '%m1'%} is non-conformant",
	"leading '%m0'%{ at the start of '%m1'%} is non-conformant",
	"syntax construct '%m0' is non-conformant",
	"usage of '%m0' is non-conformant in this context",
	"reference to '%m0' is non-conformant in this context%{, expected %m1 reference%}",
	"using keyword '%m0' as identifier is non-conformant",
	"missing %m0",
	"%m0 already defined%{ at line %p1%}%{ for %m1%}",
	"identifier '%m0' already defined%{ at line %p1%}",
	"attribute '%m0' already defined%{ at line %p1%}",
	"attribute '%m0' missing",
	"attribute '%m0' not supported%{ for %m1%}",
	"attribute '%m0' non-conformant%{, must be in '%m1' definition%}",
	"attribute '%m0' requires definition of '%m1'",
	"empty attribute '%m0' is not allowed",
	"attribute '%m0' is mutually exclusive%{ with '%m1'%}",
	"type mismatch%{ for definition of '%m0'%}%{, expected %m1%}",
	"lists%{ of %m1%} are not allowed for definition of '%m0'",
	"identifier '%m0' not declared",
	"type mismatch in reference%{ to '%m0'%}%{ defined at line %p1%}%{, expected %m1 reference%}",
	"mixing references%{ of %m0%} not allowed",
	"runtime error%{ %m0%}",
	"invalid input%{ %m0%}",
	"function%{ %m0%} not implemented",
	"circular dependencies%{ for evaluation of %m0%{ from %p1%}%}",
	"non-lvalue in%{ %m0%} expr",
	"reference%{ to '%m0'%} isn't a value",
	"expr out of range%{ in %m0 %}",
	"no constant expression",
	"two or more data types%{ in declaration of '%m0'%}",
	"invalid operand%{ to %m0%}",
	"%m0 specifier invalid%{ for '%m1'%}",
	"%m0 are mutually exclusive%{ in specification of '%m1'%}",
	"cannot convert from '%m0' to '%m1'",
	"incompatible operands in%{ %m0%} expression",
	"request for member '%m0' in something not a structure",
	"%m1 has no member named '%m0'",
	"structure has no member named '%m0'",
	"subscripted value%{ '%m0'%} isn't an array",
	"array subscript is not an integer",
	"size of array%{ '%m0'%} is negative",
	"size of array%{ '%m0'%} is null",
	"size of array%{ '%m0'%} is not specified",
	"index%{ %m0%} exceeds range of array%{ '%m1'%{ at line %p1%}%}",
	"too few arguments to method%{ '%m0'%{ at line %p1%}%}",
	"too many arguments to method%{ '%m0'%{ at line %p1%}%}",
	"too few arguments to builtin function%{ '%m0'%}",
	"too many arguments to builtin function%{ '%m0'%}",
	"no matching function call to %m0",
	"need lvalue%{ for argument %m1%} on call by reference to function '%m0'",
	"need array reference%{ for argument %m1%} on call to function '%m0'",
	"referencing '%m0' conflicts with local argument%{ defined at line %p1%}",
	"control reaches end of non-void function",
	"`return' with no value in function returning non-void",
	"`return' with a value in function returning void",
	"continue statement not within a loop",
	"break statement not within loop or switch",
	"case label not within a switch statement",
	"case label does not reduce to an integer constant",
	"duplicate case value",
	"multiple default labels in one switch",
	"switch quantity not an integer",
	"use of uninitalized variable%{ '%m0'%}",
	"overflow in constant definition",
	"underflow in constant definition",
	"%m0 does not reduce to an integer constant",
	"expression does not reduce to an integer",
	"refresh relation references local variable%{ '%m0'%}",
	"Variable '%m0' already in unit relation '%m1'%{ at line %p1%}",
	"Unit variable '%m0' cannot be part of dependency list in unit relation '%m1'%{ at line %p1%}",
	"write as one relation references local variable%{ '%m0'%}",
	"record member '%m0' must reference the same type as at line %p1",
	"Builtin '%m0' not implemented",
	"Builtin '%m0' is non-conformant%{, undocumented %m1 extension%}",
	"No such%{ %m0%} member in redefinition%{ of '%m1'%}",
	"No such import%{ %m0%} found",
	"Identification mismatch in found import %m0",
	"'%m0' already imported%{ at line %p1%}",
	"Profile of '%m0' don't match the current profile%{ (%m1)%}",
	"EDDL version %m0 not supported",
	"EDDL profile %m0 not supported",
	"%m0 for EDDL profile%{ %m1%}",
	"%m0 not allowed for EDDL profile%{ %m1%}",
	"Only %m0 allowed for EDDL profile%{ %m1%}",
	"%m0 not allowed%{ for %m1%{ at line %p1%}%}",
	"%m0 is an EDD part 1 enhancement",
	"Attribute '%m0' is an EDD part 1 enhancement",
	"constraint evaluates to nothing",
	"wrong version",
	"wrong identification",
	"Missing X/Y_VALUES in KEY_POINTS declaration",
	"EDDL object '%m0'%{ defined at line %p1%} can't be used as type reference due to constraints and/or expressions",
	"EDDL object '%m0'%{ defined at line %p1%} can't be used here; it includes references to von-value objects",
	"exactly one integer expression is required when referencing an enumeration string",
	"no such enumeration value%{ '%m0'%}%{ in VARIABLE '%m1'%{ at line %p1%}%}",
	"identification%{ '%m0'%} unknown",
	"No ARRAY_INDEX available in this context",
	"Image type '%m0' is not allowed (only png, jpg, gif)",
	"Image file '%m0' is to large%{ (%m1 bytes)%}, HART picture limit is 150.000 bytes",
	"The summary of all picture files exceeds 1.000.000 bytes%{ (currently %m0 bytes)%}",
	"HART don't allow the redefinition of DEFAULT_VALUE",
	"HART-375 requires floating point constants for FLOAT/DOUBLE VARIABLEs",
	"METHOD%{ '%m0'%} can't be used as value",
	"%{%m0 %}not supported%{ by %m1%}",
	"%m0 can not be resolved%{ to %m1%}",
	"Keyword '%m0' is a builtin name%{ for %m1; it's not recommended to use it%}",
	"division by zero",
	"Can't insert/delete on fixed-size LIST%{ '%m0'%}",
	"Insert operation would exceed the capacity%{ of '%m0'%}",
	"Object%{ %m0%} can't be used as communication object%{ [%m1]%}",
	"A new item mask must start on a byte boundary (highest bit(s) set)",
	"Item masks shall not overlap (two or more masks reference the same bit(s))",
	"Item masks shall not overflow (reference higher bits than the previous mask(s))",
	"Item mask(s) leave bit-stream gap%{: <%m1>%}",
	"Unterminated item mask(s)%{: <%m1>%}",
	"Protocol '%m0' unknown",
	"non-addressing variable '%m0' specified",
	"'%m0' not allowed when ADDRESSING is not specified",
	"Invalid size of '%m0'%{ (expected %m1)%}",
	"Unknown module '%m0' referenced",
	"Variable '%m0' missing%{ in module '%m1'%}",
	"Module count error in COMPONENT_RELATION '%m0'",
	"Module count error for '%m0' in COMPONENT_RELATION '%m1'",
	"No COMPONENT_RELATION found for module '%m0'",
	"Wrong parent module for '%m0' in relation '%m1'",
	"Relation '%m0' of COMPONENT '%m1' ignored",
	"MINUMUM_NUMBER is greater than MAXIMUM_NUMBER in COMPONENT_RELATION '%m0'",
	"identifier '%m0' already defined at line %p1 is non-conformant",
	"character%{ '%m0'%} is not an ISO Latin-1 (8859-1) character",
	"duplicate '%m0'",
	"implicit conversion from '%m0' to '%m1'",
	"declaration of '%m0' shadows previous local%{ at line %p1%}",
	"declaration of '%m0' shadows global declaration%{ at %p1%}",
	"declaration of '%m0' shadows builtin method",
	"declaration of '%m0' shadows a parameter",
	"no such dictionary entry '%m0'",
	"dictionary entry '%m0' already defined%{ at line %p1%}",
	"`/*' within comment",
	"Identifier to long, only 255 characters are allowed",
	"string contain unrepresentable characters%{ for %m0%}",
	"unterminated language tag",
	"unknown language tag%{ '%m0'%}",
	"telephone language tags are deprecated%{, please use '%m0' instead '%m1'%}",
	"Language tag '%m0' already defined",
	"Language tag 'en' conflicts with implicit default",
	"unterminated%{ '%m0'%} format specifier in string",
	"unknown identifier%{ '%m0'%} in format specifier",
	"error in assignment%{ of '%m0'%}",
	"constant expr%{ in %m0%}%{; %m1 never reached!%}",
	"statement with no effect",
	"left-hand operand of comma expression has no effect",
	"unused variable '%m0'",
	"(Bit)Enumerations with size greater than 4 are not supported",
	"Enumeration value%{ '%m0'%} already defined%{ at line %p1%}, ignored",
	"%m0 not referenced%{ in any %m1%}",
	"readable %m0 not referenced%{ in any %m1%}",
	"writeable %m0 not referenced%{ in any %m1%}",
	"readonly %m0 referenced%{ in a %m1%}",
	"writeonly %m0 referenced%{ in a %m1%}",
	"member '%m0' not defined for all possible code paths%{ following '%m1'%}",
	"index %m0 not defined for all possible code paths%{ following '%m1'%}",
	"in ARRAY '%m0' not all elements have exactly the same arithmetic type%{ following '%m1'%}",
	"Builtin '%m0' used with a multi-transaction COMMAND%{ (%m1%}",
	"Builtin '%m0' used with a single-transaction COMMAND%{ (%m1%}",
	"empty MENU not recommended",
	"%m0 incorrectly used%{, only allowed for %m1%}",
	"%m0 incorrectly used%{, not allowed for %m1%}",
	"%m0 deprecated%{, please use %m1 instead%}",
	"Mixing %m0 elements%{ with %m1%} not allowed",
	"Unreferenced object %m0",
	"Special identifier \"%m0\" shall be a %m1",
	"missing %m0 is not recommended",
	"string len shall be the maximum len of the string, not zero or negative",
	"invalid %m0 in VARIABLE%{ %m1%}",
	"invalid %m0%{ %m1%}",
	"negative value is incorrect for unsigned type%{ %m0%}",
	"overflow/underflow error%{ for %m0%}",
	"Truncated 64bit to 32bit",
	"Assigning %m0 is not recommended",
	"Can't open '%m0' for reading%{ [%m1]%}",
	"Can't open '%m0' for writing%{ [%m1]%}",
	"Can't open directory '%m0'",
	"Can't access file '%m0'",
	"Can't read identification of '%m0'",
	"Can't read the value of '%m0' from the saved values",
	"expected %m0",
	"found %m0",
	"restart",
	"this is the first default label",
	"While dereferencing '%m0'%{ defined at line %p1%}",
	"While executing %m0%{ from %m1%} ",
	"While searching %m0:",
	"Truncated to %m0",
	"Candidates are: %m0",
	"Object '%m0' overwritten by definition at line %p1",
	"ignored '%m0'",
	"ignored redefinition of '%m0'",
	"This may result in unexpected behaviour during runtime",
	"%m0 shall only be used from %m1",
	"token inserted",
};

static const char **edd_codes[] =
{
	edd_en_codes,
	edd_en_codes,
	edd_en_codes,
	edd_en_codes,
	edd_en_codes,
};

static const char *edd_codes_descr[] =
{
	"Unknown",
	"Message",
	"System",
	"Cpp",
	"OutOfMemory",
	"Internal",
	"InternalFailure",
	"StackUnderflow",
	"NoSuch",
	"NoSuchFile",
	"TooManyErrors",
	"Demoversion",
	"DemoversionObjects",
	"DemoNotSupported",
	"CannotContinue",
	"IllegalChar",
	"SyntaxError",
	"SyntaxAttributeMissing",
	"SyntaxMissing",
	"SyntaxMissingAfter",
	"SyntaxTrailing",
	"SyntaxLeading",
	"SyntaxNonConformant",
	"UsageNonConformant",
	"RefNonConformant",
	"KeywordUsage",
	"Missing",
	"AlreadyDefined",
	"IdentifierAlreadyDefined",
	"AttributeAlreadyDefined",
	"AttributeMissing",
	"AttributeNotSupported",
	"AttributeNonConformant",
	"AttributeDependency",
	"EmptyAttribute",
	"MutuallyExclusiveAttr",
	"TypeMismatch",
	"InvalidList",
	"IdentifierUnknown",
	"WrongReference",
	"MixedReferences",
	"Runtime",
	"InvalidInput",
	"NotImplemented",
	"Deadlock",
	"NonLvalue",
	"NoValue",
	"Range",
	"NoConstantExpr",
	"ToManyTypes",
	"InvalidOperand",
	"InvalidSpecifier",
	"ConflictingSpecifiers",
	"TypeCast",
	"IncompatibleOperands",
	"NoStruct",
	"NoSuchMember",
	"NoSuchStructMember",
	"NoArray",
	"ArraySubscript",
	"NegativeArraySize",
	"NullArraySize",
	"NoArraySize",
	"ArrayIndex",
	"ToFewArguments",
	"ToManyArguments",
	"ToFewArgumentsBuiltin",
	"ToManyArgumentsBuiltin",
	"NoMatchingCall",
	"NeedLvalue",
	"NeedArrayRef",
	"LocalArgConflict",
	"NoReturn",
	"NoReturnValue",
	"ReturnValue",
	"Continue",
	"Break",
	"CaseLabel",
	"CaseLabelExpr",
	"CaseDup",
	"CaseDupDefault",
	"SwitchType",
	"Uninitialized",
	"ConstantOverflow",
	"ConstantUnderflow",
	"NoIntegerConstant",
	"NoIntegerExpr",
	"RefreshReference",
	"UnitReference",
	"UnitUnitReference",
	"WAOReference",
	"RecordMember",
	"UnimplementedBuiltin",
	"NonConformantBuiltin",
	"UnknownLikeMember",
	"NoSuchImport",
	"ImportMismatch",
	"ObjectAlreadyImported",
	"ProfileMismatch",
	"UnsupportedEDDLVersion",
	"UnsupportedEDDLProfile",
	"ForProfile",
	"NotAllowedForProfile",
	"OnlyAllowedForProfile",
	"NotAllowed",
	"EDDx1Enhancement",
	"EDDx1AttrEnhancement",
	"ConstraintEvalNothing",
	"WrongVersion",
	"WrongIdentification",
	"KeyPointsValues",
	"NoADTRef",
	"NoValueRef",
	"EnumStringRefExpression",
	"NoSuchEnum",
	"IdentificationUnknown",
	"NoArrayIndex",
	"UnsupportedImageType",
	"ImageToLarge",
	"ImagesToLarge",
	"IllegalDefaultRedef",
	"NoFloatConstant375",
	"MethodUsedAsValue",
	"NotSupportedBy",
	"CantResolved",
	"BuiltinKeyword",
	"DivisionByZero",
	"FixedList",
	"InsertExceedsCapacity",
	"NoIOObject",
	"MaskStart",
	"MaskOverlap",
	"MaskOverflow",
	"MaskBitStreamGap",
	"UnterminatedMask",
	"UnknownProtocol",
	"NoAddrVar",
	"RelationType",
	"InvalidSize",
	"UnknownModule",
	"ModuleAddrMissing",
	"RelationCount",
	"ModuleCount",
	"NoRelation",
	"WrongParent",
	"ParentRelation",
	"MinMax",
	"DoubleDefined",
	"NoISOLatin1Char",
	"Duplicate",
	"ImplicitCast",
	"ShadowLocal",
	"ShadowGlobal",
	"ShadowBuiltin",
	"ShadowParameter",
	"MissingDictionaryEntry",
	"DoubleDictionaryEntry",
	"NestedComment",
	"IdentifierToLong",
	"UnrepresentableChars",
	"UnterminatedLanguageTag",
	"UnknownLanguageTag",
	"TelephonLanguageTag",
	"DoubleLanguageTag",
	"ImplicitLanguageTag",
	"UnterminatedFormat",
	"UnknownFormatIdentifier",
	"Assignment",
	"ConstantExpr",
	"StatementNoEffect",
	"CommaNoEffect",
	"UnusedVariable",
	"UnsupportedEnumSize",
	"EnumValueAlreadyDefined",
	"NotReferenced",
	"ReadableNotReferenced",
	"WriteableNotReferenced",
	"ReadonlyReferenced",
	"WriteonlyReferenced",
	"NestedStructAccess",
	"NestedArrayAccess",
	"ReferenceArrayElements",
	"SingleTransBuiltin",
	"MultiTransBuiltin",
	"EmptyMenu",
	"IncorrectlyUsedOnly",
	"IncorrectlyUsedNot",
	"Deprecated",
	"MixingElements",
	"UnreferencedObject",
	"IdentifierShallBe",
	"MissingNotRecommended",
	"StringLenBuiltins",
	"InvalidFormat",
	"Invalid",
	"NegativeUsed",
	"OverUnderFlow",
	"Truncated64to32",
	"Assign",
	"OpenFileRead",
	"OpenFileWrite",
	"OpenDirectory",
	"StatFile",
	"ReadIdentification",
	"ReadValue",
	"ExpectedTokens",
	"TokenFound",
	"RestartPoint",
	"CaseFirstDefault",
	"WhileDereferencing",
	"WhileExecuting",
	"WhileSearching",
	"Truncated",
	"Candidate",
	"OverwrittenObject",
	"Ignored",
	"IgnoredRedefinition",
	"UnexepectedBehaviour",
	"OnlyUsedFrom",
	"TokenInserted",
};

static const char edd_codes_flags[] =
{
	0x00, /* Unknown */
	0x00, /* Message */
	0x00, /* System */
	0x01, /* Cpp */
	0x00, /* OutOfMemory */
	0x00, /* Internal */
	0x00, /* InternalFailure */
	0x00, /* StackUnderflow */
	0x00, /* NoSuch */
	0x00, /* NoSuchFile */
	0x00, /* TooManyErrors */
	0x00, /* Demoversion */
	0x00, /* DemoversionObjects */
	0x00, /* DemoNotSupported */
	0x00, /* CannotContinue */
	0x00, /* IllegalChar */
	0x00, /* SyntaxError */
	0x00, /* SyntaxAttributeMissing */
	0x00, /* SyntaxMissing */
	0x00, /* SyntaxMissingAfter */
	0x01, /* SyntaxTrailing */
	0x01, /* SyntaxLeading */
	0x01, /* SyntaxNonConformant */
	0x01, /* UsageNonConformant */
	0x01, /* RefNonConformant */
	0x00, /* KeywordUsage */
	0x00, /* Missing */
	0x01, /* AlreadyDefined */
	0x01, /* IdentifierAlreadyDefined */
	0x01, /* AttributeAlreadyDefined */
	0x01, /* AttributeMissing */
	0x01, /* AttributeNotSupported */
	0x01, /* AttributeNonConformant */
	0x01, /* AttributeDependency */
	0x00, /* EmptyAttribute */
	0x00, /* MutuallyExclusiveAttr */
	0x00, /* TypeMismatch */
	0x00, /* InvalidList */
	0x00, /* IdentifierUnknown */
	0x00, /* WrongReference */
	0x00, /* MixedReferences */
	0x00, /* Runtime */
	0x00, /* InvalidInput */
	0x00, /* NotImplemented */
	0x01, /* Deadlock */
	0x00, /* NonLvalue */
	0x00, /* NoValue */
	0x00, /* Range */
	0x00, /* NoConstantExpr */
	0x00, /* ToManyTypes */
	0x00, /* InvalidOperand */
	0x00, /* InvalidSpecifier */
	0x00, /* ConflictingSpecifiers */
	0x00, /* TypeCast */
	0x00, /* IncompatibleOperands */
	0x00, /* NoStruct */
	0x00, /* NoSuchMember */
	0x00, /* NoSuchStructMember */
	0x00, /* NoArray */
	0x00, /* ArraySubscript */
	0x00, /* NegativeArraySize */
	0x00, /* NullArraySize */
	0x00, /* NoArraySize */
	0x00, /* ArrayIndex */
	0x00, /* ToFewArguments */
	0x00, /* ToManyArguments */
	0x00, /* ToFewArgumentsBuiltin */
	0x00, /* ToManyArgumentsBuiltin */
	0x00, /* NoMatchingCall */
	0x00, /* NeedLvalue */
	0x00, /* NeedArrayRef */
	0x01, /* LocalArgConflict */
	0x00, /* NoReturn */
	0x00, /* NoReturnValue */
	0x00, /* ReturnValue */
	0x00, /* Continue */
	0x00, /* Break */
	0x00, /* CaseLabel */
	0x00, /* CaseLabelExpr */
	0x00, /* CaseDup */
	0x00, /* CaseDupDefault */
	0x00, /* SwitchType */
	0x01, /* Uninitialized */
	0x00, /* ConstantOverflow */
	0x00, /* ConstantUnderflow */
	0x00, /* NoIntegerConstant */
	0x00, /* NoIntegerExpr */
	0x01, /* RefreshReference */
	0x01, /* UnitReference */
	0x00, /* UnitUnitReference */
	0x00, /* WAOReference */
	0x00, /* RecordMember */
	0x00, /* UnimplementedBuiltin */
	0x00, /* NonConformantBuiltin */
	0x00, /* UnknownLikeMember */
	0x00, /* NoSuchImport */
	0x00, /* ImportMismatch */
	0x00, /* ObjectAlreadyImported */
	0x00, /* ProfileMismatch */
	0x00, /* UnsupportedEDDLVersion */
	0x00, /* UnsupportedEDDLProfile */
	0x01, /* ForProfile */
	0x01, /* NotAllowedForProfile */
	0x01, /* OnlyAllowedForProfile */
	0x01, /* NotAllowed */
	0x00, /* EDDx1Enhancement */
	0x00, /* EDDx1AttrEnhancement */
	0x00, /* ConstraintEvalNothing */
	0x00, /* WrongVersion */
	0x00, /* WrongIdentification */
	0x00, /* KeyPointsValues */
	0x00, /* NoADTRef */
	0x00, /* NoValueRef */
	0x00, /* EnumStringRefExpression */
	0x01, /* NoSuchEnum */
	0x00, /* IdentificationUnknown */
	0x00, /* NoArrayIndex */
	0x01, /* UnsupportedImageType */
	0x01, /* ImageToLarge */
	0x01, /* ImagesToLarge */
	0x00, /* IllegalDefaultRedef */
	0x00, /* NoFloatConstant375 */
	0x00, /* MethodUsedAsValue */
	0x00, /* NotSupportedBy */
	0x00, /* CantResolved */
	0x00, /* BuiltinKeyword */
	0x00, /* DivisionByZero */
	0x00, /* FixedList */
	0x00, /* InsertExceedsCapacity */
	0x00, /* NoIOObject */
	0x00, /* MaskStart */
	0x00, /* MaskOverlap */
	0x00, /* MaskOverflow */
	0x01, /* MaskBitStreamGap */
	0x01, /* UnterminatedMask */
	0x00, /* UnknownProtocol */
	0x00, /* NoAddrVar */
	0x00, /* RelationType */
	0x00, /* InvalidSize */
	0x00, /* UnknownModule */
	0x00, /* ModuleAddrMissing */
	0x00, /* RelationCount */
	0x00, /* ModuleCount */
	0x00, /* NoRelation */
	0x00, /* WrongParent */
	0x00, /* ParentRelation */
	0x00, /* MinMax */
	0x01, /* DoubleDefined */
	0x01, /* NoISOLatin1Char */
	0x01, /* Duplicate */
	0x01, /* ImplicitCast */
	0x01, /* ShadowLocal */
	0x01, /* ShadowGlobal */
	0x01, /* ShadowBuiltin */
	0x01, /* ShadowParameter */
	0x01, /* MissingDictionaryEntry */
	0x01, /* DoubleDictionaryEntry */
	0x01, /* NestedComment */
	0x01, /* IdentifierToLong */
	0x01, /* UnrepresentableChars */
	0x01, /* UnterminatedLanguageTag */
	0x01, /* UnknownLanguageTag */
	0x01, /* TelephonLanguageTag */
	0x01, /* DoubleLanguageTag */
	0x01, /* ImplicitLanguageTag */
	0x01, /* UnterminatedFormat */
	0x01, /* UnknownFormatIdentifier */
	0x01, /* Assignment */
	0x01, /* ConstantExpr */
	0x01, /* StatementNoEffect */
	0x01, /* CommaNoEffect */
	0x01, /* UnusedVariable */
	0x01, /* UnsupportedEnumSize */
	0x01, /* EnumValueAlreadyDefined */
	0x01, /* NotReferenced */
	0x01, /* ReadableNotReferenced */
	0x01, /* WriteableNotReferenced */
	0x01, /* ReadonlyReferenced */
	0x01, /* WriteonlyReferenced */
	0x01, /* NestedStructAccess */
	0x01, /* NestedArrayAccess */
	0x01, /* ReferenceArrayElements */
	0x01, /* SingleTransBuiltin */
	0x01, /* MultiTransBuiltin */
	0x01, /* EmptyMenu */
	0x01, /* IncorrectlyUsedOnly */
	0x01, /* IncorrectlyUsedNot */
	0x01, /* Deprecated */
	0x01, /* MixingElements */
	0x01, /* UnreferencedObject */
	0x01, /* IdentifierShallBe */
	0x01, /* MissingNotRecommended */
	0x01, /* StringLenBuiltins */
	0x01, /* InvalidFormat */
	0x01, /* Invalid */
	0x01, /* NegativeUsed */
	0x01, /* OverUnderFlow */
	0x01, /* Truncated64to32 */
	0x01, /* Assign */
	0x00, /* OpenFileRead */
	0x00, /* OpenFileWrite */
	0x00, /* OpenDirectory */
	0x00, /* StatFile */
	0x00, /* ReadIdentification */
	0x00, /* ReadValue */
	0x00, /* ExpectedTokens */
	0x00, /* TokenFound */
	0x00, /* RestartPoint */
	0x00, /* CaseFirstDefault */
	0x00, /* WhileDereferencing */
	0x00, /* WhileExecuting */
	0x01, /* WhileSearching */
	0x00, /* Truncated */
	0x00, /* Candidate */
	0x00, /* OverwrittenObject */
	0x00, /* Ignored */
	0x00, /* IgnoredRedefinition */
	0x00, /* UnexepectedBehaviour */
	0x00, /* OnlyUsedFrom */
	0x00, /* TokenInserted */
};




/* some helpers
 */

bool
edd_err::id_to_lang(const char *id, enum err_lang &lang) throw()
{
	static const long id_table_size = 5;
	static const struct { long code; enum err_lang lang; } id_table[] =
	{
		{ ('e' | ('n' << 8)), xxLang_en },
		{ ('d' | ('e' << 8)), xxLang_de },
		{ ('f' | ('r' << 8)), xxLang_fr },
		{ ('i' | ('t' << 8)), xxLang_it },
		{ ('e' | ('s' << 8)), xxLang_es },
	};


	long code = id[0] | (id[1] << 8);
	bool ret = false;
	int i;

	for (i = 0; i < id_table_size; ++i)
	{
		if (id_table[i].code == code)
		{
			lang = id_table[i].lang;
			ret = true;

			break;
		}
	}

	return ret;
}

bool
edd_err::tel_to_lang(const char *tel, enum err_lang &lang) throw()
{
	static const long tel_table_size = 5;
	static const struct { long nr; enum err_lang lang; } tel_table[] =
	{
		{  51, xxLang_en },
		{  49, xxLang_de },
		{  42, xxLang_fr },
		{  43, xxLang_it },
		{  52, xxLang_es },
	};


	bool ret = false;

	if (tel[0] == '0'
	    && isdigit(tel[1])
	    && isdigit(tel[2]))
	{
		long nr = 0;
		int i;

		nr = tel[2] - 48;
		nr += (tel[1] - 48) * 10;

		for (i = 0; i < tel_table_size; ++i)
		{
			if (tel_table[i].nr == nr)
			{
				lang = tel_table[i].lang;
				ret = true;

				break;
			}
		}
	}

	return ret;
}

void
edd_err::config_code(bool onoff, err_code code)
{
	assert(code < lang_codes_size);

	if (onoff)
		disabled.reset(code);
	else
		disabled.set(code);
}

std::vector<enum err_lang>
edd_err::languages(void)
{
	std::vector<enum err_lang> ret;
	ret.reserve(lang_size);

	for (int i = 0; i < lang_size; ++i)
		ret.push_back((enum err_lang) i);

	return ret;
}

std::vector<enum err_code>
edd_err::suppressible_codes(void)
{
	std::vector<enum err_code> ret;

	for (int i = 0; i < lang_codes_size; ++i)
	{
		if (lang_codes_flags[i] & 0x01)
			ret.push_back((enum err_code) i);
	}

	return ret;
}

const char *
edd_err::language_descr(enum err_lang lang) throw()
{
	assert(lang >= 0 && lang < lang_size);
	return lang_descr[lang];
}

const char *
edd_err::message_descr(enum err_code code) throw()
{
	assert(code >= 0 && code < lang_codes_size);
	return lang_codes[0][code];
}

const char *
edd_err::code_to_id(enum err_code code) throw()
{
	assert(code >= 0 && code < lang_codes_size);
	return lang_codes_descr[code];
}

enum err_lang
edd_err::descr_to_lang(const std::string &descr)
{
	enum err_lang ret = (enum err_lang) 0;

	if (!descr.empty())
	{
		for (int i = 0; i < lang_size; ++i)
		{
			if (strcmp(lang_descr[i], descr.c_str()) == 0)
			{
				ret = (enum err_lang) i;
				break;
			}
		}
	}

	return ret;
}

int           edd_err::lang_size        = 5;
int           edd_err::lang_types_size  = 4;
int           edd_err::lang_codes_size  = 220;
const char  **edd_err::lang_descr       = edd_languages;
const char ***edd_err::lang_types       = edd_types;
const char ***edd_err::lang_codes       = edd_codes;
const char  **edd_err::lang_codes_descr = edd_codes_descr;
const char   *edd_err::lang_codes_flags = edd_codes_flags;

edd_err::warning_filter::~warning_filter(void) { }

edd_err::edd_err(lib::IDENT *id)
: ident(id)
, errThreshold(50)
, warn(true)
{
	init();
}

edd_err::~edd_err(void)
{
	clear();
}

void
edd_err::init(void)
{
	root = NULL;
	last = NULL;

	msgCount = 0;
	msgHidden = 0;
	errCount = 0;
	errUnreported = 0;
	skip_append = false;
}

void
edd_err::free_element(struct msg *element)
{
	while (element)
	{
		struct msg *chain = element->chain;

		delete element;
		element = chain;
	}
}

void
edd_err::clear(void) throw()
{
	while (root)
	{
		struct msg *next = root->next;

		free_element(root);
		root = next;
	}

	init();
}

std::auto_ptr<std::vector<err_msg> >
edd_err::vflush(err_lang lang, err_type type)
{
	std::auto_ptr<std::vector<err_msg> > ret(new std::vector<err_msg>());
	ret->reserve(msgCount);

	struct msg *e;

	e = root;

	while (e)
	{
		if (e->type == type)
		{
			vflush_msg(e, lang, ret.get());	

			while (e->next && e->next->type == type)
			{
				e = e->next;
				vflush_msg(e, lang, ret.get());
			}
		}

		e = e->next;
	}

	clear();
	return ret;
}

void
edd_err::fflush(lib::stream &f, err_lang lang)
{
	lib::scratch_buf buf(1024);
	struct msg *e;

	e = root;
	while (e)
	{
		if (e->type == xxError)
		{
			flush_msg(e, lang, buf, f);

			while (e->next && e->next->type != xxWarning)
			{
				e = e->next;
				flush_msg(e, lang, buf, f);
			}
		}

		e = e->next;
	}

	if (warn)
	{
		e = root;
		while (e)
		{
			if (e->type == xxError)
			{
				while (e->next && e->next->type != xxWarning)
					e = e->next;
			}
			else
				flush_msg(e, lang, buf, f);

			e = e->next;
		}
	}

	clear();
}

void
edd_err::set_warning_filter(std::auto_ptr<warning_filter_t> filter)
{
	warning_filter = filter;
}

void
edd_err::error(err_code code, const lib::pos &pos, const lib::IDENTIFIER &ident)
{
	lib::pos info_pos[1] = { pos };
	lib::IDENTIFIER info_msg[1] = { ident };

	put_checked(xxError, code, 1, info_pos, info_msg, false);
}

void
edd_err::error(err_code code, const lib::pos &pos1, const lib::IDENTIFIER &ident1,
				  const lib::pos &pos2, const lib::IDENTIFIER &ident2)
{
	lib::pos info_pos[2] = { pos1, pos2 };
	lib::IDENTIFIER info_msg[2] = { ident1, ident2 };

	put_checked(xxError, code, 2, info_pos, info_msg, false);
}

void
edd_err::warning(err_code code, const lib::pos &pos, const lib::IDENTIFIER &ident, bool append)
{
	lib::pos info_pos[1] = { pos };
	lib::IDENTIFIER info_msg[1] = { ident };

	put_checked(xxWarning, code, 1, info_pos, info_msg, append);
}

void
edd_err::warning(err_code code, const lib::pos &pos1, const lib::IDENTIFIER &ident1,
				    const lib::pos &pos2, const lib::IDENTIFIER &ident2, bool append)
{
	lib::pos info_pos[2] = { pos1, pos2 };
	lib::IDENTIFIER info_msg[2] = { ident1, ident2 };

	put_checked(xxWarning, code, 2, info_pos, info_msg, append);
}

void
edd_err::information(err_code code, const lib::pos &pos, const lib::IDENTIFIER &ident, bool append)
{
	lib::pos info_pos[1] = { pos };
	lib::IDENTIFIER info_msg[1] = { ident };

	put_checked(xxInformation, code, 1, info_pos, info_msg, append);
}

void
edd_err::information(err_code code, const lib::pos &pos1, const lib::IDENTIFIER &ident1,
					const lib::pos &pos2, const lib::IDENTIFIER &ident2, bool append)
{
	lib::pos info_pos[2] = { pos1, pos2 };
	lib::IDENTIFIER info_msg[2] = { ident1, ident2 };

	put_checked(xxInformation, code, 2, info_pos, info_msg, append);
}

void
edd_err::repair(err_code code, const lib::pos &pos, const lib::IDENTIFIER &ident, bool append)
{
	lib::pos info_pos[1] = { pos };
	lib::IDENTIFIER info_msg[1] = { ident };

	put_checked(xxRepair, code, 1, info_pos, info_msg, append);
}

void
edd_err::repair(err_code code, const lib::pos &pos1, const lib::IDENTIFIER &ident1,
				   const lib::pos &pos2, const lib::IDENTIFIER &ident2, bool append)
{
	lib::pos info_pos[2] = { pos1, pos2 };
	lib::IDENTIFIER info_msg[2] = { ident1, ident2 };

	put_checked(xxRepair, code, 2, info_pos, info_msg, append);
}

void
edd_err::error(const RuntimeError &xx)
{
	error(xx.error, xx.pos, xx.ident);
}

void
edd_err::error(const std::exception &e)
{
	error(xxRuntime, lib::NoPosition, ident->create(e.what()));
}

void
edd_err::warning(const RuntimeError &xx)
{
	warning(xx.error, xx.pos, xx.ident);
}

void
edd_err::warning(const std::exception &e)
{
	warning(xxRuntime, lib::NoPosition, ident->create(e.what()));
}

void
edd_err::put_checked(err_type type,
			 err_code code,
			 size_t size,
			 lib::pos *info_pos,
			 lib::IDENTIFIER *info_msg,
			 bool append)
{
	assert(type < lang_types_size);
	assert(code < lang_codes_size);
	assert(size > 0);

	assert(!append || (type != xxError));

	if (type != xxError)
	{
		if (append && skip_append)
		{
			++msgHidden;
			return;
		}

		if (!warn || disabled.test(code)
		    || (warning_filter.get() && !(*warning_filter)(code, *info_pos, *info_msg)))
		{
			skip_append = true;

			++msgHidden;
			return;
		}
	}

	if (type == xxError && errCount > errThreshold)
	{
		skip_append = true;

		++errUnreported;
		return;
	}

	skip_append = false;

	put(type, code, size, info_pos, info_msg, append);

	if (type == xxError)
	{
		if (errCount + 1 > errThreshold)
		{
			char buf[32];
			snprintf(buf, sizeof(buf), "%lu", (unsigned long)errThreshold);

			info_msg[0] = ident->create(buf);

			put(xxError, xxTooManyErrors, 1, info_pos, info_msg, false);
		}
	}
}

void
edd_err::put(err_type type,
		 err_code code,
		 size_t size,
		 const lib::pos *info_pos,
		 const lib::IDENTIFIER *info_msg,
		 bool append)
{
	struct msg *e;

	e = new struct msg;
	assert(e);

	e->next = NULL;
	e->chain = NULL;
	e->type = type;
	e->code = code;
	e->size = size;

	for (size_t i = 0; i < size; ++i)
	{
		e->info_pos[i] = info_pos[i];
		e->info_msg[i] = info_msg[i];
	}

	if (last)
	{
		if (append)
		{
			assert(type != xxError);

			struct msg *ptr = last;

			while (ptr->chain)
				ptr = ptr->chain;

			ptr->chain = e;
		}
		else
		{
			/* append on the end of the list */
			last->next = e;
			last = e;
		}
	}
	else
	{
		/* first list entry */
		root = last = e;
	}

	if (type == xxError)
		++errCount;

	++msgCount;
}

/**
 * Convert edd::RuntimeError to a human readable string (ISO-Latin1 encoding).
 */
std::string
edd_err::errmsg(err_lang lang, const RuntimeError &xx) const
{
	struct msg e;

	e.type = xxError;
	e.code = xx.error;

	e.size = 1;
	e.info_pos[0] = xx.pos;
	e.info_msg[0] = xx.ident;

	return getmsg(e, lang);
}

/**
 * Convert #err_code to a human readable string (ISO-Latin1 encoding).
 */
std::string
edd_err::errmsg(err_lang lang,
		    err_code code, const lib::pos &p, const lib::IDENTIFIER &m) const
{
	struct msg e;

	e.type = xxError;
	e.code = code;

	e.size = 1;
	e.info_pos[0] = p;
	e.info_msg[0] = m;

	return getmsg(e, lang);
}

/**
 * Convert #err_code to a human readable string (ISO-Latin1 encoding).
 */
std::string
edd_err::errmsg(err_lang lang,
		    err_code code, const lib::pos &p1, const lib::IDENTIFIER &m1,
		                   const lib::pos &p2, const lib::IDENTIFIER &m2) const
{
	struct msg e;

	e.type = xxError;
	e.code = code;

	e.size = 2;
	e.info_pos[0] = p1;
	e.info_msg[0] = m1;
	e.info_pos[1] = p2;
	e.info_msg[1] = m2;

	return getmsg(e, lang);
}

std::string
edd_err::getmsg(const struct msg &e, err_lang lang) const
{
	lib::scratch_buf buf(1024);

	getmsg(e, lang, buf);

	return buf.str();
}

void
edd_err::flush_msg(const struct msg *ptr, err_lang lang, lib::scratch_buf &buf, lib::stream &f) const
{
	while (ptr)
	{
		buf.clear();
		getmsg(*ptr, lang, buf);

		f.put(buf.data(), buf.length());
		f.put('\n');

		ptr = ptr->chain;
	}
}

void edd_err::vflush_msg(const struct msg *ptr, err_lang lang, std::vector<err_msg>* v) const
{
	assert(ptr);

	while (ptr)
	{
		assert(ptr->size > 0);
		err_msg e;

		if (ptr->info_pos[0] != lib::NoPosition)
		{
			if (ptr->info_pos[0].file != ident->NoIdent)
				e.file = ptr->info_pos[0].file.c_str();
			e.line = ptr->info_pos[0].line;
			e.column = ptr->info_pos[0].column;
			e.end_line = ptr->info_pos[0].end_line;
			e.end_column = ptr->info_pos[0].end_column;
			
			lib::scratch_buf buf(100);
			err_sprintf(buf, lang_codes[lang][ptr->code], *ptr);
			e.msg = buf.str();

			v->push_back(e);
		}

		ptr = ptr->chain;
	}
}

void
edd_err::getmsg(const struct msg &e, err_lang lang, lib::scratch_buf &buf) const
{
	assert(e.size > 0);

	if (e.info_pos[0] != lib::NoPosition)
	{
		if (e.info_pos[0].file != ident->NoIdent)
			buf.printf("%s: %lu,%lu: ",
				   e.info_pos[0].file.c_str(),
				   e.info_pos[0].line,
				   e.info_pos[0].column);
		else
			buf.printf("<stdin>: %lu,%lu: ",
				   e.info_pos[0].line,
				   e.info_pos[0].column);
	}

	buf.printf("%s: ", lang_types[lang][e.type]);

	err_sprintf(buf, lang_codes[lang][e.code], e);
}

void
edd_err::err_sprintf(lib::scratch_buf &buf, const char *fmt, const struct msg &e) const
{
	std::stack<size_t> skip_pos_stack, skip_stack;
	size_t skip_pos = 0, skip = 0;
	char c;

	while ((c = *fmt++))
	{
		int action = 0;

		if (c != '%')
		{
			buf += c;
			continue;
		}

		c = *fmt++;
		switch (c)
		{
			case '%':
			{
				buf += c;
				break;
			}
			case '{':
			{
				skip_pos_stack.push(skip_pos);
				skip_stack.push(skip);

				skip_pos = buf.length();

				break;
			}
			case '}':
			{
				if (skip)
					buf.del(buf.length() - skip);

				assert(!skip_pos_stack.empty());
				assert(!skip_stack.empty());

				skip_pos = skip_pos_stack.top();
				skip = skip_stack.top();

				skip_pos_stack.pop();
				skip_stack.pop();

				break;
			}
			case 'p':
			{
				action = 1;
				break;
			}
			case 'm':
			{
				action = 2;
				break;
			}
			case 'l':
			{
				action = 3;
				break;
			}
			default:
				break;
		}

		if (action)
		{
			lib::pos info_pos;
			lib::IDENTIFIER info_msg;

			size_t index = 0;

			c = *fmt;
			while (c && isdigit(c))
			{
				index = (((index << 2) + index) << 1) + (c - '0');
				c = *++fmt;
			}

			if (index < e.size)
			{
				info_pos = e.info_pos[index];
				info_msg = e.info_msg[index];
			}
			else
				skip = skip_pos;

			if (skip)
				continue;

			switch (action)
			{
				case 1:
				case 3:
				{
					if (info_pos != lib::NoPosition)
					{
						buf.printf("%lu, %lu",
							   info_pos.line,
							   info_pos.column);

						if ((action == 1) && (e.info_pos[0].file != info_pos.file))
						{
							buf.put(" in ");
							buf.put(info_pos.file.c_str());
						}
					}
					else
					{
						if (skip_pos)
							skip = skip_pos;
						else
							buf.printf("<unknown>");
					}

					break;
				}
				case 2:
				{
					if (info_msg != ident->NoIdent)
					{
						buf.put(info_msg.c_str());
					}
					else
					{
						if (skip_pos)
							skip = skip_pos;
						else
							buf.printf("<unknown>");
					}

					break;
				}
			}
		}
	}
}

} /* namespace edd */
