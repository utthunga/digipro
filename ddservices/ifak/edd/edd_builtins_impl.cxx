
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_builtins_impl.h"
#include "edd_ast.h"
#include "edd_tree.h"
#include <iostream>
/* BEGIN IMPORT */

// C stdlib
#if 1
# include <assert.h>
# define BUILTIN_ASSERT(x) assert x
#else
# define BUILTIN_ASSERT(x)
#endif

#include <stdlib.h>
#include <stdio.h>
#include <float.h>

// C++ stdlib
#include <memory>

// my lib
#include "absdate.h"
#include "fstream.h"
#include "port.h"
#include "scratch_buf.h"
#include "string.h"
#include "BasicType.h"
#include "Instr.h"

// own header
#include "device_io_hart.h"
#include "device_io_interface.h"
#include "edd_builtins_null.h"
#include "edd_dct.h"
#include "edd_values.h"

// windows header
// include at last to avoid any conflict with ugly windows macros
#ifdef _MSC_VER
# include <windows.h>
# undef ShellExecute
#else
# include <unistd.h>
# include <sys/time.h>
#endif


/* END IMPORT */

using namespace std;

namespace builtins
{

using namespace edd;

/* BEGIN GLOBAL */

void method_set(class method_env *env);

static int
bi_rc(class edd_tree &parent, int ret)
{
	int32_value op(ret);
	*(parent.bi_rc) = op;

	return ret;
}

static std::string
abort_errmsg(const char *func)
{
	std::string msg;

	msg += func;
	msg += " can't be called from an abort method";

	return msg;
}

static void
check_max_strlen_arg(class edd_tree *parent,
		     const class c_type &targ,
		     const lib::pos &pos)
{
	if (targ.is_constant())
	{
		int32 len = targ.getdata();

		if (len <= 0)
		{
			parent->err().warning(xxStringLenBuiltins,
					      pos,
					      parent->ident().NoIdent);
		}
	}
}

static void
check_dictionary_string(class edd_tree *parent,
			const class c_type &targ,
			const lib::pos &pos)
{
	BUILTIN_ASSERT((parent));
	BUILTIN_ASSERT((targ && targ.is_ident()));

	lib::IDENTIFIER id(parent->ident().create(targ.get_ident()));
	bool found;

	parent->dictionary().lookup(id, found);

	if (!found)
	{
		parent->err().warning(xxMissingDictionaryEntry,
			 	      pos,
				      id);
	}
}

static bool
check_eddobject(class edd_tree *parent,
		const class c_type &targ,
		enum kinds kind,
		const lib::pos &pos)
{
	BUILTIN_ASSERT((parent));
	BUILTIN_ASSERT((targ && targ.is_eddobject()));
	bool error = false;

	c_variable_ptr var(targ.getvar());

	const class edd_type *l = var->edd_type();
	BUILTIN_ASSERT((l));

	while (l)
	{
		if (l->kind() != kind)
		{
			error = true;

			parent->err().error(xxWrongReference,
					    pos,
					    l->ident(),
					    l->linkpos(),
					    parent->ident().create(tree::get_descr(kind)));
		}

		l = l->next();
	}

	return error;
}

static void
check_abort_method(class edd_tree *parent,
		   const class c_type &targ,
		   const lib::pos &pos)
{
	BUILTIN_ASSERT((parent));
	BUILTIN_ASSERT((targ && targ.is_eddobject()));

	c_variable_ptr var(targ.getvar());

	const class edd_type *l = var->edd_type();
	BUILTIN_ASSERT((l));

	while (l)
	{
		if (l->kind() != EDD_METHOD)
		{
			parent->err().error(xxWrongReference,
					    pos,
					    l->ident(),
					    l->linkpos(),
					    parent->ident().create(tree::get_descr(EDD_METHOD)));
		}
		else
		{
			class METHOD_tree *method = (class METHOD_tree *)l->getlink();

			if (method)
			{
				if (method->returntype() != c_type::tvoid)
				{
					parent->err().error(xxNotAllowed,
							    pos,
							    parent->ident().create("Return value"),
							    l->linkpos(),
							    parent->ident().create("abort METHOD"));
				}

				if (method->parameter_count() != 0)
				{
					parent->err().error(xxNotAllowed,
							    pos,
							    parent->ident().create("Arguments"),
							    l->linkpos(),
							    parent->ident().create("abort METHOD"));
				}
			}
		}

		l = l->next();
	}
}

static const c_type::typespec exact_float[] = { c_type::tfloat32, c_type::none };
static const c_type::typespec exact_double[] = { c_type::tfloat64, c_type::none };
static const c_type::typespec exact_string[] = { c_type::tstring, c_type::none };
static const c_type::typespec float_type[] = { c_type::tfloat32, c_type::tfloat64, c_type::none };
static const c_type::typespec int_type[] =
{
	c_type::tint8, c_type::tint16, c_type::tint32, c_type::tint64,
	c_type::tuint8, c_type::tuint16, c_type::tuint32, c_type::tuint64,
	c_type::none
};
static const c_type::typespec arithmetic_type[] =
{
	c_type::tint8, c_type::tint16, c_type::tint32, c_type::tint64,
	c_type::tuint8, c_type::tuint16, c_type::tuint32, c_type::tuint64,
	c_type::tfloat32, c_type::tfloat64,
	c_type::none
};

static bool
check_type(const c_type::typespec *types, c_type::typespec ref)
{
	BUILTIN_ASSERT((types));

	while (*types != c_type::none)
	{
		if (*types == ref)
			return true;

		types++;
	}

	return false;
}

static bool
check_variable(class edd_tree *parent,
	       const class c_type &targ,
	       const lib::pos &pos,
	       const c_type::typespec *_expected = NULL,
	       const c_type::typespec *_fallback = NULL)
{
	BUILTIN_ASSERT((parent));
	BUILTIN_ASSERT((targ && targ.is_eddobject()));
	bool error = true;
	bool fallback = false;

	if (targ.is_arithmetic() || targ.type() == c_type::tstring)
	{
		if (_expected)
		{
			c_type::typespec ref = targ.type();

			error = !check_type(_expected, ref);
			if (error && _fallback)
				fallback = check_type(_fallback, ref);
		}
		else
			error = false;
	}

	if (error)
	{
		lib::IDENTIFIER refident(parent->ident().NoIdent);
		lib::pos refpos(lib::NoPosition);

		c_variable_ptr var(targ.getvar());
		if (var)
		{
			refident = var->ident();
			refpos = var->pos();
		}

		lib::scratch_buf buf(100);

		buf += tree::get_descr(EDD_VARIABLE);

		const c_type::typespec *types = _expected;
		if (types)
		{
			bool first = true;
			buf += " [";

			while (*types != c_type::none)
			{
				if (first)
					first = false;
				else
					buf += ',';

				buf += c_type(*types).descr();
				types++;
			}

			buf += ']';
		}

		if (fallback && parent->pdm_mode())
		{
			parent->err().warning(xxWrongReference,
					      pos,
					      refident,
					      refpos,
					      parent->ident().create(buf));
		}
		else
		{
			parent->err().error(xxWrongReference,
					    pos,
					    refident,
					    refpos,
					    parent->ident().create(buf));
		}
	}

	return error;
}

static void
check_enum_variable(class edd_tree *parent,
		    const class c_type &var_arg,
		    const lib::pos &var_pos,
		    const class c_type &value_arg,
		    const lib::pos &value_pos,
		    bool bitenum_only)
{
	BUILTIN_ASSERT((var_arg && var_arg.is_eddobject()));

	c_variable_ptr var(var_arg.getvar());

	const class edd_type *l = var->edd_type();
	BUILTIN_ASSERT((l));

	while (l)
	{
		if (l->kind() != EDD_VARIABLE)
		{
			parent->err().error(xxWrongReference,
					    var_pos,
					    l->ident(),
					    l->linkpos(),
					    parent->ident().create(tree::get_descr(EDD_VARIABLE)));
		}
		else
		{
			class VARIABLE_tree *vt = (class VARIABLE_tree *)l->getlink();

			if (vt)
			{
				if (vt->gettype() != EDD_VARIABLE_TYPE_ENUMERATED
				    || (bitenum_only && vt->getsubtype() != EDD_VARIABLE_TYPE_ENUMERATED_TYPE_BITENUMERATED))
				{
					const char *type_descr = bitenum_only ?
						"VARIABLE TYPE BIT_ENUMERATED" : "VARIABLE TYPE (BIT_)ENUMERATED";

					parent->err().error(xxWrongReference,
							    var_pos,
							    l->ident(),
							    l->linkpos(),
							    parent->ident().create(type_descr));
				}
				else
				{
					if (value_arg.is_constant() &&
					    !vt->check_enumval_exist(node_tree::c_constant2op(value_arg)))
					{
						parent->err().error(xxNoSuchEnum,
								    value_pos,
								    parent->ident().NoIdent,
								    vt->pos,
								    vt->ident());
					}
				}
			}
		}

		l = l->next();
	}
}

static void
check_command(class edd_tree *parent,
	      const class c_type &targ,
	      const lib::pos &pos,
	      bool read)
{
	BUILTIN_ASSERT((parent));
	BUILTIN_ASSERT((targ && targ.is_eddobject()));
	bool error = false;

	c_variable_ptr var(targ.getvar());
	const class edd_type *l = var->edd_type();
	BUILTIN_ASSERT((l));

	while (l)
	{
		if (l->kind() != EDD_COMMAND)
		{
			error = true;

			parent->err().error(xxWrongReference,
					    pos,
					    l->ident(),
					    l->linkpos(),
					    parent->ident().create(tree::get_descr(EDD_COMMAND)));
		}
		else
		{
			class tree *t = l->getlink();

			if (t)
			{
				class COMMAND_tree *command;

				BUILTIN_ASSERT((t->kind == EDD_COMMAND));
				command = (class COMMAND_tree *)t;

				if (!command->check_read_write(read))
				{
					lib::IDENTIFIER id;

					if (read)
						id = parent->ident().create("COMMAND OPERATION READ");
					else
						id = parent->ident().create("COMMAND OPERATION WRITE");

					if (parent->pedantic())
						parent->err().error(xxWrongReference,
								    pos,
								    l->ident(),
								    l->linkpos(),
								    id);
					else
						parent->err().warning(xxWrongReference,
								      pos,
								      l->ident(),
								      l->linkpos(),
								      id);
				}
			}
		}

		l = l->next();
	}
}

static const class COMMAND_tree *
check_cmd_nr_only(class edd_tree *parent,
		  const class c_type &nr_targ,
		  const lib::pos &nr_pos)
{
	const class COMMAND_tree *command = NULL;

	if (nr_targ.is_constant())
	{
		int32 nr = nr_targ.getdata();

		command = parent->lookup_cmd_by_number_pass1(nr);
		if (!command)
		{
			char buf[60];
			snprintf(buf, sizeof(buf), "COMMAND %" PRId32 , nr);

			if (parent->pedantic())
				parent->err().error(xxNoSuch,
						    nr_pos,
						    parent->ident().create(buf));
			else
				parent->err().warning(xxNoSuch,
						      nr_pos,
						      parent->ident().create(buf));
		}
	}

	return command;
}

static void
check_cmd_nr_without_trans(class edd_tree *parent, const char *name, const lib::pos &pos,
			   const class c_type &nr_targ,
			   const lib::pos &nr_pos)
{
	const class COMMAND_tree *command = check_cmd_nr_only(parent, nr_targ, nr_pos);

	if (command)
	{
		if (!command->no_explicit_transactions())
		{
			parent->err().warning(xxSingleTransBuiltin,
					      pos,
					      parent->ident().create(name));
		}
	}
}

static void
check_cmd_nr_with_trans(class edd_tree *parent, const char *name, const lib::pos &pos,
			const class c_type &nr_targ,
			const lib::pos &nr_pos,
			const class c_type &transaction_targ,
			const lib::pos &transaction_pos)
{
	const class COMMAND_tree *command = check_cmd_nr_only(parent, nr_targ, nr_pos);

	if (command)
	{
		if (transaction_targ.is_constant())
		{
			int32 transaction = transaction_targ.getdata();

			if (!command->check_transaction_nr(transaction))
			{
				char buf[60];
				snprintf(buf, sizeof(buf), "TRANSACTION %" PRId32 , transaction);

				if (parent->pedantic())
					parent->err().error(xxAttributeMissing,
							    transaction_pos,
							    parent->ident().create(buf));
				else
					parent->err().warning(xxAttributeMissing,
							      transaction_pos,
							      parent->ident().create(buf));
			}
		}

		if (command->no_explicit_transactions())
		{
			parent->err().warning(xxMultiTransBuiltin,
					      pos,
					      parent->ident().create(name));
		}
	}
}

static void
check_cmd48(class edd_tree *parent, const char *name, const lib::pos &pos)
{
	c_type cmd48(c_type::tint32, new stackmachine::int32Type(48));

	check_cmd_nr_without_trans(parent, name, pos, cmd48, pos);
}

static void
check_hart_mask(class edd_tree *parent,
		const class c_type &targ,
		const lib::pos &pos,
		int mask, const char *name)
{
	if (targ.is_constant())
	{
		int32 value = targ.getdata();

		if (value & ~mask)
		{
			parent->err().warning(xxInvalid,
					      pos,
					      parent->ident().create(name));
		}
	}
}
#define check_device_status(parent,targ,pos) check_hart_mask(parent, targ, pos, 0xff, "device status")
#define check_comm_status(parent,targ,pos)   check_hart_mask(parent, targ, pos, 0x7f, "comm status")
#define check_response_code(parent,targ,pos) check_hart_mask(parent, targ, pos, 0x7f, "response code")

static void
runtime_check_hart_mask(class method_env *__env, const lib::pos &__caller_pos,
			int &value, int mask,
			const char *name)
{
	if (value & ~mask)
	{
		std::string msg(__env->parent.err().errmsg(__env->parent.get_language(),
					    xxInvalid,
					    __caller_pos,
					    __env->parent.ident().create(name)));

		__env->error(__env->BUILTINS, msg.c_str());
	}
	value &= mask;
}
#define runtime_check_device_status(value) runtime_check_hart_mask(__env, __caller_pos, value, 0xff, "device status")
#define runtime_check_comm_status(value)   runtime_check_hart_mask(__env, __caller_pos, value, 0x7f, "comm status")
#define runtime_check_response_code(value) runtime_check_hart_mask(__env, __caller_pos, value, 0x7f, "response code")

static void
check_hart_data_byte(class edd_tree *parent,
		     const class c_type &targ,
		     const lib::pos &pos)
{
	if (targ.is_constant())
	{
		int32 byte = targ.getdata();

		if (byte < 0 || (size_t)byte >= method_env::hart_masks_t::cmd48_data_maxlen)
		{
			char buf[64];
			snprintf(buf, sizeof(buf), "byte value (0 <= byte < %u",
				 method_env::hart_masks_t::cmd48_data_maxlen);

			parent->err().warning(xxInvalid,
					      pos,
					      parent->ident().create(buf));
		}
	}
}
#define check_hart_data_bit(parent,targ,pos) check_hart_mask(parent, targ, pos, 0xff, "bit value")

static bool
runtime_check_hart_data_byte(class method_env *__env, const lib::pos &__caller_pos, int byte)
{
	bool ret = true;

	if (byte < 0 || (size_t)byte >= __env->hart_masks.cmd48_data_maxlen)
	{
		std::string msg(__env->parent.err().errmsg(__env->parent.get_language(),
					    xxInvalid,
					    __caller_pos,
					    __env->parent.ident().create("byte value (0 <= byte < %u)")));

		__env->error(__env->BUILTINS, msg.c_str(), __env->hart_masks.cmd48_data_maxlen);

		ret = false;
	}

	return ret;
}
#define runtime_check_hart_data_bit(value) runtime_check_hart_mask(__env, __caller_pos, value, 0xff, "bit value")

#if 0
static void
assign_code(const std::vector<class c_type> &targs,
	    const std::vector<class c_node_tree *> &args,
	    stackmachine::SMachine &sm,
	    class c_compound_statement_tree *env)
{
	targs[0].load_address(args[0], sm, env);
	targs[1].load_value(args[1], targs[0].type(), sm, env);
	targs[0].sti(sm);
	sm.emit(new stackmachine::instr_POP());
	sm.emit(new stackmachine::instr_LDC_int32(0));
}

static void
read_code(const class c_type &targ,
	  class c_node_tree *arg,
	  c_type::typespec type,
	  stackmachine::SMachine &sm,
	  class c_compound_statement_tree *env)
{
	targ.load_value(arg, type, sm, env);
}
#endif

static operand_ptr
variable_getval(class method_env *__env, const lib::pos &__caller_pos,
		class VARIABLE_tree *t,
		operand_ptr op,
		const c_type::typespec *expected = NULL)
{
	BUILTIN_ASSERT((__env));

	if (t)
	{
		if (expected)
		{
			c_type::typespec typespec = t->c_synth_flat().type();

			while (*expected != c_type::none)
			{
				if (*expected == typespec)
					return op ? op : __env->value(t);

				expected++;
			}
		}
		else
			return op ? op : __env->value(t);
	}

	throw RuntimeError(xxTypeMismatch);
}

static operand_ptr
variable_setval0(class method_env *__env, const lib::pos &__caller_pos,
		 class VARIABLE_tree *t,
		 operand_ptr op,
		 const class operand &new_value)
{
	BUILTIN_ASSERT((__env));

	if (!op)
		op = __env->value(t);

	(*op) = new_value;

	return op;
}

static operand_ptr
variable_setval(class method_env *__env, const lib::pos &__caller_pos,
		class VARIABLE_tree *t,
		operand_ptr op,
		const class operand &new_value,
		const c_type::typespec *expected = NULL)
{
	BUILTIN_ASSERT((__env));

	if (t)
	{
		if (expected)
		{
			c_type::typespec typespec = t->c_synth_flat().type();

			while (*expected != c_type::none)
			{
				if (*expected == typespec)
					return variable_setval0(__env, __caller_pos, t, op, new_value);

				expected++;
			}
		}
		else
			return variable_setval0(__env, __caller_pos, t, op, new_value);
	}

	throw RuntimeError(xxTypeMismatch);
}

static int
hart_send(class method_env *__env, const lib::pos &__caller_pos,
	  int cmd_number, int transaction, bool do_cmd48,
	  edd_string_opmap *cmd_status,
	  edd_string_opmap *more_data_status,
	  edd_string_opmap *more_data_info)
{
	BUILTIN_ASSERT((__env));

	class device_io_hart *io = dynamic_cast<class device_io_hart *>(&__env->parent.device_io());

	if (!io)
		return bi_rc(__env->parent, BI_COMM_ERROR);

	std::string cmd_status_str;
	std::string more_data_status_str;
	std::string more_data_info_str;

	std::string *cmd_status_ptr = cmd_status ? &cmd_status_str : NULL;
	std::string *more_data_status_ptr = more_data_status ? &more_data_status_str : NULL;
	std::string *more_data_info_ptr = more_data_info ? &more_data_info_str : NULL;
	int warnings = 0;

	int ret;

	ret = io->send(__env, __caller_pos, cmd_number, transaction,
		       device_io_interface::action_command, do_cmd48,
		       cmd_status_ptr, more_data_status_ptr, more_data_info_ptr,
		       warnings);

	if (cmd_status)
		*cmd_status = cmd_status_str;

	if (more_data_status)
		*more_data_status = more_data_status_str;

	if (more_data_info)
		*more_data_info = more_data_info_str;

	return bi_rc(__env->parent, ret);
}

static std::string
lookup_response_code(class method_env *__env, const lib::pos &__caller_pos,
		     const class COMMAND_tree *cmd, int trans_nr, int response_code)
{
	BUILTIN_ASSERT((__env));

	class RESPONSE_CODE_tree *code;
	std::string ret;

	code = cmd->lookup_response_code(response_code, __env, trans_nr);
	if (code && code->have_description())
		ret = code->description(__env);

	if (ret.empty())
		ret = device_io_hart::responsecode2str0(response_code);

	return ret;
}

static std::string
lookup_response_code(class method_env *__env, const lib::pos &__caller_pos,
		     int cmd_nr, int trans_nr, int response_code)
{
	BUILTIN_ASSERT((__env));

	const class COMMAND_tree *cmd;

	cmd = __env->parent.lookup_cmd_by_number(cmd_nr);
	if (!cmd)
	{
		char buf[60];
		snprintf(buf, sizeof(buf), "COMMAND %i", cmd_nr);

		throw RuntimeError(xxNoSuch,
				   (__caller_pos != lib::NoPosition) ? __caller_pos : __env->context_node()->pos,
				   __env->parent.ident().create(buf));
	}

	return lookup_response_code(__env, __caller_pos, cmd, trans_nr, response_code);
}

class pass2_check_lists : public METHOD_BASE_tree::task
{
public:
	pass2_check_lists(std::vector<class LIST_tree *> &l, const lib::pos &p)
	: pos(p)
	{
		lists.swap(l);
		assert(lists.size() > 1);
	}

	void operator() (void)
	{
		class LIST_tree *first = lists[0];

		for (size_t i = 1, i_max = lists.size(); i < i_max; ++i)
			first->check_compatibility(lists[i], pos);
	}

private:
	std::vector<class LIST_tree *> lists;
	const lib::pos pos;
};

template<typename T>
static T builtin_nan(edd_string value, unsigned int size)
{
	T ret;

	if (value.empty())
		return std::numeric_limits<T>::signaling_NaN();
	
	if (value[0] == 'Q' || value[0] == 'q')
		ret = std::numeric_limits<T>::quiet_NaN();
	else if (value[0] == 'S' || value[0] == 's')
		ret = std::numeric_limits<T>::signaling_NaN();

	else
		throw RuntimeError(xxInvalidInput);

	if (value.size() == 1)
		return ret;

	if (value.size() == 2)
		throw RuntimeError(xxInvalidInput);

	if (value[1] != '_')
		throw RuntimeError(xxInvalidInput);

	char *str = new char[value.size()-1];
	char *end;

	strncpy(str, value.data()+2, value.size()-2);

	unsigned int *ptr = (unsigned int *) &ret;
	unsigned long v = 1;

	v <<= size;
	--v;

	*ptr &= ~v;	       
	
	v = strtol(str, &end, 16);

	if (end != str + value.size() - 2)
		throw RuntimeError(xxInvalidInput);

	v <<= size - (value.size()-2)*4;

	*ptr |= v;

	delete [] str;

	return ret;
}

/* END GLOBAL */


void
ABORT_ON_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_status_abort = __env->hart_masks.ALL_BITS;
}

void
ABORT_ON_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.device_status_abort = __env->hart_masks.ALL_BITS;
}

void
ABORT_ON_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.response_code_abort = __env->hart_masks.ALL_BITS;
	__env->hart_masks.response_all = true;
}

void
ABORT_ON_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_error_abort = true;
}

void
ABORT_ON_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.comm_status_abort |= comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_ABORT_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
ABORT_ON_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.device_status_abort |= device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_ABORT_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
ABORT_ON_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.no_device_abort = true;
}

void
ABORT_ON_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.response_code_abort |= response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_ABORT_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
AddTime(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 t0,
	edd_real64 seconds)
{
	edd_int32 __ret = 0;

	__ret = t0 + (long) seconds;

	return __ret;
}

edd_string
BUILD_MESSAGE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string text)
{
	edd_string __ret = "";

	__ret = __env->builtin_printf(text, NULL, NULL);

	return __ret;
}

edd_real64
ByteToDouble(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint08 in0,
	edd_uint08 in1,
	edd_uint08 in2,
	edd_uint08 in3,
	edd_uint08 in4,
	edd_uint08 in5,
	edd_uint08 in6,
	edd_uint08 in7)
{
	edd_real64 __ret = 0.0;

	union { double out; unsigned char in[8]; } conv;

	conv.in[0] = in7;
	conv.in[1] = in6;
	conv.in[2] = in5;
	conv.in[3] = in4;
	conv.in[4] = in3;
	conv.in[5] = in2;
	conv.in[6] = in1;
	conv.in[7] = in0;

	__ret = conv.out;

	return __ret;
}

edd_real32
ByteToFloat(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint08 in0,
	edd_uint08 in1,
	edd_uint08 in2,
	edd_uint08 in3)
{
	edd_real32 __ret = 0.0;

	union { float out; unsigned char in[4]; } conv;

	conv.in[0] = in3;
	conv.in[1] = in2;
	conv.in[2] = in1;
	conv.in[3] = in0;

	__ret = conv.out;

	return __ret;
}

edd_int32
ByteToLong(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint08 in0,
	edd_uint08 in1,
	edd_uint08 in2,
	edd_uint08 in3)
{
	edd_int32 __ret = 0;

	union { long out; unsigned char in[4]; } conv;

	conv.in[0] = in3;
	conv.in[1] = in2;
	conv.in[2] = in1;
	conv.in[3] = in0;

	__ret = conv.out;

	return __ret;
}

edd_int16
ByteToShort(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint08 in0,
	edd_uint08 in1)
{
	edd_int16 __ret = 0;

	union { short out; unsigned char in[2]; } conv;

	conv.in[0] = in1;
	conv.in[1] = in0;

	__ret = conv.out;

	return __ret;
}

#ifndef DOXYGEN_IGNORE
void
check_Date_to_DayOfMonth_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_Date_to_Month_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_Date_to_Month_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type);
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_Date_to_Year_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_real64
DiffTime(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 t1,
	edd_int32 t0)
{
	edd_real64 __ret = 0.0;

	if (t1 <= t0)
	{
		std::string msg(__env->parent.errmsg(RuntimeError(xxMessage, __caller_pos, __env->parent.ident().create("DiffTime: t1 is not greater than t0"))));

		__env->error(__env->BUILTINS, msg.c_str());
	}
						
	__ret = (double) t1 - (double) t0;

	return __ret;
}

void
DoubleToByte(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 in,
	edd_uint08_ref &out0,
	edd_uint08_ref &out1,
	edd_uint08_ref &out2,
	edd_uint08_ref &out3,
	edd_uint08_ref &out4,
	edd_uint08_ref &out5,
	edd_uint08_ref &out6,
	edd_uint08_ref &out7)
{
	union { double in; unsigned char out[2]; } conv;

	conv.in = in;

	out0 = conv.out[7];
	out1 = conv.out[6];
	out2 = conv.out[5];
	out3 = conv.out[4];
	out4 = conv.out[3];
	out5 = conv.out[2];
	out6 = conv.out[1];
	out7 = conv.out[0];
}

void
FloatToByte(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 in,
	edd_uint08_ref &out0,
	edd_uint08_ref &out1,
	edd_uint08_ref &out2,
	edd_uint08_ref &out3)
{
	union { float in; unsigned char out[2]; } conv;

	conv.in = in;

	out0 = conv.out[3];
	out1 = conv.out[2];
	out2 = conv.out[1];
	out3 = conv.out[0];
}

edd_int32
GET_DD_REVISION(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__ret = __env->parent.get_root()->get_identification()->dd_revision();

	return __ret;
}

edd_int32
GET_DEVICE_REVISION(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__ret = __env->parent.get_root()->get_identification()->device_revision();

	return __ret;
}

edd_int32
GET_DEVICE_TYPE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__ret = __env->parent.get_root()->get_identification()->device_type();

	return __ret;
}

#ifndef DOXYGEN_IGNORE
void
check_GET_DEV_VAR_VALUE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[1],
		       args[1]->pos);
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_GET_LOCAL_VAR_VALUE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	const int index = 1;

	if (!targs[index].is_lval())
	{
		parent->err().error(xxIdentifierUnknown,
				    args[index]->pos,
				    args[index]->ident());
	}
	else if (targs[index].is_eddobject())
	{
		class EDD_OBJECT_tree *t = targs[index].getvar()->edd_entry();
		BUILTIN_ASSERT((t));

		parent->err().error(xxWrongReference,
				    args[index]->pos,
				    t->ident(),
				    t->pos,
				    parent->ident().create("local variable"));
	}
	else
		targs[index].setinit();
}
#endif /* DOXYGEN_IGNORE */


edd_int32
GET_MANUFACTURER(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__ret = __env->parent.get_root()->get_identification()->manufacturer();

	return __ret;
}

edd_int32
GET_TICK_COUNT(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

#ifdef _MSC_VER
	__ret = timeGetTime();
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);

	__ret = tv.tv_sec;
	__ret *= 1000;
	__ret += tv.tv_usec * 1000;
#endif

	return __ret;
}

void
IGNORE_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_status_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.comm_status_retry = __env->hart_masks.NO_BITS;
}

void
IGNORE_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.device_status_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.device_status_retry = __env->hart_masks.NO_BITS;
}

void
IGNORE_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.response_code_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.response_code_retry = __env->hart_masks.NO_BITS;
	__env->hart_masks.response_all = true;
}

void
IGNORE_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_error_abort = false;
	__env->hart_masks.comm_error_retry = false;
}

void
IGNORE_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.comm_status_abort &= ~comm_status;
	__env->hart_masks.comm_status_retry &= ~comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_IGNORE_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
IGNORE_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.device_status_abort &= ~device_status;
	__env->hart_masks.device_status_retry &= ~device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_IGNORE_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
IGNORE_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.no_device_abort = false;
	__env->hart_masks.no_device_retry = false;
}

void
IGNORE_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.response_code_abort &= ~response_code;
	__env->hart_masks.response_code_retry &= ~response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_IGNORE_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_uint32
ITEM_ID(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * name)
{
	edd_uint32 __ret = 0;

	class EDD_OBJECT_tree *object;

	__ret = __env->parent.treemap().NoHandle;

	object = name->is_edd_object();
	if (object)
		__ret = object->id();

	return __ret;
}

edd_int32
ListDeleteElementAt(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * listname,
	edd_int32 index)
{
	edd_int32 __ret = 0;

	try
	{
		class tree *t = listname;
		BUILTIN_ASSERT((t && t->kind == EDD_LIST));

		class LIST_tree *list = (class LIST_tree *)t;

		list->delete_at(__env, index);

		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const RuntimeError &e)
	{
		__env->error(__env->BUILTINS, __env->parent.errmsg(e).c_str());

		__ret = bi_rc(__env->parent, BLTIN_BAD_ITEM_TYPE);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ListDeleteElementAt_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_eddobject(parent,
			targs[0],
			EDD_LIST,
			args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
ListInsert(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * listname,
	edd_int32 index,
	class tree * object)
{
	edd_int32 __ret = 0;

	try
	{
		class tree *t = listname;
		BUILTIN_ASSERT((t && t->kind == EDD_LIST));

		class LIST_tree *list = (class LIST_tree *)t;

		t = object;

		list->insert_at(__env, index, t->value(__env));

		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const RuntimeError &e)
	{
		__env->error(__env->BUILTINS, __env->parent.errmsg(e).c_str());

		__ret = bi_rc(__env->parent, BLTIN_BAD_ITEM_TYPE);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ListInsert_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	BUILTIN_ASSERT((targs[0] && targs[0].is_eddobject()));
	BUILTIN_ASSERT((targs[2] && targs[2].is_eddobject()));

	c_variable_ptr list(targs[0].getvar());
// XXX TODO: check object type
//	c_variable_ptr object(targs[2].getvar());

	const class edd_type *list_types = list->edd_type();
//	const class edd_type *object_types = object->edd_type();
//	BUILTIN_ASSERT((list_types && object_types));

	std::vector<class LIST_tree *> lists;

	while (list_types)
	{
		if (list_types->kind() != EDD_LIST)
		{
			parent->err().error(xxWrongReference,
					    args[0]->pos,
					    list_types->ident(),
					    list_types->linkpos(),
					    parent->ident().create(tree::get_descr(EDD_LIST)));
		}
		else
		{
			class LIST_tree *list = (class LIST_tree *)list_types->getlink();

			if (list)
				lists.push_back(list);
		}

		list_types = list_types->next();
	}

	if (lists.size() > 1)
		method->register_pass2_task(new pass2_check_lists(lists, args[2]->pos));
}
#endif /* DOXYGEN_IGNORE */


void
LongToByte(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 in,
	edd_uint08_ref &out0,
	edd_uint08_ref &out1,
	edd_uint08_ref &out2,
	edd_uint08_ref &out3)
{
	union { long in; unsigned char out[2]; } conv;

	conv.in = in;

	out0 = conv.out[3];
	out1 = conv.out[2];
	out2 = conv.out[1];
	out3 = conv.out[0];
}

edd_int32
METHODID(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * variable_name)
{
	edd_int32 __ret = 0;

	class tree *t = variable_name;
	BUILTIN_ASSERT((t));

	class EDD_OBJECT_tree *object = t->is_edd_object();
	BUILTIN_ASSERT((object));

	__ret = object->id();

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_METHODID_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	BUILTIN_ASSERT((targs[0] && targs[0].is_eddobject()));

	c_variable_ptr var(targs[0].getvar());
	const class edd_type *types = var->edd_type();

	if (types)

	while (types)
	{
		if (   types->kind() != EDD_METHOD
		    && types->kind() != EDD_VARIABLE)
		{
			parent->err().error(xxWrongReference,
					    args[0]->pos,
					    types->ident(),
					    types->linkpos(),
					    parent->ident().create("VARIABLE/METHOD"));
		}

		types = types->next();
	}
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_MenuDisplay_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	bool error;

	error = check_eddobject(parent,
				targs[0],
				EDD_MENU,
				args[0]->pos);

	if (!error)
	{
		c_variable_ptr var(targs[0].getvar());
		const class edd_type *types = var->edd_type();

		if (types)

		while (types)
		{
			BUILTIN_ASSERT((types->kind() == EDD_MENU));

			class MENU_tree *menu = (class MENU_tree *) types->getlink();

			switch (menu->get_gui_type(NULL))
			{
				case MENU_tree::GUI_OnlineDialog:
				case MENU_tree::GUI_OfflineDialog:
					break;

				default:
					parent->err().warning(xxWrongReference,
							      args[0]->pos,
							      menu->ident(),
							      menu->pos,
							      parent->ident().create("MENU DIALOG"));
					break;
			}

			types = types->next();
		}
	}
}
#endif /* DOXYGEN_IGNORE */


edd_int32
NaN_value(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64_ref &value)
{
	edd_int32 __ret = 0;

	value = std::numeric_limits<float>::quiet_NaN();

	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}

edd_int32
READ_COMMAND(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * command_name)
{
	edd_int32 __ret = 0;

	__ret = builtins::ReadCommand(__env, __caller_pos, command_name);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_READ_COMMAND_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_command(parent,
		      targs[0],
		      args[0]->pos,
		      true);
}
#endif /* DOXYGEN_IGNORE */


void
RETRY_ON_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_status_retry = __env->hart_masks.ALL_BITS;
}

void
RETRY_ON_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.device_status_retry = __env->hart_masks.ALL_BITS;
}

void
RETRY_ON_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.response_code_retry = __env->hart_masks.ALL_BITS;
	__env->hart_masks.response_all = true;
}

void
RETRY_ON_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.comm_error_retry = true;
}

void
RETRY_ON_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.comm_status_retry |= comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_RETRY_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
RETRY_ON_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.device_status_retry |= device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_RETRY_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
RETRY_ON_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.no_device_retry = true;
}

void
RETRY_ON_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.response_code_retry |= response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_RETRY_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
ReadCommand(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * command_name)
{
	edd_int32 __ret = 0;

	class COMMAND_tree *t;
	int warnings = 0;

	BUILTIN_ASSERT((command_name->kind == EDD_COMMAND));
	t = (class COMMAND_tree *) command_name;

	bool opt = __env->optimize();
	__env->set_optimize(false);
	__ret = __env->parent.device_io().do_command(__env, t, 0,
						 device_io_interface::action_read, warnings);
	__env->set_optimize(opt);

	__ret = bi_rc(__env->parent, __ret);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ReadCommand_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_command(parent,
		      targs[0],
		      args[0]->pos,
		      true);
}
#endif /* DOXYGEN_IGNORE */


void
SET_NUMBER_OF_RETRIES(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 n)
{
	if (n >= 0)
		__env->set_retries(n);
}

void
ShortToByte(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int16 in,
	edd_uint08_ref &out0,
	edd_uint08_ref &out1)
{
	union { short in; unsigned char out[2]; } conv;

	conv.in = in;

	out0 = conv.out[1];
	out1 = conv.out[0];
}

edd_int32
TIME_VALUE_to_Hour(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 time_value)
{
	edd_int32 __ret = 0;

	double tv = time_value;
	const lib::absdate base(1972, 1, 1);
	lib::absdate date(tv / 32000.0 / 86400.0);
	date += base;

	return date.get_hours();

	return __ret;
}

edd_int32
TIME_VALUE_to_Minute(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 time_value)
{
	edd_int32 __ret = 0;

	double tv = time_value;
	const lib::absdate base(1972, 1, 1);
	lib::absdate date(tv / 32000.0 / 86400.0);
	date += base;

	return date.get_minutes();

	return __ret;
}

edd_int32
TIME_VALUE_to_Second(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 time_value)
{
	edd_int32 __ret = 0;

	double tv = time_value;
	const lib::absdate base(1972, 1, 1);
	lib::absdate date(tv / 32000.0 / 86400.0);
	date += base;

	return date.get_seconds();

	return __ret;
}

edd_real64
TIME_VALUE_to_seconds(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 time_value)
{
	edd_real64 __ret = 0.0;

	double tv = time_value;

	return time_value * 32000.0;

	return __ret;
}

edd_int32
TIME_VALUE_to_string(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string_ref &str,
	edd_string format,
	edd_uint32 time_value)
{
	edd_int32 __ret = 0;

	char s[1024];
	struct tm tm;

	bzero(&tm, sizeof(tm));

	lib::absdate date(time_value / 32000.0 / 86400.0);
	const lib::absdate base(1972, 1, 1);

	date += base;

	tm.tm_sec = date.get_seconds();
	tm.tm_min = date.get_minutes();
	tm.tm_hour = date.get_hours();
	tm.tm_year = date.get_year() - 1900;
	tm.tm_mon = date.get_mon();
	tm.tm_mday = date.get_day();

	size_t ret = strftime(s, 1024, format.c_str(), &tm);

	str = s;

	if (ret == 0)
		return -1;
	else
		return ret;

	return __ret;
}

edd_real64
To_TIME_VALUE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 hours,
	edd_int32 minutes,
	edd_int32 seconds)
{
	edd_real64 __ret = 0.0;

	__ret = (seconds + minutes*60 + hours*3600)*32000;

	return __ret;
}

edd_int32
VARID(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * variable_name)
{
	edd_int32 __ret = 0;

	class tree *t = variable_name;

	BUILTIN_ASSERT((t && t->kind == EDD_VARIABLE));

	__ret = ((class EDD_OBJECT_tree *) t)->id();

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_VARID_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
WRITE_COMMAND(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * command_name)
{
	edd_int32 __ret = 0;

	__ret = builtins::WriteCommand(__env, __caller_pos, command_name);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_WRITE_COMMAND_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_command(parent,
		      targs[0],
		      args[0]->pos,
		      false);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
WriteCommand(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * command_name)
{
	edd_int32 __ret = 0;

	class COMMAND_tree *t;
	int warnings = 0;

	BUILTIN_ASSERT((command_name->kind == EDD_COMMAND));
	t = (class COMMAND_tree *) command_name;

	bool opt = __env->optimize();
	__env->set_optimize(false);
	__ret = __env->parent.device_io().do_command(__env, t, 0,
						 device_io_interface::action_write, warnings);
	__env->set_optimize(opt);

	__ret = bi_rc(__env->parent, __ret);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_WriteCommand_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_command(parent,
		      targs[0],
		      args[0]->pos,
		      false);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_ABORT_ON_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_status_abort = __env->hart_masks.ALL_BITS;
}

void
XMTR_ABORT_ON_ALL_DATA(
	class method_env *__env, const lib::pos &__caller_pos)
{
	assert(__env->hart_masks.cmd48_data_abort.size() == __env->hart_masks.cmd48_data_maxlen);

	__env->hart_masks.cmd48_data_abort.clear();
	__env->hart_masks.cmd48_data_abort.resize(__env->hart_masks.cmd48_data_maxlen, 0xff);

	assert(__env->hart_masks.cmd48_data_abort.size() == __env->hart_masks.cmd48_data_maxlen);
}

void
XMTR_ABORT_ON_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_device_status_abort = __env->hart_masks.ALL_BITS;
}

void
XMTR_ABORT_ON_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_response_code_abort = __env->hart_masks.ALL_BITS;
	__env->hart_masks.cmd48_response_all = true;
}

void
XMTR_ABORT_ON_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_error_abort = true;
}

void
XMTR_ABORT_ON_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.cmd48_comm_status_abort |= comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_ABORT_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_ABORT_ON_DATA(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 byte,
	edd_int32 bit)
{
	assert(__env->hart_masks.cmd48_data_abort.size() == __env->hart_masks.cmd48_data_maxlen);

	if (runtime_check_hart_data_byte(__env, __caller_pos, byte))
	{
		int bit_value = bit;
		runtime_check_hart_data_bit(bit_value);

		__env->hart_masks.cmd48_data_abort[byte] |= bit_value;
	}
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_ABORT_ON_DATA_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_hart_data_byte(parent, targs[0], args[0]->pos);
	check_hart_data_bit(parent, targs[1], args[1]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_ABORT_ON_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.cmd48_device_status_abort &= ~device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_ABORT_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_ABORT_ON_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_no_device_abort = true;
}

void
XMTR_ABORT_ON_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.cmd48_response_code_abort |= response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_ABORT_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_IGNORE_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_status_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.cmd48_comm_status_retry = __env->hart_masks.NO_BITS;
}

void
XMTR_IGNORE_ALL_DATA(
	class method_env *__env, const lib::pos &__caller_pos)
{
	assert(__env->hart_masks.cmd48_data_abort.size() == __env->hart_masks.cmd48_data_maxlen);
	assert(__env->hart_masks.cmd48_data_retry.size() == __env->hart_masks.cmd48_data_maxlen);

	__env->hart_masks.cmd48_data_abort.clear();
	__env->hart_masks.cmd48_data_abort.resize(__env->hart_masks.cmd48_data_maxlen, 0x00);

	__env->hart_masks.cmd48_data_retry.clear();
	__env->hart_masks.cmd48_data_retry.resize(__env->hart_masks.cmd48_data_maxlen, 0x00);

	assert(__env->hart_masks.cmd48_data_abort.size() == __env->hart_masks.cmd48_data_maxlen);
	assert(__env->hart_masks.cmd48_data_retry.size() == __env->hart_masks.cmd48_data_maxlen);
}

void
XMTR_IGNORE_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_device_status_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.cmd48_device_status_retry = __env->hart_masks.NO_BITS;
}

void
XMTR_IGNORE_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_response_code_abort = __env->hart_masks.NO_BITS;
	__env->hart_masks.cmd48_response_code_retry = __env->hart_masks.NO_BITS;
	__env->hart_masks.cmd48_response_all = true;
}

void
XMTR_IGNORE_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_error_abort = false;
	__env->hart_masks.cmd48_comm_error_retry = false;
}

void
XMTR_IGNORE_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.cmd48_comm_status_abort &= ~comm_status;
	__env->hart_masks.cmd48_comm_status_retry &= ~comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_IGNORE_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_IGNORE_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.cmd48_device_status_abort &= ~device_status;
	__env->hart_masks.cmd48_device_status_retry &= ~device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_IGNORE_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_IGNORE_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_no_device_abort = false;
	__env->hart_masks.cmd48_no_device_retry = false;
}

void
XMTR_IGNORE_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.cmd48_response_code_abort &= ~response_code;
	__env->hart_masks.cmd48_response_code_retry &= ~response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_IGNORE_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_RETRY_ON_ALL_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_status_retry = __env->hart_masks.ALL_BITS;
}

void
XMTR_RETRY_ON_ALL_DATA(
	class method_env *__env, const lib::pos &__caller_pos)
{
	assert(__env->hart_masks.cmd48_data_retry.size() == __env->hart_masks.cmd48_data_maxlen);

	__env->hart_masks.cmd48_data_retry.clear();
	__env->hart_masks.cmd48_data_retry.resize(__env->hart_masks.cmd48_data_maxlen, 0xff);

	assert(__env->hart_masks.cmd48_data_retry.size() == __env->hart_masks.cmd48_data_maxlen);
}

void
XMTR_RETRY_ON_ALL_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_device_status_retry = __env->hart_masks.ALL_BITS;
}

void
XMTR_RETRY_ON_ALL_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	builtins::XMTR_RETRY_ON_ALL_RESPONSE_CODES(__env, __caller_pos);
}

void
XMTR_RETRY_ON_ALL_RESPONSE_CODES(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_response_code_retry = __env->hart_masks.ALL_BITS;
	__env->hart_masks.cmd48_response_all = true;
}

void
XMTR_RETRY_ON_COMM_ERROR(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_comm_error_retry = true;
}

void
XMTR_RETRY_ON_COMM_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	__env->hart_masks.cmd48_comm_status_retry |= comm_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_RETRY_ON_COMM_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_RETRY_ON_DATA(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 byte,
	edd_int32 bit)
{
	assert(__env->hart_masks.cmd48_data_retry.size() == __env->hart_masks.cmd48_data_maxlen);

	if (runtime_check_hart_data_byte(__env, __caller_pos, byte))
	{
		int bit_value = bit;
		runtime_check_hart_data_bit(bit_value);

		__env->hart_masks.cmd48_data_retry[byte] |= bit_value;
	}
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_RETRY_ON_DATA_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_hart_data_byte(parent, targs[0], args[0]->pos);
	check_hart_data_bit(parent, targs[1], args[1]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_RETRY_ON_DEVICE_STATUS(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	__env->hart_masks.cmd48_device_status_retry |= device_status;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_RETRY_ON_DEVICE_STATUS_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
XMTR_RETRY_ON_NO_DEVICE(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->hart_masks.cmd48_no_device_retry = true;
}

void
XMTR_RETRY_ON_RESPONSE_CODE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	__env->hart_masks.cmd48_response_code_retry |= response_code;
}
#ifndef DOXYGEN_IGNORE
void
check_XMTR_RETRY_ON_RESPONSE_CODE_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_response_code(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_real32
abs(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 value)
{
	edd_real32 __ret = 0.0;

 __ret = ::fabsf(value); 
	return __ret;
}

edd_real64
abs_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_real64 __ret = 0.0;

 __ret = ::fabs(value); 
	return __ret;
}

edd_real32
acos(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 value)
{
	edd_real32 __ret = 0.0;

 __ret = ::acosf(value); 
	return __ret;
}

edd_real64
acos_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_real64 __ret = 0.0;

 __ret = ::acos(value); 
	return __ret;
}

edd_int32
add_abort_method(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * abort_method_name)
{
	edd_int32 __ret = 0;

	class tree *t;

	t = abort_method_name;
	BUILTIN_ASSERT((t && t->kind == EDD_METHOD));

	try
	{
		__env->add_abort((class METHOD_tree *) t);
		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const std::exception &e)
	{
		__env->error(__env->BUILTINS, e.what());
		__ret = bi_rc(__env->parent, BI_ERROR);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_add_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_abort_method(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
add_abort_method_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 method_id)
{
	edd_int32 __ret = 0;

	class tree *t;

	t = __env->id2tree(method_id);
	if (t && t->kind == EDD_METHOD)
	{
		class METHOD_tree *method = (class METHOD_tree *) t;
		__ret = 0;

		if (method->returntype() != c_type::tvoid)
		{
			std::string msg(__env->parent.err().errmsg(__env->parent.get_language(),
							     xxNotAllowed,
							     __caller_pos,
							     __env->parent.ident().create("Return value"),
							     method->pos,
							     __env->parent.ident().create("abort METHOD")));

			__env->error(__env->BUILTINS, msg.c_str());

			__ret = bi_rc(__env->parent, BI_ERROR);
		}

		if (method->parameter_count() != 0)
		{
			std::string msg(__env->parent.err().errmsg(__env->parent.get_language(),
							     xxNotAllowed,
							     __caller_pos,
							     __env->parent.ident().create("Arguments"),
							     method->pos,
							     __env->parent.ident().create("abort METHOD")));

			__env->error(__env->BUILTINS, msg.c_str());

			__ret = bi_rc(__env->parent, BI_ERROR);
		}

		if (!__ret)
		{
			try
			{
				__env->add_abort(method);
				__ret = bi_rc(__env->parent, BI_SUCCESS);
			}
			catch (const std::exception &e)
			{
				__env->error(__env->BUILTINS, e.what());
				__ret = bi_rc(__env->parent, BI_ERROR);
			}
		}
	}
	else
		__ret = bi_rc(__env->parent, BI_ERROR);

	return __ret;
}

edd_real32
asin(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 value)
{
	edd_real32 __ret = 0.0;

 __ret = ::asinf(value); 
	return __ret;
}

edd_real64
asin_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_real64 __ret = 0.0;

 __ret = ::asin(value); 
	return __ret;
}

edd_int32
assign_double(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_real64 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	edd::float64_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_double_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       exact_double,
		       arithmetic_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
assign_float(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_real64 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	edd::float64_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_float_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       exact_float,
		       arithmetic_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
assign_int(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_int32 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	int32_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_int_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
assign_long(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_int32 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	int32_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_long_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
assign_str(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_string new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	edd::string_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_str_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       exact_string);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
assign_var(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * destination_var,
	class tree * source_var)
{
	edd_int32 __ret = 0;

	operand_ptr src(source_var->value(__env));
	operand_ptr dst(destination_var->value(__env));
	BUILTIN_ASSERT((src && dst));

	(*dst) = (*src);
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_assign_var_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	bool dst_error, src_error;

	dst_error =
		check_variable(parent,
			       targs[0],
			       args[0]->pos);

	src_error =
		check_variable(parent,
			       targs[1],
			       args[1]->pos);

	if (!dst_error && !src_error)
	{
		if (targs[1].cast(targs[0]) == c_type::no)
		{
			parent->err().error(xxIncompatibleOperands,
					    args[0]->pos,
					    parent->ident().create("assignment"));
		}
	}
}
#endif /* DOXYGEN_IGNORE */


edd_real32
atan(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 value)
{
	edd_real32 __ret = 0.0;

 __ret = ::atanf(value); 
	return __ret;
}

edd_real64
atan_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_real64 __ret = 0.0;

 __ret = ::atan(value); 
	return __ret;
}

edd_real32
atof(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string text)
{
	edd_real32 __ret = 0.0;

 __ret = float(::atof(text.c_str())); 
	return __ret;
}

edd_int32
atoi(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string text)
{
	edd_int32 __ret = 0;

 __ret = ::atoi(text.c_str()); 
	return __ret;
}

edd_int32
atol(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string text)
{
	edd_int32 __ret = 0;

 __ret = ::atol(text.c_str()); 
	return __ret;
}

edd_real32
cbrt(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

#ifdef WIN32
	return ::powf(x, float(1.0/3.0));
#else
	return ::cbrtf(x);
#endif

	return __ret;
}

edd_real64
cbrt_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

#ifdef WIN32
	return ::pow(x, 1.0/3.0);
#else
	return ::cbrt(x);
#endif

	return __ret;
}

edd_real32
ceil(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::ceilf(x); 
	return __ret;
}

edd_real64
ceil_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::ceil(x); 
	return __ret;
}

edd_real32
cos(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::cos(x); 
	return __ret;
}

edd_real64
cos_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::cos(x); 
	return __ret;
}

edd_real32
cosh(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::coshf(x); 
	return __ret;
}

edd_real64
cosh_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::cosh(x); 
	return __ret;
}

edd_int32
dassign(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_real64 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	edd::float64_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_dassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       exact_double,
		       arithmetic_type);
}
#endif /* DOXYGEN_IGNORE */


edd_string
dictionary_string(
	class method_env *__env, const lib::pos &__caller_pos,
	lib::IDENTIFIER dict_string_name)
{
	edd_string __ret = "";

	bool found;

	__ret = __env->parent.dictionary_lookup(dict_string_name, found);

	if (!found)
	{
		__ret = "ERROR: Dict String Not Found";

		std::string msg("get_dictionary_string: ");
		msg += __env->parent.errmsg(RuntimeError(xxMissingDictionaryEntry,
						   __caller_pos,
						   dict_string_name));

		__env->warning(__env->BUILTINS, msg.c_str());
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_dictionary_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_dictionary_string(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
discard_on_exit(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->set_op_cache_state(__env->discard);
}

void
display_comm_status(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 comm_status_value)
{
	int comm_status = comm_status_value;
	runtime_check_comm_status(comm_status);

	builtins::ACKNOWLEDGE(__env, __caller_pos, device_io_hart::commstatus2str(comm_status));
}
#ifndef DOXYGEN_IGNORE
void
check_display_comm_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_comm_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
display_device_status(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 device_status_value)
{
	int device_status = device_status_value;
	runtime_check_device_status(device_status);

	builtins::ACKNOWLEDGE(__env, __caller_pos, device_io_hart::devicestatus2str(device_status));
}
#ifndef DOXYGEN_IGNORE
void
check_display_device_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_device_status(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
display_response_status(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd,
	edd_int32 response_code_value)
{
	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	std::string msg;

	try
	{
		msg = lookup_response_code(__env, __caller_pos, cmd, -1, response_code_value);
	}
	catch (const RuntimeError &xx)
	{
		msg = __env->parent.errmsg(xx);

		__env->error(__env->BUILTINS,
			    "While executing display_response_status: %s", msg.c_str());
	}

	builtins::ACKNOWLEDGE(__env, __caller_pos, msg);
}
#ifndef DOXYGEN_IGNORE
void
check_display_response_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_only(parent,
			  targs[0], args[0]->pos);

	check_response_code(parent, targs[1], args[1]->pos);

	// XXX TODO check if response_code_value is defined inside COMMAND
	// response code list
}
#endif /* DOXYGEN_IGNORE */


void
display_xmtr_status(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * xmtr_variable,
	edd_int32 response_code)
{
	std::string msg(__env->parent.langmap("Response code:\n\n"));

	operand_ptr strop(new edd::string_lvalue);
	edd_string_ref strref(strop);

	if (builtins::get_enum_string(__env, __caller_pos, xmtr_variable, response_code, strref) != BI_SUCCESS)
	{
		class tree *t = xmtr_variable;
		BUILTIN_ASSERT((t));

		msg += __env->parent.langmap("No such response value in "
				       "|de|Undefinierter Response-Wert in ");
		msg += t->name();
	}
	else
		msg += strref;

	builtins::ACKNOWLEDGE(__env, __caller_pos, msg);
}
#ifndef DOXYGEN_IGNORE
void
check_display_xmtr_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_enum_variable(parent, targs[0], args[0]->pos, targs[1], args[1]->pos, true);
}
#endif /* DOXYGEN_IGNORE */


edd_real32
exp(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::expf(x); 
	return __ret;
}

edd_real64
exp_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::exp(x); 
	return __ret;
}

edd_int32
ext_send_command(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_string_ref &cmd_status,
	edd_string_ref &more_data_status,
	edd_string_ref &more_data_info)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, 0, true,
			 &cmd_status, &more_data_status, &more_data_info);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ext_send_command_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_without_trans(parent, "ext_send_command", pos, targs[0], args[0]->pos);

	check_cmd48(parent, "ext_send_command", pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
ext_send_command_trans(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_int32 transaction,
	edd_string_ref &cmd_status,
	edd_string_ref &more_data_status,
	edd_string_ref &more_data_info)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, transaction, true,
			 &cmd_status, &more_data_status, &more_data_info);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ext_send_command_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_with_trans(parent, "ext_send_command_trans", pos,
				targs[0], args[0]->pos,
				targs[1], args[1]->pos);

	check_cmd48(parent, "ext_send_command_trans", pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
fassign(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_real64 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	edd::float64_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_fassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       exact_float,
		       arithmetic_type);
}
#endif /* DOXYGEN_IGNORE */


edd_real64
fgetval(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_real64 __ret = 0.0;

	__ret = *(variable_getval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    float_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_fgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::fgetval);
}
#endif /* DOXYGEN_IGNORE */


edd_real64
float_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_real64 __ret = 0.0;

	operand_ptr var(source_var_name->value(__env));

	__ret = (*var);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_float_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       float_type,
		       int_type);
}
#endif /* DOXYGEN_IGNORE */


edd_real32
floor(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::floorf(x); 
	return __ret;
}

edd_real64
floor_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::floor(x); 
	return __ret;
}

edd_real32
fmod(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x,
	edd_real32 y)
{
	edd_real32 __ret = 0.0;

 __ret = ::fmodf(x, y); 
	return __ret;
}

edd_real64
fmod_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x,
	edd_real64 y)
{
	edd_real64 __ret = 0.0;

 __ret = ::fmod(x, y); 
	return __ret;
}

edd_int32
fpclassify(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_int32 __ret = 0;

#pragma pop_macro("fpclassify")

#ifdef _MSC_VER
	return _fpclass(value);
#else
	return fpclassify(value);
#endif

	return __ret;
}

edd_real64
fsetval(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_real64 __ret = 0.0;

	edd::float64_value v(value);

	__ret = *(variable_setval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    v,
				    float_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_fsetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::fsetval);
}
#endif /* DOXYGEN_IGNORE */


edd_string
ftoa(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 value)
{
	edd_string __ret = "";

	__ret = lib::ftoa("%.8g", value);

	return __ret;
}

edd_real64
fvar_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_real64 __ret = 0.0;

	operand_ptr var(source_var_name->value(__env));

	__ret = (*var);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_fvar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       float_type,
		       int_type);
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_get_dev_var_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[2],
		       args[2]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_dictionary_string(
	class method_env *__env, const lib::pos &__caller_pos,
	lib::IDENTIFIER dict_string_name,
	edd_string_ref &str,
	edd_int32 maxlen)
{
	edd_int32 __ret = 0;

	std::string s;
	bool found;

	if (maxlen <= 0)
	{
		std::string msg("get_dictionary_string: ");
		msg += __env->parent.errmsg(RuntimeError(xxStringLenBuiltins,
						   __caller_pos));

		__env->warning(__env->BUILTINS, msg.c_str());
	}

	s = __env->parent.dictionary_lookup(dict_string_name, found);

	if (found)
	{
		if (maxlen > 0)
		{
			size_t maxlen_ = maxlen;

			if (s.length() > maxlen_)
				s.resize(maxlen_);
		}

		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	else
	{
		std::string msg("get_dictionary_string: ");
		msg += __env->parent.errmsg(RuntimeError(xxMissingDictionaryEntry,
						   __caller_pos,
						   dict_string_name));

		__env->warning(__env->BUILTINS, msg.c_str());

		__ret = bi_rc(__env->parent, BI_ERROR);
	}

	str = s;

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_dictionary_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_dictionary_string(parent, targs[0], args[0]->pos);

	check_max_strlen_arg(parent, targs[2], args[2]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_dictionary_string_1(
	class method_env *__env, const lib::pos &__caller_pos,
	lib::IDENTIFIER dict_string_name,
	edd_string_ref &str)
{
	edd_int32 __ret = 0;

	__ret = builtins::get_dictionary_string(__env, __caller_pos, dict_string_name, str, 65536);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_dictionary_string_1(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_dictionary_string(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_enum_string(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * variable,
	edd_int32 value,
	edd_string_ref &status_string)
{
	edd_int32 __ret = 0;

	__ret = bi_rc(__env->parent, BI_ERROR);

	class tree *t = variable;
	BUILTIN_ASSERT((t && t->kind == EDD_VARIABLE));

	class VARIABLE_ENUMERATOR_tree *t_enum;
	int32_value v(value);

	t_enum = ((class VARIABLE_tree *)t)->get_enumeration(v, __env);
	if (t_enum)
	{
		status_string = t_enum->description(__env);
		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_enum_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_enum_variable(parent, targs[0], args[0]->pos, targs[1], args[1]->pos, false);
}
#endif /* DOXYGEN_IGNORE */


#ifndef DOXYGEN_IGNORE
void
check_get_local_var_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	const int index = 2;

	if (!targs[index].is_lval())
	{
		parent->err().error(xxIdentifierUnknown,
				    args[index]->pos,
				    args[index]->ident());
	}
	else if (targs[index].is_eddobject())
	{
		class EDD_OBJECT_tree *t = targs[index].getvar()->edd_entry();
		BUILTIN_ASSERT((t));

		parent->err().error(xxWrongReference,
				    args[index]->pos,
				    t->ident(),
				    t->pos,
				    parent->ident().create("local variable"));
	}
	else
		targs[index].setinit();
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_more_status(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string_ref &more_data_status,
	edd_string_ref &more_data_info)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, 48, 0, false,
			 &more_data_status, NULL, &more_data_info);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_more_status_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd48(parent, "get_more_status", pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_rspcode_string(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_int32 response_code_value,
	edd_string_ref &response_string,
	edd_int32 response_string_length)
{
	edd_int32 __ret = 0;

	int response_code = response_code_value;
	runtime_check_response_code(response_code);

	if (response_string_length <= 0)
	{
		std::string msg("get_rspcode_string: ");
		msg += __env->parent.errmsg(RuntimeError(xxStringLenBuiltins,
						   __caller_pos));

		__env->warning(__env->BUILTINS, msg.c_str());
	}

	__ret = bi_rc(__env->parent, BI_ERROR);

	try
	{
		std::string s;

		s = lookup_response_code(__env, __caller_pos, cmd_number, -1, response_code);
		if (!s.empty())
		{
			if (response_string_length >= 0)
			{
				size_t maxlen_ = response_string_length;

				if (s.length() > maxlen_)
					s.resize(maxlen_);
			}

			response_string = s;
			__ret = bi_rc(__env->parent, BI_SUCCESS);
		}
	}
	catch (const RuntimeError &xx)
	{
		__env->error(__env->BUILTINS,
			    "While executing get_rspcode_string: %s", __env->parent.errmsg(xx).c_str());
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_rspcode_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_only(parent, targs[0], args[0]->pos);

	check_response_code(parent, targs[1], args[1]->pos);

	check_max_strlen_arg(parent, targs[3], args[3]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_rspcode_string_by_id(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * command_name,
	edd_int32 response_code_value,
	edd_string_ref &response_string)
{
	edd_int32 __ret = 0;

	int response_code = response_code_value;

	__ret = bi_rc(__env->parent, BI_ERROR);

	try
	{
		class COMMAND_tree *t;
		std::string s;

		BUILTIN_ASSERT((command_name->kind == EDD_COMMAND));
		t = (class COMMAND_tree *) command_name;

		s = lookup_response_code(__env, __caller_pos, t, -1, response_code);
		if (!s.empty())
		{
			response_string = s;
			__ret = bi_rc(__env->parent, BI_SUCCESS);
		}
	}
	catch (const RuntimeError &xx)
	{
		__env->error(__env->BUILTINS,
			    "While executing get_rspcode_by_id: %s", __env->parent.errmsg(xx).c_str());
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_rspcode_string_by_id_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_command(parent,
		      targs[0],
		      args[0]->pos,
		      true);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
get_status_code_string(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * variable_name,
	edd_int32 status_code,
	edd_string_ref &status_string,
	edd_int32 status_string_length)
{
	edd_int32 __ret = 0;

	__ret = builtins::get_enum_string(__env, __caller_pos, variable_name, status_code, status_string);

	if (status_string_length <= 0)
	{
		std::string msg("get_status_code_string: ");
		msg += __env->parent.errmsg(RuntimeError(xxStringLenBuiltins,
						   __caller_pos));

		__env->warning(__env->BUILTINS, msg.c_str());
	}

	if (__ret == BI_SUCCESS && status_string_length > 0)
	{
		std::string str(status_string);

		if (int(str.size()) > status_string_length)
		{
			str.resize(status_string_length);
			status_string = str;
		}
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_get_status_code_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_enum_variable(parent, targs[0], args[0]->pos, targs[1], args[1]->pos, true);

	check_max_strlen_arg(parent, targs[3], args[3]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
get_variable_string(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * variable,
	edd_string_ref &out)
{
	operand_ptr op(variable->value(__env));
	BUILTIN_ASSERT((op));

	out = op->pretty_print();
}
#ifndef DOXYGEN_IGNORE
void
check_get_variable_string_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
iassign(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_int32 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	int32_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);
	//std::cout<<"return of iassign is"<<(int)__ret<<std::endl;
	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_iassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	//std::cout<<"return of check_iassign is"<<std::endl;
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
igetval(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__ret = *(variable_getval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    int_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_igetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::igetval);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
int_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_int32 __ret = 0;

	operand_ptr var(source_var_name->value(__env));

	__ret = *var;

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_int_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
is_NaN(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 dvalue)
{
	edd_int32 __ret = 0;

	__ret = isnan(dvalue);

	return __ret;
}

edd_int32
isetval(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 value)
{
	edd_int32 __ret = 0;

	int32_value v(value);

	__ret = *(variable_setval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    v,
				    int_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_isetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::isetval);
}
#endif /* DOXYGEN_IGNORE */


edd_string
itoa(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 value)
{
	edd_string __ret = "";

	long v = value;

	char buf[64];
	snprintf(buf, sizeof(buf), "%li", v);

	__ret = buf;

	return __ret;
}

edd_int32
ivar_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_int32 __ret = 0;

	operand_ptr var(source_var_name->value(__env));

	__ret = *var;

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ivar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
lassign(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	edd_int32 new_value)
{
	edd_int32 __ret = 0;

	operand_ptr var(device_var->value(__env));
	int32_value value(new_value);

	(*var) = value;
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_lassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
lgetval(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;


	__ret = *(variable_getval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    int_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_lgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::lgetval);
}
#endif /* DOXYGEN_IGNORE */


edd_real32
log(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::logf(x); 
	return __ret;
}

edd_real64
log_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::log(x); 
	return __ret;
}

edd_real32
log10(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::log10f(x); 
	return __ret;
}

edd_real64
log10_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::log10(x); 
	return __ret;
}

edd_real32
log2(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::logf(x) / ::logf(2); 
	return __ret;
}

edd_real64
log2_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::log(x) / ::log((double)2); 
	return __ret;
}

edd_int32
long_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_int32 __ret = 0;

	operand_ptr var(source_var_name->value(__env));

	__ret = *var;

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_long_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
lsetval(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 value)
{
	edd_int32 __ret = 0;

	int32_value v(value);

	__ret = *(variable_setval(__env, __caller_pos,
				    __env->context_variable(),
				    __env->context_variable_value(),
				    v,
				    int_type));

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_lsetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::lsetval);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
lvar_value(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * source_var_name)
{
	edd_int32 __ret = 0;

	operand_ptr var(source_var_name->value(__env));

	__ret = *var;

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_lvar_value_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_variable(parent,
		       targs[0],
		       args[0]->pos,
		       int_type,
		       float_type);
}
#endif /* DOXYGEN_IGNORE */


edd_real64
nan(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string value)
{
	edd_real64 __ret = 0.0;

	return builtin_nan<double>(value, 56);

	return __ret;
}

edd_real32
nanf(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string value)
{
	edd_real32 __ret = 0.0;

	return builtin_nan<float>(value, 24);

	return __ret;
}

void
p_abort(
	class method_env *__env, const lib::pos &__caller_pos)
{
	builtins::abort(__env, __caller_pos);
}

edd_int32
pop_abort_method(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	try
	{
		if (__env->aborted())
			throw std::runtime_error(abort_errmsg("pop_abort_method"));

		__env->pop_abort();
		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const std::exception &e)
	{
		__env->error(__env->BUILTINS, e.what());

		__ret = bi_rc(__env->parent, BI_ERROR);
	}

	return __ret;
}

edd_real32
pow(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x,
	edd_real32 y)
{
	edd_real32 __ret = 0.0;

 __ret = ::powf(x, y); 
	return __ret;
}

edd_real64
pow_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x,
	edd_real64 y)
{
	edd_real64 __ret = 0.0;

 __ret = ::pow(x, y); 
	return __ret;
}

void
process_abort(
	class method_env *__env, const lib::pos &__caller_pos)
{
	if (__env->aborted())
		throw std::runtime_error(abort_errmsg("process_abort"));

	__env->abort();
}

edd_int32
push_abort_method(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * abort_method_name)
{
	edd_int32 __ret = 0;

	class tree *t;

	t = abort_method_name;
	BUILTIN_ASSERT((t && t->kind == EDD_METHOD));

	try
	{
		__env->push_abort((class METHOD_tree *) t);
		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const std::exception &e)
	{
		__env->error(__env->BUILTINS, "While executing push_abort_method: %s", e.what());

		__ret = bi_rc(__env->parent, BI_ERROR);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_push_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_abort_method(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
remove_abort_method(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * abort_method_name)
{
	edd_int32 __ret = 0;

	class tree *t;

	t = abort_method_name;
	BUILTIN_ASSERT((t && t->kind == EDD_METHOD));

	try
	{
		if (__env->aborted())
			throw std::runtime_error(abort_errmsg("remove_abort_method"));

		__env->remove_abort((class METHOD_tree *) t);
		__ret = bi_rc(__env->parent, BI_SUCCESS);
	}
	catch (const std::exception &e)
	{
		__env->error(__env->BUILTINS, e.what());

		__ret = bi_rc(__env->parent, BI_ERROR);
	}

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_remove_abort_method_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_abort_method(parent, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
remove_abort_method_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_uint32 method_id)
{
	edd_int32 __ret = 0;

	class tree *t;

	t = __env->id2tree(method_id);
	if (t && t->kind == EDD_METHOD)
	{
		try
		{
			if (__env->aborted())
				throw std::runtime_error(abort_errmsg("remove_abort_method"));

			__env->remove_abort((class METHOD_tree *) t);
			__ret = bi_rc(__env->parent, BI_SUCCESS);
		}
		catch (const std::exception &e)
		{
			__env->error(__env->BUILTINS, e.what());

			__ret = bi_rc(__env->parent, BI_ERROR);
		}
	}
	else
		__ret = bi_rc(__env->parent, BI_ERROR);

	return __ret;
}

void
remove_all_abort_methods(
	class method_env *__env, const lib::pos &__caller_pos)
{
	if (__env->aborted())
		throw std::runtime_error(abort_errmsg("remove_all_abort_methods"));

	__env->clear_abortlist();
}

edd_real32
round(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

#ifdef WIN32
	if (!::_finite(x))
	{
		__ret = x;
	}
	else
	{
		if (x >= 0.0)
		{
			float t = ::ceilf(x);
			if (t - x > 0.5)
				t -= 1.0;

			__ret = t;
		}
		else
		{
			float t = ::ceilf(-x);
			if (t + x > 0.5)
				t -= 1.0;

			__ret = -t;
		}
	}
#else
	__ret = ::roundf(x);
#endif

	return __ret;
}

edd_real64
round_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

#ifdef WIN32
	if (!::_finite(x))
	{
		__ret = x;
	}
	else
	{
		if (x >= 0.0)
		{
			double t = ::ceil(x);
			if (t - x > 0.5)
				t -= 1.0;

			__ret = t;
		}
		else
		{
			double t = ::ceil(-x);
			if (t + x > 0.5)
				t -= 1.0;

			__ret = -t;
		}
	}
#else
	__ret = ::round(x);
#endif

	return __ret;
}

void
save_on_exit(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->set_op_cache_state(__env->save);
}

edd_int32
save_values(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__env->set_op_cache_state(__env->save);
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}

edd_uint32
seconds_to_TIME_VALUE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 seconds)
{
	edd_uint32 __ret = 0;

	return seconds * 32000;

	return __ret;
}

edd_int32
send(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_string_ref &cmd_status)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, 0, true,
			 &cmd_status, NULL, NULL);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_send_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_without_trans(parent, "send", pos, targs[0], args[0]->pos);

	check_cmd48(parent, "send", pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
send_all_values(
	class method_env *__env, const lib::pos &__caller_pos)
{
	edd_int32 __ret = 0;

	__env->op_cache_flush(__env->send);
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}

edd_int32
send_command(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, 0,
			 false, NULL, NULL, NULL);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_send_command_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_without_trans(parent, "send_command", pos, targs[0], args[0]->pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
send_command_trans(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_int32 transaction)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, transaction, false,
			 NULL, NULL, NULL);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_send_command_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_with_trans(parent, "send_command_trans", pos,
				targs[0], args[0]->pos,
				targs[1], args[1]->pos);
}
#endif /* DOXYGEN_IGNORE */


void
send_on_exit(
	class method_env *__env, const lib::pos &__caller_pos)
{
	__env->set_op_cache_state(__env->send);
}

edd_int32
send_trans(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 cmd_number,
	edd_int32 transaction,
	edd_string_ref &cmd_status)
{
	edd_int32 __ret = 0;

	return hart_send(__env, __caller_pos, cmd_number, transaction, true,
			 &cmd_status, NULL, NULL);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_send_trans_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_cmd_nr_with_trans(parent, "send_trans", pos,
				targs[0], args[0]->pos,
				targs[1], args[1]->pos);

	check_cmd48(parent, "send_trans", pos);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
sgetval(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string_ref &str)
{
	edd_int32 __ret = 0;

	operand_ptr op;

	op = variable_getval(__env, __caller_pos,
			     __env->context_variable(),
			     __env->context_variable_value(),
			     exact_string);

	str = (std::string)(*op);
	__ret = bi_rc(__env->parent, BI_SUCCESS);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_sgetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::sgetval);
}
#endif /* DOXYGEN_IGNORE */


void
sie_set(
	class method_env *__env, const lib::pos &__caller_pos)
{
	builtins::method_set(__env, __caller_pos);
}

edd_real32
sin(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::sinf(x); 
	return __ret;
}

edd_real64
sin_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::sin(x); 
	return __ret;
}

edd_real32
sinh(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::sinhf(x); 
	return __ret;
}

edd_real64
sinh_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::sinh(x); 
	return __ret;
}

edd_real32
sqrt(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::sqrtf(x); 
	return __ret;
}

edd_real64
sqrt_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::sqrt(x); 
	return __ret;
}

edd_string
ssetval(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string value)
{
	edd_string __ret = "";

	edd::string_value v(value);
	operand_ptr op;

	op = variable_setval(__env, __caller_pos,
			     __env->context_variable(),
			     __env->context_variable_value(),
			     v,
			     exact_string);

	__ret = (std::string)(*op);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_ssetval_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	method->register_builtin(pos, edd::METHOD_BASE_tree::ssetval);
}
#endif /* DOXYGEN_IGNORE */


edd_int32
strcmp(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string s1,
	edd_string s2)
{
	edd_int32 __ret = 0;

 __ret = s1.compare(s2); 
	return __ret;
}

edd_string
strleft(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str,
	edd_int32 length)
{
	edd_string __ret = "";

 __ret = str.substr(0, length); 
	return __ret;
}

edd_int32
strlen(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str)
{
	edd_int32 __ret = 0;

 __ret = str.length(); 
	return __ret;
}

edd_string
strlwr(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str)
{
	edd_string __ret = "";

 __ret = lib::strlwr(str); 
	return __ret;
}

edd_string
strmid(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str,
	edd_int32 pos,
	edd_int32 len)
{
	edd_string __ret = "";

 __ret = str.substr(pos, len); 
	return __ret;
}

edd_string
strright(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str,
	edd_int32 length)
{
	edd_string __ret = "";

 __ret = str.substr(str.size()-length); 
	return __ret;
}

edd_string
strstr(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str,
	edd_string substr)
{
	edd_string __ret = "";

	edd_string::size_type pos = str.find(substr);

	if (pos != edd_string::npos)
		__ret = str.substr(pos);

	return __ret;
}

edd_string
strtrim(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str)
{
	edd_string __ret = "";

 __ret = lib::strtrim(str); 
	return __ret;
}

edd_string
strupr(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string str)
{
	edd_string __ret = "";

 __ret = lib::strupr(str); 
	return __ret;
}

edd_real32
tan(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::tanf(x); 
	return __ret;
}

edd_real64
tan_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::tan(x); 
	return __ret;
}

edd_real32
tanh(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = ::tanhf(x); 
	return __ret;
}

edd_real64
tanh_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = ::tanh(x); 
	return __ret;
}

edd_uint32
timet_to_TIME_VALUE(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_int32 timet)
{
	edd_uint32 __ret = 0;

	const time_t *tptr = (time_t *) &timet;

	struct tm *tm = localtime(tptr);

	unsigned long seconds = tm->tm_sec + 60*tm->tm_min + 3600*tm->tm_hour;

	return seconds * 32000;

	return __ret;
}

edd_int32
timet_to_string(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_string_ref &str,
	edd_string format,
	edd_int32 timet)
{
	edd_int32 __ret = 0;

	time_t tv;
	tv = (time_t) timet;

	struct tm *tm = localtime(&tv);

	char buf[1024];
	size_t ret = strftime(buf, 1024, format.c_str(), tm);

	str = buf;

	if (ret == 0)
		return -1;
	else
		return ret;

	return __ret;
}

edd_real32
trunc(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real32 x)
{
	edd_real32 __ret = 0.0;

 __ret = float(int(x)); 
	return __ret;
}

edd_real64
trunc_1(
	class method_env *__env, const lib::pos &__caller_pos,
	edd_real64 x)
{
	edd_real64 __ret = 0.0;

 __ret = int(x); 
	return __ret;
}

edd_int32
vassign(
	class method_env *__env, const lib::pos &__caller_pos,
	class tree * device_var,
	class tree * source_var)
{
	edd_int32 __ret = 0;

	__ret = builtins::assign_var(__env, __caller_pos, device_var, source_var);

	return __ret;
}
#ifndef DOXYGEN_IGNORE
void
check_vassign_0(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs)
{
	check_assign_var_0(parent, method, pos, args, targs);
}
#endif /* DOXYGEN_IGNORE */



} /* namespace builtins */
