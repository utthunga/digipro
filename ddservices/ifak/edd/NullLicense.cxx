
/* 
 * $Id: NullLicense.cxx,v 1.5 2009/05/19 12:35:33 fna Exp $
 * 
 * Copyright (C) 2005-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "NullLicense.h"


bool
NullLicense::have_checker_license()
{
	return false;
}

bool
NullLicense::have_workbench_license()
{
	return false;
}

bool
NullLicense::have_comm_license(int profile)
{
	return false;
}

std::string
NullLicense::get_error_msg() const
{
	return std::string("No licenses available");
}

long
NullLicense::get_error_code() const
{
	return -1;
}

std::string
NullLicense::get_expiration_info() const
{
	return std::string("Demo version");
}
