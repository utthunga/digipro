
/* 
 * $Id: device_io_pb.cxx,v 1.69 2009/08/31 12:19:46 fna Exp $
 * 
 * Copyright (C) 2003-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "device_io_pb.h"

// C stdlib
#include <assert.h>

// C++ stdlib

// other libs

// my lib
#include "sys.h"

// own header
#include "edd_ast.h"
#include "pbInterpreter.h"

#ifndef _
# define _(x) x
#endif


namespace edd
{

device_io_pb::device_io_pb(class Interpreter &parent)
: device_io(parent)
, directory_done_flag(false)
, cache_directory_flag(false)
, required_timeout(0)
{
	m_Num_PB = 0;
	m_Num_TB = 0;
	m_Num_FB = 0;
	m_Num_LO = 0;
}

int
device_io_pb::do_command(class method_env *env,
			 const class COMMAND_tree *cmd, int trans_nr, enum action action,
			 int &warnings)
{
	assert(cmd);

	if (!is_online())
	{
		error("Not online!");
		return -1;
	}

	int ret = 0;

	switch (action)
	{
		case action_read:
		{
			ret = do_read_command(cmd, trans_nr, env, warnings);
			break;
		}
		case action_write:
		{
			ret = do_write_command(cmd, trans_nr, env, warnings);
			break;
		}
		default:
		{
			switch (cmd->operation(env))
			{
				case EDD_COMMAND_OPERATION_READ:
					ret = do_read_command(cmd, trans_nr, env, warnings);
					break;
				case EDD_COMMAND_OPERATION_WRITE:
					ret = do_write_command(cmd, trans_nr, env, warnings);
					break;
				default:
					assert(0);
			}
			break;
		}
	}

	return ret;
}

void
device_io_pb::state_change(enum state newstate)
{
	if (newstate != mystate)
	{
		if (newstate == connected)
			pb_readdir();
	}

	device_io::state_change(newstate);
}

void
device_io_pb::dump_data(const class COMMAND_tree *cmd, const char *op,
			const unsigned char *buf, size_t buflen, const char *descr)
{
	lib::dyn_buf tmp(100);

	if (cmd && op)
		tmp.printf("COMMAND '%s' (%s): %s (hex): ", cmd->name(), op, descr);
	else
		tmp.printf("%s (hex): ", descr);

	for (size_t i = 0; i < buflen; i++)
		tmp.printf("%02x ", buf[i]);

	trace(tmp.c_str());
}

void 
device_io_pb::do_timeout(const class COMMAND_tree *cmd, const char *op,
			 long new_timeout)
{
	if (required_timeout > 0)
	{
		os_service::Timeval tmp(timestamp);
		os_service::Timeval waittime;

		timestamp.set_to_now();
		timestamp -= tmp;

		waittime.set(required_timeout / 1000, (required_timeout % 1000) * 1000);

		if (timestamp < waittime)
		{
			sys::uint32 ms;

			waittime -= timestamp;

			ms = waittime.to_seconds();
			ms *= 1000;
			ms += waittime.get_useconds() / 1000;

			trace(_("COMMAND '%s' (%s): Handling timeout, waiting %"  PRIu32  " ms!"),
				cmd->name(), op, ms);

			if (waittime.to_seconds())
				sys::sleep(waittime.to_seconds());

			if (waittime.get_useconds())
				sys::usleep(waittime.get_useconds());
		}
	}

	required_timeout = new_timeout;
}

int
device_io_pb::do_read_command(const class COMMAND_tree *cmd, int trans_nr,
			      class method_env *env, int &warnings)
{
	unsigned char slot, index;

	if (cmd->operation(env) != EDD_COMMAND_OPERATION_READ)
	{
		error(_("COMMAND '%s' is not of OPERATION READ, can't execute it as read COMMAND!"),
			cmd->name());
		return -1;
	}

	if (!pb_resolve_cmd(cmd, "read", env, slot, index))
	{
		error(_("COMMAND '%s' (read): pb_resolve_cmd failed, aborted"), cmd->name());
		return -1;
	}

	std::vector<class DATA_ITEM_tree *> items;

	try
	{
		items = cmd->reply_items(trans_nr, env);
	}
	catch (const RuntimeError &e)
	{
		error(_("COMMAND '%s' (read): RuntimeError while obtaining reply items, aborted"),
		      cmd->name());

		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (read): RuntimeError is: %s"), cmd->name(), msg.c_str());

		return -1;
	}

	if (can_optimize(env, items))
	{
		/* all variables are newer than the last age stamp */

		info(_("COMMAND '%s' (read) not executed, all items are up-to-date"), cmd->name());
		return 0;
	}

	unsigned char data[256];
	size_t expect, dynamic_expect;
	int32 timeout;

	if (read_encode(cmd, trans_nr, env, parent.logger, items,
			expect, dynamic_expect, timeout, warnings))
	{
		return -1;
	}

	if (expect+dynamic_expect > 240)
	{
		error(_("COMMAND '%s' (read): size of variables to large (%lu > 240)!"),
		      cmd->name(), (unsigned long)expect);

		return -1;
	}

	do_timeout(cmd, "read", timeout);

	if (dynamic_expect)
	{
		trace(_("COMMAND '%s' (read): slot %u, index %u, len 240 [variable-size: %lu + n * %lu]"),
			cmd->name(), slot, index, (unsigned long)expect, (unsigned long)dynamic_expect);
	}
	else
		trace(_("COMMAND '%s' (read): slot %u, index %u, len %lu"),
			cmd->name(), slot, index, (unsigned long)expect);

	struct request request;

	request.slot = slot;
	request.index = index;
	request.datalen = dynamic_expect ? 240 : expect;
	request.data = data;

	long rc = pb_read(request);

	if (required_timeout > 0)
		/* timeout required */
		timestamp.set_to_now();

	if (rc == 0)
	{
		dump_data(cmd, "read", request.data, request.datalen, _("Received"));

		if (dynamic_expect)
		{
			if (request.datalen < expect)
			{
				error(_("COMMAND '%s' (read) failed, expected %lu + n * %lu bytes, got %lu"),
					cmd->name(),
					(unsigned long)expect, (unsigned long)dynamic_expect,
					(unsigned long)request.datalen);

				rc = -1;
			}
			else if ((request.datalen - expect) % dynamic_expect)
			{
				error(_("COMMAND '%s' (read) failed, expected %lu + n * %lu bytes, got %lu [not aligned]"),
					cmd->name(),
					(unsigned long)expect, (unsigned long)dynamic_expect,
					(unsigned long)request.datalen);

				rc = -1;
			}
		}
		else if (expect != request.datalen)
		{
			error(_("COMMAND '%s' (read) failed, expected %lu bytes, got %lu"),
				cmd->name(),
				(unsigned long)expect,
				(unsigned long)request.datalen);

			rc = -1;
		}

		if (rc == 0)
		{
			if (read_decode(cmd, trans_nr, env, parent.logger, items,
					request.data, request.datalen))
			{
				rc = -1;
			}
		}
	}
	else
		error(_("COMMAND '%s' (read) failed (%li)"), cmd->name(), rc);

	return rc;
}

int
device_io_pb::do_write_command(const class COMMAND_tree *cmd, int trans_nr,
			       class method_env *env, int &warnings)
{
	unsigned char slot, index;

	if (cmd->operation(env) != EDD_COMMAND_OPERATION_WRITE)
	{
		error(_("COMMAND '%s' is not of OPERATION WRITE, can't execute it as write COMMAND!"),
			cmd->name());
		return -1;
	}

	if (!pb_resolve_cmd(cmd, "write", env, slot, index))
	{
		error(_("COMMAND '%s' (write): pb_resolve_cmd failed, aborted"), cmd->name());
		return -1;
	}

	std::vector<class DATA_ITEM_tree *> items;

	try
	{
		items = cmd->request_items(trans_nr, env);
	}
	catch (const RuntimeError &e)
	{
		error(_("COMMAND '%s' (write): RuntimeError while obtaining request items, aborted"),
		      cmd->name());

		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (write): RuntimeError is: %s"), cmd->name(), msg.c_str());

		return -1;
	}

	if (can_optimize(env, items))
	{
		/* all variables are newer than the last age stamp */

		info(_("COMMAND '%s' (write) not executed, all items are up-to-date"), cmd->name());
		return 0;
	}

	optimize_age_stamp_tick(env);

	unsigned char data[256];
	size_t datalen;
	int32 timeout;

	if (write_encode(cmd, trans_nr, env, parent.logger, items, data, sizeof(data), datalen, timeout))
		return -1;

	do_timeout(cmd, "write", timeout);

	trace(_("COMMAND '%s' (write): slot %u, index %u, len %lu"),
		cmd->name(), slot, index, (unsigned long)datalen);

	dump_data(cmd, "write", data, datalen, _("Send"));

	struct request request;

	request.slot = slot;
	request.index = index;
	request.datalen = datalen;
	request.data = data;

	long rc = pb_write(request);

	if (required_timeout > 0)
		/* timeout required */
		timestamp.set_to_now();

	if (rc != 0)
		error(_("COMMAND '%s' (write) failed (%li)"), cmd->name(), rc);

	write_decode(cmd, trans_nr, env, parent.logger, items, rc);

	return rc;
}

/**
 * Read and save directory.
 */
bool
device_io_pb::pb_readdir(void)
{
	/* if the EDD don't use relative commands
	 * we can skip reading the directory
	 */
	if (!parent.use_rel_cmds())
		return true;

	/* if we cache the directory and already have read it
	 * we can skip reading the directory
	 */
	if (cache_directory_flag && directory_done_flag)
		return true;

#define ENTRY_LEN       4
#define MAX_DIR_INDEX   8
#define MAX_DATA_LEN    240

	unsigned char dir_data[MAX_DATA_LEN * MAX_DIR_INDEX]; // directory management (DM)
	unsigned char buf[MAX_DATA_LEN];

	unsigned long Dir_ID;
	unsigned long Rev_Num;
	unsigned long Num_DirObj;
	unsigned long Num_DirEntry;
	unsigned long Fst_CListEntry;
	unsigned long Num_CListEntry;

	unsigned char Idx_PB;
	unsigned char Ofs_PB = 0;
	unsigned char Idx_TB;
	unsigned char Ofs_TB = 0;
	unsigned char Idx_FB;
	unsigned char Ofs_FB = 0;
	unsigned char Idx_LO;
	unsigned char Ofs_LO = 0;

	unsigned long read_direntry = 0;
	unsigned char ofs;
	size_t count = 0;
	long rc;

	struct request request;

	info(_("Read directory started ..."));
	on_progress_start(_("Read Directory"), true);

	/* set address of DIRECTORY_OBJECT_HEADER */
	request.slot	= 1;
	request.index	= 0;
	request.datalen = MAX_DATA_LEN;
	request.data	= buf;

	trace(_("Read Directory Header"));

	rc = pb_read(request);
	on_progress_activity();

	if (rc != 0 || request.datalen < 12)
	{
		/* data length must be higher than 12 Bytes */

		error(_("Directory Header - index 1, slot 0 returned %li (datalen %lu)"),
			rc, (unsigned long)request.datalen);

		goto failed;
	}

	dump_data(NULL, NULL, request.data, request.datalen, _("Received"));

	/* split the received data in 6 entries, each has 2 Bytes */
	Dir_ID		= 256 * buf[ 0] + buf[ 1];
	Rev_Num		= 256 * buf[ 2] + buf[ 3];
	/* used communication indizes for the following necessary entries (slot 0) */
	Num_DirObj	= 256 * buf[ 4] + buf[ 5];
	/* number of all entries in the following indizes (each entry has 4 Bytes) */
	Num_DirEntry	= 256 * buf[ 6] + buf[ 7];
	/* directory entry number of the first COMP_LIST_ENTRY (each entry use 4 Bytes) */
	Fst_CListEntry	= 256 * buf[ 8] + buf[ 9];
	/* number of different entries (COMP_LIST_ENTRY) in directory (PB, TB, FB, Link Objects) */
	Num_CListEntry	= 256 * buf[10] + buf[11];

	if (Num_CListEntry < 3 || Num_CListEntry > 4)
	{
		error(_("Directory Header: value of Num_Comp_List_Dir_Entry is not correct, "
			"expected value is 3 or 4, actual length is %lu"), Num_CListEntry);

		goto failed;
	}

	if (Fst_CListEntry != 1)
	{
		error(_("Directory Header: value of First_Comp_List_Dir_Entry is not correct, "
			"expected value is 1, actual length is %lu"), Fst_CListEntry);

		goto failed;
	}

	/* entry offset of all following data in directory (each entry use 4 bytes)
	 * we assume no gaps at the start
	 * otherwise: ofs = (Fst_CListEntry - 1) * ENTRY_LEN;
	 */
	ofs = 0;

	/* read all indizes (necessary Num_DirObj)
	 * last used index is 8 (9..13 reserved)
	 */
	for (size_t i = 1; (i <= MAX_DIR_INDEX) && (rc == 0); i++)
	{
		request.slot	= 1;
		request.index	= i;
		request.datalen = MAX_DATA_LEN;
		request.data	= buf;

		trace(_("Read Directory Index %u"), request.index);

		rc = pb_read(request);
		on_progress_activity();

		if (rc != 0 && i <= Num_DirObj)
		{
			error(_("Read Directory Index %u, Slot %u returned %li (datalen %lu)"),
				request.slot, request.index, rc, (unsigned long)request.datalen);

			goto failed;
		}

		if (rc == 0)
		{
			/* index is readable */

			dump_data(NULL, NULL, request.data, request.datalen, _("Received"));

			if (i == 1)
			{
				/* Composite_List_Directory_Entries must be in index 1 */

				if (request.datalen < (Num_CListEntry * ENTRY_LEN))
				{
					/* all data for Composite_List_Directory_Entries must be in Index 1 */

					error(_("Directory Calculation: length of index 1 is to short, "
						"actual length is %lu expected length is %lu"),
						(unsigned long)request.datalen,
						Num_CListEntry * ENTRY_LEN);

					goto failed;
				}

				Idx_PB   =       request.data[ofs + 0];
				Ofs_PB   =       request.data[ofs + 1];
				m_Num_PB = 256 * request.data[ofs + 2] + request.data[ofs + 3];

				Idx_TB   =       request.data[ofs + 4];
				Ofs_TB   =       request.data[ofs + 5];
				m_Num_TB = 256 * request.data[ofs + 6] + request.data[ofs + 7];

				Idx_FB   =       request.data[ofs + 8];
				Ofs_FB   =       request.data[ofs + 9];
				m_Num_FB = 256 * request.data[ofs +10] + request.data[ofs +11];

				if (Num_CListEntry > 3)
				{
					/* Link Object are optional in devices */

					Idx_LO   =       request.data[ofs + 8];
					Ofs_LO   =       request.data[ofs + 9];
					m_Num_LO = 256 * request.data[ofs +10] + request.data[ofs +11];
				}
				else
				{
					Idx_LO   = 0;
					Ofs_LO   = 0;
					m_Num_LO = 0;
				}

				m_PB_Parameter.resize(m_Num_PB+1);
				m_TB_Parameter.resize(m_Num_TB+1);
				m_FB_Parameter.resize(m_Num_FB+1);
				m_LO_Parameter.resize(m_Num_LO+1);
			}

      			/* save the data in one array for evaluation */
			for (size_t t = 0; t < request.datalen; ++t)
				dir_data[count++] = request.data[t];

			read_direntry += request.datalen;

			/* if the read the expected number of directory
			 * entries we can stop reading
			 */
			if (read_direntry >= (Num_DirEntry * 4))
				break;
		}
	}

	trace(_("Following Entries found:"));

	if (!finalize_parameter(dir_data, count, m_PB_Parameter, m_Num_PB, Ofs_PB, "Physical Block"))
		goto failed;

	if (!finalize_parameter(dir_data, count, m_TB_Parameter, m_Num_TB, Ofs_TB, "Transducer Block"))
		goto failed;

	if (!finalize_parameter(dir_data, count, m_FB_Parameter, m_Num_FB, Ofs_FB, "Function Block"))
		goto failed;

	if (!finalize_parameter(dir_data, count, m_LO_Parameter, m_Num_LO, Ofs_LO, "Link Object"))
		goto failed;

	info(_("Read directory finished ok"));
	on_progress_activity();
	on_progress_end();

	directory_done_flag = true;

	return true;

failed:
	info(_("Read directory failed"));
	on_progress_activity();
	on_progress_end();

	return false;
}

bool
device_io_pb::finalize_parameter(const unsigned char *dir_data, int dir_data_len,
				 m_Parameter_t &m_Parameter, unsigned short m_Num,
				 unsigned char Ofs, const char *descr)
{
	for (size_t i = 1; i <= m_Num; i++)
	{
		/* calculate the start of the entry */
		unsigned char ofs = ((Ofs - 1) + (i - 1)) * ENTRY_LEN;

		if (ofs + 3 < dir_data_len)
		{
			m_Parameter[i].Slot    =       dir_data[ofs];
			m_Parameter[i].Index   =       dir_data[ofs+1];
			m_Parameter[i].Num_Par = 256 * dir_data[ofs+2] + dir_data[ofs+3];

			trace(_("%lu: %s found on Slot %u Index %u"),
				(unsigned long)i,
				descr,
				m_Parameter[i].Slot,
				m_Parameter[i].Index);
		}
		else
		{
			error(_("Directory Calculation: not enough data in directory "
				"for address of %s: %lu"), descr, (unsigned long)i);

			return false;
		}
	}

	return true;
}

bool
device_io_pb::pb_resolve_cmd(const class COMMAND_tree *cmd, const char *op,
			     class method_env *env,
			     unsigned char &slot, unsigned char &index)
{
	bool index_ok = false;
	bool slot_ok = false;

	slot = 0;
	index = 0;

	if (cmd) try
	{
		long ret;

		ret = cmd->index(env);
		if (ret != -1)
		{
			assert(ret >= 0 && ret <= 255);

			index = (unsigned char)(ret);
			index_ok = true;
		}

		ret = cmd->slot(env);
		if (ret != -1)
		{
			/* absolute addressing */

			assert(ret >= 0 && ret <= 255);

			slot = (unsigned char)(ret);
			slot_ok = true;
		}
		else
		{
			/* relative addressing */

			class BLOCK_tree *block;

			block = cmd->block(env);
			if (block)
			{
				int bn = block->number(env);

				switch (block->type())
				{
					case EDD_BLOCK_TYPE_PHYSICAL:
					{
						if (bn <= m_Num_PB)
						{
							index += m_PB_Parameter[bn].Index;
							slot = m_PB_Parameter[bn].Slot;
							slot_ok = true;
						}

						break;
					}
					case EDD_BLOCK_TYPE_TRANSDUCER:
					{
						if (bn <= m_Num_TB)
						{
							index += m_TB_Parameter[bn].Index;
							slot = m_TB_Parameter[bn].Slot;
							slot_ok = true;
						}

						break;
					}
					case EDD_BLOCK_TYPE_FUNCTION:
					{
						if (bn <= m_Num_FB)
						{
							index += m_FB_Parameter[bn].Index;
							slot = m_FB_Parameter[bn].Slot;
							slot_ok = true;
						}

						break;
					}
					default:
						/* never reached */ assert(0);
						break;
				}
			}
		}
	}
	catch (const RuntimeError &e)
	{
		std::string msg(parent.errmsg(e));

		error(_("COMMAND '%s' (%s): %s"), cmd->name(), op, msg.c_str());
	}

	return (slot_ok && index_ok);
}

} /* namespace edd */
