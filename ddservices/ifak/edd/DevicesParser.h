
/* 
 * $Id: DevicesParser.h,v 1.7 2009/05/07 20:48:29 fna Exp $
 * 
 * Copyright (C) 2007 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _DevicesParser_h
#define _DevicesParser_h

// C stdlib

// C++ stdlib
#include <list>
#include <map>
#include <string>

// other libs

// own header
#include "stream.h"


namespace edd
{

class DevicesParser
{
private:
	typedef DevicesParser Self;

	/* no copy/assignment constructor semantic defined */
	DevicesParser(const Self &);
	Self& operator=(const Self &);

public:
	DevicesParser(void);

	int lookup_manufacturer_name(const std::string &) const;
	int lookup_device_type_name(int manufacturer_id, const std::string &) const;
	std::string lookup_manufacturer_id(int manufacturer_id) const;
	std::string lookup_device_type_id(int manufacturer_id, int device_type_id) const;

	void load(const lib::stream &);
	void clear(void);

private:
	typedef std::map<int, std::string> devices_t;

	struct manufacturer
	{
		std::string ident;
		devices_t devices;
	};
	typedef std::map<int, struct manufacturer> manufacturers_t;

	/** The parsed devices file. */
	manufacturers_t entries;
	/** Strict checking flag */
	bool strict_checking;

	bool valid(const manufacturers_t::iterator &iter) const { return (iter != entries.end()); }
	bool valid(const manufacturers_t::const_iterator &iter) const { return (iter != entries.end()); }

	/** current input line */
	unsigned long line;
	/** read bytes */
	size_t bytes;
	/** the next input character */
	int c;

	/* read the next input character */
	void get_next_char(const lib::stream &);

	/* main parser */
	void parse(const lib::stream &f);

	/* helper */
	void expect(const lib::stream &f, char expected);
	void expect_ident(const lib::stream &f, std::string &ident);
	void expect_integer(const lib::stream &f, int &val);
	void read_ident(const lib::stream &f, std::string &ident);
	void read_integer(const lib::stream &f, int &val);
	void skip_space(const lib::stream &f, std::string &skipped);
	void skip_c_comment(const lib::stream &f, std::string &skipped);
	void skip_line(const lib::stream &f, std::string &skipped);
	bool check_eol(const lib::stream &f);
};

} /* namespace edd */

#endif /* _DevicesParser_h */
