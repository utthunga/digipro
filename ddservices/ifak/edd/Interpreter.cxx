
/* 
 * $Id: Interpreter.cxx,v 1.42 2009/11/19 15:51:22 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "pbInterpreter.h"

// C stdlib
#include <stdio.h>

// C++ stdlib
#include <memory>
#include <dirent.h>

// my lib
#include "mstream.h"
#include "paths.h"
#include "string.h"
#include "strtoxx.h"
#include "dirent.h"
#include "statxx.h"
#include "dirent_edd.h"

// own header
#include "edd_ast.h"
#include "edd_values.h"


namespace edd
{

/**
 * \page interface Overview of the programmer interface
 * 
 * \section interface_intro Introduction
 * 
 * The main entry point of using the EDD parser and AST is the
 * edd::Interpreter class. This class inherits the basic EDD library
 * routines from the edd::edd_tree class (mainly the scanning, parsing, AST
 * construction, syntactic and semantic checking). It additionally features
 * methods for easily accessing or evaluating the AST and standard conversion
 * routines.
 * \par
 * The edd::edd_tree is a superclass and include lot of other classes for
 * managing the AST and related things like identifiers, cross references of
 * identifiers to the corresponding AST object, error messages, the
 * preprocessor, scanner, parser and so on.
 * \par
 * In the following text these class family will be generally referenced as
 * EDD library.
 * 
 * \section interface_tree The AST structure
 * 
 * During parsing the EDD specification the AST of the EDD is
 * constructed. The AST is the syntax free runtime representation of the
 * EDD specification. In general the AST is a tree with nodes. The nodes
 * are either terminal or non terminal nodes. A terminal node terminate the
 * tree and describe a number, string or identifier and so on. Non terminal
 * nodes describe complex entities and have a number of attributes. The
 * attributes of a non terminal node are also tree nodes (either terminal or
 * non terminal). At last tree nodes can be concatenated to lists.
 * 
 * \dotfile "tree_structure.dot"
 * 
 * \section interface_tree_nodes The tree nodes
 * 
 * All tree nodes belong to a polymorphic class family. The base class for all
 * tree nodes is the edd::tree class. This class include all generic data and
 * features of a tree node. This include for example the edd::tree::kind
 * attribute that is constant for every node and never change after
 * instantiation. The edd::tree::kind attribute uniquely describe the concrete
 * class type of the node. It can be used to safely cast a generic tree
 * pointer to the right specialized object pointer. Additional features of
 * every tree node are the edd::tree::pos that reference the corresponding
 * source of the tree node (file, line, column) or the edd::tree::name()
 * method that return the identifier of this node if one is available
 * (otherwise an empty string is returned).
 * 
 * \section interface_usage Using the EDD library.
 * 
 * The EDD library class is designed to be a base class for your own
 * application. You can also create an instance of the library directly and
 * use it. This have the disadvantage of limited access to the AST and some
 * other helper objects. On the other side using the EDD library as base
 * class have the disadvantage of the fact that the ddl-project is still in
 * progress and some parts of it (also of the programming interface) are
 * subject of change.
 * 
 * \par
 * Simple example for use the EDD library as EDD checker:
 * 
 * \include "examples/checker.cxx"
 * 
 * \par
 * Advanced example. The EDD parser is used as base class for a new
 * class. As demonstration an EDD specification is parsed and all
 * Variables with it's line number are printed:
 * 
 * \include "examples/mymanager.cxx"
 * 
 * \section interface_search_ident Searching for tree nodes
 * 
 * This can be easily done through the edd::Interpreter::node_lookup method
 * that access the EDD cross reference hash table:
 *
 * \code
 * 
 * 	class Interpreter edd;
 * 	class node_tree *t;
 * 
 * 	... initialize edd ...
 * 
 * 	t = edd.node_lookup("var1");
 * 	if (t)
 * 	{
 * 		... found ...
 * 	}
 * 	else
 * 	{
 * 		... not found ...
 * 	}
 * 
 * \endcode
 * 
 * \section interface_attribute_access Accessing attributes
 * 
 * Attributes are child nodes of a node. They can be accessed in a generic way
 * on all nodes that are subclasses of the edd::node_tree class. The
 * edd::node_tree class itself inherit from edd::tree and is inherited from
 * all non terminal nodes. The edd::node_tree::attr() method accept a number
 * and return the corresponding tree pointer of the attribute. If the
 * attribute is missing 0 is returned.
 * \par
 * To access for example the INDEX attribute of a COMMAND the following code
 * can be used:
 *
 * \code
 * 
 * 	class Interpreter edd;
 * 	class node_tree *t;
 * 
 * 	...
 * 
 * 	t = edd.node_lookup("command_1");
 * 	if (t && t->kind == EDD_COMMAND)
 * 	{
 * 		class tree *index;
 * 
 * 		index = t->attr(EDD_COMMAND_ATTR_INDEX);
 * 		if (index)
 * 		{
 * 			...
 * 		}
 * 	}
 * 
 * \endcode
 * 
 * EDD_COMMAND and EDD_COMMAND_ATTR_INDEX are constants that are defined in
 * the "edd_ast.h" file.
 *
 * \note You must check the concrete node type. If the concrete node type
 * differs you can't access the attributes as another node type have other
 * attributes. In case of error an execption is thrown.
 * 
 * \section interface_exception The exception model
 * 
 * To reduce the error checking overhead C++ exceptions are heavily used. In
 * general it's assumed that the attempted operation is valid and perform
 * fine. In case of any error (this can be memory allocation errors, out of
 * range errors, type mismatch errors, runtime errors) an exception is
 * thrown. This convention apply to all methods that are involved into AST
 * accessing or management. This apply specially to the edd::tree::eval()
 * method and all methods that use this. The thrown exception can be easily
 * catched. It's of type edd::RuntimeError. This error can be transformed into
 * a human readable for that can be presented to the end user.
 * 
 */

Interpreter::Interpreter(void)
: primary_component(NULL)
{
}

Interpreter::~Interpreter(void)
{
}

char *
Interpreter::check_name(char *line, const char *name)
{
	const size_t len = strlen(name);

	if (!strncmp(line, name, len))
		return line + len;

	return NULL;
}

char *
Interpreter::check_entry(char *line)
{
	while (*line)
	{
		char c = *line ++;

		if (c == '=')
		{
			char *start = strchr(line, '"');
			char *end = strrchr(line, '"');

			if (start && end && start != end)
			{
				start++;
				*end = '\0';

				return start;
			}

			break;
		}
	}

	return NULL;
}

/**
 * Parse a .Devices file.
 * Open and parse a .Devices file. On success it return 0 and store the
 * EDD filename in the edd_file argument. Found dictionary files are
 * appended to the dictionaries vector.
 *
 * Diagnostic messages are written to the log argument if not NULL.
 */
int
Interpreter::parse_Devices(const std::string &path,
			   std::vector<std::string> &includes,
			   std::vector<std::string> &dictionaries,
			   std::string &edd_file,
			   std::string &gsd_file,
			   lib::stream *log)
{
	FILE *f;
	int i = 0;

	f = fopen(path.c_str(), "r");
	if (f)
	{
		char buf[4096];
		int buflen = sizeof(buf);
		char *s;
		unsigned long line = 1;

		s = fgets(buf, buflen, f);
		while (s)
		{
			char *s1;

			if ((s1 = check_name(s, "DDL")))
			{
				if ((s1 = check_entry(s1)))
				{
					edd_file = s1;
				}
				else
				{
					if (log)
					{
						log->printf("%s: %5lu,%3lu: ", path.c_str(), line, 0UL);
						log->printf("Error       : incorrect DDL=\"...\" entry\n");
					}
					i++;
				}
			}
			else if ((s1 = check_name(s, "DicFile")))
			{
				if ((s1 = check_entry(s1)))
				{
					dictionaries.push_back(s1);
				}
				else
				{
					if (log)
					{
						log->printf("%s: %5lu,%3lu: ", path.c_str(), line, 0UL);
						log->printf("Error       : incorrect DicFile=\"...\" entry\n");
					}
					i++;
				}
			}
			else if ((s1 = check_name(s, "GSD")))
			{
				if ((s1 = check_entry(s1)))
				{
					gsd_file = s1;
				}
				else
				{
					if (log)
					{
						log->printf("%s: %5lu,%3lu: ", path.c_str(), line, 0UL);
						log->printf("Error       : incorrect GSD=\"...\" entry\n");
					}
					i++;
				}
			}

			s = fgets(buf, buflen, f);
			line++;
		}

		fclose(f);
	}
	else
	{
		if (log) log->printf("FATAL       : no such file \"%s\"\n", path.c_str());
		i++;
	}

	if (!edd_file.length())
		/* if we don't found a DDL entry we return an error */
		i++;

	return i;
}

/**
 * \internal
 */
void
Interpreter::lookup_dirs0(const std::string &path,
			  std::vector<std::string> &includes,
			  bool bottom_up,
			  lib::stream *log,
			  size_t level)
{
	if (level > 128)
	{
		if (log)
		{
			log->put("Error       : directory hierachy to deep, ");
			log->printf("can't process \"%s\"\n", path.c_str());
		}

		return;
	}

	sys::DIR *dir = sys::opendir(path);

	if (dir)
	{
		std::string entry;

		while (!(entry = sys::readdir(dir)).empty())
		{
			const char *entry_str = entry.c_str();

			/* ignore '.' and '..' */
			if (entry_str[0] == '.' && (entry_str[1] == '\0'
			    || (entry_str[1] == '.' && entry_str[2] == '\0')))
				continue;

			std::string found;

			found.reserve(path.size() + entry.size() + 10);
			found = path;
			found += lib::pathseparator();
			found += entry;

			sys::statxx st;
			int ret;

			ret = sys::stat(found, st);
			if (ret == 0)
			{
				if (st.isdir())
				{
					if (!bottom_up)
						includes.push_back(found);

					lookup_dirs0(found, includes, bottom_up, log, level+1);

					if (bottom_up)
						includes.push_back(found);
				}
			}
			else if (log)
				log->printf("Error       : can't stat \"%s\"\n", entry.c_str());
		}

		sys::closedir(dir);
	}
	else if (log)
		log->printf("Error       : can't open \"%s\"\n", path.c_str());
}

/**
 * Lookup and collect recursivly directories.
 * From the starting point path are recursivly all directories
 * collected and saved in the vector includes.
 * \param path The start path to search for directories.
 * \param includes The result vector to save all found directories.
 * \param bottom_up If true insert found directories in bottump up order,
 * top down order otherwise.
 * \param log Error report stream for failure messages, can be NULL.
 */
void
Interpreter::lookup_dirs(const std::string &path,
			   std::vector<std::string> &includes,
			   bool bottom_up,
			   lib::stream *log)
{
	lookup_dirs0(path, includes, bottom_up, log, 1);
}

/**
 * Set default \#defines.
 * This set some \#defines as I think it's used by PDM. There exist no
 * documentation about this, this is pure intuition.
 */
void
Interpreter::pdm_defines(std::vector<std::string> &defines)
{
	defines.push_back("_PDM_");
	defines.push_back("_V50_");
}

/**
 * Collect EDD objects.
 * All EDD objects of type kind are collected into an array that is returned
 * to the caller.
 */
std::vector<class EDD_OBJECT_tree *>
Interpreter::allobjects(int kind) const
{
	std::vector<class EDD_OBJECT_tree *> ret;
	size_t size = 0;
	class tree *t;

	t = root ? root->definition_list() : NULL; //Root tree
	while (t)
	{
		if (t->kind == kind)
			++size;

		t = t->next;
	}

	t = primary_component ? primary_component->attr(EDD_COMPONENT_ATTR_DECLARATION) : NULL;
	while (t)
	{
		if (t->kind == kind)
		{
			++size;
		}
		t = t->next;
	}

	t = primary_component ? primary_component->attr(EDD_COMPONENT_ATTR_COMPONENTS) : NULL;
	while(t)
	{
		class node_tree *node = t->is_node();
		class tree *t1 = node->attr(EDD_COMPONENT_ATTR_DECLARATION);
		while (t1)
		{
			if (t1->kind == kind)
				++size;

			t1 = t1->next;
		}

		t = t->next;
	}

	if (size)
	{
		ret.reserve(size);

		assert(root);

		t = root->definition_list();
		while (t)
		{
			if (t->kind == kind)
				ret.push_back((class EDD_OBJECT_tree *) t);

			t = t->next;
		}

		t = primary_component ? primary_component->attr(EDD_COMPONENT_ATTR_DECLARATION) : NULL;
		while (t)
		{
			if (t->kind == kind)
				ret.push_back((class EDD_OBJECT_tree *) t);

			t = t->next;
		}

		t = primary_component ? primary_component->attr(EDD_COMPONENT_ATTR_COMPONENTS) : NULL;
		while(t)
		{
			class node_tree *node = t->is_node();
			class tree *t1 = node->attr(EDD_COMPONENT_ATTR_DECLARATION);
			while (t1)
			{
				if (t1->kind == kind)
					ret.push_back((class EDD_OBJECT_tree *) t1);

				t1 = t1->next;
			}

			t = t->next;
		}
	}

	return ret;
}

/**
 * Calculate the current menuitems of a menu tree node.
 * This method calculate the current menuitem vector of a EDD menu object.
 * The result is a vector of menuitems. Each menuitem is identified by it's
 * item tree object inside the menu object and the resulting tree reference to
 * the EDD object.
 * \param t The menu tree pointer. Invalid or non menu tree pointers result
 * in an exception.
 * \param env The environment under which the interpreter should operate. 
 * \return The vector of the current menuitems. Note that these items can
 * change during runtime due to the dynamic semantic of the EDD language.
 * If there are no items inside the menu a zero sized vector is returned.
 * \exception edd::RuntimeError() in case of invalid pointer or incorrect
 * menu item pointer.
 * \exception edd::RuntimeError() in case of reference evaluation errors or
 * other runtime dependant things.
 */
Interpreter::menuitems_t
Interpreter::evalmenu(class tree *t, class method_env *env) const
{
	menuitems_t ret;

	if (t)
	{
		assert(t->kind == EDD_MENU);

		menuitem_t item;

		operand_ptr list(t->eval(env));

		ret.reserve(list.len());

		while (list)
		{
			class tree *l = *list;
			assert(l && l->kind == EDD_MENU_ITEM);

			class MENU_ITEM_tree *mi = (class MENU_ITEM_tree *) l;

			try
			{
				item.ref = mi->deref(env);
				item.item = mi;

				/* remember environment */
				item.env = env;

				ret.push_back(item);
			}
			catch (const ComponentRangeError &)
			{
				/* ignore non-existant modules */
			}

			list = list.next();
		}
	}

	return ret;
}

/**
 * Search all command tree objects that reference the variable tree object
 * var.
 * This method do a search over all command tree objects of the abstract
 * syntax tree. If a command object reference the variable object represented
 * by the tree pointer var inside the command operation subtree specified by
 * the argument op, it's included into the result vector.
 * \note The result vector is unsorted.
 * \param object The EDD Object tree pointer that should be searched for
 * inside the command objects. An invalid pointer result in an exception.
 * \param op The operation list of the command that should be searched for. op
 * must be of type EDD_COMMAND_OPERATION_READ or EDD_COMMAND_OPERATION_WRITE.
 * \param env The environment under which the interpreter should operate. 
 * \return A vector of all command tree pointers that match the search
 * criteria.
 * \exception edd::RuntimeError() in case of invalid or incorrect variable tree
 * pointer.
 * \exception edd::RuntimeError() in case of unknown op argument.
 * \exception edd::RuntimeError() in case of reference evaluation errors or
 * other runtime dependant things.
 */
Interpreter::command_t
Interpreter::search_command(class tree *object, unsigned long op, class method_env *env)
{
	/*
	 * Check arguments
	 */
	assert(root);

	assert(object);
	assert(object->kind == EDD_VARIABLE ||
	       object->kind == EDD_VALUE_ARRAY ||
	       object->kind == EDD_LIST);

	assert(op == EDD_COMMAND_OPERATION_READ || op == EDD_COMMAND_OPERATION_WRITE);


	COMMAND_tree::search_info_t search_info;
	std::vector<class EDD_OBJECT_tree *> commands = allobjects(EDD_COMMAND);
	const size_t commands_size = commands.size();

	/*
	 * First step
	 *
	 * - do a simple simple search
	 */

	for (size_t i = 0, i_max = commands_size; i < i_max; ++i)
			((class COMMAND_tree *)commands[i])->search_io_object(object, op, env, search_info);


	/*
	 * Second step
	 * 
	 * - if no optimal COMMAND is found, search again under the viewpoint
	 *   of INDEX based references
	 */
	if (!search_info.optimal_found() && object->kind == EDD_VARIABLE)
	{
		for (size_t i = 0, i_max = commands_size; i < i_max; ++i)
				((class COMMAND_tree *)commands[i])->search_io_variable(object, op, env, search_info);
	}

	/*
	 * Determine best fitting result vector
	 */
	std::vector<COMMAND_tree::command_info_t> *info_ptr = NULL;

	if (search_info.optimal.size())
	{
		info_ptr = &search_info.optimal;
	}
	else if (search_info.suboptimal.size())
	{
		info_ptr = &search_info.suboptimal;

		logger.log_warning("I/O",
				   "Only suboptimal COMMAND %s found for %s %s %s",
				   search_info.suboptimal[0].command->name(),
				   (op == EDD_COMMAND_OPERATION_READ) ? "reading" : "writing",
				   object->descr(),
				   object->name());
	}

	if (!info_ptr)
	{
		/* no COMMAND found */

		command_t ret;

		ret.command = NULL;
		ret.transaction = 0;

		return ret;
	}

	/*
	 * Final step
	 *
	 * - lookup COMMAND with smallest I/O size
	 * - assign INDEX VARIABLEs if needed
	 */

	const std::vector<COMMAND_tree::command_info_t> &info = *info_ptr;
	assert(!info.empty());

	size_t index = 0;

	if (info.size() > 1)
	{
		/* lookup COMMAND with smallest I/O size */

		size_t size = info[0].command->transaction_io_size(info[0].transaction);

		for (size_t i = 1, i_max = info.size(); i < i_max; ++i)
		{
			size_t tmp = info[i].command->transaction_io_size(info[i].transaction);

			if (tmp < size)
			{
				index = i;
				size = tmp;
			}
		}
	}

	const COMMAND_tree::command_info_t &command_info = info[index];

	if (!command_info.index_vars.empty())
	{
		/* assign INDEX VARIABLEs if needed */

		const std::vector<class VARIABLE_tree *> &index_vars = command_info.index_vars;
		const std::vector<int> &index_values = command_info.index_values;

		assert(index_values.size() <= index_vars.size());

		for (size_t i = 0, i_max = index_values.size(); i < i_max; ++i)
		{
			if (index_values[i] != -1)
			{
				operand_ptr op(index_vars[i]->value(env));
				int32_value v(index_values[i]);

				*op = v;
			}
		}
	}

	command_t ret;

	ret.command = command_info.command;
	ret.transaction = command_info.transaction;

	return ret;
}

/**
 * Eval EDD_OBJECT to XML description.
 * Evaluate the EDD_OBJECT specified by id to a XML style representation.
 * Can be used to transport the EDD information to a remote GUI.
 * \note See edd::EDD_OBJECT_tree::xml_eval for a detailed description
 * of the generated XML outpout!
 * \param env The edd::method_env to use for the XML creation or NULL
 * if the offline data set should be used.
 * \param id The id of the EDD_OBJECT, previously obtained with
 * edd::EDD_OBJECT_tree::id.
 * \param recurse flag to indicate to work recursivly.
 * \return The XML style representation.
 * \exception edd::RuntimeError() in case of an invalid id.
 */
std::string
Interpreter::xml_eval(class method_env *env, unsigned int id, bool recurse)
{
	class EDD_OBJECT_tree *object = id2tree(id);

	if (!object)
	{
		assert(0);
		throw RuntimeError();
	}

	return object->xml_eval(env, recurse);
}

/**
 * Eval EDD_OBJECT to XML description.
 * Evaluate the EDD_OBJECT specified by the name to a XML style representation.
 * Can be used to transport the EDD information to a remote GUI.
 * \note See edd::EDD_OBJECT_tree::xml_eval for a detailed description
 * of the generated XML outpout!
 * \param env The edd::method_env to use for the XML creation or NULL
 * if the offline data set should be used.
 * \param name The name of the EDD_OBJECT.
 * \param recurse flag to indicate to work recursivly.
 * \return The XML style representation.
 * \exception edd::RuntimeError() in case of an invalid name.
 */
std::string
Interpreter::xml_eval(class method_env *env, const std::string &name, bool recurse)
{
	class EDD_OBJECT_tree *object = node_lookup(name);

	if (!object)
		throw RuntimeError(xxIdentifierUnknown, lib::NoPosition, ident().create(name));

	return object->xml_eval(env, recurse);
}

/**
 * \internal
 */
operand_ptr
Interpreter::xml_parse_access_path(class method_env *env, std::string path)
{
	operand_ptr op;

	std::string element;
	element.reserve(16);

	for (std::string::size_type i = 0, i_max = path.size(); i < i_max; ++i)
	{
		int c = path[i];

		if (isalnum(c) || c == '_')
		{
			element += c;

			if (i+1 == i_max)
			{
				class EDD_OBJECT_tree *object;

				object = node_lookup(ident().create(lib::strrev(element)));
				if (!object)
					throw RuntimeError(xxInvalidInput);

				op = object->value(env);
				break;
			}
		}
		else if (c == '.')
		{
			if (element.empty())
				throw RuntimeError(xxInvalidInput);

			op = xml_parse_access_path(env, path.substr(i+1));
			op = op->member(ident().create(lib::strrev(element)));
			break;
		}
		else if (c == ']')
		{
			if (!element.empty())
				throw RuntimeError(xxInvalidInput);

			std::string::size_type j = i + 1;

			while (path[j] != '[')
			{
				c = path[j];

				if (!isdigit(c))
					throw RuntimeError(xxInvalidInput);

				element += c;

				++j;
				if (j >= i_max)
					break;
			}

			if (element.empty() || j >= i_max)
				throw RuntimeError(xxInvalidInput);

			element = lib::strrev(element);
			int32 index;

			if (lib::dec_ascii_to_int(index, element.c_str(), element.size()))
				throw RuntimeError(xxInvalidInput);

			op = xml_parse_access_path(env, path.substr(j+1));
			op = (*op)[long(index)];
			break;
		}
	}

	if (!op)
		throw RuntimeError(xxInvalidInput);

	assert(op->getenv() == env);
	return op;
}

/**
 * Assign value back as specified by xml_eval.
 * \param env The edd::method_env to use for the value assign or NULL
 * if the offline data set should be used.
 * \param access_path The access path as specified inside the XML output.
 * \param value The new value that should be assigned.
 * \exception edd::RuntimeError() in case of an invalid access_path or
 * value.
 */
void
Interpreter::xml_assign(class method_env *env, std::string access_path, const std::string &value)
{
	operand_ptr op(xml_parse_access_path(env, lib::strrev(access_path)));

	op->pretty_scan(value);
}

/**
 * Search all unit relation tree objects that reference the unit reference
 * object unit_ref.
 * This method do a search over all unit relation tree objects of the abstract
 * syntax tree. If a unit relation object reference the unit reference object
 * represented by the tree pointer unit_ref, it's included into the result
 * vector.
 * \note The result vector is unsorted.
 * \param unit_ref The unit reference object tree pointer that should be
 * searched for inside the unit relation objects. An invalid pointer result
 * in an exception.
 * \param env The environment under which the interpreter should operate. 
 * \return A vector of all unit relation tree pointers that match the search
 * criteria.
 * \exception edd::RuntimeError() in case of invalid or incorrect variable tree
 * pointer.
 * \exception edd::RuntimeError() in case of reference evaluation errors or
 * other runtime dependant things.
 */
Interpreter::unit_relations_t
Interpreter::search_unit_relations(class tree *unit_ref, class method_env *env) const
{
	if (!root || !unit_ref)
	{
		assert(0);
		throw RuntimeError();
	}

	unit_relations_t ret;
	class tree *list;

	ret.reserve(32);

	list = root->definition_list();
	while (list)
	{
		if (list->kind == EDD_UNIT_RELATION)
		{
			class UNIT_RELATION_tree *relation = (class UNIT_RELATION_tree *)(list);

			if (relation->search_unit_ref(unit_ref, env))
				ret.push_back(relation);
		}

		list = list->next;
	}

	return ret;
}

/**
 * Helper function to load all members of all FILE objects.
 * This method search all FILE objects of the currently loaded EDD
 * and load and assign the values from the persistent storage as
 * given by the buf argument.
 * 
 * \param buf The data buffer that is used to read the values.
 * \param env The current active environment or NULL.
 * \return 0 on success, otherwise the number of errors. The detailed
 * messages are stored in the error message handler.
 */
int
Interpreter::load_FILEs(lib::dyn_buf &buf, class method_env *env)
{
	int ret = 0;

	try
	{
		class IniParser : public lib::IniParser
		{
		public:
			IniParser(void) { }

			void load(const lib::stream &f) { lib::IniParser::load(f, NULL); }
		} db;

		/* parse from buf */
		lib::mstream f(buf);
		db.load(f);

		class tree *list;

		list = root->definition_list();
		while (list)
		{
			if (list->kind == EDD_FILE)
			{
				class FILE_tree *file = (class FILE_tree *)(list);

				try
				{
					file->load_members(db, env);
				}
				catch (const std::exception &e)
				{
					err().warning(e);
					++ret;
				}
			}

			list = list->next;
		}
	}
	catch (const std::exception &e)
	{
		err().warning(e);
		++ret;
	}

	return ret;
}

/**
 * Helper function to save all members of all FILE objects.
 * This method search all FILE objects of the currently loaded EDD
 * and save the values into the persistent storage as
 * given by the buf argument.
 * 
 * \param buf The data buffer that is used to write the values.
 * \param env The current active environment or NULL.
 * \return 0 on success, otherwise the number of errors. The detailed
 * messages are stored in the error message handler.
 */
int
Interpreter::save_FILEs(lib::dyn_buf &buf, class method_env *env) const
{
	int ret = 0;

	try
	{
		class IniParser : public lib::IniParser
		{
		public:
			IniParser(void) { }

			void save(lib::stream &f) { lib::IniParser::save(f); }
		} db;

		db.set_key("Version-Info", "version", 1);

		class tree *list;

		list = root->definition_list();
		while (list)
		{
			if (list->kind == EDD_FILE)
			{
				class FILE_tree *file = (class FILE_tree *)(list);

				try
				{
					file->save_members(db, env);
				}
				catch (const std::exception &e)
				{
					err().warning(e);
					++ret;
				}
			}

			list = list->next;
		}

		if (ret == 0)
		{
			/* no errors, save into buf */
			lib::mstream f(buf);
			db.save(f);
		}
	}
	catch (const std::exception &e)
	{
		err().error(e);
		++ret;
	}

	return ret;
}

bool
Interpreter::has_components(void) const
{
	bool ret = false;

	class tree *t = root->definition_list();

	while (t)
	{
		if (t->kind == EDD_COMPONENT)
		{
			ret = true;
			break;
		}

		t = t->next;
	}

	return ret;
}

void
Interpreter::set_primary_component(COMPONENT_tree *t) throw()
{
	primary_component = t;
}

COMPONENT_tree *
Interpreter::get_primary_component(void) const throw()
{
	return primary_component;
}

static const char *
binary_search(const char *key, const char **lower, const char **upper)
{
	assert(lower && upper);

	while (lower <= upper)
	{
		const char **middle = lower + (upper - lower) / 2;
		int check;

		assert(*middle);
		check = strcmp(key, *middle);

		if (check == 0)
			return *middle;

		if (check > 0)
			lower = middle + 1;
		else
			upper = middle - 1;
	}

	return NULL;
}

bool
Interpreter::is_pdm_rootname(class tree *t)
{
	return is_pdm_rootname(t->name());
}

bool
Interpreter::is_pdm_rootname(const std::string &name)
{
	return is_pdm_rootname(name.c_str());
}

bool
Interpreter::is_pdm_rootname(const char *name)
{
	static const char *ids[] =
	{
		"Menu_Main_Maintenance",
		"Menu_Main_Specialist",
		"Table_Main_Maintenance",
		"Table_Main_Specialist",
	};
	const size_t ids_max = sizeof(ids) / sizeof(ids[0]);

	return (binary_search(name, ids, ids + (ids_max - 1)) != NULL);
}

bool
Interpreter::is_pdm_specialmenu(const std::string &name)
{
	return is_pdm_specialmenu(name.c_str());
}

bool
Interpreter::is_pdm_specialmenu(const char *name)
{
	static const char *ids[] =
	{
		"Menu_File",
		"Menu_Help",
		"Menu_M_Options",
		"Menu_S_Options",
		"SEPARATOR",
	};
	const size_t ids_max = sizeof(ids) / sizeof(ids[0]);

	return (binary_search(name, ids, ids + (ids_max - 1)) != NULL);
}

Interpreter::menuitems_t
Interpreter::filter_pdm_menus(const menuitems_t &items)
{
	menuitems_t ret;

	for (size_t i = 0, i_max = items.size(); i < i_max; i++)
	{
		if (!is_pdm_specialmenu(items[i].ref->name()))
			ret.push_back(items[i]);
	}

	return ret;
}

/**
 * Tree node counter (main).
 * Loop over the complete abstract syntax tree and count the tree nodes. The
 * result seperately count the non-terminal and terminal nodes too.
 * \param terminal The number of terminal objects are stored here.
 * \param nonterminal The number of non-terminal objects are stored here.
 * \param eddobjects The number of EDD objects are stored here.
 * \return The number of bytes for all counted nodes (non-terminal + terminal).
 * \note This method work recursivly.
 */
unsigned long
Interpreter::stat_tree(unsigned long &terminal, unsigned long &nonterminal, unsigned long &eddobjects) const throw()
{
	terminal = 0;
	nonterminal = 0;
	eddobjects = 0;

	return stat_tree(root, terminal, nonterminal, eddobjects);
}

/**
 * Tree node counter (helper).
 * Loop over an abstract syntax subtree and count the tree nodes. The
 * result seperately count the non-terminal and terminal nodes too.
 * \param t Pointer to the tree object from which counting is started.
 * \param terminal The number of terminal objects are stored here.
 * \param nonterminal The number of non-terminal objects are stored here.
 * \param eddobjects The number of EDD objects are stored here.
 * \return The number of bytes for all counted nodes (non-terminal + terminal).
 * \note This method work recursivly.
 */
unsigned long
Interpreter::stat_tree(class tree *t,
		       unsigned long &terminal, unsigned long &nonterminal, unsigned long &eddobjects) const throw()
{
	unsigned long size = 0;

	while (t)
	{
		class node_tree *node;

		size += t->_sizeof();

		node = t->is_node();
		if (node)
		{
			unsigned long i;

			for (i = 0; i < node->attr_max(); i++)
			{
				if (node->attr(i))
					size += stat_tree(node->attr(i),
							  terminal, nonterminal, eddobjects);
			}

			nonterminal++;

			if (node->is_edd_object())
				eddobjects++;
		}
		else
			terminal++;

		t = t->next;
	}

	return size;
}

} /* namespace edd */
