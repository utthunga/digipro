
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */


/* 
 * $Id: edd_builtins.cxx.in,v 1.67 2009/05/19 12:35:34 fna Exp $
 * 
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#include "edd_builtins.h"

// C stdlib

// C++ stdlib

// my lib
#include "scratch_buf.h"

// own header
#include "edd_ast.h"
#include "edd_tree.h"
#include "edd_values.h"

#include "edd_builtins_dispatch.h"
#include "edd_builtins_impl.h"
#include "edd_builtins_null.h"


namespace edd
{

#ifndef DOXYGEN_IGNORE

static const unsigned int EDD_PROFILE_PROFIBUS = 0x00000001;
static const unsigned int EDD_PROFILE_PROFIBUS_OPTION_PDM = 0x00000002;
static const unsigned int EDD_PROFILE_PROFIBUS_OPTION_EDDx1 = 0x00000004;
static const unsigned int EDD_PROFILE_PROFIBUS_MASK = 0x00000006;
static const unsigned int EDD_PROFILE_PROFINET = 0x00000008;
static const unsigned int EDD_PROFILE_PROFINET_OPTION_PDM = 0x00000010;
static const unsigned int EDD_PROFILE_PROFINET_OPTION_EDDx1 = 0x00000020;
static const unsigned int EDD_PROFILE_PROFINET_MASK = 0x00000030;
static const unsigned int EDD_PROFILE_FIELDBUS = 0x00000040;
static const unsigned int EDD_PROFILE_FIELDBUS_OPTION_PDM = 0x00000080;
static const unsigned int EDD_PROFILE_FIELDBUS_OPTION_EDDx1 = 0x00000100;
static const unsigned int EDD_PROFILE_FIELDBUS_MASK = 0x00000180;
static const unsigned int EDD_PROFILE_HART = 0x00000200;
static const unsigned int EDD_PROFILE_HART_OPTION_PDM = 0x00000400;
static const unsigned int EDD_PROFILE_HART_OPTION_EDDx1 = 0x00000800;
static const unsigned int EDD_PROFILE_HART_MASK = 0x00000c00;

struct all_profile_flags
{
	unsigned int profile;
	unsigned int mask;
	unsigned int option_PDM;
	unsigned int option_EDDx1;
};

static const struct all_profile_flags all_profile_flags[] =
{
{ EDD_PROFILE_PROFIBUS, EDD_PROFILE_PROFIBUS_MASK, EDD_PROFILE_PROFIBUS_OPTION_PDM, EDD_PROFILE_PROFIBUS_OPTION_EDDx1 },
{ EDD_PROFILE_PROFINET, EDD_PROFILE_PROFINET_MASK, EDD_PROFILE_PROFINET_OPTION_PDM, EDD_PROFILE_PROFINET_OPTION_EDDx1 },
{ EDD_PROFILE_FIELDBUS, EDD_PROFILE_FIELDBUS_MASK, EDD_PROFILE_FIELDBUS_OPTION_PDM, EDD_PROFILE_FIELDBUS_OPTION_EDDx1 },
{ EDD_PROFILE_HART, EDD_PROFILE_HART_MASK, EDD_PROFILE_HART_OPTION_PDM, EDD_PROFILE_HART_OPTION_EDDx1 },
};

static const size_t builtins_size = 345;

struct builtin
{
	const char *name;
	const char *proto;
	enum c_type::typespec return_type;
	unsigned int nr;
	enum c_type::typespec *types;
	unsigned int *byref;
	unsigned int profile_flags;
	int special;
	operand_ptr (*dispatch)(class method_env *, operand_ptr, const lib::pos &);
	void (*check)(
		class edd_tree * const parent,
		class METHOD_BASE_tree * const method,
		const lib::pos &pos,
		const std::vector<class c_node_tree *> &args,
		std::vector<class c_type> &targs);
	void (*code)(
		class edd_tree * const parent,
		class c_node_tree * const caller,
		class stackmachine::SMachine &sm,
		class c_compound_statement_tree *env,
		const std::vector<class c_node_tree *> &args,
		const std::vector<class c_type> &targs);
	const struct builtin *next;
};

static operand_ptr
dispatch_ABORT_ON_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_ALL_COMM_STATUS_0 = 
{
	"ABORT_ON_ALL_COMM_STATUS",
	"void ABORT_ON_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_ABORT_ON_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_ALL_DEVICE_STATUS_0 = 
{
	"ABORT_ON_ALL_DEVICE_STATUS",
	"void ABORT_ON_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_ABORT_ON_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_ALL_RESPONSE_CODES_0 = 
{
	"ABORT_ON_ALL_RESPONSE_CODES",
	"void ABORT_ON_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_ABORT_ON_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_COMM_ERROR_0 = 
{
	"ABORT_ON_COMM_ERROR",
	"void ABORT_ON_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec ABORT_ON_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int ABORT_ON_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ABORT_ON_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_COMM_STATUS_0 = 
{
	"ABORT_ON_COMM_STATUS",
	"void ABORT_ON_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, ABORT_ON_COMM_STATUS_types_0, ABORT_ON_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_COMM_STATUS_0, builtins::check_ABORT_ON_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec ABORT_ON_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int ABORT_ON_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ABORT_ON_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_DEVICE_STATUS_0 = 
{
	"ABORT_ON_DEVICE_STATUS",
	"void ABORT_ON_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, ABORT_ON_DEVICE_STATUS_types_0, ABORT_ON_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_DEVICE_STATUS_0, builtins::check_ABORT_ON_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_ABORT_ON_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_NO_DEVICE_0 = 
{
	"ABORT_ON_NO_DEVICE",
	"void ABORT_ON_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec ABORT_ON_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int ABORT_ON_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ABORT_ON_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ABORT_ON_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin ABORT_ON_RESPONSE_CODE_0 = 
{
	"ABORT_ON_RESPONSE_CODE",
	"void ABORT_ON_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, ABORT_ON_RESPONSE_CODE_types_0, ABORT_ON_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ABORT_ON_RESPONSE_CODE_0, builtins::check_ABORT_ON_RESPONSE_CODE_0, 0,
	0,
};

static enum c_type::typespec ACKNOWLEDGE_types_0[1] =
{
	c_type::tstring
};
static unsigned int ACKNOWLEDGE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ACKNOWLEDGE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ACKNOWLEDGE(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ACKNOWLEDGE_0 = 
{
	"ACKNOWLEDGE",
	"long ACKNOWLEDGE(string prompt);",
	c_type::tint32,
	1, ACKNOWLEDGE_types_0, ACKNOWLEDGE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ACKNOWLEDGE_0, 0, 0,
	0,
};

static enum c_type::typespec AddTime_types_0[2] =
{
	c_type::tint32,
	c_type::tfloat64
};
static unsigned int AddTime_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_AddTime_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::AddTime(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin AddTime_0 = 
{
	"AddTime",
	"long AddTime(long t0, double seconds);",
	c_type::tint32,
	2, AddTime_types_0, AddTime_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_AddTime_0, 0, 0,
	0,
};

static enum c_type::typespec BUILD_MESSAGE_types_0[1] =
{
	c_type::tstring
};
static unsigned int BUILD_MESSAGE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_BUILD_MESSAGE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::BUILD_MESSAGE(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin BUILD_MESSAGE_0 = 
{
	"BUILD_MESSAGE",
	"string BUILD_MESSAGE(string text);",
	c_type::tstring,
	1, BUILD_MESSAGE_types_0, BUILD_MESSAGE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_BUILD_MESSAGE_0, 0, 0,
	0,
};

static enum c_type::typespec ByteToDouble_types_0[8] =
{
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int ByteToDouble_byref_0[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0
};

static operand_ptr
dispatch_ByteToDouble_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint08 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg4;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg4 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg5;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg5 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg6;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg6 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg7;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg7 = (edd_uint08)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::ByteToDouble(env, pos, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ByteToDouble_0 = 
{
	"ByteToDouble",
	"double ByteToDouble(unsigned char in0, unsigned char in1, unsigned char in2, unsigned char in3, unsigned char in4, unsigned char in5, unsigned char in6, unsigned char in7);",
	c_type::tfloat64,
	8, ByteToDouble_types_0, ByteToDouble_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ByteToDouble_0, 0, 0,
	0,
};

static enum c_type::typespec ByteToFloat_types_0[4] =
{
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int ByteToFloat_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static operand_ptr
dispatch_ByteToFloat_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint08 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_uint08)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::ByteToFloat(env, pos, arg0, arg1, arg2, arg3);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ByteToFloat_0 = 
{
	"ByteToFloat",
	"float ByteToFloat(unsigned char in0, unsigned char in1, unsigned char in2, unsigned char in3);",
	c_type::tfloat32,
	4, ByteToFloat_types_0, ByteToFloat_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ByteToFloat_0, 0, 0,
	0,
};

static enum c_type::typespec ByteToLong_types_0[4] =
{
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int ByteToLong_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static operand_ptr
dispatch_ByteToLong_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint08 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_uint08)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ByteToLong(env, pos, arg0, arg1, arg2, arg3);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ByteToLong_0 = 
{
	"ByteToLong",
	"long ByteToLong(unsigned char in0, unsigned char in1, unsigned char in2, unsigned char in3);",
	c_type::tint32,
	4, ByteToLong_types_0, ByteToLong_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ByteToLong_0, 0, 0,
	0,
};

static enum c_type::typespec ByteToShort_types_0[2] =
{
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int ByteToShort_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_ByteToShort_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint08 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint08)(*data);
	}
	args = args.next();
	
	edd_uint08 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_uint08)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int16 ret = builtins::ByteToShort(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int16_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ByteToShort_0 = 
{
	"ByteToShort",
	"short ByteToShort(unsigned char in0, unsigned char in1);",
	c_type::tint16,
	2, ByteToShort_types_0, ByteToShort_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ByteToShort_0, 0, 0,
	0,
};

static enum c_type::typespec DATE_AND_TIME_VALUE_to_string_types_0[4] =
{
	c_type::tstring,
	c_type::tstring,
	c_type::tint32,
	c_type::tuint32
};
static unsigned int DATE_AND_TIME_VALUE_to_string_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin DATE_AND_TIME_VALUE_to_string_0 = 
{
	"DATE_AND_TIME_VALUE_to_string",
	"long DATE_AND_TIME_VALUE_to_string(string str, string format, long data, unsigned long time_value);",
	c_type::tint32,
	4, DATE_AND_TIME_VALUE_to_string_types_0, DATE_AND_TIME_VALUE_to_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec DATE_to_days_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int DATE_to_days_byref_0[2] = 
{
	0,
	0
};

static const struct builtin DATE_to_days_0 = 
{
	"DATE_to_days",
	"long DATE_to_days(long date1, long date0);",
	c_type::tint32,
	2, DATE_to_days_types_0, DATE_to_days_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec DATE_to_string_types_0[3] =
{
	c_type::tstring,
	c_type::tstring,
	c_type::tint32
};
static unsigned int DATE_to_string_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin DATE_to_string_0 = 
{
	"DATE_to_string",
	"long DATE_to_string(string str, string format, long date);",
	c_type::tint32,
	3, DATE_to_string_types_0, DATE_to_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec DELAY_types_0[2] =
{
	c_type::tint32,
	c_type::tstring
};
static unsigned int DELAY_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_DELAY_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::DELAY(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin DELAY_0 = 
{
	"DELAY",
	"void DELAY(long delay_time, string prompt);",
	c_type::tvoid,
	2, DELAY_types_0, DELAY_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_DELAY_0, 0, 0,
	0,
};

static enum c_type::typespec DELAY_TIME_types_0[1] =
{
	c_type::tint32
};
static unsigned int DELAY_TIME_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_DELAY_TIME_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::DELAY_TIME(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin DELAY_TIME_0 = 
{
	"DELAY_TIME",
	"void DELAY_TIME(long delay_time);",
	c_type::tvoid,
	1, DELAY_TIME_types_0, DELAY_TIME_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_DELAY_TIME_0, 0, 0,
	0,
};

static enum c_type::typespec DICT_ID_types_0[1] =
{
	c_type::tident
};
static unsigned int DICT_ID_byref_0[1] = 
{
	0
};

static const struct builtin DICT_ID_0 = 
{
	"DICT_ID",
	"long DICT_ID(ident name);",
	c_type::tint32,
	1, DICT_ID_types_0, DICT_ID_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec DISPLAY_types_0[1] =
{
	c_type::tstring
};
static unsigned int DISPLAY_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_DISPLAY_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::DISPLAY(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin DISPLAY_0 = 
{
	"DISPLAY",
	"void DISPLAY(string prompt);",
	c_type::tvoid,
	1, DISPLAY_types_0, DISPLAY_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_DISPLAY_0, 0, 0,
	0,
};

static enum c_type::typespec Date_To_Time_types_1[1] =
{
	c_type::tint32
};
static unsigned int Date_To_Time_byref_1[1] = 
{
	0
};

static const struct builtin Date_To_Time_1 = 
{
	"Date_To_Time",
	"long Date_To_Time(long date);",
	c_type::tint32,
	1, Date_To_Time_types_1, Date_To_Time_byref_1,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec Date_To_Time_types_0[1] =
{
	c_type::teddobject
};
static unsigned int Date_To_Time_byref_0[1] = 
{
	0
};

static const struct builtin Date_To_Time_0 = 
{
	"Date_To_Time",
	"long Date_To_Time(eddobject date);",
	c_type::tint32,
	1, Date_To_Time_types_0, Date_To_Time_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, 0, 0,
	&Date_To_Time_1,
};

static enum c_type::typespec Date_to_DayOfMonth_types_1[1] =
{
	c_type::teddobject
};
static unsigned int Date_to_DayOfMonth_byref_1[1] = 
{
	0
};

static const struct builtin Date_to_DayOfMonth_1 = 
{
	"Date_to_DayOfMonth",
	"long Date_to_DayOfMonth(eddobject var);",
	c_type::tint32,
	1, Date_to_DayOfMonth_types_1, Date_to_DayOfMonth_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, builtins::check_Date_to_DayOfMonth_1, 0,
	0,
};

static enum c_type::typespec Date_to_DayOfMonth_types_0[1] =
{
	c_type::tint32
};
static unsigned int Date_to_DayOfMonth_byref_0[1] = 
{
	0
};

static const struct builtin Date_to_DayOfMonth_0 = 
{
	"Date_to_DayOfMonth",
	"long Date_to_DayOfMonth(long date);",
	c_type::tint32,
	1, Date_to_DayOfMonth_types_0, Date_to_DayOfMonth_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, 0, 0,
	&Date_to_DayOfMonth_1,
};

static enum c_type::typespec Date_to_Month_types_1[1] =
{
	c_type::teddobject
};
static unsigned int Date_to_Month_byref_1[1] = 
{
	0
};

static const struct builtin Date_to_Month_1 = 
{
	"Date_to_Month",
	"long Date_to_Month(eddobject var);",
	c_type::tint32,
	1, Date_to_Month_types_1, Date_to_Month_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, builtins::check_Date_to_Month_1, 0,
	0,
};

static enum c_type::typespec Date_to_Month_types_0[1] =
{
	c_type::tint32
};
static unsigned int Date_to_Month_byref_0[1] = 
{
	0
};

static const struct builtin Date_to_Month_0 = 
{
	"Date_to_Month",
	"long Date_to_Month(long date);",
	c_type::tint32,
	1, Date_to_Month_types_0, Date_to_Month_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, builtins::check_Date_to_Month_0, 0,
	&Date_to_Month_1,
};

static enum c_type::typespec Date_to_Year_types_1[1] =
{
	c_type::teddobject
};
static unsigned int Date_to_Year_byref_1[1] = 
{
	0
};

static const struct builtin Date_to_Year_1 = 
{
	"Date_to_Year",
	"long Date_to_Year(eddobject var);",
	c_type::tint32,
	1, Date_to_Year_types_1, Date_to_Year_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, builtins::check_Date_to_Year_1, 0,
	0,
};

static enum c_type::typespec Date_to_Year_types_0[1] =
{
	c_type::tint32
};
static unsigned int Date_to_Year_byref_0[1] = 
{
	0
};

static const struct builtin Date_to_Year_0 = 
{
	"Date_to_Year",
	"long Date_to_Year(long date);",
	c_type::tint32,
	1, Date_to_Year_types_0, Date_to_Year_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, 0, 0,
	&Date_to_Year_1,
};

static enum c_type::typespec DiffTime_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int DiffTime_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_DiffTime_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::DiffTime(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin DiffTime_0 = 
{
	"DiffTime",
	"double DiffTime(long t1, long t0);",
	c_type::tfloat64,
	2, DiffTime_types_0, DiffTime_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_DiffTime_0, 0, 0,
	0,
};

static enum c_type::typespec DoubleToByte_types_0[9] =
{
	c_type::tfloat64,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int DoubleToByte_byref_0[9] = 
{
	0,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1
};

static operand_ptr
dispatch_DoubleToByte_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	edd_uint08_opmap arg1(args);
	args = args.next();
	
	edd_uint08_opmap arg2(args);
	args = args.next();
	
	edd_uint08_opmap arg3(args);
	args = args.next();
	
	edd_uint08_opmap arg4(args);
	args = args.next();
	
	edd_uint08_opmap arg5(args);
	args = args.next();
	
	edd_uint08_opmap arg6(args);
	args = args.next();
	
	edd_uint08_opmap arg7(args);
	args = args.next();
	
	edd_uint08_opmap arg8(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::DoubleToByte(env, pos, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
	
	/* return value */
	return new no_value();
}

static const struct builtin DoubleToByte_0 = 
{
	"DoubleToByte",
	"void DoubleToByte(double in, unsigned char &out0, unsigned char &out1, unsigned char &out2, unsigned char &out3, unsigned char &out4, unsigned char &out5, unsigned char &out6, unsigned char &out7);",
	c_type::tvoid,
	9, DoubleToByte_types_0, DoubleToByte_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_DoubleToByte_0, 0, 0,
	0,
};

static enum c_type::typespec FloatToByte_types_0[5] =
{
	c_type::tfloat32,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int FloatToByte_byref_0[5] = 
{
	0,
	1,
	1,
	1,
	1
};

static operand_ptr
dispatch_FloatToByte_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	edd_uint08_opmap arg1(args);
	args = args.next();
	
	edd_uint08_opmap arg2(args);
	args = args.next();
	
	edd_uint08_opmap arg3(args);
	args = args.next();
	
	edd_uint08_opmap arg4(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::FloatToByte(env, pos, arg0, arg1, arg2, arg3, arg4);
	
	/* return value */
	return new no_value();
}

static const struct builtin FloatToByte_0 = 
{
	"FloatToByte",
	"void FloatToByte(float in, unsigned char &out0, unsigned char &out1, unsigned char &out2, unsigned char &out3);",
	c_type::tvoid,
	5, FloatToByte_types_0, FloatToByte_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_FloatToByte_0, 0, 0,
	0,
};

static enum c_type::typespec From_DATE_AND_TIME_VALUE_types_0[2] =
{
	c_type::tint32,
	c_type::tuint32
};
static unsigned int From_DATE_AND_TIME_VALUE_byref_0[2] = 
{
	0,
	0
};

static const struct builtin From_DATE_AND_TIME_VALUE_0 = 
{
	"From_DATE_AND_TIME_VALUE",
	"long From_DATE_AND_TIME_VALUE(long date, unsigned long time_value);",
	c_type::tint32,
	2, From_DATE_AND_TIME_VALUE_types_0, From_DATE_AND_TIME_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec From_TIME_VALUE_types_0[1] =
{
	c_type::tuint32
};
static unsigned int From_TIME_VALUE_byref_0[1] = 
{
	0
};

static const struct builtin From_TIME_VALUE_0 = 
{
	"From_TIME_VALUE",
	"long From_TIME_VALUE(unsigned long time_value);",
	c_type::tint32,
	1, From_TIME_VALUE_types_0, From_TIME_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static operand_ptr
dispatch_GET_DD_REVISION_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_DD_REVISION(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_DD_REVISION_0 = 
{
	"GET_DD_REVISION",
	"long GET_DD_REVISION(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_GET_DD_REVISION_0, 0, 0,
	0,
};

static operand_ptr
dispatch_GET_DEVICE_REVISION_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_DEVICE_REVISION(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_DEVICE_REVISION_0 = 
{
	"GET_DEVICE_REVISION",
	"long GET_DEVICE_REVISION(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_GET_DEVICE_REVISION_0, 0, 0,
	0,
};

static operand_ptr
dispatch_GET_DEVICE_TYPE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_DEVICE_TYPE(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_DEVICE_TYPE_0 = 
{
	"GET_DEVICE_TYPE",
	"long GET_DEVICE_TYPE(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_GET_DEVICE_TYPE_0, 0, 0,
	0,
};

static enum c_type::typespec GET_DEV_VAR_VALUE_types_0[2] =
{
	c_type::tstring,
	c_type::teddobject
};
static unsigned int GET_DEV_VAR_VALUE_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_GET_DEV_VAR_VALUE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	class tree * arg1;
	{
		/* it's a real operand here */
		arg1 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_DEV_VAR_VALUE(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_DEV_VAR_VALUE_0 = 
{
	"GET_DEV_VAR_VALUE",
	"long GET_DEV_VAR_VALUE(string prompt, eddobject device_var_name);",
	c_type::tint32,
	2, GET_DEV_VAR_VALUE_types_0, GET_DEV_VAR_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_GET_DEV_VAR_VALUE_0, builtins::check_GET_DEV_VAR_VALUE_0, 0,
	0,
};

static enum c_type::typespec GET_LOCAL_VAR_VALUE_types_0[2] =
{
	c_type::tstring,
	c_type::tident
};
static unsigned int GET_LOCAL_VAR_VALUE_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_GET_LOCAL_VAR_VALUE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	lib::IDENTIFIER arg1;
	{
		/* it's a real operand here */
		arg1 = (lib::IDENTIFIER)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_LOCAL_VAR_VALUE(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_LOCAL_VAR_VALUE_0 = 
{
	"GET_LOCAL_VAR_VALUE",
	"long GET_LOCAL_VAR_VALUE(string prompt, ident local_var_name);",
	c_type::tint32,
	2, GET_LOCAL_VAR_VALUE_types_0, GET_LOCAL_VAR_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_GET_LOCAL_VAR_VALUE_0, builtins::check_GET_LOCAL_VAR_VALUE_0, 0,
	0,
};

static operand_ptr
dispatch_GET_MANUFACTURER_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_MANUFACTURER(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_MANUFACTURER_0 = 
{
	"GET_MANUFACTURER",
	"long GET_MANUFACTURER(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_GET_MANUFACTURER_0, 0, 0,
	0,
};

static operand_ptr
dispatch_GET_TICK_COUNT_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::GET_TICK_COUNT(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin GET_TICK_COUNT_0 = 
{
	"GET_TICK_COUNT",
	"long GET_TICK_COUNT(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_GET_TICK_COUNT_0, 0, 0,
	0,
};

static const struct builtin GetCurrentDate_0 = 
{
	"GetCurrentDate",
	"long GetCurrentDate(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin GetCurrentDateAndTime_0 = 
{
	"GetCurrentDateAndTime",
	"long GetCurrentDateAndTime(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin GetCurrentTime_0 = 
{
	"GetCurrentTime",
	"long GetCurrentTime(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static operand_ptr
dispatch_IGNORE_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_ALL_COMM_STATUS_0 = 
{
	"IGNORE_ALL_COMM_STATUS",
	"void IGNORE_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_IGNORE_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_ALL_DEVICE_STATUS_0 = 
{
	"IGNORE_ALL_DEVICE_STATUS",
	"void IGNORE_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_IGNORE_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_ALL_RESPONSE_CODES_0 = 
{
	"IGNORE_ALL_RESPONSE_CODES",
	"void IGNORE_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_IGNORE_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_COMM_ERROR_0 = 
{
	"IGNORE_COMM_ERROR",
	"void IGNORE_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec IGNORE_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int IGNORE_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_IGNORE_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_COMM_STATUS_0 = 
{
	"IGNORE_COMM_STATUS",
	"void IGNORE_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, IGNORE_COMM_STATUS_types_0, IGNORE_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_COMM_STATUS_0, builtins::check_IGNORE_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec IGNORE_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int IGNORE_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_IGNORE_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_DEVICE_STATUS_0 = 
{
	"IGNORE_DEVICE_STATUS",
	"void IGNORE_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, IGNORE_DEVICE_STATUS_types_0, IGNORE_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_DEVICE_STATUS_0, builtins::check_IGNORE_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_IGNORE_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_NO_DEVICE_0 = 
{
	"IGNORE_NO_DEVICE",
	"void IGNORE_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec IGNORE_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int IGNORE_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_IGNORE_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::IGNORE_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin IGNORE_RESPONSE_CODE_0 = 
{
	"IGNORE_RESPONSE_CODE",
	"void IGNORE_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, IGNORE_RESPONSE_CODE_types_0, IGNORE_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_IGNORE_RESPONSE_CODE_0, builtins::check_IGNORE_RESPONSE_CODE_0, 0,
	0,
};

static enum c_type::typespec ITEM_ID_types_0[1] =
{
	c_type::teddobject
};
static unsigned int ITEM_ID_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ITEM_ID_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_uint32 ret = builtins::ITEM_ID(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new uint32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ITEM_ID_0 = 
{
	"ITEM_ID",
	"unsigned long ITEM_ID(eddobject name);",
	c_type::tuint32,
	1, ITEM_ID_types_0, ITEM_ID_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_ITEM_ID_0, 0, 0,
	0,
};

static enum c_type::typespec LOG_MESSAGE_types_0[2] =
{
	c_type::tint32,
	c_type::tstring
};
static unsigned int LOG_MESSAGE_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_LOG_MESSAGE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::LOG_MESSAGE(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin LOG_MESSAGE_0 = 
{
	"LOG_MESSAGE",
	"void LOG_MESSAGE(long priority, string message);",
	c_type::tvoid,
	2, LOG_MESSAGE_types_0, LOG_MESSAGE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_LOG_MESSAGE_0, 0, 0,
	0,
};

static enum c_type::typespec ListDeleteElementAt_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int ListDeleteElementAt_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_ListDeleteElementAt_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ListDeleteElementAt(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ListDeleteElementAt_0 = 
{
	"ListDeleteElementAt",
	"long ListDeleteElementAt(eddobject listname, long index);",
	c_type::tint32,
	2, ListDeleteElementAt_types_0, ListDeleteElementAt_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_ListDeleteElementAt_0, builtins::check_ListDeleteElementAt_0, 0,
	0,
};

static enum c_type::typespec ListInsert_types_0[3] =
{
	c_type::teddobject,
	c_type::tint32,
	c_type::teddobject
};
static unsigned int ListInsert_byref_0[3] = 
{
	0,
	0,
	0
};

static operand_ptr
dispatch_ListInsert_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	class tree * arg2;
	{
		/* it's a real operand here */
		arg2 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ListInsert(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ListInsert_0 = 
{
	"ListInsert",
	"long ListInsert(eddobject listname, long index, eddobject object);",
	c_type::tint32,
	3, ListInsert_types_0, ListInsert_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_ListInsert_0, builtins::check_ListInsert_0, 0,
	0,
};

static enum c_type::typespec LongToByte_types_0[5] =
{
	c_type::tint32,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int LongToByte_byref_0[5] = 
{
	0,
	1,
	1,
	1,
	1
};

static operand_ptr
dispatch_LongToByte_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_uint08_opmap arg1(args);
	args = args.next();
	
	edd_uint08_opmap arg2(args);
	args = args.next();
	
	edd_uint08_opmap arg3(args);
	args = args.next();
	
	edd_uint08_opmap arg4(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::LongToByte(env, pos, arg0, arg1, arg2, arg3, arg4);
	
	/* return value */
	return new no_value();
}

static const struct builtin LongToByte_0 = 
{
	"LongToByte",
	"void LongToByte(long in, unsigned char &out0, unsigned char &out1, unsigned char &out2, unsigned char &out3);",
	c_type::tvoid,
	5, LongToByte_types_0, LongToByte_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_LongToByte_0, 0, 0,
	0,
};

static enum c_type::typespec MEMBER_ID_types_0[1] =
{
	c_type::teddobject
};
static unsigned int MEMBER_ID_byref_0[1] = 
{
	0
};

static const struct builtin MEMBER_ID_0 = 
{
	"MEMBER_ID",
	"unsigned long MEMBER_ID(eddobject name);",
	c_type::tuint32,
	1, MEMBER_ID_types_0, MEMBER_ID_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec METHODID_types_0[1] =
{
	c_type::teddobject
};
static unsigned int METHODID_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_METHODID_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::METHODID(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin METHODID_0 = 
{
	"METHODID",
	"long METHODID(eddobject variable_name);",
	c_type::tint32,
	1, METHODID_types_0, METHODID_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_METHODID_0, builtins::check_METHODID_0, 0,
	0,
};

static enum c_type::typespec Make_Time_types_0[7] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int Make_Time_byref_0[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin Make_Time_0 = 
{
	"Make_Time",
	"long Make_Time(long year, long month, long dayOfMont, long hour, long minute, long second, long isDST);",
	c_type::tint32,
	7, Make_Time_types_0, Make_Time_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec MenuDisplay_types_0[3] =
{
	c_type::teddobject,
	c_type::tstring,
	c_type::tint32
};
static unsigned int MenuDisplay_byref_0[3] = 
{
	0,
	0,
	1
};

static operand_ptr
dispatch_MenuDisplay_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_opmap arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::MenuDisplay(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin MenuDisplay_0 = 
{
	"MenuDisplay",
	"long MenuDisplay(eddobject menu, string options, long &selection);",
	c_type::tint32,
	3, MenuDisplay_types_0, MenuDisplay_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_MenuDisplay_0, builtins::check_MenuDisplay_0, 0,
	0,
};

static enum c_type::typespec NaN_value_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int NaN_value_byref_0[1] = 
{
	1
};

static operand_ptr
dispatch_NaN_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64_opmap arg0(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::NaN_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin NaN_value_0 = 
{
	"NaN_value",
	"long NaN_value(double &value);",
	c_type::tint32,
	1, NaN_value_types_0, NaN_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_NaN_value_0, 0, 0,
	0,
};

static enum c_type::typespec ObjectReference_types_0[1] =
{
	c_type::tstring
};
static unsigned int ObjectReference_byref_0[1] = 
{
	0
};

static const struct builtin ObjectReference_0 = 
{
	"ObjectReference",
	"void ObjectReference(string name);",
	c_type::tvoid,
	1, ObjectReference_types_0, ObjectReference_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec PUT_MESSAGE_types_0[1] =
{
	c_type::tstring
};
static unsigned int PUT_MESSAGE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_PUT_MESSAGE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::PUT_MESSAGE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin PUT_MESSAGE_0 = 
{
	"PUT_MESSAGE",
	"void PUT_MESSAGE(string message);",
	c_type::tvoid,
	1, PUT_MESSAGE_types_0, PUT_MESSAGE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_PUT_MESSAGE_0, 0, 0,
	0,
};

static enum c_type::typespec READ_COMMAND_types_0[1] =
{
	c_type::teddobject
};
static unsigned int READ_COMMAND_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_READ_COMMAND_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::READ_COMMAND(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin READ_COMMAND_0 = 
{
	"READ_COMMAND",
	"long READ_COMMAND(eddobject command_name);",
	c_type::tint32,
	1, READ_COMMAND_types_0, READ_COMMAND_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_READ_COMMAND_0, builtins::check_READ_COMMAND_0, 0,
	0,
};

static operand_ptr
dispatch_RETRY_ON_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_ALL_COMM_STATUS_0 = 
{
	"RETRY_ON_ALL_COMM_STATUS",
	"void RETRY_ON_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_RETRY_ON_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_ALL_DEVICE_STATUS_0 = 
{
	"RETRY_ON_ALL_DEVICE_STATUS",
	"void RETRY_ON_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_RETRY_ON_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_ALL_RESPONSE_CODES_0 = 
{
	"RETRY_ON_ALL_RESPONSE_CODES",
	"void RETRY_ON_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_RETRY_ON_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_COMM_ERROR_0 = 
{
	"RETRY_ON_COMM_ERROR",
	"void RETRY_ON_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec RETRY_ON_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int RETRY_ON_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_RETRY_ON_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_COMM_STATUS_0 = 
{
	"RETRY_ON_COMM_STATUS",
	"void RETRY_ON_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, RETRY_ON_COMM_STATUS_types_0, RETRY_ON_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_COMM_STATUS_0, builtins::check_RETRY_ON_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec RETRY_ON_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int RETRY_ON_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_RETRY_ON_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_DEVICE_STATUS_0 = 
{
	"RETRY_ON_DEVICE_STATUS",
	"void RETRY_ON_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, RETRY_ON_DEVICE_STATUS_types_0, RETRY_ON_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_DEVICE_STATUS_0, builtins::check_RETRY_ON_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_RETRY_ON_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_NO_DEVICE_0 = 
{
	"RETRY_ON_NO_DEVICE",
	"void RETRY_ON_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec RETRY_ON_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int RETRY_ON_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_RETRY_ON_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::RETRY_ON_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin RETRY_ON_RESPONSE_CODE_0 = 
{
	"RETRY_ON_RESPONSE_CODE",
	"void RETRY_ON_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, RETRY_ON_RESPONSE_CODE_types_0, RETRY_ON_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_RETRY_ON_RESPONSE_CODE_0, builtins::check_RETRY_ON_RESPONSE_CODE_0, 0,
	0,
};

static enum c_type::typespec ReadCommand_types_0[1] =
{
	c_type::teddobject
};
static unsigned int ReadCommand_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ReadCommand_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ReadCommand(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ReadCommand_0 = 
{
	"ReadCommand",
	"long ReadCommand(eddobject command_name);",
	c_type::tint32,
	1, ReadCommand_types_0, ReadCommand_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_ReadCommand_0, builtins::check_ReadCommand_0, 0,
	0,
};

static enum c_type::typespec SELECT_FROM_LIST_types_0[2] =
{
	c_type::tstring,
	c_type::tstring
};
static unsigned int SELECT_FROM_LIST_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_SELECT_FROM_LIST_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::SELECT_FROM_LIST(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin SELECT_FROM_LIST_0 = 
{
	"SELECT_FROM_LIST",
	"long SELECT_FROM_LIST(string prompt, string option_list);",
	c_type::tint32,
	2, SELECT_FROM_LIST_types_0, SELECT_FROM_LIST_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_SELECT_FROM_LIST_0, 0, 0,
	0,
};

static enum c_type::typespec SET_NUMBER_OF_RETRIES_types_0[1] =
{
	c_type::tint32
};
static unsigned int SET_NUMBER_OF_RETRIES_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_SET_NUMBER_OF_RETRIES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::SET_NUMBER_OF_RETRIES(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin SET_NUMBER_OF_RETRIES_0 = 
{
	"SET_NUMBER_OF_RETRIES",
	"void SET_NUMBER_OF_RETRIES(long n);",
	c_type::tvoid,
	1, SET_NUMBER_OF_RETRIES_types_0, SET_NUMBER_OF_RETRIES_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_SET_NUMBER_OF_RETRIES_0, 0, 0,
	0,
};

static enum c_type::typespec ShellExecute_types_0[1] =
{
	c_type::tstring
};
static unsigned int ShellExecute_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ShellExecute_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ShellExecute(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin ShellExecute_0 = 
{
	"ShellExecute",
	"void ShellExecute(string arg);",
	c_type::tvoid,
	1, ShellExecute_types_0, ShellExecute_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_ShellExecute_0, 0, 0,
	0,
};

static enum c_type::typespec ShortToByte_types_0[3] =
{
	c_type::tint16,
	c_type::tuint8,
	c_type::tuint8
};
static unsigned int ShortToByte_byref_0[3] = 
{
	0,
	1,
	1
};

static operand_ptr
dispatch_ShortToByte_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int16 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int16)(*data);
	}
	args = args.next();
	
	edd_uint08_opmap arg1(args);
	args = args.next();
	
	edd_uint08_opmap arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::ShortToByte(env, pos, arg0, arg1, arg2);
	
	/* return value */
	return new no_value();
}

static const struct builtin ShortToByte_0 = 
{
	"ShortToByte",
	"void ShortToByte(short in, unsigned char &out0, unsigned char &out1);",
	c_type::tvoid,
	3, ShortToByte_types_0, ShortToByte_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ShortToByte_0, 0, 0,
	0,
};

static enum c_type::typespec TIME_VALUE_to_Hour_types_0[1] =
{
	c_type::tuint32
};
static unsigned int TIME_VALUE_to_Hour_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_TIME_VALUE_to_Hour_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::TIME_VALUE_to_Hour(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin TIME_VALUE_to_Hour_0 = 
{
	"TIME_VALUE_to_Hour",
	"long TIME_VALUE_to_Hour(unsigned long time_value);",
	c_type::tint32,
	1, TIME_VALUE_to_Hour_types_0, TIME_VALUE_to_Hour_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_TIME_VALUE_to_Hour_0, 0, 0,
	0,
};

static enum c_type::typespec TIME_VALUE_to_Minute_types_0[1] =
{
	c_type::tuint32
};
static unsigned int TIME_VALUE_to_Minute_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_TIME_VALUE_to_Minute_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::TIME_VALUE_to_Minute(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin TIME_VALUE_to_Minute_0 = 
{
	"TIME_VALUE_to_Minute",
	"long TIME_VALUE_to_Minute(unsigned long time_value);",
	c_type::tint32,
	1, TIME_VALUE_to_Minute_types_0, TIME_VALUE_to_Minute_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_TIME_VALUE_to_Minute_0, 0, 0,
	0,
};

static enum c_type::typespec TIME_VALUE_to_Second_types_0[1] =
{
	c_type::tuint32
};
static unsigned int TIME_VALUE_to_Second_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_TIME_VALUE_to_Second_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::TIME_VALUE_to_Second(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin TIME_VALUE_to_Second_0 = 
{
	"TIME_VALUE_to_Second",
	"long TIME_VALUE_to_Second(unsigned long time_value);",
	c_type::tint32,
	1, TIME_VALUE_to_Second_types_0, TIME_VALUE_to_Second_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_TIME_VALUE_to_Second_0, 0, 0,
	0,
};

static enum c_type::typespec TIME_VALUE_to_seconds_types_0[1] =
{
	c_type::tuint32
};
static unsigned int TIME_VALUE_to_seconds_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_TIME_VALUE_to_seconds_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::TIME_VALUE_to_seconds(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin TIME_VALUE_to_seconds_0 = 
{
	"TIME_VALUE_to_seconds",
	"double TIME_VALUE_to_seconds(unsigned long time_value);",
	c_type::tfloat64,
	1, TIME_VALUE_to_seconds_types_0, TIME_VALUE_to_seconds_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_TIME_VALUE_to_seconds_0, 0, 0,
	0,
};

static enum c_type::typespec TIME_VALUE_to_string_types_0[3] =
{
	c_type::tstring,
	c_type::tstring,
	c_type::tuint32
};
static unsigned int TIME_VALUE_to_string_byref_0[3] = 
{
	1,
	0,
	0
};

static operand_ptr
dispatch_TIME_VALUE_to_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string_opmap arg0(args);
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_uint32 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::TIME_VALUE_to_string(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin TIME_VALUE_to_string_0 = 
{
	"TIME_VALUE_to_string",
	"long TIME_VALUE_to_string(string &str, string format, unsigned long time_value);",
	c_type::tint32,
	3, TIME_VALUE_to_string_types_0, TIME_VALUE_to_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_TIME_VALUE_to_string_0, 0, 0,
	0,
};

static enum c_type::typespec Time_To_Date_types_0[1] =
{
	c_type::tint32
};
static unsigned int Time_To_Date_byref_0[1] = 
{
	0
};

static const struct builtin Time_To_Date_0 = 
{
	"Time_To_Date",
	"long Time_To_Date(long time);",
	c_type::tint32,
	1, Time_To_Date_types_0, Time_To_Date_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec To_Date_types_0[3] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int To_Date_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin To_Date_0 = 
{
	"To_Date",
	"long To_Date(long year, long month, long dayOfMonth);",
	c_type::tint32,
	3, To_Date_types_0, To_Date_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec To_Date_and_Time_types_0[5] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int To_Date_and_Time_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin To_Date_and_Time_0 = 
{
	"To_Date_and_Time",
	"double To_Date_and_Time(long days, long hour, long minute, long second, long millisecond);",
	c_type::tfloat64,
	5, To_Date_and_Time_types_0, To_Date_and_Time_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec To_TIME_VALUE_types_0[3] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int To_TIME_VALUE_byref_0[3] = 
{
	0,
	0,
	0
};

static operand_ptr
dispatch_To_TIME_VALUE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::To_TIME_VALUE(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin To_TIME_VALUE_0 = 
{
	"To_TIME_VALUE",
	"double To_TIME_VALUE(long hours, long minutes, long seconds);",
	c_type::tfloat64,
	3, To_TIME_VALUE_types_0, To_TIME_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_To_TIME_VALUE_0, 0, 0,
	0,
};

static enum c_type::typespec To_Time_types_1[5] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int To_Time_byref_1[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin To_Time_1 = 
{
	"To_Time",
	"long To_Time(long date, long hour, long minute, long second, long isDST);",
	c_type::tint32,
	5, To_Time_types_1, To_Time_byref_1,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec To_Time_types_0[5] =
{
	c_type::teddobject,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int To_Time_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin To_Time_0 = 
{
	"To_Time",
	"long To_Time(eddobject date, long hour, long minute, long second, long isDST);",
	c_type::tint32,
	5, To_Time_types_0, To_Time_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	0, 0, 0,
	&To_Time_1,
};

static enum c_type::typespec VARID_types_0[1] =
{
	c_type::teddobject
};
static unsigned int VARID_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_VARID_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::VARID(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin VARID_0 = 
{
	"VARID",
	"long VARID(eddobject variable_name);",
	c_type::tint32,
	1, VARID_types_0, VARID_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_VARID_0, builtins::check_VARID_0, 0,
	0,
};

static enum c_type::typespec WRITE_COMMAND_types_0[1] =
{
	c_type::teddobject
};
static unsigned int WRITE_COMMAND_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_WRITE_COMMAND_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::WRITE_COMMAND(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin WRITE_COMMAND_0 = 
{
	"WRITE_COMMAND",
	"long WRITE_COMMAND(eddobject command_name);",
	c_type::tint32,
	1, WRITE_COMMAND_types_0, WRITE_COMMAND_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_WRITE_COMMAND_0, builtins::check_WRITE_COMMAND_0, 0,
	0,
};

static enum c_type::typespec WriteCommand_types_0[1] =
{
	c_type::teddobject
};
static unsigned int WriteCommand_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_WriteCommand_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::WriteCommand(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin WriteCommand_0 = 
{
	"WriteCommand",
	"long WriteCommand(eddobject command_name);",
	c_type::tint32,
	1, WriteCommand_types_0, WriteCommand_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_WriteCommand_0, builtins::check_WriteCommand_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_ALL_COMM_STATUS_0 = 
{
	"XMTR_ABORT_ON_ALL_COMM_STATUS",
	"void XMTR_ABORT_ON_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_ALL_DATA_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_ALL_DATA(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_ALL_DATA_0 = 
{
	"XMTR_ABORT_ON_ALL_DATA",
	"void XMTR_ABORT_ON_ALL_DATA(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_ALL_DATA_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_ALL_DEVICE_STATUS_0 = 
{
	"XMTR_ABORT_ON_ALL_DEVICE_STATUS",
	"void XMTR_ABORT_ON_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_ALL_RESPONSE_CODES_0 = 
{
	"XMTR_ABORT_ON_ALL_RESPONSE_CODES",
	"void XMTR_ABORT_ON_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_COMM_ERROR_0 = 
{
	"XMTR_ABORT_ON_COMM_ERROR",
	"void XMTR_ABORT_ON_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_ABORT_ON_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_ABORT_ON_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_ABORT_ON_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_COMM_STATUS_0 = 
{
	"XMTR_ABORT_ON_COMM_STATUS",
	"void XMTR_ABORT_ON_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, XMTR_ABORT_ON_COMM_STATUS_types_0, XMTR_ABORT_ON_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_COMM_STATUS_0, builtins::check_XMTR_ABORT_ON_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec XMTR_ABORT_ON_DATA_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int XMTR_ABORT_ON_DATA_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_XMTR_ABORT_ON_DATA_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_DATA(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_DATA_0 = 
{
	"XMTR_ABORT_ON_DATA",
	"void XMTR_ABORT_ON_DATA(long byte, long bit);",
	c_type::tvoid,
	2, XMTR_ABORT_ON_DATA_types_0, XMTR_ABORT_ON_DATA_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_DATA_0, builtins::check_XMTR_ABORT_ON_DATA_0, 0,
	0,
};

static enum c_type::typespec XMTR_ABORT_ON_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_ABORT_ON_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_ABORT_ON_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_DEVICE_STATUS_0 = 
{
	"XMTR_ABORT_ON_DEVICE_STATUS",
	"void XMTR_ABORT_ON_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, XMTR_ABORT_ON_DEVICE_STATUS_types_0, XMTR_ABORT_ON_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_DEVICE_STATUS_0, builtins::check_XMTR_ABORT_ON_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_ABORT_ON_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_NO_DEVICE_0 = 
{
	"XMTR_ABORT_ON_NO_DEVICE",
	"void XMTR_ABORT_ON_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_ABORT_ON_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_ABORT_ON_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_ABORT_ON_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_ABORT_ON_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_ABORT_ON_RESPONSE_CODE_0 = 
{
	"XMTR_ABORT_ON_RESPONSE_CODE",
	"void XMTR_ABORT_ON_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, XMTR_ABORT_ON_RESPONSE_CODE_types_0, XMTR_ABORT_ON_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_ABORT_ON_RESPONSE_CODE_0, builtins::check_XMTR_ABORT_ON_RESPONSE_CODE_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_ALL_COMM_STATUS_0 = 
{
	"XMTR_IGNORE_ALL_COMM_STATUS",
	"void XMTR_IGNORE_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_ALL_DATA_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_ALL_DATA(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_ALL_DATA_0 = 
{
	"XMTR_IGNORE_ALL_DATA",
	"void XMTR_IGNORE_ALL_DATA(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_ALL_DATA_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_ALL_DEVICE_STATUS_0 = 
{
	"XMTR_IGNORE_ALL_DEVICE_STATUS",
	"void XMTR_IGNORE_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_ALL_RESPONSE_CODES_0 = 
{
	"XMTR_IGNORE_ALL_RESPONSE_CODES",
	"void XMTR_IGNORE_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_COMM_ERROR_0 = 
{
	"XMTR_IGNORE_COMM_ERROR",
	"void XMTR_IGNORE_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_IGNORE_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_IGNORE_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_IGNORE_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_COMM_STATUS_0 = 
{
	"XMTR_IGNORE_COMM_STATUS",
	"void XMTR_IGNORE_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, XMTR_IGNORE_COMM_STATUS_types_0, XMTR_IGNORE_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_COMM_STATUS_0, builtins::check_XMTR_IGNORE_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec XMTR_IGNORE_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_IGNORE_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_IGNORE_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_DEVICE_STATUS_0 = 
{
	"XMTR_IGNORE_DEVICE_STATUS",
	"void XMTR_IGNORE_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, XMTR_IGNORE_DEVICE_STATUS_types_0, XMTR_IGNORE_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_DEVICE_STATUS_0, builtins::check_XMTR_IGNORE_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_IGNORE_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_NO_DEVICE_0 = 
{
	"XMTR_IGNORE_NO_DEVICE",
	"void XMTR_IGNORE_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_IGNORE_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_IGNORE_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_IGNORE_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_IGNORE_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_IGNORE_RESPONSE_CODE_0 = 
{
	"XMTR_IGNORE_RESPONSE_CODE",
	"void XMTR_IGNORE_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, XMTR_IGNORE_RESPONSE_CODE_types_0, XMTR_IGNORE_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_IGNORE_RESPONSE_CODE_0, builtins::check_XMTR_IGNORE_RESPONSE_CODE_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_ALL_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_ALL_COMM_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_ALL_COMM_STATUS_0 = 
{
	"XMTR_RETRY_ON_ALL_COMM_STATUS",
	"void XMTR_RETRY_ON_ALL_COMM_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_ALL_COMM_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_ALL_DATA_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_ALL_DATA(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_ALL_DATA_0 = 
{
	"XMTR_RETRY_ON_ALL_DATA",
	"void XMTR_RETRY_ON_ALL_DATA(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_ALL_DATA_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_ALL_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_ALL_DEVICE_STATUS(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_ALL_DEVICE_STATUS_0 = 
{
	"XMTR_RETRY_ON_ALL_DEVICE_STATUS",
	"void XMTR_RETRY_ON_ALL_DEVICE_STATUS(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_ALL_DEVICE_STATUS_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_ALL_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_ALL_RESPONSE_CODE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_ALL_RESPONSE_CODE_0 = 
{
	"XMTR_RETRY_ON_ALL_RESPONSE_CODE",
	"void XMTR_RETRY_ON_ALL_RESPONSE_CODE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_ALL_RESPONSE_CODE_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_ALL_RESPONSE_CODES_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_ALL_RESPONSE_CODES(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_ALL_RESPONSE_CODES_0 = 
{
	"XMTR_RETRY_ON_ALL_RESPONSE_CODES",
	"void XMTR_RETRY_ON_ALL_RESPONSE_CODES(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_ALL_RESPONSE_CODES_0, 0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_COMM_ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_COMM_ERROR(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_COMM_ERROR_0 = 
{
	"XMTR_RETRY_ON_COMM_ERROR",
	"void XMTR_RETRY_ON_COMM_ERROR(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_COMM_ERROR_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_RETRY_ON_COMM_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_RETRY_ON_COMM_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_RETRY_ON_COMM_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_COMM_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_COMM_STATUS_0 = 
{
	"XMTR_RETRY_ON_COMM_STATUS",
	"void XMTR_RETRY_ON_COMM_STATUS(long comm_status_value);",
	c_type::tvoid,
	1, XMTR_RETRY_ON_COMM_STATUS_types_0, XMTR_RETRY_ON_COMM_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_COMM_STATUS_0, builtins::check_XMTR_RETRY_ON_COMM_STATUS_0, 0,
	0,
};

static enum c_type::typespec XMTR_RETRY_ON_DATA_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int XMTR_RETRY_ON_DATA_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_XMTR_RETRY_ON_DATA_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_DATA(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_DATA_0 = 
{
	"XMTR_RETRY_ON_DATA",
	"void XMTR_RETRY_ON_DATA(long byte, long bit);",
	c_type::tvoid,
	2, XMTR_RETRY_ON_DATA_types_0, XMTR_RETRY_ON_DATA_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_DATA_0, builtins::check_XMTR_RETRY_ON_DATA_0, 0,
	0,
};

static enum c_type::typespec XMTR_RETRY_ON_DEVICE_STATUS_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_RETRY_ON_DEVICE_STATUS_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_RETRY_ON_DEVICE_STATUS_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_DEVICE_STATUS(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_DEVICE_STATUS_0 = 
{
	"XMTR_RETRY_ON_DEVICE_STATUS",
	"void XMTR_RETRY_ON_DEVICE_STATUS(long device_status_value);",
	c_type::tvoid,
	1, XMTR_RETRY_ON_DEVICE_STATUS_types_0, XMTR_RETRY_ON_DEVICE_STATUS_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_DEVICE_STATUS_0, builtins::check_XMTR_RETRY_ON_DEVICE_STATUS_0, 0,
	0,
};

static operand_ptr
dispatch_XMTR_RETRY_ON_NO_DEVICE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_NO_DEVICE(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_NO_DEVICE_0 = 
{
	"XMTR_RETRY_ON_NO_DEVICE",
	"void XMTR_RETRY_ON_NO_DEVICE(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_NO_DEVICE_0, 0, 0,
	0,
};

static enum c_type::typespec XMTR_RETRY_ON_RESPONSE_CODE_types_0[1] =
{
	c_type::tint32
};
static unsigned int XMTR_RETRY_ON_RESPONSE_CODE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_XMTR_RETRY_ON_RESPONSE_CODE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::XMTR_RETRY_ON_RESPONSE_CODE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin XMTR_RETRY_ON_RESPONSE_CODE_0 = 
{
	"XMTR_RETRY_ON_RESPONSE_CODE",
	"void XMTR_RETRY_ON_RESPONSE_CODE(long response_code_value);",
	c_type::tvoid,
	1, XMTR_RETRY_ON_RESPONSE_CODE_types_0, XMTR_RETRY_ON_RESPONSE_CODE_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_XMTR_RETRY_ON_RESPONSE_CODE_0, builtins::check_XMTR_RETRY_ON_RESPONSE_CODE_0, 0,
	0,
};

static enum c_type::typespec YearMonthDay_to_Date_types_0[3] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tint32
};
static unsigned int YearMonthDay_to_Date_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin YearMonthDay_to_Date_0 = 
{
	"YearMonthDay_to_Date",
	"long YearMonthDay_to_Date(long year, long month, long day_of_month);",
	c_type::tint32,
	3, YearMonthDay_to_Date_types_0, YearMonthDay_to_Date_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec _ERROR_types_3[1] =
{
	c_type::tuint32
};
static unsigned int _ERROR_byref_3[1] = 
{
	0
};

static operand_ptr
dispatch__ERROR_3(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_ERROR_3(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _ERROR_3 = 
{
	"_ERROR",
	"void _ERROR(unsigned long value);",
	c_type::tvoid,
	1, _ERROR_types_3, _ERROR_byref_3,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__ERROR_3, 0, 0,
	0,
};

static enum c_type::typespec _ERROR_types_2[1] =
{
	c_type::tfloat32
};
static unsigned int _ERROR_byref_2[1] = 
{
	0
};

static operand_ptr
dispatch__ERROR_2(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_ERROR_2(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _ERROR_2 = 
{
	"_ERROR",
	"void _ERROR(float value);",
	c_type::tvoid,
	1, _ERROR_types_2, _ERROR_byref_2,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__ERROR_2, 0, 0,
	&_ERROR_3,
};

static enum c_type::typespec _ERROR_types_1[1] =
{
	c_type::tstring
};
static unsigned int _ERROR_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch__ERROR_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_ERROR_1(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _ERROR_1 = 
{
	"_ERROR",
	"void _ERROR(string message);",
	c_type::tvoid,
	1, _ERROR_types_1, _ERROR_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__ERROR_1, 0, 0,
	&_ERROR_2,
};

static enum c_type::typespec _ERROR_types_0[1] =
{
	c_type::tint32
};
static unsigned int _ERROR_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch__ERROR_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_ERROR(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _ERROR_0 = 
{
	"_ERROR",
	"void _ERROR(long value);",
	c_type::tvoid,
	1, _ERROR_types_0, _ERROR_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__ERROR_0, 0, 0,
	&_ERROR_1,
};

static enum c_type::typespec _TRACE_types_3[1] =
{
	c_type::tuint32
};
static unsigned int _TRACE_byref_3[1] = 
{
	0
};

static operand_ptr
dispatch__TRACE_3(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_TRACE_3(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _TRACE_3 = 
{
	"_TRACE",
	"void _TRACE(unsigned long value);",
	c_type::tvoid,
	1, _TRACE_types_3, _TRACE_byref_3,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__TRACE_3, 0, 0,
	0,
};

static enum c_type::typespec _TRACE_types_2[1] =
{
	c_type::tfloat32
};
static unsigned int _TRACE_byref_2[1] = 
{
	0
};

static operand_ptr
dispatch__TRACE_2(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_TRACE_2(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _TRACE_2 = 
{
	"_TRACE",
	"void _TRACE(float value);",
	c_type::tvoid,
	1, _TRACE_types_2, _TRACE_byref_2,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__TRACE_2, 0, 0,
	&_TRACE_3,
};

static enum c_type::typespec _TRACE_types_1[1] =
{
	c_type::tstring
};
static unsigned int _TRACE_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch__TRACE_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_TRACE_1(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _TRACE_1 = 
{
	"_TRACE",
	"void _TRACE(string message);",
	c_type::tvoid,
	1, _TRACE_types_1, _TRACE_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__TRACE_1, 0, 0,
	&_TRACE_2,
};

static enum c_type::typespec _TRACE_types_0[1] =
{
	c_type::tint32
};
static unsigned int _TRACE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch__TRACE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_TRACE(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _TRACE_0 = 
{
	"_TRACE",
	"void _TRACE(long value);",
	c_type::tvoid,
	1, _TRACE_types_0, _TRACE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__TRACE_0, 0, 0,
	&_TRACE_1,
};

static enum c_type::typespec _WARNING_types_3[1] =
{
	c_type::tuint32
};
static unsigned int _WARNING_byref_3[1] = 
{
	0
};

static operand_ptr
dispatch__WARNING_3(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_WARNING_3(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _WARNING_3 = 
{
	"_WARNING",
	"void _WARNING(unsigned long value);",
	c_type::tvoid,
	1, _WARNING_types_3, _WARNING_byref_3,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__WARNING_3, 0, 0,
	0,
};

static enum c_type::typespec _WARNING_types_2[1] =
{
	c_type::tfloat32
};
static unsigned int _WARNING_byref_2[1] = 
{
	0
};

static operand_ptr
dispatch__WARNING_2(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_WARNING_2(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _WARNING_2 = 
{
	"_WARNING",
	"void _WARNING(float value);",
	c_type::tvoid,
	1, _WARNING_types_2, _WARNING_byref_2,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__WARNING_2, 0, 0,
	&_WARNING_3,
};

static enum c_type::typespec _WARNING_types_1[1] =
{
	c_type::tstring
};
static unsigned int _WARNING_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch__WARNING_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_WARNING_1(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _WARNING_1 = 
{
	"_WARNING",
	"void _WARNING(string message);",
	c_type::tvoid,
	1, _WARNING_types_1, _WARNING_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__WARNING_1, 0, 0,
	&_WARNING_2,
};

static enum c_type::typespec _WARNING_types_0[1] =
{
	c_type::tint32
};
static unsigned int _WARNING_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch__WARNING_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::_WARNING(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin _WARNING_0 = 
{
	"_WARNING",
	"void _WARNING(long value);",
	c_type::tvoid,
	1, _WARNING_types_0, _WARNING_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch__WARNING_0, 0, 0,
	&_WARNING_1,
};

static operand_ptr
dispatch_abort_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::abort(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin abort_0 = 
{
	"abort",
	"void abort(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_abort_0, 0, 0,
	0,
};

static const struct builtin abort_on_all_comm_errors_0 = 
{
	"abort_on_all_comm_errors",
	"void abort_on_all_comm_errors(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin abort_on_all_response_codes_0 = 
{
	"abort_on_all_response_codes",
	"void abort_on_all_response_codes(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec abort_on_comm_error_types_0[1] =
{
	c_type::tuint32
};
static unsigned int abort_on_comm_error_byref_0[1] = 
{
	0
};

static const struct builtin abort_on_comm_error_0 = 
{
	"abort_on_comm_error",
	"long abort_on_comm_error(unsigned long error);",
	c_type::tint32,
	1, abort_on_comm_error_types_0, abort_on_comm_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec abort_on_response_code_types_0[1] =
{
	c_type::tuint32
};
static unsigned int abort_on_response_code_byref_0[1] = 
{
	0
};

static const struct builtin abort_on_response_code_0 = 
{
	"abort_on_response_code",
	"long abort_on_response_code(unsigned long code);",
	c_type::tint32,
	1, abort_on_response_code_types_0, abort_on_response_code_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec abs_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int abs_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_abs_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::abs_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin abs_1 = 
{
	"abs",
	"double abs(double value);",
	c_type::tfloat64,
	1, abs_types_1, abs_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_abs_1, 0, 0,
	0,
};

static enum c_type::typespec abs_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int abs_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_abs_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::abs(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin abs_0 = 
{
	"abs",
	"float abs(float value);",
	c_type::tfloat32,
	1, abs_types_0, abs_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_abs_0, 0, 0,
	&abs_1,
};

static enum c_type::typespec acknowledge_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int acknowledge_byref_0[2] = 
{
	0,
	2
};

static operand_ptr
dispatch_acknowledge_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::acknowledge(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin acknowledge_0 = 
{
	"acknowledge",
	"long acknowledge(string prompt, long global_var_ids[]);",
	c_type::tint32,
	2, acknowledge_types_0, acknowledge_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_acknowledge_0, 0, 0,
	0,
};

static enum c_type::typespec acos_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int acos_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_acos_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::acos_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin acos_1 = 
{
	"acos",
	"double acos(double value);",
	c_type::tfloat64,
	1, acos_types_1, acos_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_acos_1, 0, 0,
	0,
};

static enum c_type::typespec acos_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int acos_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_acos_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::acos(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin acos_0 = 
{
	"acos",
	"float acos(float value);",
	c_type::tfloat32,
	1, acos_types_0, acos_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_acos_0, 0, 0,
	&acos_1,
};

static enum c_type::typespec add_abort_method_types_1[1] =
{
	c_type::tuint32
};
static unsigned int add_abort_method_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_add_abort_method_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::add_abort_method_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin add_abort_method_1 = 
{
	"add_abort_method",
	"long add_abort_method(unsigned long method_id);",
	c_type::tint32,
	1, add_abort_method_types_1, add_abort_method_byref_1,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_add_abort_method_1, 0, 0,
	0,
};

static enum c_type::typespec add_abort_method_types_0[1] =
{
	c_type::teddobject
};
static unsigned int add_abort_method_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_add_abort_method_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::add_abort_method(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin add_abort_method_0 = 
{
	"add_abort_method",
	"long add_abort_method(eddobject abort_method_name);",
	c_type::tint32,
	1, add_abort_method_types_0, add_abort_method_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_add_abort_method_0, builtins::check_add_abort_method_0, 0,
	&add_abort_method_1,
};

static enum c_type::typespec asin_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int asin_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_asin_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::asin_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin asin_1 = 
{
	"asin",
	"double asin(double value);",
	c_type::tfloat64,
	1, asin_types_1, asin_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_asin_1, 0, 0,
	0,
};

static enum c_type::typespec asin_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int asin_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_asin_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::asin(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin asin_0 = 
{
	"asin",
	"float asin(float value);",
	c_type::tfloat32,
	1, asin_types_0, asin_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_asin_0, 0, 0,
	&asin_1,
};

static enum c_type::typespec assign_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int assign_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin assign_0 = 
{
	"assign",
	"long assign(unsigned long dst_id, unsigned long dst_member_id, unsigned long src_id, unsigned long src_member_id);",
	c_type::tint32,
	4, assign_types_0, assign_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec assign_double_types_0[2] =
{
	c_type::teddobject,
	c_type::tfloat64
};
static unsigned int assign_double_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_double_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_double(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_double_0 = 
{
	"assign_double",
	"long assign_double(eddobject device_var, double new_value);",
	c_type::tint32,
	2, assign_double_types_0, assign_double_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_assign_double_0, builtins::check_assign_double_0, 0,
	0,
};

static enum c_type::typespec assign_float_types_0[2] =
{
	c_type::teddobject,
	c_type::tfloat64
};
static unsigned int assign_float_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_float_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_float(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_float_0 = 
{
	"assign_float",
	"long assign_float(eddobject device_var, double new_value);",
	c_type::tint32,
	2, assign_float_types_0, assign_float_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_assign_float_0, builtins::check_assign_float_0, 0,
	0,
};

static enum c_type::typespec assign_int_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int assign_int_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_int_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_int(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_int_0 = 
{
	"assign_int",
	"long assign_int(eddobject device_var, long new_value);",
	c_type::tint32,
	2, assign_int_types_0, assign_int_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_assign_int_0, builtins::check_assign_int_0, 0,
	0,
};

static enum c_type::typespec assign_long_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int assign_long_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_long_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_long(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_long_0 = 
{
	"assign_long",
	"long assign_long(eddobject device_var, long new_value);",
	c_type::tint32,
	2, assign_long_types_0, assign_long_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_assign_long_0, builtins::check_assign_long_0, 0,
	0,
};

static enum c_type::typespec assign_str_types_0[2] =
{
	c_type::teddobject,
	c_type::tstring
};
static unsigned int assign_str_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_str_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_str(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_str_0 = 
{
	"assign_str",
	"long assign_str(eddobject device_var, string new_value);",
	c_type::tint32,
	2, assign_str_types_0, assign_str_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_assign_str_0, builtins::check_assign_str_0, 0,
	0,
};

static enum c_type::typespec assign_var_types_0[2] =
{
	c_type::teddobject,
	c_type::teddobject
};
static unsigned int assign_var_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_assign_var_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	class tree * arg1;
	{
		/* it's a real operand here */
		arg1 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::assign_var(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin assign_var_0 = 
{
	"assign_var",
	"long assign_var(eddobject destination_var, eddobject source_var);",
	c_type::tint32,
	2, assign_var_types_0, assign_var_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_assign_var_0, builtins::check_assign_var_0, 0,
	0,
};

static enum c_type::typespec atan_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int atan_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_atan_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::atan_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin atan_1 = 
{
	"atan",
	"double atan(double value);",
	c_type::tfloat64,
	1, atan_types_1, atan_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_atan_1, 0, 0,
	0,
};

static enum c_type::typespec atan_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int atan_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_atan_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::atan(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin atan_0 = 
{
	"atan",
	"float atan(float value);",
	c_type::tfloat32,
	1, atan_types_0, atan_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_atan_0, 0, 0,
	&atan_1,
};

static enum c_type::typespec atof_types_0[1] =
{
	c_type::tstring
};
static unsigned int atof_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_atof_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::atof(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin atof_0 = 
{
	"atof",
	"float atof(string text);",
	c_type::tfloat32,
	1, atof_types_0, atof_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	dispatch_atof_0, 0, 0,
	0,
};

static enum c_type::typespec atoi_types_0[1] =
{
	c_type::tstring
};
static unsigned int atoi_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_atoi_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::atoi(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin atoi_0 = 
{
	"atoi",
	"long atoi(string text);",
	c_type::tint32,
	1, atoi_types_0, atoi_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	dispatch_atoi_0, 0, 0,
	0,
};

static enum c_type::typespec atol_types_0[1] =
{
	c_type::tstring
};
static unsigned int atol_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_atol_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::atol(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin atol_0 = 
{
	"atol",
	"long atol(string text);",
	c_type::tint32,
	1, atol_types_0, atol_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_atol_0, 0, 0,
	0,
};

static enum c_type::typespec cbrt_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int cbrt_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_cbrt_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::cbrt_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cbrt_1 = 
{
	"cbrt",
	"double cbrt(double x);",
	c_type::tfloat64,
	1, cbrt_types_1, cbrt_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cbrt_1, 0, 0,
	0,
};

static enum c_type::typespec cbrt_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int cbrt_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_cbrt_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::cbrt(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cbrt_0 = 
{
	"cbrt",
	"float cbrt(float x);",
	c_type::tfloat32,
	1, cbrt_types_0, cbrt_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cbrt_0, 0, 0,
	&cbrt_1,
};

static enum c_type::typespec ceil_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int ceil_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_ceil_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::ceil_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ceil_1 = 
{
	"ceil",
	"double ceil(double x);",
	c_type::tfloat64,
	1, ceil_types_1, ceil_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_ceil_1, 0, 0,
	0,
};

static enum c_type::typespec ceil_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int ceil_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ceil_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::ceil(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ceil_0 = 
{
	"ceil",
	"float ceil(float x);",
	c_type::tfloat32,
	1, ceil_types_0, ceil_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_ceil_0, 0, 0,
	&ceil_1,
};

static enum c_type::typespec cos_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int cos_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_cos_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::cos_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cos_1 = 
{
	"cos",
	"double cos(double x);",
	c_type::tfloat64,
	1, cos_types_1, cos_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cos_1, 0, 0,
	0,
};

static enum c_type::typespec cos_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int cos_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_cos_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::cos(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cos_0 = 
{
	"cos",
	"float cos(float x);",
	c_type::tfloat32,
	1, cos_types_0, cos_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cos_0, 0, 0,
	&cos_1,
};

static enum c_type::typespec cosh_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int cosh_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_cosh_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::cosh_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cosh_1 = 
{
	"cosh",
	"double cosh(double x);",
	c_type::tfloat64,
	1, cosh_types_1, cosh_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cosh_1, 0, 0,
	0,
};

static enum c_type::typespec cosh_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int cosh_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_cosh_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::cosh(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin cosh_0 = 
{
	"cosh",
	"float cosh(float x);",
	c_type::tfloat32,
	1, cosh_types_0, cosh_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_cosh_0, 0, 0,
	&cosh_1,
};

static enum c_type::typespec dassign_types_0[2] =
{
	c_type::teddobject,
	c_type::tfloat64
};
static unsigned int dassign_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_dassign_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::dassign(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin dassign_0 = 
{
	"dassign",
	"long dassign(eddobject device_var, double new_value);",
	c_type::tint32,
	2, dassign_types_0, dassign_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_dassign_0, builtins::check_dassign_0, 0,
	0,
};

static enum c_type::typespec delay_types_0[3] =
{
	c_type::tint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int delay_byref_0[3] = 
{
	0,
	0,
	2
};

static operand_ptr
dispatch_delay_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::delay(env, pos, arg0, arg1, arg2);
	
	/* return value */
	return new no_value();
}

static const struct builtin delay_0 = 
{
	"delay",
	"void delay(long delay_time, string prompt, long global_var_ids[]);",
	c_type::tvoid,
	3, delay_types_0, delay_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_delay_0, 0, 0,
	0,
};

static enum c_type::typespec delayfor_types_0[5] =
{
	c_type::tint32,
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int delayfor_byref_0[5] = 
{
	0,
	0,
	2,
	2,
	0
};

static operand_ptr
dispatch_delayfor_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_uint32_array arg2(args);
	args = args.next();
	
	edd_uint32_array arg3(args);
	args = args.next();
	
	edd_int32 arg4;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg4 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::delayfor(env, pos, arg0, arg1, arg2, arg3, arg4);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin delayfor_0 = 
{
	"delayfor",
	"long delayfor(long duration, string prompt, unsigned long ids[], unsigned long indices[], long id_count);",
	c_type::tint32,
	5, delayfor_types_0, delayfor_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_delayfor_0, 0, 0,
	0,
};

static enum c_type::typespec dictionary_string_types_0[1] =
{
	c_type::tident
};
static unsigned int dictionary_string_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_dictionary_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	lib::IDENTIFIER arg0;
	{
		/* it's a real operand here */
		arg0 = (lib::IDENTIFIER)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::dictionary_string(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin dictionary_string_0 = 
{
	"dictionary_string",
	"string dictionary_string(ident dict_string_name);",
	c_type::tstring,
	1, dictionary_string_types_0, dictionary_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_dictionary_string_0, builtins::check_dictionary_string_0, 0,
	0,
};

static operand_ptr
dispatch_discard_on_exit_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::discard_on_exit(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin discard_on_exit_0 = 
{
	"discard_on_exit",
	"void discard_on_exit(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_discard_on_exit_0, 0, 0,
	0,
};

static enum c_type::typespec display_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int display_byref_0[2] = 
{
	0,
	2
};

static operand_ptr
dispatch_display_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::display(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin display_0 = 
{
	"display",
	"void display(string prompt, long global_var_ids[]);",
	c_type::tvoid,
	2, display_types_0, display_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_display_0, 0, 0,
	0,
};

static enum c_type::typespec display_bitenum_types_0[1] =
{
	c_type::teddobject
};
static unsigned int display_bitenum_byref_0[1] = 
{
	0
};

static const struct builtin display_bitenum_0 = 
{
	"display_bitenum",
	"void display_bitenum(eddobject bitenum);",
	c_type::tvoid,
	1, display_bitenum_types_0, display_bitenum_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_builtin_error_types_0[1] =
{
	c_type::tint32
};
static unsigned int display_builtin_error_byref_0[1] = 
{
	0
};

static const struct builtin display_builtin_error_0 = 
{
	"display_builtin_error",
	"long display_builtin_error(long status);",
	c_type::tint32,
	1, display_builtin_error_types_0, display_builtin_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_comm_error_types_0[1] =
{
	c_type::tuint32
};
static unsigned int display_comm_error_byref_0[1] = 
{
	0
};

static const struct builtin display_comm_error_0 = 
{
	"display_comm_error",
	"long display_comm_error(unsigned long error);",
	c_type::tint32,
	1, display_comm_error_types_0, display_comm_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_comm_status_types_0[1] =
{
	c_type::tint32
};
static unsigned int display_comm_status_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_display_comm_status_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::display_comm_status(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin display_comm_status_0 = 
{
	"display_comm_status",
	"void display_comm_status(long comm_status_value);",
	c_type::tvoid,
	1, display_comm_status_types_0, display_comm_status_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_display_comm_status_0, builtins::check_display_comm_status_0, 0,
	0,
};

static enum c_type::typespec display_device_status_types_0[1] =
{
	c_type::tint32
};
static unsigned int display_device_status_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_display_device_status_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::display_device_status(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin display_device_status_0 = 
{
	"display_device_status",
	"void display_device_status(long device_status_value);",
	c_type::tvoid,
	1, display_device_status_types_0, display_device_status_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_display_device_status_0, builtins::check_display_device_status_0, 0,
	0,
};

static enum c_type::typespec display_dynamics_types_0[4] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int display_dynamics_byref_0[4] = 
{
	0,
	2,
	2,
	0
};

static const struct builtin display_dynamics_0 = 
{
	"display_dynamics",
	"long display_dynamics(string prompt, unsigned long ids[], unsigned long indices[], long id_count);",
	c_type::tint32,
	4, display_dynamics_types_0, display_dynamics_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_message_types_0[4] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int display_message_byref_0[4] = 
{
	0,
	2,
	2,
	0
};

static const struct builtin display_message_0 = 
{
	"display_message",
	"long display_message(string prompt, unsigned long ids[], unsigned long indices[], long id_count);",
	c_type::tint32,
	4, display_message_types_0, display_message_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_response_code_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int display_response_code_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin display_response_code_0 = 
{
	"display_response_code",
	"long display_response_code(unsigned long id, unsigned long member_id, unsigned long code);",
	c_type::tint32,
	3, display_response_code_types_0, display_response_code_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec display_response_status_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int display_response_status_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_display_response_status_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::display_response_status(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin display_response_status_0 = 
{
	"display_response_status",
	"void display_response_status(long cmd, long response_code_value);",
	c_type::tvoid,
	2, display_response_status_types_0, display_response_status_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_display_response_status_0, builtins::check_display_response_status_0, 0,
	0,
};

static enum c_type::typespec display_xmtr_status_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int display_xmtr_status_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_display_xmtr_status_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::display_xmtr_status(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin display_xmtr_status_0 = 
{
	"display_xmtr_status",
	"void display_xmtr_status(eddobject xmtr_variable, long response_code);",
	c_type::tvoid,
	2, display_xmtr_status_types_0, display_xmtr_status_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_display_xmtr_status_0, builtins::check_display_xmtr_status_0, 0,
	0,
};

static enum c_type::typespec edit_device_value_types_0[6] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int edit_device_value_byref_0[6] = 
{
	0,
	2,
	2,
	0,
	0,
	0
};

static operand_ptr
dispatch_edit_device_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_uint32_array arg1(args);
	args = args.next();
	
	edd_uint32_array arg2(args);
	args = args.next();
	
	edd_int32 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_uint32 arg4;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg4 = (edd_uint32)(*data);
	}
	args = args.next();
	
	edd_uint32 arg5;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg5 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::edit_device_value(env, pos, arg0, arg1, arg2, arg3, arg4, arg5);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin edit_device_value_0 = 
{
	"edit_device_value",
	"long edit_device_value(string prompt, unsigned long ids[], unsigned long indices[], long id_count, unsigned long param_id, unsigned long param_member_id);",
	c_type::tint32,
	6, edit_device_value_types_0, edit_device_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_edit_device_value_0, 0, 0,
	0,
};

static enum c_type::typespec edit_local_value_types_0[5] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tstring
};
static unsigned int edit_local_value_byref_0[5] = 
{
	0,
	2,
	2,
	0,
	0
};

static operand_ptr
dispatch_edit_local_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_uint32_array arg1(args);
	args = args.next();
	
	edd_uint32_array arg2(args);
	args = args.next();
	
	edd_int32 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string arg4;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg4 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::edit_local_value(env, pos, arg0, arg1, arg2, arg3, arg4);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin edit_local_value_0 = 
{
	"edit_local_value",
	"long edit_local_value(string prompt, unsigned long ids[], unsigned long indices[], long id_count, string local_var);",
	c_type::tint32,
	5, edit_local_value_types_0, edit_local_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_edit_local_value_0, 0, 0,
	0,
};

static enum c_type::typespec exp_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int exp_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_exp_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::exp_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin exp_1 = 
{
	"exp",
	"double exp(double x);",
	c_type::tfloat64,
	1, exp_types_1, exp_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_exp_1, 0, 0,
	0,
};

static enum c_type::typespec exp_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int exp_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_exp_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::exp(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin exp_0 = 
{
	"exp",
	"float exp(float x);",
	c_type::tfloat32,
	1, exp_types_0, exp_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_exp_0, 0, 0,
	&exp_1,
};

static enum c_type::typespec ext_send_command_types_0[4] =
{
	c_type::tint32,
	c_type::tstring,
	c_type::tstring,
	c_type::tstring
};
static unsigned int ext_send_command_byref_0[4] = 
{
	0,
	1,
	1,
	1
};

static operand_ptr
dispatch_ext_send_command_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	edd_string_opmap arg3(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ext_send_command(env, pos, arg0, arg1, arg2, arg3);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ext_send_command_0 = 
{
	"ext_send_command",
	"long ext_send_command(long cmd_number, string &cmd_status, string &more_data_status, string &more_data_info);",
	c_type::tint32,
	4, ext_send_command_types_0, ext_send_command_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ext_send_command_0, builtins::check_ext_send_command_0, 0,
	0,
};

static enum c_type::typespec ext_send_command_trans_types_0[5] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tstring,
	c_type::tstring,
	c_type::tstring
};
static unsigned int ext_send_command_trans_byref_0[5] = 
{
	0,
	0,
	1,
	1,
	1
};

static operand_ptr
dispatch_ext_send_command_trans_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	edd_string_opmap arg3(args);
	args = args.next();
	
	edd_string_opmap arg4(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ext_send_command_trans(env, pos, arg0, arg1, arg2, arg3, arg4);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ext_send_command_trans_0 = 
{
	"ext_send_command_trans",
	"long ext_send_command_trans(long cmd_number, long transaction, string &cmd_status, string &more_data_status, string &more_data_info);",
	c_type::tint32,
	5, ext_send_command_trans_types_0, ext_send_command_trans_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ext_send_command_trans_0, builtins::check_ext_send_command_trans_0, 0,
	0,
};

static const struct builtin fail_on_all_comm_errors_0 = 
{
	"fail_on_all_comm_errors",
	"void fail_on_all_comm_errors(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin fail_on_all_response_codes_0 = 
{
	"fail_on_all_response_codes",
	"void fail_on_all_response_codes(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec fail_on_comm_error_types_0[1] =
{
	c_type::tuint32
};
static unsigned int fail_on_comm_error_byref_0[1] = 
{
	0
};

static const struct builtin fail_on_comm_error_0 = 
{
	"fail_on_comm_error",
	"long fail_on_comm_error(unsigned long error);",
	c_type::tint32,
	1, fail_on_comm_error_types_0, fail_on_comm_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec fail_on_response_code_types_0[1] =
{
	c_type::tuint32
};
static unsigned int fail_on_response_code_byref_0[1] = 
{
	0
};

static const struct builtin fail_on_response_code_0 = 
{
	"fail_on_response_code",
	"long fail_on_response_code(unsigned long code);",
	c_type::tint32,
	1, fail_on_response_code_types_0, fail_on_response_code_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec fassign_types_0[2] =
{
	c_type::teddobject,
	c_type::tfloat64
};
static unsigned int fassign_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_fassign_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::fassign(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fassign_0 = 
{
	"fassign",
	"long fassign(eddobject device_var, double new_value);",
	c_type::tint32,
	2, fassign_types_0, fassign_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_fassign_0, builtins::check_fassign_0, 0,
	0,
};

static operand_ptr
dispatch_fgetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::fgetval(env, pos);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fgetval_0 = 
{
	"fgetval",
	"double fgetval(void);",
	c_type::tfloat64,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_fgetval_0, builtins::check_fgetval_0, 0,
	0,
};

static enum c_type::typespec float_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int float_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_float_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::float_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin float_value_0 = 
{
	"float_value",
	"double float_value(eddobject source_var_name);",
	c_type::tfloat64,
	1, float_value_types_0, float_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_float_value_0, builtins::check_float_value_0, 0,
	0,
};

static enum c_type::typespec floor_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int floor_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_floor_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::floor_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin floor_1 = 
{
	"floor",
	"double floor(double x);",
	c_type::tfloat64,
	1, floor_types_1, floor_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_floor_1, 0, 0,
	0,
};

static enum c_type::typespec floor_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int floor_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_floor_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::floor(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin floor_0 = 
{
	"floor",
	"float floor(float x);",
	c_type::tfloat32,
	1, floor_types_0, floor_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_floor_0, 0, 0,
	&floor_1,
};

static enum c_type::typespec fmod_types_1[2] =
{
	c_type::tfloat64,
	c_type::tfloat64
};
static unsigned int fmod_byref_1[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_fmod_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::fmod_1(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fmod_1 = 
{
	"fmod",
	"double fmod(double x, double y);",
	c_type::tfloat64,
	2, fmod_types_1, fmod_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_fmod_1, 0, 0,
	0,
};

static enum c_type::typespec fmod_types_0[2] =
{
	c_type::tfloat32,
	c_type::tfloat32
};
static unsigned int fmod_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_fmod_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	edd_real32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::fmod(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fmod_0 = 
{
	"fmod",
	"float fmod(float x, float y);",
	c_type::tfloat32,
	2, fmod_types_0, fmod_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_fmod_0, 0, 0,
	&fmod_1,
};

static enum c_type::typespec fpclassify_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int fpclassify_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_fpclassify_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::fpclassify(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fpclassify_0 = 
{
	"fpclassify",
	"long fpclassify(double value);",
	c_type::tint32,
	1, fpclassify_types_0, fpclassify_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_fpclassify_0, 0, 0,
	0,
};

static enum c_type::typespec fsetval_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int fsetval_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_fsetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::fsetval(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fsetval_0 = 
{
	"fsetval",
	"double fsetval(double value);",
	c_type::tfloat64,
	1, fsetval_types_0, fsetval_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_fsetval_0, builtins::check_fsetval_0, 0,
	0,
};

static enum c_type::typespec ftoa_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int ftoa_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ftoa_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::ftoa(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ftoa_0 = 
{
	"ftoa",
	"string ftoa(double value);",
	c_type::tstring,
	1, ftoa_types_0, ftoa_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ftoa_0, 0, 0,
	0,
};

static enum c_type::typespec fvar_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int fvar_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_fvar_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::fvar_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin fvar_value_0 = 
{
	"fvar_value",
	"double fvar_value(eddobject source_var_name);",
	c_type::tfloat64,
	1, fvar_value_types_0, fvar_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_fvar_value_0, builtins::check_fvar_value_0, 0,
	0,
};

static enum c_type::typespec get_acknowledgement_types_0[4] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int get_acknowledgement_byref_0[4] = 
{
	0,
	2,
	2,
	0
};

static const struct builtin get_acknowledgement_0 = 
{
	"get_acknowledgement",
	"long get_acknowledgement(string prompt, unsigned long ids[], unsigned long indices[], long id_count);",
	c_type::tint32,
	4, get_acknowledgement_types_0, get_acknowledgement_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_acknowlegement_types_0[4] =
{
	c_type::tstring,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int get_acknowlegement_byref_0[4] = 
{
	0,
	2,
	2,
	0
};

static const struct builtin get_acknowlegement_0 = 
{
	"get_acknowlegement",
	"long get_acknowlegement(string prompt, unsigned long ids[], unsigned long indices[], long id_count);",
	c_type::tint32,
	4, get_acknowlegement_types_0, get_acknowlegement_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_block_instance_by_object_index_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_block_instance_by_object_index_byref_0[2] = 
{
	0,
	0
};

static const struct builtin get_block_instance_by_object_index_0 = 
{
	"get_block_instance_by_object_index",
	"long get_block_instance_by_object_index(unsigned long obj_idx, unsigned long blk_num);",
	c_type::tint32,
	2, get_block_instance_by_object_index_types_0, get_block_instance_by_object_index_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_block_instance_by_tag_types_0[3] =
{
	c_type::tuint32,
	c_type::tstring,
	c_type::tuint32
};
static unsigned int get_block_instance_by_tag_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin get_block_instance_by_tag_0 = 
{
	"get_block_instance_by_tag",
	"long get_block_instance_by_tag(unsigned long blk_id, string blk_tag, unsigned long blk_num);",
	c_type::tint32,
	3, get_block_instance_by_tag_types_0, get_block_instance_by_tag_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_block_instance_count_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_block_instance_count_byref_0[2] = 
{
	0,
	0
};

static const struct builtin get_block_instance_count_0 = 
{
	"get_block_instance_count",
	"long get_block_instance_count(unsigned long blk_id, unsigned long count);",
	c_type::tint32,
	2, get_block_instance_count_types_0, get_block_instance_count_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin get_comm_error_0 = 
{
	"get_comm_error",
	"unsigned long get_comm_error(void);",
	c_type::tuint32,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_comm_error_string_types_0[3] =
{
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_comm_error_string_byref_0[3] = 
{
	0,
	1,
	0
};

static const struct builtin get_comm_error_string_0 = 
{
	"get_comm_error_string",
	"long get_comm_error_string(unsigned long error, string &str, long maxlen);",
	c_type::tint32,
	3, get_comm_error_string_types_0, get_comm_error_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_date_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_date_byref_0[2] = 
{
	0,
	1
};

static const struct builtin get_date_0 = 
{
	"get_date",
	"long get_date(string data, long &size);",
	c_type::tint32,
	2, get_date_types_0, get_date_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_date_lelem_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_date_lelem_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	1,
	1
};

static const struct builtin get_date_lelem_0 = 
{
	"get_date_lelem",
	"long get_date_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id, string &data, long &size);",
	c_type::tint32,
	6, get_date_lelem_types_0, get_date_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_date_lelem2_types_0[8] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_date_lelem2_byref_0[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	1,
	1
};

static const struct builtin get_date_lelem2_0 = 
{
	"get_date_lelem2",
	"long get_date_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id, string &data, long &size);",
	c_type::tint32,
	8, get_date_lelem2_types_0, get_date_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_date_value_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_date_value_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin get_date_value_0 = 
{
	"get_date_value",
	"long get_date_value(unsigned long id, unsigned long member_id, string data, long size);",
	c_type::tint32,
	4, get_date_value_types_0, get_date_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_date_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_date_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin get_date_value2_0 = 
{
	"get_date_value2",
	"long get_date_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, string data, long size);",
	c_type::tint32,
	5, get_date_value2_types_0, get_date_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_dds_error_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_dds_error_byref_0[2] = 
{
	1,
	0
};

static const struct builtin get_dds_error_0 = 
{
	"get_dds_error",
	"long get_dds_error(string &str, long maxlen);",
	c_type::tint32,
	2, get_dds_error_types_0, get_dds_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_dev_var_value_types_0[3] =
{
	c_type::tstring,
	c_type::tint32,
	c_type::teddobject
};
static unsigned int get_dev_var_value_byref_0[3] = 
{
	0,
	2,
	0
};

static operand_ptr
dispatch_get_dev_var_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	class tree * arg2;
	{
		/* it's a real operand here */
		arg2 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_dev_var_value(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_dev_var_value_0 = 
{
	"get_dev_var_value",
	"long get_dev_var_value(string prompt, long ids[], eddobject device_var_name);",
	c_type::tint32,
	3, get_dev_var_value_types_0, get_dev_var_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_dev_var_value_0, builtins::check_get_dev_var_value_0, 0,
	0,
};

static enum c_type::typespec get_dictionary_string_types_1[2] =
{
	c_type::tident,
	c_type::tstring
};
static unsigned int get_dictionary_string_byref_1[2] = 
{
	0,
	1
};

static operand_ptr
dispatch_get_dictionary_string_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	lib::IDENTIFIER arg0;
	{
		/* it's a real operand here */
		arg0 = (lib::IDENTIFIER)(*args);
	}
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_dictionary_string_1(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_dictionary_string_1 = 
{
	"get_dictionary_string",
	"long get_dictionary_string(ident dict_string_name, string &str);",
	c_type::tint32,
	2, get_dictionary_string_types_1, get_dictionary_string_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_get_dictionary_string_1, builtins::check_get_dictionary_string_1, 0,
	0,
};

static enum c_type::typespec get_dictionary_string_types_0[3] =
{
	c_type::tident,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_dictionary_string_byref_0[3] = 
{
	0,
	1,
	0
};

static operand_ptr
dispatch_get_dictionary_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	lib::IDENTIFIER arg0;
	{
		/* it's a real operand here */
		arg0 = (lib::IDENTIFIER)(*args);
	}
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	edd_int32 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_dictionary_string(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_dictionary_string_0 = 
{
	"get_dictionary_string",
	"long get_dictionary_string(ident dict_string_name, string &str, long maxlen);",
	c_type::tint32,
	3, get_dictionary_string_types_0, get_dictionary_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_dictionary_string_0, builtins::check_get_dictionary_string_0, 0,
	&get_dictionary_string_1,
};

static enum c_type::typespec get_double_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int get_double_byref_0[1] = 
{
	1
};

static const struct builtin get_double_0 = 
{
	"get_double",
	"long get_double(double &value);",
	c_type::tint32,
	1, get_double_types_0, get_double_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_double_lelem_types_0[4] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_double_lelem_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin get_double_lelem_0 = 
{
	"get_double_lelem",
	"double get_double_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tfloat64,
	4, get_double_lelem_types_0, get_double_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_double_lelem2_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_double_lelem2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin get_double_lelem2_0 = 
{
	"get_double_lelem2",
	"double get_double_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tfloat64,
	6, get_double_lelem2_types_0, get_double_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_double_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int get_double_value_byref_0[3] = 
{
	0,
	0,
	1
};

static const struct builtin get_double_value_0 = 
{
	"get_double_value",
	"long get_double_value(unsigned long id, unsigned long member_id, double &value);",
	c_type::tint32,
	3, get_double_value_types_0, get_double_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_double_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int get_double_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	1
};

static const struct builtin get_double_value2_0 = 
{
	"get_double_value2",
	"long get_double_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, double &value);",
	c_type::tint32,
	5, get_double_value2_types_0, get_double_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_enum_string_types_0[3] =
{
	c_type::teddobject,
	c_type::tint32,
	c_type::tstring
};
static unsigned int get_enum_string_byref_0[3] = 
{
	0,
	0,
	1
};

static operand_ptr
dispatch_get_enum_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_enum_string(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_enum_string_0 = 
{
	"get_enum_string",
	"long get_enum_string(eddobject variable, long value, string &status_string);",
	c_type::tint32,
	3, get_enum_string_types_0, get_enum_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	dispatch_get_enum_string_0, builtins::check_get_enum_string_0, 0,
	0,
};

static enum c_type::typespec get_float_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int get_float_byref_0[1] = 
{
	1
};

static const struct builtin get_float_0 = 
{
	"get_float",
	"long get_float(float &value);",
	c_type::tint32,
	1, get_float_types_0, get_float_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_float_lelem_types_0[4] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_float_lelem_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin get_float_lelem_0 = 
{
	"get_float_lelem",
	"float get_float_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tfloat32,
	4, get_float_lelem_types_0, get_float_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_float_lelem2_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_float_lelem2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin get_float_lelem2_0 = 
{
	"get_float_lelem2",
	"float get_float_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tfloat32,
	6, get_float_lelem2_types_0, get_float_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_float_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat32
};
static unsigned int get_float_value_byref_0[3] = 
{
	0,
	0,
	1
};

static const struct builtin get_float_value_0 = 
{
	"get_float_value",
	"long get_float_value(unsigned long id, unsigned long member_id, float &value);",
	c_type::tint32,
	3, get_float_value_types_0, get_float_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_float_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat32
};
static unsigned int get_float_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	1
};

static const struct builtin get_float_value2_0 = 
{
	"get_float_value2",
	"long get_float_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, float &value);",
	c_type::tint32,
	5, get_float_value2_types_0, get_float_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_local_var_value_types_0[3] =
{
	c_type::tstring,
	c_type::tint32,
	c_type::tident
};
static unsigned int get_local_var_value_byref_0[3] = 
{
	0,
	2,
	0
};

static operand_ptr
dispatch_get_local_var_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	lib::IDENTIFIER arg2;
	{
		/* it's a real operand here */
		arg2 = (lib::IDENTIFIER)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_local_var_value(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_local_var_value_0 = 
{
	"get_local_var_value",
	"long get_local_var_value(string prompt, long global_var_ids[], ident local_var_name);",
	c_type::tint32,
	3, get_local_var_value_types_0, get_local_var_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_local_var_value_0, builtins::check_get_local_var_value_0, 0,
	0,
};

static enum c_type::typespec get_more_status_types_0[2] =
{
	c_type::tstring,
	c_type::tstring
};
static unsigned int get_more_status_byref_0[2] = 
{
	1,
	1
};

static operand_ptr
dispatch_get_more_status_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string_opmap arg0(args);
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_more_status(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_more_status_0 = 
{
	"get_more_status",
	"long get_more_status(string &more_data_status, string &more_data_info);",
	c_type::tint32,
	2, get_more_status_types_0, get_more_status_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_more_status_0, builtins::check_get_more_status_0, 0,
	0,
};

static const struct builtin get_resolve_status_0 = 
{
	"get_resolve_status",
	"unsigned long get_resolve_status(void);",
	c_type::tuint32,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_response_code_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_response_code_byref_0[3] = 
{
	1,
	1,
	1
};

static const struct builtin get_response_code_0 = 
{
	"get_response_code",
	"long get_response_code(unsigned long &resp_code, unsigned long &err_id, unsigned long &err_member_id);",
	c_type::tint32,
	3, get_response_code_types_0, get_response_code_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_response_code_string_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_response_code_string_byref_0[5] = 
{
	0,
	0,
	0,
	1,
	0
};

static const struct builtin get_response_code_string_0 = 
{
	"get_response_code_string",
	"long get_response_code_string(unsigned long id, unsigned long member_id, unsigned long code, string &str, long maxlen);",
	c_type::tint32,
	5, get_response_code_string_types_0, get_response_code_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_rspcode_string_types_0[4] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_rspcode_string_byref_0[4] = 
{
	0,
	0,
	1,
	0
};

static operand_ptr
dispatch_get_rspcode_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	edd_int32 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_rspcode_string(env, pos, arg0, arg1, arg2, arg3);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_rspcode_string_0 = 
{
	"get_rspcode_string",
	"long get_rspcode_string(long cmd_number, long response_code_value, string &response_string, long response_string_length);",
	c_type::tint32,
	4, get_rspcode_string_types_0, get_rspcode_string_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_rspcode_string_0, builtins::check_get_rspcode_string_0, 0,
	0,
};

static enum c_type::typespec get_rspcode_string_by_id_types_0[3] =
{
	c_type::teddobject,
	c_type::tint32,
	c_type::tstring
};
static unsigned int get_rspcode_string_by_id_byref_0[3] = 
{
	0,
	0,
	1
};

static operand_ptr
dispatch_get_rspcode_string_by_id_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_rspcode_string_by_id(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_rspcode_string_by_id_0 = 
{
	"get_rspcode_string_by_id",
	"long get_rspcode_string_by_id(eddobject command_name, long response_code_value, string &response_string);",
	c_type::tint32,
	3, get_rspcode_string_by_id_types_0, get_rspcode_string_by_id_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_get_rspcode_string_by_id_0, builtins::check_get_rspcode_string_by_id_0, 0,
	0,
};

static enum c_type::typespec get_signed_types_0[1] =
{
	c_type::tint32
};
static unsigned int get_signed_byref_0[1] = 
{
	1
};

static const struct builtin get_signed_0 = 
{
	"get_signed",
	"long get_signed(long &value);",
	c_type::tint32,
	1, get_signed_types_0, get_signed_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_signed_lelem_types_0[4] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_signed_lelem_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin get_signed_lelem_0 = 
{
	"get_signed_lelem",
	"long get_signed_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tint32,
	4, get_signed_lelem_types_0, get_signed_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_signed_lelem2_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_signed_lelem2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin get_signed_lelem2_0 = 
{
	"get_signed_lelem2",
	"long get_signed_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tint32,
	6, get_signed_lelem2_types_0, get_signed_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_signed_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int get_signed_value_byref_0[3] = 
{
	0,
	0,
	1
};

static const struct builtin get_signed_value_0 = 
{
	"get_signed_value",
	"long get_signed_value(unsigned long id, unsigned long member_id, long &value);",
	c_type::tint32,
	3, get_signed_value_types_0, get_signed_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_signed_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int get_signed_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	1
};

static const struct builtin get_signed_value2_0 = 
{
	"get_signed_value2",
	"long get_signed_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, long &value);",
	c_type::tint32,
	5, get_signed_value2_types_0, get_signed_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_status_code_string_types_0[4] =
{
	c_type::teddobject,
	c_type::tint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_status_code_string_byref_0[4] = 
{
	0,
	0,
	1,
	0
};

static operand_ptr
dispatch_get_status_code_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	edd_int32 arg3;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg3 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::get_status_code_string(env, pos, arg0, arg1, arg2, arg3);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin get_status_code_string_0 = 
{
	"get_status_code_string",
	"long get_status_code_string(eddobject variable_name, long status_code, string &status_string, long status_string_length);",
	c_type::tint32,
	4, get_status_code_string_types_0, get_status_code_string_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_get_status_code_string_0, builtins::check_get_status_code_string_0, 0,
	0,
};

static enum c_type::typespec get_status_string_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_status_string_byref_0[5] = 
{
	0,
	0,
	0,
	1,
	0
};

static const struct builtin get_status_string_0 = 
{
	"get_status_string",
	"long get_status_string(unsigned long id, unsigned long member_id, unsigned long status, string &str, long maxlen);",
	c_type::tint32,
	5, get_status_string_types_0, get_status_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_stddict_string_types_0[3] =
{
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_stddict_string_byref_0[3] = 
{
	0,
	1,
	0
};

static const struct builtin get_stddict_string_0 = 
{
	"get_stddict_string",
	"long get_stddict_string(unsigned long id, string &str, long maxlen);",
	c_type::tint32,
	3, get_stddict_string_types_0, get_stddict_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_string_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_string_byref_0[2] = 
{
	1,
	1
};

static const struct builtin get_string_0 = 
{
	"get_string",
	"long get_string(string &str, long &len);",
	c_type::tint32,
	2, get_string_types_0, get_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_string_lelem_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_string_lelem_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	1,
	1
};

static const struct builtin get_string_lelem_0 = 
{
	"get_string_lelem",
	"long get_string_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id, string &str, long &length);",
	c_type::tint32,
	6, get_string_lelem_types_0, get_string_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_string_lelem2_types_0[8] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_string_lelem2_byref_0[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	1,
	1
};

static const struct builtin get_string_lelem2_0 = 
{
	"get_string_lelem2",
	"long get_string_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id, string &str, long &length);",
	c_type::tint32,
	8, get_string_lelem2_types_0, get_string_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_string_value_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_string_value_byref_0[4] = 
{
	0,
	0,
	1,
	1
};

static const struct builtin get_string_value_0 = 
{
	"get_string_value",
	"long get_string_value(unsigned long id, unsigned long member_id, string &str, long &len);",
	c_type::tint32,
	4, get_string_value_types_0, get_string_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_string_value2_types_0[6] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int get_string_value2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	1,
	1
};

static const struct builtin get_string_value2_0 = 
{
	"get_string_value2",
	"long get_string_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, string &str, long &len);",
	c_type::tint32,
	6, get_string_value2_types_0, get_string_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_unsigned_types_0[1] =
{
	c_type::tuint32
};
static unsigned int get_unsigned_byref_0[1] = 
{
	1
};

static const struct builtin get_unsigned_0 = 
{
	"get_unsigned",
	"long get_unsigned(unsigned long &value);",
	c_type::tint32,
	1, get_unsigned_types_0, get_unsigned_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_unsigned_lelem_types_0[4] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_unsigned_lelem_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin get_unsigned_lelem_0 = 
{
	"get_unsigned_lelem",
	"unsigned long get_unsigned_lelem(unsigned long list_id, long index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tuint32,
	4, get_unsigned_lelem_types_0, get_unsigned_lelem_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_unsigned_lelem2_types_0[6] =
{
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_unsigned_lelem2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin get_unsigned_lelem2_0 = 
{
	"get_unsigned_lelem2",
	"unsigned long get_unsigned_lelem2(unsigned long list_id, long index, unsigned long embedded_list_id, long embedded_list_index, unsigned long element_id, unsigned long subelement_id);",
	c_type::tuint32,
	6, get_unsigned_lelem2_types_0, get_unsigned_lelem2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_unsigned_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_unsigned_value_byref_0[3] = 
{
	0,
	0,
	1
};

static const struct builtin get_unsigned_value_0 = 
{
	"get_unsigned_value",
	"long get_unsigned_value(unsigned long id, unsigned long member_id, unsigned long &value);",
	c_type::tint32,
	3, get_unsigned_value_types_0, get_unsigned_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_unsigned_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int get_unsigned_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	1
};

static const struct builtin get_unsigned_value2_0 = 
{
	"get_unsigned_value2",
	"long get_unsigned_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, unsigned long &value);",
	c_type::tint32,
	5, get_unsigned_value2_types_0, get_unsigned_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec get_variable_string_types_0[2] =
{
	c_type::teddobject,
	c_type::tstring
};
static unsigned int get_variable_string_byref_0[2] = 
{
	0,
	1
};

static operand_ptr
dispatch_get_variable_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::get_variable_string(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin get_variable_string_0 = 
{
	"get_variable_string",
	"void get_variable_string(eddobject variable, string &out);",
	c_type::tvoid,
	2, get_variable_string_types_0, get_variable_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_get_variable_string_0, builtins::check_get_variable_string_0, 0,
	0,
};

static enum c_type::typespec iassign_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int iassign_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_iassign_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::iassign(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin iassign_0 = 
{
	"iassign",
	"long iassign(eddobject device_var, long new_value);",
	c_type::tint32,
	2, iassign_types_0, iassign_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_iassign_0, builtins::check_iassign_0, 0,
	0,
};

static operand_ptr
dispatch_igetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::igetval(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin igetval_0 = 
{
	"igetval",
	"long igetval(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_igetval_0, builtins::check_igetval_0, 0,
	0,
};

static enum c_type::typespec int_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int int_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_int_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::int_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin int_value_0 = 
{
	"int_value",
	"long int_value(eddobject source_var_name);",
	c_type::tint32,
	1, int_value_types_0, int_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_int_value_0, builtins::check_int_value_0, 0,
	0,
};

static enum c_type::typespec is_NaN_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int is_NaN_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_is_NaN_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::is_NaN(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin is_NaN_0 = 
{
	"is_NaN",
	"long is_NaN(double dvalue);",
	c_type::tint32,
	1, is_NaN_types_0, is_NaN_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_is_NaN_0, 0, 0,
	0,
};

static enum c_type::typespec isetval_types_0[1] =
{
	c_type::tint32
};
static unsigned int isetval_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_isetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::isetval(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin isetval_0 = 
{
	"isetval",
	"long isetval(long value);",
	c_type::tint32,
	1, isetval_types_0, isetval_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_isetval_0, builtins::check_isetval_0, 0,
	0,
};

static enum c_type::typespec itoa_types_0[1] =
{
	c_type::tint32
};
static unsigned int itoa_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_itoa_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::itoa(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin itoa_0 = 
{
	"itoa",
	"string itoa(long value);",
	c_type::tstring,
	1, itoa_types_0, itoa_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|0,
	0, /* normal builtin */
	dispatch_itoa_0, 0, 0,
	0,
};

static enum c_type::typespec ivar_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int ivar_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ivar_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::ivar_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ivar_value_0 = 
{
	"ivar_value",
	"long ivar_value(eddobject source_var_name);",
	c_type::tint32,
	1, ivar_value_types_0, ivar_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_ivar_value_0, builtins::check_ivar_value_0, 0,
	0,
};

static enum c_type::typespec lassign_types_0[2] =
{
	c_type::teddobject,
	c_type::tint32
};
static unsigned int lassign_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_lassign_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::lassign(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin lassign_0 = 
{
	"lassign",
	"long lassign(eddobject device_var, long new_value);",
	c_type::tint32,
	2, lassign_types_0, lassign_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_lassign_0, builtins::check_lassign_0, 0,
	0,
};

static operand_ptr
dispatch_lgetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::lgetval(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin lgetval_0 = 
{
	"lgetval",
	"long lgetval(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_lgetval_0, builtins::check_lgetval_0, 0,
	0,
};

static enum c_type::typespec log_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int log_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_log_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::log_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log_1 = 
{
	"log",
	"double log(double x);",
	c_type::tfloat64,
	1, log_types_1, log_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log_1, 0, 0,
	0,
};

static enum c_type::typespec log_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int log_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_log_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::log(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log_0 = 
{
	"log",
	"float log(float x);",
	c_type::tfloat32,
	1, log_types_0, log_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log_0, 0, 0,
	&log_1,
};

static enum c_type::typespec log10_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int log10_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_log10_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::log10_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log10_1 = 
{
	"log10",
	"double log10(double x);",
	c_type::tfloat64,
	1, log10_types_1, log10_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log10_1, 0, 0,
	0,
};

static enum c_type::typespec log10_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int log10_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_log10_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::log10(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log10_0 = 
{
	"log10",
	"float log10(float x);",
	c_type::tfloat32,
	1, log10_types_0, log10_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log10_0, 0, 0,
	&log10_1,
};

static enum c_type::typespec log2_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int log2_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_log2_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::log2_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log2_1 = 
{
	"log2",
	"double log2(double x);",
	c_type::tfloat64,
	1, log2_types_1, log2_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log2_1, 0, 0,
	0,
};

static enum c_type::typespec log2_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int log2_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_log2_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::log2(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin log2_0 = 
{
	"log2",
	"float log2(float x);",
	c_type::tfloat32,
	1, log2_types_0, log2_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_log2_0, 0, 0,
	&log2_1,
};

static enum c_type::typespec long_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int long_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_long_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::long_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin long_value_0 = 
{
	"long_value",
	"long long_value(eddobject source_var_name);",
	c_type::tint32,
	1, long_value_types_0, long_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_long_value_0, builtins::check_long_value_0, 0,
	0,
};

static enum c_type::typespec lsetval_types_0[1] =
{
	c_type::tint32
};
static unsigned int lsetval_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_lsetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::lsetval(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin lsetval_0 = 
{
	"lsetval",
	"long lsetval(long value);",
	c_type::tint32,
	1, lsetval_types_0, lsetval_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_lsetval_0, builtins::check_lsetval_0, 0,
	0,
};

static enum c_type::typespec lvar_value_types_0[1] =
{
	c_type::teddobject
};
static unsigned int lvar_value_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_lvar_value_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::lvar_value(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin lvar_value_0 = 
{
	"lvar_value",
	"long lvar_value(eddobject source_var_name);",
	c_type::tint32,
	1, lvar_value_types_0, lvar_value_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_lvar_value_0, builtins::check_lvar_value_0, 0,
	0,
};

static enum c_type::typespec method_abort_types_0[1] =
{
	c_type::tstring
};
static unsigned int method_abort_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_method_abort_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::method_abort(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin method_abort_0 = 
{
	"method_abort",
	"void method_abort(string prompt);",
	c_type::tvoid,
	1, method_abort_types_0, method_abort_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_method_abort_0, 0, 0,
	0,
};

static operand_ptr
dispatch_method_set_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::method_set(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin method_set_0 = 
{
	"method_set",
	"void method_set(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_method_set_0, 0, 0,
	0,
};

static enum c_type::typespec nan_types_0[1] =
{
	c_type::tstring
};
static unsigned int nan_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_nan_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::nan(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin nan_0 = 
{
	"nan",
	"double nan(string value);",
	c_type::tfloat64,
	1, nan_types_0, nan_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_nan_0, 0, 0,
	0,
};

static enum c_type::typespec nanf_types_0[1] =
{
	c_type::tstring
};
static unsigned int nanf_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_nanf_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::nanf(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin nanf_0 = 
{
	"nanf",
	"float nanf(string value);",
	c_type::tfloat32,
	1, nanf_types_0, nanf_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_nanf_0, 0, 0,
	0,
};

static operand_ptr
dispatch_p_abort_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::p_abort(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin p_abort_0 = 
{
	"p_abort",
	"void p_abort(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_p_abort_0, 0, 0,
	0,
};

static operand_ptr
dispatch_pop_abort_method_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::pop_abort_method(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin pop_abort_method_0 = 
{
	"pop_abort_method",
	"long pop_abort_method(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_pop_abort_method_0, 0, 0,
	0,
};

static enum c_type::typespec pow_types_1[2] =
{
	c_type::tfloat64,
	c_type::tfloat64
};
static unsigned int pow_byref_1[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_pow_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	edd_real64 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::pow_1(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin pow_1 = 
{
	"pow",
	"double pow(double x, double y);",
	c_type::tfloat64,
	2, pow_types_1, pow_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_pow_1, 0, 0,
	0,
};

static enum c_type::typespec pow_types_0[2] =
{
	c_type::tfloat32,
	c_type::tfloat32
};
static unsigned int pow_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_pow_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	edd_real32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::pow(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin pow_0 = 
{
	"pow",
	"float pow(float x, float y);",
	c_type::tfloat32,
	2, pow_types_0, pow_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_pow_0, 0, 0,
	&pow_1,
};

static operand_ptr
dispatch_process_abort_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::process_abort(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin process_abort_0 = 
{
	"process_abort",
	"void process_abort(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_process_abort_0, 0, 0,
	0,
};

static enum c_type::typespec push_abort_method_types_0[1] =
{
	c_type::teddobject
};
static unsigned int push_abort_method_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_push_abort_method_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::push_abort_method(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin push_abort_method_0 = 
{
	"push_abort_method",
	"long push_abort_method(eddobject abort_method_name);",
	c_type::tint32,
	1, push_abort_method_types_0, push_abort_method_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_push_abort_method_0, builtins::check_push_abort_method_0, 0,
	0,
};

static enum c_type::typespec put_date_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_date_byref_0[2] = 
{
	0,
	0
};

static const struct builtin put_date_0 = 
{
	"put_date",
	"long put_date(string data, long size);",
	c_type::tint32,
	2, put_date_types_0, put_date_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_date_value_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_date_value_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin put_date_value_0 = 
{
	"put_date_value",
	"long put_date_value(unsigned long longid, unsigned long member_id, string data, long size);",
	c_type::tint32,
	4, put_date_value_types_0, put_date_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_date_value2_types_0[6] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_date_value2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0
};

static const struct builtin put_date_value2_0 = 
{
	"put_date_value2",
	"long put_date_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, string data, long size);",
	c_type::tint32,
	6, put_date_value2_types_0, put_date_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_double_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int put_double_byref_0[1] = 
{
	0
};

static const struct builtin put_double_0 = 
{
	"put_double",
	"long put_double(double value);",
	c_type::tint32,
	1, put_double_types_0, put_double_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_double_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int put_double_value_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin put_double_value_0 = 
{
	"put_double_value",
	"long put_double_value(unsigned long longid, unsigned long member_id, double value);",
	c_type::tint32,
	3, put_double_value_types_0, put_double_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_double_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int put_double_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin put_double_value2_0 = 
{
	"put_double_value2",
	"long put_double_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, double value);",
	c_type::tint32,
	5, put_double_value2_types_0, put_double_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_float_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int put_float_byref_0[1] = 
{
	0
};

static const struct builtin put_float_0 = 
{
	"put_float",
	"long put_float(double value);",
	c_type::tint32,
	1, put_float_types_0, put_float_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_float_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int put_float_value_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin put_float_value_0 = 
{
	"put_float_value",
	"long put_float_value(unsigned long id, unsigned long member_id, double value);",
	c_type::tint32,
	3, put_float_value_types_0, put_float_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_float_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tfloat64
};
static unsigned int put_float_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin put_float_value2_0 = 
{
	"put_float_value2",
	"long put_float_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, double value);",
	c_type::tint32,
	5, put_float_value2_types_0, put_float_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_message_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_message_byref_0[2] = 
{
	0,
	2
};

static operand_ptr
dispatch_put_message_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::put_message(env, pos, arg0, arg1);
	
	/* return value */
	return new no_value();
}

static const struct builtin put_message_0 = 
{
	"put_message",
	"void put_message(string message, long global_var_ids[]);",
	c_type::tvoid,
	2, put_message_types_0, put_message_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_put_message_0, 0, 0,
	0,
};

static enum c_type::typespec put_signed_types_0[1] =
{
	c_type::tint32
};
static unsigned int put_signed_byref_0[1] = 
{
	0
};

static const struct builtin put_signed_0 = 
{
	"put_signed",
	"long put_signed(long value);",
	c_type::tint32,
	1, put_signed_types_0, put_signed_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_signed_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int put_signed_value_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin put_signed_value_0 = 
{
	"put_signed_value",
	"long put_signed_value(unsigned long id, unsigned long member_id, long value);",
	c_type::tint32,
	3, put_signed_value_types_0, put_signed_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_signed_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32
};
static unsigned int put_signed_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin put_signed_value2_0 = 
{
	"put_signed_value2",
	"long put_signed_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, long value);",
	c_type::tint32,
	5, put_signed_value2_types_0, put_signed_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_string_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_string_byref_0[2] = 
{
	1,
	0
};

static const struct builtin put_string_0 = 
{
	"put_string",
	"long put_string(string &str, long len);",
	c_type::tint32,
	2, put_string_types_0, put_string_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_string_value_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_string_value_byref_0[4] = 
{
	0,
	0,
	1,
	0
};

static const struct builtin put_string_value_0 = 
{
	"put_string_value",
	"long put_string_value(unsigned long id, unsigned long member_id, string &str, long len);",
	c_type::tint32,
	4, put_string_value_types_0, put_string_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_string_value2_types_0[6] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int put_string_value2_byref_0[6] = 
{
	0,
	0,
	0,
	0,
	1,
	0
};

static const struct builtin put_string_value2_0 = 
{
	"put_string_value2",
	"long put_string_value2(unsigned long id, unsigned long member_id, unsigned long id, unsigned long member_id, string &str, long len);",
	c_type::tint32,
	6, put_string_value2_types_0, put_string_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_unsigned_types_0[1] =
{
	c_type::tuint32
};
static unsigned int put_unsigned_byref_0[1] = 
{
	0
};

static const struct builtin put_unsigned_0 = 
{
	"put_unsigned",
	"long put_unsigned(unsigned long value);",
	c_type::tint32,
	1, put_unsigned_types_0, put_unsigned_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_unsigned_value_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int put_unsigned_value_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin put_unsigned_value_0 = 
{
	"put_unsigned_value",
	"long put_unsigned_value(unsigned long id, unsigned long member_id, unsigned long value);",
	c_type::tint32,
	3, put_unsigned_value_types_0, put_unsigned_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec put_unsigned_value2_types_0[5] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int put_unsigned_value2_byref_0[5] = 
{
	0,
	0,
	0,
	0,
	0
};

static const struct builtin put_unsigned_value2_0 = 
{
	"put_unsigned_value2",
	"long put_unsigned_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id, unsigned long value);",
	c_type::tint32,
	5, put_unsigned_value2_types_0, put_unsigned_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec read_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int read_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin read_value_0 = 
{
	"read_value",
	"long read_value(unsigned long id, unsigned long member_id);",
	c_type::tint32,
	2, read_value_types_0, read_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec read_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int read_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin read_value2_0 = 
{
	"read_value2",
	"long read_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id);",
	c_type::tint32,
	4, read_value2_types_0, read_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec refresh_types_0[1] =
{
	c_type::tint32
};
static unsigned int refresh_byref_0[1] = 
{
	2
};

static operand_ptr
dispatch_refresh_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32_array arg0(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::refresh(env, pos, arg0);
	
	/* return value */
	return new no_value();
}

static const struct builtin refresh_0 = 
{
	"refresh",
	"void refresh(long var_ids[]);",
	c_type::tvoid,
	1, refresh_types_0, refresh_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	0, /* normal builtin */
	dispatch_refresh_0, 0, 0,
	0,
};

static enum c_type::typespec remove_abort_method_types_1[1] =
{
	c_type::tuint32
};
static unsigned int remove_abort_method_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_remove_abort_method_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_uint32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_uint32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::remove_abort_method_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin remove_abort_method_1 = 
{
	"remove_abort_method",
	"long remove_abort_method(unsigned long method_id);",
	c_type::tint32,
	1, remove_abort_method_types_1, remove_abort_method_byref_1,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_remove_abort_method_1, 0, 0,
	0,
};

static enum c_type::typespec remove_abort_method_types_0[1] =
{
	c_type::teddobject
};
static unsigned int remove_abort_method_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_remove_abort_method_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::remove_abort_method(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin remove_abort_method_0 = 
{
	"remove_abort_method",
	"long remove_abort_method(eddobject abort_method_name);",
	c_type::tint32,
	1, remove_abort_method_types_0, remove_abort_method_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_remove_abort_method_0, builtins::check_remove_abort_method_0, 0,
	&remove_abort_method_1,
};

static operand_ptr
dispatch_remove_all_abort_methods_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::remove_all_abort_methods(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin remove_all_abort_methods_0 = 
{
	"remove_all_abort_methods",
	"void remove_all_abort_methods(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_remove_all_abort_methods_0, 0, 0,
	0,
};

static enum c_type::typespec resolve_array_ref_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int resolve_array_ref_byref_0[2] = 
{
	0,
	0
};

static const struct builtin resolve_array_ref_0 = 
{
	"resolve_array_ref",
	"unsigned long resolve_array_ref(unsigned long id, unsigned long member_id);",
	c_type::tuint32,
	2, resolve_array_ref_types_0, resolve_array_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_block_ref_types_0[1] =
{
	c_type::tuint32
};
static unsigned int resolve_block_ref_byref_0[1] = 
{
	0
};

static const struct builtin resolve_block_ref_0 = 
{
	"resolve_block_ref",
	"unsigned long resolve_block_ref(unsigned long member_id);",
	c_type::tuint32,
	1, resolve_block_ref_types_0, resolve_block_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_list_ref_types_0[1] =
{
	c_type::tuint32
};
static unsigned int resolve_list_ref_byref_0[1] = 
{
	0
};

static const struct builtin resolve_list_ref_0 = 
{
	"resolve_list_ref",
	"unsigned long resolve_list_ref(unsigned long list_id);",
	c_type::tuint32,
	1, resolve_list_ref_types_0, resolve_list_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_local_ref_types_0[1] =
{
	c_type::tuint32
};
static unsigned int resolve_local_ref_byref_0[1] = 
{
	0
};

static const struct builtin resolve_local_ref_0 = 
{
	"resolve_local_ref",
	"unsigned long resolve_local_ref(unsigned long member_id);",
	c_type::tuint32,
	1, resolve_local_ref_types_0, resolve_local_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_local_ref2_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int resolve_local_ref2_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin resolve_local_ref2_0 = 
{
	"resolve_local_ref2",
	"unsigned long resolve_local_ref2(unsigned long block_id, unsigned long block_instance, unsigned long member_id);",
	c_type::tuint32,
	3, resolve_local_ref2_types_0, resolve_local_ref2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_param_list_ref_types_0[1] =
{
	c_type::tuint32
};
static unsigned int resolve_param_list_ref_byref_0[1] = 
{
	0
};

static const struct builtin resolve_param_list_ref_0 = 
{
	"resolve_param_list_ref",
	"unsigned long resolve_param_list_ref(unsigned long member_id);",
	c_type::tuint32,
	1, resolve_param_list_ref_types_0, resolve_param_list_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_param_ref_types_0[1] =
{
	c_type::tuint32
};
static unsigned int resolve_param_ref_byref_0[1] = 
{
	0
};

static const struct builtin resolve_param_ref_0 = 
{
	"resolve_param_ref",
	"unsigned long resolve_param_ref(unsigned long member_id);",
	c_type::tuint32,
	1, resolve_param_ref_types_0, resolve_param_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_param_ref2_types_0[3] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int resolve_param_ref2_byref_0[3] = 
{
	0,
	0,
	0
};

static const struct builtin resolve_param_ref2_0 = 
{
	"resolve_param_ref2",
	"unsigned long resolve_param_ref2(unsigned long block_id, unsigned long block_instance, unsigned long member_id);",
	c_type::tuint32,
	3, resolve_param_ref2_types_0, resolve_param_ref2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_record_ref_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int resolve_record_ref_byref_0[2] = 
{
	0,
	0
};

static const struct builtin resolve_record_ref_0 = 
{
	"resolve_record_ref",
	"unsigned long resolve_record_ref(unsigned long id, unsigned long member_id);",
	c_type::tuint32,
	2, resolve_record_ref_types_0, resolve_record_ref_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec resolve_record_ref2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int resolve_record_ref2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin resolve_record_ref2_0 = 
{
	"resolve_record_ref2",
	"unsigned long resolve_record_ref2(unsigned long block_id, unsigned long block_instance, unsigned long id, unsigned long member_id);",
	c_type::tuint32,
	4, resolve_record_ref2_types_0, resolve_record_ref2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_double_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_double_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin ret_double_value_0 = 
{
	"ret_double_value",
	"double ret_double_value(unsigned long item_id, unsigned long member_id);",
	c_type::tfloat64,
	2, ret_double_value_types_0, ret_double_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_double_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_double_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin ret_double_value2_0 = 
{
	"ret_double_value2",
	"double ret_double_value2(unsigned long block_id, unsigned long block_instance, unsigned long item_id, unsigned long member_id);",
	c_type::tfloat64,
	4, ret_double_value2_types_0, ret_double_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_float_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_float_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin ret_float_value_0 = 
{
	"ret_float_value",
	"float ret_float_value(unsigned long item_id, unsigned long member_id);",
	c_type::tfloat32,
	2, ret_float_value_types_0, ret_float_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_float_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_float_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin ret_float_value2_0 = 
{
	"ret_float_value2",
	"float ret_float_value2(unsigned long block_id, unsigned long block_instance, unsigned long item_id, unsigned long member_id);",
	c_type::tfloat32,
	4, ret_float_value2_types_0, ret_float_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_signed_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_signed_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin ret_signed_value_0 = 
{
	"ret_signed_value",
	"long ret_signed_value(unsigned long item_id, unsigned long member_id);",
	c_type::tint32,
	2, ret_signed_value_types_0, ret_signed_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_signed_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_signed_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin ret_signed_value2_0 = 
{
	"ret_signed_value2",
	"long ret_signed_value2(unsigned long block_id, unsigned long block_instance, unsigned long item_id, unsigned long member_id);",
	c_type::tint32,
	4, ret_signed_value2_types_0, ret_signed_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_unsigned_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_unsigned_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin ret_unsigned_value_0 = 
{
	"ret_unsigned_value",
	"unsigned long ret_unsigned_value(unsigned long item_id, unsigned long member_id);",
	c_type::tuint32,
	2, ret_unsigned_value_types_0, ret_unsigned_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec ret_unsigned_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int ret_unsigned_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin ret_unsigned_value2_0 = 
{
	"ret_unsigned_value2",
	"unsigned long ret_unsigned_value2(unsigned long block_id, unsigned long block_instance, unsigned long item_id, unsigned long member_id);",
	c_type::tuint32,
	4, ret_unsigned_value2_types_0, ret_unsigned_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin retry_on_all_comm_errors_0 = 
{
	"retry_on_all_comm_errors",
	"void retry_on_all_comm_errors(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static const struct builtin retry_on_all_response_codes_0 = 
{
	"retry_on_all_response_codes",
	"void retry_on_all_response_codes(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec retry_on_comm_error_types_0[1] =
{
	c_type::tuint32
};
static unsigned int retry_on_comm_error_byref_0[1] = 
{
	0
};

static const struct builtin retry_on_comm_error_0 = 
{
	"retry_on_comm_error",
	"long retry_on_comm_error(unsigned long error);",
	c_type::tint32,
	1, retry_on_comm_error_types_0, retry_on_comm_error_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec retry_on_response_code_types_0[1] =
{
	c_type::tuint32
};
static unsigned int retry_on_response_code_byref_0[1] = 
{
	0
};

static const struct builtin retry_on_response_code_0 = 
{
	"retry_on_response_code",
	"long retry_on_response_code(unsigned long code);",
	c_type::tint32,
	1, retry_on_response_code_types_0, retry_on_response_code_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec round_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int round_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_round_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::round_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin round_1 = 
{
	"round",
	"double round(double x);",
	c_type::tfloat64,
	1, round_types_1, round_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_round_1, 0, 0,
	0,
};

static enum c_type::typespec round_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int round_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_round_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::round(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin round_0 = 
{
	"round",
	"float round(float x);",
	c_type::tfloat32,
	1, round_types_0, round_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_round_0, 0, 0,
	&round_1,
};

static operand_ptr
dispatch_save_on_exit_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::save_on_exit(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin save_on_exit_0 = 
{
	"save_on_exit",
	"void save_on_exit(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_save_on_exit_0, 0, 0,
	0,
};

static operand_ptr
dispatch_save_values_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::save_values(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin save_values_0 = 
{
	"save_values",
	"long save_values(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_save_values_0, 0, 0,
	0,
};

static enum c_type::typespec seconds_to_TIME_VALUE_types_0[1] =
{
	c_type::tfloat64
};
static unsigned int seconds_to_TIME_VALUE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_seconds_to_TIME_VALUE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_uint32 ret = builtins::seconds_to_TIME_VALUE(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new uint32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin seconds_to_TIME_VALUE_0 = 
{
	"seconds_to_TIME_VALUE",
	"unsigned long seconds_to_TIME_VALUE(double seconds);",
	c_type::tuint32,
	1, seconds_to_TIME_VALUE_types_0, seconds_to_TIME_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_seconds_to_TIME_VALUE_0, 0, 0,
	0,
};

static enum c_type::typespec select_from_list_types_0[3] =
{
	c_type::tstring,
	c_type::tint32,
	c_type::tstring
};
static unsigned int select_from_list_byref_0[3] = 
{
	0,
	2,
	0
};

static operand_ptr
dispatch_select_from_list_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32_array arg1(args);
	args = args.next();
	
	edd_string arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::select_from_list(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin select_from_list_0 = 
{
	"select_from_list",
	"long select_from_list(string prompt, long global_var_ids[], string option_list);",
	c_type::tint32,
	3, select_from_list_types_0, select_from_list_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_select_from_list_0, 0, 0,
	0,
};

static enum c_type::typespec select_from_menu_types_0[6] =
{
	c_type::tint8,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int select_from_menu_byref_0[6] = 
{
	0,
	2,
	2,
	0,
	0,
	1
};

static const struct builtin select_from_menu_0 = 
{
	"select_from_menu",
	"long select_from_menu(char prompt, unsigned long ids[], unsigned long indices[], long id_count, string options, long &selection);",
	c_type::tint32,
	6, select_from_menu_types_0, select_from_menu_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec select_from_menu2_types_0[8] =
{
	c_type::tint8,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tint32,
	c_type::tstring,
	c_type::tint32
};
static unsigned int select_from_menu2_byref_0[8] = 
{
	0,
	2,
	2,
	2,
	2,
	0,
	0,
	1
};

static const struct builtin select_from_menu2_0 = 
{
	"select_from_menu2",
	"long select_from_menu2(char prompt, unsigned long blk_ids[], unsigned long blk_nums[], unsigned long ids[], unsigned long indices[], long id_count, string options, long &selection);",
	c_type::tint32,
	8, select_from_menu2_types_0, select_from_menu2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec send_types_0[2] =
{
	c_type::tint32,
	c_type::tstring
};
static unsigned int send_byref_0[2] = 
{
	0,
	1
};

static operand_ptr
dispatch_send_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg1(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::send(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin send_0 = 
{
	"send",
	"long send(long cmd_number, string &cmd_status);",
	c_type::tint32,
	2, send_types_0, send_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_send_0, builtins::check_send_0, 0,
	0,
};

static operand_ptr
dispatch_send_all_values_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::send_all_values(env, pos);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin send_all_values_0 = 
{
	"send_all_values",
	"long send_all_values(void);",
	c_type::tint32,
	0, 0, 0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_send_all_values_0, 0, 0,
	0,
};

static enum c_type::typespec send_command_types_0[1] =
{
	c_type::tint32
};
static unsigned int send_command_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_send_command_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::send_command(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin send_command_0 = 
{
	"send_command",
	"long send_command(long cmd_number);",
	c_type::tint32,
	1, send_command_types_0, send_command_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_send_command_0, builtins::check_send_command_0, 0,
	0,
};

static enum c_type::typespec send_command_trans_types_0[2] =
{
	c_type::tint32,
	c_type::tint32
};
static unsigned int send_command_trans_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_send_command_trans_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::send_command_trans(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin send_command_trans_0 = 
{
	"send_command_trans",
	"long send_command_trans(long cmd_number, long transaction);",
	c_type::tint32,
	2, send_command_trans_types_0, send_command_trans_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_send_command_trans_0, builtins::check_send_command_trans_0, 0,
	0,
};

static operand_ptr
dispatch_send_on_exit_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::send_on_exit(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin send_on_exit_0 = 
{
	"send_on_exit",
	"void send_on_exit(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_send_on_exit_0, 0, 0,
	0,
};

static enum c_type::typespec send_trans_types_0[3] =
{
	c_type::tint32,
	c_type::tint32,
	c_type::tstring
};
static unsigned int send_trans_byref_0[3] = 
{
	0,
	0,
	1
};

static operand_ptr
dispatch_send_trans_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_string_opmap arg2(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::send_trans(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin send_trans_0 = 
{
	"send_trans",
	"long send_trans(long cmd_number, long transaction, string &cmd_status);",
	c_type::tint32,
	3, send_trans_types_0, send_trans_byref_0,
	EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_send_trans_0, builtins::check_send_trans_0, 0,
	0,
};

static enum c_type::typespec send_value_types_0[2] =
{
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int send_value_byref_0[2] = 
{
	0,
	0
};

static const struct builtin send_value_0 = 
{
	"send_value",
	"long send_value(unsigned long id, unsigned long member_id);",
	c_type::tint32,
	2, send_value_types_0, send_value_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec send_value2_types_0[4] =
{
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32,
	c_type::tuint32
};
static unsigned int send_value2_byref_0[4] = 
{
	0,
	0,
	0,
	0
};

static const struct builtin send_value2_0 = 
{
	"send_value2",
	"long send_value2(unsigned long blk_id, unsigned long blk_num, unsigned long id, unsigned long member_id);",
	c_type::tint32,
	4, send_value2_types_0, send_value2_byref_0,
	EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	0, 0, 0,
	0,
};

static enum c_type::typespec sgetval_types_0[1] =
{
	c_type::tstring
};
static unsigned int sgetval_byref_0[1] = 
{
	1
};

static operand_ptr
dispatch_sgetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string_opmap arg0(args);
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::sgetval(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sgetval_0 = 
{
	"sgetval",
	"long sgetval(string &str);",
	c_type::tint32,
	1, sgetval_types_0, sgetval_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_sgetval_0, builtins::check_sgetval_0, 0,
	0,
};

static operand_ptr
dispatch_sie_set_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::sie_set(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin sie_set_0 = 
{
	"sie_set",
	"void sie_set(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_sie_set_0, 0, 0,
	0,
};

static enum c_type::typespec sin_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int sin_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_sin_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::sin_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sin_1 = 
{
	"sin",
	"double sin(double x);",
	c_type::tfloat64,
	1, sin_types_1, sin_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sin_1, 0, 0,
	0,
};

static enum c_type::typespec sin_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int sin_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_sin_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::sin(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sin_0 = 
{
	"sin",
	"float sin(float x);",
	c_type::tfloat32,
	1, sin_types_0, sin_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sin_0, 0, 0,
	&sin_1,
};

static enum c_type::typespec sinh_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int sinh_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_sinh_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::sinh_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sinh_1 = 
{
	"sinh",
	"double sinh(double x);",
	c_type::tfloat64,
	1, sinh_types_1, sinh_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sinh_1, 0, 0,
	0,
};

static enum c_type::typespec sinh_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int sinh_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_sinh_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::sinh(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sinh_0 = 
{
	"sinh",
	"float sinh(float x);",
	c_type::tfloat32,
	1, sinh_types_0, sinh_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sinh_0, 0, 0,
	&sinh_1,
};

static enum c_type::typespec sqrt_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int sqrt_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_sqrt_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::sqrt_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sqrt_1 = 
{
	"sqrt",
	"double sqrt(double x);",
	c_type::tfloat64,
	1, sqrt_types_1, sqrt_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sqrt_1, 0, 0,
	0,
};

static enum c_type::typespec sqrt_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int sqrt_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_sqrt_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::sqrt(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin sqrt_0 = 
{
	"sqrt",
	"float sqrt(float x);",
	c_type::tfloat32,
	1, sqrt_types_0, sqrt_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_sqrt_0, 0, 0,
	&sqrt_1,
};

static enum c_type::typespec ssetval_types_0[1] =
{
	c_type::tstring
};
static unsigned int ssetval_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_ssetval_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::ssetval(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin ssetval_0 = 
{
	"ssetval",
	"string ssetval(string value);",
	c_type::tstring,
	1, ssetval_types_0, ssetval_byref_0,
	EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|0,
	0, /* normal builtin */
	dispatch_ssetval_0, builtins::check_ssetval_0, 0,
	0,
};

static operand_ptr
dispatch_std_Funktionsleiste_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_Funktionsleiste(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_Funktionsleiste_0 = 
{
	"std_Funktionsleiste",
	"void std_Funktionsleiste(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_Funktionsleiste_0, 0, 0,
	0,
};

static operand_ptr
dispatch_std_LoadinDevice_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_LoadinDevice(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_LoadinDevice_0 = 
{
	"std_LoadinDevice",
	"void std_LoadinDevice(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_LoadinDevice_0, 0, 0,
	0,
};

static operand_ptr
dispatch_std_LoadinPC_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_LoadinPC(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_LoadinPC_0 = 
{
	"std_LoadinPC",
	"void std_LoadinPC(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_LoadinPC_0, 0, 0,
	0,
};

static operand_ptr
dispatch_std_NodeAddress_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_NodeAddress(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_NodeAddress_0 = 
{
	"std_NodeAddress",
	"void std_NodeAddress(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_NodeAddress_0, 0, 0,
	0,
};

static operand_ptr
dispatch_std_Statuszeile_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_Statuszeile(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_Statuszeile_0 = 
{
	"std_Statuszeile",
	"void std_Statuszeile(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_Statuszeile_0, 0, 0,
	0,
};

static operand_ptr
dispatch_std_Update_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	builtins::std_Update(env, pos);
	
	/* return value */
	return new no_value();
}

static const struct builtin std_Update_0 = 
{
	"std_Update",
	"void std_Update(void);",
	c_type::tvoid,
	0, 0, 0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_PDM|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_PDM|0,
	1, /* special builtin */
	dispatch_std_Update_0, 0, 0,
	0,
};

static enum c_type::typespec strcmp_types_0[2] =
{
	c_type::tstring,
	c_type::tstring
};
static unsigned int strcmp_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_strcmp_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::strcmp(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strcmp_0 = 
{
	"strcmp",
	"long strcmp(string s1, string s2);",
	c_type::tint32,
	2, strcmp_types_0, strcmp_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strcmp_0, 0, 0,
	0,
};

static enum c_type::typespec strleft_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int strleft_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_strleft_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strleft(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strleft_0 = 
{
	"strleft",
	"string strleft(string str, long length);",
	c_type::tstring,
	2, strleft_types_0, strleft_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strleft_0, 0, 0,
	0,
};

static enum c_type::typespec strlen_types_0[1] =
{
	c_type::tstring
};
static unsigned int strlen_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_strlen_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::strlen(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strlen_0 = 
{
	"strlen",
	"long strlen(string str);",
	c_type::tint32,
	1, strlen_types_0, strlen_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strlen_0, 0, 0,
	0,
};

static enum c_type::typespec strlwr_types_0[1] =
{
	c_type::tstring
};
static unsigned int strlwr_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_strlwr_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strlwr(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strlwr_0 = 
{
	"strlwr",
	"string strlwr(string str);",
	c_type::tstring,
	1, strlwr_types_0, strlwr_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strlwr_0, 0, 0,
	0,
};

static enum c_type::typespec strmid_types_0[3] =
{
	c_type::tstring,
	c_type::tint32,
	c_type::tint32
};
static unsigned int strmid_byref_0[3] = 
{
	0,
	0,
	0
};

static operand_ptr
dispatch_strmid_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	edd_int32 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strmid(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strmid_0 = 
{
	"strmid",
	"string strmid(string str, long pos, long len);",
	c_type::tstring,
	3, strmid_types_0, strmid_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strmid_0, 0, 0,
	0,
};

static enum c_type::typespec strright_types_0[2] =
{
	c_type::tstring,
	c_type::tint32
};
static unsigned int strright_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_strright_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32 arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strright(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strright_0 = 
{
	"strright",
	"string strright(string str, long length);",
	c_type::tstring,
	2, strright_types_0, strright_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strright_0, 0, 0,
	0,
};

static enum c_type::typespec strstr_types_0[2] =
{
	c_type::tstring,
	c_type::tstring
};
static unsigned int strstr_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_strstr_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strstr(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strstr_0 = 
{
	"strstr",
	"string strstr(string str, string substr);",
	c_type::tstring,
	2, strstr_types_0, strstr_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strstr_0, 0, 0,
	0,
};

static enum c_type::typespec strtrim_types_0[1] =
{
	c_type::tstring
};
static unsigned int strtrim_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_strtrim_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strtrim(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strtrim_0 = 
{
	"strtrim",
	"string strtrim(string str);",
	c_type::tstring,
	1, strtrim_types_0, strtrim_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strtrim_0, 0, 0,
	0,
};

static enum c_type::typespec strupr_types_0[1] =
{
	c_type::tstring
};
static unsigned int strupr_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_strupr_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_string)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_string ret = builtins::strupr(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new string_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin strupr_0 = 
{
	"strupr",
	"string strupr(string str);",
	c_type::tstring,
	1, strupr_types_0, strupr_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_strupr_0, 0, 0,
	0,
};

static enum c_type::typespec tan_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int tan_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_tan_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::tan_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin tan_1 = 
{
	"tan",
	"double tan(double x);",
	c_type::tfloat64,
	1, tan_types_1, tan_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_tan_1, 0, 0,
	0,
};

static enum c_type::typespec tan_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int tan_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_tan_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::tan(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin tan_0 = 
{
	"tan",
	"float tan(float x);",
	c_type::tfloat32,
	1, tan_types_0, tan_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_tan_0, 0, 0,
	&tan_1,
};

static enum c_type::typespec tanh_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int tanh_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_tanh_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::tanh_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin tanh_1 = 
{
	"tanh",
	"double tanh(double x);",
	c_type::tfloat64,
	1, tanh_types_1, tanh_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_tanh_1, 0, 0,
	0,
};

static enum c_type::typespec tanh_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int tanh_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_tanh_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::tanh(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin tanh_0 = 
{
	"tanh",
	"float tanh(float x);",
	c_type::tfloat32,
	1, tanh_types_0, tanh_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_tanh_0, 0, 0,
	&tanh_1,
};

static enum c_type::typespec timet_to_TIME_VALUE_types_0[1] =
{
	c_type::tint32
};
static unsigned int timet_to_TIME_VALUE_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_timet_to_TIME_VALUE_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_int32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_uint32 ret = builtins::timet_to_TIME_VALUE(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new uint32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin timet_to_TIME_VALUE_0 = 
{
	"timet_to_TIME_VALUE",
	"unsigned long timet_to_TIME_VALUE(long timet);",
	c_type::tuint32,
	1, timet_to_TIME_VALUE_types_0, timet_to_TIME_VALUE_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_timet_to_TIME_VALUE_0, 0, 0,
	0,
};

static enum c_type::typespec timet_to_string_types_0[3] =
{
	c_type::tstring,
	c_type::tstring,
	c_type::tint32
};
static unsigned int timet_to_string_byref_0[3] = 
{
	1,
	0,
	0
};

static operand_ptr
dispatch_timet_to_string_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_string_opmap arg0(args);
	args = args.next();
	
	edd_string arg1;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg1 = (edd_string)(*data);
	}
	args = args.next();
	
	edd_int32 arg2;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg2 = (edd_int32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::timet_to_string(env, pos, arg0, arg1, arg2);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin timet_to_string_0 = 
{
	"timet_to_string",
	"long timet_to_string(string &str, string format, long timet);",
	c_type::tint32,
	3, timet_to_string_types_0, timet_to_string_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_timet_to_string_0, 0, 0,
	0,
};

static enum c_type::typespec trunc_types_1[1] =
{
	c_type::tfloat64
};
static unsigned int trunc_byref_1[1] = 
{
	0
};

static operand_ptr
dispatch_trunc_1(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real64 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real64)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real64 ret = builtins::trunc_1(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float64_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin trunc_1 = 
{
	"trunc",
	"double trunc(double x);",
	c_type::tfloat64,
	1, trunc_types_1, trunc_byref_1,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_trunc_1, 0, 0,
	0,
};

static enum c_type::typespec trunc_types_0[1] =
{
	c_type::tfloat32
};
static unsigned int trunc_byref_0[1] = 
{
	0
};

static operand_ptr
dispatch_trunc_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	edd_real32 arg0;
	{
		std::auto_ptr<stackmachine::Type> data(*args);
		arg0 = (edd_real32)(*data);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_real32 ret = builtins::trunc(env, pos, arg0);
	
	/* return value */
	operand_ptr op(new float32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin trunc_0 = 
{
	"trunc",
	"float trunc(float x);",
	c_type::tfloat32,
	1, trunc_types_0, trunc_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFIBUS_OPTION_EDDx1|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|EDD_PROFILE_HART_OPTION_EDDx1|EDD_PROFILE_FIELDBUS|0,
	0, /* normal builtin */
	dispatch_trunc_0, 0, 0,
	&trunc_1,
};

static enum c_type::typespec vassign_types_0[2] =
{
	c_type::teddobject,
	c_type::teddobject
};
static unsigned int vassign_byref_0[2] = 
{
	0,
	0
};

static operand_ptr
dispatch_vassign_0(class method_env *env, operand_ptr args, const lib::pos &pos)
{
	class tree * arg0;
	{
		/* it's a real operand here */
		arg0 = (class tree *)(*args);
	}
	args = args.next();
	
	class tree * arg1;
	{
		/* it's a real operand here */
		arg1 = (class tree *)(*args);
	}
	args = args.next();
	
	/* verify argument list is empty */
	assert(!args);
	
	/* callout the builtin implementation */
	edd_int32 ret = builtins::vassign(env, pos, arg0, arg1);
	
	/* return value */
	operand_ptr op(new int32_value(ret));
	assert(op);
	
	return op;
}

static const struct builtin vassign_0 = 
{
	"vassign",
	"long vassign(eddobject device_var, eddobject source_var);",
	c_type::tint32,
	2, vassign_types_0, vassign_byref_0,
	EDD_PROFILE_PROFIBUS|EDD_PROFILE_PROFINET|EDD_PROFILE_PROFINET|EDD_PROFILE_HART|0,
	0, /* normal builtin */
	dispatch_vassign_0, builtins::check_vassign_0, 0,
	0,
};


static const struct builtin *builtins[345] =
{
	&ABORT_ON_ALL_COMM_STATUS_0,
	&ABORT_ON_ALL_DEVICE_STATUS_0,
	&ABORT_ON_ALL_RESPONSE_CODES_0,
	&ABORT_ON_COMM_ERROR_0,
	&ABORT_ON_COMM_STATUS_0,
	&ABORT_ON_DEVICE_STATUS_0,
	&ABORT_ON_NO_DEVICE_0,
	&ABORT_ON_RESPONSE_CODE_0,
	&ACKNOWLEDGE_0,
	&AddTime_0,
	&BUILD_MESSAGE_0,
	&ByteToDouble_0,
	&ByteToFloat_0,
	&ByteToLong_0,
	&ByteToShort_0,
	&DATE_AND_TIME_VALUE_to_string_0,
	&DATE_to_days_0,
	&DATE_to_string_0,
	&DELAY_0,
	&DELAY_TIME_0,
	&DICT_ID_0,
	&DISPLAY_0,
	&Date_To_Time_0,
	&Date_to_DayOfMonth_0,
	&Date_to_Month_0,
	&Date_to_Year_0,
	&DiffTime_0,
	&DoubleToByte_0,
	&FloatToByte_0,
	&From_DATE_AND_TIME_VALUE_0,
	&From_TIME_VALUE_0,
	&GET_DD_REVISION_0,
	&GET_DEVICE_REVISION_0,
	&GET_DEVICE_TYPE_0,
	&GET_DEV_VAR_VALUE_0,
	&GET_LOCAL_VAR_VALUE_0,
	&GET_MANUFACTURER_0,
	&GET_TICK_COUNT_0,
	&GetCurrentDate_0,
	&GetCurrentDateAndTime_0,
	&GetCurrentTime_0,
	&IGNORE_ALL_COMM_STATUS_0,
	&IGNORE_ALL_DEVICE_STATUS_0,
	&IGNORE_ALL_RESPONSE_CODES_0,
	&IGNORE_COMM_ERROR_0,
	&IGNORE_COMM_STATUS_0,
	&IGNORE_DEVICE_STATUS_0,
	&IGNORE_NO_DEVICE_0,
	&IGNORE_RESPONSE_CODE_0,
	&ITEM_ID_0,
	&LOG_MESSAGE_0,
	&ListDeleteElementAt_0,
	&ListInsert_0,
	&LongToByte_0,
	&MEMBER_ID_0,
	&METHODID_0,
	&Make_Time_0,
	&MenuDisplay_0,
	&NaN_value_0,
	&ObjectReference_0,
	&PUT_MESSAGE_0,
	&READ_COMMAND_0,
	&RETRY_ON_ALL_COMM_STATUS_0,
	&RETRY_ON_ALL_DEVICE_STATUS_0,
	&RETRY_ON_ALL_RESPONSE_CODES_0,
	&RETRY_ON_COMM_ERROR_0,
	&RETRY_ON_COMM_STATUS_0,
	&RETRY_ON_DEVICE_STATUS_0,
	&RETRY_ON_NO_DEVICE_0,
	&RETRY_ON_RESPONSE_CODE_0,
	&ReadCommand_0,
	&SELECT_FROM_LIST_0,
	&SET_NUMBER_OF_RETRIES_0,
	&ShellExecute_0,
	&ShortToByte_0,
	&TIME_VALUE_to_Hour_0,
	&TIME_VALUE_to_Minute_0,
	&TIME_VALUE_to_Second_0,
	&TIME_VALUE_to_seconds_0,
	&TIME_VALUE_to_string_0,
	&Time_To_Date_0,
	&To_Date_0,
	&To_Date_and_Time_0,
	&To_TIME_VALUE_0,
	&To_Time_0,
	&VARID_0,
	&WRITE_COMMAND_0,
	&WriteCommand_0,
	&XMTR_ABORT_ON_ALL_COMM_STATUS_0,
	&XMTR_ABORT_ON_ALL_DATA_0,
	&XMTR_ABORT_ON_ALL_DEVICE_STATUS_0,
	&XMTR_ABORT_ON_ALL_RESPONSE_CODES_0,
	&XMTR_ABORT_ON_COMM_ERROR_0,
	&XMTR_ABORT_ON_COMM_STATUS_0,
	&XMTR_ABORT_ON_DATA_0,
	&XMTR_ABORT_ON_DEVICE_STATUS_0,
	&XMTR_ABORT_ON_NO_DEVICE_0,
	&XMTR_ABORT_ON_RESPONSE_CODE_0,
	&XMTR_IGNORE_ALL_COMM_STATUS_0,
	&XMTR_IGNORE_ALL_DATA_0,
	&XMTR_IGNORE_ALL_DEVICE_STATUS_0,
	&XMTR_IGNORE_ALL_RESPONSE_CODES_0,
	&XMTR_IGNORE_COMM_ERROR_0,
	&XMTR_IGNORE_COMM_STATUS_0,
	&XMTR_IGNORE_DEVICE_STATUS_0,
	&XMTR_IGNORE_NO_DEVICE_0,
	&XMTR_IGNORE_RESPONSE_CODE_0,
	&XMTR_RETRY_ON_ALL_COMM_STATUS_0,
	&XMTR_RETRY_ON_ALL_DATA_0,
	&XMTR_RETRY_ON_ALL_DEVICE_STATUS_0,
	&XMTR_RETRY_ON_ALL_RESPONSE_CODE_0,
	&XMTR_RETRY_ON_ALL_RESPONSE_CODES_0,
	&XMTR_RETRY_ON_COMM_ERROR_0,
	&XMTR_RETRY_ON_COMM_STATUS_0,
	&XMTR_RETRY_ON_DATA_0,
	&XMTR_RETRY_ON_DEVICE_STATUS_0,
	&XMTR_RETRY_ON_NO_DEVICE_0,
	&XMTR_RETRY_ON_RESPONSE_CODE_0,
	&YearMonthDay_to_Date_0,
	&_ERROR_0,
	&_TRACE_0,
	&_WARNING_0,
	&abort_0,
	&abort_on_all_comm_errors_0,
	&abort_on_all_response_codes_0,
	&abort_on_comm_error_0,
	&abort_on_response_code_0,
	&abs_0,
	&acknowledge_0,
	&acos_0,
	&add_abort_method_0,
	&asin_0,
	&assign_0,
	&assign_double_0,
	&assign_float_0,
	&assign_int_0,
	&assign_long_0,
	&assign_str_0,
	&assign_var_0,
	&atan_0,
	&atof_0,
	&atoi_0,
	&atol_0,
	&cbrt_0,
	&ceil_0,
	&cos_0,
	&cosh_0,
	&dassign_0,
	&delay_0,
	&delayfor_0,
	&dictionary_string_0,
	&discard_on_exit_0,
	&display_0,
	&display_bitenum_0,
	&display_builtin_error_0,
	&display_comm_error_0,
	&display_comm_status_0,
	&display_device_status_0,
	&display_dynamics_0,
	&display_message_0,
	&display_response_code_0,
	&display_response_status_0,
	&display_xmtr_status_0,
	&edit_device_value_0,
	&edit_local_value_0,
	&exp_0,
	&ext_send_command_0,
	&ext_send_command_trans_0,
	&fail_on_all_comm_errors_0,
	&fail_on_all_response_codes_0,
	&fail_on_comm_error_0,
	&fail_on_response_code_0,
	&fassign_0,
	&fgetval_0,
	&float_value_0,
	&floor_0,
	&fmod_0,
	&fpclassify_0,
	&fsetval_0,
	&ftoa_0,
	&fvar_value_0,
	&get_acknowledgement_0,
	&get_acknowlegement_0,
	&get_block_instance_by_object_index_0,
	&get_block_instance_by_tag_0,
	&get_block_instance_count_0,
	&get_comm_error_0,
	&get_comm_error_string_0,
	&get_date_0,
	&get_date_lelem_0,
	&get_date_lelem2_0,
	&get_date_value_0,
	&get_date_value2_0,
	&get_dds_error_0,
	&get_dev_var_value_0,
	&get_dictionary_string_0,
	&get_double_0,
	&get_double_lelem_0,
	&get_double_lelem2_0,
	&get_double_value_0,
	&get_double_value2_0,
	&get_enum_string_0,
	&get_float_0,
	&get_float_lelem_0,
	&get_float_lelem2_0,
	&get_float_value_0,
	&get_float_value2_0,
	&get_local_var_value_0,
	&get_more_status_0,
	&get_resolve_status_0,
	&get_response_code_0,
	&get_response_code_string_0,
	&get_rspcode_string_0,
	&get_rspcode_string_by_id_0,
	&get_signed_0,
	&get_signed_lelem_0,
	&get_signed_lelem2_0,
	&get_signed_value_0,
	&get_signed_value2_0,
	&get_status_code_string_0,
	&get_status_string_0,
	&get_stddict_string_0,
	&get_string_0,
	&get_string_lelem_0,
	&get_string_lelem2_0,
	&get_string_value_0,
	&get_string_value2_0,
	&get_unsigned_0,
	&get_unsigned_lelem_0,
	&get_unsigned_lelem2_0,
	&get_unsigned_value_0,
	&get_unsigned_value2_0,
	&get_variable_string_0,
	&iassign_0,
	&igetval_0,
	&int_value_0,
	&is_NaN_0,
	&isetval_0,
	&itoa_0,
	&ivar_value_0,
	&lassign_0,
	&lgetval_0,
	&log_0,
	&log10_0,
	&log2_0,
	&long_value_0,
	&lsetval_0,
	&lvar_value_0,
	&method_abort_0,
	&method_set_0,
	&nan_0,
	&nanf_0,
	&p_abort_0,
	&pop_abort_method_0,
	&pow_0,
	&process_abort_0,
	&push_abort_method_0,
	&put_date_0,
	&put_date_value_0,
	&put_date_value2_0,
	&put_double_0,
	&put_double_value_0,
	&put_double_value2_0,
	&put_float_0,
	&put_float_value_0,
	&put_float_value2_0,
	&put_message_0,
	&put_signed_0,
	&put_signed_value_0,
	&put_signed_value2_0,
	&put_string_0,
	&put_string_value_0,
	&put_string_value2_0,
	&put_unsigned_0,
	&put_unsigned_value_0,
	&put_unsigned_value2_0,
	&read_value_0,
	&read_value2_0,
	&refresh_0,
	&remove_abort_method_0,
	&remove_all_abort_methods_0,
	&resolve_array_ref_0,
	&resolve_block_ref_0,
	&resolve_list_ref_0,
	&resolve_local_ref_0,
	&resolve_local_ref2_0,
	&resolve_param_list_ref_0,
	&resolve_param_ref_0,
	&resolve_param_ref2_0,
	&resolve_record_ref_0,
	&resolve_record_ref2_0,
	&ret_double_value_0,
	&ret_double_value2_0,
	&ret_float_value_0,
	&ret_float_value2_0,
	&ret_signed_value_0,
	&ret_signed_value2_0,
	&ret_unsigned_value_0,
	&ret_unsigned_value2_0,
	&retry_on_all_comm_errors_0,
	&retry_on_all_response_codes_0,
	&retry_on_comm_error_0,
	&retry_on_response_code_0,
	&round_0,
	&save_on_exit_0,
	&save_values_0,
	&seconds_to_TIME_VALUE_0,
	&select_from_list_0,
	&select_from_menu_0,
	&select_from_menu2_0,
	&send_0,
	&send_all_values_0,
	&send_command_0,
	&send_command_trans_0,
	&send_on_exit_0,
	&send_trans_0,
	&send_value_0,
	&send_value2_0,
	&sgetval_0,
	&sie_set_0,
	&sin_0,
	&sinh_0,
	&sqrt_0,
	&ssetval_0,
	&std_Funktionsleiste_0,
	&std_LoadinDevice_0,
	&std_LoadinPC_0,
	&std_NodeAddress_0,
	&std_Statuszeile_0,
	&std_Update_0,
	&strcmp_0,
	&strleft_0,
	&strlen_0,
	&strlwr_0,
	&strmid_0,
	&strright_0,
	&strstr_0,
	&strtrim_0,
	&strupr_0,
	&tan_0,
	&tanh_0,
	&timet_to_TIME_VALUE_0,
	&timet_to_string_0,
	&trunc_0,
	&vassign_0,
};



#endif /* DOXYGEN_IGNORE */

/**
 * \internal
 * builtin_method.
 */
class builtin_method : public c_method
{
public:
	builtin_method(const struct builtin *b)
	: builtin(b), mynext(NULL)
	{
		if (builtin->next)
		{
			mynext = new builtin_method(builtin->next);
			assert(mynext);
		}
	}

	~builtin_method(void) { if (mynext) delete mynext; }

	const char *ident(void) const { return builtin->name; }

	/* return type */
	c_type::typespec returntype(void) const { return builtin->return_type; }

	/* arguments */
	unsigned int argcount(void) const { return builtin->nr; }

	c_type::typespec argtype(unsigned int index) const
	{
		assert(index < builtin->nr);

		return builtin->types[index];
	}

	calling argmethod(unsigned int index) const
	{
		assert(index < builtin->nr);

		switch (builtin->byref[index])
		{
			case 0: return byvalue;		break;
			case 1: return byref;		break;
			case 2: return byref_array;	break;
		}

		throw RuntimeError(xxInternal);
	}

	bool special(void) const { return builtin->special ? true : false; }

	const c_method *next(void) const throw() { return mynext; }

	void check(class edd_tree * const parent,
		   class METHOD_BASE_tree * const method,
		   const lib::pos &pos,
		   const std::vector<class c_node_tree *> &args,
		   std::vector<class c_type> &targs) const
	{
		unsigned int profile_flags = builtin->profile_flags;
		unsigned int check = 0;

		switch (parent->get_profile())
		{
			case edd_tree::HART:
				check = EDD_PROFILE_HART;
				break;
			case edd_tree::FIELDBUS:
				check = EDD_PROFILE_FIELDBUS;
				break;
			case edd_tree::PROFIBUS:
				check = EDD_PROFILE_PROFIBUS;
				break;
			case edd_tree::PROFINET:
				check = EDD_PROFILE_PROFINET;
				break;
		}

		if (!(profile_flags & check))
		{
			lib::scratch_buf buf(50);

			buf += "Builtin ";
			buf += builtin->name;

			parent->err().error(xxNotAllowedForProfile,
					    pos,
					    parent->ident().create(buf.c_str()),
					    lib::NoPosition,
					    parent->ident().create(parent->profile_descr()));
		}
		else
		{
			const struct all_profile_flags *flags = NULL;

			for (size_t i = 0; i < (sizeof(all_profile_flags)/sizeof(all_profile_flags[0])); i++)
			{
				if (all_profile_flags[i].profile == check)
				{
					flags = &(all_profile_flags[i]);
					break;
				}
			}

			assert(flags);

			if (profile_flags & flags->mask)
			{
				bool error = false;
				const char *msg = NULL;

				if (profile_flags & flags->option_PDM)
				{
					if (!parent->pdm_mode())
					{
						error = true;
						msg = "Profile without PDM comptibility";
					}
					else
					{
						parent->err().warning(xxNonConformantBuiltin,
								      pos,
								      parent->ident().create(builtin->name),
								      lib::NoPosition,
								      parent->ident().create("PDM"));
					}
				}

				if (profile_flags & flags->option_EDDx1)
				{
					if (!!parent->no_eddx1())
					{
						error = true;
						msg = "Profile without EDD 1.2 extensions";
					}
				}

				if (error)
				{
					lib::scratch_buf buf(50);

					buf += "Builtin ";
					buf += builtin->name;

					assert(msg);

					parent->err().error(xxNotAllowed,
							    pos,
							    parent->ident().create(buf.c_str()),
							    lib::NoPosition,
							    parent->ident().create(msg));
				}
			}
		}

		if (builtin->check)
			builtin->check(parent, method, pos, args, targs);
	}

	void name_conflict(class edd_tree * const parent,
			   class METHOD_BASE_tree * const method,
		   	   const lib::pos &pos) const
	{
		unsigned int profile_flags = builtin->profile_flags;
		unsigned int check = 0;

		switch (parent->get_profile())
		{
			case edd_tree::HART:
				check = EDD_PROFILE_HART;
				break;
			case edd_tree::FIELDBUS:
				check = EDD_PROFILE_FIELDBUS;
				break;
			case edd_tree::PROFIBUS:
				check = EDD_PROFILE_PROFIBUS;
				break;
			case edd_tree::PROFINET:
				check = EDD_PROFILE_PROFINET;
				break;
		}

		if (profile_flags & check)
		{
			const struct all_profile_flags *flags = NULL;

			for (size_t i = 0; i < (sizeof(all_profile_flags)/sizeof(all_profile_flags[0])); i++)
			{
				if (all_profile_flags[i].profile == check)
				{
					flags = &(all_profile_flags[i]);
					break;
				}
			}

			assert(flags);

			if ((profile_flags & flags->option_EDDx1) && !!parent->no_eddx1())
			{
				parent->err().warning(xxBuiltinKeyword,
						      pos,
						      parent->ident().create(builtin->name),
						      lib::NoPosition,
						      parent->ident().create("enhanced EDDs"));
			}
			else
			{
				parent->err().error(xxBuiltinKeyword,
						    pos,
						    parent->ident().create(builtin->name));
			}
		}
		else
		{
			lib::scratch_buf buf(50);

			if (profile_flags & EDD_PROFILE_HART)
				buf += parent->profile_descr(parent->HART);

			if (profile_flags & EDD_PROFILE_FIELDBUS)
			{
				if (!buf.empty())
					buf += '|';

				buf += parent->profile_descr(parent->FIELDBUS);
			}

			if (profile_flags & EDD_PROFILE_PROFIBUS)
			{
				if (!buf.empty())
					buf += '|';

				buf += parent->profile_descr(parent->PROFIBUS);
			}

			parent->err().warning(xxBuiltinKeyword,
					      pos,
					      parent->ident().create(builtin->name),
					      lib::NoPosition,
					      parent->ident().create(buf.c_str()));
		}
	}

	operand_ptr dispatch(method_env *env, operand_ptr args, const lib::pos &pos) const
	{
		if (!builtin->dispatch)
			throw RuntimeError(xxUnimplementedBuiltin,
					   (pos != lib::NoPosition) ? pos : env->context_node()->pos,
					   env->parent.ident().create(builtin->name));

		env->trace("BUILTINS", "call builtin %s", ident());

		return builtin->dispatch(env, args, pos);
	}

	bool self_code(void) const throw() { return (builtin->code != 0); }

	void code(class edd_tree * const parent,
		  class c_node_tree * const caller,
		  class stackmachine::SMachine &sm,
		  class c_compound_statement_tree *env,
		  const std::vector<class c_node_tree *> &args,
		  const std::vector<class c_type> &targs) const
	{
		if (!builtin->code)
		{
			throw RuntimeError(xxInternal,
					   caller->pos,
					   parent->ident().create("builtin_method::code"));
		}

		builtin->code(parent, caller, sm, env, args, targs);
	}

private:
	const struct builtin *builtin;
	class builtin_method *mynext;
};


edd_builtins::edd_builtins(void)
: methods(builtins_size)
{
}

edd_builtins::~edd_builtins(void)
{
	for (size_t i = 0; i < builtins_size; i++)
	{
		if (methods[i])
			delete methods[i];
	}
}

size_t
edd_builtins::search(const char *name)
{
	int lower = 0;
	int upper = builtins_size - 1;

	while (lower <= upper)
	{
		int middle = lower + (upper - lower) / 2;
		int cmp;

		cmp = strcmp(builtins[middle]->name, name);
		if (cmp == 0)
			return middle;

		if (cmp < 0)
			lower = middle + 1;
		else
			upper = middle - 1;
	}

	return builtins_size;
}

const class c_method *
edd_builtins::lookup(const std::string &name)
{
	size_t index = search(name.c_str());
	const class c_method *ret = NULL;

	if (index < builtins_size)
	{
		if (!methods[index])
		{
			methods[index] = new builtin_method(builtins[index]);
			assert(methods[index]);
		}

		ret = methods[index];
	}

	return ret;
}

void
edd_builtins::lookup_protos(const std::string &name, std::vector<std::string> &ret)
{
	size_t index = search(name.c_str());

	ret.clear();

	if (index < builtins_size)
	{
		const struct builtin *builtin = builtins[index];

		while (builtin)
		{
			ret.push_back(builtin->proto);

			builtin = builtin->next;
		}
	}
}

} /* namespace edd */
