
/* 
 * $Id: Interpreter.h,v 1.24 2009/07/18 09:49:33 fna Exp $
 * 
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 *               2011-2013 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the enhanced EDD library.
 */

#ifndef _Interpreter_h
#define _Interpreter_h

#ifdef _MSC_VER
# pragma warning (disable: 4200)
#endif

// C stdlib

// C++ stdlib
#include <string>
#include <vector>

// my lib

// own header
#include "edd_ast.h"
#include "edd_tree.h"


namespace edd
{

/**
 * General EDD interface.
 * This is the general EDD interface class. It features lot of nice
 * methods for easy access to the abstract syntax tree.
 */
class Interpreter : public edd_tree
{
public:
	/** \name Constructor/Desctructor. */
	//! \{
	Interpreter(void);
	~Interpreter(void);
	//! \}

	static int parse_Devices(const std::string &path,
				 std::vector<std::string> &includes,
				 std::vector<std::string> &dictionaries,
				 std::string &edd_file,
				 std::string &gsd_file,
				 lib::stream *log = NULL);

	static void lookup_dirs(const std::string &path,
				std::vector<std::string> &includes,
				bool bottom_up = true,
				lib::stream *log = NULL);

	static void pdm_defines(std::vector<std::string> &defines);

	std::vector<class EDD_OBJECT_tree *> allobjects(int kind) const;

	typedef struct { class MENU_ITEM_tree *item; class node_tree *ref; class method_env *env; } menuitem_t;
	typedef std::vector<menuitem_t> menuitems_t;

	menuitems_t evalmenu(class tree *t, class method_env *) const;

	std::string xml_eval(class method_env *, unsigned int id, bool recurse);
	std::string xml_eval(class method_env *, const std::string &name, bool recurse);
	void xml_assign(class method_env *, std::string access_path, const std::string &value);

	typedef struct { class COMMAND_tree *command; int transaction; } command_t;

	command_t search_command(class tree *var,
				 unsigned long op,
				 class method_env *);

	typedef std::vector<class UNIT_RELATION_tree *> unit_relations_t;

	unit_relations_t search_unit_relations(class tree *var,
					       class method_env *) const;

	int load_FILEs(lib::dyn_buf &buf, class method_env *env);
	int save_FILEs(lib::dyn_buf &buf, class method_env *env) const;

	bool has_components(void) const;
	void set_primary_component(COMPONENT_tree *) throw();
	COMPONENT_tree * get_primary_component(void) const throw();

	/** \name GUI helper. */
	//! \{
	static bool is_pdm_rootname(class tree *t);
	static bool is_pdm_rootname(const std::string &name);
	static bool is_pdm_rootname(const char *name);

	static bool is_pdm_specialmenu(const std::string &name);
	static bool is_pdm_specialmenu(const char *name);

	static menuitems_t filter_pdm_menus(const menuitems_t &);
	//! \}

	/** \name Tree statistics. */
	//! \{
	unsigned long stat_tree(unsigned long &terminal, unsigned long &nonterminal, unsigned long &eddobjects) const throw();
	unsigned long stat_tree(class edd::tree *t, unsigned long &, unsigned long &, unsigned long &) const throw();
	//! \}

private:
	operand_ptr xml_parse_access_path(class method_env *, std::string);

	static char *check_name(char *line, const char *name);
	static char *check_entry(char *line);

	static void lookup_dirs0(const std::string &path,
				 std::vector<std::string> &includes,
				 bool bottom_up,
				 lib::stream *log,
				 size_t level);

	COMPONENT_tree *primary_component;
};

} /* namespace edd */

#endif /* _Interpreter_h */
