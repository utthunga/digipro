
/* 
 * $Id: device_io_null.h,v 1.6 2009/08/11 14:23:10 fna Exp $
 * 
 * Copyright (C) 2001-2006 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the no communication object.
 */

#ifndef _device_io_null_h
#define _device_io_null_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "device_io_interface.h"


namespace edd
{

/**
 * Null I/O implementation.
 * This is a dummy implementation of the I/O mapper for the command
 * line checker or other standalone tools without specific communication.
 * It's used as default initialization by the EDD library. It
 * just do nothing if called.
 */
class device_io_null : public device_io_interface
{
public:
	virtual bool is_online(void) const;

	virtual int read(class method_env *, class EDD_OBJECT_tree *);
	virtual int write(class method_env *, class EDD_OBJECT_tree *);

	virtual int do_command(class method_env *,
			       const class COMMAND_tree *, int trans_nr, enum action,
			       int &warnings);
};

} /* namespace edd */

#endif /* _device_io_null_h */
