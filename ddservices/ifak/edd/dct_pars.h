
/* $Id: Parser.hh,v 1.5 2005/10/03 10:35:32 fna Exp $ */

#ifndef _dct_pars_h
#define _dct_pars_h

#include "position.h"
#include "reuse.h"

#include "dct_scan.h"

/* XXX $- user import declarations doesn't work :-( */
/* BEGIN IMPORT */
#include "cocktail_types.h"
/* END IMPORT */

namespace CT_NAMESPACE
{

#ifndef DOXYGEN_IGNORE

# line 2 "dct_pars.lalr"


/* EXPORT BEGIN */

struct str_list
{
	struct str_list *next;
	unsigned long len;
	char *str;
};

typedef int dct_pars_ret;
typedef struct str_list *plist;
typedef char *pchar;
typedef reuse::pos POS;

/* EXPORT END */

typedef struct { int Major; int Minor; } dct_pars_yyDictionaryKey;
typedef struct { plist Tree; } dct_pars_yystring_listR;
typedef struct { pchar Tree; POS Pos; } dct_pars_yyIdentifier;
typedef struct { int Value; } dct_pars_yyInteger;
typedef struct { plist Tree; } dct_pars_yyString;

typedef union {
dct_scan_tScanAttribute Scan;
dct_pars_yyDictionaryKey DictionaryKey;
dct_pars_yystring_listR string_listR;
dct_pars_yyIdentifier Identifier;
dct_pars_yyInteger Integer;
dct_pars_yyString String;
} dct_pars_tParsAttribute;

# line 57 "dct_pars.h"

typedef unsigned short yyStateRange;
typedef unsigned short yySymbolRange;

#endif /* DOXYGEN_IGNORE */

class dct_pars
{
public:
	/* named constants for start symbols */
	
	const char * const *TokenName;
	bool		Debug;
	
			dct_pars		(PARENT *);
			~dct_pars		(void);
int		Parse		(dct_pars_ret **, int mode);
int		Parse		(dct_pars_ret **, int mode, yyStateRange yyStartSymbol);
	
	lib::pos	mypos		(const reuse::tPosition &pos) const;
	lib::pos  mypos (const reuse::tPosition &pos1, const reuse::tPosition &pos2) const;
dct_scan		*scanner;
	
private:
	PARENT		*parent;

typedef dct_pars_tParsAttribute tParsAttribute;
typedef dct_scan_tScanAttribute tScanAttribute;
	typedef struct { short yyMode; bool yyActions, yyMessages; } yytControl;
	
	yytControl	yyControl;
	
	unsigned long	yyStateStackSize;
	yyStateRange	*yyStateStack;
	yyStateRange	*yyEndOfStack;
	
	unsigned long	yyAttrStackSize;
	tParsAttribute	*yyAttributeStack;
		
#if defined YYTrialParse | defined YYReParse
	yyStateRange	*yyStateStackPtr;
	tParsAttribute	*yyAttrStackPtr;
#endif
	yyStateRange	*yyIsContStackPtr;
	long		yyIsContStackSize;
	yyStateRange	*yyCompResStackPtr;
	long		yyCompResStackSize;
	
	void		yyStateRangeArrayExtend(yyStateRange *&ptr, long &size);
	
#if defined YYTrialParse | defined YYReParse | defined YYGetLook
	typedef struct
	{
		yySymbolRange	yyToken;
		tScanAttribute	yyAttribute;
#ifdef YYMemoParse
		short		yyStart;
#endif
	} yytBuffer;
	yytBuffer *yyBuffer;

	unsigned long	yyBufferSize;
	unsigned long	yyBufferNext;
	unsigned long	yyBufferLast;
	bool		yyBufferClear;
	short		yyParseLevel;
	
	void		yyBufferExtend	(void);
	int		yyGetToken	(void);
	void		yyBufferSet	(yySymbolRange);
#endif
#ifdef YYGetLook
	int		yyGetLookahead	(int, yySymbolRange);
 	void		xxGetAttribute	(int, yySymbolRange, tScanAttribute *);
#endif
#ifdef YYTrialParse
	int		yyTrialParse	(yyStateRange, yySymbolRange, int);
#endif
#ifdef YYReParse
	yytControl	yyPrevControl;
	
	int		ReParse		(yyStateRange, int, int, bool, bool);
	long		yyBufferOn	(bool, bool, yySymbolRange);
	long		BufferOff	(void);
	void		BufferClear	(void);
#endif
#ifdef YYDEBUG
	long		yyCount;
	FILE		*yyTrace;
	
	void		yyNl		(void) const;
	void		yyPrintState	(yyStateRange);
	bool		yyPrintResult	(yyStateRange, int, bool);
#endif
	void		yySyntaxError	(void) const;
int		yyParse		(dct_pars_ret **, int, yyStateRange, yySymbolRange, int);
	void		yyParse2	(yyStateRange &, yyStateRange *&, tParsAttribute *&,
					 int &, tParsAttribute &);
#ifndef NO_RECOVER
	void		yyErrorRecovery	(yySymbolRange *, yyStateRange *, long);
	bool		yyIsContinuation (yySymbolRange, yyStateRange *, long);
	void		yyComputeContinuation (yyStateRange *, long, reuse::Set &);
	void		yyComputeRestartPoints (yyStateRange *, long, reuse::Set &);
	yyStateRange	yyNext(yyStateRange yyState, yySymbolRange yySymbol) const;
#endif
};

} /* namespace CT_NAMESPACE */

#endif /* _dct_pars_h */
