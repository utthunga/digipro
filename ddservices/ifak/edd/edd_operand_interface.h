
/* 
 * $Id: edd_operand_interface.h,v 1.73 2009/09/21 11:01:36 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 *               2012 Marco Meier <marco.meier@ifak.eu>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the mathematic tree object abstraction.
 */

#ifndef _edd_operand_interface_h
#define _edd_operand_interface_h

// C stdlib
#include <assert.h>

// C++ stdlib
#include <memory>
#include <stdexcept>
#include <string>

// my lib
#include "ident.h"
#include "stream.h"

// own
#include "cocktail_types.h"
#include "edd_types.h"


namespace edd
{

class operand;

class operand_ptr_base_help
{
protected:
	static inline void ref(class operand *);
	static inline void unref(class operand *);
};

template<typename T>
class operand_ptr_base : public operand_ptr_base_help
{
public:
	typedef class operand_ptr_base<T> Self;

	~operand_ptr_base() { unref(ptr); if (nxt) delete nxt; }

	operand_ptr_base(T *p = NULL)
	: ptr(p), nxt(NULL) { ref(ptr); }

	explicit operand_ptr_base(std::auto_ptr<T> p)
	: ptr(p.get()), nxt(NULL) { p.release(); ref(ptr); }

	/* Copy constructor. */
	operand_ptr_base(const Self &p)
	: ptr(p.ptr) { ref(ptr); nxt = p.nxt ? new Self(*p.nxt) : NULL; }

	/* Assignment constructor. */
	Self& operator=(const Self &p)
	{ reset(p.ptr); if (p.nxt) nxt = new Self(*p.nxt); return *this; }

	/* Assignment constructor. */
	Self& operator=(T *p)
	{ reset(p); return *this; }

	void reset(T *p = NULL) { ref(p); unref(ptr); ptr = p; if (nxt) { delete nxt; nxt = NULL; } }

	T& operator*()
	{
		assert(ptr);
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return *ptr;
	}
	const T& operator*() const
	{
		assert(ptr);
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return *ptr;
	}

	T* operator->()
	{
		assert(ptr);
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return ptr;
	}
	const T* operator->() const
	{
		assert(ptr);
		if (!ptr) throw std::logic_error("dereferencing NULL");
		return ptr;
	}

	T* get() const { return ptr; }

	operator bool() const throw() { return ptr != NULL; }

	bool operator==(const Self &p) const throw() { return (ptr == p.ptr); }
	bool operator!=(const Self &p) const throw() { return (ptr != p.ptr); }

	/* fulfill strict weak ordering */
	bool operator< (const Self &p) const throw() { return (ptr <  p.ptr); }

	/**
	 * \name List handling.
	 * To every %operand object an %operand list can be attached. This are
	 * all neccessary methods to manipulate these list. Supported routines
	 * are getting the next element, enqueue operation and length calculation.
	 */
	//! \{
	/**
	 * Lookup next element in list.
	 * This return the first element in the %operand list. It's called
	 * next because it return the next element in the list following this
	 * element if this element is also part of an %operand list.
	 */
	Self next(void) const throw() { return nxt ? *nxt : Self(); }
	/**
	 * Enqueue a new element.
	 * This method enqueue an element at the tail of the %operand list.
	 */
	void concat(const Self &p)
	{ if (p) { if (ptr) { Self **t = &nxt; while (*t) t = &((*t)->nxt); *t = new Self(p); }
	           else { assert(!nxt); reset(p.ptr); if (p.nxt) nxt = new Self(*p.nxt); } } }
	/**
	 * Calculate list len.
	 * This method account the length of the %operand list.
	 */
	int len(void) const throw()
	{ int len = ptr ? 1 : 0; Self *t = nxt; while (t) { t = t->nxt; ++len; } return len; }
	//! \}

	/**
	 * Dump out short description.
	 * For debugging only. Useful to inspect the result of specific
	 * evaluation operations. It take care of lists.
	 */
	void dump(lib::stream &f) const { if (ptr) ptr->dump(f); if (nxt) nxt->dump(f); }

protected:
	T *ptr;
	Self *nxt;
};

typedef operand_ptr_base<class operand> operand_ptr;

/**
 * Mathematic EDD tree object abstraction.
 * This is the abstract interface used for every runtime evaluation of EDD
 * tree objects. It's in the simple case just an elementar data type, or a
 * complex data type with special post/pre conditions, or it's a tree
 * reference to an EDD object and attached values. Operand objects can be
 * concatenated to lists there it's neccessary. All required methods for that
 * are inherited by the specializations of the %operand class.
 */
class operand
{
private:
	/* no copy constructor semantic defined */
	operand(const operand &);

protected:
	class method_env *env;

	friend class operand_ptr_base_help;
	size_t refs;

public:
	/** \name Constructor/destructor. */
	//! \{
	operand(void) : env(NULL), refs(0) { }
	virtual ~operand(void);
	//! \}

	/**
	 * \name Environment handling.
	 */
	//! \{
	//! \}

	/**
	 * \name Mandatory helper methods.
	 * Support routines that must be implemented by a concrete class
	 * specialization.
	 */
	//! \{
	/**
	 * Internal clone itself.
	 * This method create an exact copy of *this. This must
	 * be implemented, it's needed in several places for automatic storage
	 * management and easy abstract handling of the %operand objects.
	 * \return Newly allocated, exact copy of *this.
	 * \exception RuntimeError(xxOutOfMemory) - not enough memory to
	 * complete the operation.
	 */
	virtual operand_ptr clone(void) const = 0;
	/**
	 * Get operand related environment.
	 */
	class method_env *getenv(void) const throw() { return env; }
	/**
	 * Set environment the operand relate too.
	 */
	void setenv(class method_env *env) throw();
	/**
	 * Internal consistency helper.
	 */
	virtual void env_assert(class method_env *env) const;
	/**
	 * Dump out short description.
	 * For debugging only. Useful to inspect the result of specific
	 * evaluation operations. It take care of lists.
	 */
	void dump(lib::stream &) const;

protected:
	/**
	 * Internal setenv helper.
	 * Must be implemented by operand implementations that encapsule
	 * other operand to push down the setenv request.
	 */
	virtual void setenv0(class method_env *env) throw();
	/**
	 * Internal dump out short description.
	 * For debugging only. Useful to inspect the result of specific
	 * evaluation operations.
	 */
	virtual void dump0(lib::stream &) const = 0;
	//! \}

public:
	/**
	 * \name String I/O methods (optional).
	 * Simple input and output routines for an %operand.
	 * Are mainly used by the concrete data type representations for easy
	 * reading and writing by the higher levels. They use the std::string
	 * as transport medium for uncomplicated, generic handling.
	 */
	//! \{
	/**
	 * Lowlevel print operation.
	 * Simple print of the value to a readable string. This method don't
	 * do any EDD related transformations or pretty printing
	 * (\see pretty_print instead).
	 * The fmt argument can be used to use an alternative printf format
	 * mask.
	 * \warning Be sure you use a correct printf format string for
	 * this type. Otherweise the behaviour is undefined.
	 * \param fmt Explicit printf style format string.
	 * \return The printed value of this %operand.
	 * \exception RuntimeError(xxNoValue) - this %operand cannot be
	 * printed.
	 */
	virtual std::string print(const char *fmt = NULL) const;
	/**
	 * Pretty print operation.
	 * This is the main entry point to obtain a pretty printed value.
	 * This method is specially designed for the usage in the graphical
	 * user interface. It do all EDD related transformations and pretty
	 * printing (taking SCALING_FACTOR, DISPLAY_FORMAT and enumerations
	 * into account for example).
	 * \return The pretty printed value of this %operand.
	 * \exception RuntimeError(xxNoValue) - this %operand cannot be
	 * printed.
	 */
	virtual std::string pretty_print(void) const;
	/**
	 * Lowlevel input format conversion.
	 * Scan the input string and assign the input value to the operand.
	 * This method don't take any special EDD aspects into account.
	 * \param str The input string.
	 * \param strict Enable/disable strict value checking. Strict value
	 * checking enforce that the new value is a valid value of the
	 * variable and don't violate ranges etc. If strict checking is
	 * disabled, no exception is thrown.
	 * \exception RuntimeError(xxNonValue) - a value can't assigned  to
	 * this %operand.
	 * \exception RuntimeError(xxRange) - input value out of range.
	 * \exception RuntimeError(...) - other error happened (%operand
	 * depending).
	 */
	virtual void scan(const std::string &str, bool strict = true);
	/**
	 * Advanced input format conversion.
	 * This is the main entry point for the graphical user interface to
	 * assign a new value to the %operand. It take special EDD aspects
	 * into account (like SCALING_FACTOR, enumeration values and so on).
	 * If the string or new value is invalid an exception is thrown.
	 * \param str The input string.
	 * \exception RuntimeError(xxNonValue) - a value can't assigned  to
	 * this %operand.
	 * \exception RuntimeError(xxRange) - input value out of range.
	 * \exception RuntimeError(...) - other error happened (%operand
	 * depending).
	 */
	virtual void pretty_scan(const std::string &str);
	//! \}

	/**
	 * \name Stackmachine data I/O.
	 * This methods interfaces the %operand to the
	 * stackmachine::mstackdata class used by the %stackmachine.
	 */
	//! \{
	virtual          operator stackmachine::Type *() const;
	virtual void     operator= (const stackmachine::Type &);
	//! \}

	/** \name Status flag constants. */
	enum value_status
	{
		/**
		 * Specifies that the value is valid and not of one of the
		 * other states.
		 */
		VALUE_STATUS_NONE = 0,
		/**
		 * Specifies that the value is out of range.
		 */
		VALUE_STATUS_INVALID = 1,
		/**
		 * Specifies that the value could not be transfered.
		 */
		VALUE_STATUS_NOT_ACCEPTED = 2,
		/**
		 * Specifies that the value is not supported.
		 */
		VALUE_STATUS_NOT_SUPPORTED = 4,
		/**
		 * Specifies that the value has been changed by the user or
		 * by a method.
		 */
		VALUE_STATUS_CHANGED = 8,
		/**
		 * Specifies that the value has been successful loaded.
		 */
		VALUE_STATUS_LOADED = 16,
		/**
		 * Specifies that the value is not changed by the user or a
		 * method until now.
		 */
		VALUE_STATUS_INITIAL = 32,
		/**
		 * Specifies that the value has not been converted
		 * e.g because the units are not compatible.
		 */
		VALUE_STATUS_NOT_CONVERTED = 64
	};
	typedef enum value_status value_status_t;
	static const char *value_status_descr(value_status_t);

	/** \name Helper methods (optional). */
	//! \{
	virtual operand::value_status_t& status(void);
	virtual const operand::value_status_t& status(void) const;
	virtual void eval_values(std::vector<operand_ptr> &);
	//! \}

	/**
	 * EDD object status management.
	 * Helper class for managing the lvalue status flags of EDD objects.
	 */
	class status
	{
	private:
		operand::value_status_t mystatus;

	public:
		status(operand::value_status_t status) : mystatus(status) { }

		void dump(lib::stream &f) const throw();
		operand::value_status_t& reference(void) throw() { return mystatus; }
		const operand::value_status_t& reference(void) const throw() { return mystatus; }

		/* status transitions */
		void none(void) throw() { mystatus = operand::VALUE_STATUS_NONE; }
		void invalid(void) throw() { mystatus = operand::VALUE_STATUS_INVALID; }
		void not_accepted(void) throw() { mystatus = operand::VALUE_STATUS_NOT_ACCEPTED; }
		void not_supported(void) throw() { mystatus = operand::VALUE_STATUS_NOT_SUPPORTED; }
		void changed(void) throw() { mystatus = operand::VALUE_STATUS_CHANGED; }
		void loaded(void) throw() { mystatus = operand::VALUE_STATUS_LOADED; }
		void initial(void) throw() { mystatus = operand::VALUE_STATUS_INITIAL; }
		void not_converted(void) throw() { mystatus = operand::VALUE_STATUS_NOT_CONVERTED; }
	};

	/** \name Cast operators. */
	//! \{
	// Basic Types
	virtual             operator bool() const;
	virtual             operator int8() const;
	virtual             operator int16() const;
	virtual             operator int32() const;
	virtual             operator int64() const;
	virtual             operator uint8() const;
	virtual             operator uint16() const;
	virtual             operator uint32() const;
	virtual             operator uint64() const;
	virtual             operator float32() const;
	virtual             operator float64() const;
	virtual             operator std::string() const;
	// EDD related
	virtual             operator lib::IDENTIFIER() const;
	virtual             operator node_tree *() const;
	virtual             operator EDD_OBJECT_tree *() const;
	//! \}

	/** \name Postfix operators. */
	//! \{
	virtual operand_ptr operator++  (int); // post
	virtual operand_ptr operator--  (int); // post
	//! \}

	/** \name Unary operators. */
	//! \{
	virtual operand_ptr operator++  (void); // pre
	virtual operand_ptr operator--  (void); // pre
	virtual operand_ptr operator+   (void) const;
	virtual operand_ptr operator-   (void) const;
	virtual operand_ptr operator~   (void) const;
	virtual operand_ptr operator!   (void) const;
	//! \}

	/** \name Binary operators. */
	//! \{
	virtual operand_ptr operator*   (const operand &) const;
	virtual operand_ptr operator/   (const operand &) const;
	virtual operand_ptr operator+   (const operand &) const;
	virtual operand_ptr operator-   (const operand &) const;
	virtual operand_ptr operator%   (const operand &) const;
	virtual operand_ptr operator^   (const operand &) const;
	virtual operand_ptr operator&   (const operand &) const;
	virtual operand_ptr operator|   (const operand &) const;
	virtual operand_ptr operator<<  (const operand &) const;
	virtual operand_ptr operator>>  (const operand &) const;
	virtual bool        operator<   (const operand &) const;
	virtual bool        operator>   (const operand &) const;
	virtual bool        operator==  (const operand &) const;
	virtual bool        operator!=  (const operand &) const;
	virtual bool        operator<=  (const operand &) const;
	virtual bool        operator>=  (const operand &) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void        operator=   (const operand &);
	virtual void        operator+=  (const operand &);
	virtual void        operator-=  (const operand &);
	virtual void        operator*=  (const operand &);
	virtual void        operator/=  (const operand &);
	virtual void        operator%=  (const operand &);
	virtual void        operator^=  (const operand &);
	virtual void        operator&=  (const operand &);
	virtual void        operator|=  (const operand &);
	virtual void        operator>>= (const operand &);
	virtual void        operator<<= (const operand &);
	//! \}

	/** \name Other operators. */
	//! \{
	virtual operand_ptr operator[]  (long) const;
	virtual operand_ptr operator()  (operand_ptr) const;
	virtual operand_ptr member      (const lib::IDENTIFIER &) const;
	//! \}

	/** \name Logic operators. */
	//! \{
	bool operator&& (const operand &op) const;
	bool operator|| (const operand &op) const;
	//! \}
};

inline void
operand_ptr_base_help::ref(class operand *ptr)
{
	if (ptr)
		++ptr->refs;
}

inline void
operand_ptr_base_help::unref(class operand *ptr)
{
	if (ptr && --ptr->refs == 0)
		delete ptr;
}

template<typename T>
class operand_ptr_T : public operand_ptr_base<T>
{
public:
	operand_ptr_T(T *p = NULL) : operand_ptr_base<T>(p) { }
	explicit operand_ptr_T(std::auto_ptr<T> p) : operand_ptr_base<T>(p) { }

	/* Copy constructor. */
	operand_ptr_T(const operand_ptr_T<T> &p) : operand_ptr_base<T>(p) { }

	/* Assignment constructor. */
	operand_ptr_T<T>& operator=(const operand_ptr_T<T> &p) { this->reset(p.ptr); return *this; }
	operand_ptr_T<T>& operator=(T *p) { this->reset(p); return *this; }

	operator operand_ptr() const { return operand_ptr_base<T>::ptr; }
};

} /* namespace edd */

#endif /* _edd_operand_interface_h */
