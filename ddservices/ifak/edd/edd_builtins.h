
/* 
 * Wed Nov 20 09:30:48 2013 CET
 * 
 * DO NOT EDIT!
 * 
 * This file is automatically created by the builtins-proto
 * generator tool, written for the EDDL project.
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */


/* 
 * $Id: edd_builtins.h.in,v 1.15 2005/10/03 10:35:32 fna Exp $
 * 
 * Copyright (C) 2001-2005 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the builtins.
 * \note This is a generated file. Any changes you made are lost on the next
 * rebuild.
 */

#ifndef _edd_builtins_h
#define _edd_builtins_h

// C stdlib

// C++ stdlib
#include <vector>

// my lib

// own header
#include "edd_builtins_interface.h"


namespace edd
{

/**
 * \internal
 * Concrete builtin interface.
 * This class manage the builtins. It's generated and hold statically
 * all neccessary informations about the available builtins (this include
 * return type, argument types and the corresponding calling convention and
 * at last the calling dispatcher that execute the builtin function.
 */
class edd_builtins : public edd_builtins_interface
{
public:
	edd_builtins(void);
	~edd_builtins(void);

	const class c_method *lookup(const std::string &name);

	static void lookup_protos(const std::string &name, std::vector<std::string> &);

private:
	static size_t search(const char *name);

	std::vector<class c_method *> methods;
};

} /* namespace edd */

#endif /* _edd_builtins_h */
