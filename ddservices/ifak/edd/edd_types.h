
/* 
 * $Id: edd_types.h,v 1.21 2008/09/29 21:41:49 fna Exp $
 * 
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/**
 * \file
 * \author Frank Naumann <frank.naumann@ifak-md.de>
 * \brief Definitions for the basic data types.
 */

#ifndef _edd_types_h
#define _edd_types_h

// C stdlib

// C++ stdlib

// my lib

// own
#include "Type.h"


namespace edd
{

using stackmachine::int8;
using stackmachine::int16;
using stackmachine::int32;
using stackmachine::int64;

using stackmachine::uint8;
using stackmachine::uint16;
using stackmachine::uint32;
using stackmachine::uint64;

using stackmachine::float32;
using stackmachine::float64;

} /* namespace edd */

#endif /* _edd_types_h */
