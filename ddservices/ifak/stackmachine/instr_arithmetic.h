
/* 
 * $Id: instr_arithmetic.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_arithmetic_h
#define _stackmachine_instr_arithmetic_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"


namespace stackmachine
{

/* arithmetic instructions
 */

class instr_ADD : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_SUB : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_MUL : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_DIV : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_ASR : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_ASL : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_AND : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_XOR : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_OR : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_MOD : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_NEG : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_POS : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_INV : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_arithmetic_h */
