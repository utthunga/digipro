
/* 
 * $Id: BasicType.cxx,v 1.22 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>

// C++ stdlib
#include <stdexcept>

// my lib
#include "check_fit_float.h"
#include "port.h"
#include "string.h"

// own header
#include "BasicType.h"

#ifndef _
# define _(x) x
#endif


#define LOG_LOWTRACE(x)
#define LOG_TRACE(x)
#define LOG_WARNING(x)


#ifndef LOG_LOWTRACE
#define LOG_LOWTRACE(x) ::printf x
#endif

#ifndef LOG_TRACE
#define LOG_TRACE(x)    ::printf x
#endif

#ifndef LOG_WARNING
#define LOG_WARNING(x)  ::printf x
#endif

#ifndef LOG_ERROR
#define LOG_ERROR(x)    ::printf x
#endif


namespace stackmachine
{

static const std::runtime_error range_error(_("overflow/underflow error"));
static const std::runtime_error illegal_character_error(_("illegal character"));


/* BasicType implementation */

template<typename T>
Type* BasicType<T>::clone(void) const { return new BasicType<T>(value); }

template<typename T>
std::string BasicType<T>::type_descr(void) const { return BasicType<T>::descr(); }

template<typename T>
bool  BasicType<T>::operator==  (const Type &t) const { return (value == (T)t); }

template<typename T>
bool  BasicType<T>::operator!=  (const Type &t) const { return (value != (T)t); }

template<typename T>
void  BasicType<T>::operator=   (const Type &t) { value = (T)t; }

template<typename T>
void  BasicType<T>::operator=   (const Self &t) { value = t.value; }

template<typename T>
template<typename T1>
T BasicType<T>::scan0<0, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(!nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef unsigned long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoul(s, &check, 0);

	/* strtoul accept negative numbers and
	 * return the negated value as result [no error];
	 * so explicitly check for a negation sign
	 */
	if (*s == '-')
		throw range_error;

	if ((errno == ERANGE && (tmp == ULONG_MAX))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp > nl::max())
			throw range_error;
	}

	return T(tmp);
}

template<typename T>
template<typename T1>
T BasicType<T>::scan0<1, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtol(s, &check, 0);

	if ((errno == ERANGE && (tmp == LONG_MAX || tmp == LONG_MIN))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp < nl::min() || tmp > nl::max())
			throw range_error;
	}

	return T(tmp);
}
template<typename T>
template<typename T1>
T BasicType<T>::scan0<2, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(!nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef unsigned long long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoull(s, &check, 0);

	/* strtoull accept negative numbers and
	 * return the negated value as result [no error];
	 * so explicitly check for a negation sign
	 */
	if (*s == '-')
		throw range_error;

	if ((errno == ERANGE && (tmp == ULLONG_MAX))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp > nl::max())
			throw range_error;
	}

	return T(tmp);
}
template<typename T>
template<typename T1>
T BasicType<T>::scan0<3, T1>::go(const std::string &str0)
{
	typedef std::numeric_limits<T> nl;
	assert(nl::is_signed);

	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;

	typedef long long strtoX_t;
	typedef std::numeric_limits<strtoX_t> nl_strtoX_t;
	strtoX_t tmp;

	errno = 0;
	tmp = strtoll(s, &check, 0);

	if ((errno == ERANGE && (tmp == LLONG_MAX || tmp == LLONG_MIN))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (nl::digits < nl_strtoX_t::digits)
	{
		if (tmp < nl::min() || tmp > nl::max())
			throw range_error;
	}

	return T(tmp);
}
template<typename T>
void BasicType<T>::scan(const std::string &str)
{
	typedef std::numeric_limits<T> nl;

	assert(nl::is_specialized);
	assert(nl::is_bounded);
	assert(nl::is_integer);

	const int val =
		  (nl::is_signed   ? 0x1 : 0x0)
		| (nl::digits > 32 ? 0x2 : 0x0);

	/* 
	 * 0 | strtoul   (unsigned long)
	 * 1 | strtol    (long)
	 * 2 | strtoull  (unsigned long long)
	 * 3 | strtoll   (long long)
	 */
	value = scan0<val, T>::go(str);
}

/* T - unsigned
 */
template<typename T>
template<typename T1>
int BasicType<T>::min_digits0<false, T1>::go(T1 value, bool &need_sign)
{
	assert(is_integer());
	need_sign = false;

	int ret = 0;

	while (value)
	{
		value >>= 1;
		++ret;

		assert(ret < 256);
	}

	return ret;
}
/* T - signed
 */
template<typename T>
template<typename T1>
int BasicType<T>::min_digits0<true, T1>::go(T1 value, bool &need_sign)
{
	assert(is_integer());
	need_sign = false;

	if (value < 0)
	{
		need_sign = true;

		typedef std::numeric_limits<int8>  nl_int8;
		typedef std::numeric_limits<int16> nl_int16;
		typedef std::numeric_limits<int32> nl_int32;
		typedef std::numeric_limits<int64> nl_int64;

		if (value == nl_int8::min())
			return nl_int8::digits;

		if (nl::digits >= nl_int16::digits)
		{
			if (value == nl_int16::min())
				return nl_int16::digits;

			if (nl::digits >= nl_int32::digits)
			{
				if (value == nl_int32::min())
					return nl_int32::digits;

				if (nl::digits >= nl_int64::digits)
				{
					if (value == nl_int64::min())
						return nl_int64::digits;
				}
			}
		}

		value *= -1;
		assert(!(value < 0));
	}

	int ret = 0;

	while (value)
	{
		value >>= 1;
		++ret;

		assert(ret < 256);
	}

	return ret;
}

/* dst - unsigned
 * src - unsigned
 */
template<typename T>
template<typename T_dst, typename T_src>
T_dst BasicType<T>::to_basic_int0<0, T_dst, T_src>::go(T_src value)
{
	typedef std::numeric_limits<T_dst> nl_dst;
	typedef std::numeric_limits<T_src> nl_src;

	LOG_LOWTRACE(("#0 unsigned/unsigned [dst bits = %i, src bits = %i]\n",
		      nl_dst::digits, nl_src::digits));

	if (value > nl_dst::max())
	{
		BasicType<T_src> t_src(value);
		BasicType<T_dst> t_dst(nl_dst::max());

		LOG_WARNING(("#0 unsigned/unsigned [dst bits = %i, src bits = %i]\n",
			     nl_dst::digits, nl_src::digits));

		LOG_WARNING(("to_basic_int<>: overflow [%s > dst max %s]\n",
			     t_src.print().c_str(), t_dst.print().c_str()));
	}

	return (T_dst)value;
}
/* dst - signed
 * src - unsigned
 */
template<typename T>
template<typename T_dst, typename T_src>
T_dst BasicType<T>::to_basic_int0<1, T_dst, T_src>::go(T_src value)
{
	typedef std::numeric_limits<T_dst> nl_dst;
	typedef std::numeric_limits<T_src> nl_src;

	LOG_LOWTRACE(("#1 signed/unsigned [dst bits = %i, src bits = %i]\n",
		      nl_dst::digits, nl_src::digits));

	if (nl_src::digits > nl_dst::digits)
	{
		if (value > (T_src)nl_dst::max())
		{
			BasicType<T_src> t_src(value);
			BasicType<T_dst> t_dst(nl_dst::max());

			LOG_WARNING(("#1 signed/unsigned [dst bits = %i, src bits = %i]\n",
				     nl_dst::digits, nl_src::digits));

			LOG_WARNING(("to_basic_int<>: overflow [%s > dst max %s]\n",
				     t_src.print().c_str(), t_dst.print().c_str()));
		}
	}

	return (T_dst)value;
}
/* dst - unsigned
 * src - signed
 */
template<typename T>
template<typename T_dst, typename T_src>
T_dst BasicType<T>::to_basic_int0<2, T_dst, T_src>::go(T_src value)
{
	typedef std::numeric_limits<T_dst> nl_dst;
	typedef std::numeric_limits<T_src> nl_src;

	LOG_LOWTRACE(("#2 unsigned/signed [dst bits = %i, src bits = %i]\n",
		      nl_dst::digits, nl_src::digits));

	if (value < 0)
	{
		BasicType<T_src> t_src(value);

		LOG_WARNING(("#2 unsigned/signed [dst bits = %i, src bits = %i]\n",
			     nl_dst::digits, nl_src::digits));

		LOG_WARNING(("to_basic_int<>: assigning negative to unsigned [%s]\n",
			     t_src.print().c_str()));
	}
	else
	{
		if (nl_src::digits > nl_dst::digits)
		{
			if (value > (T_src)nl_dst::max())
			{
				BasicType<T_src> t_src(value);
				BasicType<T_dst> t_dst(nl_dst::max());

				LOG_WARNING(("#2 unsigned/signed [dst bits = %i, src bits = %i]\n",
					     nl_dst::digits, nl_src::digits));

				LOG_WARNING(("to_basic_int<>: overflow [%s > dst max %s]\n",
					     t_src.print().c_str(), t_dst.print().c_str()));
			}
		}
	}

	return (T_dst)value;
}
/* dst - signed
 * src - signed
 */
template<typename T>
template<typename T_dst, typename T_src>
T_dst BasicType<T>::to_basic_int0<3, T_dst, T_src>::go(T_src value)
{
	typedef std::numeric_limits<T_dst> nl_dst;
	typedef std::numeric_limits<T_src> nl_src;

	LOG_LOWTRACE(("#3 signed/signed [dst bits = %i, src bits = %i]\n",
		      nl_dst::digits, nl_src::digits));

	if (value > nl_dst::max())
	{
		BasicType<T_src> t_src(value);
		BasicType<T_dst> t_dst(nl_dst::max());

		LOG_WARNING(("#3 signed/signed [dst bits = %i, src bits = %i]\n",
			     nl_dst::digits, nl_src::digits));

		LOG_WARNING(("to_basic_int<>: overflow [%s > dst max %s]\n",
			     t_src.print().c_str(), t_dst.print().c_str()));
	}
	else if (value < nl_dst::min())
	{
		BasicType<T_src> t_src(value);
		BasicType<T_dst> t_dst(nl_dst::min());

		LOG_WARNING(("#3 signed/signed [dst bits = %i, src bits = %i]\n",
			     nl_dst::digits, nl_src::digits));

		LOG_WARNING(("to_basic_int<>: underflow [%s < dst min %s]\n",
			     t_src.print().c_str(), t_dst.print().c_str()));
	}

	return (T_dst)value;
}

/*
 * Nice example for advanced template meta programming.
 * Really easy, if you understand the template principles.
 * -------------------------------------------------------
 * Templates are discussed in detail in:
 * - C++ Templates - The Complete Guide
 *   from David Vandevoorde / Nicolai M. Josuttis
 *   Addision-Wesley, ISBN 0201734842
 * - or ask me :-)
 */

template<typename T>
template<typename T1>
T1 BasicType<T>::to_basic_int(void) const
{
	typedef std::numeric_limits<T1> nl_dst;

	assert(nl_dst::is_specialized);
	assert(nl_dst::is_bounded);
	assert(nl_dst::is_integer);

	const int val =
		  (nl_dst::is_signed ? 0x1 : 0x0)
		| (nl::is_signed     ? 0x2 : 0x0);

	/*   | dst      | src
	 * --+----------+---------
	 * 0 | unsigned | unsigned
	 * 1 | signed   | unsigned
	 * 2 | unsigned | signed
	 * 3 | signed   | signed
	 */

	return to_basic_int0<val, T1, T>::go(value);
}

template<typename T>
template<typename T1>
T1 BasicType<T>::to_basic_float(void) const
{
	typedef std::numeric_limits<T1> dst_nl;

	assert(dst_nl::is_specialized);
	assert(!dst_nl::is_integer);

	return (T1)value;
}


/* BoolBasicType implementation */

BoolBasicType::BoolBasicType(void) { }

BoolBasicType::BoolBasicType(bool v) : BasicType<bool>(v) { }

Type* BoolBasicType::clone(void) const { return new BoolBasicType(value); }

      BoolBasicType::operator bool()    const { return value; }
      BoolBasicType::operator int8()    const { return value ? 1 : 0; }
      BoolBasicType::operator int16()   const { return value ? 1 : 0; }
      BoolBasicType::operator int32()   const { return value ? 1 : 0; }
      BoolBasicType::operator int64()   const { return value ? 1 : 0; }
      BoolBasicType::operator uint8()   const { return value ? 1 : 0; }
      BoolBasicType::operator uint16()  const { return value ? 1 : 0; }
      BoolBasicType::operator uint32()  const { return value ? 1 : 0; }
      BoolBasicType::operator uint64()  const { return value ? 1 : 0; }
      BoolBasicType::operator float32() const { return value ? 1.0f : 0.0f; }
      BoolBasicType::operator float64() const { return value ? 1.0  : 0.0; }

void  BoolBasicType::operator!   (void) { value = !value; }

void  BoolBasicType::operator^=  (const Type &t) { value ^=  (bool)t; }
void  BoolBasicType::operator&=  (const Type &t) { value &=  (bool)t; }
void  BoolBasicType::operator|=  (const Type &t) { value |=  (bool)t; }


/* ValueBasicType implementation */

template<typename T>
Type* ValueBasicType<T>::clone(void) const { return new ValueBasicType<T>(value); }

template<typename T>
ValueBasicType<T>::operator bool() const { return value ? true : false; }
template<typename T>
ValueBasicType<T>::operator int8() const { return BasicType<T>::template to_basic_int<int8>(); }
template<typename T>
ValueBasicType<T>::operator int16() const { return BasicType<T>::template to_basic_int<int16>(); }
template<typename T>
ValueBasicType<T>::operator int32() const { return BasicType<T>::template to_basic_int<int32>(); }
template<typename T>
ValueBasicType<T>::operator int64() const { return BasicType<T>::template to_basic_int<int64>(); }
template<typename T>
ValueBasicType<T>::operator uint8() const { return BasicType<T>::template to_basic_int<uint8>(); }
template<typename T>
ValueBasicType<T>::operator uint16() const { return BasicType<T>::template to_basic_int<uint16>(); }
template<typename T>
ValueBasicType<T>::operator uint32() const { return BasicType<T>::template to_basic_int<uint32>(); }
template<typename T>
ValueBasicType<T>::operator uint64() const { return BasicType<T>::template to_basic_int<uint64>(); }
template<typename T>
ValueBasicType<T>::operator float32() const { return BasicType<T>::template to_basic_float<float32>(); }
template<typename T>
ValueBasicType<T>::operator float64() const { return BasicType<T>::template to_basic_float<float64>(); }

template<typename T>
void ValueBasicType<T>::operator+   (void) { value = +value; }
template<typename T>
void ValueBasicType<T>::operator-   (void) { value = -value; }
template<typename T>
void ValueBasicType<T>::operator!   (void) { value = !value; }

template<typename T>
bool  ValueBasicType<T>::operator<   (const Type &t) const { return (value <  (T)t); }
template<typename T>
bool  ValueBasicType<T>::operator>   (const Type &t) const { return (value >  (T)t); }
template<typename T>
bool  ValueBasicType<T>::operator<=  (const Type &t) const { return (value <= (T)t); }
template<typename T>
bool  ValueBasicType<T>::operator>=  (const Type &t) const { return (value >= (T)t); }

template<typename T>
void  ValueBasicType<T>::operator+=  (const Type &t) { value +=  (T)t; }
template<typename T>
void  ValueBasicType<T>::operator-=  (const Type &t) { value -=  (T)t; }
template<typename T>
void  ValueBasicType<T>::operator*=  (const Type &t) { value *=  (T)t; }
template<typename T>
void  ValueBasicType<T>::operator/=  (const Type &t)
{
	/* catch division by zero */
	if (this->is_integer() && (T)t == 0)
		throw std::runtime_error(_("division by zero"));

	value /= (T)t;
}


/* IntegerBasicType implementation */

template<typename T>
Type* IntegerBasicType<T>::clone(void) const { return new IntegerBasicType<T>(value); }

template<typename T>
void IntegerBasicType<T>::operator~   (void) { value = ~value; }

template<typename T>
void  IntegerBasicType<T>::operator%=  (const Type &t) { value %=  (T)t; }
template<typename T>
void  IntegerBasicType<T>::operator^=  (const Type &t) { value ^=  (T)t; }
template<typename T>
void  IntegerBasicType<T>::operator&=  (const Type &t) { value &=  (T)t; }
template<typename T>
void  IntegerBasicType<T>::operator|=  (const Type &t) { value |=  (T)t; }
template<typename T>
void  IntegerBasicType<T>::operator>>= (const Type &t) { value >>= (T)t; }
template<typename T>
void  IntegerBasicType<T>::operator<<= (const Type &t) { value <<= (T)t; }


/* StringBasicType implementation */

StringBasicType::StringBasicType(void) { }

StringBasicType::StringBasicType(const char *v) : BasicType<std::string>(v) { }

StringBasicType::StringBasicType(const std::string &v) : BasicType<std::string>(v) { }

Type* StringBasicType::clone(void) const { return new StringBasicType(value); }

      StringBasicType::operator std::string() const { return value; }

void  StringBasicType::operator+=  (const Type &t) { value += (std::string)t; }


/* min_digits */

#define BASIC_TYPE_MIN_DIGITS(T) \
	template<> \
	int BasicType<T>::min_digits(T value, bool &need_sign) \
	{ \
		assert(is_integer()); \
		const bool flag = nl::is_signed ? true : false; \
		return BasicType<T>::min_digits0<flag, T>::go(value, need_sign); \
	}

BASIC_TYPE_MIN_DIGITS(bool)

BASIC_TYPE_MIN_DIGITS(int8)
BASIC_TYPE_MIN_DIGITS(int16)
BASIC_TYPE_MIN_DIGITS(int32)
BASIC_TYPE_MIN_DIGITS(int64)

BASIC_TYPE_MIN_DIGITS(uint8)
BASIC_TYPE_MIN_DIGITS(uint16)
BASIC_TYPE_MIN_DIGITS(uint32)
BASIC_TYPE_MIN_DIGITS(uint64)


/* descr */

#define BASIC_TYPE_DESCR(T) \
	template<> \
	const char* BasicType<T>::descr(void) { return #T; }

BASIC_TYPE_DESCR(bool)

BASIC_TYPE_DESCR(int8)
BASIC_TYPE_DESCR(int16)
BASIC_TYPE_DESCR(int32)
BASIC_TYPE_DESCR(int64)

BASIC_TYPE_DESCR(uint8)
BASIC_TYPE_DESCR(uint16)
BASIC_TYPE_DESCR(uint32)
BASIC_TYPE_DESCR(uint64)

BASIC_TYPE_DESCR(float32)
BASIC_TYPE_DESCR(float64)

template<>
const char* BasicType<std::string>::descr(void) { return "string"; }


/* creators */

#define BASIC_TYPE_CREATE(T) \
	template<> \
	Type* BasicType<T>::create(T value) { return new T ## Type(value); }

BASIC_TYPE_CREATE(bool)

BASIC_TYPE_CREATE(int8)
BASIC_TYPE_CREATE(int16)
BASIC_TYPE_CREATE(int32)
BASIC_TYPE_CREATE(int64)

BASIC_TYPE_CREATE(uint8)
BASIC_TYPE_CREATE(uint16)
BASIC_TYPE_CREATE(uint32)
BASIC_TYPE_CREATE(uint64)

BASIC_TYPE_CREATE(float32)
BASIC_TYPE_CREATE(float64)

template<>
Type* BasicType<std::string>::create(std::string value) { return new stringType(value); }


/* dump, print, scan specializations */

#define ROUND_8(bytes) (((bytes) + 7) & ~7)

#define BASIC_TYPE_DUMP(T, fmt) \
	template<> \
	void BasicType<T>::dump(lib::stream &f) const { f.printf(#T " [%" fmt "]", value); }

#define BASIC_TYPE_PRINT(T, fmt) \
	template<> \
	std::string BasicType<T>::print(void) const \
	{ \
		char buf[ROUND_8((nl::digits10)+4)]; \
		snprintf(buf, sizeof(buf), "%" fmt, value); \
		return std::string(buf); \
	}

/* boolType */

template<>
void BasicType<bool>::dump(lib::stream &f) const { f.printf("bool [%s]", value ? "true" : "false"); }
template<>
std::string BasicType<bool>::print(void) const
{
	char buf[64];
	snprintf(buf, sizeof(buf), "%i", (int) value);
	return std::string(buf);
}
template<>
void BasicType<bool>::scan(const std::string &str0)
{
	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;
	long tmp;

	errno = 0;
	tmp = strtol(s, &check, 0);

	if ((errno == ERANGE && (tmp == LONG_MAX || tmp == LONG_MIN))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (tmp < 0 || tmp > 1)
		throw range_error;

	value = tmp ? true : false;
}

/* int8Type */

BASIC_TYPE_DUMP(int8, PRId8)
BASIC_TYPE_PRINT(int8, PRId8)

/* int16Type */

BASIC_TYPE_DUMP(int16, PRId16)
BASIC_TYPE_PRINT(int16, PRId16)

/* int32Type */

BASIC_TYPE_DUMP(int32, PRId32)
BASIC_TYPE_PRINT(int32, PRId32)

/* int64Type */

BASIC_TYPE_DUMP(int64, PRId64)
BASIC_TYPE_PRINT(int64, PRId64)

/* uint8Type */

BASIC_TYPE_DUMP(uint8, PRIu8)
BASIC_TYPE_PRINT(uint8, PRIu8)

/* uint16Type */

BASIC_TYPE_DUMP(uint16, PRIu16)
BASIC_TYPE_PRINT(uint16, PRIu16)

/* uint32Type */

BASIC_TYPE_DUMP(uint32, PRIu32)
BASIC_TYPE_PRINT(uint32, PRIu32)

/* uint64Type */

BASIC_TYPE_DUMP(uint64, PRIu64)
BASIC_TYPE_PRINT(uint64, PRIu64)

/* float32Type */

BASIC_TYPE_DUMP(float32, "f")

template<>
std::string BasicType<float32>::print(void) const
{
	return lib::ftoa("%.6g", value);
}
template<>
void BasicType<float32>::scan(const std::string &str0)
{
	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;
	double tmp;

	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(isnan(tmp));     goto done; }
	if (lib::stricmp(s, "inf")  == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "+inf") == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "-inf") == 0) { tmp = -nl::infinity();  assert(isinf(tmp) < 0); goto done; }

	errno = 0;
	tmp = strtod(s, &check);

	if ((errno == ERANGE && (tmp == HUGE_VAL || tmp == 0))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

	if (lib::check_fit_float(tmp) != 0)
		throw range_error;

done:
	value = (float32)tmp;
}

/* float64Type */

BASIC_TYPE_DUMP(float64, "f")

template<>
std::string BasicType<float64>::print(void) const
{
	return lib::ftoa("%.8g", value);
}
template<>
void BasicType<float64>::scan(const std::string &str0)
{
	std::string str(lib::strtrim(str0));
	const char *s = str.c_str();
	char *check;
	double tmp;

	if (lib::stricmp(s, "nan")  == 0) { tmp =  nl::quiet_NaN(); assert(isnan(tmp));     goto done; }
	if (lib::stricmp(s, "inf")  == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "+inf") == 0) { tmp =  nl::infinity();  assert(isinf(tmp) > 0); goto done; }
	if (lib::stricmp(s, "-inf") == 0) { tmp = -nl::infinity();  assert(isinf(tmp) < 0); goto done; }

	errno = 0;
	tmp = strtod(s, &check);

	if ((errno == ERANGE && (tmp == HUGE_VAL || tmp == 0))
	    || (errno != 0 && tmp == 0))
		throw range_error;

	if (check == s || *check != '\0')
		throw illegal_character_error;

done:
	value = tmp;
}

/* stringType */

template<>
void BasicType<std::string>::dump(lib::stream &f) const { f.printf("string [\"%s\"]", value.c_str()); }
template<>
std::string BasicType<std::string>::print(void) const { return value; }
template<>
void BasicType<std::string>::scan(const std::string &str) { value = str; }

/* instantiate code */

namespace
{
	boolType    dummy00;

	int8Type    dummy01;
	int16Type   dummy02;
	int32Type   dummy03;
	int64Type   dummy04;

	uint8Type   dummy05;
	uint16Type  dummy06;
	uint32Type  dummy07;
	uint64Type  dummy08;

	float32Type dummy09;
	float64Type dummy10;

	stringType  dummy11;
}

} /* namespace stackmachine */
