
/* 
 * $Id: FrameInfo.cxx,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// own libs

// own header
#include "FrameInfo.h"


namespace stackmachine
{

void
FrameInfo::add(const lib::IDENTIFIER &id, const lib::pos &pos, unsigned int off)
{
	info_t info;

	info.id = id;
	info.pos = pos;
	info.off = off;

	infos.push_back(info);
}

int
FrameInfo::lookup(const lib::IDENTIFIER &id) const
{
	int off = -1;

	for (size_t i = 0, i_max = infos.size(); i < i_max; ++i)
	{
		if (infos[i].id == id)
		{
			off = infos[i].off;
			break;
		}
	}

	return off;
}

const lib::pos &
FrameInfo::getpos(const lib::IDENTIFIER &id) const
{
	for (size_t i = 0, i_max = infos.size(); i < i_max; ++i)
	{
		if (infos[i].id == id)
			return infos[i].pos;
	}

	assert(0);
	return lib::NoPosition;
}

void
FrameInfo::dump(lib::stream &f) const
{
	for (size_t i = 0, i_max = infos.size(); i < i_max; ++i)
		f.printf(" - %s(%u)", infos[i].id.c_str(), infos[i].off);
}

} /* namespace stackmachine */
