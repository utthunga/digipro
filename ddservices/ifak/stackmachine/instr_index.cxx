
/* 
 * $Id: instr_index.cxx,v 1.5 2009/08/24 11:26:41 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_index.h"

#include "AddrType.h"
#include "Context.h"


namespace stackmachine
{

instr_IXA::instr_IXA(uint32 s) : size(s) { }

void
instr_IXA::execute(class Context &c)
{
	uint32 addr = size;

	addr *= (uint32)(c.stack[c.SP--]);
	addr += (uint32)(c.stack[c.SP]);

	c.stack[c.SP] = new AddrType(addr);
}
void
instr_IXA::dump(lib::stream &f) const
{
	f.printf("\tIXA (%"PRIu32")", size);
	dump_pos(f);
}

} /* namespace stackmachine */
