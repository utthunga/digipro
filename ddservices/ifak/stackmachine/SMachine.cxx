
/* 
 * $Id: SMachine.cxx,v 1.3 2009/09/30 11:00:28 fna Exp $
 *
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

/*
 * see stackmachine.txt for implementation details
 */

// C stdlib
#include <assert.h>
#include <iostream>
// C++ stdlib

// my lib

// own header
#include "SMachine.h"

#include "Context.h"
#include "Instr.h"


namespace stackmachine
{

SMachine::SMachine(void)
: fixed(false)
{
	code.reserve(100);
}

SMachine::~SMachine(void)
{
	for (size_t i = 0, i_max = code.size(); i < i_max; ++i)
	{
		assert(code[i]);
		delete code[i];
	}
}

void
SMachine::dump(lib::stream &f) const
{
	for (size_t i = 0, i_max = code.size(); i < i_max; ++i)
	{
		f.printf("%5lu:", (unsigned long)i);

		assert(code[i]);
		code[i]->dump(f);
	}

	f.put('\n');
}

void
SMachine::emit(class InstrInterface *instr, const lib::pos &p)
{
	assert(instr);

	/* remember pos the instruction belongs too */
	instr->pos = p;

	/* program is fixated */
	if (fixed)
		throw std::logic_error("stackmachine: program relocated, can't emit new instruction");

	code.push_back(instr);

	/* check for label instruction */
	{
		instr_LAB *lab;

		lab = dynamic_cast<instr_LAB *>(instr);
		if (lab)
			labels.replace(lab->label(), (code.size() - 1));
	}

	/* check for halt instruction */
	{
		instr_HLT *hlt;

		hlt = dynamic_cast<instr_HLT *>(instr);
		if (hlt)
			/* HLT must be last instruction, run fixup */
			fixup();
	}
}


SMachine::label_t
SMachine::newlabel(void)
{
	return labels.install(0);
}

void
SMachine::fixup(void)
{
	for (size_t i = 0, i_max = code.size(); i < i_max; ++i)
	{
		instr_jump *jmp;

		jmp = dynamic_cast<instr_jump *>(code[i]);
		if (jmp)
			jmp->fixup(labels[jmp->label()]);
	}

	fixed = true;
}

void
SMachine::execute(void)
{
	class Context c(size());
	execute(c);
}

void
SMachine::execute(class Context &c)
{
	if (!fixed)
		throw std::logic_error("stackmachine: program not relocated, can't execute");

	c.steps = 0;

	on_pre_execute(c);

	for (;;)
	{
		/* do pre step actions */
		on_pre_step(c);

		/* break the execute loop if we ar no longer running */
		if (!c.run)
			break;

		class InstrInterface *i;

		/* fetch */
		i = code[c.PC++];

		/* decode & execute */
		i->execute(c);
	//	std::cout<<"inside SMachine::execute"<<std::endl;
		/* statistic the total number of instructions executed */
		++c.steps;

		/* break the execute loop if we ar no longer running */
		if (!c.run)
			break;
	}

	on_post_execute(c);
}

void SMachine::on_pre_execute(class Context &) { /* nothing */ }
void SMachine::on_post_execute(class Context &) { /* nothing */ }
void SMachine::on_pre_step(class Context &) { /* nothing */ }

} /* namespace stackmachine */
