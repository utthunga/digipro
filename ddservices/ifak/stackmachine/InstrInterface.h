
/* 
 * $Id: InstrInterface.h,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_InstrInterface_h
#define _stackmachine_InstrInterface_h

// C stdlib

// C++ stdlib

// my lib
#include "position.h"
#include "stream.h"

// own header


namespace stackmachine
{

/* forward declaration */
class Context;

/**
 * \internal
 * The instruction abstraction.
 * This is the virtual base type for all instructions of the stackmachine. The
 * stackmachine itself only need to know this interface. As a result of this
 * there can be any self defined instruction used by the stackmachine too.
 * E.g. the stackmachine can be very easily extended with new instructions at
 * any time.
 */
class InstrInterface
{
private:
	typedef class InstrInterface Self;

	/* no copy/assignment constructor semantic defined */
	InstrInterface(const Self &);
	Self& operator=(const Self &);

protected:
	void dump_pos(lib::stream &) const;

public:
	InstrInterface(void) { }
	virtual ~InstrInterface(void);

	/**
	 * run the InstrInterface.
	 * This virtual method must be implemented and run the InstrInterface on
	 * the given context.
	 */
	virtual void execute(class Context &) = 0;

	/**
	 * print short explanation.
	 * This virtual method must be implemented and should print a short
	 * explanation of the InstrInterface and the paramaters of it. It's used
	 * mainly for debugging to lookup up the content of a program.
	 */
	virtual void dump(lib::stream &) const = 0;

	/**
	 * Position this InstrInterface belongs too.
	 */
	lib::pos pos;
};

} /* namespace stackmachine */

#endif /* _stackmachine_InstrInterface_h */
