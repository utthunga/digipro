
/* 
 * $Id: instr_logic.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_logic_h
#define _stackmachine_instr_logic_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "Context.h"
#include "InstrInterface.h"


namespace stackmachine
{

/* logic instructions
 */

class instr_NOT : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_EQU : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_GEQ : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_LEQ : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_LES : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_GRT : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_NEQ : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_logic_h */
