
/* 
 * $Id: Instr.h,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_Instr_h
#define _stackmachine_Instr_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_arithmetic.h"
#include "instr_check.h"
#include "instr_convert.h"
#include "instr_index.h"
#include "instr_jump.h"
#include "instr_load.h"
#include "instr_logic.h"
#include "instr_other.h"
#include "instr_stack.h"
#include "instr_store.h"


#endif /* _stackmachine_Instr_h */
