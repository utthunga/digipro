
/* 
 * $Id: instr_convert.cxx,v 1.7 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_convert.h"

#include "BasicType.h"
#include "Context.h"


namespace stackmachine
{

template<typename T>
void
instr_CONV<T>::execute(class Context &c)
{
	c.stack[c.SP] = BasicType<T>::create(c.stack[c.SP]);
}

/* specialization for conversion to string
 * use print instead cast
 */
template<>
void
instr_CONV<std::string>::execute(class Context &c)
{
	c.stack[c.SP] = BasicType<std::string>::create(c.stack[c.SP].print());
}

template<typename T>
void
instr_CONV<T>::dump(lib::stream &f) const
{
	f.printf("\tCONV (%s)", BasicType<T>::descr());
	dump_pos(f);
}

/* instantiate code */

namespace
{
#define INSTR_CONV_INSTANCE(T, name) \
	instr_CONV_ ## T name

	INSTR_CONV_INSTANCE(bool,    dummy00);

	INSTR_CONV_INSTANCE(int8,    dummy01);
	INSTR_CONV_INSTANCE(int16,   dummy02);
	INSTR_CONV_INSTANCE(int32,   dummy03);
	INSTR_CONV_INSTANCE(int64,   dummy04);

	INSTR_CONV_INSTANCE(uint8,   dummy05);
	INSTR_CONV_INSTANCE(uint16,  dummy06);
	INSTR_CONV_INSTANCE(uint32,  dummy07);
	INSTR_CONV_INSTANCE(uint64,  dummy08);

	INSTR_CONV_INSTANCE(float32, dummy09);
	INSTR_CONV_INSTANCE(float64, dummy10);

	INSTR_CONV_INSTANCE(string,  dummy11);
}

} /* namespace stackmachine */
