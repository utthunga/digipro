
/* 
 * $Id: Context.h,v 1.3 2009/02/04 15:20:34 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_Context_h
#define _stackmachine_Context_h

// C stdlib

// C++ stdlib
#include <stdexcept>
#include <string>
#include <vector>

// my lib
#include "stream.h"

// own header
#include "Type.h"


namespace stackmachine
{

/**
 * \internal
 * The Context description for a program to execute.
 * This is the Context there a stackmachine can be run. It consist of a stack,
 * the stackpointer, framepointer, programcounter, run condition flag and so
 * on. The stack/framepointer and programcounter are special self defined
 * types with a restricted operation set to make usage much safer and easier.
 * The stack perform automatic storage allokation and destruktion and have
 * the correct copy and assignment semantic.
 */
class Context
{
public:
	/**
	 * \internal
	 * The stack element class.
	 * This is an element on the stack. It's just a wrapper class for the
	 * abstract stackmachine Type to realize the automatic storage
	 * management and the right copy and assignment semantic. This wrapper
	 * class ensure the correct memory allocation and deallocation on
	 * stack access and destruction.
	 */
	class stackelement
	{
	private:
		class noneType : public Type
		{
		public:
			noneType(void);

			/** \name Mandatory helper methods. */
			//! \{
			virtual Type *clone(void) const;
			virtual std::string type_descr(void) const;
			virtual void dump(lib::stream &f) const;
			//! \}

			/** \name Optional helper methods. */
			//! \{
			virtual std::string print(void) const;
			virtual void scan(const std::string &);
			//! \}

		private:
			Type *me;
		};

	protected:
		friend class noneType;
		/**
		 * The no content stack element.
		 * This is the default stack content of every stackelement.
		 * As optimization it only exist as one static object and thus
		 * need never be allocated or deallocated.
		 */
		static class noneType no_value;

	private:
		inline void freeup(void) { if (value != &no_value) delete value; }

	public:
		/** default constructor */
		stackelement(void) : value(&no_value) { }

		/** copy constructor */
		stackelement(const stackelement &v)
		{
			value = v.value->clone();
		}

		/** assignment */
		stackelement & operator=(const stackelement &v)
		{
			if (this != &v)
			{
				freeup();
				value = v.value->clone();
			}
			return *this;
		}
		/** assignment */
		stackelement & operator=(class Type *v)
		{
			freeup();
			value = v;

			return *this;
		}

		/** destructor */
		~stackelement(void) { freeup(); }

		/** \name Mandatory helper methods. */
		//! \{
		Type *clone(void) const { return value->clone(); }
		void dump(lib::stream &f) const { value->dump(f); }
		//! \}

		/** \name Optional helper methods. */
		//! \{
		std::string print(void) const { return value->print(); }
		void scan(const std::string &s) { value->scan(s); }
		//! \}

		/** \name cast operators */
		//! \{
		     operator bool        () const { return         (bool)(*value); }
		     operator int8        () const { return         (int8)(*value); }
		     operator int16       () const { return        (int16)(*value); }
		     operator int32       () const { return        (int32)(*value); }
		     operator int64       () const { return        (int64)(*value); }
		     operator uint8       () const { return        (uint8)(*value); }
		     operator uint16      () const { return       (uint16)(*value); }
		     operator uint32      () const { return       (uint32)(*value); }
		     operator uint64      () const { return       (uint64)(*value); }
		     operator float32     () const { return      (float32)(*value); }
		     operator float64     () const { return      (float64)(*value); }
		     operator std::string () const { return  (std::string)(*value); }

		     /** for binary/assignment operator automatic cast */
		     operator Type&       () const { return *value; } 
		//! \}

		/** \name unary operators */
		//! \{
		void operator+   (void) { +(*value); }
		void operator-   (void) { -(*value); }
		void operator~   (void) { ~(*value); }
		void operator!   (void) { !(*value); }
		//! \}

		/* \name binary operators */
		//! \{
		bool operator<   (const Type &v) const { return ((*value) <  v); }
		bool operator>   (const Type &v) const { return ((*value) >  v); }
		bool operator==  (const Type &v) const { return ((*value) == v); }
		bool operator!=  (const Type &v) const { return ((*value) != v); }
		bool operator<=  (const Type &v) const { return ((*value) <= v); }
		bool operator>=  (const Type &v) const { return ((*value) >= v); }
		//! \}

		/** \name assignment operators */
		//! \{
		void operator=   (const Type &v) { (*value) =   v; }
		void operator+=  (const Type &v) { (*value) +=  v; }
		void operator-=  (const Type &v) { (*value) -=  v; }
		void operator*=  (const Type &v) { (*value) *=  v; }
		void operator/=  (const Type &v) { (*value) /=  v; }
		void operator%=  (const Type &v) { (*value) %=  v; }
		void operator^=  (const Type &v) { (*value) ^=  v; }
		void operator&=  (const Type &v) { (*value) &=  v; }
		void operator|=  (const Type &v) { (*value) |=  v; }
		void operator>>= (const Type &v) { (*value) >>= v; }
		void operator<<= (const Type &v) { (*value) <<= v; }
		//! \}

	private:
		class Type *value;
	};

	/**
	 * \internal
	 * The stack.
	 * This is the stack of a Context. It's mainly a vector of
	 * stackelement objects. The automatic stack grow is implemented here
	 * too. Also any stack access through the overloaded [] operator is
	 * index checked.
	 */
	class stack
	{
	public:
		stack(void) : data(100), SP(0) { }

		size_t size(void) const { return data.size(); }

		void dump(lib::stream &f, size_t elements) const;

		stackelement & operator[](uint32 index)
		{
			if (index < data.size())
				return data[index];

			throw std::logic_error("stackmachine: stack overflow");
		}

		uint32 getSP(void) const { return SP; }
		void setSP(uint32 sp)
		{
			SP = sp;

			if (SP >= data.size())
				data.resize(SP + (data.size() / 3));
		}
		void push(uint32 offset)
		{
			setSP(SP + offset);
		}
		void pop(uint32 offset)
		{
			if (offset > SP)
				throw std::logic_error("stackmachine: stack undeflow");

			SP -= offset;
		}

	private:
		std::vector<class stackelement> data;
		uint32 SP;
	};

	/**
	 * \internal
	 * The programcounter.
	 * This is the programcounter of a Context. The programcounter cannot
	 * exceed the highest instruction. Also only the assignment and
	 * increment/decrement operations are allowd. The semantic is still
	 * fully C++ compatible.
	 */
	class pc
	{
	public:
		pc(unsigned int m) : maxPC(m), PC(0) { }

		void         operator= (unsigned int pc) { if (pc >= maxPC) throw_overflow();    PC  = pc; }
		void         operator+=(unsigned int pc) { if (PC+pc >= maxPC) throw_overflow(); PC += pc; }
		void         operator-=(unsigned int pc) { if (pc > PC) throw_underflow();       PC -= pc; }

		unsigned int operator++(int) { unsigned int tmp = PC; if (PC+1 > maxPC) throw_overflow(); PC++; return tmp; }
		unsigned int operator--(int) { unsigned int tmp = PC; if (PC == 0) throw_underflow();     PC--; return tmp; }

		unsigned int operator++(void) { if (PC+1 > maxPC) throw_overflow(); return ++PC; }
		unsigned int operator--(void) { if (PC == 0) throw_underflow();     return --PC; }

		             operator unsigned int() const { return PC; }

	private:
		const unsigned int maxPC;
		unsigned int PC;

		static void throw_overflow(void);
		static void throw_underflow(void);
	};

	/**
	 * \internal
	 * The stackpointer.
	 * This is the stackpointer of a Context. It support the assignment
	 * and increment/decrement operations. The semantic is fully C++
	 * compatible.
	 */
	class sp
	{
	public:
		sp(class stack &s) : stack(s) { }

		void         operator= (uint32 sp) { stack.setSP(sp); }
		void         operator+=(uint32 offset) { stack.push(offset); }
		void         operator-=(uint32 offset) { stack.pop(offset); }

		unsigned int operator++(int)  { uint32 tmp = stack.getSP(); stack.push(1); return tmp; }
		unsigned int operator--(int)  { uint32 tmp = stack.getSP(); stack.pop(1);  return tmp; }

		unsigned int operator++(void) { stack.push(1); return stack.getSP(); }
		unsigned int operator--(void) { stack.pop(1);  return stack.getSP(); }

		             operator uint32() const { return stack.getSP(); }

	private:
		class stack &stack;
	};

	/**
	 * \internal
	 * The framepointer.
	 * This is the framepointer of a Context. It only support assignment
	 * as the framepointer can only be read and written.
	 */
	class fp
	{
	public:
		fp(void) : FP(0) { }

		void         operator= (uint32 fp) { FP  = fp; }

		             operator uint32() const { return FP; }

	private:
		uint32 FP;
	};

private:
	typedef class Context Self;

	/* no copy/assignment constructor semantic defined */
	Context(const Self &);
	Self& operator=(const Self &);

public:
	/**
	 * The constructor.
	 * The maximum valid instruction number must be given so that the
	 * programcounter type can work correctly.
	 */
	Context(unsigned int maxPC)
	: run(true), PC(maxPC), SP(stack), steps(0)
	, env_data(NULL) { }

	void dump(lib::stream &f) const;

	/**
	 * The run condition flag.
	 * As long this flag is true the program is runnable and the virtual
	 * processor execute the next instruction.
	 */
	bool run;

	/**
	 * The stack of the Context.
	 * The stack automatically grows as long there is enough free memory
	 * available. If memory is low and the stack cannot be extended it
	 * throw a out_of_memory exception.
	 */
	class stack stack;

	/**
	 * Scratch stack element.
	 * This is a scratch stack element that can be used in instructions to
	 * perform various special things.
	 */
	class stackelement ret;

	class pc PC;
	class sp SP;
	class fp FP;

	/**
	 * Statistic counter.
	 * Count the total number of executed instructions since the program
	 * was started.
	 */
	size_t steps;

	/**
	 * User defined additional environment.
	 * \note This is not very good programming style.
	 */
	void *env_data;
};

} /* namespace stackmachine */

#endif /* _stackmachine_Context_h */
