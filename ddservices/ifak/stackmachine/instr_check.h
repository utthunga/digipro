
/* 
 * $Id: instr_check.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_check_h
#define _stackmachine_instr_check_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"
#include "Type.h"


namespace stackmachine
{

/* check instructions
 */

class instr_CHK : public InstrInterface
{
public:
	instr_CHK(uint32 s);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	uint32 size;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_check_h */
