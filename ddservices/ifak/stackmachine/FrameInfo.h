
/* 
 * $Id: FrameInfo.h,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_FrameInfo_h
#define _stackmachine_FrameInfo_h

// C stdlib

// C++ stdlib
#include <vector>

// my lib
#include "ident.h"
#include "position.h"
#include "stream.h"

// own header


namespace stackmachine
{

class FrameInfo
{
public:
	void add(const lib::IDENTIFIER &, const lib::pos &, unsigned int off);
	int lookup(const lib::IDENTIFIER &) const;
	const lib::pos &getpos(const lib::IDENTIFIER &) const;

	size_t size(void) const { return infos.size(); }
	lib::IDENTIFIER at(size_t index) const { return infos[index].id; }

	void dump(lib::stream &f) const;

private:
	struct info
	{
		lib::IDENTIFIER id;
		lib::pos pos;
		unsigned int off;
	};
	typedef struct info info_t;
	typedef std::vector<info_t> infos_t;

	infos_t infos;
};

} /* namespace stackmachine */

#endif /* _stackmachine_FrameInfo_h */
