
/* 
 * $Id: instr_convert.h,v 1.7 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_convert_h
#define _stackmachine_instr_convert_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"
#include "Type.h"


namespace stackmachine
{

/* convert instructions
 */

template<typename T>
class instr_CONV : public InstrInterface
{
public:
	instr_CONV(void) { }

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	char dummy;
};

typedef instr_CONV<bool>        instr_CONV_bool;
typedef instr_CONV<int8>        instr_CONV_int8;
typedef instr_CONV<int16>       instr_CONV_int16;
typedef instr_CONV<int32>       instr_CONV_int32;
typedef instr_CONV<int64>       instr_CONV_int64;
typedef instr_CONV<uint8>       instr_CONV_uint8;
typedef instr_CONV<uint16>      instr_CONV_uint16;
typedef instr_CONV<uint32>      instr_CONV_uint32;
typedef instr_CONV<uint64>      instr_CONV_uint64;
typedef instr_CONV<float32>     instr_CONV_float32;
typedef instr_CONV<float64>     instr_CONV_float64;
typedef instr_CONV<std::string> instr_CONV_string;

} /* namespace stackmachine */

#endif /* _stackmachine_instr_convert_h */
