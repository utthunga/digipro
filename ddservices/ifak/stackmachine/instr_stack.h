
/* 
 * $Id: instr_stack.h,v 1.8 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_stack_h
#define _stackmachine_instr_stack_h

// C stdlib

// C++ stdlib
#include <memory>

// my lib

// own header
#include "InstrInterface.h"
#include "FrameInfo.h"


namespace stackmachine
{

/* forward declarations */
class Context;

/* stack manipulation instructions
 */

class instr_LINK : public InstrInterface
{
public:
	instr_LINK(unsigned int s, std::auto_ptr<class FrameInfo> = std::auto_ptr<class FrameInfo>());

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int size;
	std::auto_ptr<class FrameInfo> info;
};

class instr_UNLK : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_UNWIND : public InstrInterface
{
public:
	instr_UNWIND(unsigned int f);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int frames;
};

class instr_POP : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_DUP : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_SWP : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_stack_h */
