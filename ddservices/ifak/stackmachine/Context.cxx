
/* 
 * $Id: Context.cxx,v 1.3 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <stdexcept>

// own header
#include "Context.h"


namespace stackmachine
{

Context::stackelement::noneType::noneType(void) : me(&no_value) { }

Type *
Context::stackelement::noneType::clone(void) const
{
	return me;
}

std::string
Context::stackelement::noneType::type_descr(void) const
{
	return "no type";
}

void
Context::stackelement::noneType::dump(lib::stream &f) const
{
	f.printf("no content");
}

std::string
Context::stackelement::noneType::print(void) const
{
	return std::string("no content");
}

void
Context::stackelement::noneType::scan(const std::string &)
{
	throw std::runtime_error("stackmachine: can't assign value to a NULL stack element");
}

class Context::stackelement::noneType Context::stackelement::no_value;

void
Context::stack::dump(lib::stream &f, size_t elements) const
{
	if (data.size() < elements)
		elements = data.size();

	for (size_t i = 0; i < elements; i++)
	{
		f.printf(" %5lu\t", (unsigned long)i);
		data[i].dump(f);
		f.printf("\n");
	}
}

void
Context::pc::throw_overflow(void)
{
	throw std::runtime_error("stackmachine: pc overflow");
}

void
Context::pc::throw_underflow(void)
{
	throw std::runtime_error("stackmachine: pc underflow");
}

void
Context::dump(lib::stream &f) const
{
	unsigned int pc = PC;
	unsigned int sp = SP;
	unsigned int fp = FP;

	f.printf("PC = %u, SP = %u, FP = %u\n", pc, sp, fp);
	f.put('\n');

	stack.dump(f, stack.size());
}

} /* namespace stackmachine */
