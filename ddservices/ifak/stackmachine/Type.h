
/* 
 * $Id: Type.h,v 1.4 2009/05/07 20:48:30 fna Exp $
 *
 * Copyright (C) 2008-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_Type_h
#define _stackmachine_Type_h

// C stdlib

// C++ stdlib
#include <string>

// my lib
#include "stream.h"
#include "sys_types.h"

// own header


namespace stackmachine
{

using sys::int8;
using sys::int16;
using sys::int32;
using sys::int64;

using sys::uint8;
using sys::uint16;
using sys::uint32;
using sys::uint64;

using sys::float32;
using sys::float64;

/**
 * \internal
 * The stackmachine data abstraction.
 * This is the abstract interface for any stackmachine data type that can be
 * handled on the stack. All supported operator methods are overloaded and
 * default to an exception in the case that an inherited class doesn't define
 * them. Du to the abstract interface there can be user defined data types
 * handled too.
 */
class Type
{
public:
	/** \name Constructor/Destructor. */
	//! \{
	virtual ~Type(void);
	//! \}

	/**
	 * \name Mandatory helper methods.
	 * Support routines that must be implemented by a concrete class
	 * specialization.
	 */
	//! \{
	/**
	 * Clone itself.
	 * This method must be implemented. It's used to clone a data type.
	 * This is neccessary on several places, especially for the automatic
	 * storage handling of the context::stack.
	 * \return A newly allocated Type object of the same type and
	 * with the same data attached.
	 * \exception out_of_memory Not enough memory to complete the
	 * operation.
	 */
	virtual Type* clone(void) const = 0;
	/**
	 * Type description.
	 * For debugging purposes it's sometimes nice to see a human readable
	 * description this Type belong too. This method must be implemented
	 * and should return a description about the object type.
	 */
	virtual std::string type_descr(void) const = 0;
	/**
	 * Dump description.
	 * For debugging purposes it's sometimes nice to see the content on
	 * the stack. This method must be implemented and should print out a
	 * short description about the object to the filestream f.
	 */
	virtual void dump(lib::stream &) const = 0;
	//! \}

	/**
	 * \name Optional helper methods.
	 * Support routines that can be implemented by a concrete class
	 * specialization.
	 */
	//! \{
	/**
	 * Print value.
	 * This should print the stack value and return a standard string.
	 * \return The value of this %Type.
	 * \exception RuntimeError(xxNoValue) - this value cannot be
	 * printed.
	 */
	virtual std::string print(void) const;
	/**
	 * Set value.
	 * This should scan the input string for a valid value for this
	 * stack value and assign it.
	 * \param str The input string.
	 * \exception RuntimeError(xxRange) - input value out of range.
	 * \exception RuntimeError(...) - other error happened.
	 */
	virtual void scan(const std::string &str);
	//! \}

	/** \name Cast operators. */
	//! \{
	virtual      operator bool() const;
	virtual      operator int8() const;
	virtual      operator int16() const;
	virtual      operator int32() const;
	virtual      operator int64() const;
	virtual      operator uint8() const;
	virtual      operator uint16() const;
	virtual      operator uint32() const;
	virtual      operator uint64() const;
	virtual      operator float32() const;
	virtual      operator float64() const;
	virtual      operator std::string() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	virtual void operator+   (void);
	virtual void operator-   (void);
	virtual void operator~   (void);
	virtual void operator!   (void);
	//! \}

	/** \name Binary operators. */
	//! \{
	virtual bool operator<   (const Type &) const;
	virtual bool operator>   (const Type &) const;
	virtual bool operator==  (const Type &) const;
	virtual bool operator!=  (const Type &) const;
	virtual bool operator<=  (const Type &) const;
	virtual bool operator>=  (const Type &) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator=   (const Type &);
	virtual void operator+=  (const Type &);
	virtual void operator-=  (const Type &);
	virtual void operator*=  (const Type &);
	virtual void operator/=  (const Type &);
	virtual void operator%=  (const Type &);
	virtual void operator^=  (const Type &);
	virtual void operator&=  (const Type &);
	virtual void operator|=  (const Type &);
	virtual void operator>>= (const Type &);
	virtual void operator<<= (const Type &);
	//! \}
};

} /* namespace stackmachine */

#endif /* _stackmachine_Type_h */
