
/* 
 * $Id: instr_index.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_index_h
#define _stackmachine_instr_index_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"
#include "Type.h"


namespace stackmachine
{

/* address calculation instructions
 */

class instr_IXA : public InstrInterface
{
public:
	instr_IXA(uint32 s);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	uint32 size;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_index_h */
