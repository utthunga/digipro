
/* 
 * $Id: instr_logic.cxx,v 1.4 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_logic.h"

#include "BasicType.h"
#include "Context.h"


namespace stackmachine
{

/*
 * EQU
 */

void
instr_EQU::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] == c.stack[c.SP+1]);
}
void
instr_EQU::dump(lib::stream &f) const
{
	f.put("\tEQU");
	dump_pos(f);
}

/*
 * GEQ
 */

void
instr_GEQ::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] >= c.stack[c.SP+1]);
}
void
instr_GEQ::dump(lib::stream &f) const
{
	f.put("\tGEQ");
	dump_pos(f);
}

/*
 * GRT
 */

void
instr_GRT::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] > c.stack[c.SP+1]);
}
void
instr_GRT::dump(lib::stream &f) const
{
	f.put("\tGRT");
	dump_pos(f);
}

/*
 * LEQ
 */

void
instr_LEQ::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] <= c.stack[c.SP+1]);
}
void
instr_LEQ::dump(lib::stream &f) const
{
	f.put("\tLEQ");
	dump_pos(f);
}

/*
 * LES
 */

void
instr_LES::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] < c.stack[c.SP+1]);
}
void
instr_LES::dump(lib::stream &f) const
{
	f.put("\tLES");
	dump_pos(f);
}

/*
 * NEQ
 */

void
instr_NEQ::execute(class Context &c)
{
	c.SP--;
	c.stack[c.SP] = new boolType(c.stack[c.SP] != c.stack[c.SP+1]);
}
void
instr_NEQ::dump(lib::stream &f) const
{
	f.put("\tNEQ");
	dump_pos(f);
}

/*
 * NOT
 */

void
instr_NOT::execute(class Context &c)
{
	!c.stack[c.SP];
}
void
instr_NOT::dump(lib::stream &f) const
{
	f.put("\tNOT");
	dump_pos(f);
}

} /* namespace stackmachine */
