
/* 
 * $Id: instr_stack.cxx,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_stack.h"

#include "AddrType.h"
#include "Context.h"
#include "FrameInfoType.h"


namespace stackmachine
{

/*
 * DUP
 */

void instr_DUP::execute(class Context &c)
{
	c.SP++;
	c.stack[c.SP] = c.stack[c.SP-1];
}
void instr_DUP::dump(lib::stream &f) const
{
	f.put("\tDUP");
	dump_pos(f);
}

/*
 * LINK
 */

instr_LINK::instr_LINK(unsigned int s, std::auto_ptr<class FrameInfo> i) : size(s), info(i) { }

void instr_LINK::execute(class Context &c)
{
	c.stack[++c.SP] = new FrameInfoType(c.FP, info.get());
	c.stack[++c.SP] = new AddrType(c.FP);
	c.FP = ++c.SP;
	c.SP += (size - 1);
}
void instr_LINK::dump(lib::stream &f) const
{
	f.printf("\tLINK %u", size);
	dump_pos(f);
}

/*
 * POP
 */

void instr_POP::execute(class Context &c)
{
	c.SP--;
}
void instr_POP::dump(lib::stream &f) const
{
	f.put("\tPOP");
	dump_pos(f);
}

/*
 * SWP
 */

void instr_SWP::execute(class Context &c)
{
	Context::stackelement x = c.stack[c.SP];

	c.stack[c.SP] = c.stack[c.SP-1];
	c.stack[c.SP-1] = x;
}
void instr_SWP::dump(lib::stream &f) const
{
	f.put("\tSWP");
	dump_pos(f);
}

/*
 * UNLK
 */

void instr_UNLK::execute(class Context &c)
{
	c.SP = c.FP - 1;
	c.FP = c.stack[c.SP--];
	c.SP--;
}
void instr_UNLK::dump(lib::stream &f) const
{
	f.put("\tUNLINK");
	dump_pos(f);
}

/*
 * UNWIND
 */

instr_UNWIND::instr_UNWIND(unsigned int f) : frames(f) { }

void instr_UNWIND::execute(class Context &c)
{
	unsigned int f = frames;

	while (f--)
	{
		c.SP = c.FP - 1;
		c.FP = c.stack[c.SP--];
		c.SP--;
	}
}
void instr_UNWIND::dump(lib::stream &f) const
{
	f.printf("\tUNWIND %u", frames);
	dump_pos(f);
}

} /* namespace stackmachine */
