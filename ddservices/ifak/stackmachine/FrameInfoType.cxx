
/* 
 * $Id: FrameInfoType.cxx,v 1.4 2009/08/24 11:26:41 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// own libs

// own header
#include "FrameInfoType.h"


namespace stackmachine
{

FrameInfoType::FrameInfoType(uint32 f, const class FrameInfo *i)
: frame(f), info(i)
{
}

Type *FrameInfoType::clone(void) const { return new FrameInfoType(frame, info); }

std::string FrameInfoType::type_descr(void) const { return "frame info"; }

void
FrameInfoType::dump(lib::stream &f) const
{
	f.printf("FrameInfo [%"PRIu32"]", frame);
	if (info)
		info->dump(f);
}

int
FrameInfoType::lookup(const lib::IDENTIFIER &id) const
{
	if (info)
		return info->lookup(id);

	return -1;
}

} /* namespace stackmachine */
