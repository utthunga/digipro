
/* 
 * $Id: instr_arithmetic.cxx,v 1.4 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_arithmetic.h"
#include "Context.h"


namespace stackmachine
{

/*
 * ADD
 */

void
instr_ADD::execute(class Context &c)
{
	c.stack[c.SP-1] += c.stack[c.SP];
	c.SP--;
}
void
instr_ADD::dump(lib::stream &f) const
{
	f.put("\tADD");
	dump_pos(f);
}

/*
 * AND
 */

void
instr_AND::execute(class Context &c)
{
	c.stack[c.SP-1] &= c.stack[c.SP];
	c.SP--;
}
void
instr_AND::dump(lib::stream &f) const
{
	f.put("\tAND");
	dump_pos(f);
}

/*
 * ASL
 */

void
instr_ASL::execute(class Context &c)
{
	c.stack[c.SP-1] <<= c.stack[c.SP];
	c.SP--;
}
void
instr_ASL::dump(lib::stream &f) const
{
	f.put("\tASL");
	dump_pos(f);
}

/*
 * ASR
 */

void
instr_ASR::execute(class Context &c)
{
	c.stack[c.SP-1] >>= c.stack[c.SP];
	c.SP--;
}
void
instr_ASR::dump(lib::stream &f) const
{
	f.put("\tASR");
	dump_pos(f);
}

/*
 * DIV
 */

void
instr_DIV::execute(class Context &c)
{
	c.stack[c.SP-1] /= c.stack[c.SP];
	c.SP--;
}
void
instr_DIV::dump(lib::stream &f) const
{
	f.put("\tDIV");
	dump_pos(f);
}

/*
 * INV
 */

void
instr_INV::execute(class Context &c)
{
	~c.stack[c.SP];
}
void
instr_INV::dump(lib::stream &f) const
{
	f.put("\tINV");
	dump_pos(f);
}

/*
 * MOD
 */

void
instr_MOD::execute(class Context &c)
{
	c.stack[c.SP-1] %= c.stack[c.SP];
	c.SP--;
}
void
instr_MOD::dump(lib::stream &f) const
{
	f.put("\tMOD");
	dump_pos(f);
}

/*
 * MUL
 */

void
instr_MUL::execute(class Context &c)
{
	c.stack[c.SP-1] *= c.stack[c.SP];
	c.SP--;
}
void
instr_MUL::dump(lib::stream &f) const
{
	f.put("\tMUL");
	dump_pos(f);
}

/*
 * NEG
 */

void
instr_NEG::execute(class Context &c)
{
	-c.stack[c.SP];
}
void
instr_NEG::dump(lib::stream &f) const
{
	f.put("\tNEG");
	dump_pos(f);
}

/*
 * OR
 */

void
instr_OR::execute(class Context &c)
{
	c.stack[c.SP-1] |= c.stack[c.SP];
	c.SP--;
}
void
instr_OR::dump(lib::stream &f) const
{
	f.put("\tOR");
	dump_pos(f);
}

/*
 * POS
 */

void
instr_POS::execute(class Context &c)
{
	+c.stack[c.SP];
}
void
instr_POS::dump(lib::stream &f) const
{
	f.put("\tPOS");
	dump_pos(f);
}

/*
 * SUB
 */

void
instr_SUB::execute(class Context &c)
{
	c.stack[c.SP-1] -= c.stack[c.SP];
	c.SP--;
}
void
instr_SUB::dump(lib::stream &f) const
{
	f.put("\tSUB");
	dump_pos(f);
}

/*
 * XOR
 */

void
instr_XOR::execute(class Context &c)
{
	c.stack[c.SP-1] ^= c.stack[c.SP];
	c.SP--;
}
void
instr_XOR::dump(lib::stream &f) const
{
	f.put("\tXOR");
	dump_pos(f);
}

} /* namespace stackmachine */
