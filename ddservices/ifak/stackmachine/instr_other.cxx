
/* 
 * $Id: instr_other.cxx,v 1.4 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_other.h"
#include "Context.h"


namespace stackmachine
{

/*
 * HLT
 */

void
instr_HLT::execute(class Context &c)
{
	c.run = false;
}
void
instr_HLT::dump(lib::stream &f) const
{
	f.put("\tHALT");
	dump_pos(f);
}

/*
 * LOAD_RET
 */

void
instr_LOAD_RET::execute(class Context &c)
{
	c.stack[++c.SP] = c.ret;
}
void
instr_LOAD_RET::dump(lib::stream &f) const
{
	f.put("\tLOAD_RET");
	dump_pos(f);
}

/*
 * STORE_RET
 */

void
instr_STORE_RET::execute(class Context &c)
{
	c.ret = c.stack[c.SP--];
}
void
instr_STORE_RET::dump(lib::stream &f) const
{
	f.put("\tSTORE_RET");
	dump_pos(f);
}

} /* namespace stackmachine */
