
/* 
 * $Id: instr_check.cxx,v 1.5 2009/08/24 11:26:41 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <stdexcept>

// my lib

// own header
#include "instr_check.h"
#include "Context.h"


namespace stackmachine
{

instr_CHK::instr_CHK(uint32 s) : size(s) { }

void
instr_CHK::execute(class Context &c)
{
	uint32 value = c.stack[c.SP];

	if (value >= size)
		throw std::out_of_range("range check error");
}
void
instr_CHK::dump(lib::stream &f) const
{
	f.printf("\tCHK (%"PRIu32")", size);
	dump_pos(f);
}

} /* namespace stackmachine */
