
/* 
 * $Id: SMachine.h,v 1.2 2009/09/30 11:00:28 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_SMachine_h
#define _stackmachine_SMachine_h

// C stdlib
#include <sys/types.h>

// C++ stdlib
#include <vector>

// my lib
#include "maptable.h"
#include "position.h"
#include "stream.h"

// own header
#include "Type.h"


/**
 * Namespace for the stackmachine implementation.
 * In this namespace are all things for the stackmachine defined. This are
 * mainly the SMachine manager class, the Context and Type classes and
 * the basic instruction set.
 */
namespace stackmachine
{

/* forward declarations */
class Context;
class InstrInterface;

/**
 * \internal
 * The main stackmachine class.
 * This class allow the creation and execution of virtual
 * processor programs.
 */
class SMachine
{
private:
	typedef class SMachine Self;

	/* no copy/assignment constructor semantic defined */
	SMachine(const Self &);
	Self& operator=(const Self &);

public:
	SMachine(void);
	virtual ~SMachine(void);

	typedef lib::maptable<uint32> labels_t;
	typedef labels_t::size_type label_t;

	/** dump the program to the descriptor f */
	void dump(lib::stream &) const;
	/** add a new instruction to the program */
	void emit(class InstrInterface *, const lib::pos & = lib::NoPosition);
	/** return a new unique label number */
	label_t newlabel(void);
	/** return the size of the program */
	size_t size(void) const { return code.size(); }

	/** create a new empty Context and execute the program on it */
	void execute(void);
	/** execute the program on the Context c */
	void execute(class Context &);

protected:
	virtual void on_pre_execute(class Context &);
	virtual void on_post_execute(class Context &);
	virtual void on_pre_step(class Context &);

	/** the instruction vector */
	std::vector<class InstrInterface *> code;

private:
	void fixup(void);

	/* label management */
	labels_t labels;
	bool fixed;
};

} /* namespace stackmachine */

#endif /* _stackmachine_SMachine_h */
