
/* 
 * $Id: FrameInfoType.h,v 1.3 2009/02/04 15:20:34 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_FrameInfoType_h
#define _stackmachine_FrameInfoType_h

// C stdlib

// C++ stdlib

// my lib
#include "ident.h"
#include "stream.h"

// own header
#include "Type.h"
#include "FrameInfo.h"


namespace stackmachine
{

/**
 */
class FrameInfoType : public Type
{
public:
	/** \name Constructor/Destructor. */
	//! \{
	FrameInfoType(uint32 f, const class FrameInfo *i);
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	virtual std::string type_descr(void) const;
	virtual void dump(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	//! \}

	/** \name Special things. */
	//! \{
	int lookup(const lib::IDENTIFIER &id) const;
	const class FrameInfo *get(void) const { return info; }
	//! \}

private:
	uint32 frame;
	const class FrameInfo *info;
};

} /* namespace stackmachine */

#endif /* _stackmachine_FrameInfoType_h */
