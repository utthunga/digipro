
/* 
 * $Id: InstrInterface.cxx,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"


namespace stackmachine
{

InstrInterface::~InstrInterface(void) { }

void
InstrInterface::dump_pos(lib::stream &f) const
{
	if (0 && pos != lib::NoPosition)
	{
		f.printf("\t\t\t[%lu,%lu] %s",
			 pos.line, pos.column, pos.file.c_str());
	}
	f.put('\n');
}

} /* namespace stackmachine */
