
/* 
 * $Id: BasicType.h,v 1.9 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_BasicType_h
#define _stackmachine_BasicType_h

// C stdlib

// C++ stdlib
#include <limits>

// my lib

// own header
#include "Type.h"


namespace stackmachine
{

template<typename T>
class BasicType : public Type
{
private:
	typedef BasicType<T> Self;

public:
	typedef std::numeric_limits<T> nl;
	typedef T cpptype;

	BasicType(void) : value(T()) { }
	BasicType(const Self &v) : value(v.value) { }
	BasicType(const T &v) : value(v) { }

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	virtual std::string type_descr(void) const;
	virtual void dump(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	virtual std::string print(void) const;
	virtual void scan(const std::string &);
	//! \}

	/** \name Binary operators. */
	//! \{
	virtual bool operator==  (const Type &) const;
	virtual bool operator!=  (const Type &) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator=   (const Type &);
	virtual void operator=   (const Self &);
	//! \}

	/** \name Static helper methods. */
	//! \{
	static const char* descr(void);
	static Type* create(T = T());
	static int min_digits(T, bool &need_sign);
	//! \}

	/** \name Type specific limits. */
	//! \{
	static bool is_integer(void) throw() { return nl::is_integer; }
	static bool is_signed(void) throw() { return nl::is_signed; }
	static int digits(void) throw() { return nl::digits; }
	static T max_value(void) throw() { return nl::max(); }
	static T min_value(void) throw() { return nl::min(); }
	//! \}

protected:
	/** The encapsuled value. */
	T value;


	/** Helper for scan. */
	template<int, typename T1> class scan0 { };

	/* Specializations for scan0. */
	template<typename T1> class scan0<0, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<1, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<2, T1> { public: static T go(const std::string &); };
	template<typename T1> class scan0<3, T1> { public: static T go(const std::string &); };


	/** Helper for min_digits. */
	template<bool, typename T1> class min_digits0 { };

	/* Specializations for min_digits0. */
	template<typename T1> class min_digits0<false, T1> { public: static int go(T1, bool &); };
	template<typename T1> class min_digits0<true, T1>  { public: static int go(T1, bool &); };


	/** Helper for to_basic_int */
	template<int, typename T_dst, typename T_src> class to_basic_int0 { };

	/* dst - unsigned, src - unsigned */
	template<typename T_dst, typename T_src>
	class to_basic_int0<0, T_dst, T_src> { public: static T_dst go(T_src); };
	/* dst - signed, src - unsigned */
	template<typename T_dst, typename T_src>
	class to_basic_int0<1, T_dst, T_src> { public: static T_dst go(T_src); };
	/* dst - unsigned, src - signed */
	template<typename T_dst, typename T_src>
	class to_basic_int0<2, T_dst, T_src> { public: static T_dst go(T_src); };
	/* dst - signed, src - signed */
	template<typename T_dst, typename T_src>
	class to_basic_int0<3, T_dst, T_src> { public: static T_dst go(T_src); };

	template<typename T1> T1 to_basic_int(void) const;
	template<typename T1> T1 to_basic_float(void) const;
};

class BoolBasicType : public BasicType<bool>
{
public:
	BoolBasicType(void);
	BoolBasicType(bool);

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	virtual      operator bool() const;
	virtual      operator int8() const;
	virtual      operator int16() const;
	virtual      operator int32() const;
	virtual      operator int64() const;
	virtual      operator uint8() const;
	virtual      operator uint16() const;
	virtual      operator uint32() const;
	virtual      operator uint64() const;
	virtual      operator float32() const;
	virtual      operator float64() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	virtual void operator!   (void);
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator^=  (const Type &);
	virtual void operator&=  (const Type &);
	virtual void operator|=  (const Type &);
	//! \}

protected:
	using BasicType<bool>::value;
};

template<typename T>
class ValueBasicType : public BasicType<T>
{
public:
	ValueBasicType(void) { }
	ValueBasicType(T v) : BasicType<T>(v) { }

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	virtual      operator bool() const;
	virtual      operator int8() const;
	virtual      operator int16() const;
	virtual      operator int32() const;
	virtual      operator int64() const;
	virtual      operator uint8() const;
	virtual      operator uint16() const;
	virtual      operator uint32() const;
	virtual      operator uint64() const;
	virtual      operator float32() const;
	virtual      operator float64() const;
	//! \}

	/** \name Unary operators. */
	//! \{
	virtual void operator+   (void);
	virtual void operator-   (void);
	virtual void operator!   (void);
	//! \}

	/** \name Binary operators. */
	//! \{
	virtual bool operator<   (const Type &) const;
	virtual bool operator>   (const Type &) const;
	virtual bool operator<=  (const Type &) const;
	virtual bool operator>=  (const Type &) const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator+=  (const Type &);
	virtual void operator-=  (const Type &);
	virtual void operator*=  (const Type &);
	virtual void operator/=  (const Type &);
	//! \}

protected:
	using BasicType<T>::value;
};

template<typename T>
class IntegerBasicType : public ValueBasicType<T>
{
public:
	IntegerBasicType(void) { }
	IntegerBasicType(T v) : ValueBasicType<T>(v) { }

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	//! \}

	/** \name Unary operators. */
	//! \{
	virtual void operator~   (void);
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator%=  (const Type &);
	virtual void operator^=  (const Type &);
	virtual void operator&=  (const Type &);
	virtual void operator|=  (const Type &);
	virtual void operator>>= (const Type &);
	virtual void operator<<= (const Type &);
	//! \}

protected:
	using ValueBasicType<T>::value;
};

class StringBasicType : public BasicType<std::string>
{
public:
	StringBasicType(void);
	StringBasicType(const char *);
	StringBasicType(const std::string &);

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type* clone(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	virtual      operator std::string() const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator+=  (const Type &);
	//! \}

protected:
	using BasicType<std::string>::value;
};

typedef class BoolBasicType            boolType;

typedef class IntegerBasicType<int8>   int8Type;
typedef class IntegerBasicType<int16>  int16Type;
typedef class IntegerBasicType<int32>  int32Type;
typedef class IntegerBasicType<int64>  int64Type;

typedef class IntegerBasicType<uint8>  uint8Type;
typedef class IntegerBasicType<uint16> uint16Type;
typedef class IntegerBasicType<uint32> uint32Type;
typedef class IntegerBasicType<uint64> uint64Type;

typedef class ValueBasicType<float32>  float32Type;
typedef class ValueBasicType<float64>  float64Type;

typedef class StringBasicType          stringType;

} /* namespace stackmachine */

#endif /* _stackmachine_BasicType_h */
