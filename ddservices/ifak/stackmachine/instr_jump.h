
/* 
 * $Id: instr_jump.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_jump_h
#define _stackmachine_instr_jump_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"
#include "SMachine.h"


namespace stackmachine
{

/* jump instructions
 */

class instr_jump : public InstrInterface
{
public:
	instr_jump(SMachine::label_t l);

	SMachine::label_t label(void) const;
	void fixup(uint32 p);

protected:
	SMachine::label_t L;
	uint32 P;
};

class instr_JMP : public instr_jump
{
public:
	instr_JMP(SMachine::label_t l);

	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_TJP : public instr_jump
{
public:
	instr_TJP(SMachine::label_t l);

	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_FJP : public instr_jump
{
public:
	instr_FJP(SMachine::label_t l);

	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_LAB : public InstrInterface
{
public:
	instr_LAB(SMachine::label_t l);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

	SMachine::label_t label(void) const;

private:
	uint32 L;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_jump_h */
