
/* 
 * $Id: AddrType.h,v 1.2 2008/11/13 00:59:37 fna Exp $
 *
 * Copyright (C) 2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_AddrType_h
#define _stackmachine_AddrType_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "Type.h"


namespace stackmachine
{

/**
 */
class AddrType : public Type
{
public:
	/** \name Constructor/Destructor. */
	//! \{
	AddrType(uint32 v) : value(v) { }
	//! \}

	/** \name Mandatory helper methods. */
	//! \{
	virtual Type *clone(void) const;
	virtual std::string type_descr(void) const;
	virtual void dump(lib::stream &f) const;
	//! \}

	/** \name Optional helper methods. */
	//! \{
	virtual std::string print(void) const;
	//! \}

	/** \name Cast operators. */
	//! \{
	virtual      operator uint32() const;
	//! \}

	/** \name Assignment operators. */
	//! \{
	virtual void operator=   (const Type &d);
	virtual void operator+=  (const Type &d);
	virtual void operator-=  (const Type &d);
	virtual void operator*=  (const Type &d);
	virtual void operator/=  (const Type &d);
	//! \}

private:
	uint32 value;
};

} /* namespace stackmachine */

#endif /* _stackmachine_AddrType_h */
