#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    Compiler Options #######################################################
#############################################################################
add_compile_options(${FLUKE_IGNORE_COMPILE_WARNINGS})

#############################################################################
#    include the source files ###############################################
#############################################################################
set (STACKMACHINE_SRC   ${CMAKE_CURRENT_SOURCE_DIR}/AddrType.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/FrameInfo.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_check.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/InstrInterface.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_logic.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_store.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/BasicType.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/FrameInfoType.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_convert.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_jump.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_other.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/SMachine.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/Context.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_arithmetic.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_index.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_load.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/instr_stack.cxx
                        ${CMAKE_CURRENT_SOURCE_DIR}/Type.cxx)

#############################################################################
#    include the header files ###############################################
#############################################################################
include_directories (${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/edd
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/osservice
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/support
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/sys
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/profibus
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/stackmachine
                     ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/ucpp
                     ${CMAKE_PROSTAR_SRC_DIR}/clogger)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(stackmachine OBJECT ${STACKMACHINE_SRC})
