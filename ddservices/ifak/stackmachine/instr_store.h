
/* 
 * $Id: instr_store.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_store_h
#define _stackmachine_instr_store_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"


namespace stackmachine
{

/* store instructions
 */

class instr_STI : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_STORE : public InstrInterface
{
public:
	instr_STORE(unsigned int f, unsigned int o);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int frame;
	unsigned int offset;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_store_h */
