
/* 
 * $Id: instr_store.cxx,v 1.4 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_store.h"
#include "Context.h"


namespace stackmachine
{

/*
 * STI
 */

void
instr_STI::execute(class Context &c)
{
	c.stack[c.stack[c.SP-1]] = c.stack[c.SP];
	c.SP--;
}
void
instr_STI::dump(lib::stream &f) const
{
	f.put("\tSTI");
	dump_pos(f);
}

/*
 * STORE
 */

instr_STORE::instr_STORE(unsigned int f, unsigned int o) : frame(f), offset(o) { }

void
instr_STORE::execute(class Context &c)
{
	uint32 fp = c.FP;
	int level = frame;

	/* first search the right frame pointer */
	while (level > 0)
	{
		fp = c.stack[fp-1];
		level--;
	}

	c.stack[fp + offset] = c.stack[c.SP--];
}
void
instr_STORE::dump(lib::stream &f) const
{
	f.printf("\tSTORE [%u] %u", frame, offset);
	dump_pos(f);
}

} /* namespace stackmachine */
