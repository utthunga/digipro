
/* 
 * $Id: AddrType.cxx,v 1.4 2009/08/24 11:26:41 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib
#include "port.h"

// own header
#include "AddrType.h"


namespace stackmachine
{

Type* AddrType::clone(void) const { return new AddrType(value); }
std::string AddrType::type_descr(void) const { return "addr"; }
void AddrType::dump(lib::stream &f) const { f.printf("addr [%"PRIu32"]", value); }

std::string AddrType::print(void) const
{
	char buf[32];
	snprintf(buf, sizeof(buf), "%"PRIu32, value);
	return std::string(buf);
} 

AddrType::operator uint32() const { return value; }

void AddrType::operator=   (const Type &t) { value =   ((uint32) t); }
void AddrType::operator+=  (const Type &t) { value +=  ((uint32) t); }
void AddrType::operator-=  (const Type &t) { value -=  ((uint32) t); }
void AddrType::operator*=  (const Type &t) { value *=  ((uint32) t); }
void AddrType::operator/=  (const Type &t) { value /=  ((uint32) t); }

} /* namespace stackmachine */
