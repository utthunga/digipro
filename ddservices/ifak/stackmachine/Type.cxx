
/* 
 * $Id: Type.cxx,v 1.1 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib
#include <stdexcept>

// my lib

// own header
#include "Type.h"


namespace stackmachine
{

static std::string
errmsg(const char *msg)
{
	std::string ret;
	ret.reserve(100);

	ret += "stackmachine: operation ";
	ret += msg;
	ret += " not available";

	return ret;
}

Type::~Type(void) { }

std::string Type::print(void) const { throw std::runtime_error(errmsg("print")); }
void Type::scan(const std::string &) { throw std::runtime_error(errmsg("scan")); }

#define TYPE_CAST_OPERATOR(T) \
	Type::operator T() const { throw std::runtime_error(errmsg(#T "()")); }

TYPE_CAST_OPERATOR(bool)
TYPE_CAST_OPERATOR(int8)
TYPE_CAST_OPERATOR(int16)
TYPE_CAST_OPERATOR(int32)
TYPE_CAST_OPERATOR(int64)
TYPE_CAST_OPERATOR(uint8)
TYPE_CAST_OPERATOR(uint16)
TYPE_CAST_OPERATOR(uint32)
TYPE_CAST_OPERATOR(uint64)
TYPE_CAST_OPERATOR(float32)
TYPE_CAST_OPERATOR(float64)
TYPE_CAST_OPERATOR(std::string)

#define TYPE_UNARY_OPERATOR(T) \
	void Type::operator T (void) { throw std::runtime_error(errmsg(#T)); }

TYPE_UNARY_OPERATOR(+)
TYPE_UNARY_OPERATOR(-)
TYPE_UNARY_OPERATOR(~)
TYPE_UNARY_OPERATOR(!)

#define TYPE_BINARY_OPERATOR(T) \
	bool Type::operator T   (const Type &) const { throw std::runtime_error(errmsg(#T)); }

TYPE_BINARY_OPERATOR(<)
TYPE_BINARY_OPERATOR(>)
TYPE_BINARY_OPERATOR(==)
TYPE_BINARY_OPERATOR(!=)
TYPE_BINARY_OPERATOR(<=)
TYPE_BINARY_OPERATOR(>=)

#define TYPE_ASSIGNMENT_OPERATOR(T) \
	void Type::operator T   (const Type &) { throw std::runtime_error(errmsg(#T)); }

TYPE_ASSIGNMENT_OPERATOR(=)
TYPE_ASSIGNMENT_OPERATOR(+=)
TYPE_ASSIGNMENT_OPERATOR(-=)
TYPE_ASSIGNMENT_OPERATOR(*=)
TYPE_ASSIGNMENT_OPERATOR(/=)
TYPE_ASSIGNMENT_OPERATOR(%=)
TYPE_ASSIGNMENT_OPERATOR(^=)
TYPE_ASSIGNMENT_OPERATOR(&=)
TYPE_ASSIGNMENT_OPERATOR(|=)
TYPE_ASSIGNMENT_OPERATOR(>>=)
TYPE_ASSIGNMENT_OPERATOR(<<=)

} /* namespace stackmachine */
