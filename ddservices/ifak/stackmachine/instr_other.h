
/* 
 * $Id: instr_other.h,v 1.5 2008/09/11 01:08:16 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_other_h
#define _stackmachine_instr_other_h

// C stdlib

// C++ stdlib

// my lib

// own header
#include "InstrInterface.h"


namespace stackmachine
{

/* other instructions
 */

class instr_STORE_RET : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_LOAD_RET : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_HLT : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

} /* namespace stackmachine */

#endif /* _stackmachine_instr_other_h */
