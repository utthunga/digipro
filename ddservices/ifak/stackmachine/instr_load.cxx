
/* 
 * $Id: instr_load.cxx,v 1.8 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib
#include <assert.h>

// C++ stdlib

// my lib

// own header
#include "instr_load.h"

#include "AddrType.h"
#include "BasicType.h"
#include "Context.h"


namespace stackmachine
{

/*
 * LDA
 */

instr_LDA::instr_LDA(unsigned int f, unsigned int o) : frame(f), offset(o) { }

void
instr_LDA::execute(class Context &c)
{
	uint32 fp = c.FP;
	int level = frame;

	/* first search the right frame pointer */
	while (level > 0)
	{
		fp = c.stack[fp-1];
		level--;
	}

	c.stack[++c.SP] = new AddrType(fp + offset);
}
void
instr_LDA::dump(lib::stream &f) const
{
	f.printf("\tLDA %i %i", frame, offset);
	dump_pos(f);
}

/*
 * LDI
 */

void
instr_LDI::execute(class Context &c)
{
	c.stack[c.SP] = c.stack[c.stack[c.SP]];
}
void
instr_LDI::dump(lib::stream &f) const
{
	f.put("\tLDI");
	dump_pos(f);
}

/*
 * LOAD
 */

instr_LOAD::instr_LOAD(unsigned int f, unsigned int o) : frame(f), offset(o) { }

void
instr_LOAD::execute(class Context &c)
{
	uint32 fp = c.FP;
	int level = frame;

	/* first search the right frame pointer */
	while (level > 0)
	{
		fp = c.stack[fp-1];
		level--;
	}

	c.stack[++c.SP] = c.stack[fp + offset];
}
void
instr_LOAD::dump(lib::stream &f) const
{
	f.printf("\tLOAD [%u] %u", frame, offset);
	dump_pos(f);
}

/*
 * LDC Type
 */

instr_LDC_Type::instr_LDC_Type(std::auto_ptr<Type> v)
: value(v)
{
	assert(value.get());
}

void instr_LDC_Type::execute(class Context &c) { c.stack[++c.SP] = value->clone(); }

void instr_LDC_Type::dump(lib::stream &f) const { f.put("\tLDC "); value->dump(f); dump_pos(f); }

/*
 * LDC
 */

template<typename T>
void instr_LDC<T>::execute(class Context &c) { c.stack[++c.SP] = BasicType<T>::create(value); }

template<typename T>
void instr_LDC<T>::dump(lib::stream &f) const { f.put("\tLDC "); BasicType<T>(value).dump(f); dump_pos(f); }

/* instantiate code */

namespace
{
#define INSTR_LDC_INSTANCE(T, name, val) \
	instr_LDC_ ## T name(val)

	INSTR_LDC_INSTANCE(bool,    dummy00, false);

	INSTR_LDC_INSTANCE(int8,    dummy01, 0);
	INSTR_LDC_INSTANCE(int16,   dummy02, 0);
	INSTR_LDC_INSTANCE(int32,   dummy03, 0);
	INSTR_LDC_INSTANCE(int64,   dummy04, 0);

	INSTR_LDC_INSTANCE(uint8,   dummy05, 0);
	INSTR_LDC_INSTANCE(uint16,  dummy06, 0);
	INSTR_LDC_INSTANCE(uint32,  dummy07, 0);
	INSTR_LDC_INSTANCE(uint64,  dummy08, 0);

	INSTR_LDC_INSTANCE(float32, dummy09, 0.0);
	INSTR_LDC_INSTANCE(float64, dummy10, 0.0);

	INSTR_LDC_INSTANCE(string,  dummy11, std::string(""));
}

} /* namespace stackmachine */
