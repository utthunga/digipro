
/* 
 * $Id: instr_jump.cxx,v 1.9 2009/08/24 06:20:06 fna Exp $
 *
 * Copyright (C) 2001-2009 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

// C stdlib

// C++ stdlib

// my lib

// own header
#include "instr_jump.h"
#include "Context.h"


namespace stackmachine
{

instr_jump::instr_jump(SMachine::label_t l) : L(l), P() { }

SMachine::label_t
instr_jump::label(void) const
{
	return L;
}
void
instr_jump::fixup(uint32 p)
{
	P = p;
}

/*
 * JMP
 */

instr_JMP::instr_JMP(SMachine::label_t l) : instr_jump(l) { }

void
instr_JMP::execute(class Context &c)
{
	c.PC = P;
}
void
instr_JMP::dump(lib::stream &f) const
{
	f.printf("\tJMP %"PRIu32" (label %lu)", P, (unsigned long)L);
	dump_pos(f);
}

/*
 * TJP
 */

instr_TJP::instr_TJP(SMachine::label_t l) : instr_jump(l) { }

void
instr_TJP::execute(class Context &c)
{
	if (((bool) c.stack[c.SP--]))
		c.PC = P;
}
void
instr_TJP::dump(lib::stream &f) const
{
	f.printf("\tTJP %"PRIu32" (label %lu)", P, (unsigned long)L);
	dump_pos(f);
}

/*
 * FJP
 */

instr_FJP::instr_FJP(SMachine::label_t l) : instr_jump(l) { }

void
instr_FJP::execute(class Context &c)
{
	if (!((bool) c.stack[c.SP--]))
		c.PC = P;
}
void
instr_FJP::dump(lib::stream &f) const
{
	f.printf("\tFJP %"PRIu32" (label %lu)", P, (unsigned long)L);
	dump_pos(f);
}

/*
 * LAB
 */

instr_LAB::instr_LAB(SMachine::label_t l) : L(l) { }

void
instr_LAB::execute(class Context &c)
{
}
void
instr_LAB::dump(lib::stream &f) const
{
	f.printf("\tLAB %lu", (unsigned long)L);
	dump_pos(f);
}
SMachine::label_t
instr_LAB::label(void) const
{
	return L;
}

} /* namespace stackmachine */
