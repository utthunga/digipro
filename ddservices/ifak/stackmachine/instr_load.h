
/* 
 * $Id: instr_load.h,v 1.9 2009/08/05 10:37:54 fna Exp $
 *
 * Copyright (C) 2001-2008 Frank Naumann <frank.naumann@ifak-md.de>
 * ifak e.V. Magdeburg
 * 
 */

#ifndef _stackmachine_instr_load_h
#define _stackmachine_instr_load_h

// C stdlib

// C++ stdlib
#include <memory>
#include <string>

// my lib

// own header
#include "InstrInterface.h"
#include "Type.h"


namespace stackmachine
{

/* load instructions
 */

class instr_LDA : public InstrInterface
{
public:
	instr_LDA(unsigned int f, unsigned int o);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int frame;
	unsigned int offset;
};

class instr_LDI : public InstrInterface
{
public:
	void execute(class Context &c);
	void dump(lib::stream &f) const;
};

class instr_LOAD : public InstrInterface
{
public:
	instr_LOAD(unsigned int f, unsigned int o);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	unsigned int frame; /* relative frame */
	unsigned int offset;
};

/*
 * generic load constant
 */

class instr_LDC_Type : public InstrInterface
{
public:
	instr_LDC_Type(std::auto_ptr<Type>);

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	std::auto_ptr<Type> value;
};

/*
 * load constant values
 */

template<typename T>
class instr_LDC : public InstrInterface
{
public:
	instr_LDC(T v = T()) : value(v) { }

	void execute(class Context &c);
	void dump(lib::stream &f) const;

private:
	T value;
};

typedef instr_LDC<bool>        instr_LDC_bool;
typedef instr_LDC<int8>        instr_LDC_int8;
typedef instr_LDC<int16>       instr_LDC_int16;
typedef instr_LDC<int32>       instr_LDC_int32;
typedef instr_LDC<int64>       instr_LDC_int64;
typedef instr_LDC<uint8>       instr_LDC_uint8;
typedef instr_LDC<uint16>      instr_LDC_uint16;
typedef instr_LDC<uint32>      instr_LDC_uint32;
typedef instr_LDC<uint64>      instr_LDC_uint64;
typedef instr_LDC<float32>     instr_LDC_float32;
typedef instr_LDC<float64>     instr_LDC_float64;
typedef instr_LDC<std::string> instr_LDC_string;

} /* namespace stackmachine */

#endif /* _stackmachine_instr_load_h */
