/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - Fluke IDC
*/

/** @file
    IFakInterpreter class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "IFakInterpreter.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    IFakInterpreter class instance initialization
*/
IFakInterpreter::IFakInterpreter()
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
IFakInterpreter::~IFakInterpreter()
{
}

/*  ----------------------------------------------------------------------------
    This function is used to clear edd tree
*/
void IFakInterpreter::clearTree()
{
    // Clears all edd interpreter tree
    clear();
}

/*  ----------------------------------------------------------------------------
    This function is used to get error count
*/
int IFakInterpreter::getErrorCount()
{
    // Clears all edd interpreter tree
    return err_->get_errCount();
}
