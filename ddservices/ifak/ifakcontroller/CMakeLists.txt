#############################################################################
#    Copyright (c) 2017,2018 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    compiler options #######################################################
#############################################################################
add_compile_options(${FLK_COMPILE_OPTIONS})

# Enable code coverage for debug build
if(OPTION_BUILD_COVERAGE)
    APPEND_COVERAGE_COMPILER_FLAGS(${CMAKE_CURRENT_SOURCE_DIR})
endif()

#############################################################################
#    include the source files ###############################################
#############################################################################
set (IFAKCONTROLLER_SRC	${CMAKE_CURRENT_SOURCE_DIR}/IFakController.cpp
                        ${CMAKE_CURRENT_SOURCE_DIR}/IFakInterpreter.cpp)

# Add .h files to clang and doxygen tools
set (IFAKCONTROLLER_HDR ${CMAKE_CURRENT_SOURCE_DIR}/include/IFakController.h
                        ${CMAKE_CURRENT_SOURCE_DIR}/include/IFakInterpreter.h)

#############################################################################
#    include the header files ###############################################
#############################################################################

set (IFAK_CONTROLLER_INCLUDE_PATH ${CMAKE_PROSTAR_SRC_DIR}/communicator/include
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/ifakcontroller/include
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/edd
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/support
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/stackmachine
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/sys
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/osservice
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/profibus
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/drivers/softing
                                ${CMAKE_PROSTAR_SRC_DIR}/ddservices/ifak/upp
                                ${CMAKE_PROSTAR_SRC_DIR}/csl/connectionmanager/include
                                ${CMAKE_PROSTAR_SRC_DIR}/csl/parametercache/include
                                ${CMAKE_PROSTAR_SRC_DIR}/adaptor/include
                                ${CMAKE_PROSTAR_SRC_DIR}/stack/fbapi/common/inc
                                ${CMAKE_PROSTAR_SRC_DIR}/stack/fbapi/ff/inc
                                ${CMAKE_PROSTAR_SRC_DIR}/stack/fbapi/pa/inc
                                ${CMAKE_PROSTAR_SRC_DIR}/adaptor/businesslogic/include
                                ${CMAKE_PROSTAR_SRC_DIR}/dbus
                                ${CMAKE_PROSTAR_SRC_DIR}/cpmutil/include/
                                ${CMAKE_PROSTAR_SRC_DIR}/cpmutil/configparser/include/
                                ${CMAKE_PROSTAR_SRC_DIR}/cpmutil/threadmanager/include/
                                ${CMAKE_PROSTAR_SRC_DIR}/clogger
                                ${STARSKY_SHARED_INCLUDE_DIRS})

include_directories (${IFAK_CONTROLLER_INCLUDE_PATH})

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(ifakcontroller OBJECT ${IFAKCONTROLLER_SRC})
add_dependencies(ifakcontroller external_proj_starsky_shared)
#############################################################################
#    set CLANG and DOXYGEN Variables ########################################
#############################################################################
flk_add_clang_sources (ifakcontroller "${IFAKCONTROLLER_SRC}" "${IFAKCONTROLLER_SRC};${IFAKCONTROLLER_HDR}")
flk_add_doxygen_sources (ifakcontroller "${IFAKCONTROLLER_SRC};${IFAKCONTROLLER_HDR}")
