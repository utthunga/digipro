/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar - Fluke IDC
*/

/** @file
    IFakInterpreter class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _IFAK_INTERPRETER_H
#define _IFAK_INTERPRETER_H

/* INCLUDE FILES **************************************************************/
// Log4Cplus header files
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

// IFak header files
#include "edd_ast.h"
#include "edd_err.h"
#include "edd_values_arithmetic.h"
#include "pbInterpreter.h"

using namespace edd;

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** IFakInterpreter Class Definition.

    IFakInterpreter class implements the DD Services
    mechanism for ProfiBus protocol.
*/
class IFakInterpreter : public Interpreter
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a IFakInterpreter
    IFakInterpreter();

    /*------------------------------------------------------------------------*/
    /// Destruct a IFakInterpreter
    virtual ~IFakInterpreter();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Clears edd tree object
    */
    void clearTree(void);

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Error count in parsing dd files
    */
    int getErrorCount(void);
};

#endif  // _IFAK_INTERPRETER_H
