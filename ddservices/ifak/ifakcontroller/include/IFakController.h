/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    IFakController class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _IFAK_CONTROLLER_H
#define _IFAK_CONTROLLER_H

/* INCLUDE FILES **************************************************************/

#include <string>
#include <vector>
#include <condition_variable>
#include "IFakInterpreter.h"
#include "IConnectionManager.h"
#include "IParameterCache.h"
#include "PBAdaptor.h"
#include "IFakInterpreter.h"
#include "PMErrorCode.h"
#include "PMStatus.h"
#include "CommonProtocolManager.h"

// IFak header files
#include "edd_ast.h"
#include "edd_err.h"
#include "edd_values_arithmetic.h"

using namespace edd;

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** IFakController Class Definition.

    IFakController class implements the DD Services
    mechanism for ProfiBus protocol.
*/
class IFakController
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a IFakController
    IFakController();

    /*------------------------------------------------------------------------*/
    /// Destruct a IFakController
    virtual ~IFakController();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the IFakController with PBAdaptor, IConnectionManager and
        IParameterCache instances

        @param pba - PBAdaptor instance
        @param cm  - IConnectionManager instance
        @param pc  - IParameterCache instance

        @return status
    */

    PMStatus initialize(PBAdaptor* pba, IConnectionManager* cm,
                        IParameterCache* pc);
    
    /*------------------------------------------------------------------------*/
    /** Get edd interpreter instance

        @return edd::Interpreter
    */
    IFakInterpreter* getIFakInterpreter(void);

    /*------------------------------------------------------------------------*/
    /** Provides required item matching with the item id
        
        @param itemID - item ID for the requested DD item
        
        @return EDD_OBJECT_tree, edd object tree for the requested item id
    */
    edd::EDD_OBJECT_tree* getItemByID(const unsigned long itemID);

    /*------------------------------------------------------------------------*/
    /** Provides required item matching with the item id
        
        @param itemID - item ID for the requested DD item
        
        @return EDD_OBJECT_tree, edd object for the requested item id
    */
   edd::EDD_OBJECT_tree* getItemByIdent(const std::string identifier);

    /*------------------------------------------------------------------------*/
    /** Provides complete item tree for the provided item type
        
        @param itemID - item type for the requested DD item

        @return EDD_OBJECT_tree, edd object tree
    */
    std::vector<edd::EDD_OBJECT_tree*> getItemByType(
            const unsigned short int itemType);

    /*------------------------------------------------------------------------*/
    /** Provides item type for the corresponding item id
        
        @param itemID - item ID for the requested DD item

        @return item type
    */
    unsigned short int getItemTypeByID (const unsigned long itemID);

    /*------------------------------------------------------------------------*/
    /** Clears edd interpreter tree instances created by ifak services */
    void clearEDDTree(void);

    /*  ------------------------------------------------------------------------*/
    /** Fetches menu items

        @param itemID contains the item ID for the requested DD Item

        @param menuItems holds the menu items for the given item ID

        @return status
    */
    PMStatus getEDDMenuItems(const unsigned long itemID,
                             edd::Interpreter::menuitems_t &menuItems);


    /*  ------------------------------------------------------------------------*/
    /** Fetches DD object tree for the given ID

        @param itemID contains the item ID for the requested DD Item

        @return edd::EDD_OBJECT_tree
    */
    edd::EDD_OBJECT_tree* getEDDObjectTree(unsigned int itemID);

    /*  ------------------------------------------------------------------------*/
    /** Fetches DD menu information for the given ID

        @param itemID contains the item ID for the requested DD item

        @return edd::MENU_tree
    */
    edd::MENU_tree* getEDDMenuTree(unsigned int itemID);


    /*= Explicitly Disabled Methods ==========================================*/
    IFakController& operator = (const IFakController& rhs) = delete;
    IFakController(const IFakController& rhs) = delete;

public:
    /* Static Data Members ===================================================*/
    std::vector<std::string> includes, defines;
    std::string file, file_path ,include_path, include_path2, dct_path, std_dct_path;

private:
    /* Private Data Members ==================================================*/
    /// iFAK EDD interpreter instance
    IFakInterpreter* m_interpreter;

    /// Connection manager instance
    IConnectionManager* m_pCM;

    /// Parameter Cache instance
    IParameterCache* m_pPC;

    /// FFAdaptor instance
    PBAdaptor* m_pPBA; 
};

#endif  // _IFAK_CONTROLLER_H
