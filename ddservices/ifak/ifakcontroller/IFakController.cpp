/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    IFakController class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include "IFakController.h"
#include "CommonProtocolManager.h"

#include "fstream.h"
#include "edd_ast.h"
#include "edd_err.h"
#include "edd_values_arithmetic.h"

#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    IFakController class instance initialization
*/
IFakController::IFakController()
    : m_interpreter(nullptr), m_pCM(nullptr), m_pPC(nullptr), m_pPBA(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
IFakController::~IFakController()
{
    // Clear the member variables
    m_interpreter = nullptr;
    m_pCM  = nullptr;
    m_pPC  = nullptr;
    m_pPBA = nullptr;
    m_interpreter = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function is used to initialize the module
*/
PMStatus IFakController::initialize(PBAdaptor* pba, IConnectionManager* cm,
                                    IParameterCache* pc)
{
    PMStatus status;

    if (nullptr != pba && nullptr != cm && nullptr != pc
        && nullptr == m_interpreter)
    {
        if (nullptr == m_interpreter)
        {
            m_interpreter = new IFakInterpreter();
        }

        m_pPBA = pba;
        m_pCM  = cm;
        m_pPC  = pc;
        m_interpreter = new IFakInterpreter();
    }
    else
    {
        status.setError(
                    PB_CTRL_ERR_INITIALIZE_FAILED,
                    PM_ERR_CL_PB_CTRL,
                    PBDDS_ERR_SC_DDS);
    }

    return status;
}

/* ----------------------------------------------------------------------------
    This function clears up the edd tree object
*/
void IFakController::clearEDDTree()
{
    if (nullptr != m_interpreter)
          m_interpreter->clearTree();
}

/*  ----------------------------------------------------------------------------
    This function return edd interpreter instance
*/
IFakInterpreter* IFakController::getIFakInterpreter(void)
{
    return m_interpreter;
}

/* ----------------------------------------------------------------------------
    This function retrive dd item from ifak service
*/
EDD_OBJECT_tree* IFakController::getItemByID
(
    const unsigned long itemID
)
{
    if (nullptr == m_interpreter)
        return nullptr;

    // Fetch edd object with respect to the item-ID
    return m_interpreter->id2tree(itemID);
}

/* ----------------------------------------------------------------------------
    This function retrive dd item from ifak service
*/
EDD_OBJECT_tree* IFakController::getItemByIdent
(
    const std::string identifier
)
{
    if (nullptr == m_interpreter)
    { 
        LOGF_ERROR(CPMAPP, "IFakInterpreter instance is null!!");
        return nullptr;
    }

    // Fetch edd object with respect to the identifier
    return m_interpreter->node_lookup(identifier);
}

/* ----------------------------------------------------------------------------
    This function retrive dd item tree from ifak service
*/
std::vector<EDD_OBJECT_tree*> IFakController::getItemByType
(
    const unsigned short int itemType
)
{
    // Fetch all the object that corresponds to itemType (eg: EDD_VARIABLE)
    return m_interpreter->allobjects(itemType);
}

/* ----------------------------------------------------------------------------
    This function retrive dd item kind from ifak service
*/
unsigned short int IFakController::getItemTypeByID(
    const unsigned long itemID
)
{
    unsigned short int itemType = EDD_NONE;

    EDD_OBJECT_tree* pEDDObj = getItemByID(itemID);

    if (nullptr != pEDDObj)
    {
        itemType = pEDDObj->kind;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Fetch EDD item type failed for item ID : " 
                << itemID;)
    }

    return itemType;
}

/*  ----------------------------------------------------------------------------
    This function evaluates items inside a menu
*/
PMStatus IFakController::getEDDMenuItems(
    const unsigned long itemID,
    edd::Interpreter::menuitems_t &menuItems
)
{
    PMStatus status;
    std::string itemName = "";
   //get edd object tree
    EDD_OBJECT_tree* objectTree =  getItemByID(itemID);

    if(nullptr != objectTree)
    {
        // get Ident name
        itemName = objectTree->ident().c_str();
        
        // get edd tree
        edd::tree *eddTree =  getItemByIdent(itemName);

        if (nullptr != eddTree)
        {
             // fetch menu items
             menuItems =  m_interpreter->evalmenu(eddTree, nullptr);
        }
        else
        {
             status.setError(PB_CTRL_FETCH_MENU_ITEMS_FAILED,
                        PM_ERR_CL_PB_CTRL,
                        PBDDS_ERR_SC_DDS);

             LOGF_ERROR(CPMAPP, "Failed to fetch menu items for menu ItemID : "
                     << itemID);
        }
    }
    else
    {
        status.setError(PB_CTRL_FETCH_MENU_ITEMS_FAILED,
                        PM_ERR_CL_PB_CTRL,
                        PBDDS_ERR_SC_DDS);

        LOGF_ERROR(CPMAPP, "Failed to fetch menu items for menu ItemID : "
                 << itemID)  
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function returns edd menu tree
*/
edd::MENU_tree* IFakController::getEDDMenuTree(
    unsigned int itemID)
{
    EDD_OBJECT_tree* objectTree =  getItemByID(itemID);
    edd::MENU_tree *menuTree = dynamic_cast<edd::MENU_tree *>(objectTree);
    return menuTree;
}

