/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       FF Designers
    Origin:            Hutch
*/

/** @file
    Language string data access from the standard Dictionary File. 

    Contains the functions to access and set language string data from
    the standard dictionary files.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "std.h"
#include "dict.h"


/*
 * This controls what text is passed to the install function.
 */
// For English Language
long language = ('e' << 24) + ('n' << 16);
// For German Language
//long language = ('d' << 24) + ('e' << 16); 

/*
 * Set this and error messages will contain the
 * name of the program (see dicterr() for details).
 */
char *dict_program;

extern unsigned long num_dict_table_entries;

/*
 *	Formats for error messages
 */
static char *errfmt[] = {
	"string for language '%s' not found in\n\t%s",
	"duplicate entries found for section %d, offset %d",
	"non contiguous entries, no entries between offsets %d and %d, sect %d",
	"invalid start of section %d",
	"high text marker not highest offset in section %d",
	"high text marker is missing for section %d",
};

/*
 *	The special text the must be the highest offset for each section
 */
static char *high_text = "@XX@Highest Offset";

/*
 * Info kept in globals for efficiency.
 */
static int dicterrs;
static FILE *dictfp;
static int dictline;
static char *dictfile;

/*
 * The token buffer.
 */
static char *tokbase;
static char *tokptr;
static char *toklim;

/*
 * Token values (lex() return values).
 */
#define ERROR 0
#define DONE 1
#define NUMBER 2
#define NAME 3
#define TEXT 4
#define COMMA 5

/*
 *	Local structures for reading in the entire dictionary file
 *	to allow for sorting.
 */

typedef struct {
	unsigned long section;	/* The entry section number*/
	unsigned long offset;	/* The entry number within the section */
	unsigned long value;	/* The value is computed from section and offset*/
	char *name;				/* The name of the entry*/
	char *dict_text;		/* The text for the entry*/
	} DICT_ENT;

static DICT_ENT *dict_array = NULL;	/* Array of DICT_ENTs */
static int dict_limit;			/* Size of dict_array */
static int dict_count;			/* Number of entries in dict_array */

void (*dicterr_callback)(char *file,int line,char *msg)=NULL;

void 
setlanguage(const char *lang)
{
	if(lang)
		language = (lang[0] << 24) + (lang[1] << 16);
}

/*********************************************************************
 *
 *	Name: dicterr
 *	ShortDesc: print error message
 *
 *	Description:
 *		Print an error message on standard error and returns.
 *		If the global variable "dict_program" is set, it is printed
 *		before the text of the error message.
 *
 *		We use this function instead of the other error printing
 *		functions we have (such as error(), panic(), etc), so that
 *		this file remains as self contained as possible. 
 *
 *	Inputs:
 *		msg - error message to print
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Nothing
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static void
dicterr(char *msg)
{

	++dicterrs;
	if (dict_program != (char *) 0) {
		fprintf(stderr, "%s: ", dict_program);
	}
	(void)fprintf(stderr, "\"%s\", ", dictfile);
	if (dictline) {
		fprintf(stderr, "line %d: ", dictline);
	}
	(void)fprintf(stderr, "%s\n", msg);

	if(dicterr_callback)
		dicterr_callback(dictfile,dictline,msg);
}

/*********************************************************************
 *
 *	Name: strip
 *	ShortDesc: strips excess language from a text string
 *
 *	Description:
 *		Locates the part of a text string associacated with the
 *		country code stored in the global variable "language".
 *		A pointer to the located string is returned. This
 *		pointer is a pointer into the original string, i.e.,
 *		no data is ever moved. In order to return a null terminated
 *		string, a null byte may be written into the middle of
 *		the original string. Therefore, the original string is
 *		probably not useful, except when referenced via the
 *		the pointer return by this function.
 *
 *	Inputs:
 *		s - string to be stripped
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Pointer to requested string.
 *
 *	Author:
 *		Bruce Davis
 *		Jon Westbrock
 *
 *  Modified:
 *		Author							Reason
 *	--------------------		--------------------------------------
 *		Del Fredricks			Install support for the ISO 639 and
 *								3166 Standards for Language and Country
 *								codes for strings.
 *
 *********************************************************************/

char *
strip(char *s)
{

	int		index, code_del = 0;
	long	code;
	long	english_code = ('e' << 24) + ('n' << 16);
	char	*english_pointer, *base_lang_pnt, *begin;
	char	*errbuf, *lang_code, *end = 0;

	english_pointer = (char *) 0;
	base_lang_pnt = (char *) 0;
	begin = s;

	/*
	 *	Find the start of the first country code.
	 *
	 *  No need to handle default case since the lexical analyzer
	 *  for the dictionary parser will always ensure that string
	 *  begins with country code (i.e. it adds the |en| english
	 *  default specification if one is not present).
	 */
	s = strchr(s, '|');
	if (s != (char *) 0)
	{
		code_del = 1;
		++s;
	}

	while (s)
	{
		/*
		 *	If the string following the "|" is < 3 characters, this
		 *	cannot be a legal language/country code.  If we have found
		 *	an English string, return that.  Otherwise, error.
		 */
		if (strlen(s) < 3)
		{
			if (english_pointer == (char *) 0)
			{
				break;
			}

			return english_pointer;
		}

		if (code_del)
		{
			code = 0;
			index = -1;

				/*
		 	 	 *	Get the ISO 639 language code
		 	 	 */

				if ( isalpha (s[0]) && isalpha (s[1]) )
				{
					code += (s[0] << 24) + (s[1] << 16);
					index = 2;
	
					if ( isspace (s[2]) )
					{
						/*
					 	 * Process ISO 3166 country code specification
					 	 */
					
						if ( isalpha (s[3]) && isalpha (s[4]) )
						{
							code += (s[3] << 8) + s[4];
							index = 5;
						}
					}
	
					/*
					 * Verify that closing delimiter was properly specified;
					 * otherwise language/country codes parsed are ignored.
					 */
	
					if ( (s[++index] == '|') )
					{
						dicterr ("Closing language/country code delimiter not found");
						dicterr ("Assuming English string");
						/*
					 	 * Skip to next country code delimiter or end of string
					 	 */
	
						code = english_code;
						index = -1;
					}
				}
				else
				{
					code = english_code;
				}
	

			if (code) {
				/*	Search for the end of the string (which is either
				 *	the end of the string, or the start of the next
				 *	country code.
				 */

				for (end = s + index; end < s + strlen(s); ++end)
				{
					if (*end == '|' && strlen(end) > 4)
					{


							if (isalpha (end[1]) && isalpha (end[2]))
							{
								if (isspace (end[3]) )
								{
									if (end[6] == '|' &&
										(isalpha (end[4]) && isalpha (end[5])) )
									{
										break;
									}
								}
								else
								{
									if (end[3] == '|')
									{
										break;
									}
								}
							}

					}
				}
			
			}
			/*
			 *	If the country code matches our desired language,
			 *	return a pointer to that string.
			 */
			if (code == language)
			{
				if (end)
				{
					*end = 0;
				}

				return s + index;

			} else if ((unsigned)code == (language & 0xFFFF0000)) {
				/*
				 *	If the code matches the language code, save
				 *	a pointer to the string.  If we cannot find
				 *	a string that matches the language/country
				 *	code, we will use this language string.
				 */

				base_lang_pnt = s + index;
				break;

			}
			else if (code == english_code)
			{

				/*
				 *	If the country code matches English (the default
				 *	language), save a pointer to the English string.
				 *	If we cannot find a string that matches the
				 *	country code, we will use this English string.
				 */

				english_pointer = s + index;
			}

			/*
			 *	Point to the next string.
			 */
			if (*end)
			{
				s = end + 1;
				*end = 0;
				code_del = 1;
			}
			else
			{
				s = (char *) 0;
			}
		}
		else
		{
			/*
			 * Skip to next country code delimiter or end of string
			 */

			s = strchr(s, '|');
			if (s != (char *) 0)
			{
				code_del = 1;
				++s;
			}
		}
	}

	/*
	 * We couldn't find the requested language/country, return
	 * the base language string or English one if we've got it.
	 */

	if ( base_lang_pnt != (char *) 0) 
	{
		return base_lang_pnt;
	}

	if (english_pointer == (char *) 0)
	{
		errbuf = (char *)malloc ((unsigned)(strlen(begin) + strlen(errfmt[0]) + 1));
		if (errbuf == (char *) 0)
		{
			dicterr("memory exhausted");
			exit(-1);
		};

		lang_code = (char *)malloc (6);
		lang_code[2] = '\0';
		lang_code[0] =(char)( (language & 0xFF000000) >> 24);
		lang_code[1] =(char)( (language & 0x00FF0000) >> 16);

		if ((language & 0x0000FFFF)  > 0)
		{
			lang_code[2] = ' ';
			lang_code[3] = (char)((language & 0x0000FF00) >> 8);
			lang_code[4] = (char)(language & 0x000000FF);
			lang_code[5] = '\0';
		};

		(void)sprintf(errbuf,errfmt[0],lang_code,begin);
		dicterr (errbuf);
		free (errbuf);
		free (lang_code);
	}

	return english_pointer;
}


/*********************************************************************
 *
 *	Name: inschar
 *	ShortDesc: insert a character in the token buffer
 *
 *	Description:
 *		Insert a character into the token buffer (tokbase,
 *		tokptr, toklim). The token buffer is always kept
 *		null terminated, and is grown as needed.
 *
 *	Inputs:
 *		c - character to be inserted
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Nothing
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static void
inschar(int c)
{

	int		off, size;

	/*
	 * Make sure token buffer exists, and can accommodate another
	 * character.  We keep the token buffer null terminated, hence
	 * the strange limit check.
	 */
	if (tokbase == (char *) 0 || tokptr + 1 == toklim) {
		off = tokptr - tokbase;
		size = toklim - tokbase;
		if (size == 0) {
			size = 32;
			tokbase = (char *) malloc((unsigned)size);
		} else {
			size *= 2;
			tokbase = (char *) realloc(tokbase, (unsigned)size);
		}
		if (tokbase == (char *) 0) {
			dicterr("memory exhausted");
			exit(-1);
		}
		tokptr = tokbase + off;
		toklim = tokbase + size;
	}

	/*
	 * Insert the character, keeping the buffer null terminated.
	 */
	*tokptr++ = (char) c;
	*tokptr = 0;
}


/*********************************************************************
 *
 *	Name: comment
 *	ShortDesc: reads a C style comment
 *
 *	Description:
 *		Read a C style comment from the current dictionary
 *		file (dictfp). If an EOF is seen prior to the closing
 * 		of the comment, an error is returned.
 *
 *	Inputs:
 *		None		
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		DONE - if successful
 *		ERROR - otherwise
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static int
comment()
{

	int		c;

	/*
	 * Verify we're at the start of a comment. If we don't
	 * find a valid start of comment, return an error.
	 */
	c = getc(dictfp);
	if (c != '*') {
		return ERROR;
	}

	/*
	 * Consume the comment.
	 */
	c = getc(dictfp);
	for (;;) {
		if (c == EOF) {
			return ERROR;
		}

		/*
		 * If it's a *, check for end of comment.
		 */
		if (c == '\n') {
			++dictline;
		} else if (c == '*') {
			c = getc(dictfp);
			if (c == EOF) {
				return ERROR;
			}
			if (c == '\n') {
				++dictline;
			} else if (c == '/') {
				break;
			} else {
				continue;
			}
		}
		/*
		 * Next please.
		 */
		c = getc(dictfp);
	}

	/*
	 * Indicates valid comment consumed.
	 */
	return DONE;
}


/*********************************************************************
 *
 *	Name:  lex
 *	ShortDesc: standard dictionary lexical analyser
 *
 *	Description:
 *		Reads characters from the current dictionary file (dictfp) and
 *		translates them into tokens, which are parsed by parse() (see below).
 *
 *	Inputs:
 *		None
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		ERROR  - error in the input
 *		DONE   - end of file
 *		NUMBER - standard dictionary number
 *		NAME   - standard dictionary name
 *		TEXT   - text of a standard dictionary string
 *		COMMA  - the comma character
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static int
lex()
{

	int			c;
	static int	nextchar = -1;

	/*
	 * Get next character.
	 */
	if (nextchar < 0) {
		c = getc(dictfp);
	} else {
		c = nextchar;
		nextchar = -1;
	}

	for (;;) {
		switch (c) {
		case EOF:
			return DONE;

		/*
		 * Eat whitespace.
		 */
		case '\n':
			++dictline;
			/* FALL THROUGH */

		case ' ':
		case '\b':
		case '\f':
		case '\r':
		case '\t':
		case '\v':
        case 0xef: /* UTF-8 marker: ignore */
        case 0xbb: /* UTF-8 marker: ignore */
        case 0xbf: /* UTF-8 marker: ignore */
			c = getc(dictfp);
			break;

		/*
		 * Eat a comment.
		 */
		case '/':
			if (comment() != DONE) {
				return ERROR;
			}
			c = getc(dictfp);
			break;

		case ',':
			return COMMA;

		/*
		 * Read a number.
		 */
		case '[':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * Read a digit sequence.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isdigit(c));

			/*
			 * Read a comma.
			 */
			if (c != ',') {
				return ERROR;
			}

			/*
			 * Read another digit sequence.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isdigit(c));

			/*
			 * Verify it ends correctly.
			 */
			if (c != ']') {
				return ERROR;
			}
			inschar(c);

			/*
			 * Return it.
			 */
			return NUMBER;

		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
		case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
		case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
		case 'Y': case 'Z': case '_': case 'a': case 'b': case 'c':
		case 'd': case 'e': case 'f': case 'g': case 'h': case 'i':
		case 'j': case 'k': case 'l': case 'm': case 'n': case 'o':
		case 'p': case 'q': case 'r': case 's': case 't': case 'u':
		case 'v': case 'w': case 'x': case 'y': case 'z':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * Read a sequence of alphanumerics and underscores.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isalnum(c) || c == '_');

			/*
			 * Save the lookahead character.
			 */
			nextchar = c;

			/*
			 * Return it.
			 */
			return NAME;

		case '\"':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * If string does not start with a country code,
			 * add the default country code (English).
			 */
			c = getc(dictfp);
			if (c != '|') {
				inschar('|');
				inschar('e');
				inschar('n');
				inschar('|');
			}

			/*
			 * Read the characters of the string.
			 */
			for (;; c = getc(dictfp)) {
				switch (c) {
				/*
				 * Unterminated string.
				 */
				case EOF:
				case '\n':
					return ERROR;

				/*
				 * Anything can be escaped with a backslash,
				 * this will usually be \".
				 */
				case '\\':
					inschar(c);
					c = getc(dictfp);
					if (c == EOF) {
						return ERROR;
					}
					inschar(c);
					break;

				/*
				 * We've come to the end of the string. Read ahead
				 * to see if there is string immediately following
				 * this one, and if there is read that one also.
				 */
				case '\"':
					c = getc(dictfp);
					for (;;) {
						switch (c) {
						/*
						 * Eat whitespace.
						 */
						case '\n':
							dictline++;
							/* FALL THROUGH */

						case ',':
						case ' ':
						case '\b':
						case '\f':
						case '\r':
						case '\t':
							c = getc(dictfp);
							continue;

						/*
						 * Eat a comment.
						 */
						case '/':
							if (comment() != DONE) {
								return ERROR;
							}
							c = getc(dictfp);
							continue;

						/*
						 * We've seen something other than
						 * a comment or whitespace.
						 */
						default:
							break;
						}
						break;
					}

					/*
					 * If what we've seen is the start
					 * of another string, read it also.
					 */
					if (c == '\"')
						break;

					/*
					 * We've seen something other than the start
					 * of a string.  Save the lookahead character.
					 */
					nextchar = c;
					return TEXT;

				/*
				 * Ordinary character, add it to the token buffer.
				 */
				default:
					inschar(c);
					break;
				}
			}
			/*NOTREACHED*/

		/*
		 * Invalid input.
		 */
		default:
			return ERROR;
		}
	}
}

/*********************************************************************
 *
 *	Name: parse
 *	ShortDesc: standard dictionary parser
 *
 *	Description:
 *		Parses the current dictionary file (dictfp). Calls lex()
 *		to get tokens. When an error is detected, it resyncronizes
 *		(by looking for the beginning of a valid dictionary entry)
 *		and continues parsing from that point. This allows multiple
 *		errors in a dictionary file to be detected at once.
 *
 *	Inputs:
 *		None
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Nothing
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static void
parse()
{

	int				tok;
	char			*last, *name;
	unsigned long	section, offset;
	char			*strptr, *strbuf;
	DICT_ENT		*curr_dict, *end_dict;

	/*
	 * Initialize.
	 */
	tok = lex();
	curr_dict = dict_array + dict_count;
	end_dict = dict_array + dict_limit;

	while (tok != DONE) {
		/*
		 * Verify the token is a number.
		 */
		if (tok != NUMBER) {
			dicterr("syntax error");
			do { 
				tok = lex();
			} while (tok != DONE && tok != NUMBER);
			continue;
		}

		/*
		 * Extract the section number and offset from
		 * the token.
		 */
		section = strtoul(tokbase + 1, &last, 10);
		offset = strtoul(last + 1, (char **) 0, 10);

		/*
		 * Get next token and verify it's a string name.
		 */
		tok = lex();
		if (tok != NAME) {
			dicterr("syntax error");
			do {
				tok = lex();
			} while (tok != DONE && tok != NUMBER);
			continue;
		}

		/*
		 * Save the name.
		 */
		name = (char *) malloc((unsigned)(strlen(tokbase) + 1));
		if (name == (char *) 0) {
			dicterr("memory exhausted");
			exit(-1);
		}
		strcpy(name, tokbase);

		/*
		 * Get the next token and verify it's a string.
		 */
		tok = lex();
		if (tok != TEXT) {
			dicterr("syntax error");
			do {
				tok = lex();
			} while (tok != DONE && tok != NUMBER);
			continue;
		}

		/*
		 * Strip away unwanted languages.
		 */
		strptr = strip(tokbase);

		/*
		 * Save the desired language.
		 */
		if (strptr == (char *)0) {
			strbuf = (char *)0;
		} else {
			strbuf = (char *) malloc((unsigned)(strlen(strptr) + 1));
			if (strbuf == (char *) 0) {
				dicterr("memory exhausted");
				exit(-1);
			}
			strcpy (strbuf, strptr);
		}

		/*
		 *	Save this entry into dict_array, expanding it if necessary.
		 */

		if (curr_dict == end_dict) {
			dict_count = dict_limit;
			dict_limit *= 2;
			dict_array = (DICT_ENT *)realloc ((char *)dict_array,
							(unsigned)(dict_limit * sizeof (*dict_array)));
			if (!dict_array) {
				dicterr("memory exhausted");
				exit(-1);
			}
			curr_dict = dict_array + dict_count;
			end_dict = dict_array + dict_limit;
		}
		curr_dict->section = section;
		curr_dict->offset = offset;
		curr_dict->value = section * 65536 + offset;
		curr_dict->name = name;
		curr_dict->dict_text = strbuf;
		curr_dict++;

		/*
		 *	Get the next token
		 */

		tok = lex();
	}
	dict_count = curr_dict - dict_array;
}

/*********************************************************************
 *
 *	Name: compdict
 *	ShortDesc: Compare function for qsort of DICT_ENTs
 *
 *	Description
 *		Compares two DICT_ENT entries, and returns less than zero,
 *		equal zero, or greater than zero depending on whether the
 *		first entry is smaller, equal, or greater than the second.
 *
 *	Inputs:
 *		dict_ent1, dict_ent2 - pointers to the two DICT_ENT entries
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		< 0 - *dict_ent1 < *dict_ent2
 *		  0 - *dict_ent1 == *dict_ent2
 *		> 0 - *dict_ent1 > *dict_ent2
 *
 *	Author:
 *		Bruce Davis
 *********************************************************************/

static int
compdict (DICT_ENT *dict_ent1, DICT_ENT *dict_ent2)
{

	return (int)(dict_ent1->value - dict_ent2->value);
}

/*********************************************************************
 *
 *	Name: makedict
 *	ShortDesc: build a dictionary table
 *
 *	Description:
 *		Reads a dictionary file and creates a user defined
 *		dictionary table. The caller must supply an install
 *		function which is called every time a valid dictionary
 *		entry is read. This allows the caller to create as
 *		large or small a table as needed, and allows the 
 *		complete control over the structure of the table.
 *
 *		The install function takes three arguments:
 *			unsigned long value - string number
 *			char *name 			- string name
 *			char *text			- text of string
 *
 *		The name and the text arguments are malloc'd, and
 *	 	if the caller does not store them, they should be
 *		free'd. If a text string does not contain the
 *		requested language (global variable "language"),
 * 		the text argument may be 0. This situation is
 *		flagged as an error, so exit'ing if the dictionary
 *		is not correctly parsed avoids this situation.
 *
 *	Inputs:
 *		file - file name of dictionary file
 *		func - install function
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Number of error detected.
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

extern char dd_path[];

int
makedict(char *file, void	(*func)(ulong, char *, char *), int val)
{
	DICT_ENT	*curr_dict, *end_dict, *next_dict;
	char		*errbuf;

	/*
	 * Initialize the globals.
	 */
	dicterrs = 0;
	dictline = 1;
	dictfile = file;

	/*
	 *	Create the DICT_ENT pointer array.
	 */
    if (dict_array)
    {
        free(dict_array);
        dict_array = NULL;
    }

	dict_limit = 200;
	dict_count = 0;
	dict_array = (DICT_ENT *)malloc ((unsigned)(dict_limit * sizeof (*dict_array)));
	if (!dict_array) {
		dicterr("memory exhausted");
		exit(-1);
	}

    num_dict_table_entries = 0;

	/*
	 * Open the file.
	 */
	dictfp = fopen(dictfile, "r");
	if (!dictfp) {
		if (dict_program != (char *) 0) {
			fprintf(stderr, "%s: ", dict_program);
		}
#ifdef DD_VIEWER
		return -1;
#else
		fprintf(stderr, "can't open dictionary \"%s\"\n", file);
		exit(-1);
#endif
	}

	/*
	 * Read it.
	 */
	parse();

	/*
	 *	Sort the entries from the files
	 */

	(void)qsort ((char *)dict_array, dict_count, 
				sizeof (*dict_array), (CMP_FN_PTR)compdict);

	/*
	 *	Step through the sorted entries, verifying them and passing
	 *	them to the use supplied function.  Verification includes
	 *	that the offsets are continguous within a section, that each
	 *	section starts with offset zero, and that each section ends
	 *	with a special entry with text matching high_text.
	 *
	 *	Dictline is set to zero to signal dicterr() not to print
	 *	and line numbers.
	 */

	dictline = 0;
	curr_dict = dict_array;
	end_dict = dict_array + dict_count - 1;
	for (; curr_dict <= end_dict; curr_dict++) {
		
		/*
		 *	If this is the last entry, it must be a high text entry.
		 */

		if (curr_dict == end_dict) {
			if (strcmp (curr_dict->dict_text, high_text)) {
				errbuf = (char *)malloc ((unsigned)(strlen(errfmt[5]) + 20));
				if (errbuf == (char *) 0) {
					dicterr("memory exhausted");
					exit(-1);
				}
				(void)sprintf(errbuf,errfmt[5],curr_dict->section);
				dicterr (errbuf);
				free (errbuf);
			}

			/*
			 *	This is a "hidden" entry, so don't pass it to the
			 *	user function.
			 */
			free (curr_dict->name);
			free (curr_dict->dict_text);
			continue;
		}

		/*
		 *	Each entry must be unique.
		 */

		next_dict = curr_dict + 1;
		if (curr_dict->value == next_dict->value) {
			errbuf = (char *)malloc ((unsigned)(strlen(errfmt[1]) + 20));
			if (errbuf == (char *) 0) {
				dicterr("memory exhausted");
				exit(-1);
			}
			(void)sprintf(errbuf,errfmt[1],curr_dict->section, 
						curr_dict->offset);
			dicterr (errbuf);
			free (errbuf);
			curr_dict++;
			continue;
		}

		/*
		 *	If this entry is the special high text string ...
		 */

		if (curr_dict->dict_text && strcmp (curr_dict->dict_text, high_text) == 0) {

			/*
			 *	... then the next section must be different than
			 *	this section, and ...
			 */

			free (curr_dict->name);
			if (curr_dict->section == next_dict->section) {
				errbuf = (char *)malloc ((unsigned)(strlen(errfmt[4]) + 20));
				if (errbuf == (char *) 0) {
					dicterr("memory exhausted");
					exit(-1);
				}
				(void)sprintf(errbuf,errfmt[4],curr_dict->section);
				dicterr (errbuf);
				free (errbuf);
			}

			/*
			 *	... the first offset in the next section must
			 *	be zero.
			 */

			else if (next_dict->offset != 0) {
				errbuf = (char *)malloc ((unsigned)(strlen(errfmt[3]) + 20));
				if (errbuf == (char *) 0) {
					dicterr("memory exhausted");
					exit(-1);
				}
				(void)sprintf(errbuf,errfmt[3],next_dict->section);
				dicterr (errbuf);
				free (errbuf);
			}

			/*
			 *	This is a "hidden" entry, so don't pass it to the
			 *	user function.
			 */

			free (curr_dict->dict_text);
			continue;

		} else {
			/*
			 *	This is a "regular" entry.  It must not be the
			 *	last entry in a section.
			 */

			if (curr_dict->section != next_dict->section) {
				errbuf = (char *)malloc ((unsigned)(strlen(errfmt[5]) + 20));
				if (errbuf == (char *) 0) {
					dicterr("memory exhausted");
					exit(-1);
				}
				(void)sprintf(errbuf,errfmt[5],curr_dict->section);
				dicterr (errbuf);
				free (errbuf);
			}

			/*
			 *	The offset of the next entry must be one larger
			 *	than this one.
			 */

			else if (curr_dict->offset != next_dict->offset - 1) {
				errbuf = (char *)malloc ((unsigned)(strlen(errfmt[2]) + 40));
				if (errbuf == (char *) 0) {
					dicterr("memory exhausted");
					exit(-1);
				}
				(void)sprintf(errbuf,errfmt[2], curr_dict->offset, 
							next_dict->offset, curr_dict->section);
				dicterr (errbuf);
				free (errbuf);
			}

			/*
			 *	Call the install function.
			 */

			(*func) (curr_dict->value, curr_dict->name, curr_dict->dict_text);
		}
	}
		
	/*
	 * Tidy up.
	 */
	(void) fclose(dictfp);
	if (tokbase) {
		free (tokbase);
		tokbase = NULL;
		tokptr = NULL;
		toklim = NULL;
	}
	free ((char *)dict_array);
    dict_array = NULL;

	/*
	 * Return number of errors seen.
	 */
	return dicterrs;
}


void removeDict(void(*func)())
{
	/* call uninstall dict function */
	func();
}
