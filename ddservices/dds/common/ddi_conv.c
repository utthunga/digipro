/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  This file contains all of the functions for DDI convenience
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include	<stdlib.h>
#include <stdio.h>

#include "std.h"
#include "app_xmal.h"
#include "ddi_lib.h"
#include "ddi_tab.h"
#include "dds_tab.h"
#include "tst_fail.h"
#include "cm_rod.h"

#define FFDDINFO_BLOCK_ATTRS_ENUM 11
#define FFDDINFO_CONDITIONAL_FLAG "cond"
int parse_block_attr_enum(char *enum_str, ITEM_ID block_id, char *flag_name, int *outputValue);
int locate_symbol_id(ENV_INFO2 *env, char *sym_name, ITEM_ID *out_id);

/*********************************************************************
 *
 *	Name: conv_block_spec
 *	ShortDesc: convert the block_specifier
 *
 *	Description:
 *		ddi_conv_specifiers converts the block_specifier into a
 *		block_handle if the type is DDI_BLOCK_TAG.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *
 *	Outputs:
 *		block_handle:	the block_handle
 *
 *	Returns:DDS_SUCCESS, DDI_INVALID_PARAM
 *		returns from other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
conv_block_spec(const DDI_BLOCK_SPECIFIER *block_spec, BLOCK_HANDLE *block_handle)
{
	int             rc;	/* return code */

	switch (block_spec->type) {

	case DDI_BLOCK_TAG:
		rc = tr_block_tag_to_block_handle(block_spec->block.tag, block_handle);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
		return DDS_SUCCESS;

	case DDI_BLOCK_HANDLE:
		*block_handle = block_spec->block.handle;
		return DDS_SUCCESS;

	default:
		return DDI_INVALID_TYPE;

	}
}


/*********************************************************************
 *
 *	Name: convert_param_spec
 *	ShortDesc: convert the param_specifer structure into a bint offset
 *
 *	Description:
 *		convert_param_spec takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the Block Item Name Table offset
 *
 *	Inputs:
 *		block_handle:	the block handle
 *		param_spec:	    a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		bint_offset:	a pointer to the block item name table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_param_spec(BLOCK_HANDLE block_handle, const DDI_PARAM_SPECIFIER *param_spec,
	BLK_TBL_ELEM *bt_elem, int *bint_offset)
{

	int             rc;	/* return code */

	/*
	 * Convert the param specifier to a block item name table offset
	 */

	switch (param_spec->type) {

	case DDI_PS_ITEM_ID:
	case DDI_PS_ITEM_ID_SI:
		rc = tr_id_to_bint_offset(bt_elem, param_spec->item.id, bint_offset);
		break;

	case DDI_PS_PARAM_NAME:
	case DDI_PS_PARAM_NAME_SI:
		rc = tr_name_to_bint_offset(bt_elem, param_spec->item.name,
			bint_offset);
		break;

	case DDI_PS_OP_INDEX:
	case DDI_PS_OP_INDEX_SI:
		rc = tr_op_index_to_bint_offset(block_handle, bt_elem,
			param_spec->item.op_index, bint_offset);
		break;	

	case DDI_PS_PARAM_OFFSET:
	case DDI_PS_PARAM_OFFSET_SI:
		rc = tr_param_to_bint_offset(bt_elem, param_spec->item.param,
			bint_offset);
		break;

	case DDI_PS_CHARACTERISTICS_SI:

/* It's not clear what this does because the block item name table entry
   for characteristics is empty except for the item table offset, and the
   core entries are useless (i.e. parameter table offset */

		rc = tr_char_to_bint_offset(bt_elem, bint_offset);

		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: conv_block_and_param_spec
 *
 *	Description:
 *       convert a block specifier to a block handle and a
 *       parameter specifier to a block item name table offset
 *
 *	Inputs:
 *		block_spec: a pointer to the block specifier
 *      param_spec: a pointer to the parameter specifier
 *
 *	Outputs:
 *		block_handle:	a block handle
 *		bt_elem:		a pointer to a pointer of block table element
 *		bint_offset:	an index into the Block Item Name Table
 *
 *	Returns:
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
conv_block_and_param_spec(const DDI_BLOCK_SPECIFIER *block_spec,
	const DDI_PARAM_SPECIFIER *param_spec, BLOCK_HANDLE *block_handle,
	BLK_TBL_ELEM **bt_elem, int *bint_offset)
{

	int             rc;			/* return code */
	DD_HANDLE       dd_handle;	/* device description handle */

	rc = conv_block_spec(block_spec, block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(*block_handle, bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = convert_param_spec(*block_handle, param_spec, *bt_elem, bint_offset);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_param_spec fails because of no block,
			 * request a load for the dd
			 */

			rc = ds_dd_block_load(*block_handle, &dd_handle);
			if (!rc) {

				rc = convert_param_spec(*block_handle,
					param_spec, *bt_elem, bint_offset);

				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: convert_param_spec_to_bint_offset
 *	ShortDesc:  convert the param specifier structure into an bint
 *				offset
 *
 *	Description:
 *		convert_param_spec_to_bint_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the named item table offset
 *
 *	Inputs:
 *              block_handle    the block handle
 *
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER
 *						structure
 *		bt_elem:	the current block table element
 *
 *	Outputs:
 *		bint_offset:	a pointer to the Block Item Name Table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from bint_offset table
 *  request functions
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

static int
convert_param_spec_to_bint_offset(BLOCK_HANDLE block_handle,
	const DDI_PARAM_SPECIFIER *param_spec, BLK_TBL_ELEM *bt_elem, int *bint_offset)
{

  int             rc;	/* return code */

  switch (param_spec->type) {

  case DDI_PS_ITEM_ID:
    rc = tr_id_to_bint_offset(bt_elem, param_spec->item.id, bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME:
    rc = tr_name_to_bint_offset(bt_elem, param_spec->item.name, bint_offset);
    break;
    
  case DDI_PS_PARAM_OFFSET:
    rc = tr_param_to_bint_offset(bt_elem, param_spec->item.param, bint_offset);
    break;
    
  case DDI_PS_OP_INDEX:
    rc = tr_op_index_to_bint_offset
		(block_handle, bt_elem, param_spec->item.op_index, bint_offset);
      break;

  case DDI_PS_ITEM_ID_SI:
    rc = tr_id_si_to_bint_offset
		(bt_elem, param_spec->subindex,param_spec->item.id, bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME_SI:
    rc = tr_name_si_to_bint_offset
		(bt_elem, param_spec->subindex,param_spec->item.name, bint_offset);
    break;
    
  case DDI_PS_OP_INDEX_SI:
    rc = tr_op_index_si_to_bint_offset
		(block_handle, bt_elem, param_spec->item.op_index, bint_offset, param_spec->subindex);
    break;

  case DDI_PS_PARAM_OFFSET_SI:
    rc = tr_param_si_to_bint_offset
		(bt_elem, param_spec->subindex, param_spec->item.param, bint_offset);
    break;
  case DDI_PS_CHARACTERISTICS_SI:
    rc = tr_char_to_bint_offset(bt_elem, bint_offset);
    break;
  default:
    rc = DDI_INVALID_REQUEST_TYPE;
  }
  
  return rc;
}


/*********************************************************************
 *
 *	Name: get_rt_offset
 *	ShortDesc: convert a bint_offset or bint_offset_si into a relation table offset
 *
 *	Description:
 *		get_rt_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate conversion function to
 *              convert the bint_offset to a relation table offset.
 *
 *	Inputs:
 *		bint_offset:	the bint offset
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		rt_offset:		a pointer to the Relation Table
 *						offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
get_rt_offset(int bint_offset, const DDI_PARAM_SPECIFIER *param_spec,
	BLK_TBL_ELEM *bt_elem, int *rt_offset)
{

	int             rc;	/* return code */

	/*
	 * Convert the request to a Relation Offset
	 */

	switch (param_spec->type) {

	case DDI_PS_ITEM_ID:
	case DDI_PS_PARAM_NAME:
	case DDI_PS_OP_INDEX:
	case DDI_PS_PARAM_OFFSET:
		rc = tr_bint_offset_to_rt_offset(bt_elem, bint_offset, rt_offset);
		break;

	case DDI_PS_ITEM_ID_SI:
	case DDI_PS_PARAM_NAME_SI:
	case DDI_PS_OP_INDEX_SI:
	case DDI_PS_PARAM_OFFSET_SI:
	case DDI_PS_CHARACTERISTICS_SI:
		rc = tr_bint_offset_si_to_rt_offset(bt_elem,
			param_spec->subindex, bint_offset, rt_offset);
		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
		break;

	}
	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_check_enum_var_value
 *	ShortDesc: check for the value in the FLAT_VAR
 *
 *	Description:
 *		ddi_check_enum_var_value searches the enum_value_list for
 *		the given value if the FLAT_VAR is of type ENUMERATED
 *		 or BIT_ENUMERATED
 *
 *	Inputs:
 *		var:	a pointer to the flat variable to search
 *		value:	a pointer to the value to look for
 *
 *	Outputs:
 *		none
 *
 *	Returns: DDI_LEGAL_ENUM_VAR_VALUE, DDI_ILLEGAL_ENUM_VAR_VALUE
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_check_enum_var_value(FLAT_VAR *var, unsigned long *value)
{

	unsigned short  inc;		/* counter for enum list */
	ENUM_VALUE     *enum_ptr;	/* temp pointer for the enum_value */

    if(NULL == value)
        return DDI_LEGAL_ENUM_VAR_VALUE;

    if(NULL != var) 
    {
    	ASSERT_RET(var && value, DDI_INVALID_PARAM);
        enum_ptr = var->enums.list;

    	if ((var->type_size.type == DDS_ENUMERATED) ||
	    	(var->type_size.type == DDS_BIT_ENUMERATED)) {

		    for (inc = 0; inc < var->enums.count; inc++, enum_ptr++) {

			/*
			 * Check that the value is available
			 */

			    if (enum_ptr->evaled & ENUM_VAL_EVALED) {

				    if (enum_ptr->val == *value) {

					    return DDI_LEGAL_ENUM_VAR_VALUE;
				    }
			    }
	    	}
	    	return DDI_ILLEGAL_ENUM_VAR_VALUE;
	    }
	    else {

		    return DDI_ILLEGAL_ENUM_VAR_VALUE;
	    }
    }
		
    else
    {
        return DDI_ILLEGAL_ENUM_VAR_VALUE;
    }
}


/*********************************************************************
 *
 *	Name: convert_item_spec_to_item_type
 *	ShortDesc: convert the item_spec structure into an item_type
 *
 *	Description:
 *		convert_item_spec_to_item_type takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the item_type.
 *
 *	Inputs:
 *		block_handle:	the current block handle
 *		item_spec:	a pointer to the DDI_ITEM_SPECIFIER structure
 *		bt_elem:	the Block Table element
 *
 *	Outputs:
 *		item_type:		a pointer to the item type
 *
 *	Returns:DDI_INVALID_REQUEST_TYPE and returns from
 *  	other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_item_spec_to_item_type2(FLAT_DEVICE_DIR *flat_device_dir, BLOCK_HANDLE block_handle,
	DDI_ITEM_SPECIFIER *item_spec, BLK_TBL_ELEM *bt_elem, ITEM_TYPE *item_type)
{

	int             rc;			/* return code */
	ITEM_TBL       	*it;		/* item table */
	ITEM_TBL_ELEM  	*it_elem;	/* item table element */

	TEST_FAIL(CONVERT_ITEM_SPEC);

	it = &flat_device_dir->item_tbl;
	switch (item_spec->type) {

	case DDI_ITEM_ID:
	  rc = tr_id_to_ite(it, item_spec->item.id, &it_elem);
		break;

	case DDI_ITEM_NAME:
	  rc = tr_name_to_ite(it, bt_elem, item_spec->item.name, &it_elem);
	  break;

	case DDI_ITEM_OP_INDEX:
	  rc = tr_op_index_to_ite(block_handle, it,
				  bt_elem, item_spec->item.op_index, &it_elem);
	  break;
		
	case DDI_ITEM_PARAM:
	  rc = tr_param_to_ite(it, bt_elem, item_spec->item.param, &it_elem);
	  break;
		
	case DDI_ITEM_ID_SI:
	  rc = tr_id_si_to_ite(it, bt_elem, item_spec->item.id,
			       item_spec->subindex, &it_elem);
	  break;
		
	case DDI_ITEM_NAME_SI:
	  rc = tr_name_si_to_ite(it, bt_elem, item_spec->item.name,
				 item_spec->subindex, &it_elem);
	  break;

	case DDI_ITEM_OP_INDEX_SI:
	  rc = tr_op_index_si_to_ite(block_handle, it, bt_elem,
				 item_spec->item.op_index, item_spec->subindex, &it_elem);
	  break;

	case DDI_ITEM_PARAM_SI:
	  rc = tr_param_si_to_ite(it, bt_elem,
				  item_spec->item.param, item_spec->subindex, &it_elem);
	  break;

	case DDI_ITEM_BLOCK:
	  *item_type = BLOCK_ITYPE;
	  return DDS_SUCCESS;
	  
	case DDI_ITEM_PARAM_LIST:
	  *item_type = PARAM_LIST_ITYPE;
	  return DDS_SUCCESS;
	  
	case DDI_ITEM_CHARACTERISTICS:
	  *item_type = BLOCK_CHAR_ITYPE;
	  return DDS_SUCCESS;

	default:
		return DDI_INVALID_REQUEST_TYPE;
	}

	if (rc != DDS_SUCCESS) {
		return rc;
	}

	*item_type = ITE_ITEM_TYPE(it_elem);
	return rc;
}

static
int
convert_item_spec_to_item_type(BLOCK_HANDLE block_handle,
	DDI_ITEM_SPECIFIER *item_spec, BLK_TBL_ELEM *bt_elem, ITEM_TYPE *item_type)
{

	int             rc;			/* return code */
	FLAT_DEVICE_DIR	*flat_device_dir ;

	TEST_FAIL(CONVERT_ITEM_SPEC);

	rc = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
    return convert_item_spec_to_item_type2(flat_device_dir, block_handle, item_spec, bt_elem, item_type);
}



/*********************************************************************
 *
 *	Name: ddi_get_type
 *	ShortDesc: get the type of the item
 *
 *	Description:
 *		ddi_get_type will return the item_type of the
 *		requested type
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		item_spec:		a pointer to the item specifier
 *
 *	Outputs:
 *		item_type:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_type(DDI_BLOCK_SPECIFIER *block_spec, DDI_ITEM_SPECIFIER *item_spec,
	ITEM_TYPE *item_type)
{

	
	int             rc;				/* return code */
	DD_HANDLE       dd_handle;		/* device description handle */
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	BLOCK_HANDLE    block_handle;	/* Block Handle */


	ASSERT_RET(block_spec && item_spec && item_type, DDI_INVALID_PARAM);

	rc = conv_block_spec(block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = convert_item_spec_to_item_type(block_handle, item_spec,
		bt_elem, item_type);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_item_spec_to_item_type fails because of no block,
			 * request a load for the dd
			 */

			rc = ds_dd_block_load(block_handle, &dd_handle);
			if (!rc) {

				rc = convert_item_spec_to_item_type(block_handle, item_spec,
					bt_elem, item_type);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}

int
ddi_get_type2(ENV_INFO2 *env_info2, DDI_ITEM_SPECIFIER *item_spec,
	ITEM_TYPE *item_type)
{

	int             rc;				/* return code */
	DD_HANDLE       dd_handle;		/* device description handle */
	BLK_TBL_ELEM   *bt_elem = NULL;		/* Block Table element pointer */
    FLAT_DEVICE_DIR *flat_device_dir = NULL;

	ASSERT_RET(env_info2 && item_spec && item_type, DDI_INVALID_PARAM);

    switch(env_info2->type)
    {
    case ENV_BLOCK_HANDLE:
    	rc = get_abt_dd_dev_tbls(env_info2->block_handle, (void **)&flat_device_dir) ;
        if (rc)
            return rc;
	    rc = block_handle_to_bt_elem(env_info2->block_handle, &bt_elem);
	    if (rc != DDS_SUCCESS) {
		    return rc;
	    }
        break;
    case ENV_DEVICE_HANDLE:
    	rc = get_adt_dd_dev_tbls(env_info2->device_handle, (void **)&flat_device_dir) ;
        if (rc)
            return rc;
        bt_elem = NULL;
        break;
    default:
        return DDI_INVALID_DD_HANDLE_TYPE;
    }

    rc = convert_item_spec_to_item_type2(flat_device_dir, env_info2->block_handle,
	    item_spec, bt_elem, item_type);

	if (rc != DDS_SUCCESS) {
		if (rc == DDI_TAB_NO_BLOCK) {

            if (env_info2->type != ENV_BLOCK_HANDLE)
                return rc;
			/*
			 * If convert_item_spec_to_item_type fails because of no block,
			 * request a load for the dd
			 */

			rc = ds_dd_block_load(env_info2->block_handle, &dd_handle);
			if (!rc) {

				rc = convert_item_spec_to_item_type(env_info2->block_handle, item_spec,
					bt_elem, item_type);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}

int
ddi_find_unit_ids(DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID id_to_find,
	ITEM_ID *item_id_of_unit_rel, ITEM_ID *item_id_of_unit_var)
{
    DDI_GENERIC_ITEM gi;
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
    int rc;
    ENV_INFO env_info;
    DDI_ITEM_SPECIFIER blk_item_spec;
    BLOCK_HANDLE block_handle;
    int req_mask;
    FLAT_BLOCK *flat;
    int i;

	/*
	 * convert the block specifier to a block handle
	 */

	rc = conv_block_spec(block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {
		return rc;
	}
	else {
		env_info.block_handle = block_handle;
	}
	rc = block_handle_to_bt_elem(block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

    blk_item_spec.type = DDI_ITEM_BLOCK;
    blk_item_spec.item.id = bt_elem->blk_id;

    req_mask = BLOCK_UNIT;

    memset(&gi, 0, sizeof(gi));
    rc = ddi_get_item(block_spec, &blk_item_spec, &env_info, req_mask, &gi); 
    if (rc)
        return rc;

    flat = (FLAT_BLOCK *)gi.item;
    for (i=0; i < flat->unit.count; i++)
    {
        DDI_GENERIC_ITEM gi_unit;
        DDI_ITEM_SPECIFIER is_unit;
        FLAT_UNIT *unit;
        int u;

        memset(&gi_unit, 0, sizeof(gi_unit));
        is_unit.type = DDI_ITEM_ID;
        is_unit.item.id = flat->unit.list[i];
        rc = ddi_get_item(block_spec, &is_unit, &env_info, UNIT_ITEMS, &gi_unit);
        if (rc)
        {
            ddi_clean_item(&gi);
            return rc;
        }
        unit = (FLAT_UNIT *)gi_unit.item;
        for (u=0; u < unit->items.var_units.count; u++)
        {
            if ((unit->items.var_units.list[u].desc_id == id_to_find) ||
                (unit->items.var_units.list[u].op_id == id_to_find))
            {
                /* We have found the matching unit relation */
                if (item_id_of_unit_rel)
                    *item_id_of_unit_rel = unit->id;
                if (item_id_of_unit_var)
                    *item_id_of_unit_var = unit->items.var.desc_id;

                ddi_clean_item(&gi_unit);
                ddi_clean_item(&gi);
                return 0;
            }
        }

        ddi_clean_item(&gi_unit);
    }
    ddi_clean_item(&gi);
    return DDI_TAB_BAD_DDID;
}


/*********************************************************************
 *
 *	Name: ddi_get_unit
 *	ShortDesc: get the item_id of the unit
 *
 *	Description:
 *		ddi_get_unit will return the item_id of the unit relation
 *		that contains the unit variable of the requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the parameter specifier
 *
 *	Outputs:
 *		item_id:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_unit(DDI_BLOCK_SPECIFIER *block_spec, DDI_PARAM_SPECIFIER *param_spec,
	ITEM_ID *item_id)
{

	int             rc;				/* return code */
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	ITEM_TBL       *it;				/* item table */
	ITEM_TBL_ELEM  *it_elem;		/* item table element */
	BLOCK_HANDLE    block_handle;	/* Block Handle */
	REL_TBL_ELEM   *rt_elem;		/* relation table element */
	int             rt_offset;		/* relation table offset */
	int             bint_offset;    /* bint offset */
	FLAT_DEVICE_DIR	*flat_device_dir ;

	ASSERT_RET(block_spec && param_spec && item_id, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(block_spec, param_spec, &block_handle,
				       &bt_elem, &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rt_offset);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	if (rt_offset == -1) {
		return DDI_TAB_NO_UNIT;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rt_offset);

	if (RTE_UNIT_IT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_UNIT;
	}

	rc = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it, RTE_UNIT_IT_OFFSET(rt_elem));

	*item_id = ITE_ITEM_ID(it_elem);
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_wao
 *	ShortDesc: get the item_id of the wao
 *
 *	Description:
 *		ddi_get_wao will return the item_id of the wao of the
 *		requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the param specifier
 *
 *	Outputs:
 *		item_id:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_wao(DDI_BLOCK_SPECIFIER *block_spec, DDI_PARAM_SPECIFIER *param_spec,
	ITEM_ID *item_id)
{

	int             rc;				/* return code */
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	ITEM_TBL       *it;				/* item table */
	ITEM_TBL_ELEM  *it_elem;		/* item table element */
	BLOCK_HANDLE    block_handle;	/* a Block Handle */
	int             rt_offset;		/* relation table offset */
	int             bint_offset;	/* bint offset */
	REL_TBL_ELEM   *rt_elem;		/* relation table element */
	FLAT_DEVICE_DIR	*flat_device_dir ;

	ASSERT_RET(block_spec && param_spec && item_id, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(block_spec, param_spec, &block_handle,
				       &bt_elem, &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rt_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * Use the Relation Table Offset to form the item id of the Write As
	 * One Relation.
	 */

	if (rt_offset == -1) {
		return DDI_TAB_NO_WAO;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rt_offset);

	if (RTE_WAO_IT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_WAO;
	}

	rc = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it, RTE_WAO_IT_OFFSET(rt_elem));
	*item_id = ITE_ITEM_ID(it_elem);
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_update_items
 *	ShortDesc: get the items of a relation
 *
 *	Description:
 *		ddi_get_update_items will return the list of item_ids associated
 *      with the right side of a relation.  This only affects wao and
 *      refresh relations.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:	a pointer to the relation request
 *
 *	Outputs:
 *		op_desc_list:	a pointer to the requested list of operational
 *						references (and descriptive references).
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_update_items(DDI_BLOCK_SPECIFIER *block_spec,
	DDI_PARAM_SPECIFIER *param_spec, OP_DESC_LIST *op_desc_list)
{

	int             rc;				/* return code */
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	REL_TBL_ELEM   *rt_elem;		/* relation table element */
	int             count;			/* number of relation items */
	int             rel_tbl_offset;	/* relation table offset */
	OP_DESC        *update_ptr;		/* update list pointer */
	int             bint_offset;    /* bint offset */
	int             ut_offset;		/* Update table offset */
	UPDATE_TBL     *update_tbl;		/* Update table */
	int             inc;			/* loop variable */
	BLOCK_HANDLE    block_handle;	/* A Block Handle */
	ITEM_TBL_ELEM  *op_it_elemp;	/* Operational reference */
	ITEM_TBL_ELEM  *desc_it_elemp;	/* Descriptive reference */
	ITEM_TBL       *it;
	FLAT_DEVICE_DIR	*flat_device_dir ;

	ASSERT_RET(block_spec && param_spec && op_desc_list, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(block_spec, param_spec, &block_handle,
				       &bt_elem, &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rel_tbl_offset);

	if (rc != DDS_SUCCESS) {
		return rc;
	}

	if (rel_tbl_offset == -1) {
		return DDI_TAB_NO_UPDATE;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rel_tbl_offset);

	if (RTE_UT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_UPDATE;
	}

	count = RTE_U_COUNT(rt_elem);
	ut_offset = RTE_UT_OFFSET(rt_elem);
	update_tbl = BTE_UT(bt_elem);

	update_ptr = (OP_DESC *) xrealloc((void *) op_desc_list->list,
		(size_t) count * sizeof(OP_REF_TRAIL));

	if (update_ptr == NULL) {
		return DDI_MEMORY_ERROR;
	}

	op_desc_list->list = update_ptr;
	op_desc_list->count = (unsigned short) count;

	rc  = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl ;

	for (inc = 0; inc < count; inc++, update_ptr++) {

		desc_it_elemp = ITE(it,
			UTE_DESC_IT_OFFSET(UTE(update_tbl, (ut_offset + inc))));
		update_ptr->desc_ref.id = ITE_ITEM_ID(desc_it_elemp);
		update_ptr->desc_ref.type = ITE_ITEM_TYPE(desc_it_elemp);

		op_it_elemp = ITE(it,
			UTE_OP_IT_OFFSET(UTE(update_tbl, (ut_offset + inc))));
		update_ptr->op_ref.subindex =
			UTE_OP_SUBINDEX(UTE(update_tbl, (ut_offset + inc)));
		update_ptr->op_ref.id = ITE_ITEM_ID(op_it_elemp);
		update_ptr->op_ref.type = ITE_ITEM_TYPE(op_it_elemp);

	}
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_clean_update_item_list
 *	ShortDesc: free the relation item id list
 *
 *	Description:
 *		ddi_clean_update_item_list() will free the relation item id list
 *		 malloced by ddi_get_update_items() call
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		update_list:		a pointer to the freed list
 *
 *	Returns: none
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
ddi_clean_update_item_list(OP_DESC_LIST *update_list)
{

	if (!update_list) {
		return;
	}

	xfree((void **) &update_list->list);
	update_list->count = 0;
	return;
}


/*********************************************************************
 *
 *	Name: ddi_get_var_type
 *	ShortDesc: get the variable type and size
 *
 *	Description:
 *		ddi_get_var_type will return the type and size of
 *		the given variable
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier structure
 *		param_spec:		a pointer to the parameter specifier structure
 *
 *	Outputs:
 *		type_size:		the loaded type/size structure
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_var_type(DDI_BLOCK_SPECIFIER *block_spec, DDI_PARAM_SPECIFIER *param_spec,
	TYPE_SIZE *type_size)
{

	BLK_TBL_ELEM   *bt_elem;      /* Block Table element pointer */
	BLOCK_HANDLE    block_handle; /* Block Handle */
	int				param_offset; /* Param Table offset */
	int				bint_offset;  /* Block Item Name Table offset */
	int				rec_mem_count;/* Temp count for record member count */
	int				arr_ele_count;/* Temp count for array element count */
	int				offset;       /* Temp count offset */
	int             rc;	          /* return code */

	ASSERT_RET(block_spec && param_spec && type_size, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(block_spec, param_spec,
								&block_handle, &bt_elem, &bint_offset);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * Handle characteristics individually
     */

	if (param_spec->type == DDI_PS_CHARACTERISTICS_SI) {

		rec_mem_count = CMT_COUNT(BTE_CMT(bt_elem));

		if ((param_spec->subindex >rec_mem_count) || (param_spec->subindex < 1)) {

			return DDI_TAB_BAD_SUBINDEX;
		}

		type_size->type =
			(ushort) CMTE_CM_TYPE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));
		type_size->size =
			(ushort) CMTE_CM_SIZE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));

		return DDS_SUCCESS;
	}

	param_offset = BINTE_PT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
	if (param_offset == -1) {

		return DDI_TAB_BAD_PARAM_OFFSET;
	}

	rec_mem_count  = PTE_PM_COUNT(PTE(BTE_PT(bt_elem), param_offset));
	arr_ele_count  = PTE_PE_MX_COUNT(PTE(BTE_PT(bt_elem), param_offset));

	switch (param_spec->type) {

	/*
	 * get type and size of parameter without subindex
	 */

	case DDI_PS_ITEM_ID:
	case DDI_PS_OP_INDEX:
	case DDI_PS_PARAM_NAME:
	case DDI_PS_PARAM_OFFSET:

		if((rec_mem_count) || (arr_ele_count)) {

			return DDI_INVALID_REQUEST_TYPE;		
		}

		type_size->type = (ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
		type_size->size = (ushort) PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
		break;

	/*
	 * get type and size of parameter with subindex
	 */

	case DDI_PS_ITEM_ID_SI:
	case DDI_PS_OP_INDEX_SI:
	case DDI_PS_PARAM_NAME_SI:
	case DDI_PS_PARAM_OFFSET_SI:

		if (rec_mem_count) {

			offset = PTE_PMT_OFFSET(PTE(BTE_PT(bt_elem), param_offset));

			if ((param_spec->subindex > rec_mem_count) || (param_spec->subindex < 1)) {

				return DDI_TAB_BAD_SUBINDEX;
			}

			type_size->type = (ushort)
				PMTE_PM_TYPE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));
			type_size->size = (ushort)
				PMTE_PM_SIZE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));

		} else {
			if (arr_ele_count) {

				type_size->type =
					(ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
				type_size->size =
					(ushort) PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
			}
			else {

				return DDI_INVALID_REQUEST_TYPE;
			}
		}
		break;

		default:
			rc = DDI_INVALID_REQUEST_TYPE;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_get_item_id
 *	ShortDesc: get the item id
 *
 *	Description:
 *		ddi_get_item_id will return the item id for the parameter
 *		name, or characteristic, or parameter list with member name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		item_id_req:	a pointer to the item id request structure
 *
 *	Outputs:
 *		item_id:		the item_id of the requested item
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_item_id(DDI_BLOCK_SPECIFIER *block_spec, DDI_ITEM_ID_REQUEST *item_id_req,
	ITEM_ID *item_id)
{

	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	int             rc;				/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */
	ITEM_TYPE       item_type;		/* Throw away item type */

	ASSERT_RET(block_spec && item_id_req && item_id, DDI_INVALID_PARAM);

	rc = conv_block_spec(block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	switch (item_id_req->type) {

	case DDI_ITEM_ID_PARAM_NAME:
		rc = tr_resolve_name_to_id_type(block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id, &item_type, 1);
		break;

	case DDI_ITEM_ID_PARAM_LIST_MEMBER:
		rc = tr_resolve_lname_pname_to_ddid(block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id_req->member_name,
			item_id);
		break;

	case DDI_ITEM_ID_PARAM_LIST_ID_MEMBER:
		rc = tr_resolve_lid_pname_to_id_type(block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id_req->member_name,
			item_id, &item_type);
		break;

	case DDI_ITEM_ID_CHARACTERISTIC:
		rc = tr_resolve_char_rec_to_ddid(block_handle, bt_elem, item_id);
		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;

	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_get_subindex
 *	ShortDesc: get the subindex of the member of the item
 *
 *	Description:
 *		ddi_get_subindex() will return the subindex of the
 *		item based on the inputs of item_id and member name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		item_id:		the item_id of the item to which the member belongs
 *		mem_name:		the item_id of the member
 *
 *	Outputs:
 *		subindex:		the subindex of the member
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_subindex(DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID item_id,
	ITEM_ID mem_name, SUBINDEX *subindex)
{

	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
	int             rc;				/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */

	ASSERT_RET(block_spec && item_id && mem_name && subindex, DDI_INVALID_PARAM);

	rc = conv_block_spec(block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	rc = tr_resolve_ddid_mem_to_si(bt_elem, item_id, mem_name, subindex);
	return rc;
}


/*********************************************************************
 *
 *	Name: param_spec_to_item_spec
 *
 *	ShortDesc: convert a DDI_PARAM_SPECIFIER into a DDI_ITEM_SPECIFIER
 *
 *	Description: 
 *              param_spec_to_item_spec() will convert a DDI_PARAM_SPECIFIER
 *              structure into a DDI_ITEM_SPECIFIER.  This is safe since a
 *              parameter is an item.
 *
 *	Inputs:
 *		param_spec:    a pointer to the param specifier
 *
 *	Outputs:
 *              item_spec:     a pointer to the item specifier        
 *
 *	Returns:
 *		returns DDI_INVALID_REQUEST_TYPE or DDS_SUCCESS
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
param_spec_to_item_spec(DDI_PARAM_SPECIFIER *param_spec, DDI_ITEM_SPECIFIER *item_spec)
{

  item_spec->subindex = param_spec->subindex;

  switch(param_spec->type) {

  case DDI_PS_ITEM_ID:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID;
    break;

  case DDI_PS_PARAM_NAME:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME;
    break;

  case DDI_PS_PARAM_OFFSET:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM;
    break;

  case DDI_PS_OP_INDEX:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX;
    break;

  case DDI_PS_ITEM_ID_SI:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID_SI;
    break;

  case DDI_PS_OP_INDEX_SI:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX_SI;
    break;

  case DDI_PS_PARAM_NAME_SI:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME_SI;
    break;

  case DDI_PS_PARAM_OFFSET_SI:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM_SI;
    break;

  default:
    return DDI_INVALID_REQUEST_TYPE;
    
  }
  return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_param_info
 *	ShortDesc: Get information about a parameter
 *
 *	Description:
 *		ddi_get_param_info() accepts a DDI_PARAM_SPECIFIER
 *		and fills in a PARAM_INFO structure with the parameter's
 *              information.
 *     
 *              The idea behind this function is that a user knows one piece
 *              of information about a parameter ( either the item id, 
 *              param name, param offset or object index ) and the user
 *              is interested in either some or all of the other information        
 *              about the parameter. 
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		param_spec:     a pointer to the param specifier
 *
 *	Outputs:
 *		param_info:		a pointer to the parameter information
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
ddi_get_param_info(DDI_BLOCK_SPECIFIER *block_spec,
	DDI_PARAM_SPECIFIER *param_spec, DDI_PARAM_INFO *param_info)
{

  int                rc;                 /* return code */
  BLK_TBL_ELEM       *bt_elem;           /* Block Table element pointer */
  BLOCK_HANDLE       block_handle;       /* Block Handle */
  int                bint_offset;       /* Block item Name Table Offset */
  DD_HANDLE          dd_handle;          /* device description handle */
  BLK_ITEM_NAME_TBL_ELEM *bint_elem;     /* blk item name tb elem */
  FLAT_DEVICE_DIR       *flat_device_dir ;
  ITEM_TBL              *it;            /* item table */
  ITEM_TBL_ELEM         *it_elem;       /* item table element */

  ASSERT_RET(block_spec && param_spec && param_info, DDI_INVALID_PARAM);
  
  rc = conv_block_spec(block_spec, &block_handle);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  rc = block_handle_to_bt_elem(block_handle, &bt_elem);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  /*
   * Get the Block Item Name Table Offset of the Parameter
   */

  rc = convert_param_spec_to_bint_offset(block_handle,param_spec, bt_elem,&bint_offset);
  if (rc != DDS_SUCCESS) {
    
    if (rc == DDI_TAB_NO_BLOCK) {
      
      /*
       * If convert_param_spec_to_bint_offset() fails because of no block,
       * request a load for the dd
       */
      
      rc = ds_dd_block_load(block_handle, &dd_handle);
      if (!rc) {
        
        rc = convert_param_spec_to_bint_offset(block_handle,param_spec, bt_elem,&bint_offset);
        if (rc != DDS_SUCCESS) {
			return rc;
        }
      }
      else {
        
        return DDI_BAD_DD_BLOCK_LOAD;
      }
      
    } else {
      return rc;
    }
  }
  
  bint_elem = BINTE(BTE_BINT(bt_elem), bint_offset);

  /* 
   * Fill in param offset
   */
  
  param_info->param_offset = BINTE_PT_OFFSET(bint_elem);

  /* 
   * Fill in item id
   */
  
  rc = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
  if (rc != SUCCESS) {
    return(rc) ;
  }
  it = &flat_device_dir->item_tbl;
  it_elem = ITE(it, BINTE_IT_OFFSET(bint_elem));
  param_info->item_id = ITE_ITEM_ID(it_elem);

  /* 
   * Fill in param name 
   */

  param_info->param_name = BINTE_BLK_ITEM_NAME(bint_elem);

  /* 
   * Fill in op index  
   */

  rc = tr_param_offset_to_op_index(block_handle,
                BINTE_PT_OFFSET(bint_elem), &param_info->op_index);

  /* 
   * Fill in item type
   */
  
  param_info->item_type = ITE_ITEM_TYPE(it_elem);
  return rc;
}

const char *getFilePart(const char *fullPath)
{
    /* Check for DOS/Win style path */
    char *ptr = strrchr(fullPath, '\\');
    if (ptr)
        return ptr+1;

    /* Check for UNIX style path */
    ptr = strrchr(fullPath, '/');
    if (ptr)
        return ptr+1;
    return fullPath;
}


int
ddi_get_string_encoding(ENV_INFO2 *env_info2, DDI_STRING_ENCODING *enc)
{
    int rc;
    unsigned long rod_ver;
    DD_HANDLE ddh;

    if ((!env_info2) || (!enc))
        return DDI_INVALID_PARAM;

    *enc = ENCODING_UNKNOWN;

    switch(env_info2->type)
    {
    case ENV_BLOCK_HANDLE:
            rc = get_abt_dd_handle (env_info2->block_handle, &ddh);
            if(rc)
                return rc;
	    ASSERT_RET(ddh.dd_handle_type == DDHT_ROD, DDI_INVALID_DD_HANDLE_TYPE);
        break;
    case ENV_DEVICE_HANDLE:
        rc = get_adt_dd_handle (env_info2->device_handle, &ddh);
        if(rc)
            return rc;
	    ASSERT_RET(ddh.dd_handle_type == DDHT_ROD, DDI_INVALID_DD_HANDLE_TYPE);
        break;
    default:
        return DDI_INVALID_DD_HANDLE_TYPE;
    }
	rod_ver = rod_get_version((ROD_HANDLE)ddh.dd_handle_number);
    if (rod_ver <= 0x0500)
        *enc = ISO_LATIN_1;
    else if (rod_ver == 0x0501)
        *enc = ENCODING_UNKNOWN;
    else
        *enc = UTF_8;
    return 0;
}

 /* Get maximum needed buffer size for conversion from Latin-1 to UTF-8 */
int ddi_max_utf8_size(char *pszIso)
{
    size_t isoLen;

    isoLen = strlen(pszIso);
    /* this is the worst case - allocate enough buffer. */
    return ((isoLen * 2) + 1);
}

/**
* Converts an ISO Latin-1 string to the corresponding UTF-8 encoding.
* 
* @param pszIso - address of the ISO Latin-1 string.
* @param pszOut - Output buffer where to store the converted UTF-8 
* encoded string.
* @param sizeOut - size of the output buffer. Minimum size of the
* output buffer is the length of the string referenced
* with the argument pszIso * 2 + 1;
* @return 0: success, -1: Invalid argument.
*/
int ddi_iso_to_utf8(char * pszIso, char * pszOut, size_t sizeOut)
{
    size_t isoLen;
    unsigned char ch;
    unsigned char *psz;
    
    if(pszIso == 0 || pszOut == 0 ) {
        return -1;
    }
    /** this is an assumption that has to be fulfilled. */
    if(sizeof(char) != sizeof(unsigned char)) {
        return -1;
    }
    
    /* check the size first */
    
    isoLen = strlen(pszIso);
    /* this is the worst case - allocate enough buffer. */
    if(sizeOut < ((isoLen * 2) + 1)) { 
        return -1;
    }
    psz = (unsigned char *) pszOut;
    while(*pszIso) {
        ch = (unsigned char) *pszIso;
        if(ch < 0xA0) {
            *psz = ch;
        }
        else if(ch < 0xC0) {
            *psz++ = 0xC2;
            *psz = ch;
        }
        else {
            *psz++ = 0xC3;
            *psz = (ch - 0x40); 
        }
        pszIso++;
        psz++;
    }
    *psz = 0;
    return 0;
}

/* params_are_conditional
 * Tell the caller whether the parameters in the specified block have been marked as conditional
 * by the Tokenizer.
 * Inputs: 
 *    env_info : the environment
 *    block_id : the ITEM ID of the block
 * Outputs:
 *    has_conditionals : IF no errors, nonzero if the block has conditionals
 * Returns:
 *    0     : successfully determined conditional state
 *  nonzero : unable to evaluate conditional state due to errors 
 * NOTES:
 *    If the DD was created by a Tokenizer prior to 5.2, the conditional state will not be available
 *    and the function will return an error.
 */
int params_are_conditional(ENV_INFO2 *env, ITEM_ID block_id, int *has_conditionals)
{
    DEVICE_HANDLE dh;
    int rs;
    ITEM_ID ff_id;

    /* Locate __ffddinfo in the device */
    rs = locate_symbol_id(env, "__ffddinfo", &ff_id);
    if (rs)
        return rs;


    /* Get the attributes for __ffddinfo.  First, locate a device handle */
    if (env->type == ENV_DEVICE_HANDLE)
        dh = env->device_handle;
    else
    {
        rs = get_abt_adt_offset(env->block_handle, &dh);
        if (rs)
            return rs;
    }
    {
        DDI_ITEM_SPECIFIER item_spec;
        ENV_INFO2 new_env_info;
        DDI_ATTR_REQUEST req_mask;
        DDI_GENERIC_ITEM gen_item;
        FLAT_VAR *flat_var;
        char *enum_str;
        int val;

        new_env_info.app_info = env->app_info;
        new_env_info.device_handle = dh;
        new_env_info.type = ENV_DEVICE_HANDLE;
        new_env_info.block_handle = 0;

        item_spec.type = DDI_ITEM_ID;
        item_spec.item.id = ff_id;
        
        memset(&gen_item, 0, sizeof(gen_item));

        req_mask = ALL_ITEM_ATTR_MASK;

        rs = ddi_get_item2(&item_spec, &new_env_info, req_mask, &gen_item);
        if (rs)
            return rs;

        /* Parse the attributes for __ffddinfo */
        if (gen_item.item_type != VARIABLE_ITYPE)
            return DDI_INVALID_TYPE;

        flat_var = (FLAT_VAR *)gen_item.item;
        /* Does the flags string for the block contain a conditional? */
        if (flat_var->type_size.type != DDS_ENUMERATED)
            return DDI_INVALID_TYPE;

        /* If there aren't enough enums, let the user know that we failed to evaluate */
        if (flat_var->enums.count < (1 + FFDDINFO_BLOCK_ATTRS_ENUM))
            return DDI_ILLEGAL_ENUM_VAR_VALUE;

        /* Find the Block Attributes enumeration string */
        enum_str = flat_var->enums.list[FFDDINFO_BLOCK_ATTRS_ENUM].desc.str;

        /* Parse the string for a nonzero conditional flag for our block */
        val = *has_conditionals = 0;
        rs = parse_block_attr_enum(enum_str, block_id, FFDDINFO_CONDITIONAL_FLAG, &val);
        if (rs)
            return rs;
        
        *has_conditionals = val;

        ddi_clean_item(&gen_item);
    }
    return 0;
}


int parse_block_attr_enum(char *enum_str, ITEM_ID block_id, char *flag_name, int *output_value)
{
    char *cur = enum_str;
    char *delim = "[](),";  // Delimiters in our string format
    char *delim2 = "=],";  // Delimiters in our string format within the value []
    ITEM_ID cur_id;

    if ((!enum_str) || (!flag_name) || (!output_value))
        return DDI_INVALID_PARAM;

    /* Parse the enumeration string, searching for the block_id specified */
    cur = strtok(enum_str, delim);

    while(cur)
    {
        if ((sscanf(cur, "0x%lx", &cur_id)) == 1)
        {
            // Discovered a block Item ID.   Is it the one requested by the caller?
            if (cur_id == block_id)
            {
                // Yes: get the string up to the next "]"
                cur = strtok(NULL, "[]");
                if (cur)
                {
                    // Find our requested flag in this value string
                    char *flag_str = strstr(cur, flag_name);
                    if (flag_str)
                    {
                        char *arg_ptr;
                        // Locate the value of this flag in the string
                        arg_ptr = strstr(flag_str, "=");
                        if (arg_ptr)
                        {
                            char *arg;
                            arg_ptr++; // Skip the "=" sign
                            arg = strtok(arg_ptr, delim2);  // Get the argument itself
                            if (arg)
                            {
                                if ((sscanf(arg, "%d", output_value)) != 1)
                                    return DDI_ILLEGAL_ENUM_VAR_VALUE;
                                
                                /*** Success: The output_value is now set ***/
                                return 0;
                            }
                        }
                    }
                }
            }
        }
        cur = strtok(NULL, delim);
    }
    return 0;
}

#if 0
    DEVICE_TYPE_HANDLE dth;
    /* Convert the device handle to a device-type handle */
    rs = get_adt_adtt_offset(dh, &dth);
    if (rs)
        return rs;
#endif









