#ifndef METH_TBL_H
#define METH_TBL_H

typedef struct methTable {
    char meth_disp_name[34];
    char meth_help_str[256];
    unsigned long meth_id;
    struct methodTable *next;
}methodTable;

typedef struct methTableList {
    char block_tag[34];
    int count;
    methodTable *meth_tbl;
}methTableList;

#endif
