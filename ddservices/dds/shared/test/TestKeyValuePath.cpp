//#include "this/package/foo.h"
#include "gtest/gtest.h"
//#include "../keyvaluepath.h"
#include "../kv_path.h"

#include <string>
using namespace std;

namespace {

// The fixture for testing class Foo.
class FooTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.

  FooTest() {
    // You can do set-up work for each test here.
  }

  virtual ~FooTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  // Objects declared here can be used by all tests in the test case for Foo.
};

// Test chompFirst functions first (they are used by the tests that follow)
TEST_F(FooTest, TestchompFirst)
{
    pair<string, string> path = KeyValueInterface::chompFirst("/device/rosey-644");

    EXPECT_EQ(true, path.first == "device");
    EXPECT_EQ(true, path.second == "/rosey-644");
}

TEST_F(FooTest, TestchompFirst_secondLevel)
{
    // Consume the entire path one key or value at a time....

    pair<string, string> path = KeyValueInterface::chompFirst("/device/rosey-644/block/transducer");
    EXPECT_EQ(true, path.first == "device");
    EXPECT_EQ(true, path.second == "/rosey-644/block/transducer");

    path = KeyValueInterface::chompFirst(path.second);

    EXPECT_EQ(true, path.first == "rosey-644");
    EXPECT_EQ(true, path.second == "/block/transducer");

    path = KeyValueInterface::chompFirst(path.second);

    EXPECT_EQ(true, path.first == "block");
    EXPECT_EQ(true, path.second == "/transducer");

    path = KeyValueInterface::chompFirst(path.second);

    EXPECT_EQ(true, path.first == "transducer");
    EXPECT_EQ(true, path.second == "");

    // path.second is empty, so both first and second should also be emtpy
    path = KeyValueInterface::chompFirst(path.second);

    EXPECT_EQ(true, path.first == "");
    EXPECT_EQ(true, path.second == "");
}

TEST_F(FooTest, TestchompFirst_no_slash)
{
    // note that there is no slash before 'device'
    pair<string, string> path = KeyValueInterface::chompFirst("device/rosey-644");

    EXPECT_EQ(true, path.first == "device");

    // since the first component didn't have a preceeding slash,
    // we expect the leading slash from the next component to be pruned, too
    EXPECT_EQ(true, path.second == "rosey-644");

    path = KeyValueInterface::chompFirst(path.second);

    EXPECT_EQ(true, path.first == "rosey-644");
    EXPECT_EQ(true, path.second == "");

    path = KeyValueInterface::chompFirst(path.second);

    // path should be fully consumed
    EXPECT_EQ(true, path.first == "");
    EXPECT_EQ(true, path.second == "");
}

TEST_F(FooTest, TestchompFirst_no_slash_single_word)
{
    // note that there is no slash before 'device'
    pair<string, string> path = KeyValueInterface::chompFirst("rosey-644");

    EXPECT_EQ(true, path.first == "rosey-644");
    EXPECT_EQ(true, path.second == "");
}

TEST_F(FooTest, TestInitializeKeyPath) {
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    EXPECT_EQ(true, root->fullPathName() == "/device");

    EXPECT_EQ(true, val1->fullPathName() == "/device/rosey-644");

    KeyPath* k = val1->addChild("block");
    ValuePath* val2 = k->addChild("transducer");

    cout << val2->fullPathName() << endl;
    EXPECT_EQ(true, val2->fullPathName() == "/device/rosey-644/block/transducer");

    ValuePath* val3 = val2->addPair("pvvar", "1");

    EXPECT_EQ(true, val3->fullPathName() == "/device/rosey-644/block/transducer/pvvar/1");

    delete root;
}

TEST_F(FooTest, TestKeyValueFinder_find)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    KeyPath* block = val1->addChild("block");
    (void) block->addChild("transformer");
    ValuePath* transducer = block->addChild("transducer");
    (void) block->addChild("AI");

    EXPECT_EQ(true, block->find("transducer") == transducer);
    EXPECT_EQ(true, block->find("/transducer") == 0);

    EXPECT_EQ(true, transducer->find("transducer") == 0);
    EXPECT_EQ(true, transducer->find("/transducer") == transducer);

    EXPECT_EQ(true, val1->find("block") == block);

    EXPECT_EQ(true, block->find("/block/transducer") == transducer);
    EXPECT_EQ(true, block->find("block/transducer") == 0);

    EXPECT_EQ(true, val1->find("block/transducer") == transducer);

    delete root;
}

TEST_F(FooTest, TestKeyValueFinder_find_and_getChildren)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    ValuePath* val2 = val1->addPair("block", "transducer");

    EXPECT_EQ(true, val2->fullPathName() == "/device/rosey-644/block/transducer");

    KeyValueInterface* block = root->find("/device/rosey-644/block");
    ASSERT_TRUE(block != 0);
    EXPECT_EQ("block", block->getName());

    vector<KeyValueInterface*> objs = block->getChildren();
    EXPECT_EQ(1, objs.size());

    EXPECT_EQ("transducer", objs[0]->getName());
    EXPECT_EQ("/device/rosey-644/block/transducer", objs[0]->fullPathName());

    delete root;
}

TEST_F(FooTest, TestKeyValueFinder_findChildren_and_getParent)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");
    EXPECT_EQ(true, root->getParent() == 0);

    (void) val1->addPair("block", "transformer");
    ValuePath* val2 = val1->addPair("block", "transducer");

    EXPECT_EQ(true, val2->fullPathName() == "/device/rosey-644/block/transducer");

    vector<KeyValueInterface*> objs = root->findChildren("/device/rosey-644/block");
    EXPECT_EQ(2, objs.size());

    EXPECT_EQ(true, objs[0]->getParent()->getName() == "block");
    EXPECT_EQ(true, objs[1]->getParent()->getName() == "block");

    EXPECT_EQ("transformer", objs[0]->getName());
    EXPECT_EQ("/device/rosey-644/block/transformer", objs[0]->fullPathName());

    EXPECT_EQ("transducer", objs[1]->getName());
    EXPECT_EQ("/device/rosey-644/block/transducer", objs[1]->fullPathName());

    delete root;
}

TEST_F(FooTest, TestKeyValueFinder_findFirstChild)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    (void) val1->addPair("block", "transducer");
    (void) val1->addPair("block", "resource");

    string child = val1->queryFirstChild("block");

    EXPECT_EQ(true, child == "transducer");

    delete root;
}


//TEST_F(FooTest, TestKeyValueFinder_query_non_root)
//{
//    KeyPath* root = new KeyPath("device");
//    ValuePath* val1 = root->addChild("rosey-644");
//
//    (void) val1->addPair("block", "transducer");
//    (void) val1->addPair("block", "resource");
//
//    KeyValueInterface::debug_printTree(val1);
//    vector<KeyValueInterface*> objs = val1->query("/block");
//
//    EXPECT_EQ(1, objs.size());
//
//    EXPECT_EQ("block", objs[0]->getName());
//
//    delete root;
//}

//TEST_F(FooTest, TestKeyValueFinder_query_bad)
//{
//    KeyPath* root = new KeyPath("device");
//    ValuePath* val1 = root->addChild("rosey-644");
//
//    (void) val1->addPair("block", "transducer");
//    (void) val1->addPair("block", "resource");
//
//    vector<KeyValueInterface*> objs = root->query("/device/rosey-644/bad");
//
//    EXPECT_EQ(0, objs.size());
//
//    delete root;
//}
//
//TEST_F(FooTest, TestKeyValueFinder_query_bad2)
//{
//    KeyPath* root = new KeyPath("device");
//    ValuePath* val1 = root->addChild("rosey-644");
//
//    (void) val1->addPair("block", "transducer");
//    (void) val1->addPair("block", "resource");
//
//    vector<KeyValueInterface*> objs = root->query("/device/rosey-644/block/ai1");
//
//    EXPECT_EQ(0, objs.size());
//
//    delete root;
//}
//
//TEST_F(FooTest, TestKeyValueFinder_query_bad3)
//{
//    KeyPath* root = new KeyPath("device");
//    ValuePath* val1 = root->addChild("rosey-644");
//
//    (void) val1->addPair("block", "transducer");
//    (void) val1->addPair("block", "resource");
//
//    vector<KeyValueInterface*> objs = root->query("/device/rosey-655/block");
//
//    EXPECT_EQ(0, objs.size());
//
//    delete root;
//}

TEST_F(FooTest, Test_C_Interface)
{
    KeyPath* kv_path = new KeyPath("device");

    ValuePath* kv_device;

    const char* pd_tag = "pd_tag";
    const char* dev_id = "dev_id value";
    unsigned char station_address = 242;

    kv_device = kv_add_child_value(kv_path, pd_tag);

    (void) kv_add_pair_str(kv_device, "dev_id", dev_id);
    (void) kv_add_pair_uint8(kv_device, "station_address", station_address);

    vector<KeyValueInterface*> objs = kv_path->getChildren();
    EXPECT_EQ(1, objs.size());
    EXPECT_EQ("pd_tag", objs[0]->getName());

    KeyValueInterface* dev = kv_path->find("/device/pd_tag");
    ASSERT_TRUE(dev != 0);

    objs = dev->getChildren();
    EXPECT_EQ(2, objs.size());
    EXPECT_EQ("dev_id", objs[0]->getName());
    EXPECT_EQ("station_address", objs[1]->getName());

    dev = kv_path->find("/device/pd_tag/dev_id");
    ASSERT_TRUE(dev != 0);

    objs = dev->getChildren();
    EXPECT_EQ(1, objs.size());
    EXPECT_EQ("dev_id value", objs[0]->getName());

    dev = kv_path->find("/device/pd_tag/station_address");
    ASSERT_TRUE(dev != 0);

    objs = dev->getChildren();
    EXPECT_EQ(1, objs.size());
    EXPECT_EQ("242", objs[0]->getName());

    delete kv_path;

}

TEST_F(FooTest, Test_C_Interface_with_spaces)
{
    KeyPath* kv_path = new KeyPath("device");

    ValuePath* kv_device;
    const char* pd_tag = "RSMTTEMP-3244-446016";

    // here we prepend/append spaces to the value.
    // we expect that the spaces will be trimmed when saved.
    kv_device = kv_add_child_value(kv_path,
        ("  " + string(pd_tag) + "        ").c_str());

    KeyValueInterface::debug_printTree(kv_path);

    // we shouldn't see any spaces
    EXPECT_EQ(true, kv_path->childExists(pd_tag));

    delete kv_path;
}


TEST_F(FooTest, TestKeyValueFinder_childExists)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    (void) val1->addPair("block", "transducer");
    (void) val1->addPair("block", "resource");

    KeyValueInterface* block = val1->find("block");
    ASSERT_TRUE(block != 0);

    EXPECT_EQ(2, block->getChildren().size());

    EXPECT_EQ(true, block->childExists("transducer"));
    EXPECT_EQ(true, block->childExists("resource"));
    EXPECT_EQ(false, block->childExists("reduction"));
    EXPECT_EQ(false, block->childExists("transformer"));

    delete root;
}

TEST_F(FooTest, TestKeyValueFinder_indexOfChild)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    ValuePath* blockTransducer = val1->addPair("block", "transducer");
    ValuePath* blockResource = val1->addPair("block", "resource");

    KeyValueInterface* block = val1->find("block");
    ASSERT_TRUE(block != 0);

    EXPECT_EQ(2, block->getChildren().size());

    // using string searches
    EXPECT_EQ(0, block->indexOfChild("transducer"));
    EXPECT_EQ(1, block->indexOfChild("resource"));
    EXPECT_EQ(-1, block->indexOfChild("I'm not here"));

    // using pointer searches
    EXPECT_EQ(0, block->indexOfChild(blockTransducer));
    EXPECT_EQ(1, block->indexOfChild(blockResource));
    EXPECT_EQ(-1, block->indexOfChild(val1)); // not a child of block

    delete root;
}

TEST_F(FooTest, TestKeyValue_merge)
{
    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");
    (void) val1->addPair("block", "transducer");

    KeyValueInterface* rosey = root->getChild("rosey-644");

    EXPECT_EQ(1, root->getChildren().size());
    EXPECT_EQ(1, rosey->getChildren().size());

    KeyPath* root2 = new KeyPath("device");
    ValuePath* val2 = root2->addChild("rosey-644");
    (void) val2->addPair("block", "resource");

    root->merge(root2);
    KeyValueInterface::debug_printTree(root);

    EXPECT_EQ(1, root->getChildren().size());
    EXPECT_EQ(1, rosey->getChildren().size());

    EXPECT_EQ(true, root->childExists("rosey-644"));

    KeyValueInterface* block = rosey->find("block");
    ASSERT_TRUE(block != 0);
    EXPECT_EQ(2, block->getChildren().size());

    ASSERT_TRUE(block->find("transducer") != 0);
    ASSERT_TRUE(block->find("resource") != 0);

    delete root2;
    delete root;
}

TEST_F(FooTest, TestKeyValue_mergeConflict)
{
    KeyPath* root1 = new KeyPath("device");
    ValuePath* val1 = root1->addChild("rosey-644");
    val1->addPair("timeLastSeen", "today");

    KeyPath* root2 = new KeyPath("device");
    ValuePath* val2 = root2->addChild("rosey-644");
    val2->addPair("timeLastSeen", "yesterday");

    KeyValueInterface::debug_printTree(root1);
    cout << endl;
    root1->merge(root2);
    KeyValueInterface::debug_printTree(root1);

    KeyValueInterface* rosey = root1->find("/device/rosey-644");
    EXPECT_EQ(true, rosey->queryLastChild("timeLastSeen") == "yesterday");

    delete root2;
    delete root1;
}

TEST_F(FooTest, TestKeyValue_remove)
{
    KeyPath* root1 = new KeyPath("device");
    ValuePath* val1 = root1->addChild("rosey-644");
    ValuePath* someDate = val1->addPair("timeLastSeen", "someDate");
    ValuePath* oldDate = val1->addPair("timeLastSeen", "yesterday");
    ValuePath* newDate = val1->addPair("timeLastSeen", "today");

    KeyValueInterface* timeEntries = root1->find("/device/rosey-644/timeLastSeen");
    EXPECT_EQ(3, timeEntries->numChildren());

    cout << "before remove:" << endl;
    KeyValueInterface::debug_printTree(root1);

    root1->remove(oldDate);
    cout << "after remove:" << endl;
    KeyValueInterface::debug_printTree(root1);
    EXPECT_EQ(2, timeEntries->numChildren());

    KeyValueInterface* rosey = root1->find("/device/rosey-644");
    EXPECT_EQ(true, rosey->queryFirstChild("timeLastSeen") == "someDate");
    EXPECT_EQ(true, rosey->queryLastChild("timeLastSeen") == "today");

    root1->remove(timeEntries);
    cout << "after removing timeLastSeen & children..." << endl;
    KeyValueInterface::debug_printTree(root1);

    EXPECT_EQ(true, root1->find("/device/rosey-644/timeLastSeen") == 0);

    delete root1;
}

TEST_F(FooTest, TestKeyValue_firstAndLastChild)
{
    KeyPath* root1 = new KeyPath("device");
    ValuePath* val1 = root1->addChild("rosey-644");
    ValuePath* someDate = val1->addPair("timeLastSeen", "someDate");
    ValuePath* oldDate = val1->addPair("timeLastSeen", "yesterday");
    ValuePath* newDate = val1->addPair("timeLastSeen", "today");

    KeyValueInterface* timeEntries = root1->find("/device/rosey-644/timeLastSeen");
    EXPECT_EQ(3, timeEntries->numChildren());

    KeyValueInterface* first = timeEntries->firstChild();
    ASSERT_TRUE(first != 0);

    KeyValueInterface* last = timeEntries->lastChild();
    ASSERT_TRUE(last != 0);

    // ask for first/last child of a node that has no children
    // Note: this shouldn't segfault, but it should return nullptr

    KeyPath* root2 = new KeyPath("device");

    KeyValueInterface* first2 = root2->firstChild();
    ASSERT_TRUE(first2 == 0);

    KeyValueInterface* last2 = root2->firstChild();
    ASSERT_TRUE(last2 == 0);

    delete root2;
    delete root1;
}

TEST_F(FooTest, TestKeyValue_queryChildrenFalseChecks)
{
    KeyPath* root1 = new KeyPath("device");

    // query for a child that not only doesn't exist,
    // query an object that doesn't have children at all

    EXPECT_EQ(true, root1->queryFirstChild("bogus") == "");
    EXPECT_EQ(true, root1->queryLastChild("bogus") == "");

    delete root1;
}


/***********************************************/
}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
