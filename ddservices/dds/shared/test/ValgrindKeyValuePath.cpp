#include "gtest/gtest.h"
#include "../keyvaluepath.h"

#include <string>
using namespace std;


// tests the remove() function for memory leaks
void test_remove()
{
    KeyPath* root1 = new KeyPath("device");
    ValuePath* val1 = root1->addChild("rosey-644");
    ValuePath* someDate = val1->addPair("timeLastSeen", "someDate");
    ValuePath* oldDate = val1->addPair("timeLastSeen", "yesterday");
    ValuePath* newDate = val1->addPair("timeLastSeen", "today");

    KeyValueInterface* timeEntries = root1->find("/device/rosey-644/timeLastSeen");

    root1->remove(oldDate);

    delete root1;
}


// Tests that the Foo::Bar() method does Abc.
int main(int argc, char **argv)
{
    //int* nums = new int[5];
    //int* ptr = nums;
    //delete [] ptr;

    KeyPath* root = new KeyPath("device");
    ValuePath* val1 = root->addChild("rosey-644");

    cout << val1->fullPathName() << endl;

    KeyPath* k = val1->addChild("block");
    ValuePath* val2 = k->addChild("transducer");

    cout << val2->fullPathName() << endl;

    ValuePath* val3 = val2->addPair("pvvar", "1");

    cout << val3->fullPathName() << endl;

    delete root;

    //////////////////////////////////////////////////

    //
    // other memory checks, in the form of function calls
    //

    test_remove();

    return 0;
}
