#ifndef STATUS_CODES_H
#define STATUS_CODES_H

#define MAX_QUALITY             4
#define MAX_SUB_STATUS          10
#define MAX_STR_LEN             32

#define MAX_LIMIT               4

/* Quality of the Process Variable */
#define QLTY_GOOD_CASCADE       3
#define QLTY_GOOD_NON_CASCADE   2
#define QLTY_UNCERTAIN          1
#define QLTY_BAD                0

#define PROCESS_STATUS_STR_LEN  100

const char quality_code[MAX_LIMIT][MAX_STR_LEN] = {
    "Bad",
    "Uncertain",
    "Good Non Cascade",
    "Good Cascade"
};

const char status_code[MAX_QUALITY][MAX_SUB_STATUS][MAX_STR_LEN] = {
    /* Bad Sub status */
    "Non Specific",
    "Configuration Error",
    "Not Connected",
    "Device Failure",
    "Sensor Failure",
    "No Communication",
    "No Communication",
    "Out of Service",
    "Transducer in MAN",
    "SIS",
    /* Uncertain Sub Status */
    "Non Specific",
    "Last Usable Value",
    "Substitute",
    "Initial Value",
    "Sensor Conversion Not Accurate",
    "Engg Unit Range Violation",
    "Sub-normal",
    "Transducer in MAN",
    "Invalid sub state",
    "Invalid sub state",
    /* Good Non Cascade sub status */
    "Non Specific",
    "Active Block Alarm",
    "Active Advisory Alarm",
    "Active Critical Alarm",
    "Un acknowledged Block Alarm",
    "Un acknowledged Advisory Alarm",
    "Un acknowledged Critical Alarm",
    "Reserved",
    "Initiate Fault State",
    "Invalid sub status",
    /* Good Cascade sub Status */
    "Non Specific",
    "Initialization Acknowledge",
    "Initialization Request",
    "Not Invited",
    "Not Selected",
    "Reserved",
    "Local Override",
    "Fault state Active",
    "Initiate Fault State",
    "Invalid sub status"
};

const char limit_code[MAX_LIMIT][MAX_STR_LEN] = {
    "Not Limited",
    "Low Limited",
    "High Limited",
    "Constant"
};


#endif
