/*****************************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Matthew Kraus
    Origin:            Hutch
*/

/** @file
    Key-value paths are a way to store hierarchal string information.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#ifndef KEYVALUEPATH_H
#define KEYVALUEPATH_H

/* INCLUDE FILES ************************************************************/

#include <string>
#include <vector>

/* CLASS FORWARDS ***********************************************************/

class KeyPath;
class ValuePath;

/*==========================================================================*/
/** @class KeyValueInterface
@brief A KeyValueInterface is a generic tree for representing hierarchal data.

A KeyValueInterface is the interface that can be used to manipulate nodes
in a "key-value tree." A @b key-value @b tree is very similar to the directory
structure in a Linux file system. Just like in a file system, there is
exactly one root, and likewise, all files in a specific directory must have
unique names. However, there are also a few differences.

## Keys and Values

The tree structure is designed to support key-value pairs. The key-value
relationship is enforced by alternating node types. In other words,
for any KeyPath node, all of its children are ValuePath nodes. Therefore,
the KeyPath is the key, and it can have multiple ValuePath nodes which
belong to its parent (which is a key). For a given key, it might not have
any values associated with it, it might have only one value, or it might
have multiple values assocaited with it. The value part of a key-value
pair is really a *multi-value*.

## Alternating Structure of Key and Value Nodes

A KeyPath is always the root of the key-value tree. The root can
have one or more ValuePath nodes as children. Parent nodes and
child nodes alternate in type. Here is a visual representation of a
valid key-value tree:

@dot
graph {
    a -- b -- c
    a -- d
    a -- e
    c -- f
    e -- g
    e -- h
    a [shape=suqre, color=lightblue, style=filled, label="KeyPath"];
    b [shape=suqre, color=orange, style=filled, label="ValuePath"];
    c [shape=suqre, color=lightblue, style=filled, label="KeyPath"];
    d [shape=suqre, color=orange, style=filled, label="ValuePath"];
    e [shape=suqre, color=orange, style=filled, label="ValuePath"];
    f [shape=suqre, color=orange, style=filled, label="ValuePath"];
    g [shape=suqre, color=lightblue, style=filled, label="KeyPath"];
    h [shape=suqre, color=lightblue, style=filled, label="KeyPath"];
}
@enddot

## Properties of a Key-Value Tree

Note the following properties of a key-value tree:

-# The structure is a *generic* tree with exactly *one* root.
-# The root is always a KeyPath. A ValuePath cannot be the root.
-# Any node may have 0, 1, 2...n (zero or more) child nodes.
-# Every child of a KeyPath must be a ValuePath.
-# Every child of a ValuePath must be a KeyPath.

## Creating a New Key-Value Tree
It all begins by creating a KeyPath.

### Example

@code
KeyPath* root = new KeyPath("device");
@endcode

The root KeyPath must be tracked like any other resource and freed using
the @b delete operator. Forgetting to call @b delete will result in a
memory leak. However, deleting the root will do the "right thing" and
properly free all of its children.

It is not possible to instantiate a ValuePath directly, nor is it
possible to instatiate a KeyValueInterface.

@warning
Never call @b delete on a KeyValueInterface * or a ValuePath *.

@warning
You should never explicitly call @b delete on pointers
to KeyPath objects other than the root.

## Adding Child Nodes

Child nodes can be added to any node. To add a child, call addChild():

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");
@endcode

We now have a (small) key-value path that looks like this:

@dot
graph {
    a -- b;
    a [shape=none, color=lightblue, style=filled, label=<
        <b>device</b>
        <br/>
        <font point-size="12">(KeyPath)</font>
    >];

    b [shape=none, color=orange, style=filled, label=<
        <b>rosey-644</b>
        <br/>
        <font point-size="12">(ValuePath)</font>
    >];
}
@enddot


## Ordering Property of Child Nodes

All child nodes are stored in the order that they were added. Therefore,
when addChild() or addPair() are called, the children are added to the
end of the existing child list (should the node already have children).
This means that when firstChild() is called, it will always returned the
oldest child that was added--this child will normally be the first child
that was added to that node.

### Removing Children

When a child node is removed with remove(), the order of the removed child's
siblings is preserved.



*/
class KeyValueInterface
{
public:
    /** Returns the root of the entire tree. */
    KeyPath* findTopmostParent();

    /** Returns the fully qualified path name. */
    std::string fullPathName() const;

    /** Get the name of the current node. */
    std::string getName() const;

    /** This overloaded version is used for the C interface to this class. */
    virtual KeyValueInterface* addChild(
        std::string component,
        KeyValueInterface *parent
    );

    /** Finds the KeyPath or ValuePath matching the given query. */
    KeyValueInterface* find(std::string keyOrValueName);

    /** Finds the children of the specified key or value. */
    const std::vector<KeyValueInterface*>&
        findChildren(std::string keyOrValueName);

    /** Look for the specified child. */
    bool childExists(std::string childName) const;

    /** Given the name of a child, find the index in the child list. */
    int indexOfChild(std::string childName);

    /** Given the node of a child, return the child's index in
        the child list. */
    int indexOfChild(KeyValueInterface* child);

    /** Returns true if this node has children. */
    bool hasChildren() const;

    /** Returns the number of children. */
    size_t numChildren() const;

    /** Returns the child with the given name, or null if
        it doesn't exist. */
    KeyValueInterface* getChild(std::string value) const;

    /** Returns the list of all children. */
    const std::vector<KeyValueInterface*>& getChildren() const;

    /** Returns the first child of this node. */
    KeyValueInterface* firstChild();

    /** Returns the last child of this node. */
    KeyValueInterface* lastChild();

    /** Retruns the string name of the first child of the given query. */
    std::string queryFirstChild(std::string pathQuery);

    /** Retruns the string name of the last child of the given query. */
    std::string queryLastChild(std::string pathQuery);

    /** Returns the parent of this node. */
    KeyValueInterface* getParent() const;

    /** Merges the other tree into this one. */
    void merge(KeyValueInterface* other);

    /** Removes the child that is pointed to. */
    void remove(KeyValueInterface* child);

    /** Deletes all children (recursively). */
    void deleteChildren();

    /** A constant that represents an empty child list of key-value paths.
    This is returned by findChildren() if no children exist. */
    static const std::vector<KeyValueInterface*> emptyChildList;

    /** Separate the head of the path from the rest of the path. */
    static std::pair<std::string, std::string> chompFirst(std::string path);

    /** Prints the entire tree in path form. */
    static void debug_printTree(KeyValueInterface* root);

protected:    
    /** The constructor is not available for public use. */
    KeyValueInterface();

    /** Deleting a KeyValueInterface is not allowed from
        the public interface */
    virtual ~KeyValueInterface();

    /** The component holds the name of this node. The component, or
        "path component," is the name of this node. Once it has
        been set, it cannot be changed. */
    std::string component;

    /** Points to the parent of this node. A parent that is null is
        the top-most parent. */
    KeyValueInterface* parent;

    /** A list of all children of this node. Since this is a generic tree,
        a node may have 0 or more children. */
    std::vector<KeyValueInterface*> children;

private:
    /** Allocates a child on the heap with the given name and assigns
        it to parent. */
    virtual KeyValueInterface* newChild(
        std::string component,
        KeyValueInterface* parent
    ) = 0;

    // copy & copy-assignment are disabled
    explicit KeyValueInterface(const KeyValueInterface& other);  // = delete // TODO: C++11/14
    KeyValueInterface& operator=(KeyValueInterface&);  // = delete // TODO: C++11/14
};


/****************************************************************************/


/** @class KeyPath
@brief A KeyPath represents the key of the key-value pair hierarchy.

Although KeyValueInterface exposes the majority of the functions available,
creating a KeyPath is the @b only way to make a new key-value hierarchy.
Note that of the three (KeyValueInterface, KeyPath, and ValuePath), only
a KeyPath can be publicly constructed.

## Relationship between interfaces

The relationship between a KeyValueInterface, KeyPath, and ValuePath is
as follows. A KeyValueInterface can point to either a KeyPath or a ValuePath.
Once a KeyPath is created, you can call KeyPath::addChild(std::string) and
create a child node, which will be a ValuePath object. This object should
not be deleted explicitly; addChild() automatically adds the new child to
the hierarchy, and it will be deleted when the topmost KeyPath is destroyed.
*/
class KeyPath : public KeyValueInterface
{
public:

    /** Create a new KeyPath. */
    explicit KeyPath(std::string key);

    /** Removes all children of this KeyPath */
    virtual ~KeyPath();

    /** Creates a new child and adds it to the hierarchy. */
    ValuePath* addChild(std::string value);

private:
    KeyPath() {}

    explicit KeyPath(std::string key, KeyValueInterface* parent);

    virtual KeyValueInterface* newChild(std::string key, KeyValueInterface* parent);

    friend class ValuePath;
};


/****************************************************************************/


/**
@class ValuePath
@brief A ValuePath represents the value of the key-value pair hierarchy.

Conceptually, a key can have one value, or many values. The invariant
that the key-value tree system maintains is that for any given node,
all the children must be unique; that is, duplicate siblings are not
allowed.

Most of the functionality of a ValuePath is implemented and exposed
through the KeyValueInterface; only two functions are unique to
ValuePath: addChild(), which returns a KeyPath object/pointer, and
addPair().

## Creating a ValuePath

A value path can not be explicitly constructed; it must be created by the
KeyPath::addChild() function, or by the ValuePath::addPair() function.

*/
class ValuePath : public KeyValueInterface
{
public:
    /** Cleans up all children as well as this node. */
    virtual ~ValuePath();

    /** Creates a new child and adds it to the hierarchy. */
    KeyPath* addChild(std::string value);

    /** Creates a key-value pair, returning the value pointer. */
    ValuePath* addPair(std::string key, std::string value);

protected:

private:
    ValuePath() {}

    explicit ValuePath(const ValuePath& other); // disabled

    virtual KeyValueInterface* newChild(std::string value, KeyValueInterface* parent) { return new KeyPath(value, parent); }

    friend class KeyPath;
};

#endif // KEYVALUEPATH_H
