#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <vector>


template <typename T>
class CircularBuffer
{
public:
    explicit CircularBuffer(size_t max_elements);

    bool Add(T element);
    bool RemoveLast(T* popedElement);
    bool isFull();
    bool isEmpty();
    size_t size() { return current_size; }

    T& operator[](size_t idx);
    const T& operator[](size_t idx) const;

    std::vector<T> buffer; // TODO: make private
    size_t tail;           // TODO: make private
private:
    size_t head;

    size_t capacity;
    size_t current_size;

    size_t offset(size_t index);
};

template <typename T>
CircularBuffer<T>::CircularBuffer(size_t max_elements) :
    buffer(max_elements),
    tail(0),
    head(0),
    capacity(max_elements),
    current_size(0)
{
}

template <typename T>
size_t CircularBuffer<T>::offset(size_t index)
{
    size_t offset;
    offset = tail + index;
    if (offset >= capacity) offset -= capacity;

    return offset;
}

template <typename T>
T& CircularBuffer<T>::operator[](size_t idx)
{
    return buffer[offset(idx)];
}

template <typename T>
const T& CircularBuffer<T>::operator[](size_t idx) const
{
    return buffer[offset(idx)];
}

template <typename T>
bool CircularBuffer<T>::isFull()
{
    return current_size >= capacity;
}

template <typename T>
bool CircularBuffer<T>::isEmpty()
{
    return current_size == 0;
}

template <typename T>
bool CircularBuffer<T>::Add(T element)
{
    if (this->isFull()) return false;

    buffer[head] = element;
    ++current_size;

    ++head;
    if (head >= capacity)
    {
        head = 0;
    }

    return true;
}

template <typename T>
bool CircularBuffer<T>::RemoveLast(T* popedElement)
{
    if (isEmpty()) return false;

    *popedElement = buffer[tail];
    --current_size;

    ++tail;
    if (tail >= capacity)
    {
        tail = 0;
    }

    return true;
}

#endif // CIRCULARBUFFER_H
