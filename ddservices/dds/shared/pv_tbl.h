#ifndef PV_TBL_H
#define PV_TBL_H

typedef struct ProcessValTable {
    char pv_disp_name[34];
    char pv_sym_name[34];
    float pv_value;
    unsigned long pv_id;
    unsigned char status;
    /* Process value unit information */
    unsigned int unit_val;
    int          unit_si; 
    unsigned long unit_id;
    char unit_name[34];
    /* Process value High range information */
    char pv_hi_range_disp_name[34];
    unsigned long pv_hi_range_id;
    float pv_hi_range_val;
    int          pv_hi_range_si; 
    /* Process value Low range information */
    char pv_low_range_disp_name[34];
    unsigned long pv_low_range_id;
    float pv_low_range_val;
    int          pv_low_range_si; 
    struct ProcessValTable *next;
}ProcessValTable;

typedef struct ProcessValTableList {
    char block_tag[34];
    int count;
    ProcessValTable *pv_tbl;
}ProcessValTableList;


typedef struct {
    unsigned long item_id;
    char str[64];
    int bh;
}SavePvItem;

typedef struct {
    unsigned long item_id;
    int sub_index;
    unsigned short type;
    union {
        float           f;
        double          d;
        unsigned long   u;
        long            i;
        char            str[64];
        unsigned short  sa[4];
        char            ca[8];
    }val;
    int             day;
    int             mon;
    int             yr;
    int             hr;
    int             min;
    int             sec;
    int             time_type;
    int             weekday;
    int             days;
    int             ms;
}VARIABLE_VAL;


typedef struct {
    int count;
    VARIABLE_VAL *var_val; 
}VARIABLES;

#endif
