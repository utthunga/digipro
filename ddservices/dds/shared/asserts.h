/*****************************************************************************
    Copyright (c) 2009 Fluke Corporation. All rights reserved.
******************************************************************************

    File           - $URL: https://engsvnhost.tc.fluke.com/repos/igdev.dmm/wallace/branches/hw_revx/trunk/src/arm/shared/asserts.h $
    Version Number - $Revision: 31222 $
    Last Updated   - $Date: 2012-11-02 15:58:17 -0700 (Fri, 02 Nov 2012) $
    Updated By     - $Author: jody $
    Authored By    - Tony Garland
*/

/** @file
    ASSERT and VERIFY Macros

    @par ASSERT vs. VERIFY

    The ASSERT macro declaration follows an ISO standard.  See
    http://www.opengroup.org/onlinepubs/000095399/basedefs/assert.h.html

    The ASSERT macro, and its variations, check a runtime condition within
    development builds which is never expected to occur within properly
    written code. Thus, the condition is not checked in production code.
    Failed ASSERTs represent serious logic flaws rather than expected
    runtime error conditions (which should not be detected using
    the ASSERT macro). The statement within the ASSERT macro is completely
    omitted from production builds.

    The VERIFY macro, and its variations, also check a runtime condition
    within development builds which is never expected to occur in properly
    written code. Unlike the ASSERT macro, the statement within the
    VERIFY macro is included in production builds. Thus, the VERIFY macro
    is used for situations where a check is to be made in development builds,
    but where the code which is part of the check must still execute in
    production builds (e.g., it has a side effect).

    Examples:

    <pre>
                            +----------------------------------------------+
                            |                   GENERATED CODE             |
    ------------------------+---------------------------+------------------+
    | EXAMPLE               | DEVELOPMENT BUILD         | PRODUCTION BUILD |
    +-----------------------+---------------------------+------------------+
    | ASSERT(x);            | assert(x);                |                  |
    | ASSERT_PTR(x);        | assert(NULL!=(x));        |                  |
    +-----------------------+---------------------------+------------------+
    | VERIFY(x++);          | assert(x++);              | x++;             |
    | VERIFY_PTR(p = new X);| assert(NULL!=(p = new X));| p = new X;       |
    -----------------------------------------------------------------------

    </pre>

    Notice that the ASSERT macro generates no code output for production
    builds. Therefore, invoking the ASSERT macro on a statement with side
    effects is an error:

    @code

        // DON'T USE ASSERT WHEN THERE ARE SIDE-EFFECTS: USE VERIFY INSTEAD!
        ASSERT(x++);
        ASSERT_PTR(p = new X);

    @endcode

    @par VERIFY, Pointers, and Side-Effects

    The VERIFY_PTR macro is especially useful in situations where 
    it is impractical to insert code between an incoming pointer and its
    assignment, for example in an initializer list:

    @code

        MyClass::MyClass(
            Foo* pFoo,
            Bar* pBar
        ) :
            m_pFoo(VERIFY_PTR(pFoo)),
            m_bar(*VERIFY_PTR(pBar))
            ...
    @endcode

    In order to be used in this way, the macro is carefully written to
    avoid duplicating side effects. Otherwise, statements such as:

    @code

        Foo* pFoo = VERIFY_PTR(new foo); // Likely and useful.

        pThisFoo = VERIFY_PTR(pFoo++);   // Unlikely, but possible.

    @endcode
 
    will have unexpected results because the single macro argument gets 
    evaluated multiple times. In the first example, two allocations could
    occur within the macro, but only one would be remembered--leading to
    wasted memory and leaks. In the second example, the pointer could be
    incremented twice.
    
    Note several details of how the VERIFY_PTR macro is written:

        - The use of the non-standard GNU C preprocessor extensions
          "({ ... })" syntax which allows a compound statement to act as an
          expression. We need three statements so the typical use of the
          comma operator won't suffice.

        - The use of the 'typeof' keyword to make the macro dynamically-typed.

        - The temporary variable __p is a reference to a pointer rather
          than a pointer.  This prevents __p from getting initialized
          followed by a subsequent copy operation from p (which results
          in the macro argument being evaluated twice).

    For more information on the non-standard GNU C preprocessor extensions,
    see http://gcc.gnu.org/onlinedocs/cpp/Duplication-of-Side-Effects.html

*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#ifndef _ASSERTS_H
#define _ASSERTS_H

// Unless explicitly specified, ASSERT is disabled in production build.
#ifndef ASSERT_DISABLED
#ifdef NDEBUG
#define ASSERT_DISABLED
#endif
#endif

// Our ASSERT builds on the standard assert.
#ifndef ASSERT_DISABLED
#include <assert.h>
#include <stdio.h>
#endif // ASSERT_DISABLED

// ASSERT or VERIFY a pointer with associated failure message.

/* Assertions ***************************************************************/

#ifndef ASSERT_DISABLED

#ifdef __cplusplus

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

    // ASSERT a condition as true.
    #define ASSERT(x) \
        do { \
            try { \
                if (!(x)) { \
                    IGNORE_FUNC_RETURN LOG_ERROR(#x); \
                    abort(); \
                } \
            } catch (...) { \
                abort(); \
            } \
        } while (0)

    #define ASSERT_MSG_OUT(msg) \
        LOG_ERROR("ASSERT_MSG: " << msg)

#else //  __cplusplus
    #define ASSERT(x) assert(x)

    #define ASSERT_MSG_OUT(msg) \
        printf("\nASSERT_MSG: %s\n", msg)

#endif // __cplusplus
    #define ASSERT_PTR(p) /*lint -e(58)*/ ASSERT(NULL != (p))
    #define ASSERT_MSG(x,msg) \
        do { \
            if(!(x)) \
            { \
                ASSERT_MSG_OUT(msg); \
                IGNORE_FUNC_RETURN fflush(NULL); \
                ASSERT(x); \
            } \
        } while(0)
    #define ASSERT_PTR_MSG(p,msg) ASSERT_MSG(NULL != (p), (msg))

    /* 
        VERIFY a condition as true. See file header for additional info.
        
        In all versions except for VERIFY_PTR_MSG, we logically AND in a 
        pointer to the stringized condition so that it will be included 
        in the assert output when the verification fails.

        For VERIFY_PTR_MSG, the stringized condition is printed as part
        of the user message, before the user message.
    */
    #define VERIFY(x) \
        ({ \
            const typeof(x)& _verify_ = (x); \
            ASSERT(_verify_ && #x); \
            _verify_; \
        })
    #define VERIFY_MSG(x,msg) \
        ({ \
            const typeof(x)& _verify_ = (x); \
            ASSERT_MSG(_verify_ && #x,msg); \
            _verify_; \
        })
    #define VERIFY_PTR(p) \
        ({ \
            const typeof(p)& _verify_ptr_ = (p); \
            ASSERT((NULL != _verify_ptr_) && #p); \
            _verify_ptr_; \
        })
    #define VERIFY_PTR_MSG(p,msg) \
        ({ \
            const typeof(p)& _verify_ptr_ = (p); \
            ASSERT_PTR_MSG(_verify_ptr_,"NULL != " #p "; " msg); \
            _verify_ptr_; \
        })

#else // ASSERT_DISABLED

    // If an included header hasn't already defined this.
    #ifndef assert
    #define assert(x) ((void) 0) // Defined as per ISO standard above.
    #endif

    // ASSERT generates no code whatsoever when disabled.
    #define ASSERT(x)
    #define ASSERT_PTR(p)
    #define ASSERT_MSG(x,msg)
    #define ASSERT_PTR_MSG(p,msg)

    // VERIFY does no checking, but retains side effects when disabled.
    #define VERIFY(x) (x)
    #define VERIFY_MSG(x,msg) (x)
    #define VERIFY_PTR(p) (p)
    #define VERIFY_PTR_MSG(p,msg) (p)

#endif // ASSERT_ENABLED



/*****************************************************************************
    $Id: asserts.h 31222 2012-11-02 22:58:17Z jody $
*****************************************************************************/
#endif // _ASSERTS_H

