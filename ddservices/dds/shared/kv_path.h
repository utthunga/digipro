#ifndef KV_PATH_H
#define KV_PATH_H


#ifdef __cplusplus
extern "C" {
#endif

#ifndef RET_INFO_H
#include "ret_info.h"
#endif

#ifndef HOST_INFO_H
#include "host.h"
#endif

#ifndef PV_TBL_H
#include "pv_tbl.h"
#endif

typedef struct
{
    char     pd_tag [34];       /* physical device tag      */
    char     dev_id [34];       /* device id                */
    int      node_address;
    int      manufacturer;
    int     device_type;
    int     device_rev;
    int     dd_rev;
    void*    device_element;    /* Protocol Specific  device information */

} DeviceInfo;

typedef struct
{

} CommunicationStatus;

typedef int DeviceHandle;

/* DD Scan Demo Interface */
int ff_initialize(RET_INFO*, const char *, HOST_CONFIG *);
int ff_configure(RET_INFO*, HOST_CONFIG *);
int ff_get_host_info(RET_INFO *, HOST_INFO *, HOST_CONFIG *);
extern int ff_connect(RET_INFO*, unsigned short, DeviceInfo*);
int ff_offline_connect(RET_INFO*, unsigned short);

int ff_scan_begin(RET_INFO*);
unsigned char ff_scan_has_next(void);
// int ff_scan_next(RET_INFO*);
void ff_scan_end(void);

int ff_connect_block(RET_INFO *, const char *);
int ff_disconnect_block(RET_INFO *, const char *);
// int ff_read_block_pv(RET_INFO* , ValuePath* block_path);
int ff_disconnect(RET_INFO* , unsigned short );
int ff_uninitialize(RET_INFO *, unsigned char);
int ff_get_mode_block(RET_INFO *, char *, char *);
int ff_read_param_value(RET_INFO *, char *, VARIABLES *);
int ff_reset_fbkboard(RET_INFO *, unsigned short, HOST_CONFIG *);
int ff_continous_read_param_value(RET_INFO *, char *, VARIABLE_VAL *);
int ff_exec_method(RET_INFO *ret_info, const char *, unsigned long );
int ff_set_pd_tag(RET_INFO *ret_info, unsigned short device_addr);

#ifdef __cplusplus
}
#endif

#endif // KV_PATH_H

