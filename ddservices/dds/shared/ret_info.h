#ifndef RET_INFO_H
#define RET_INFO_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    int host_state;
    int stack_service;
    int stack_primitive;
    int stack_result;
    int stack_layer;
}DETAIL;


typedef struct {
    int reason_code;
    char reason_str[128];
    DETAIL detail;
    
}RET_INFO;

#ifdef __cplusplus
}
#endif


#endif // RET_INFO_H

