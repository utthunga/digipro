/*****************************************************************************
    Copyright (c) 2009 Fluke Corporation. All rights reserved.
******************************************************************************

    File           - $URL: std.h $
    Version Number - $Revision: 26885 $
    Last Updated   - $Date: 2011-07-14 11:45:23 -0700 (Thu, 14 Jul 2011) $
    Updated By     - $Author: moriarty $
    Authored By    - Tony Garland
*/

/** @file
    Standard Include File for ARM Platform

    Standard include file for products which use the ARM platform such as:

        * Wall - i.MX27L ARM processor
        * [add other products here]

    All project .cpp files should refer to this file as the very first
    include:

        \code
        #include "src/arm/shared/std.h"
        \endcode

    Within this file, use the NDEBUG macro to test whether a development
    or production build is in progress:

        - NDEBUG   defined = production build
        - NDEBUG undefined = development build

    The NDEBUG preprocessor variable follows an ISO standard.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#ifndef _STD_H
#define _STD_H

#if 0 // Now use just pieces of this file.

#include <pthread.h>
#include <stdlib.h>

/* Code annotation **********************************************************/

/** Mark incomplete code.

    Mark incomplete code with your initials and a description.
    These are items which should be implemented prior to the next
    product release.
*/
#define TODO(initials, description)
    
/** Mark incomplete code with iteration.

    Mark incomplete code with your initials, a coding iteration (e.g., "I2",
    and a description.  These are items which should be implemented prior to
    the next product release; the iteration specifies the coding iteration
    when implementation should occur.
*/
#define TODO_I(initials, iteration, description)

/** Mark future code.

    Mark future code with your initials and a description.
    These are items which will not be implemented in the next product
    release, but should be implemented in releases to follow.
*/
#define TODO_F(initials, description)
    
/** Mark code to be cleaned up or refactored.

    Mark clean up code with your initials and a description.
    These are sections of code which would benefit from clean up and/or
    re-factoring, but are functioning and do not have any potential
    for bugs.
*/
#define TODO_C(initials, description)
    
/*
    Mark code where we find a lint issue which has been suppressed until
    the original author can review the code to decide if a revision to the
    code should be made and the lint suppression removed.
*/
#define TODO_LINT(initials, description)

/** NULL

    In C, NULL is typically defined to be (void*) 0. However, this definition
    does not work for C++ because automatic conversion between void* and class
    pointers is not allowed. GCC has a __null builtin that represents a "zero"
    pointer that may be converted to and compared with pointers to any type.
*/
#ifndef NULL
#define NULL __null
#endif


/* Unit Testing *************************************************************/

#ifdef UNIT_TEST

/// For unit testing, substitute a MockFoo object for the real Foo object.
#define MOCK(type) Mock ## type

/// Allows test code to get the mock object from the owning class.
#define MOCK_GET(type, name, var) type get ## name() { return var; }

/// Use this macro to make a function mockable (virtual) only when testing.
#define MOCKABLE virtual

#else // UNIT_TEST

/// In non-testing build, use the real type.
#define MOCK(type) type

/// No need to get the mock variable in non-test code.
#define MOCK_GET(type, name, var)

/// Functions made virtual for testing are non virtual otherwise.
#define MOCKABLE

#endif // UNIT_TEST


/* Time conversion **********************************************************/

/// Larger to smaller time conversions.
#define SEC_TO_MSEC(s)   ((s)  * 1000)
#define MSEC_TO_USEC(ms) ((ms) * 1000)
#define USEC_TO_NSEC(us) ((us) * 1000)
#define SEC_TO_USEC(s)   MSEC_TO_USEC(SEC_TO_MSEC(s))
#define SEC_TO_NSEC(s)   USEC_TO_NSEC(SEC_TO_USEC(s))
#define MSEC_TO_NSEC(ms) USEC_TO_NSEC((MSEC_TO_USEC(ms)))

/// Smaller to larger time conversions (floating point result).
#define MSEC_TO_SEC(ms)  ((1.0 * (ms)) / 1000.0)
#define USEC_TO_MSEC(us) ((1.0 * (us)) / 1000.0)
#define NSEC_TO_USEC(ns) ((1.0 * (ns)) / 1000.0)
#define USEC_TO_SEC(us)  MSEC_TO_SEC(USEC_TO_MSEC(us))
#define NSEC_TO_SEC(ns)  USEC_TO_SEC((NSEC_TO_USEC(ns)))


/* Frequency conversion *****************************************************/
#define KHZ_TO_HZ(KHz)  ((KHz) * 1000)
#define MHZ_TO_KHZ(MHz) ((MHz) * 1000)
#define MHZ_TO_HZ(MHz)  ((MHz) * 1000000.0)
#endif // 0
/* Assertions ***************************************************************/
#include "asserts.h"

#if 0 // Now use just pieces of this file.
/* Types ********************************************************************/

/// Common integral types: convenient abbreviated versions.
typedef unsigned int uint;
typedef unsigned long ulong;

/// Timestamp according to the epoch of the instrument.
TODO_F(GAM, "Consider changing all time type double to Timestamp.");
typedef double Timestamp;       // 64 bit floating point value.

/**
    Exact Integral Types defined by C99.

    This is a tiny subset of what is normally provided by stdint.h.
    We are not presently using 64-bit types so we omit them here to
    avoid the complexity of determining the architecture using macros
    such as __WORDSIZE which are not defined by arm-elf-gcc.

    Makefile sources should include stdint.cpp which contains code to validate
    the sizes at compile time.

*/
typedef signed char              int8_t; // Signed,    8-bits wide.
typedef unsigned char           uint8_t; // Unsigned,  8-bits wide.
typedef short                   int16_t; // Signed,   16-bits wide.
typedef unsigned short         uint16_t; // Unsigned, 16-bits wide.
typedef int                     int32_t; // Signed,   32-bits wide.
typedef unsigned int           uint32_t; // Unsigned, 32-bits wide.
typedef volatile unsigned char CYG_ATOMIC; // Atomic access without lock.

#if !defined(__cplusplus)
typedef unsigned char bool;
#endif // !defined(__cplusplus)

/* Helpers ******************************************************************/

/// Type-safe way to calculate the length of an array.
#define ARRAY_LENGTH(x) (sizeof(x) / sizeof(x[0]))

/** Delete dynamically allocated structures and arrays.

    After deleting what a pointer points to, always set the pointer to NULL.

    Consider using smart pointers instead of calling these macros directly.

    \code
    - std::auto_ptr        #include <memory.h>     // NOT STL compatible.
    - std::tr1::shared_ptr #include <tr1/memory.h> // STL compatible
    \endcode

    It is safe to use the delete operator on a NULL pointer.
*/
#define DELETE_PTR_IN_EXCEPTION_SAFE(p) { delete (p); (p) = NULL; }

#define DELETE_PTR(p) \
    do { EXCEPTION_SAFE(delete (p)); (p) = NULL; } while (0)

#define DELETE_ARRAY_PTR(p) \
    do { EXCEPTION_SAFE(delete[] (p)); (p) = NULL; } while (0)

/// Macros for setting, clearing, and toggling a bit.
#define SETBIT(value, mask) ((value) |= (mask))
#define CLEARBIT(value, mask) ((value) &= ~(mask))
#define FLIPBIT(value, mask) ((value) ^= (mask))
#define MASK_AND_SHIFT(value,mask,shift) (((value) & (mask)) >> (shift))

/// Macros for testing bits.
#define BIT_IS_SET(value, mask) ((bool)((value) & mask))
#define BIT_IS_CLEAR(value, mask) BIT_IS_SET(~(value), mask)

/**  How close do floating point values have to be to compare equal?

    We use an 'epsilon' value to specify how close two floating point
    values must be before we declare them to be equal.

    When building with optimization for the I386, we have to reduce accuracy
    because the -msse compiler flag generates floating point arithmetic
    with reduced accuracy.
*/
#if  defined(NDEBUG) && defined(ARM_ARCH_I386)
#define EPSILON 1.0e-13
#else // NDEBUG or !ARM_ARCH_I386
#define EPSILON 1.0e-15
#endif // NDEBUG and ARM_ARCH_I386

/** Macros for comparing floating point numbers.

   Requires inclusion of math.h.
   (Prefer Google Test's ASSERT_NEAR in unit tests.)

   <pre>

   <------------->|---------x---------|<---------------->
       REGION 1   |     REGION 2      |   REGION 3                  
         LT       |     EQUAL         |   GT
                  |                   |                             
                  x-epsilon           x+epsilon

    y in REGION    MACROS EVALUATE TRUE
    -----------    --------------------
    1              FLOATING_POINT_LT(x,y)
                   FLOATING_POINT_LE(x,y)

    2              FLOATING_POINT_EQ(x,y)
                   FLOATING_POINT_LE(x,y)
                   FLOATING_POINT_GE(x,y)

    3              FLOATING_POINT_GT(x,y)
                   FLOATING_POINT_GE(x,y)

   </pre>
*/
#define FLOATING_POINT_EQ_WITHIN(x, y, epsilon) (fabs(x - y) <= epsilon)
#define FLOATING_POINT_EQ(x, y) FLOATING_POINT_EQ_WITHIN(x, y, EPSILON)
#define FLOATING_POINT_LT(x,y) (((x) + EPSILON) < y)
#define FLOATING_POINT_GT(x,y) (((y) + EPSILON) < x)
#define FLOATING_POINT_LE(x,y) \
    (FLOATING_POINT_LT(x,y) || FLOATING_POINT_EQ(x,y))
#define FLOATING_POINT_GE(x,y) \
    (FLOATING_POINT_GT(x,y) || FLOATING_POINT_EQ(x,y))

/// Macro for comparing timestampes (1 ns epsilon)
#define TIMESTAMPS_ARE_EQUAL(x,y) \
    FLOATING_POINT_EQ_WITHIN(x, y, 1.0e-9)


/** Compile-time static assert.

    Can be used at global scope or function scope.
    Consumes no code space, since it is an extern declaration only.
    Useful for compile time size checking like CT_ASSERT(sizeof(int) == 4);
    When the assertion is false, a compile error is generated due to a
    negative array size.
    Source: http://www.pixelbeat.org/programming/gcc/static_assert.html
*/
#if defined(__cplusplus)
#define CT_ASSERT(e) extern "C" { \
    /* use C linkage for CT_ASSERT */  \
    extern char (*ct_assert(void)) [sizeof(char[1 - 2*!(e)])]; \
    }
#else // defined(__cplusplus)
#define CT_ASSERT(e) \
    extern char (*ct_assert(void)) [sizeof(char[1 - 2*!(e)])]
#endif // defined(__cplusplus)

/* Enumeration Helper Functions *********************************************/
#include "shared/enum_funcs.h"

#endif // 0

/* Lint Helper Functions ****************************************************/

/** Return value explicitly ignored.

    Make our intention clear when ignoring the return value of a function.
    use: IGNORE_FUNC_RETURN memcpy(foo .... )
*/
#define IGNORE_FUNC_RETURN (void)

#if 0 // Now use just pieces of this file.

/** Exception safety.

    The enclosed code is exception-safe since we suppress any exceptions.
    Useful in destructors when lint complains that an exception could be
    thrown by the called function.

    An alternate approach is to investigate whether the called function
    can be declared as not throwing exceptions by appending an empty
    "throw()" exception specifier to the function declaration and definition
    to indicate that the function is known not to throw any exceptions.

    If an exception is thrown, an error is logged before calling abort().
*/
//lint -esym(665, EXCEPTION_SAFE) Unparenthesized parameter OK.
#define EXCEPTION_SAFE(code) \
    do \
    { \
        try \
        { \
            code; \
        } \
        catch (...) \
        { \
            LOG_ERROR(exception, "Unexpected exception!"); \
            ::abort(); \
        } \
    } \
    while (0)


/* Environment **************************************************************/
// True if 'set environment GDB' is placed in .gdbinit file.
#define IS_GDB (NULL != getenv("GDB"))

// True if 'HART_DEBUG' is enabled.
#define IS_HART_DEBUG (NULL != getenv("HART_DEBUG"))

/*
    Communication overrun allowed if in debugger or running on i386 Linux box.
    In either case, there can be unexpected delays (e.g., breakpoints,
    other applications with higher priority) which cause the proxies to
    be unable to poll the MASH on schedule leading to results buffer overrun.
*/
#ifdef ARM_ARCH_I386
#define IS_RESULTS_OVERRUN_ALLOWED (true)
//lint -emacro(506, IS_RESULTS_OVERRUN_ALLOWED) Allow constant boolean value.
#else // ARM_ARCH_I386
#define IS_RESULTS_OVERRUN_ALLOWED ((IS_GDB) || (IS_HART_DEBUG))
#endif // ARM_ARCH_I386


// C++ code only.
#if defined(__cplusplus)

/* Synchronization **********************************************************/

#include "arm/shared/thread/threads.h"


/* Logging ******************************************************************/
#include "arm/shared/log4cplus.h"


/* Static Initialization ****************************************************/

/** Early Static Variables.

    Certain static variables are used very early during initialization.
    These same variables may have order interdependencies between them.
    Because they may reside in different translation units (e.g., files),
    there is no guarantee concerning their relative order of initialization.

    A similar problem arises during late destruction/cleanup as the program
    is about to exit: the relative order in which these statics from different
    translation units get destructed is undefined.

    By using the following macro to declare and return a reference to
    such static variables, we solve both these problems:

    - We encapsulate a static pointer to the Early Statics to guarantee the
      pointer is automatically initialized (triggering allocation on the heap)
      before first use. C++ guarantees that function statics will be
      initialized before use.

    - After Early Statics are allocated on the heap, they are never
      deleted.  Therefore they continue to exist throughout program
      execution--even beyond the destruction process.  Once the application
      has completely exited, the OS reclaims the heap effectively deleting the
      Early Statics from memory.

    NOTE: Early Statics will never have their destructor called!

    It is important that initialization of these statics be guaranteed 
    to occur when only a single thread is accessing them. Typically, this
    occurs during static initialization by the prologue code which executes
    before main()--and before the application or its containing library have
    the opportunity to create additional threads.  This avoids potential race
    conditions in creating the lock on first use in a multi-threaded context.

    The macros are made exception-safe: when a memory failure occurs
    they log an error and then abort to avoid unsafe operation with
    a NULL reference..
*/
// No parameter to constructor.
#define RETURN_EARLY_STATIC(T) \
do { \
    try { \
        static T* const _p = new T(); \
        ASSERT_PTR(_p); \
        return *_p; \
    } catch (...) { \
        LOG_ERROR(exception, "Unexpected exception!"); \
        abort(); \
    } \
} while(0)

// One parameter for constructor.
#define RETURN_EARLY_STATIC1(T, P) \
do { \
    try { \
        static T* const _p = new T(P); \
        ASSERT_PTR(_p); \
        return *_p; \
    } catch (...) { \
        LOG_ERROR(exception, "Unexpected exception!"); \
        abort(); \
    } \
} while(0)

// Two parameters for constructor.
#define RETURN_EARLY_STATIC2(T, P1, P2) \
do { \
    try { \
        static T* const _p = new T(P1, P2); \
        ASSERT_PTR(_p); \
        return *_p; \
    } catch (...) { \
        LOG_ERROR(exception, "Unexpected exception!"); \
        abort(); \
    } \
} while(0)

// Three parameters for constructor.
#define RETURN_EARLY_STATIC3(T, P1, P2, P3) \
do { \
    try { \
        static T* const _p = new T(P1, P2, P3); \
        ASSERT_PTR(_p); \
        return *_p; \
    } catch (...) { \
        LOG_ERROR(exception, "Unexpected exception!"); \
        abort(); \
    } \
} while(0)

// No parameter to constructor, return member.
#define RETURN_EARLY_STATIC_MEMBER(T, M) \
do { \
    try { \
        static T* const _p = new T(); \
        ASSERT_PTR(_p); \
        return (_p->M); \
    } catch (...) { \
        LOG_ERROR(exception, "Unexpected exception!"); \
        abort(); \
    } \
} while(0)

#endif // defined(__cplusplus)

#endif // 0


/*****************************************************************************
    $Id: std.h 26885 2011-07-14 18:45:23Z moriarty $
*****************************************************************************/
#endif // _STD_H

