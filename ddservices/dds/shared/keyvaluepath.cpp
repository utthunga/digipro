/*****************************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Matthew Kraus
    Origin:            Hutch
*/

/** @file
    One-line summary of source file purpose.

    Low-level design, physical design discussions, build dependencies,
    assumptions, implementation issues, notes, etc.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/

#include "kv_path.h"

#include <algorithm>
#include <iostream> // TEST use for cout; needed for debug_printTree()
#include <sstream>
#include "string.h"

using namespace std;

/** Convert a number to a std::string.

The type @b T can be any numeric type that supports the << operator
and @c ostringstream(), such as @c int, @c float, or @c double. Be careful with
@c char, as there might not be an implicit conversion to @c int. It might
be best to perform a @c static_cast<int>() to a @c char datatype.

@param number is a numeric type
@return a std::string that represents the string value of @c number
*/
template <typename T>
string NumberToString(T number)
{
    return dynamic_cast<ostringstream&>((ostringstream() << std::dec << number)).str();
}


/** This table defines standardized string names to be used by
 * both the C back-end and C++ UI
 */
const char* const KV_DEVICE = "device";
const char* const KV_STATION_ADDRESS = "station_address";
const char* const KV_DEVICE_ID = "dev_id";
const char* const KV_MANUFACTURER = "manufacturer";
const char* const KV_DEVICE_TYPE = "device_type";
const char* const KV_DEVICE_REV = "device_rev";
const char* const KV_DD_REV = "dd_rev";
const char* const KV_BLOCK = "block";

const char* const KV_PROCESS_DISPLAY_NAME = "process_display_name";
const char* const KV_PROCESS_VALUE = "process_value";
const char* const KV_PROCESS_UNITS = "process_units";
const char* const KV_PROCESS_ITEM_ID = "process_item_id";
const char* const KV_PROCESS_STATUS = "process_status";

/** Represents an empty child list.

This vector of KeyValueInterface* nodes is statically defined to be empty.
Since its size is zero, it is returned in situations where a function
needs to return an empty list of child nodes.
*/
const vector<KeyValueInterface*> KeyValueInterface::emptyChildList;


/**
Prints the full paths of all the leaves in the tree.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");

(void) val1->addPair("block", "transducer");
(void) val1->addPair("block", "AI");
(void) val1->addPair("block", "AI2");
(void) val1->addPair("block", "RESOURCE");
@endcode

In the above tree, note that the node "block" only exists once;
that is, there is only a single node with the name "block."
Note that each leaf is printed in the output:

@code
KeyValueInterface::debug_printTree(root);

// sample output follows
/device/rosey-644/block/transducer
/device/rosey-644/block/AI
/device/rosey-644/block/AI2
/device/rosey-644/block/RESOURCE
@endcode

There is one line of output for each leaf in the tree. Just as
there is only one node called "block", there is also only one
node for "rosey-644" and "device", respectively.
*/
void KeyValueInterface::debug_printTree(KeyValueInterface* root)
{
    if (!root) return;

    // print children
    vector<KeyValueInterface*>::iterator itr;
    for (itr = root->children.begin();
         itr != root->children.end();
         ++itr)
    {
        root->debug_printTree(*itr);
    }

    // we only print if we are a leaf
    if (!root->hasChildren())
    {
        cout << root->fullPathName() << endl;
    }
}

/**
By design, you cannot directly instantiate a KeyValueInterface. Instead,
you must instantiate a KeyPath as a root node. The KeyValueInterface
provides a common interface through which most of the functionality of
key-value paths can be accessed. A KeyValueInterface* is returned by many
of the functions in the interface, and most of the time, it is not
necessary to know whether it is an actual KeyPath* or a ValuePath* because
of the fact that most functionality is accessible through
KeyValueInterface.
*/
KeyValueInterface::KeyValueInterface()
{
    parent = 0;
}

/**
KeyValueInterface pointers are used to point to concrete KeyPath or ValuePath
objects, which should not be deleted through the KeyValueInterface class.
*/
KeyValueInterface::~KeyValueInterface()
{
}

/**
This function is probably mostly of use internally, although it might
have some use elsewhere, so it is made public.

This function separates the head of the path from the rest of
the path. Two specific cases are handled:

-# The path starts with a slash.
-# The path starts with a letter (and does NOT start with a slash).

Either way, the "head" of the path is separated from the rest of the path.
A std::pair is returned, where @c pair.first is the head, and pair.second
is the remainder of the path.

### Example

For example, calling chompFirst() on this string:
@code
/device/rosey-644/block
@endcode

will return in the pair:
@code
pair.first == "device";              // true (no leading slash)
pair.second == "/rosey-644/block";   // true (leading slash)
@endcode

Likewise, if there is no leading slash, calling chompFirst() on this string:
@code
device/rosey-644/block
@endcode

will return in the pair:
@code
pair.first == "device";              // true (no leading slash)
pair.second == "rosey-644/block";    // true (no leading slash)
@endcode

Note that in both of the above cases, @c pair.first never starts with a slash.

## Case I: Path starts with a slash "/"

@code
// Consume the entire path one key or value at a time....

pair<string, string> path = KeyValueInterface::chompFirst("/device/rosey-644/block/transducer");
path.first == "device";                        // true
path.second == "/rosey-644/block/transducer";  // true

path = KeyValueInterface::chompFirst(path.second);

path.first == "rosey-644";                     // true
path.second == "/block/transducer";            // true

path = KeyValueInterface::chompFirst(path.second);

path.first == "block";                         // true
path.second == "/transducer";                  // true

path = KeyValueInterface::chompFirst(path.second);

path.first == "transducer";                    // true
path.second == "";                             // true

// path.second is empty, so both first and second should also be emtpy
path = KeyValueInterface::chompFirst(path.second);

path.first == "";                              // true
path.second == "";                             // true
@endcode


## Case II: Path does NOT begin with a slash "/"

@code
// note that there is no slash before 'device'
pair<string, string> path = KeyValueInterface::chompFirst("device/rosey-644");

path.first == "device";         // true

// since the first component didn't have a preceeding slash,
// we expect the leading slash from the next component to be pruned, too
path.second == "rosey-644";     // true

path = KeyValueInterface::chompFirst(path.second);

path.first == "rosey-644";      // true
path.second == "");             // true

path = KeyValueInterface::chompFirst(path.second);

// path should be fully consumed
path.first == "";               // true
path.second == "";              // true
@endcode
*/
pair<string, string> KeyValueInterface::chompFirst(string path)
{
    if (path.empty()) return pair<string, string>();

    size_t firstSlash = path.find('/');
    if (firstSlash == string::npos)
    {
        return pair<string, string>(path, string());
    }

    size_t secondSlash = path.find('/', firstSlash + 1);

    string first, second;

    if (firstSlash != 0)
    {
        first = path.substr(0, firstSlash);

        // since the string didn't start with a slash, we prune the
        // leading slash off of the next path component
        second = path.substr(firstSlash + 1, path.size() - firstSlash);
    }
    else if (secondSlash != string::npos)
    {
        first = path.substr(1, secondSlash - 1);
        second = path.substr(secondSlash, path.size() - secondSlash);
    }
    else
    {
        first = path.substr(1, path.size() - 1);
    }

    return pair<string, string>(first, second);
}

/**
Internally, this function uses findChildren() to perform the path query,
and so it uses the same rules as that function.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");

(void) val1->addPair("block", "transducer");
(void) val1->addPair("block", "resource");

string child = val1->queryFirstChild("block");

child == "transducer";    // true

delete root;              // free entire tree including children
@endcode

Note that although there are two children, the order that the
children were added is preserved internally, so even if more
children nodes are added (as in the addPair() function above),
"transducer" will always be the first child (unless it is
removed with the remove() function).

@param pathQuery is the search term used to locate the child.
@return a string name of the first child found, or an empty
string if no child was found.
*/
string KeyValueInterface::queryFirstChild(string pathQuery)
{
    const vector<KeyValueInterface*>& result = findChildren(pathQuery);

    if (result.size() > 0)
    {
        return result.front()->getName();
    }
    else
    {
        return string();
    }
}

/**
Internally, this function uses findChildren() to perform the path query,
and so it uses the same rules as that function.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");

(void) val1->addPair("block", "transducer");
(void) val1->addPair("block", "resource");

string child = val1->queryLastChild("block");

child == "resource";      // true

delete root;              // free entire tree including children
@endcode

As with queryLastChild(), the order that child nodes are added is
guaranteed to be preserved, so the last child node that was added
is the node that will be returned by queryLastChild().

@param pathQuery is the search term used to locate the child.
@return a string name of the first child found, or an empty
string if no child was found.
*/
string KeyValueInterface::queryLastChild(string pathQuery)
{
    const vector<KeyValueInterface*>& result = findChildren(pathQuery);

    if (result.size() > 0)
    {
        return result.back()->getName();
    }
    else
    {
        return string();
    }
}

/**
If this node is a KeyPath*, and getParent() returns non-null, then the
returned node is guaranteed to be a ValuePath*. If this node is a
ValuePath*, and getParent() returns non-null, then the returned node is
guaranteed to be a KeyPath*. This is the same as saying that all of the
children of a KeyPath are ValuePath objects, and all of the children of a
ValuePath are KeyPath objects.

@return the parent of this node, or null if this is the topmost parent.
*/
KeyValueInterface *KeyValueInterface::getParent() const
{
    return parent;
}

//void KeyValueInterface::merge(vector<KeyValueInterface*> newChildren)
//{
//    vector<KeyValueInterface*>::iterator itr;
//    for (itr = newChildren.begin();
//         itr != newChildren.end();
//         ++itr)
//    {

//    }
//}

/**
This function merges @a other and @a this tree into one tree. In
order for this to work, the specified nodes must have the same
name. In other words, this test must pass:

@code
this->getName() == other->getName()     // must be true
@endcode

@param other the other tree to merge into this one.
*/
void KeyValueInterface::merge(KeyValueInterface* other)
{
    // TODO: after the merge, are all of the parents correct?

    // the two trees must have a common ancestor
    if (getName() != other->getName()) return;

    // add all of other's children to this node's children
    vector<KeyValueInterface*>::iterator itr;
    for (itr = other->children.begin();
         itr != other->children.end();
         ++itr)
    {
        KeyValueInterface* subTree = addChild((*itr)->getName(), this);
        subTree->merge(*itr);
    }
}

/**
Removes the given node from the value tree.  The search is performed in
breadth-first order, so elements that are shallower (not as deep / closer to
the root) will be found first.

The child must exist in this tree and may not be the same instance as the
caller; in other words, this is illegal:

@code
root->remove(root);  // not legal
@endcode

The relative order of the remaining child nodes is guaranteed to be preserved.
*/
void KeyValueInterface::remove(KeyValueInterface* child)
{
    if (child == 0) return;

    if (child == this)
    {
        // can't delete ourself; technically we could do a
        // "delete this;" here, but the caller almost certainly
        // isn't expecting this, and it could lead to bugs
        return;
    }

    vector<KeyValueInterface*>::iterator itr;
    for (itr = children.begin();
         itr != children.end();
         ++itr)
    {
        if (*itr == child)
        {
            delete *itr;
            children.erase(itr);
            return;
        }
    }

    // check children for the child node (in breadth-first order)
    for (itr = children.begin();
         itr != children.end();
         ++itr)
    {
        (*itr)->remove(child);
        return;
    }
}

/**
This function looks through this node's list of children for the child
specified by childName. For each child, getName() is returned and comapred
with childName, and if a match is found, true is returned. The invariant
of the key-value path system guarantees that all children are unique
amongst their siblings, so that no two children may have the same name.
(Note however, that a child of one node may have the same name as a child
of a different node.)

@param childName is the string name of the child to search for.
@return true if the child was found, false otherwise.
@see hasChildren()
*/
bool KeyValueInterface::childExists(std::string childName) const
{
    //vector<KeyValueInterface*>::iterator itr;
    vector<KeyValueInterface*>::const_iterator itr;
    for (itr = children.begin();
         itr != children.end();
         ++itr)
    {
        if ((*itr)->getName() == childName)
        {
            return true;
        }
    }

    return false;
}


/**
The child list is searched for the child by the given string name.

@return the index of the given child, -1 if the child is not found
@see indexOfChild(KeyValueInterface*)
*/
int KeyValueInterface::indexOfChild(string childName)
{
    vector<KeyValueInterface*>::iterator itr;
    int i;
    for (itr = children.begin(), i = 0;
         itr != children.end();
         ++itr, i++)
    {
        if ((*itr)->getName() == childName)
        {
            return i;
        }
    }

    return -1;
}


/**
This works similarly to indexOfChild(std::string), except that the
argument is the node to look for. The linear search algorithm is
the same, but it uses pointer comparison instead of string comparison,
so it should be faster.

@param child the child to look for
@return the index of the given child, or -1 if the child is not found
@see indexOfChild(std::string)
*/
int KeyValueInterface::indexOfChild(KeyValueInterface* child)
{
    vector<KeyValueInterface*>::iterator itr;
    int i;
    for (itr = children.begin(), i = 0;
         itr != children.end();
         ++itr, i++)
    {
        if (*itr == child)
        {
            return i;
        }
    }

    return -1;
}

/**
@return true if this node has children; false if there are no children.
*/
bool KeyValueInterface::hasChildren() const
{
    return children.size() > 0;
}

/**
@return the number of children that this node has.
*/
size_t KeyValueInterface::numChildren() const
{
    return children.size();
}

/**
Calling a KeyPath constructor is the only way to create a new key-value
tree. This is the only object you need to manage the lifetime of.

@param key the component/name of this node.
*/
KeyPath::KeyPath(std::string key)
{
    component = key;
    parent = 0;
}

KeyPath::KeyPath(string key, KeyValueInterface* parent)
{
    component = key;
    this->parent = parent;
}

KeyPath::~KeyPath()
{
    deleteChildren();
}

/**
All children are deleted. This function is recursive, which means
that deleteChildren() is called on each child. It is safe to call
this function on a node that does not (or might not) have children.
*/
void KeyValueInterface::deleteChildren()
{
    std::vector<KeyValueInterface*>::iterator itr;

    for(itr = children.begin(); itr != children.end(); ++itr)
    {
        if ((*itr)->hasChildren())
        {
            (*itr)->deleteChildren();
        }

        delete *itr;
    }

    children.clear();
}

KeyValueInterface* KeyPath::newChild(string key, KeyValueInterface *parent)
{
    ValuePath* path = new ValuePath;
    path->component = key;
    path->parent = parent;

    return path;
}

/**
Looks only in the children list for this key-value component, and does not
search descendents of children; for the latter, see find().  The returned
child will either be a ValuePath* or a KeyPath* (use dynamic_cast<> to
figure out which one if this is important, although most functionality will
be exposed through the KeyValueInterface* that is returned).

@return KeyValueInterface* of the given child.
@see find()
*/
KeyValueInterface* KeyValueInterface::getChild(string value) const
{
    vector<KeyValueInterface*>::const_iterator itr;
    for(itr = children.begin(); itr != children.end(); ++itr)
    {
        if ((*itr)->getName() == value) return *itr; // return existing value
    }

    return 0;
}

/**
A cosnt reference to the underlying data structure (std::vector) of
children is returned.

@see hasChildren(), childExists()
*/
const std::vector<KeyValueInterface *> &KeyValueInterface::getChildren() const
{
    return children;
}

/**
@return the first child of this node, or null if there are no children.
*/
KeyValueInterface* KeyValueInterface::firstChild()
{
    if (children.size() > 0)
    {
        return children.front();
    }
    else
    {
        return 0;
    }
}

/**
@return the last child of this node, or null if there are no children.
*/
KeyValueInterface* KeyValueInterface::lastChild()
{
    if (children.size() > 0)
    {
        return children.back();
    }
    else
    {
        return 0;
    }
}

ValuePath::~ValuePath()
{
    deleteChildren();
    //cout << "deleting " << this->getName() << endl;
}

/**
Adds a child node with the given name. If the child already exists
by this name, then the existing child with this name is returned.
However, if a child with this name does not yet exist, a new child
is created and added to the internal child list.

@param value the (component) name of this child.
@return a ValuePath object that points to the child. There is no need to
manage this pointer manually; the returned child has been added to this
nodes child list and will be deleted when the top-level (root) node
is destroyed.
@see ValuePath::addChild()
*/
ValuePath* KeyPath::addChild(string value)
{
    return dynamic_cast<ValuePath*>(KeyValueInterface::addChild(value, this));
}

/**
Adds a child node with the given name. If the child already exists
by this name, then the existing child with this name is returned.
However, if a child with this name does not yet exist, a new child
is created and added to the internal child list.

@param value the (component) name of this child.
@return a KeyPath object that points to the child. There is no need to
manage this pointer manually; the returned child has been added to this
nodes child list and will be deleted when the top-level (root) node
is destroyed.
@see KeyPath::addChild()
*/
KeyPath* ValuePath::addChild(string value)
{
    return dynamic_cast<KeyPath*>(KeyValueInterface::addChild(value, this));
}

/**
Convenience function that is the same as adding a key path, then
subsequently adding a value path. The resulting ValuePath is returned.
*/
ValuePath* ValuePath::addPair(string key, string value)
{
    KeyPath* keyPath = this->addChild(key);
    ValuePath* valuePath = keyPath->addChild(value);

    return valuePath;
}

/**
When using C++, you should call KeyPath::addChild() or ValuePath::addChild().
This class is needed to support the C API. This function provides the main
implementation for addChild(), but doesn't provide the polymorphic behavior
that the KeyPath and ValuePath classes provide.

@see KeyPath::addChild(), ValuePath::addChild()
*/
KeyValueInterface* KeyValueInterface::addChild(string value, KeyValueInterface* parent)
{
    static const string WHITESPACE = " ";

    // trim following whitespace TODO: use/make library function
    value.erase(value.find_last_not_of(WHITESPACE) + 1);

    // trim leading whitespace TODO: use/make library function
    value.erase(0, value.find_first_not_of(WHITESPACE));

    KeyValueInterface* path = getChild(value);

    if (!path)
    {
        path = newChild(value, parent);
        children.push_back(path);
    }

    return path;
}

/**
The format of keyOrValueName can take a few different formats, but in
general, the query string KeyOrValueName is a path name that specifies
what to look for.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");
KeyPath* block = val1->addChild("block");

(void) block->addChild("eggs");
(void) block->addChild("spam");
ValuePath* transducer = block->addChild("transducer");

delete root; // free entire tree including children
@endcode

The above code would create the following tree structure (that could be
printed using debug_printTree():

@code
/device/rosey-644/block/eggs
/device/rosey-644/block/spam
/device/rosey-644/block/transducer
@endcode

Then, we would expect the following results:

@code
block->find("transducer") == transducer       // true
block->find("/transducer") == 0               // true

transducer->find("transducer") == 0           // true
transducer->find("/transducer") == transducer // true
@endcode

With no slash, the search includes searching through the children. With a
slash prepended to the query string, the search begins starting at this
node.

Note that the query string can have multiple path components separated by
forward slashes:

@code
block->find("/block/transducer") == transducer // true
block->find("block/transducer") == 0           // true
@endcode

As before, a slash says to start searching at this node, and no slash
means to start the search by looking through the child list of nodes.

@param keyOrValueName is the string text to search for.
*/
KeyValueInterface* KeyValueInterface::find(string keyOrValueName)
{
    if (keyOrValueName.empty()) return 0;

    const pair<string, string> split = chompFirst(keyOrValueName);

    if (keyOrValueName[0] != '/') // don't check 'this'; look for children
    {
        KeyValueInterface* child = getChild(split.first);

        if (child)
        {
            if (split.second.empty())
            {
                return child; // found
            }
            else
            {
                return child->find(split.second);
            }
        }
    }
    else
    {
        if (split.first == getName())
        {
            const pair<string, string> split2 = chompFirst(split.second);

            if (!split2.first.empty())
            {
                // find child to recurse into
                KeyValueInterface* child = getChild(split2.first);

                if (child)
                {
                    return child->find(split.second);
                }
            }
            else if (split2.second.empty())
            {
                return this;
            }
        }
    }

    return 0;
}


/**
Internally, this function uses find() and therefore accepts the same
path format for @b keyOrValueName.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");

ValuePath* val2 = val1->addPair("block", "transducer");
(void) val1->addPair("block", "AI");
@endcode

Let's say we wanted to get a list of all the children of "block." There
are two ways to go about this:

-# Use a "fully qualified" path query
-# Query the parent's child for all of that child's children

Here is the first way:

@code
// get all children of "block"
vector<KeyValueInterface*> objs = root->findChildren("/device/rosey-644/block");

objs.size() == 2;                     // true
objs[0]->getName() == "transducer";   // true
objs[1]->getName() == "AI";           // true
@endcode

Note that the absolute query in the above example is relative to @b root.

Here is the second way:

@code
// get all children of "block"
vector<KeyValueInterface*> objs = val1->findChildren("block");

objs.size() == 2;                     // true
objs[0]->getName() == "transducer";   // true
objs[1]->getName() == "AI";           // true

delete root;    // free entire tree including children
@endcode

This time, the search in the example above is relative to @b val1. Because
@b val1 is the node with the name "rosey-644", the child that is found
is "block", and the function then returns the list of the children of "block."

@param keyOrValueName is the search query or path to find
@return the list of children that were found, or
KeyValueInterface::emptyChildList if the node was not found.
@see find()
*/
const vector<KeyValueInterface*>& KeyValueInterface::findChildren(string keyOrValueName)
{
    KeyValueInterface* results = find(keyOrValueName);

    if (results)
    {
        return results->getChildren();
    }
    else
    {
        return emptyChildList;
    }
}

/**
The "topmost" parent is a node that has no parent. By definition, the root
of a key-value tree is a KeyPath. Conversely, the topmost parent (root)
cannot be a ValuePath.

@return A KeyPath* that points to the topmost parent.
*/
KeyPath* KeyValueInterface::findTopmostParent()
{
    KeyValueInterface* ptr = this;

    while(ptr->parent)
    {
        ptr = ptr->parent;
    }

    #if 0 // TEST
    ValuePath* valuePath = new ValuePath("val1");
    KeyPath* keyPath = new KeyPath("device");
    KeyValueInterface* p1 = dynamic_cast<KeyValueInterface*>(valuePath);
    KeyValueInterface* p2 = dynamic_cast<KeyValueInterface*>(keyPath);
    KeyPath* result = dynamic_cast<KeyPath*>(p1);  // ALWAYS null
    KeyPath* result2 = dynamic_cast<KeyPath*>(p2); // NEVER null
    KeyValueInterface* p3 = valuePath; // specific -> general conversion OK
    #endif // END TEST

    // if ptr is a KeyPath, the return value is a valid pointer to a KeyPath object
    // if ptr is a ValuePath, this dynamic cast returns null
    return dynamic_cast<KeyPath*>(ptr);
}

/**
The full path name (or "fully qualified" path name) is the concatination of
all parent nodes folllowed by this node.

### Example

@code
KeyPath* root = new KeyPath("device");
ValuePath* val1 = root->addChild("rosey-644");
ValuePath* val2 = val1->addChild("transducer");

root->fullPathName() == "/device";                       // true (non-leaf)
val1->fullPathName() == "/device/rosey-644";             // true (non-leaf)
val2->fullPathName() == "/device/rosey-644/transducer";  // true (leaf)

delete root; // free entire tree including children
@endcode

Note that the topmost parent is listed first, followed by a forward slash,
followed by a child node, etc. The full path name makes the most sense when
invoked on a leaf (a node that has no children), however, invoking it at
any node in the tree will print all of its parents.

@return The full path name of this node.
*/
string KeyValueInterface::fullPathName() const
{
    string path = "/" + getName();

    //const KeyValueInterface* ptr = getParent();
    const KeyValueInterface* ptr = parent;
    while (ptr)
    {
        path = "/" + ptr->getName() + path;
        //ptr = ptr->getParent();
        ptr = ptr->parent;
    }

    return path;
}

/**
Given either a KeyPath* node or ValuePath* node, call getName() to get the
string name of this path component. Each node in the key-value tree has
a string name which is returned by this function.

@return The string name of this node in the key-value tree.
*/
string KeyValueInterface::getName() const
{
    return component;
}





//
// C access API //////////////////////////////////////////
//
//KeyValueInterface* kv_add_child(KeyValueInterface* kv, const char* name)
//{
//    return kv->addChild(name, kv);
//}

KeyPath* kv_add_child_key(ValuePath* value_path, const char* name)
{
    return value_path->addChild(name);
}

ValuePath* kv_add_child_value(KeyPath* key_path, const char* name)
{
    return key_path->addChild(name);
}

ValuePath* kv_add_pair_str(ValuePath* value_path, const char* key, const char* value)
{
    return value_path->addPair(key, value);
}

ValuePath* kv_add_pair_int8(ValuePath* value_path, const char* key, int8_t value)
{
    // the note in kv_add_pair_uint8() applies here
    return value_path->addPair(
        key,
        NumberToString(static_cast<int>(value))
    );
}

ValuePath* kv_add_pair_uint8(ValuePath* value_path, const char* key, uint8_t value)
{
    // here we are interpreting "value" as a number,
    // so we need to cast it to a real integer or
    // NumberToString() won't convert it properly
    // (if we don't static_cast it to an int, it will
    // appear as an escaped hex like "\xF2" instead
    // of what we expect, "242".

    return value_path->addPair(
        key,
        NumberToString(static_cast<unsigned int>(value))
    );
}

ValuePath* kv_add_pair_int16(ValuePath* value_path, const char* key, int16_t value)
{
    return value_path->addPair(key, NumberToString(value));
}

ValuePath* kv_add_pair_uint16(ValuePath* value_path, const char* key, uint16_t value)
{
    return value_path->addPair(key, NumberToString(value));
}

ValuePath* kv_add_pair_int32(ValuePath* value_path, const char* key, int32_t value)
{
    return value_path->addPair(key, NumberToString(value));
}

ValuePath* kv_add_pair_uint32(ValuePath* value_path, const char* key, uint32_t value)
{
    return value_path->addPair(key, NumberToString(value));
}

ValuePath* kv_add_pair_float(ValuePath* value_path, const char* key, float value)
{
    return value_path->addPair(key, NumberToString(value));
}

void kv_get_path_name(KeyValueInterface* kv_path, char* dest, int maxLength)
{
    strncpy(dest, kv_path->getName().c_str(), maxLength);
}
