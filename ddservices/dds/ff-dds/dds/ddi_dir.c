/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#ifdef SUN
#include <memory.h>     /* K&R */
#else
#include <string.h>     /* ANSI */
#endif /* SUN */

#include "std.h"
#include "evl_lib.h"
#include "ddi_tab.h"
#include "fch_lib.h"
#include "app_xmal.h"

/*
 *	DEVICE_DIRECTORY_OFFSET really does not belong here.
 *	It will be moved when the FLAT table interface is implemented.
 */

#define DEVICE_DIRECTORY_OFFSET	0

/*********************************************************************
 *  Name: 	ddi_get_device_dd_handle
 *  ShortDesc: 	obtains dd_handle and dd_reference for device directory requests.
 *
 *  Description:
 *		This routine is responsible for obtaining dd_handle and dd_reference, from
 *		the ddi tables.  DD_HANDLE and DD_REF are needed to get directory information
 *		from EVAL and FETCH.
 *
 *  Inputs:
 *      req - ptr to a DDI_DEVICE_DIR_REQUEST.
 *
 *  Outputs:
 *		dd_handle - ptr to a DD_HANDLE.
 *      ref	- ptr to a DD_REFERENCE.
 *
 *  Returns:
 *      DDS_SUCCESS,
 * 		DDI_INVALID_DEV_TYPE_HANDLE;
 *		DDI_INVALID_DEVICE_HANDLE;
 * 		DDI_INVALID_REQUEST_TYPE;
 *		DDI_BAD_DD_DEVICE_LOAD;
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
static int
ddi_get_device_dd_handle(DDI_DEVICE_DIR_REQUEST *req, DD_HANDLE *dd_handle,
	DD_REFERENCE *ref)
{

	int		rc = DDS_SUCCESS;	/* return code */

	/*
	 * If DD_REFERENCE is not passed in, this routine knows where to get
	 * DD_REF from
	 */

	switch (req->type) {
		case DD_DD_HANDLE:

			/*
			 * DEVICE_DIR request by DD_REFERENCE and DD_HANDLE
			 */

			dd_handle->dd_handle_type = req->spec.ref.handle.dd_handle_type;
			dd_handle->dd_handle_number = req->spec.ref.handle.dd_handle_number;

			if (dd_handle->dd_handle_type == DDHT_ROD) {
				ref->object_index = req->spec.ref.ref.object_index;
			}
			else {		/** handle = DDHT_FLAT  **/
				ref->file_offset = req->spec.ref.ref.file_offset;
			}

			return DDS_SUCCESS;

		case DD_DT_HANDLE:

			/*
			 * DEVICE_DIR request by DEVICE_TYPE_HANDLE device_type_handle
			 * = index into ACTIVE_DEVICE_TYPE_TABLE
			 */

			if (!valid_device_type_handle(req->spec.device_type_handle)) {
				return DDI_INVALID_DEV_TYPE_HANDLE;
			}

			rc = get_adtt_dd_handle(req->spec.device_type_handle, 
					dd_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (dd_handle->dd_handle_type == DDHT_ROD) {
				ref->object_index = (OBJECT_INDEX) DEVICE_DIRECTORY_INDEX;
			}
			else {		/** handle = DDHT_FLAT  **/
				ref->file_offset = (FILE_OFFSET) DEVICE_DIRECTORY_OFFSET;
			}

			return DDS_SUCCESS;
	
		case DD_DEVICE_HANDLE:

			/*
			 * DEVICE_DIR request by DEVICE_HANDLE device_handle is an
			 * index into the "ACTIVE_DEVICE_TABLE"
			 */

			if (!valid_device_handle(req->spec.device_handle)) {
				return DDI_INVALID_DEVICE_HANDLE;
			}

			rc = get_adt_dd_handle(req->spec.device_handle, 
					dd_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (!valid_dd_handle(dd_handle)) {

				/*
				 * This device is currently not in the tables. Load the
				 * device tables. ds_dd_device_load() returns dd_handle)
				 */

				rc = ds_dd_device_load(req->spec.device_handle, dd_handle);
				if (rc) {
					return DDI_BAD_DD_DEVICE_LOAD;
				}
			}

			/*
			 * dd is loaded.
			 */

			if (dd_handle->dd_handle_type == DDHT_ROD) {
				ref->object_index = (OBJECT_INDEX) DEVICE_DIRECTORY_INDEX;
			}
			else {		/** handle = DDHT_FLAT  **/
				ref->file_offset = (FILE_OFFSET) DEVICE_DIRECTORY_OFFSET;
			}

			return DDS_SUCCESS;

		default:
			return DDI_INVALID_REQUEST_TYPE;
	}
}


/*********************************************************************
 *  Name: 	ddi_get_block_dd_handle
 *  ShortDesc: 	obtains dd_handle and dd_reference for block directory requests.
 *
 *  Description:
 *		This routine is responsible for obtaining dd_handle and dd_reference, from
 *		the ddi tables.  DD_HANDLE and DD_REF are needed to get directory information
 *		from EVAL and FETCH.
 *
 *  Inputs:
 *      req - ptr to a DDI_BLOCK_DIR_REQUEST.
 *
 *  Outputs:
 *		dd_handle - ptr to a DD_HANDLE.
 *      ref	- ptr to a DD_REFERENCE.
 *
 *  Returns:
 *      DDS_SUCCESS,
 * 		DDI_INVALID_DEV_TYPE_HANDLE;
 * 		DDI_INVALID_BLOCK_HANDLE;
 *		DDI_INVALID_REQUEST_TYPE;
 *		DDI_BAD_DD_BLOCK_LOAD;
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
static int
ddi_get_block_dd_handle(DDI_BLOCK_DIR_REQUEST *req, DD_HANDLE *dd_handle,
	DD_REFERENCE *ref)
{

	int             rc = DDS_SUCCESS;	/* return code */
	int				blk_tbl_offset ;

	/*
	 * If DD_REFERENCE is not passed in, this routine knows where to get
	 * DD_REF from
	 */

	switch( req->type ) {
		case BD_DD_HANDLE:

			/*
			 * BLOCK_DIR request by DD_REFERENCE and DD_HANDLE
			 */

			dd_handle->dd_handle_type = req->spec.ref.handle.dd_handle_type;
			dd_handle->dd_handle_number = req->spec.ref.handle.dd_handle_number;

			if (dd_handle->dd_handle_type == DDHT_ROD) {
				ref->object_index = req->spec.ref.ref.object_index;
			}
			else {		/** handle = DDHT_FLAT  **/
				ref->file_offset = req->spec.ref.ref.file_offset;
			}

			return DDS_SUCCESS;
	
		case BD_BLOCKNAME:

			/*
			 * BLOCK_DIR request by DEVICE_TYPE_HANDLE BLOCKNAME
			 */

			if (!valid_device_type_handle(req->spec.block.dt_handle)) {
				return DDI_INVALID_DEV_TYPE_HANDLE;
			}

			rc = get_adtt_dd_handle(req->spec.block.dt_handle, 
					dd_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			/*
			 * pass in a device_type_handle and a blockname and get back a
			 * dd_ref.
			 */

			rc = tr_gdr_device_type_handle_blockname(
				req->spec.block.dt_handle, req->spec.block.blockname, ref);

			return rc;
	
		case BD_BLOCK_HANDLE:

			/*
			 * BLOCK request by BLOCK_HANDLE
			 */

			if (!valid_block_handle(req->spec.block_handle)) {
				return DDI_INVALID_BLOCK_HANDLE;
			}

			/*
			 * If the Block_name_table_offset for this block_handle is
			 * invalid (ie < 0 ), The block table information has not been
			 * loaded from the dd.  Load the block table info.  We should
			 * now have a valid dd_handle.
			 */

			rc = get_abt_dd_blk_tbl_offset(req->spec.block_handle,
					&blk_tbl_offset) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (blk_tbl_offset < 0) {
				rc = ds_dd_block_load(req->spec.block_handle, dd_handle);
				if (rc) {
					return DDI_BAD_DD_BLOCK_LOAD;
				}
			}
			else {
				rc = get_abt_dd_handle(req->spec.block_handle, 
						dd_handle) ;
				if (rc != SUCCESS) {
					return(rc) ;
				}
			}

			/*
			 * dd is loaded. get dd_ref using block_handle.
			 */

			rc = tr_gdr_block_handle(req->spec.block_handle, ref);
			return rc;

		default:
			return DDI_INVALID_REQUEST_TYPE;
	}
}


/*********************************************************************
 *  Name: 	ddi_device_dir_request
 *  ShortDesc: 	Returns the desired device directories
 *
 *  Description:
 *		  This routine is a DDI interface routine which allows ddi users to get
 *		values from the device tables.
 *		  If this is a ROD request, a call to  fetch is made to get the desired
 *		chunks of device directory binary.  Eval is then called to evaluate the
 *		binary chunks.
 *		  If this is a FLAT request, the device tables are already evaluated.
 *		A call to fetch is made to get the desired device tables.
 *
 *  Inputs:
 *		dir_req - ptr to a DDI_DEVICE_DIR_REQUEST
 *
 *  Outputs:
 *		dir_resp - ptr to FLAT_DEVICE_DIR structure containing the desired
 *			device table values.
 *
 *  Returns:
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_device_dir_request(DDI_DEVICE_DIR_REQUEST *dir_req, FLAT_DEVICE_DIR *dir_resp)
{

	int             rc;			/* return code */
	DD_HANDLE       dd_handle;	/* device handle */
	DD_REFERENCE    dd_ref;		/* device_reference */
	SCRATCH_PAD     s_pad;		/* storage area for binary chunks */
	BIN_DEVICE_DIR  device_dir_bin;	/* list of ptrs to binary chunks */
	unsigned long   s_pad_size;	/* size to make scratch_pad */
	unsigned short  dr_type;	/* directory request type */

	ASSERT_RET(dir_req && dir_resp, DDI_INVALID_PARAM);

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, dir_resp);
#endif

	s_pad.size = 0;
	s_pad.used = 0;
	s_pad.pad = NULL;
	(void)memset((char *) &device_dir_bin, 0, sizeof(BIN_DEVICE_DIR));
	s_pad_size = 0;
	dr_type = DEVICE_DIR_TYPE;

	rc = ddi_get_device_dd_handle(dir_req, &dd_handle, &dd_ref);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	/**
	 *	If we have a ROD type handle:
	 *		get binary chunks from fetch.
	 *		call eval to evaluate binary chunks.
	 *	If we have a FLAT type handle: (the tables are already evaluated)
	 *		get tables from fetch.
	 **/

	if (dd_handle.dd_handle_type == DDHT_ROD) {

		/*
		 * Send fetch_request to determine the size of scratchpad needed.
		 */

		rc = fch_rod_dir_spad_size((ROD_HANDLE) dd_handle.dd_handle_number,
			dd_ref.object_index, &s_pad_size, dir_req->mask, dr_type);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		s_pad.pad = (unsigned char *) xmalloc((size_t) s_pad_size);
		if (s_pad.pad == NULL) {
			return DDL_MEMORY_ERROR;
		}
		s_pad.size = s_pad_size;

		/*
		 * Send second fetch_request to get the chunks of binary to be
		 * evaluated (ie. get a value). Fetch people say s_pad_size and
		 * s_pad->used must be zeroed out.
		 */

		s_pad_size = 0;
		s_pad.used = 0;
		rc = fch_rod_device_dir((ROD_HANDLE) dd_handle.dd_handle_number,
			&s_pad, &s_pad_size, dir_req->mask, &device_dir_bin);

		/*
		 * if s_pad_size > 0, we should not be returning DDS_SUCCESS.  I am
		 * assuming rod_fetch_dir() will not be returning DDS_SUCCESS in this
		 * instance.  Therefore, I am not checking the size of s_pad_size
		 * explicitly.
		 */

		if (rc != DDS_SUCCESS) {
			return rc;
		}

		rc = eval_dir_device_tables(dir_resp, &device_dir_bin, dir_req->mask);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		/*
		 * Free the memory malloc'd for scratch_pad.
		 */

		if (s_pad.pad) {
			xfree((void **) &s_pad.pad);
		}
	}
	else {			/* FLAT_DEVICE_DIR request */
		rc = fch_flat_device_dir((FLAT_HANDLE) dd_handle.dd_handle_number, dir_resp);
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, dir_resp);
#endif

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_block_dir_request
 *  ShortDesc: 	Returns the desired block directories
 *
 *  Description:
 *		This routine is a DDI interface routine which allows ddi users to get
 *		values from the block tables.  This function calls fetch to get the
 *		desired chunks of block directory binary.  It then calls eval to get
 *		the values of these block tables desired by the user.
 *
 *  Inputs:
 *		dir_req - ptr to a DDI_BLOCK_DIR_REQUEST
 *
 *  Outputs:
 *		dir_resp - ptr to FLAT_BLOCK_DIR structure containing the desired
 *			block table values.
 *
 *  Returns:
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_block_dir_request(DDI_BLOCK_DIR_REQUEST *dir_req, FLAT_BLOCK_DIR *dir_resp)
{

	int             rc;			/* return code */
	DD_HANDLE       dd_handle;	/* device handle */
	DD_REFERENCE    dd_ref;		/* device_reference */
	SCRATCH_PAD     s_pad;		/* storage area for binary chunks */
	BIN_BLOCK_DIR   block_dir_bin;	/* list of ptrs to binary chunks */
	unsigned long   s_pad_size;	/* size to make scratch_pad */
	unsigned short  dr_type;	/* directory request type */

	ASSERT_RET(dir_req && dir_resp, DDI_INVALID_PARAM);

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, dir_resp);
#endif

	s_pad.size = 0;
	s_pad.used = 0;
	s_pad.pad = NULL;
	(void)memset((char *) &block_dir_bin, 0, sizeof(BIN_BLOCK_DIR));
	s_pad_size = 0;
	dr_type = BLOCK_DIR_TYPE;

	rc = ddi_get_block_dd_handle(dir_req, &dd_handle, &dd_ref);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	/*
	 * If we have a ROD type handle, get binary chunks from fetch. call
	 * eval to evaluate binary chunks. If we have a FLAT type handle (the
	 * tables are already evaluated). get table information from fetch. do
	 * NOT call eval.
	 */

	if (dd_handle.dd_handle_type == DDHT_ROD) {

		/*
		 * Send fetch_request to determine the size of scratchpad needed.
		 */

		rc = fch_rod_dir_spad_size((ROD_HANDLE) dd_handle.dd_handle_number,
			dd_ref.object_index, &s_pad_size, dir_req->mask, dr_type);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		s_pad.pad = (unsigned char *) xmalloc((size_t) s_pad_size);
		if (s_pad.pad == NULL) {
			return DDL_MEMORY_ERROR;
		}
		s_pad.size = s_pad_size;

		/*
		 * Send second fetch_request to get the chunks of binary to be
		 * evaluated (ie. get a value). Fetch people say s_pad_size and
		 * s_pad->used must be zeroed out.
		 */

		s_pad_size = 0;
		s_pad.used = 0;
		rc = fch_rod_block_dir((ROD_HANDLE) dd_handle.dd_handle_number,
			dd_ref.object_index, &s_pad, &s_pad_size, dir_req->mask, &block_dir_bin);

		/*
		 * if s_pad_size > 0, we should not be returning DDS_SUCCESS.  I am
		 * assuming rod_fetch_dir() will not be returning DDS_SUCCESS in this
		 * instance.  Therefore, I am not checking the size of s_pad_size
		 * explicitly.
		 */

		if (rc != DDS_SUCCESS) {
			return rc;
		}

		rc = eval_dir_block_tables(dir_resp, &block_dir_bin, dir_req->mask);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		/*
		 * Free the memory malloc'd for scratch_pad.
		 */

		if (s_pad.pad) {
			xfree((void **) &s_pad.pad);
		}
	}
	else {			/** dd_handle.dd_handle_type == DDHT_FLAT **/

		rc = fch_flat_block_dir((FLAT_HANDLE) dd_handle.dd_handle_number,
			dd_ref.file_offset, dir_resp);
	}

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, dir_resp);
#endif

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_load_device_tables
 *  ShortDesc: 	Loads the device tables.
 *
 *  Description:
 *     	This routine is a DDI interface routine responsible for
 *		generating device tables.
 *
 *  Inputs:
 *		table_req - ptr to a DIR_SPECIFIER_REFERENCE structure which contains
 *			the dd_handle and the dd_reference of the desired device tables.
 *
 *  Outputs:
 *		table_resp - ptr to a FLAT_DEVICE_DIR structure which contains the
 *			values from the desired device tables.
 *
 *  Returns:
 *      return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/

#define DEVICE_TABLES   (BLK_TBL_MASK | ITEM_TBL_MASK)

int
ddi_load_device_tables(DIR_SPECIFIER_REFERENCE *table_req, FLAT_DEVICE_DIR *table_resp)
{

	int						rc;			/* return code */
	DDI_DEVICE_DIR_REQUEST	dir_req;	/* list of requested device tables */

	ASSERT_RET(table_req && table_resp, DDI_INVALID_PARAM);

	/*
	 * The BLOCK NAME TABLE and ITEM TABLE will be loaded if any
	 * of the other device tables are to be loaded, the user must call
	 * ddi_device_dir_request() with the appropriate masks set.
	 */

	dir_req.type = DD_DD_HANDLE;
    /* Changed req_mask from DEVICE_TABLES to DEVICE_TBL_MASKS to fill all the attributes.
     * Before changing the req_mask it was supplying only block table and item table attributes
     * since its required to fill all the attributes we using DEVICE_TBL_MASKS as req_mask
     */
    dir_req.mask = DEVICE_TBL_MASKS;
	dir_req.spec.ref.handle.dd_handle_type = table_req->handle.dd_handle_type;
	dir_req.spec.ref.handle.dd_handle_number = table_req->handle.dd_handle_number;
	if (dir_req.spec.ref.handle.dd_handle_type == DDHT_ROD) {
		dir_req.spec.ref.ref.object_index = table_req->ref.object_index;
	}
	else {		/* type = DDHT_FLAT */
		dir_req.spec.ref.ref.file_offset = table_req->ref.file_offset;
	}

	rc = ddi_device_dir_request(&dir_req, table_resp);

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_load_block_tables
 *  ShortDesc: 	Loads the block tables.
 *
 *  Description:
 *     	This routine is a DDI interface routine responsible for generating
 *		all of the block tables automatically.
 *
 *  Inputs:
 *		table_req - ptr to a DIR_SPECIFIER_REFERENCE structure which contains
 *			the dd_handle and the dd_reference of the desired block tables.
 *
 *  Outputs:
 *		table_resp - ptr to a FLAT_BLOCK_DIR structure which contains the
 *			values from the desired block tables.
 *
 *  Returns:
 *      return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_load_block_tables(DIR_SPECIFIER_REFERENCE *table_req, FLAT_BLOCK_DIR *table_resp)
{

	int						rc;			/* return code */
	DDI_BLOCK_DIR_REQUEST	dir_req;	/* list of requested block tables */

	ASSERT_RET(table_req && table_resp, DDI_INVALID_PARAM);

	dir_req.type = BD_DD_HANDLE;
	dir_req.mask = BLOCK_TBL_MASKS;

	dir_req.spec.ref.handle.dd_handle_type = table_req->handle.dd_handle_type;
	dir_req.spec.ref.handle.dd_handle_number = table_req->handle.dd_handle_number;
	if (dir_req.spec.ref.handle.dd_handle_type == DDHT_ROD) {
		dir_req.spec.ref.ref.object_index = table_req->ref.object_index;
	}
	else {		/* type = DDHT_FLAT */
		dir_req.spec.ref.ref.file_offset = table_req->ref.file_offset;
	}

	rc = ddi_block_dir_request(&dir_req, table_resp);

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_clean_block_dir
 *  ShortDesc: 	Frees the block tables
 *
 *  Description:
 *     	This routine is a DDI interface routine which allows ddi users to
 *		delete the block tables.
 *
 *  Inputs:
 *      block_dir - ptr to a FLAT_BLOCK_DIR structure, which contains the
 *			block tables to be deleted.
 *
 *  Outputs:
 *      none
 *
 *  Returns:
 *      void
 *
 *  Author:	steve beyerl
 *********************************************************************/
void
ddi_clean_block_dir(FLAT_BLOCK_DIR *block_dir)
{

	if (!block_dir) {
		return;
	}

	/*
	 * Call EVAL to free the block table.
	 */

	eval_clean_block_dir(block_dir);
}


/*********************************************************************
 *  Name: 	ddi_clean_device_dir
 *  ShortDesc: 	Frees device tables
 *
 *  Description:
 *     	This routine is a DDI interface routine which allows ddi users to
 *		delete the device tables.  Because the BLOCK_NAME_TABLE is to be 
 *		deleted, the block tables are also deleted.
 *
 *  Inputs:
 *      device_dir - ptr to a FLAT_DEVICE_DIR structure, which contains the
 *			device tables to be deleted.
 *
 *  Outputs:
 *      none
 *
 *  Returns:
 *      void
 *
 *  Author:	steve beyerl
 *********************************************************************/
void
ddi_clean_device_dir(FLAT_DEVICE_DIR *device_dir)
{

	int             i;	/* loop variable */
	int             count;	/* number of elements in BLOCK_NAME_TABLE */
	BLK_TBL_ELEM   *bt_ptr;	/* temp ptr to the BLOCK_NAME_TABLE */

	if (!device_dir) {
		return;
	}

	count = (int) device_dir->blk_tbl.count;
	bt_ptr = device_dir->blk_tbl.list;

	/*
	 * Loop to remove all block tables from the BLOCK_NAME_TABLE
	 */

	for (i = 0; i < count; i++) {
		eval_clean_block_dir(&bt_ptr[i].flat_block_dir);
	}

	/*
	 * Call EVAL to free the device table.
	 */

	eval_clean_device_dir(device_dir);
}
