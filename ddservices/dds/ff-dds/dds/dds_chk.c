/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	These are the DDS flat structure consistency check functions
 *
 */

/* #includes */
#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include	"dds_chk.h"

#include	"tst_fail.h"

/*
 * Function pointer used for the "fail" command in DDSTEST
 */

int (*incr_fail_func_count_ptr) P((int)) = 0;


/*********************************************************************
 *
 *	Name: 	chk_depbin
 *
 *	ShortDesc: 	Check consistency of DEPBIN structure elements
 *
 *	Description:
 *		This function checks the contents and references in an
 *		attribute's DEPBIN structure.  Pointers to and sizes of
 *		various items should be non-null/zero under certain
 *		conditions and zero under other conditions.  An ASSERT
 *		occurs if these conditions are violated.
 *
 *	Inputs:
 *		depbin_ptr -	pointer to attribute DEPBIN structure
 *
 *		attr_mask_bit - bit corresponding to current attribute
 *
 *		hooked -		item masks bin_hooked value
 *
 *		dynamic -		item masks dynamic value
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
chk_depbin(DEPBIN *depbin_ptr, unsigned long attr_mask_bit, unsigned long hooked,
	unsigned long dynamic)
{

	if (hooked & attr_mask_bit) {
	
		/*
		 * Check DEPBIN contents for attributes that correspond to set
		 * bin_hooked bits
		 */

		ASSERT_DBG(depbin_ptr);
		ASSERT_DBG(depbin_ptr->bin_chunk && depbin_ptr->bin_size);
	}
	else if (depbin_ptr) {

		/*
		 * Check for depbin chunk & size values that are not flagged as hooked
		 * but have non-zero values.
		 */

		ASSERT_DBG(!depbin_ptr->bin_chunk && !depbin_ptr->bin_size);
	}

	if (dynamic & attr_mask_bit) {

		/*
		 * Check DEPBIN contents for attributes that correspond to set dynamic
		 * bits
		 */

		ASSERT_DBG(depbin_ptr);
		ASSERT_DBG(depbin_ptr->bin_chunk && depbin_ptr->bin_size);
		ASSERT_DBG(depbin_ptr->dep.list && depbin_ptr->dep.count);
	}
	else if (depbin_ptr) {

		/*
		 * Check for depbin OP_REF_LIST values that are not flagged as dynamic
		 * but have non-zero values.
		 */

		ASSERT_DBG(!depbin_ptr->dep.list && !depbin_ptr->dep.count);
	}

}

/*********************************************************************
 *
 *	Name: 	var_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
var_flat_check(void *flat)
{

	FLAT_VAR       *var_flat;		/* pointer to item flat structure */
	VAR_ACTIONS_DEPBIN *var_actions_ptr; /* actions DEPBIN list pointer */
	VAR_MISC_DEPBIN *var_misc_ptr;	/* misc DEPBIN list pointer */
	VAR_DEPBIN     *var_main_ptr;	/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	var_flat = (FLAT_VAR *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(var_flat->masks.bin_exists &
			~((unsigned long) VAR_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (var_flat->masks.bin_hooked & (unsigned long) VAR_ACT_MASKS) {
		ASSERT_DBG(var_flat->actions);
		ASSERT_DBG(var_flat->actions->depbin);
	}
	if (var_flat->masks.bin_hooked & (unsigned long) VAR_MISC_MASKS) {
		ASSERT_DBG(var_flat->misc);
		ASSERT_DBG(var_flat->misc->depbin);
	}
	if (var_flat->masks.bin_hooked & (unsigned long) VAR_MAIN_MASKS) {
		ASSERT_DBG(var_flat->depbin);
	}

	/*
	 * Check for var_flat->misc, var_flat->actions structures if
	 * attr_avail bits are set
	 */

	if (var_flat->masks.attr_avail & (unsigned long) VAR_ACT_MASKS) {
		ASSERT_DBG(var_flat->actions);
	}
	if (var_flat->masks.attr_avail & (unsigned long) VAR_MISC_MASKS) {
		ASSERT_DBG(var_flat->misc);
	}

	/*
	 * Initialize the DEPBIN list pointers if they exist in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	var_actions_ptr = (VAR_ACTIONS_DEPBIN *) 0L;
	var_misc_ptr = (VAR_MISC_DEPBIN *) 0L;
	var_main_ptr = (VAR_DEPBIN *) 0L;

	if (var_flat->actions) {
		if (var_flat->actions->depbin) {
			var_actions_ptr = var_flat->actions->depbin;
		}
	}
	if (var_flat->misc) {
		if (var_flat->misc->depbin) {
			var_misc_ptr = var_flat->misc->depbin;
		}
	}
	if (var_flat->depbin) {
		var_main_ptr = var_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = var_flat->masks.bin_hooked;
	dynamic_attr = var_flat->masks.dynamic;
	eval_attr = var_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_VAR_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case VAR_CLASS_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_class;
				attr_mask_bit = (unsigned long) VAR_CLASS;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & VAR_CLASS)) {
				ASSERT_DBG(!var_flat->class_attr);
			}
			break;

		case VAR_HANDLING_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_handling;
				attr_mask_bit = (unsigned long) VAR_HANDLING;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & VAR_HANDLING)) {
				ASSERT_DBG(!var_flat->handling);
			}
			break;

		case VAR_UNIT_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_unit;
				attr_mask_bit = (unsigned long) VAR_UNIT;
			}

			/* Flat value type: STRING */

			if (eval_attr & VAR_UNIT) {
				if (var_flat->misc->unit.len) {
					ASSERT_DBG(var_flat->misc->unit.str);
				}
			}
			else if (var_flat->misc) {
				ASSERT_DBG(!var_flat->misc->unit.str &&
					!var_flat->misc->unit.len);
			}
			break;

		case VAR_LABEL_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_label;
				attr_mask_bit = (unsigned long) VAR_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_LABEL, var_flat->label);
			break;

		case VAR_HELP_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_help;
				attr_mask_bit = (unsigned long) VAR_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_HELP, var_flat->help);
			break;

		case VAR_READ_TIME_OUT_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_read_time_out;
				attr_mask_bit = (unsigned long) VAR_READ_TIME_OUT;
			}

			/* Flat value type: unsigned long */

			if (var_flat->misc) {
				if (!(eval_attr & VAR_READ_TIME_OUT)) {
					ASSERT_DBG(!var_flat->misc->read_time_out);
				}
			}
			break;

		case VAR_WRITE_TIME_OUT_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_write_time_out;
				attr_mask_bit = (unsigned long) VAR_WRITE_TIME_OUT;
			}

			/* Flat value type: unsigned long */

			if (var_flat->misc) {
				if (!(eval_attr & VAR_WRITE_TIME_OUT)) {
					ASSERT_DBG(!var_flat->misc->write_time_out);
				}
			}
			break;

		case VAR_VALID_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_valid;
				attr_mask_bit = (unsigned long) VAR_VALID;
			}

			/* Flat value type: unsigned long */

			if (var_flat->misc) {
				if (!(eval_attr & VAR_VALID)) {
					ASSERT_DBG(!var_flat->misc->valid);
				}
			}
			break;

		case VAR_PRE_READ_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_pre_read_act;
				attr_mask_bit = (unsigned long) VAR_PRE_READ_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_PRE_READ_ACT,
				var_flat->actions,
				var_flat->actions->pre_read_act);
			break;

		case VAR_POST_READ_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_post_read_act;
				attr_mask_bit = (unsigned long) VAR_POST_READ_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_POST_READ_ACT,
				var_flat->actions,
				var_flat->actions->post_read_act);
			break;

		case VAR_PRE_WRITE_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_pre_write_act;
				attr_mask_bit = (unsigned long) VAR_PRE_WRITE_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_PRE_WRITE_ACT,
				var_flat->actions,
				var_flat->actions->pre_write_act);
			break;

		case VAR_POST_WRITE_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_post_write_act;
				attr_mask_bit = (unsigned long) VAR_POST_WRITE_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_POST_WRITE_ACT,
				var_flat->actions,
				var_flat->actions->post_write_act);
			break;

		case VAR_PRE_EDIT_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_pre_edit_act;
				attr_mask_bit = (unsigned long) VAR_PRE_EDIT_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_PRE_EDIT_ACT,
				var_flat->actions,
				var_flat->actions->pre_edit_act);
			break;

		case VAR_POST_EDIT_ACT_ID:
			if (var_actions_ptr) {
				depbin_ptr = var_actions_ptr->db_post_edit_act;
				attr_mask_bit = (unsigned long) VAR_POST_EDIT_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_POST_EDIT_ACT,
				var_flat->actions,
				var_flat->actions->post_edit_act);
			break;

		case VAR_RESP_CODES_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) VAR_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & VAR_RESP_CODES)) {
				ASSERT_DBG(!var_flat->resp_codes);
			}
			break;

		case VAR_TYPE_SIZE_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_type_size;
				attr_mask_bit = (unsigned long) VAR_TYPE_SIZE;
			}

			/* Flat value type: TYPE_SIZE */

			if (!(eval_attr & VAR_TYPE_SIZE)) {
				ASSERT_DBG(!var_flat->type_size.size && 
					!var_flat->type_size.type);
			}
			break;

		case VAR_DISPLAY_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_display;
				attr_mask_bit = (unsigned long) VAR_DISPLAY;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_DISPLAY, var_flat->display);
			break;

		case VAR_EDIT_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_edit;
				attr_mask_bit = (unsigned long) VAR_EDIT;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_EDIT, var_flat->edit);
			break;

		case VAR_MIN_VAL_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_min_val;
				attr_mask_bit = (unsigned long) VAR_MIN_VAL;
			}

			/* Flat value type: RANGE_DATA_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_MIN_VAL,
				var_flat->misc,
				var_flat->misc->min_val);
			break;

		case VAR_MAX_VAL_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_max_val;
				attr_mask_bit = (unsigned long) VAR_MAX_VAL;
			}

			/* Flat value type: RANGE_DATA_LIST */

			CHK_VAR_FLAT_LIST(eval_attr, VAR_MAX_VAL,
				var_flat->misc,
				var_flat->misc->max_val);
			break;

		case VAR_SCALE_ID:
			if (var_misc_ptr) {
				depbin_ptr = var_misc_ptr->db_scale;
				attr_mask_bit = (unsigned long) VAR_SCALE;
			}

			/* Flat value type: EXPR */

			if (eval_attr & VAR_SCALE) {
				ASSERT_DBG(var_flat->misc->scale.type);
			}
			else if (var_flat->misc) {
				ASSERT_DBG(!var_flat->misc->scale.type);
			}
			break;

		case VAR_ENUMS_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_enums;
				attr_mask_bit = (unsigned long) VAR_ENUMS;
			}

			/* Flat value type: ENUM_VALUE_LIST */

			CHK_FLAT_LIST(eval_attr, VAR_ENUMS,
				var_flat->enums);
			break;

		case VAR_INDEX_ITEM_ARRAY_ID:
			if (var_main_ptr) {
				depbin_ptr = var_main_ptr->db_index_item_array;
				attr_mask_bit = (unsigned long) VAR_INDEX_ITEM_ARRAY;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & VAR_INDEX_ITEM_ARRAY)) {
				ASSERT_DBG(!var_flat->index_item_array);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	menu_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
menu_flat_check(void *flat)
{

	FLAT_MENU      *menu_flat;		/* pointer to item flat structure */
	MENU_DEPBIN    *menu_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	menu_flat = (FLAT_MENU *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(menu_flat->masks.bin_exists &
			~((unsigned long) MENU_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (menu_flat->masks.bin_hooked) {
		ASSERT_DBG(menu_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	menu_ptr = (MENU_DEPBIN *) 0L;

	if (menu_flat->depbin) {
		menu_ptr = menu_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = menu_flat->masks.bin_hooked;
	dynamic_attr = menu_flat->masks.dynamic;
	eval_attr = menu_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_MENU_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case MENU_LABEL_ID:
			if (menu_ptr) {
				depbin_ptr = menu_ptr->db_label;
				attr_mask_bit = (unsigned long) MENU_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, MENU_LABEL, menu_flat->label);
			break;

		case MENU_ITEMS_ID:
			if (menu_ptr) {
				depbin_ptr = menu_ptr->db_items;
				attr_mask_bit = (unsigned long) MENU_ITEMS;
			}

			/* Flat value type: MENU_ITEM_LIST */

			CHK_FLAT_LIST(eval_attr, MENU_ITEMS, menu_flat->items);
			break;

        case MENU_VALID_ID:
			if (menu_ptr) {
				depbin_ptr = menu_ptr->db_valid;
				attr_mask_bit = (unsigned long) MENU_VALID;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & MENU_VALID)) {
				ASSERT_DBG(!menu_flat->valid);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	edit_disp_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
edit_disp_flat_check(void *flat)
{

	FLAT_EDIT_DISPLAY *edit_disp_flat; /* pointer to item flat structure */
	EDIT_DISPLAY_DEPBIN *edit_disp_ptr; /* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	edit_disp_flat = (FLAT_EDIT_DISPLAY *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(edit_disp_flat->masks.bin_exists &
			~((unsigned long) EDIT_DISP_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (edit_disp_flat->masks.bin_hooked) {
		ASSERT_DBG(edit_disp_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	edit_disp_ptr = (EDIT_DISPLAY_DEPBIN *) 0L;

	if (edit_disp_flat->depbin) {
		edit_disp_ptr = edit_disp_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = edit_disp_flat->masks.bin_hooked;
	dynamic_attr = edit_disp_flat->masks.dynamic;
	eval_attr = edit_disp_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_EDIT_DISPLAY_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case EDIT_DISPLAY_LABEL_ID:
			if (edit_disp_ptr) {
				depbin_ptr = edit_disp_ptr->db_label;
				attr_mask_bit = (unsigned long) EDIT_DISPLAY_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, EDIT_DISPLAY_LABEL, edit_disp_flat->label);
			break;

		case EDIT_DISPLAY_EDIT_ITEMS_ID:
			if (edit_disp_ptr) {
				depbin_ptr = edit_disp_ptr->db_edit_items;
				attr_mask_bit = (unsigned long) EDIT_DISPLAY_EDIT_ITEMS;
			}

			/* Flat value type: OP_REF_TRAIL_LIST */

			CHK_FLAT_LIST(eval_attr, EDIT_DISPLAY_EDIT_ITEMS,
				edit_disp_flat->edit_items);
			break;

		case EDIT_DISPLAY_DISP_ITEMS_ID:
			if (edit_disp_ptr) {
				depbin_ptr = edit_disp_ptr->db_disp_items;
				attr_mask_bit = (unsigned long) EDIT_DISPLAY_DISP_ITEMS;
			}

			/* Flat value type: OP_REF_TRAIL_LIST */

			CHK_FLAT_LIST(eval_attr, EDIT_DISPLAY_DISP_ITEMS,
				edit_disp_flat->disp_items);
			break;

		case EDIT_DISPLAY_PRE_EDIT_ACT_ID:
			if (edit_disp_ptr) {
				depbin_ptr = edit_disp_ptr->db_pre_edit_act;
				attr_mask_bit = (unsigned long) EDIT_DISPLAY_PRE_EDIT_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, EDIT_DISPLAY_PRE_EDIT_ACT,
				edit_disp_flat->pre_edit_act);
			break;

		case EDIT_DISPLAY_POST_EDIT_ACT_ID:
			if (edit_disp_ptr) {
				depbin_ptr = edit_disp_ptr->db_post_edit_act;
				attr_mask_bit = (unsigned long) EDIT_DISPLAY_POST_EDIT_ACT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, EDIT_DISPLAY_POST_EDIT_ACT,
				edit_disp_flat->post_edit_act);
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	method_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
method_flat_check(void *flat)
{

	FLAT_METHOD    *method_flat;	/* pointer to item flat structure */
	METHOD_DEPBIN  *method_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	method_flat = (FLAT_METHOD *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(method_flat->masks.bin_exists &
			~((unsigned long) METHOD_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (method_flat->masks.bin_hooked) {
		ASSERT_DBG(method_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	method_ptr = (METHOD_DEPBIN *) 0L;

	if (method_flat->depbin) {
		method_ptr = method_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = method_flat->masks.bin_hooked;
	dynamic_attr = method_flat->masks.dynamic;
	eval_attr = method_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_METHOD_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case METHOD_CLASS_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_class;
				attr_mask_bit = (unsigned long) METHOD_CLASS;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & METHOD_CLASS)) {
				ASSERT_DBG(!method_flat->class_attr);
			}
			break;

		case METHOD_LABEL_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_label;
				attr_mask_bit = (unsigned long) METHOD_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, METHOD_LABEL, method_flat->label);
			break;

		case METHOD_HELP_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_help;
				attr_mask_bit = (unsigned long) METHOD_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, METHOD_HELP, method_flat->help);
			break;

		case METHOD_DEF_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_def;
				attr_mask_bit = (unsigned long) METHOD_DEF;
			}

			/* Flat value type: DEFINITION */

			if (eval_attr & METHOD_DEF) {
				if (method_flat->def.size) 	{
					ASSERT_DBG(method_flat->def.data);
				}
			}
			else {
				ASSERT_DBG(!method_flat->def.size && !method_flat->def.data);
			}
			break;

		case METHOD_VALID_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_valid;
				attr_mask_bit = (unsigned long) METHOD_VALID;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & METHOD_VALID)) {
				ASSERT_DBG(!method_flat->valid);
			}
			break;

		case METHOD_SCOPE_ID:
			if (method_ptr) {
				depbin_ptr = method_ptr->db_scope;
				attr_mask_bit = (unsigned long) METHOD_SCOPE;
			}

			/* Flat value type: int */

			if (!(eval_attr & METHOD_SCOPE)) {
				ASSERT_DBG(!method_flat->scope);
			}
			break;

		default:
			break;


		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	refresh_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
refresh_flat_check(void *flat)
{

	FLAT_REFRESH   *refresh_flat;	/* pointer to item flat structure */
	REFRESH_DEPBIN *refresh_ptr;	/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	refresh_flat = (FLAT_REFRESH *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(refresh_flat->masks.bin_exists &
			~((unsigned long) REFRESH_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (refresh_flat->masks.bin_hooked) {
		ASSERT_DBG(refresh_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	refresh_ptr = (REFRESH_DEPBIN *) 0L;

	if (refresh_flat->depbin) {
		refresh_ptr = refresh_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = refresh_flat->masks.bin_hooked;
	dynamic_attr = refresh_flat->masks.dynamic;
	eval_attr = refresh_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_REFRESH_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case REFRESH_ITEMS_ID:
			if (refresh_ptr) {
				depbin_ptr = refresh_ptr->db_items;
				attr_mask_bit = (unsigned long) REFRESH_ITEMS;
			}

			/* Flat value type: REFRESH_RELATION */

			if (eval_attr & REFRESH_ITEMS) {
				if (refresh_flat->items.depend_items.count) {
					ASSERT_DBG(refresh_flat->items.depend_items.limit &&
						refresh_flat->items.depend_items.list);
				}
				else {
					ASSERT_DBG((refresh_flat->items.depend_items.limit &&
						refresh_flat->items.depend_items.list) ||
						(!refresh_flat->items.depend_items.limit &&
						!refresh_flat->items.depend_items.list));
				}
				if (refresh_flat->items.update_items.count) {
					ASSERT_DBG(refresh_flat->items.update_items.limit &&
						refresh_flat->items.update_items.list);
				}
				else {
					ASSERT_DBG((refresh_flat->items.update_items.limit &&
						refresh_flat->items.update_items.list) ||
						(!refresh_flat->items.update_items.limit &&
						!refresh_flat->items.update_items.list));
				}
			}
			else {
				ASSERT_DBG(!refresh_flat->items.depend_items.count &&
					!refresh_flat->items.depend_items.limit &&
					!refresh_flat->items.depend_items.list);
				ASSERT_DBG(!refresh_flat->items.update_items.count &&
					!refresh_flat->items.update_items.limit &&
					!refresh_flat->items.update_items.list);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	unit_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
unit_flat_check(void *flat)
{

	FLAT_UNIT      *unit_flat;		/* pointer to item flat structure */
	UNIT_DEPBIN    *unit_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	unit_flat = (FLAT_UNIT *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(unit_flat->masks.bin_exists &
			~((unsigned long) UNIT_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (unit_flat->masks.bin_hooked) {
		ASSERT_DBG(unit_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	unit_ptr = (UNIT_DEPBIN *) 0L;

	if (unit_flat->depbin) {
		unit_ptr = unit_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = unit_flat->masks.bin_hooked;
	dynamic_attr = unit_flat->masks.dynamic;
	eval_attr = unit_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_UNIT_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case UNIT_ITEMS_ID:
			if (unit_ptr) {
				depbin_ptr = unit_ptr->db_items;
				attr_mask_bit = (unsigned long) UNIT_ITEMS;
			}

			/* Flat value type: UNIT_RELATION */

			if (eval_attr & UNIT_ITEMS) {
				if (unit_flat->items.var.trail_count) {
					ASSERT_DBG(unit_flat->items.var.trail_limit &&
						unit_flat->items.var.trail);
				}
				else {
					ASSERT_DBG((unit_flat->items.var.trail_limit &&
						unit_flat->items.var.trail) ||
						(!unit_flat->items.var.trail_limit &&
						!unit_flat->items.var.trail));
				}
				if (unit_flat->items.var_units.count) {
					ASSERT_DBG(unit_flat->items.var_units.limit &&
						unit_flat->items.var_units.list);
				}
				else {
					ASSERT_DBG((unit_flat->items.var_units.limit &&
						unit_flat->items.var_units.list) ||
						(!unit_flat->items.var_units.limit &&
						!unit_flat->items.var_units.list));
				}
			}
			else {
				ASSERT_DBG(!unit_flat->items.var.trail_count &&
					!unit_flat->items.var.trail_limit &&
					!unit_flat->items.var.trail);
				ASSERT_DBG(!unit_flat->items.var_units.count &&
					!unit_flat->items.var_units.limit &&
					!unit_flat->items.var_units.list);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	wao_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
wao_flat_check(void *flat)
{

	FLAT_WAO       *wao_flat;		/* pointer to item flat structure */
	WAO_DEPBIN     *wao_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	wao_flat = (FLAT_WAO *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(wao_flat->masks.bin_exists &
			~((unsigned long) WAO_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (wao_flat->masks.bin_hooked) {
		ASSERT_DBG(wao_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	wao_ptr = (WAO_DEPBIN *) 0L;

	if (wao_flat->depbin) {
		wao_ptr = wao_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = wao_flat->masks.bin_hooked;
	dynamic_attr = wao_flat->masks.dynamic;
	eval_attr = wao_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_WAO_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case WAO_ITEMS_ID:
			if (wao_ptr) {
				depbin_ptr = wao_ptr->db_items;
				attr_mask_bit = (unsigned long) WAO_ITEMS;
			}

			/* Flat value type: OP_REF_TRAIL_LIST */

			CHK_FLAT_LIST(eval_attr, WAO_ITEMS, wao_flat->items);
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	item_array_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
item_array_flat_check(void *flat)
{

	FLAT_ITEM_ARRAY *item_array_flat; /* pointer to item flat structure */
	ITEM_ARRAY_DEPBIN *item_array_ptr; /* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	item_array_flat = (FLAT_ITEM_ARRAY *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(item_array_flat->masks.bin_exists &
			~((unsigned long) ITEM_ARRAY_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (item_array_flat->masks.bin_hooked) {
		ASSERT_DBG(item_array_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	item_array_ptr = (ITEM_ARRAY_DEPBIN *) 0L;

	if (item_array_flat->depbin) {
		item_array_ptr = item_array_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = item_array_flat->masks.bin_hooked;
	dynamic_attr = item_array_flat->masks.dynamic;
	eval_attr = item_array_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_ITEM_ARRAY_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case ITEM_ARRAY_ELEMENTS_ID:
			if (item_array_ptr) {
				depbin_ptr = item_array_ptr->db_elements;
				attr_mask_bit = (unsigned long) ITEM_ARRAY_ELEMENTS;
			}

			/* Flat value type: ITEM_ARRAY_ELEMENT_LIST */

			CHK_FLAT_LIST(eval_attr, ITEM_ARRAY_ELEMENTS,
				item_array_flat->elements);
			break;

		case ITEM_ARRAY_LABEL_ID:
			if (item_array_ptr) {
				depbin_ptr = item_array_ptr->db_label;
				attr_mask_bit = (unsigned long) ITEM_ARRAY_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, ITEM_ARRAY_LABEL, item_array_flat->label);
			break;

		case ITEM_ARRAY_HELP_ID:
			if (item_array_ptr) {
				depbin_ptr = item_array_ptr->db_help;
				attr_mask_bit = (unsigned long) ITEM_ARRAY_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, ITEM_ARRAY_HELP, item_array_flat->help);
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	collection_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
collection_flat_check(void *flat)
{

	FLAT_COLLECTION *collection_flat; /* pointer to item flat structure */
	COLLECTION_DEPBIN *collection_ptr; /* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	collection_flat = (FLAT_COLLECTION *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(collection_flat->masks.bin_exists &
			~((unsigned long) COLLECTION_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (collection_flat->masks.bin_hooked) {
		ASSERT_DBG(collection_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	collection_ptr = (COLLECTION_DEPBIN *) 0L;

	if (collection_flat->depbin) {
		collection_ptr = collection_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = collection_flat->masks.bin_hooked;
	dynamic_attr = collection_flat->masks.dynamic;
	eval_attr = collection_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_COLLECTION_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case COLLECTION_MEMBERS_ID:
			if (collection_ptr) {
				depbin_ptr = collection_ptr->db_members;
				attr_mask_bit = (unsigned long) COLLECTION_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, COLLECTION_MEMBERS, 
				collection_flat->members);
			break;

		case COLLECTION_LABEL_ID:
			if (collection_ptr) {
				depbin_ptr = collection_ptr->db_label;
				attr_mask_bit = (unsigned long) COLLECTION_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, COLLECTION_LABEL, collection_flat->label);
			break;

		case COLLECTION_HELP_ID:
			if (collection_ptr) {
				depbin_ptr = collection_ptr->db_help;
				attr_mask_bit = (unsigned long) COLLECTION_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, COLLECTION_HELP, collection_flat->help);
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	block_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
block_flat_check(void *flat)
{

	FLAT_BLOCK     *block_flat;		/* pointer to item flat structure */
	BLOCK_DEPBIN   *block_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	block_flat = (FLAT_BLOCK *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(block_flat->masks.bin_exists &
			~((unsigned long) BLOCK_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (block_flat->masks.bin_hooked) {
		ASSERT_DBG(block_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	block_ptr = (BLOCK_DEPBIN *) 0L;

	if (block_flat->depbin) {
		block_ptr = block_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = block_flat->masks.bin_hooked;
	dynamic_attr = block_flat->masks.dynamic;
	eval_attr = block_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_BLOCK_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case BLOCK_CHARACTERISTIC_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_characteristic;
				attr_mask_bit = (unsigned long) BLOCK_CHARACTERISTIC;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & BLOCK_CHARACTERISTIC)) {
				ASSERT_DBG(!block_flat->characteristic);
			}
			break;

		case BLOCK_LABEL_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_label;
				attr_mask_bit = (unsigned long) BLOCK_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, BLOCK_LABEL, block_flat->label);
			break;

		case BLOCK_HELP_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_help;
				attr_mask_bit = (unsigned long) BLOCK_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, BLOCK_HELP, block_flat->help);
			break;

		case BLOCK_PARAM_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_param;
				attr_mask_bit = (unsigned long) BLOCK_PARAM;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_PARAM, block_flat->param);
			break;

		case BLOCK_MENU_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_menu;
				attr_mask_bit = (unsigned long) BLOCK_MENU;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_MENU, block_flat->menu);
			break;

		case BLOCK_EDIT_DISP_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_edit_disp;
				attr_mask_bit = (unsigned long) BLOCK_EDIT_DISP;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_EDIT_DISP, block_flat->edit_disp);
			break;

		case BLOCK_METHOD_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_method;
				attr_mask_bit = (unsigned long) BLOCK_METHOD;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_METHOD, block_flat->method);
			break;

		case BLOCK_UNIT_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_unit;
				attr_mask_bit = (unsigned long) BLOCK_UNIT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_UNIT, block_flat->unit);
			break;

		case BLOCK_REFRESH_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_refresh;
				attr_mask_bit = (unsigned long) BLOCK_REFRESH;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_REFRESH, block_flat->refresh);
			break;

		case BLOCK_WAO_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_wao;
				attr_mask_bit = (unsigned long) BLOCK_WAO;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_WAO, block_flat->wao);
			break;

		case BLOCK_COLLECT_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_collect;
				attr_mask_bit = (unsigned long) BLOCK_COLLECT;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_COLLECT, block_flat->collect);
			break;

		case BLOCK_ITEM_ARRAY_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_item_array;
				attr_mask_bit = (unsigned long) BLOCK_ITEM_ARRAY;
			}

			/* Flat value type: ITEM_ID_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_ITEM_ARRAY, block_flat->item_array);
			break;

		case BLOCK_PARAM_LIST_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_param_list;
				attr_mask_bit = (unsigned long) BLOCK_PARAM_LIST;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_PARAM_LIST, block_flat->param_list);
			break;

		case BLOCK_CHART_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_chart_members;
				attr_mask_bit = (unsigned long) BLOCK_CHART_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_CHART_MEMBERS, block_flat->chart_members);
			break;

        case BLOCK_FILE_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_file_members;
				attr_mask_bit = (unsigned long) BLOCK_FILE_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_FILE_MEMBERS, block_flat->file_members);
			break;

		case BLOCK_GRAPH_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_graph_members;
				attr_mask_bit = (unsigned long) BLOCK_GRAPH_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_GRAPH_MEMBERS, block_flat->graph_members);
			break;

		case BLOCK_GRID_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_grid_members;
				attr_mask_bit = (unsigned long) BLOCK_GRID_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_GRID_MEMBERS, block_flat->grid_members);
			break;

		case BLOCK_MENU_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_menu_members;
				attr_mask_bit = (unsigned long) BLOCK_MENU_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_MENU_MEMBERS, block_flat->menu_members);
			break;

		case BLOCK_METHOD_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_method_members;
				attr_mask_bit = (unsigned long) BLOCK_METHOD_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_METHOD_MEMBERS, block_flat->method_members);
			break;

		case BLOCK_LIST_MEMBERS_ID:
			if (block_ptr) {
				depbin_ptr = block_ptr->db_list_members;
				attr_mask_bit = (unsigned long) BLOCK_LIST_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, BLOCK_LIST_MEMBERS, block_flat->list_members);
			break;


		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	program_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
program_flat_check(void *flat)
{

	FLAT_PROGRAM   *program_flat;	/* pointer to item flat structure */
	PROGRAM_DEPBIN *program_ptr;	/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	program_flat = (FLAT_PROGRAM *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(program_flat->masks.bin_exists &
			~((unsigned long) PROGRAM_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (program_flat->masks.bin_hooked) {
		ASSERT_DBG(program_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	program_ptr = (PROGRAM_DEPBIN *) 0L;

	if (program_flat->depbin) {
		program_ptr = program_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = program_flat->masks.bin_hooked;
	dynamic_attr = program_flat->masks.dynamic;
	eval_attr = program_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_PROGRAM_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case PROGRAM_ARGS_ID:
			if (program_ptr) {
				depbin_ptr = program_ptr->db_args;
				attr_mask_bit = (unsigned long) PROGRAM_ARGS;
			}

			/* Flat value type: DATA_ITEM_LIST */

			CHK_FLAT_LIST(eval_attr, PROGRAM_ARGS, program_flat->args);
			break;

		case PROGRAM_RESP_CODES_ID:
			if (program_ptr) {
				depbin_ptr = program_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) PROGRAM_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & PROGRAM_RESP_CODES)) {
				ASSERT_DBG(!program_flat->resp_codes);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	record_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
record_flat_check(void *flat)
{

	FLAT_RECORD    *record_flat;	/* pointer to item flat structure */
	RECORD_DEPBIN  *record_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	record_flat = (FLAT_RECORD *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(record_flat->masks.bin_exists &
			~((unsigned long) RECORD_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (record_flat->masks.bin_hooked) {
		ASSERT_DBG(record_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	record_ptr = (RECORD_DEPBIN *) 0L;

	if (record_flat->depbin) {
		record_ptr = record_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = record_flat->masks.bin_hooked;
	dynamic_attr = record_flat->masks.dynamic;
	eval_attr = record_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_RECORD_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case RECORD_MEMBERS_ID:
			if (record_ptr) {
				depbin_ptr = record_ptr->db_members;
				attr_mask_bit = (unsigned long) RECORD_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, RECORD_MEMBERS, record_flat->members);
			break;

		case RECORD_LABEL_ID:
			if (record_ptr) {
				depbin_ptr = record_ptr->db_label;
				attr_mask_bit = (unsigned long) RECORD_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, RECORD_LABEL, record_flat->label);
			break;

		case RECORD_HELP_ID:
			if (record_ptr) {
				depbin_ptr = record_ptr->db_help;
				attr_mask_bit = (unsigned long) RECORD_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, RECORD_HELP, record_flat->help);
			break;

		case RECORD_RESP_CODES_ID:
			if (record_ptr) {
				depbin_ptr = record_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) RECORD_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & RECORD_RESP_CODES)) {
				ASSERT_DBG(!record_flat->resp_codes);
			}
			break;

        case RECORD_VALID_ID:
			if (record_ptr) {
				depbin_ptr = record_ptr->db_valid;
				attr_mask_bit = (unsigned long) RECORD_VALID;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & RECORD_VALID)) {
				ASSERT_DBG(!record_flat->valid);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	array_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
array_flat_check(void *flat)
{

	FLAT_ARRAY     *array_flat;		/* pointer to item flat structure */
	ARRAY_DEPBIN   *array_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	array_flat = (FLAT_ARRAY *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(array_flat->masks.bin_exists &
			~((unsigned long) ARRAY_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (array_flat->masks.bin_hooked) {
		ASSERT_DBG(array_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	array_ptr = (ARRAY_DEPBIN *) 0L;

	if (array_flat->depbin) {
		array_ptr = array_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = array_flat->masks.bin_hooked;
	dynamic_attr = array_flat->masks.dynamic;
	eval_attr = array_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_ARRAY_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case ARRAY_LABEL_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_label;
				attr_mask_bit = (unsigned long) ARRAY_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, ARRAY_LABEL, array_flat->label);
			break;

		case ARRAY_HELP_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_help;
				attr_mask_bit = (unsigned long) ARRAY_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, ARRAY_HELP, array_flat->help);
			break;

		case ARRAY_TYPE_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_type;
				attr_mask_bit = (unsigned long) ARRAY_TYPE;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & ARRAY_TYPE)) {
				ASSERT_DBG(!array_flat->type);
			}
			break;

		case ARRAY_NUM_OF_ELEMENTS_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_num_of_elements;
				attr_mask_bit = (unsigned long) ARRAY_NUM_OF_ELEMENTS;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & ARRAY_NUM_OF_ELEMENTS)) {
				ASSERT_DBG(!array_flat->num_of_elements);
			}
			break;

		case ARRAY_RESP_CODES_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) ARRAY_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & ARRAY_RESP_CODES)) {
				ASSERT_DBG(!array_flat->resp_codes);
			}
			break;

        case ARRAY_VALID_ID:
			if (array_ptr) {
				depbin_ptr = array_ptr->db_valid;
				attr_mask_bit = (unsigned long) ARRAY_VALID;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & ARRAY_VALID)) {
				ASSERT_DBG(!array_flat->valid);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	var_list_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
var_list_flat_check(void *flat)
{

	FLAT_VAR_LIST  *var_list_flat;	/* pointer to item flat structure */
	VAR_LIST_DEPBIN *var_list_ptr;	/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	var_list_flat = (FLAT_VAR_LIST *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(var_list_flat->masks.bin_exists &
			~((unsigned long) VAR_LIST_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (var_list_flat->masks.bin_hooked) {
		ASSERT_DBG(var_list_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	var_list_ptr = (VAR_LIST_DEPBIN *) 0L;

	if (var_list_flat->depbin) {
		var_list_ptr = var_list_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = var_list_flat->masks.bin_hooked;
	dynamic_attr = var_list_flat->masks.dynamic;
	eval_attr = var_list_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_VAR_LIST_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case VAR_LIST_MEMBERS_ID:
			if (var_list_ptr) {
				depbin_ptr = var_list_ptr->db_members;
				attr_mask_bit = (unsigned long) VAR_LIST_MEMBERS;
			}

			/* Flat value type: MEMBER_LIST */

			CHK_FLAT_LIST(eval_attr, VAR_LIST_MEMBERS, var_list_flat->members);
			break;

		case VAR_LIST_LABEL_ID:
			if (var_list_ptr) {
				depbin_ptr = var_list_ptr->db_label;
				attr_mask_bit = (unsigned long) VAR_LIST_LABEL;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_LIST_LABEL, var_list_flat->label);
			break;

		case VAR_LIST_HELP_ID:
			if (var_list_ptr) {
				depbin_ptr = var_list_ptr->db_help;
				attr_mask_bit = (unsigned long) VAR_LIST_HELP;
			}

			/* Flat value type: STRING */

			CHK_STRING(eval_attr, VAR_LIST_HELP, var_list_flat->help);
			break;

		case VAR_LIST_RESP_CODES_ID:
			if (var_list_ptr) {
				depbin_ptr = var_list_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) VAR_LIST_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & VAR_LIST_RESP_CODES)) {
				ASSERT_DBG(!var_list_flat->resp_codes);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	resp_codes_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
resp_codes_flat_check(void *flat)
{

	FLAT_RESP_CODE *resp_codes_flat; /* pointer to item flat structure */
	RESP_CODE_DEPBIN *resp_codes_ptr; /* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	resp_codes_flat = (FLAT_RESP_CODE *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(resp_codes_flat->masks.bin_exists &
			~((unsigned long) RESP_CODE_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (resp_codes_flat->masks.bin_hooked) {
		ASSERT_DBG(resp_codes_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	resp_codes_ptr = (RESP_CODE_DEPBIN *) 0L;

	if (resp_codes_flat->depbin) {
		resp_codes_ptr = resp_codes_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = resp_codes_flat->masks.bin_hooked;
	dynamic_attr = resp_codes_flat->masks.dynamic;
	eval_attr = resp_codes_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_RESP_CODE_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case RESP_CODE_MEMBER_ID:
			if (resp_codes_ptr) {
				depbin_ptr = resp_codes_ptr->db_member;
				attr_mask_bit = (unsigned long) RESP_CODE_MEMBER;
			}

			/* Flat value type: RESPONSE_CODE_LIST */

			CHK_FLAT_LIST(eval_attr, RESP_CODE_MEMBER, resp_codes_flat->member);
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}

/*********************************************************************
 *
 *	Name: 	domain_flat_check
 *
 *	ShortDesc:	Check consistency of flat structure
 *
 *	Description:
 *		Checks the DEPBIN structure contents against the state of 
 *		the masks values.  It also checks the flat attribute value
 *		state against the attr_avail state.  If available, the
 *		function then checks the consistency of the attribute value
 *		structure members, if applicable.
 *
 *	Inputs:
 *		flat -		pointer to item flat structure
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
static void
domain_flat_check(void *flat)
{

	FLAT_DOMAIN    *domain_flat;	/* pointer to item flat structure */
	DOMAIN_DEPBIN  *domain_ptr;		/* pointer to item DEPBIN list */
	DEPBIN         *depbin_ptr;		/* pointer to attribute DEPBIN */
	unsigned long   attr_mask_bit;	/* mask bit for current attribute */
	unsigned long   hooked_attr;	/* local copy of bin_hooked mask */
	unsigned long   dynamic_attr;	/* local copy of dynamic mask */
	unsigned long   eval_attr;		/* local copy of attr_avail mask */
	unsigned short  tag;

	/*
	 * Assign the pointer for the specific item flat structure
	 */

	domain_flat = (FLAT_DOMAIN *) flat;

	/*
	 * Check bin_exists for valid range of attributes
	 */

	ASSERT_DBG(!(domain_flat->masks.bin_exists &
			~((unsigned long) DOMAIN_ATTR_MASKS)));

	/*
	 * Check for DEPBIN lists as well as parent structures.
	 */

	if (domain_flat->masks.bin_hooked) {
		ASSERT_DBG(domain_flat->depbin);
	}

	/*
	 * Initialize the DEPBIN list pointer if it exists in the flat
	 * structure, regardless of whether associated bits in bin_hooked are
	 * set.  This will facilitate testing for consistency between DEPBIN
	 * values and mask field bits.
	 */

	domain_ptr = (DOMAIN_DEPBIN *) 0L;

	if (domain_flat->depbin) {
		domain_ptr = domain_flat->depbin;
	}

	/*
	 * Set mask values for item attributes.
	 */

	hooked_attr = domain_flat->masks.bin_hooked;
	dynamic_attr = domain_flat->masks.dynamic;
	eval_attr = domain_flat->masks.attr_avail;

	/*
	 * Check the state of the flat structure for each possible attribute
	 */

	for (tag = 0; tag < MAX_DOMAIN_ID; tag++) {

		depbin_ptr = (DEPBIN *) 0L;
		attr_mask_bit = 0L;

		switch (tag) {

		case DOMAIN_HANDLING_ID:
			if (domain_ptr) {
				depbin_ptr = domain_ptr->db_handling;
				attr_mask_bit = (unsigned long) DOMAIN_HANDLING;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & DOMAIN_HANDLING)) {
				ASSERT_DBG(!domain_flat->handling);
			}
			break;

		case DOMAIN_RESP_CODES_ID:
			if (domain_ptr) {
				depbin_ptr = domain_ptr->db_resp_codes;
				attr_mask_bit = (unsigned long) DOMAIN_RESP_CODES;
			}

			/* Flat value type: unsigned long */

			if (!(eval_attr & DOMAIN_RESP_CODES)) {
				ASSERT_DBG(!domain_flat->resp_codes);
			}
			break;

		default:
			break;

		}		/* end switch */

		/*
		 * This section checks the contents of any DEPBIN structures
		 * for consistency with the corresponding mask field bits.
		 */

		chk_depbin(depbin_ptr, attr_mask_bit, hooked_attr,
			dynamic_attr);

	}			/* end for */

}



/*********************************************************************
 *
 *	Name:	dds_item_flat_check
 *
 *	ShortDesc:	Items flat check interface routine
 *
 *	Description:
 *		Checks the four masks in the flat structure for consistency,
 *		then calls the apprpriate attribute-checking routine for the
 *		item.
 *
 *	Inputs:
 *		flat -			pointer to item flat structure
 *		itype -			type of item
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
void
dds_item_flat_check(void *flat,ITEM_TYPE itype)
{

	FLAT_MASKS	*masks;					/* pointer to item masks */
	void		(*attr_check_fn) ();	/* pointer to attribute check
										   function for item */

	/*
	 * Select the flat structure for the item type
	 */

	masks = (FLAT_MASKS *) 0;
	attr_check_fn = (void (*) ()) 0;

#ifdef lint
	if ( masks ) {
		attr_check_fn = (void (*) ()) 0;
	}
#endif
		
	switch (itype) {

	case VARIABLE_ITYPE:
		masks = &((FLAT_VAR *) flat)->masks;
		attr_check_fn = var_flat_check;
		break;

	case MENU_ITYPE:
		masks = &((FLAT_MENU *) flat)->masks;
		attr_check_fn = menu_flat_check;
		break;

	case EDIT_DISP_ITYPE:
		masks = &((FLAT_EDIT_DISPLAY *) flat)->masks;
		attr_check_fn = edit_disp_flat_check;
		break;

	case METHOD_ITYPE:
		masks = &((FLAT_METHOD *) flat)->masks;
		attr_check_fn = method_flat_check;
		break;

	case REFRESH_ITYPE:
		masks = &((FLAT_REFRESH *) flat)->masks;
		attr_check_fn = refresh_flat_check;
		break;

	case UNIT_ITYPE:
		masks = &((FLAT_UNIT *) flat)->masks;
		attr_check_fn = unit_flat_check;
		break;

	case WAO_ITYPE:
		masks = &((FLAT_WAO *) flat)->masks;
		attr_check_fn = wao_flat_check;
		break;

	case ITEM_ARRAY_ITYPE:
		masks = &((FLAT_ITEM_ARRAY *) flat)->masks;
		attr_check_fn = item_array_flat_check;
		break;

	case COLLECTION_ITYPE:
		masks = &((FLAT_COLLECTION *) flat)->masks;
		attr_check_fn = collection_flat_check;
		break;

	case BLOCK_ITYPE:
		masks = &((FLAT_BLOCK *) flat)->masks;
		attr_check_fn = block_flat_check;
		break;

	case PROGRAM_ITYPE:
		masks = &((FLAT_PROGRAM *) flat)->masks;
		attr_check_fn = program_flat_check;
		break;

	case RECORD_ITYPE:
		masks = &((FLAT_RECORD *) flat)->masks;
		attr_check_fn = record_flat_check;
		break;

	case ARRAY_ITYPE:
		masks = &((FLAT_ARRAY *) flat)->masks;
		attr_check_fn = array_flat_check;
		break;

	case VAR_LIST_ITYPE:
		masks = &((FLAT_VAR_LIST *) flat)->masks;
		attr_check_fn = var_list_flat_check;
		break;

	case RESP_CODES_ITYPE:
		masks = &((FLAT_RESP_CODE *) flat)->masks;
		attr_check_fn = resp_codes_flat_check;
		break;

	case DOMAIN_ITYPE:
		masks = &((FLAT_DOMAIN *) flat)->masks;
		attr_check_fn = domain_flat_check;
		break;

 	case AXIS_ITYPE:
		masks = &((FLAT_AXIS *) flat)->masks;
		attr_check_fn = NULL; /* axis_flat_check; */
		break;

 	case CHART_ITYPE:
		masks = &((FLAT_CHART *) flat)->masks;
		attr_check_fn = NULL; /* chart_flat_check; */
		break;

 	case FILE_ITYPE:
		masks = &((FLAT_FILE *) flat)->masks;
		attr_check_fn = NULL; /* file_flat_check; */
		break;

 	case GRAPH_ITYPE:
		masks = &((FLAT_GRAPH *) flat)->masks;
		attr_check_fn = NULL; /* graph_flat_check; */
		break;

 	case LIST_ITYPE:
		masks = &((FLAT_LIST *) flat)->masks;
		attr_check_fn = NULL; /* list_flat_check; */
		break;

 	case SOURCE_ITYPE:
		masks = &((FLAT_SOURCE *) flat)->masks;
		attr_check_fn = NULL; /* source_flat_check; */
		break;

 	case WAVEFORM_ITYPE:
		masks = &((FLAT_WAVEFORM *) flat)->masks;
		attr_check_fn = NULL; /* waveform_flat_check; */
		break;

	default:
		return;
	}

	/*
	 * Check the bin_exists and bin_hooked mask fields.  A bin_hooked bit
	 * can not be set if the corresponding bit in bin_exists is not set.
	 */

	ASSERT_DBG(!(masks->bin_hooked & ~masks->bin_exists));

	/*
	 * Check the bin_exists and dynamic mask fields.  A dynamic bit can not
	 * be set if the corresponding bit in bin_exists is not set.
	 */

	ASSERT_DBG(!(masks->dynamic & ~masks->bin_exists));

	/*
	 * Check the bin_hooked and dynamic mask fields.  A dynamic bit can not
	 * be set if the corresponding bit in bin_hooked is not set.
	 */

	ASSERT_DBG(!(masks->dynamic & ~masks->bin_hooked));

	/*
	 * Call the flat checking routine for the specific item flat structure
	 */

	if (attr_check_fn) {
		(*attr_check_fn) (flat);
	}
}

/*********************************************************************
 *
 *	Name: 	dds_dev_dir_flat_chk
 *
 *	ShortDesc:	Device directory flat structure check routine
 *
 *	Description:
 *		Checks the contents of the binary structure and/or flat 
 *		structure for the device directory.  The state of the 
 *		bin_hooked, bin_exists, and attr_avail masks are compared
 *		with the state of associated table structures.  An ASSERT 
 *		is generated if an inconsistency is detected.
 *
 *	Inputs:
 *		dev_dir_bin -	pointer to binary structure for the device
 *						directory.  Not checked if NULL is passed in.
 *
 *		dev_dir_flat -	pointer to flat structure for the device
 *						directory.  Not checked if NULL is passed in.
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
void
dds_dev_dir_flat_chk(BIN_DEVICE_DIR *dev_dir_bin, FLAT_DEVICE_DIR *dev_dir_flat)
{

	unsigned long	bin_hooked;	/* local storage for dev_dir_bin->bin_hooked */
	unsigned long	attr_avail; /* local storage for dev_dir_flat->attr_avail */

	if (dev_dir_bin) {

		/*
		 * Check bin_exists for valid range of attributes
		 */

		ASSERT_DBG(!(dev_dir_bin->bin_exists &
				~((unsigned long) DEVICE_TBL_MASKS | (1 << RESERVED1_TBL_ID))));

		/*
		 * Check the bin_exists and bin_hooked mask fields.  A
		 * bin_hooked bit can not be set if the corresponding bit in
		 * bin_exists is not set.
		 */

		ASSERT_DBG(!(dev_dir_bin->bin_hooked & ~dev_dir_bin->bin_exists));

		/*
		 * If the bin_hooked bit is set for a table, the BININFO
		 * structure values should both have values.  Otherwise, both
		 * values should be zero/null.
		 */

		bin_hooked = dev_dir_bin->bin_hooked;

		/* Block Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->blk_tbl, 
			BLK_TBL_MASK);

		/* Item Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->item_tbl, 
			ITEM_TBL_MASK);

		/* Program Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->prog_tbl, 
			PROG_TBL_MASK);

		/* Domain Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->domain_tbl, 
			DOMAIN_TBL_MASK);

		/* String Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->string_tbl, 
			STRING_TBL_MASK);

		/* Dictionary Reference Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->dict_ref_tbl, 
			DICT_REF_TBL_MASK);

		/* Local Variable Table */

		CHK_BIN_TABLE(bin_hooked, dev_dir_bin->local_var_tbl, 
			LOCAL_VAR_TBL_MASK);

	}

	if (dev_dir_flat) {

		attr_avail = dev_dir_flat->attr_avail;

		/* Block Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->blk_tbl, 
			BLK_TBL_MASK);

		/* Item Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->item_tbl, 
			ITEM_TBL_MASK);

		/* Program Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->prog_tbl, 
			PROG_TBL_MASK);

		/* Domain Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->domain_tbl, 
			DOMAIN_TBL_MASK);

		/* String Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->string_tbl, 
			STRING_TBL_MASK);

		/* Dictionary Reference Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->dict_ref_tbl, 
			DICT_REF_TBL_MASK);

		/* Local Variable Table */

		CHK_FLAT_TABLE(attr_avail, dev_dir_flat->local_var_tbl, 
			LOCAL_VAR_TBL_MASK);


	}
}

/*********************************************************************
 *
 *	Name: 	dds_blk_dir_flat_chk
 *
 *	ShortDesc:	Block directory flat structure check routine
 *
 *	Description:
 *		Checks the contents of the binary structure and/or flat 
 *		structure for a given block directory.  The state of the 
 *		bin_hooked, bin_exists, and attr_avail masks are compared
 *		with the state of associated table structures.  An ASSERT 
 *		is generated if an inconsistency is detected.
 *
 *	Inputs:
 *		blk_dir_bin -	pointer to binary structure for the block
 *						directory.  Not checked if NULL is passed in.
 *
 *		blk_dir_flat -	pointer to flat structure for the block
 *						directory.  Not checked if NULL is passed in.
 *
 *	Outputs:
 *		NONE
 *
 *	Returns:
 *		NONE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/
void
dds_blk_dir_flat_chk(BIN_BLOCK_DIR *blk_dir_bin, FLAT_BLOCK_DIR *blk_dir_flat)
{

	unsigned long	bin_hooked;	/* local storage for dev_dir_bin->bin_hooked */
	unsigned long	attr_avail; /* local storage for dev_dir_flat->attr_avail */

	if (blk_dir_bin) {

		/*
		 * Check bin_exists for valid range of attributes
		 */

		ASSERT_DBG(!(blk_dir_bin->bin_exists &
				~((unsigned long) BLOCK_TBL_MASKS)));

		/*
		 * Check the bin_exists and bin_hooked mask fields.  A
		 * bin_hooked bit can not be set if the corresponding bit in
		 * bin_exists is not set.
		 */

		ASSERT_DBG(!(blk_dir_bin->bin_hooked & ~blk_dir_bin->bin_exists));

		/*
		 * If the bin_hooked bit is set for a table, the BININFO
		 * structure values should both have values.  Otherwise, both
		 * values should be zero/null.
		 */

		/* Block Item Table */

		bin_hooked = blk_dir_bin->bin_hooked;

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->blk_item_tbl, 
			BLK_ITEM_TBL_MASK);

		/* Block Item Name Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->blk_item_name_tbl, 
			BLK_ITEM_NAME_TBL_MASK);

		/* Parameter Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_tbl,
			PARAM_TBL_MASK);

		/* Parameter Member Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_mem_tbl,
			PARAM_MEM_TBL_MASK);

		/* Parameter Member Name Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_mem_name_tbl,
			PARAM_MEM_NAME_TBL_MASK);

		/* Parameter Element Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_elem_tbl,
			PARAM_ELEM_TBL_MASK);

		/* Parameter List Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_list_tbl,
			PARAM_LIST_TBL_MASK);

		/* Parameter List Member Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_list_mem_tbl,
			PARAM_LIST_MEM_TBL_MASK);

		/* Parameter List Member Name Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->param_list_mem_name_tbl,
			PARAM_LIST_MEM_NAME_TBL_MASK);

		/* Characteristic Member Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->char_mem_tbl,
			CHAR_MEM_TBL_MASK);

		/* Characteristic Member Name Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->char_mem_name_tbl,
			CHAR_MEM_NAME_TBL_MASK);

		/* Relation Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->rel_tbl,
			REL_TBL_MASK);

		/* Update Table */

		CHK_BIN_TABLE(bin_hooked, blk_dir_bin->update_tbl,
			UPDATE_TBL_MASK);


	}

	if (blk_dir_flat) {

		/*
		 * If the attr_avail bit is set for a table, the table flat
		 * structure values should both have values.  Otherwise, both
		 * values should be zero/null.
		 */

		attr_avail = blk_dir_flat->attr_avail;

		/* Block Item Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->blk_item_tbl, 
			BLK_ITEM_TBL_MASK);

		/* Block Item Name Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->blk_item_name_tbl, 
			BLK_ITEM_NAME_TBL_MASK);

		/* Parameter Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_tbl,
			PARAM_TBL_MASK);

		/* Parameter Member Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_mem_tbl,
			PARAM_MEM_TBL_MASK);

		/* Parameter Member Name Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_mem_name_tbl,
			PARAM_MEM_NAME_TBL_MASK);

		/* Parameter Element Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_elem_tbl,
			PARAM_ELEM_TBL_MASK);

		/* Parameter List Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_list_tbl,
			PARAM_LIST_TBL_MASK);

		/* Parameter List Member Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_list_mem_tbl,
			PARAM_LIST_MEM_TBL_MASK);

		/* Parameter List Member Name Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->param_list_mem_name_tbl,
			PARAM_LIST_MEM_NAME_TBL_MASK);

		/* Characteristic Member Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->char_mem_tbl,
			CHAR_MEM_TBL_MASK);

		/* Characteristic Member Name Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->char_mem_name_tbl,
			CHAR_MEM_NAME_TBL_MASK);

		/* Relation Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->rel_tbl,
			REL_TBL_MASK);

		/* Update Table */

		CHK_FLAT_TABLE(attr_avail, blk_dir_flat->update_tbl,
			UPDATE_TBL_MASK);


	}
}
