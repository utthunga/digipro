#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    include the source files ###############################################
#############################################################################
set (FF_DDS_SRC ${FF_DDS_SRC}	${CMAKE_CURRENT_SOURCE_DIR}/ddi_dir.c
                		${CMAKE_CURRENT_SOURCE_DIR}/ddi_item.c
				${CMAKE_CURRENT_SOURCE_DIR}/ddi_lang.c
				${CMAKE_CURRENT_SOURCE_DIR}/ddi_tab.c
				${CMAKE_CURRENT_SOURCE_DIR}/dds_chk.c
				${CMAKE_CURRENT_SOURCE_DIR}/dds_cm.c
				${CMAKE_CURRENT_SOURCE_DIR}/dds_err.c
				${CMAKE_CURRENT_SOURCE_DIR}/dds_tab.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_base.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_cln.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_cond.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_dir.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_enum.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_expr.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_iary.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_item.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_mem.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_menu.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_ref.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_rel.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_resp.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_rslv.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_str.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_tran.c
				${CMAKE_CURRENT_SOURCE_DIR}/evl_type.c
				${CMAKE_CURRENT_SOURCE_DIR}/fch_rdir.c
				${CMAKE_CURRENT_SOURCE_DIR}/fch_ritm.c
				${CMAKE_CURRENT_SOURCE_DIR}/fch_rodf.c
				${CMAKE_CURRENT_SOURCE_DIR}/fch_slct.c
				PARENT_SCOPE)


