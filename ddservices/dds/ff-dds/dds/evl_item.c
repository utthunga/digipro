/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include "evl_loc.h"
#include "app_xmal.h"
#include "ddi_lib.h"
#include "tst_fail.h"

#include "flk_log4cplus_defs.h"
#include "flk_log4cplus_clogger.h"

extern char g_dds_language_code[];

/*
 *	Macros used to limit the availability of some of the type subattributes
 *	based on type in eval_item_var().
 */

#define INVALID_VAR_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

#define INVALID_ARITH_TYPE_SUBATTR_MASK ~(VAR_ENUMS | VAR_INDEX_ITEM_ARRAY)

#define INVALID_ENUM_TYPE_SUBATTR_MASK ~(VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_INDEX_ITEM_ARRAY )

#define INVALID_STRING_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

#define INVALID_INDEX_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS )

#define INVALID_DATE_TIME_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )


/*********************************************************************
 *
 *	Name: item_mask_man()
 *	ShortDesc: item_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		item_mask_man() handles all mask switch cases for the flat structure interface
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		attribute: 		the attribute to load if evaluation is necessary
 *		env_info:		the upcall information for this call to DDS
 *		var_needed:		the if the value of a variable is needed, but is not
 *						available, "var_needed" info is returned to the application
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by item_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *		var_needed:		var_needed specifies the variable needed to complete
 *						evaluation
 *		depbin:			the dependencies for this attribute
 *		var_needed:		the variable needed if parameter value service does not
 *						succeed.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
item_mask_man(unsigned long attr_mask, FLAT_MASKS *masks, int (*eval) (),
	DEPBIN *depbin, void *attribute, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */

	TEST_FAIL(MASK_MAN_EVAL);

	/*
	 * No binary exists
	 */

	if (!(attr_mask & masks->bin_exists)) {

		if (attr_mask & masks->attr_avail) {

			rc = DDL_SUCCESS;	/* value already exists */
		}

		/*
		 * Must be defaulted
		 */

		else {

			rc = DDL_DEFAULT_ATTR;
		}
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & masks->bin_hooked) && !(attr_mask & masks->dynamic)) {

		/*
		 * If value is already available
		 */

		if (attr_mask & masks->attr_avail) {

			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else {

			rc = DDL_BINARY_REQUIRED;
		}
	}

	else {

		/*
		 * check the pointer
		 */

		if (depbin == NULL) {
			rc = DDL_NULL_POINTER;
		}

		/*
		 * check masks for evaluating
		 */

		else if ((!(attr_mask & masks->attr_avail)) || (attr_mask & masks->dynamic)) {

			rc = (*eval) (depbin->bin_chunk, depbin->bin_size,
				attribute, &depbin->dep, env_info, var_needed);

			if (rc == DDL_SUCCESS) {
				masks->attr_avail |= attr_mask;
			}

			if (depbin->dep.count) {
				masks->dynamic |= attr_mask;
			}
			else {

				/*
				 * Turn off the dynamic mask for this attribute
				 */

				masks->dynamic &= ~attr_mask;
			}

		/*
		 * evaluation is not necessary
		 */
		} else {

			rc = DDL_SUCCESS;
		}
	}

	if (rc != DDL_SUCCESS) {

		/*
		 * Turn off the attr_available mask for this attribute
		 */

		masks->attr_avail &= ~attr_mask;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: load_errors
 *	ShortDesc: load the RETURN_LIST
 *
 *	Description:
 *		load_errors will append the RETURN_LIST with an error
 *
 *	Inputs:
 *		errors:		the RETURN_LIST
 *		rc:			the return code to add to the list
 *		var_needed:	the OP_REF if parameter value service does not succeed
 *		attr_mask:	the attribute that the error is for
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static void
load_errors(RETURN_LIST *errors, int rc, OP_REF *var_needed, unsigned long attr_mask)
{

	RETURN_INFO    *temp_errors;	/* temp storage for return info */

	if (errors->count >= RETURN_LIST_SIZE) {
		return;
	}

	temp_errors = &errors->list[errors->count++];
	temp_errors->bad_attr = attr_mask;
	temp_errors->var_needed.id = 0;
	temp_errors->var_needed.subindex = 0;
	temp_errors->var_needed.type = 0;

	switch (rc) {

		case DDL_DEFAULT_ATTR:

		/*
		 * if load error is called with a return code of
		 * DDL_DEFAULT_ATTR, convert this to a DDL_ENCODING ERROR
		 */
			temp_errors->rc = DDL_ENCODING_ERROR;
			break;

		default:
			if (var_needed && var_needed->id != 0) {
				temp_errors->var_needed.id = var_needed->id;
				temp_errors->var_needed.subindex = var_needed->subindex;
				temp_errors->var_needed.type = var_needed->type;
			}
			temp_errors->rc = rc;
			break;
	}
}


/*********************************************************************
 *
 *	Name: promote_range_data_element()
 *	ShortDesc: promote each element of a range data list to a specified type.
 *
 *	Description:
 *		Promote elements of a range data list to a specified type according
 *		to the following rules:
 *		specified type = int, uint, float, double and
 *				element type = int, uint, float, double
 *				-> promote element type.
 *		specified type = int, uint, float, double and
 *				element type = index, enum, bit_enum
 *				-> do nothing, go to next element.
 *		specified type = index, enum, bit_enum and
 *				element type = don't care
 *				-> return.
 *
 *	Inputs:
 *		type:	specified type to promote range data element to.
 *		list:	list of range data elements to be promoted.
 *
 *	Outputs:
 *		list:	list of promoted range data elements.
 *
 *	Returns:
 *		DDL_SUCCESS,
 *		DDL_INVALID_PARAM
 *
 *	Author: Steve Beyerl
 *
 **********************************************************************/
static void
promote_range_data_element(unsigned short type, RANGE_DATA_LIST *list)
{

	int             i;		/* loop counter */
	EXPR           *expr;	/* pointer to list of EXPRs */

	if (!list) {
		return;
	}

	if (!list->list || (list->count == 0)) {
		return;
	}

	expr = list->list;	/* pointer to the first element of the list */
	for (i = 0; i < list->count; i++, expr++) {
		switch (type) {	/* desired type */
			case DDS_INTEGER:
				switch (expr->type) {	/* current type */
					case DDS_INTEGER:
						expr->size = 4;
						break;
					case DDS_UNSIGNED:
					case DDS_INDEX:
					case DDS_ENUMERATED:
					case DDS_BIT_ENUMERATED:
						expr->val.i = (long) expr->val.u;
						expr->type = DDS_INTEGER;
						expr->size = 4;
						break;
					case DDS_FLOAT:
						expr->val.i = (long) expr->val.f;
						expr->type = DDS_INTEGER;
						expr->size = 4;
						break;
					case DDS_DOUBLE:
						expr->val.i = (long) expr->val.d;
						expr->type = DDS_INTEGER;
						expr->size = 4;
						break;
					default:
						break;
					}
					break;

			case DDS_UNSIGNED:
				switch (expr->type) {
					case DDS_INTEGER:
						expr->val.u = (unsigned long) expr->val.i;
						expr->type = DDS_UNSIGNED;
						expr->size = 4;
						break;
					case DDS_UNSIGNED:
					case DDS_INDEX:
					case DDS_ENUMERATED:
					case DDS_BIT_ENUMERATED:
						expr->type = DDS_UNSIGNED;
						expr->size = 4;
						break;
					case DDS_FLOAT:
						expr->val.u = (unsigned long) expr->val.f;
						expr->type = DDS_UNSIGNED;
						expr->size = 4;
						break;
					case DDS_DOUBLE:
						expr->val.u = (unsigned long) expr->val.d;
						expr->type = DDS_UNSIGNED;
						expr->size = 4;
						break;
					default:
						break;
					}
					break;

			case DDS_FLOAT:
				switch (expr->type) {
					case DDS_INTEGER:
						expr->val.f = (float) expr->val.i;
						expr->type = DDS_FLOAT;
						expr->size = 4;
						break;
					case DDS_UNSIGNED:
					case DDS_INDEX:
					case DDS_ENUMERATED:
					case DDS_BIT_ENUMERATED:
						expr->val.f = (float) expr->val.u;
						expr->type = DDS_FLOAT;
						expr->size = 4;
						break;
					case DDS_FLOAT:
						break;
					case DDS_DOUBLE:
						expr->val.f = (float) expr->val.d;
						expr->type = DDS_FLOAT;
						expr->size = 4;
						break;
					default:
						break;
					}
					break;

			case DDS_DOUBLE:
				switch (expr->type) {
					case DDS_INTEGER:
						expr->val.d = (double) expr->val.i;
						expr->type = DDS_DOUBLE;
						expr->size = 8;
						break;
					case DDS_UNSIGNED:
					case DDS_INDEX:
					case DDS_ENUMERATED:
					case DDS_BIT_ENUMERATED:
						expr->val.d = (double) expr->val.u;
						expr->type = DDS_DOUBLE;
						expr->size = 8;
						break;
					case DDS_FLOAT:
						expr->val.d = (double) expr->val.f;
						expr->type = DDS_DOUBLE;
						expr->size = 8;
						break;
					case DDS_DOUBLE:
						break;
					default:
						break;
					}
					break;

			default:	/* specified type = enum, bit_enum, index */
				return;
				/* NOTREACHED */
				break;
		}
	}			/* end of FOR */
}


/*********************************************************************
 *
 *	Name: eval_attr_reflist
 *	ShortDesc: evaluate a reflist
 *
 *	Description:
 *		eval_attr_reflist is a generic handler for reflists which are
 *		an optional attribute. It calls item_mask_man() and if DDL_DEFAULT is
 *		returned it loads an empty list and returns
 *
 *	Inputs:
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		env_info:		the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		reflist:		the reference list to load
 *		depbin:    		the depbin structure for this attribute
 *
 *	Outputs:
 *		errors:			the appended RETURN_LIST if there was an error
 *		reflist:		the loaded reflist if eval was called
 *		depbin:			the dependencies for this attribute
 *
 *	Returns:
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_reflist(FLAT_MASKS *masks, unsigned long attr_mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors, ITEM_ID_LIST *reflist, DEPBIN *depbin)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp var_needed storage */

	var_needed.id = 0;	/* Zero id says that this is not set */

	rc = item_mask_man(attr_mask, masks, (int (*) ()) eval_reflist,
		depbin, (void *) reflist, env_info, &var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			reflist->count = 0;
			masks->attr_avail |= attr_mask;

		} else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_attr_string
 *	ShortDesc: evaluate a string
 *
 *	Description:
 *		eval_attr_string is a generic function for evaluating a string
 *		when that string can be defaulted
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		string: 		the string to load if evaluation is necessary
 *		env_info:		the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		def_string:		indicates which default string should be gotten from
 *						standart.dct.
 *
 *	Outputs:
 *		depbin:			the dependencies for this attribute
 *		string:			the loaded string if eval was called
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		return 			void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static void
eval_attr_string(unsigned long attr_mask, FLAT_MASKS *masks, int (*eval) (),
	DEPBIN *depbin, STRING *string, ENV_INFO2 *env_info, RETURN_LIST *errors,
	unsigned long def_string)
{

	int             rc;	/* return code */
	OP_REF          var_needed;	/* temp var_needed storage */

	var_needed.id = 0;	/* Zero id says that this is not set */

	rc = item_mask_man(attr_mask, masks, (int (*) ()) eval, depbin,
		(void *) string, env_info, &var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			rc = app_func_get_dict_string((ENV_INFO *)env_info, def_string, string);

			/*
			 * If a string was not found, get the default error string.
			 */

			if (rc != DDL_SUCCESS) {

				rc = app_func_get_dict_string((ENV_INFO *)env_info, DEFAULT_STD_DICT_STRING, string);
				if (rc != DDL_SUCCESS) {

					load_errors(errors, rc, &var_needed, attr_mask);
				}
			}

			if (rc == DDL_SUCCESS) {

				masks->attr_avail |= attr_mask;

			} else {
				load_errors(errors, rc, &var_needed, attr_mask);
			}

		} else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return;
}


/*********************************************************************
 *
 *	Name: eval_attr_var_actions
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_attr_var_actions will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
eval_attr_var_actions(FLAT_VAR *var, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc = DDI_INVALID_FLAT_MASKS;	/* return code */
	FLAT_VAR_ACTIONS *act;			/* temp pointer for actions struct */
	FLAT_MASKS     *var_masks;		/* temp ptr to the masks of a var */

	act = var->actions;
	var_masks = &var->masks;

	if (mask & VAR_PRE_EDIT_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_EDIT_ACT,
			env_info, errors, &act->pre_edit_act,
			(act->depbin ? act->depbin->db_pre_edit_act : NULL));
	}

	if (mask & VAR_POST_EDIT_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_EDIT_ACT,
			env_info, errors, &act->post_edit_act,
			(act->depbin ? act->depbin->db_post_edit_act : NULL));
	}

	if (mask & VAR_PRE_READ_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_READ_ACT,
			env_info, errors, &act->pre_read_act,
			(act->depbin ? act->depbin->db_pre_read_act : NULL));
	}

	if (mask & VAR_POST_READ_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_READ_ACT,
			env_info, errors, &act->post_read_act,
			(act->depbin ? act->depbin->db_post_read_act : NULL));
	}

	if (mask & VAR_PRE_WRITE_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_WRITE_ACT,
			env_info, errors, &act->pre_write_act,
			(act->depbin ? act->depbin->db_pre_write_act : NULL));
	}

	if (mask & VAR_POST_WRITE_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_WRITE_ACT,
			env_info, errors, &act->post_write_act,
			(act->depbin ? act->depbin->db_post_write_act : NULL));
	}

	if (mask & VAR_REFRESH_ACT) {

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_REFRESH_ACT,
			env_info, errors, &act->refresh_act,
			(act->depbin ? act->depbin->db_refresh_act : NULL));
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_attr_var_misc
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_attr_var_misc will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
eval_attr_var_misc(FLAT_VAR *var, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc = DDI_INVALID_FLAT_MASKS;	/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_VAR_MISC  *misc;		/* temp pointer for misc struct */
	FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */

	var_needed.id = 0;	/* Zero id says that this is not set */

	misc = var->misc;
	var_masks = &var->masks;

	if (mask & VAR_UNIT) {
		rc = item_mask_man((unsigned long) VAR_UNIT, var_masks,
			(int (*) ()) eval_string, (misc->depbin ? misc->depbin->db_unit : NULL),
			(void *) &misc->unit, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->unit.len = 0;
				misc->unit.flags = 0;
				var_masks->attr_avail |= (unsigned long) VAR_UNIT;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_UNIT);
			}
		}
	}

	if (mask & VAR_WRITE_TIME_OUT) {

		rc = item_mask_man((unsigned long) VAR_WRITE_TIME_OUT, var_masks,
			(int (*) ()) eval_attr_int_expr,
			(misc->depbin ? misc->depbin->db_write_time_out : NULL),
			(void *) &misc->write_time_out, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->write_time_out = 0;
				var_masks->attr_avail |= (unsigned long) VAR_WRITE_TIME_OUT;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_WRITE_TIME_OUT);
			}
		}
	}

	if (mask & VAR_WRITE_MODE) {

		rc = item_mask_man((unsigned long) VAR_WRITE_MODE, var_masks,
			(int (*) ()) eval_attr_int_expr,
			(misc->depbin ? misc->depbin->db_write_mode : NULL),
			(void *) &misc->write_mode, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {
				var->misc->write_mode = 0;
				var_masks->attr_avail |= (unsigned long) VAR_WRITE_MODE;
			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_WRITE_MODE);
			}
		}
	}

	if (mask & VAR_READ_TIME_OUT) {

		rc = item_mask_man((unsigned long) VAR_READ_TIME_OUT, var_masks,
			(int (*) ()) eval_attr_int_expr,
			(misc->depbin ? misc->depbin->db_read_time_out : NULL),
			(void *) &misc->read_time_out, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->read_time_out = 0;
				var_masks->attr_avail |= (unsigned long) VAR_READ_TIME_OUT;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_READ_TIME_OUT);
			}
		}
	}

	if (mask & VAR_MIN_VAL) {

		rc = item_mask_man((unsigned long) VAR_MIN_VAL, var_masks,
			(int (*) ()) eval_attr_min_values,
			(misc->depbin ? misc->depbin->db_min_val : NULL),
			(void *) &misc->min_val, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->min_val.count = 0;
				var_masks->attr_avail |= (unsigned long) VAR_MIN_VAL;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_MIN_VAL);
			}

		} else {		/* promote the list of EXPR's */
			if (var_masks->attr_avail & (unsigned long) VAR_MIN_VAL) {
				promote_range_data_element(var->type_size.type, &misc->min_val);
			}
		}
	}

	if (mask & VAR_MAX_VAL) {

		rc = item_mask_man((unsigned long) VAR_MAX_VAL, var_masks,
			(int (*) ()) eval_attr_max_values,
			(misc->depbin ? misc->depbin->db_max_val : NULL),
			(void *) &misc->max_val, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->max_val.count = 0;
				var_masks->attr_avail |= (unsigned long) VAR_MAX_VAL;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_MAX_VAL);
			}

		} else {		/* promote the list of EXPR's */
			if (var_masks->attr_avail & (unsigned long) VAR_MAX_VAL) {
				promote_range_data_element(var->type_size.type, &misc->max_val);
			}
		}
	}

	if (mask & VAR_SCALE) {

		rc = item_mask_man((unsigned long) VAR_SCALE, var_masks,
			(int (*) ()) eval_attr_scaling_factor,
			(misc->depbin ? misc->depbin->db_scale : NULL),
			(void *) &misc->scale, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->scale.size = 1;
				misc->scale.type = DDS_INTEGER;
				misc->scale.val.i = 1;
				var_masks->attr_avail |= (unsigned long) VAR_SCALE;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_SCALE);
			}
		}
	}

	if (mask & VAR_VALID) {

		rc = item_mask_man((unsigned long) VAR_VALID, var_masks,
			(int (*) ()) eval_attr_ulong,
			(misc->depbin ? misc->depbin->db_valid : NULL),
			(void *) &misc->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->valid = TRUE;
				var_masks->attr_avail |= (unsigned long) VAR_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_VALID);
			}
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_item_var
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_item_var will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_var_actions(), eval_attr_var_misc()
 *		eval_attr_string(), eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_var(FLAT_VAR *var, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	unsigned long   index;		/* Standard dictionary index value */
	FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */

	TEST_FAIL(I_EVAL_VAR);

#ifdef DEBUG
	dds_item_flat_check((void *) var, VARIABLE_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	var_masks = &var->masks;

	/*
	 * Sanity check
	 */

	if ((var_masks->bin_hooked & VAR_MAIN_MASKS) && (var->depbin == NULL)) {

		return DDL_NULL_POINTER;
	}

	/*
     *  Fetch automatically looks for a TYPE binary chunk.  Type needs to be
	 *  evaluated, to determine if the desired type subattributes make sense.
     */

	rc = item_mask_man((unsigned long) VAR_TYPE_SIZE, var_masks,
		(int (*) ()) eval_attr_type,
		(var->depbin ? var->depbin->db_type_size : NULL),
		(void *) &var->type_size, env_info, &var_needed);


	if (rc != DDL_SUCCESS) {
		load_errors(errors, rc, &var_needed, (unsigned long) VAR_TYPE_SIZE);
	}

	/*
     *  Some combinations of var_type and var_type_subattributes do not
     *  make any sense.  The following code is an attempt to remove
     *  invalid combinations of type and type_subattributes.
     */

	if (var_masks->attr_avail & (unsigned long) VAR_TYPE_SIZE) {
		switch (var->type_size.type) {
			case DDS_INTEGER:
			case DDS_UNSIGNED:
			case DDS_FLOAT:
			case DDS_DOUBLE:
				mask &= INVALID_ARITH_TYPE_SUBATTR_MASK;
				break;

			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
				mask &= INVALID_ENUM_TYPE_SUBATTR_MASK;
				break;

			case DDS_INDEX:
				mask &= INVALID_INDEX_TYPE_SUBATTR_MASK;
				break;

			case DDS_EUC:
			case DDS_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
				mask &= INVALID_STRING_TYPE_SUBATTR_MASK;
				break;

			case DDS_TIME:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				mask &= INVALID_DATE_TIME_TYPE_SUBATTR_MASK;
				break;

			default:	/* should never happen */
				mask &= INVALID_VAR_TYPE_SUBATTR_MASK;
				break;
		}

	} else {			/* TYPE is not available */
		mask &= INVALID_VAR_TYPE_SUBATTR_MASK;
	}

	if (mask & VAR_CLASS) {

		rc = item_mask_man((unsigned long) VAR_CLASS, var_masks,
			(int (*) ()) eval_attr_bitstring,
			(var->depbin ? var->depbin->db_class : NULL),
			(void *) &var->class_attr, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) VAR_CLASS);
		}
	}


	if (mask & VAR_HANDLING) {

		rc = item_mask_man((unsigned long) VAR_HANDLING, var_masks,
			(int (*) ()) eval_attr_bitstring,
			(var->depbin ? var->depbin->db_handling : NULL),
			(void *) &var->handling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->handling = READ_HANDLING | WRITE_HANDLING;
				var_masks->attr_avail |= (unsigned long) VAR_HANDLING;

			} else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_HANDLING);
			}
		}
	}

	if (mask & VAR_HELP) {

		eval_attr_string((unsigned long) VAR_HELP, var_masks,
			(int (*) ()) eval_string,
			(var->depbin ? var->depbin->db_help : NULL),
			&var->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & VAR_LABEL) {

		eval_attr_string((unsigned long) VAR_LABEL, var_masks,
			(int (*) ()) eval_string,
			(var->depbin ? var->depbin->db_label : NULL),
			&var->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & VAR_DISPLAY) {

		rc = item_mask_man((unsigned long) VAR_DISPLAY, var_masks,
			(int (*) ()) eval_attr_display_format,
			(var->depbin ? var->depbin->db_display : NULL),
			(void *) &var->display, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
			if (rc == DDL_DEFAULT_ATTR) {
				if (!(var_masks->attr_avail & VAR_TYPE_SIZE)) {
					load_errors(errors, DDL_VAR_TYPE_NEEDED, (OP_REF *) NULL,
						(unsigned long) VAR_DISPLAY);

				} else {
					switch (var->type_size.type) {
					case DDS_INTEGER:

						/*
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the integer DEFAULT DISPLAY FORMAT from the
						 * standard dictionary.
						 */

						index = DEFAULT_STD_DICT_DISP_INT;
						break;

					case DDS_UNSIGNED:

						/*
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the unsigned integer DEFAULT DISPLAY FORMAT from
						 * the standard dictionary.
						 */

						index = DEFAULT_STD_DICT_DISP_UINT;
						break;

					case DDS_FLOAT:

						/*
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the floating point DEFAULT DISPLAY FORMAT from
						 * the standard dictionary.
						 */

						index = DEFAULT_STD_DICT_DISP_FLOAT;
						break;

					case DDS_DOUBLE:

						/*
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the double DEFAULT DISPLAY FORMAT from the
						 * standard dictionary.
						 */

						index = DEFAULT_STD_DICT_DISP_DOUBLE;
						break;

					default:	/* should never occur */
						return DDL_INVALID_TYPE_SUBATTR;
					}

					/*
					 * If a DISPLAY FORMAT string was not
					 * found in the binary, use the FORMAT
					 * from the standard dictionary.
					 */

					rc = app_func_get_dict_string((ENV_INFO *)env_info, index, &var->display);

					/*
					 * If a string was not found, get the
					 * default error string.
					 */

					if (rc != DDL_SUCCESS) {
						rc = app_func_get_dict_string((ENV_INFO *)env_info, DEFAULT_STD_DICT_STRING,
							&var->display);
						if (rc != DDL_SUCCESS) {
							load_errors(errors, rc, &var_needed,
								(unsigned long) VAR_DISPLAY);
						}
					}
					if (rc == DDL_SUCCESS) {
						var->masks.attr_avail |= VAR_DISPLAY;
					}
				}
			}
			else {
				load_errors(errors, rc, &var_needed,
					(unsigned long) VAR_DISPLAY);
			}
		}
	}

	if (mask & VAR_EDIT) {
		rc = item_mask_man((unsigned long) VAR_EDIT,
			var_masks,
			(int (*) ()) eval_attr_edit_format,
			(var->depbin ? var->depbin->db_edit : NULL),
			(void *) &var->edit,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {
			if (rc == DDL_DEFAULT_ATTR) {
				if (!(var_masks->attr_avail & VAR_TYPE_SIZE)) {
					load_errors(errors, DDL_VAR_TYPE_NEEDED, (OP_REF *) NULL,
						(unsigned long) VAR_EDIT);

				} else {
					switch (var->type_size.type) {
						case DDS_INTEGER:

							/*
							 * If an EDIT FORMAT string was not found in the
							 * binary, use the integer DEFAULT EDIT FORMAT
							 * from the standard dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_INT;
							break;

						case DDS_UNSIGNED:

							/*
							 * If an EDIT FORMAT string was not found in the
							 * binary, use the integer DEFAULT EDIT FORMAT
							 * from the standard dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_UINT;
							break;

						case DDS_FLOAT:

							/*
							 * If an EDIT FORMAT string was not found in the
							 * binary, use the integer DEFAULT EDIT FORMAT
							 * from the standard dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_FLOAT;
							break;

						case DDS_DOUBLE:

							/*
							 * If an EDIT FORMAT string was not found in the
							 * binary, use the integer DEFAULT EDIT FORMAT
							 * from the standard dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_DOUBLE;
							break;

						default:	/* should never occur */
							return DDL_INVALID_TYPE_SUBATTR;
					}

					/*
					 * If an EDIT FORMAT string was not found in the
					 * binary, use the integer DEFAULT EDIT FORMAT
					 * from the standard dictionary.
					 */

					rc = app_func_get_dict_string((ENV_INFO *)env_info, index, &var->edit);

					/*
					 * If a string was not found, get the
					 * default error string.
					 */

					if (rc != DDL_SUCCESS) {
						rc = app_func_get_dict_string((ENV_INFO *)env_info, DEFAULT_STD_DICT_STRING,
							&var->edit);
						if (rc != DDL_SUCCESS) {
							load_errors(errors, rc, &var_needed,
								(unsigned long) VAR_EDIT);
						}
					}
					if (rc == DDL_SUCCESS) {
						var->masks.attr_avail |= VAR_EDIT;
					}
				}

			} else {
				load_errors(errors, rc, &var_needed,
					(unsigned long) VAR_EDIT);
			}
		}
	}

	if (mask & VAR_ENUMS) {

		rc = item_mask_man((unsigned long) VAR_ENUMS, var_masks,
			(int (*) ()) eval_attr_enum,
			(var->depbin ? var->depbin->db_enums : NULL),
			(void *) &var->enums, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->enums.count = 0;
				var_masks->attr_avail |= (unsigned long) VAR_ENUMS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_ENUMS);
			}
		}
	}

	if (mask & VAR_INDEX_ITEM_ARRAY) {

		rc = item_mask_man((unsigned long) VAR_INDEX_ITEM_ARRAY, var_masks,
			(int (*) ()) eval_attr_arrayname,
			(var->depbin ? var->depbin->db_index_item_array : NULL),
			(void *) &var->index_item_array, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->index_item_array = 0;
				var_masks->attr_avail |= (unsigned long) VAR_INDEX_ITEM_ARRAY;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_INDEX_ITEM_ARRAY);
			}
		}
	}


	if (mask & VAR_RESP_CODES) {

		rc = item_mask_man((unsigned long) VAR_RESP_CODES, var_masks,
			(int (*) ()) eval_attr_ref,
			(var->depbin ? var->depbin->db_resp_codes : NULL),
			(void *) &var->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->resp_codes = 0;
				var_masks->attr_avail |= (unsigned long) VAR_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_RESP_CODES);
			}
		}
	}

	if (mask & VAR_ACT_MASKS) {	/* if any action masks are set */

		if (var->actions == NULL) {

			return DDL_NULL_POINTER;
		}

		if ((var_masks->bin_hooked & VAR_ACT_MASKS) && (var->actions->depbin == NULL)) {

			return DDL_NULL_POINTER;
		}

		rc = eval_attr_var_actions(var, mask, env_info, errors);

	}

	if (mask & VAR_MISC_MASKS) {	/* if any misc masks are set */

		if (var->misc == NULL) {

			return DDL_NULL_POINTER;
		}

		if ((var_masks->bin_hooked & VAR_MISC_MASKS) && (var->misc->depbin == NULL)) {

			return DDL_NULL_POINTER;
		}

		rc = eval_attr_var_misc(var, mask, env_info, errors);
	}

#ifdef DEBUG
	dds_item_flat_check((void *) var, VARIABLE_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

        LOGC_TRACE (CPM_FF_DDSERVICE, "Variable total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_program
 *	ShortDesc: evaluate the FLAT_PROGRAM
 *
 *	Description:
 *		eval_item_program will load the FLAT_PROGRAM structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_program() call
 *
 *	Outputs:
 *		program:		the loaded FLAT_PROGRAM structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_program(FLAT_PROGRAM *program, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *prog_masks;	/* temp ptr to the masks of a program */

	TEST_FAIL(I_EVAL_PROGRAM);

#ifdef DEBUG
	dds_item_flat_check((void *) program, PROGRAM_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	prog_masks = &program->masks;

	/*
	 * Sanity check
	 */

	if ((program->depbin == NULL) && (prog_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & PROGRAM_ARGS) {

		rc = item_mask_man((unsigned long) PROGRAM_ARGS, prog_masks,
			(int (*) ()) eval_attr_dataitems,
			(program->depbin ? program->depbin->db_args : NULL),
			(void *) &program->args, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) PROGRAM_ARGS);
		}
	}

	if (mask & PROGRAM_RESP_CODES) {

		rc = item_mask_man((unsigned long) PROGRAM_RESP_CODES, prog_masks,
			(int (*) ()) eval_attr_ref,
			(program->depbin ? program->depbin->db_resp_codes : NULL),
			(void *) &program->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				program->resp_codes = 0;
				prog_masks->attr_avail |= (unsigned long) PROGRAM_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) PROGRAM_RESP_CODES);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) program, PROGRAM_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_menu
 *	ShortDesc: evaluate the FLAT_MENU
 *
 *	Description:
 *		eval_item_menu will load the FLAT_MENU structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_menu() call
 *
 *	Outputs:
 *		menu:			the loaded FLAT_MENU structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_menu(FLAT_MENU *menu, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *menu_masks;	/* temp ptr to the masks of a menu */

	TEST_FAIL(I_EVAL_MENU);

#ifdef DEBUG
	dds_item_flat_check((void *) menu, MENU_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	menu_masks = &menu->masks;

	/*
	 * Sanity check
	 */

	if ((menu->depbin == NULL) && (menu_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & MENU_LABEL) {

		rc = item_mask_man((unsigned long) MENU_LABEL, menu_masks,
			(int (*) ()) eval_string,
			(menu->depbin ? menu->depbin->db_label : NULL),
            (void *) &menu->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) MENU_LABEL);
		}
	}

	if (mask & MENU_ITEMS) {

		rc = item_mask_man((unsigned long) MENU_ITEMS, menu_masks,
			(int (*) ()) eval_attr_menuitems,
			(menu->depbin ? menu->depbin->db_items : NULL),
			(void *) &menu->items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) MENU_ITEMS);
		}
	}

    if (mask & MENU_STYLE) 
    {
        unsigned long   tmp_bin_size;       /* temp storage for DEPBIN chunk size */
        unsigned char  *tmp_bin_chunk;      /* temp ptr to DEPBIN chunk */
    
        if (menu->depbin)
        {
            if (menu->depbin->db_style)
            {
                tmp_bin_chunk = menu->depbin->db_style->bin_chunk;
                tmp_bin_size =  menu->depbin->db_style->bin_size;
                DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, (DDL_UINT *)(&menu->style));
                menu_masks->attr_avail |= MENU_STYLE;
            }
        }
    }

    if (mask & MENU_STYLE_STRING) {
		rc = item_mask_man((unsigned long) MENU_STYLE_STRING, menu_masks,
			(int (*) ()) eval_string,
			(menu->depbin ? menu->depbin->db_style_string : NULL),
			(void *) &menu->style_string, env_info, &var_needed);

        if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                menu->style_string.flags = 0;
                menu->style_string.len = 0;
                menu->style_string.str = 0;
            }
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) MENU_STYLE_STRING);
        }
	}

	if (mask & MENU_VALID) {

		rc = item_mask_man((unsigned long) MENU_VALID, menu_masks,
			(int (*) ()) eval_attr_ulong,
			(menu->depbin ? menu->depbin->db_valid : NULL),
			(void *) &menu->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				menu->valid = TRUE;
				menu_masks->attr_avail |= (unsigned long) MENU_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) MENU_VALID);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) menu, MENU_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Menu total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_edit_display
 *	ShortDesc: evaluate the FLAT_EDIT_DISPLAY
 *
 *	Description:
 *		eval_item_edit_display will load the FLAT_EDIT_DISPLAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_edit_display() call
 *
 *	Outputs:
 *		edit_display:	the loaded FLAT_EDIT_DISPLAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_edit_display(FLAT_EDIT_DISPLAY *edit_display, unsigned long mask,
	ENV_INFO2 *env_info, RETURN_LIST *errors)
{

	int             rc;					/* return code */
	OP_REF          var_needed;			/* temp storage for var_needed */
	FLAT_MASKS     *edit_disp_masks;	/* temp ptr to the masks of a var */
	TEST_FAIL(I_EVAL_EDIT_DISPLAY);

#ifdef DEBUG
	dds_item_flat_check((void *) edit_display, EDIT_DISP_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	edit_disp_masks = &edit_display->masks;

	/*
	 * Sanity check
	 */

	if ((edit_display->depbin == NULL) && (edit_disp_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & EDIT_DISPLAY_DISP_ITEMS) {

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_DISP_ITEMS,
			edit_disp_masks, (int (*) ()) eval_op_ref_trail_list,
			(edit_display->depbin ? edit_display->depbin->db_disp_items : NULL),
			(void *) &edit_display->disp_items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				edit_disp_masks->attr_avail |= (unsigned long) EDIT_DISPLAY_DISP_ITEMS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_DISP_ITEMS);
			}
		}
	}

	if (mask & EDIT_DISPLAY_EDIT_ITEMS) {

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_EDIT_ITEMS,
			edit_disp_masks, (int (*) ()) eval_op_ref_trail_list,
			(edit_display->depbin ? edit_display->depbin->db_edit_items : NULL),
			(void *) &edit_display->edit_items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_EDIT_ITEMS);
		}
	}

	if (mask & EDIT_DISPLAY_LABEL) {

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_LABEL, edit_disp_masks,
			(int (*) ()) eval_string,
			(edit_display->depbin ? edit_display->depbin->db_label : NULL),
			(void *) &edit_display->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_LABEL);
		}
	}

	if (mask & EDIT_DISPLAY_PRE_EDIT_ACT) {

		rc = eval_attr_reflist(edit_disp_masks,
			(unsigned long) EDIT_DISPLAY_PRE_EDIT_ACT, env_info,
			errors, &edit_display->pre_edit_act,
			(edit_display->depbin ? edit_display->depbin->db_pre_edit_act : NULL));
	}

	if (mask & EDIT_DISPLAY_POST_EDIT_ACT) {

		rc = eval_attr_reflist(edit_disp_masks,
			(unsigned long) EDIT_DISPLAY_POST_EDIT_ACT, env_info,
			errors, &edit_display->post_edit_act,
			(edit_display->depbin ? edit_display->depbin->db_post_edit_act : NULL));

	}

#ifdef DEBUG
	dds_item_flat_check((void *) edit_display, EDIT_DISP_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_method
 *	ShortDesc: evaluate the FLAT_METHOD
 *
 *	Description:
 *		eval_item_method will load the FLAT_METHOD structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_method() call
 *
 *	Outputs:
 *		method:			the loaded FLAT_METHOD structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_string(), eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_method(FLAT_METHOD *method, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;				/* return code */
	OP_REF          var_needed;		/* temp storage for var_needed */
	FLAT_MASKS     *meth_masks;		/* temp ptr to the masks of a method */
	unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

	TEST_FAIL(I_EVAL_METHOD);

#ifdef DEBUG
	dds_item_flat_check((void *) method, METHOD_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	meth_masks = &method->masks;

	if ((method->depbin == NULL) && (meth_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & METHOD_CLASS) {

		rc = item_mask_man((unsigned long) METHOD_CLASS, meth_masks,
			(int (*) ()) eval_attr_bitstring,
			(method->depbin ? method->depbin->db_class : NULL),
			(void *) &method->class_attr, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) METHOD_CLASS);
		}
	}

	if (mask & METHOD_DEF) {

		rc = item_mask_man((unsigned long) METHOD_DEF, meth_masks,
			(int (*) ()) eval_attr_definition,
			(method->depbin ? method->depbin->db_def : NULL),
			(void *) &method->def, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) METHOD_DEF);
		}
	}

	if (mask & METHOD_HELP) {

		eval_attr_string((unsigned long) METHOD_HELP, meth_masks,
			(int (*) ()) eval_string,
			(method->depbin ? method->depbin->db_help : NULL),
			&method->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & METHOD_LABEL) {

		rc = item_mask_man((unsigned long) METHOD_LABEL, meth_masks,
			(int (*) ()) eval_string,
			(method->depbin ? method->depbin->db_label : NULL),
			(void *) &method->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) METHOD_LABEL);
		}
	}

	if (mask & METHOD_VALID) {

		rc = item_mask_man((unsigned long) METHOD_VALID, meth_masks,
			(int (*) ()) eval_attr_ulong,
			(method->depbin ? method->depbin->db_valid : NULL),
			(void *) &method->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				method->valid = TRUE;
				meth_masks->attr_avail |= (unsigned long) METHOD_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) METHOD_VALID);
			}
		}
	}

	if (mask & METHOD_SCOPE) {
        if (method->depbin->db_scope)
        {
		    tmp_bin_chunk = method->depbin->db_scope->bin_chunk;
		    tmp_bin_size = method->depbin->db_scope->bin_size;
		    DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &method->scope);
    		meth_masks->attr_avail |= METHOD_SCOPE;
		} else {
			load_errors(errors, DDL_BINARY_REQUIRED, &var_needed, (unsigned long) METHOD_SCOPE);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) method, METHOD_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Method total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_refresh
 *	ShortDesc: evaluate the FLAT_REFRESH
 *
 *	Description:
 *		eval_item_refresh will load the FLAT_REFRESH structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_refresh() call
 *
 *	Outputs:
 *		refresh:		the loaded FLAT_REFRESH structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_refresh(FLAT_REFRESH *refresh, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */

	TEST_FAIL(I_EVAL_REFRESH);

#ifdef DEBUG
	dds_item_flat_check((void *) refresh, REFRESH_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */

	if ((refresh->depbin == NULL) && (refresh->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & REFRESH_ITEMS) {

		rc = item_mask_man((unsigned long) REFRESH_ITEMS, &refresh->masks,
			(int (*) ()) eval_attr_refresh,
			(refresh->depbin ? refresh->depbin->db_items : NULL),
			(void *) &refresh->items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) REFRESH_ITEMS);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) refresh, REFRESH_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Refresh relation total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_unit
 *	ShortDesc: evaluate the FLAT_UNIT
 *
 *	Description:
 *		eval_item_unit will load the FLAT_UNIT structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_unit() call
 *
 *	Outputs:
 *		unit:			the loaded FLAT_UNIT structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_unit(FLAT_UNIT *unit, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */

	TEST_FAIL(I_EVAL_UNIT);

#ifdef DEBUG
	dds_item_flat_check((void *) unit, UNIT_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */

	if ((unit->depbin == NULL) && (unit->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & UNIT_ITEMS) {

		rc = item_mask_man((unsigned long) UNIT_ITEMS, &unit->masks,
			(int (*) ()) eval_attr_unit,
			(unit->depbin ? unit->depbin->db_items : NULL),
			(void *) &unit->items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) UNIT_ITEMS);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) unit, UNIT_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Unit total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_wao
 *	ShortDesc: evaluate the FLAT_WAO
 *
 *	Description:
 *		eval_item_wao will load the FLAT_WAO structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_wao() call
 *
 *	Outputs:
 *		wao:			the loaded FLAT_WAO structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_wao(FLAT_WAO *wao, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */

	TEST_FAIL(I_EVAL_WAO);

#ifdef DEBUG
	dds_item_flat_check((void *) wao, WAO_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */

	if ((wao->depbin == NULL) && (wao->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & WAO_ITEMS) {

		rc = item_mask_man((unsigned long) WAO_ITEMS, &wao->masks,
			(int (*) ()) eval_op_ref_trail_list,
			(wao->depbin ? wao->depbin->db_items : NULL),
			(void *) &wao->items, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) WAO_ITEMS);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) wao, WAO_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "WAO total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_itemarray
 *	ShortDesc: evaluate the FLAT_ITEM_ARRAY
 *
 *	Description:
 *		eval_item_itemarray will load the FLAT_ITEM_ARRAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_itemarray() call
 *
 *	Outputs:
 *		item_array:		the loaded FLAT_ITEM_ARRAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_itemarray(FLAT_ITEM_ARRAY *item_array, unsigned long mask,
	ENV_INFO2 *env_info, RETURN_LIST *errors)
{

	int             rc;					/* return code */
	OP_REF          var_needed;			/* temp storage for var_needed */
	FLAT_MASKS     *item_array_masks;	/* temp ptr to masks of item_array */
	TEST_FAIL(I_EVAL_ITEM_ARRAY);

#ifdef DEBUG
	dds_item_flat_check((void *) item_array, ITEM_ARRAY_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	item_array_masks = &item_array->masks;

	if ((item_array->depbin == NULL) && (item_array_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & ITEM_ARRAY_ELEMENTS) {

		rc = item_mask_man((unsigned long) ITEM_ARRAY_ELEMENTS, item_array_masks,
			(int (*) ()) eval_attr_itemarray,
			(item_array->depbin ? item_array->depbin->db_elements : NULL),
			(void *) &item_array->elements, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) ITEM_ARRAY_ELEMENTS);
		}
	}

	if (mask & ITEM_ARRAY_HELP) {

		eval_attr_string((unsigned long) ITEM_ARRAY_HELP, item_array_masks,
			(int (*) ()) eval_string,
			(item_array->depbin ? item_array->depbin->db_help : NULL),
			&item_array->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & ITEM_ARRAY_LABEL) {

		eval_attr_string((unsigned long) ITEM_ARRAY_LABEL, item_array_masks,
			(int (*) ()) eval_string,
			(item_array->depbin ? item_array->depbin->db_label : NULL),
			&item_array->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & ITEM_ARRAY_VALID) {

		rc = item_mask_man((unsigned long) ARRAY_VALID, item_array_masks,
			(int (*) ()) eval_attr_ulong,
			(item_array->depbin ? item_array->depbin->db_valid : NULL),
			(void *) &item_array->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				item_array->valid = TRUE;
				item_array_masks->attr_avail |= (unsigned long) ITEM_ARRAY_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) ITEM_ARRAY_VALID);
			}
		}
	}
#ifdef DEBUG
	dds_item_flat_check((void *) item_array, ITEM_ARRAY_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Item array total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_array
 *	ShortDesc: evaluate the FLAT_ARRAY
 *
 *	Description:
 *		eval_item_array will load the FLAT_ARRAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_array() call
 *
 *	Outputs:
 *		array:			the loaded FLAT_ARRAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_array(FLAT_ARRAY *array, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;				/* return code */
	OP_REF          var_needed;		/* temp storage for var_needed */
	FLAT_MASKS     *array_masks;	/* temp ptr to the masks of a array */
	unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	unsigned char  *tmp_bin_chunk;	/* temp ptr to DEPBIN chunk */

	TEST_FAIL(I_EVAL_ARRAY);

#ifdef DEBUG
	dds_item_flat_check((void *) array, ARRAY_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	array_masks = &array->masks;

	if ((array->depbin == NULL) && (array_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & ARRAY_NUM_OF_ELEMENTS) {
        if (array->depbin->db_num_of_elements)
        {
		    tmp_bin_chunk = array->depbin->db_num_of_elements->bin_chunk;
		    tmp_bin_size = array->depbin->db_num_of_elements->bin_size;
		    DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size,
			    &array->num_of_elements);
    		array_masks->attr_avail |= ARRAY_NUM_OF_ELEMENTS;
        }
        else
			load_errors(errors, DDL_BINARY_REQUIRED, &var_needed, (unsigned long) ARRAY_NUM_OF_ELEMENTS);
	}

	if (mask & ARRAY_HELP) {

		eval_attr_string((unsigned long) ARRAY_HELP, array_masks,
			(int (*) ()) eval_string,
			(array->depbin ? array->depbin->db_help : NULL),
			&array->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & ARRAY_LABEL) {

		eval_attr_string((unsigned long) ARRAY_LABEL, array_masks,
			(int (*) ()) eval_string,
			(array->depbin ? array->depbin->db_label : NULL),
			&array->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & ARRAY_TYPE) {

		rc = item_mask_man((unsigned long) ARRAY_TYPE, array_masks,
			(int (*) ()) eval_attr_ref,
			(array->depbin ? array->depbin->db_type : NULL),
			(void *) &array->type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_TYPE);
		}
	}

	if (mask & ARRAY_RESP_CODES) {

		rc = item_mask_man((unsigned long) ARRAY_RESP_CODES, array_masks,
			(int (*) ()) eval_attr_ref,
			(array->depbin ? array->depbin->db_resp_codes : NULL),
			(void *) &array->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				array->resp_codes = 0;
				array_masks->attr_avail |= (unsigned long) ARRAY_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_RESP_CODES);
			}
		}
	}

	if (mask & ARRAY_LABEL) {

		rc = item_mask_man((unsigned long) ARRAY_LABEL, array_masks,
			(int (*) ()) eval_string,
			(array->depbin ? array->depbin->db_label : NULL),
			(void *) &array->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_LABEL);
		}
	}

	if (mask & ARRAY_VALID) {

		rc = item_mask_man((unsigned long) ARRAY_VALID, array_masks,
			(int (*) ()) eval_attr_ulong,
			(array->depbin ? array->depbin->db_valid : NULL),
			(void *) &array->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				array->valid = TRUE;
				array_masks->attr_avail |= (unsigned long) ARRAY_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_VALID);
			}
		}
	}

	if (mask & ARRAY_WRITE_MODE) {

		rc = item_mask_man((unsigned long) ARRAY_WRITE_MODE, array_masks,
			(int (*) ()) eval_attr_int_expr,
			(array->depbin ? array->depbin->db_write_mode : NULL),
			(void *) &array->write_mode, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				array->write_mode = 0;
				array_masks->attr_avail |= (unsigned long) ARRAY_WRITE_MODE;
			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_WRITE_MODE);
			}
		}
	}


#ifdef DEBUG
	dds_item_flat_check((void *) array, ARRAY_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Array total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_collection
 *	ShortDesc: evaluate the FLAT_COLLECTION
 *
 *	Description:
 *		eval_item_collection will load the FLAT_COLLECTION structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_collection() call
 *
 *	Outputs:
 *		collection:		the loaded FLAT_COLLECTION structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_collection(FLAT_COLLECTION *collection, unsigned long mask,
	ENV_INFO2 *env_info, RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *coll_masks;	/* temp ptr to the masks of a collection */

	TEST_FAIL(I_EVAL_COLLECTION);

#ifdef DEBUG
	dds_item_flat_check((void *) collection, COLLECTION_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	coll_masks = &collection->masks;

	if ((collection->depbin == NULL) && (coll_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & COLLECTION_MEMBERS) {

		rc = item_mask_man((unsigned long) COLLECTION_MEMBERS, coll_masks,
			(int (*) ()) eval_attr_op_members,
			(collection->depbin ? collection->depbin->db_members : NULL),
			(void *) &collection->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
            if (rc == DDL_DEFAULT_ATTR)
            {
                /* AR 2655 - empty collections are now allowed */
                collection->members.count = 0;
                collection->members.list = 0;
                collection->members.limit = 0;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) COLLECTION_MEMBERS);
		}
	}

	if (mask & COLLECTION_HELP) {

		eval_attr_string((unsigned long) COLLECTION_HELP, coll_masks,
			(int (*) ()) eval_string,
			(collection->depbin ? collection->depbin->db_help : NULL),
			&collection->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & COLLECTION_LABEL) {

		eval_attr_string((unsigned long) COLLECTION_LABEL, coll_masks,
			(int (*) ()) eval_string,
			(collection->depbin ? collection->depbin->db_label : NULL),
			&collection->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

#ifdef DEBUG
	dds_item_flat_check((void *) collection, COLLECTION_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_record
 *	ShortDesc: evaluate the FLAT_RECORD
 *
 *	Description:
 *		eval_item_record will load the FLAT_RECORD structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_record() call
 *
 *	Outputs:
 *		record:			the loaded FLAT_RECORD structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_record(FLAT_RECORD *record, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *rec_masks;	/* temp ptr to the masks of a record */

	TEST_FAIL(I_EVAL_RECORD);

#ifdef DEBUG
	dds_item_flat_check((void *) record, RECORD_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	rec_masks = &record->masks;

	if ((record->depbin == NULL) && (rec_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & RECORD_MEMBERS) {

		rc = item_mask_man((unsigned long) RECORD_MEMBERS, rec_masks,
			(int (*) ()) eval_attr_members,
			(record->depbin ? record->depbin->db_members : NULL),
			(void *) &record->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) RECORD_MEMBERS);
		}
	}

	if (mask & RECORD_HELP) {

		eval_attr_string((unsigned long) RECORD_HELP, rec_masks,
			(int (*) ()) eval_string,
			(record->depbin ? record->depbin->db_help : NULL),
			&record->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & RECORD_LABEL) {

		eval_attr_string((unsigned long) RECORD_LABEL, rec_masks,
			(int (*) ()) eval_string,
			(record->depbin ? record->depbin->db_label : NULL),
			&record->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & RECORD_RESP_CODES) {


		rc = item_mask_man((unsigned long) RECORD_RESP_CODES, rec_masks,
			(int (*) ()) eval_attr_ref,
			(record->depbin ? record->depbin->db_resp_codes : NULL),
			(void *) &record->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				record->resp_codes = 0;
				rec_masks->attr_avail |= (unsigned long) RECORD_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) RECORD_RESP_CODES);
			}
		}
	}

	if (mask & RECORD_LABEL) {

		rc = item_mask_man((unsigned long) RECORD_LABEL, rec_masks,
			(int (*) ()) eval_string,
			(record->depbin ? record->depbin->db_label : NULL),
			(void *) &record->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) RECORD_LABEL);
		}
	}

	if (mask & RECORD_VALID) {

		rc = item_mask_man((unsigned long) RECORD_VALID, rec_masks,
			(int (*) ()) eval_attr_ulong,
			(record->depbin ? record->depbin->db_valid : NULL),
			(void *) &record->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				record->valid = TRUE;
				rec_masks->attr_avail |= (unsigned long) RECORD_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) RECORD_VALID);
			}
		}
	}

    if (mask & RECORD_WRITE_MODE) {

		rc = item_mask_man((unsigned long) RECORD_WRITE_MODE, rec_masks,
			(int (*) ()) eval_attr_int_expr,
			(record->depbin ? record->depbin->db_write_mode : NULL),
			(void *) &record->write_mode, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				record->write_mode = 0;
				rec_masks->attr_avail |= (unsigned long) RECORD_WRITE_MODE;
			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) RECORD_WRITE_MODE);
			}
		}
	}


#ifdef DEBUG
	dds_item_flat_check((void *) record, RECORD_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Record total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", nIndex, errors->list[nIndex].bad_attr, 
                        errors->list[nIndex].rc);
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_var_list
 *	ShortDesc: evaluate the FLAT_VAR_LIST
 *
 *	Description:
 *		eval_item_var_list will load the FLAT_VAR_LIST structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var_list() call
 *
 *	Outputs:
 *		var_list:		the loaded FLAT_VAR_LIST structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_var_list(FLAT_VAR_LIST *var_list, unsigned long mask,
	ENV_INFO2 *env_info, RETURN_LIST *errors)
{

	int             rc;				/* return code */
	OP_REF          var_needed;		/* temp storage for var_needed */
	FLAT_MASKS     *var_list_masks;	/* temp ptr to the masks of a var */

	TEST_FAIL(I_EVAL_VAR_LIST);

#ifdef DEBUG
	dds_item_flat_check((void *) var_list, VAR_LIST_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	var_list_masks = &var_list->masks;

	if ((var_list->depbin == NULL) && (var_list_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & VAR_LIST_MEMBERS) {

		rc = item_mask_man((unsigned long) VAR_LIST_MEMBERS, var_list_masks,
			(int (*) ()) eval_attr_members,
			(var_list->depbin ? var_list->depbin->db_members : NULL),
			(void *) &var_list->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) VAR_LIST_MEMBERS);
		}
	}

	if (mask & VAR_LIST_HELP) {

		eval_attr_string((unsigned long) VAR_LIST_HELP, var_list_masks,
			(int (*) ()) eval_string,
			(var_list->depbin ? var_list->depbin->db_help : NULL),
			&var_list->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & VAR_LIST_LABEL) {

		eval_attr_string((unsigned long) VAR_LIST_LABEL, var_list_masks,
			(int (*) ()) eval_string,
			(var_list->depbin ? var_list->depbin->db_label : NULL),
			&var_list->label, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & VAR_LIST_RESP_CODES) {

		rc = item_mask_man((unsigned long) VAR_LIST_RESP_CODES, var_list_masks,
			(int (*) ()) eval_attr_ref,
			(var_list->depbin ? var_list->depbin->db_resp_codes : NULL),
			(void *) &var_list->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var_list->resp_codes = 0;
				var_list_masks->attr_avail |= (unsigned long) VAR_LIST_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_LIST_RESP_CODES);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) var_list, VAR_LIST_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Variable list total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_block
 *	ShortDesc: evaluate the FLAT_BLOCK
 *
 *	Description:
 *		eval_item_block will load the FLAT_BLOCK structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_block() call
 *
 *	Outputs:
 *		block:			the loaded FLAT_BLOCK structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_block(FLAT_BLOCK *block, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;				/* return code */
	OP_REF          var_needed;		/* temp storage for var_needed */
	FLAT_MASKS     *block_masks;	/* temp ptr to the masks of a block */

	TEST_FAIL(I_EVAL_BLOCK);

#ifdef DEBUG
	dds_item_flat_check((void *) block, BLOCK_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	block_masks = &block->masks;

	if ((block->depbin == NULL) && (block_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & BLOCK_CHARACTERISTIC) {

		rc = item_mask_man((unsigned long) BLOCK_CHARACTERISTIC, block_masks,
			(int (*) ()) eval_attr_ref,
			(block->depbin ? block->depbin->db_characteristic : NULL),
			(void *) &block->characteristic, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_CHARACTERISTIC);
		}
	}

	if (mask & BLOCK_HELP) {

		eval_attr_string((unsigned long) BLOCK_HELP, block_masks,
			(int (*) ()) eval_string,
			(block->depbin ? block->depbin->db_help : NULL),
			&block->help, env_info, errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & BLOCK_LABEL) {

		rc = item_mask_man((unsigned long) BLOCK_LABEL, block_masks,
			(int (*) ()) eval_string,
			(block->depbin ? block->depbin->db_label : NULL),
			(void *) &block->label, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_LABEL);
		}
	}

	if (mask & BLOCK_PARAM) {

		rc = item_mask_man((unsigned long) BLOCK_PARAM, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_param : NULL),
			(void *) &block->param, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_PARAM);
		}
	}

    if (mask & BLOCK_LOCAL_PARAM) {

		rc = item_mask_man((unsigned long) BLOCK_LOCAL_PARAM, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_local_param : NULL),
			(void *) &block->local_param, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
            if (rc == DDL_DEFAULT_ATTR)
                block->local_param.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_LOCAL_PARAM);
		}
	}

	if (mask & BLOCK_PARAM_LIST) {

		rc = item_mask_man((unsigned long) BLOCK_PARAM_LIST, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_param_list : NULL),
			(void *) &block->param_list, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->param_list.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_PARAM_LIST;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_PARAM_LIST);
			}
		}
	}

	if (mask & BLOCK_ITEM_ARRAY) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_ITEM_ARRAY,
			env_info, errors, &block->item_array,
			(block->depbin ? block->depbin->db_item_array : NULL));
	}

	if (mask & BLOCK_COLLECT) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_COLLECT,
			env_info, errors, &block->collect,
			(block->depbin ? block->depbin->db_collect : NULL));
	}

	if (mask & BLOCK_MENU) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_MENU, env_info,
			errors, &block->menu, (block->depbin ? block->depbin->db_menu : NULL));
	}

	if (mask & BLOCK_EDIT_DISP) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_EDIT_DISP,
			env_info, errors, &block->edit_disp,
			(block->depbin ? block->depbin->db_edit_disp : NULL));
	}

	if (mask & BLOCK_METHOD) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_METHOD,
			env_info, errors, &block->method,
			(block->depbin ? block->depbin->db_method : NULL));
	}

	if (mask & BLOCK_UNIT) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_UNIT,
			env_info, errors, &block->unit,
			(block->depbin ? block->depbin->db_unit : NULL));
	}

	if (mask & BLOCK_REFRESH) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_REFRESH,
			env_info, errors, &block->refresh,
			(block->depbin ? block->depbin->db_refresh : NULL));
	}

	if (mask & BLOCK_WAO) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_WAO, env_info,
			errors, &block->wao, (block->depbin ? block->depbin->db_wao : NULL));
	}

	if (mask & BLOCK_AXIS) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_AXIS, env_info,
			errors, &block->axis, (block->depbin ? block->depbin->db_axis : NULL));
	}

	if (mask & BLOCK_CHART) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_CHART, env_info,
			errors, &block->chart, (block->depbin ? block->depbin->db_chart : NULL));
	}

	if (mask & BLOCK_FILE) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_FILE, env_info,
			errors, &block->file, (block->depbin ? block->depbin->db_file : NULL));
	}

	if (mask & BLOCK_GRAPH) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_GRAPH, env_info,
			errors, &block->graph, (block->depbin ? block->depbin->db_graph : NULL));
	}

	if (mask & BLOCK_LIST) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_LIST, env_info,
			errors, &block->list, (block->depbin ? block->depbin->db_list : NULL));
	}

	if (mask & BLOCK_SOURCE) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_SOURCE, env_info,
			errors, &block->source, (block->depbin ? block->depbin->db_source : NULL));
	}

	if (mask & BLOCK_WAVEFORM) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_WAVEFORM, env_info,
			errors, &block->waveform, (block->depbin ? block->depbin->db_waveform : NULL));
	}

	if (mask & BLOCK_GRID) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_GRID, env_info,
			errors, &block->grid, (block->depbin ? block->depbin->db_grid : NULL));
	}

	if (mask & BLOCK_IMAGE) {

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_IMAGE, env_info,
			errors, &block->image, (block->depbin ? block->depbin->db_image : NULL));
	}

    if (mask & BLOCK_CHART_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_CHART_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_chart_members : NULL),
			(void *) &block->chart_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->chart_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_CHART_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_CHART_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_FILE_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_FILE_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_file_members : NULL),
			(void *) &block->file_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->file_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_FILE_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_FILE_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_GRAPH_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_GRAPH_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_graph_members : NULL),
			(void *) &block->graph_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->graph_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_GRAPH_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_GRAPH_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_GRID_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_GRID_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_grid_members : NULL),
			(void *) &block->grid_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->grid_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_GRID_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_GRID_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_MENU_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_MENU_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_menu_members : NULL),
			(void *) &block->menu_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->menu_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_MENU_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_MENU_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_METHOD_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_METHOD_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_method_members : NULL),
			(void *) &block->method_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->method_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_METHOD_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_METHOD_MEMBERS);
			}
		}
	}

	if (mask & BLOCK_LIST_MEMBERS) {

		rc = item_mask_man((unsigned long) BLOCK_LIST_MEMBERS, block_masks,
			(int (*) ()) eval_attr_members,
			(block->depbin ? block->depbin->db_list_members : NULL),
			(void *) &block->list_members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->list_members.count = 0;
				block_masks->attr_avail |= (unsigned long) BLOCK_LIST_MEMBERS;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_LIST_MEMBERS);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) block, BLOCK_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
        LOGC_TRACE (CPM_FF_DDSERVICE, "Block total error count: %d", errors->count);
        for (int nIndex = 0; nIndex < errors->count; nIndex++)
            LOGC_TRACE (CPM_FF_DDSERVICE, "Error info: index: %d, value: %d, rc: %d", index, errors->list[nIndex].bad_attr,
                        errors->list[nIndex].rc);

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_response_code
 *	ShortDesc: evaluate the FLAT_RESPONSE_CODE
 *
 *	Description:
 *		eval_item_response_code will load the FLAT_RESPONSE_CODE structure
 *		with the evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_response_code() call
 *
 *	Outputs:
 *		response_code:	the loaded FLAT_RESPONSE_CODE structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_response_code(FLAT_RESP_CODE *resp_code, unsigned long mask,
	ENV_INFO2 *env_info, RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */

	TEST_FAIL(I_EVAL_RESPONSE_CODE);

#ifdef DEBUG
	dds_item_flat_check((void *) resp_code, RESP_CODES_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */

	if ((resp_code->depbin == NULL) && (resp_code->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & RESP_CODE_MEMBER) {

		rc = item_mask_man((unsigned long) RESP_CODE_MEMBER, &resp_code->masks,
			(int (*) ()) eval_attr_resp_codes,
			(resp_code->depbin ? resp_code->depbin->db_member : NULL),
			(void *) &resp_code->member, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) RESP_CODE_MEMBER);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) resp_code, RESP_CODES_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_domain
 *	ShortDesc: evaluate the FLAT_DOMAIN
 *
 *	Description:
 *		eval_item_domain will load the FLAT_DOMAIN structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_domain() call
 *
 *	Outputs:
 *		domain:			the loaded FLAT_DOMAIN structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		FAILURE, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_domain(FLAT_DOMAIN *domain, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *dom_masks;	/* temp ptr to the masks of a domain */

	TEST_FAIL(I_EVAL_DOMAIN);

#ifdef DEBUG
	dds_item_flat_check((void *) domain, DOMAIN_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	dom_masks = &domain->masks;

	if ((domain->depbin == NULL) && (dom_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & DOMAIN_HANDLING) {

		rc = item_mask_man((unsigned long) DOMAIN_HANDLING, dom_masks,
			(int (*) ()) eval_attr_bitstring,
			(domain->depbin ? domain->depbin->db_handling : NULL),
			(void *) &domain->handling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				domain->handling = READ_HANDLING | WRITE_HANDLING;
				dom_masks->attr_avail |= (unsigned long) DOMAIN_HANDLING;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) DOMAIN_HANDLING);
			}
		}
	}

	if (mask & DOMAIN_RESP_CODES) {

		rc = item_mask_man((unsigned long) DOMAIN_RESP_CODES, dom_masks,
			(int (*) ()) eval_attr_ref,
			(domain->depbin ? domain->depbin->db_resp_codes : NULL),
			(void *) &domain->resp_codes, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				domain->resp_codes = 0;
				dom_masks->attr_avail |= (unsigned long) DOMAIN_RESP_CODES;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) DOMAIN_RESP_CODES);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) domain, DOMAIN_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

int
eval_item_axis(FLAT_AXIS *axis, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *axis_masks;	/* temp ptr to the masks of a axis */

	TEST_FAIL(I_EVAL_AXIS);

#ifdef DEBUG
	dds_item_flat_check((void *) axis, AXIS_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	axis_masks = &axis->masks;

	if ((axis->depbin == NULL) && (axis_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & AXIS_HELP) {
		eval_attr_string((unsigned long) AXIS_HELP, axis_masks,
            (int (*) ()) eval_string,
			(axis->depbin ? axis->depbin->db_help : NULL),
			&axis->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & AXIS_LABEL) {
		eval_attr_string((unsigned long) AXIS_LABEL, axis_masks,
			(int (*) ()) eval_string,
			(axis->depbin ? axis->depbin->db_label : NULL),
			&axis->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & AXIS_MIN_VALUE) {
        rc = item_mask_man((unsigned long) AXIS_MIN_VALUE, axis_masks,
			(int (*) ()) eval_attr_expr,
			(axis->depbin ? axis->depbin->db_min_value : NULL),
			(void *) &axis->min_value, env_info, &var_needed);

        if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
			load_errors(errors, rc, &var_needed, (unsigned long) AXIS_MIN_VALUE);
	}

	if (mask & AXIS_MAX_VALUE) {
        rc = item_mask_man((unsigned long) AXIS_MAX_VALUE, axis_masks,
			(int (*) ()) eval_attr_expr,
			(axis->depbin ? axis->depbin->db_max_value : NULL),
			(void *) &axis->max_value, env_info, &var_needed);
        if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
			load_errors(errors, rc, &var_needed, (unsigned long) AXIS_MAX_VALUE);
	}
	if (mask & AXIS_SCALING) {
		rc = item_mask_man((unsigned long) AXIS_SCALING, axis_masks,
			(int (*) ()) eval_attr_ulong,
			(axis->depbin ? axis->depbin->db_scaling : NULL),
			(void *) &axis->scaling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                axis->scaling = AXIS_SCALING_LINEAR;
                rc = DDL_SUCCESS;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) AXIS_SCALING);
        }
	}
	if (mask & AXIS_CONSTANT_UNIT) {
		eval_attr_string((unsigned long) AXIS_CONSTANT_UNIT, axis_masks,
            (int (*) ()) eval_string,
			(axis->depbin ? axis->depbin->db_constant_unit : NULL),
			&axis->constant_unit, env_info, errors,	DEFAULT_STD_DICT_STRING);
	}

#ifdef DEBUG
	dds_item_flat_check((void *) axis, AXIS_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

int
eval_item_chart(FLAT_CHART *chart, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *chart_masks;	/* temp ptr to the masks of a chart */

	TEST_FAIL(I_EVAL_CHART);

#ifdef DEBUG
	dds_item_flat_check((void *) chart, CHART_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	chart_masks = &chart->masks;

	if ((chart->depbin == NULL) && (chart_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & CHART_HELP) {
		eval_attr_string((unsigned long) CHART_HELP, chart_masks,
            (int (*) ()) eval_string,
			(chart->depbin ? chart->depbin->db_help : NULL),
			&chart->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & CHART_LABEL) {
		eval_attr_string((unsigned long) CHART_LABEL, chart_masks,
			(int (*) ()) eval_string,
			(chart->depbin ? chart->depbin->db_label : NULL),
			&chart->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & CHART_CYCLE_TIME) {

		rc = item_mask_man((unsigned long) CHART_CYCLE_TIME, chart_masks,
			(int (*) ()) eval_attr_int_expr,
			(chart->depbin ? chart->depbin->db_cycle_time : NULL),
			(void *) &chart->cycle_time, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->cycle_time = 1000;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) CHART_CYCLE_TIME);
        }

        /*
        rc = eval_attr_int_expr(chart->depbin->db_cycle_time->bin_chunk,
            chart->depbin->db_cycle_time->bin_size,
            &chart->cycle_time, 
            &chart->depbin->db_cycle_time->dep, 
            env_info, &var_needed);
        if (rc != DDL_SUCCESS)
			load_errors(errors, rc, &var_needed, (unsigned long) CHART_CYCLE_TIME);
        */
	}
	if (mask & CHART_HEIGHT) {
		rc = item_mask_man((unsigned long) CHART_HEIGHT, chart_masks,
			(int (*) ()) eval_attr_ulong,
			(chart->depbin ? chart->depbin->db_height : NULL),
			(void *) &chart->height, env_info, &var_needed);
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->height = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) CHART_HEIGHT);
        }
	}
	if (mask & CHART_WIDTH) {
		rc = item_mask_man((unsigned long) CHART_WIDTH, chart_masks,
			(int (*) ()) eval_attr_ulong,
			(chart->depbin ? chart->depbin->db_width : NULL),
			(void *) &chart->width, env_info, &var_needed);
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->width = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) CHART_WIDTH);
        }
	}
	if (mask & CHART_LENGTH) {
        rc = item_mask_man((unsigned long) CHART_LENGTH, chart_masks,
			(int (*) ()) eval_attr_int_expr,
			(chart->depbin ? chart->depbin->db_length : NULL),
			(void *) &chart->length, env_info, &var_needed);
        /*
        rc = eval_attr_int_expr(chart->depbin->db_length->bin_chunk,
            chart->depbin->db_length->bin_size,
            &chart->length, 
            &chart->depbin->db_length->dep, 
            env_info, &var_needed);
            */
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->length = 600000;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) CHART_LENGTH);
        }
	}
	if (mask & CHART_TYPE) {

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (chart->depbin && chart->depbin->db_type)
        {
		    tmp_bin_chunk = chart->depbin->db_type->bin_chunk;
		    tmp_bin_size = chart->depbin->db_type->bin_size;
		    DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, (DDL_UINT *)(&chart->type));
        }
        else
            chart->type = CHART_TYPE_STRIP;
		chart_masks->attr_avail |= CHART_TYPE;

/*
		rc = item_mask_man((unsigned long) CHART_TYPE, chart_masks,
			(int (*) ()) eval_attr_ulong,
			(chart->depbin ? chart->depbin->db_type : NULL),
			(void *) &chart->type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->type = CHART_TYPE_STRIP;
            else
		    	load_errors(errors, rc, &var_needed, (unsigned long) CHART_TYPE);
        }
        */
	}
	if (mask & CHART_MEMBER) {

		rc = item_mask_man((unsigned long) CHART_MEMBER, chart_masks,
			(int (*) ()) eval_attr_members,
			(chart->depbin ? chart->depbin->db_members : NULL),
			(void *) &chart->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
			load_errors(errors, rc, &var_needed, (unsigned long) CHART_MEMBER);
		}
	}

	if (mask & CHART_VALID) {

		rc = item_mask_man((unsigned long) CHART_VALID, chart_masks,
			(int (*) ()) eval_attr_ulong,
			(chart->depbin ? chart->depbin->db_valid : NULL),
			(void *) &chart->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				chart->valid = TRUE;
				chart_masks->attr_avail |= (unsigned long) CHART_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) CHART_VALID);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) chart, CHART_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

int
eval_item_file(FLAT_FILE *file, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *file_masks;	/* temp ptr to the masks of a file */

	TEST_FAIL(I_EVAL_FILE);

#ifdef DEBUG
	dds_item_flat_check((void *) file, FILE_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	file_masks = &file->masks;

	if ((file->depbin == NULL) && (file_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & FILE_HELP) {
		eval_attr_string((unsigned long) FILE_HELP, file_masks,
            (int (*) ()) eval_string,
			(file->depbin ? file->depbin->db_help : NULL),
			&file->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & FILE_LABEL) {
		eval_attr_string((unsigned long) FILE_LABEL, file_masks,
			(int (*) ()) eval_string,
			(file->depbin ? file->depbin->db_label : NULL),
			&file->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & FILE_MEMBER) {

		rc = item_mask_man((unsigned long) FILE_MEMBER, file_masks,
			(int (*) ()) eval_attr_members,
			(file->depbin ? file->depbin->db_members : NULL),
			(void *) &file->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) FILE_MEMBER);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) file, FILE_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

int
eval_item_graph(FLAT_GRAPH *graph, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *graph_masks;	/* temp ptr to the masks of a graph */

	TEST_FAIL(I_EVAL_GRAPH);

#ifdef DEBUG
	dds_item_flat_check((void *) graph, GRAPH_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	graph_masks = &graph->masks;

	if ((graph->depbin == NULL) && (graph_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & GRAPH_HELP) {
		eval_attr_string((unsigned long) GRAPH_HELP, graph_masks,
            (int (*) ()) eval_string,
			(graph->depbin ? graph->depbin->db_help : NULL),
			&graph->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & GRAPH_LABEL) {
		eval_attr_string((unsigned long) GRAPH_LABEL, graph_masks,
			(int (*) ()) eval_string,
			(graph->depbin ? graph->depbin->db_label : NULL),
			&graph->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & GRAPH_HEIGHT) {
		rc = item_mask_man((unsigned long) GRAPH_HEIGHT, graph_masks,
			(int (*) ()) eval_attr_ulong,
			(graph->depbin ? graph->depbin->db_height : NULL),
			(void *) &graph->height, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                graph->height = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_HEIGHT);
        }
	}
	if (mask & GRAPH_WIDTH) {
		rc = item_mask_man((unsigned long) GRAPH_WIDTH, graph_masks,
			(int (*) ()) eval_attr_ulong,
			(graph->depbin ? graph->depbin->db_width : NULL),
			(void *) &graph->width, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                graph->width = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_WIDTH);
        }
	}
	if (mask & GRAPH_MEMBER) {

		rc = item_mask_man((unsigned long) GRAPH_MEMBER, graph_masks,
			(int (*) ()) eval_attr_members,
			(graph->depbin ? graph->depbin->db_members : NULL),
			(void *) &graph->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_MEMBER);
		}
	}
    
	if (mask & GRAPH_X_AXIS) {

		rc = item_mask_man((unsigned long) GRAPH_X_AXIS, graph_masks,
			(int (*) ()) eval_attr_ref,
			(graph->depbin ? graph->depbin->db_x_axis : NULL),
			(void *) &graph->x_axis, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR)) {
			load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_X_AXIS);
		}
        else if (rc == DDL_DEFAULT_ATTR)
        {
            /* AR 2713: Default to item id of 0 for missing axis */
            graph->x_axis = 0;
        }
	}

	if (mask & GRAPH_VALID) {

		rc = item_mask_man((unsigned long) GRAPH_VALID, graph_masks,
			(int (*) ()) eval_attr_ulong,
			(graph->depbin ? graph->depbin->db_valid : NULL),
			(void *) &graph->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				graph->valid = TRUE;
				graph_masks->attr_avail |= (unsigned long) GRAPH_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_VALID);
			}
		}
	}
	if (mask & GRAPH_CYCLE_TIME) {

		rc = item_mask_man((unsigned long) GRAPH_CYCLE_TIME, graph_masks,
			(int (*) ()) eval_attr_int_expr,
			(graph->depbin ? graph->depbin->db_cycle_time : NULL),
			(void *) &graph->cycle_time, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                graph->cycle_time = 1000;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_CYCLE_TIME);
        }
	}


#ifdef DEBUG
	dds_item_flat_check((void *) graph, GRAPH_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}
int
eval_item_list(FLAT_LIST *list, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *list_masks;	/* temp ptr to the masks of a list */

	TEST_FAIL(I_EVAL_LIST);

#ifdef DEBUG
	dds_item_flat_check((void *) list, LIST_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	list_masks = &list->masks;

	if ((list->depbin == NULL) && (list_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & LIST_HELP) {
		eval_attr_string((unsigned long) LIST_HELP, list_masks,
            (int (*) ()) eval_string,
			(list->depbin ? list->depbin->db_help : NULL),
			&list->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & LIST_LABEL) {
		eval_attr_string((unsigned long) LIST_LABEL, list_masks,
			(int (*) ()) eval_string,
			(list->depbin ? list->depbin->db_label : NULL),
			&list->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & LIST_CAPACITY) {
		rc = item_mask_man((unsigned long) LIST_CAPACITY, list_masks,
			(int (*) ()) eval_attr_int_expr,
			(list->depbin ? list->depbin->db_capacity : NULL),
			(void *) &list->capacity, env_info, &var_needed);
        /*
        rc = eval_attr_int_expr(list->depbin->db_capacity->bin_chunk,
            list->depbin->db_capacity->bin_size,
            &list->capacity, 
            &list->depbin->db_capacity->dep, 
            env_info, &var_needed);
            */

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                list->capacity = 0;
            else
    		    load_errors(errors, rc, &var_needed, (unsigned long) LIST_CAPACITY);
        }
    }
	if (mask & LIST_COUNT) {
        
		rc = item_mask_man((unsigned long) LIST_COUNT, list_masks,
			(int (*) ()) eval_attr_int_expr,
			(list->depbin ? list->depbin->db_count : NULL),
			(void *) &list->count, env_info, &var_needed);
            
        /* rc = eval_attr_int_expr(list->depbin->db_count->bin_chunk,
            list->depbin->db_count->bin_size,
            &list->count, 
            &list->depbin->db_count->dep, 
            env_info, &var_needed);
        */

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                list->count = 0;
            else
    		    load_errors(errors, rc, &var_needed, (unsigned long) LIST_COUNT);
        }
	}
	if (mask & LIST_TYPE) {

		rc = item_mask_man((unsigned long) LIST_TYPE, list_masks,
			(int (*) ()) eval_attr_ref,
			(list->depbin ? list->depbin->db_type : NULL),
			(void *) &list->type, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
			load_errors(errors, rc, &var_needed, (unsigned long) LIST_TYPE);
	}
	if (mask & LIST_VALID) {

		rc = item_mask_man((unsigned long) LIST_VALID, list_masks,
			(int (*) ()) eval_attr_ulong,
			(list->depbin ? list->depbin->db_valid : NULL),
			(void *) &list->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				list->valid = TRUE;
				list_masks->attr_avail |= (unsigned long) LIST_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) LIST_VALID);
			}
		}
	}


#ifdef DEBUG
	dds_item_flat_check((void *) list, LIST_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}
int
eval_item_source(FLAT_SOURCE *source, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *source_masks;	/* temp ptr to the masks of a source */

	TEST_FAIL(I_EVAL_SOURCE);

#ifdef DEBUG
	dds_item_flat_check((void *) source, SOURCE_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	source_masks = &source->masks;

	if ((source->depbin == NULL) && (source_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & SOURCE_HELP) {
		eval_attr_string((unsigned long) SOURCE_HELP, source_masks,
            (int (*) ()) eval_string,
			(source->depbin ? source->depbin->db_help : NULL),
			&source->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & SOURCE_LABEL) {
		eval_attr_string((unsigned long) SOURCE_LABEL, source_masks,
			(int (*) ()) eval_string,
			(source->depbin ? source->depbin->db_label : NULL),
			&source->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & SOURCE_MEMBER) {

		rc = item_mask_man((unsigned long) SOURCE_MEMBER, source_masks,
			(int (*) ()) eval_attr_op_members,
			(source->depbin ? source->depbin->db_members : NULL),
			(void *) &source->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_MEMBER);
	}
	if (mask & SOURCE_EMPHASIS) {
		rc = item_mask_man((unsigned long) SOURCE_EMPHASIS, source_masks,
			(int (*) ()) eval_attr_ulong,
			(source->depbin ? source->depbin->db_emphasis : NULL),
			(void *) &source->emphasis, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->emphasis = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_EMPHASIS);
        }
	}
	if (mask & SOURCE_LINE_TYPE) {
		rc = item_mask_man((unsigned long) SOURCE_LINE_TYPE, source_masks,
			(int (*) ()) eval_attr_ulong,
			(source->depbin ? source->depbin->db_line_type : NULL),
			(void *) &source->line_type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->line_type = LINE_TYPE_DATA_N;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_LINE_TYPE);
        }
	}
	if (mask & SOURCE_INIT_ACTIONS) {
		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_INIT_ACTIONS,
			env_info, errors, &source->init_actions,
			(source->depbin ? source->depbin->db_init_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->init_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_INIT_ACTIONS);
        }
	}
	if (mask & SOURCE_REFRESH_ACTIONS) {
		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_REFRESH_ACTIONS,
			env_info, errors, &source->refresh_actions,
			(source->depbin ? source->depbin->db_refresh_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->refresh_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_REFRESH_ACTIONS);
        }
	}
	if (mask & SOURCE_EXIT_ACTIONS) {
		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_EXIT_ACTIONS,
			env_info, errors, &source->exit_actions,
			(source->depbin ? source->depbin->db_exit_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->exit_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_EXIT_ACTIONS);
        }
	}
	if (mask & SOURCE_LINE_COLOR) {
        rc = item_mask_man((unsigned long) SOURCE_LINE_COLOR, source_masks,
			(int (*) ()) eval_attr_int_expr,
			(source->depbin ? source->depbin->db_line_color : NULL),
			(void *) &source->line_color, env_info, &var_needed);
		/*
        rc = eval_attr_int_expr(source->depbin->db_line_color->bin_chunk,
            source->depbin->db_line_color->bin_size,
            &source->line_color, 
            &source->depbin->db_line_color->dep, 
            env_info, &var_needed);
            */

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->line_color = LINE_COLOR_DEFAULT;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_LINE_COLOR);
        }
	}
	if (mask & SOURCE_Y_AXIS) {

		rc = item_mask_man((unsigned long) SOURCE_Y_AXIS, source_masks,
			(int (*) ()) eval_attr_ref,
			(source->depbin ? source->depbin->db_y_axis : NULL),
			(void *) &source->y_axis, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            /* AR 2713: Default to item id of 0 for missing axis */
            if (rc == DDL_DEFAULT_ATTR) 
                source->y_axis = 0;
			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_Y_AXIS);
        }
	}

	if (mask & SOURCE_VALID) {

		rc = item_mask_man((unsigned long) SOURCE_VALID, source_masks,
			(int (*) ()) eval_attr_ulong,
			(source->depbin ? source->depbin->db_valid : NULL),
			(void *) &source->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				source->valid = TRUE;
				source_masks->attr_avail |= (unsigned long) SOURCE_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_VALID);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) source, SOURCE_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

/* Count the number of data points specified in the DATA_ITEM_LIST,
 * which can contain arrays or individual points
 */
static int data_items_count(ENV_INFO2 *env_info, DATA_ITEM_LIST *items)
{
    int i;
    int count = 0;
    for (i=0; i < items->count; i++)
    {
        DATA_ITEM *item = &items->list[i];
        switch(item->type)
        {
		case DATA_CONSTANT:
		case DATA_FLOATING:
            /* A single item */
            count++;
			break;
                           
		case DATA_REFERENCE:

			/*
			 * A data reference.  Parse the reference.
			 */
            {
                switch(item->data.ref.desc_type)
                {
                case ARRAY_ITYPE:
                    {
                        /* Find out the number of points in the array */
                        DDI_ITEM_SPECIFIER is;
                        DDI_GENERIC_ITEM gi;
                        FLAT_ARRAY *fa;
                        int attr;

                        is.type = DDI_ITEM_ID;
                        is.item.id = item->data.ref.desc_id;
                        memset(&gi, 0, sizeof(DDI_GENERIC_ITEM));
                        attr = ARRAY_NUM_OF_ELEMENTS | ARRAY_TYPE;
                        if (ddi_get_item2(&is, env_info, attr, &gi))
                            return 0;
                        fa = (FLAT_ARRAY *)gi.item;
                        
                        /* Add in the number of points in this array */
                        count += fa->num_of_elements;

                        ddi_clean_item(&gi);
                    }
                    break;
                case VARIABLE_ITYPE:
                    count++;  /* AR 3954 */
                    break;

                }
            }

			break;
        default:
            /* Error - should not get here */
            return 0;
        }
    }
    return count;
}

int
eval_item_waveform(FLAT_WAVEFORM *waveform, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *waveform_masks;	/* temp ptr to the masks of a waveform */
    EXPR tmpExpr;

	TEST_FAIL(I_EVAL_WAVEFORM);

#ifdef DEBUG
	dds_item_flat_check((void *) waveform, WAVEFORM_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	waveform_masks = &waveform->masks;

	if ((waveform->depbin == NULL) && (waveform_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & WAVEFORM_HELP) {
		eval_attr_string((unsigned long) WAVEFORM_HELP, waveform_masks,
            (int (*) ()) eval_string,
			(waveform->depbin ? waveform->depbin->db_help : NULL),
			&waveform->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & WAVEFORM_LABEL) {
		eval_attr_string((unsigned long) WAVEFORM_LABEL, waveform_masks,
			(int (*) ()) eval_string,
			(waveform->depbin ? waveform->depbin->db_label : NULL),
			&waveform->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & WAVEFORM_EMPHASIS) {
		rc = item_mask_man((unsigned long) WAVEFORM_EMPHASIS, waveform_masks,
			(int (*) ()) eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_emphasis : NULL),
			(void *) &waveform->emphasis, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->emphasis = 0;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_EMPHASIS);
        }
	}
	if (mask & WAVEFORM_LINE_TYPE) {
		rc = item_mask_man((unsigned long) WAVEFORM_LINE_TYPE, waveform_masks,
			(int (*) ()) eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_line_type : NULL),
			(void *) &waveform->line_type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->line_type = LINE_TYPE_DATA_N;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_LINE_TYPE);
        }
	}
	if (mask & WAVEFORM_LINE_COLOR) {
		rc = item_mask_man((unsigned long) WAVEFORM_LINE_COLOR, waveform_masks,
			(int (*) ()) eval_attr_int_expr,
			(waveform->depbin ? waveform->depbin->db_line_color : NULL),
			(void *) &waveform->line_color, env_info, &var_needed);

        if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->line_color = LINE_COLOR_DEFAULT;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_LINE_COLOR);
        }
	}
	if (mask & WAVEFORM_HANDLING) {
		rc = item_mask_man((unsigned long) WAVEFORM_HANDLING, waveform_masks,
			(int (*) ()) eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_handling : NULL),
			(void *) &waveform->handling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->handling = READ_HANDLING | WRITE_HANDLING;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_HANDLING);
        }
	}
	if (mask & WAVEFORM_INIT_ACTIONS) {
		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_INIT_ACTIONS,
			env_info, errors, &waveform->init_actions,
			(waveform->depbin ? waveform->depbin->db_init_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->init_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_INIT_ACTIONS);
        }
	}
	if (mask & WAVEFORM_REFRESH_ACTIONS) {
		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_REFRESH_ACTIONS,
			env_info, errors, &waveform->refresh_actions,
			(waveform->depbin ? waveform->depbin->db_refresh_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->refresh_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_REFRESH_ACTIONS);
        }
	}
	if (mask & WAVEFORM_EXIT_ACTIONS) {
		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_EXIT_ACTIONS,
			env_info, errors, &waveform->exit_actions,
			(waveform->depbin ? waveform->depbin->db_exit_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->exit_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_EXIT_ACTIONS);
        }
	}
	if (mask & WAVEFORM_TYPE) {
        if (waveform->depbin && waveform->depbin->db_type)
        {
	        unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	        unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

		    tmp_bin_chunk = waveform->depbin->db_type->bin_chunk;
		    tmp_bin_size = waveform->depbin->db_type->bin_size;
		    DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, (DDL_UINT *)(&waveform->type));
		    waveform_masks->attr_avail |= WAVEFORM_TYPE;
        }
        else
			load_errors(errors, DDL_BINARY_REQUIRED, &var_needed, (unsigned long) WAVEFORM_TYPE);
	}
	if (mask & WAVEFORM_X_INITIAL) {
		rc = item_mask_man((unsigned long) WAVEFORM_X_INITIAL, waveform_masks,
			(int (*) ()) eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_x_initial : NULL),
			(void *) &waveform->x_initial, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                waveform->x_initial.size = 0;
                waveform->x_initial.type = 0;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_INITIAL);
        }
	}
	if (mask & WAVEFORM_X_INCREMENT) {
		rc = item_mask_man((unsigned long) WAVEFORM_X_INCREMENT, waveform_masks,
			(int (*) ()) eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_x_increment : NULL),
			(void *) &waveform->x_increment, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                waveform->x_increment.size = 0;
                waveform->x_increment.type = 0;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_INCREMENT);
        }
	}
	if (mask & WAVEFORM_X_VALUES) {
		rc = item_mask_man((unsigned long) WAVEFORM_X_VALUES, waveform_masks,
			(int (*) ()) eval_attr_dataitems,
			(waveform->depbin ? waveform->depbin->db_x_values : NULL),
			(void *) &waveform->x_values, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->x_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_VALUES);
        }
	}
	if (mask & WAVEFORM_Y_VALUES) {
		rc = item_mask_man((unsigned long) WAVEFORM_Y_VALUES, waveform_masks,
			(int (*) ()) eval_attr_dataitems,
			(waveform->depbin ? waveform->depbin->db_y_values : NULL),
			(void *) &waveform->y_values, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->y_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_Y_VALUES);
        }
	}
	if (mask & WAVEFORM_NUMBER_OF_POINTS) {
        /*
        rc = eval_attr_expr(waveform->depbin->db_number_of_points->bin_chunk,
            waveform->depbin->db_number_of_points->bin_size,
            &tmpExpr, 
            &waveform->depbin->db_number_of_points->dep, 
            env_info, &var_needed);
            */

		rc = item_mask_man((unsigned long) WAVEFORM_NUMBER_OF_POINTS, waveform_masks,
			(int (*) ()) eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_number_of_points : NULL),
			(void *) &tmpExpr, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                /* AR 2809 - add a function to count the points */
                int x_count = data_items_count(env_info, &waveform->x_values);
                int y_count = data_items_count(env_info, &waveform->y_values);
                if (x_count == y_count)
                    waveform->number_of_points = x_count;
                else
                    waveform->number_of_points = 0;
            }
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_NUMBER_OF_POINTS);
        }
        else
            waveform->number_of_points = tmpExpr.val.u;
	}
	if (mask & WAVEFORM_KEY_X_VALUES) {
		rc = item_mask_man((unsigned long) WAVEFORM_KEY_X_VALUES, waveform_masks,
			(int (*) ()) eval_attr_dataitems,
			(waveform->depbin ? waveform->depbin->db_key_x_values : NULL),
			(void *) &waveform->key_x_values, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->key_x_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_KEY_X_VALUES);
        }
	}
	if (mask & WAVEFORM_KEY_Y_VALUES) {
		rc = item_mask_man((unsigned long) WAVEFORM_KEY_Y_VALUES, waveform_masks,
			(int (*) ()) eval_attr_dataitems,
			(waveform->depbin ? waveform->depbin->db_key_y_values : NULL),
			(void *) &waveform->key_y_values, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->key_y_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_KEY_Y_VALUES);
        }
	}
	if (mask & WAVEFORM_Y_AXIS) {

		rc = item_mask_man((unsigned long) WAVEFORM_Y_AXIS, waveform_masks,
			(int (*) ()) eval_attr_ref,
			(waveform->depbin ? waveform->depbin->db_y_axis : NULL),
			(void *) &waveform->y_axis, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->y_axis = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_Y_AXIS);
        }
	}
	if (mask & WAVEFORM_VALID) {

		rc = item_mask_man((unsigned long) WAVEFORM_VALID, waveform_masks,
			(int (*) ()) eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_valid : NULL),
			(void *) &waveform->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				waveform->valid = TRUE;
				waveform_masks->attr_avail |= (unsigned long) WAVEFORM_VALID;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_VALID);
			}
		}
	}



#ifdef DEBUG
	dds_item_flat_check((void *) waveform, WAVEFORM_ITYPE);
#endif /* DEBUG */

	if (errors->count) {
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

int
eval_item_grid(FLAT_GRID *grid, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *grid_masks;	/* temp ptr to the masks of a grid */

	TEST_FAIL(I_EVAL_GRID);

#ifdef DEBUG
	dds_item_flat_check((void *) grid, GRID_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	grid_masks = &grid->masks;

	if ((grid->depbin == NULL) && (grid_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & GRID_HELP) {
		eval_attr_string((unsigned long) GRID_HELP, grid_masks,
            (int (*) ()) eval_string,
			(grid->depbin ? grid->depbin->db_help : NULL),
			&grid->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & GRID_LABEL) {
		eval_attr_string((unsigned long) GRID_LABEL, grid_masks,
			(int (*) ()) eval_string,
			(grid->depbin ? grid->depbin->db_label : NULL),
			&grid->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & GRID_VECTOR) {
		rc = item_mask_man((unsigned long) GRID_VECTOR, grid_masks,
			(int (*) ()) eval_attr_vectors,
			(grid->depbin ? grid->depbin->db_vectors : NULL),
			(void *) &grid->vectors, env_info, &var_needed);
		if (rc != DDL_SUCCESS) 
        {
	    	load_errors(errors, rc, &var_needed, (unsigned long) GRID_VECTOR);
        }
	}
	if (mask & GRID_HEIGHT) {
		rc = item_mask_man((unsigned long) GRID_HEIGHT, grid_masks,
			(int (*) ()) eval_attr_ulong,
			(grid->depbin ? grid->depbin->db_height : NULL),
			(void *) &grid->height, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                grid->height = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) GRID_HEIGHT);
        }
	}
	if (mask & GRID_WIDTH) {
		rc = item_mask_man((unsigned long) GRID_WIDTH, grid_masks,
			(int (*) ()) eval_attr_ulong,
			(grid->depbin ? grid->depbin->db_width : NULL),
			(void *) &grid->width, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                grid->width = DISPLAY_SIZE_MEDIUM;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) GRID_WIDTH);
        }
	}

    if (mask & GRID_HANDLING) {
		rc = item_mask_man((unsigned long) GRID_HANDLING, grid_masks,
			(int (*) ()) eval_attr_bitstring,
			(grid->depbin ? grid->depbin->db_handling : NULL),
			(void *) &grid->handling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
			if (rc == DDL_DEFAULT_ATTR) {
				grid->handling = READ_HANDLING | WRITE_HANDLING;
				grid_masks->attr_avail |= (unsigned long) GRID_HANDLING;
			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) GRID_HANDLING);
			}
		}
	}
	if (mask & GRID_VALIDITY) {
		rc = item_mask_man((unsigned long) GRID_VALIDITY, grid_masks,
			(int (*) ()) eval_attr_ulong,
			(grid->depbin ? grid->depbin->db_valid : NULL),
			(void *) &grid->validity, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				grid->validity = TRUE;
				grid_masks->attr_avail |= (unsigned long) GRID_VALIDITY;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) GRID_VALIDITY);
			}
		}
	}

	if (mask & GRID_ORIENTATION) {
		rc = item_mask_man((unsigned long) GRID_ORIENTATION, grid_masks,
			(int (*) ()) eval_attr_ulong,
			(grid->depbin ? grid->depbin->db_orientation : NULL),
			(void *) &grid->orientation, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                grid->orientation = GRID_ORIENTATION_VERTICAL;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) GRID_ORIENTATION);
        }
	}

#ifdef DEBUG
	dds_item_flat_check((void *) grid, GRID_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

/* Strip out the proper image, based on language code, then locate
 * the image in the device directory table.
 */
static int lookupImage(char *path, BINARY_IMAGE *outBin, ENV_INFO2 *env_info)
{
    FLAT_DEVICE_DIR *fdd = NULL;
    char newPath[255];
    int i;

    // Verify the path is valid
    if ((!path) || !(*path))
        return -1;

    // Strip out the correct subpath
    if (ddi_get_string_translation(path, g_dds_language_code, newPath, sizeof(newPath)))
        return -1;

    // Locate the device directory from the env_info
    if (env_info->type == ENV_BLOCK_HANDLE)
    {
        if (get_abt_dd_dev_tbls(env_info->block_handle, (void **)&fdd))
            return -1;
    }
    else if (env_info->type == ENV_DEVICE_HANDLE)
    {
        if (get_adt_dd_dev_tbls(env_info->device_handle, (void **)&fdd))
            return -1;
    }

    // Look up the path in the image table
    for(i=0; i < fdd->binary_tbl.count; i++)
    {
        if (!strcmp(newPath, fdd->binary_tbl.list[i].name))
        {
            // Assign the outBin structure and return success
            outBin->data = fdd->binary_tbl.list[i].data;
            outBin->length = fdd->binary_tbl.list[i].count;
            return 0;
        }
    }
    return -1;
}

int
eval_item_image(FLAT_IMAGE *image, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed;	/* temp storage for var_needed */
	FLAT_MASKS     *image_masks;	/* temp ptr to the masks of a image */

	TEST_FAIL(I_EVAL_IMAGE);

#ifdef DEBUG
	dds_item_flat_check((void *) image, IMAGE_ITYPE);
#endif /* DEBUG */

	var_needed.id = 0;	/* Zero id says that this is not set */
	image_masks = &image->masks;

	if ((image->depbin == NULL) && (image_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & IMAGE_HELP) {
		eval_attr_string((unsigned long) IMAGE_HELP, image_masks,
            (int (*) ()) eval_string,
			(image->depbin ? image->depbin->db_help : NULL),
			&image->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & IMAGE_LABEL) {
		eval_attr_string((unsigned long) IMAGE_LABEL, image_masks,
			(int (*) ()) eval_string,
			(image->depbin ? image->depbin->db_label : NULL),
			&image->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & IMAGE_VALIDITY) {
		rc = item_mask_man((unsigned long) IMAGE_VALIDITY, image_masks,
			(int (*) ()) eval_attr_ulong,
			(image->depbin ? image->depbin->db_valid : NULL),
			(void *) &image->valid, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				image->valid = TRUE;
				image_masks->attr_avail |= (unsigned long) IMAGE_VALIDITY;

			} else {
				load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_VALIDITY);
			}
		}
	}
	if ((mask & IMAGE_PATH) || (mask & IMAGE_BINARY)) {
		eval_attr_string((unsigned long) IMAGE_PATH, image_masks,
            (int (*) ()) eval_string,
			(image->depbin ? image->depbin->db_path : NULL),
			&image->path, env_info, errors,	0);
	}
       
	if (mask & IMAGE_BINARY) {
        /* The path must be present to find the image */
        if (!(image->masks.attr_avail & IMAGE_PATH))
            image->masks.attr_avail &= ~IMAGE_BINARY;
        else
        {
            if (lookupImage(image->path.str, &image->entry, env_info))
                image->masks.attr_avail &= ~IMAGE_BINARY;
            else
                image->masks.attr_avail |= IMAGE_BINARY;
        }
	}

    if (mask & IMAGE_LINK) {

		rc = item_mask_man((unsigned long) IMAGE_LINK, image_masks,
			(int (*) ()) eval_attr_desc_ref,
			(image->depbin ? image->depbin->db_link : NULL),
			(void *) &image->link, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR)) {
			load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_LINK);
		}
	}


#ifdef DEBUG
	dds_item_flat_check((void *) image, IMAGE_ITYPE);
#endif /* DEBUG */

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		eval_item() is a distributor function for all "eval_item_*" routines.
 *		eval_item() "swithes" on the "item_type" and calls the corresponding
 *		"eval_item_*" routine.
 *
 *	Inputs:
 *		mask:			bit mask specifying which attributes are desired.
 *		env_info:		environment information
 *		item_type:		type of item to be evaluated (var, menu,....).
 *
 *	Outputs:
 *		item:			FLAT structure containing the requested item info.
 *		errors:			list of errors incurred processing an item.
 *
 *	Returns:
 *		DDL_INVALID_PARAM,
 *		return values from other "eval_item_*" functions
 *
 *	Author: steve beyerl
 *
 **********************************************************************/
int
eval_item(void *item, unsigned long mask, ENV_INFO2 *env_info,
	RETURN_LIST *errors, ITEM_TYPE item_type)
{

	int             rc;

	TEST_FAIL(EVAL_ONE_ITEM);

	switch (item_type) {
	case VARIABLE_ITYPE:
		rc = eval_item_var((FLAT_VAR *) item, mask, env_info, errors);
		break;
	case MENU_ITYPE:
		rc = eval_item_menu((FLAT_MENU *) item, mask, env_info, errors);
		break;
	case EDIT_DISP_ITYPE:
		rc = eval_item_edit_display((FLAT_EDIT_DISPLAY *) item, mask,
			env_info, errors);
		break;
	case METHOD_ITYPE:
		rc = eval_item_method((FLAT_METHOD *) item, mask, env_info, errors);
		break;
	case REFRESH_ITYPE:
		rc = eval_item_refresh((FLAT_REFRESH *) item, mask, env_info, errors);
		break;
	case UNIT_ITYPE:
		rc = eval_item_unit((FLAT_UNIT *) item, mask, env_info, errors);
		break;
	case WAO_ITYPE:
		rc = eval_item_wao((FLAT_WAO *) item, mask, env_info, errors);
		break;
	case ITEM_ARRAY_ITYPE:
		rc = eval_item_itemarray((FLAT_ITEM_ARRAY *) item, mask, env_info,
			errors);
		break;
	case COLLECTION_ITYPE:
		rc = eval_item_collection((FLAT_COLLECTION *) item, mask, env_info,
			errors);
		break;
	case BLOCK_ITYPE:
		rc = eval_item_block((FLAT_BLOCK *) item, mask, env_info, errors);
		break;
	case PROGRAM_ITYPE:
		rc = eval_item_program((FLAT_PROGRAM *) item, mask, env_info, errors);
		break;
	case RECORD_ITYPE:
		rc = eval_item_record((FLAT_RECORD *) item, mask, env_info, errors);
		break;
	case ARRAY_ITYPE:
		rc = eval_item_array((FLAT_ARRAY *) item, mask, env_info, errors);
		break;
	case VAR_LIST_ITYPE:
		rc = eval_item_var_list((FLAT_VAR_LIST *) item, mask, env_info, errors);
		break;
	case RESP_CODES_ITYPE:
		rc = eval_item_response_code((FLAT_RESP_CODE *) item, mask, env_info,
			errors);
		break;
	case DOMAIN_ITYPE:
		rc = eval_item_domain((FLAT_DOMAIN *) item, mask, env_info, errors);
		break;
	case AXIS_ITYPE:
		rc = eval_item_axis((FLAT_AXIS *) item, mask, env_info, errors);
		break;
	case CHART_ITYPE:
		rc = eval_item_chart((FLAT_CHART *) item, mask, env_info, errors);
		break;
	case FILE_ITYPE:
		rc = eval_item_file((FLAT_FILE *) item, mask, env_info, errors);
		break;
	case GRAPH_ITYPE:
		rc = eval_item_graph((FLAT_GRAPH *) item, mask, env_info, errors);
		break;
	case LIST_ITYPE:
		rc = eval_item_list((FLAT_LIST *) item, mask, env_info, errors);
		break;
	case SOURCE_ITYPE:
		rc = eval_item_source((FLAT_SOURCE *) item, mask, env_info, errors);
		break;
	case WAVEFORM_ITYPE:
		rc = eval_item_waveform((FLAT_WAVEFORM *) item, mask, env_info, errors);
		break;
	case GRID_ITYPE:
		rc = eval_item_grid((FLAT_GRID *) item, mask, env_info, errors);
		break;
	case IMAGE_ITYPE:
		rc = eval_item_image((FLAT_IMAGE *) item, mask, env_info, errors);
		break;
	default:
		return DDL_INVALID_PARAM;
	}

	return rc;
}
