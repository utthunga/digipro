/*
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  This file contains all of the functions for DDI language
 */

#include <string.h>
#include <ctype.h>

#include "std.h"
#include "rtn_code.h"
#include "app_xmal.h"

char g_dds_language_code[32] = {'|', 'e', 'n', '|', '\0'};

#define D_CHAR	0X2A	/* default char for unconvertable characters */
						/* currently using asterisk '*' as default   */

static const
unsigned char   iso_to_pc[] = {

/* offset for character look up is 0XA0 */

	0XFF,			/* 0XA0		phantom space */
	0XAD,			/* 0XA1		opening exclamation */
	0X9B,			/* 0XA2		cent sign */
	0X9C,			/* 0XA3		pound sign */
	D_CHAR,			/* 0XA4 */
	0X9D,			/* 0XA5		yen sign */
	D_CHAR,			/* 0XA6 */
	0X15,			/* 0XA7		section sign */
	D_CHAR,			/* 0XA8 */
	D_CHAR,			/* 0XA9 */
	0X0C,			/* 0XAA		female sign */
	0XAE,			/* 0XAB		opening guillemets */
	D_CHAR,			/* 0XAC */
	D_CHAR,			/* 0XAD */
	D_CHAR,			/* 0XAE */
	D_CHAR,			/* 0XAF */

	0XF8,			/* 0XB0		degree */
	0XF1,			/* 0XB1		plus minus sign */
	0XFD,			/* 0XB2		superscript 2 */
	D_CHAR,			/* 0XB3 */
	D_CHAR,			/* 0XB4 */
	0XE6,			/* 0XB5		micro sign / mu */
	0X14,			/* 0XB6		paragraph sign */
	0XFE,			/* 0XB7		box */
	D_CHAR,			/* 0XB8 */
	D_CHAR,			/* 0XB9 */
	0X0B,			/* 0XBA		male sign */
	0XAF,			/* 0XBB		closing guillemets */
	0XAC,			/* 0XBC		1/4 */
	0XAB,			/* 0XBD		1/2 */
	D_CHAR,			/* 0XBE */
	0XA8,			/* 0XBF		opening question mark */

	0X41,			/* 0XC0		A */
	0X41,			/* 0XC1		A */
	0X41,			/* 0XC2		A */
	0X41,			/* 0XC3		A */
	0X8E,			/* 0XC4		A diaeresis */
	0X8F,			/* 0XC5		A ring */
	0X92,			/* 0XC6		AE dipthong */
	0X80,			/* 0XC7		C cedilla */
	0X45,			/* 0XC8		E */
	0X90,			/* 0XC9		E acute */
	0X45,			/* 0XCA		E */
	0X45,			/* 0XCB		E */
	0X49,			/* 0XCC		I */
	0X49,			/* 0XCD		I */
	0X49,			/* 0XCE		I */
	0X49,			/* 0XCF		I */

	0X44,			/* 0XD0		D */
	0XA5,			/* 0XD1		N tilde */
	0X4F,			/* 0XD2		O */
	0X4F,			/* 0XD3		O */
	0X4F,			/* 0XD4		O */
	0X4F,			/* 0XD5		O */
	0X99,			/* 0XD6		O diaeresis */
	0X78,			/* 0XD7		x */
	D_CHAR,			/* 0XD8 */
	0X55,			/* 0XD9		U */
	0X55,			/* 0XDA		U */
	0X55,			/* 0XDB		U */
	0X9A,			/* 0XDC		U diaeresis */
	0X59,			/* 0XDD		Y */
	D_CHAR,			/* 0XDE */
	D_CHAR,			/* 0XDF */

	0X85,			/* 0XE0		a grave */
	0XA0,			/* 0XE1		a acute */
	0X83,			/* 0XE2		a circumflex */
	0X61,			/* 0XE3		a */
	0X61,			/* 0XE4		a */
	0XC8,			/* 0XE5		a ring */
	0X91,			/* 0XE6		ae dipthong */
	0X87,			/* 0XE7		c cedilla */
	0X8A,			/* 0XE8		e grave */
	0X65,			/* 0XE9		e */
	0X88,			/* 0XEA		e circumflex */
	0X89,			/* 0XEB		e diaeresis */
	0X8D,			/* 0XEC		i circumflex */
	0XA1,			/* 0XED		i acute */
	0X8C,			/* 0XEE		i circumflex */
	0X8B,			/* 0XEF		i diaeresis */

	D_CHAR,			/* 0XF0 */
	0XA4,			/* 0XF1		n tilde */
	0X95,			/* 0XF2		o grave */
	0XA2,			/* 0XF3		o acute */
	0X93,			/* 0XF4		o circumflex */
	0X6F,			/* 0XF5		o */
	0X94,			/* 0XF6		o diaeresis */
	0XF6,			/* 0XF7		divide by sign */
	D_CHAR,			/* 0XF8 */
	0X97,			/* 0XF9		u grave */
	0XA3,			/* 0XFA		u acute */
	0X96,			/* 0XFB		u circumflex */
	0X75,			/* 0XFC		u */
	0X79,			/* 0XFD		y */
	D_CHAR,			/* 0XFE */
	0X98			/* 0XFF		y diaeresis */

};

#define PC_TABLE_OFFSET		0XA0
#define ISO_TABLE_OFFSET	0X7F
#define NO_CONVERSION		0X7F

#define DEF_LANG_CTRY		"|en|" /*	Of the form "|LL|" or "|LL CC|", where
									*	LL = language code and
									*	CC = country code (optional)
									*/

/*********************************************************************
 *
 *	Name: ddi_iso_to_pc
 *	ShortDesc: convert an ISO string to an ASCII string
 *
 *	Description:
 *		ddi_iso_to_pc takes a pointer to an ASCII string
 *		as input and allocates memory for a new string
 *		and then converts the ISO string to an ASCII string,
 *		storing the converted string in the malloced space.
 *
 *	Inputs:
 *		iso:	     a pointer to the ISO string
 *		buffer_size: the size of the inbound buffer
 *
 *	Outputs:
 *		buffer:	the loaded buffer
 *
 *	Returns:
 *		DDS_SUCCESS, DDI_INSUFFICIENT_BUFFER
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_iso_to_pc(char *iso, char *buffer, int buffer_size)
{

	size_t          inc;		/* incrementer */
	size_t          length;		/* length of string */
	unsigned char	*iso_temp;	/* temp unsigned char buffer */

	ASSERT_RET(iso && buffer && buffer_size, DDI_INVALID_PARAM);

	iso_temp = (unsigned char *) iso;
	length = strlen(iso);
	length++;		/* add the NULL character to the count */

	if (length > (size_t) buffer_size) {
		return DDI_INSUFFICIENT_BUFFER;
	}

	for (inc = 0; inc < length; inc++, buffer++, iso_temp++) {
		
		if (*iso_temp < NO_CONVERSION) {
			*buffer = (char) *iso_temp;	/* no conversion */

		} else if (*iso_temp < PC_TABLE_OFFSET) { 
			
			/*
			 * chunk of iso character set which is empty
			 */
			*buffer = 0X20;	/* space character */

		} else {
			*buffer = (char) iso_to_pc[*iso_temp - PC_TABLE_OFFSET];	/* conversion table */
		}
	}

	return DDS_SUCCESS;
}

static const
unsigned char   pc_to_iso[] = {

	D_CHAR,			/* 0X7F	 */

	0XC7,			/* 0X80		C cedille	 */
	0XFC,			/* 0X81		u umlaut	 */
	0XE9,			/* 0X82		e acute	 */
	0XE2,			/* 0X83		a circumflex	 */
	0XE4,			/* 0X84		a umlaut	 */
	0XE0,			/* 0X85		a grave	 */
	0XE5,			/* 0X86		a ring	 */
	0XE7,			/* 0X87		c cedille	 */
	0XEA,			/* 0X88		e circumflex	 */
	0XEB,			/* 0X89		e umlaut	 */
	0XE8,			/* 0X8A		e grave	 */
	0XEF,			/* 0X8B		i umlaut	 */
	0XEE,			/* 0X8C		i curcumflex	 */
	0XEC,			/* 0X8D		i grave	 */
	0XC4,			/* 0X8E		A umlaut	 */
	0XC5,			/* 0X8F		A ring	 */

	0XC9,			/* 0X90		E acute	 */
	0XE6,			/* 0X91		ae	 */
	0XC6,			/* 0X92		AE	 */
	0XF4,			/* 0X93		o circumflex	 */
	0XF6,			/* 0X94		o umlaut	 */
	0XF2,			/* 0X95		o grave	 */
	0XFB,			/* 0X96		u circumflex	 */
	0XF9,			/* 0X97		u grave	 */
	0XFF,			/* 0X98		y umlaut	 */
	0XD6,			/* 0X99		O umlaut	 */
	0XDC,			/* 0X9A		U umlaut	 */
	0XA2,			/* 0X9B		cent sign	 */
	0XA3,			/* 0X9C		pound sign (British currency)	 */
	0XA5,			/* 0X9D		yen sign	 */
	D_CHAR,			/* 0X9E */
	D_CHAR,			/* 0X9F */

	0XE1,			/* 0XA0		a acute	 */
	0XED,			/* 0XA1		i acute	 */
	0XF3,			/* 0XA2		o acute	 */
	0XFA,			/* 0XA3		u acute	 */
	0XF1,			/* 0XA4		n tilde	 */
	0XD1,			/* 0XA5		N tilde	 */
	0XAA,			/* 0XA6		a macron (superscripted, underscored a)	 */
	0XBA,			/* 0XA7		o macron (superscripted, underscored o)	 */
	0XBF,			/* 0XA8		inverted question mark	 */
	D_CHAR,			/* 0XA9 */
	0XAC,			/* 0XAA		not sign	 */
	0XBD,			/* 0XAB		1/2	 */
	0XBC,			/* 0XAC		1/4	 */
	0XA1,			/* 0XAD		inverted exclamation mark	 */
	0XAB,			/* 0XAE		left angle quotation mark	 */
	0XBB,			/* 0XAF		right angle quotation mark	 */

	D_CHAR,			/* 0XB0 */
	0X02,			/* 0XB1		medium block */
	D_CHAR,			/* 0XB2 */
	0X19,			/* 0XB3 	single vertical */
	0X16,			/* 0XB4 	single right juncture */
	D_CHAR,			/* 0XB5 */
	D_CHAR,			/* 0XB6 */
	D_CHAR,			/* 0XB7 */
	D_CHAR,			/* 0XB8 */
	D_CHAR,			/* 0XB9 */
	D_CHAR,			/* 0XBA */
	D_CHAR,			/* 0XBB */
	D_CHAR,			/* 0XBC */
	D_CHAR,			/* 0XBD */
	D_CHAR,			/* 0XBE */
	0X0C,			/* 0XBF 	single upper right */

	0X0E,			/* 0XC0 	single lower left */
	0X17,			/* 0XC1 	single lower juncture */
	0X18,			/* 0XC2		single upper juncture */
	0X15,			/* 0XC3 	single left juncture */
	0X12,			/* 0XC4 	single horizontal */
	0X0F,			/* 0XC5 	single intersection */
	D_CHAR,			/* 0XC6 */
	D_CHAR,			/* 0XC7 */
	D_CHAR,			/* 0XC8 */
	D_CHAR,			/* 0XC9 */
	D_CHAR,			/* 0XCA */
	D_CHAR,			/* 0XCB */
	D_CHAR,			/* 0XCC */
	D_CHAR,			/* 0XCD */
	D_CHAR,			/* 0XCE */
	D_CHAR,			/* 0XCF */

	D_CHAR,			/* 0XD0 */
	D_CHAR,			/* 0XD1 */
	D_CHAR,			/* 0XD2 */
	D_CHAR,			/* 0XD3 */
	D_CHAR,			/* 0XD4 */
	D_CHAR,			/* 0XD5 */
	D_CHAR,			/* 0XD6 */
	D_CHAR,			/* 0XD7 */
	D_CHAR,			/* 0XD8 */
	0X0B,			/* 0XD9		single lower right */
	0X0D,			/* 0XDA 	single upper left */
	D_CHAR,			/* 0XDB */
	D_CHAR,			/* 0XDC */
	D_CHAR,			/* 0XDD */
	D_CHAR,			/* 0XDE */
	D_CHAR,			/* 0XDF */

	D_CHAR,			/* 0XE0 */
	D_CHAR,			/* 0XE1 */
	D_CHAR,			/* 0XE2 */
	D_CHAR,			/* 0XE3 */
	D_CHAR,			/* 0XE4 */
	D_CHAR,			/* 0XE5 */
	D_CHAR,			/* 0XE6 */
	D_CHAR,			/* 0XE7 */
	D_CHAR,			/* 0XE8 */
	D_CHAR,			/* 0XE9 */
	D_CHAR,			/* 0XEA */
	D_CHAR,			/* 0XEB */
	D_CHAR,			/* 0XEC */
	D_CHAR,			/* 0XED */
	D_CHAR,			/* 0XEE */
	D_CHAR,			/* 0XEF */

	D_CHAR,			/* 0XF0 */
	0X08,			/* 0XF1		plus/minus */
	0X1B,			/* 0XF2		greater/equal sign */
	0X1A,			/* 0XF3 	less/equal sign */
	D_CHAR,			/* 0XF4 */
	D_CHAR,			/* 0XF5 */
	0XF8,			/* 0XF6 	divide by sign */
	D_CHAR,			/* 0XF7 */
	0X07,			/* 0XF8 	degree */
	D_CHAR,			/* 0XF9 */
	D_CHAR,			/* 0XFA */
	D_CHAR,			/* 0XFB */
	D_CHAR,			/* 0XFC */
	0XB2,			/* 0XFD 	superscript 2 */
	0X1F,			/* 0XFE 	box */
	0XA0			/* 0XFF 	phantom space */
};


/*********************************************************************
 *
 *	Name: ddi_pc_to_iso
 *	ShortDesc: convert an ASCII string to an ISO string
 *
 *	Description:
 *		ddi_pc_to_iso takes a pointer to an ASCII string as input and
 *		allocates memory for a new string and then converts the ASCII string
 *		to an ISO string, storing the converted string in the malloced space.
 *
 *	Inputs:
 *		pc:		     a pointer to the ASCII string
 *      buffer_size: the length of the inbound buffer
 *
 *	Outputs:
 *		bufffer: the loaded buffer
 *
 *	Returns:
 *		DDS_SUCCESS, DDI_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_pc_to_iso(char *pc, char *buffer, int buffer_size)
{

	size_t          inc;	    /* incrementer */
	size_t          length;	    /* length of string */
	unsigned char	*pc_temp;	/* temp unsigned char buffer */

	ASSERT_RET(pc && buffer && buffer_size, DDI_INVALID_PARAM);

	pc_temp = (unsigned char *) pc;
	length = strlen(pc);
	length++;		/* add the NULL character to the count */

	if (length > (size_t) buffer_size) {
		return DDI_INSUFFICIENT_BUFFER;
	}

	for (inc = 0; inc < length; inc++, buffer++, pc_temp++) {

		if (*pc_temp < NO_CONVERSION) {
			*buffer = (char) *pc_temp;	/* no conversion */

		} else {
			*buffer = (char) pc_to_iso[ *pc_temp - ISO_TABLE_OFFSET];	/* conversion table */
		}
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: get_string_length
 *
 *	ShortDesc: get the string length
 *
 *	Description:
 *		get the length of the string which is pointed to by start and is
 *		delimited by a COUNTRY_CODE_MARK symbol or an end of string character
 *		and which may be embedded in a list of other translation strings
 *
 *	Inputs:
 *		start:	a pointer to the start of the string
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		the length of the string
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

#define COUNTRY_CODE_MARK 	'|'

static size_t
get_string_length(char *start)
{

	char           *stop;
	char           *temp;
	size_t          length;

	temp = start;

	while ((stop = strchr(temp, COUNTRY_CODE_MARK)) != NULL) {

		/*
		 *	Check to see if stop is pointing to the next language/country
		 *	code.  First check for the old-style code, then for the new,
		 *	improved code (both language only and language/country codes).
		 */
		if ((strlen(stop) >= 4) && isdigit(stop[1]) &&
				isdigit(stop[2]) && isdigit(stop[3])) {
			break;

		} else if ((strlen(stop) >= 4) && (stop[3] == COUNTRY_CODE_MARK)) {
			break;

		} else if ((strlen(stop) >= 7) && (stop[6] == COUNTRY_CODE_MARK)) {
			break;

		} else {
			temp = stop + 1;	/* Got a '|' but no language/country code */
		}
	}

	if (stop == NULL) {
		length = strlen(start);
	} else {
		length = (stop - start);
	}

	return (length);
}


/*********************************************************************
 *
 *	Name: ddi_get_string_translation
 *	ShortDesc: return the requested translation of the string
 *
 *	Description:
 *		ddi_get_string_translation will take the given list of strings,
 *		and using the country code, extract and copy the correct string
 *		into the buffer
 *
 *	Inputs:
 *		string:			a pointer to the string to search for the
 *						correct translation
 *		country_code:	a pointer to the country code string
 *		buffer_size:	the length of the buffer
 *
 *	Outputs:
 *		buffer:			a pointer to the buffer with the correct
 *						translation loaded
 *
 *	Returns:
 *		DDS_SUCCESS, DDI_INSUFFICIENT_BUFFER
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_string_translation(const char *string, char *country_code, char *buffer,
	int buffer_size)
{

	char           *start;		/* the start of the actual string */
	size_t          length;     /* the length of the actual string */

	ASSERT_RET(string && country_code && buffer && buffer_size, DDI_INVALID_PARAM);

	/*
	 *	Look for the specified language/country code in the string.  If the
	 *	specified language/country code can't be found in the string, look
	 *	for the default (i.e., English) language/country code.  If there is
	 *	no default language/country code, use the first string found.
	 */

	if ((start = strstr(string, country_code)) != NULL) {
		start += strlen(country_code);

	} else if ((start = strstr(string, "|001")) != NULL) {
		start += strlen("|001");

	} else if ((start = strstr(string, DEF_LANG_CTRY)) != NULL) {
		start += strlen(DEF_LANG_CTRY);

	} else {
		start = string;
	}

	/*
	 *	We may have found a string to output.  Check the length of the
	 *	output string.  If the string is longer than the output buffer
	 *	or shows a negative length, return an error code; else, return OK.
	 */
	length = get_string_length(start);

	if ((size_t) buffer_size < (length + 1)) {
		buffer[0] = '\0';
		return DDI_INSUFFICIENT_BUFFER;
	}

	if (length < 1) {
		buffer[0] = '\0';
		return DDI_INVALID_DEV_SPEC_STRING;
	}

	(void)strncpy(buffer, start, length);
	buffer[length] = '\0';

	return DDS_SUCCESS;
}

/*
 * ddi_set_language_code
 * Set the global language code used inside DDS for image loading.
 * Example codes are "|en|", "|de|", etc.
 */
extern int ddi_set_language_code(char *new_code)
{
    if (strlen(new_code) >= (sizeof(g_dds_language_code) - 2))
        return DDI_INSUFFICIENT_BUFFER;

    /* Make sure we keep the bars in the string */
    if (new_code[0] == '|')
        strcpy(g_dds_language_code, new_code);
    else
    {
        strcpy(g_dds_language_code, "|");
        strcat(g_dds_language_code, new_code);
        strcat(g_dds_language_code, "|");
    }
    return DDS_SUCCESS;
}
