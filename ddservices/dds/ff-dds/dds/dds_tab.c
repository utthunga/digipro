/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/************************************************************
 * Includes
 ************************************************************/

#ifdef SUN
#include	<memory.h>	/* K&R only */
#endif /* SUN */
#include	<stdlib.h>
#include	<string.h>

#include	"std.h"
#include	"attrs.h"
#include	"flats.h"
#include	"table.h"
#include	"rtn_code.h"
#include	"dds_tab.h"
#include "tst_fail.h"

/***********************************************************************
 *
 *  Name:  compare_bint_elem
 *  ShortDesc:  Compare parameter (parameter list) names of two elements
 *				in the Block Item Name Table.
 *
 *  Description:
 *      The compare_bint_elem function takes in two pointers to Block
 *		Item Name Table element structures in a Block Item Name Table
 *		structure and compares their names (Item ID's).  The value
 *		returned is reflective of the comparison of the two names.
 *      This function is used in conjunction with the bsearch function
 *		(for searching the Block Item Name Table).
 *
 *  Inputs:
 *      bint_elem_1 - pointer to a Block Item Name Table element
 *					  structure in a Block Item Name Table structure.
 *      bint_elem_2 - pointer to another Block Item Name Table
 *					  element structure in a Block Item Name Table
 *					  structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_bint_elem(BLK_ITEM_NAME_TBL_ELEM *bint_elem_1, BLK_ITEM_NAME_TBL_ELEM *bint_elem_2)
{

	/*
	 *	Compare the two names and return the reflective value.
	 */

	if (BINTE_BLK_ITEM_NAME(bint_elem_1) <
			BINTE_BLK_ITEM_NAME(bint_elem_2)) {
		return(-1);
	}

	else if (BINTE_BLK_ITEM_NAME(bint_elem_1) >
			BINTE_BLK_ITEM_NAME(bint_elem_2)) {
		return(1);
	}

	else {
		return(0);
	}
}


/***********************************************************************
 *
 *  Name:  find_bint_elem
 *  ShortDesc:  Find an element in the Block Item Name Table using a
 *				parameter list name.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the Block Item Named Table.  The function
 *				compare_bint_elem used to compare elements of the
 *				Block Item Name Table.  If an element (containing the
 *				desired item id) is found, it is returned to the caller.
 *
 *  Inputs:
 *      bint - pointer to a Block Item Name Table structure to search
 *			   for the parameter name or parameter list name.
 *      name - the item id of the name of the requested parameter or
 *			   parameter list.
 *
 *  Outputs:
 *      bint_elem - a pointer to the Block Item Name Table element
 *				    that contains the parameter name or parameter
 *				    list name.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_NAME.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
find_bint_elem(BLK_ITEM_NAME_TBL *bint, ITEM_ID name, BLK_ITEM_NAME_TBL_ELEM **bint_elem)
{

	int						assert_conds ;
    BLK_ITEM_NAME_TBL_ELEM  dummy_elem;

	assert_conds = bint && name && bint_elem && BINT_LIST(bint) &&
			BINT_COUNT(bint) ;
	ASSERT_RET(assert_conds, DDI_INVALID_PARAM) ;

	/*
	 *	Find the Block Item Name Table element Containing the parameter
	 *	name or parameter list name.
	 */

    /* AR 4626: avoid possible buffer-overread by creating a dummy elem to hold the item ID */
    memset(&dummy_elem, 0, sizeof(dummy_elem));
    dummy_elem.blk_item_name = name;

	*bint_elem = (BLK_ITEM_NAME_TBL_ELEM *) bsearch(
		(char *)&dummy_elem, (char *)BINT_LIST(bint),
		(unsigned int)BINT_COUNT(bint), sizeof(BLK_ITEM_NAME_TBL_ELEM),
		(CMP_FN_PTR)compare_bint_elem);

	/*
	 *	Determine if the parameter name or parameter list name was found.
	 */

	if (*bint_elem == 0) {
		return(DDI_TAB_BAD_NAME);
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  compare_bit_elem
 *  ShortDesc:  Compare Item ID's of two items in the Block Item
 *              Table.
 *
 *  Description:
 *      The compare_bit_elem function takes in two pointers to Block
 *      Item Table element structures in a Block Item Table structure
 *      and compares their Item ID's.  The value returned is
 *		reflective of the comparison of the two Item ID's.  This
 *      function is used in conjunction with the bsearch function
 *      (for searching the Block Item Table).
 *
 *  Inputs:
 *      bit_elem_1 - pointer to a Block Item Table element structure
 *					 in a Block Item Table structure.
 *      bit_elem_2 - pointer to another Block Item Table element
 *					 structure in a Block Item Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_bit_elem(BLK_ITEM_TBL_ELEM *bit_elem_1, BLK_ITEM_TBL_ELEM *bit_elem_2)
{

	/*
	 *	Compare the two Item ID's and return the reflective value.
	 */

	if (BITE_BLK_ITEM_ID(bit_elem_1) <
			BITE_BLK_ITEM_ID(bit_elem_2)) {
		return(-1);
	}

	else if (BITE_BLK_ITEM_ID(bit_elem_1) >
			BITE_BLK_ITEM_ID(bit_elem_2)) {
		return(1);
	}

	else {
		return(0);
	}
}


/***********************************************************************
 *
 *  Name:  find_bit_elem
 *  ShortDesc:  Find an element in the Block Item Table using an item
 *				id.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the Block Item Table.  The function compare_bit_elem
 *				is used to compare elements of the Block Item Table.
 *				If an element (containing the desired item id) is found,
 *				it is returned to the caller.
 *
 *  Inputs:
 *      bit - pointer to a Block Item Table structure to search for the
 *			  item id.
 *      item_id - the item id of the element in the Block Item Table to
 *				  find.
 *
 *  Outputs:
 *      bit_elem - a pointer to the Block Item Table element that
 *				   contains the requested item id.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_DDID.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
find_bit_elem(BLK_ITEM_TBL *bit, ITEM_ID item_id, BLK_ITEM_TBL_ELEM **bit_elem)
{

	int						assert_conds ;
    BLK_ITEM_TBL_ELEM       dummy_elem;

	assert_conds = bit && item_id && bit_elem && BIT_LIST(bit) &&
			BIT_COUNT(bit) ;
	ASSERT_RET(assert_conds, DDI_INVALID_PARAM) ;

	/*
	 *	Find the Block Item Table element containing the Item ID.
	 */

    /* AR 4626: avoid possible buffer-overread by creating a dummy elem to hold the item ID */
    memset(&dummy_elem, 0, sizeof(dummy_elem));
    dummy_elem.blk_item_id = item_id;

	*bit_elem = (BLK_ITEM_TBL_ELEM *) bsearch(
		(char *)&dummy_elem, (char *)BIT_LIST(bit),
		(unsigned int)BIT_COUNT(bit), sizeof(BLK_ITEM_TBL_ELEM),
		(CMP_FN_PTR)compare_bit_elem);

	/*
	 *	Determine if the block Item ID was found.
	 */

	if (*bit_elem == 0) {
		return(DDI_TAB_BAD_DDID);
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  compare_cmnt_elem
 *  ShortDesc:  Compare Item IDs of two items in the Characteristic
 *				Member Name Table.
 *
 *  Description:
 *      The compare_cmnt_elem function takes in two pointers to 
 *		Characteristic Member Name Table element structures
 *      and compares their Item IDs.  The value returned is
 *		reflective of the comparison of the two Item IDs.  This
 *      function is used in conjunction with the bsearch function
 *      (for searching the Characteristic Member Name Table).
 *
 *  Inputs:
 *      cmnt_elem_1 - pointer to a Characteristic Member Name Table element 
 *					structure in a Characteristic Member Name Table structure.
 *      cmnt_elem_2 - pointer to another Characteristic Member Name Table 
 *					element structure in a Characteristic Member Name
 *					Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_cmnt_elem(CHAR_MEM_NAME_TBL_ELEM *cmnt_elem_1, CHAR_MEM_NAME_TBL_ELEM *cmnt_elem_2)
{

	/*
	 *	Compare the two Item IDs and return the reflective value.
	 */

	if (CMNTE_CM_NAME(cmnt_elem_1) <
			CMNTE_CM_NAME(cmnt_elem_2)) {
		return(-1);

	} else if (CMNTE_CM_NAME(cmnt_elem_1) >
			CMNTE_CM_NAME(cmnt_elem_2)) {
		return(1);

	} else {
		return(0);
	}
}

/***********************************************************************
 *
 *  Name:  find_cmnt_elem
 *  ShortDesc:  Find an element in the Characteristic Member Name Table 
 *				using a	characteristic record member name.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the characteristic member name Table.  The function 
 *				compare_cmnt_elem is used to compare elements of the 
 *				characteristic member name table.  If an element 
 *				(containing the desired name) is found, it is returned 
 *				to the caller.
 *
 *  Inputs:
 *      cmnt - pointer to a Characteristic Member Name Table structure 
 *				to search for the characteristic member name.
 *      name - the item id of the name of the requested characteristic
 *			   member.
 *
 *  Outputs:
 *      cmnt_elem - a pointer to the Characteristic Member Name Table
 *					element that contains the requested characteristic
 *					member name.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_NAME.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
find_cmnt_elem(CHAR_MEM_NAME_TBL *cmnt, ITEM_ID name, CHAR_MEM_NAME_TBL_ELEM **cmnt_elem)
{

	int						assert_conds ;
    CHAR_MEM_NAME_TBL_ELEM  dummy_elem;

	assert_conds = cmnt && name && cmnt_elem && CMNT_LIST(cmnt) &&
			CMNT_COUNT(cmnt) ;
	ASSERT_RET(assert_conds, DDI_INVALID_PARAM) ;

	/*
	 *	Find the Characteristic Member Name Table element Containing 
	 *	the requested characteristic member name (name).
	 */

    /* AR 4626: avoid possible buffer-overread by creating a dummy elem to hold the member name */
    memset(&dummy_elem, 0, sizeof(dummy_elem));
    dummy_elem.char_mem_name = name;

	*cmnt_elem = (CHAR_MEM_NAME_TBL_ELEM *) bsearch(
		(char *)&dummy_elem, (char *)CMNT_LIST(cmnt),
		(unsigned int)CMNT_COUNT(cmnt), sizeof(CHAR_MEM_NAME_TBL_ELEM),
		(CMP_FN_PTR)compare_cmnt_elem);

	/*
	 *	Determine if the characteristic member name was found.
	 */

	if (*cmnt_elem == 0) {
		return(DDI_TAB_BAD_NAME);
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  compare_pmnt_elem
 *  ShortDesc:  Compare Item ID's of two items in the Parameter Member
 *              Name Table.
 *
 *  Description:
 *      The compare_pmnt_elem function takes in two pointers to 
 *		Parameter Member Name Table element structures in a Parameter
 *		Member Name Table structure  and compares their Item ID's.
 *      The value returned is reflective of the comparison of the 
 *		two Item IDs.  This function is used in conjunction with
 *		the bsearch function (for searching the Block Item Table).
 *
 *  Inputs:
 *      pmnt_elem_1 - pointer to a Parameter Member Name Table element 
 *					structure in a Parameter Member Name Table structure.
 *      pmnt_elem_2 - pointer to a Parameter Member Name Table element 
 *					structure in a Parameter Member Name Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_pmnt_elem(PARAM_MEM_NAME_TBL_ELEM *pmnt_elem_1,
	PARAM_MEM_NAME_TBL_ELEM *pmnt_elem_2)
{

	/*
	 *	Compare the two Item IDs and return the reflective value.
	 */

	if (PMNTE_PM_NAME(pmnt_elem_1) <
			PMNTE_PM_NAME(pmnt_elem_2)) {
		return(-1);

	} else if (PMNTE_PM_NAME(pmnt_elem_1) >
			PMNTE_PM_NAME(pmnt_elem_2)) {
		return(1);

	} else {
		return(0);
	}
}


/***********************************************************************
 *
 *  Name:  find_pmnt_elem
 *  ShortDesc:  Find an element in the Parameter Member Name Table using a
 *				parameter record member name.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the parameter member name Table.  The function 
 *				compare_pmnt_elem is used to compare elements of the 
 *				parameter member name table.  If an element (containing 
 *				the desired name) is found, it is returned to the caller.
 *
 *  Inputs:
 *      pmnt - pointer to a Parameter Member Name Table structure to 
 *				search for the parameter record member name.
 *     	pt_elem - a pointer to the Parameter Table element
 *				    that contains the parameter information.
 *      name - the item id of the name of the requested parameter
 *
 *  Outputs:
 *     	pmnt_elem - a pointer to the Parameter Member Name Table element
 *				    that contains the parameter name.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_NAME.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
find_pmnt_elem(PARAM_MEM_NAME_TBL *pmnt, PARAM_TBL_ELEM *pt_elem, ITEM_ID name,
	PARAM_MEM_NAME_TBL_ELEM **pmnt_elem)
{

	char					*list_start ;
	int						assert_conds ;
    PARAM_MEM_NAME_TBL_ELEM dummy_elem;

	assert_conds = pmnt && name && pmnt_elem && PMNT_LIST(pmnt) &&
			PMNT_COUNT(pmnt) ;
	ASSERT_RET(assert_conds, DDI_INVALID_PARAM) ;

	/*
	 * Check the Number of Members in the Parameter Record
	 */

	if (PTE_PM_COUNT(pt_elem) <= 0) {
		return(DDI_TAB_BAD_PARAM_TYPE) ;
	}	

	/*
	 * Calculate the beginning of the member name of the requested
	 * parameter
	 */

	list_start = (char *)&(PMNT_LIST(pmnt)[PTE_PMT_OFFSET(pt_elem)]) ; 

	/*
	 *	Find the Parameter Member Name Table element containing the 
	 *	requested parameter record member name
	 */

    /* AR 4626: avoid possible buffer-overread by creating a dummy elem to hold the member name */
    memset(&dummy_elem, 0, sizeof(dummy_elem));
    dummy_elem.param_mem_name = name;

	*pmnt_elem = (PARAM_MEM_NAME_TBL_ELEM *) bsearch(
		(char *)&dummy_elem, list_start, (unsigned int)PTE_PM_COUNT(pt_elem),
		sizeof(PARAM_MEM_NAME_TBL_ELEM), (CMP_FN_PTR)compare_pmnt_elem);

	/*
	 *	Determine if the parameter record member name was found.
	 */

	if (*pmnt_elem == 0) {
		return(DDI_TAB_BAD_NAME);
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  compare_plmnt_elem
 *  ShortDesc:  Compare Item ID's of two items in the Parameter List
 *				Member Name Table.
 *
 *  Description:
 *      The compare_plmnt_elem function takes in two pointers to 
 *		Parameter List Member Name Table element structures in a 
 *		Parameter List Member Name Table structure  and compares 
 *		their Item IDs.  The value returned is reflective of the 
 *		comparison of the two Item IDs.  This function is used in 
 *		conjunction with the bsearch function (for searching the
 *		Block Item Table).
 *
 *  Inputs:
 *      plmnt_elem_1 - pointer to a Parameter List Member Name Table element 
 *					structure in a Parameter List Member Name Table structure.
 *      plmnt_elem_2 - pointer to a Parameter List Member Name Table element 
 *					structure in a Parameter List Member Name Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_plmnt_elem(PARAM_LIST_MEM_NAME_TBL_ELEM *plmnt_elem_1,
	PARAM_LIST_MEM_NAME_TBL_ELEM *plmnt_elem_2)
{

	/*
	 *	Compare the two Item ID's and return the reflective value.
	 */

	if (PLMNTE_PLM_NAME(plmnt_elem_1) <
			PLMNTE_PLM_NAME(plmnt_elem_2)) {
		return(-1);

	} else if (PLMNTE_PLM_NAME(plmnt_elem_1) >
			PLMNTE_PLM_NAME(plmnt_elem_2)) {
		return(1);

	} else {
		return(0);
	}
}


/***********************************************************************
 *
 *  Name:  find_plmnt_elem
 *  ShortDesc:  Find an element in the Parameter List Member Name Table 
 *				using a	variable list member name.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the parameter list member name Table.  The function 
 *				compare_plmnt_elem is used to compare elements of the 
 *				parameter list member name table.  If an element 
 *				(containing the desired name) is found, it is returned 
 *				to the caller.
 *
 *  Inputs:
 *      plmnt - pointer to a Parameter List Member Name Table structure to 
 *				search for the variable list member name.
 *     	plt_elem - a pointer to the Parameter List Table element
 *				    that contains the parameter list information.
 *      name - the item id of the name of the requested parameter list
 *
 *  Outputs:
 *     	plmnt_elem - a pointer to the Parameter List Member Name Table element
 *				    that contains the parameter list name.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_NAME.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
find_plmnt_elem(PARAM_LIST_MEM_NAME_TBL *plmnt, PARAM_LIST_TBL_ELEM *plt_elem,
	ITEM_ID name, PARAM_LIST_MEM_NAME_TBL_ELEM **plmnt_elem)
{

	char					*list_start ;
	int						assert_conds ;
    PARAM_MEM_NAME_TBL_ELEM dummy_elem;

	assert_conds = plmnt && name && plmnt_elem && 
			PLMNT_LIST(plmnt) && PLMNT_COUNT(plmnt) ;
	ASSERT_RET(assert_conds, DDI_INVALID_PARAM) ;

	/*
	 * Calculate the beginning of the list names of the requested
	 * variable list
	 */

	list_start = (char *)&(PMNT_LIST(plmnt)[PLTE_PLMT_OFFSET(plt_elem)]) ; 

	/*
	 *	Find the Parameter Member Name Table element Containing the 
	 *	requested parameter record member name
	 */

    /* AR 4626: avoid possible buffer-overread by creating a dummy elem to hold the member name */
    memset(&dummy_elem, 0, sizeof(dummy_elem));
    dummy_elem.param_mem_name = name;

	*plmnt_elem = (PARAM_LIST_MEM_NAME_TBL_ELEM *) bsearch(
		(char *)&dummy_elem, list_start,
		(unsigned int)PLTE_PLM_COUNT(plt_elem),
		sizeof(PARAM_MEM_NAME_TBL_ELEM), (CMP_FN_PTR)compare_plmnt_elem);

	/*
	 *	Determine if the variable list member name was found.
	 */

	if (*plmnt_elem == 0) {
		return(DDI_TAB_BAD_NAME);
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_resolve_name_to_id_type
 *  ShortDesc: 	Convert a block table element and parameter name  
 *		into the item id of the parameter.
 *
 *  Description:
 *		1.	Find the name in the block item name table
 *		2.	Check the name to be sure that it is a the name of
 *			a parameter, if check_for_param is nonzero.
 *		3.	Get the item table offset of the name from the 
 *			corresponding element of the block item name table.
 *		4.	Get the item id from the corresponding element of the
 *			item table.
 *
 *  Inputs:
 *		block_handle - the block handle of the block containing the
 *				requested parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *		name - The name of a parameter to get convert to an item id 
 *      check_for_param - Verify that the item is a parameter
 *
 *  Outputs:
 *      ddid - the item id of the requested name.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_resolve_name_to_id_type(BLOCK_HANDLE block_handle, BLK_TBL_ELEM *bt_elem,
	ITEM_ID name, ITEM_ID *ddid, ITEM_TYPE *item_type, int check_for_param)
{

	ITEM_TBL						*it ;
	ITEM_TBL_ELEM					*it_elem ;
	BLK_ITEM_NAME_TBL_ELEM			*bint_elem ;
	int								rs ;
	FLAT_DEVICE_DIR					*flat_device_dir ;

	/*
	 * Find the requested name in the Block Item Name Table.
	 */
	rs = find_bint_elem(BTE_BINT(bt_elem), name, &bint_elem) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}
	/*
	 * Make sure the name is the name of a parameter
	 */
    if (check_for_param)
    {
	    rs = CHECK_PARAM_OFFSET(BTE_PT(bt_elem),BINTE_PT_OFFSET(bint_elem)) ;
	    if (rs != DDS_SUCCESS) {
		    rs = CHECK_PARAM_LIST_OFFSET(BTE_PT(bt_elem), BINTE_PLT_OFFSET(bint_elem)) ;
		    if (rs != DDS_SUCCESS) {
			    return(rs) ;
		    }
	    }
    }

	/*
	 * Get the item table pointer, then get the item offset of the name,
	 * and finally get the item id of the name
	 */
	rs = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));

	*ddid = ITE_ITEM_ID(it_elem);
	*item_type = ITE_ITEM_TYPE(it_elem);

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_resolve_char_rec_to_ddid
 *  ShortDesc: 	Convert a block table element into the item id of the
 *				item id of the characteristic record.
 *
 *  Description:
 *		1.	Get the item table offset of the characteristic record
 *			from the corresponding element of the block table.
 *		2.	Get the item id from the corresponding element of the 
 *			item table.
 *
 *  Inputs:
 *		block_handle - the block handle of the block containing the
 *				requested parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *  Outputs:
 *      ddid - the item id of the characteristic record.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_resolve_char_rec_to_ddid(BLOCK_HANDLE block_handle, BLK_TBL_ELEM *bt_elem,
	ITEM_ID *ddid)
{

	ITEM_TBL				*it ;
	ITEM_TBL_ELEM			*it_elem ;
	FLAT_DEVICE_DIR			*flat_device_dir ;
	int						rc ;

	/*
	 * Get the item table pointer, then get the item offset of the
	 * characteristic record and finally get the item id of the
	 * characteristic record
	 */

	rc =  get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it,BTE_CR_IT_OFFSET(bt_elem));
	*ddid = ITE_ITEM_ID(it_elem);
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_resolve_loffset_pname_to_id_type
 *  ShortDesc: 	Convert a block_handle, parameter list offset, and
 *		variable list member name into the item id of the corresponding
 *		parameter.
 *
 *  Description:
 *		1.	Find the parameter list member name in the parameter list
 *			member name table.
 *		2.	Get the parameter list member table offset from the 
 *			corresponding element of the parmeter list member name 
 *			table.
 *		3.	Get the block item name table offset from the corresponding
 *			element of the parameter list member table.
 *		4.	Get the item table offset from the corresponding element of 
 *			the	block item name table.
 *		5.	Get the item id from the corresponding element of the item
 *			table.
 *
 *  Inputs:
 *		block_handle - the block handle of the block containing the
 *				requested parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *		paramlist_offset - The parameter list offset the parameter 
				list to find.
 *		pname - The name of the member of the parameter list
 *  Outputs:
 *		ddid - The ddid of the parameter found
 *		item_type - the type of the item id found
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
tr_resolve_loffset_pname_to_id_type(BLOCK_HANDLE block_handle, BLK_TBL_ELEM *bt_elem,
	int paramlist_offset, ITEM_ID pname, ITEM_ID *ddid, ITEM_TYPE *item_type)
{

	BLK_ITEM_NAME_TBL_ELEM			*bint_elem ;
	PARAM_LIST_MEM_NAME_TBL_ELEM	*plmnt_elem ;
	PARAM_LIST_MEM_TBL_ELEM			*plmt_elem ;
	PARAM_LIST_TBL_ELEM				*plt_elem ;
	ITEM_TBL						*it ;
	ITEM_TBL_ELEM					*it_elem ;
	int								rs ;
	FLAT_DEVICE_DIR					*flat_device_dir;

	/*
	 * Check the Block Item Name Table Element to be sure that it
	 * is referring to a parameter list.
	 */

	rs = CHECK_PARAM_LIST_OFFSET(BTE_PLT(bt_elem),paramlist_offset) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}

	/*
	 * Find the paramter list member in the parameter list member
	 * name table
	 */
	plt_elem = PLTE(BTE_PLT(bt_elem),paramlist_offset) ;
	rs = find_plmnt_elem(BTE_PLMNT(bt_elem),plt_elem,pname,&plmnt_elem) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}

	/*
	 * Get the item id of the parameter
	 */
	plmt_elem = PLMTE(BTE_PLMT(bt_elem),PLMNTE_PLMT_OFFSET(plmnt_elem)) ;
	rs = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}
	it = &flat_device_dir->item_tbl;
	bint_elem = BINTE(BTE_BINT(bt_elem),PLMTE_BINT_OFFSET(plmt_elem)) ;
	it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));

	*ddid = ITE_ITEM_ID(it_elem);
	*item_type = ITE_ITEM_TYPE(it_elem);
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_resolve_lid_pname_to_id_type
 *  ShortDesc: 	Convert a block_handle, parameter list item id, and
 *		variable list member name into the item id of the corresponding
 *		parameter.
 *
 *  Description:
 *		1.	Find the parameter list id in the block item table
 *		2.	Check the name found to be sure that it is a parameter list
 *			name.
 *		3.	call tr_resolve_loffset_pname_to_id_type to get the item id
 *
 *  Inputs:
 *		block_handle - the block handle of the block containing the
 *				requested parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *		lid	- The item id of the parameter list to find.
 *		pname - The name of the member of the parameter list
 *  Outputs:
 *		ddid - The ddid of the parameter found
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_resolve_lid_pname_to_id_type(BLOCK_HANDLE block_handle, BLK_TBL_ELEM *bt_elem,
	ITEM_ID lid, ITEM_ID pname, ITEM_ID *ddid, ITEM_TYPE *item_type)
{

	BLK_ITEM_TBL_ELEM			*bit_elem ;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem ;
	int							rs ;
	int							paramlist_offset ;

	/*
	 * Find the parameter list name in the block item name
	 * table
	 */
	rs = find_bit_elem(BTE_BIT(bt_elem), lid, &bit_elem) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}
	bint_elem = BINTE(BTE_BINT(bt_elem), BITE_BINT_OFFSET(bit_elem)) ;
	paramlist_offset = BINTE_PLT_OFFSET(bint_elem) ;

	/*
	 * Convert the parameter list offset and the member name
	 * into the corresponding item id.
	 */

	rs = tr_resolve_loffset_pname_to_id_type(block_handle, bt_elem,
			paramlist_offset, pname, ddid, item_type) ;

	return(rs);
}


/***********************************************************************
 *
 *  Name: tr_resolve_lname_pname_to_ddid
 *  ShortDesc: 	Convert a block_handle, parameter list name, and
 *		variable list member name into the item id of the corresponding
 *		parameter.
 *
 *  Description:
 *		1.	Find the parameter list id in the block item table
 *		2.	Check the name found to be sure that it is a parameter list
 *			name.
 *		3.	call tr_resolve_loffset_pname_to_id_type to get the item id
 *
 *  Inputs:
 *		block_handle - the block handle of the block containing the
 *				requested parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *		lname - The name of the parameter list to find.
 *		pname - The name of the member of the parameter list
 *  Outputs:
 *		ddid - The ddid of the parameter found
 *		item_type - The type of the item
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_resolve_lname_pname_to_ddid(BLOCK_HANDLE block_handle, BLK_TBL_ELEM *bt_elem,
	ITEM_ID lname, ITEM_ID pname, ITEM_ID *ddid)
{

	int								rs ;
	int								paramlist_offset ;
	BLK_ITEM_NAME_TBL_ELEM			*bint_elem ;
	ITEM_TYPE						item_type;

	/*
	 * Find the parameter list name in the block item name
	 * table
	 */
	rs = find_bint_elem(BTE_BINT(bt_elem), lname, &bint_elem) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}
	paramlist_offset = BINTE_PLT_OFFSET(bint_elem) ;

	/*
	 * Convert the parameter list offset and the member name
	 * into the corresponding item id.
	 */

	rs = tr_resolve_loffset_pname_to_id_type(block_handle, bt_elem,
			paramlist_offset, pname, ddid, &item_type) ;
	return(rs);
}


/***********************************************************************
 *
 *  Name: tr_resolve_ddid_mem_to_si
 *  ShortDesc: 	Convert a block_handle, item id, record member name 
 *		into the subindex of the record member.
 *
 *  Description:
 *		1.	Find the item id in the block item table
 *		2.	Determine if the requested item is a parameter list,
 *			a parameter, or neither (i.e. the characteristic record).	
 *		3.	Find the requested member name in either the characteristic
 *			member name table or the parameter member name table.
 *		4.	Using the corresponding element (of either the characteristic
 *			member name table or the parameter member name table) get
 *			the subindex.
 *
 *  Inputs:
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *      item_id - The item_id the item to find the subindex of 
 *      mem_name - The record member name to find 
 *  Outputs:
 *		subindex - the subindex of the requested record member.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_resolve_ddid_mem_to_si(BLK_TBL_ELEM *bt_elem, ITEM_ID item_id, ITEM_ID mem_name,
	SUBINDEX *subindex)
{

	int						rs;
	int						rs_param;
	int						rs_paramlist;
	int                     table_offset;
	BLK_ITEM_TBL_ELEM		*bit_elem;
	BLK_ITEM_NAME_TBL_ELEM	*bint_elem;
	CHAR_MEM_NAME_TBL_ELEM	*cmnt_elem ;
	PARAM_TBL_ELEM			*pt_elem ;
	PARAM_MEM_NAME_TBL_ELEM	*pmnt_elem ;

	/*
	 * Find the requested item id in the block item table
	 */
	rs = find_bit_elem(BTE_BIT(bt_elem), item_id, &bit_elem);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	/*
	 * Determine if the requested item is a parameter (that's a record)
	 * or the characteristics record. Otherwise, issue an error.
	 */
	bint_elem = BINTE(BTE_BINT(bt_elem), BITE_BINT_OFFSET(bit_elem)) ;
	rs_param = CHECK_PARAM_OFFSET(BTE_PT(bt_elem), BINTE_PT_OFFSET(bint_elem)) ;
	rs_paramlist = CHECK_PARAM_LIST_OFFSET(BTE_PLT(bt_elem),
			BINTE_PLT_OFFSET(bint_elem)) ;
	if (rs_param != DDS_SUCCESS && rs_paramlist != DDS_SUCCESS) {
		/*
		 * It's the characteristics record so find the member name
		 * in the characteristic member name table
		 */
		rs = find_cmnt_elem(BTE_CMNT(bt_elem), mem_name, &cmnt_elem) ;
		if (rs != DDS_SUCCESS) {
			return(rs) ;
		}

		table_offset = CMNTE_CM_OFFSET(cmnt_elem) ;

		/*
		 * The name of the table member record.subindex will soon be changed
		 * to record.table_offset, then we convert record.offset to a record
		 * subindex by calling the following macro
		 */

		*subindex = (SUBINDEX) CHAR_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset);

	} else if (rs_param == DDS_SUCCESS) {
		/*
		 * It's a parameter record so find the member name
		 * in the parameter member name table
		 */
		pt_elem = PTE(BTE_PT(bt_elem), BINTE_PT_OFFSET(bint_elem)) ;
		rs = find_pmnt_elem(BTE_PMNT(bt_elem), pt_elem, mem_name, &pmnt_elem) ;
		if (rs != DDS_SUCCESS) {
			return(rs) ;
		}

		table_offset = PMNTE_PM_OFFSET(pmnt_elem) ;

		/*
		 * The name of the table member record.subindex will soon be changed
		 * to record.table_offset, then we convert record.offset to a record
		 * subindex by calling the following macro
		 */

		*subindex = (SUBINDEX) PARAM_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset);

	} else {
		return(DDI_TAB_BAD_PARAM_TYPE) ;
	}
	return(DDS_SUCCESS);
}


#ifdef DEBUG
/***********************************************************************
 *
 *  Name: chk_typ_siz_cls
 *  ShortDesc: 	Check the type, size, and class 
 *
 *  Description:
 *				Check the type, size, and class elements of a table
 *				element.  The Entries may also be -1 which is valid
 *				indication of no type, size, and class.
 *
 *  Inputs:
 *  type	- The type of a variable
 *  size	- The size of a variable
 *  class	- The class of a variable
 *
 *  Outputs:
 *	none
 *
 *  Returns:
 *      TRUE (if the type, size, and class are good) or FALSE
 *		(if either type, size, or classs is bad).
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
chk_typ_siz_cls(int type, int size, ulong class)
{

	int			good_type ;		/* local bool variable */
	int			good_size ;		/* local bool variable */
	int			good_class ;	/* local bool variable */

	/*
	 * Check the type
	 */

	good_type = (type == -1) || 
			((type >= DDS_INTEGER) && (type < DDS_MAX_TYPE)) ;
	good_size = (size == -1) || (size > 0) ;
	good_class =  (class == DIAGNOSTIC_CLASS) ||
			(class & DYNAMIC_CLASS) ||
			(class & SERVICE_CLASS) ||
			(class & CORRECTION_CLASS) ||
			(class & COMPUTATION_CLASS) ||
			(class & ANALOG_OUTPUT_CLASS) ||
			(class & LOCAL_DISPLAY_CLASS) ||
			(class & FREQUENCY_CLASS) ||
			(class & DISCRETE_CLASS) ||
			(class & DEVICE_CLASS) ||
			(class & LOCAL_CLASS) ||
			(class & INPUT_CLASS) ||
			(class & OUTPUT_CLASS) ||
			(class & CONTAINED_CLASS) ||
			(class & OPERATE_CLASS) ||
			(class & ALARM_CLASS) ||
			(class & TUNE_CLASS) ; 

	return (good_type && good_size && good_class);
}


/***********************************************************************
 *
 *  Name: check_it
 *  ShortDesc: 	Check referential internal consistency 
 *
 *  Description:
 *				Check the item table offset (for each element in
 *				the table) is with in the bounds of the item table.
 *
 *  Inputs:
 *  it		- The item table
 *
 *  Outputs:
 *	none
 *
 *  Returns:
 *      TRUE (if the references in the table are good) or FALSE
 *	(if there is one bad reference in the table).
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_it(ITEM_TBL *it)
{

	ITEM_TBL_ELEM	*it_elem ;	/* Item Table Element */
	int				i ;			/* Local Loop Counter */
	ITEM_TYPE		itype ;		/* The type of an item */
	int				good_itype ;/* Local boolean value */

	/*
	 * Check each Item Table Element for Internal Consistency
	 */

	for (i = 0 ; i < IT_COUNT(it) ; i ++) {

		it_elem = ITE(it, i) ;
		itype = ITE_ITEM_TYPE(it_elem) ;
		good_itype = (itype > RESERVED_ITYPE1) && (itype < MAX_ITYPE) ;

		ASSERT_DBG(good_itype) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_bt
 *  ShortDesc: 	Check referential integrity of the the Block Table
 *
 *  Description:
 *				Check the item table offset (for each element in
 *				the table) is with in the bounds of the item table.
 *				Check the item table offset of the characteristic
 *				record (for each element in the item table) is
 *				within the bounds of the item table.
 *
 *  Inputs:
 *  bt		- The block table
 *	it_cnt 	- The number of elements in the item table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_bt(BLK_TBL *bt, int it_cnt)
{

	BLK_TBL_ELEM	*bt_elem ; 	/* Block Table Element */
	int				i ;			/* Local Loop Counter */
	int				it_ref ;	/* Item Table Reference (Offset) */
	int				good_it_ref ;/* Local bool value */

	/*
	 * Check each Characteristic Member Table Element for
	 * Integrity with the item table
	 */

	for (i = 0 ; i < BT_COUNT(bt) ; i ++) {

		bt_elem = BTE(bt, i) ;
		it_ref = BTE_IT_OFFSET(bt_elem) ;
		good_it_ref = it_ref >= 0 && it_ref < it_cnt ; 


		ASSERT_DBG(good_it_ref) ;

	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_cmt
 *  ShortDesc: 	Check referential integrity of the the Characteristic 
 *		Member Table
 *
 *  Description:
 *				Check the item table offset (for each element in
 *				the table) is with in the bounds of the item table.
 *
 *  Inputs:
 *  cmt		- The characteristic member table
 *	it_cnt 	- The number of elements in the item table
 *	rt_cnt 	- The number of elements in the relation table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_cmt(CHAR_MEM_TBL *cmt, int it_cnt, int rt_cnt)
{

	CHAR_MEM_TBL_ELEM	*cmt_elem ;	/* Char member table element */
	int					i ;			/* Local Loop Counter */
	int					it_ref ;	/* Item Table Reference (Offset) */
	int					rt_ref ;	/* Rel Table Reference (Offset) */
	int					good_it_ref ; /* Local bool value */
	int					good_rt_ref ; /* Local bool value */
	int					good_typ_siz_cls ; /* Local bool value */

	/*
	 * Check each Characteristic Member Table Element for
	 * Integrity with the item table
	 */

	for (i = 0 ; i < CMT_COUNT(cmt) ; i ++) {

		cmt_elem = CMTE(cmt, i) ;

		it_ref = CMTE_IT_OFFSET(cmt_elem) ;
		rt_ref = CMTE_RT_OFFSET(cmt_elem) ;
		good_typ_siz_cls = chk_typ_siz_cls(CMTE_CM_TYPE(cmt_elem),
				CMTE_CM_SIZE(cmt_elem), CMTE_CM_CLASS(cmt_elem)) ;
		good_it_ref = (it_ref >= 0) && (it_ref < it_cnt) ;
		good_rt_ref = (rt_ref >= DEFAULT_OFFSET) && (rt_ref < rt_cnt) ;

		ASSERT_DBG(good_it_ref && good_rt_ref && good_typ_siz_cls) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_cmnt
 *  ShortDesc: 	Check referential integrity of the the Characteristic 
 *				Member Name Table
 *
 *  Description:
 *				Check the characteristic record subindex (for each 
 *				element in the table) is with in the bounds of the 
 *				characteristic member table.
 *
 *  Inputs:
 *  cmnt	- The characteristic member name table
 */
/*
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_cmnt(CHAR_MEM_NAME_TBL *cmnt, int cmt_cnt)
{

	CHAR_MEM_NAME_TBL_ELEM	*cmnt_elem ;/* Char mem name tbl element */
	int						i ;			/* Local Loop Counter */
	int						ref ;		/* Internal Tbl Ref (Offset) */
	int						good_ref ; 	/* Local bool value */

	/*
	 * Check each Characteristic Member Name Table Element for
	 * Integrity with the characteristic member table
	 */

	for (i = 0 ; i < CMT_COUNT(cmnt) ; i ++) {

		cmnt_elem = CMNTE(cmnt,i) ;
		ref = CMNTE_CM_OFFSET(cmnt_elem) ;
		good_ref = ref >= 0 && ref < cmt_cnt ;

		ASSERT_DBG(good_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_pt
 *  ShortDesc: 	Check referential integrity of the the Parameter Table
 *
 *  Description:
 *				Check the item table offset of the array elment (for 
 *				each element in the table) is with in the bounds of 
 *				the item table.
 *				Check the block item name table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the block item name table.
 *				Check the parameter member table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the parameter member table.
 *
 *  Inputs:
 *  pt		- The parameter table
 *	it_cnt 	- The number of elements in the item table
 *  pmt_cnt - The number of elements in the parameter member table
 *  pmt_cnt - The number of elements in the block item name tbl
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_pt(PARAM_TBL *pt, int it_cnt, int pmt_cnt, int bint_cnt)
{

	PARAM_TBL_ELEM		*pt_elem ;	/* Paremeter table element */
	int					i ;			/* Local Loop Counter */
	int					bint_ref ;	/* Blk Item Name Tbl Ref (Offset) */
	int					ae_it_ref ;	/* Array Item Tbl Ref (Offset) */
	int					pmt_ref ;	/* Param Member Tbl Ref (Offset) */
	int					good_ae_it_ref ; /* Local bool value */
	int					good_bint_ref ; /* Local bool value */
	int					good_pmt_ref ; /* Local bool value */
	int					good_typ_siz_cls ; /* Local bool value */

	/*
	 * Check each Parameter Table Element for Integrity with the item
	 * table and integrity with the Parameter Member Table
	 */

	for (i = 0 ; i < PT_COUNT(pt) ; i ++) {

		pt_elem = PTE(pt, i) ;

		bint_ref = PTE_BINT_OFFSET(pt_elem) ;
		good_bint_ref = bint_ref >= 0 && bint_ref < bint_cnt ;

		ae_it_ref = PTE_AE_IT_OFFSET(pt_elem) ;
		good_ae_it_ref = (ae_it_ref >= DEFAULT_OFFSET) && 
				(ae_it_ref < it_cnt) ;

		pmt_ref = PTE_PMT_OFFSET(pt_elem) ;
		good_pmt_ref = pmt_ref >= DEFAULT_OFFSET && 
				(pmt_ref + PTE_PM_COUNT(pt_elem)-1) < pmt_cnt ;

	
		if (PTE_AE_COUNT(pt_elem) > 0) {
			good_typ_siz_cls = chk_typ_siz_cls(PTE_AE_VAR_TYPE(pt_elem),
					PTE_AE_VAR_SIZE(pt_elem),PTE_AE_VAR_CLASS(pt_elem)) ;

		} else {
			good_typ_siz_cls = TRUE ;
		}

		ASSERT_DBG(good_bint_ref && good_ae_it_ref) ;
		ASSERT_DBG(good_pmt_ref && good_typ_siz_cls) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_plt
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				List Table
 *
 *  Description:
 *				Check the block item name table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the block item  name table.
 *				Check the parameter list member table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the parameter list member table.
 *
 *  Inputs:
 *  plt		- The Parameter List table
 *	bint_cnt - The number of elements in the block item name table
 *	plmt_cnt - The number of elements in the Parameter List member tbl
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_plt(PARAM_LIST_TBL *plt, int bint_cnt, int plmt_cnt)
{

	PARAM_LIST_TBL_ELEM	*plt_elem ;	/* Param List table element */
	int					i ;			/* Local Loop Counter */
	int					bint_ref ;	/* Blk Item Name Tbl Ref (Offset) */
	int					plmt_ref ;	/* Param List Mem Tbl Ref */
	int					good_bint_ref ; /* Local bool value */
	int					good_plmt_ref ; /* Local bool value */

	/*
	 * Check each Parameter List Table Element for Integrity with the block
	 * item name table and integrity with the Parameter List Member Table
	 */

	for (i = 0 ; i < PLT_COUNT(plt) ; i ++) {

		plt_elem = PLTE(plt, i) ;

		bint_ref = PLTE_BINT_OFFSET(plt_elem) ;
		good_bint_ref = bint_ref >= 0 && bint_ref < bint_cnt ;

		plmt_ref = PLTE_PLMT_OFFSET(plt_elem) ;
		good_plmt_ref = plmt_ref >= 0 && 
				(plmt_ref+PLTE_PLM_COUNT(plt_elem)-1) < plmt_cnt ;

		ASSERT_DBG(good_bint_ref && good_plmt_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_pmt
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				Member Table
 *
 *  Description:
 *				Check the item table offset (for each element in the
 *				table) is within the bounds of the item table.
 *
 *  Inputs:
 *  pmt		- The Parameter Member table
 *	it_cnt 	- The number of elements in the item table
 *	rt_cnt 	- The number of elements in the relation table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_pmt(PARAM_MEM_TBL *pmt, int it_cnt, int rt_cnt)
{

	PARAM_MEM_TBL_ELEM	*pmt_elem ;	/* Param Mem Table Element */
	int					i ;			/* Local Loop Counter */
	int					it_ref ;	/* Item Tbl Ref (Offset) */
	int					rt_ref ;	/* Rel Tbl Ref (Offset) */
	int					good_it_ref ; /* Local bool value */
	int					good_rt_ref ; /* Local bool value */
	int					good_typ_siz_cls ; /* Local bool value */

	/*
	 * Check each Parameter Member Table Element for
	 * Integrity with the item table
	 */

	for (i = 0 ; i < PMT_COUNT(pmt) ; i ++) {

		pmt_elem = PMTE(pmt, i) ;

		it_ref = PMTE_IT_OFFSET(pmt_elem) ;
		rt_ref = PMTE_RT_OFFSET(pmt_elem) ;
		good_it_ref = it_ref >= 0 && it_ref < it_cnt ;
		good_rt_ref = rt_ref >= DEFAULT_OFFSET && rt_ref < rt_cnt ;
		good_typ_siz_cls = chk_typ_siz_cls(PMTE_PM_TYPE(pmt_elem),
				PMTE_PM_SIZE(pmt_elem),PMTE_PM_CLASS(pmt_elem)) ;

		ASSERT_DBG(good_it_ref && good_rt_ref && good_typ_siz_cls) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_pmnt
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				Member Name Table
 *
 *  Description:
 *				Check the parameter member subindex (for each element in the
 *				table) is within the bounds of the parameter member table.
 *
 *  Inputs:
 *  pmnt		- The Parameter Member Name table
 *	pmt_cnt		- The number of elements in the Parameter Member table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_pmnt(PARAM_MEM_NAME_TBL *pmnt, int pmt_cnt)
{

	PARAM_MEM_NAME_TBL_ELEM	*pmnt_elem ;	/* Param Mem Name table element */
	int					i ;				/* Local Loop Counter */
	int					pmt_ref ;		/* Parameter Member Tbl Ref (Offset) */
	int					good_pmt_ref ; 	/* Local bool value */

	/*
	 * Check each Parameter Member Name Table Element for
	 * Integrity with the Parameter Member  table 
	 */

	for (i = 0 ; i < PMNT_COUNT(pmnt) ; i ++) {

		pmnt_elem = PMNTE(pmnt, i) ;
		pmt_ref = PMNTE_PM_OFFSET(pmnt_elem) ;
		good_pmt_ref = pmt_ref >= 0 && pmt_ref < pmt_cnt ;

		ASSERT_DBG(good_pmt_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_plmt
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				List Member Table
 *
 *  Description:
 *				Check the block item name table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the block item name table.
 *
 *  Inputs:
 *  plmt	- The Parameter List Member table
 *	bint_cnt - The number of elements in the block item name table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_plmt(PARAM_LIST_MEM_TBL *plmt, int bint_cnt)
{

	PARAM_LIST_MEM_TBL_ELEM	*plmt_elem ; /* Param List Mem table element */
	int					i ;				/* Local Loop Counter */
	int					bint_ref ;		/* Blk Item Name Tbl Ref (Offset) */
	int					good_bint_ref ; /* Local bool value */

	/*
	 * Check each Parameter List Member Table Element for
	 * Integrity with the block item name table
	 */

	for (i = 0 ; i < PLMT_COUNT(plmt) ; i ++) {

		plmt_elem = PLMTE(plmt, i) ;
		bint_ref = PLMTE_BINT_OFFSET(plmt_elem) ;
		good_bint_ref = bint_ref >= 0 && bint_ref < bint_cnt ;

		ASSERT_DBG(good_bint_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_plmnt
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				List Member Name Table
 *
 *  Description:
 *				Check the parameter list member table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the parameter list member table.
 *
 *  Inputs:
 *  plmnt		- The Parameter List Member Name table
 *	plmt_cnt	- The number of elements in the Parameter List Member table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_plmnt(PARAM_LIST_MEM_NAME_TBL *plmnt, int plmt_cnt)
{

	PARAM_LIST_MEM_NAME_TBL_ELEM	*plmnt_elem ;	/* Param List Mem Name table element */
	int					i ;				/* Local Loop Counter */
	int					plmt_ref ;		/* Param List Mem Tbl Ref */
	int					good_plmt_ref ; /* Local bool value */

	/*
	 * Check each Parameter List Table Element for
	 * Integrity with the the Parameter List Member Table
	 */

	for (i = 0 ; i < PLMNT_COUNT(plmnt) ; i ++) {

		plmnt_elem = PLMNTE(plmnt, i) ;
		plmt_ref = PLMNTE_PLMT_OFFSET(plmnt_elem) ;
		good_plmt_ref = plmt_ref >= 0 && plmt_ref < plmt_cnt ;

		ASSERT_DBG(good_plmt_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_bit
 *  ShortDesc: 	Check referential integrity of the the Block 
 *				Item Table
 *
 *  Description:
 *				Check the block item name table offset (for each element 
 *				in the table) is with in the bounds of the block item name
 *				table.
 *
 *  Inputs:
 *  bit		- The Block Item table
 *	bint_cnt - The number of elements in the Block item name table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_bit(BLK_ITEM_TBL *bit, int bint_cnt)
{

	BLK_ITEM_TBL_ELEM	*bit_elem ;	/* Block Item table element */
	int					i ;			/* Local Loop Counter */
	int					bint_ref ;	/* Blk Item Name Tbl Ref (Offset) */
	int					good_bint_ref ; /* Local bool value */

	/*
	 * Check each Parameter List Table Element for
	 * Integrity with the block item name table 
	 */

	for (i = 0 ; i < BIT_COUNT(bit) ; i ++) {

		bit_elem = BITE(bit, i) ;
		bint_ref = BITE_BINT_OFFSET(bit_elem) ;
		good_bint_ref = bint_ref >= 0 && bint_ref < bint_cnt ;

		ASSERT_DBG(good_bint_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_bint
 *  ShortDesc: 	Check referential integrity of the the Block Item 
 *				Name Table
 *
 *  Description:
 *				Check the item table offset (for each element in the
 *				table) is with in the bounds of the item table.
 *				Check the item table offset of the unit (for 
 *				each element in the table) is with in the bounds of 
 *				the item table.
 *				Check the item table offset of the wao (for 
 *				each element in the table) is with in the bounds of 
 *				the item table.
 *				Check the parameter table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the parameter table.
 *				Check the parameter list table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the parameter list table.
 *				Check the Relation table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the relation table.
 *
 *  Inputs:
 *  bint	- The Block Item Name table
 *	it_cnt 	- The number of elements in the item table
 *	pt_cnt - The number of elements in the Parameter tbl
 *	plt_cnt - The number of elements in the Parameter list tbl
 *	rt_cnt - The number of elements in the Relation tbl
 *	ct_cnt - The number of elements in the Command table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_bint(BLK_ITEM_NAME_TBL *bint, int it_cnt, int rt_cnt, int pt_cnt, int plt_cnt)
{

	BLK_ITEM_NAME_TBL_ELEM	*bint_elem ; /* Block Item Name table element */
	int					i ;			/* Local Loop Counter */
	int					it_ref ;	/* Item Tbl Ref (Offset) */
	int					pt_ref ;	/* Param Tbl Ref */
	int					plt_ref ;	/* Param List Tbl Ref */
	int					rt_ref ;	/* Relation Tbl Ref */

	int					good_it_ref ; /* Local bool value */
	int					good_pt_ref ; /* Local bool value */
	int					good_plt_ref ; /* Local bool value */
	int					good_rt_ref ; /* Local bool value */


	/*
	 * Check each Block Item Name Table Element for Integrity with the item
	 * table, parameter table, parameter list table, and the relation table.
	 */

	for (i = 0 ; i < BINT_COUNT(bint) ; i ++) {

		bint_elem = BINTE(bint, i) ;

		it_ref = BINTE_IT_OFFSET(bint_elem) ;
		good_it_ref = it_ref >= 0 && it_ref < it_cnt ;

		rt_ref = BINTE_RT_OFFSET(bint_elem) ;
		good_rt_ref = (rt_ref >= DEFAULT_OFFSET) && 
				(rt_ref < rt_cnt) ;

		pt_ref = BINTE_PT_OFFSET(bint_elem) ;
		good_pt_ref = (pt_ref >= DEFAULT_OFFSET) && 
				(pt_ref < pt_cnt) ;

		plt_ref = BINTE_PLT_OFFSET(bint_elem) ;
		good_plt_ref = (plt_ref >= DEFAULT_OFFSET) && 
				(plt_ref < plt_cnt) ;


		ASSERT_DBG(good_rt_ref && good_it_ref) ;
		ASSERT_DBG(good_pt_ref && good_plt_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_rt
 *  ShortDesc: 	Check referential integrity of the the Relation Table
 *
 *  Description:
 *				Check the item table offset (for each element in the
 *				table) is with in the bounds of the item table.
 *
 *  Inputs:
 *  rt		- The Relation table
 *	it_cnt 	- The number of elements in the item table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_rt(REL_TBL *rt, int it_cnt, int ut_cnt)
{

	REL_TBL_ELEM		*rt_elem ;	/* Relation table element */
	int					i ;			/* Local Loop Counter */
	int					wao_it_ref ;/* WAO item Tbl Ref (Offset) */
	int					unit_it_ref ;/* UNIT item Tbl Ref (Offset) */
	int					ut_ref ;	/* Update Tbl Ref (Offset) */
	int					u_count ;	/* Update Count */
	int					good_wao_it_ref ; /* Local bool value */
	int					good_unit_it_ref ; /* Local bool value */
	int					good_ut_ref ; /* Local bool value */
	int					good_u_count ; /* Local bool value */

	/*
	 * Check each Relation Table Element for Integrity with the item table
	 */

	for (i = 0 ; i < RT_COUNT(rt) ; i ++) {
		rt_elem = RTE(rt, i) ;
		wao_it_ref = RTE_WAO_IT_OFFSET(rt_elem) ;
		unit_it_ref = RTE_UNIT_IT_OFFSET(rt_elem) ;
		ut_ref = RTE_UT_OFFSET(rt_elem) ;
		u_count = RTE_U_COUNT(rt_elem) ;
		good_wao_it_ref =
			wao_it_ref >= DEFAULT_OFFSET && wao_it_ref < it_cnt ;
		good_unit_it_ref =
			unit_it_ref >= DEFAULT_OFFSET && unit_it_ref < it_cnt ;
		good_ut_ref = ut_ref >= DEFAULT_OFFSET && ut_ref < ut_cnt ;
		good_u_count = u_count >= 0 && u_count <= ut_cnt ;
		ASSERT_DBG(good_wao_it_ref && good_unit_it_ref && good_ut_ref && good_u_count) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_ut
 *  ShortDesc: 	Check referential integrity of the the Update Table
 *
 *  Description:
 *				Check the item table offset (for each element in the
 *				table) is with in the bounds of the item table.
 *
 *  Inputs:
 *  ut		- The Update table
 *	it_cnt 	- The number of elements in the item table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_ut(UPDATE_TBL *ut, int it_cnt)
{

	UPDATE_TBL_ELEM		*ut_elem ;			/* Update table element */
	int					i ;					/* Local Loop Counter */
	int					desc_it_ref ; 		/* item table offset */
	int					op_it_ref ; 		/* item table offset */
	int					good_desc_it_ref ; 	/* Local bool value */
	int					good_op_it_ref ; 	/* Local bool value */

	/*
	 * Check each Relation Table Element for Integrity with the item table
	 */

	for (i = 0 ; i < UT_COUNT(ut) ; i ++) {
		ut_elem = UTE(ut, i) ;
		desc_it_ref = UTE_DESC_IT_OFFSET(ut_elem) ;
		op_it_ref = UTE_OP_IT_OFFSET(ut_elem) ;
		good_desc_it_ref = desc_it_ref < it_cnt ;
		good_op_it_ref = op_it_ref < it_cnt ;
		ASSERT_DBG(good_desc_it_ref && good_op_it_ref) ;
	}
	return ;
}


/***********************************************************************
 *
 *  Name: check_pet
 *  ShortDesc: 	Check referential integrity of the the Parameter 
 *				Element Table
 *
 *  Description:
 *				Check the relation table offset (for 
 *				each element in the table) is with in the bounds of 
 *				the relation table.
 *
 *  Inputs:
 *  pet		- The parameter element table
 *	rt_cnt 	- The number of elements in the relation table
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		none
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
check_pet(PARAM_ELEM_TBL pet, int rt_cnt)
{

	PARAM_ELEM_TBL_ELEM	*pet_elem ;	/* Param Element table element */
	int					i ;			/* Local Loop Counter */
	int					rt_ref ; 	/* Local relation table reference */
	int					good_rt_ref ; /* Local boolean */

	/*
	 * Check each Relation Table Element for Integrity with the item table
	 */

	for (i = 0 ; i < PET_COUNT(pet) ; i ++) {
		pet_elem = PETE(&pet,i) ;
		rt_ref = PETE_RT_OFFSET(pet_elem) ;
		good_rt_ref = rt_ref >= DEFAULT_OFFSET && rt_ref < rt_cnt ;
		ASSERT_DBG(good_rt_ref) ;
	}
	return ;
}
#endif


/***********************************************************************
 *
 *  Name:  find_bt_elem
 *  ShortDesc:  Find an element in the Block Table using a block name.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the Block Table.  The function compare_bt_elem
 *				is used to compare elements of the Block Table.
 *				If an element (containing the desired block name) is
 *				found, it is returned to the caller.
 *
 *  Inputs:
 *      bt - pointer to a Block Table structure to search for the
 *			 block name.
 *      block_name - pointer to a character string that is the
 *					 block name.
 *
 *  Outputs:
 *      bt_elem - a pointer to the Block Table element that
 *				  contains the requested block name.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_NO_BLOCKNAME.
 *
 *  Author:
 *		Chris Gustafson
 *
 **********************************************************************/

static int
find_bt_elem(BLK_TBL *bt, BLOCK_HANDLE block_handle, BLK_TBL_ELEM **bt_elem)
{

	int		bt_offset;
	int		rc ;

	TEST_FAIL(FIND_BT_ELEM);

	rc = get_abt_dd_blk_tbl_offset(block_handle, &bt_offset);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	if (bt_offset != -1) {

		*bt_elem = BTE(bt, bt_offset);

	} else {

		return DDI_TAB_NO_BLOCK;
	}
	
	return(DDS_SUCCESS);
}


/*********************************************************************
 *
 *	Name: block_handle_to_bt_elem
 *	ShortDesc: get the bt_elem
 *
 *	Description:
 *		block_handle_to_bt_elem takes a block_handle
 *		and returns a pointer to Block Table element pointer
 *
 *	Inputs:
 *		block_handle:	the block_handle
 *
 *	Outputs:
 *		bt_elem:	a pointer to a bt_elem pointer	
 *
 *	Returns: returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
block_handle_to_bt_elem(BLOCK_HANDLE block_handle, BLK_TBL_ELEM **bt_elem)
{

	BLK_TBL				*bt;
	int					rc;
	FLAT_DEVICE_DIR		*flat_device_dir ;

	TEST_FAIL(DDI_BLOCK_HANDLE_TO_BT_ELEM);

	/*
	 * Check the block handle
	 */

	if (!valid_block_handle(block_handle)) {
		return DDI_INVALID_BLOCK_HANDLE;
	}

	/*
	 * Find the bt elem that corresponds to the block handle
	 */

	rc = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	bt = &flat_device_dir->blk_tbl;
	rc = find_bt_elem(bt, block_handle, bt_elem);
	return rc;
}


/***********************************************************************
 *
 *  Name: tr_characteristics_to_ite
 *  ShortDesc:  Convert a block_handle and offset into the
 *				characteristics	record into an item table element
 *				pointer.
 *
 *  Description:
 *				1.	Find the block type (bt_elem) of the 
 *					specified block_handle.
 *				2.	Check the characteristic list offset passed in using
 *					the Characteristic Member Table pointer (from
 *					bt_elem).
 *				3.	Form a Characteristic Member Table element using
 *					Characteristic Member Table pointer and the
 *					characteristic offset.
 *				4.	Form a Block Item Name Table pointer using the Block
 *					Item Name Table pointer (from bt_elem) and the
 *					Block Item Name Table offset (from plt_elem).
 *				5.	Form an Item Table element using the Item Table
 *					pointer and the Item Table offset (from
 *					bint_elem).
 *				6.	Get the dd reference of the specified characteristic
 *					from the Item Table using the Item Table element
 *					pointer.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *      char_offset - The offset into the characteristis record for the
 *					  requested dd reference.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_characteristics_to_ite(ITEM_TBL *it, BLK_TBL_ELEM *bt_elem,
	SUBINDEX subindex, ITEM_TBL_ELEM **it_elem)
{

	CHAR_MEM_TBL_ELEM	*cmt_elem;
	int					char_offset;
	int					rs;

	/*
	 *	Convert subindex to a subindex-table offset
	 */

	char_offset = CHAR_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex);

	/*
	 *	Check the Characteristic member offset to see if it is in range.
	 */

	rs = CHECK_CHAR_MEMBER_OFFSET(BTE_CMT(bt_elem),char_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Get the Item Table location from the Characteristic Member 
	 *	Table offset and Block Table element.
	 */

	cmt_elem = CMTE(BTE_CMT(bt_elem),char_offset);
	*it_elem = ITE(it,CMTE_IT_OFFSET(cmt_elem));

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_param_si_to_ite
 *  ShortDesc:  Convert a block_handle, an offset into the list of
 *				parameters, and record subindex into the item table
 *				element pointer of the corresponding variable.
 *
 *  Description:
 *				1.	Find the block type (bt_elem) of the 
 *					specified block_handle.
 *				2.	check the Parameter Table offset using the Parameter
 *					Table pointer (from bt_elem) and the parameter
 *					offset (param_offset).
 *				3.	Form a Parameter Table element pointer using the
 *					Parameter Table pointer (from bt_elem) and the
 *					Parameter Table offset.
 *				4.	Form a Parameter Member Table element pointer using
 *					the Parameter Member Table pointer (from
 *					bt_elem) and the Parameter Member Table offset
 *					(from pt_elem) and subindex.
 *				5.	Form an Item Table element using the Item Table
 *					pointer and the Item Table offset (from pmt_elem).
 *				6.	Get the dd reference of the specified name from
 *					the Item Table using the Item Table element pointer.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *      offset - The offset into the list of parameters for the
 *				 requested block.
 *		subindex - The offset into the list of member for the record
 *				   parameter requested.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *      DDI_TAB_NO_DEVICE.
 *      DDI_TAB_NOT_A_RECORD.
 *      DDI_TAB_SUBINDEX_TOO_LARGE.
 *      DDI_TAB_OFFSET_TOO_LARGE.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_param_si_to_ite(ITEM_TBL *it, BLK_TBL_ELEM *bt_elem, int param_offset,
	SUBINDEX subindex, ITEM_TBL_ELEM **it_elem)
{

	PARAM_TBL_ELEM				*pt_elem;
	PARAM_MEM_TBL_ELEM			*pmt_elem;
	int							param_mem_tbl_offset; /* table offset for the subindex */
	int							rs;

	/*
	 *	Check the parameter offset to see if it is in range.
	 */

	rs = CHECK_PARAM_OFFSET(BTE_PT(bt_elem), param_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Convert subindex to a subindex-table offset
	 */

	param_mem_tbl_offset = PARAM_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex);

	/*
	 *	Check the offset to see if it is in range.
	 */

	pt_elem = PTE(BTE_PT(bt_elem),param_offset);
	rs = CHECK_PARAM_MEMBER_OFFSET(pt_elem, param_mem_tbl_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Calculate the Item Table offset and get the dd reference.
	 */

	if (PTE_AE_COUNT(pt_elem) > 0) {
		*it_elem = ITE(it, PTE_AE_IT_OFFSET(pt_elem));

	} else {
		pmt_elem = PMTE(BTE_PMT(bt_elem),
				(PTE_PMT_OFFSET(pt_elem) + param_mem_tbl_offset));
		*it_elem = ITE(it, PMTE_IT_OFFSET(pmt_elem));
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_id_si_to_ite
 *  ShortDesc: Convert a block_handle, item id, and subindex into the
 *				item table element pointer of the corresponding variable.
 *
 *  Description:
 *				1.	Find the block type (bt_elem) of the 
 *					specified block_handle.
 *				2.	Find the item id in the Block Item Table. 
 *				3.	Form a Block Item Name Table element pointer using
 *					the Block Item Name Table pointer (from bt_elem)
 *					and the Block Item Name Table offset (from
 *					bit_elem).
 *				4.	Form a param_offset and a param_list_offset from the
 *					Block Item Name Table element pointer.  Check the
 *					param_offset and the param_list_offset.  Use the
 *					subindex as a record member if the param_offset is
 *					valid, and use the subindex	as parameter list offset
 *					if the param_list_offset is valid.
 *				5.	Form an Item Table element using the Item Table
 *					pointer and the Item Table offset (from
 *					bint_elem).
 *				6.	Get the dd reference of the specified Item ID from
 *					the Item Table using the Item Table element pointer.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *		id			- the Item ID of a parameter or parameter list in
 *					  the specified block.
 *		subindex	- the subindex of either a member of a parameter
 *					  that is a record or the offset into a parameter
 *					  list.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_id_si_to_ite(ITEM_TBL *it, BLK_TBL_ELEM *bt_elem, ITEM_ID id,
SUBINDEX subindex, ITEM_TBL_ELEM **it_elem)
{

	BLK_ITEM_TBL_ELEM			*bit_elem;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							rs;
	int							param_offset;
	ITEM_ID						char_id ;
	ITEM_TBL_ELEM				*char_it_elem ;

	/*
	 *	Check the parameter offset to see if it is in range.  Find the
	 *	Item Table element that corresponds to the block handle.
	 */

	rs = find_bit_elem(BTE_BIT(bt_elem), id, &bit_elem);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Calculate the parameter offset.
	 */

	bint_elem = BINTE(BTE_BINT(bt_elem), BITE_BINT_OFFSET(bit_elem));
	param_offset = BINTE_PT_OFFSET(bint_elem);
	rs = tr_param_si_to_ite(it, bt_elem, param_offset, subindex, it_elem) ;
	if (rs != DDS_SUCCESS) {
		/*
		 * A parameter (with subindex) wasn't specified so check to
		 * see if the characteristics record was specified
		 */
		char_it_elem = ITE(it, BTE_CR_IT_OFFSET(bt_elem)) ;
		char_id = ITE_ITEM_ID(char_it_elem) ;
		if (id == char_id) {	
			rs = tr_characteristics_to_ite(it, bt_elem, subindex, it_elem) ;
		}
	}
	return(rs);
}


#ifdef DEBUG
/***********************************************************************
 *
 *  Name: dds_check_table
 *  ShortDesc: 	Check the table structures 
 *
 *  Description:
 *				Check the referential integrity of each of the tables.
 *				Also, check that each of the required table is loaded.
 *				Finally, check that tables with multiple keys (i.e.
 *				block item name table and block item table is really
 *				a single table with two keys) have equal numbers of
 *				elements.
 *
 *  Inputs:
 *      dt_handle - The device type table handle
 *  Outputs:
 *
 *  Returns:
 *      void.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

void
dds_check_table(DEVICE_TYPE_HANDLE dt_handle)
{

	FLAT_DEVICE_DIR	*fdd ;		/* Flat Device Directory */
	BLK_TBL_ELEM	*bt_elem ; 	/* Block Table Element Pointer */
	BLK_TBL			*bt ;		/* Block Table Pointer */
	BLK_ITEM_TBL	*bit ;		/* Block Item Table Pointer */
	BLK_ITEM_NAME_TBL *bint ;	/* Block Item Name Table Pointer */
	ITEM_TBL		*it	;		/* Item Table Pointer */
	REL_TBL			*rt	;		/* Relation Item Table Pointer */
	UPDATE_TBL		*ut	;		/* Update Table Pointer */
	CHAR_MEM_TBL	*cmt	;	/* Characteristic Mem Table Pointer */
	CHAR_MEM_NAME_TBL *cmnt	;	/* Char Mem Name Table Pointer */
	PARAM_TBL		*pt	;		/* Param Table Pointer */
	PARAM_MEM_TBL	*pmt	;	/* Param member name Table Pointer */
	PARAM_ELEM_TBL	*pet	;	/* Param element Table Pointer */
	PARAM_LIST_TBL	*plt	;	/* Param List Table Pointer */
	PARAM_LIST_MEM_TBL	*plmt ;	/* Param List Member Table Pointer */
	PARAM_LIST_MEM_NAME_TBL	*plmnt ;/* Param Lst Mem Name Tbl Pointer */
	PARAM_MEM_NAME_TBL	*pmnt ;	/* Param member name Table Pointer */

	int				it_cnt ;	/* Item Table element count */
	int				bt_cnt ;	/* Block Table element count */

	int				bit_cnt ;	/* Block Item Table element count */
	int				bint_cnt ;	/* Block Item Name Table elem count */
	int				pt_cnt ;	/* Param Table element count */
	int				pmt_cnt ;	/* Param member table elem cnt */
	int				pet_cnt ;	/* Param element table elem cnt */
	int				pmnt_cnt ;	/* Param member name table elem cnt */
	int				plt_cnt ;	/* Param List Table element count */
	int				plmt_cnt ;	/* Param list mem table elem cnt */
	int				plmnt_cnt ;	/* Param list mem name tbl elem cnt */
	int				rt_cnt ;	/* Relation Item Table element count */
	int				ut_cnt ;	/* Update Table element count */

	int				i ;			/* Local loop counter */
	int				good_tbl ;	/* Local boolean value */
	int				r_code ;

	/*
	 * Be sure the Device Type Handle is Valid
	 */

	ASSERT_DBG(valid_device_type_handle(dt_handle)) ;

	/*
	 * Get the Flat Device Directory from the Active Device
	 * type table
	 */

	r_code =  get_adtt_dd_dev_tbls(dt_handle, (void **)&fdd) ; 
	ASSERT_DBG(r_code == SUCCESS) ; 

	/*
	 * Be sure that the Flat Device Directory is valid
	 */

	ASSERT_DBG(fdd) ;

	/*
	 * Calculate the Table pointers and element counts
	 */

	bt = &(fdd->blk_tbl) ;
	bt_cnt = BT_COUNT(bt) ;

	it = &(fdd->item_tbl) ;
	it_cnt = IT_COUNT(it) ;

	/*
	 * Check the Block Table
	 */

	good_tbl = REQ_TABLE_CHK(bt_cnt, BT_LIST(bt)) ;

	ASSERT_DBG(good_tbl) ;

	check_bt(bt,it_cnt) ;

	/*
	 * Check the item table
	 */

	good_tbl = REQ_TABLE_CHK(it_cnt, IT_LIST(it)) ; 

	ASSERT_DBG(good_tbl) ;

	check_it(it) ;

	/*
	 * Check the block Tables
	 */

	for (i = 0 ; i < bt_cnt ; i++) {

		/*
		 * Get the Current Block Table Element
		 */

		bt_elem = BTE(bt,i) ;

		/*
		 * Check the usage of the block.  If the block is in use, assume
		 * the tables are loaded.  Otherwise, go get the next block.
		 */

		if (BTE_USAGE(bt_elem) <= 0) {
			continue ;
		}

		/*
		 * Calculate the Table pointers and element counts
		 */

		bit = BTE_BIT(bt_elem) ;
		bit_cnt = BIT_COUNT(bit) ;

		bint = BTE_BINT(bt_elem) ;
		bint_cnt = BINT_COUNT(bint) ;

		pt = BTE_PT(bt_elem) ;
		pt_cnt = PT_COUNT(pt) ;

		pmt = BTE_PMT(bt_elem) ;
		pmt_cnt = PMT_COUNT(pmt) ;

		pet = BTE_PET(bt_elem) ;
		pet_cnt = PET_COUNT(pet) ;

		pmnt = BTE_PMNT(bt_elem) ;
		pmnt_cnt = PMNT_COUNT(pmnt) ;

		plt = BTE_PLT(bt_elem) ;
		plt_cnt = PLT_COUNT(plt) ;

		plmt = BTE_PLMT(bt_elem) ;
		plmt_cnt = PLMT_COUNT(plmt) ;

		plmnt = BTE_PLMNT(bt_elem) ;
		plmnt_cnt = PLMNT_COUNT(plmnt) ;

		cmt = BTE_CMT(bt_elem) ;

		cmnt = BTE_CMNT(bt_elem) ;

		rt = BTE_RT(bt_elem) ;
		rt_cnt = RT_COUNT(rt) ;

		ut = BTE_UT(bt_elem) ;
		ut_cnt = UT_COUNT(ut) ;


		/*
		 * Check the tables that must have one or more elements (i.e., Block
		 * item name table, block item table, characteristic member table,
		 * the parameter table and the characteristic member name table. 
		 * The list pointers for each of the tables must also be non-null. 
		 * Check the other tables to see that either the count > 0 and the
		 * list is not null or the count is <= 0 and the list is null.
		 */

		good_tbl = 	REQ_TABLE_CHK(bit_cnt  ,BIT_LIST(bit)) &&
					REQ_TABLE_CHK(bint_cnt ,BINT_LIST(bint)) &&
					REQ_TABLE_CHK(pt_cnt   ,PT_LIST(pt)) &&
					OPT_TABLE_CHK(pmt_cnt  ,PMT_LIST(pmt)) &&
					OPT_TABLE_CHK(pet_cnt  ,PET_LIST(pet)) &&
					OPT_TABLE_CHK(pmnt_cnt ,PMNT_LIST(pmnt)) &&
					OPT_TABLE_CHK(plt_cnt  ,PLT_LIST(plt)) &&
					OPT_TABLE_CHK(plmt_cnt ,PLMT_LIST(plmt)) &&
					OPT_TABLE_CHK(plmnt_cnt,PLMNT_LIST(plmnt)) &&


					OPT_TABLE_CHK(rt_cnt   ,RT_LIST(rt)) &&
					OPT_TABLE_CHK(ut_cnt   ,UT_LIST(ut)) ;

		ASSERT_DBG(good_tbl) ;

		/*
		 * Check the tables pairs that must have equal size.
		 * (i.e. block item name table == block item table
		 * 		 parameter member name table == parameter member table
		 *		 parameter list member name table == parameter list 
	 	 *		 member table
		 *		 characteristic member name table == characteristic
		 *		 member table
		 *		 The block item table == parameter table + parameter
		 *		 list table + 1 (for the characteristic record).
		 */

		good_tbl = bint_cnt == bit_cnt && 
				pmnt_cnt == pmt_cnt &&
				plmnt_cnt == plmt_cnt   
				;


		ASSERT_DBG(good_tbl) ;

		/*
		 * Check that the contents of each of the tables is
		 * consistent with the tables it references.
		 */

		check_bit(bit, bint_cnt) ;
		check_bint(bint, it_cnt, rt_cnt, pt_cnt, plt_cnt) ;
		check_pt(pt, it_cnt, pmt_cnt, bint_cnt) ;
		check_pmt(pmt, it_cnt, rt_cnt) ;
		check_pmnt(pmnt, pmt_cnt) ;
		check_plt(plt, it_cnt, plmt_cnt) ;
		check_plmt(plmt, bint_cnt) ;
		check_plmnt(plmnt, plmt_cnt) ;


		check_rt(rt, it_cnt, ut_cnt) ;
		check_pet((*pet), rt_cnt) ;
		check_ut(ut, it_cnt) ;
	}
}
#endif
