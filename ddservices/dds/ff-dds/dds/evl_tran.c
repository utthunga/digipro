/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This file contains all functions relating to the data structure
 *  TRANSACTION, DATA_ITEM_LIST, and DATA_ITEM.
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */
#include <malloc.h>
#include "std.h"
#include "evl_loc.h"
#include "app_xmal.h"
#include "tst_fail.h"

static int ddl_parse_vector(unsigned char **chunkp, DDL_UINT *size, VECTOR *vector,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed);
static int
ddl_vectorlist_choice(unsigned char **chunkp, DDL_UINT *size, VECTOR_LIST *list,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO2 *env_info, OP_REF *var_needed);
/*
 * global_trans_number is a global value which is used by the tokenizer
 * when it is creating ptoc tables.  global_trans_number broadcasts the
 * number of the current transaction being evaluated.
 */

unsigned long   global_trans_number = 0;
int				processing_reply = 0;

/*
 * global_trans_reply_index is a global value which is used by the tokenizer
 * when it is creating ptoc tables.  global_trans_reply_index broadcasts the
 * index of the current reply transaction being evaluated.
 */

int             global_trans_reply_index = -1;

/*********************************************************************
 *  Name: ddl_shrink_dataitems_list
 *  ShortDesc: Shrink a DATA_ITEM_LIST.
 *
 *  Description:
 *      ddl_shrink_dataitems_list reallocates the DATA_ITEM_LIST to
 *		contain space only for the DATA_ITEM's currently being used
 *
 *  Inputs:
 *      items:   a pointer to the DATA_ITEM_LIST
 *
 *  Outputs:
 *      items:   a pointer to the condensed DATA_ITEM_LIST
 *
 *  Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *  Author: steve beyerl
 ***************************************************************************/
static int
ddl_shrink_dataitems_list(DATA_ITEM_LIST *items)
{

	TEST_FAIL(DDL_SHRINK_DATAITEMS_LIST);

	if (!items) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (!items->list) {
		ASSERT_DBG(!items->count && !items->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (items->count == items->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (items->count == 0) {
		ddl_free_dataitems(items, FREE_ATTR);
	}
	else {
		items->limit = items->count;

		items->list = (DATA_ITEM *) xrealloc((void *) items->list,
			(size_t) (items->limit * sizeof(DATA_ITEM)));
		if (!items->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_dataitems
 *	ShortDesc: Free the list of DATA_ITEMs
 *
 *	Description:
 *		ddl_free_dataitems will free the list of DATA_ITEMs in the
 *		DATA_ITEM_LIST structure.
 *
 *	Inputs:
 *		data_items: pointer to the DATA_ITEM_LIST structure.
 *
 *	Outputs:
 *		data_items: pointer to an empty DATA_ITEM_LIST structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/

void
ddl_free_dataitems(DATA_ITEM_LIST *data_items, uchar dest_flag)
{

	int             i;
	DATA_ITEM      *temp_list;

	if (data_items == NULL) {
		return;
	}

	if (data_items->list == NULL) {
		ASSERT_DBG(!data_items->count && !data_items->limit);
		data_items->limit = 0;
	}
	else {

		temp_list = data_items->list;
		for (i = 0; i < data_items->count; temp_list++, i++) {
			if (temp_list->type == DATA_REFERENCE) {
				ddl_free_op_ref_trail(&temp_list->data.ref);
			}
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of DATA_ITEMs
			 */

			xfree((void **) &data_items->list);
			data_items->list = NULL;
			data_items->limit = 0;
		}
	}

	data_items->count = 0;
}


void
ddl_free_vector(VECTOR *vector_items, uchar dest_flag)
{

	int             i;
	VECTOR_ITEM      *temp_list;

	if (vector_items == NULL) {
		return;
	}

	if (vector_items->list == NULL) {
		ASSERT_DBG(!vector_items->count && !vector_items->limit);
		vector_items->limit = 0;
	}
	else {
		temp_list = vector_items->list;
		for (i = 0; i < vector_items->count; temp_list++, i++) {
			if (temp_list->type == VECTOR_REFERENCE) {
				ddl_free_op_ref_trail(&temp_list->vector.ref);
			}
			if (temp_list->type == VECTOR_STRING) {
				ddl_free_string(&temp_list->vector.str);
			}
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of VECTOR_ITEMs
			 */

			xfree((void **) &vector_items->list);
			vector_items->list = NULL;
			vector_items->limit = 0;
		}
	}

	vector_items->count = 0;
}




/***************************************************************
 *	Name:	ddl_parse_dataitems
 *	ShortDesc:	Parses a list of DATA_ITEMS from binary.
 *
 *	Description:
 *		ddl_parse_dataitems will parse a binary chunk and load the
 *		DATA_ITEM_LIST and/or load the dependency information into
 *		the OP_REF_LIST.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      ref - pointer to an ITEM_ID
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      ref - pointer to an ITEM_ID
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			DDL_PARSE_TAG()
 *			DDL_PARSE_INTEGER()
 *			ddl_parse_op_ref_trail()
 *			ddl_parse_bitstring()
 *			ddl_parse_float()
 *
 *	Author: Steve Beyerl
 *
 ***************************************************************/
static int
ddl_parse_dataitems(unsigned char **chunkp, DDL_UINT *size, DATA_ITEM_LIST *items,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        i;	/* temp storage for a parsed integer */
	DDL_UINT        tag;	/* temp storage of a parsed tag */
	DATA_ITEM      *tmp = NULL;	/* temp ptr to a list of data items */
	float           fvalue;	/* temp storage for a parsed float */


	TEST_FAIL(DDL_PARSE_DATAITEMS);

	while (*size > 0) {

		/*
		 * Parse a series of DATA_ITEMs.  If we need more room in the
		 * array of structures, malloc more room.  Then parse the next
		 * DATA_ITEM structure.
		 */

		if (items) {
			if (items->count >= items->limit) {
				items->limit += DATA_ITEM_LIST_INCSZ;
				tmp = (DATA_ITEM *) xrealloc((void *) items->list,
					(size_t) (items->limit * sizeof *tmp));
				if (!tmp) {
					items->limit = items->count;
					ddl_free_dataitems(items, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				items->list = tmp;
				(void)memset((char *) &items->list[items->count], 0,
					DATA_ITEM_LIST_INCSZ * sizeof *items->list);
			}
			tmp = items->list + items->count++;
		}

		/*
		 * Parse the tag to see what kind of DATA_ITEM this is.
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		switch (tag) {
		case DATA_CONSTANT:

			/*
			 * A constant data item.  Parse the constant value.
			 * Note the call to free and subreferences chained to
			 * this structure.  This is necessary, since we may be
			 * re-using this structure, and we need to clean up any
			 * previously allocated memory.
			 */

			DDL_PARSE_INTEGER(chunkp, size, &i);

			if (items) {
				tmp->type = DATA_CONSTANT;
				tmp->flags = 0;
				tmp->data.iconst = (short)i;
				tmp->width = 0;
			}
			break;

		case DATA_REFERENCE:

			/*
			 * A data reference.  Parse the reference.
			 */

			if (items) {
				tmp->type = DATA_REFERENCE;
				tmp->flags = 0;
				tmp->width = 0;

				rc = ddl_parse_op_ref_trail(chunkp, size, &tmp->data.ref,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			else {
				rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			break;


		case DATA_FLOATING:

			/*
			 * A floating point data item.  Parse the data value.
			 * Note the call to free and subreferences chained to
			 * this structure.  This is necessary, since we may be
			 * re-using this structure, and we need to clean up any
			 * previously allocated memory.
			 */

			rc = ddl_parse_float(chunkp, size, &fvalue);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (items) {
				tmp->type = DATA_FLOATING;
				tmp->flags = 0;
				tmp->data.fconst = fvalue;
				tmp->width = 0;
			}
			break;

		default:
			return DDL_ENCODING_ERROR;
		}

		if (global_trans_reply_index >= 0) {

			++global_trans_reply_index;
		}

	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_dataitems_choice
 *	ShortDesc: Choose the correct data item from a binary.
 *
 *	Description:
 *		ddl_dataitems_choice will parse the binary for a list of data items,
 *		according to the current conditionals (if any).  The value of
 *		the data items is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		items - pointer to a DATA_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		items - pointer to an DATA_ITEM_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_cond_list()
 *			ddl_parse_dataitems()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_dataitems_choice(unsigned char **chunkp, DDL_UINT *size,
	DATA_ITEM_LIST *items, OP_REF_LIST *depinfo, int *data_valid,
	ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk list */

	TEST_FAIL(DDL_DATAITEMS_CHOICE);

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to store chunks of binary temporarily.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		DATA_ITEMS_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (items && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (items == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_dataitems(&(chunk_ptr->chunk), &(chunk_ptr->size),
				items, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (items && data_valid) {
			*data_valid = TRUE;	/* items has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


/***********************************************************************
*
* Name: eval_dataitem
* ShortDesc: evaluate a dataitem attribute
*
* Description:
*	The eval_dataitem function evaluates a dataitem attribute.
*	The buffer pointed to by CHUNK should contain a binary chunk for a
*	dataitem attribute.	SIZE should specify the size of the binary.
*	If ITEMS is not a null pointer, the list of ITEMS is returned in ITEMS.
*	If depinfo is not a null pointer, dependency information about the
*	DATAITEMS is returned in depinfo.
*
* Inputs:
*      chunk - pointer to the address of the binary
*      size  - size of the binary
*      items - pointer to a DATA_ITEM_LIST structure where the result will
*              be stored.  If this is NULL, no result is computed
*              or stored.
*      depinfo - pointer to a OP_REF_LIST structure where dependency
*              information will be stored.  If this is NULL, no
*              dependency information will be stored.
*      env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*      chunk - pointer to the address following this binary
*      size  - size of the binary
*      items  - pointer to a DATA_ITEM_LIST structure containing
*				the result
*      depinfo - pointer to a OP_REF_LIST structure containing the
*              dependency information.
*	   var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*	DDL_DEFAULT_ATTR,
*	DDL_SUCCESS
* 	return codes from:
*		ddl_dataitems_choice()
*		ddl_shrink_depinfo()
*
* Author:	Steve Beyerl
****************************************************************/
int
eval_attr_dataitems(unsigned char *chunk, DDL_UINT size, DATA_ITEM_LIST *items,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	int             valid;	/* indicates validity of the data in "items" */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_DATAITEMS);

	valid = 0;

	if (items) {
		ddl_free_dataitems(items, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_dataitems_choice(&chunk, &size, items, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_dataitems_list(items);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_dataitems(items, FREE_ATTR);
		return rc;
	}

	if (items && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

static int
ddl_parse_vector_item(unsigned char **chunkp, DDL_UINT *size, VECTOR_ITEM *item,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        i;	/* temp storage for a parsed integer */
	DDL_UINT        tag;	/* temp storage of a parsed tag */
	float           fvalue;	/* temp storage for a parsed float */


	TEST_FAIL(DDL_PARSE_VECTOR_ITEM);

	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);
    
    if (tag != VECTOR_ITEM_TAG)
        return DDL_ENCODING_ERROR;

	/*
	 * Parse the tag to see what kind of VECTOR_ITEM this is.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

	switch (tag) {
	case VECTOR_CONSTANT:

		/*
		 * A constant vector item.  Parse the constant value.
		 * Note the call to free and subreferences chained to
		 * this structure.  This is necessary, since we may be
		 * re-using this structure, and we need to clean up any
		 * previously allocated memory.
		 */

		DDL_PARSE_INTEGER(chunkp, size, &i);

		if (item) {
			item->type = VECTOR_CONSTANT;
			item->vector.iconst = (long)i;
		}
		break;

	case VECTOR_REFERENCE:

		/*
		 * A vector reference.  Parse the reference.
		 */

		if (item) {
			item->type = VECTOR_REFERENCE;

			rc = ddl_parse_op_ref_trail(chunkp, size, &item->vector.ref,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		else {
			rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		break;


	case VECTOR_FLOATING:

		/*
		 * A floating point vector item.  Parse the vector value.
		 * Note the call to free and subreferences chained to
		 * this structure.  This is necessary, since we may be
		 * re-using this structure, and we need to clean up any
		 * previously allocated memory.
		 */

		rc = ddl_parse_float(chunkp, size, &fvalue);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (item) {
			item->type = VECTOR_FLOATING;
			item->vector.fconst = fvalue;
		}
		break;

	case VECTOR_STRING:
		/*
		 * A string vector item.  Parse the vector value.
		 */
        if (item)
			rc = ddl_parse_string(chunkp, size, 
				&item->vector.str, 
				depinfo, env_info, var_needed);
        else
			rc = ddl_parse_string(chunkp, size, 
				NULL, 
				depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (item) {
			item->type = VECTOR_STRING;
		}
		break;

	default:
		return DDL_ENCODING_ERROR;
	}

	return DDL_SUCCESS;
}

static int
ddl_shrink_vector(VECTOR *items)
{

	TEST_FAIL(DDL_SHRINK_VECTOR);

	if (!items) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (!items->list) {
		ASSERT_DBG(!items->count && !items->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (items->count == items->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (items->count == 0) {
		ddl_free_vector(items, FREE_ATTR);
	}
	else {
		items->limit = items->count;

		items->list = (VECTOR_ITEM *) xrealloc((void *) items->list,
			(size_t) (items->limit * sizeof(VECTOR_ITEM)));
		if (!items->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


static int
ddl_vectoritems_choice(unsigned char **chunkp, DDL_UINT *size,
	VECTOR *items, OP_REF_LIST *depinfo, int *vector_valid,
	ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk list */

	TEST_FAIL(DDL_VECTORITEMS_CHOICE);

	if (vector_valid) {
		*vector_valid = FALSE;
	}

	/*
	 * create a list to store chunks of binary temporarily.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		VECTOR_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (items && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, vector_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((vector_valid != NULL) || (items == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_vector(&(chunk_ptr->chunk), &(chunk_ptr->size),
				items, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (items && vector_valid) {
			*vector_valid = TRUE;	/* items has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}

void
ddl_free_vector_list(VECTOR_LIST *vector_list, uchar dest_flag)
{

	if (vector_list == NULL) {
		return;
	}

	if (vector_list->vectors == NULL) {

		ASSERT_DBG(!vector_list->count && !vector_list->limit);
		vector_list->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list
		 */
        int i;
        for (i=0; i < vector_list->count; i++)
            ddl_free_vector(&(vector_list->vectors[i]), FREE_ATTR);
        xfree((void **)&vector_list->vectors);
        vector_list->limit = 0;
	}
	vector_list->count = 0;
}


int
eval_attr_vectors(unsigned char *chunk, DDL_UINT size, VECTOR_LIST *items,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	int             valid;	/* indicates validity of the vector list in "items" */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_VECTORS);

	valid = 0;
	var_needed->id = 0;	/* Zero id says that this is not set */

	if (items) {
		ddl_free_vector_list(items, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_vectorlist_choice(&chunk, &size, items, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_vector_list(items, FREE_ATTR);
		return rc;
	}

	if (items && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;


}

static int
ddl_parse_vector(unsigned char **chunkp, DDL_UINT *size, VECTOR *vector,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	VECTOR_ITEM     *temp_vector;/* temporary pointer */
    DDL_UINT        tag;
    DDL_UINT        numExpected; /* Number of items expected */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_VECTOR);

	/*
	 * Parse a VECTOR.  If we need more room in the array of structures,
	 * xrealloc more room.  Then call ddl_parse_item_id to load the next VECTOR
	 * structure.
	 */
	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);
    
    if (tag != VECTOR_SEQLIST_TAG)
        return DDL_ENCODING_ERROR;

    DDL_PARSE_TAG(chunkp, size, &numExpected, (DDL_UINT *) NULL_PTR);


	if (vector != NULL) {

		/*
		 * parse the binary and load the VECTOR structure
		 */

		while (*size > 0) {

            if (vector->count == numExpected)
                break;

			if (vector->count == vector->limit) {

				/*
				 * reallocate the VECTOR structure for
				 * more VECTORs
				 */

				vector->limit += VECTOR_INCSZ;

				/*
				 * realloc VECTOR array
				 */

				temp_vector = (VECTOR_ITEM *) xrealloc((void *) vector->list,
					(size_t) (vector->limit * sizeof(VECTOR_ITEM)));

				if (temp_vector == NULL) {

					vector->limit = vector->count;
					ddl_free_vector(vector, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				vector->list = temp_vector;

				/*
				 * Initialize the new VECTORs to zero
				 */
				(void)memset((char *) &vector->list[vector->count], 0,
					(VECTOR_INCSZ * sizeof(VECTOR_ITEM)));
			}

			rc = ddl_parse_vector_item(chunkp, size, &vector->list[vector->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_vector(vector, FREE_ATTR);
				return rc;
			}

			vector->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_vector(vector);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
        unsigned count = 0;
		while (*size > 0) {

            if (count >= numExpected)
                break;

			/*
			 * Don't load the VECTOR structure
			 */

			rc = ddl_parse_vector_item(chunkp, size, (VECTOR_ITEM *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
            count++;
		}
	}

	return DDL_SUCCESS;
}


static int
ddl_parse_vectorlist(unsigned char **chunkp, DDL_UINT *size, VECTOR_LIST *vector_list,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	VECTOR        *temp_id;/* temporary pointer */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_VECTORLIST);

	/*
	 * Parse a VECTOR_LIST.  If we need more room in the array of structures,
	 * xrealloc more room.  Then call ddl_parse_item_id to load the next VECTOR
	 * structure.
	 */

	if (vector_list != NULL) {

		/*
		 * parse the binary and load the VECTOR_LIST structure
		 */

		while (*size > 0) {

			if (vector_list->count == vector_list->limit) {

				/*
				 * reallocate the VECTOR_LIST structure for
				 * more VECTORs
				 */

				vector_list->limit += VECTOR_LIST_INC;

				/*
				 * realloc VECTOR array
				 */

				temp_id = (VECTOR *) xrealloc((void *) vector_list->vectors,
					(size_t) (vector_list->limit * sizeof(VECTOR)));

				if (temp_id == NULL) {

					vector_list->limit = vector_list->count;
					ddl_free_vector_list(vector_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				vector_list->vectors = temp_id;

				/*
				 * Initialize the new VECTORs to zero
				 */
				(void)memset((char *) &vector_list->vectors[vector_list->count], 0,
					(VECTOR_LIST_INC * sizeof(VECTOR)));
			}

			rc = ddl_parse_vector(chunkp, size, &vector_list->vectors[vector_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_vector_list(vector_list, FREE_ATTR);
				return rc;
			}

			vector_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_vector_list(vector_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the VECTOR_LIST structure
			 */

			rc = ddl_parse_vector(chunkp, size, (VECTOR *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}

int ddl_shrink_vector_list(VECTOR_LIST *vector_list)
{
	TEST_FAIL(DDL_SHRINK_REFLIST);

	if (vector_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (vector_list->vectors == NULL) {
		ASSERT_DBG(!vector_list->count && !vector_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (vector_list->count == vector_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (vector_list->count == 0) {
		ddl_free_vector_list(vector_list, FREE_ATTR);
	}
	else {
		vector_list->limit = vector_list->count;

		vector_list->vectors = (VECTOR *) xrealloc((void *) vector_list->vectors,
			(size_t) (vector_list->limit * sizeof(VECTOR)));

		if (vector_list->vectors == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

static
int
ddl_vectorlist_choice(unsigned char **chunkp, DDL_UINT *size, VECTOR_LIST *list,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;				/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

	TEST_FAIL(DDL_VECTORLIST_CHOICE);

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		VECTORS_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_vectorlist(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


int
eval_attr_vectoritems(unsigned char *chunk, DDL_UINT size, VECTOR *items,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	int             valid;	/* indicates validity of the vector in "items" */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_VECTORITEMS);

	valid = 0;

	if (items) {
		ddl_free_vector(items, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_vectoritems_choice(&chunk, &size, items, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_vector(items);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_vector(items, FREE_ATTR);
		return rc;
	}

	if (items && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

