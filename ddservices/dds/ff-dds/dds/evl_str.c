/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *
 *	This file contains all functions relating to the STRING data structure.
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include <malloc.h>
#include "std.h"
#include "fch_lib.h"
#include "evl_loc.h"
#include "app_xmal.h"

#include "tst_fail.h"

int (*get_enum_suppression_list_ptr) (ENV_INFO2 *env, char *str, unsigned int *max_len) = NULL;

unsigned char enum_check_for_suppression(STRING* enum_str, char *suppression_str);
void acquire_suppression_str(ENV_INFO2 *env_info, char **suppression_str);
void remove_all_suppression_flags(STRING *enum_str);


/*********************************************************************
 *
 *	Name: ddl_free_string
 *	ShortDesc: Free STRING structure.
 *
 *	Description:
 *		ddl_free_string will check the flags in the STRING structure.
 *      If flags indicate to free string then the STRING structure will
 *      be freed.
 *
 *	Inputs:
 *		str: pointer to the STRING structure
 *
 *	Outputs:
 *		str: pointer to the empty STRING structure
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_string(STRING *str)
{

	if (str->flags & FREE_STRING) {
		xfree((void **) &str->str);
	}
    if ((str->wflags & FREE_STRING) && (str->wstr))
    {
        xfree((void **) &str->wstr);
    }

	str->flags = 0;
	str->len = 0;
	str->str = NULL;

    str->wflags = 0;
    str->wlen = 0;
    str->wstr = NULL;

	return;
}


/*********************************************************************
 *
 *	Name: string_enum_lookup
 *
 *	ShortDesc: lookup an enumerated string
 *
 *	Description:
 *		string_enum_lookup will find the string referred to by an
 *		enumeration value string. This string contains the ITEM_ID
 *		of an enumerated variable, and the enumeration value which
 *		corresponds to the description string desired
 *
 *	Inputs:
 *		enum_id - ITEM_ID of the enumeration
 *		enum_val - value of the enumeration
 *      string - a pointer to the STRING struct to be loaded
 *		depinfo - a pointer to the OP_REF_LIST struct
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string - the loaded STRING structure
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				  dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		FAILURE
 *		return codes from fetch_var(), eval_attr_enum(), append_depinfo()
 *		eval_clean_var()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
string_enum_lookup(ITEM_ID enum_id, unsigned long enum_val, STRING *string,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	FLAT_VAR        var;		/* flat variable struct for calling fetch with */
	ENUM_VALUE     *enum_ptr;	/* temp enum pointer */
	SCRATCH_PAD     scratch[2];	/* scratch pads for fetch */
	int             rc;			/* return code */
	int             rc2 = DDL_SUCCESS;	/* secondary return code */
	int             inc;		/* incrementer */
	int             count;		/* temp enum.count */
	int             free_attr = FALSE;	/* flag whether the attr should
						 				* be freed */
	DEPBIN         *cur_depbin;	/* Current dependency and binary info */

	TEST_FAIL(STRING_ENUM_LOOKUP);

	(void)memset((char *) &var, 0, sizeof(FLAT_VAR));

	/*
	 * Fetch the binary for the enumerated variable
	 */

	rc = resolve_fetch(enum_id, (unsigned long) VAR_ENUMS, (void *) &var,
		(unsigned short) VARIABLE_ITYPE, env_info->block_handle, scratch);

	if (rc == DDL_SUCCESS) {

		if (!(var.masks.attr_avail & VAR_ENUMS)) {

			/*
			 * Evaluate the binary for the enumerated variable
			 */
			cur_depbin = var.depbin->db_enums;

			rc = eval_attr_enum(cur_depbin->bin_chunk, cur_depbin->bin_size,
				&var.enums, &cur_depbin->dep, env_info, var_needed);

			if (rc == DDL_SUCCESS) {
				free_attr = TRUE;
			}

			if (depinfo != NULL) {
				rc2 = append_depinfo(depinfo, &cur_depbin->dep);
				if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
					rc = rc2;
				}
			}

			ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
		}
	}

	if (rc == DDL_SUCCESS) {

		enum_ptr = var.enums.list;
		count = var.enums.count;

		/*
		 * Find the match in the list
		 */

		for (inc = 0; inc < count; enum_ptr++, inc++) {

			if (enum_ptr->val == enum_val) {

				string->str = enum_ptr->desc.str;
				string->len = enum_ptr->desc.len;
				string->flags = enum_ptr->desc.flags;

				enum_ptr->desc.str = NULL;
				enum_ptr->desc.len = 0;
				enum_ptr->desc.flags = DONT_FREE_STRING;

				break;
			}
		}

		if (inc == count) {	/* The enum was not found */
			rc = FAILURE;
		}
	}

	resolve_fetch_free((void *) &var, (unsigned short) VARIABLE_ITYPE, scratch,
		env_info->block_handle);

	if (free_attr) {
		ddl_free_enum_list(&var.enums, FREE_ATTR);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_string(chunkp, size, string)
 *
 *	ShortDesc: ddl_parse_string takes a binary chunk and loads a STRING structure with it.
 *
 *	Description:
 *		ddl_parse_string takes a binary chunk and reads the tag to determine which string type
 *		the string is: DICTIONARY_STRING, ENUMERATION_STRING, DEV_SPEC_STRING, TEXT_STRING,
 *		UNUSED_STRING or VARIABLE_STRING, it loads the string info structure with that type
 *		and looks up the corresponding string.
 *		If the string argument is NULL the structure is not loaded but the chunk pointer is
 *		updated.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to an STRING structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from DDL_PARSE_INTEGER(), ddl_parse_item_id(),
 *		app_func_get_dev_spec_string(), ddl_add_depinfo(), app_func_get_param_value()
 *		app_func_get_dict_string(), string_enum_lookup()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
ddl_parse_string(unsigned char **chunkp, DDL_UINT *size, STRING *string,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	DEV_STRING_INFO dev_str_info;	/* holds device specific string info */
	DDL_UINT        tag;	/* temporary tag */
	DDL_UINT        temp;	/* temporary storage for casting conversion */
	DDL_UINT        tag_len;/* length of binary assoc. w/ parsed tag */
	EVAL_VAR_VALUE  var_value;	/* used by app_func_get_param_value */
	OP_REF          oper_ref;	/* used by variable strings */
	ITEM_ID         enum_id;/* used for enumeration strings */
	unsigned long   enum_val;	/* used for enumeration strings */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_STRING);

	/*
	 * initialize string struct
	 */

	if (string != NULL) {
		string->flags = DONT_FREE_STRING;
		string->len = 0;
		string->str = NULL;
	}

	/*
	 * Initialize the op_ref structure.
	 */

	(void)memset((char *) &oper_ref, 0, sizeof(OP_REF));

	/*
	 * Parse the tag to find out what kind of string it is.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &tag_len);

	switch (tag) {

	case DEV_SPEC_STRING_TAG:

		/*
		 * device specific string (ie. string number). Parse the file
		 * information and string number.
		 */

		if (string) {

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.mfg = (unsigned long) temp;

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.dev_type = (unsigned short) temp;

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.rev = (unsigned char) temp;

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.ddrev = (unsigned char) temp;

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.id = (unsigned int) temp;

			rc = app_func_get_dev_spec_string((ENV_INFO *)env_info, &dev_str_info,
				string);

			/*
			 * If a string was not found, get the default error string.
			 */

			if (rc != DDL_SUCCESS) {
				rc = app_func_get_dict_string((ENV_INFO *)env_info, DEFAULT_DEV_SPEC_STRING,
					string);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
		else {
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	case VARIABLE_STRING_TAG:
	case VAR_REF_STRING_TAG:

		/*
		 * variable/variable_reference string. Parse the variable id.
		 */

		if (tag == VARIABLE_STRING_TAG) {
			DDL_PARSE_INTEGER(chunkp, size, &temp);
			oper_ref.id = (ITEM_ID) temp;
			oper_ref.subindex = 0;
			oper_ref.type = 0;
		}
		else {		/* VAR_REF_STRING_TAG 	 */
			rc = ddl_parse_op_ref(chunkp, size, &oper_ref, depinfo, env_info, var_needed, NULL /* bitmask unused */);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		if (depinfo) {

			/*
			 * Add item_id of string to dependency info.
			 */

			rc = ddl_add_depinfo(&oper_ref, depinfo);

		}

		if (string) {
            if (env_info->type == ENV_DEVICE_HANDLE)
            {
                /* Make this into a friendly block handle for the callback */
                if (!app_func_get_block_handle)
                    return DDI_BLOCK_NOT_FOUND;
                {
                    ENV_INFO2 env;
                    env.app_info = env_info->app_info;
                    env.type = ENV_BLOCK_HANDLE;
                    env.device_handle = 0;

                    rc = (*app_func_get_block_handle)((ENV_INFO *)env_info,
                        oper_ref.block_id, oper_ref.block_instance,
                         &env.block_handle);
                    if (rc)
                        return rc;
        			rc = retrieve_param_value((ENV_INFO *)&env, &oper_ref, &var_value);
                }
            }
            else
    			rc = retrieve_param_value((ENV_INFO *)env_info, &oper_ref, &var_value);
			if (rc != DDL_SUCCESS) {
				var_needed->id = oper_ref.id;
				var_needed->subindex = oper_ref.subindex;
				var_needed->type = oper_ref.type;
				var_needed->container_type = oper_ref.container_type;
				var_needed->container_id = oper_ref.container_id;
				var_needed->container_index = oper_ref.container_index;
				return rc;
			}

			string->str = var_value.val.s.str;
			string->len = var_value.val.s.len;
			string->flags = var_value.val.s.flags;
		}
		break;

	case ENUMERATION_STRING_TAG:
	case ENUM_REF_STRING_TAG:

		/*
		 * enumeration/enumeration_reference string. Parse the
		 * enumeration ID, and the value within the enumeration.
		 */

		if (string) {

			if (tag == ENUMERATION_STRING_TAG) {
				DDL_PARSE_INTEGER(chunkp, size, &temp);
				enum_id = (ITEM_ID) temp;
			}
			else {	/* ENUM_REF_STRING_TAG */
				rc = ddl_parse_item_id(chunkp, size, &oper_ref.id, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				enum_id = oper_ref.id;
			}

			DDL_PARSE_INTEGER(chunkp, size, &enum_val);
			rc = string_enum_lookup(enum_id, enum_val, string,
				depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				return (rc);
			}
		}
		else {
			if (tag == ENUMERATION_STRING_TAG) {
				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			}
			else {
				rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	case DICTIONARY_STRING_TAG:

		/*
		 * dictionary string. Parse the dictionary string number.
		 */

		if (string) {

			DDL_PARSE_INTEGER(chunkp, size, &temp);

			rc = app_func_get_dict_string((ENV_INFO *)env_info, temp, string);

			/*
			 * If a string was not found, get the default error string.
			 */

			if (rc != DDL_SUCCESS) {
				rc = app_func_get_dict_string((ENV_INFO *)env_info,
					DEFAULT_STD_DICT_STRING, string);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
		else {
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	default:

		/*
		 * Unknown tag. Default this string.
		 */

		if (string) {
			rc = app_func_get_dict_string((ENV_INFO *)env_info,
				DEFAULT_STD_DICT_STRING, string);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		*chunkp += tag_len;
		*size -= tag_len;
	}

    /* Determine if the host wants this enumeration suppressed */
    if (string)
    {
		char *suppression_str;
		static int count = 0;

		count++;

        /* Query the host as to the list of suppressed enumerations */
        acquire_suppression_str(env_info, &suppression_str);

        if (enum_check_for_suppression(string, suppression_str))
		{
			/* This string is supposed to be suppressed - inform the user */
			string->flags |= DDS_SUPPRESS;
		}
	    if (suppression_str)
		    free(suppression_str);

		/* Remove any suppression flags ("|*jpsi|", etc.) from the string */
        remove_all_suppression_flags(string);
    }

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_string_choice
 *	ShortDesc: Choose the correct string from a binary.
 *
 *	Description:
 *		ddl_string_choice will parse the binary for an string,
 *		according to the current conditionals (if any).  The value of
 *		the string is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to a STRING structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_string().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_string_choice(unsigned char **chunkp, DDL_UINT *size, STRING *string,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

	TEST_FAIL(DDL_STRING_CHOICE);

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (string && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/*
		 * We have finally gotten to the actual value!  Parse it.
		 */

		rc = ddl_parse_string(&val.chunk, &val.size, string, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_string
 *	ShortDesc: Evaluate a string
 *
 *	Description:
 *		eval_string parses a chunk of binary data and loads string
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		string - space for parsed string
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string is loaded with the string
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		return codes from ddl_string_choice(), ddl_shrink_depinfo()
 *		DDL_DEFAULT_ATTR - invalid string
 *		DDL_SUCCESS - chunk was parsed and string was loaded successfully
 *
 *	Author: Jon Westbrock
 *
 *********************************************************************/
int
eval_string(unsigned char *chunk, DDL_UINT size, STRING *string,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	int             valid;	/* data valid flag */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_STRING);

	valid = 0;

	if (string) {
		ddl_free_string(string);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_string_choice(&chunk, &size, string, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (string && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

static int findNextCountryCode(char **sup_ptr, char *out_code, unsigned int max_len)
{
    char *ptr = strchr(*sup_ptr, '|');
    char *end_ptr;
    unsigned int len;

    if (!ptr)
        return 1;

    end_ptr = strchr(ptr+1, '|');

    if (!end_ptr)
        return 1;

    len = (unsigned int)(end_ptr - ptr);
    if (len >= max_len)
        return 1;

    memcpy(out_code, ptr, len+1);
    out_code[len+1] = '\0';
    *sup_ptr = end_ptr + 1;

    return 0;
}

/* enum_check_for_suppression
 * Determine if the specified string "enum_str" must be suppressed based on the 
 * supplied "suppression_str", which contains a list of or-bar ("|") separated codes.
 * For example, "suppression_str" could contain "|*U||*X|" in which case, if 
 * either "|*U|" or "|*X|" occured in "enum_str", the function would return nonzero.
 * The function returns 0 for no match (and no suppression), or nonzero for match
 * (and suppression required).
 */
unsigned char enum_check_for_suppression(STRING* enum_str, char *suppression_str)
{
    char sup_code[32];  /* The current code we're looking for */
    char *sup_ptr;
    char *enum_ptr;

    if (!enum_str)
        return 0;
    if (!suppression_str)
        return 0;

    /* Check through the list of country-code-like strings in suppression_str which
     * occur in enum_str */
    sup_ptr = suppression_str;
    enum_ptr = enum_str->str;
    while (!findNextCountryCode(&sup_ptr, sup_code, sizeof(sup_code)))
    {
        if (strstr(enum_ptr, sup_code))  /* Found a match */
            return 1;
    }
    return 0;
}

void acquire_suppression_str(ENV_INFO2 *env_info, char **suppression_str)
{
    unsigned int sz = 64;
    unsigned int prev_sz = sz;
    *suppression_str = malloc(sz);
    if (!(*suppression_str))
        return;
    *suppression_str[0] = '\0';  /* Initialize to an empty string */

    if ((get_enum_suppression_list_ptr) && 
        ((*get_enum_suppression_list_ptr)(env_info, *suppression_str, &sz)))
    {
        /* We either have a real error, or a size problem.  See which one.
         */
        if (sz != prev_sz)
        {
            /* The host is saying our buffer is too small - realloc and try again */
            *suppression_str = realloc(*suppression_str, sz);
            if (!(*suppression_str))
                return;
            /* Now call again */
            if ((*get_enum_suppression_list_ptr)(env_info, *suppression_str, &sz))
            {
                free(*suppression_str);
                *suppression_str = NULL;
                return;
            }
            else
            {            	
            	free(*suppression_str);
            	*suppression_str = NULL;
            }
        }
        else
        {
            /* Call failed and it wasn't a buffer size problem */
            free(*suppression_str);
            *suppression_str = NULL;
            return;
        }
    }
    else
    {
		 free(*suppression_str);
		 *suppression_str = NULL;
    }
}

/* Traverse "enum_str" and remove any suppression flags.  Suppression flags look like
 * country codes, but start with an "*".  For example, "|*U|" is a suppression flag.
 */
void remove_all_suppression_flags(STRING *enum_str)
{
    char *ptr;
    char *pastFlag;

    if (!enum_str) 
        return;
    ptr = enum_str->str;
    if (!ptr)
        return;
    for (;;)
    {
        ptr = strstr(ptr, "|*");
        if ( !ptr )
        {
           break;
        }

        pastFlag = strchr(ptr+1, '|');
        if (!pastFlag)
        {
            return;
        }
        strcpy(ptr, pastFlag + 1);
    }
    enum_str->len = (unsigned short)(strlen(enum_str->str));
}
