/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This file contains the functions for the ROD Directory Fetch
 */

/* #includes */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include <stdio.h> /* ANSI */

#include	"fch_lib.h"
#include "tst_fail.h"
#include "tags_sa.h"


/* #defines */

#define	DEVICE_DIR_LEN	((MAX_DEVICE_TBL_ID * TABLE_REF_LEN) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE)

#define	DEVICE_DIR_LEN2	((MAX_DEVICE_TBL_ID * TABLE_REF_LEN2) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE)

#define	BLOCK_DIR_LEN	((MAX_BLOCK_TBL_ID * TABLE_REF_LEN) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE)

/* local procedure declarations */


/*********************************************************************
 *
 *	Name:	set_dir_bin_exists
 *
 *	ShortDesc: Sets the bin_exists field in the BININFO structure
 *
 *	Description:
 *		A bit is set in the bin_exists field in the BININFO structure
 *		corresponding to each non-zero length table reference in the
 *		directory object extension.
 *
 *	Inputs:
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
set_dir_bin_exists(unsigned short rights, unsigned char usage, unsigned char *local_data_ptr, unsigned long *dir_bin_exists,
	unsigned short dir_type)
{
	int		i, max_table_id;
    int     is_5x_format = 0;
	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(dir_bin_exists, FETCH_INVALID_PARAM);


    if (calc_version(rights, usage) >= 0x0500)
        is_5x_format = 1;

	/*
	 * Set a bit in the bin_exists field for each non-zero length field in
	 * the table references in the object extension
	 */

	*dir_bin_exists = 0L;
    if (dir_type == DEVICE_DIR_TYPE)
    {
        if (is_5x_format)
            max_table_id = MAX_DEVICE_TBL_ID;
        else
            max_table_id = MAX_DEVICE_TBL_ID_PRE_5;
    }
    else
    {
        if (is_5x_format)
            max_table_id = MAX_BLOCK_TBL_ID;
        else
            max_table_id = MAX_BLOCK_TBL_ID_PRE_5;
    }

	if(is_5x_format)
	{
		for (i = 0; i < max_table_id; i++) {
			if (	local_data_ptr[4] 
				||  local_data_ptr[5]
				||  local_data_ptr[6]
				||  local_data_ptr[7]) {
				*dir_bin_exists |= (1L << i);
			}
			local_data_ptr += TABLE_REF_LEN2;
		}
	}
	else
	{
		for (i = 0; i < max_table_id; i++) {
			if (local_data_ptr[2] || local_data_ptr[3]) {
				*dir_bin_exists |= (1L << i);
			}
			local_data_ptr += TABLE_REF_LEN;
		}
	}
	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name: get_ref_data
 *
 *	ShortDesc: Get directory reference (local) data
 *
 *	Description:
 *		Retrieves the table from the local data area.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		obj_ref_ptr -	pointer to the directory object extension
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		table_size -	size of the requested table
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_ref_data(OBJECT *obj, SCRATCH_PAD *scrpad, unsigned char *obj_ref_ptr,
	unsigned long *table_size)
{

	unsigned long  dir_data_offset;	/* unpacked table offset */
	unsigned long  dir_data_size;	/* unpacked table size */
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(obj && scrpad && obj_ref_ptr,
		FETCH_INVALID_PARAM);

	/*
	 * Get the local data area offset and data size from the directory
	 * reference
	 */

	if(calc_version(obj->access.rights, obj->specific->domain.usage) >= 0x0500)
	{
		dir_data_offset =
			  (((unsigned short) obj_ref_ptr[0]) << 24) 
			| (((unsigned short) obj_ref_ptr[1]) << 16) 
			| (((unsigned short) obj_ref_ptr[2]) << 8) 
			| obj_ref_ptr[3];

		dir_data_size =
			  (((unsigned short) obj_ref_ptr[4]) << 24) 
			| (((unsigned short) obj_ref_ptr[5]) << 16) 
			| (((unsigned short) obj_ref_ptr[6]) << 8) 
			| obj_ref_ptr[7];
	}
	else
	{
		dir_data_offset =
			(((unsigned short) obj_ref_ptr[0]) << 8) | obj_ref_ptr[1];
		dir_data_size =
			(((unsigned short) obj_ref_ptr[2]) << 8) | obj_ref_ptr[3];
	}

	/*
	 * If the scratchpad size is too small to load the data, return with an
	 * insufficient scratchpad error
	 */

	*table_size = (unsigned long) dir_data_size;
	if (dir_data_size > (scrpad->size - scrpad->used)) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	/*
	 * Retrieve the data from the ROD if there is any (non-zero length).
	 * Otherwise just return.
	 */

	if (dir_data_size) {
		rcode = get_local_data(obj, dir_data_offset, dir_data_size, scrpad);

		return (rcode);
	}

	return (SUCCESS);
}


/***********************************************************************
 *
 *	Name: get_rev_numbers
 *
 *	ShortDesc: Get the major and minor revision numbers of the DDROD.
 *
 *	Description:
 *		The get_rev_numbers function takes a handle to a DDROD, and gets 
 *		its DDOD Major Revision Number and Minor Revision Number.
 *
 *	Inputs:
 *		rod_handle - handle of the DDROD.
 *
 *	Outputs:
 *		major_rev_number - DDOD Major Revision Number of the DDROD.
 *		minor_rev_number - DDOD Minor Revision Number of the DDROD.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_rev_numbers(ROD_HANDLE rod_handle, UINT8 *major_rev_number,
	UINT8 *minor_rev_number)
{

	OBJECT			*format_object;
	int				 r_code;

	/*
	 *	Point to the Format Object in the DDROD.
	 */

	r_code = rod_get(rod_handle, (OBJECT_INDEX)FORMAT_OBJECT_INDEX,
			&format_object);

	/*
	 *	Check to make sure that there were no errors in pointing to the
	 *	Format Object, and that the Format Object's extension exists.
	 */

	if (r_code || !format_object->extension) {
		return(FETCH_OBJECT_NOT_FOUND);
	}

	/*
	 *	Set the revision numbers.
	 */

	*major_rev_number = format_object->extension[CODING_FMT_MAJOR_OFFSET];
	*minor_rev_number = format_object->extension[CODING_FMT_MINOR_OFFSET];

	return(SUCCESS);
}


/*********************************************************************
 *
 *	Name: fetch_rod_device_dir
 *
 *	ShortDesc: Perform ROD fetch for device directories
 *
 *	Description:
 *		Retrieves the ROD object for the device directories and the
 *		associated tables in the local data area.  The table binaries
 *		are then attached to the appropriate BININFO structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_device_dir(OBJECT *obj, SCRATCH_PAD *scrpad, unsigned char *obj_extn_ptr,
	BIN_DEVICE_DIR *bin_dev_dir, unsigned long *req_mask)
{

	unsigned char  *local_data_ptr;	/* points to table data in scratchpad */
	unsigned short  tag;			/* indicator for table type */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	unsigned short  dir_extn_offset;	/* offset in object extension
						 * for table reference field */
	BININFO        *bin_table_ptr;	/* points to a specific table BININFO
									 * structure */
	unsigned long   tbl_length;		/* size of table data */
	int             rcode;
	int             max_table_id;
	int             is_5x_version = 0;

	/*
	 * Set the bin_exists field in the BININFO structure if not already set
	 */

	if (!(bin_dev_dir->bin_exists)) {

		rcode = set_dir_bin_exists(obj->access.rights, obj->specific->domain.usage,
			obj_extn_ptr + DIRECTORY_DATA_OFFSET,
			&bin_dev_dir->bin_exists, DEVICE_DIR_TYPE);

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

    if (calc_version(obj->access.rights, obj->specific->domain.usage) >= 0x0500)
        is_5x_version = 1;

	tag = 0;
	dir_extn_offset = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (BININFO *) 0L;
    max_table_id = ((is_5x_version) ? 
        MAX_DEVICE_TBL_ID : MAX_DEVICE_TBL_ID_PRE_5);
	while ((*req_mask) && (tag < MAX_DEVICE_TBL_ID)) {

		/*
		 * Check for request mask bit corresponding to the tag value.
		 * Skip to next tag value if not requested.
		 */

		if (!((*req_mask) & (1L << tag))) {
			tag++;
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		if(is_5x_version)
		{
			switch (tag++) {

				case BLK_TBL_ID:	/* Block Table */
					tbl_mask_bit = BLK_TBL_MASK;
					dir_extn_offset = BLK_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->blk_tbl;
					break;

				case ITEM_TBL_ID:	/* Item Table */
					tbl_mask_bit = ITEM_TBL_MASK;
					dir_extn_offset = ITEM_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->item_tbl;
					break;

				case PROG_TBL_ID:	/* Program Table */
					tbl_mask_bit = PROG_TBL_MASK;
					dir_extn_offset = PROG_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->prog_tbl;
					break;

				case DOMAIN_TBL_ID:	/* Domain Table */
					tbl_mask_bit = DOMAIN_TBL_MASK;
					dir_extn_offset = DOMAIN_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->domain_tbl;
					break;

				case AXIS_TBL_ID:	/* Axis Table */
					tbl_mask_bit = AXIS_TBL_MASK;
					dir_extn_offset = AXIS_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->axis_tbl;
					break;

				case CHART_TBL_ID:	/* CHART Table */
					tbl_mask_bit = CHART_TBL_MASK;
					dir_extn_offset = CHART_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->chart_tbl;
					break;

				case FILE_TBL_ID:	/* FILE Table */
					tbl_mask_bit = FILE_TBL_MASK;
					dir_extn_offset = FILE_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->file_tbl;
					break;

				case GRAPH_TBL_ID:	/* GRAPH Table */
					tbl_mask_bit = GRAPH_TBL_MASK;
					dir_extn_offset = GRAPH_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->graph_tbl;
					break;

				case LIST_TBL_ID:	/* LIST Table */
					tbl_mask_bit = LIST_TBL_MASK;
					dir_extn_offset = LIST_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->list_tbl;
					break;

				case SOURCE_TBL_ID:	/* SOURCE Table */
					tbl_mask_bit = SOURCE_TBL_MASK;
					dir_extn_offset = SOURCE_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->source_tbl;
					break;

				case WAVEFORM_TBL_ID:	/* WAVEFORM Table */
					tbl_mask_bit = WAVEFORM_TBL_MASK;
					dir_extn_offset = WAVEFORM_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->waveform_tbl;
					break;

				case GRID_TBL_ID:	/* GRID Table */
					tbl_mask_bit = GRID_TBL_MASK;
					dir_extn_offset = GRID_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->grid_tbl;
					break;

				case IMAGE_TBL_ID:	/* IMAGE Table */
					tbl_mask_bit = IMAGE_TBL_MASK;
					dir_extn_offset = IMAGE_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->image_tbl;
					break;

				case BINARY_TBL_ID:	/* BINARY Table */
					tbl_mask_bit = BINARY_TBL_MASK;
					dir_extn_offset = BINARY_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->binary_tbl;
					break;

				case STRING_TBL_ID:	/* String Table */
					tbl_mask_bit = STRING_TBL_MASK;
					dir_extn_offset = STRING_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->string_tbl;
					break;

				case DICT_REF_TBL_ID:	/* Dictionary Reference Table */
					tbl_mask_bit = DICT_REF_TBL_MASK;
					dir_extn_offset = DICT_REF_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->dict_ref_tbl;
					break;

				case LOCAL_VAR_TBL_ID:	/* Dictionary Reference Table */
					tbl_mask_bit = LOCAL_VAR_TBL_MASK;
					dir_extn_offset = LOCAL_VAR_REF_OFFSET2;
					bin_table_ptr = &bin_dev_dir->local_var_tbl;
					break;

				default:	/* goes here for reserved or undefined table IDs */
					break;
			}
		}
		else
		{
			switch (tag++) {

				case BLK_TBL_ID:	/* Block Table */
					tbl_mask_bit = BLK_TBL_MASK;
					dir_extn_offset = BLK_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->blk_tbl;
					break;

				case ITEM_TBL_ID:	/* Item Table */
					tbl_mask_bit = ITEM_TBL_MASK;
					dir_extn_offset = ITEM_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->item_tbl;
					break;

				case PROG_TBL_ID:	/* Program Table */
					tbl_mask_bit = PROG_TBL_MASK;
					dir_extn_offset = PROG_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->prog_tbl;
					break;

				case DOMAIN_TBL_ID:	/* Domain Table */
					tbl_mask_bit = DOMAIN_TBL_MASK;
					dir_extn_offset = DOMAIN_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->domain_tbl;
					break;

				case STRING_TBL_ID:	/* String Table */
					tbl_mask_bit = STRING_TBL_MASK;
					dir_extn_offset = STRING_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->string_tbl;
					break;

				case DICT_REF_TBL_ID:	/* Dictionary Reference Table */
					tbl_mask_bit = DICT_REF_TBL_MASK;
					dir_extn_offset = DICT_REF_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->dict_ref_tbl;
					break;

				case LOCAL_VAR_TBL_ID:	/* Dictionary Reference Table */
					tbl_mask_bit = LOCAL_VAR_TBL_MASK;
					dir_extn_offset = LOCAL_VAR_REF_OFFSET;
					bin_table_ptr = &bin_dev_dir->local_var_tbl;
					break;


				default:	/* goes here for reserved or undefined table IDs */
					break;
			}
		}

		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and if it not zero length.
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {

			local_data_ptr = scrpad->pad + scrpad->used;

			rcode = get_ref_data(obj, scrpad, obj_extn_ptr + dir_extn_offset, &tbl_length);

			if (rcode == SUCCESS) {

				/*
				 * Attach the table if non-zero length, else go
				 * to the next table
				 */

				if (tbl_length) {
					bin_table_ptr->chunk = local_data_ptr;
					bin_table_ptr->size = tbl_length;
					bin_dev_dir->bin_hooked |= tbl_mask_bit;
				}
			}
			else {
				return (rcode);
			}
		}
	}			/* end while */

	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name: fetch_rod_block_dir
 *
 *	ShortDesc: Perform ROD fetch for block directories
 *
 *	Description:
 *		Retrieves the ROD object for the block directories and
 *		the associated tables in the local data area.  The table
 *		binaries are then attached to the appropriate BININFO
 *		structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_block_dir(OBJECT *obj, SCRATCH_PAD *scrpad, unsigned char *obj_extn_ptr,
	BIN_BLOCK_DIR *bin_blk_dir, unsigned long *req_mask)
{

	unsigned char  *local_data_ptr;	/* points to table data in scratchpad */
	unsigned short  tag;			/* indicator for table type */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	unsigned short  dir_extn_offset;	/* offset in object extension
						 * for table reference field */
	BININFO        *bin_table_ptr;	/* points to a specific table BININFO
									 * structure */
	unsigned long   tbl_length;		/* size of table data */
	int             rcode;
    int             is_50_format = 0;

	/*
	 * Set the bin_exists field in the BININFO structure if not already set
	 */

	if (!(bin_blk_dir->bin_exists)) {

		rcode = set_dir_bin_exists(obj->access.rights,
            obj->specific->domain.usage,
            obj_extn_ptr + DIRECTORY_DATA_OFFSET,
			&bin_blk_dir->bin_exists, BLOCK_DIR_TYPE);

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

    if (obj && calc_version(obj->access.rights, obj->specific->domain.usage) >= 0x0500)
        is_50_format = 1;

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

	tag = 0;
	dir_extn_offset = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (BININFO *) 0L;
	while ((*req_mask) && (tag < MAX_BLOCK_TBL_ID)) {

		/*
		 * Check for request mask bit corresponding to the tag value
		 * Skip to next tag value if not requested
		 */

		if (!((*req_mask) & (1L << tag))) {
			tag++;
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		switch (tag++) {

			case BLK_ITEM_TBL_ID:	/* Block Item Table */
				tbl_mask_bit = BLK_ITEM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = BLK_ITEM_REF_OFFSET2;
                else
                    dir_extn_offset = BLK_ITEM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->blk_item_tbl;
				break;

			case BLK_ITEM_NAME_TBL_ID:	/* Block Item Name Table */
				tbl_mask_bit = BLK_ITEM_NAME_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = BLK_ITEM_NAME_REF_OFFSET2;
                else
                    dir_extn_offset = BLK_ITEM_NAME_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->blk_item_name_tbl;
				break;

			case PARAM_TBL_ID:	/* Parameter Table */
				tbl_mask_bit = PARAM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_tbl;
				break;

			case PARAM_MEM_TBL_ID:	/* Parameter Member Table */
				tbl_mask_bit = PARAM_MEM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_MEM_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_MEM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_mem_tbl;
				break;

			case PARAM_MEM_NAME_TBL_ID:	/* Parameter Member Table */
				tbl_mask_bit = PARAM_MEM_NAME_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_MEM_NAME_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_MEM_NAME_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_mem_name_tbl;
				break;

			case PARAM_ELEM_TBL_ID:	/* Parameter Element Table */
				tbl_mask_bit = PARAM_ELEM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_ELEM_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_ELEM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_elem_tbl;
				break;

			case PARAM_LIST_TBL_ID:	/* Parameter List Table */
				tbl_mask_bit = PARAM_LIST_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_LIST_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_LIST_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_list_tbl;
				break;

			case PARAM_LIST_MEM_TBL_ID:	/* Parameter List Member Table */
				tbl_mask_bit = PARAM_LIST_MEM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_LIST_MEM_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_LIST_MEM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_list_mem_tbl;
				break;

			case PARAM_LIST_MEM_NAME_TBL_ID:	/* Parameter List Member Table */
				tbl_mask_bit = PARAM_LIST_MEM_NAME_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = PARAM_LIST_MEM_NAME_REF_OFFSET2;
                else
                    dir_extn_offset = PARAM_LIST_MEM_NAME_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->param_list_mem_name_tbl;
				break;

			case CHAR_MEM_TBL_ID:	/* Characteristic Member Table */
				tbl_mask_bit = CHAR_MEM_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = CHAR_MEM_REF_OFFSET2;
                else
                    dir_extn_offset = CHAR_MEM_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->char_mem_tbl;
				break;

			case CHAR_MEM_NAME_TBL_ID:	/* Characteristic Member Name Table */
				tbl_mask_bit = CHAR_MEM_NAME_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = CHAR_MEM_NAME_REF_OFFSET2;
                else
                    dir_extn_offset = CHAR_MEM_NAME_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->char_mem_name_tbl;
				break;

			case REL_TBL_ID:	/* Relation Table */
				tbl_mask_bit = REL_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = REL_REF_OFFSET2;
                else
                    dir_extn_offset = REL_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->rel_tbl;
				break;

			case UPDATE_TBL_ID:	/* Update Table */
				tbl_mask_bit = UPDATE_TBL_MASK;
				if (is_50_format)
                    dir_extn_offset = UPDATE_REF_OFFSET2;
                else
                    dir_extn_offset = UPDATE_REF_OFFSET;
				bin_table_ptr = &bin_blk_dir->update_tbl;
				break;


			default:	/* goes here for reserved or undefined table IDs */
				break;
		}

		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and it is not zero length.
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {
			local_data_ptr = scrpad->pad + scrpad->used;

			rcode = get_ref_data(obj, scrpad,
				obj_extn_ptr + dir_extn_offset, &tbl_length);

			if (rcode == SUCCESS) {

				/*
				 * Attach the table if non-zero length, else go
				 * to the next table
				 */

				if (tbl_length) {
					bin_table_ptr->chunk = local_data_ptr;
					bin_table_ptr->size = tbl_length;
					bin_blk_dir->bin_hooked |= tbl_mask_bit;
				}
			}
			else {
				return (rcode);
			}
		}
	}			/* end while */

	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name:	fch_rod_dir_spad_size
 *
 *	ShortDesc: Compute needed scratchpad memory size for directory fetch
 *
 *	Description:
 *		This function returns the amount of scratchpad memory required
 *		for a ROD Directory Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		tbl_req_mask -	tables requested for the directory, indicated
 *						by individual bits set for each table
 *						(tables depend on the directory type)
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *		sp_addlsize -	pointer to the value which is the minimum size
 *						of scratchpad memory needed to complete
 *						the fetch request.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INVALID_EXTN_LEN
 *		FETCH_INVALID_DIR_TYPE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_dir_spad_size(ROD_HANDLE rhandle, OBJECT_INDEX obj_index,
	unsigned long *sp_addlsize, unsigned long tbl_req_mask, unsigned short dir_type)
{

	OBJECT         *rod_obj;		/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;	/* pointer to ROD object extension in
									 * scratchpad */
	unsigned char  *local_data_ptr;	/* pointer to current table reference */
	unsigned short  dir_extn_len;	/* length of directory object extension */
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
									 * object extension */
	unsigned char   pad_buf[MAX_OBJ_EXTN_LEN];	/* holds object extension to */
									/* allow calculation of scratchpad size */
	unsigned char	major_rev_number;
	unsigned char	minor_rev_number;
	int             rcode;

	/*
	 * This function or fch_rod_device_dir() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */

	if (dir_type == DEVICE_DIR_TYPE) {
		rcode = get_rev_numbers(rhandle, &major_rev_number, &minor_rev_number);
		if (rcode != SUCCESS) {
			return(rcode);
		}
		if (major_rev_number > DDOD_REVISION_MAJOR) {
			return(DDS_WRONG_DDOD_REVISION);
		}

		/*
		 * This check sends a message to stdout if the minor revision number
		 * in the DDOD doesn't match the one in DDS.  Processing continues.
		 * The print statement is active only when compiler option DEBUG is
		 * used.  Normally, user interface is not part of library functions.
		 */
		if ((major_rev_number==DDOD_REVISION_MAJOR) 
			&& (minor_rev_number > DDOD_REVISION_MINOR)) {
#ifdef DEBUG
			printf("fch_rod_dir_spad_size: DDS/DDOD minor revision mismatch");
#endif /* DEBUG */
		}
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(sp_addlsize, FETCH_INVALID_PARAM);

	TEST_FAIL(ROD_FCH_DIR_SCRATCHPAD_SIZE);

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Use a temporary buffer for the directory object extension
	 */

	local_scrpad.pad = pad_buf;
	local_scrpad.size = sizeof(pad_buf);
	local_scrpad.used = 0L;

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = local_scrpad.pad + local_scrpad.used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory
	 * table reference.  Return if the extension length is not large
	 * enough for the particular type of directory.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	switch (dir_type) {

		case DEVICE_DIR_TYPE:
			if (dir_extn_len > DEVICE_DIR_LEN2) {
				return(FETCH_INVALID_EXTN_LEN);
			}
			break;

		case BLOCK_DIR_TYPE:
			if (dir_extn_len < BLOCK_DIR_LEN) {
				return(FETCH_INVALID_EXTN_LEN);
			}
			break;

		default:
			return(FETCH_INVALID_DIR_TYPE);
	}

	/*
	 * Validate that the object is the correct type of directory.
	 */

	if (*(obj_extn_ptr + 1) != dir_type) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Minimum required scratchpad size is the object length
	 */

	*sp_addlsize += (dir_extn_len + EXTEN_LENGTH_SIZE);

	/*
	 * Calculate the required size for each requested table data block
	 */

	if (dir_type == DEVICE_DIR_TYPE) {

		if(calc_version(rod_obj->access.rights, rod_obj->specific->domain.usage) >= 0x0500)
		{
			if (tbl_req_mask & BLK_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + BLK_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & ITEM_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + ITEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & PROG_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + PROG_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & DOMAIN_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + DOMAIN_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & AXIS_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + AXIS_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & CHART_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + CHART_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & FILE_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + FILE_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & GRAPH_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + GRAPH_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & LIST_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + LIST_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & SOURCE_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + SOURCE_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & WAVEFORM_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + WAVEFORM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & GRID_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + GRID_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & IMAGE_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + IMAGE_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & BINARY_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + BINARY_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & STRING_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + STRING_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & DICT_REF_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + DICT_REF_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}

			if (tbl_req_mask & LOCAL_VAR_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + LOCAL_VAR_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
			}
		}
		else
		{
			if (tbl_req_mask & BLK_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + BLK_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & ITEM_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + ITEM_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & PROG_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + PROG_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & DOMAIN_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + DOMAIN_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & STRING_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + STRING_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & DICT_REF_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + DICT_REF_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}

			if (tbl_req_mask & LOCAL_VAR_TBL_MASK) {
				local_data_ptr = obj_extn_ptr + LOCAL_VAR_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}
	}
	else if (dir_type == BLOCK_DIR_TYPE) {
		if(calc_version(rod_obj->access.rights, rod_obj->specific->domain.usage) >= 0x0500)
		{
		    if (tbl_req_mask & BLK_ITEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + BLK_ITEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & BLK_ITEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + BLK_ITEM_NAME_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_MEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_MEM_NAME_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_ELEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_ELEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_LIST_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_LIST_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & PARAM_LIST_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_NAME_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & CHAR_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + CHAR_MEM_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & CHAR_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + CHAR_MEM_NAME_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & REL_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + REL_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }

		    if (tbl_req_mask & UPDATE_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + UPDATE_REF_OFFSET2;
				*sp_addlsize +=
					  (((unsigned long) local_data_ptr[4]) << 24) 
					| (((unsigned long) local_data_ptr[5]) << 16)
					| (((unsigned long) local_data_ptr[6]) << 8 )
					| (((unsigned long) local_data_ptr[7])      );
		    }
        }
        else
        {
		    if (tbl_req_mask & BLK_ITEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + BLK_ITEM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & BLK_ITEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + BLK_ITEM_NAME_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_MEM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_MEM_NAME_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_ELEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_ELEM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_LIST_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_LIST_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & PARAM_LIST_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_NAME_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & CHAR_MEM_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + CHAR_MEM_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & CHAR_MEM_NAME_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + CHAR_MEM_NAME_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & REL_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + REL_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }

		    if (tbl_req_mask & UPDATE_TBL_MASK) {
			    local_data_ptr = obj_extn_ptr + UPDATE_REF_OFFSET;
			    *sp_addlsize +=
				    (((unsigned short) local_data_ptr[2]) << 8) |
				    (unsigned short) local_data_ptr[3];
		    }
	    }
    }
	else {
		CRASH_RET(FETCH_INVALID_DIR_TYPE);
	}

	if (*sp_addlsize < MAX_OBJ_EXTN_LEN) {
		*sp_addlsize = MAX_OBJ_EXTN_LEN;
	}

	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name:	fch_rod_device_dir
 *
 *	ShortDesc:	Interface for device ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for device directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_dev_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_device_dir(ROD_HANDLE rhandle, SCRATCH_PAD *scrpad,
	unsigned long *sp_addlsize, unsigned long tbl_req_mask,
	BIN_DEVICE_DIR *bin_dev_dir)
{

	OBJECT         *rod_obj;		/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned short  dir_extn_len;	/* extension length (first field in
									 * object extension) */
	unsigned char	major_rev_number;
	unsigned char	minor_rev_number;
	int             rcode;

	/*
	 * This function or fch_rod_dir_spad_size() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */
	 
	rcode = get_rev_numbers(rhandle, &major_rev_number, &minor_rev_number);
	if (rcode != SUCCESS) {
		return(rcode);
	}
	if (major_rev_number > DDOD_REVISION_MAJOR) {
		return(DDS_WRONG_DDOD_REVISION);
	}

	/*
	 * This check sends a message to stdout if the minor revision number
	 * in the DDOD doesn't match the one in DDS.  Processing continues.
	 * The print statement is active only when compiler option DEBUG is
	 * used.  Normally, user interface is not part of library functions.
	 */
	if ((major_rev_number==DDOD_REVISION_MAJOR) 
		&& (minor_rev_number > DDOD_REVISION_MINOR)) {
#ifdef DEBUG
		printf("fch_rod_device_dir: DDS/DDOD minor revision mismatch");
#endif /* DEBUG */
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_dev_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

	TEST_FAIL(ROD_FETCH_DEVICE_DIR);

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Check the available scratchpad size.  If there is not enough space
	 * for a directory object extension, call the size calculation and
	 * return the computed value for the minimum scratchpad size for the
	 * request.
	 */

	if ((scrpad->size - scrpad->used) < MAX_OBJ_EXTN_LEN) {
		rcode = fch_rod_dir_spad_size(rhandle, DEVICE_DIRECTORY_INDEX,
			sp_addlsize, tbl_req_mask, DEVICE_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(rhandle, DEVICE_DIRECTORY_INDEX, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	/*
	 * Return with an error code if the extension length is not
	 * large enough for a device directory object extension.
	 */

	if (dir_extn_len > DEVICE_DIR_LEN2) {
		return (FETCH_INVALID_EXTN_LEN);
	}

	/*
	 * Validate that the object is a device directory.
	 */

	if (*(obj_extn_ptr + 1) != DEVICE_DIR_TYPE) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Call the device directory fetch function
	 */

#ifdef DEBUG
    /*
     * This first check will make sure bin_dev_dir is properly initialized
     * (zeroed out) before the call to fetch_rod_device_dir
     */
	dds_dev_dir_flat_chk(bin_dev_dir, (FLAT_DEVICE_DIR *) 0);
#endif
	rcode = fetch_rod_device_dir(rod_obj, scrpad,
				obj_extn_ptr, bin_dev_dir, &tbl_req_mask);
#ifdef DEBUG
    /*
     * This second check will make sure fetch_rod_device_dir correctly
     * set the data in bin_dev_dir
     */
    dds_dev_dir_flat_chk(bin_dev_dir, (FLAT_DEVICE_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(rhandle, DEVICE_DIRECTORY_INDEX,
			sp_addlsize, tbl_req_mask, DEVICE_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}
	return (rcode);
}


/*********************************************************************
 *
 *	Name:	fch_rod_block_dir
 *
 *	ShortDesc:	Interface for block ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for block directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		block_index -	index of the block directory object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_block_dir(ROD_HANDLE rhandle, OBJECT_INDEX block_index,
	SCRATCH_PAD *scrpad, unsigned long *sp_addlsize, unsigned long tbl_req_mask,
	BIN_BLOCK_DIR *bin_blk_dir)
{

	OBJECT         *rod_obj;		/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned short  dir_extn_len;	/* extension length (first field in
									 * object extension) */
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_blk_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

	TEST_FAIL(ROD_FETCH_BLOCK_DIR);

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Check the available scratchpad size.  If there is not enough space
	 * for a directory object extension, call the size calculation and
	 * return the computed value for the minimum scratchpad size for the
	 * request.
	 */

	if ((scrpad->size - scrpad->used) < MAX_OBJ_EXTN_LEN) {
		rcode = fch_rod_dir_spad_size(rhandle, block_index,
			sp_addlsize, tbl_req_mask, BLOCK_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(rhandle, block_index, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	/*
	 * Return with an error code if the extension length is not
	 * large enough for a block directory object extension.
	 */

	if (dir_extn_len < BLOCK_DIR_LEN) {
		return (FETCH_INVALID_EXTN_LEN);
	}

	/*
	 * Validate that the object is a block directory.
	 */

	if (*(obj_extn_ptr + 1) != BLOCK_DIR_TYPE) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Call the block directory fetch function
	 */

#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (FLAT_BLOCK_DIR *) 0);
#endif
	rcode = fetch_rod_block_dir(rod_obj, scrpad,
		obj_extn_ptr, bin_blk_dir, &tbl_req_mask);
#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (FLAT_BLOCK_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(rhandle, block_index,
			sp_addlsize, tbl_req_mask, BLOCK_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	return (rcode);
}
