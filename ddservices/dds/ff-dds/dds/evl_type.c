/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This file contains all functions relating to type information.
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif
#include <malloc.h>
#include "std.h"
#include "evl_loc.h"
#include "app_xmal.h"
#include "tst_fail.h"

/*
 *  UNKNOWN parameter macros
 */

#define UNKNOWN_TYPE 0
#define UNKNOWN_SIZE 0

/*
 * Constant Size values
 */

#define DEFAULT_SIZE		1
#define BOOLEAN_SIZE		1
#define FLOAT_SIZE			4
#define DURATION_SIZE		6
#define TIME_SIZE			6
#define TIME_VALUE_SIZE		8
#define DATE_AND_TIME_SIZE	7
#define DOUBLE_SIZE			8



/*********************************************************************
 *
 *	Name:	ddl_shrink_range()
 *
 *	ShortDesc:	Condense list containing MIN/MAX values.
 *
 *	Description:
 *		Condense list containing MIN/MAX values.  The range list is
 *		"limit" elements long.  Only "size" number of elements are
 *		needed.  The list is condensed to contain "size" number of
 *		elements.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_shrink_range_list(RANGE_DATA_LIST *list)
{

	TEST_FAIL(DDL_SHRINK_RANGE_LIST);

	if (!list) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (!list->list) {
		ASSERT_DBG(!list->count && !list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (list->count == list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (list->count == 0) {
		ddl_free_range_list(list, FREE_ATTR);
	}
	else {
		list->limit = list->count;

		list->list = (EXPR *) xrealloc((void *) list->list,
			(size_t) (list->limit * sizeof(*list->list)));
		if (!list->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	ddl_free_range_list()
 *
 *	ShortDesc:	Free the list containing MIN/MAX range values.
 *
 *	Description:
 *		Free the list containing MIN/MAX range values.  The MIN/MAX
 *		range value list is no longer needed.  Return the memory used
 *		by "list" to available memory.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

void
ddl_free_range_list(RANGE_DATA_LIST *list, uchar dest_flag)
{

	if (list == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
		list->limit = 0;

	} else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list, and set the values in list.
		 */

		xfree((void **) &list->list);
		list->list = NULL;
		list->limit = 0;
	}

	list->count = 0;
}


/*********************************************************************
 *
 *	Name:	ddl_init_range_list()
 *
 *	ShortDesc:	Initialize the list for storing MIN/MAX range values.
 *
 *	Description:
 *		Initialize the list in which MIN/MAX range values are to be
 *		stored.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - Initialized list for storing MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static void
ddl_init_range_list(RANGE_DATA_LIST *list)
{

	EXPR           *datap;	/* ptr used to init range_data_list */
	EXPR           *endp;	/* range_data_list tail ptr */

	if (list == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
		list->limit = 0;
	}

	endp = &list->list[list->limit];
	for (datap = list->list; datap < endp; datap++) {
		datap->val.i = 0;
		datap->size = 0;
		datap->type = DDS_UNUSED;
	}

	list->count = 0;
}


/*********************************************************************
 *
 *	Name:	ddl_add_value()
 *
 *	ShortDesc:	Add another MIN/MAX value to the MIN/MAX list.
 *
 *	Description:
 *		Add another MIN/MAX value to the MIN/MAX range value list.
 *		If the type of the MIN/MAX value does not match the type
 *		of the variable, the type of the MIN/MAX value is "promoted"
 *		to match the type of the variable.  Type promotion is
 *		performed before the value is added to the list.
 *		  If "var_type" equals UNKNOWN_TYPE.  The value is NOT
 *		promoted to the type of the variable.  The value (along
 *		with its size and type) is added to the list of MIN/MAX
 *		values.
 *
 *	Inputs:
 *		value -	item to be added to list.
 *		var_type - variable type.
 *		var_size - variable size.
 *		position -	location in list where item is to be added.
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		DDL_LARGE_VALUE
 *		return codes from:
 *			a)	ddl_promote_to()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_add_value(EXPR *value, unsigned short var_type, unsigned short var_size,
	DDL_UINT position, RANGE_DATA_LIST *list)
{

	EXPR           *tmp;	/** temporary list pointer */
	EXPR           *endp;	/** ptr to end of min/max list */

	/* Unused parameters */
	(void)var_type;
	(void)var_size;


	ASSERT_DBG(list);

	TEST_FAIL(DDL_ADD_VALUE);

	/*
	 * Add a min or max value to the RANGE_DATA_LIST structure. Expand the
	 * list if necessary, and parse the value. The position parameter
	 * specifies which min/max value (0,1,...) is being referenced.
	 */

	if ((unsigned short) position > list->limit) {
		do {
			list->limit += RANGE_DATA_INC;
		} while ((unsigned short) position > list->limit);
		tmp = (EXPR *) xrealloc((void *) list->list,
			(size_t) (list->limit * sizeof(*tmp)));
		if (!tmp) {
			return DDL_MEMORY_ERROR;
		}

		list->list = tmp;
		endp = &list->list[list->limit];
		for (tmp = &list->list[list->count]; tmp < endp; tmp++) {
			tmp->val.i = 0;
			tmp->size = 0;
			tmp->type = DDS_UNUSED;
		}
		list->count = (unsigned short) position;
	}
	else if (list->count < (unsigned short) position) {
		list->count = (unsigned short) position;
	}

	tmp = &list->list[position - 1];
	if (value) {		/* a real value is to be added to the list */


		/*
		 * Add the value.
		 */

		tmp->size = value->size;
		tmp->type = value->type;
		switch (value->type) {
			case DDS_INTEGER:
				tmp->val.i = value->val.i;
				break;

			case DDS_UNSIGNED:
				tmp->val.u = value->val.u;
				break;

			case DDS_FLOAT:
				tmp->val.f = value->val.f;
				break;

			case DDS_DOUBLE:
				tmp->val.d = value->val.d;
				break;

			default:
				return DDL_ENCODING_ERROR;
		}

	} else {

		/*
		 * If an existing structure was passed in, we must be sure to
		 * write over existing values in the structure.
		 */

		tmp->type = DDS_UNUSED;	/* default values are to be used */
		tmp->size = 0;
		tmp->val.i = 0;
	}

	return DDL_SUCCESS;
}




/*********************************************************************
 *
 *	Name:	ddl_parse_type_size()
 *
 *	ShortDesc:	Parse variable size from binary
 *
 *	Description:
 *		Parse variable size from binary (ie. one-byte integer)
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		typesize - variable size.
 *
 *	Outputs:
 *		typesize - variable size (in bytes)
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 * 			DDL_PARSE_INTEGER()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_parse_type_size(unsigned char **chunkp,DDL_UINT       *size,DDL_UINT       *typesize)
{
	int             rc;

	ASSERT_DBG(chunkp && *chunkp && size && typesize);

	TEST_FAIL(DDL_PARSE_TYPE_SIZE);

	/*
	 * Parse out the length of the item.  The length of an item is encoded
	 * with a first byte of zero, and then the following byte(s) encode the
	 * length.  If the first byte of the chunk is non-zero, then there is
	 * no length encoded, and the default value of 1 is returned.
	 */

	if (*size == 0 || **chunkp) {
		*typesize = DEFAULT_SIZE;
	}
	else {

		/*
		 * Skip the zero, and parse the size.
		 */

		(*chunkp)++;
		(*size)--;

		DDL_PARSE_INTEGER(chunkp, size, typesize);
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	ddl_parse_type()
 *
 *	ShortDesc:	Parse TYPE information from binary.
 *
 *	Description:
 *		Parse TYPE information (including subattribute information)
 *		for the current variable from the binary.  TYPE and subattribute
 *		information is all contained in the same chunk of binary.
 *		Variable type and dependency information are returned.  If a
 *		type value is found (every variable must have a TYPE) "data_valid"
 *		will be set to TRUE.
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		vtype - variable type.
 *		vsize - variable size.
 *		env_info - environment information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		vtype - variable type.
 *		vsize - variable size.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			a)	DDL_PARSE_TAG -
 *			b)	ddl_parse_type_size -
 *			c)	ddl_parse_options -
 *			d)	ddl_enums_choice -
 *			e)	ddl_parse_item_id -
 *			f)	DDL_PARSE_INTEGER -
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_parse_type(unsigned char **chunkp, DDL_UINT *size, TYPEINFO *type,
	OP_REF_LIST *depinfo, int *data_valid, unsigned short *vtype,
	unsigned short *vsize, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	unsigned char  *leave_pointer;	/* chunk ptr for early exit */
	int             rc;
	TYPE_SIZE      *vartype;
	DDL_UINT        len;			/* length of binary associated w/ tag parsed */
	DDL_UINT        tag;			/* parsed tag */
	DDL_UINT        typesize;		/* size of tag item (2-byte int) */

	/* Unused parameters */
	(void)depinfo;
	(void)env_info;
	(void)var_needed;

	/*
	 * This routine evaluates all of the TYPE information for a variable.
	 * Each part of the TYPE field returns a different kind of structure,
	 * so the TYPEINFO structure contains information about the part of the
	 * TYPE field currently being parsed (type.tag), and a pointer to
	 * whatever structure is appropriate.
	 * 
	 * The possible parts of the TYPE field, and their returned structures
	 * are: type.tag					struct TYPE_TAG
	 * AR_DATA_TYPE	(arithmetic type) 	DISPLAY_FORMAT_TAG TRING
	 * EDIT_FORMAT_TAG					STRING
	 * SCALING_FACTOR_TAG				EXPR MIN_VALUE_TAG
	 * ANGE_DATA_LIST MAX_VALUE_TAG		RANGE_DATA_LIST
	 * ENUMERATOR_SEQLIST_TAG			ENUM_VALUE_LIST 0
	 * EFERENCE item_array name)
	 */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_TYPE);

	/*
	 * Parse the tag to find out if this a select statement, an if/else,
	 * or a simple assignment.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	/*
	 * Calcuate the chunk pointer at exit (we may be able to use it later
	 * for an early exit).
	 */

	leave_pointer = *chunkp + len;

	if (type && type->tag == TYPE_TAG) {
		vartype = (TYPE_SIZE *) type->ptr;

	} else {
		vartype = NULL;
	}

	if (vtype) {
		*vtype = (unsigned short) tag;
	}

	switch (tag) {

		case DDS_BOOLEAN_T:
			*vsize = (unsigned short) BOOLEAN_SIZE;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_BOOLEAN_T;
				vartype->size = *vsize;
			}
			break;

		case DDS_INTEGER:
		case DDS_UNSIGNED:		/* UNSIGNED_INTEGER */
			if (len > *size) {
				return DDL_ENCODING_ERROR;
			}
			*size -= len;

		/*
		 * If there is nothing to return (value pointer is NULL), we
		 * can just update the chunk pointer and return.
		 */

			if (!type) {
				*chunkp = leave_pointer;
				return DDL_SUCCESS;
			}

		/*
		 * Find the size of the integer
		 */

			rc = ddl_parse_type_size(chunkp, &len, &typesize);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = (unsigned short) tag;
				vartype->size = *vsize;
			}

			break;

		case DDS_FLOAT:
		case DDS_DOUBLE:
			if (len > *size) {
				return DDL_ENCODING_ERROR;
			}
			*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

			if (!type) {
				*chunkp = leave_pointer;
				return DDL_SUCCESS;
			}

		/*
		 * Save the item type and size
		 */

			if (tag == DDS_FLOAT) {
				*vsize = FLOAT_SIZE;
			} else {
				*vsize = DOUBLE_SIZE;
			}

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = (unsigned short) tag;
				vartype->size = *vsize;
			}


			break;

		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
			if (len > *size) {
				return DDL_ENCODING_ERROR;
			}
			*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

			if (!type) {
				*chunkp = leave_pointer;
				return DDL_SUCCESS;
			}

		/*
		 * Find the size of the enumerated item
		 */

			rc = ddl_parse_type_size(chunkp, &len, &typesize);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = (unsigned short) tag;
				vartype->size = *vsize;
			}


			break;

		case DDS_INDEX:
			if (len > *size) {
				return DDL_ENCODING_ERROR;
			}
			*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

			if (!type) {
				*chunkp = leave_pointer;
				return DDL_SUCCESS;
			}

		/*
		 * Find the size of the index
		 */

			rc = ddl_parse_type_size(chunkp, &len, &typesize);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_INDEX;
				vartype->size = *vsize;
			}


			break;

		case DDS_ASCII:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_ASCII;
				vartype->size = *vsize;
			}
			break;


		case DDS_PASSWORD:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_PASSWORD;
				vartype->size = *vsize;
			}
			break;

		case DDS_BITSTRING:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_BITSTRING;
				vartype->size = *vsize;
			}
			break;

		case DDS_OCTETSTRING:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_OCTETSTRING;
				vartype->size = *vsize;
			}
			break;

		case DDS_VISIBLESTRING:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_VISIBLESTRING;
				vartype->size = *vsize;
			}
			break;


		case DDS_TIME:

			*vsize = (unsigned short) TIME_SIZE;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_TIME;
				vartype->size = *vsize;
			}
			break;

		case DDS_TIME_VALUE:

			*vsize = (unsigned short) TIME_VALUE_SIZE;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_TIME_VALUE;
				vartype->size = *vsize;
			}
			break;

		case DDS_DATE_AND_TIME:

			*vsize = (unsigned short) DATE_AND_TIME_SIZE;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_DATE_AND_TIME;
				vartype->size = *vsize;
			}
			break;

		case DDS_DURATION:		/* DURATION */

			*vsize = (unsigned short) DURATION_SIZE;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_DURATION;
				vartype->size = *vsize;
			}
			break;

		case DDS_EUC:

		/*
		 * Find the size of the item
		 */

			DDL_PARSE_INTEGER(chunkp, size, &typesize);
			*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

			if (vartype) {
				*data_valid = TRUE;
				vartype->type = DDS_EUC;
				vartype->size = *vsize;
			}
			break;

		default:
			return DDL_ENCODING_ERROR;
	}
	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	eval_typeinfo()
 *
 *	ShortDesc:	Evaluates all of the TYPE information for a variable.
 *
 *	Description:
 * 		This routine evaluates all of the TYPE information for a variable.
 * 		Each part of the TYPE field returns a different kind of structure,
 * 		so the TYPEINFO structure contains information about the part of the
 * 		TYPE field currently being parsed (ptag), and a pointer to whatever
 * 		structure is appropriate.
 *
 *		This routine also takes care of import information.
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - size of the binary
 *		ptag - item for which information is desired.
 *		value - value of the desired item.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		value - value of the desired item.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		DDL_MEMORY_ERROR
 *		DDL_DEFAULT_ATTR
 *		return codes from:
 *			a)	ddl_parse_type
 *			b)	ddl_parse_tag_func
 *			c)	DDL_PARSE_INTEGER
 *			d)	ddl_expr_choice
 *			e)	ddl_add_value
 *			f)	ddl_parse_arth_options
 *			g)	ddl_free_output_class
 *			h)	ddl_free_refs
 *			i)	ddl_parse_one_enum
 *			j)	ddl_insert_enum
 *			k)	ddl_shrink_depinfo
 *			l)	ddl_shrink_range_list
 *			m)	ddl_shrink_enum_list
 *			n)	ddl_free_depinfo
 *			o)	ddl_free_range_list
 *			p)	ddl_free_enum_list
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
eval_typeinfo(unsigned char *chunk, DDL_UINT size, int ptag, void *value,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	int             data_valid;	/** if TRUE, value has been modified */
	TYPEINFO        type;		/** local storage for attribute info */
	unsigned short  vtype;		/** variable type */
	unsigned short  vsize;		/** variable size */


	ASSERT_DBG(chunk);

	TEST_FAIL(EVAL_TYPEINFO);

	/*
	 * This routine evaluates all of the TYPE information for a variable.
	 * Each part of the TYPE field returns a different kind of structure,
	 * so the TYPEINFO structure contains information about the part of the
	 * TYPE field currently being parsed (ptag), and a pointer to whatever
	 * structure is appropriate.
	 * 
	 * The possible parts of the TYPE field, and their returned structures
	 * are: ptag						struct TYPE_TAG
	 * AR_DATA_TYPE	(arithmetic type) 	DISPLAY_FORMAT_TAG TRING
	 * EDIT_FORMAT_TAG					STRING
	 * SCALING_FACTOR_TAG				EXPR MIN_VALUE_TAG
	 * ANGE_DATA_LIST MAX_VALUE_TAG		RANGE_DATA_LIST
	 * ENUMERATOR_SEQLIST_TAG		ENUM_VALUE_LIST 0 EFERENCE
	 * item_array name)
	 */

	data_valid = FALSE;

	type.tag = ptag;
	type.ptr = value;

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_parse_type(&chunk, &size, &type, depinfo, &data_valid,
		(unsigned short *) &vtype, &vsize, env_info, var_needed);
	if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR)) {
		goto err_exit;
	}

	/*
	 * Handle any Additions, Redefinitions, and Deletions of the type
	 * attributes.  The way these are handled is determined by the tag type
	 * of the original definition.  So, switch on that tag type.
	 */

	switch (vtype) {
		case IF_TAG:		/* IF */
		case SELECT_TAG:	/* SELECT */
		case DDS_BOOLEAN_T:		/* BOOLEAN */
		case DDS_INDEX:			/* INDEX */
		case DDS_ASCII:			/* ASCII */
		case DDS_PASSWORD:		/* PASSWORD */
		case DDS_BITSTRING:		/* BITSTRING */
		case DDS_OCTETSTRING:	/* OCTETSTRING */
		case DDS_VISIBLESTRING:	/* VISIBLESTRING */
		case DDS_TIME:			/* TIME */
		case DDS_TIME_VALUE:	/* TIME_VALUE */
		case DDS_DATE_AND_TIME:	/* DATE_AND_TIME */
		case DDS_DURATION:		/* DURATION */
		case DDS_EUC:			/* Extended Unix Code */
			break;

		case DDS_INTEGER:		/* INTEGER */
		case DDS_UNSIGNED:		/* UNSIGNED_INTEGER */
		case DDS_FLOAT:			/* FLOAT */
		case DDS_DOUBLE:		/* DOUBLE */

			break;

		case DDS_ENUMERATED:	/* ENUMERATED */
		case DDS_BIT_ENUMERATED:	/* BIT_ENUMERATED */

			break;

		default:
			rc = DDL_ENCODING_ERROR;
			goto err_exit;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	if (value && !data_valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}


/*******************************************************
*
* Name: eval_attr_type
*
* ShortDesc: evaluate variable type.
*
* Description:
*
*	The eval_attr_type function evaluates the datatype
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If type is not a null pointer, the datatype is returned in type.
*	If depinfo is not a null pointer, dependency information about
*	the datatype is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		vartype - variable type.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		vartype - variable type.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_type(unsigned char *chunk, DDL_UINT size, TYPE_SIZE *vartype,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_TYPE);

	if (vartype) {
		vartype->type = 0;	/** initialize output parameter **/
		vartype->size = 0;	/** initialize output parameter **/
	}

	return eval_typeinfo(chunk, size, TYPE_TAG, (void *) vartype,
		depinfo, env_info, var_needed);
}


/**********************************************
*
* Name: eval_attr_display_format
*
* ShortDesc: evaluate the display format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_display_format function evaluates the display format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the display format is returned in
*	format.  If depinfo is not a null pointer, dependency information
*	about the display format is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - display format of variable
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		format - display format of variable
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string(0
*
* Author:
*	Steve Beyerl
*
*****************/

int
eval_attr_display_format(unsigned char *chunk, DDL_UINT size, STRING *format,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	DDL_UINT        tag;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_DISPLAY_FORMAT);

	ddl_free_string(format);


	/*
 	 *	FF code:
	 *    The first byte of an FF chunk will be a DISPLAY_FORMAT_TAG.
	 *	Strip off the DISPLAY_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != DISPLAY_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
}


/**********************************************
*
* Name: eval_attr_edit_format
*
* ShortDesc: evaluate the edit format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_edit_format function evaluates the edit format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the edit format is returned in
*	format.  If depinfo is not a null pointer, dependency information
*	about the edit format is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - edit format of desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		format - edit format of desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string()
*
* Author:
*	Steve Beyerl
*
******************************************************/

int
eval_attr_edit_format(unsigned char *chunk, DDL_UINT size, STRING *format,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	DDL_UINT        tag;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_EDIT_FORMAT);

	ddl_free_string(format);


	/*
 	 *	FF code:
	 *    The first byte of an FF chunk will be a EDIT_FORMAT_TAG.
	 *	Strip off the EDIT_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != EDIT_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
}


/**********************************************
*
* Name: eval_attr_scaling_factor
*
* ShortDesc: evaluate the scaling factor of an arithmetic variable.
*
* Description:
*
*	The eval_attr_scaling_factor function evaluates the scaling factor
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If factor is not a null pointer, the scaling factor is returned in
*	factor.  If depinfo is not a null pointer, dependency information
*	about the scaling factor is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_attr_expr()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_scaling_factor(unsigned char *chunk, DDL_UINT size, EXPR *factor,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	DDL_UINT        tag;


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_SCALING_FACTOR);

	if (factor) {
		(void)memset((char *) factor, 0, sizeof(EXPR));
	}


	/*
 	 *	FF code:
	 *    The first byte of an FF chunk will be a SCALING_FACTOR_TAG.
	 * Strip off the SCALING_FACTOR_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != SCALING_FACTOR_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_attr_expr(chunk, size, factor, depinfo, env_info, var_needed);
}


/**********************************************
*
* Name: eval_attr_min_values
*
* ShortDesc: evaluate the min values of an arithmetic variable
*
* Description:
*	The eval_attr_min_values function evaluates the minimum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the minimum values are returned in
*	values.  If depinfo is not a null pointer, dependency information
*	about the minimum values are returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
* 	Steve Beyerl
*
********************************************************/

int
eval_attr_min_values(unsigned char *chunk, DDL_UINT size, RANGE_DATA_LIST *values,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	int             data_valid;	/** set if "data_value" has been modified **/
	EXPR            data_value;	/* min value parsed from binary */
	DDL_UINT        which;		/* "values" list index */
	DDL_UINT        tag;		/* label associated with current chunk */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_MIN_VALUES);

	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);


	/*
 	 *	FF code:
	 *	  This code was modified to support the current
	 *  status of the DDL spec.  (modified 12/22/93 stevbey)
	 *  The first byte of an FF chunk will be a MIN_VALUE_TAG.
	 *	Strip off the MIN_VALUE_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MIN_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	while (size > 0) {	/* assuming multiple MIN_VALUES */

		/***
		 * Parse a MIN_VALUE_TAG by:
		 * a) get the number of this occurence of MIN_VALUE (ie. which).
		 * 	(i.e. MIN_VALUE1, MIN_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		data_valid = FALSE;

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL,
			(unsigned short) UNKNOWN_TYPE, UNKNOWN_SIZE, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
}


/**********************************************
*
* Name: eval_attr_max_values
*
* ShortDesc: evaluate the max values of an arithmetic variable
*
* Description:
*	The eval_attr_max_values function evaluates the maximum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the maximum values are returned in
*	values.  If depinfo is not a null pointer, dependency information
*	about the maximum values are returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_max_values(unsigned char *chunk, DDL_UINT size, RANGE_DATA_LIST *values,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;
	int             data_valid;	/* set if "data_value" has been modified */
	EXPR            data_value;	/* max value parsed from binary */
	DDL_UINT        which;		/* "values" list index */
	DDL_UINT        tag;		/* label associated with current chunk */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_MAX_VALUES);


	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);


	/*
 	 *	FF code:
	 *	  This code was modified to support the current
	 *  status of the DDL spec.  (modified 12/22/93 stevbey)
	 *  The first byte of an FF chunk will be a MAX_VALUE_TAG.
	 *	Strip off the MAX_VALUE_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MAX_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	while (size > 0) {	/* assuming multiple MAX_VALUES */

		/***
		 * Parse a MAX_VALUE_TAG by:
		 * a) get the number of this occurence of MAX_VALUE.
		 * 	(i.e. MAX_VALUE1, MAX_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		data_valid = FALSE;

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL,
			(unsigned short) UNKNOWN_TYPE, UNKNOWN_SIZE, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
}


/**********************************************
*
* Name: eval_attr_enum
*
* ShortDesc: evaluate the enumeration items of an enumerated variable
*
* Description:
*	The eval_attr_enum function evaluates the enumeration items
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If enums is not a null pointer, the enumeration items are returned
*	in enums.  If depinfo is not a null pointer, dependency information
*	about the enumeration items are returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_enum(unsigned char *chunk, DDL_UINT size, ENUM_VALUE_LIST *enums,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	TYPEINFO        type;
	int             data_valid;
	int             rc;


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_ENUM);

	if (enums) {
		ddl_free_enum_list(enums, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}


	/*
 	 *	FF code:
	 *  The first byte of an FF chunk will be a ENUMERATOR_SEQLIST_TAG.
	 */

	type.tag = ENUMERATOR_SEQLIST_TAG;
	type.ptr = (void *) enums;
	data_valid = FALSE;

	/*
	 * Enumeration values are handled via ddl_enums_choice
	 */

	rc = ddl_enums_choice(&chunk, &size, &type, depinfo, &data_valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);

		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_enum_list(enums);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);
		return rc;
	}

	if (type.ptr && (data_valid == FALSE)) {
		return DDL_DEFAULT_ATTR;
	}

	return rc;

}


/**********************************************
*
* Name: eval_attr_arrayname
*
* ShortDesc: evaluate the item_array name of an index variable.
*
* Description:
*
*	The eval_attr_arrayname function evaluates the item_array name
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If item_arrayref is not a null pointer, the item_array name is
*	returned in item_arrayref.  If depinfo is not a null pointer,
*	dependency information about the item_array name is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*				information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Outputs:
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_arrayname(unsigned char *chunk, DDL_UINT size, ITEM_ID *item_arrayref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	DDL_UINT        tag;
	int             rc;


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_ITEM_ARRAYNAME);

	if (item_arrayref) {
		*item_arrayref = 0;
	}


	/*
 	 *	FF code:
	 *  The first byte of an FF chunk will be a ITEM_ARRAYNAME_TAG.
	 *	Strip off the ITEM_ARRAYNAME_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != ITEM_ARRAYNAME_TAG) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Parse the variable item_array name. INDEX info is NOT conditional.
	 */

	return ddl_parse_item_id(&chunk, &size, item_arrayref, depinfo,
		env_info, var_needed);
}


