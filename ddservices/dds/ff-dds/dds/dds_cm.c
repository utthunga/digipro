/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	dds_cm.c - DDS/Connection Manager DD Support Module
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#ifdef SUN
#include <memory.h>		/* K&R only */
#endif /* SUN */

#include "cm_lib.h"
#include "cm_loc.h"
#include "tags_sa.h"
#include "table.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"


/*
 * Sleazy global variable for explicit DD file loading
 */

static	char	extension[10] ;

int	rod_object_lookup P((ROD_HANDLE, OBJECT_INDEX, OBJECT**));

int	(*build_dd_dev_tbl_fn_ptr) P((DEVICE_HANDLE)) = 0;
int	(*build_dd_blk_tbl_fn_ptr) P((BLOCK_HANDLE)) = 0;
int	(*build_blk_app_info_fn_ptr) P((BLOCK_HANDLE)) = 0 ;
int	(*build_dev_type_app_info_fn_ptr) P((DEVICE_TYPE_HANDLE)) = 0;
int  (*dd_loaded_callback)P((char *fileName)) = 0;
 

/***********************************************************************
 *
 *	Name:  ds_init_dds_tbl_fn_ptrs
 *
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The ds_init_dds_tbl_fn_ptrs function links the pointers of
 *		the DDS functions that the Connection Manager calls when
 *		any DDS tables need to be built.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
ds_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)

{
	/*
	 *	Link the pointers of the functions that build DD device
	 *	and DD block tables (DDS Tables).
	 */

	build_dd_dev_tbl_fn_ptr = dds_tbl_fn_ptrs->build_dd_dev_tbl_fn_ptr;
	build_dd_blk_tbl_fn_ptr = dds_tbl_fn_ptrs->build_dd_blk_tbl_fn_ptr;
}


/***********************************************************************
 *
 *	Name:  ds_init_app_info_fn_ptrs
 *
 *	ShortDesc:  Initialize the Application information function
 *				pointers.
 *
 *	Description:
 *		The ds_init_app_info_fn_ptrs function links the pointers of
 *		the Application functions that the Connection Manager calls
 *		when any Application information needs to be built.
 *
 *	Inputs:
 *		app_info_fn_ptrs - the necessary Application information
 *						   function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
ds_init_app_info_fn_ptrs(APP_INFO_FN_PTRS *app_info_fn_ptrs)

{
	/*
	 *	Link the pointers of the functions that build Application
	 *	information in the Active Block and Active Device Type Tables.
	 */

	build_blk_app_info_fn_ptr = app_info_fn_ptrs->build_blk_app_info_fn_ptr;
	build_dev_type_app_info_fn_ptr =
		app_info_fn_ptrs->build_dev_type_app_info_fn_ptr;
}


/***********************************************************************
 *
 *	Name:  ds_dd_file_search
 *
 *	ShortDesc:  Search for the latest revision of a DD file.
 *
 *	Description:
 *		The ds_dd_file_search function takes a DD Device ID and file
 *		extension and searches for the latest revision of the
 *		corresponding DD file.  The searching procedure used is
 *		limited to series of DD files that have no gaps in their
 *		revision numbers.  If a gap occurs, the DD file with the
 *		revision number before the gap will be returned.  (For
 *		example:  If revision numbers 1, 2, 3, and 5 exist, revision
 *		3 will be returned.)
 *
 *	Inputs:
 *		dd_device_id - The DD Device ID that pertains to the DD file.
 *		file_extension - "out", "sym", "tbl", etc.
 *
 *	Outputs:
 *		dd_rev - the DD file that had the latest revision.
 *
 *	Returns:
 *		A pointer to filename string (if successful).
 *		A null pointer (if not successful).
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static char *
ds_dd_file_search(DD_DEVICE_ID *dd_device_id, char *file_extension, int *dd_rev)
{

	int				dd_rev_num;
	struct stat		sbuf;
	char			filename[256];
	char			last_filename[256];

	(void)strcpy(last_filename, "");

	/*
	 *	Go through each DD revision.
	 */

	for (dd_rev_num = 0; dd_rev_num < 256; dd_rev_num ++) {

		/*
		 *	Obtain the DD file name, and check if the the 
		 *	file exists.
		 */
		(void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
				dd_device_id->ddid_manufacturer,
				dd_device_id->ddid_device_type, 
				dd_device_id->ddid_device_rev,
				dd_rev_num, file_extension);

		if( -1 == access( filename, F_OK ) ) {
			(void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path,
				dd_device_id->ddid_manufacturer,
				dd_device_id->ddid_device_type, 
				dd_device_id->ddid_device_rev,
				dd_rev_num, file_extension);
		}

		if (stat(filename, &sbuf) == 0) {

			/*
			 *	Store the valid DD file name.
			 */

			(void)strcpy(last_filename, filename);

		} else {

			/*
			 *	Check if a prior DD file existed.
			 */
		
			if (strcmp(last_filename, "")) {
				break;
			}
		}
	}

	/*
	 *	Check if no DD files existed.
	 */

	if (!strcmp(last_filename, "")) {
		return((char *)0);
	}

	*dd_rev = dd_rev_num - 1;
	return((char *) strdup(last_filename));
}


/***********************************************************************
 *
 *	Name:  cm_get_integer
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static UINT32
cm_get_integer(UINT8 *bin_ptr, int bin_size)

{
	UINT32	value = 0;
	int		byte_num;

	/*
	 *	Go through each byte and total up the integer value.
	 */

	for (byte_num = 0; byte_num < bin_size; byte_num ++) {

		value <<= 8;
		/* Codecenter thinks values which equal 0xbf have not been set*/
		/*SUPPRESS 113*/
		value += (UINT32)bin_ptr[byte_num];
	}

	return(value);
}

/***********************************************************************
 *
 *	Name:  ds_blk_item_id_to_name
 *
 *	ShortDesc:  Convert an Item ID to block name.
 *
 *	Description:
 *		The ds_blk_item_id_to_name function takes a block name and
 *		obtains the corresponding DD device ID for the
 *		Item ID.
 *
 *	Inputs:
 *		dd_device_id - the DD Device ID for a device.
 *		search_mode - the mode to use to search for the symbol file:
 *					  1. Search for explicit symbol file.
 *							(SM_EXPLICIT_DDREV)
 *					  2. Search for latest symbol file.
 *							(SM_LATEST_DDREV)
 *		block_id - the Item ID of the block.
 *
 *	Outputs:
 *		block_id - the name of the block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_NOT_FOUND.
 *		CM_BAD_FILE_OPEN.
 *		CM_BAD_FILE_CLOSE.
 *		CM_NO_DD_BLK_REF.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_blk_item_id_to_name(DD_DEVICE_ID *dd_device_id, int search_mode,
	char *blk_name, UINT32 *item_id)
{

	char			*filename;
	FILE			*symbol_file;
	int				 dd_rev = 0;
	char			 search_string[50];
	char			 string_buffer[256];
	char			*string_ptr;
	int				 r_code;
	const char *s =  " ";
	char *token;
	char p[256];
	UINT32 local_item_id;

	/*
	 *	Get the name of the latest DDL symbol file.
	 */

	switch (search_mode) {

		case SM_EXPLICIT_DDREV:

			/*
			 *	Get the explicit symbol file using the DD Device ID.
			 */

			filename = (char *) malloc(256);
			(void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path,
					dd_device_id->ddid_manufacturer,
					dd_device_id->ddid_device_type,
					dd_device_id->ddid_device_rev,
					dd_device_id->ddid_dd_rev, "sym");

            // CPMHACK: Manufacturer ID and Device ID can be both in caps or small
            // Adding this condition to handle this case
            if (-1 == access(filename, F_OK)) {
                LOGC_DEBUG(CPM_FF_DDSERVICE, "Explicit (X): Symbol file: %s (sym) not found for the Device %06X/%04X/%02X%02X\n",
                            filename,
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_device_id->ddid_dd_rev);
               (void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
                                    dd_device_id->ddid_manufacturer,
                                    dd_device_id->ddid_device_type,
                                    dd_device_id->ddid_device_rev,
                                    dd_device_id->ddid_dd_rev, "sym");
                if (-1 == access(filename, F_OK)) {
                    LOGC_DEBUG(CPM_FF_DDSERVICE, "Explicit (x): Symbol file: %s (sym) not found for the Device %06x/%04x/%02x%02x\n",
                            filename,
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_device_id->ddid_dd_rev);
                }
                return(CM_FILE_NOT_FOUND);
            }
			break;

		case SM_LATEST_DDREV:

			/*
			 *	Get the latest symbol file using the DD Device ID.
			 */

            filename = ds_dd_file_search(dd_device_id, "sy5", &dd_rev);
			if (!filename) {
                LOGC_DEBUG(CPM_FF_DDSERVICE, "Latest: Symbol file (sy5) not found for the Device %06x/%04x/%02x%02x\n",
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_rev);
                filename = ds_dd_file_search(dd_device_id, "sym", &dd_rev);
			    if (!filename) {
                    LOGC_DEBUG(CPM_FF_DDSERVICE, "Latest: Symbol file (sym) not found for the Device %06x/%04x/%02x%02x\n",
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_rev);
				    return(CM_FILE_NOT_FOUND);
                }
			}
			break;

		default:
			return(CM_BAD_DDOD_LOAD);
	}

	/*
	 *	Initialize the search string using the block name.
	 */

	//(void)strcpy(search_string, " ");
	//(void)strcat(search_string, blk_name);
	//(void)strcat(search_string, " ");

	//(void *)memcpy(search_string, "\0", sizeof(search_string));
	//(void)strcpy(search_string, blk_name);  //jayanth

	//*item_id = 0;

	/*
	 *	Open the symbol file, and read in each line.
	 */

	local_item_id = 0;
	symbol_file = fopen(filename, "r");
	free(filename);
	if (symbol_file == 0) {
		return(CM_BAD_FILE_OPEN);
	}

	while (!feof(symbol_file)) {

		(void) fgets(string_buffer, 256, symbol_file);

		/*
		 *	Check if the line contains a block.
		 */

		if (strstr(string_buffer, "block ")) {

			/*
			 *	Check if the line contains the matching block name.
			 */
			(void *)memcpy(p,string_buffer, sizeof(string_buffer));
			token = strtok(p, s);
			token = strtok(NULL, s);

			strncpy(blk_name, token, 31);

			token = strtok(NULL, s);

			local_item_id = strtoul(token, (char **)0, 16);

			if(local_item_id == *item_id)
			{
				break;
			}
		}
	}

	/*
	 *	Close the DDL symbol file, and check if the block Item Id
	 *	was found.
	 */

	r_code = fclose(symbol_file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}
	if (*item_id == 0) {
		return(CM_NO_DD_BLK_REF);
	}

	return(CM_SUCCESS);
}




/***********************************************************************
 *
 *	Name:  ds_blk_name_to_item_id
 *
 *	ShortDesc:  Convert a block name to an Item ID.
 *
 *	Description:
 *		The ds_blk_name_to_item_id function takes a DD device ID and
 *		block name and obtains the corresponding Item ID for the
 *		block.
 *
 *	Inputs:
 *		dd_device_id - the DD Device ID for a device.
 *		search_mode - the mode to use to search for the symbol file:
 *					  1. Search for explicit symbol file.
 *							(SM_EXPLICIT_DDREV)
 *					  2. Search for latest symbol file.
 *							(SM_LATEST_DDREV)
 *		block_name - the name of the block.
 *
 *	Outputs:
 *		block_id - the Item ID of the block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_NOT_FOUND.
 *		CM_BAD_FILE_OPEN.
 *		CM_BAD_FILE_CLOSE.
 *		CM_NO_DD_BLK_REF.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_blk_name_to_item_id(DD_DEVICE_ID *dd_device_id, int search_mode,
	char *blk_name, UINT32 *item_id)
{

	char			*filename;
	FILE			*symbol_file;
	int				 dd_rev = 0;
	char			 search_string[50];
	char			 string_buffer[256];
	int				 r_code;
	const char *s =  " ";
	char *token;
	char p[256];

	/*
	 *	Get the name of the latest DDL symbol file.
	 */

	switch (search_mode) {

		case SM_EXPLICIT_DDREV:

			/* 
			 *	Get the explicit symbol file using the DD Device ID.
			 */

			filename = (char *) malloc(256);
			(void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path, 
					dd_device_id->ddid_manufacturer,
					dd_device_id->ddid_device_type, 
					dd_device_id->ddid_device_rev,
                    dd_device_id->ddid_dd_rev, "sym");

            // Validation needed for file exitence when searching DD file explicitely
            filename = ds_dd_file_search(dd_device_id, "sym", &dd_rev);
            if (!filename) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "Symbol file not found for the Device %06x/%04x/%02x%02x\n",
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_rev);
                return(CM_FILE_NOT_FOUND);
            }
			break;

		case SM_LATEST_DDREV:

			/*
			 *	Get the latest symbol file using the DD Device ID.
			 */

            filename = ds_dd_file_search(dd_device_id, "sy5", &dd_rev);
			if (!filename) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "Symbol file not found for the Device %06x/%04x/%02x%02x\n",
                            dd_device_id->ddid_manufacturer,
                            dd_device_id->ddid_device_type,
                            dd_device_id->ddid_device_rev,
                            dd_rev);
				return(CM_FILE_NOT_FOUND);
			}
			break;

		default:
			return(CM_BAD_DDOD_LOAD);
	}

	/*
	 *	Initialize the search string using the block name.
	 */

	(void)strcpy(search_string, " ");
	(void)strcat(search_string, blk_name);
	(void)strcat(search_string, " ");

	(void *)memcpy(search_string, "\0", sizeof(search_string));
	(void)strcpy(search_string, blk_name);


	*item_id = 0;

	/*
	 *	Open the symbol file, and read in each line.
	 */

	symbol_file = fopen(filename, "r");
	free(filename);
	if (symbol_file == 0) {
		return(CM_BAD_FILE_OPEN);
	}
	while(fgets(string_buffer,256, symbol_file) != NULL) {
/*	
	while (!feof(symbol_file)) 

		(void) fgets(string_buffer, 256, symbol_file);  commented by jayanth
*/
		/*
		 *	Check if the line contains a block.
		 */

		if (strstr(string_buffer, "block ")) {
			
			/*
			 *	Check if the line contains the matching block name.
			 */
			(void *)memcpy(p,string_buffer, sizeof(string_buffer));
			token = strtok(p, s);
			token = strtok(NULL, s);

			if(0 == strcmp(token, search_string))
			{
				token = strtok(NULL, s);
				*item_id = strtoul(token, (char **)0, 16);
					break;

			}
#if 0
			string_ptr = strstr(string_buffer, search_string);
			if (string_ptr != 0) {

				/*
				 *	Retrieve the block's Item ID.
				 */

				string_ptr += strlen(search_string);
				*item_id = strtoul(string_ptr, (char **)0, 16);
				break;
			}
#endif
		}
	}

	/*
	 *	Close the DDL symbol file, and check if the block Item Id
	 *	was found.
	 */

	r_code = fclose(symbol_file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}
	if (*item_id == 0) {
		return(CM_NO_DD_BLK_REF);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_header
 *
 *	ShortDesc:  Get the DDOD file header from the DDOD file.
 *
 *	Description:
 *		The ds_get_header function takes a DDOD file and gets all of
 *		the header information.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		header - the DDOD file header information. 
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_header(FILE *file, DDOD_HEADER *header)
{

	UINT8			header_buffer[HEADER_SIZE];
	int				r_code;

	/*
	 *	Read the DDOD Header into the buffer.
	 */

	r_code = fread((char *)header_buffer, 1, HEADER_SIZE, file);
	if (r_code != HEADER_SIZE) {
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the DDOD Header, and perform verification
	 *	where applicable.
     */

	header->magic_number = cm_get_integer(
			&header_buffer[MAGIC_NUMBER_OFFSET], MAGIC_NUMBER_SIZE);

	if (header->magic_number != 0x7F3F5F77L) {
		return(CM_FILE_ERROR);
	}

	header->header_size = cm_get_integer(
			&header_buffer[HEADER_SIZE_OFFSET], HEADER_SIZE_SIZE);

	header->objects_size = cm_get_integer(
			&header_buffer[OBJECTS_SIZE_OFFSET], OBJECTS_SIZE_SIZE);

	header->data_size = cm_get_integer(
			&header_buffer[DATA_SIZE_OFFSET], DATA_SIZE_SIZE);

	header->manufacturer = cm_get_integer(
			&header_buffer[MANUFACTURER_OFFSET], MANUFACTURER_SIZE);

	header->device_type = (UINT16) cm_get_integer(
			&header_buffer[DEVICE_TYPE_OFFSET], DEVICE_TYPE_SIZE);

	header->device_revision = (UINT8) cm_get_integer(
			&header_buffer[DEVICE_REV_OFFSET], DEVICE_REV_SIZE);

	header->dd_revision = (UINT8) cm_get_integer(
			&header_buffer[DD_REV_OFFSET], DD_REV_SIZE);

	header->reserved1 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED1_OFFSET], RESERVED1_SIZE);

	header->reserved2 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED2_OFFSET], RESERVED2_SIZE);

	header->reserved3 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED3_OFFSET], RESERVED3_SIZE);

	header->reserved4 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED4_OFFSET], RESERVED4_SIZE);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_dev_od_description
 *
 *	ShortDesc:  Get the OD Description from the Device.
 *
 *	Description:
 *		The ds_get_dev_od_description function takes a Device and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_dev_od_description(DEVICE_HANDLE dh, OD_DESCRIPTION_SPECIFIC *od_description)
{
    int			            r_code;
    NETWORK_HANDLE          nh;
	OD_DESCRIPTION_SPECIFIC oc_desc_specific;

    nh = ADT_NETWORK(dh);

    r_code = get_nt_net_fbap_od(nh, &oc_desc_specific);
    if(r_code != SUCCESS)
    {
        return r_code;
    }

    /*
	 *	Get each field in the OD Description.
     */

	od_description->ram_rom_flag = oc_desc_specific.ram_rom_flag;
	od_description->name_length = oc_desc_specific.name_length;
	od_description->access_protection_flag = oc_desc_specific.access_protection_flag;
	od_description->version = oc_desc_specific.version;
	od_description->stod_object_count = oc_desc_specific.stod_object_count;
	od_description->sod_first_index = oc_desc_specific.sod_first_index;
	od_description->sod_object_count = oc_desc_specific.sod_object_count;
	od_description->dvod_first_index = oc_desc_specific.dvod_first_index;
	od_description->dvod_object_count = oc_desc_specific.dvod_object_count;
	od_description->dpod_first_index = oc_desc_specific.dpod_first_index;
	od_description->dpod_object_count = oc_desc_specific.dpod_object_count;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_od_description
 *
 *	ShortDesc:  Get the OD Description from the DDOD file.
 *
 *	Description:
 *		The ds_get_od_description function takes a DDOD file and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_od_description(FILE *file, OD_DESCRIPTION_SPECIFIC *od_description)
{

    UINT8		od_description_buffer[ODES_SIZE];
    int			r_code;

	/*
	 *	Read the OD Description into the buffer.
	 */

    r_code = fread((char *)od_description_buffer, 1, ODES_SIZE, file);
    if (r_code != ODES_SIZE) {
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the OD Description.
     */

	od_description->ram_rom_flag = (UINT8) cm_get_integer(
			&od_description_buffer[ROM_RAM_FLAG_OFFSET], ROM_RAM_FLAG_SIZE);

	od_description->name_length = (UINT8) cm_get_integer(
			&od_description_buffer[NAME_LENGTH_OFFSET], NAME_LENGTH_SIZE);

	od_description->access_protection_flag = (UINT8) cm_get_integer(
			&od_description_buffer[ACCSS_PROTECT_OFFSET], ACCSS_PROTECT_SIZE);

	od_description->version = (UINT16) cm_get_integer(
			&od_description_buffer[VERSION_OD_OFFSET], VERSION_OD_SIZE);

	od_description->stod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[STOD_LENGTH_OFFSET], STOD_LENGTH_SIZE);

	od_description->sod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_SOD_OFFSET], FIRST_IN_SOD_SIZE);

	od_description->sod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[SOD_LENGTH_OFFSET], SOD_LENGTH_SIZE);

	od_description->dvod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DVOD_OFFSET], FIRST_IN_DVOD_SIZE);

	od_description->dvod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[DVOD_LENGTH_OFFSET], DVOD_LENGTH_SIZE);

	od_description->dpod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DPOD_OFFSET], FIRST_IN_DPOD_SIZE);

	od_description->dpod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[DPOD_LENGTH_OFFSET], DPOD_LENGTH_SIZE);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_object
 *
 *	ShortDesc:  Get an object from the DDOD file.
 *
 *	Description:
 *		The ds_get_object function takes a DDOD file and ROD handle
 *		and gets an object from the file and stores it into the
 *		corresponding ROD.
 *
 *	Inputs:
 *		file - the DDOD file.
 *		rod_handle - the handle of the ROD in which the object will
 *					 be stored.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_object(FILE *file, ROD_HANDLE rod_handle)
{

	UINT8		 object_buffer[FIXED_SIZE];
	OBJECT		*object;
	UINT8		 extension_length_buffer[EXTEN_LENGTH_SIZE];
	UINT8		 extension_length;
	int			 r_code;

	/*
	 *	Allocate space in the ROD heap for an object, and for a
	 *	domain object's specific information.
	 */

	object = (OBJECT *)rod_calloc(1, sizeof(OBJECT));
	if (!object) {
		return(CM_NO_ROD_MEMORY);
	}

	object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object->specific) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Read the fixed fields of the (domain) object into the buffer.
	 */

    r_code = fread((char *)object_buffer, 1, FIXED_SIZE, file);
    if (r_code != FIXED_SIZE) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get each field in the (domain) object, and perform
	 *	verification where applicable.
	 */

	object->index = (OBJECT_INDEX) cm_get_integer(
			&object_buffer[OBJECT_INDEX_OFFSET], OBJECT_INDEX_SIZE);

	object->code = (UINT8) cm_get_integer(
			&object_buffer[OBJECT_CODE_OFFSET], OBJECT_CODE_SIZE);

	if (object->code != OC_DOMAIN) {
		return(CM_FILE_ERROR);
	}

	object->access.password = (UINT8) cm_get_integer(
			&object_buffer[PASSWORD_OFFSET], PASSWORD_SIZE);

	object->access.groups = (UINT8) cm_get_integer(
			&object_buffer[ACCESS_GROUP_OFFSET], ACCESS_GROUP_SIZE);

	object->access.rights = (UINT16) cm_get_integer(
			&object_buffer[ACCESS_RIGHTS_OFFSET], ACCESS_RIGHTS_SIZE);

	/* 
	 * If password or group contains a value other than 0
	 * the DD is in the new large format. These 2 fields are not used
	 * so instead we are using them as storage for the enlaged DDO information.
	 */

	object->specific->domain.local_address = (UINT32) cm_get_integer(
			&object_buffer[LOCAL_ADDRESS_OFFSET], LOCAL_ADDRESS_SIZE);

	object->specific->domain.state = (UINT8) cm_get_integer(
			&object_buffer[DOMAIN_STATE_OFFSET], DOMAIN_STATE_SIZE);

	object->specific->domain.upload_state = (UINT8) cm_get_integer(
			&object_buffer[UPLOAD_STATE_OFFSET], UPLOAD_STATE_SIZE);

	object->specific->domain.usage = (UINT8) cm_get_integer(
			&object_buffer[COUNTER_OFFSET], COUNTER_SIZE);

	if(calc_version(object->access.rights, object->specific->domain.usage) >= 0x0500)
	{
		object->specific->domain.size = (UINT32) cm_get_integer(
				&object_buffer[MAX_OCTETS_OFFSET], MAX_OCTETS_SIZE2);
	}
	else
	{
		object->specific->domain.size = (UINT16) cm_get_integer(
				&object_buffer[MAX_OCTETS_OFFSET], MAX_OCTETS_SIZE);
	}

    /*
	 *	Read the extension length field of the (domain) object
	 *	into the buffer.
	 */

    r_code = fread((char *)extension_length_buffer, 1, EXTEN_LENGTH_SIZE, file);
    if (r_code != EXTEN_LENGTH_SIZE) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get the extension length field and allocate space in the
	 *	ROD heap for the (domain) object's extension.
	 */

	extension_length = (UINT8) cm_get_integer(
		&extension_length_buffer[EXTEN_LENGTH_OFFSET], EXTEN_LENGTH_SIZE);

	object->extension = (UINT8 *) rod_malloc(
			(int)(EXTEN_LENGTH_SIZE + extension_length));
	if (!object->extension) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the first field in the extension to the length, and
	 *	read in the remaining part of the extension.
	 */

	object->extension[EXTEN_LENGTH_OFFSET] = extension_length;

	r_code = fread((char *)(object->extension + 1), 1,
			(int)extension_length, file);
	if (r_code != extension_length) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Put the (domain) object into the ROD.
	 */

	r_code = rod_put(rod_handle, object->index, object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_object_value
 *
 *	ShortDesc:  Get an object's value.
 *
 *	Description:
 *		The ds_get_object_value function takes a DDOD file, DDOD file
 *		header, and object and gets the objects value.  It will attach
 *		the object's value to the object (both being in the ROD).
 *
 *	Inputs:
 *		file - the DDOD file.
 *		header - the header of the DDOD file.
 *		object - the object whose value will be retrieved.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_object_value(FILE *file, DDOD_HEADER *header, OBJECT *object)

{
	UINT32		 offset;
	UINT8		*domain_data;
	int			 r_code;

	/*
	 *	Check if there is no object to get a value for, or no
	 *	domain data to get.
	 */

	if (!object) {
		return(CM_SUCCESS);
	}
	if (!object->specific) {
		return(CM_BAD_OBJECT);
	}
	if (!object->specific->domain.size) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}
	if (object->specific->domain.local_address == 0xFFFFFFFF) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}

	/*
	 *	Calculate the offset into the DDOD file where the domain
	 *	data is located.
	 */

	offset = object->specific->domain.local_address +
			header->header_size + header->objects_size;

	/*
	 *	Allocate space in the ROD heap for the domain data.
	 */

	domain_data = (UINT8 *)rod_malloc((int)object->specific->domain.size);
	if (!domain_data) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the file position and read in the domain data.
	 */

	r_code = fseek(file, (long)offset, 0);
	if (r_code < 0) {
		return(CM_FILE_ERROR);
	}
	r_code = fread((char *)domain_data, 1,
			(int)object->specific->domain.size, file);
	
	if (r_code != (int)object->specific->domain.size) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Connect the domain data to the ROD object.
	 */

	object->value = domain_data;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_ddod_file_load
 *
 *	ShortDesc:  Load a DDOD file.
 *
 *	Description:
 *		The ds_ddod_file_load function takes a DDOD file name and loads
 *		the corresponding DDOD file into a ROD.
 *
 *	Inputs:
 *		filename - the name of the DDOD file that is to be loaded.
 *
 *	Outputs:
 *		rod_handle - the handle of the ROD in which the DDOD was
 *					 loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_ddod_file_load(char *filename, ROD_HANDLE *rod_handle, DEVICE_HANDLE device_handle)
{

	FILE						*file;
	DDOD_HEADER					 header;
	OBJECT						*object_0;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 object_num;
	int							 r_code;
    NET_TYPE                     net_type;

	/*
	 *	Open the DDOD file and get the Header information.
	 */

	file = fopen(filename, "rb");
	if (!file) {
		return(CM_BAD_FILE_OPEN);
	}

	r_code = ds_get_header(file, &header);
	if (r_code != CM_SUCCESS) {
		fclose(file);
		return(r_code);
	}

	/*
	 *	Allocate space in the ROD heap for an object, and set it up
	 *	as an OD description object.
	 */

	object_0 = (OBJECT *)rod_calloc(1, sizeof(OBJECT));
	if (!object_0) {
		fclose(file);
		return(CM_NO_ROD_MEMORY);
	}

	object_0->index = 0;
	object_0->code = OC_OD_DESCRIPTION;

	/*
	 *	Allocate space in the ROD heap for an OD description object's
	 *	specific information, and set it up to describe the DDROD.
	 */

	object_0->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object_0->specific) {
		fclose(file);
		return(CM_NO_ROD_MEMORY);
	}
	od_description = (OD_DESCRIPTION_SPECIFIC *)object_0->specific;


	
    net_type = ADT_NET_TYPE(device_handle);
	
    switch (net_type) {

		case NT_A:

			/*
			 * Get the OD header from the Device
			 */

			r_code = ds_get_od_description (file, od_description);
			if (r_code != CM_SUCCESS) {
				fclose(file);
				return(r_code);
			}
			break ;

		case NT_A_SIM:
		case NT_OFF_LINE:  //LNT

			/* Get Network OD_Description from the DDOD file */
			r_code = ds_get_od_description(file, od_description);
			if (r_code != CM_SUCCESS) {
				fclose(file);
				return(r_code);
			}
			break;
		}

	/*End */



	/*
	 *	Open up the DDROD with object 0.
	 */

	*rod_handle = rod_open(object_0);
	if (*rod_handle < 0) {
		fclose(file);
		return(CM_BAD_ROD_OPEN);
	}

	/*
	 *	Load all of the objects in the S-OD.  Note that these will
	 *	all be domain objects.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object(file, *rod_handle);
		if (r_code != CM_SUCCESS) {
			fclose(file);
			return(r_code);
		}
	}

	/*
	 *	Load all of the object's values in the S-OD.  Note that these
	 *	will all be domain data.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object_value(file, &header, 
				rod_tbl.list[*rod_handle]->sod[object_num]);
		if (r_code != CM_SUCCESS) {
			fclose(file);
			return(r_code);
		}
	}

	r_code = fclose(file);
	if (r_code) {
		fclose(file);
		return(CM_BAD_FILE_CLOSE);
	}

	return(CM_SUCCESS);
}


/* Return the filename extension given a specific DD Tokenizer format major version */
static char *get_ext(int for_version)
{
    switch(for_version)
    {
    case 5: return "ff5";
    case 6: return "ff6";
    case 7: return "ff7";
    case 8: return "ff8";
    case 9: return "ff9";
    default:
        return "ffo";
    }
}

/* Check for the existence of a file */
static int file_exists(char *fname)
{
    FILE *fp;
    if (!fname)
        return 0;
    fp = fopen(fname, "rb");
    if (!fp)
        return 0;
    fclose(fp);
    return 1;
}

/***********************************************************************
 *
 *	Name:  ds_ddod_load
 *
 *	ShortDesc:  Load a DDOD.
 *
 *	Description:
 *		The ds_ddod_load function takes a device handle and search
 *		mode, and loads the corresponding DDOD into a ROD.  Only DDOD
 *		file loading is supported (currently there is no support for
 *		obtaining a DDOD over a network).
 *      The function will call a callback to report the correct filename
 *      if the load was successful, and if the callback pointer is 
 *      non-NULL.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *		search_mode - the mode to use to search for the DDOD file:
 *					  1. Search for explicit DDOD file.
 *							(SM_EXPLICIT_DDREV)
 *					  2. Search for latest DDOD file.
 *							(SM_LATEST_DDREV)
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_FILE_NOT_FOUND.
 *		Return values from ddod_file_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_ddod_load(DEVICE_HANDLE device_handle, int search_mode, DD_HANDLE *dd_handle)
{

	char				*filename = NULL;
	ROD_HANDLE			 rod_handle;
	int					 dd_rev = 0;
	DD_DEVICE_ID		*dd_device_id;
	DD_HANDLE			 temp_dd_handle;
	int					 r_code;
	char				*ext ;
	NET_TYPE			net_type ;
    int                 cur_version = 5;

	ext = "";

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!VALID_DEVICE_HANDLE(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	dd_device_id = (DD_DEVICE_ID *)ADT_DD_DEVICE_ID(device_handle);

	switch (search_mode) {

		case SM_EXPLICIT_FFO:

			/*
			 *	Get the explicit DDOD file using the DD Device ID.
             *  Search for a current-version binary first.  If that fails,
             *  then check for older-format binaries.
			 */

			filename = (char *) malloc(256);
			net_type = ADT_NET_TYPE(device_handle) ;
            {
 			    ext = "ffo";
			    (void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path, 
					dd_device_id->ddid_manufacturer,
					dd_device_id->ddid_device_type, 
					dd_device_id->ddid_device_rev,
					dd_device_id->ddid_dd_rev, ext);

                // CPMHACK: Manufacturer ID and Device ID can be both in caps or small
                // Adding this condition to handle this case
                if( -1 == access( filename, F_OK ) ) {
                    (void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
                                    dd_device_id->ddid_manufacturer,
                                    dd_device_id->ddid_device_type,
                                    dd_device_id->ddid_device_rev,
                                    dd_device_id->ddid_dd_rev, ext);
                }
            }
			break;

		case SM_EXPLICIT_FF5:

			/*
			 *	Get the explicit DDOD file using the DD Device ID.
             *  Search for a current-version binary first.  If that fails,
             *  then check for older-format binaries.
			 */

			filename = (char *) malloc(256);
			net_type = ADT_NET_TYPE(device_handle) ;
            {
 			    ext = "ff5";
			    (void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path, 
					dd_device_id->ddid_manufacturer,
					dd_device_id->ddid_device_type, 
					dd_device_id->ddid_device_rev,
					dd_device_id->ddid_dd_rev, ext);

                // CPMHACK: Manufacturer ID and Device ID can be both in caps or small
                // Adding this condition to handle this case
                if( -1 == access( filename, F_OK ) ) {
                    (void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
                                    dd_device_id->ddid_manufacturer,
                                    dd_device_id->ddid_device_type,
                                    dd_device_id->ddid_device_rev,
                                    dd_device_id->ddid_dd_rev, ext);
                }
            }
			break;

		case SM_EXPLICIT_DDREV:

			/*
			 *	Get the explicit DDOD file using the DD Device ID.
             *  Search for a current-version binary first.  If that fails,
             *  then check for older-format binaries.
			 */

			filename = (char *) malloc(256);
			net_type = ADT_NET_TYPE(device_handle) ;
            {
                int v;
                for (v = cur_version; v >= 4; v--)
                {
			        if ((net_type == NT_A) || (net_type == NT_A_SIM)
			        		|| (net_type == NT_OFF_LINE)) {
				        ext = get_ext(v);
			        }
			        else {
				        ext = "fms";
			        }
			        (void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path, 
					    dd_device_id->ddid_manufacturer,
					    dd_device_id->ddid_device_type, 
					    dd_device_id->ddid_device_rev,
					    dd_device_id->ddid_dd_rev, ext);

                    // CPMHACK: Manufacturer ID and Device ID can be both in caps or small
                    // Adding this condition to handle this case
                    if( -1 == access( filename, F_OK ) ) {
                        (void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
                                    dd_device_id->ddid_manufacturer,
                                    dd_device_id->ddid_device_type,
                                    dd_device_id->ddid_device_rev,
                                    dd_device_id->ddid_dd_rev, ext);
                    }

                    if (file_exists(filename))
                    {
                        break;
                    }
                }
            }
			break;

		case SM_LATEST_DDREV:

			/*
			 *	Get the latest DDOD file using the DD Device ID.
             *  If we can't find any binary at all that is the current format,
             *  begin to look for older format binaries, starting with the 
             *  most recent.
			 */

			net_type = ADT_NET_TYPE(device_handle) ;
            {
                int v;
                for (v = cur_version; v >= 4; v--)
                {
			        if ((net_type == NT_A) || (net_type == NT_A_SIM) ||
						(net_type == NT_OFF_LINE)) {
				        ext = get_ext(v);
			        }
			        else {
				        ext = "fms";
			        }
        			filename = ds_dd_file_search(dd_device_id, ext, &dd_rev);
			        if (filename) {
                        break;
                    }
                }
				if ((v < 4) && (!filename))
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE, "Failed to get DD binary for the Device ID %d with Revision %d\n",
                               dd_device_id, dd_rev);
                    return(CM_FILE_NOT_FOUND);
                }
			}
			break;

		default:
			return(CM_BAD_DDOD_LOAD);
	}

	/*
	 *	Load the DDOD file into a ROD.
	 */

	r_code = ds_ddod_file_load(filename, &rod_handle, device_handle);
	if(r_code != CM_SUCCESS) {
	free(filename);
		return(r_code);
	}

	/*
	 *	If we were searching for the latest DD file, put the
	 *	DD Revision into the Active Device Type Table.
	 */

	if (search_mode == SM_LATEST_DDREV) {
		dd_device_id->ddid_dd_rev = (UINT8)dd_rev;
	}

	/*
	 *	Put the DD Handle into the Active Device Type Table.
	 */

	temp_dd_handle.dd_handle_type = DDHT_ROD;
	temp_dd_handle.dd_handle_number = rod_handle;
	SET_ADT_DD_HANDLE(device_handle, &temp_dd_handle);
	(void) memcpy((char *)dd_handle, (char *)&temp_dd_handle,
			sizeof(DD_HANDLE));

    /* If the DD load callbacks have been initialized, tell the caller what filename we loaded */
    if (dd_loaded_callback)
    {
        (*dd_loaded_callback)(filename);
    }

	free(filename);
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_flat_load
 *
 *	ShortDesc:  Load a DD Flat file.
 *
 *	Description:
 *		The ds_dd_flat_load function takes a device handle and search
 *		mode, and loads the corresponding DD Flat file.  (Currently DD
 *		Flat files are not yet supported, so this function only serves
 *		as a stub.)
 *
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *		search_mode - the mode to use to search for the DD Flat file:
 *					  1. Search for explicit DD Flat file.
 *							(SM_EXPLICIT_DDREV)
 *					  2. Search for latest DD Flat file.
 *							(SM_LATEST_DDREV)
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_FLAT_LOAD.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_dd_flat_load(DEVICE_HANDLE device_handle, int search_mode, DD_HANDLE *dd_handle)
{

	FLAT_HANDLE		flat_handle = -1;
	int				supported = 0;
	DD_HANDLE		*dd_handle_ptr;

	/*
	 *	Flat files are not yet supported.
	 */

	if (supported) {

		if (!VALID_DEVICE_HANDLE(device_handle)) {
			return(CM_BAD_DEVICE_HANDLE);
		}

		switch (search_mode) {

		case SM_EXPLICIT_DDREV:
			break;

		case SM_LATEST_DDREV:
			break;

		default:
			return(CM_BAD_DD_FLAT_LOAD);
		}

		dd_handle_ptr = (DD_HANDLE *)ADT_DD_HANDLE(device_handle);
		dd_handle_ptr->dd_handle_type = DDHT_FLAT;
		dd_handle_ptr->dd_handle_number = flat_handle;

		(void) memcpy((char *)dd_handle,
				(char *)ADT_DD_HANDLE(device_handle), sizeof(DD_HANDLE));

		return(CM_SUCCESS);

	} else {

		return(CM_BAD_DD_FLAT_LOAD);
	}
}


/***********************************************************************
 *
 *	Name:  ds_dd_device_load
 *
 *	ShortDesc:  Load a DD for a device.
 *
 *	Description:
 *		The ds_dd_device_load function takes a device handle and loads
 *		the corresponding DD for the device.  After the DD is loaded,
 *		the DD device tables will be built.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_DEV_LOAD.
 *		Return values from ds_ddod_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
int dd_search_mode=SM_LATEST_DDREV;

int
ds_dd_device_load(DEVICE_HANDLE device_handle, DD_HANDLE *dd_handle)
{

	int		r_code;

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!VALID_DEVICE_HANDLE(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	/*
	 *	Check if a DD Flat file can be loaded first.
	 */

	r_code = ds_dd_flat_load(device_handle, dd_search_mode, dd_handle);
	if (r_code != CM_SUCCESS) {

		/*
		 *	Otherwise, load a DDOD.
		 */

		r_code = ds_ddod_load(device_handle, SM_EXPLICIT_DDREV, dd_handle);
		if (r_code != CM_SUCCESS) {
		    r_code = ds_ddod_load(device_handle, SM_LATEST_DDREV, dd_handle);
            if (r_code != CM_SUCCESS) {
			    return(r_code);
            }
		}
	}



	/*
	 *	Build the DD device tables.
	 */

	if (build_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*build_dd_dev_tbl_fn_ptr)(ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(r_code);
		}
	}

	/*
	 *	Build any device type specific Application information.
	 */

	if (build_dev_type_app_info_fn_ptr != 0) {
		r_code = (*build_dev_type_app_info_fn_ptr)(ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(CM_BAD_DD_DEV_LOAD);
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_block_load
 *
 *	ShortDesc:  Load a DD for a block.
 *
 *	Description:
 *		The ds_dd_block_load function takes a block handle and loads
 *		the corresponding DD for the block.  Loading a DD for a block
 *		consists of first loading the DD for the device (if not already
 *		done), and then building the DD block tables.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose DD is to be
 *					   loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_NO_DD_BLK_REF.
 *		CM_BAD_DD_BLK_LOAD.
 *		Return values from ds_dd_device_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_dd_block_load(BLOCK_HANDLE block_handle, DD_HANDLE *dd_handle)
{

	int			r_code;
 
	

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that there is a DD Block ID.
	 */

	if (ABT_DD_BLK_ID(block_handle) == 0) {
		return(CM_NO_DD_BLK_REF);
	}

	/*
	 *	Check if the DD has been loaded for the device.
	 */

	if (!VALID_DD_HANDLE((ABT_DD_HANDLE(block_handle)))) {
		r_code = ds_dd_device_load(ABT_ADT_OFFSET(block_handle), dd_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Build the DD block tables.
	 */

	if (build_dd_blk_tbl_fn_ptr != 0) {
		r_code = (*build_dd_blk_tbl_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}
	
	/*
	 *	Build any block specific Application information.
	 */

	if (build_blk_app_info_fn_ptr != 0) {
		r_code = (*build_blk_app_info_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}

	(void) memcpy((char *)dd_handle,
			(char *)ABT_DD_HANDLE(block_handle), sizeof(DD_HANDLE));

	return CM_SUCCESS;
}


/***********************************************************************
 *
 *	Name:  ds_dd_device_load_ex
 *
 *	ShortDesc:  Load a DD for a device (explicit DD revision used).
 *
 *	Description:
 *		The ds_dd_device_load_ex function takes a device handle and
 *		loads the corresponding DD for the device.  After the DD is
 *		loaded, the DD device tables will be built.
 *
 *		NOTE:	The DD that will be loaded is determined by the entire
 *				(4 number) Device ID in the Active Device Type Table.
 *				That is, no search will be performed for the latest DD.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_DEV_LOAD.
 *		Return values from ds_ddod_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_dd_device_load_ex(DEVICE_HANDLE device_handle, char *ext, DD_HANDLE *dd_handle)
{

	int		r_code;

	/*
	 * Copy the extension into the global extension for later
	 */

	(void)strcpy(extension, ext) ;

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!VALID_DEVICE_HANDLE(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	/*
	 *	Check if a DD Flat file can be loaded first.
	 */

	r_code = ds_dd_flat_load(device_handle, SM_EXPLICIT_DDREV, dd_handle);
	if (r_code != CM_SUCCESS) {

		/*
		 *	Otherwise, load a DDOD.  Try loading the explicit DDOD.
		 *	If this does not work, try loading the latest DDOD.
		 */
		r_code = ds_ddod_load(device_handle, SM_EXPLICIT_DDREV, dd_handle);
		if (r_code != CM_SUCCESS) {
		    r_code = ds_ddod_load(device_handle, SM_LATEST_DDREV, dd_handle);
            if (r_code != CM_SUCCESS) {
			    return(r_code);
            }
        }
	}

	/*
	 *	Build the DD device tables.
	 */

	if (build_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*build_dd_dev_tbl_fn_ptr)(ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(CM_BAD_DD_DEV_LOAD);
		}
	}

	/*
	 *	Build any device type specific Application information.
	 */

	if (build_dev_type_app_info_fn_ptr != 0) {
		r_code = (*build_dev_type_app_info_fn_ptr)(ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(CM_BAD_DD_DEV_LOAD);
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_block_load_ex
 *
 *	ShortDesc:  Load a DD for a block (explicit DD revision used).
 *
 *	Description:
 *		The ds_dd_block_load_ex function takes a block handle and loads
 *		the corresponding DD for the block.  Loading a DD for a block
 *		consists of first loading the DD for the device (if not already
 *		done), and then building the DD block tables.
 *
 *		NOTE:	The DD that will be loaded is determined by the entire
 *				(4 number) Device ID in the Active Device Type Table.
 *				That is, no search will be performed for the latest DD.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose DD is to be
 *					   loaded.
 *		ext			 - the extension of the file to load
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_NO_DD_BLK_REF.
 *		CM_BAD_DD_BLK_LOAD.
 *		Return values from ds_dd_device_load_ex function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_dd_block_load_ex(BLOCK_HANDLE block_handle, char *ext, DD_HANDLE *dd_handle)
{

	int			r_code;

	/*
	 * Copy the extension into the global extension for later
	 */

	(void)strcpy(extension, ext) ;

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that there is a DD Block ID.
	 */

	if (ABT_DD_BLK_ID(block_handle) == 0) {
		return(CM_NO_DD_BLK_REF);
	}

	/*
	 *	Check if the DD has been loaded for the device.
	 */

	if (!VALID_DD_HANDLE((ABT_DD_HANDLE(block_handle)))) {
		r_code = ds_dd_device_load_ex(ABT_ADT_OFFSET(block_handle),
				ext, dd_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Build the DD block tables.
	 */

	if (build_dd_blk_tbl_fn_ptr != 0) {
		r_code = (*build_dd_blk_tbl_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}
	
	/*
	 *	Build any block specific Application information.
	 */

	if (build_blk_app_info_fn_ptr != 0) {
		r_code = (*build_blk_app_info_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}

	(void) memcpy((char *)dd_handle,
			(char *)ABT_DD_HANDLE(block_handle), sizeof(DD_HANDLE));

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get
 *
 *	ShortDesc:  Get an object from a ROD.
 *
 *	Description:
 *		The rod_get function takes a ROD handle and object index and
 *		gets the corresponding object.  The (requested) object output
 *		is a pointer to an object.  This object and any of its
 *		allocated contents reside in the ROD.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the requested object.
 *
 *	Outputs:
 *		object - the requested object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_OBJECT.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)

{
	int		r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object.
	 */

	r_code = rod_object_lookup(rod_handle, object_index, object);
	if (r_code != CM_SUCCESS) {
		return (r_code);
	}

	/*
	 *	Check to make sure that the object exists.
	 */

	if (!*object) {
		return (CM_NO_OBJECT);
	}

    /* Check the object version and update the ROD version if necessary */
    rod_set_version(rod_handle, calc_version((*object)->access.rights,
        (*object)->specific->domain.usage));


	return(CM_SUCCESS);
}

int locate_symbol_id(ENV_INFO2 *env, char *sym_name, ITEM_ID *out_id)
{
    FILE *fp;
    DD_DEVICE_ID *dd_device_id;
    char str[999], file_name_str[512];

//    *out_id = 0x200A3;
    if (env->type == ENV_DEVICE_HANDLE)
	    dd_device_id = (DD_DEVICE_ID *)ADT_DD_DEVICE_ID(env->device_handle);
    else
	    dd_device_id = (DD_DEVICE_ID *)ABT_DD_DEVICE_ID(env->block_handle);

    if (find_symbol_file(dd_device_id, file_name_str, sizeof(file_name_str)))
        return DDL_FILE_NOT_FOUND;

    fp = fopen(file_name_str, "r");
    if (!fp)
        return DDL_FILE_NOT_FOUND;

    while (fgets(str, sizeof(str), fp))
    {
        /* Parse the lines */
        /* Lines have either 3 or 4 entries */
        ITEM_ID id;
        char sym_in_file[255], dum2[255], dum3[255];
        if (((sscanf(str, "%s %s %s 0x%lx", dum2, sym_in_file, dum3, &id)) == 4) ||
           ((sscanf(str, "%s %s 0x%lx", dum2, sym_in_file, &id)) == 3))
        {
            /* Line successfully parsed.  Is this our symbol? */
            if (!strcmp(sym_name, sym_in_file))
            {
                /* Yes: record its ID and return */
                *out_id = id;
                fclose(fp);
                return 0;
            }
        }
    }

    fclose(fp);
    return CM_BAD_SYMBOL;
}

int find_symbol_file(DD_DEVICE_ID *dd_device_id, char *out_filename, int out_filename_size)
{
    FILE *fp;
    char *filename, *ext;
    int ver;
	filename = (char *) malloc(256);

    for (ver = 5; ver >= 4; ver--)
    {
        switch(ver)
        {
        case 5:
 	        ext = "sy5"; break;
        default:
 	        ext = "sym"; break;
        }

		(void)sprintf(filename, "%s/%06X/%04X/%02X%02X.%s", dd_path, 
			dd_device_id->ddid_manufacturer,
			dd_device_id->ddid_device_type, 
			dd_device_id->ddid_device_rev,
			dd_device_id->ddid_dd_rev, ext);

		if( -1 == (access( filename, F_OK ) ) ) {
			(void)sprintf(filename, "%s/%06x/%04x/%02x%02x.%s", dd_path,
				dd_device_id->ddid_manufacturer,
				dd_device_id->ddid_device_type, 
				dd_device_id->ddid_device_rev,
                dd_device_id->ddid_dd_rev, ext);
		}


        if ((fp = fopen(filename, "r")) != NULL)
        {
            fclose(fp);

            /* Success: Copy the filename if the caller wants it, and return the file pointer */
            if (out_filename)
                strncpy(out_filename, filename, out_filename_size-1);
            free(filename);
            return 0;
        }
    }
    free(filename);
    return 0;
}

extern int      (*build_dd_dev_tbl_fn_ptr) P((DEVICE_HANDLE));
extern int      (*build_dd_blk_tbl_fn_ptr) P((BLOCK_HANDLE));
extern int      (*build_blk_app_info_fn_ptr) P((BLOCK_HANDLE));
extern int      (*build_dev_type_app_info_fn_ptr) P((DEVICE_TYPE_HANDLE));
extern int  (*dd_loaded_callback)P((char *fileName));

