/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This file contains all functions relating to the data structure
 *  ITEM_ID_LIST and ITEM_ID.
 */


#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif
#include <malloc.h>
#include "std.h"
#include "evl_loc.h"
#include "app_xmal.h"
#include "tst_fail.h"


/*********************************************************************
 *
 *	Name: ddl_parse_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_ref will parse the binary data chunk and load
 *		a REFERENCE_INFO structure and call resolve_ref() to get
 *		the OP_REF_TRAIL loaded. If the pointer to the structure is
 *		NULL, then ddl_pars_ref just unloads the binary without
 *		loading a structure.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an OP_REF_TRAIL
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *      options - these are flags which are passed through to resolve_ref()
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an OP_REF_TRAIL
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_ENCODING_ERROR
 *		error returns from resolve_ref(), ddl_eval_expr(),
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

/*
 * These defines specify the extra length to be appended to the
 * OP_REF_TRAIL_LIST
 */

#define NO_ET	   0		/* no extra trail count */
#define PARAM_ET   1		/* PARM and PARM_LIST extra trail count */
#define BLOCK_ET   2		/* BLOCK extra trail count */

static
int
ddl_parse_ref(unsigned char **chunkp, DDL_UINT *size, OP_REF_TRAIL *ref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed, unsigned int options)
{

	extern const int resolve_id_table[];	/* converts parse tags to item types */
	REF_INFO_LIST   info_list;			/* list where the parsed binary is stored */
	REF_INFO       *info_ptr;			/* temp pointer for info_list */
	unsigned short  info_inc = 0;		/* info list counter */
	DDL_UINT        tag;			/* stores the binary REFERENCE type number */
	EXPR            expr;			/* expression for converting ARRAY_REFs */
	int             rc = DDL_SUCCESS;	/* return code */
	int             parse = TRUE;		/* conditional for while loop */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_REF);

	/*
	 * if they want a reference to be loaded
	 */

	if (ref != NULL) {
        ref->bit_mask = 0;
		ref->container_id = 0;
		ref->container_index = 0;
		ref->container_type = 0;

		info_ptr = info_list.list;

		while (parse) {

			if (info_inc >= REF_INFO_LIST_SIZE) {

				return DDL_ENCODING_ERROR;
			}

			/*
			 * Parse the tag to find out what kind of reference this is.
			 */

			DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

			switch (tag) {

			case ITEM_ID_REF:
			case ITEM_ARRAY_ID_REF:
			case COLLECTION_ID_REF:
			case BLOCK_ID_REF:
			case VARIABLE_ID_REF:
			case MENU_ID_REF:
			case EDIT_DISP_ID_REF:
			case METHOD_ID_REF:
			case REFRESH_ID_REF:
			case UNIT_ID_REF:
			case WAO_ID_REF:
			case RECORD_ID_REF:
			case ARRAY_ID_REF:
			case VAR_LIST_ID_REF:
			case PROGRAM_ID_REF:
			case DOMAIN_ID_REF:
            case AXIS_ID_REF:
            case CHART_ID_REF   :
            case FILE_ID_REF    :
            case GRAPH_ID_REF   :
            case LIST_ID_REF    :
            case SOURCE_ID_REF  :
            case WAVEFORM_ID_REF:
            case IMAGE_ID_REF:
            case GRID_ID_REF:
			case RESP_CODES_ID_REF:
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, size, &info_ptr->val.id);

				/*
				 * If it's a simple item_id we don't call resolve
				 */

				if (info_inc == 0) {

					if (options & RESOLVE_OP_REF) {

						ref->op_id = info_ptr->val.id;
						ref->op_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];
						ref->op_subindex = 0;
					}
					if (options & RESOLVE_DESC_REF) {

						ref->desc_id = info_ptr->val.id;
						ref->desc_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];
					}
				}
				else {

					if (info_ptr->type == ITEM_ID_REF) {

						if (info_list.list[info_inc - 1].type == VIA_COLLECTION_REF) {

							info_ptr->type = COLLECTION_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_ITEM_ARRAY_REF) {

							info_ptr->type = ITEM_ARRAY_ID_REF;
						}
					}

					info_list.count = info_inc + 1;
					rc = resolve_ref(&info_list, ref, depinfo, env_info,
						var_needed, options, NO_ET);
				}
				parse = FALSE;
				break;

            case VIA_BLOCK_XREF:
            case VIA_BIT_ENUM_REF:      /* bit enum; the index is the bit mask */
			case VIA_ITEM_ARRAY_REF:	/* item array reference */
			case VIA_ARRAY_REF:			/* profibus array reference */
			case VIA_LIST_REF:
				info_ptr->type = (unsigned short) tag;
				rc = ddl_eval_expr(chunkp, size, &expr, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				switch (expr.type) {
				case DDS_UNSIGNED:
				case DDS_INDEX:
				case DDS_ENUMERATED:
				case DDS_BIT_ENUMERATED:
					info_ptr->val.index = (ITEM_ID) expr.val.u;
					break;

				case DDS_INTEGER:
					info_ptr->val.index = (ITEM_ID) expr.val.i;
					break;

				default:
					return DDL_ENCODING_ERROR;

				}
				break;

			case VIA_COLLECTION_REF:	/* collection reference */
			case VIA_RECORD_REF:		/* record reference */
			case VIA_FILE_REF:
			case VIA_VAR_LIST_REF:		/* variable list reference */
            case VIA_CHART_REF:
            case VIA_SOURCE_REF:
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, size, &info_ptr->val.member);
				break;

            case VIA_BCHART_REF:        /* Block chart members */
            case VIA_BFILE_REF:         /* Block file members */
            case VIA_BGRAPH_REF:        /* Block graph members */
            case VIA_BGRID_REF:         /* Block grid members */
            case VIA_BMENU_REF:         /* Block menu members */
            case VIA_BMETHOD_REF:       /* Block method members */
			case VIA_PARAM_REF:			/* param name ref */
			case VIA_PARAM_LIST_REF:	/* param list ref */
            case VIA_LOCAL_PARAM_REF:   /* local parameter ref */

				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, size, &info_ptr->val.member);

				/*
				 * Because this is a PARAM_LIST or a
				 * PARAM_LIST_REF we're at the end
				 */
                 
				info_list.count = info_inc + 1;
				rc = resolve_ref(&info_list, ref, depinfo, env_info, var_needed,
					options, PARAM_ET);
				parse = FALSE;
				break;

			case VIA_BLOCK_REF:	/* block characteristics ref */
				info_ptr->type = VIA_BLOCK_REF;
				DDL_PARSE_INTEGER(chunkp, size, &info_ptr->val.id);

				/*
				 * Because this is the BLOCK_REF we're at the
				 * end
				 */

				info_list.count = info_inc + 1;
				rc = resolve_ref(&info_list, ref, depinfo, env_info, var_needed,
					options, BLOCK_ET);
				parse = FALSE;
				break;

            case BLOCK_ID_XREF:
                info_ptr->type = BLOCK_ID_XREF;
				DDL_PARSE_INTEGER(chunkp, size, &info_ptr->val.id);
                /* Continue parsing */
                break;      

			default:
				return DDL_ENCODING_ERROR;
			}
			/*******************************************************************
			 * LOOK HERE to see info_inc AND info_ptr INCREMENTED !!!
			 ******************************************************************/
			info_inc++;
			info_ptr++;
		}
	}
	else {			/* parsing without saving a value */

		while (parse) {

			DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

			switch (tag) {

			case ITEM_ID_REF:
			case ITEM_ARRAY_ID_REF:
			case COLLECTION_ID_REF:
			case BLOCK_ID_REF:
			case VARIABLE_ID_REF:
			case MENU_ID_REF:
			case EDIT_DISP_ID_REF:
			case METHOD_ID_REF:
			case REFRESH_ID_REF:
			case UNIT_ID_REF:
			case WAO_ID_REF:
			case RECORD_ID_REF:
			case ARRAY_ID_REF:
			case VAR_LIST_ID_REF:
			case PROGRAM_ID_REF:
			case DOMAIN_ID_REF:
			case RESP_CODES_ID_REF:
            case AXIS_ID_REF:
            case CHART_ID_REF   :
            case FILE_ID_REF    :
            case GRAPH_ID_REF   :
            case LIST_ID_REF    :
            case SOURCE_ID_REF  :
            case WAVEFORM_ID_REF:
            case IMAGE_ID_REF:
            case GRID_ID_REF:

			case VIA_PARAM_REF:
			case VIA_PARAM_LIST_REF:
			case VIA_BLOCK_REF:
				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
				parse = FALSE;
				break;

			case VIA_ITEM_ARRAY_REF:
			case VIA_ARRAY_REF:
			case VIA_LIST_REF:
				rc = ddl_eval_expr(chunkp, size, (EXPR *) NULL, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				break;

			case VIA_COLLECTION_REF:
			case VIA_RECORD_REF:
			case VIA_VAR_LIST_REF:
				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
				break;

			default:
				return DDL_ENCODING_ERROR;
			}
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_item_id
 *	ShortDesc: Parse an item_id.
 *
 *	Description:
 *		ddl_parse_item_id calls ddl_parse_ref and converts the output
 *		to an ITEM_ID
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		item_id - pointer to an ITEM_ID
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		item_id - pointer to an ITEM_ID
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_item_id(unsigned char **chunkp, DDL_UINT *size, ITEM_ID *item_id,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	OP_REF_TRAIL    ref;	/* temp OP_REF_TRAIL for calling parse_ref() with */

	if (item_id) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*item_id = ref.desc_id;
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_op_ref_trail
 *	ShortDesc: Parse an op_ref_trail
 *
 *	Description:
 *		ddl_parse_op_ref_trail calls ddl_parse_ref with the options
 *		flag set for DESC_REF, OP_REF and OP_REF_TRAIL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an OP_REF_TRAIL
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an OP_REF_TRAIL
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: steve beyerl
 *
 ********************************************************************************/

int
ddl_parse_op_ref_trail(unsigned char **chunkp, DDL_UINT *size, OP_REF_TRAIL *op_ref_trail, OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	unsigned int    resolve_options = 0;	/* temp options for resolving */

	if (op_ref_trail) {

		resolve_options = RESOLVE_OP_REF | RESOLVE_DESC_REF | RESOLVE_TRAIL;

		rc = ddl_parse_ref(chunkp, size, op_ref_trail, depinfo,
			env_info, var_needed, resolve_options);
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_op_ref_trail_list
 *	ShortDesc: Parse the binary and load an OP_REF_TRAIL_LIST.
 *
 *	Description:
 *		ddl_parse_op_ref_trail_list will parse the binary data chunk and
 *		load a OP_REF_TRAIL_LIST structure unless the pointer to the
 *		OP_REF_TRAIL_LIST passed in is NULL, in which case it will
 *		simply read through the binary data.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		op_ref_trail_list - pointer to a OP_REF_TRAIL_LIST structure
 *			where the result will be stored.  If this is NULL, no result
 *			is computed or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		op_ref_trail_list - pointer to an OP_REF_TRAIL_LIST structure
 *				containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from:
 *			ddl_parse_op_ref_trail()
 *			ddl_shrink_op_ref_trail_list()
 *
 *	Author: steve beyerl
 *
 **********************************************************************/

static int
ddl_parse_op_ref_trail_list(unsigned char **chunkp, DDL_UINT *size, OP_REF_TRAIL_LIST *op_ref_trail_list, OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;			/* return code */
	OP_REF_TRAIL   *temp_ptr;	/* temporary pointer */

	ASSERT_DBG(chunkp && *chunkp && size);

	/*
	 * Parse an OP_REF_TRAIL_LIST.  If we need more room in the array of
	 * structures, xrealloc more room.  Then call ddl_parse_op_ref_trail to
	 * load the next OP_REF_TRAIL_LIST structure.
	 */

	if (op_ref_trail_list != NULL) {

		/*
		 * parse the binary and load the OP_REF_TRAIL_LIST structure
		 */

		while (*size > 0) {

			if (op_ref_trail_list->count == op_ref_trail_list->limit) {

				/*
				 * reallocate the OP_REF_TRAIL_LIST structure
				 * for more OP_REF_TRAILs
				 */

				op_ref_trail_list->limit += REF_LIST_INC;

				/*
				 * realloc OP_REF_TRAIL  array
				 */

				temp_ptr = (OP_REF_TRAIL *) xrealloc((void *) op_ref_trail_list->list,
					(size_t) (op_ref_trail_list->limit * sizeof(OP_REF_TRAIL)));

				if (temp_ptr == NULL) {
					op_ref_trail_list->limit = op_ref_trail_list->count;
					ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				op_ref_trail_list->list = temp_ptr;

				/*
				 * Initialize the new OP_REF_TRAILs to zero
				 */

				(void)memset((char *) &op_ref_trail_list->list[op_ref_trail_list->count],
					0, (REF_LIST_INC * sizeof(OP_REF_TRAIL)));

			}

			rc = ddl_parse_op_ref_trail(chunkp, size,
				&op_ref_trail_list->list[op_ref_trail_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
				return rc;
			}

			op_ref_trail_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_op_ref_trail_list(op_ref_trail_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the OP_REF_TRAIL_LIST structure
			 */

			rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
				depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_desc_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_desc_ref calls ddl_parse_ref and converts the output
 *		to an DESC_REF
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		desc_ref - pointer to an DESC_REF
 *		depinfo - pointer to a DEPINFO structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		desc_ref - pointer to an DESC_REF
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_desc_ref(unsigned char **chunkp, DDL_UINT *size,DESC_REF *desc_ref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{
	int             rc;	/* return code */
	OP_REF_TRAIL    ref;	/* temp OP_REF_TRAIL */

	if (desc_ref) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		desc_ref->id = ref.desc_id;
		desc_ref->type = ref.desc_type;
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_op_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_desc_ref calls ddl_parse_ref and converts the output
 *		to an OP_REF
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		op_ref - pointer to an OP_REF
 *		depinfo - pointer to a DEPINFO structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		op_ref - pointer to an OP_REF
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_op_ref(unsigned char **chunkp, DDL_UINT *size, OP_REF *op_ref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed, unsigned long *bitmask)
{

	int             rc;		/* return code */
	OP_REF_TRAIL    ref;	/* temp OP_REF_TRAIL */

	if (op_ref) {

		ref.desc_id = 0;
		ref.desc_type = 0;
		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_OP_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		op_ref->id = ref.op_id;
		op_ref->type = ref.op_type;
		op_ref->subindex = ref.op_subindex;
        op_ref->block_id = ref.op_block_id;
        op_ref->block_instance = ref.op_block_instance;
		/* AR 3746 */
		op_ref->container_id = ref.container_id;
		op_ref->container_index = ref.container_index;
		op_ref->container_type = ref.container_type;
		
		/* If the caller is interested, return the bit mask */
		if (bitmask)
			*bitmask = ref.bit_mask;
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_ref_choice
 *	ShortDesc: Choose the correct reference from a binary.
 *
 *	Description:
 *		ddl_ref_choice will parse the binary for a reference,
 *		according to the current conditionals (if any).  The value of
 *		the reference is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an ITEM_ID structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an ITEM_ID structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_ref().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static
int
ddl_ref_choice(unsigned char **chunkp, DDL_UINT *size, ITEM_ID *item_id,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO2 *env_info, OP_REF *var_needed)
{
	int             rc;		/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

	TEST_FAIL(DDL_REF_CHOICE);

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (item_id && (val.chunk)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (item_id == NULL));

		/* We have finally gotten to the actual value!  Parse it. */

		rc = ddl_parse_item_id(&val.chunk, &val.size, item_id, depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (item_id && data_valid) {
			*data_valid = TRUE;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_attr_ref
 *
 *	ShortDesc: Evaluate a reference
 *
 * Description:
 *	The eval_attr_ref function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference and size should specify its size. If ref is
 *	not a null pointer, the item id is returned in ref. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to a ITEM_ID where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to a ITEM_ID containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_ref_choice(), ddl_shrink_depinfo()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
eval_attr_ref(unsigned char *chunk, DDL_UINT size, ITEM_ID *ref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_REF);

	valid = 0;

	if (ref) {
		*ref = 0;	/** initialize output parameter **/
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_ref_choice(&chunk, &size, ref, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {

		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	if (ref && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_desc_ref
 *
 *	ShortDesc: Evaluate a reference
 *
 * Description:
 *	The eval_attr_ref function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference and size should specify its size. If ref is
 *	not a null pointer, the item id is returned in ref. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to a DESC_REF where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to a DESC_REF structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_ref_choice(), ddl_shrink_depinfo()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
eval_attr_desc_ref(unsigned char *chunk, DDL_UINT size, DESC_REF *desc_ref,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
    CHUNK           val_chunk;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_REF);

	if (desc_ref) {
		desc_ref->id = desc_ref->type = 0;	/** initialize output parameter **/
	}

	if (depinfo) {
		depinfo->count = 0;
	}

    rc = ddl_cond(&chunk, &size, &val_chunk, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}


    rc = ddl_parse_desc_ref(&val_chunk.chunk, &val_chunk.size, desc_ref, depinfo, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {

		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 *	Name: ddl_free_reflist
 *	ShortDesc: frees the list of ITEM_IDs
 *	Description:
 *		ddl_free_reflist will free a list of ITEM_IDSs including the
 *		array of ITEM_ID_ITEMs included in each ITEM_ID
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		ref_list:   points to the empty list
 *
 *	Returns: void
 *
 *	Author: Chris Gustafson
 *
 ************************************************************************/

void
ddl_free_reflist(ITEM_ID_LIST *ref_list, uchar dest_flag)
{

	if (ref_list == NULL) {
		return;
	}

	if (ref_list->list == NULL) {

		ASSERT_DBG(!ref_list->count && !ref_list->limit);
		ref_list->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list in ITEM_ID_LIST
		 */

		xfree((void **) &ref_list->list);
		ref_list->list = NULL;
		ref_list->limit = 0;
	}

	ref_list->count = 0;

}


/*********************************************************************
 *
 *	Name: ddl_shrink_reflist
 *	ShortDesc: Shrink a ITEM_ID_LIST.
 *
 *	Description:
 *		ddl_shrink_reflist reallocates the ITEM_ID_LIST to only contain
 *		space for the ITEM_IDS currently being used
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *
 *	Outputs:
 *		ref_list:   points to the new list
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

static int
ddl_shrink_reflist(ITEM_ID_LIST *ref_list)
{

	TEST_FAIL(DDL_SHRINK_REFLIST);

	if (ref_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (ref_list->list == NULL) {
		ASSERT_DBG(!ref_list->count && !ref_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (ref_list->count == ref_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (ref_list->count == 0) {
		ddl_free_reflist(ref_list, FREE_ATTR);
	}
	else {
		ref_list->limit = ref_list->count;

		ref_list->list = (ITEM_ID *) xrealloc((void *) ref_list->list,
			(size_t) (ref_list->limit * sizeof(ITEM_ID)));

		if (ref_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_reflist
 *	ShortDesc: Parse the binary and load a ITEM_ID_LIST.
 *
 *	Description:
 *		ddl_parse_reflist will parse the binary data chunk and load a ITEM_ID_LIST structure
 *		unless the pointer to the ITEM_ID_LIST passed in is NULL, in which case it will
 *		simply read through the binary data.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref_list - pointer to a ITEM_ID_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref_list - pointer to a ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from ddl_parse_ref(), ddl_shrink_reflist()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

static int
ddl_parse_reflist(unsigned char **chunkp, DDL_UINT *size, ITEM_ID_LIST *id_list,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	ITEM_ID        *temp_id;/* temporary pointer */

	ASSERT_DBG(chunkp && *chunkp && size);

	TEST_FAIL(DDL_PARSE_REFLIST);

	/*
	 * Parse a ITEM_ID_LIST.  If we need more room in the array of structures,
	 * xrealloc more room.  Then call ddl_parse_item_id to load the next ITEM_ID
	 * structure.
	 */

	if (id_list != NULL) {

		/*
		 * parse the binary and load the ITEM_ID_LIST structure
		 */

		while (*size > 0) {

			if (id_list->count == id_list->limit) {

				/*
				 * reallocate the ITEM_ID_LIST structure for
				 * more ITEM_IDs
				 */

				id_list->limit += REF_LIST_INC;

				/*
				 * realloc ITEM_ID array
				 */

				temp_id = (ITEM_ID *) xrealloc((void *) id_list->list,
					(size_t) (id_list->limit * sizeof(ITEM_ID)));

				if (temp_id == NULL) {

					id_list->limit = id_list->count;
					ddl_free_reflist(id_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				id_list->list = temp_id;

				/*
				 * Initialize the new ITEM_IDs to zero
				 */
				(void)memset((char *) &id_list->list[id_list->count], 0,
					(REF_LIST_INC * sizeof(ITEM_ID)));
			}

			rc = ddl_parse_item_id(chunkp, size, &id_list->list[id_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_reflist(id_list, FREE_ATTR);
				return rc;
			}

			id_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_reflist(id_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the ITEM_ID_LIST structure
			 */

			rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_reflist_choice
 *	ShortDesc: Choose the correct reference list from a binary.
 *
 *	Description:
 *		ddl_reflist_choice will parse the binary for an reference list,
 *		according to the current conditionals (if any).  The value of
 *		the reference list is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an REFERENCE_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_reflist().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static
int
ddl_reflist_choice(unsigned char **chunkp, DDL_UINT *size, ITEM_ID_LIST *list,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;				/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

	TEST_FAIL(DDL_REFLIST_CHOICE);

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		REFERENCE_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_reflist(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_reflist
 *
 *	ShortDesc: Evaluate a reference
 *
 * Description:
 *	The eval_reflist function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference list and size should specify its size. If list is
 *	not a null pointer, the item id is returned in list. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to a REFERENCE_LIST where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to a REFERENCE_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_reflist_choice(), ddl_shrink_depinfo()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
eval_reflist(unsigned char *chunk, DDL_UINT size, ITEM_ID_LIST *list, OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_REFLIST);

	valid = 0;

	ddl_free_reflist(list, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_reflist_choice(&chunk, &size, list, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_reflist(list, FREE_ATTR);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 *	Name: ddl_free_op_ref_trail
 *	ShortDesc: frees the resolve trail from the OP_REF_TRAIL structure.
 *
 *	Description:
 *		ddl_free_op_ref_trail will free a list of resolve trail items
 *		associated with an OP_REF_TRAIL.
 *
 *	Inputs:
 *		op_ref_trail:	a pointer to an OP_REF_TRAIL structure.
 *
 *	Outputs:
 *		op_ref_trail:	a pointer to an OP_REF_TRAIL structure.
 *
 *	Returns: void
 *
 *	Author: steve beyerl
 *
 ************************************************************************/

void
ddl_free_op_ref_trail(OP_REF_TRAIL *op_ref_trail)
{

	if (op_ref_trail == NULL) {
		return;
	}

	if (op_ref_trail->trail == NULL) {

		ASSERT_DBG(!op_ref_trail->trail_count && !op_ref_trail->trail_limit);
	}
	else {

		/*
		 * Free the trail list in OP_REF_TRAIL
		 */

		xfree((void **) &op_ref_trail->trail);
		op_ref_trail->trail = NULL;
	}

	op_ref_trail->trail_limit = 0;
	op_ref_trail->trail_count = 0;

	return;
}


/*********************************************************************
 *
 *  Name: ddl_op_ref_trail_choice
 *  ShortDesc: Choose the correct reference from a binary.
 *
 *  Description:
 *      ddl_op_ref_trail_choice will parse the binary for a reference,
 *      according to the current conditionals (if any).  The value of
 *      the reference is returned, along with dependency information.
 *      If a value is found, the valid flag is set to 1.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      op_ref_trail - pointer to an OP_REF_TRAIL structure where the result
 *              will be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      op_ref_trail - pointer to an OP_REF_TRAIL structure containing
 *              the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *      data_valid - pointer to an int which, if set to 1, indicates
 *              that there is a valid value in ref
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Returns:
 *      DDL_SUCCESS
 *      error returns from ddl_cond() and ddl_parse_ref().
 *
 *  Author:
 *      Steve Beyerl
 *
 *********************************************************************/

int
ddl_op_ref_trail_choice(unsigned char **chunkp, DDL_UINT *size,
	OP_REF_TRAIL *op_ref_trail, OP_REF_LIST *depinfo, int *data_valid,
	ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

	TEST_FAIL(DDL_OP_REF_TRAIL_CHOICE);

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (op_ref_trail && (val.chunk)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (op_ref_trail == NULL));

		/* We have finally gotten to the actual value!  Parse it. */

		rc = ddl_parse_op_ref_trail(&val.chunk, &val.size, op_ref_trail,
			depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (op_ref_trail && data_valid) {
			*data_valid = TRUE;
		}
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 *	Name: ddl_free_op_ref_trail_list
 *	ShortDesc: frees the list of OP_REF_TRAILs
 *	Description:
 *		ddl_free_op_ref_trail_list will free the list of OP_REF_TRAIL
 *		structures associated with an OP_REF_TRAIL_LIST.
 *
 *	Inputs:
 *		op_ref_trail_list:	a pointer to an OP_REF_TRAIL_LIST
 *		destruct_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		op_ref_trail_list:	a pointer to an OP_REF_TRAIL_LIST
 *
 *	Returns: void
 *
 *	Author: steve beyerl
 *
 ************************************************************************/

void
ddl_free_op_ref_trail_list(OP_REF_TRAIL_LIST *op_ref_trail_list, unsigned char destruct_flag)
{

	int             i;	/* incrementer */

	if (op_ref_trail_list == NULL) {
		return;
	}

	if (op_ref_trail_list->list == NULL) {

		ASSERT_DBG(!op_ref_trail_list->count && !op_ref_trail_list->limit);
		op_ref_trail_list->count = 0;
		op_ref_trail_list->limit = 0;
		return;
	}

	/*
	 * Delete the list of RESOLVE INFO associated with each OP_REF_TRAIL.
	 * This should happen regardless of the value of "destruct_flag".
	 */

	for (i = 0; i <  op_ref_trail_list->limit; i++) {
		ddl_free_op_ref_trail(&op_ref_trail_list->list[i]);
	}

	if (destruct_flag == FREE_ATTR) {

		/*
		 * Free the list of OP_REF_TRAILs.
		 */

		xfree((void **) &op_ref_trail_list->list);
		op_ref_trail_list->list = NULL;
		op_ref_trail_list->limit = 0;
	}
	else {			/* destruct_flag == CLEAN_ATTR */

		/*
		 * zero out the list. Leave the list and limit intact.
		 */

		(void)memset((char *) op_ref_trail_list->list, 0,
			(size_t) op_ref_trail_list->limit * sizeof(OP_REF_TRAIL));
	}

	op_ref_trail_list->count = 0;

	return;
}



/*********************************************************************
 *
 *	Name: ddl_shrink_op_ref_trail_list
 *	ShortDesc: Shrink a OP_REF_TRAIL_LIST.
 *
 *	Description:
 *		ddl_shrink_op_ref_trail_list reallocates the OP_REF_TRAIL_LIST
 *		to only contain	space for the OP_REF_TRAILs currently being used
 *
 *	Inputs:
 *		op_ref_trail_list:	a pointer to the ITEM_ID_LIST
 *
 *	Outputs:
 *		op_ref_trail_list:   points to the new list
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: steve beyerl
 *
 ***********************************************************************/

int
ddl_shrink_op_ref_trail_list(OP_REF_TRAIL_LIST *op_ref_trail_list)
{

	TEST_FAIL(DDL_SHRINK_OP_REF_TRAIL_LIST);

	if (op_ref_trail_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (op_ref_trail_list->list == NULL) {
		ASSERT_DBG(!op_ref_trail_list->count && !op_ref_trail_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (op_ref_trail_list->count == op_ref_trail_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (op_ref_trail_list->count == 0) {
		ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
	}
	else {
		op_ref_trail_list->limit = op_ref_trail_list->count;

		op_ref_trail_list->list = (OP_REF_TRAIL *) xrealloc(
			(void *) op_ref_trail_list->list,
			(size_t) (op_ref_trail_list->limit * sizeof(OP_REF_TRAIL)));

		if (op_ref_trail_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_op_ref_trail_list_choice
 *	ShortDesc: Choose the correct reference list from a binary.
 *
 *	Description:
 *		ddl_op_ref_trail_trail_choice will parse the binary for an reference list,
 *		according to the current conditionals (if any).  The value of
 *		the op_ref_trail_list is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an OP_REF_TRAIL_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an OP_REF_TRAIL_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_op_ref_trail_list().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_op_ref_trail_list_choice(unsigned char **chunkp, DDL_UINT *size,
	OP_REF_TRAIL_LIST *list, OP_REF_LIST *depinfo, int *data_valid,
	ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;				/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;		/* temp pointer for chunk list */

	TEST_FAIL(DDL_OP_REF_TRAIL_LIST_CHOICE);

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		REFERENCE_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_op_ref_trail_list(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


/*********************************************************************
 *
 *  Name: eval_op_ref_trail_list
 *
 *  ShortDesc: Evaluate an operational reference trail list
 *
 * Description:
 *  The eval_op_ref_trail_list function evaluates an operational
 *  reference trail list.  The buffer pointed to by chunk should
 *  contain the binary for a reference list and size should specify
 *  its size. If list is not a null pointer, the item id is returned
 *  in list. If depinfo is not a null pointer, dependency information
 *  about the reference is returned in depinfo.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      list - pointer to an OP_REF_TRAIL where the result will
 *              be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      list - pointer to an OP_REF_TRAIL structure containing the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *      data_valid - pointer to an int which, if set to 1, indicates
 *              that there is a valid value in expr
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 * Returns:
 *  DDL_SUCCESS, DDL_DEFAULT_ATTR
 *  return codes from:
 *		ddl_reflist_choice()
 *		ddl_shrink_depinfo()
 *
 *  Author: steve beyerl
 *
 *********************************************************************/

int
eval_op_ref_trail_list(unsigned char *chunk, DDL_UINT size, OP_REF_TRAIL_LIST *list,
	OP_REF_LIST *depinfo, ENV_INFO2 *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	TEST_FAIL(EVAL_OP_REF_TRAIL_LIST);

	valid = 0;

	if (list) {
		ddl_free_op_ref_trail_list(list, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_op_ref_trail_list_choice(&chunk, &size, list, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_op_ref_trail_list(list, FREE_ATTR);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


