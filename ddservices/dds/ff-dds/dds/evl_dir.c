/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This file contains the functions for evaluating a block or device
 *	directory.
 */

#ifdef SUN
#include <memory.h>		/* K&R only */
#endif /* SUN */
#include <string.h>

#include "std.h"
#include "evl_loc.h"
#include "app_xmal.h"
#include "tst_fail.h"

/*********************************************************************
 *
 *	Name: eval_dir_param_list_tbl
 *	ShortDesc: evaluate the PARAM_LIST_TBL
 *
 *	Description:
 *		eval_dir_param_list_tbl will load the PARAM_LIST_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_table: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_tbl(PARAM_LIST_TBL *param_list_tbl, BININFO *bin)
{

	PARAM_LIST_TBL_ELEM		*element;	 /* temp pointer for the list */
	PARAM_LIST_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        		temp_int;	 /* integer value */
	int						rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_LIST_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_tbl->count = 0;
		param_list_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_tbl->list = (PARAM_LIST_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(PARAM_LIST_TBL_ELEM) ) );

	if (param_list_tbl->list == NULL) {

		param_list_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_list_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_LIST_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_tbl->list, end_element = element +
			temp_int; element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_count = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_string_tbl
 *	ShortDesc: evaluate the STRING_TBL
 *
 *	Description:
 *		eval_dir_string_tbl will load the STRING_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		string_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_string_tbl(STRING_TBL *string_tbl, BININFO *bin)
{

	STRING			*string;	 	/* temp pointer for the list */
	STRING			*end_string;	/* end pointer for the list */
	DDL_UINT    	temp_int;		/* integer value */
	unsigned long	size;			/* temp size */
	unsigned char	*root_ptr;		/* temp pointer */
	int				rc;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_STRING_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		string_tbl->count = 0;
		string_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	string_tbl->count = (int) temp_int;

	/* malloc the list */
	string_tbl->list = (STRING *) xmalloc(
			(size_t) (temp_int * sizeof(STRING) ) );
	if (string_tbl->list == NULL) {

		string_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* malloc the root */
	string_tbl->root = (unsigned char *) xmalloc(
			(size_t) (bin->size * sizeof(unsigned char)));
	if (string_tbl->root == NULL) {

		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) string_tbl->list, 0,
			(size_t) (temp_int * sizeof(STRING)));

	/* copy the chunk to the root */
	(void)memcpy((char *) string_tbl->root, (char *) bin->chunk,
			(size_t) (bin->size * sizeof(unsigned char)));

	/*
	 * load the list
	 */

	size = bin->size;
	root_ptr = string_tbl->root;

	for (string = string_tbl->list, end_string = string + temp_int;
		 string < end_string; string++) {

		DDL_PARSE_INTEGER(&root_ptr, &size, &temp_int);
		string->len = (unsigned short) temp_int;		

		string->str = (char *) root_ptr;

		size -= temp_int;             /* decrement size */
		root_ptr += temp_int;        /* increment the root pointer */

		string->flags = DONT_FREE_STRING;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_binary_tbl(BINARY_TBL *binary_tbl, BININFO *bin)
{

	BINARY_ENTRY	*binary;	 	/* temp pointer for the list */
	BINARY_ENTRY    *end_binary;	/* end pointer for the list */
	DDL_UINT    	temp_int;		/* integer value */
	unsigned long	size;			/* temp size */
	unsigned char	*root_ptr;		/* temp pointer */
	int				rc;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_STRING_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		binary_tbl->count = 0;
		binary_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	binary_tbl->count = (int) temp_int;

	/* malloc the list */
	binary_tbl->list = (BINARY_ENTRY *) xmalloc(
			(size_t) (temp_int * sizeof(BINARY_ENTRY) ) );
	if (binary_tbl->list == NULL) {

		binary_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* malloc the root */
	binary_tbl->root = (unsigned char *) xmalloc(
			(size_t) (bin->size * sizeof(unsigned char)));
	if (binary_tbl->root == NULL) {

		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) binary_tbl->list, 0,
			(size_t) (temp_int * sizeof(BINARY_ENTRY)));

	/* copy the chunk to the root */
	(void)memcpy((char *) binary_tbl->root, (char *) bin->chunk,
			(size_t) (bin->size * sizeof(unsigned char)));

	/*
	 * load the list
	 */

	size = bin->size;
	root_ptr = binary_tbl->root;

	for (binary = binary_tbl->list, end_binary = binary + temp_int;
		 binary < end_binary; binary++) {

		/* first is the name length - not stored */
		DDL_PARSE_INTEGER(&root_ptr, &size, &temp_int);

		binary->name = (char *) root_ptr;

		size -= temp_int;             /* decrement size */
		root_ptr += temp_int;        /* increment the root pointer */

        /* Next we have the binary data length */
		DDL_PARSE_INTEGER(&root_ptr, &size, &temp_int);
        binary->count = temp_int;
        binary->data = root_ptr;

		size -= temp_int;             /* decrement size */
		root_ptr += temp_int;        /* increment the root pointer */
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_domain_tbl
 *	ShortDesc: evaluate the DOMAIN_TBL
 *
 *	Description:
 *		eval_dir_domain_tbl will load the DOMAIN_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		domain_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_domain_tbl(DOMAIN_TBL *domain_tbl, BININFO *bin)
{

	DOMAIN_TBL_ELEM		*element;	 /* temp pointer for the list */
	DOMAIN_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_DOMAIN_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		domain_tbl->count = 0;
		domain_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	domain_tbl->count = (int) temp_int;

	/* malloc the list */
	domain_tbl->list = (DOMAIN_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(DOMAIN_TBL_ELEM) ) );

	if (domain_tbl->list == NULL) {

		domain_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) domain_tbl->list, 0,
			(size_t) temp_int * sizeof(DOMAIN_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = domain_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}


static
int eval_dir_axis_tbl(AXIS_TBL *axis_tbl, BININFO *bin)
{

	AXIS_TBL_ELEM		*element;	 /* temp pointer for the list */
	AXIS_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_AXIS_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		axis_tbl->count = 0;
		axis_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	axis_tbl->count = (int) temp_int;

	/* malloc the list */
	axis_tbl->list = (AXIS_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(AXIS_TBL_ELEM) ) );

	if (axis_tbl->list == NULL) {

		axis_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) axis_tbl->list, 0,
			(size_t) temp_int * sizeof(AXIS_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = axis_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_chart_tbl(CHART_TBL *chart_tbl, BININFO *bin)
{

	CHART_TBL_ELEM		*element;	 /* temp pointer for the list */
	CHART_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_CHART_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		chart_tbl->count = 0;
		chart_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	chart_tbl->count = (int) temp_int;

	/* malloc the list */
	chart_tbl->list = (CHART_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(CHART_TBL_ELEM) ) );

	if (chart_tbl->list == NULL) {

		chart_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) chart_tbl->list, 0,
			(size_t) temp_int * sizeof(CHART_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = chart_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_file_tbl(FILE_TBL *file_tbl, BININFO *bin)
{

	FILE_TBL_ELEM		*element;	 /* temp pointer for the list */
	FILE_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_FILE_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		file_tbl->count = 0;
		file_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	file_tbl->count = (int) temp_int;

	/* malloc the list */
	file_tbl->list = (FILE_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(FILE_TBL_ELEM) ) );

	if (file_tbl->list == NULL) {

		file_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) file_tbl->list, 0,
			(size_t) temp_int * sizeof(FILE_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = file_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_graph_tbl(GRAPH_TBL *graph_tbl, BININFO *bin)
{

	GRAPH_TBL_ELEM		*element;	 /* temp pointer for the list */
	GRAPH_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_GRAPH_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		graph_tbl->count = 0;
		graph_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	graph_tbl->count = (int) temp_int;

	/* malloc the list */
	graph_tbl->list = (GRAPH_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(GRAPH_TBL_ELEM) ) );

	if (graph_tbl->list == NULL) {

		graph_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) graph_tbl->list, 0,
			(size_t) temp_int * sizeof(GRAPH_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = graph_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_list_tbl(LIST_TBL *list_tbl, BININFO *bin)
{

	LIST_TBL_ELEM		*element;	 /* temp pointer for the list */
	LIST_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_LIST_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		list_tbl->count = 0;
		list_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	list_tbl->count = (int) temp_int;

	/* malloc the list */
	list_tbl->list = (LIST_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(LIST_TBL_ELEM) ) );

	if (list_tbl->list == NULL) {

		list_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) list_tbl->list, 0,
			(size_t) temp_int * sizeof(LIST_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = list_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_source_tbl(SOURCE_TBL *source_tbl, BININFO *bin)
{

	SOURCE_TBL_ELEM		*element;	 /* temp pointer for the list */
	SOURCE_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_SOURCE_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		source_tbl->count = 0;
		source_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	source_tbl->count = (int) temp_int;

	/* malloc the list */
	source_tbl->list = (SOURCE_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(SOURCE_TBL_ELEM) ) );

	if (source_tbl->list == NULL) {

		source_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) source_tbl->list, 0,
			(size_t) temp_int * sizeof(SOURCE_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = source_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}

static
int eval_dir_waveform_tbl(WAVEFORM_TBL *waveform_tbl, BININFO *bin)
{

	WAVEFORM_TBL_ELEM		*element;	 /* temp pointer for the list */
	WAVEFORM_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_WAVEFORM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		waveform_tbl->count = 0;
		waveform_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	waveform_tbl->count = (int) temp_int;

	/* malloc the list */
	waveform_tbl->list = (WAVEFORM_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(WAVEFORM_TBL_ELEM) ) );

	if (waveform_tbl->list == NULL) {

		waveform_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) waveform_tbl->list, 0,
			(size_t) temp_int * sizeof(WAVEFORM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = waveform_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_item_tbl
 *	ShortDesc: evaluate the ITEM_TBL
 *
 *	Description:
 *		eval_dir_item_tbl will load the ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_item_tbl(ITEM_AND_EXPANDED_TBLS *dual_item_tbl, BININFO *bin)
{
	ITEM_TBL *item_tbl;
	ITEM_TBL_ELEM	*element;	  /* temp pointer for the list */
	ITEM_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT       	temp_int;	  /* integer value */
	int				rc;			  /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_ITEM_TBL);

	/* Set our pointer to be the actual item table */
	item_tbl = dual_item_tbl->item_tbl;

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		item_tbl->count = 0;
		item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	item_tbl->count = (int) temp_int;

	/* malloc the list */
	item_tbl->list = (ITEM_TBL_ELEM *) xmalloc((size_t) (temp_int * 
			sizeof(ITEM_TBL_ELEM) ) );

	if (item_tbl->list == NULL) {

		item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) item_tbl->list, 0,
		(size_t) temp_int * sizeof(ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_type = (ITEM_TYPE) temp_int;
	}

	if (!bin->size)
		return DDL_SUCCESS;

	/* If there are bytes remaining, we have an expanded attributes table */
	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/* Set our pointer to be the expanded attribute table */
	item_tbl = dual_item_tbl->expanded_attr_tbl;

	/*
	 * if count is zero
	 */

	if(!temp_int) {
		item_tbl->count = 0;
		item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	item_tbl->count = (int) temp_int;

	/* malloc the list */
	item_tbl->list = (ITEM_TBL_ELEM *) xmalloc((size_t) (temp_int * 
			sizeof(ITEM_TBL_ELEM) ) );

	if (item_tbl->list == NULL) {

		item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) item_tbl->list, 0,
		(size_t) temp_int * sizeof(ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_type = (ITEM_TYPE) temp_int;
	}
	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_prog_tbl
 *	ShortDesc: evaluate the PROG_TBL
 *
 *	Description:
 *		eval_dir_prog_tbl will load the PROG_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		prog_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_prog_tbl(PROG_TBL *prog_tbl, BININFO *bin)
{

	PROG_TBL_ELEM		*element;	 /* temp pointer for the list */
	PROG_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PROG_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		prog_tbl->count = 0;
		prog_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	prog_tbl->count = (int) temp_int;

	/* malloc the list */
	prog_tbl->list = (PROG_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(PROG_TBL_ELEM) ) );

	if (prog_tbl->list == NULL) {

		prog_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) prog_tbl->list, 0,
			(size_t) temp_int * sizeof(PROG_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = prog_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_local_var_tbl
 *	ShortDesc: evaluate the LOCAL_VAR_TBL
 *
 *	Description:
 *		eval_dir_local_var_tbl will load the LOCAL_VAR_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		local_var_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_local_var_tbl(LOCAL_VAR_TBL *local_var_tbl, BININFO *bin)
{

	LOCAL_VAR_TBL_ELEM	*element;	  /* temp pointer for the list */
	LOCAL_VAR_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT        	temp_int;	  /* integer value */
	int					rc;			  /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		local_var_tbl->count = 0;
		local_var_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	local_var_tbl->count = (int) temp_int;

	/* malloc the list */
	local_var_tbl->list = (LOCAL_VAR_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(LOCAL_VAR_TBL_ELEM) ) );

	if (local_var_tbl->list == NULL) {

		local_var_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) local_var_tbl->list, 0,
			(size_t) temp_int * sizeof(LOCAL_VAR_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = local_var_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		/* parse type */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->type = (unsigned short) temp_int;

		/* parse size */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->size = (unsigned short) temp_int;

		/* parse DD reference */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_dict_ref_tbl
 *	ShortDesc: evaluate the DICT_REF_TBL
 *
 *	Description:
 *		eval_dir_dict_ref_tbl will load the DICT_REF_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		dict_ref_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_dict_ref_tbl(DICT_REF_TBL *dict_ref_tbl, BININFO *bin)
{

	UINT32		*element;	 			/* temp pointer for the list */
	UINT32		*end_element;			/* end pointer for the list */
	DDL_UINT 	temp_int;	 			/* integer value */
	int			rc;						/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_DICT_REF_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		dict_ref_tbl->count = 0;
		dict_ref_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	dict_ref_tbl->count = (int) temp_int;

	/* malloc the list */
	dict_ref_tbl->list = (UINT32 *) xmalloc(
			(size_t) (temp_int * sizeof(UINT32) ) );

	if (dict_ref_tbl->list == NULL) {

		dict_ref_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) dict_ref_tbl->list, 0,
			(size_t) temp_int * sizeof(UINT32));

	/*
	 * load the list
	 */

	for (element = dict_ref_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		*element = (UINT32) temp_int;
	}


	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_blk_tbl
 *	ShortDesc: evaluate the BLK_TBL
 *
 *	Description:
 *		eval_dir_blk_tbl will load the BLK_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_tbl(BLK_TBL *blk_tbl, BININFO *bin)
{

	BLK_TBL_ELEM	*element;	 /* temp pointer for the list */
	BLK_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int;	 /* integer value */
	int				rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_BLK_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		blk_tbl->count = 0;
		blk_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_tbl->count = (int) temp_int;

	/* malloc the list */
	blk_tbl->list = (BLK_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(BLK_TBL_ELEM) ) );

	if (blk_tbl->list == NULL) {

		blk_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) blk_tbl->list, 0,
			(size_t) temp_int * sizeof(BLK_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = blk_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* load the name */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_id = (ITEM_ID) temp_int;


		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_rec_item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_rec_bint_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_dir_dd_ref.object_index = 	(OBJECT_INDEX) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_rel_tbl
 *	ShortDesc: evaluate the REL_TBL
 *
 *	Description:
 *		eval_dir_rel_tbl will load the REL_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		rel_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_rel_tbl(REL_TBL *rel_tbl, BININFO *bin)
{

	REL_TBL_ELEM	*element;	 /* temp pointer for the list */
	REL_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int;	 /* integer value */
	int				rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_REL_TBL);


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		rel_tbl->count = 0;
		rel_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	rel_tbl->count = (int) temp_int;

	/* malloc the list */
	rel_tbl->list = (REL_TBL_ELEM *) xmalloc((size_t) (temp_int * sizeof(REL_TBL_ELEM)));

	if (rel_tbl->list == NULL) {

		rel_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) rel_tbl->list, 0,(size_t) temp_int * sizeof(REL_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = rel_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->wao_item_tbl_offset = -1;
		}
		else {
			element->wao_item_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->unit_item_tbl_offset = -1;
		}
		else {
			element->unit_item_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->update_tbl_offset = -1;
		}
		else {
			element->update_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->update_count = (int) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_update_tbl
 *	ShortDesc: evaluate the UPDATE_TBL
 *
 *	Description:
 *		eval_dir_update_tbl will load the UPDATE_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		update_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_update_tbl(UPDATE_TBL *update_tbl, BININFO *bin)
{

	UPDATE_TBL_ELEM	*element;			/* temp pointer for the list */
	UPDATE_TBL_ELEM	*end_element;		/* end pointer for the list */
	DDL_UINT 		temp_int;	 		/* integer value */
	int				rc;					/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_UPDATE_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		update_tbl->count = 0;
		update_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	update_tbl->count = (int) temp_int;

	/* malloc the list */
	update_tbl->list = (UPDATE_TBL_ELEM *) xmalloc(
		(size_t) (temp_int * sizeof(UPDATE_TBL_ELEM) ) );

	if (update_tbl->list == NULL) {

		update_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) update_tbl->list, 0,
			(size_t) temp_int * sizeof(UPDATE_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = update_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->desc_it_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->op_it_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->op_subindex = (SUBINDEX) temp_int;
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_param_tbl
 *	ShortDesc: evaluate the PARAM_TBL
 *
 *	Description:
 *		eval_dir_param_tbl will load the PARAM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_tbl(PARAM_TBL *param_tbl, BININFO *bin)
{
	PARAM_TBL_ELEM	*element;	 		/* temp pointer for the list */
	DDL_UINT        temp_int;			/* integer value */
	DDL_UINT		count;				/* element count */
	DDL_UINT		tag;				/* the tag value */
	int				rc;					/* return code */
	unsigned long	inc = 0;			/* element tracker */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);

	/*
	 * if count is zero
	 */

	if(!count) {

		param_tbl->count = 0;
		param_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_tbl->count = (int) count;

	/* malloc the list */
	param_tbl->list = (PARAM_TBL_ELEM *)
			xmalloc((size_t) (count * sizeof(PARAM_TBL_ELEM)));

	if (param_tbl->list == NULL) {

		param_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_tbl->list, 0,
			(size_t) count * sizeof(PARAM_TBL_ELEM));

	/*
	 * load the list
	 */

	element = param_tbl->list;


	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {


		case PT_BLK_ITEM_NAME_TBL_OFFSET_TAG:

			/*
			 * Increment the element pointer
			 */

			if (inc > 0) {

				if(inc == count) {

					return DDL_ENCODING_ERROR;
				}
				element++;
			}
			++inc;

			/*
			 * Set optional table offsets to -1
		     */
			element->param_mem_tbl_offset = (int) -1;
			element->param_elem_tbl_offset = (int) -1;
			element->array_elem_item_tbl_offset = (int) -1;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_MEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_mem_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_MEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_mem_count = (int) temp_int;
			break;

		case PT_PARAM_ELEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_elem_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_ELEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_elem_count = (int) temp_int;
			break;

		case PT_PARAM_ELEM_MAX_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_count = (int) temp_int;
			break;

		case PT_ARRAY_ELEM__ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_item_tbl_offset = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_TYPE_OR_VAR_TYPE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_type_or_var_type = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_SIZE_OR_VAR_SIZE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_size_or_var_size = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_CLASS_VAR_CLASS_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_class_or_var_class = (ulong) temp_int;
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_blk_item_tbl
 *	ShortDesc: evaluate the BLK_ITEM_TBL
 *
 *	Description:
 *		eval_dir_blk_item_tbl will load the BLK_ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_tbl(BLK_ITEM_TBL *blk_item_tbl, BININFO *bin)
{

	BLK_ITEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	BLK_ITEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_BLK_ITEM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		blk_item_tbl->count = 0;
		blk_item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_tbl->count = (int) temp_int;

	/* malloc the list */
	blk_item_tbl->list = (BLK_ITEM_TBL_ELEM *) xmalloc((size_t) (temp_int * 
				sizeof(BLK_ITEM_TBL_ELEM) ) );

	if (blk_item_tbl->list == NULL) {

		blk_item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) blk_item_tbl->list, 0,
		(size_t) temp_int * sizeof(BLK_ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = blk_item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_id = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_param_mem_name_tbl will load the PARAM_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_name_tbl(PARAM_MEM_NAME_TBL *param_mem_name_tbl,
	BININFO *bin)
{

	PARAM_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;		 /* integer value */
	int					rc;				 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_MEM_NAME_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_name_tbl->count = 0;
		param_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_name_tbl->list = (PARAM_MEM_NAME_TBL_ELEM *) xmalloc((size_t) (temp_int * 
				sizeof(PARAM_MEM_NAME_TBL_ELEM) ) );

	if (param_mem_name_tbl->list == NULL) {

		param_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_mem_name_tbl->list, 0,
		(size_t) temp_int * sizeof(PARAM_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_mem_name_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_name = (ITEM_ID) temp_int;		

		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_elem_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_elem_tbl(PARAM_ELEM_TBL *param_elem_tbl, BININFO *bin)
{

	PARAM_ELEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_ELEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_ELEM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (PARAM_ELEM_TBL_ELEM *) xmalloc((size_t) (temp_int * 
				sizeof(PARAM_ELEM_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_elem_tbl->list, 0,
		(size_t) temp_int * sizeof(PARAM_ELEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_elem_subindex = (SUBINDEX) temp_int;		

		/* parse relation table offset */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_char_mem_name_tbl
 *	ShortDesc: evaluate the CHAR_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_char_mem_name_tbl will load the CHAR_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		char_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_char_mem_name_tbl(CHAR_MEM_NAME_TBL *char_mem_name_tbl, BININFO *bin)
{

	CHAR_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	CHAR_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;		 /* integer value */
	int					rc;				 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_CHAR_MEM_NAME_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		char_mem_name_tbl->count = 0;
		char_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	char_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	char_mem_name_tbl->list = (CHAR_MEM_NAME_TBL_ELEM *) xmalloc((size_t) (temp_int * 
				sizeof(CHAR_MEM_NAME_TBL_ELEM) ) );

	if (char_mem_name_tbl->list == NULL) {

		char_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) char_mem_name_tbl->list, 0,
		(size_t) temp_int * sizeof(CHAR_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = char_mem_name_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_name = (ITEM_ID) temp_int;		

		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_LIST_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_param_list_mem_name_tbl will load the PARAM_LIST_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_name_tbl(PARAM_LIST_MEM_NAME_TBL *param_list_mem_name_tbl,
	BININFO *bin)
{

	PARAM_LIST_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_LIST_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;				 /* integer value */
	int					rc;						 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_LIST_MEM_NAME_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_mem_name_tbl->count = 0;
		param_list_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_mem_name_tbl->list = (PARAM_LIST_MEM_NAME_TBL_ELEM *) xmalloc((size_t)
		(temp_int * sizeof(PARAM_LIST_MEM_NAME_TBL_ELEM) ) );

	if (param_list_mem_name_tbl->list == NULL) {

		param_list_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_list_mem_name_tbl->list, 0,
		(size_t) temp_int * sizeof(PARAM_LIST_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_mem_name_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_name = (ITEM_ID) temp_int;		

		/* parse item offset */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_tbl_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_blk_item_name_tbl
 *	ShortDesc: evaluate the BLK_ITEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_blk_item_name_tbl will load the BLK_ITEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_name_tbl(BLK_ITEM_NAME_TBL *blk_item_name_tbl, BININFO *bin)
{

	BLK_ITEM_NAME_TBL_ELEM	*element;	/* temp pointer for the list */
	DDL_UINT				count;		/* element count */
	DDL_UINT				tag;		/* the tag value */
	DDL_UINT       			temp_int;	/* integer value */
	int						rc;			/* return code */
	unsigned long			inc = 0;	/* element tracker */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_BLK_ITEM_NAME_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);

	/*
	 * if count is zero
	 */

	if(!count) {

		blk_item_name_tbl->count = 0;
		blk_item_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_name_tbl->count = (int) count;

	/* malloc the list */
	blk_item_name_tbl->list = (BLK_ITEM_NAME_TBL_ELEM *)
			xmalloc((size_t)(count * sizeof(BLK_ITEM_NAME_TBL_ELEM) ) );

	if (blk_item_name_tbl->list == NULL) {

		blk_item_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) blk_item_name_tbl->list, 0,
		(size_t) count * sizeof(BLK_ITEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	element = blk_item_name_tbl->list;

	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {

		case BINT_BLK_ITEM_NAME_TAG:
			/*
			 * Increment the element pointer
			 */

			if (inc > 0) {

				if(inc == count) {

					return DDL_ENCODING_ERROR;
				}

				++element;
			}
			++inc;

			/*
			 * Set optional table offsets to -1
		     */

			element->item_tbl_offset = (int) -1;
			element->param_tbl_offset = (int) -1;
			element->param_list_tbl_offset = (int) -1;
			element->rel_tbl_offset = (int) -1;
			element->read_cmd_tbl_offset = (int) -1;
			element->write_cmd_tbl_offset = (int) -1;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name = (ITEM_ID) temp_int;
			break;

		case BINT_ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_LIST_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_list_tbl_offset = (int) temp_int;
			break;

		case BINT_REL_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->rel_tbl_offset = (int) temp_int;
			break;

		case BINT_READ_CMD_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->read_cmd_tbl_offset = (int) temp_int;
			break;

		case BINT_READ_CMD_TBL_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->read_cmd_count = (int) temp_int;
			break;

		case BINT_WRITE_CMD_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->write_cmd_tbl_offset = (int) temp_int;
			break;

		case BINT_WRITE_CMD_TBL_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->write_cmd_count = (int) temp_int;
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_char_mem_tbl
 *	ShortDesc: evaluate the CHAR_MEM_TBL
 *
 *	Description:
 *		eval_dir_char_mem_tbl will load the CHAR_MEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		char_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
eval_dir_char_mem_tbl(CHAR_MEM_TBL *char_mem_tbl, BININFO *bin)
{

	CHAR_MEM_TBL_ELEM	*element;		/* temp pointer for the list */
	CHAR_MEM_TBL_ELEM	*end_element;	/* end pointer for the list */
	DDL_UINT 			temp_int; 		/* integer value */
	int					rc;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_CHAR_MEM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		char_mem_tbl->count = 0;
		char_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	char_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	char_mem_tbl->list = (CHAR_MEM_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(CHAR_MEM_TBL_ELEM) ) );

	if (char_mem_tbl->list == NULL) {

		char_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) char_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(CHAR_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = char_mem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_type = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_size = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_class = (ulong) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {		
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}


	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_tbl
 *	ShortDesc: evaluate the PARAM_LIST_MEM_TBL
 *
 *	Description:
 *		eval_dir_param_list_mem_tbl will load the PARAM_LIST_MEM_TBL
 *		structure by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_tbl(PARAM_LIST_MEM_TBL *param_list_mem_tbl,BININFO *bin)
{

	PARAM_LIST_MEM_TBL_ELEM	*element;	/* temp pointer for the list */
	PARAM_LIST_MEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT 				temp_int;	/* integer value */
	int						rc;			/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_LIST_MEM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_mem_tbl->count = 0;
		param_list_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_mem_tbl->list = (PARAM_LIST_MEM_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(PARAM_LIST_MEM_TBL_ELEM) ) );

	if (param_list_mem_tbl->list == NULL) {

		param_list_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_list_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_LIST_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_mem_tbl->list, end_element = element +
			temp_int; element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_mem_tbl
 *	ShortDesc: evaluate the PARAM_MEM_TBL
 *
 *	Description:
 *		eval_dir_param_mem_tbl will load the PARAM_MEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_tbl(PARAM_MEM_TBL *param_mem_tbl, BININFO *bin)
{
	PARAM_MEM_TBL_ELEM	*element;		/* temp pointer for the list */
	PARAM_MEM_TBL_ELEM	*end_element;	/* end pointer for the list */
	DDL_UINT            temp_int;	 	/* integer value */
	int                 rc;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	TEST_FAIL(EVAL_PARAM_MEM_TBL);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_tbl->count = 0;
		param_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_tbl->list = (PARAM_MEM_TBL_ELEM *) xmalloc(
			(size_t) (temp_int * sizeof(PARAM_MEM_TBL_ELEM) ) );

	if (param_mem_tbl->list == NULL) {

		param_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	(void)memset((char *) param_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_mem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_type = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_size = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_class = (ulong) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: dir_mask_man()
 *	ShortDesc: dir_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		dir_mask_man() handles all mask switch cases for the directory structures
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		bin_exists:		the mask which indicates which binaries are available
 *		bin_hooked:		the mask which indicates which binaries are hooked to the
 *						bin structure
 *		attr_avail:		
 *		(*eval):   		the eval function to call if the attribute needs evaluating
 *		bin:			the binary to evaluate
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by dir_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
dir_mask_man(unsigned long attr_mask, UINT32 bin_exists, UINT32 bin_hooked,
	UINT32 *attr_avail, int (*eval)(), void *attribute, BININFO *bin)
{

	int             rc;	/* return code */

	TEST_FAIL(MASK_MAN_DIR);

	/*
	 * No binary exists
	 */

	if (!(attr_mask & bin_exists)) {

		/*
		 * This is a DDOD error, by definition all directory tables
         * must have binary available
		 */

		rc = DDL_BINARY_REQUIRED;
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & bin_hooked)) {

		/*
		 * If value is already available
		 */

		if (attr_mask & *attr_avail) {

			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else {

			rc = DDL_BINARY_REQUIRED;
		}
	}

	else {

		/*
		 * check masks for evaluating
		 */

		if (!(attr_mask & *attr_avail)) {

			rc = (*eval) (attribute, bin);

			if (rc == DDL_SUCCESS) {

				*attr_avail |= attr_mask;
			}
		}

		/*
		 * evaluation is not necessary
		 */

		else {

			rc = DDL_SUCCESS;
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_dir_block_tables
 *	ShortDesc: evaluate (ie. create) the block tables
 *
 *	Description:
 *		eval_dir_block_tables will load the desired block tables into the 
 *		FLAT_BLOCK_DIR structure. The user must specify which block tables
 *		are desired by setting the appropriate bits in the "mask parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying which block tables are requested.
 *
 *	Outputs:
 *		block_dir:	pointer to a FLAT_BLOCK_DIR structure. Will contain
 *					the desired block tables.
 *      block_bin:  pointer to a BIN_BLOCK_DIR structure. Stores the 
 *					binary block information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		returns from other dds functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_dir_block_tables(FLAT_BLOCK_DIR *block_dir, BIN_BLOCK_DIR *block_bin,
	unsigned long mask)
{

	int  rc = DDS_SUCCESS;	/* return code */

	TEST_FAIL(I_EVAL_BLOCK_DIR);

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	if (mask & BLK_ITEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_blk_item_tbl,
			(void *) &block_dir->blk_item_tbl, &block_bin->blk_item_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & CHAR_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CHAR_MEM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_char_mem_tbl,
			(void *) &block_dir->char_mem_tbl, &block_bin->char_mem_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & BLK_ITEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_NAME_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_blk_item_name_tbl,
			(void *) &block_dir->blk_item_name_tbl, &block_bin->blk_item_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_list_mem_tbl,
			(void *) &block_dir->param_list_mem_tbl, &block_bin->param_list_mem_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_list_tbl,
			(void *) &block_dir->param_list_tbl, &block_bin->param_list_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}	

	if (mask & PARAM_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_mem_tbl,
			(void *) &block_dir->param_mem_tbl, &block_bin->param_mem_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_tbl,
			(void *) &block_dir->param_tbl, &block_bin->param_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_NAME_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_mem_name_tbl,
			(void *) &block_dir->param_mem_name_tbl, &block_bin->param_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_ELEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_ELEM_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_elem_tbl,
			(void *) &block_dir->param_elem_tbl, &block_bin->param_elem_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_NAME_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_param_list_mem_name_tbl,
			(void *) &block_dir->param_list_mem_name_tbl,
			&block_bin->param_list_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & CHAR_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CHAR_MEM_NAME_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_char_mem_name_tbl,
			(void *) &block_dir->char_mem_name_tbl, &block_bin->char_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & REL_TBL_MASK) {

		rc = dir_mask_man((unsigned long) REL_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_rel_tbl,
			(void *) &block_dir->rel_tbl, &block_bin->rel_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & UPDATE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) UPDATE_TBL_MASK,
			block_bin->bin_exists, block_bin->bin_hooked,
			(UINT32*)&block_dir->attr_avail, (int (*) ()) eval_dir_update_tbl,
			(void *) &block_dir->update_tbl, &block_bin->update_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}


#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif /* DEBUG */
	
	return rc;
}


/*********************************************************************
 *
 *	Name: eval_clean_block_dir
 *	ShortDesc: Frees the block tables
 *
 *	Description:
 *		eval_clean_block_dir will check the attr_avail flags
 *      in the FLAT_BLOCK_DIR structure to see which block tables
 *		exist and free them. Everything in the structure is then 
 *		set to zero.
 *
 *	Inputs:
 *		block_dir: 	pointer to a FLAT_BLOCK_DIR to be cleaned.
 *
 *	Outputs:
 *		block_dir: 	pointer to the empty FLAT_BLOCK_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_block_dir(FLAT_BLOCK_DIR *block_dir)
{
	ulong           temp_attr_avail;

	if (block_dir == NULL) {
		return;
	}
	
#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	temp_attr_avail = block_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_ITEM_TBL_MASK) && 
		(block_dir->blk_item_tbl.list)) {
		xfree((void **) &block_dir->blk_item_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_TBL_MASK) && 
		(block_dir->char_mem_tbl.list)) {
		xfree((void **) &block_dir->char_mem_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_NAME_TBL_MASK) && 
		(block_dir->char_mem_name_tbl.list)) {
		xfree((void **) &block_dir->char_mem_name_tbl.list);
	}

	if ((temp_attr_avail & BLK_ITEM_NAME_TBL_MASK) && 
		(block_dir->blk_item_name_tbl.list)) {
		xfree((void **) &block_dir->blk_item_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_TBL_MASK) && 
		(block_dir->param_list_mem_tbl.list)) {
		xfree((void **) &block_dir->param_list_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_NAME_TBL_MASK) && 
		(block_dir->param_list_mem_name_tbl.list)) {
		xfree((void **) &block_dir->param_list_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_TBL_MASK) && 
		(block_dir->param_mem_tbl.list)) {
		xfree((void **) &block_dir->param_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_NAME_TBL_MASK) && 
		(block_dir->param_mem_name_tbl.list)) {
		xfree((void **) &block_dir->param_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_ELEM_TBL_MASK) && 
		(block_dir->param_elem_tbl.list)) {
		xfree((void **) &block_dir->param_elem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_TBL_MASK) && 
		(block_dir->param_list_tbl.list)) {
		xfree((void **) &block_dir->param_list_tbl.list);
	}

	if ((temp_attr_avail & PARAM_TBL_MASK) && 
		(block_dir->param_tbl.list)) {
		xfree((void **) &block_dir->param_tbl.list);
	}

	if ((temp_attr_avail & REL_TBL_MASK) && 
		(block_dir->rel_tbl.list)) {
		xfree((void **) &block_dir->rel_tbl.list);
	}

	if ((temp_attr_avail & UPDATE_TBL_MASK) && 
		(block_dir->update_tbl.list)) {
		xfree((void **) &block_dir->update_tbl.list);
	}


	(void)memset((char *) block_dir, 0, sizeof(FLAT_BLOCK_DIR));

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif /* HART */
	
}

/*********************************************************************
 *
 *	Name: eval_dir_device_tables
 *	ShortDesc: evaluate (ie. create) the device tables
 *
 *	Description:
 *		eval_dir_device_tables will load the desired device tables into the 
 *		FLAT_DEVICE_DIR structure. The user must specify which device tables
 *		are desired by setting the appropriate bits in the "mask" parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying the desired device tables.
 *
 *	Outputs:
 *		device_dir:	pointer to a FLAT_DEVICE_DIR structure. Will contain
 *					the desired device tables.
 *      device_bin: pointer to a BIN_DEVICE_DIR structure. Stores the 
 *					binary device information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		return codes from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_dir_device_tables(FLAT_DEVICE_DIR *device_dir, BIN_DEVICE_DIR *device_bin,
	unsigned long mask)
{

	int  rc = DDS_SUCCESS;	/* return code */

	TEST_FAIL(I_EVAL_DEVICE_DIR);

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	if (mask & BLK_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_blk_tbl,
			(void *) &device_dir->blk_tbl, &device_bin->blk_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & DICT_REF_TBL_MASK) {

		rc = dir_mask_man((unsigned long) DICT_REF_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_dict_ref_tbl,
			(void *) &device_dir->dict_ref_tbl, &device_bin->dict_ref_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & DOMAIN_TBL_MASK) {

		rc = dir_mask_man((unsigned long) DOMAIN_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_domain_tbl,
			(void *) &device_dir->domain_tbl, &device_bin->domain_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & AXIS_TBL_MASK) {

		rc = dir_mask_man((unsigned long) AXIS_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_axis_tbl,
			(void *) &device_dir->axis_tbl, &device_bin->axis_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & CHART_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CHART_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_chart_tbl,
			(void *) &device_dir->chart_tbl, &device_bin->chart_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & FILE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) FILE_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_file_tbl,
			(void *) &device_dir->file_tbl, &device_bin->file_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & GRAPH_TBL_MASK) {

		rc = dir_mask_man((unsigned long) GRAPH_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_graph_tbl,
			(void *) &device_dir->graph_tbl, &device_bin->graph_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & LIST_TBL_MASK) {

		rc = dir_mask_man((unsigned long) LIST_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_list_tbl,
			(void *) &device_dir->list_tbl, &device_bin->list_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & SOURCE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) SOURCE_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_source_tbl,
			(void *) &device_dir->source_tbl, &device_bin->source_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & WAVEFORM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) WAVEFORM_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_waveform_tbl,
			(void *) &device_dir->waveform_tbl, &device_bin->waveform_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}
	if (mask & ITEM_TBL_MASK) {

		ITEM_AND_EXPANDED_TBLS tbl_holder; /* This holds pointers to both tables encoded in this chunk */
		tbl_holder.item_tbl = &device_dir->item_tbl;
		tbl_holder.expanded_attr_tbl = &device_dir->expanded_attr_tbl;

		rc = dir_mask_man((unsigned long) ITEM_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_item_tbl,
			(void *) &tbl_holder, &device_bin->item_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & PROG_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PROG_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_prog_tbl,
			(void *) &device_dir->prog_tbl, &device_bin->prog_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & STRING_TBL_MASK) {

		rc = dir_mask_man((unsigned long) STRING_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_string_tbl,
			(void *) &device_dir->string_tbl, &device_bin->string_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & BINARY_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BINARY_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_binary_tbl,
			(void *) &device_dir->binary_tbl, &device_bin->binary_tbl);

		if ((rc != DDL_SUCCESS) && (rc != DDL_BINARY_REQUIRED)) {
			return rc;
		}
	}

	if (mask & LOCAL_VAR_TBL_MASK) {

		rc = dir_mask_man((unsigned long) LOCAL_VAR_TBL_MASK,
			device_bin->bin_exists, device_bin->bin_hooked,
			(UINT32*)&device_dir->attr_avail, (int (*) ()) eval_dir_local_var_tbl,
			(void *) &device_dir->local_var_tbl, &device_bin->local_var_tbl);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}


#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	return rc;
}


/*********************************************************************
 *
 *	Name: eval_clean_device_dir
 *	ShortDesc: Free the device tablesIR structure
 *
 *	Description:
 *		eval_clean_device_dir will check the attr_avail flags
 *      in the FLAT_DEVICE_DIR structure to see which device tables
 *		exist and free them. Everything in the structure is then
 *		set to zero.
 *
 *	Inputs:
 *		device_dir: pointer to the FLAT_DEVICE_DIR to be cleaned.
 *
 *	Outputs:
 *		device_dir: pointer to the empty FLAT_DEVICE_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_device_dir(FLAT_DEVICE_DIR *device_dir)
{
	ulong           temp_attr_avail;

	if (device_dir == NULL) {
		return;
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	temp_attr_avail = device_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_TBL_MASK) && 
		(device_dir->blk_tbl.list)) {
		xfree((void **) &device_dir->blk_tbl.list);
	}

	if ((temp_attr_avail & DICT_REF_TBL_MASK) && 
		(device_dir->dict_ref_tbl.list)) {
		xfree((void **) &device_dir->dict_ref_tbl.list);
	}

	if ((temp_attr_avail & DOMAIN_TBL_MASK) && 
		(device_dir->domain_tbl.list)) {
		xfree((void **) &device_dir->domain_tbl.list);
	}

	if ((temp_attr_avail & AXIS_TBL_MASK) && 
		(device_dir->axis_tbl.list)) {
		xfree((void **) &device_dir->axis_tbl.list);
	}

	if ((temp_attr_avail & CHART_TBL_MASK) && 
		(device_dir->chart_tbl.list)) {
		xfree((void **) &device_dir->chart_tbl.list);
	}

	if ((temp_attr_avail & FILE_TBL_MASK) && 
		(device_dir->file_tbl.list)) {
		xfree((void **) &device_dir->file_tbl.list);
	}

	if ((temp_attr_avail & GRAPH_TBL_MASK) && 
		(device_dir->graph_tbl.list)) {
		xfree((void **) &device_dir->graph_tbl.list);
	}

	if ((temp_attr_avail & LIST_TBL_MASK) && 
		(device_dir->list_tbl.list)) {
		xfree((void **) &device_dir->list_tbl.list);
	}

	if ((temp_attr_avail & SOURCE_TBL_MASK) && 
		(device_dir->source_tbl.list)) {
		xfree((void **) &device_dir->source_tbl.list);
	}

	if ((temp_attr_avail & WAVEFORM_TBL_MASK) && 
		(device_dir->waveform_tbl.list)) {
		xfree((void **) &device_dir->waveform_tbl.list);
	}

	if (temp_attr_avail & ITEM_TBL_MASK) {
        if (device_dir->item_tbl.list) {
            xfree((void **)&device_dir->item_tbl.list);
        }
        if (device_dir->expanded_attr_tbl.list) {
            xfree((void **)&device_dir->expanded_attr_tbl.list);
        }
    }

	if ((temp_attr_avail & PROG_TBL_MASK) && 
		(device_dir->prog_tbl.list)) {
		xfree((void **) &device_dir->prog_tbl.list);
	}

	if ((temp_attr_avail & STRING_TBL_MASK) && 
		(device_dir->string_tbl.list)) {

		xfree((void **) &device_dir->string_tbl.root);
		xfree((void **) &device_dir->string_tbl.list);
	}

	if ((temp_attr_avail & BINARY_TBL_MASK) && 
		(device_dir->binary_tbl.list)) {

		xfree((void **) &device_dir->binary_tbl.root);
		xfree((void **) &device_dir->binary_tbl.list);
	}

	if ((temp_attr_avail & LOCAL_VAR_TBL_MASK) && 
		(device_dir->local_var_tbl.list)) {

		xfree((void **) &device_dir->local_var_tbl.list);
	}


	(void)memset((char *) device_dir, 0, sizeof(FLAT_DEVICE_DIR));

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif /* DEBUG */
	
}
