/*
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *
 *	This file contains all functions relating to
 *	resolving references
 */

#ifdef SUN
#include <memory.h>		/* K&R */
#else
#include <string.h>		/* ANSI */
#endif /* SUN */

#include "std.h"
#include "evl_loc.h"
#include "app_xmal.h"
#include "cm_lib.h"
#include "fch_lib.h"
#include "dds_tab.h"
#include "cm_lib.h"
#include "tst_fail.h"
#include "pc_lib.h"

/*********************************************************************
* lookup table for converting parse tags to item types
* used by both resolve_ref() and parse_ref()
*********************************************************************/

const
int             resolve_id_table[] = {0,
	ITEM_ARRAY_ITYPE,
	COLLECTION_ITYPE,
	ITEM_ARRAY_ITYPE,
	COLLECTION_ITYPE,
	RECORD_ITYPE,
	ARRAY_ITYPE,
	VAR_LIST_ITYPE,
	PARAM_ITYPE,
	PARAM_LIST_ITYPE,
	BLOCK_ITYPE,
	BLOCK_ITYPE,
	VARIABLE_ITYPE,
	MENU_ITYPE,
	EDIT_DISP_ITYPE,
	METHOD_ITYPE,
	REFRESH_ITYPE,
	UNIT_ITYPE,
	WAO_ITYPE,
	RECORD_ITYPE,
	ARRAY_ITYPE,
	VAR_LIST_ITYPE,
	PROGRAM_ITYPE,
	DOMAIN_ITYPE,
	RESP_CODES_ITYPE,
    AXIS_ITYPE,             /* AXIS_ID_REF            */
    CHART_ITYPE,            /* CHART_ID_REF           */
    FILE_ITYPE,             /* FILE_ID_REF            */
    GRAPH_ITYPE,            /* GRAPH_ID_REF           */
    LIST_ITYPE,             /* LIST_ID_REF            */
    SOURCE_ITYPE,           /* SOURCE_ID_REF          */
    WAVEFORM_ITYPE,         /* WAVEFORM_ID_REF        */
    0,
    0,
    0,
    0,
    0,
    0,
    GRID_ITYPE,             /* GRID_ID_REF */
    IMAGE_ITYPE,            /* IMAGE_ID_REF */
    0,                      /* VIA_BIT_ENUM_REF 40 */
    BLOCK_XREF_ITYPE,       /* BLOCK_ID_XREF    41 Crossblock reference (block ID) */
    BLOCK_XREF_ITYPE        /* VIA_BLOCK_XREF   42 Crossblock reference (instance index) */
};

int (*app_func_get_block_handle) P((ENV_INFO *env_info, ITEM_ID block_id,
                              unsigned int instance_num, BLOCK_HANDLE *out_bh)) = NULL;

/**********************************************************************
 *
 *  Name: rslv_find_local
 *  ShortDesc:	Given Member ID, find the Item ID for a LOCAL
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Given a member ID for a LOCAL PARAMETER of a block, find the
 *      DESC_REF record associated with it.
 *
 *  Inputs:
 *		block_handle, Member ID
 *
 *  Outputs:
 *      DESC_REF record will be overwritten if successful, zeroed if not
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/
int rslv_find_local(BLOCK_HANDLE block_handle, ITEM_ID memberID, DESC_REF *out_mem)
{
    DDI_BLOCK_SPECIFIER bs;
    DDI_ITEM_SPECIFIER is;
    DDI_GENERIC_ITEM gi;
    ENV_INFO env;
    int rs;
    FLAT_BLOCK *fb;
    int i;

    bs.type = DDI_BLOCK_HANDLE;
    bs.block.handle = block_handle;
    is.type = DDI_ITEM_BLOCK;
    env.block_handle = block_handle;
    env.app_info = NULL;

    is.type = DDI_ITEM_BLOCK;
    memset(&gi, 0, sizeof(DDI_GENERIC_ITEM));

    rs = ddi_get_item(&bs, &is, &env, BLOCK_LOCAL_PARAM, &gi);
    if (rs)
        return rs;

    fb = (FLAT_BLOCK *)gi.item;
    for (i=0; i < fb->local_param.count; i++)
    {
        if (memberID == fb->local_param.list[i].name)
        {
            memcpy(out_mem, &fb->local_param.list[i].ref, sizeof(DESC_REF));
            ddi_clean_item(&gi);
            return 0;
        }
    }
    ddi_clean_item(&gi);
    return PC_PARAM_NOT_FOUND;
}


/**********************************************************************
 *
 *  Name: rslv_get_one_local
 *  ShortDesc:	 Get a single local_param for a block
 *
 *  Include:
 *
 *  Description:
 *		pc get_one_local gets the specified local parameter for the block.
 *      The caller is responsible for freeing the DDI_GENERIC_ITEM record
 *      by calling ddi_clean_item.
 *
 *  Inputs:
 *		block_handle, MEMBER that represents the local param
 *
 *  Outputs:
 *      A DDI_GENERIC_ITEM structure for the VARIABLE, RECORD, or ARRAY.
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/
int rslv_get_one_local(BLOCK_HANDLE block_handle, MEMBER *m, DDI_GENERIC_ITEM *gi)
{
    DDI_BLOCK_SPECIFIER bs;
    DDI_ITEM_SPECIFIER is;
    ENV_INFO env;
    int attr;

    bs.type = DDI_BLOCK_HANDLE;
    bs.block.handle = block_handle;
    is.type = DDI_ITEM_ID;
    is.item.id = m->ref.id;
    env.block_handle = block_handle;
    env.app_info = NULL;
    memset(gi, 0, sizeof(DDI_GENERIC_ITEM));
    switch(m->ref.type)
    {
    case VARIABLE_ITYPE:
        attr = VAR_TYPE_SIZE | VAR_CLASS;
        break;
    case ARRAY_ITYPE:
        attr = ARRAY_NUM_OF_ELEMENTS | ARRAY_TYPE;
        break;
    case RECORD_ITYPE:
        attr = RECORD_MEMBERS;
        break;
    case LIST_ITYPE:
        attr = LIST_TYPE;
        break;
    default:
        return PC_WRONG_DATA_TYPE;
    }

    return ddi_get_item(&bs, &is, &env, attr, gi);
}


/*********************************************************************
 *
 *	Name: resolve_fetch()
 *	ShortDesc: fetch the binary for the given flat item
 *
 *	Description:
 *		resolve_fetch determines which type of fetch we are using
 *		and makes the appropriate call to fetch the binary for the
 *		given flat item
 *
 *	Inputs:
 *		item_id:      the item id of the item to fetch
 *		mask:         the request mask for the attributes of the item
 *		item_type:    the type of the flat item
 *		block_handle: the handle of the current block
 *		scratch:	  a pointer to an array of at least two SCRATCH_PADs
 *                    which are used by FF fetch to store the binaries
 *
 *	Outputs:
 *		item:        the flat item with binaries attached
 *
 *
 *	Returns: DDI_INVALID_BLOCK_HANDLE
 *           DDL_RESOLVE_FETCH_FAIL
 *			 DDL_MEMORY_ERROR
 *			 and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

/*
 * PAD_START_SIZE is the number of bytes initially malloced
 *  as a scratchpad
 *
 * PAD_MAX_SIZE defines the maximum value that the static
 * variable pad_size can grow to
 */

#define PAD_START_SIZE		128
#define PAD_MAX_SIZE 		1024

int
resolve_fetch(ITEM_ID item_id, unsigned long mask, void *item,
	unsigned short item_type, BLOCK_HANDLE block_handle, SCRATCH_PAD *scratch)
{

	static unsigned long pad_size = PAD_START_SIZE;	/* pad malloc size */
	DEVICE_HANDLE   device_handle = 0; /* device handle from the block handle */
	int             fetch_type = 0;	   /* FF or alternate fetch indicator */
	int             rc;	               /* return code */
	unsigned long   add_size;	       /* additional size for fetch */

	TEST_FAIL(RESOLVE_FETCH);

	/*
	 * Check for a valid block handle
	 */

	if (!valid_block_handle(block_handle)) {

		return DDI_INVALID_BLOCK_HANDLE;
	}

	rc  = get_abt_adt_offset(block_handle, &device_handle);
	if (rc != SUCCESS) {
		return(rc) ;
	}
	rc = get_abt_dev_family(block_handle, &fetch_type);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	switch (fetch_type) {


	case DF_FF:

		/*
		 * Check scratch pointers and the fetch_item_func pointer
		 */

		ASSERT_RET(scratch || (scratch + 1), DDL_NULL_POINTER);

		scratch->pad = NULL;
		scratch->used = 0;
		scratch->size = 0;

		(scratch + 1)->pad = NULL;
		(scratch + 1)->used = 0;
		(scratch + 1)->size = 0;

		scratch->pad =
			(unsigned char *) xmalloc((size_t) (pad_size) * sizeof(unsigned char));
		if (scratch->pad == NULL) {

			return DDL_MEMORY_ERROR;
		}

		scratch->used = 0;
		scratch->size = pad_size;
		rc = fch_item(device_handle, item_id, scratch, &add_size, mask, item, item_type);

		if (rc == FETCH_INSUFFICIENT_SCRATCHPAD) {

			/*
			 * Not enough memory on first round, so malloc more and
			 * call fetch again
			 */

			++scratch;	/* increment the pointer */

			scratch->pad =
				(unsigned char *) xmalloc((size_t) (add_size) * sizeof(unsigned char));

			if (scratch->pad == NULL) {

				return DDL_MEMORY_ERROR;
			}

			scratch->used = 0;
			scratch->size = add_size;

			if ((add_size + pad_size) < PAD_MAX_SIZE) {

				pad_size += add_size;
			}

			rc = fch_item(device_handle, item_id, scratch, &add_size, mask,
				item, item_type);
		}
		break;

	default:
		rc = DDL_BAD_FETCH_TYPE;
		break;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: resolve_fetch_free()
 *	ShortDesc: free the memory malloced by calls to resolve_fetch()
 *
 *	Description:
 *		resolve_fetch_free determines which fetch was used with
 *		the item and then frees the memory which was malloced
 *		for the binaries
 *
 *	Inputs:
 *		item_type:    the type of flat item to free
 *		scratch:	  the pointers to the malloced scratchpads
 *		block_handle: the block handle associated with the item
 *
 *	Outputs:
 *		item:         a pointer to the flat item with the
 *		              binaries freed
 *	Returns:
 *		 DDI_INVALID_BLOCK_HANDLE
 *		 DDL_BAD_FETCH_TYPE
 *		 DDL_SUCCESS
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
resolve_fetch_free(void *item, unsigned short item_type,
	SCRATCH_PAD *scratch, BLOCK_HANDLE block_handle)
{

	int             fetch_type = 0;	    /* fetch type indicator */
	int				rc ;

	if (!valid_block_handle(block_handle)) {

		return;
	}

	rc = get_abt_dev_family(block_handle, &fetch_type);
	ASSERT_DBG (rc == SUCCESS) ; 

	switch (fetch_type) {


	case DF_FF:

		if (scratch->pad != NULL) {
			xfree((void **) &scratch->pad);
		}
		++scratch;

		if (scratch->pad != NULL) {
			xfree((void **) &scratch->pad);
		}

		switch (item_type) {

		case ITEM_ARRAY_ITYPE:
			((FLAT_ITEM_ARRAY *) item)->masks.bin_hooked = 0;
			((FLAT_ITEM_ARRAY *) item)->depbin = NULL;
			break;

		case COLLECTION_ITYPE:
			((FLAT_COLLECTION *) item)->masks.bin_hooked = 0;
			((FLAT_COLLECTION *) item)->depbin = NULL;
			break;

		case VARIABLE_ITYPE:
			((FLAT_VAR *) item)->masks.bin_hooked = 0;
			((FLAT_VAR *) item)->depbin = NULL;
			break;

        case CHART_ITYPE:
            ((FLAT_CHART *) item)->masks.bin_hooked = 0;
            ((FLAT_CHART *) item)->depbin = NULL;
            break;
        case FILE_ITYPE:
            ((FLAT_FILE *) item)->masks.bin_hooked = 0;
            ((FLAT_FILE *) item)->depbin = NULL;
            break;
        case GRAPH_ITYPE:
            ((FLAT_GRAPH *) item)->masks.bin_hooked = 0;
            ((FLAT_GRAPH *) item)->depbin = NULL;
            break;
        case SOURCE_ITYPE:
            ((FLAT_SOURCE *) item)->masks.bin_hooked = 0;
            ((FLAT_SOURCE *) item)->depbin = NULL;
            break;

		default:
			CRASH_DBG();

		}
		break;

	default:
		return;
	}

	return;
}

/* Convert an old-style ENV_INFO to a new ENV_INFO2 */
void dds_env_to_env2(ENV_INFO *env_info, ENV_INFO2 *env_info2)
{
    env_info2->app_info = env_info->app_info;
    env_info2->block_handle = env_info->block_handle;
    env_info2->device_handle = 0;
    env_info2->type = ENV_BLOCK_HANDLE;
}


/*********************************************************************
 *
 *	Name: resolve_ref
 *
 *	ShortDesc: resolve a ddl reference
 *
 *	Description:
 *		resolve_ref takes a ref_info array and resolves it
 *		to the description reference which is the item_id and
 * 		type of the reference. It will also load an operational
 *		reference and a resolve trail if the options argument is
 *		set accordingly
 *
 *	Inputs:
 *		ref_info:	  A pointer to the list of reference information
 *					  used to resolve a reference.
 *		depinfo:	  The dependency information if any part of the
 *					  reference has a dependency.
 *		env_info:     The env_info for the reference to be resolved.
 *		var_needed:	  The variable for which a value is needed if
 *					  the parameter value service did not succeed.
 *		options:	  The options mask for requesting different types
 *					  of reference information including RESOLVE_TRAIL
 *					  RESOLVE_OP_REF and RESOLVE_DESC_REF
 *		extra_trail:  The number of trail elements to be added beyond
 *					  the standard count.
 *
 *	Outputs:
 *		ref:		  A pointer to the op_ref_trail which is loaded with
 *					  the requested resolved reference information.
 *
 *	Returns:
 *		DDI_INVALID_BLOCK_HANDLE
 *		and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
resolve_ref(REF_INFO_LIST *ref_info, OP_REF_TRAIL *ref, OP_REF_LIST *depinfo,
	ENV_INFO2 *env_info, OP_REF *var_needed, unsigned int options,
	unsigned short extra_trail)
{

	int             fetch_type = 0;	    /* fetch type indicator */
	int             rc = DDL_SUCCESS;	/* return code */
	int             rc2 = DDL_SUCCESS;	/* secondary return code */
	int             free_attr = FALSE;	/* free attribute indicator */
	int             info_inc;	        /* ref_info counter */
	REF_INFO       *info_ptr;	        /* ref_info pointer */
	unsigned short  inc;	            /* incrementer for item lists */
	int             count;	            /* count for item lists */
	unsigned short  trail_inc = 0;	    /* trail incrementer */
	SCRATCH_PAD     scratch[2];	        /* scratch_pad array */
	SUBINDEX        subindex;	    	/* subindex for name to subindex */
	BLK_TBL_ELEM   *bt_elem = NULL;		/* Block Table element pointer */
	ITEM_TBL       *it;	           		/* item table */
	ITEM_TBL_ELEM  *it_elem;       		/* item table element */
	FLAT_ITEM_ARRAY     item_array;	    /* item array for fetch and resolve */
	ITEM_ARRAY_ELEMENT *item_array_ptr; /* item array pointer for speed */
	FLAT_COLLECTION collection;	        /* collection for fetch and resolve */
	MEMBER         *member_ptr;	        /* collection pointer for speed */
    OP_MEMBER      *op_member_ptr;      /* for use with SOURCEs */
	DEPBIN		   *cur_depbin;			/* Current dependency and binary */
	ITEM_ID         current_id = 0;	    /* the current id in the resolve trail */
	ITEM_ID         next_id = 0;	    /* id argument to table request functions */
	ITEM_TYPE       current_type = 0;	/* current type in the resolve trail */
	FLAT_DEVICE_DIR	*flat_device_dir ;
	int				blk_tbl_offset ;
    FLAT_CHART      chart;
    FLAT_FILE       file;
    FLAT_GRAPH      graph;
    FLAT_SOURCE     source;
    ENV_INFO2       env;
    int             stop_at_index = 0;

	TEST_FAIL(RESOLVE_REF);

	/*
	 * malloc the ref_trail if requested
	 */

	if (options & RESOLVE_TRAIL) {

		ref->trail_limit = ref_info->count + extra_trail + 1;
		ref->trail =
			(RESOLVE_INFO *) xmalloc((size_t) ref->trail_limit * sizeof(RESOLVE_INFO));
		if (ref->trail == NULL) {
			return	DDL_MEMORY_ERROR;
		}
	}

    /* If this is a cross-block reference, override the env_info and 
     * its associated block_handle passed to us, with the block specified
     * in the reference 
     */
    if ((ref_info->count > 1) && (ref_info->list[0].type == BLOCK_ID_XREF)
        && (ref_info->list[1].type == VIA_BLOCK_XREF))
    {
        /* If the linked application has not specified a block instance 
         * lookup callback, we cannot proceed with this reference.
         */
        if (!app_func_get_block_handle)
        {
            xfree((void **)&ref->trail);
            ref->trail_limit = ref->trail_count = 0;
            return DDI_BLOCK_NOT_FOUND;
        }
        {
            env.app_info = env_info->app_info;
            env.type = ENV_BLOCK_HANDLE;
            env.device_handle = 0;

            rc = (*app_func_get_block_handle)((ENV_INFO *)env_info,
                ref_info->list[0].val.id, ref_info->list[1].val.index,
                 &env.block_handle);
            if (rc)
            {
    	        if (options & RESOLVE_TRAIL)
                    xfree((void **)&ref->trail);
                ref->trail_limit = ref->trail_count = 0;
                return rc;
            }
        }

        env_info = &env;
        /* Force loop below to skip elements 0 and 1, which we have just processed */
        stop_at_index = 2;

		if (options & RESOLVE_TRAIL) {

			ref->trail[trail_inc].id = ref_info->list[0].val.id;
			ref->trail[trail_inc].type = BLOCK_XREF_ITYPE;
            ref->trail[trail_inc].element = ref_info->list[1].val.index;
            trail_inc++;
		}
        ref->op_block_id = ref_info->list[0].val.id;
        ref->op_block_instance = ref_info->list[1].val.index;
    }
    else
    {
        //env_info = (ENV_INFO *)in_env_info;
        ref->op_block_id = 0;
        ref->op_block_instance = 0;
    }

    /*
	 * Check for valid block handle
	 */

	if (!valid_block_handle(env_info->block_handle)) {

    	if (options & RESOLVE_TRAIL)
            xfree((void **)&ref->trail);
        ref->trail_limit = ref->trail_count = 0;
		return DDI_INVALID_BLOCK_HANDLE;
	}

	/*
	 * get the fetch type
	 */

	rc = get_abt_dev_family(env_info->block_handle, &fetch_type);
	if (rc != SUCCESS) {
        xfree((void **)&ref->trail);
        ref->trail_limit = ref->trail_count = 0;
		return(rc) ;
	}

	switch (fetch_type) {


	case DF_FF:

		/*
		 * Check that tables exist
         */

        /* We must have a block-type environment at this point */
        if (env_info->type != ENV_BLOCK_HANDLE)
            return DDI_BLOCK_NOT_FOUND;

		rc = get_abt_dd_blk_tbl_offset(env_info->block_handle, &blk_tbl_offset) ;
		if (rc != SUCCESS) {
            xfree((void **)&ref->trail);
            ref->trail_limit = ref->trail_count = 0;
			return(rc) ;
		}
		if ((blk_tbl_offset) < 0) {

            xfree((void **)&ref->trail);
            ref->trail_limit = ref->trail_count = 0;
			return DDL_BLOCK_TABLES_NOT_LOADED; 
		}

		rc = block_handle_to_bt_elem(env_info->block_handle, &bt_elem);
		if (rc != DDS_SUCCESS) {

            xfree((void **)&ref->trail);
            ref->trail_limit = ref->trail_count = 0;
			return rc;
		}
	}

	/*
	 * memset the flat item structs
	 */

	(void)memset((char *) &item_array, 0, sizeof(FLAT_ITEM_ARRAY));
	(void)memset((char *) &collection, 0, sizeof(FLAT_COLLECTION));
	(void)memset((char *) &chart, 0, sizeof(FLAT_CHART));
	(void)memset((char *) &file, 0, sizeof(FLAT_FILE));
	(void)memset((char *) &graph, 0, sizeof(FLAT_GRAPH));
	(void)memset((char *) &source, 0, sizeof(FLAT_SOURCE));


	/*
	 * Move backward through the info list to resolve the reference
	 */

	for (info_inc = (ref_info->count - 1), info_ptr = &ref_info->list[info_inc];
		info_inc >= stop_at_index; info_inc--, info_ptr--) {

		switch (info_ptr->type) {

		case ITEM_ID_REF:
		case ITEM_ARRAY_ID_REF:
		case COLLECTION_ID_REF:
		case BLOCK_ID_REF:
		case VARIABLE_ID_REF:
		case MENU_ID_REF:
		case EDIT_DISP_ID_REF:
		case METHOD_ID_REF:
		case REFRESH_ID_REF:
		case UNIT_ID_REF:
		case WAO_ID_REF:
		case RECORD_ID_REF:
		case ARRAY_ID_REF:
		case VAR_LIST_ID_REF:
		case PROGRAM_ID_REF:
		case DOMAIN_ID_REF:
		case RESP_CODES_ID_REF:
        case CHART_ID_REF:
        case AXIS_ID_REF:
        case FILE_ID_REF:
        case GRAPH_ID_REF:
        case LIST_ID_REF:
        case SOURCE_ID_REF:
        case WAVEFORM_ID_REF:
        case GRID_ID_REF:
        case IMAGE_ID_REF:
			current_id = info_ptr->val.id;
			current_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];

			if (options & RESOLVE_OP_REF) {

				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = 0;
			}
			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case VIA_ITEM_ARRAY_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) ITEM_ARRAY_ELEMENTS,
				(void *) &item_array, (unsigned short) ITEM_ARRAY_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(item_array.masks.attr_avail & ITEM_ARRAY_ELEMENTS)) {

					/*
					 * eval the binary
					 */
					cur_depbin = item_array.depbin->db_elements;

					rc = eval_attr_itemarray(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &item_array.elements,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {

				/*
				 * find the index in the item_array list
				 */

				item_array_ptr = item_array.elements.list;
				count = item_array.elements.count;

				for (inc = 0; inc < count; item_array_ptr++, inc++) {

					if (item_array_ptr->index == info_ptr->val.index) {

						current_id = item_array_ptr->ref.id;
						current_type = item_array_ptr->ref.type;

						/*
						 * if the current type is an ITEM_ID_REF convert this
						 * to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}
						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = item_array_ptr->index;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}
						break;
					}
				}

				if (inc == count) {			/* The element was not found */
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &item_array,
						(unsigned short) ITEM_ARRAY_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_itemarray_list(&item_array.elements, FREE_ATTR);
				free_attr = FALSE;
			}
			break;

		case VIA_COLLECTION_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) COLLECTION_MEMBERS,
				(void *) &collection, (unsigned short) COLLECTION_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(collection.masks.attr_avail & COLLECTION_MEMBERS)) {

					/*
					 * eval the binary
					 */
					cur_depbin = collection.depbin->db_members;

					rc = eval_attr_op_members(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &collection.members,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {
				/*
				 * find the member name in the collection list
				 */

				op_member_ptr = collection.members.list;
				count = collection.members.count;

				for (inc = 0; inc < count; op_member_ptr++, inc++) {

					if (op_member_ptr->name == info_ptr->val.member) {

						current_id = op_member_ptr->ref.op_id;
						current_type = op_member_ptr->ref.op_type;

						/*
						 * if the current type is an ITEM_ID_REF convert
						 * this to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}

						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = op_member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) {
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &collection,
				(unsigned short) COLLECTION_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_op_members_list(&collection.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;

        case VIA_FILE_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) FILE_MEMBER,
				(void *) &file, (unsigned short) FILE_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(file.masks.attr_avail & FILE_MEMBER)) {

					/*
					 * eval the binary
					 */
					cur_depbin = file.depbin->db_members;

					rc = eval_attr_members(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &file.members,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {
				/*
				 * find the member name in the file's member list
				 */

				member_ptr = file.members.list;
				count = file.members.count;

				for (inc = 0; inc < count; member_ptr++, inc++) {

					if (member_ptr->name == info_ptr->val.member) {

						current_id = member_ptr->ref.id;
						current_type = member_ptr->ref.type;

						/*
						 * if the current type is an ITEM_ID_REF convert
						 * this to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}

						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) {
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &file,
				(unsigned short) FILE_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_members_list(&file.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;
/******************************** ???? ***********************************/
        case VIA_CHART_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) CHART_MEMBER,
				(void *) &chart, (unsigned short) CHART_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(chart.masks.attr_avail & CHART_MEMBER)) {

					/*
					 * eval the binary
					 */
					cur_depbin = chart.depbin->db_members;

					rc = eval_attr_members(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &chart.members,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {
				/*
				 * find the member name in the chart's member list
				 */

				member_ptr = chart.members.list;
				count = chart.members.count;

				for (inc = 0; inc < count; member_ptr++, inc++) {

					if (member_ptr->name == info_ptr->val.member) {

						current_id = member_ptr->ref.id;
						current_type = member_ptr->ref.type;

						/*
						 * if the current type is an ITEM_ID_REF convert
						 * this to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}

						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) {
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &chart,
				(unsigned short) CHART_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_members_list(&chart.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;
/******************************** ???? ***********************************/
        case VIA_GRAPH_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) GRAPH_MEMBER,
				(void *) &graph, (unsigned short) GRAPH_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(graph.masks.attr_avail & GRAPH_MEMBER)) {

					/*
					 * eval the binary
					 */
					cur_depbin = graph.depbin->db_members;

					rc = eval_attr_members(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &graph.members,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {
				/*
				 * find the member name in the graph's member list
				 */

				member_ptr = graph.members.list;
				count = graph.members.count;

				for (inc = 0; inc < count; member_ptr++, inc++) {

					if (member_ptr->name == info_ptr->val.member) {

						current_id = member_ptr->ref.id;
						current_type = member_ptr->ref.type;

						/*
						 * if the current type is an ITEM_ID_REF convert
						 * this to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}

						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) {
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &graph,
				(unsigned short) GRAPH_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_members_list(&graph.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;
/******************************** ???? ***********************************/
        case VIA_SOURCE_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(current_id, (unsigned long) SOURCE_MEMBER,
				(void *) &source, (unsigned short) SOURCE_ITYPE,
				env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(source.masks.attr_avail & SOURCE_MEMBER)) {

					/*
					 * eval the binary
					 */
					cur_depbin = source.depbin->db_members;

					rc = eval_attr_op_members(cur_depbin->bin_chunk,
						cur_depbin->bin_size, &source.members,
						&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {
				/*
				 * find the member name in the source's member list
				 */

				op_member_ptr = source.members.list;
				count = source.members.count;

				for (inc = 0; inc < count; op_member_ptr++, inc++) {

					if (op_member_ptr->name == info_ptr->val.member) {

						current_id = op_member_ptr->ref.op_id;
						current_type = op_member_ptr->ref.op_type;

						/*
						 * if the current type is an ITEM_ID_REF convert
						 * this to an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type =
									(ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {

							ref->op_id = current_id;
							ref->op_type = current_type;
							ref->op_subindex = 0;
						}

						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = op_member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) {
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free((void *) &source,
				(unsigned short) SOURCE_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_op_members_list(&source.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;
/******************************** ???? ***********************************/
		case VIA_RECORD_REF:
			/*
			 * table request change item id and member name to subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id,
				info_ptr->val.member, &subindex);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			if (options & RESOLVE_OP_REF) {	/* save previous id and type */

				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = subindex;
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL)) {

				rc = get_abt_dd_dev_tbls(env_info->block_handle,
						(void **)&flat_device_dir) ;
				if (rc != SUCCESS) {
					return(rc) ;
				}
				it = &flat_device_dir->item_tbl;

				/*
				 * table request item id and subindex to item table element
				 */

				rc = tr_id_si_to_ite(it, bt_elem, current_id, subindex, &it_elem);
				if (rc != DDS_SUCCESS) {

					return rc;
				}

				current_id = ITE_ITEM_ID(it_elem);
				current_type = ITE_ITEM_TYPE(it_elem);

				if (options & RESOLVE_TRAIL) {

					ref->trail[trail_inc].element = info_ptr->val.member;
					++trail_inc;
					ref->trail[trail_inc].id = current_id;
					ref->trail[trail_inc].type = current_type;
				}
			}
			break;

        case VIA_BIT_ENUM_REF:
			if (options & RESOLVE_OP_REF) {	/* save previous id and type */

                // Initialize BIT_MASK!!!
                ref->bit_mask = (unsigned long)info_ptr->val.index;
/***
				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = (SUBINDEX) info_ptr->val.index;
***/
			}
			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].element = info_ptr->val.index;
				++trail_inc;
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
            break;

        case VIA_LIST_REF:
		case VIA_ARRAY_REF:
			if (options & RESOLVE_OP_REF) {	/* save previous id and type */

				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = (SUBINDEX) info_ptr->val.index;
				if (info_ptr->type == VIA_LIST_REF)
				{
					DDI_BLOCK_SPECIFIER bs;
					DDI_ITEM_SPECIFIER is;
					ENV_INFO env;
					DDI_GENERIC_ITEM gi;
					FLAT_LIST *fl;

					ref->container_id = current_id;
					ref->container_type = LIST_ITYPE;
					ref->container_index = (SUBINDEX)info_ptr->val.index;
					// Move current_id and current_type to the type of this list
    
					bs.type = DDI_BLOCK_HANDLE;
					bs.block.handle = env_info->block_handle;
					is.type = DDI_ITEM_ID;
					is.item.id = current_id;
					env.block_handle = env_info->block_handle;
					env.app_info = NULL;
					memset(&gi, 0, sizeof(DDI_GENERIC_ITEM));
					rc = ddi_get_item(&bs, &is, &env, LIST_TYPE, &gi);
					if (rc)
						return rc;
					fl = (FLAT_LIST *)gi.item;
					/* Assign the item ID of the list type to our current_id */
					current_id = is.item.id = fl->type;
                    ddi_clean_item(&gi);
					/* Look up the data type of the list */
					rc = ddi_get_type(&bs, &is, &current_type);
					if (rc)
						return rc;
				}
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL)) {

				rc = get_abt_dd_dev_tbls(env_info->block_handle, 
						(void **)&flat_device_dir);
				if (rc != SUCCESS) {
					return(rc) ;
				}
				it = &flat_device_dir->item_tbl;

				/*
				 * table request item id and subindex to item table element
				 */

				rc = tr_id_si_to_ite(it, bt_elem, current_id,
										(SUBINDEX) info_ptr->val.index,
										&it_elem);
				if (rc != DDS_SUCCESS) 
                {
                    MEMBER m;
                    DDI_GENERIC_ITEM gi;
                    m.ref.id = current_id;
                    if (info_ptr->type == VIA_ARRAY_REF)
                        m.ref.type = ARRAY_ITYPE;
                    else
                        m.ref.type = LIST_ITYPE;
                    rc = rslv_get_one_local(env_info->block_handle, &m, &gi);
                    if (!rc)
                    {
                        current_id = m.ref.id;
                        current_type = m.ref.type;
                        ddi_clean_item(&gi);
                    }
                    else
    					return rc;
				}
                else
                {
				    current_id = ITE_ITEM_ID(it_elem);
				    current_type = ITE_ITEM_TYPE(it_elem);
                }

				if (options & RESOLVE_TRAIL) {

					ref->trail[trail_inc].element = info_ptr->val.index;
					++trail_inc;
                }

                if (info_ptr->type == VIA_LIST_REF)
                {
                    // Move current_id and current_type to the type of this list
                    DDI_BLOCK_SPECIFIER bs;
                    DDI_ITEM_SPECIFIER is;
                    ENV_INFO env;
                    DDI_GENERIC_ITEM gi;
                    FLAT_LIST *fl;
                    
                    bs.type = DDI_BLOCK_HANDLE;
                    bs.block.handle = env_info->block_handle;
                    is.type = DDI_ITEM_ID;
                    is.item.id = current_id;
                    env.block_handle = env_info->block_handle;
                    env.app_info = NULL;
                    memset(&gi, 0, sizeof(DDI_GENERIC_ITEM));
                    rc = ddi_get_item(&bs, &is, &env, LIST_TYPE, &gi);
                    if (rc)
                        return rc;
                    fl = (FLAT_LIST *)gi.item;
                    /* Assign the item ID of the list type to our current_id */
                    current_id = is.item.id = fl->type;
                    /* Look up the data type of the list */
                    ddi_clean_item(&gi);
                    rc = ddi_get_type(&bs, &is, &current_type);
                    if (rc)
                        return rc;
                }

				if (options & RESOLVE_TRAIL) {
					ref->trail[trail_inc].id = current_id;
					ref->trail[trail_inc].type = current_type;
				}

			}
			break;

		case VIA_VAR_LIST_REF:

			/*
			 * table request item id and member name to item id and type
			 */

			rc = tr_resolve_lid_pname_to_id_type(env_info->block_handle,
				bt_elem, current_id, info_ptr->val.member,
				&next_id, &current_type);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			current_id = next_id;

			if (options & RESOLVE_OP_REF) {

				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].element = info_ptr->val.member;
				++trail_inc;
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

        case VIA_BCHART_REF:        /* Block chart members */
        case VIA_BFILE_REF:         /* Block file members */
        case VIA_BGRAPH_REF:        /* Block graph members */
        case VIA_BGRID_REF:         /* Block grid members */
        case VIA_BMENU_REF:         /* Block menu members */
        case VIA_BMETHOD_REF:       /* Block method members */
		case VIA_PARAM_LIST_REF:
		case VIA_PARAM_REF:
        case VIA_LOCAL_PARAM_REF:
			if (options & RESOLVE_TRAIL) {
                int new_type = 0;

				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = info_ptr->val.member;

                switch(info_ptr->type)
                {
                case VIA_PARAM_LIST_REF:
					new_type = PARAM_LIST_ITYPE;
                    break;
                case VIA_PARAM_REF:
					new_type = PARAM_ITYPE;
                    break;
                case VIA_LOCAL_PARAM_REF:
					new_type = LOCAL_PARAM_ITYPE;
                    break;
                case VIA_BCHART_REF:
                    new_type = CHART_ITYPE;
                    break;
                case VIA_BFILE_REF:
                    new_type = FILE_ITYPE;
                    break;
                case VIA_BGRAPH_REF:
                    new_type = GRAPH_ITYPE;
                    break;
                case VIA_BGRID_REF:
                    new_type = GRID_ITYPE;
                    break;
                case VIA_BMENU_REF:
                    new_type = MENU_ITYPE;
                    break;
                case VIA_BMETHOD_REF:
                    new_type = METHOD_ITYPE;
                    break;
                default:
                    return DDL_ENCODING_ERROR;
                }
                ref->trail[trail_inc].type = (ITEM_TYPE)new_type;
				trail_inc++;
			}

			/*
			 * table request member name to item id and type
			 */
            if (info_ptr->type != VIA_LOCAL_PARAM_REF)
            {
    			rc = tr_resolve_name_to_id_type(env_info->block_handle,
	    			bt_elem, info_ptr->val.member, &current_id, &current_type, 0);
            }
            else
            {
                DESC_REF dr;
                rc = rslv_find_local(env_info->block_handle, info_ptr->val.member, &dr);
                if (!rc)
                {
                    current_id = dr.id;
                    current_type = dr.type;
                }
            }

			if (rc != DDS_SUCCESS) {

				return rc;
			}

			if (options & RESOLVE_OP_REF) {

				ref->op_id = current_id;
				ref->op_type = current_type;
				ref->op_subindex = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case VIA_BLOCK_REF:
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = 0;
				ref->trail[trail_inc].type = BLOCK_CHAR_ITYPE;
				trail_inc++;
			}

			/*
			 * table request characteristic record to item id
			 */

			rc = tr_resolve_char_rec_to_ddid(env_info->block_handle, bt_elem, &current_id);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			/*
			 * table request item id and member name to subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id,
				info_ptr->val.member, &subindex);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			if (options & RESOLVE_OP_REF) {	/* save previous id and type */
				ref->op_id = current_id;
				ref->op_type = RECORD_ITYPE;
				ref->op_subindex = subindex;
			}
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = RECORD_ITYPE;
				ref->trail[trail_inc].element = (unsigned long) subindex;
				trail_inc++;
			}

			/*
			 * get item table
			 */

			rc = get_abt_dd_dev_tbls(env_info->block_handle, 
					(void **)&flat_device_dir) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}
			it = &flat_device_dir->item_tbl;

			/*
			 * table request characteristic to item table element
			 */

			rc = tr_characteristics_to_ite(it, bt_elem, subindex, &it_elem);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			current_id = ITE_ITEM_ID(it_elem);
			current_type = ITE_ITEM_TYPE(it_elem);

			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
				ref->trail[trail_inc].element = 0;
			}
			break;


		default:
			CRASH_RET(DDL_ENCODING_ERROR);
			/* NOTREACHED */
			break;
		}

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (options & RESOLVE_TRAIL) {

		/* finish off the trail elements */

		ref->trail[trail_inc].element = 0;
		++trail_inc;
		ref->trail_count = trail_inc;
	}

	if (options & RESOLVE_DESC_REF) {

		ref->desc_id = current_id;
		ref->desc_type = current_type;
	}

	return rc;
}
