/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	rtn_code.c -
 *  These are the return codes for all routines.  There is a set of
 *  general purpose codes and a set for each of the subsystems.
 */

#include <stdio.h>
#include "rtn_code.h"
#include "ddi_lib.h"

#define nelem(x) (sizeof(x)/sizeof(x[0]))

struct err_struct {
	int             code;
	char           *string;
};

static const
struct err_struct errstrs[] = {

#include "rtn_def.h"

};

#define BUF_LEN     512      /** length of error string buffer **/

char           *
dds_error_string(int err)
{

    const struct	err_struct *ptr;
    const struct	err_struct *end;
    static char     buf[BUF_LEN];
    char			country_code[5];
    int				rc;

    end = errstrs + nelem(errstrs);
    ptr = errstrs;

   /*
	*	Scan through the structure of error messages to find the one with
	*	the specified error code.  If it has a message associated with it,
	*	get a copy of the English version of the message (right now, English
	*	is the only kind available) and return.  If there is no message, or
	*	the string translation fails, get out of this loop and fill the output
	*	buffer with the "Error not found" message, then return.
	*/
    for (ptr = errstrs; ptr < end; ptr++) {
        if ((err == ptr->code) && (ptr->string != NULL)) {
			(void)sprintf(country_code, "|en|");
            rc = ddi_get_string_translation(ptr->string, country_code, buf,
                (int)BUF_LEN );
            if ( rc != DDS_SUCCESS ) {
                break;

            } else {
                return (buf);
            }
		/* NOTREACHED */
		}
    }

    (void)sprintf(buf, "Error #%d: not found", err);
    return (buf);
}

int
dds_error_valid(int err)
{
    const struct	err_struct *ptr;
    const struct	err_struct *end;
    //static char     buf[BUF_LEN];

    end = errstrs + nelem(errstrs);
    ptr = errstrs;

    for (ptr = errstrs; ptr < end; ptr++) {
        if ((err == ptr->code) && (ptr->string != NULL)) {
			return 1;
		}
	}

	return 0;
}

