#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    include the source files ###############################################
#############################################################################
set (FF_DDS_SRC ${FF_DDS_SRC}	${CMAKE_CURRENT_SOURCE_DIR}/cm_blkld.c
                		${CMAKE_CURRENT_SOURCE_DIR}/cm_comm.c
				${CMAKE_CURRENT_SOURCE_DIR}/cm_dds.c
				${CMAKE_CURRENT_SOURCE_DIR}/cm_gstor.c
				${CMAKE_CURRENT_SOURCE_DIR}/cm_init.c
				${CMAKE_CURRENT_SOURCE_DIR}/cm_rod.c
				${CMAKE_CURRENT_SOURCE_DIR}/cm_tbl.c
				${CMAKE_CURRENT_SOURCE_DIR}/cmtblacc.c
				PARENT_SCOPE)
  
