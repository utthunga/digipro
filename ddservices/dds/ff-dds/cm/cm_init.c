/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_init.c - Connection Manager Initialization Module
 *
 *	This file contains all functions pertaining to the
 *	initialization of the Connection Manager.  It also
 *	contains any functions that are common to all of the
 *	Connection Manager modules.
 *
 */

#include "cm_lib.h"
#include "cm_loc.h"


/***********************************************************************
 *
 *	Name:  cm_init
 *	ShortDesc:  Initialize the Connection Manager.
 *
 *	Description:
 *		The cm_init function initializes the Connection Manager by
 *		initializing each of its sub-systems.
 *
 *	Inputs:
 *		path - path to the DD files.
 *		mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_init function.
 *		Return values from rod_init function.
 *		Return values from ds_init function.
 *		Return values from cr_init function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
cm_init(ENV_INFO *env_info, const char *path, 
        HOST_CONFIG *host_config)
{
	int		r_code;

	/*
	 *	Initialize each of the Connection Manager's sub-systems.
	 */

    /* 
     * ct_init() and rod_init() are called during device connection
     * in online mode and as the device connection is simulated in 
     * simulator mode, these functions are called here to initialize
     * the tables.
     */

	/* DD Support */

	r_code = ds_init(path);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/* Communication Support */
    
	r_code = cr_init(env_info, host_config);
	if (r_code != CM_SUCCESS) {
	    return(r_code);
	}
    
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  cm_init_dds_tbl_fn_ptrs
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The cm_init_dds_tbl_fn_ptrs function links the pointers of the
 *		DDS functions that the Connection Manager calls when any DDS
 *		tables need to be built or removed.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
cm_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that build the DDS tables.
	 */

	ds_init_dds_tbl_fn_ptrs(dds_tbl_fn_ptrs);

	/*
	 *	Link the pointers of the functions that remove the DDS tables.
	 */

	ct_init_dds_tbl_fn_ptrs(dds_tbl_fn_ptrs);
}


/***********************************************************************
 *
 *	Name:  cm_init_app_info_fn_ptrs
 *	ShortDesc:  Initialize the Application information function
 *				pointers.
 *
 *	Description:
 *		The cm_init_app_info_fn_ptrs function links the pointers of the
 *		Application functions that the Connection Manager calls when
 *		any Application information needs to be built or removed.
 *
 *		NOTE:  For general purpose use, the functions that build the
 *		Application information are called when all other information
 *		of an Active Table has been established.
 *
 *		This will be as follows:
 *
 *		Active Block Table - After loading a DD for a block (in the
 *		ds_dd_block_load function in cm_dds.c).
 *
 *		Active Device Table - After loading a device's OPROD (in the
 *		bl_block_load function in cm_blkld.c).
 *
 *		Active Device Type Table - After loading a DD for a device (in
 *		the ds_dd_device_load function in cm_dds.c).
 *
 *
 *	Inputs:
 *		app_info_fn_ptrs - the necessary Application information
 *						   function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
cm_init_app_info_fn_ptrs(APP_INFO_FN_PTRS *app_info_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that build the Application
	 *	information.
	 */

	ds_init_app_info_fn_ptrs(app_info_fn_ptrs);
	bl_init_app_info_fn_ptrs(app_info_fn_ptrs);

	/*
	 *	Link the pointers of the functions that remove the Application
	 *	information.
	 */

	ct_init_app_info_fn_ptrs(app_info_fn_ptrs);
}

/***********************************************************************
 *
 *	Name:  cm_configure_stack
 *	ShortDesc:  configures the FF stack
 *
 *	Description:
 *		The cm_init_app_info_fn_ptrs function links the pointers of the
 *		Application functions that the Connection Manager calls when
 *		any Application information needs to be built or removed.
 *
 *		NOTE:  For general purpose use, the functions that build the
 *		Application information are called when all other information
 *		of an Active Table has been established.
 *
 *		This will be as follows:
 *
 *		Active Block Table - After loading a DD for a block (in the
 *		ds_dd_block_load function in cm_dds.c).
 *
 *		Active Device Table - After loading a device's OPROD (in the
 *		bl_block_load function in cm_blkld.c).
 *
 *		Active Device Type Table - After loading a DD for a device (in
 *		the ds_dd_device_load function in cm_dds.c).
 *
 *
 *	Inputs:
 *		app_info_fn_ptrs - the necessary Application information
 *						   function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 **********************************************************************/
int
cm_config_stack(ENV_INFO *env_info, CM_NMA_INFO *nma_info, 
        HOST_CONFIG *host_config)
{
	int		r_code;

	/* Communication Support */
    
	r_code = cr_config_stack(env_info, nma_info, host_config);
	if (r_code != CM_SUCCESS) {
	    return(r_code);
	}
    
	return(CM_SUCCESS);
}

int cm_get_stack_config(ENV_INFO *env_info, CM_NMA_INFO *nma_info)
{
    int r_code;

    r_code = cr_get_stack_config(env_info, nma_info);
    if(r_code != CM_SUCCESS)
    {
        return r_code;
    }

    return CM_SUCCESS;
}

