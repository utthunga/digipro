/**
 *      Device Description Services Rel. 4.3
 *      Copyright 1994-2002 - Fieldbus Foundation
 *      All rights reserved.
 */

/*
 *  cmtblacc.c - Connection Manager Tables Module
 *
 *  This file contains all access functions pertaining to the Connection
 *  (Active) Tables.
 */

#include <string.h>
#include <pthread.h>

#include "cm_lib.h"
#include "cm_loc.h"
#include "malloc.h"



NETWORK_HANDLE  network_handle;

/* 
 * Functions for accessing Active Block Information 
 */


void
clr_abt_tbls (void)
{
    for (int cnt = 0; cnt < active_blk_tbl.count; cnt++)
    {
        free(active_blk_tbl.list[cnt]);
    }
    free(active_blk_tbl.list);
    active_blk_tbl.count = 0;
}

int
get_abt_count ()
{
    return(active_blk_tbl.count) ;
}


int
get_abt_tag(BLOCK_HANDLE bh, char *tag)
{

    if(!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(tag, CM_BAD_POINTER) ;
    (void)strcpy(tag, active_blk_tbl.list[bh]->tag) ;
    return(CM_SUCCESS) ;
}


int 
set_abt_tag(BLOCK_HANDLE bh, char *tag)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }

    if( strlen( active_blk_tbl.list[bh]->tag) < strlen(tag) ) {
        active_blk_tbl.list[bh]->tag = realloc(active_blk_tbl.list[bh]->tag, strlen(tag) +1);
        if ( active_blk_tbl.list[bh]->tag == NULL ) return FAILURE;
    }

    (void)strcpy(active_blk_tbl.list[bh]->tag, tag) ;
    return(CM_SUCCESS) ;
}


int 
get_abt_op_index(BLOCK_HANDLE bh, OBJECT_INDEX *oi)
{

    DEVICE_HANDLE   device_handle ;
    NET_TYPE        net_type ;
    BOOLEAN_T         bool;

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }

    device_handle = ABT_ADT_OFFSET(bh) ;

    ASSERT_RET(VALID_DEVICE_HANDLE(device_handle), CM_BAD_DEVICE_HANDLE) ;

    net_type = ADT_NET_TYPE(device_handle) ;
    bool = ((net_type == NT_OFF_LINE) 
        || (net_type == NT_A) || (net_type == NT_A_SIM)
        );

    ASSERT_RET(bool, CM_NO_COMM) ;
    ASSERT_RET(oi, CM_BAD_POINTER) ;

    *oi = active_blk_tbl.list[bh]->abt_net_spec.abt_sp_a.op_index ;
    return(CM_SUCCESS) ;
}


int 
set_abt_op_index(BLOCK_HANDLE bh, OBJECT_INDEX oi)
{
    NET_TYPE        net_type ;
    BOOLEAN_T         bool;

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }

    net_type = NT_NET_TYPE(0) ;

    bool = ((net_type == NT_OFF_LINE) 
        || (net_type == NT_A) || (net_type == NT_A_SIM)
        );

    ASSERT_RET(bool, CM_NO_COMM) ;

    active_blk_tbl.list[bh]->abt_net_spec.abt_sp_a.op_index = oi ;
    return(CM_SUCCESS) ;
}


int 
get_abt_param_count(BLOCK_HANDLE bh, unsigned int *pc)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(pc, CM_BAD_POINTER) ;
    *pc = active_blk_tbl.list[bh]->param_count ;
    return(CM_SUCCESS) ;
}


int 
set_abt_param_count(BLOCK_HANDLE bh, unsigned int pc)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->param_count = pc ;
    return(CM_SUCCESS) ;
}


int 
get_abt_dd_blk_id(BLOCK_HANDLE bh, ITEM_ID *dbi)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(dbi, CM_BAD_POINTER) ;
    *dbi = active_blk_tbl.list[bh]->dd_blk_id ;
    return(CM_SUCCESS) ;
}


int 
set_abt_dd_blk_id (BLOCK_HANDLE bh, UINT32 dbi)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->dd_blk_id = dbi ;
    return(CM_SUCCESS) ;
}


int 
get_abt_dd_blk_tbl_offset(BLOCK_HANDLE bh, int *dbto)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(dbto, CM_BAD_POINTER) ;
    *dbto = active_blk_tbl.list[bh]->dd_blk_tbl_offset ;
    return(CM_SUCCESS) ;
}


int 
set_abt_dd_blk_tbl_offset(BLOCK_HANDLE bh, int dbto)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto ;
    return(CM_SUCCESS) ;
}



int
get_abt_app_info(BLOCK_HANDLE bh, void **ai)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(ai, CM_BAD_POINTER) ;
    *ai = active_blk_tbl.list[bh]->app_info ;
    return(CM_SUCCESS) ;
}


int 
set_abt_app_info(BLOCK_HANDLE bh, void *ai)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->app_info = ai ;
    return(CM_SUCCESS) ;
}

// CPMHACK - Added get / set method for pc information
int
get_abt_pc_info(BLOCK_HANDLE bh, void **pci)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(pci, CM_BAD_POINTER) ;
    *pci = active_blk_tbl.list[bh]->pc_info ;
    return(CM_SUCCESS) ;
}


int 
set_abt_pc_info(BLOCK_HANDLE bh, void *pci)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->pc_info = pci ;
    return(CM_SUCCESS) ;
}

int 
get_abt_adt_offset(BLOCK_HANDLE bh, DEVICE_HANDLE *dh)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(dh, CM_BAD_POINTER) ;
    *dh = active_blk_tbl.list[bh]->active_dev_tbl_offset ;
    return(CM_SUCCESS) ;
}


int 
set_abt_adt_offset(BLOCK_HANDLE bh, DEVICE_HANDLE adto)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->active_dev_tbl_offset = adto ;
    return(CM_SUCCESS) ;
}


int
get_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE *dth)
{

    DEVICE_HANDLE   device_handle ;

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(dth, CM_BAD_POINTER) ;
    device_handle = ABT_ADT_OFFSET(bh) ;
    if (!VALID_DEVICE_HANDLE(device_handle)) { 
            return(CM_BAD_DEVICE_HANDLE) ;
    }
    *dth = ADT_ADTT_OFFSET(device_handle) ;
    return(CM_SUCCESS) ;
}


int
set_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto)
{

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    SET_ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh),adtto) ;
    return(CM_SUCCESS) ;
}


int
get_abt_dd_device_id(BLOCK_HANDLE bh, DD_DEVICE_ID *ddi) 
{

    DEVICE_HANDLE   device_handle ;

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(ddi, CM_BAD_POINTER) ;
    device_handle = ABT_ADT_OFFSET(bh) ;
    if (!VALID_DEVICE_HANDLE(device_handle)) { 
            return(CM_BAD_DEVICE_HANDLE) ;
    }
    (void)memcpy(ddi, ADT_DD_DEVICE_ID(device_handle), sizeof(DD_DEVICE_ID)) ;
    return(CM_SUCCESS) ;
}


int
set_abt_dd_device_id(BLOCK_HANDLE bh, DD_DEVICE_ID *ddi) 
{

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    SET_ADT_DD_DEVICE_ID(ABT_ADT_OFFSET(bh), ddi) ;
    return(CM_SUCCESS) ;
}


int
get_abt_dd_handle(BLOCK_HANDLE bh, DD_HANDLE *ddh)
{

    DEVICE_HANDLE   device_handle ;

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(ddh, CM_BAD_POINTER) ;
    device_handle = ABT_ADT_OFFSET(bh) ;
    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    (void)memcpy((char *)ddh, (char *)ADT_DD_HANDLE(device_handle), 
            sizeof(DD_HANDLE)) ;
    return(CM_SUCCESS) ;
}


int
set_abt_dd_handle(BLOCK_HANDLE bh, DD_HANDLE *ddh) 
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    SET_ADT_DD_HANDLE(ABT_ADT_OFFSET(bh),ddh) ;
    return(CM_SUCCESS) ;
}


int
get_abt_dd_dev_tbls(BLOCK_HANDLE bh, void **ddt)
{

    DEVICE_HANDLE   device_handle ;
    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    device_handle = ABT_ADT_OFFSET(bh) ;
    if (!VALID_DEVICE_HANDLE(device_handle)) { 
            return(CM_BAD_DEVICE_HANDLE) ;
    }
    *ddt = ADT_DD_DEV_TBLS(device_handle) ; 
    return(CM_SUCCESS) ;
}


int
set_abt_dd_dev_tbls(BLOCK_HANDLE bh, void *ddt) 
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }           
    SET_ADT_DD_DEV_TBLS(ABT_ADT_OFFSET(bh),ddt) ;
    return(CM_SUCCESS) ;
}


int
get_abt_dev_family(BLOCK_HANDLE bh, int *df)
{

    DEVICE_HANDLE   device_handle ;

    if (!VALID_BLOCK_HANDLE(bh)) { 
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(df, CM_BAD_POINTER) ;
    device_handle = ABT_ADT_OFFSET(bh) ;
    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    *df = ADT_DEV_FAMILY(device_handle) ;
    return (CM_SUCCESS) ;
}


int
set_abt_dev_family(BLOCK_HANDLE bh, int df)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    SET_ADT_DEV_FAMILY(ABT_ADT_OFFSET(bh),df) ;
    return(CM_SUCCESS) ;
}


void
clr_adt_tbls (void)
{
    for (int cnt = 0; cnt < active_dev_tbl.count; cnt++)
    {
        free(active_dev_tbl.list[cnt]);
    }
    free(active_dev_tbl.list);
    active_dev_tbl.count = 0;
}


int
get_adt_count()
{
    return(active_dev_tbl.count) ;
}


int
get_adt_station_address(DEVICE_HANDLE dh, UINT16 *sa)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(sa, CM_BAD_POINTER) ;
    *sa = active_dev_tbl.list[dh]->station_address ;
    return(CM_SUCCESS) ;
}


int
set_adt_station_address(DEVICE_HANDLE dh, UINT16 sa)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->station_address = sa ;
    return(CM_SUCCESS) ;
}


int
get_adt_a_opod_cref(DEVICE_HANDLE dh, CREF *oo)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;
    ASSERT_RET(oo, CM_BAD_POINTER) ;

    *oo = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref ;
    return(CM_SUCCESS) ;
}


int
set_adt_a_opod_cref(DEVICE_HANDLE dh, CREF oc)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;

    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref = oc ;
    return(CM_SUCCESS) ;
}


int
get_adt_a_ddod_cref(DEVICE_HANDLE dh, CREF *ddo)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;
    ASSERT_RET(ddo, CM_BAD_POINTER) ;
    *ddo = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref ;
    return(CM_SUCCESS) ;
}


int
set_adt_a_ddod_cref(DEVICE_HANDLE dh, CREF dc)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;

    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref = dc ;
    return(CM_SUCCESS) ;
}


int
get_adt_a_mib_cref(DEVICE_HANDLE dh, CREF *mi)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;
    ASSERT_RET(mi, CM_BAD_POINTER) ;

    *mi = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref ;
    return(CM_SUCCESS) ;
}


int
set_adt_a_mib_cref(DEVICE_HANDLE dh, CREF mc)
{

    NET_TYPE        net_type ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    net_type = ADT_NET_TYPE(dh) ;

    ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
            (net_type == NT_OFF_LINE), CM_NO_COMM) ;

    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref = mc ;
    return(CM_SUCCESS) ;
}


int
get_adt_dd_device_id(DEVICE_HANDLE dh, DD_DEVICE_ID *ddi)
{

    DEVICE_TYPE_HANDLE  device_type_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(ddi, CM_BAD_POINTER) ;
    device_type_handle = ADT_ADTT_OFFSET(dh) ;
    if (!VALID_DEVICE_TYPE_HANDLE(device_type_handle)) { 
            return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    (void)memcpy((char *)ddi,(char *)ADTT_DD_DEVICE_ID(device_type_handle),
            sizeof(DD_DEVICE_ID)) ;
    return(CM_SUCCESS) ;
}

int
set_adt_dd_device_id(DEVICE_HANDLE dh, DD_DEVICE_ID *ddi)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh),ddi) ;
    return(CM_SUCCESS) ;
}        

                    
int
get_adt_dd_handle(DEVICE_HANDLE dh, DD_HANDLE *ddh)
{

    DEVICE_TYPE_HANDLE  device_type_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(ddh, CM_BAD_POINTER) ;
    device_type_handle = ADT_ADTT_OFFSET(dh) ;
    if (!VALID_DEVICE_TYPE_HANDLE(device_type_handle)) { 
            return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    (void)memcpy((char *)ddh, (char *)ADTT_DD_HANDLE(device_type_handle),
        sizeof(DD_HANDLE)) ;
    return(CM_SUCCESS) ;
}


int
set_adt_dd_handle(DEVICE_HANDLE dh, DD_HANDLE *ddh)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh),ddh) ;
    return(CM_SUCCESS) ;
}


int
get_adt_dd_dev_tbls(DEVICE_HANDLE dh, void **ddt)
{

    DEVICE_TYPE_HANDLE  device_type_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(ddt, CM_BAD_POINTER) ;
    device_type_handle = ADT_ADTT_OFFSET(dh) ;
    *ddt = ADTT_DD_DEV_TBLS(device_type_handle) ;
    return (CM_SUCCESS) ;
}


int
set_adt_dd_dev_tbls(DEVICE_HANDLE dh, void *ddt)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh), ddt) ;
    return(CM_SUCCESS) ;
}


int
get_adt_net_type(DEVICE_HANDLE dh, NET_TYPE *nt)
{

    NETWORK_HANDLE network_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) { 
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(nt, CM_BAD_POINTER) ;
    network_handle = ADT_NETWORK(dh) ;
    if (!VALID_NETWORK_HANDLE(network_handle)) { 
            return(CM_BAD_NETWORK_HANDLE) ;
    }
    *nt = NT_NET_TYPE(network_handle) ;
    return (CM_SUCCESS) ;
}


int
set_adt_net_type(DEVICE_HANDLE dh, NET_TYPE nt)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_NT_NET_TYPE(ADT_NETWORK(dh),nt) ;
    return(CM_SUCCESS) ;
}


int
get_adt_fbap_cr(DEVICE_HANDLE dh, UINT8 *cr)
{

    NETWORK_HANDLE network_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) { 
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(cr, CM_BAD_POINTER) ;
    network_handle = ADT_NETWORK(dh) ;
    if (!VALID_NETWORK_HANDLE(network_handle)) { 
            return(CM_BAD_NETWORK_HANDLE) ;
    }
    *cr = NT_FBAP_CR(network_handle) ;
    return (CM_SUCCESS) ;
}


int
get_adt_net_route(DEVICE_HANDLE dh, NET_ROUTE *nr)
{

    NETWORK_HANDLE  network_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(nr, CM_BAD_POINTER) ;
    network_handle =  ADT_NETWORK(dh) ;
    if (!VALID_NETWORK_HANDLE(network_handle)) { 
            return(CM_BAD_NETWORK_HANDLE) ;
    }
    (void)memcpy((char *)nr, (char *)NT_NET_ROUTE(network_handle),
        sizeof(NET_ROUTE)) ;
    return (CM_SUCCESS) ;
}


int
set_adt_net_route(DEVICE_HANDLE dh, NET_ROUTE *nr)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_NT_NET_ROUTE(ADT_NETWORK(dh), nr) ;
    return(CM_SUCCESS) ;
}


int
get_adt_dev_family(DEVICE_HANDLE dh, int *df)
{

    NETWORK_HANDLE  network_handle ;

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(df, CM_BAD_POINTER) ;
    network_handle = ADT_NETWORK(dh) ;
    if (!VALID_NETWORK_HANDLE(network_handle)) { 
            return(CM_BAD_NETWORK_HANDLE) ;
    }
    *df = NT_DEV_FAMILY(network_handle) ;
    return (CM_SUCCESS) ;
}


int
set_adt_dev_family(DEVICE_HANDLE dh, int df)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    SET_NT_DEV_FAMILY(ADT_NETWORK(dh), df) ;
    return(CM_SUCCESS) ;
}


int 
get_abt_usage(BLOCK_HANDLE bh, int *usage)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(usage, CM_BAD_POINTER) ;
    *usage = active_blk_tbl.list[bh]->usage ;
    return(CM_SUCCESS) ;
}


int 
set_abt_usage(BLOCK_HANDLE bh, int usage)
{

    if (!VALID_BLOCK_HANDLE(bh)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    active_blk_tbl.list[bh]->usage = usage ;
    return(CM_SUCCESS) ;
}


int 
get_adt_network(DEVICE_HANDLE dh, NETWORK_HANDLE *nh)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(nh, CM_BAD_POINTER) ;
    *nh = active_dev_tbl.list[dh]->network ;
    return(CM_SUCCESS) ;
}


int 
set_adt_network(DEVICE_HANDLE dh, NETWORK_HANDLE nh)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->network = nh ;
    return(CM_SUCCESS) ;
}


int 
get_adt_oprod_handle(DEVICE_HANDLE dh, ROD_HANDLE *rh)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(rh, CM_BAD_POINTER) ;
    *rh = active_dev_tbl.list[dh]->oprod_handle ;
    return (CM_SUCCESS) ;
}


int 
set_adt_oprod_handle(DEVICE_HANDLE dh, ROD_HANDLE oh)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->oprod_handle = oh ;
    return(CM_SUCCESS) ;
}


int
get_adt_app_info(DEVICE_HANDLE dh, void **ai)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(ai, CM_BAD_POINTER) ;
    *ai = active_dev_tbl.list[dh]->app_info ;
    return (CM_SUCCESS) ;
}


int 
set_adt_app_info(DEVICE_HANDLE dh, void *ai)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->app_info = ai ;
    return(CM_SUCCESS) ;
}


int
get_adt_adtt_offset(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE *dth)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(dth, CM_BAD_POINTER) ;
    *dth = active_dev_tbl.list[dh]->active_dev_type_tbl_offset ;
    return (CM_SUCCESS) ;
}


int 
set_adt_adtt_offset(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto ;
    return(CM_SUCCESS) ;
}


int 
get_adt_usage(DEVICE_HANDLE dh, int *usage)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    ASSERT_RET(usage, CM_BAD_POINTER) ;
    *usage = active_dev_tbl.list[dh]->usage ;
    return (CM_SUCCESS) ;
}


int 
set_adt_usage(DEVICE_HANDLE dh, int usage)
{

    if (!VALID_DEVICE_HANDLE(dh)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }
    active_dev_tbl.list[dh]->usage = usage ;
    return(CM_SUCCESS) ;
}

pthread_t * 
get_adt_thread_handle( void )
{
    if (!VALID_THREAD_HANDLE(active_thread_data.thread_id)) {
        return NULL;
    }
    return (&active_thread_data.thread_id) ;
}

int 
set_adt_thread_handle(pthread_t th)
{
    active_thread_data.thread_id = th;
    return(CM_SUCCESS) ;
}

pthread_mutex_t * 
get_adt_thread_mutex( void )
{
    if (!VALID_THREAD_HANDLE(active_thread_data.m_mutex)) {
        return NULL;
    }
    return(&active_thread_data.m_mutex) ;
}

int 
set_adt_thread_mutex(pthread_t th)
{
    active_thread_data.thread_id = th;
    return(CM_SUCCESS) ;
}


ACTIVE_THREAD_DATA *
get_adt_thread_data( void )
{
   return(&active_thread_data); 

}



void
clr_adtt_tbls (void)
{
    for (int cnt = 0; cnt < active_dev_type_tbl.count; cnt++)
    {
        free(active_dev_type_tbl.list[cnt]);
    }
    free(active_dev_type_tbl.list);
    active_dev_type_tbl.count = 0;
}


int
get_adtt_count ()
{
    return(active_dev_type_tbl.count) ;
}


int
get_adtt_dd_device_id(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) {
        return(CM_BAD_BLOCK_HANDLE) ;
    }
    ASSERT_RET(ddi, CM_BAD_POINTER) ;
    (void)memcpy((char *)ddi, 
            (char *)&active_dev_type_tbl.list[dth]->dd_device_id, 
            sizeof(DD_DEVICE_ID)) ;
    return(CM_SUCCESS) ;
}


int 
set_adtt_dd_device_id(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    (void)memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_device_id), 
            (char *)ddi,sizeof(DD_DEVICE_ID)) ;
    return(CM_SUCCESS) ;
}


int
get_adtt_dd_handle(DEVICE_TYPE_HANDLE dth, DD_HANDLE *ddh)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    ASSERT_RET(ddh, CM_BAD_POINTER) ;
    (void)memcpy((char *)ddh,
            (char *)&active_dev_type_tbl.list[dth]->dd_handle, 
            sizeof(DD_HANDLE)) ;
    return(CM_SUCCESS) ;
}


int 
set_adtt_dd_handle(DEVICE_TYPE_HANDLE dth, DD_HANDLE *ddh)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    (void)memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_handle), 
            (char *)ddh,sizeof(DD_HANDLE)) ;
    return(CM_SUCCESS) ;
}


int
get_adtt_dd_dev_tbls(DEVICE_TYPE_HANDLE dth, void **ddt)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    ASSERT_RET(ddt, CM_BAD_POINTER) ;
    *ddt = active_dev_type_tbl.list[dth]->dd_dev_tbls ;
    return(CM_SUCCESS) ;
}


int 
set_adtt_dd_dev_tbls(DEVICE_TYPE_HANDLE dth, void *ddt)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    active_dev_type_tbl.list[dth]->dd_dev_tbls = ddt ;
    return(CM_SUCCESS) ;
}


int
get_adtt_app_info(DEVICE_TYPE_HANDLE dth, void **ai)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    ASSERT_RET(ai, CM_BAD_POINTER) ;
    *ai =  active_dev_type_tbl.list[dth]->app_info ;
    return (CM_SUCCESS) ;
}


int 
set_adtt_app_info(DEVICE_TYPE_HANDLE dth, void *ai)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    active_dev_type_tbl.list[dth]->app_info = ai ;
    return(CM_SUCCESS) ;
}


int 
get_adtt_usage(DEVICE_TYPE_HANDLE dth, int *usage)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    ASSERT_RET(usage, CM_BAD_POINTER) ;
    *usage = active_dev_type_tbl.list[dth]->usage ;
    return(CM_SUCCESS) ;
}


int 
set_adtt_usage(DEVICE_TYPE_HANDLE dth, int usage)
{

    if (!VALID_DEVICE_TYPE_HANDLE(dth)) { 
        return(CM_BAD_DEVICE_TYPE_HANDLE) ;
    }
    active_dev_type_tbl.list[dth]->usage = usage ;
    return(CM_SUCCESS) ;
}


int 
get_nt_net_count(NT_COUNT *nt_count)
{
    ASSERT_RET(nt_count, CM_BAD_POINTER) ;
    *nt_count = network_tbl.count;
    return (CM_SUCCESS) ;
}


int 
get_nt_net_handle_by_cr(NT_COUNT nt_count, UINT16 cr, NETWORK_HANDLE *nh)
{
    int count;
    if (!VALID_NETWORK_COUNT(nt_count)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    if (!VALID_NETWORK_CR(cr)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(nh, CM_BAD_POINTER) ;

    for(count = 0; count < nt_count; count++)
    {
        if((network_tbl.list[count].fbap_addr.cr == (UINT8)cr)
                 || (network_tbl.list[count].mib_addr.cr == (UINT8)cr))
        {
            *nh = count;
            break;
        }
    }
    
    return (CM_SUCCESS) ;
}


int 
get_nt_net_handle_by_addr(NT_COUNT nt_count, UINT16 dev_addr, NETWORK_HANDLE *nh)
{
    int count;

    if (!VALID_NETWORK_COUNT(nt_count)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    if (!VALID_NETWORK_ADDR(dev_addr)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(nh, CM_BAD_POINTER) ;

    for(count = 0; count < nt_count; count++)
    {
        if(network_tbl.list[count].dev_table.station_address == dev_addr)
        {
            *nh = count;
            break;
        }
    }
    
    return (CM_SUCCESS) ;
}

int
get_nt_addr_by_net_handle(NETWORK_HANDLE nh, UINT16 *dev_addr)
{
    ASSERT_RET(nh, CM_BAD_POINTER) ;
    *dev_addr = (network_tbl.list[nh].dev_table.station_address);
    return (CM_SUCCESS) ;
    
}

int
get_nt_net_type(NETWORK_HANDLE nh, NET_TYPE *nt)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(nt, CM_BAD_POINTER) ;
    *nt = network_tbl.network_type ;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_type(NETWORK_HANDLE nh, NET_TYPE nt)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.network_type = nt ;
    return(CM_SUCCESS) ;
}


int
get_nt_net_mib_cr(NETWORK_HANDLE nh, UINT16 *cr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(cr, CM_BAD_POINTER) ;
    *cr = network_tbl.list[nh].mib_addr.cr ;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_mib_cr(NETWORK_HANDLE nh, UINT16 cr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].mib_addr.cr = cr ;
    return(CM_SUCCESS) ;
}

int
get_nt_net_fbap_cr(NETWORK_HANDLE nh, UINT16 *cr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(cr, CM_BAD_POINTER) ;
    *cr = network_tbl.list[nh].fbap_addr.cr ;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_fbap_cr(NETWORK_HANDLE nh, UINT16 cr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].fbap_addr.cr = cr ;
    return(CM_SUCCESS) ;
}


int
get_nt_net_fbap_addr(NETWORK_HANDLE nh, UINT16 *addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(addr, CM_BAD_POINTER) ;
    *addr = network_tbl.list[nh].fbap_addr.remote;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_fbap_addr(NETWORK_HANDLE nh, UINT16 addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].fbap_addr.remote = addr ;
    return(CM_SUCCESS) ;
}


int
get_nt_net_mib_addr(NETWORK_HANDLE nh, UINT16 *addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(addr, CM_BAD_POINTER) ;
    *addr = network_tbl.list[nh].mib_addr.remote;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_mib_addr(NETWORK_HANDLE nh, UINT16 addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].mib_addr.remote = addr ;
    return(CM_SUCCESS) ;
}


int
get_nt_net_fbap_local_addr(NETWORK_HANDLE nh, UINT16 *addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(addr, CM_BAD_POINTER) ;
    *addr = network_tbl.list[nh].fbap_addr.local;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_fbap_local_addr(NETWORK_HANDLE nh, UINT16 addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].fbap_addr.local = addr ;
    return(CM_SUCCESS) ;
}


int
get_nt_net_mib_local_addr(NETWORK_HANDLE nh, UINT16 *addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(addr, CM_BAD_POINTER) ;
    *addr = network_tbl.list[nh].mib_addr.local;
    return (CM_SUCCESS) ;
}


int 
set_nt_net_mib_local_addr(NETWORK_HANDLE nh, UINT16 addr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].mib_addr.local = addr ;
    return(CM_SUCCESS) ;
}


int 
set_nt_dev_tbl (NETWORK_HANDLE nh, DEV_TBL *dev_table)
{
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    (void)memcpy((char *)&(network_tbl.list[nh].dev_table), (char *)dev_table,  
            sizeof(DEV_TBL)) ;
    
    return(CM_SUCCESS) ;
}

int 
get_nt_dev_tbl (NETWORK_HANDLE nh, DEV_TBL *dev_table)
{
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    (void)memcpy((char *)dev_table, (char *)&(network_tbl.list[nh].dev_table),  
            sizeof(DEV_TBL)) ;
    
    return(CM_SUCCESS) ;
}

int 
get_nt_device_addr (NETWORK_HANDLE nh, UINT8 *station_address)
{
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    *station_address = network_tbl.list[nh].dev_table.station_address ;
    return(CM_SUCCESS) ;
}

int
set_nt_dd_device_id(NETWORK_HANDLE nh, DD_DEVICE_ID *ddi) 
{

    if (!VALID_NETWORK_HANDLE(nh)) { 
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    SET_ADT_DD_DEVICE_ID(ABT_ADT_OFFSET(nh), ddi) ;
    return(CM_SUCCESS) ;
}

int
get_nt_net_route(NETWORK_HANDLE nh, NET_ROUTE *nr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(nr, CM_BAD_POINTER) ;
    (void)memcpy((char *)nr, (char *)&(network_tbl.list[nh].network_route), 
            sizeof(NET_ROUTE)) ;
    return(CM_SUCCESS) ;
}


int 
set_nt_net_route(NETWORK_HANDLE nh, NET_ROUTE *nr)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    (void)memcpy((char *)&(network_tbl.list[nh].network_route),
            (char *)nr, sizeof(NET_ROUTE)) ;
    return(CM_SUCCESS) ;
}


int 
get_nt_dev_family(NETWORK_HANDLE nh, int *df)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(df, CM_BAD_POINTER) ;
    *df = network_tbl.list[nh].device_family ;
    return(CM_SUCCESS) ;
}


int 
set_nt_dev_family(NETWORK_HANDLE nh, int df)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].device_family = df ;
    return(CM_SUCCESS) ;
}


int 
get_nt_cfg_file_id(NETWORK_HANDLE nh, int *cfi)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    ASSERT_RET(cfi, CM_BAD_POINTER) ;
    *cfi = network_tbl.list[nh].config_file_id;
    return(CM_SUCCESS) ;
}


int 
set_nt_cfg_file_id(NETWORK_HANDLE nh, int cfi)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }
    network_tbl.list[nh].config_file_id = cfi ;
    return(CM_SUCCESS) ;
}

int
get_nt_net_fbap_od(NETWORK_HANDLE nh, OD_DESCRIPTION_SPECIFIC *od_desc)
{
    
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(od_desc, CM_BAD_POINTER) ;

    (void)memcpy((char *)od_desc, 
        (char *)&(network_tbl.list[nh].fbap_addr.od_desc),
        sizeof(OD_DESCRIPTION_SPECIFIC)) ;

    return(CM_SUCCESS) ;
}


int
set_nt_net_fbap_od(NETWORK_HANDLE nh, OD_DESCRIPTION_SPECIFIC *od_desc)
{
    
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(od_desc, CM_BAD_POINTER) ;

    (void)memcpy((char *)&(network_tbl.list[nh].fbap_addr.od_desc),
            (char *)od_desc, sizeof(OD_DESCRIPTION_SPECIFIC)) ;

    return(CM_SUCCESS) ;
}

int
get_nt_net_mib_od(NETWORK_HANDLE nh, OD_DESCRIPTION_SPECIFIC *od_desc)
{
    
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(od_desc, CM_BAD_POINTER) ;

    (void)memcpy((char *)od_desc,
        (char *)&(network_tbl.list[nh].mib_addr.od_desc),
        sizeof(OD_DESCRIPTION_SPECIFIC)) ;

    return(CM_SUCCESS) ;
}

int
set_nt_net_mib_od(NETWORK_HANDLE nh, OD_DESCRIPTION_SPECIFIC *od_desc)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(od_desc, CM_BAD_POINTER) ;

    (void)memcpy((char *)&(network_tbl.list[nh].mib_addr.od_desc),
            (char *)od_desc, sizeof(OD_DESCRIPTION_SPECIFIC)) ;

    return(CM_SUCCESS) ;
}

int
get_nt_net_sm_dir(NETWORK_HANDLE nh, T_SM_DIR *sm_dir)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(sm_dir, CM_BAD_POINTER) ;

    (void)memcpy((char *)sm_dir,
        (char *)&(network_tbl.list[nh].mib_addr.sm_dir),
        sizeof(T_SM_DIR)) ;

    return(CM_SUCCESS) ;
}

int     
set_nt_net_sm_dir(NETWORK_HANDLE nh, T_SM_DIR *sm_dir)
{       
        
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }   
        
    ASSERT_RET(sm_dir, CM_BAD_POINTER) ;

    (void)memcpy((char *)&(network_tbl.list[nh].mib_addr.sm_dir),
            (char *)sm_dir, sizeof(T_SM_DIR)) ;

    return(CM_SUCCESS) ;
}

int
get_nt_net_nm_dir(NETWORK_HANDLE nh, T_NM_DIR *nm_dir)
{

    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }

    ASSERT_RET(nm_dir, CM_BAD_POINTER) ;

    (void)memcpy((char *)nm_dir,
        (char *)&(network_tbl.list[nh].mib_addr.nm_dir),
        sizeof(T_NM_DIR)) ;

    return(CM_SUCCESS) ;
}

int     
set_nt_net_nm_dir(NETWORK_HANDLE nh, T_NM_DIR *nm_dir)
{       
        
    if (!VALID_NETWORK_HANDLE(nh)) {
        return(CM_BAD_NETWORK_HANDLE) ;
    }   
        
    ASSERT_RET(nm_dir, CM_BAD_POINTER) ;

    (void)memcpy((char *)&(network_tbl.list[nh].mib_addr.nm_dir),
            (char *)nm_dir, sizeof(T_NM_DIR)) ;

    return(CM_SUCCESS) ;
}

/*  Functions for accessing Network objects */
int
set_nmo_stack_cap_rec(NM_OBJ_STACK_CAP *p_nm_stck_cap_data)
{
    ASSERT_RET(p_nm_stck_cap_data, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_STACK_CAP *)&nm_obj.stack_mgmt,
                    (NM_OBJ_STACK_CAP *)p_nm_stck_cap_data,sizeof(NM_OBJ_STACK_CAP));

    return(CM_SUCCESS) ;  
}

int
set_nmo_vcr_lst_ctrl(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&nm_obj.var_list.vcr_lst_ctrl,
                                    (UINT8 *)p_read,sizeof(UINT8));

    return(CM_SUCCESS) ;  
}

int
set_nmo_vcr_lst_charac(NM_OBJ_VCR_LST_CHARAC *p_nm_vcrl_charac)
{
    ASSERT_RET(p_nm_vcrl_charac, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_LST_CHARAC *)&nm_obj.var_list.vcr_lst_charac,
                    (NM_OBJ_VCR_LST_CHARAC *)p_nm_vcrl_charac,sizeof(NM_OBJ_VCR_LST_CHARAC));

    return(CM_SUCCESS) ;
}

int
set_nmo_vcr_lst_static(NM_OBJ_VCR_STATIC_ENRY *p_nm_vcr_static_ent)
{
    ASSERT_RET(p_nm_vcr_static_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STATIC_ENRY *)&nm_obj.var_list.vcr_static_entry,
                    (NM_OBJ_VCR_STATIC_ENRY *) p_nm_vcr_static_ent,sizeof(NM_OBJ_VCR_STATIC_ENRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_vcr_lst_dyn(NM_OBJ_VCR_DYN_ENTRY *p_nm_vcr_dyn_ent)
{
    ASSERT_RET(p_nm_vcr_dyn_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_DYN_ENTRY *)&nm_obj.var_list.vcr_dyn_entry,
                    (NM_OBJ_VCR_DYN_ENTRY *) p_nm_vcr_dyn_ent,sizeof(NM_OBJ_VCR_DYN_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_vcr_stat_entry(NM_OBJ_VCR_STAT_ENTRY *p_nm_vcr_stat_entry)
{
    ASSERT_RET(p_nm_vcr_stat_entry, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STAT_ENTRY *)&nm_obj.var_list.vcr_stat_entry,
                    (NM_OBJ_VCR_STAT_ENTRY *)p_nm_vcr_stat_entry, sizeof(NM_OBJ_VCR_STAT_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_dlme_bsc_charac(NM_OBJ_DLME_BSC_CHARAC *p_nm_dlme_bsc_charc)
{
    ASSERT_RET(p_nm_dlme_bsc_charc, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_CHARAC *)&nm_obj.dlme_basic.dlme_basic_charc,
                    (NM_OBJ_DLME_BSC_CHARAC *)p_nm_dlme_bsc_charc,sizeof(NM_OBJ_DLME_BSC_CHARAC));

    return(CM_SUCCESS) ;
}

int
set_nmo_dlme_bsc_info(NM_OBJ_DLME_BSC_INFO *p_nm_dlme_bsc_info)
{
    ASSERT_RET(p_nm_dlme_bsc_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_INFO *)&nm_obj.dlme_basic.dlme_basic_info,
                    (NM_OBJ_DLME_BSC_INFO *)p_nm_dlme_bsc_info,sizeof(NM_OBJ_DLME_BSC_INFO));

    return(CM_SUCCESS) ;
}

int
set_nmo_dlme_bsc_stat(NM_OBJ_DLME_STAT *p_nm_dlme_stat)
{
    ASSERT_RET(p_nm_dlme_stat, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_STAT *)&nm_obj.dlme_basic.dlme_stat_entry,
                    (NM_OBJ_DLME_STAT *)p_nm_dlme_stat,sizeof(NM_OBJ_DLME_STAT));

    return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_cap(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&nm_obj.dlme_lm.dlme_lm_cap,
                        (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_info(NM_OBJ_DLME_LM_INFO *p_nm_dlme_lm_info)
{
    ASSERT_RET(p_nm_dlme_lm_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_INFO *)&nm_obj.dlme_lm.dlme_lm_info,
                    (NM_OBJ_DLME_LM_INFO *)p_nm_dlme_lm_info,sizeof(NM_OBJ_DLME_LM_INFO));

      return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_pri_flag(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&nm_obj.dlme_lm.primary_lm_flag,
                            (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dlme_live_list_arr(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&nm_obj.dlme_lm.live_lst_arr[0],
                            (UINT8 *)p_read,sizeof(UINT8)*32);
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_boot_op(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&nm_obj.dlme_lm.boot_operat,
                         (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_curr_link_sett(NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)&nm_obj.dlme_lm.curr_link_sett,
                    (NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
set_nmo_dlme_lm_config_link_sett(NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)&nm_obj.dlme_lm.conf_link_sett,
                    (NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
set_nmo_dev_stack_cap_rec(NETWORK_HANDLE nh, NM_OBJ_STACK_CAP *p_nm_stck_cap_data)
{
    ASSERT_RET(p_nm_stck_cap_data, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_STACK_CAP *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.stack_mgmt),
                    (NM_OBJ_STACK_CAP *)p_nm_stck_cap_data,sizeof(NM_OBJ_STACK_CAP));

    return(CM_SUCCESS) ;  
}

int
set_nmo_dev_vcr_lst_ctrl(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_lst_ctrl),
                                    (UINT8 *)p_read,sizeof(UINT8));

    return(CM_SUCCESS) ;  
}

int
set_nmo_dev_vcr_lst_charac(NETWORK_HANDLE nh, NM_OBJ_VCR_LST_CHARAC *p_nm_vcrl_charac)
{
    ASSERT_RET(p_nm_vcrl_charac, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_LST_CHARAC *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_lst_charac),
                    (NM_OBJ_VCR_LST_CHARAC *)p_nm_vcrl_charac,sizeof(NM_OBJ_VCR_LST_CHARAC));

    return(CM_SUCCESS) ;
}

int
set_nmo_dev_vcr_lst_static(NETWORK_HANDLE nh, NM_OBJ_VCR_STATIC_ENRY *p_nm_vcr_static_ent)
{
    ASSERT_RET(p_nm_vcr_static_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STATIC_ENRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_static_entry),
                    (NM_OBJ_VCR_STATIC_ENRY *) p_nm_vcr_static_ent,sizeof(NM_OBJ_VCR_STATIC_ENRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_dev_vcr_lst_dyn(NETWORK_HANDLE nh, NM_OBJ_VCR_DYN_ENTRY *p_nm_vcr_dyn_ent)
{
    ASSERT_RET(p_nm_vcr_dyn_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_DYN_ENTRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_dyn_entry),
                    (NM_OBJ_VCR_DYN_ENTRY *) p_nm_vcr_dyn_ent,sizeof(NM_OBJ_VCR_DYN_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_dev_vcr_stat_entry(NETWORK_HANDLE nh, NM_OBJ_VCR_STAT_ENTRY *p_nm_vcr_stat_entry)
{
    ASSERT_RET(p_nm_vcr_stat_entry, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STAT_ENTRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_stat_entry),
                    (NM_OBJ_VCR_STAT_ENTRY *)p_nm_vcr_stat_entry, sizeof(NM_OBJ_VCR_STAT_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_bsc_charac(NETWORK_HANDLE nh, NM_OBJ_DLME_BSC_CHARAC *p_nm_dlme_bsc_charc)
{
    ASSERT_RET(p_nm_dlme_bsc_charc, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_CHARAC *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_basic_charc),
                    (NM_OBJ_DLME_BSC_CHARAC *)p_nm_dlme_bsc_charc,sizeof(NM_OBJ_DLME_BSC_CHARAC));

    return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_bsc_info(NETWORK_HANDLE nh, NM_OBJ_DLME_BSC_INFO *p_nm_dlme_bsc_info)
{
    ASSERT_RET(p_nm_dlme_bsc_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_INFO *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_basic_info),
                    (NM_OBJ_DLME_BSC_INFO *)p_nm_dlme_bsc_info,sizeof(NM_OBJ_DLME_BSC_INFO));

    return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_bsc_stat(NETWORK_HANDLE nh, NM_OBJ_DLME_STAT *p_nm_dlme_stat)
{
    ASSERT_RET(p_nm_dlme_stat, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_STAT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_stat_entry),
                    (NM_OBJ_DLME_STAT *)p_nm_dlme_stat,sizeof(NM_OBJ_DLME_STAT));

    return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_cap(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.dlme_lm_cap),
                        (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_info(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_INFO *p_nm_dlme_lm_info)
{
    ASSERT_RET(p_nm_dlme_lm_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_INFO *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.dlme_lm_info),
                    (NM_OBJ_DLME_LM_INFO *)p_nm_dlme_lm_info,sizeof(NM_OBJ_DLME_LM_INFO));

      return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_pri_flag(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.primary_lm_flag),
                            (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_live_list_arr(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.live_lst_arr[0]),
                            (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_boot_op(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.boot_operat),
                         (UINT8 *)p_read,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_curr_link_sett(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.curr_link_sett),
                    (NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
set_nmo_dev_dlme_lm_config_link_sett(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.conf_link_sett),
                    (NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

/*  Functions for accessing Network objects */
int
get_nmo_stack_cap_rec(NM_OBJ_STACK_CAP *p_nm_stck_cap_data)
{
    ASSERT_RET(p_nm_stck_cap_data, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_STACK_CAP *)p_nm_stck_cap_data,
                    (NM_OBJ_STACK_CAP *)&nm_obj.stack_mgmt,sizeof(NM_OBJ_STACK_CAP));

    return(CM_SUCCESS) ;  
}

int
get_nmo_vcr_lst_ctrl(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&nm_obj.var_list.vcr_lst_ctrl,sizeof(UINT8));

    return(CM_SUCCESS) ;  
}

int
get_nmo_vcr_lst_charac(NM_OBJ_VCR_LST_CHARAC *p_nm_vcrl_charac)
{
    ASSERT_RET(p_nm_vcrl_charac, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_LST_CHARAC *)p_nm_vcrl_charac,
                    (NM_OBJ_VCR_LST_CHARAC *)&nm_obj.var_list.vcr_lst_charac,sizeof(NM_OBJ_VCR_LST_CHARAC));

    return(CM_SUCCESS) ;
}

int
get_nmo_vcr_lst_static(NM_OBJ_VCR_STATIC_ENRY *p_nm_vcr_static_ent)
{
    ASSERT_RET(p_nm_vcr_static_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STATIC_ENRY *) p_nm_vcr_static_ent,
                    (NM_OBJ_VCR_STATIC_ENRY *)&nm_obj.var_list.vcr_static_entry,sizeof(NM_OBJ_VCR_STATIC_ENRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_vcr_lst_dyn(NM_OBJ_VCR_DYN_ENTRY *p_nm_vcr_dyn_ent)
{
    ASSERT_RET(p_nm_vcr_dyn_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_DYN_ENTRY *) p_nm_vcr_dyn_ent,
                    (NM_OBJ_VCR_DYN_ENTRY *)&nm_obj.var_list.vcr_dyn_entry, sizeof(NM_OBJ_VCR_DYN_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_vcr_stat_entry(NM_OBJ_VCR_STAT_ENTRY *p_nm_vcr_stat_entry)
{
    ASSERT_RET(p_nm_vcr_stat_entry, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STAT_ENTRY *)p_nm_vcr_stat_entry,
                    (NM_OBJ_VCR_STAT_ENTRY *)&nm_obj.var_list.vcr_stat_entry, sizeof(NM_OBJ_VCR_STAT_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_dlme_bsc_charac(NM_OBJ_DLME_BSC_CHARAC *p_nm_dlme_bsc_charc)
{
    ASSERT_RET(p_nm_dlme_bsc_charc, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_CHARAC *)p_nm_dlme_bsc_charc,
                    (NM_OBJ_DLME_BSC_CHARAC *)&nm_obj.dlme_basic.dlme_basic_charc,sizeof(NM_OBJ_DLME_BSC_CHARAC));

    return(CM_SUCCESS) ;
}

int
get_nmo_dlme_bsc_info(NM_OBJ_DLME_BSC_INFO *p_nm_dlme_bsc_info)
{
    ASSERT_RET(p_nm_dlme_bsc_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_INFO *)p_nm_dlme_bsc_info,
                    (NM_OBJ_DLME_BSC_INFO *)&nm_obj.dlme_basic.dlme_basic_info,sizeof(NM_OBJ_DLME_BSC_INFO));

    return(CM_SUCCESS) ;
}

int
get_nmo_dlme_bsc_stat(NM_OBJ_DLME_STAT *p_nm_dlme_stat)
{
    ASSERT_RET(p_nm_dlme_stat, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_STAT *)p_nm_dlme_stat,
                    (NM_OBJ_DLME_STAT *)&nm_obj.dlme_basic.dlme_stat_entry,
                    sizeof(NM_OBJ_DLME_STAT));

    return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_cap(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&nm_obj.dlme_lm.dlme_lm_cap,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_info(NM_OBJ_DLME_LM_INFO *p_nm_dlme_lm_info)
{
    ASSERT_RET(p_nm_dlme_lm_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_INFO *)p_nm_dlme_lm_info,
                    (NM_OBJ_DLME_LM_INFO *)&nm_obj.dlme_lm.dlme_lm_info,sizeof(NM_OBJ_DLME_LM_INFO));

      return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_pri_flag(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&nm_obj.dlme_lm.primary_lm_flag,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dlme_live_list_arr(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&nm_obj.dlme_lm.live_lst_arr[0],
                            sizeof(UINT8)*32);
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_boot_op(UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&nm_obj.dlme_lm.boot_operat,sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_curr_link_sett(NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,
                    (NM_OBJ_DLME_LM_LINKSETT *)&nm_obj.dlme_lm.curr_link_sett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
get_nmo_dlme_lm_config_link_sett(NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,
                    (NM_OBJ_DLME_LM_LINKSETT *)&nm_obj.dlme_lm.conf_link_sett,sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
get_nmo_dev_stack_cap_rec(NETWORK_HANDLE nh, NM_OBJ_STACK_CAP *p_nm_stck_cap_data)
{
    ASSERT_RET(p_nm_stck_cap_data, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_STACK_CAP *)p_nm_stck_cap_data,
                    (NM_OBJ_STACK_CAP *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.stack_mgmt),sizeof(NM_OBJ_STACK_CAP));

    return(CM_SUCCESS) ;  
}

int
get_nmo_dev_vcr_lst_ctrl(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_lst_ctrl),sizeof(UINT8));

    return(CM_SUCCESS) ;  
}

int
get_nmo_dev_vcr_lst_charac(NETWORK_HANDLE nh, NM_OBJ_VCR_LST_CHARAC *p_nm_vcrl_charac)
{
    ASSERT_RET(p_nm_vcrl_charac, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_LST_CHARAC *)p_nm_vcrl_charac,
                    (NM_OBJ_VCR_LST_CHARAC *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_lst_charac),
                    sizeof(NM_OBJ_VCR_LST_CHARAC));

    return(CM_SUCCESS) ;
}

int
get_nmo_dev_vcr_lst_static(NETWORK_HANDLE nh, NM_OBJ_VCR_STATIC_ENRY *p_nm_vcr_static_ent)
{
    ASSERT_RET(p_nm_vcr_static_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STATIC_ENRY *) p_nm_vcr_static_ent,
                    (NM_OBJ_VCR_STATIC_ENRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_static_entry),
                    sizeof(NM_OBJ_VCR_STATIC_ENRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_dev_vcr_lst_dyn(NETWORK_HANDLE nh, NM_OBJ_VCR_DYN_ENTRY *p_nm_vcr_dyn_ent)
{
    ASSERT_RET(p_nm_vcr_dyn_ent, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_DYN_ENTRY *) p_nm_vcr_dyn_ent,
                    (NM_OBJ_VCR_DYN_ENTRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_dyn_entry),
                     sizeof(NM_OBJ_VCR_DYN_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_dev_vcr_stat_entry(NETWORK_HANDLE nh, NM_OBJ_VCR_STAT_ENTRY *p_nm_vcr_stat_entry)
{
    ASSERT_RET(p_nm_vcr_stat_entry, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_VCR_STAT_ENTRY *)p_nm_vcr_stat_entry,
                    (NM_OBJ_VCR_STAT_ENTRY *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.var_list.vcr_stat_entry),
                     sizeof(NM_OBJ_VCR_STAT_ENTRY));
     
    return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_bsc_charac(NETWORK_HANDLE nh, NM_OBJ_DLME_BSC_CHARAC *p_nm_dlme_bsc_charc)
{
    ASSERT_RET(p_nm_dlme_bsc_charc, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_CHARAC *)p_nm_dlme_bsc_charc,
                    (NM_OBJ_DLME_BSC_CHARAC *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_basic_charc),
                    sizeof(NM_OBJ_DLME_BSC_CHARAC));

    return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_bsc_info(NETWORK_HANDLE nh, NM_OBJ_DLME_BSC_INFO *p_nm_dlme_bsc_info)
{
    ASSERT_RET(p_nm_dlme_bsc_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_BSC_INFO *)p_nm_dlme_bsc_info,
                    (NM_OBJ_DLME_BSC_INFO *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_basic_info),
                    sizeof(NM_OBJ_DLME_BSC_INFO));

    return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_bsc_stat(NETWORK_HANDLE nh, NM_OBJ_DLME_STAT *p_nm_dlme_stat)
{
    ASSERT_RET(p_nm_dlme_stat, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_STAT *)p_nm_dlme_stat,
                    (NM_OBJ_DLME_STAT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_basic.dlme_stat_entry),
                    sizeof(NM_OBJ_DLME_STAT));

    return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_cap(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.dlme_lm_cap),sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_info(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_INFO *p_nm_dlme_lm_info)
{
    ASSERT_RET(p_nm_dlme_lm_info, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_INFO *)p_nm_dlme_lm_info,
                    (NM_OBJ_DLME_LM_INFO *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.dlme_lm_info),
                    sizeof(NM_OBJ_DLME_LM_INFO));

      return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_pri_flag(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.primary_lm_flag),sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_live_list_arr(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.live_lst_arr[0]),
                            sizeof(UINT8)*32);
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_boot_op(NETWORK_HANDLE nh, UINT8 *p_read)
{
    ASSERT_RET(p_read, CM_BAD_POINTER);

    (void)memcpy((UINT8 *)p_read,(UINT8 *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.boot_operat),sizeof(UINT8));
    
     return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_curr_link_sett(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,
                    (NM_OBJ_DLME_LM_LINKSETT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.curr_link_sett),
                    sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int
get_nmo_dev_dlme_lm_config_link_sett(NETWORK_HANDLE nh, NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett)
{
    ASSERT_RET(p_nm_dlme_lm_linksett, CM_BAD_POINTER);

    (void)memcpy((NM_OBJ_DLME_LM_LINKSETT *)p_nm_dlme_lm_linksett,
                    (NM_OBJ_DLME_LM_LINKSETT *)&(network_tbl.list[nh].mib_addr.remote_nm_obj.dlme_lm.conf_link_sett),
                    sizeof(NM_OBJ_DLME_LM_LINKSETT));

      return(CM_SUCCESS) ;
}

int 
valid_block_handle(BLOCK_HANDLE bh)
{
    return  ((bh >= 0) && (bh < active_blk_tbl.count) 
            && (active_blk_tbl.list[bh])) ;
}


int 
valid_device_handle(DEVICE_HANDLE dh)
{
    return ((dh >= 0) && (dh < active_dev_tbl.count)
            && (active_dev_tbl.list[dh])) ;
}


int 
valid_device_type_handle(DEVICE_TYPE_HANDLE dth)
{
    return ((dth >= 0) && (dth < active_dev_type_tbl.count) 
            && (active_dev_type_tbl.list[dth])) ;
}


int 
valid_network_handle(NETWORK_HANDLE nh)
{
    return((nh >= 0) && (nh < network_tbl.count)) ;
}


int 
valid_rod_handle(ROD_HANDLE rh)
{
    return ((rh >= 0) && (rh < rod_tbl.count) 
            && (rod_tbl.list[rh])) ;
}


int 
valid_dd_handle(DD_HANDLE *ddh)
{
    return ((((ddh)->dd_handle_type == DDHT_ROD) || 
            ((ddh)->dd_handle_type == DDHT_FLAT)) && 
            ((ddh)->dd_handle_number >= 0)) ;
}
