/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_gstor.c - Connection Manager Communication global storage
 *
 */

#include "cm_lib.h"
#include "cm_loc.h"


/*
 *	The Active Tables.
 */

ACTIVE_BLK_TBL			active_blk_tbl;			/* Block */
ACTIVE_DEV_TBL			active_dev_tbl;			/* Device */
ACTIVE_DEV_TYPE_TBL     active_dev_type_tbl;    /* Device Type */


/*
 *	The Network Table
 */

NETWORK_TBL		network_tbl;


/*
 *	The Network Objects Table
 */
CM_NM_OBJ       nm_obj;

      
/*
 *	The ROD Table
 */

ROD_TBL			 rod_tbl;

/*
 *	The Thread structure
 */

ACTIVE_THREAD_DATA		active_thread_data;

/*
 *	The Directory for DD Files
 */

char	dd_path[256];
