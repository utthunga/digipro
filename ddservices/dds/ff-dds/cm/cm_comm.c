/**
 *      Device Description Services Rel. 4.3
 *      Copyright 1994-2002 - Fieldbus Foundation
 *      All rights reserved.
 */

/*
 *  cm_comm.c - Connection Manager Communication module
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     /* ANSI */

#include "cm_lib.h"
#include "cm_loc.h"
#include "ddi_lib.h"
#include "int_diff.h"
#include "dds_comm.h"
#include "ddssetup.h"

/* Do not warn when some function variables are unused. */
#if defined(_MSC_VER)
/* warning C4100: 'varname' : unreferenced formal parameter */
#pragma warning(disable:4100)
#endif

/*
 *  Type A Comm Stack Function Pointers
 */

static int  (*initialize_fn_ptr) P((ENV_INFO *, 
            HOST_CONFIG *)) = 0;
static int  (*config_stack_fn_ptr) P((ENV_INFO *, 
            CM_NMA_INFO *, HOST_CONFIG *)) = 0;
static int  (*get_stack_config_fn_ptr) P((ENV_INFO *, 
            CM_NMA_INFO *)) = 0;
static int  (*init_network_fn_ptr) P((NETWORK_HANDLE )) = 0;
static int  (*find_block_fn_ptr)
    P((char *, NETWORK_HANDLE, FIND_CNF_A **, ENV_INFO *)) = 0;
static int  (*initiate_comm_fn_ptr)
    P((DEVICE_HANDLE, CREF *, CREF *, CREF *, ENV_INFO *)) = 0;
static int  (*terminate_comm_fn_ptr) P((DEVICE_HANDLE, ENV_INFO *)) = 0;
static int  (*comm_get_fn_ptr)
    P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT **, ENV_INFO *)) = 0;
static int  (*comm_put_fn_ptr)
    P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT *, ENV_INFO *)) = 0;
static int  (*comm_read_fn_ptr)
    P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
        CM_VALUE_LIST *, ENV_INFO *)) = 0;
static int  (*comm_write_fn_ptr)
    P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
        CM_VALUE_LIST *, ENV_INFO *)) = 0;
static int  (*comm_upload_fn_ptr)
    P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, 
    UINT8 *, ENV_INFO *)) = 0;
static int  (*param_read_fn_ptr) P((ENV_INFO *, int, SUBINDEX)) = 0;
static int  (*param_write_fn_ptr) P((ENV_INFO *, int, SUBINDEX)) = 0;
static int  (*param_load_fn_ptr)
    P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) = 0;
static int  (*param_unload_fn_ptr)
    P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) = 0;
static int (*offline_init_fn_ptr) P((NETWORK_HANDLE)) = 0; 
static int  (*scan_network_fn_ptr) 
    P((ENV_INFO *, SCAN_DEV_TBL *)) = 0;
static int  (*build_dev_tbl_fn_ptr) 
    P(( ENV_INFO *, NETWORK_HANDLE)) = 0;
static int  (*delete_dev_tbl_fn_ptr) 
    P(( ENV_INFO *, NETWORK_HANDLE)) = 0;
static int  (*sm_identify_fn_ptr) 
    P((ENV_INFO*,
    unsigned short, CR_SM_IDENTIFY_CNF*)) = 0; 
static int  (*sm_clr_node_fn_ptr) 
    P((ENV_INFO *,
    unsigned short int, CR_SM_IDENTIFY_CNF* )) = 0;
static int  (*sm_set_node_fn_ptr) 
    P((ENV_INFO *,
    unsigned short int, char* )) = 0;
static int  (*sm_clr_tag_fn_ptr) 
    P((ENV_INFO *,
    unsigned short int, CR_SM_IDENTIFY_CNF* )) = 0;
static int  (*sm_assign_tag_fn_ptr) 
    P((ENV_INFO *,
    unsigned short int, CR_SM_IDENTIFY_CNF* )) = 0;
static int (*cm_get_actual_index_fn_ptr)
	P((unsigned short *, int *, unsigned short *)) = 0;
static int (*cm_dds_bypass_read_fn_ptr)
	P((ENV_INFO *, unsigned short, int, unsigned short, void *)) = 0;

// Network related functions

static int (*nm_wr_local_vcr_fn_ptr)
    P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* )) = 0;
static int (*nm_fms_initiate_fn_ptr)
    P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* )) = 0;
static int (*nm_get_od_fn_ptr)
    P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* )) = 0;
static int (*nm_read_fn_ptr)
    P((ENV_INFO *, NETWORK_HANDLE, unsigned short, int, int)) = 0;
static int (*nm_dev_boot_fn_ptr)
    P((ENV_INFO *, NETWORK_HANDLE, unsigned short, int)) = 0;
static int (*cm_exit_fn_ptr) P((ENV_INFO *, NET_TYPE )) = 0;
static int (*cm_firmware_download_fn_ptr) P((ENV_INFO *, char * )) = 0;
static int (*cm_get_host_info_fn_ptr) P((ENV_INFO *, HOST_INFO *, HOST_CONFIG *)) = 0;
static int (*cm_reset_fbk_fn_ptr) 
    P((ENV_INFO *, NETWORK_HANDLE, HOST_CONFIG *)) = 0;
static int (*cm_set_appl_state_fn_ptr) P((ENV_INFO *, int *, int)) = 0;
static int (*cm_abort_cr_fn_ptr) P((ENV_INFO *, unsigned short, unsigned short )) = 0;


/***********************************************************************
 *
 *  Name:  cr_init_cstk_a_fn_ptrs
 *
 *  ShortDesc:  Initialize the type A Comm Stack function pointers.
 *
 *  Description:
 *      The cr_init_cstk_a_fn_ptrs function links the pointers of
 *      the functions that are used in the type A Comm Stack.
 *
 *  Inputs:
 *      cstk_a_fn_ptrs - the type A Comm Stack function pointers.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Void.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

void
cr_init_cstk_a_fn_ptrs(const CSTK_A_FN_PTRS *cstk_a_fn_ptrs)
{

    /*
     *  Link the pointers of the functions that are used in the
     *  type A Comm Stack.
     */

    initialize_fn_ptr = cstk_a_fn_ptrs->initialize_fn_ptr;
    config_stack_fn_ptr = cstk_a_fn_ptrs->config_stack_fn_ptr;
    get_stack_config_fn_ptr = cstk_a_fn_ptrs->get_stack_config_fn_ptr;
    init_network_fn_ptr = cstk_a_fn_ptrs->init_network_fn_ptr;
    find_block_fn_ptr = cstk_a_fn_ptrs->find_block_fn_ptr;
    initiate_comm_fn_ptr = cstk_a_fn_ptrs->initiate_comm_fn_ptr;
    terminate_comm_fn_ptr = cstk_a_fn_ptrs->terminate_comm_fn_ptr;
    comm_get_fn_ptr = cstk_a_fn_ptrs->comm_get_fn_ptr;
    comm_put_fn_ptr = cstk_a_fn_ptrs->comm_put_fn_ptr;
    comm_read_fn_ptr = cstk_a_fn_ptrs->comm_read_fn_ptr;
    comm_write_fn_ptr = cstk_a_fn_ptrs->comm_write_fn_ptr;
    comm_upload_fn_ptr = cstk_a_fn_ptrs->comm_upload_fn_ptr;
    param_read_fn_ptr = cstk_a_fn_ptrs->param_read_fn_ptr;
    param_write_fn_ptr = cstk_a_fn_ptrs->param_write_fn_ptr;
    param_load_fn_ptr = cstk_a_fn_ptrs->param_load_fn_ptr;
    param_unload_fn_ptr = cstk_a_fn_ptrs->param_unload_fn_ptr;
    scan_network_fn_ptr = cstk_a_fn_ptrs->scan_network_fn_ptr;
    build_dev_tbl_fn_ptr = cstk_a_fn_ptrs->build_dev_tbl_fn_ptr;
    delete_dev_tbl_fn_ptr = cstk_a_fn_ptrs->delete_dev_tbl_fn_ptr;
    sm_identify_fn_ptr = cstk_a_fn_ptrs->sm_identify_fn_ptr;
    offline_init_fn_ptr = cstk_a_fn_ptrs->offline_init_fn_ptr; 
    sm_clr_node_fn_ptr = cstk_a_fn_ptrs->sm_clr_node_fn_ptr;
    sm_set_node_fn_ptr = cstk_a_fn_ptrs->sm_set_node_fn_ptr;
    sm_clr_tag_fn_ptr = cstk_a_fn_ptrs->sm_clr_tag_fn_ptr;
    sm_assign_tag_fn_ptr = cstk_a_fn_ptrs->sm_assign_tag_fn_ptr;
    nm_wr_local_vcr_fn_ptr = cstk_a_fn_ptrs->nm_wr_local_vcr_fn_ptr;
    nm_fms_initiate_fn_ptr = cstk_a_fn_ptrs->nm_fms_initiate_fn_ptr;
    nm_get_od_fn_ptr = cstk_a_fn_ptrs->nm_get_od_fn_ptr;
    nm_read_fn_ptr = cstk_a_fn_ptrs->nm_read_fn_ptr;
    nm_dev_boot_fn_ptr = cstk_a_fn_ptrs->nm_dev_boot_fn_ptr;
    cm_exit_fn_ptr = cstk_a_fn_ptrs->cm_exit_fn_ptr;
    cm_firmware_download_fn_ptr = cstk_a_fn_ptrs->cm_firmware_download_fn_ptr;
    cm_get_host_info_fn_ptr = cstk_a_fn_ptrs->cm_get_host_info_fn_ptr;
    cm_reset_fbk_fn_ptr = cstk_a_fn_ptrs->cm_reset_fbk_fn_ptr;
    cm_set_appl_state_fn_ptr = cstk_a_fn_ptrs->cm_set_appl_state_fn_ptr;
    cm_get_actual_index_fn_ptr = cstk_a_fn_ptrs->cm_get_actual_index_fn_ptr;
    cm_dds_bypass_read_fn_ptr = cstk_a_fn_ptrs->cm_dds_bypass_read_fn_ptr;
    cm_abort_cr_fn_ptr = cstk_a_fn_ptrs->cm_abort_cr_fn_ptr;
}


/***********************************************************************
 *
 *  Name:   init_network_table
 *
 *  ShortDesc:  Initialize the Network Table.
 *
 *  Description:
 *      The init_network_table function initializes the Network Table.
 *      If the Connection Manager mode is On-Line, it reads in the
 *      "network.dat" data file and uses this information to set up
 *      the Network Table.  If the Connection Manager mode is Off-Line,
 *      it sets up the Network Table with one generic entry.
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_CM_MODE.
 *      CM_BAD_FILE_OPEN.
 *      CM_NO_MEMORY.
 *      CM_BAD_FILE_CLOSE.
 *      CM_FILE_ERROR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
init_network_table(unsigned char cm_mode)
{
	int r_code = CM_SUCCESS;
    /*
     *  Preliminary Initialize the Network Table.
     */

    network_tbl.count = 0;
    network_tbl.list =
    		(NETWORK_TBL_ELEM *) malloc(sizeof (NETWORK_TBL_ELEM));
    if (!network_tbl.list) {
        return(CM_NO_MEMORY);
    }

    switch (cm_mode) {

        case CMM_ON_LINE:

            /*
             * No requirement for network.dat file as network table
             * as network table will be built after scanning the
             * network bus
             */
        	SET_NT_NET_TYPE(0, NT_A);
            r_code = (CM_SUCCESS);
            break;

        case CMM_OFF_LINE:

            /*
             *  Set up for off-line mode (one Network Table entry).
             */
            SET_NT_NET_TYPE(0, NT_OFF_LINE);
            r_code = (CM_SUCCESS);
            break;

        default:
            r_code = (CM_BAD_CM_MODE);
            break;
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name:   cr_scan_network
 *
 *  ShortDesc:  Scan list of Device connected to the FF BUS.
 *
 *  Description:
 *      The cr_scan_network function does the scan of the devices connected
 *      in the the bus and populated the list devices boards.
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *      Return values from ...
 *      Return values from initialize function.
 *      Return values from init_network function.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_scan_network(ENV_INFO *env_info, SCAN_DEV_TBL *scan_dev_table)
{
    int r_code = CM_SUCCESS;
 
    if (scan_network_fn_ptr != NULL) {
        r_code = (*scan_network_fn_ptr)(env_info, scan_dev_table);
    }
        
    return(r_code);
}


/***********************************************************************
 *
 *  Name:   cr_build_device_table
 *
 *  ShortDesc:  Builds the device table for the device 
 *              connected to the FF BUS.
 *
 *  Description:
 *      The cr_build_device_table function does the read the device
 *      information and builds the table
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *      Return values from ...
 *      Return values from initialize function.
 *      Return values from init_network function.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_build_device_table( ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int r_code = CM_SUCCESS;

    APP_INFO    *app_info;

    app_info = env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Populate the device Table.
     */

    if (build_dev_tbl_fn_ptr != NULL) {
        r_code = (*build_dev_tbl_fn_ptr)( env_info, nh);
    }
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:   cr_delete_device_table
 *
 *  ShortDesc:  Deletes the memory from the device table for the device 
 *              connected to the FF BUS.
 *
 *  Description:
 *      The cr_delete_device_table function does the memeory clean-up 
 *      for the device information in the table
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *      Return values from ...
 *      Return values from initialize function.
 *      Return values from init_network function.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_delete_device_table( ENV_INFO *env_info, NETWORK_HANDLE nh)
{

    int r_code = CM_SUCCESS;

    /*
     *  Initialize a type A Network.
     */

    if (delete_dev_tbl_fn_ptr != NULL) {
        r_code = (*delete_dev_tbl_fn_ptr)( env_info, nh);
        if (r_code != CM_SUCCESS) {
            return(r_code);
        }
    }

    /* Clear the active block table */
    clr_abt_tbls();
    
    /* Clear the active device table */
    clr_adt_tbls();

    /* Clear the active device type table */
    clr_adtt_tbls();
    
    /* Clear the rod table */
   
    rod_free((char *)rod_tbl.list);
    
    return(r_code);
}



/***********************************************************************
 *
 *  Name:   cr_build_network_table
 *
 *  ShortDesc:  Build the network table according to the connected
 *              devices
 *  Description:
 *      The cr_build_network_table function does whatever is necessary 
 *      build the network for all communication.  
 *      In the case of a PC with communication
 *      boards, this process would initialize all the boards.
 *
 *  Inputs:
 *      network_handle - the handler for each transmitter device 
 *             connected int the network
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_build_network_table(NETWORK_HANDLE   network_handle)
{
    int r_code = CM_SUCCESS;

    if (init_network_fn_ptr != NULL) {
        r_code = (*init_network_fn_ptr)(network_handle);
    }
    return(r_code);
}

/***********************************************************************
 *
 *  Name:   cr_init
 *
 *  ShortDesc:  Initialize the communication support.
 *
 *  Description:
 *      The cr_init function does whatever is necessary to initialize
 *      all communication.  In the case of a PC with communication
 *      boards, this process would initialize all the boards.
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *      Return values from ...
 *      Return values from initialize function.
 *      Return values from init_network function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_init(ENV_INFO *env_info, HOST_CONFIG *host_config)
{
    int             r_code;

    /*
     *  Initialize the Network Table for whatever networks will be used. 
     *  This information is obtained from a data file when using on-line mode.
     */

    r_code = init_network_table(host_config->host_mode);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Perform any general network initialization.
     */


    if (initialize_fn_ptr != NULL) {
        r_code = (*initialize_fn_ptr)(env_info, host_config) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_comm_get
 *
 *  ShortDesc:   Get an object from a device.
 *
 *  Description:
 *      The cr_comm_get function takes a device handle, communication
 *      type, and object index and gets the corresponding object.
 *      The (requested) object output is a pointer to an object.
 *      This object and any of its allocated contents reside in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      exists in.
 *      comm_type - the particular OD that the object exists in
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the requested object.
 *
 *  Outputs:
 *      object - the requested object.
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_DEVICE_HANDLE.
 *      CM_NO_COMM.
 *      Return values from ...
 *      Return values from cs_get_cref function.
 *      Return values from comm_get function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_comm_get(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, 
    OBJECT_INDEX object_index, OBJECT **object, ENV_INFO *env_info)
{

    int         r_code = CM_SUCCESS;
    APP_INFO    *app_info;

    app_info = env_info->app_info;  
    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */
            
    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to get an object from a device on an Ethernet 
             *  Network.  Use the specified network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to get an object from a device on a Control
             *  Network.  Use the specified network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        case NT_A:
        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             *  Get an object from a device on a Simulated Network.
             */

            ASSERT_RET(comm_get_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*comm_get_fn_ptr)(device_handle, comm_type,
                    object_index, object, env_info);
            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_comm_put
 *
 *  ShortDesc:   Put an object into a device.
 *
 *  Description:
 *      The cr_comm_put function takes a device handle, communication
 *      type, object index, and object and puts the object into the
 *      corresponding device/OD.  The object being put may be allocated
 *      anywhere as its information is copied before it is transmitted.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      is to be put in.
 *      comm_type - the particular OD that the object will be put in
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the object that is to be put.
 *      object - the object that is to be put.
 *
 *  Outputs:
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_DEVICE_HANDLE.
 *      CM_NO_COMM.
 *      Return values from ...
 *      Return values from cs_get_cref function.
 *      Return values from comm_put function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_comm_put(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, 
        OBJECT_INDEX object_index, OBJECT *object, ENV_INFO *env_info)
{
    int             r_code = CM_SUCCESS;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to put an object into a device on an Ethernet
             *  Network.  Use the specified network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to put an object into a device on a Control
             *  Network.  Use the specified network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        case NT_A:
        case NT_A_SIM:

            /*
             *  Put an object into a device on a Simulated Network.
             */

            ASSERT_RET(comm_put_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*comm_put_fn_ptr)(device_handle, comm_type,
                    object_index, object, env_info);

            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_comm_read
 *
 *  ShortDesc:   Read an object's value from a device.
 *
 *  Description:
 *      The cr_comm_read function takes a device handle, communication
 *      type, object index, and subindex and reads the corresponding
 *      object's value.  The (requested) object's value output is a
 *      pointer to a value list.  This value list and any of its
 *      allocated contents reside in system memory and should be freed
 *      after it is used.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      exists in.
 *      comm_type - the particular OD that the object exists in
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the requested object.
 *      subindex - the subindex of the requested object.
 *
 *  Outputs:
 *      cm_value_list - the requested object's value.
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_DEVICE_HANDLE.
 *      CM_NO_COMM.
 *      Return values from ...
 *      Return values from cs_get_cref function.
 *      Return values from comm_read function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_comm_read(DEVICE_HANDLE device_handle, COMM_TYPE comm_type,
    OBJECT_INDEX object_index, SUBINDEX subindex, CM_VALUE_LIST *cm_value_list,
    ENV_INFO *env_info)
{
    int             r_code = CM_SUCCESS;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to read an object's value from a device
             *  on an Ethernet Network.  Use the specified network
             *  route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to read an object's value from a device
             *  on a Control Network.  Use the specified network
             *  route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        case NT_A:
        case NT_A_SIM:

            /*
             *  Read an object's value from a device on a type A Network.
             */

            ASSERT_RET(comm_read_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*comm_read_fn_ptr)(device_handle, comm_type,
                    object_index, subindex, cm_value_list, env_info);
            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_comm_write
 *
 *  ShortDesc:   Write an object's value to a device.
 *
 *  Description:
 *      The cr_comm_write function takes a device handle, communication
 *      type, object index, subindex, and value list and writes the
 *      object's value to the corresponding device/OD.  The value
 *      being written may be allocated anywhere as its information is
 *      copied before it is transmitted.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the value
 *                      is to be written to.
 *      comm_type - the particular OD that the value will be written to
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the object.
 *      subindex - the subindex of the object.
 *      cm_value_list - the value to be written.
 *
 *  Outputs:
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_DEVICE_HANDLE.
 *      CM_NO_COMM.
 *      Return values from ...
 *      Return values from cs_get_cref function.
 *      Return values from comm_write function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_comm_write(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, 
        OBJECT_INDEX object_index, SUBINDEX subindex, 
        CM_VALUE_LIST *cm_value_list, ENV_INFO *env_info)
{

    int     r_code = CM_SUCCESS;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to write an object's value to a device
             *  on an Ethernet Network.  Use the specified network
             *  route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to write an object's value to a device
             *  on a Control Network.  Use the specified network
             *  route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        case NT_A:
        case NT_A_SIM:

            /*
             *  Write an object's value to a device on a
             *  type A Network.
             */

            ASSERT_RET(comm_write_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*comm_write_fn_ptr)(device_handle, comm_type,
                    object_index, subindex, cm_value_list, env_info);

            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_comm_upload
 *
 *  ShortDesc:   Upload an object's data to a device.
 *
 *  Description:
 *      The cr_comm_upload function takes a device handle, commun-
 *      ication type, object index, and data and uploads the object's
 *      data into the corresponding device/OD.  The data being up-
 *      loaded may be allocated anywhere as its information is copied
 *      before it is transmitted.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      exists in.
 *      comm_type - the particular OD that the object will be uploaded
 *                  to (OPOD, DDOD, or MIB).
 *      object_index - the index of the object to be uploaded.
 *      data - the data to be uploaded.
 *
 *  Outputs:
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_DEVICE_HANDLE.
 *      CM_NO_COMM.
 *      Return values from ...
 *      Return values from cs_get_cref function.
 *      Return values from comm_upload function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_comm_upload(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, 
        OBJECT_INDEX object_index, UINT8 *data, ENV_INFO *env_info)
{

    int             r_code = CM_SUCCESS;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to upload an object's data into
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to upload an object's data into
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        case NT_A:
        case NT_A_SIM:

            /*
             *  Upload an object's data into a device on a
             *  type A Network.
             */

            ASSERT_RET(comm_upload_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*comm_upload_fn_ptr)(device_handle, comm_type,
                    object_index, data, env_info);
            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_object_free
 *
 *  ShortDesc:  Free an object and all of its allocated contents
 *              from system memory.
 *
 *  Description:
 *      The cr_object_free function takes an object in system memory
 *      and frees it along with all of its allocated contents.
 *
 *  Inputs:
 *      object - the object to be freed.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Void.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

void
cr_object_free(OBJECT *object)

{

    if (object->name) {
        free((char *)object->name);
    }
    if (object->extension) {
        free((char *)object->extension);
    }
    if (object->value) {
        free((char *)object->value);
    }
    if (object->specific) {

        switch (object->code) {

            case OC_NULL:
                break;

            case OC_OD_DESCRIPTION:
                break;

            case OC_DOMAIN:
                break;

            case OC_PROGRAM_INVOCATION:
                if (object->specific->program_invocation.domain_list) {
                    free((char *)object->specific->
                            program_invocation.domain_list);
                }
                break;

            case OC_EVENT:
                break;

            case OC_DATA_TYPE:
                if (object->specific->data_type.symbol) {
                    free((char *)object->specific->data_type.symbol);
                }
                break;

            case OC_DATA_TYPE_STRUCTURE:
                if (object->specific->data_type_structure.type_list) {
                    free((char *)object->specific->
                            data_type_structure.type_list);
                }
                if (object->specific->data_type_structure.size_list) {
                    free((char *)object->specific->
                            data_type_structure.size_list);
                }
                break;

            case OC_SIMPLE_VARIABLE:
                break;

            case OC_ARRAY:
                break;

            case OC_RECORD:
                break;

            case OC_VARIABLE_LIST:
                if (object->specific->variable_list.list) {
                    free((char *)object->specific->variable_list.list);
                }
                break;

            default:
                break;
        }

        free((char *)object->specific);
    }

    free((char *)object);
}


/**********************************************************************
 *
 *  Name: cr_free_value_list
 *  ShortDesc: Free a value list and all of its allocated contents
 *
 *  Include:
 *      cm_lib.h
 *
 *  Description:
 *
 *      Cr_free_value_list frees all of the memory allocated for
 *      a COMM_VALUE_LIST.  Note that the COMM_VALUE_LIST structure
 *      itself is not free'ed.
 *
 *  Inputs:
 *      cm_value_listp = The value list to free
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      None.
 *
 *  Author:
 *      Bruce Davis
 *      Kent Anderson
 *
 *********************************************************************/

void
cr_free_value_list (COMM_VALUE_LIST *comm_value_listp)
{

    COMM_VALUE  *comm_valuep, *comm_value_endp;

    /*
     *  Step through the list, freeing all of the strings.
     */

    comm_valuep = comm_value_listp->cmvl_list;
    comm_value_endp = comm_valuep + comm_value_listp->cmvl_count;

    for ( ; comm_valuep < comm_value_endp; comm_valuep++) {
        switch (comm_valuep->cm_type) {
            case DDS_ASCII:
            case DDS_PASSWORD:
            case DDS_BITSTRING:
            case DDS_OCTETSTRING:
            case DDS_VISIBLESTRING:
            case DDS_EUC:
                free (comm_valuep->cm_val.cm_stream);
                break;

            default:
                break;
        }
    }

    /*
     *  Free the list.
     */

    free ((char *)comm_value_listp->cmvl_list);
    comm_value_listp->cmvl_list = NULL;
    comm_value_listp->cmvl_count = 0;
}


/***********************************************************************
 *
 *  Name:  cr_param_read
 *
 *  ShortDesc:  Read a parameter's value into the parameter cache
 *
 *  Description:
 *      The cr_param_read function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      p_ref - contains the reference (id or offset) of the parameter 
 *                  to be read and the sub-index of the (array or 
 *                  record) member.
 *
 *  Outputs:
 *      env_info -  contains returns of any communication errors and it
 *                  contains returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_param_read(ENV_INFO *env_info, const P_REF *p_ref)
{
    DEVICE_HANDLE               device_handle ;
    int                         r_code = CM_SUCCESS ;
    DDI_BLOCK_SPECIFIER         block_spec ;
    DDI_PARAM_SPECIFIER         param_spec;
    DDI_PARAM_INFO              param_info;
    int                         po ;
    NET_TYPE                    net_type ;
    APP_INFO 					*app_info;

    ASSERT_RET(env_info && p_ref, CM_BAD_POINTER) ;

    if(!(env_info && p_ref))
    	return CM_BAD_BLOCK_HANDLE;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
            CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    net_type = ADT_NET_TYPE(device_handle) ;

    if (
        (net_type == NT_A || net_type == NT_A_SIM || net_type == NT_OFF_LINE) &&
        (p_ref->type == PC_ITEM_ID_REF)) {
        block_spec.type = DDI_BLOCK_HANDLE ;
        block_spec.block.handle = env_info->block_handle ;
        param_spec.type = DDI_PS_ITEM_ID ;
        param_spec.subindex = p_ref->subindex ;
        param_spec.item.id = p_ref->id ;
        r_code = ddi_get_param_info(&block_spec, &param_spec, &param_info) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
        po = (int)(param_info.op_index - ABT_A_OP_INDEX(env_info->block_handle)) ;
    } else {
        po = p_ref->po ;
    }

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Read a parameter from a network
             */

            ASSERT_RET(param_read_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*param_read_fn_ptr)(env_info, po, p_ref->subindex);
            break ;

        case NT_ETHER:

            /*
             *  Put code here to read an object's value from
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Put code here to read an object's value from
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_OFF_LINE:
        	r_code =  CM_SUCCESS;
        	break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    return(r_code) ;
}


/***********************************************************************
 *
 *  Name:  cr_param_write
 *
 *  ShortDesc:  Write a parameter's value from the parameter cache
 *          to a device.
 *
 *  Description:
 *      The cr_param_write function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      p_ref - contains the reference (id or offset) of the parameter 
 *                  to be read, and it contains the sub-index of the 
 *                  (array or record) member.
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and it contains returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_param_write(ENV_INFO *env_info, const P_REF *p_ref)
{
    DEVICE_HANDLE           device_handle ;
    int                     r_code = CM_SUCCESS ;
    int                     po ;
    NET_TYPE                net_type ;
    APP_INFO                *app_info ;
    DDI_PARAM_SPECIFIER     param_spec ;
    DDI_PARAM_INFO          param_info;
    DDI_BLOCK_SPECIFIER     block_spec ;

    app_info = env_info->app_info;

    ASSERT_RET(env_info && p_ref, CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    net_type = ADT_NET_TYPE(device_handle) ;

    if ((net_type == NT_A || net_type == NT_A_SIM) &&
            (p_ref->type == PC_ITEM_ID_REF)) {
        block_spec.type = DDI_BLOCK_HANDLE ;
        block_spec.block.handle = env_info->block_handle ;
        param_spec.type = DDI_PS_ITEM_ID ;
        param_spec.item.id = p_ref->id ;
        r_code = ddi_get_param_info(&block_spec, &param_spec, &param_info) ;
        po = (int)(param_info.op_index - ABT_A_OP_INDEX(env_info->block_handle)) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
    }
    else {
        po = p_ref->po ;
    }

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Write a parameter from a type A network
             */

            ASSERT_RET(param_write_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*param_write_fn_ptr)(env_info, po, p_ref->subindex);
            break ;

        case NT_ETHER:

            /*
             *  Put code here to write an object's value from
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to write an object's value from
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        default:
            return(CM_NO_COMM);
    }

    return(r_code) ;
}


/***********************************************************************
 *
 *  Name:  cr_param_load
 *
 *  ShortDesc:  Read a parameter's value from a device.
 *
 *  Description:
 *      The cr_param_load function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *      Note: This function does not take values from the parameter
 *            cache and it does not update the parameter cache.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      p_ref - contains  a reference (id or offset) of the parameter 
 *                  to be read and the sub-index of the (array or 
 *                  record) member.
 *
 *  Outputs:
 *      cm_value_listp - The requested value(s).
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_param_load(ENV_INFO *env_info, const P_REF *p_ref, COMM_VALUE_LIST *cm_value_listp)
{
    DEVICE_HANDLE           device_handle ;
    int                     r_code = CM_SUCCESS ;
    int                     po ;
    NET_TYPE                net_type ;
    APP_INFO                *app_info ;
    DDI_BLOCK_SPECIFIER     block_spec ;
    DDI_PARAM_SPECIFIER     param_spec ;
    DDI_PARAM_INFO          param_info;

    app_info = env_info->app_info;

    ASSERT_RET(env_info && p_ref && cm_value_listp, CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    net_type = ADT_NET_TYPE(device_handle) ;

    if ((net_type == NT_A || net_type == NT_A_SIM) &&
            (p_ref->type == PC_ITEM_ID_REF)) {
        block_spec.type = DDI_BLOCK_HANDLE ;
        block_spec.block.handle = env_info->block_handle ;
        param_spec.type = DDI_PS_ITEM_ID ;
        param_spec.item.id = p_ref->id ;
        r_code = ddi_get_param_info(&block_spec, &param_spec, &param_info) ;
        po = (int)(param_info.op_index - ABT_A_OP_INDEX(env_info->block_handle)) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
    }
    else {
        po = p_ref->po ;
    }

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             * Read a parameter from a type A network
             */

            ASSERT_RET(param_load_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*param_load_fn_ptr)(env_info, po,
                    p_ref->subindex, cm_value_listp);
            break ;

        case NT_ETHER:

            /*
             *  Put code here to read an object's value from
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to read an object's value from
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_param_unload
 *
 *  ShortDesc:  Write a parameter's value to a device.
 *
 *  Description:
 *      The cr_param_unload function takes a block handle, 
 *      param ref, and sub-index and writes a parameter's
 *      value to the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be written.  
 *
 *      Note: This function does not take values from the parameter
 *            cache and it does not update the parameter cache.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      p_ref - contains  a reference (id or offset) of the parameter 
 *                  to be read and the sub-index of the (array or 
 *                  record) member.
 *      cm_value_listp - The requested value(s).
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_param_unload(ENV_INFO *env_info, const P_REF *p_ref, COMM_VALUE_LIST *cm_value_listp)
{
    DEVICE_HANDLE           device_handle ;
    int                     r_code = CM_SUCCESS ;
    int                     po ;
    NET_TYPE                net_type ;
    APP_INFO                *app_info ;
    DDI_BLOCK_SPECIFIER     block_spec ;
    DDI_PARAM_SPECIFIER     param_spec ;
    DDI_PARAM_INFO          param_info;

    app_info = env_info->app_info;

    ASSERT_RET(env_info && p_ref && cm_value_listp, CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    net_type = ADT_NET_TYPE(device_handle) ;

    if ((net_type == NT_A || net_type == NT_A_SIM) &&
            (p_ref->type == PC_ITEM_ID_REF)) {
        block_spec.type = DDI_BLOCK_HANDLE ;
        block_spec.block.handle = env_info->block_handle ;
        param_spec.type = DDI_PS_ITEM_ID ;
        param_spec.item.id = p_ref->id ;
        r_code = ddi_get_param_info(&block_spec, &param_spec, &param_info) ;
        po = (int)(param_info.op_index - ABT_A_OP_INDEX(env_info->block_handle)) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
    }
    else {
        po = p_ref->po ;
    }

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Write a parameter to a network
             */

            ASSERT_RET(param_unload_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*param_unload_fn_ptr)(env_info, po,
                    p_ref->subindex, cm_value_listp);
            break ;

        case NT_ETHER:

            /*
             *  Put code here to read an object's value from
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             return(CM_NO_COMM); /* For now return an error. */

        case NT_CONTROL:

            /*
             *  Put code here to read an object's value from
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            return(CM_NO_COMM); /* For now return an error. */


        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_create_block
 *
 *  ShortDesc:  Remotely create a block on a device.
 *
 *  Description:
 *      Issue a request to the desired device to create a block.
 *      If the request is a success, open a block using the
 *      block's network address and store the assigned tag
 *      into the active block table.
 *
 *  Inputs:
 *      device_handle - unique identifier of the device to get
 *                      block created.
 *      substation -    the sub-station address of the location
 *                      to create the block.
 *      block_id -      The item id of the type of block to create
 *      tag -           The tag to give the newly created block.
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_create_block(DEVICE_HANDLE device_handle, OBJECT_INDEX substation, 
        ITEM_ID block_id, char *tag, ENV_INFO *env_info)
{

    int             r_code = CM_SUCCESS ;    /* return from block creation */
    BLOCK_HANDLE    block_handle ;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    ASSERT_RET(tag && env_info, CM_BAD_POINTER) ;
    ASSERT_RET(block_id, CM_COMM_ERR) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE) ;
    }

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Create a block on a type A device
             */

            r_code = CM_BAD_DEVICE_HANDLE ;
            break;

        case NT_ETHER:

            r_code = CM_NO_COMM; /* For now return an error. */
            break; 

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = CM_NO_COMM;
            break;

        default:

            r_code = (CM_NO_COMM);
            break;
    }

    if (r_code != CM_SUCCESS) {
        return(r_code) ;
    }

    r_code = ct_addr_open(device_handle, substation, &block_handle) ;
    if (r_code != CM_SUCCESS) {
        return(r_code) ;
    }
    SET_ABT_TAG(env_info->block_handle, tag) ;
    return(CM_SUCCESS) ;
}


/***********************************************************************
 *
 *  Name:  cr_delete_block
 *
 *  ShortDesc:  Remotely delete a block on a device.
 *
 *  Description:
 *      Issue a request to the delete a block.
 *      If the request is a success, remove the active block 
 *      using the block's handle.
 *
 *  Inputs:
 *      env_info - contains the block handle of the block to delete.
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_delete_block(ENV_INFO *env_info)
{
    int             r_code = CM_SUCCESS ;/* return from block delete */ 
    DEVICE_HANDLE   device_handle ;         /* local device handle var */
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;  

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check to make sure that the block handle is valid.
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle),
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    ASSERT_RET(VALID_DEVICE_HANDLE(device_handle),
            CM_BAD_DEVICE_HANDLE) ;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Delete a block on a device
             */

            r_code = (CM_BAD_DEVICE_HANDLE) ;
            break;

        case NT_ETHER:

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = CM_NO_COMM;
            break;
    }

    if (r_code != CM_SUCCESS) {
        return(r_code) ;
    }

    r_code = ct_block_close(env_info->block_handle) ;
    return(r_code) ;
}


/***********************************************************************
 *
 *  Name:  cr_sm_ident
 *
 *  ShortDesc:  Get the unique identifier from a remote device
 *              using its device address.
 *
 *  Description:
 *              The function cr_sm_ident takes  a device address,
 *              and issues a request to a get the unique
 *              dentifier of the remote device.
 *
 *  Inputs:
 *      dev_addr            - the address of device on the network
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *      sm_identify_cnf - the network device identifier
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_sm_ident(
            ENV_INFO *env_info, 
            unsigned short dev_addr, 
            CR_SM_IDENTIFY_CNF *sm_identify_cnf
)
{
    int             r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    ASSERT_RET(env_info && sm_identify_cnf, CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Identify the device
     */
    if (sm_identify_fn_ptr != NULL) {
          r_code = (*sm_identify_fn_ptr)(
            env_info, 
            dev_addr,
            sm_identify_cnf
            );
    }
            
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_ident
 *
 *  ShortDesc:  Get the unique identifier from a remote device
 *              using its polling address.
 *
 *  Description:
 *              The function cr_ident takes  a network_handle,
 *              station, and issues a request to a get the unique
 *              dentifier of the remote device.
 *
 *  Inputs:
 *      network_handle - the unique identifier of the network
 *                  that contains the device.
 *      station         - the address of device on the network
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *      identify_cnf - the network device identifier
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_ident(NETWORK_HANDLE network_handle, ENV_INFO *env_info,
    const IDENTIFY_CNF *identify_cnf)
{
    int             r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;


    ASSERT_RET(env_info && identify_cnf, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check to make sure that the network handle is valid.
     */

    ASSERT_RET(network_handle >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (NT_NET_TYPE(network_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Identify a block on a type A device
             */

            r_code = (CM_NO_COMM) ;
            break;

        case NT_ETHER:

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_CONTROL:

            /*
             *  Identify a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = CM_NO_COMM;
            break;
    }
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_read_tag
 *
 *  ShortDesc:  Read the tag of a remote field device.
 *
 *  Description:
 *              The function cr_read_tag takes env_info ptr
 *              and issues a request to a read the tag of the
 *              remote block. The tag is also stored in the
 *              active block table.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_read_tag(ENV_INFO *env_info)
{
    DEVICE_HANDLE   device_handle ;
    int             r_code = CM_SUCCESS ;
    char            tag[8];
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    ASSERT_RET(env_info,  CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Read a tag on a device
             */
            r_code = CM_BAD_DEVICE_HANDLE;
            break;

        case NT_ETHER:

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Read a tag on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    /*
     * Check to see if the tag was successfully received
     * from the block.
     */

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     * Put the tag into the active block table
     */

    SET_ABT_TAG(env_info->block_handle, tag) ;
    return(CM_SUCCESS) ;
}

/***********************************************************************
 *
 *  Name:  cr_assign_tag
 *
 *  ShortDesc:  Write the tag of a remote field device.
 *
 *  Description:
 *              The function cr_assign_tag takes env_info ptr, tag
 *              and issues a request to a write the tag of the
 *              remote block. 
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      sm_identify_cnf - the tag to write to the specified block
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_assign_tag(ENV_INFO *env_info, 
              unsigned short int dev_addr, 
              CR_SM_IDENTIFY_CNF *sm_identify_cnf
)
{
    int             r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;

    app_info = env_info->app_info;

    ASSERT_RET(env_info,  CM_BAD_POINTER) ;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

            
	/* Assign the new Tag to the device */
	if (sm_assign_tag_fn_ptr != NULL) {
		r_code = (*sm_assign_tag_fn_ptr)(
			env_info,
			(unsigned short)dev_addr,
			sm_identify_cnf
		);
	}
            
    return(r_code) ;
}

/***********************************************************************
 *
 *  Name:  cr_send_cmd
 *
 *  ShortDesc:  Send a command to a field device.
 *
 *  Description:
 *              The function cr_send_cmd takes env_info ptr, cmd num,
 *              and issues a request to a send the specified cmd to the
 *              remote block. 
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      cmd - the cmd that is sent to the remote device.
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_send_cmd(ENV_INFO *env_info, unsigned short cmd)
{
    DEVICE_HANDLE   device_handle ;
    int             r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;


    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    cmd = 0;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(VALID_BLOCK_HANDLE(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    device_handle = ABT_ADT_OFFSET(env_info->block_handle) ;

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Create a block on a device
             */

            r_code = (CM_NO_COMM) ;
            break;

        case NT_ETHER:

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    /*
     * Added the below assignment to remove the flexelint warning on unused
     * variable cmd.
     */
    device_handle = ++cmd;

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_set_poll_addr
 *
 *  ShortDesc:  Set the polling address of a block.
 *
 *  Description:
 *              The function cr_set_poll_addr takes PD Tag as 
 *              input and changes its polling address (substation address).
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      poll_addr - the polling address that the block will
 *                  be set to following the request.
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_set_poll_addr(ENV_INFO *env_info, 
                 int new_dev_addr,
                 char *pd_tag 
)
{
    int r_code = CM_SUCCESS ;    /* local return status variable */
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

	/*
	 * Set the poll address on remote device
	 */
	if (sm_set_node_fn_ptr != NULL) {
		r_code = (*sm_set_node_fn_ptr)(
			env_info,
			(unsigned short)new_dev_addr,
			pd_tag
		);
	}

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_clear_poll_addr
 *
 *  ShortDesc:  Clear the polling address of a block.
 *
 *  Description:
 *              The function cr_clear_poll_addr takes block a block and
 *              clears its polling address (substation address).
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      poll_addr - the polling address that the block will
 *                  be set to following the request.
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_clear_poll_addr(ENV_INFO *env_info, 
                   int dev_addr,
                   CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
    int r_code = CM_SUCCESS ;    /* local return status variable */
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
    * Clear the poll address on remote device
    */
	if (sm_clr_node_fn_ptr != NULL) {
		r_code = (*sm_clr_node_fn_ptr)(
			env_info,
			(unsigned short)dev_addr,
			sm_identify_cnf
		);
	}

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_clear_tag
 *
 *  ShortDesc:  Clears the Physical Device tag of device.
 *
 *  Description:
 *              The function cr_clear_tag takes pd_tag, dev_id and
 *              node address of the device as the input and
 *              clears PD Tag of the device.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      poll_addr - the polling address that the block will
 *                  be set to following the request.
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_clear_tag(ENV_INFO *env_info, 
                   int dev_addr,
                   CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
    int r_code = CM_SUCCESS ;    /* local return status variable */
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

	/*
	 * Clear the poll address on remote device
	 */
	if (sm_clr_tag_fn_ptr != NULL) {
		r_code = (*sm_clr_tag_fn_ptr)(
			env_info,
			(unsigned short)dev_addr,
			sm_identify_cnf
		);
	}

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_initiate_comm
 *
 *  ShortDesc:   Initiate communication with a device.
 *
 *  Description:
 *      The cr_initiate_comm function takes a device handle and
 *      initiates communication with the corresponding device.
 *      This function will put device specific information into the 
 *      Active Device Table.
 *
 *  Inputs:
 *      device_handle - the handle of the device that communication
 *                      is to be initiated with.
 *
 *  Outputs:
 *      env_info - Returns any communication errors and returns 
 *                  response codes (application errors and warnings).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_initiate_comm(DEVICE_HANDLE device_handle, ENV_INFO *env_info)
{
    APP_INFO        *app_info ;
    int             r_code;
    CREF            ddod_cref ;
    CREF            mib_cref ;
    CREF            opod_cref ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to initiate communication with
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Put code here to initiate communication with
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_A:
        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             *  Call the Simulator to establish the CREFs.
             */

            ASSERT_RET(initiate_comm_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*initiate_comm_fn_ptr)(device_handle, &opod_cref,
                &ddod_cref, &mib_cref, env_info);

            if (r_code != CM_SUCCESS) {
                return(r_code);
            }

            SET_ADT_A_OPOD_CREF(device_handle, opod_cref);
            SET_ADT_A_DDOD_CREF(device_handle, ddod_cref);
            SET_ADT_A_MIB_CREF(device_handle, mib_cref);

            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_terminate_comm
 *
 *  ShortDesc:  Terminate communication with a device. 
 *
 *  Description:
 *      The cr_terminate_comm function takes a device handle and
 *      terminates communication with the corresponding device.
 *      If the function will remove device specific information
 *      from the Active Device Table.
 *
 *  Inputs:
 *      device_handle - the identifier of the device to
 *                      start the communication session.
 *
 *  Outputs:
 *      env_info -      contains the communication error and
 *                      response code of the initiate comm service
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_terminate_comm(DEVICE_HANDLE device_handle, ENV_INFO *env_info)
{
    int             r_code;
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0 ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    if (!VALID_DEVICE_HANDLE(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    switch (ADT_NET_TYPE(device_handle)) {

        case NT_ETHER:

            /*
             *  Put code here to terminate communication with
             *  a device on an Ethernet Network.  Use the specified
             *  network route if necessary.
             */

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Put code here to terminate communication with
             *  a device on a Control Network.  Use the specified
             *  network route if necessary.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_A:
        case NT_A_SIM:

            /*
             *  Terminate communication with a device on a Network.
             */

            ASSERT_RET(terminate_comm_fn_ptr != NULL, CM_NO_COMM);
            r_code = (*terminate_comm_fn_ptr)(device_handle, env_info);
            if (r_code != SUCCESS) {
                return(r_code);
            }

            SET_ADT_A_OPOD_CREF(device_handle, -1) ;
            SET_ADT_A_DDOD_CREF(device_handle, -1) ;
            SET_ADT_A_MIB_CREF(device_handle, -1) ;

            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_find_block
 *
 *  ShortDesc:  Find a block with the specified tag.
 *
 *  Description:
 *
 *  Inputs:
 *      tag - the tag of the block that is sought.
 *
 *  Outputs:
 *      find_cnf - the corresponding Find Confirm.
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/

int
cr_find_block(char *block_tag, FIND_CNF *find_cnf, ENV_INFO *env_info)
{
    NETWORK_HANDLE      network_handle;
    int                 r_code = CM_SUCCESS;
    FIND_CNF_A          *findcnf;
    APP_INFO            *app_info ;

    ASSERT_RET(block_tag && find_cnf && env_info, CM_BAD_POINTER);

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Go through each Network.
     */

    for (network_handle = 0; network_handle < network_tbl.count;
            network_handle ++) {

        /*
         *  Check the type of the network.
         */
         
        switch (NT_NET_TYPE(network_handle)) {

            case NT_ETHER:

                /*
                 *  Put code here for finding a block on an Ethernet
                 *  Network.  Use the specified network route
                 *  if necessary.
                 */

                break;

            case NT_CONTROL:

                /*
                 *  Put code here for finding a block on a Control Network.
                 *  Use the specified network route if necessary.
                 */

                break;


            case NT_A:
            case NT_A_SIM:
            case NT_OFF_LINE: //LNT

                /*
                 *  Call the Simulator to find the block.
                 */

                (find_cnf)->type = NT_NET_TYPE(network_handle) ;
                ASSERT_RET(find_block_fn_ptr != NULL, CM_NO_COMM);
                r_code = (*find_block_fn_ptr)(block_tag, network_handle,
                    &findcnf, env_info);

                if (r_code == CM_SUCCESS) {
                    (void)memcpy((char *)&((find_cnf)->find_cnf.find_cnf_a),
                            (char *)findcnf, sizeof(FIND_CNF_A)) ;
                    return(CM_SUCCESS);
                }
                break;

            default:

                break;
        }

    }

    return(CM_BLOCK_NOT_FOUND);
}

/***********************************************************************
 *
 *  Name:  cr_wr_local_vcr
 *
 *  ShortDesc:  Write the local CR 
 *
 *  Description:
 *              The function cr_wr_local_vcr takes CR number as 
 *              input and writes into local address of NMA in host
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      cr_no - The CR reference number to write NMA VCR configuration
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_wr_local_vcr(ENV_INFO *env_info, 
                 NETWORK_HANDLE network_handle, 
                 CM_NMA_VCR_LOAD* nma_vcr_load 
)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the network handle is valid
     */

    ASSERT_RET(network_handle >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (NT_NET_TYPE(network_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Write the VCR congigurations to local address 
             */
           
            if (nm_wr_local_vcr_fn_ptr != NULL) {
                r_code = (*nm_wr_local_vcr_fn_ptr)(
                    env_info, 
                    network_handle, 
                    nma_vcr_load
                );
            }
            break;

        case NT_ETHER:

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_set_appl_state_busy
 *
 *  ShortDesc:  Set the Application state to busy
 *
 *  Description:
 *              The function cr_wr_local_vcr takes CR number as
 *              input and writes into local address of NMA in host
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      cr_no - The CR reference number to write NMA VCR configuration
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_set_appl_state_busy(ENV_INFO *env_info, int *curr_state)
{
    int r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Write the VCR congigurations to local address
     */

    if (cm_set_appl_state_fn_ptr != NULL) {
           r_code = (*cm_set_appl_state_fn_ptr)(
                env_info, curr_state, APPL_BUSY
           );
    }


    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_set_appl_state_busy
 *
 *  ShortDesc:  Set the Application state to busy
 *
 *  Description:
 *              The function cr_wr_local_vcr takes CR number as
 *              input and writes into local address of NMA in host
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      cr_no - The CR reference number to write NMA VCR configuration
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_set_appl_state_normal(ENV_INFO *env_info)
{
    int r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Write the VCR congigurations to local address
     */

    if (cm_set_appl_state_fn_ptr != NULL) {
           r_code = (*cm_set_appl_state_fn_ptr)(
                env_info, NULL, APPL_NORMAL
           );
    }


    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cr_fms_initiate
 *
 *  ShortDesc:  Initiates the Feild Message Service on CR 
 *
 *  Description:
 *              The function cr_fms_initiate takes CR number as 
 *              input and initiates the FMS service on the CR
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      nma_vcr_load - The CR structure data
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_fms_initiate(ENV_INFO *env_info, 
                 NETWORK_HANDLE network_handle, 
                 CM_NMA_VCR_LOAD* nma_vcr_load 
)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the network handle is valid
     */

    ASSERT_RET(network_handle >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (NT_NET_TYPE(network_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Write the VCR congigurations to local address 
             */
           
            if (nm_fms_initiate_fn_ptr != NULL) {
                r_code = (*nm_fms_initiate_fn_ptr)(
                    env_info, 
                    network_handle, 
                    nma_vcr_load
                );
            }
            break;

        case NT_ETHER:

             r_code = (CM_NO_COMM); /* For now return an error. */
             break;

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = (CM_NO_COMM);
            break;
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_get_od
 *
 *  ShortDesc:  Get OD on CR 
 *
 *  Description:
 *              The function cr_get_od takes CR number as 
 *              input and Gets the Obect Description on the CR
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      nma_vcr_load - The CR structure data
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_get_od(ENV_INFO *env_info, 
                 NETWORK_HANDLE network_handle, 
                 CM_NMA_VCR_LOAD* nma_vcr_load 
)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Check to make sure that the network handle is valid
     */

    ASSERT_RET(network_handle >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    switch (NT_NET_TYPE(network_handle)) {

        case NT_A:
        case NT_A_SIM:

            /*
             * Write the VCR congigurations to local address 
             */
           
            if (nm_get_od_fn_ptr != NULL) {
                r_code = (*nm_get_od_fn_ptr)(
                    env_info, 
                    network_handle, 
                    nma_vcr_load
                );
            }
            break;

        case NT_ETHER:

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        case NT_CONTROL:

            /*
             *  Create a block on a remote Control System device.
             */

            r_code = (CM_NO_COMM); /* For now return an error. */
            break;

        default:
            r_code = CM_NO_COMM;
            break;
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_dev_boot
 *
 *  ShortDesc: Write value in Boot operational class of the device  
 *
 *  Description:
 *          This function calls nm_dev_boot_fn_ptr() to write value in
 *          remote device boot operational class.
 *          
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      NETWORK_HANDLE - network handle of the connected device
 *      index - Index of remote device boot operational class
 *      value - To be written in remote device boot operational class
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors: Shivaji Tikkireddy
 *
 **********************************************************************/

int
cr_dev_boot(ENV_INFO *env_info, NETWORK_HANDLE nh,unsigned short index, int value)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *) env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;
    
    /*
     * Check to make sure that the network handle is valid
     */

    ASSERT_RET(nh >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    if (nm_dev_boot_fn_ptr != NULL) {
        
        r_code = (*nm_dev_boot_fn_ptr)(
                    env_info, nh, index, value);
        }
           
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_nma_read
 *
 *  ShortDesc: Read NM objects  
 *
 *  Description:
 *          Read NM objects of Host or Remote device using index.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      NETWORK_HANDLE - Network Handle of the connected device.
 *      index - index of the NM object.
 *      service - enum value of the NM object.
 *      device_select - Host or Remote Device.
 *       
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:Shivaji Tikkireddy
 *
 **********************************************************************/

int
cr_nma_read(ENV_INFO *env_info, NETWORK_HANDLE nh,
                unsigned short idx, int service, int device_select
)
{
    int r_code = CM_SUCCESS;    
    APP_INFO        *app_info;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;
    
    /*
     * Check to make sure that the network handle is valid
     */

    ASSERT_RET(nh >= 0, CM_COMM_ERR) ;

    /*
     *  Check which type of network the device is connected to.
     */

    if (nm_read_fn_ptr != NULL) {
        r_code = (*nm_read_fn_ptr)(
                    env_info, nh, idx, service, device_select);
    }
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_cm_exit
 *
 *  ShortDesc:  close the communication Manager  
 *
 *  Description:
 *              The function cr_cm_exit calls fbk_exit to close
 *              the communication manger and resetting the FBK board
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_cm_exit(ENV_INFO *env_info, unsigned char hm)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;
    NET_TYPE		net_type;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *) env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    if(hm == CMM_OFF_LINE)
    	net_type = NT_OFF_LINE;
    else
    	net_type = NT_A;

    if (cm_exit_fn_ptr != NULL) {
        r_code = (*cm_exit_fn_ptr)(
             env_info, net_type
        );
    }
    else {
        return(CM_NO_COMM); 
    }
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_cm_firmware_download
 *
 *  ShortDesc:  Download Firmware  
 *
 *  Description:
 *              The function cr_cm_firmware_download calls 
 *              fbkhost_firmware_download to downlaod the firmware
 *              into the FBK2 Hardware
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      path - Path to firmware
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_cm_firmware_download(ENV_INFO *env_info, char *path)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;


    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    if (nm_get_od_fn_ptr != NULL) {
        r_code = (*cm_firmware_download_fn_ptr)(
             env_info,
             path 
        );
    }
    else {
        return(CM_NO_COMM); 
    }
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_cm_get_host_info
 *
 *  ShortDesc:  Get host FBK board information  
 *
 *  Description:
 *              The function cr_get_host_info calls 
 *              fbkhost_get_device_info to get the FBK board 
 *              information.
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      host_info - pointer to host information structure
 *
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *      host_info - Information about the FBK board.
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_cm_get_host_info(ENV_INFO *env_info, HOST_INFO *host_info,
		HOST_CONFIG *hc)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER);

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    if (cm_get_host_info_fn_ptr != NULL) {
        r_code = (*cm_get_host_info_fn_ptr)(
             env_info,
             host_info,
             hc
        );
    }
    else {
        return(CM_NO_COMM); 
    }
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cr_cm_reset_fbk
 *
 *  ShortDesc:  Reset the FBK Board  
 *
 *  Description:
 *              The function cr_cm_reset_fbk does the  
 *              FBAP and MIB CR abort and re-initiates it.
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *  Outputs:
 *      env_info -  contains the response code and communication
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_cm_reset_fbk(ENV_INFO *env_info, NETWORK_HANDLE nh, HOST_CONFIG *hc)
{
    int r_code = CM_SUCCESS ;    
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER);

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    if (cm_reset_fbk_fn_ptr != NULL) {
        r_code = (*cm_reset_fbk_fn_ptr)(
             env_info, nh, hc
        );
    }
    else {
        return(CM_NO_COMM); 
    }
    
    return(r_code);
}


int
offline_initialize(void)
{
    NETWORK_HANDLE  network_handle;
    int             r_code = CM_SUCCESS;

    for (network_handle = 0; network_handle < network_tbl.count;
    		network_handle ++)
    {
         if (offline_init_fn_ptr != NULL) {
        	 r_code = (*offline_init_fn_ptr)(network_handle);
             if (r_code != CM_SUCCESS) {
                 return(r_code);
             }
         }
    }

    return(r_code);
}

/***********************************************************************
 *
 *  Name:   cr_config_stack
 *
 *  ShortDesc:  Configure the FF stack.
 *
 *  Description:
 *      The cr_config_stack function does whatever is necessary to initialize
 *      all communication.  In the case of a PC with communication
 *      boards, this process would initialize all the boards.
 *
 *  Inputs:
 *      cm_mode - the Connection Manager mode to be used:
 *             1. On-Line (for communication with devices).
 *                  (CMM_ON_LINE)
 *             2. Off-Line (for no communication with devices).
 *                  (CMM_OFF_LINE)
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_NO_MEMORY.
 *      CM_NO_COMM.
 *      Return values from init_network_table function.
 *      Return values from ...
 *      Return values from initialize function.
 *      Return values from init_network function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cr_config_stack(ENV_INFO *env_info, CM_NMA_INFO *nma_info, HOST_CONFIG *host_config)
{
    int             r_code;

    /*
     *  Perform configureation of stack.
     */


    if (config_stack_fn_ptr != NULL) {
        r_code = (*config_stack_fn_ptr)(env_info, nma_info,  host_config) ;
        if (r_code != CM_SUCCESS) {
            return(r_code) ;
        }
    }

    return(r_code);
}

int cr_get_stack_config(ENV_INFO *env_info, CM_NMA_INFO *nma_info)
{
    int r_code;

    if(get_stack_config_fn_ptr != NULL) {
        r_code = (*get_stack_config_fn_ptr) (env_info, nma_info);
        if(r_code != CM_SUCCESS) {
            return r_code;
        }
    }

    return r_code;
}


int cr_get_actual_index(unsigned short *index, int *si, unsigned short *cr)
{
    int r_code;

    if(cm_get_actual_index_fn_ptr != NULL) {
        r_code = (*cm_get_actual_index_fn_ptr) (index, si, cr);
        if(r_code != CM_SUCCESS) {
            return r_code;
        }
    }

    return r_code;
}

int cr_dds_bypass_read(ENV_INFO *env_info, unsigned short index, int si,
		unsigned short cr, void *val)
{
    int r_code;

    if(cm_dds_bypass_read_fn_ptr != NULL) {
        r_code = (*cm_dds_bypass_read_fn_ptr) (env_info, index, si, cr, val);
        if(r_code != CM_SUCCESS) {
            return r_code;
        }
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name:  cr_abort_cr
 *
 *  ShortDesc:  Disconnect the communication by aborting CR's
 *
 *  Description:
 *              The function cr_abort_cr ABORT command to disconnect the
 *              communication between host and device.
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      mib_cr - MIB communication reference number.
 *      fbap_cr - FBAP Communication reference number.
 *
 *  Outputs:
 *		None
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
cr_abort_cr(ENV_INFO *env_info, unsigned short mib_cr, unsigned short fbap_cr)
{
    int r_code = CM_SUCCESS ;
    APP_INFO        *app_info ;

    ASSERT_RET(env_info, CM_BAD_POINTER);

    app_info = (APP_INFO *)env_info->app_info;
    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    if (cm_abort_cr_fn_ptr != NULL) {
        r_code = (*cm_abort_cr_fn_ptr)(
             env_info,
             mib_cr,
             fbap_cr
        );
    }
    else {
        return(CM_NO_COMM);
    }

    return(r_code);
}

