/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_tbl.c - Connection Manager Tables Module
 *
 *	This file contains all functions pertaining to the Connection
 *	(Active) Tables.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cm_lib.h"
#include "cm_loc.h"
#include "ddi_lib.h"
#include "int_diff.h"

/*
 *	Function pointers for removing DDS Tables and Application
 *	Information from the Active Tables.
 */

static int (*remove_dd_dev_tbl_fn_ptr) P((DEVICE_HANDLE)) = 0;
static int (*remove_dd_blk_tbl_fn_ptr) P((BLOCK_HANDLE)) = 0;
static int (*remove_blk_app_info_fn_ptr) P((BLOCK_HANDLE)) = 0;
static int (*remove_dev_app_info_fn_ptr) P((DEVICE_HANDLE)) = 0;
static int (*remove_dev_type_app_info_fn_ptr) P((DEVICE_TYPE_HANDLE)) = 0;


/***********************************************************************
 *
 *	Name:  ct_init
 *
 *	ShortDesc:  Initialize the Connection Table Manager.
 *
 *	Description:
 *		The ct_init function initializes the Connection (Active)
 *		Tables by setting each table count to zero and initializing
 *		each table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ct_init()
{
	/*
	 *	Set each table count to zero and initialize each table
	 *	list pointer.
	 */

	active_blk_tbl.count = 0;
	active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM **) malloc(
			sizeof(ACTIVE_BLK_TBL_ELEM **));
	if (!active_blk_tbl.list) {
		return(CM_NO_MEMORY);
	}

	active_dev_tbl.count = 0;
	active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM **) malloc(
			sizeof(ACTIVE_DEV_TBL_ELEM **));
	if (!active_dev_tbl.list) {
		return(CM_NO_MEMORY);
	}

	active_dev_type_tbl.count = 0;
	active_dev_type_tbl.list = (ACTIVE_DEV_TYPE_TBL_ELEM **) malloc(
			sizeof(ACTIVE_DEV_TYPE_TBL_ELEM **));
	if (!active_dev_type_tbl.list) {
		return(CM_NO_MEMORY);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_init_dds_tbl_fn_ptrs
 *
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The ct_init_dds_tbl_fn_ptrs function links the pointers of
 *		the DDS functions that the Connection Manager calls when any
 *		DDS tables need to be removed.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
ct_init_dds_tbl_fn_ptrs(const DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that remove DD device
	 *	and DD block tables (DDS Tables).
	 */

	remove_dd_dev_tbl_fn_ptr = dds_tbl_fn_ptrs->remove_dd_dev_tbl_fn_ptr;

	remove_dd_blk_tbl_fn_ptr = dds_tbl_fn_ptrs->remove_dd_blk_tbl_fn_ptr;
}


/***********************************************************************
 *
 *	Name:  ct_init_app_info_fn_ptrs
 *
 *	ShortDesc:  Initialize the Application information function
 *				pointers.
 *
 *	Description:
 *		The ct_init_app_info_fn_ptrs function links the pointers of
 *		the Application functions that the Connection Manager calls
 *		when any Application information needs to be removed.
 *
 *	Inputs:
 *		app_info_fn_ptrs - the necessary Application information
 *						   function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
ct_init_app_info_fn_ptrs(const APP_INFO_FN_PTRS *app_info_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that remove Application
	 *	information from the Active Block, Active Device, and Active
	 *	Device Type Tables.
	 */

	remove_blk_app_info_fn_ptr = app_info_fn_ptrs->remove_blk_app_info_fn_ptr;
	remove_dev_app_info_fn_ptr = app_info_fn_ptrs->remove_dev_app_info_fn_ptr;
	remove_dev_type_app_info_fn_ptr =
		app_info_fn_ptrs->remove_dev_type_app_info_fn_ptr;
}


/***********************************************************************
 *
 *	Name:  ct_substation_search
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				device handle and substation.
 *
 *	Description:
 *
 *	Inputs:
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The block handle of the matching Active Block Table element
 *		if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

BLOCK_HANDLE
ct_substation_search(DEVICE_HANDLE device_handle, int substation)
{

	BLOCK_HANDLE	block_handle;

	ASSERT_RET(VALID_DEVICE_HANDLE(device_handle), CM_BAD_DEVICE_HANDLE) ;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (block_handle = 0; block_handle < active_blk_tbl.count;
			block_handle ++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the device handle and substation of the element.
			 */

			if (device_handle == ABT_ADT_OFFSET(block_handle) &&
					(OBJECT_INDEX)substation ==
						ABT_A_OP_INDEX(block_handle)) {
				return(block_handle);
			}
		}
	}

	return(-1);
}


/***********************************************************************
 *
 *	Name:  ct_block_search
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				a tag.
 *
 *	Description:
 *		The ct_block_search function takes a block tag and searches
 *		through the valid (allocated) Active Block Table elements in
 *		the Active Block Table list, looking for the element that
 *		matches the tag.  If found, the corresponding block handle
 *		will be returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		tag - unique identifier of a block.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The block handle of the matching Active Block Table element
 *		if successful or -1 if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/


BLOCK_HANDLE
ct_block_search(const char *tag)
{

	BLOCK_HANDLE	block_handle;

	ASSERT_RET(tag, CM_BAD_POINTER) ;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (block_handle = 0; block_handle < active_blk_tbl.count;
			block_handle ++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the tag of the element.
			 */

            if (!strcmp(ABT_TAG(block_handle),tag)) {
				return(block_handle);
			}
		}
	}

	return(-1);
}
/***********************************************************************
 *
 *	Name:  ct_device_search
 *
 *	ShortDesc:  Search for a device in the Active Device Table using
 *				a network and station address.
 *
 *	Description:
 *		The ct_device_search function takes a network and station
 *		address and searches through the valid (allocated) Active
 *		Device Table elements in the Active Device Table list,
 *		looking for the element that matches the network and station
 *		address.  If found, the corresponding device handle will be
 *		returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		network - unique identifier of a device (along with station
 *				  address).
 *		station_address - unique identifier of a device (along with
 *						  network).
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device handle of the matching Active Device Table element
 *		if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static DEVICE_HANDLE 
ct_device_search(int network, UINT16 station_address)
{

	DEVICE_HANDLE	device_handle;

	/*
	 *	Go through each element in the Active Device Table.
	 */

	for (device_handle = 0; device_handle < active_dev_tbl.count;
			device_handle ++) {

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_tbl.list[device_handle]) {

			/*
			 *	Check the network and station address of the element.
			 */

			if ((ADT_NETWORK(device_handle) == network) &&
				(ADT_STATION_ADDRESS(device_handle) == station_address)) {
				return(device_handle);
			}
		}
	}

	return(-1);
}




/***********************************************************************
 *
 *	Name:  ct_device_type_search
 *
 *	ShortDesc:  Search for a device type in the Active Device Type
 *				Table using a DD device ID.
 *
 *	Description:
 *		The ct_device_type_search function takes a DD device ID and
 *		searches through the valid (allocated) Active Device Type
 *		Table elements in the Active Device Type Table list, looking
 *		for the element that matches the DD device ID.  If found, the
 *		corresponding device type handle will be returned.  Otherwise
 *		an error will be returned.
 *
 *	Inputs:
 *		dd_device_id - unique identifier of a device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device type handle of the matching Active Device Type
 *		Table element if successful or -1 if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static DEVICE_TYPE_HANDLE 
ct_device_type_search(DD_DEVICE_ID *dd_device_id)
{

	DEVICE_TYPE_HANDLE	device_type_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (device_type_handle = 0; device_type_handle <
			active_dev_type_tbl.count; device_type_handle ++) {

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_type_tbl.list[device_type_handle]) {

			/*
			 *	Check the DD device ID of the element.
			 */

			if (ADTT_DD_DEVICE_ID(device_type_handle)->
					ddid_manufacturer ==
					dd_device_id->ddid_manufacturer &&
					ADTT_DD_DEVICE_ID(device_type_handle)->
					ddid_device_type ==
					dd_device_id->ddid_device_type &&
					ADTT_DD_DEVICE_ID(device_type_handle)->
					ddid_device_rev ==
					dd_device_id->ddid_device_rev) {

				return(device_type_handle);
			}
		}
	}

	return(-1);
}


/***********************************************************************
 *
 *	Name:  ct_new_block
 *
 *	ShortDesc:  Create a new Active Block Table element.
 *
 *	Description:
 *		The ct_new_block function creates a new Active Block Table
 *		element either in the first unused (deallocated) element
 *		space or at the end of the existing Active Block Table list.
 *		The corresponding block handle will be returned unless there
 *		is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		block_handle - handle of the new Active Block Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ct_new_block(BLOCK_HANDLE *block_handle)
{

	BLOCK_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (temp_handle = 0; temp_handle < active_blk_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_blk_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Block Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_blk_tbl.count) {

		/*
		 *	Extend the Active Block Table.
		 */

		active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM **) realloc(
				(char *)active_blk_tbl.list,
				(unsigned int)(sizeof(ACTIVE_BLK_TBL_ELEM *) *
				(active_blk_tbl.count + 1)));

		if (!active_blk_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_blk_tbl.count ++;
		temp_handle = active_blk_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Block Table element where the block
	 *	handle is.
	 */

	active_blk_tbl.list[temp_handle] =
			(ACTIVE_BLK_TBL_ELEM *) calloc(1, sizeof(ACTIVE_BLK_TBL_ELEM));

	if (!active_blk_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}
	
	*block_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_new_device
 *
 *	ShortDesc:  Create a new Active Device Table element.
 *
 *	Description:
 *		The ct_new_device function creates a new Active Device Table
 *		element either in the first unused (deallocated) element space
 *		or at the end of the existing Active Device Table list.  The
 *		corresponding device handle will be returned unless there is
 *		an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_handle - handle of the new Active Device Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ct_new_device(DEVICE_HANDLE *device_handle)
{
	DEVICE_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Device Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_tbl.count;
			temp_handle ++) {
		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_tbl.count) {

		/*
		 *	Extend the Active Device Table.
		 */

		active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM **) realloc(
				(char *)active_dev_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TBL_ELEM *) *
				(active_dev_tbl.count + 1)));

		if (!active_dev_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_tbl.count ++;
		temp_handle = active_dev_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Table element where the device
	 *	handle is.
	 */

	active_dev_tbl.list[temp_handle] =
			(ACTIVE_DEV_TBL_ELEM *) calloc(1, sizeof(ACTIVE_DEV_TBL_ELEM));

	if (!active_dev_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_new_device_type
 *
 *	ShortDesc:  Create a new Active Device Type table element.
 *
 *	Description:
 *		The ct_new_device_type function creates a new Active Device
 *		Type Table element either in the first unused (deallocated)
 *		element space or at the end of the existing Active Device
 *		Type Table list.  The corresponding device type table handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_type_handle - handle of the new Active Device Type
 *							 Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ct_new_device_type(DEVICE_TYPE_HANDLE *device_type_handle)
{

	DEVICE_TYPE_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_type_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_type_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Type Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_type_tbl.count) {

		/*
		 *	Extend the Active Device Type Table.
		 */

		active_dev_type_tbl.list = (ACTIVE_DEV_TYPE_TBL_ELEM **) realloc(
				(char *)active_dev_type_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TYPE_TBL_ELEM *) *
				(active_dev_type_tbl.count + 1)));

		if (!active_dev_type_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_type_tbl.count ++;

		temp_handle = active_dev_type_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Type Table element where the
	 *	device type handle is.
	 */

	active_dev_type_tbl.list[temp_handle] =
			(ACTIVE_DEV_TYPE_TBL_ELEM *) calloc(1,
			sizeof(ACTIVE_DEV_TYPE_TBL_ELEM));

	if (!active_dev_type_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_type_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_remove_block
 *
 *	ShortDesc:  Remove a specific element of the Active Block Table.
 *
 *	Description:
 *		The ct_remove_block function removes a specific element of
 *		the Active Block Table.  Note that the element will be
 *		deallocated but the Active Block Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		block_handle - handle (offset) of the specific Active Block
 *					   Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static void
ct_remove_block(BLOCK_HANDLE block_handle)
{
	/*
	 *	Check if a tag exits.
	 */

	if (ABT_TAG(block_handle)) {
		free(ABT_TAG(block_handle));
	}

	/*
	 *	Free the Active Block Table element and null the element pointer.
	 */

	free((char *)active_blk_tbl.list[block_handle]);
	active_blk_tbl.list[block_handle] = 0;
}


/***********************************************************************
 *
 *	Name:  ct_remove_device
 *
 *	ShortDesc:  Remove a specific member of the Active Device Table.
 *
 *	Description:
 *		The ct_remove_device function removes a specific element of
 *		the Active Device Table.  Note that the element will be
 *		deallocated but the Active Device Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		device_handle - handle (offset) of the specific Active Device
 *						Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static void
ct_remove_device(DEVICE_HANDLE device_handle)
{

	/*
	 *	Free the Active Device Table element and null the element
	 *	pointer.
	 */

	free((char *)active_dev_tbl.list[device_handle]);
	active_dev_tbl.list[device_handle] = 0;
}


/***********************************************************************
 *
 *	Name:  ct_remove_device_type
 *
 *	ShortDesc:  Remove a specific member of the Active Device Type
 *				Table.
 *
 *	Description:
 *		The ct_remove_device_type function removes a specific element
 *		of the Active Device Type Table.  Note that the element will
 *		be deallocated but the Active Device Type Table list will not
 *		be reduced.
 *
 *	Inputs:
 *		device_type_handle - handle (offset) of the specific Active
 *							 Device Type Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static void
ct_remove_device_type(DEVICE_TYPE_HANDLE device_type_handle)
{

	/*
	 *	Free the Active Device Type Table element and null the
	 *	element pointer.
	 */

	free((char *)active_dev_type_tbl.list[device_type_handle]);
	active_dev_type_tbl.list[device_type_handle] = 0;
}

/***********************************************************************
 *
 *	Name:  ct_device_fill
 *
 *	ShortDesc:  Fill in the table entries for a device
 *
 *	Description:
 *				Open a new active device and initialize the active device
 *				with the values passed in with find_cnf.  Also, open a new
 *				active device type if necessary.
 *
 *	Inputs:
 *		find_cnf - the structure identifies a device.  The
 *				   structure is normally filled by either
 *				   find tag or identify service.
 *		device_address - the address of the identified device.
 *					It is saved into the connection tables
 *					for later use.
 *
 *	Outputs:
 *		device_handle - the corresponding device handle of the
 *						opened device.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_COMM.
 *		Return values from ct_new_device function.
 *		Return values from cr_initiate_comm function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

static int
ct_device_fill(const FIND_CNF *find_cnf, DEVICE_ADDRESS *device_address,
	DEVICE_HANDLE *device_handle)
{

	DEVICE_TYPE_HANDLE	device_type_handle ;
	int					r_code ;
	ENV_INFO			env_info ;
	APP_INFO			app_info ;
	DD_HANDLE			dd_handle ;
	DD_DEVICE_ID		dd_device_id ;
	NETWORK_HANDLE		network ;
	UINT16				stat_addr ;

	/*
	 *	Allocate a new Active Device Table element and initialize
	 *	its members.
	 */

	(void)device_address; /* Unused variable */

	(void)memset((char *)&env_info, 0, sizeof(ENV_INFO));
	(void)memset((char *)&app_info, 0, sizeof(APP_INFO));
	env_info.app_info = &app_info;

	r_code = ct_new_device(device_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	network = find_cnf->find_cnf.find_cnf_a.network ;
	stat_addr = find_cnf->find_cnf.find_cnf_a.station_address ;
	SET_ADT_NETWORK(*device_handle, network) ;
	SET_ADT_STATION_ADDRESS(*device_handle, stat_addr) ;

	SET_ADT_A_OPOD_CREF(*device_handle, -1) ;
	SET_ADT_A_DDOD_CREF(*device_handle, -1) ;
	SET_ADT_A_MIB_CREF(*device_handle, -1) ;

	(void)memcpy((char *)&dd_device_id,
			(char *)&find_cnf->find_cnf.find_cnf_a.dd_device_id,
			sizeof(DD_DEVICE_ID)) ;

	SET_ADT_OPROD_HANDLE(*device_handle, -1);
	SET_ADT_APP_INFO(*device_handle, 0);
	SET_ADT_USAGE(*device_handle, 1);

	/*
	 *	Initiate any communication with the device.
	 */

	//if (ADT_NET_TYPE(*device_handle) != NT_OFF_LINE) {  //LNT
		r_code = cr_initiate_comm(*device_handle, &env_info) ;
		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}
	//}

	/*
	 *	Search the Active Device Type Table for the DD device ID.
	 */

	device_type_handle = ct_device_type_search(&dd_device_id);

	/*
	 *	Check if the device type is already in use.
	 */

	if (device_type_handle >= 0) {

		/*
		 *	Increment the device type's usage, set the Active
		 *	Device Type Table offset of the device, and return.
		 */

		SET_ADTT_USAGE(device_type_handle,
				(ADTT_USAGE(device_type_handle) + 1)) ;
		SET_ADT_ADTT_OFFSET(*device_handle, device_type_handle) ;
		return(CM_SUCCESS) ;
	}

	/*
	 *	Allocate a new Active Device Type Table element and
	 *	initialize its members.
	 */

	r_code = ct_new_device_type(&device_type_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	SET_ADTT_DD_DEVICE_ID(device_type_handle, &dd_device_id) ;
	dd_handle.dd_handle_type = 0 ;
	dd_handle.dd_handle_number = -1 ;
	SET_ADTT_DD_HANDLE(device_type_handle, &dd_handle) ;
	SET_ADTT_DD_DEV_TBLS(device_type_handle, 0) ;
	SET_ADTT_APP_INFO(device_type_handle, 0) ;
	SET_ADTT_USAGE(device_type_handle, 1) ;
	SET_ADT_ADTT_OFFSET(*device_handle, device_type_handle) ;

	return(CM_SUCCESS) ;
}


/***********************************************************************
 *
 *	Name:  ct_block_fill
 *
 *	ShortDesc:  Fill in the table entries for a device
 *
 *	Description:
 *				Open a new active block and initialize
 *				the active block with the values passed
 *				in with find_cnf and tag.  Also, open a new
 *				active device type if necessary.
 *
 *	Inputs:
 *			tag - tag of the block to fill
 *			find_cnf - information to initialize the block with
 *
 *	Outputs:
 *		block_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_new_block function.
 *		Return values from ct_device_fill function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int
ct_block_fill(char *tag, FIND_CNF *find_cnf, BLOCK_HANDLE *block_handle)
{

	DEVICE_HANDLE		device_handle;
	int					r_code;
	DEVICE_ADDRESS		device_address ;
	NETWORK_HANDLE		network ;
	UINT16				stat_addr ;

	ASSERT_RET(tag && find_cnf && block_handle, CM_BAD_POINTER) ;

	/*
	 *	Allocate a new Active Block Table element and initialize
	 *	its members.
	 */

	(void)memset( (char *)&device_address, 0, sizeof(device_address) );

	r_code = ct_new_block(block_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	network = find_cnf->find_cnf.find_cnf_a.network ;
	stat_addr = find_cnf->find_cnf.find_cnf_a.station_address;
	SET_ABT_A_OP_INDEX(*block_handle, find_cnf->find_cnf.find_cnf_a.op_index );

    SET_ABT_TAG(*block_handle, (char *) strdup(tag));
	SET_ABT_PARAM_COUNT(*block_handle, (unsigned)-1);
	SET_ABT_DD_BLK_ID(*block_handle, 0);
	SET_ABT_DD_BLK_TBL_OFFSET(*block_handle, -1);
	SET_ABT_APP_INFO(*block_handle, 0);
    // CPMHACK - Initialize pc info element within active block table
	SET_ABT_PC_INFO(*block_handle, (void *) NULL);
	SET_ABT_USAGE(*block_handle, 1);

	/*
	 *	Search the Active Device Table for the network and
	 *	station address.
	 */

	device_handle = ct_device_search(network, stat_addr) ;

	/*
	 *	Check if the device is already in use.
	 */

	if (device_handle >= 0) {

		/*
		 *	Increment the device's usage, set the Active Device
		 *	Table offset of the block, and return.
		 */

		SET_ADT_USAGE(device_handle, (ADT_USAGE(device_handle) + 1));
		SET_ABT_ADT_OFFSET(*block_handle, device_handle);
		return(CM_SUCCESS);
	}

	r_code = ct_device_fill(find_cnf, &device_address, &device_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	SET_ABT_ADT_OFFSET(*block_handle, device_handle) ;
	return(CM_SUCCESS) ;
}




/***********************************************************************
 *
 *	Name:  ct_block_open
 *
 *	ShortDesc:  Open a session with a tagged block.
 *
 *	Description:
 *		The ct_block_open function takes a block tag and opens a new
 *		session with a block that matches the tag.  A system-wide tag
 *		search may be used if the block was not found in the Active
 *		Block Table.  If successful, the corresponding block handle 
 *		will be returned.  If necessary, the a new active block will be
 *		allocated and filled.  Otherwise, an error code will be 
 *		returned.
 *
 *	Inputs:
 *		tag - tag of the block to be opened.
 *
 *	Outputs:
 *		block_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from cr_find_block function.
 *		Return values from ct_block_fill function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int
ct_block_open(char *tag, BLOCK_HANDLE *block_handle)
{

	FIND_CNF			find_cnf;
	ENV_INFO			env_info;
	APP_INFO			app_info;
	int					r_code;

	(void)memset((char *)&env_info, 0, sizeof(env_info));
	(void)memset((char *)&app_info, 0, sizeof(app_info));
	env_info.app_info = &app_info;

	ASSERT_RET(tag && block_handle, CM_BAD_POINTER);
	 
	/*
	 *	Search the Active Block Table for the tag.
	 */

	*block_handle = ct_block_search(tag);

	/*
	 *	Check if the the block is already in use.
	 */

    if (*block_handle >= 0) {

        /*
         *	Increment the block's usage and return.
         */

        SET_ABT_USAGE(*block_handle, ABT_USAGE(*block_handle) + 1);
        return(CM_SUCCESS);
    }

	/*
	 *	Perform a system-wide tag search.
	 */

	r_code = cr_find_block(tag, &find_cnf, &env_info);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	r_code = ct_block_fill(tag, &find_cnf, block_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_addr_open
 *
 *	ShortDesc:  Open a session with an address of a block.
 *
 *	Description:
 *		The ct_addr_open function takes a device handle and
 *		a sub-station address and opens a new session with a block 
 *		that matches that address.  If successful, the corresponding 
 *		block handle will be returned.  If necessary, the a new 
 *		active block will be allocated and filled.  Otherwise, an 
 *		error code will be returned.
 *
 *	Inputs:
 *		device_handle - the unique identifier of the device to
 *					    connect to.
 *		substation - the address of the block (within the device)
 *					 to connect to.
 *
 *	Outputs:
 *		block_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_block_fill function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int
ct_addr_open(DEVICE_HANDLE device_handle, OBJECT_INDEX substation, 
		BLOCK_HANDLE *block_handle)
{

	FIND_CNF		find_cnf ;
	int				r_code ;
	 
	/*
	 *	Search the Active Block Table for the device handle and substation.
	 */

	ASSERT_RET(VALID_DEVICE_HANDLE(device_handle), CM_BAD_DEVICE_HANDLE) ;
	ASSERT_RET(block_handle, CM_BAD_POINTER) ;

	*block_handle = ct_substation_search(device_handle, substation) ;

	/*
	 *	Check if the the block is already in use.
	 */

	if (*block_handle >= 0) {

		/*
		 *	Increment the block's usage and return.
		 */

		SET_ABT_USAGE(*block_handle, ABT_USAGE(*block_handle) + 1) ;
		return(CM_SUCCESS) ;
	}

	/*
	 *  It's a new block, so zero it out and fill it in.
	 */

	(void)memset((char *)&find_cnf, 0, sizeof(FIND_CNF)) ;

	switch(ADT_NET_TYPE(device_handle)) {
	case NT_A:
	case NT_A_SIM:
		find_cnf.type = NT_A;
		find_cnf.find_cnf.find_cnf_a.op_index = substation ;
		find_cnf.find_cnf.find_cnf_a.network = 
				ADT_NETWORK(device_handle) ;
		find_cnf.find_cnf.find_cnf_a.station_address =
				ADT_STATION_ADDRESS(device_handle) ;
		break ;

	case NT_OFF_LINE:
		find_cnf.type = NT_OFF_LINE;
		find_cnf.find_cnf.find_cnf_a.op_index = substation ;
		find_cnf.find_cnf.find_cnf_a.network =
				ADT_NETWORK(device_handle) ;
		find_cnf.find_cnf.find_cnf_a.station_address =
				ADT_STATION_ADDRESS(device_handle) ;
		break;

	default:
		return(CM_NO_COMM) ;
	}

	r_code = ct_block_fill("", &find_cnf, block_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	return(CM_SUCCESS) ;
}




/***********************************************************************
 *
 *	Name:  ct_block_close
 *
 *	ShortDesc:  Close a previously opened session with a block.
 *
 *	Description:
 *		The ct_block_close function takes a block handle and closes
 *		a previously opened session with a block.  The function checks
 *		for the corresponding Active Block, Device, and Device Type
 *		elements that are no longer used, and deallocates them.
 *
 *	Inputs:
 *		block_handle - handle of the block to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_BLOCK_CLOSE.
 *		Return values from rod_close function.
 *		Return values from cr_terminate_comm function.
 *              CM_BAD_DD_HANDLE
 *              CRASH_RET(CM_BAD_DD_HANDLE);
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ct_block_close(BLOCK_HANDLE block_handle)
{

	DEVICE_HANDLE		device_handle;
	DEVICE_TYPE_HANDLE	device_type_handle;
	int					block_deallocate = 0;
	int					device_deallocate = 0;
	int					device_type_deallocate = 0;
	ENV_INFO			env_info ;
	APP_INFO			app_info ;
	int					r_code;

	ASSERT_RET(VALID_BLOCK_HANDLE(block_handle), CM_BAD_BLOCK_HANDLE) ;
	(void)memset((char *)&env_info,0,sizeof(ENV_INFO));
	(void)memset((char *)&app_info,0,sizeof(APP_INFO));
	env_info.app_info = &app_info;

	/*
	 *	Check to make sure that the block handle is valid, and
	 *	get the device handle and device type handle.
	 */

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	device_handle = ABT_ADT_OFFSET(block_handle);
	device_type_handle = ADT_ADTT_OFFSET(device_handle);

	/*
	 *	Check if the block has multiple sessions.
	 */

	if (ABT_USAGE(block_handle) > 1) {

		/*
		 *	Decrement the block's usage and return.
		 */

		SET_ABT_USAGE(block_handle,ABT_USAGE(block_handle) - 1);
		return(CM_SUCCESS);

	} else {

		/*
		 *	Set the flag to deallocate the block.
		 */

		block_deallocate = 1;

		/*
		 *	Check if the device has multiple sessions.
		 */

		if (ADT_USAGE(device_handle) > 1) {

			/*
			 *	Decrement the device's usage.
			 */

			SET_ADT_USAGE(device_handle,ADT_USAGE(device_handle) - 1);

		} else {

			/*
			 *	Set the flag to deallocate the device.
			 */

			device_deallocate = 1;

			/*
			 *	Check if the device type has multiple sessions.
			 */

			if (ADTT_USAGE(device_type_handle) > 1) {

				/*
				 *	Decrement the device type's usage.
				 */

				SET_ADTT_USAGE(device_type_handle,
						ADTT_USAGE(device_type_handle) - 1);

			} else {

				/*
				 *	Set the flag to deallocate the device type.
				 */

				 device_type_deallocate = 1;
			}
		}
	}

	/*
	 *	Check if the block is to be deallocated.
	 */

	if (block_deallocate) {

		/*
		 *	Remove any data connected to the Active Block Table and
		 *	deallocate the block.
		 */

		if (remove_dd_blk_tbl_fn_ptr != 0) {
			r_code = (*remove_dd_blk_tbl_fn_ptr)(block_handle);
			ASSERT_RET((!r_code), CM_BAD_BLOCK_CLOSE);
		}
		if (remove_blk_app_info_fn_ptr != 0) {
			r_code = (*remove_blk_app_info_fn_ptr)(block_handle);
			ASSERT_RET((!r_code), CM_BAD_BLOCK_CLOSE);
			ASSERT_DBG(!ABT_APP_INFO(block_handle));
            // CPMHACK - Check if pc info is cleared before deallocating 
            // the block
			ASSERT_DBG(!ABT_PC_INFO(block_handle));
		}
		ct_remove_block(block_handle);
	}

	/*
	 *	Check if the device is to be deallocated.
	 */

	if (device_deallocate) {

		/*
		 *	Close the OPROD of the device, terminate communication
		 *	with the device, remove any data connected to the Active
		 *	Device Table, and deallocate the device.
		 */

		if (VALID_ROD_HANDLE(ADT_OPROD_HANDLE(device_handle))) {
			r_code = rod_close(ADT_OPROD_HANDLE(device_handle));
			ASSERT_RET((r_code == CM_SUCCESS), r_code);

			SET_ADT_OPROD_HANDLE(device_handle, -1);
		}

		if (ADT_NET_TYPE(device_handle) != NT_OFF_LINE) {
			r_code = cr_terminate_comm(device_handle, &env_info);
			ASSERT_RET((r_code == CM_SUCCESS), r_code);
		}

		if (remove_dev_app_info_fn_ptr != 0) {
			r_code = (*remove_dev_app_info_fn_ptr)(device_handle);
			ASSERT_RET((!r_code), CM_BAD_BLOCK_CLOSE);
			ASSERT_DBG(!ADT_APP_INFO(device_handle));
		}
		ct_remove_device(device_handle);
	}

	/*
	 *	Check if the device type is to be deallocated.
	 */

	if (device_type_deallocate) {

		/*
		 *	Remove the DD of the device type, remove any data connected to
		 *	the Active Device Type Table, and deallocate the device type.
		 */

		if (VALID_DD_HANDLE((ADTT_DD_HANDLE(device_type_handle)))) {

			switch (ADTT_DD_HANDLE(device_type_handle)->dd_handle_type) {

				case DDHT_ROD:
					r_code = rod_close(
						ADTT_DD_HANDLE(device_type_handle)-> dd_handle_number);
					ASSERT_RET((r_code == CM_SUCCESS), r_code);
					break;

				case DDHT_FLAT:
					/* Not yet supported. */
					break;

				default:
					CRASH_RET(CM_BAD_DD_HANDLE);
			}
		}

		if (remove_dd_dev_tbl_fn_ptr != 0) {
			r_code = (*remove_dd_dev_tbl_fn_ptr)(device_type_handle);
			ASSERT_RET((!r_code),CM_BAD_BLOCK_CLOSE);
			ASSERT_DBG(!ADTT_DD_DEV_TBLS(device_type_handle));
		}
		if (remove_dev_type_app_info_fn_ptr != 0) {
			r_code = (*remove_dev_type_app_info_fn_ptr)(device_type_handle);
			ASSERT_RET((!r_code),CM_BAD_BLOCK_CLOSE);
			ASSERT_DBG(!ADTT_APP_INFO(device_type_handle));
		}
		ct_remove_device_type(device_type_handle);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_op_index_to_param_offset
 *
 *	ShortDesc:  Convert an operational index into a parameter offset.
 *
 *	Description:
 *		The ct_op_index_to_param_offset function takes a block handle
 *		and operational index of a parameter and calculates the
 *		corresponding parameter offset.
 *
 *	Inputs:
 *		block_handle - handle of the block that contains the parameter.
 *		operational_index - operational index of the parameter.
 *
 *	Outputs:
 *		parameter_offset - offset of the parameter.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_PARAM_OFFSET.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ct_op_index_to_param_offset(BLOCK_HANDLE block_handle, 
	OBJECT_INDEX operational_index, int *parameter_offset)
{

	unsigned int			temp_offset;

	ASSERT_RET(parameter_offset, CM_BAD_POINTER) ;

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	/*
	 *	Calculate the parameter offset.
	 */

	temp_offset = (unsigned int) ((operational_index) -
		(ABT_A_OP_INDEX(block_handle)) - 1);

	/*
	 *	Check to make sure that the parameter offset is valid.
	 */

	if ((((temp_offset + 1) > ABT_PARAM_COUNT(block_handle))) ||
			((temp_offset + 1) == 0)) {
		return(CM_BAD_PARAM_OFFSET);
	}

	*parameter_offset = (int)temp_offset;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_param_offset_to_op_index
 *
 *	ShortDesc:  Convert a parameter offset to an operational index.
 *
 *	Description:
 *		The ct_param_offset_to_op_index function takes a block handle
 *		and offset of a parameter and calculates the corresponding
 *		operational index.
 *
 *	Inputs:
 *		block_handle - handle of the block that contains the parameter.
 *		parameter_offset - offset of the parameter.
 *
 *	Outputs:
 *		operational_index - operational index of the parameter.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_PARAM_OFFSET.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ct_param_offset_to_op_index(BLOCK_HANDLE block_handle, int parameter_offset,
	OBJECT_INDEX *operational_index)
{

	ASSERT_RET(operational_index, CM_BAD_POINTER) ;

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	/*
	 *	Check to make sure that the parameter offset is valid.
	 */

	if (((unsigned)(parameter_offset + 1) > ABT_PARAM_COUNT(block_handle)) ||
			(parameter_offset + 1 <= 0)) {
		return(CM_BAD_PARAM_OFFSET);
	}

	/*
	 *	Calculate the operational index.
	 */

	*operational_index = (OBJECT_INDEX)
			(ABT_A_OP_INDEX(block_handle) + 1 + parameter_offset);

	return(CM_SUCCESS);
}



