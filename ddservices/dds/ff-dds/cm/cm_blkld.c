/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_blkld.c - Connection Manager Block Load Module
 *
 *	This file contains all functions pertaining to the
 *	block loader process of the Connection Manager.
 */

#include <stdlib.h>
#include <stdio.h>

#include "cm_lib.h"
#include "cm_loc.h"
#include "ddi_lib.h"
#include "int_diff.h"

/*
 *	Function pointers for building Application information in the
 *	Active Tables.
 */

static int (*build_dev_app_info_fn_ptr) P((DEVICE_HANDLE)) = 0;

/***********************************************************************
 *
 *	Name:  bl_init_app_info_fn_ptrs
 *
 *	ShortDesc:  Initialize the Application information function
 *				pointers.
 *
 *	Description:
 *		The lb_init_app_info_fn_ptrs function links the pointers of
 *		the Application functions that the Connection Manager calls
 *		when any Application information needs to be built.
 *
 *	Inputs:
 *		app_info_fn_ptrs - the necessary Application information
 *						   function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
bl_init_app_info_fn_ptrs(const APP_INFO_FN_PTRS *app_info_fn_ptrs)
{

	/*
	 *	Link the pointer of the function that builds Application
	 *	information in the Active Device Table.
	 */

	build_dev_app_info_fn_ptr =
		app_info_fn_ptrs->build_dev_app_info_fn_ptr;

}


/***********************************************************************
 *
 *	Name:  bl_oprod_open
 *
 *	ShortDesc:  Open up an OPROD for a device.
 *
 *	Description:
 *		The bl_oprod_open function takes a device handle and opens up
 *		an OPROD for a device.  It will also load the data type
 *		structure object that describes a Block Object
 *		from the OPOD in the device into the OPROD.
 *
 *	Inputs:
 *		device_handle - handle of the device whose OPROD is to be
 *						opened.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_OPROD_OPEN.
 *		Return values from cr_comm_get function.
 *		Return values from rod_object_copy function.
 *		Return values from rod_open function.
 *		Return values from rod_put function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
bl_oprod_open(DEVICE_HANDLE device_handle, ENV_INFO *env_info)
{

	OBJECT		*comm_object;
	OBJECT		*rod_object;
	ROD_HANDLE	 rod_handle;
	int			 r_code;

	/*
	 *	Get the the OPOD's OD Object Description from the device,
	 *	and use it to open a new Operational ROD (OPROD).
	 */

	r_code = cr_comm_get(device_handle, CT_OPOD, 0, &comm_object, env_info);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	r_code = rod_object_copy(&rod_object, comm_object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	cr_object_free(comm_object);

	rod_handle = rod_open(rod_object);
	if (rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);
	}

	/*
	 *	Get the data type structure object that describes a
	 *	Block Object, and put it into the OPROD.
	 */

	r_code = cr_comm_get(device_handle, CT_OPOD, DT_BLOCK, &comm_object,
			env_info);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	r_code = rod_object_copy(&rod_object, comm_object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	cr_object_free(comm_object);

	r_code = rod_put(rod_handle, DT_BLOCK, rod_object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Put the OPROD Handle into the Active Device Table and
	 *	build any device specific Application information.
	 */

	SET_ADT_OPROD_HANDLE(device_handle, rod_handle);

	if (build_dev_app_info_fn_ptr != 0) {
		r_code = (*build_dev_app_info_fn_ptr)(ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(CM_BAD_OPROD_OPEN);
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  bl_block_get_op
 *
 *	ShortDesc:  Load a Block's operational value.
 *
 *	Description:
 *		The bl_block_get_op function takes a block handle and 
 *		loads the operational value of a Block from the device
 *		into the connection manager.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose object is to be
 *					   loaded.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from cr_param_load function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

static int
bl_block_get_op(BLOCK_HANDLE block_handle)
{

	COMM_VALUE_LIST	comm_value_list;
	P_REF			p_ref ;
	int				r_code;
	ENV_INFO		env_info ;
	APP_INFO		app_info ;

	/*
	 *	Read the operational value of the Block from the device.
	 */

	p_ref.type = 0 ;
	p_ref.subindex = 0 ;
	p_ref.po = 0 ;

	(void)memset((char *)&env_info, 0, sizeof(env_info)) ;
	(void)memset((char *)&app_info, 0, sizeof(app_info)) ;
	env_info.app_info = &app_info;
	env_info.block_handle = block_handle ;

	r_code = cr_param_load(&env_info, &p_ref, &comm_value_list) ;
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Put some of the Block info into the Active Block Table.
	 */

	SET_ABT_DD_BLK_ID(block_handle,
		comm_value_list.cmvl_list[BLK_DD_REFERENCE_NUM].cm_val.cm_ulong) ;
	if(NT_OFF_LINE == (NT_NET_TYPE(network_handle)))
	{
		SET_ABT_PARAM_COUNT(block_handle, (unsigned int)
			comm_value_list.cmvl_list[BLK_SIM_PARAM_COUNT_NUM].cm_val.cm_ulong) ;
	}
	else
	{
		SET_ABT_PARAM_COUNT(block_handle, (unsigned int)
			comm_value_list.cmvl_list[BLK_ACTUAL_PARAM_COUNT_NUM].cm_val.cm_ulong) ;
	}
	cr_free_value_list(&comm_value_list) ;
	return(CM_SUCCESS);
}




/***********************************************************************
 *
 *	Name:  bl_block_load
 *
 *	ShortDesc:  Load operational block information from a device into the 
 *				Connection Manager 
 *
 *	Description:
 *				The OPROD is opened for later use and operational part of the
 *				block is loaded from the device.  The number of parameters,
 *				DD device id, and the OPROD handle are filled in as a result.
 *
 *	Inputs:
 *		block_handle - the handle of the block that is to be loaded.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		Return values from bl_oprod_open function.
 *		Return values from bl_block_get_op function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int
bl_block_load(BLOCK_HANDLE block_handle, ENV_INFO* env_info)
{

	int				r_code;
	DEVICE_HANDLE	device_handle ;

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!VALID_BLOCK_HANDLE(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	device_handle = ABT_ADT_OFFSET(block_handle) ;
	if (!VALID_DEVICE_HANDLE(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}


	if((NT_NET_TYPE(network_handle) != NT_OFF_LINE) && (NT_NET_TYPE(network_handle) != NT_A)){
		/*
		 * Open the OPROD because the Device Simulator may want to use it.
		 */

		r_code = bl_oprod_open(device_handle, env_info) ;
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}
	/*
	 *	If the block is not already loaded, load the Block 
	 *	value from the device into the connection manager.
	 */

	if (ABT_PARAM_COUNT(block_handle) == (unsigned)-1) {
		r_code = bl_block_get_op(block_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	At this point the block's parameters may be loaded from
	 *	the device into the parameter cache.  It is not done here 
	 *	in order to minimize communication calls.
	 */

	return(CM_SUCCESS);
}
