/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_rod.c - Connection Manager - ROD Manager Module
 *
 *	This file contains all functions pertaining to RODs (Remote
 *	Object Dictionaries).
 *
 *	A Remote Object Dictionary (ROD) is a host's copy of an Object
 *	Dictionary (OD) that exists in an actual device.  The data
 *	structures used to describe a ROD are local to this system and
 *	are defined in "cm_rod.h".  These structures are not identical
 *	memory copies of what is in the device's OD, but contain the
 *	same information.
 *
 *	All data within a ROD must be allocated in the ROD heap.  The
 *	following functions are used to maintain the ROD heap:
 *
 *	rod_init			rod_malloc			rod_calloc
 *	rod_realloc			rod_free			rod_member
 *
 *	A ROD is initiated by using the rod_open function with an OD
 *	description object (OD Object #0) as its parameter.  The OD
 *	description object contains information that dictates the size
 *	of the four sections of an OD.
 *
 *	A ROD's information may be accessed using the following functions:
 *
 *	rod_get		rod_put		rod_read		rod_write
 *
 *	The rod_get function is found in dds_cm.c; all the rest mentioned
 *	above (and many more) are contained herein.
 */

#include <string.h>
#ifdef SUN
#include <memory.h>		/* K&R only */
#endif

#include "cm_lib.h"
#include "cm_loc.h"
#include "rmalloc.h"
#include "tags_sa.h"

/*
 *	The ROD Heap
 */

#define ROD_HEAP_SIZE		20000000

static RHEAP	*rheap;
static long	 rod_heap[ROD_HEAP_SIZE/sizeof(long)];


/***********************************************************************
 *
 *	Name:  rod_init
 *
 *	ShortDesc:  Initialize the ROD Manager.
 *
 *	Description:
 *		The rod_init function initializes the ROD heap, and
 *		initializes the ROD Table by setting the table count
 *		to zero and initializing the table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_init(void)
{

#if defined(SABER) || defined(CODECENTER)
	/*
	 *	Tell saber and codecenter that this heap is uninitialized.
	 */

	(void)memset ((char *)rod_heap, 0xbf, sizeof (rod_heap));
#endif

	/*
	 *	Initialize the ROD heap.
	 */

	rheap = (RHEAP *) rod_heap;
	rheapinit(rheap, (long) sizeof(rod_heap));

	/*
	 *	Initialize the ROD Table.
	 */
	rod_tbl.count = DT_BLOCK_COUNT;

	rod_tbl.list = (OBJECT_DICTIONARY **)rod_calloc(rod_tbl.count,
				sizeof (*rod_tbl.list));
	if (!rod_tbl.list) {
		return(CM_NO_ROD_MEMORY);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_malloc
 *
 *	ShortDesc:  Allocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_malloc function takes a size and allocates a segment
 *		in the ROD heap.
 *
 *	Inputs:
 *		size - number of bytes to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *
rod_malloc(int size)
{

	/*
	 *	Allocate a segment in the ROD heap.
	 */

	return((char *)rmalloc(rheap, size));
}


/***********************************************************************
 *
 *	Name:  rod_calloc
 *
 *	ShortDesc:  Allocate and clear a segment in the ROD heap.
 *
 *	Description:
 *		The rod_calloc function takes in a number and size and
 *		allocates and clears a segment in the ROD heap.
 *
 *	Inputs:
 *		number - the number of elements to allocate.
 *		size - the size of each element to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated and cleared memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *
rod_calloc(int number, int size)
{

	/*
	 *	Allocate and clear a segment in the ROD heap.
	 */

	return((char *)rcalloc(rheap, number, size));
}


/***********************************************************************
 *
 *	Name:  rod_realloc
 *
 *	ShortDesc:  Reallocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_realloc function takes a pointer and size and
 *		reallocates the corresponding segment in the ROD heap
 *		to the new size.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to reallocate.
 *		size - the new size.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the reallocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *
rod_realloc(char *pointer, int size)
{

	/*
	 *	Reallocate the segment in the ROD heap.
	 */

	return((char *)rrealloc(rheap, pointer, size));
}


/***********************************************************************
 *
 *	Name:  rod_free
 *
 *	ShortDesc:	Free a segment in the ROD heap
 *
 *	Description:
 *		The rod_free function takes a pointer and frees the
 *		corresponding segment in the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to free.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
rod_free(char *pointer)
{

	/*
	 *	Free the segment in the ROD heap.
	 */

	rfree(rheap, pointer);
}


/*********************************************************************
 *
 *	Name:  rod_member
 *
 *	ShortDesc:  Determine if a pointer is a member of the ROD heap.
 *
 *	Description:
 *		The rod_member function takes a pointer and determines if
 *		it is a memeber of the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to be identified.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		1 if the pointer is a member of the ROD heap.
 *		0 if the pointer is not a member of the ROD heap.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_member(char *pointer)
{

	/*
	 *	Determine if the pointer is a member of the ROD heap.
	 */

	return(rmemberof(rheap, pointer));
}


/***********************************************************************
 *
 *	Name:  rod_object_heap_check
 *
 *	ShortDesc:  Check if an object is in the ROD heap.
 *
 *	Description:
 *		The rod_object_heap_check function takes an object and checks
 *		if all of its components are located in the ROD heap.
 *
 *	Inputs:
 *		object - the object to be checked.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_OBJECT_NOT_IN_HEAP.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
rod_object_heap_check(OBJECT *object)
{

	if (!object) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}
	if (!rod_member((char *)object)) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}

	if (object->name) {
		if (!rod_member((char *)object->name)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->extension) {
		if (!rod_member((char *)object->extension)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->value) {
		if (!rod_member((char *)object->value)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->specific) {
		if (!rod_member((char *)object->specific)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}

		switch (object->code) {

			case OC_PROGRAM_INVOCATION:
				if (!rod_member((char *)object->specific->
						program_invocation.domain_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_DATA_TYPE:
				if (!rod_member((char *)object->specific->
						data_type.symbol)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:
				if (!rod_member((char *)object->specific->
						data_type_structure.type_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				if (!rod_member((char *)object->specific->
						data_type_structure.size_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_VARIABLE_LIST:
				if (!rod_member((char *)object->specific->
						variable_list.list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			default:
				break;
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_new_rod
 *
 *	ShortDesc:  Create a new ROD in the ROD Table.
 *
 *	Description:
 *		The rod_new_rod function creates a new ROD either in the
 *		first unused (deallocated) ROD space or at the end of the
 *		existing ROD Table list.  The corresponding ROD handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the new ROD.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static ROD_HANDLE
rod_new_rod()
{

	ROD_HANDLE	rod_handle;

	/*
	 *	Go through each ROD in the ROD Table.
	 */

	for (rod_handle = 0; rod_handle < rod_tbl.count; rod_handle ++) {

		/*
		 *	Check if the ROD has been deallocated.
		 */

		if (!rod_tbl.list[rod_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire ROD Table was gone through without
	 *	finding any deallocated ROD's.
	 */

	if (rod_handle >= rod_tbl.count) {

		/*
		 *	Extend the ROD Table.
		 */

		rod_tbl.count ++;
		rod_tbl.list = (OBJECT_DICTIONARY **) rod_realloc(
				(char *)rod_tbl.list,
				sizeof(OBJECT_DICTIONARY *) * rod_tbl.count);
		if (!rod_tbl.list) {
			return(CM_NO_ROD_MEMORY);
		}

		rod_handle = rod_tbl.count - 1;
	}

	/*
	 *	Allocate a new ROD where the ROD handle is.
	 */

	rod_tbl.list[rod_handle] =
			(OBJECT_DICTIONARY *) rod_calloc(1,
			sizeof(OBJECT_DICTIONARY));
	if (!rod_tbl.list[rod_handle]) {
		return(CM_NO_ROD_MEMORY);
	}
	
	return(rod_handle);
}


/***********************************************************************
 *
 *	Name:  rod_open
 *
 *	ShortDesc:  Open a ROD.
 *
 *	Description:
 *		The rod_open function takes an OD description object and 
 *		opens a new ROD.  The maximum number of objects will be
 *		set.  All objects will be empty.  The corresponding ROD
 *		handle will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		object_0 - the OD description object.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the opened ROD.
 *		Return values from rod_new_rod function (CM_NO_ROD_MEMORY).
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

ROD_HANDLE
rod_open(OBJECT *object_0)
{

	ROD_HANDLE					 rod_handle;
	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 r_code;

	/*
	 *	Check to make sure that the OD description object is in the
	 *	ROD heap.
	 */

	r_code = rod_object_heap_check(object_0);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Allocate a new ROD and initialize its description.
	 */

	rod_handle = rod_new_rod();

	if (rod_handle < 0) {
		return(rod_handle);
	}
		
	rod = rod_tbl.list[rod_handle];
	rod->od_description = object_0;
	od_description = &object_0->specific->od_description;

	/*
	 *	Allocate the four sections of the ROD.
	 *
	 *	(Note that this does not allocate any objects - just
	 *	object pointers.)
	 */

	if (od_description->stod_object_count) {
		rod->stod = (OBJECT **) rod_calloc((int)od_description->
				stod_object_count, sizeof(OBJECT *));
		if (!rod->stod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->sod_object_count) {
		rod->sod = (OBJECT **) rod_calloc((int)od_description->
				sod_object_count, sizeof(OBJECT *));
		if (!rod->sod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->dvod_object_count) {
		rod->dvod = (OBJECT **) rod_calloc((int)od_description->
				dvod_object_count, sizeof(OBJECT *));
		if (!rod->dvod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->dpod_object_count) {
		rod->dpod = (OBJECT **) rod_calloc((int)od_description->
				dpod_object_count, sizeof(OBJECT *));
		if (!rod->dpod)
			return (CM_NO_ROD_MEMORY);
	}

    rod->version = 0x0402;  // Default to old version (may be overwritten later)
	return(rod_handle);
}

/*
 * calc_version
 *
 * Given the access rights field from an object, determine the binary format version
 * 0x0402 = DDS 4.2
 * 0x0500 = DDS 5.0
 * 0x0501 = DDS 5.1
 */
unsigned long calc_version(unsigned int access_rights, unsigned char access_groups)
{
    switch(access_rights)
    {
    case ENHANCED_FORMAT_50:
        return 0x0500 + (unsigned long)((access_groups & 0xf0) << 4) + (access_groups & 0x0f);
    default:
        return 0x0402;
    }
}

int rod_set_version(ROD_HANDLE rhandle, unsigned long version)
{
    OBJECT_DICTIONARY *rod;
	if (!VALID_ROD_HANDLE(rhandle)) {
		return 0;
	}
	rod = rod_tbl.list[rhandle];
    rod->version = version;
    return 0;
}
unsigned long rod_get_version(ROD_HANDLE rhandle)
{
    OBJECT_DICTIONARY *rod;
	if (!VALID_ROD_HANDLE(rhandle)) {
		return 0;
	}
	rod = rod_tbl.list[rhandle];
    return rod->version;
}


/***********************************************************************
 *
 *	Name:  rod_object_free
 *
 *	ShortDesc:  Free an object and all of its allocated contents.
 *
 *	Description:
 *		The rod_object_free function takes an object and frees it
 *		from the ROD heap along with any of its allocated contents.
 *
 *	Inputs:
 *		object - the object to be freed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
rod_object_free(OBJECT *object)
{

	/*
	 *	Go through all possible allocated components of an object
	 *	and free them if necessary.
	 */

	if (object->name) {
		rod_free((char *)object->name);
	}
	if (object->extension) {
		rod_free((char *)object->extension);
	}
	if (object->value) {
		rod_free((char *)object->value);
	}
	if (object->specific) {

		switch (object->code) {

			case OC_NULL:
				break;

			case OC_OD_DESCRIPTION:
				break;

			case OC_DOMAIN:
				break;

			case OC_PROGRAM_INVOCATION:
				if (object->specific->program_invocation.domain_list) {
					rod_free((char *)object->specific->
							program_invocation.domain_list);
				}
				break;

			case OC_EVENT:
				break;

			case OC_DATA_TYPE:
				if (object->specific->data_type.symbol) {
					rod_free((char *)object->specific->
							data_type.symbol);
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:
				if (object->specific->data_type_structure.type_list) {
					rod_free((char *)object->specific->
							data_type_structure.type_list);
				}
				if (object->specific->data_type_structure.size_list) {
					rod_free((char *)object->specific->
							data_type_structure.size_list);
				}
				break;

			case OC_SIMPLE_VARIABLE:
				break;

			case OC_ARRAY:
				break;

			case OC_RECORD:
				break;

			case OC_VARIABLE_LIST:
				if (object->specific->variable_list.list) {
					rod_free((char *)object->specific->variable_list.list);
				}
				break;

			default:
				break;
		}

		rod_free((char *)object->specific);
	}

	rod_free((char *)object);
}


/***********************************************************************
 *
 *	Name:  rod_close
 *
 *	ShortDesc:  Close a ROD.
 *
 *	Description:
 *		The rod_close function takes a ROD handle and closes a
 *		specific ROD in the ROD Table.  Note that the ROD will be
 *		deallocated but the ROD Table list will not be reduced.
 *		This function will be successful unless an invalid ROD
 *		handle is passed in.
 *
 *	Inputs:
 *		rod_handle - the handle of the specific ROD to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_close(ROD_HANDLE rod_handle)
{

	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							object_num;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Go through each object in the Static List of Object Types
	 *	(ST-OD) and free the object if necessary, then free the ST-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->stod_object_count;
			object_num ++) {

		if (rod->stod[object_num]) {
			rod_object_free(rod->stod[object_num]);
		}
	}
	rod_free((char *)rod->stod);

	/*
	 *	Go through each object in the Static Object Dictionary
	 *	(S-OD) and free the object if necessary, then free the S-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {

		if (rod->sod[object_num]) {
			rod_object_free(rod->sod[object_num]);
		}
	}
	rod_free((char *)rod->sod);

	/*
	 *	Go through each object in the Dynamic List of Variable Lists
	 *	(DV-OD) and free the object if necessary, then free the DV-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->dvod_object_count;
			object_num ++) {

		if (rod->dvod[object_num]) {
			rod_object_free(rod->dvod[object_num]);
		}
	}
	rod_free((char *)rod->dvod);

	/*
	 *	Go through each object in the Dynamic List of Program
	 *	Invocations (DP-OD) and free the object if necessary.
	 *	Then free the DP-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->dpod_object_count;
			object_num ++) {

		if (rod->dpod[object_num]) {
			rod_object_free(rod->dpod[object_num]);
		}
	}
	rod_free((char *)rod->dpod);

	/*
	 *	Free the ROD description object.
	 */

	rod_object_free(rod->od_description);

	/*
	 *	Free the ROD.
	 */

	rod_free((char *)rod);
	rod_tbl.list[rod_handle] = (OBJECT_DICTIONARY *)0;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_object_lookup
 *
 *	ShortDesc:  Look up an object in a ROD.
 *
 *	Description:
 *		The rod_object_lookup function takes a ROD handle and object
 *		index and searches the four sections of the ROD for the
 *		corresponding object.  The (found) object output is a pointer
 *		to an object.  If this object pointer is null, it indicates
 *		that the object index is valid, but the object is not yet
 *		allocated.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the object that is being looked up.
 *
 *	Outputs:
 *		object - the found object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_INDEX.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{

	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check which section of the ROD the object index resides.
	 */

	if (object_index == 0) {
		*object = rod->od_description;
	}
	else if (object_index <= od_description->stod_object_count) {
		*object = rod->stod[object_index - 1];
	}
	else if ((object_index >= od_description->sod_first_index) &&
			(object_index < od_description->sod_first_index +
			od_description->sod_object_count)) {
		*object = rod->sod[object_index -
				od_description->sod_first_index];
	}
	else if ((object_index >= od_description->dvod_first_index) &&
			(object_index < od_description->dvod_first_index +
			od_description->dvod_object_count)) {
		*object = rod->dvod[object_index -
				od_description->dvod_first_index];
	}
	else if ((object_index >= od_description->dpod_first_index) &&
			(object_index < od_description->dpod_first_index +
			od_description->dpod_object_count)) {
		*object = rod->dpod[object_index -
				od_description->dpod_first_index];
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_put
 *
 *	ShortDesc:  Put an object into a ROD.
 *
 *	Description:
 *		The rod_put function takes a ROD handle, object index, and
 *		object and puts the object into the corresponding ROD.  The
 *		object and any of its allocated contents must be allocated
 *		in the ROD heap, and checking is done to ensure this.  If an
 *		object exists where the new object is being put, the existing
 *		object will be freed.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD where the object will be put.
 *		object_index - the index of the object to be put.
 *		object - the object to be put.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_ILLEGAL_PUT.
 *		CM_BAD_INDEX.
 *		Return values from rod_object_heap_check function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX	object_index, OBJECT *object)
{

	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 r_code;

	/*
	 *	Check to make sure that the ROD handle and the object are valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}
	if (!object) {
		return(CM_NO_OBJECT);
	}

	/*
	 *	Get the ROD and description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check to make sure that OD description object is not
	 *	being overwritten.
	 */

	if (object_index == 0) {
		return(CM_ILLEGAL_PUT);
	}

	/*
	 *	Check to make sure that the object to be put is in the ROD heap.
	 */

	r_code = rod_object_heap_check(object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check which section of the Object Dictionary the object index
	 *	resides.  Then free any existing object and put in the new object.
	 */

	if (object_index <= od_description->stod_object_count) {

		if (rod->stod[object_index - 1]) {

			rod_object_free(rod->stod[object_index - 1]);
		}
		rod->stod[object_index - 1] = object;
	}
	else if ((object_index >= od_description->sod_first_index) &&
			(object_index < od_description->sod_first_index +
			od_description->sod_object_count)) {

		if (rod->sod[object_index -
				od_description->sod_first_index]) {

			rod_object_free(rod->sod[object_index -
					od_description->sod_first_index]);
		}
		rod->sod[object_index - od_description->sod_first_index] =
				object;
	}
	else if ((object_index >= od_description->dvod_first_index) &&
			(object_index < od_description->dvod_first_index +
			od_description->dvod_object_count)) {

		if (rod->dvod[object_index -
				od_description->dvod_first_index]) {

			rod_object_free(rod->dvod[object_index -
					od_description->dvod_first_index]);
		}
		rod->dvod[object_index - od_description->dvod_first_index] =
				object;
	}
	else if ((object_index >= od_description->dpod_first_index) &&
			(object_index < od_description->dpod_first_index +
			od_description->dpod_object_count)) {

		if (rod->dpod[object_index -
				od_description->dpod_first_index]) {

			rod_object_free(rod->dpod[object_index -
					od_description->dpod_first_index]);
		}
		rod->dpod[object_index - od_description->dpod_first_index] =
				object;
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get_record_size
 *
 *	ShortDesc:  Get the size of a record object's value.
 *
 *	Description:
 *		The rod_get_record_size function takes a ROD handle and record
 *		object and calculates the size of the record object's value.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the record exists in.
 *		object - the record object.
 *
 *	Outputs:
 *		value_size - the size of the record object's value.
 *		type_objectp - the object describing the structure of the value.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_DATA_TYPE.
 *		CM_BAD_OBJECT.
 *		Return values from rod_get function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
rod_get_record_size(ROD_HANDLE rod_handle, OBJECT *object, int *value_size,
	OBJECT **type_objectp)
{

	OBJECT	*type_object;
	int		 member_num;
	int		 r_code;

	/*
	 *	Get the data type structure object of the record object.
	 */

	r_code = rod_get(rod_handle, object->specific->record.type,
			&type_object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check to make sure that it is a data type structure object,
	 *	and that it is properly allocated.
	 */

	if (type_object->code != OC_DATA_TYPE_STRUCTURE) {
		return(CM_NO_DATA_TYPE);
	}
	if ((!type_object->specific) ||
			(!type_object->specific->data_type_structure.size_list)) {
		return(CM_BAD_OBJECT);
	}

	/*
	 *	Go through each record member and total up the member sizes
	 *	to produce an overall size.
	 */

	*value_size = 0;

	for (member_num = 0; member_num < type_object->specific->
			data_type_structure.elem_count; member_num ++) {

		*value_size += type_object->specific->data_type_structure.
				size_list[member_num];
	}

	*type_objectp = type_object;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get_record_mem_info
 *
 *	ShortDesc:  Get the offset and size of a record object member's
 *				value.
 *
 *	Description:
 *		The rod_get_record_mem_info function takes a ROD handle, record
 *		object, and sub-index and determines the offset and size of the
 *		record object member's value.  If the sub-index is zero, the
 *		offset and size of the record object's entire value will be
 *		determined.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the record exists in.
 *		object - the record object.
 *		sub_index - the sub-index of the record member.
 *
 *	Outputs:
 *		value_offset - the offset the record object memeber's value.
 *		value_size - the size of the record object member's value.
 *		type_objectp - the object describing the structure of the value
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_DATA_TYPE.
 *		CM_BAD_OBJECT.
 *		CM_BAD_SUBINDEX.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
rod_get_record_mem_info(ROD_HANDLE rod_handle, OBJECT *object, SUBINDEX sub_index,
	int *value_offset, int *value_size, OBJECT **type_objectp)
{

	OBJECT	*type_object;
	int		 member_num;
	int		 r_code;

	/*
	 *	Get the data type structure object of the record object.
	 */

	r_code = rod_get(rod_handle, object->specific->record.type, &type_object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check to make sure that it is a data type structure object,
	 *	and that it is properly allocated.
	 */

	if (type_object->code != OC_DATA_TYPE_STRUCTURE) {
		return(CM_NO_DATA_TYPE);
	}
	if ((!type_object->specific) ||
			(!type_object->specific->data_type_structure.size_list)) {
		return(CM_BAD_OBJECT);
	}

	/*
	 *	Check to make sure that the sub-index is valid.
	 */

	if (sub_index >
			type_object->specific->data_type_structure.elem_count) {
		return(CM_BAD_SUBINDEX);
	}

	/*
	 *	Go through each record member and total up the member sizes
	 *	to produce an offset.
	 */

	*value_offset = 0;

	for (member_num = 0; member_num < sub_index - 1; member_num ++) {

		*value_offset += type_object->specific->data_type_structure.
				size_list[member_num];
	}

	/*
	 *	Get the size.
	 */

	*value_size = type_object->specific->data_type_structure.
			size_list[sub_index - 1];

	*type_objectp = type_object;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get_value_info
 *
 *	ShortDesc:  Get the offset and size of an object member's value.
 *
 *	Description:
 *		The rod_get_value_info function takes a ROD handle, object,
 *		and subindex and determines the offset and size of the
 *		object member's value.  If the sub-index is zero, the offset
 *		and size of the object's entire value will be determined.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object - the object.
 *		sub_index - the sub-index of the member.
 *
 *	Outputs:
 *		value_offset - the offset the object memeber's value.
 *		value_size - the size of the object member's value.
 *		type_objectp - the object describing the structure of the value.
 *					   (This may be NULL.)
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_BAD_OBJECT.
 *		CM_NO_OBJECT.
 *		CM_BAD_SUBINDEX.
 *		CM_NO_VALUE.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
rod_get_value_info(ROD_HANDLE rod_handle, OBJECT *object, SUBINDEX sub_index,
	int *value_offset, int *value_size, OBJECT **type_objectp)
{

	int			 r_code;
	OBJECT		 *type_object;

	if (!object->specific) {
		return(CM_BAD_OBJECT);
	}

	/*
	 *	Check the type of the object and take appropriate action.
	 */

	switch (object->code) {

		case OC_DOMAIN:

			/*
			 *	For a domain object, any sub-index other than zero is
			 *	invalid.  Return the offset and size for the entire value.
			 */

			if (sub_index) {
				return(CM_BAD_SUBINDEX);
			}
			*value_offset = 0;
			*value_size = object->specific->domain.size;
			if (type_objectp) {
				*type_objectp = object;
			}
			break;

		case OC_SIMPLE_VARIABLE:

			/*
			 *	For a simple variable object, any sub-index other than zero
			 *	is invalid.  Return the offset and size for the entire value.
			 */

			if (sub_index) {
				return(CM_BAD_SUBINDEX);
			}
			*value_offset = 0;
			*value_size = object->specific->simple_variable.size;
			if (type_objectp) {
				*type_objectp = object;
			}
			break;

		case OC_ARRAY:

			/*
			 *	For an array object, the sub-index must be validated.
			 *	If the sub-index is zero, return the offset and size
			 *	for the entire value.  Otherwise return the offset
			 *	and size of the specific array member's value.
			 */

			if (sub_index > object->specific->array.elem_count) {
				return(CM_BAD_SUBINDEX);
			}
			if (sub_index == 0) {
				*value_offset = 0;
				*value_size = object->specific->array.elem_count *
						object->specific->array.size;
			}
			else {
				
				*value_offset = (sub_index - 1) *
						object->specific->array.size;
				*value_size = object->specific->array.size;
			}
			if (type_objectp) {
				*type_objectp = object;
			}
			break;

		case OC_RECORD:

			/*
			 *	For a record object, the sub-index must be validated.
			 *	(This happens in the rod_get_record_mem_info function.)
			 *	If the sub-index is zero, return the offset and size
			 *	for the entire value.  Otherwise return the offset
			 *	and size of the specific record member's value.
			 */

			if (sub_index == 0) {
				*value_offset = 0;
				r_code = rod_get_record_size(rod_handle, object,
						value_size, &type_object);
				if (r_code != CM_SUCCESS) {
					return(r_code);
				}
			}
			else {
				r_code = rod_get_record_mem_info(rod_handle, object,
						sub_index, value_offset, value_size, &type_object);
				if (r_code != CM_SUCCESS) {
					return(r_code);
				}
			}
			if (type_objectp) {
				*type_objectp = type_object;
			}
			break;

		default:

			/*
			 *	Any other object type will have no value.
			 */

			return(CM_NO_VALUE);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_read
 *
 *	ShortDesc:  Read an object's value from a ROD.
 *
 *	Description:
 *		The rod_read function takes a ROD handle, object index, and
 *		sub-index and reads an object's value from the
 *		corresponding ROD.  If the sub-index is zero, the object's
 *		entire value will be read.  The (requested) value output is
 *		a pointer to a byte stream which resides in the ROD heap.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the object to be read.
 *		sub_index - the sub-index of the (array or record) member.
 *
 *	Outputs:
 *		value - the requested value.
 *		type_objectp - the object describing the structure of the value.
 *					   (This may be NULL.)
 *		lengthp - the length of the data.  (This may be NULL.)
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_VALUE.
 *		Return values from rod_get_value_info function.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_read(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, SUBINDEX sub_index,
	UINT8 **value, OBJECT **type_objectp, int *lengthp)

{
	int			 value_offset;
	int			 value_size;
	OBJECT		*object;
	int			 r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object and check to make sure that its value
	 *	is properly allocated.
	 */

	r_code = rod_object_lookup(rod_handle, object_index, &object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	if (!object->value) {
		return(CM_NO_VALUE);
	}

	/*
	 *	Get the offset and size of the object's value.
	 */

	r_code = rod_get_value_info(rod_handle, object, sub_index,
			&value_offset, &value_size, type_objectp);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Point to the value.
	 */

	*value = object->value + value_offset;
	if (lengthp) {
		*lengthp = value_size;
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_write
 *
 *	ShortDesc:  Write an object's value into a ROD.
 *
 *	Description:
 *		The rod_write function takes a ROD handle, object index, sub-
 *		index, and value and writes the object's value into the
 *		corresponding ROD.  The value to be written may be allocated
 *		anywhere as this function will copy it into the ROD heap.
 *		If the sub-index is zero, the object's entire value will be
 *		written.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the object to be written.
 *		sub_index - the sub-index of the (array or record) member.
 *		value - the value to be written.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_ROD_MEMORY.
 *		CM_NO_VALUE.
 *		Return values from rod_get_value_info function.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_write(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, SUBINDEX sub_index,
	UINT8 *value)
{

	int			 value_offset;
	int			 value_size;
	OBJECT		*object;
	int			 r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object.
	 */

	r_code = rod_object_lookup(rod_handle, object_index, &object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Get the offset and size of the object's value.
	 */

	r_code = rod_get_value_info(rod_handle, object, sub_index,
			&value_offset, &value_size, NULL);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check if the entire value is to be written.
	 */

	if (sub_index == 0) {
		
		/*
		 *	Check if the existing value needs to be freed.
		 */
	
		if (object->value) {
			rod_free((char *)object->value);
		}

		/*
		 *	Allocate space in the ROD heap for the new value and
		 *	copy the value.
		 */

		object->value = (UINT8 *) rod_calloc(1, value_size);
		if (!object->value) {
			return(CM_NO_ROD_MEMORY);
		}
		(void) memcpy((char *)object->value, (char *)value, value_size);
	}

	else {

		/*
		 *	Check to make sure that the existing value is there and
		 *	copy the new element value.
		 */

		if (!object->value) {
			return(CM_NO_VALUE);
		}
		(void) memcpy((char *)(object->value + value_offset),
				(char *)value, value_size);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_object_copy
 *
 *	ShortDesc:  Copy an object and all of its allocated contents into
 *				the ROD heap.
 *
 *	Description:
 *		The rod_object_copy function takes a source object and copies
 *		it into a destination object.  The destination object will be
 *		located in the ROD heap.
 *
 *	Inputs:
 *		source_object - the object to be copied from.
 *
 *	Outputs:
 *		destination_object - the object to be copied to.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
rod_object_copy(OBJECT **destination_object, OBJECT *source_object)
{

	OBJECT	*temp_object;

	/*
	 *	Check the object, and allocate and copy if necessary.
	 */

	if (!source_object) {
		return(CM_BAD_OBJECT);
	}

	temp_object = (OBJECT *) rod_calloc(1, sizeof(OBJECT));
	if (!temp_object) {
		return(CM_NO_ROD_MEMORY);
	}
	(void) memcpy((char *)temp_object, (char *)source_object,
			sizeof(OBJECT));

	/*
	 *	Null the value pointer.
	 */

	temp_object->value = 0;

	/*
	 *	Check the name, and allocate and copy if necessary.
	 */

	if (source_object->name) {
		temp_object->name = (char *) rod_calloc(1, ROD_OBJECT_NAME_LENGTH);
		if (!temp_object->name) {
			return(CM_NO_ROD_MEMORY);
		}
		(void)strcpy(temp_object->name, source_object->name);
	}

	/*
	 *	Check the extension, and allocate and copy if necessary.
	 */

	if (source_object->extension) {
		temp_object->extension = (UINT8 *) rod_calloc(1,
				(int)source_object->extension[0]);
		if (!temp_object->extension) {
			return(CM_NO_ROD_MEMORY);
		}
		(void) memcpy((char *)temp_object->extension,
				(char *)source_object->extension,
				(int)source_object->extension[0]);
	}

	/*
	 *	Check the specific information, and allocate and copy if necessary.
	 */

	if (source_object->specific) {

		temp_object->specific = (OBJECT_SPECIFIC *)
				rod_calloc(1, sizeof(OBJECT_SPECIFIC));
		if (!temp_object->specific) {
			return(CM_NO_ROD_MEMORY);
		}
		(void) memcpy((char *)temp_object->specific,
				(char *)source_object->specific,
				sizeof(OBJECT_SPECIFIC));

		/*
		 *	Check the type of the object and take appropriate action.
		 */

		switch (source_object->code) {

			case OC_PROGRAM_INVOCATION:

				/*
				 *	For a program invocation object, the domain list
				 *	must be checked and copied if necessary.
				 */

				if (source_object->specific->
						program_invocation.domain_list) {
					temp_object->specific->program_invocation.domain_list =
							(OBJECT_INDEX *) rod_calloc(
							(int)source_object->specific->
							program_invocation.domain_count,
							sizeof(OBJECT_INDEX));
					if (!temp_object->specific->
							program_invocation.domain_list) {
						return(CM_NO_ROD_MEMORY);
					}
					(void) memcpy((char *)temp_object->specific->
							program_invocation.domain_list,
							(char *)source_object->specific->
							program_invocation.domain_list,
							(int)(source_object->specific->
							program_invocation.domain_count *
							sizeof(OBJECT_INDEX)));
				}
				break;

			case OC_DATA_TYPE:

				/*
				 *	For a data type object, the symbol must be
				 *	checked and copied if necessary.
				 */

				if (source_object->specific->data_type.symbol) {
					temp_object->specific->data_type.symbol =
							(char *) rod_calloc(
							(int)source_object->specific->
							data_type.symbol_size, sizeof(char));
					if (!temp_object->specific->
							data_type.symbol) {
						return(CM_NO_ROD_MEMORY);
					}
					(void) memcpy((char *)temp_object->specific->
							data_type.symbol,
							(char *)source_object->specific->
							data_type.symbol,
							(int)(source_object->specific->
							data_type.symbol_size * sizeof(char)));
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:

				/*
				 *	For a data type structure object, the type list and
				 *	size list must be checked and copied if necessary.
				 */

				if (source_object->specific->data_type_structure.type_list) {
					temp_object->specific->data_type_structure.type_list =
							(OBJECT_INDEX *) rod_calloc(
							(int)source_object->specific->
							data_type_structure.elem_count,
							sizeof(OBJECT_INDEX));
					if (!temp_object->specific->
							data_type_structure.type_list) {
						return(CM_NO_ROD_MEMORY);
					}
					(void) memcpy((char *)temp_object->specific->
							data_type_structure.type_list,
							(char *)source_object->specific->
							data_type_structure.type_list,
							(int)(source_object->specific->
							data_type_structure.elem_count *
							sizeof(OBJECT_INDEX)));
				}

				if (source_object->specific->data_type_structure.size_list) {
					temp_object->specific->data_type_structure.size_list =
							(UINT8 *) rod_calloc(
							(int)source_object->specific->
							data_type_structure.elem_count, sizeof(UINT8));
					if (!temp_object->specific->
							data_type_structure.size_list) {
						return(CM_NO_ROD_MEMORY);
					}
					(void) memcpy((char *)temp_object->specific->
							data_type_structure.size_list,
							(char *)source_object->specific->
							data_type_structure.size_list,
							(int)(source_object->specific->
							data_type_structure.elem_count * sizeof(UINT8)));
				}
				break;

			case OC_VARIABLE_LIST:

				/*
				 *	For a variable list object, the list must be
				 *	checked and copied if necessary.
				 */

				if (source_object->specific->variable_list.list) {
					temp_object->specific->variable_list.list =
							(OBJECT_INDEX *) rod_calloc(
							(int)source_object->specific->
							variable_list.elem_count, sizeof(OBJECT_INDEX));
					if (!temp_object->specific->
							variable_list.list) {
						return(CM_NO_ROD_MEMORY);
					}
					(void) memcpy((char *)temp_object->specific->
							variable_list.list,
							(char *)source_object->specific->
							variable_list.list,
							(int)(source_object->specific->
							variable_list.elem_count *
							sizeof(OBJECT_INDEX)));
				}
				break;

			default:
				break;
		}
	}

	*destination_object = temp_object;

	return(CM_SUCCESS);
}
