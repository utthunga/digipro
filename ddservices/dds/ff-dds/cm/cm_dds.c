/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_dds.c - Connection Manager DD Support Module
 *
 *	This file contains all functions pertaining to DD
 *	Support in the Connection Manager.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "cm_lib.h"
#include "cm_loc.h"


/***********************************************************************
 *
 *	Name:  ds_init
 *
 *	ShortDesc:  Initialize the DD Support.
 *
 *	Description:
 *		The ds_init function takes a path and sets it as the path
 *		to the DD files.
 *
 *	Inputs:
 *		path - the path to the DD files.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DD_PATH.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int
ds_init(const char *path)
{

	struct stat		sbuf;

	/*
	 *	Check if the path is valid.
	 */

	if (stat(path, &sbuf) == 0) {

		/*
		 *	Set the path to the DD files.
		 */

		(void)strcpy(dd_path, path);
		return(CM_SUCCESS);
	}

	return(CM_BAD_DD_PATH);
}
