/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  @(#)int_meth.c	30.3	 30  14 Nov 1996
 *
 *	int_meth.c - parse and execute a method
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "std.h"
#include "rtn_code.h"
#include "int_loc.h"
#include "dds_lock.h"
#include "int_meth.h"
#include "ddi_lib.h"
#include "ddsbltin.h"
#include "app_xmal.h"
#include "int_diff.h"

/*
 *	External Variables
 */
#ifdef __cplusplus
	extern "C" {
#endif

extern char *lexptr; 		/* pointer to character string which is the method */
extern int error_count; 		/* number of errors while parsing */
extern STATEMENT *parsed_stmt; /* pointer to head of statement list after parsing */
#ifdef __cplusplus
	}
#endif

/* pointer to the head of the type A built-in symbols list */
extern SYM *bi_func_symbols_a;


/*
 * These macros are used to calculate the size of an interpreter
 * heap. These macros are used in the function do_method. 
 *
 * HEAP_MULTIPLIER
 *
 *		The greatest factor in determining the size of the
 *		heap is the size of the method definition. The size
 *		of the heap is estimated by multiplying the size
 *		of the method definition by HEAP_MULTIPLIER. 
 *
 * SLOP_SIZE
 *
 *		SLOP_SIZE is also added to the estimated size of the heap
 * 		and includes the space required by any method, e.g., a
 *		global scope. It also guarantees that the size of the heap
 *		is sufficient for even very small methods.  28 bytes is needed 
 *		for smallest method with required attributes.
 *
 * NOTE
 *
 *		These values were found empirically.  Slop is there so the buffer
 *		is large enough for smallest methods.
 *
 */
#define HEAP_MULTIPLIER	10
#define SLOP_SIZE		512	
							

/*********************************************************************
 *
 *	Name: parse_method
 *	ShortDesc: Parse a method definition
 *
 *	Description:
 *
 *		Parses a method definition. If no errors occur, the parsed
 *		method is returned.
 *
 *		The global `error_count' (the number of errors encountered
 *		while parsing and executing a method) is cleared before
 *		the method is parsed.
 *
 *	Inputs:
 *		env - The environment in which to parse the method.
 *		def - Method definition string.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		In no errors occur, a pointer to the parsed statement
 *		is returned. Otherwise, a null pointer is returned.
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

static STATEMENT *
parse_method(ENVIRON *env, char *def)
{
	int 		retval;
	STATEMENT 	*loc_stmt;	/* copy of parsed statement for return */

	/*
	 * Lock the parser because we want exclusive control
	 * of it.
	 */

	lock(PARSE_LOCK) ;
	lexptr = def;

	/*
	 * Call the parser. If successful, st_parse returns
	 * zero and the global parsed_stmt is set.
	 */

	error_count = 0;
	retval = st_parse(env);

	/*
	 * If successful, return the parsed statement.
	 * Otherwise, return a null pointer.
	 */
	if ((retval + error_count) != 0) {
		unlock(PARSE_LOCK) ;
		return (STATEMENT *) 0;
	}

	loc_stmt = parsed_stmt;

	/*
	 * Unlock the parser because we're done with it.
	 */

	unlock(PARSE_LOCK) ;
	return loc_stmt;
}

extern int globalMethodAbort;
/*********************************************************************
 *
 *	Name: do_method
 *	ShortDesc: Parse and execute a method
 *
 *	Description:
 *		Parses a method and if no errors occur, executes the method.
 *		First, the definition of the method to be parsed/executed is
 *		retrieved via DDI. Then a heap for the method parsing/execution
 *		is created. The size of this heap is based on the size of the
 *		method definition (see explanation of the defines at top of
 *		this file). Then an environment for this method (which is
 *		stored in the meth_info table) is initialized. The method is
 *		then parsed, and if no errors occur, it's executed.
 *		
 *	Inputs:
 *		env_info - The ENV_INFO structure.
 *		meth_id - Id of the method.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		SUCCESS, METH_PARSE_ERROR,
 *		and error returns from ddi_clean_item.
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

long
do_method(ENV_INFO2 *env_info, ITEM_ID meth_id)
{
	DEVICE_HANDLE	device_handle;
	ENVIRON			*env;
	RHEAP			*heap;
	long			heap_size;
	NET_TYPE		net_type;
	int 			retval;
	int				retval2;
	STATEMENT		*stmt;
	long			subheap_size;
	FLAT_METHOD		flat_meth;

	DDI_GENERIC_ITEM	generic_item;
	DDI_ITEM_SPECIFIER	item_spec;
	APP_INFO			*app_info;

	app_info = (APP_INFO *)env_info->app_info;

	/*
	 * Initialize the DDI parameters.
	 */

	item_spec.type = DDI_ITEM_ID;
	item_spec.item.id = meth_id;
	item_spec.subindex = 0;

	(void)memset((void *) &generic_item, 0, sizeof (DDI_GENERIC_ITEM));

	retval = ddi_get_item2(&item_spec, 
			      env_info,
			      (unsigned long) METHOD_DEF, 
			      &generic_item);

	if (retval != SUCCESS) {
		if (retval == DDL_CHECK_RETURN_LIST) {
			ASSERT_DBG(generic_item.errors.count != 0);
			retval = generic_item.errors.list[0].rc;
			ASSERT_DBG(retval != SUCCESS);
		}
		(void) ddi_clean_item(&generic_item);
	}

    flat_meth = *(FLAT_METHOD *)generic_item.item;

	/*
	 * Estimate the interpreter original(first) heap size.
	 * The size of each additional Sub-heap is choosing to
	 * be one-third of the first heap size.
	 */

#ifdef CPM_64_BIT_OS
        // Doubled the heap size to handle built-ins for 64 Bit
        heap_size = (long)((flat_meth.def.size * HEAP_MULTIPLIER) + SLOP_SIZE) * 2;
#else
        // Bharath, please confirm if this is fine for 32-bit !
        heap_size = (long)((flat_meth.def.size * HEAP_MULTIPLIER) + SLOP_SIZE);
#endif
	subheap_size = (long)heap_size / 3;

	/*
	 * Create heap header and heap itself.
	 */
	heap = (RHEAP *) xmalloc(sizeof(RHEAP));
	rcreateheap(heap, heap_size, subheap_size);

	/*
	 * Allocate the environment. A pointer to the heap
	 * is stored in the environment, which allows the
	 * other parts of the interpreter to find it.
	 */
	env = &meth_info[app_info->method_tag - 1].meth_environ;
	environ_init(env, heap);

	/*
	 * Add the builtin specific info to the environment.
	 */
	env->env_info = env_info; 
 
    if (env_info->type == ENV_BLOCK_HANDLE)
    {
	    retval = get_abt_adt_offset (env_info->block_handle, &device_handle); 
	    if (retval != CM_SUCCESS) {   
		    return (retval); 
	    } 
    }
    else
    {
        device_handle = env_info->device_handle;
    }
  
	retval = get_adt_net_type(device_handle, &net_type); 
	if (retval != CM_SUCCESS) {   
		return (retval); 
	} 
  
    init_a_bi_symbols();
	env->global_scope->symbols = bi_func_symbols_a;
 
	ASSERT_DBG(env->global_scope->symbols != NULL);

	/*
	 * Parse and execute the method. The return value from
	 * exec_stmt() is of no use to us here, so it's ignored.
	 */

	stmt = parse_method(env, flat_meth.def.data);

	/*
	 * The method-text was parsed and the parsed tree is created.
	 * Therefore, call to clean up the method flat structure.
	 */ 
	
	retval2 = ddi_clean_item(&generic_item);

	globalMethodAbort=0;
	if (stmt == (STATEMENT *) 0) {
		retval = METH_PARSE_ERROR;
	} else {
		(void) exec_stmt(stmt, env);
		retval = SUCCESS;
	}

	/*
	 * Free the entire list of heaps then the heap header.	
	 */
	rfreeheap(env->heap_ptr);
	xfree((void **) &env->heap_ptr);
	if (retval == SUCCESS) {
		return retval2;
	}
	return retval;
}
