
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *  This file contains the builtin wrapper functions.
 */

#include "int_inc.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#define NELEM(x)      (sizeof(x)/sizeof(x[0]))

#define COUNTRY_CODE_MARK   '|'

SYM *bi_func_symbols_a = 0;

#if !defined(WIN32)
extern int puts(const char *);
#endif

#if defined(MSDOS) || defined(WIN32) || defined(_MSC_VER)
#define BYTES_PER_SYM   256
#else
#define BYTES_PER_SYM   256
#endif /* MSDOS || WIN32 || _MSC_VER */

static VALUE r_mhd(SYM *params, ENVIRON *env_ptr);
static VALUE r_putchar(SYM *params, ENVIRON *env_ptr);
static VALUE r_puts(SYM *params, ENVIRON *env_ptr);

static VALUE wrap_ret_unsigned_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_signed_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_sel_double (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_float_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_double_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_menu_display (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_list_create_element (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_list_create_element2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_list_delete_element (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_list_delete_element2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_debug_line (SYM *params, ENVIRON *env_ptr);

static VALUE wrap_add_abort_method (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_remove_abort_method (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_remove_all_abort_methods (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_method_abort (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_discard_on_exit (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_send_on_exit (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_save_on_exit (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_date_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_double_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_float_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_signed_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_string_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_unsigned_lelem (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_sel_float (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_sel_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_float_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_float_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_double_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_double_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_signed_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_signed_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_unsigned_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_unsigned_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_string_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_string_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_date_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_date_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_assign (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_read_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_send_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_send_all_values (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_response_code (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_comm_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_abort_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_abort_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_abort_on_comm_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_abort_on_response_code (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_fail_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_fail_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_fail_on_comm_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_fail_on_response_code (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_retry_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_retry_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_retry_on_comm_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_retry_on_response_code (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_status_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_comm_error_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_response_code_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_builtin_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_message (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_dynamics (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_comm_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_response_code (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_delayfor (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_edit_device_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_edit_local_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_select_from_menu (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_acknowledgement (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_is_NaN (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_NaN_value (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_stddict_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_block_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_local_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_param_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_param_list_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_array_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_list_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_record_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_selector_ref (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_resolve_status (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_dds_error (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_float (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_float (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_double (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_double (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_signed (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_signed (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_unsigned (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_unsigned (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_string (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_date (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_date (SYM *params, ENVIRON *env_ptr);

/* FF-900 5.1 Additions Wrappers */
static VALUE wrap_assign2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_delayfor2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_dynamics2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_display_message2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_edit_device_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_edit_local_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_acknowledgement2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_block_instance_by_object_index (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_block_instance_by_tag (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_block_instance_count (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_date_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_double_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_float_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_signed_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_string_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_get_unsigned_value2 (SYM *params, ENVIRON *env_ptr);

static VALUE wrap_ret_double_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_float_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_signed_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_ret_unsigned_value2 (SYM *params, ENVIRON *env_ptr);

static VALUE wrap_put_date_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_double_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_float_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_signed_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_string_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_put_unsigned_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_read_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_select_from_menu2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_send_value2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_local_ref2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_param_ref2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_array_ref2 (SYM *params, ENVIRON *env_ptr);
static VALUE wrap_resolve_record_ref2 (SYM *params, ENVIRON *env_ptr);


/*
 * These next two functions aren't used anywhere but the
 * declarations are included for the sake of completeness.
 */
static VALUE wrap_bi_rcsim_set_response_code(SYM *params, ENVIRON *env_ptr);
static VALUE wrap_bi_rcsim_set_comm_err(SYM *params, ENVIRON *env_ptr);


/* ARGSUSED */

static VALUE
r_mhd(SYM *params, ENVIRON *env_ptr)
{
	VALUE ret_val;

	(void)env_ptr;

	if (params->v.l_p) {
		ret_val.l = (*(params->v.l_p))++;

	} else {
		ret_val.l = -1;
	}

	return ret_val;
}


static VALUE
r_putchar(SYM *params, ENVIRON *env_ptr)
{
	VALUE ret_val;

	(void)env_ptr;

	ret_val.l = putchar( *(params->v.l_p));
	return ret_val;
}


/* ARGSUSED */

static VALUE
r_puts(SYM *params, ENVIRON *env_ptr)
{
	VALUE ret_val;
	(void)env_ptr;

	ret_val.l = puts(params->v.c_p);
	return ret_val;
}


static VALUE
wrap_add_abort_method (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = add_abort_method (*params->v.ul_p, env_ptr->env_info);
        LOGC_INFO(CPM_FF_DDSERVICE, "add_abort_method: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_remove_abort_method (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = remove_abort_method (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "remove_abort_method: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_remove_all_abort_methods (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	remove_all_abort_methods (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "remove_all_abort_methods: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_method_abort (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	method_abort (params->v.c_p, env_ptr->env_info);
	/* NOTREACHED */
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "method_abort: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_discard_on_exit (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	discard_on_exit (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "discard_on_exit: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_send_on_exit (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	send_on_exit (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "send_on_exit: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_save_on_exit (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	save_on_exit (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "save_on_exit: return_value.l = %ld\n", return_value.l);

	return (return_value);
}

static VALUE
wrap_get_sel_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			selector;
	ITEM_ID			additional;
    long            *inout_len;
    char            *out_str;

    out_str = params->v.c_p;
    params = params->next_sym;
	var_id = *params->v.ul_p;
	params = params->next_sym;
	selector = *params->v.ul_p;
	params = params->next_sym;
	additional = *params->v.ul_p;
	params = params->next_sym;
    inout_len = params->v.l_p;

	return_value.l = get_sel_string(out_str, var_id, selector, additional, 
			inout_len, env_ptr->env_info);
			
	return (return_value);
}

static VALUE
wrap_get_sel_double (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			selector;
	ITEM_ID			additional;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	selector = *params->v.ul_p;
	params = params->next_sym;
	additional = *params->v.ul_p;

	return_value.d = get_sel_double(var_id, selector, additional, 
			env_ptr->env_info);
			
	return (return_value);
}

static VALUE
wrap_get_sel_double2 (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
    ITEM_ID         index;
    ITEM_ID         embedded_id;
	ITEM_ID			selector;
	ITEM_ID			additional;

	var_id = *params->v.ul_p;
	params = params->next_sym;

	index = *params->v.ul_p;
	params = params->next_sym;

	embedded_id = *params->v.ul_p;
	params = params->next_sym;
        
    selector = *params->v.ul_p;
	params = params->next_sym;

	additional = *params->v.ul_p;

	return_value.d = get_sel_double2(var_id, index, embedded_id, selector, additional, 
			env_ptr->env_info);
			
	return (return_value);
}


static VALUE
wrap_get_float_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	float			*valuep;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.f_p;

	return_value.l = get_float_value (var_id, var_member_id, valuep, 
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_float_value: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_float_value: param3: *valuep = %f\n", *valuep);

	return (return_value);
}

static VALUE
wrap_ret_float_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.f = ret_float_value (var_id, var_member_id,
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "ret_float_value: return_value.f = %f\n", return_value.f);

	return (return_value);
}

static VALUE
wrap_put_float_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	double			value;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	value = *params->v.d_p;

	return_value.l = put_float_value (var_id, var_member_id, value, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_float_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}

static VALUE wrap_get_date_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;
    char            *out_date;
    long            *out_len;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    params = params->next_sym;

    out_date = params->v.c_p;
    params = params->next_sym;

    out_len = params->v.l_p;

    return_value.l = get_date_lelem(list_id, list_index, 
                                    element_id, subelement_id, out_date, 
                                    out_len, env_ptr->env_info);

    return return_value;
}

static VALUE wrap_get_date_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;
    char            *out_date;
    long            *out_len;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    params = params->next_sym;

    out_date = params->v.c_p;
    params = params->next_sym;

    out_len = params->v.l_p;

    return_value.l = get_date_lelem2(list_id, list_index, 
                                    embedded_list_id, embedded_list_index,
                                    element_id, subelement_id, out_date, 
                                    out_len, env_ptr->env_info);

    return return_value;
}


static VALUE wrap_get_string_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;
    char            *out_string;
    long            *out_len;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    params = params->next_sym;

    out_string = params->v.c_p;
    params = params->next_sym;

    out_len = params->v.l_p;

    return_value.l = get_string_lelem(list_id, list_index, element_id, subelement_id, 
                                      out_string, out_len, env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_string_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;
    char            *out_string;
    long            *out_len;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    params = params->next_sym;

    out_string = params->v.c_p;
    params = params->next_sym;

    out_len = params->v.l_p;

    return_value.l = get_string_lelem2(list_id, list_index, embedded_list_id, embedded_list_index,
                                      element_id, subelement_id, 
                                      out_string, out_len, env_ptr->env_info);
    return (return_value);
}


static VALUE wrap_get_double_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;

    return_value.d = get_double_lelem(list_id, list_index, element_id, subelement_id, 
                                      env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_double_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;

    return_value.d = get_double_lelem2(list_id, list_index, 
                                      embedded_list_id, embedded_list_index,
                                      element_id, subelement_id, 
                                      env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_float_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;

    return_value.f = get_float_lelem(list_id, list_index, element_id, subelement_id, 
                                     env_ptr->env_info);

    return (return_value);
}

static VALUE wrap_get_float_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;

    return_value.f = get_float_lelem2(list_id, list_index, 
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id, 
                                     env_ptr->env_info);

    return (return_value);
}

static VALUE wrap_get_signed_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    return_value.l = get_signed_lelem(list_id, list_index, element_id, subelement_id, 
                                     env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_signed_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    return_value.l = get_signed_lelem2(list_id, list_index, 
                                      embedded_list_id, embedded_list_index,
                                      element_id, subelement_id, 
                                      env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_unsigned_lelem(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    return_value.ul = get_unsigned_lelem(list_id, list_index, element_id, subelement_id, 
                                        env_ptr->env_info);
    return (return_value);
}

static VALUE wrap_get_unsigned_lelem2(SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    ITEM_ID         list_id;
    ITEM_ID         list_index;
    ITEM_ID         embedded_list_id;
    ITEM_ID         embedded_list_index;
    ITEM_ID         element_id;
    ITEM_ID         subelement_id;

    list_id = *params->v.ul_p;
    params = params->next_sym;

    list_index = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.ul_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;
    params = params->next_sym;

    subelement_id = *params->v.ul_p;
    return_value.ul = get_unsigned_lelem2(list_id, list_index, 
                                        embedded_list_id, embedded_list_index,
                                        element_id, subelement_id, 
                                        env_ptr->env_info);
    return (return_value);
}

static VALUE
wrap_get_double_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	double			*valuep;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.d_p;

	return_value.l = get_double_value (var_id, var_member_id, valuep, 
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_double_value: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_double_value: *valuep = %f\n", *valuep);

	return (return_value);
}

static VALUE
wrap_ret_double_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.d = ret_double_value (var_id, var_member_id,
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "ret_double_value: return_value.d = %lf\n", return_value.d);

	return (return_value);
}

static VALUE
wrap_put_double_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	double			value;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	value = *params->v.d_p;

	return_value.l = put_double_value (var_id, var_member_id, value, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_double_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


static VALUE
wrap_get_signed_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	long			*valuep;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.l_p;

	return_value.l = get_signed_value (var_id, var_member_id, valuep, 
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_signed_value: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_signed_value: param3: *valuep = %ld\n", *valuep);

	return (return_value);
}

static VALUE
wrap_ret_signed_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.l = ret_signed_value (var_id, var_member_id,
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "ret_signed_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}

static VALUE
wrap_put_signed_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	long			value;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	value = *params->v.l_p;

	return_value.l = put_signed_value (var_id, var_member_id, value, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_signed_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


static VALUE
wrap_get_unsigned_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	unsigned long	*valuep;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.ul_p;

	return_value.l = get_unsigned_value (var_id, var_member_id, valuep, 
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned_value: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned_value: param3: *valuep = %lu\n", *valuep);
			
	return (return_value);
}

static VALUE
wrap_ret_unsigned_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.ul = ret_unsigned_value (var_id, var_member_id,
			env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "ret_unsigned_value: return_value.ul = %ld\n", return_value.ul);

	return (return_value);
}

static VALUE
wrap_put_unsigned_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	unsigned long	value;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	value = *params->v.ul_p;

	return_value.l = put_unsigned_value (var_id, var_member_id, value, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


static VALUE
wrap_get_string_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	char			*valuep;
	long			*lengthp;
	char			string_format[60];

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.c_p;
	params = params->next_sym;
	lengthp = params->v.l_p;

	return_value.l = get_string_value (var_id, var_member_id, valuep, 
			lengthp, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_string_value: return_value.l = %ld\n", return_value.l);
	sprintf(string_format, "get_string_value: param3: valuep = %%-.%lds \n", *lengthp);
        LOGC_INFO(CPM_FF_DDSERVICE, string_format, valuep);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_string_value: param4: *lengthp = %ld\n", *lengthp);
			
	return (return_value);
}


static VALUE
wrap_put_string_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	char			*valuep;
	long			length;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	valuep = params->v.c_p;
	params = params->next_sym;
	length = *params->v.l_p;
    /*
	 * Strip off the country code prepended by the Tokenizer if one exists.
	 */
	if (*valuep == COUNTRY_CODE_MARK) {
	  valuep += 4;
	 }

	return_value.l = put_string_value (var_id, var_member_id, valuep, 
			length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


static VALUE
wrap_get_date_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	char			*datap;
	long			*lengthp;
	char date_format[60];

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	datap = params->v.c_p;
	params = params->next_sym;
	lengthp = params->v.l_p;

	return_value.l = get_date_value (var_id, var_member_id, datap, 
			lengthp, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_date_value: return_value.l = %ld\n", return_value.l);
	sprintf(date_format, "get_date_value: param3: datap = %%-.%lds \n", *lengthp);
        LOGC_INFO(CPM_FF_DDSERVICE, date_format, datap);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_date_value: param4: *lengthp = %ld\n", *lengthp);

	return (return_value);
}


static VALUE
wrap_put_date_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;
	char			*datap;
	long			length;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;
	params = params->next_sym;
	datap = params->v.c_p;
	params = params->next_sym;
	length = *params->v.l_p;
    /*
	 * Strip off the country code prepended by the Tokenizer if one exists.
	 */
	if (*datap == COUNTRY_CODE_MARK) {
	  datap += 4;
	 }

	return_value.l = put_date_value (var_id, var_member_id, datap, 
			length, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value: return_value.l = %ld\n", return_value.l);

	return (return_value);
}


static VALUE
wrap_assign (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			dest_id, src_id;
	ITEM_ID			dest_member_id, src_member_id;

	dest_id = *params->v.ul_p;
	params = params->next_sym;
	dest_member_id = *params->v.ul_p;
	params = params->next_sym;
	src_id = *params->v.ul_p;
	params = params->next_sym;
	src_member_id = *params->v.ul_p;

	return_value.l = assign (dest_id, dest_member_id, src_id, src_member_id, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "assign: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_read_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.l = read_value (var_id, var_member_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "read_value: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_send_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			var_id;
	ITEM_ID			var_member_id;

	var_id = *params->v.ul_p;
	params = params->next_sym;
	var_member_id = *params->v.ul_p;

	return_value.l = send_value (var_id, var_member_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "send_value: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_send_all_values (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = send_all_values (env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "send_all_values: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_response_code (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	UINT32			*resp_codep;
	ITEM_ID			*itemp;
	ITEM_ID			*member_idp;

	resp_codep = (UINT32*)params->v.ul_p;
	params = params->next_sym;
	itemp = params->v.ul_p;
	params = params->next_sym;
	member_idp = params->v.ul_p;

	get_response_code (resp_codep, itemp, member_idp, env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code: *resp_codep = %u\n", *resp_codep);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code: *itemp = %lu\n", *itemp);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code: *member_idp = %lu\n", *member_idp);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_get_comm_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.ul = get_comm_error (env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_comm_error: return_value.ul = %lu\n", return_value.ul);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_abort_on_all_comm_errors (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	abort_on_all_comm_errors (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_all_comm_errors: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_abort_on_all_response_codes (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	abort_on_all_response_codes (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_all_response_codes: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_abort_on_comm_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = abort_on_comm_error (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_comm_error: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_abort_on_response_code (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = abort_on_response_code (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_response_code: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_fail_on_all_comm_errors (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	fail_on_all_comm_errors (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_all_comm_errors: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_fail_on_all_response_codes (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	fail_on_all_response_codes (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_all_response_codes: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_fail_on_comm_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = fail_on_comm_error (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_comm_error: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_fail_on_response_code (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = fail_on_response_code (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_response_code: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_retry_on_all_comm_errors (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	retry_on_all_comm_errors (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_all_comm_errors: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_retry_on_all_response_codes (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	retry_on_all_response_codes (env_ptr->env_info);
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_all_response_codes: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_retry_on_comm_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = retry_on_comm_error (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_comm_error: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_retry_on_response_code (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = retry_on_response_code (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_response_code: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_status_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			id;
	ITEM_ID			member_id;
	UINT32			status;
	char			*out_buf;
	long			max_length;

	id = *params->v.ul_p;
	params = params->next_sym;
	member_id = *params->v.ul_p;
	params = params->next_sym;
	status = *params->v.ul_p;
	params = params->next_sym;
	out_buf = params->v.c_p;
	params = params->next_sym;
	max_length = *params->v.l_p;

	return_value.l = get_status_string (id, member_id, status, out_buf,
			max_length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: param4: out_buf = %s\n", out_buf);
			
	return (return_value);
}


static VALUE
wrap_get_comm_error_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	UINT32			error_no;
	char			*out_buf;
	long			max_length;

	error_no = *params->v.ul_p;
	params = params->next_sym;
	out_buf = params->v.c_p;
	params = params->next_sym;
	max_length = *params->v.l_p;

	return_value.l = get_comm_error_string (error_no, out_buf, max_length, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_comm_error_string: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_comm_error_string: out_buf = %s\n", out_buf);
			
	return (return_value);
}


static VALUE
wrap_get_response_code_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			id;
	ITEM_ID			member_id;
	UINT32			code;
	char			*out_buf;
	long			max_length;

	id = *params->v.ul_p;
	params = params->next_sym;
	member_id = *params->v.ul_p;
	params = params->next_sym;
	code = *params->v.ul_p;
	params = params->next_sym;
	out_buf = params->v.c_p;
	params = params->next_sym;
	max_length = *params->v.l_p;

	return_value.l = get_response_code_string (id, member_id, code, 
			out_buf, max_length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: return_value.l = %ld\n", return_value.l);
	if (return_value.l == 0)
	{
                LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param4: out_buf = %s\n", out_buf);
	}
	else {
                LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param4: out_buf = Invalid\n" );
	}
	return (return_value);
}

static VALUE
wrap_list_delete_element (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long	listID;
    int             index;

	listID = *params->v.ul_p;
    params = params->next_sym;
    index = *params->v.l_p;

	return_value.l = list_delete_element (listID, index, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_list_delete_element: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_list_delete_element2 (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long	listID, embedded_list_id;
    int             index, embedded_index;

	listID = *params->v.ul_p;
    params = params->next_sym;

    index = *params->v.l_p;
    params = params->next_sym;

	embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_index = *params->v.l_p;
    params = params->next_sym;

	return_value.l = list_delete_element2 (listID, index, 
        embedded_list_id, embedded_index, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_list_delete_element2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_list_create_element (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long	listID;
    unsigned long   element_id;
    int index;

	listID = *params->v.ul_p;
    params = params->next_sym;

    index = *params->v.l_p;
    params = params->next_sym;

    element_id = *params->v.ul_p;

	return_value.l = list_create_element (listID, index, element_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_list_create_element: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_list_create_element2 (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long	listID;
    unsigned long   embedded_list_id, embedded_element_id;
    int index, embedded_list_index;

	listID = *params->v.ul_p;
    params = params->next_sym;

    index = *params->v.l_p;
    params = params->next_sym;

    embedded_list_id = *params->v.ul_p;
    params = params->next_sym;

    embedded_list_index = *params->v.l_p;
    params = params->next_sym;

    embedded_element_id = *params->v.ul_p;

	return_value.l = list_create_element2 (listID, index, 
        embedded_list_id, embedded_list_index, embedded_element_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_list_create_element2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_plot_create (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long	plotID;

	plotID = *params->v.ul_p;

	return_value.l = plot_create (plotID, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_plot_create: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_plot_destroy (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	long	plot_handle;

	plot_handle = *params->v.l_p;

	return_value.l = plot_destroy (plot_handle, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_plot_destroy: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_plot_display (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	long	plot_handle;

	plot_handle = *params->v.l_p;

	return_value.l = plot_display (plot_handle, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_plot_display: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_plot_edit (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
    long            plot_handle;
    unsigned long   waveform_id;

	plot_handle = *params->v.l_p;
	params = params->next_sym;
    waveform_id = *params->v.ul_p;

	return_value.l = plot_edit (plot_handle, waveform_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_plot_edit: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_display_builtin_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	long			error;

	error = *params->v.l_p;

	return_value.l = display_builtin_error (error, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "display_builtin_error: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_display_message (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;

	return_value.l = display_message (prompt, var_ids, var_member_ids,
			id_count, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "display_message: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_display_dynamics (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;

	return_value.l = display_dynamics (prompt, var_ids, var_member_ids,
			id_count, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "display_dynamics: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_display_comm_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = display_comm_error (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "display_comm_error: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_display_response_code (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			id;
	ITEM_ID			member_id;
	UINT32			code;

	id = *params->v.ul_p;
	params = params->next_sym;
	member_id = *params->v.ul_p;
	params = params->next_sym;
	code = *params->v.ul_p;

	return_value.l = display_response_code (id, member_id, code, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "display_response_code: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_delayfor (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	long			duration;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;

	duration = *params->v.l_p;
	params = params->next_sym;
	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;

	return_value.l = delayfor (duration, prompt, var_ids, var_member_ids,
			id_count, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_edit_device_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;
	ITEM_ID			param_id;
	ITEM_ID			param_member_id;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;
	params = params->next_sym;
	param_id = *params->v.ul_p;
	params = params->next_sym;
	param_member_id = *params->v.ul_p;

	return_value.l = edit_device_value (prompt, var_ids, var_member_ids,
			id_count, param_id, param_member_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_edit_local_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;
	char			*localvar;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;
	params = params->next_sym;
	localvar = params->v.c_p;
	/*
	 * Strip off the country code prepended by the Tokenizer.
	 */
	if (localvar[0] == COUNTRY_CODE_MARK) {
		localvar += 4;
	}

	return_value.l = edit_local_value (prompt, var_ids, var_member_ids,
			id_count, localvar, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "edit_local_value: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_select_from_menu (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;
	char			*options;
	long			*selection;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;
	params = params->next_sym;
	options = params->v.c_p;
	params = params->next_sym;
	selection = params->v.l_p;

	return_value.l = select_from_menu (prompt, var_ids, var_member_ids,
			id_count, options, selection, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param6: *selection = %ld\n", *selection);
			
	return (return_value);
}

static VALUE
wrap_menu_display (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			menuid;
	long			*selection;

	menuid = *params->v.l_p;
	params = params->next_sym;

	prompt = params->v.c_p;
	params = params->next_sym;

	selection = params->v.l_p;

	return_value.l = menu_display(menuid, prompt, selection, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "menu_display: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "menu_display: *selection = %ld\n", *selection);
			
	return (return_value);
}


static VALUE
wrap_get_acknowledgement (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*prompt;
	ITEM_ID			*var_ids;
	ITEM_ID			*var_member_ids;
	long			id_count;

	prompt = params->v.c_p;
	params = params->next_sym;
	var_ids = params->v.ul_p;
	params = params->next_sym;
	var_member_ids = params->v.ul_p;
	params = params->next_sym;
	id_count = *params->v.l_p;

	return_value.l = get_acknowledgement (prompt, var_ids, var_member_ids,
			id_count, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_acknowledgement: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


/*ARGSUSED*/
static VALUE
wrap_is_NaN (SYM *params, ENVIRON *env_ptr)
{
	VALUE 		return_value;
	double 		double_value;

	double_value = *params->v.d_p;

	return_value.l = is_NaN (double_value);

        LOGC_INFO(CPM_FF_DDSERVICE, "is_NaN: return_value.l = %ld\n", return_value.l);
			
	return return_value;
}

   
/*ARGSUSED*/
static VALUE
wrap_NaN_value (SYM *params, ENVIRON *env_ptr)
{
	VALUE 		return_value;
	double 		*double_ptr;

	double_ptr = params->v.d_p;

	return_value.l = NaN_value (double_ptr);

        LOGC_INFO(CPM_FF_DDSERVICE, "NaN_value: return_value.l = %ld\n", return_value.l);
			
	return return_value;
}


static VALUE
wrap_get_stddict_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	UINT32			id;
	char			*str_buf;
	long			maxlen;

	id = *params->v.ul_p;
	params = params->next_sym;
	str_buf = params->v.c_p;
	params = params->next_sym;
	maxlen = *params->v.l_p;

	return_value.l = get_stddict_string (id, str_buf, maxlen, 
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_stddict_string: return_value.l = %ld\n", return_value.l);
	if (return_value.l == 0)
	{
                LOGC_INFO(CPM_FF_DDSERVICE, "get_stddict_string: param2: str_buf = %s\n", str_buf);
	}
	else
	{
                LOGC_INFO(CPM_FF_DDSERVICE, "get_stddict_string: param2: str_buf = Invalid\n");
	}
			
	return (return_value);
}


static VALUE
wrap_resolve_block_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			member;

	member = *params->v.ul_p;

	return_value.ul = resolve_block_ref(member, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_block_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return return_value;
}


static VALUE
wrap_resolve_param_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			member;

	member = *params->v.ul_p;

	return_value.ul = resolve_param_ref(member, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return return_value;
}

static VALUE
wrap_resolve_local_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			member;

	member = *params->v.ul_p;

	return_value.ul = resolve_local_ref(member, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_local_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return return_value;
}


static VALUE
wrap_resolve_param_list_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			member_id;

	member_id = *params->v.ul_p;

	return_value.ul = resolve_param_list_ref(member_id, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_list_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return return_value;
}


static VALUE
wrap_resolve_array_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			array_id;
	UINT32			subindex;

	array_id = *params->v.ul_p;
	params = params->next_sym;
	subindex = *params->v.ul_p;

	return_value.ul = resolve_array_ref (array_id, subindex,
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return (return_value);
}

static VALUE
wrap_resolve_list_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			list_id;

	list_id = *params->v.ul_p;

	return_value.ul = resolve_list_ref (list_id,
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_list_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return (return_value);
}


static VALUE
wrap_resolve_record_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			collection_id;
	ITEM_ID			member;

	collection_id = *params->v.ul_p;
	params = params->next_sym;
	member = *params->v.ul_p;

	return_value.ul = resolve_record_ref (collection_id, member,
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return (return_value);
}

static VALUE
wrap_resolve_selector_ref (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	ITEM_ID			collection_id;
	unsigned long   selector;
    unsigned long   additional;

	collection_id = *params->v.ul_p;
	params = params->next_sym;
	selector = *params->v.ul_p;
	params = params->next_sym;
    additional = *params->v.ul_p;

	return_value.ul = resolve_selector_ref (collection_id, selector, additional,
			env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "resolve_selector_ref: return_value.ul = 0x%lx\n", return_value.ul);
			
	return (return_value);
}


/* ARGSUSED */
static VALUE
wrap_get_resolve_status (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = get_resolve_status (env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_resolve_status: return_value.l = %ld\n", return_value.l);
			

	return (return_value);
}


static VALUE
wrap_get_dds_error (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*out_buf;
	long			max_length;

	out_buf = params->v.c_p;
	params = params->next_sym;
	max_length = *params->v.l_p;

	return_value.l = get_dds_error (out_buf, max_length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_dds_error: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_dds_error: param1: out_buf = %s\n", out_buf);
			
	return (return_value);
}


static VALUE
wrap_get_float (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = get_float (params->v.f_p, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_float: return_value.l = %ld\n", return_value.l);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_float: param1: *valuep = %f\n", *params->v.f_p);
			
	return (return_value);
}


static VALUE
wrap_put_float (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = put_float (*params->v.d_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_float: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_double (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = get_double (params->v.d_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_double: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_double: param1: *valuep = %f\n", *params->v.d_p);
			
	return (return_value);
}


static VALUE
wrap_put_double (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = put_double (*params->v.d_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_double: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_signed (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = get_signed (params->v.l_p, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_signed: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_signed: param1: *valuep = %ld\n", *params->v.l_p);
			
	return (return_value);
}


static VALUE
wrap_put_signed (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = put_signed (*params->v.l_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_signed: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_unsigned (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = get_unsigned (params->v.ul_p, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned: return_value.l = %ld\n", return_value.l);
                LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned: param1: *valuep = %lu\n", *params->v.ul_p);
			
	return (return_value);
}


static VALUE
wrap_put_unsigned (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;

	return_value.l = put_unsigned (*params->v.ul_p, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*valuep;
	long			*lengthp;
	char			string_format[60];

	valuep = params->v.c_p;
	params = params->next_sym;
	lengthp = params->v.l_p;

	return_value.l = get_string (valuep, lengthp, env_ptr->env_info);
			
        LOGC_INFO(CPM_FF_DDSERVICE, "get_string: return_value.l = %ld\n", return_value.l);
	sprintf(string_format, "get_string: param1: valuep = %%-.%lds \n", *lengthp);
        LOGC_INFO(CPM_FF_DDSERVICE, string_format, valuep);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_string: param2: *lengthp = %ld\n", *lengthp);
			
	return (return_value);
}


static VALUE
wrap_put_string (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*valuep;
	long			length;

	valuep = params->v.c_p;
	params = params->next_sym;
	length = *params->v.l_p;
    /*
	 * Strip off the country code prepended by the Tokenizer if one exists.
	 */
	if (*valuep == COUNTRY_CODE_MARK) {
	  valuep += 4;
	 }

	return_value.l = put_string (valuep, length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_string: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}


static VALUE
wrap_get_date (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*datap;
	long			*lengthp;
	char			date_format[60];

	datap = params->v.c_p;
	params = params->next_sym;
	lengthp = params->v.l_p;

	return_value.l = get_date (datap, lengthp, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "get_date: return_value.l = %ld\n", return_value.l);
	sprintf(date_format, "get_date: param1: datap = %%-.%lds \n", *lengthp);
        LOGC_INFO(CPM_FF_DDSERVICE, date_format, datap);
        LOGC_INFO(CPM_FF_DDSERVICE, "get_date: param2: *lengthp = %ld\n", *lengthp);

	return (return_value);
}


static VALUE
wrap_put_date (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	char			*datap;
	long			length;

	datap = params->v.c_p;
	params = params->next_sym;
	length = *params->v.l_p;
    /*
	 * Strip off the country code prepended by the Tokenizer if one exists.
	 */
	if (*datap == COUNTRY_CODE_MARK) {
	  datap += 4;
	 }

	return_value.l = put_date (datap, length, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "put_date: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

/*
 * These next two functions aren't used anywhere but 
 * are included for the sake of completeness.
 */

static VALUE
wrap_bi_rcsim_set_response_code(SYM *params, ENVIRON *env_ptr)
{
	VALUE 		return_value;
	int			network_handle;
	int			device_stn_address;
	UINT32		response_code;


	network_handle = *params->v.l_p;
	params = params->next_sym;
	device_stn_address = (int)*params->v.l_p;
	params = params->next_sym;
	response_code = *params->v.ul_p;

	return_value.l = bi_rcsim_set_response_code(network_handle, device_stn_address, 
													response_code, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE,
		"bi_rcsim_set_response_code: return_value.l = %ld\n", return_value.l);
			
	return return_value;
}


static VALUE
wrap_bi_rcsim_set_comm_err(SYM *params, ENVIRON *env_ptr)
{
	VALUE 		return_value;
	int			network_handle;
	int			device_stn_address;
	UINT32		comm_error;


	network_handle = *params->v.l_p;
	params = params->next_sym;
	device_stn_address = (int)*params->v.l_p;
	params = params->next_sym;
	comm_error = *params->v.ul_p;

	return_value.l = bi_rcsim_set_comm_err(network_handle, device_stn_address, 
													comm_error, env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE,"bi_rcsim_set_comm_err: return_value.l = %ld\n", return_value.l);
			
	return return_value;
}


static VALUE
wrap_debug_line (SYM *params, ENVIRON *env_ptr)
{
	VALUE			return_value;
	unsigned long			line;

    line = *params->v.ul_p;
	return_value.l = 0;

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_debug_line: return_value.l = %ld\n", return_value.l);
			
    _l(line, env_ptr);

	return (return_value);
}


/* FF-900 5.1 Additions Wrappers */

static VALUE
wrap_assign2 (SYM *params, ENVIRON *env_ptr)
{
//, LONG_T, "assign2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T,
//   U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},

//extern long   assign2(unsigned long   dstBlockId,
//               unsigned long   dstBlockInstance,
//               unsigned long   dstParamId,
//               unsigned long   dstMemberId,
//               unsigned long   srcBlockId,
//               unsigned long   srcBlockInstance,
//               unsigned long   srcParamId,
//               unsigned long   srcMemberId);

	
	VALUE			return_value;
	unsigned long	dstBlkId, dstBlkInst, dstParamId, dstMemId;
	unsigned long	srcBlkId, srcBlkInst, srcParamId, srcMemId;

	dstBlkId = *params->v.ul_p;
	params = params->next_sym;
	dstBlkInst = *params->v.ul_p;
	params = params->next_sym;
	dstParamId = *params->v.ul_p;
	params = params->next_sym;
	dstMemId = *params->v.ul_p;
	params = params->next_sym;
	srcBlkId = *params->v.ul_p;
	params = params->next_sym;
	srcBlkInst = *params->v.ul_p;
	params = params->next_sym;
	srcParamId = *params->v.ul_p;
	params = params->next_sym;
	srcMemId = *params->v.ul_p;

	return_value.l = assign2(dstBlkId, dstBlkInst, dstParamId, dstMemId, 
		                     srcBlkId, srcBlkInst, srcParamId, srcMemId,
			                 env_ptr->env_info);

        LOGC_INFO(CPM_FF_DDSERVICE, "assign2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_delayfor2 (SYM *params, ENVIRON *env_ptr)
{
//, LONG_T, "delayfor2", {LONG_T, PTR_TO_CHAR_T,U_LONG_T,
//                                           U_LONG_T, U_LONG_T, U_LONG_T, LONG_T}},

//extern long   delayfor2(long            duration,
//                 char           *prompt,
//                 unsigned long   blockIds[],
//                 unsigned long   blockInstances[],
//                 unsigned long   paramIds[],
//                 unsigned long   memberIds[],
//                 long            count);
	VALUE	return_value;
	char	*prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	duration, count;

	duration = *params->v.l_p;
	params = params->next_sym;
	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;

	return_value.l = delayfor2( duration, prompt, blkIds, blkInsts, paramIds, memIds, count, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "delayfor2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_display_dynamics2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "display_dynamics2", {PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, 
	//                                                                    ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},

//extern long   display_dynamics2(char           *prompt,
//                         unsigned long   blockIds[],
//                         unsigned long   blockInstances[],
//                         unsigned long   paramIds[],
//                         unsigned long   memberIds[],
//                         long            count);

	VALUE	return_value;
	char	*prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	count;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;

	return_value.l = display_dynamics2( prompt, blkIds, blkInsts, paramIds, memIds, count, env_ptr->env_info ); 

        LOGC_INFO(CPM_FF_DDSERVICE, "display_dynamics2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_display_message2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "display_message2", {PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
      //                               ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},

//	extern long   display_message2(char           *prompt,
//                         unsigned long   blockIds[],
//                         unsigned long   blockInstances[],
//                         unsigned long   paramIds[],
//                         unsigned long   memberIds[],
//                         long            count);

	VALUE	return_value;
	char	*prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	count;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;

	return_value.l = display_message2( prompt, blkIds, blkInsts, paramIds, memIds, count, env_ptr->env_info ); 

        LOGC_INFO(CPM_FF_DDSERVICE, "display_message2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}
									 
static VALUE
wrap_edit_device_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T,  "edit_device_value2", {PTR_TO_CHAR_T,ARRAY_OF_U_LONG_T, 
      //                                                        ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
		//													  LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},

//extern long   edit_device_value2(char           *prompt,
//                          unsigned long   blockIds[],
//                          unsigned long   blockInstances[],
//                          unsigned long   paramIds[],
//                          unsigned long   memberIds[],
//                          long            count,
//                          unsigned long   blockId,
//                          unsigned long   blockInstance,
//                          unsigned long   paramId,
//                          unsigned long   memberId);

	VALUE	return_value;
	char	      *prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	      count;
	unsigned long blkId, blkInst, paramId, memId;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;
	params = params->next_sym;
	blkId    = *params->v.ul_p;
	params = params->next_sym;
	blkInst = *params->v.ul_p;
	params = params->next_sym;
	paramId  = *params->v.ul_p;
	params = params->next_sym;
	memId    = *params->v.ul_p;

	return_value.l = edit_device_value2( prompt, blkIds, blkInsts, paramIds, memIds, count, 
		                                 blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_edit_local_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "edit_local_value2",{PTR_TO_CHAR,ARRAY_OF_U_LONG_T,ARRAY_OF_U_LONG_T,
	  //                                                    U_LONG_T,PTR_TO_CHAR}},  

//extern long   edit_local_value2(char           *prompt,
//                         unsigned long   blockIds[],
//                         unsigned long   blockInstances[],
//                         unsigned long   paramIds[],
//                         unsigned long   memberIds[],
//                         long            count,
//                         char           *localVariable);

	VALUE	return_value;
	char	*prompt, *localVar;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	count;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;
	params = params->next_sym;
	localVar = params->v.c_p;
	/*
	 * AR 3481
	 * Strip off the country code prepended by the Tokenizer.
	 */
	if (localVar[0] == COUNTRY_CODE_MARK) {
		localVar += 4;
	}

	return_value.l = edit_local_value2( prompt, blkIds, blkInsts, paramIds, memIds, count, localVar, env_ptr->env_info ); 

        LOGC_INFO(CPM_FF_DDSERVICE, "edit_local_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_acknowledgement2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_acknowledgement2",{PTR_TO_CHAR, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
	  //                                                                       ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},    

//extern long   get_acknowledgement2(char           *prompt,
//                            unsigned long   blockIds[],
//                            unsigned long   blockInstances[],
//                            unsigned long   paramIds[],
//                            unsigned long   memberIds[],
//                            long            count);

	VALUE	return_value;
	char	*prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	count;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;

	return_value.l = get_acknowledgement2( prompt, blkIds, blkInsts, paramIds, memIds, count, env_ptr->env_info ); 

        LOGC_INFO(CPM_FF_DDSERVICE, "get_acknowledgement2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_block_instance_by_object_index (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_block_instance_by_object_index",{U_LONG_T,PTR_TO_U_LONG_T}},

//extern long   get_block_instance_by_object_index(unsigned long   objectIndex,
//                                          unsigned long  *relativeIndex);

	VALUE	return_value;
	unsigned long	objIdx, *relIdx;

	objIdx = *params->v.l_p;
	params = params->next_sym;
	relIdx = (unsigned long *)params->v.l_p;

	return_value.l = get_block_instance_by_object_index( objIdx, relIdx, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_block_instance_by_object_index: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_block_instance_by_tag (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_block_instance_by_tag", {U_LONG_T,PTR_TO_CHAR,PTR_TO_U_LONG_T}},

//extern long   get_block_instance_by_tag(unsigned long   blockId,
//                                 char           *blockTag,
//                                 unsigned long  *relativeIndex);
	VALUE	return_value;
	unsigned long blkId, *relIdx;
	char	*blkTag;

	blkId = *params->v.ul_p;
	params = params->next_sym;
	blkTag = params->v.c_p;
	params = params->next_sym;
	relIdx = params->v.ul_p;

	return_value.l = get_block_instance_by_tag( blkId, blkTag, relIdx, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_block_instance_by_tag: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_block_instance_count (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_block_instance_count", {U_LONG_T,PTR_TO_U_LONG_T}},

//extern long   get_block_instance_count(unsigned long   blockId,
//                                unsigned long  *count);

	VALUE	return_value;
	unsigned long	blkId, *count;

	blkId = *params->v.ul_p;
	params = params->next_sym;
	count = params->v.ul_p;

	return_value.l = get_block_instance_count( blkId, count, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_block_instance_count: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_get_date_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_date_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,
	  //                                                 PTR_TO_CHAR_T, PTR_TO_LONG_T }},

//extern long   get_date_value2(unsigned long   blockId,
//                       unsigned long   blockInstance,
//                       unsigned long   paramId,
//                       unsigned long   memberId,
//                       char           *value,
//                       long           *length);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	char			*val;
	long			*len;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.c_p;
	params  = params->next_sym;
	len     = params->v.l_p;

	return_value.l = get_date_value2( blkId, blkInst, paramId, memId, val, len, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_date_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_double_value2 (SYM *params, ENVIRON *env_ptr)
{
//, LONG_T, "get_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_DOUBLE}},

//extern long   get_double_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         double         *value);


	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	double			*val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.d_p;

	return_value.l = get_double_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_double_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_float_value2 (SYM *params, ENVIRON *env_ptr)
{
//, LONG_T, "get_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_FLOAT}},

//extern long   get_float_value2(unsigned long   blockId,
//                        unsigned long   blockInstance,
//                        unsigned long   paramId,
//                        unsigned long   memberId,
//                        float          *value);


	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	float			*val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.f_p;

	return_value.l = get_float_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_float_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_signed_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_LONG_T}},

//extern long   get_signed_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         long           *value);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	long			*val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.l_p;

	return_value.l = get_signed_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );


        LOGC_INFO(CPM_FF_DDSERVICE, "get_signed_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_get_string_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_string_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,PTR_TO_LONG_T}},

//extern long   get_string_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         char           *value,
//                         long           *length);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	char			*val;
	long			*len;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.c_p;
	params  = params->next_sym;
	len		= params->v.l_p;


	return_value.l = get_string_value2( blkId, blkInst, paramId, memId, val, len, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_string_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_get_unsigned_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "get_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_U_LONG_T}},

//extern long   get_unsigned_value2(unsigned long   blockId,
//                           unsigned long   blockInstance,
//                           unsigned long   paramId,
//                           unsigned long   memberId,
//                           unsigned long  *value);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	unsigned long	*val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.ul_p;

	return_value.l = get_unsigned_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_ret_double_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, DOUBLE_T, "ret_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.d = ret_double_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "ret_double_value2: return_value.f = %f\n", return_value.d);
			
	return (return_value);
}

static VALUE
wrap_ret_float_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, FLOAT_T, "ret_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.f = ret_float_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "ret_float_value2: return_value.f = %f\n", return_value.f);
			
	return (return_value);
}

static VALUE
wrap_ret_signed_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "ret_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.l = ret_signed_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "ret_signed_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_ret_unsigned_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, U_LONG_T, "ret_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.ul = ret_unsigned_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "ret_unsigned_value2: return_value.ul = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_put_date_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_date_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,LONG_T}},

//extern long   put_date_value2(unsigned long   blockId,
//                       unsigned long   blockInstance,
//                       unsigned long   paramId,
//                       unsigned long   memberId,
//                       char           *value,
//                       long            length);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	char			*val;
	long			len;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.c_p;
	params  = params->next_sym;
	len		= *params->v.l_p;


	return_value.l = put_date_value2( blkId, blkInst, paramId, memId, val, len, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_put_double_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,DOUBLE_T}},

//extern long   put_double_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         double          value);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	double			val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = *params->v.d_p;

	return_value.l = put_double_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_double_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);


}

static VALUE
wrap_put_float_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,DOUBLE_T}},

//extern long   put_float_value2(unsigned long   blockId,
//                        unsigned long   blockInstance,
//                        unsigned long   paramId,
//                        unsigned long   memberId,
//                        double          value);      //Todd ? double??

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	double			val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = *params->v.d_p;

	return_value.l = put_float_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_float_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_put_signed_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,LONG_T}},

//extern long   put_signed_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         long            value);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	long			val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = *params->v.l_p;

	return_value.l = put_signed_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_signed_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_put_string_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_string_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,LONG_T}},

//extern long   put_string_value2(unsigned long   blockId,
//                         unsigned long   blockInstance,
//                         unsigned long   paramId,
//                         unsigned long   memberId,
//                         char           *value,
//                         long            length);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	char			*val;
	long			len;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = params->v.c_p;
	params  = params->next_sym;
	len     = *params->v.l_p;

	return_value.l = put_string_value2( blkId, blkInst, paramId, memId, val, len, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_put_unsigned_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "put_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

//extern long   put_unsigned_value2(unsigned long   blockId,
//                           unsigned long   blockInstance,
//                           unsigned long   paramId,
//                           unsigned long   memberId,
//                           unsigned long   value);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;
	unsigned long			val;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;
	params  = params->next_sym;
	val     = *params->v.ul_p;

	return_value.l = put_unsigned_value2( blkId, blkInst, paramId, memId, val, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_read_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "read_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

//extern long   read_value2(unsigned long   blockId,
//                   unsigned long   blockInstance,
//                   unsigned long   paramId,
//                   unsigned long   memberId);

	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.l = read_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "read_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_select_from_menu2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "select_from_menu2", {PTR_TO_CHAR_T,U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T
			                                               //LONG_T,PTR_TO_CHAR_T,PTR_TO_LONG_T}},
//extern long   select_from_menu2(char           *prompt,
//                         unsigned long   blockIds[],
//                         unsigned long   blockInstances[],
//                         unsigned long   paramIds[],
//                         unsigned long   memberIds[],
//                         long            count,
//                         char           *options,
//                         long           *selection);
	VALUE	return_value;
	char	*prompt;
	unsigned long *blkIds, *blkInsts, *paramIds, *memIds;
	long	count;
	char	*options;
	long	*selection;

	prompt = params->v.c_p;
	params = params->next_sym;
	blkIds = params->v.ul_p;
	params = params->next_sym;
	blkInsts = params->v.ul_p;
	params = params->next_sym;
	paramIds = params->v.ul_p;
	params = params->next_sym;
	memIds = params->v.ul_p;
	params = params->next_sym;
	count = *params->v.l_p;
	params = params->next_sym;
	options = params->v.c_p;
	params = params->next_sym;
	selection = params->v.l_p;

	return_value.l = select_from_menu2( prompt, blkIds, blkInsts, paramIds, memIds, count, options, selection, env_ptr->env_info ); 

        LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_send_value2 (SYM *params, ENVIRON *env_ptr)
{
	//, LONG_T, "send_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

//extern long   send_value2(unsigned long   blockId,
//                   unsigned long   blockInstance,
//                   unsigned long   paramId,
//                   unsigned long   memberId);
	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.l = send_value2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "send_value2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);

}

static VALUE
wrap_resolve_param_ref2 (SYM *params, ENVIRON *env_ptr)
{
    //U_LONG_T, "resolve_param_ref2", {U_LONG_T, U_LONG_T, U_LONG_T}},
	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;

	return_value.l = resolve_param_ref2( blkId, blkInst, paramId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_resolve_param_ref2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_resolve_local_ref2 (SYM *params, ENVIRON *env_ptr)
{
    //U_LONG_T, "resolve_local_ref2", {U_LONG_T, U_LONG_T, U_LONG_T}},
	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;

	return_value.l = resolve_local_ref2( blkId, blkInst, paramId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_resolve_local_ref2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_resolve_array_ref2 (SYM *params, ENVIRON *env_ptr)
{
    //U_LONG_T, "resolve_array_ref2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, subindex;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	subindex= *params->v.ul_p;

	return_value.l = resolve_array_ref2( blkId, blkInst, paramId, subindex, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_resolve_array_ref2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_resolve_record_ref2 (SYM *params, ENVIRON *env_ptr) 
{
    //U_LONG_T, "resolve_record_ref2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	VALUE	return_value;
	unsigned long	blkId, blkInst, paramId, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	paramId = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.l = resolve_record_ref2( blkId, blkInst, paramId, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_resolve_record_ref2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

static VALUE
wrap_resolve_block_ref2 (SYM *params, ENVIRON *env_ptr) 
{
    //U_LONG_T, "resolve_block_ref2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	VALUE	return_value;
	unsigned long	blkId, blkInst, memId;

	blkId   = *params->v.ul_p;
	params  = params->next_sym;
	blkInst = *params->v.ul_p;
	params  = params->next_sym;
	memId   = *params->v.ul_p;

	return_value.l = resolve_block_ref2( blkId, blkInst, memId, env_ptr->env_info );

        LOGC_INFO(CPM_FF_DDSERVICE, "wrap_resolve_block_ref2: return_value.l = %ld\n", return_value.l);
			
	return (return_value);
}

#define INCLUDE_BLTIN_TBL_RAW	1
#include "int_bltin_tbl.c"

void
init_a_bi_symbols(void)
{
	
	size_t heap_size;
	RHEAP *heap;
	ENVIRON env;
	SYM *arg, *prev_arg, *first_arg;
	SYM *bltin, *prev_bltin, *first_bltin;
	const struct bi_proto *proto;
	TYPE_SPEC *type;
	int i;

	lock(BUILTIN_LOCK);		/* Only one process in here at a time */

	if ( bi_func_symbols_a != 0 ) {	/* If already initialized,	*/
		unlock(BUILTIN_LOCK);			/*   just return			*/
		return;
	}

	/*	??? also put this in init_b_bi_symbols
	 *  direct error messages to stdout
	 */

	/**
	 * Build an environment to store these symbols.
	 *	NOTE: BYTES_PER_SYM bytes per builtin is a good generous guess.
	 */

#ifdef CPM_64_BIT_OS
        // Doubled the heap size to handle built-ins for 64 Bit
        heap_size = ((BYTES_PER_SYM * NELEM(built_in)) + HEAP_OVERHEAD) * 2;
#else
        // Bharath please confirm if this is fine for 32 Bit
        heap_size = ((BYTES_PER_SYM * NELEM(built_in)) + HEAP_OVERHEAD);
#endif
	heap = (RHEAP *) xmalloc(heap_size);
	rheapinit(heap, (long) heap_size);
	environ_init(&env, heap);

	first_bltin = prev_bltin = NULL;
    for (proto = built_in; proto->bltin_ptr != NULL; proto++) {

        first_arg = prev_arg = NULL;
        for (i = 0; i < NUM_ARGS; i++) {
            type = proto->arg_type[i];
            if (type == NULL)
                break;

            arg = bld_abstr_sym(0, type, &env);

            if (prev_arg) {
                prev_arg->next_sym = arg;
            } else {
                first_arg = arg;
            }
            prev_arg = arg;
        }

		bltin = bld_bltin_decl(0, proto->bltin_name, first_arg, &env);

		bltin->type->typ = METH_BUILT_IN_TYPE;
		bltin->type->madeof = proto->ret_type;	/* return type */

		bltin->v.bltin_p->bi = proto->bltin_ptr;

		if (prev_bltin) {
			prev_bltin->next_sym = bltin;
		} else {
			first_bltin = bltin;
		}
        prev_bltin = bltin;
	}

	bi_func_symbols_a = first_bltin;
	unlock(BUILTIN_LOCK) ;
}


