
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CHAR_TOK = 258,
     FLOAT_TOK = 259,
     LONG_TOK = 260,
     INT_TOK = 261,
     DOUBLE_TOK = 262,
     UNSIGNED_TOK = 263,
     SHORT_TOK = 264,
     SIGNED_TOK = 265,
     BREAK_TOK = 266,
     CASE_TOK = 267,
     CONTINUE_TOK = 268,
     DEFAULT_TOK = 269,
     DO_TOK = 270,
     ELSE_TOK = 271,
     FOR_TOK = 272,
     IF_TOK = 273,
     RETURN_TOK = 274,
     SWITCH_TOK = 275,
     WHILE_TOK = 276,
     IDENTIFIER = 277,
     CHAR_CONST = 278,
     INT_CONST = 279,
     FLOAT_CONST = 280,
     STRING_CONST = 281,
     ASSIGN_TOK = 282,
     LOG_OR_TOK = 283,
     LOG_AND_TOK = 284,
     EQOP_TOK = 285,
     RELOP_TOK = 286,
     SHIFT_TOK = 287,
     MULTOP_TOK = 288,
     MINUSMINUS = 289,
     PLUSPLUS = 290,
     UNARY = 291
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 93 "./INT_PARS.Y"

    SYM 			*sym_ptr;
    EXPRESS 		*exp_ptr;
    STATEMENT 		*stmt_ptr;
    STMT_PTRS 		*stmt_ptrs;
    unsigned int	line;

	struct {
    	int 		 typ;
		unsigned int l;
    } typ_tok;

	struct {
    	int 		 o;			/* operator */
		unsigned int l;
    } op_tok;

	struct {
    	char 		 *str;
    	unsigned int l;
    } str_tok;




/* Line 1676 of yacc.c  */
#line 114 "./int_pars.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


