/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      imalloc.c - interpreter malloc checking routine.
 *
 ******************************************************/

#include "int_inc.h"

/*********************************************************************
 *
 *	Name: imalloc_chk()
 *	ShortDesc: allocates memory from the heap and checks returned pointer
 *
 *	Description:
 *		Allocate memory from heap and check for valid pointer.  If the
 *		pointer is NULL, then call method_abort with appropriate message.
 *
 *	Inputs:
 * 		env_ptr - the environment pointer.
 *		nb		- number of bytes to allocate.
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		pointer to allocated memory.
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void *
imalloc_chk(register ENVIRON *env_ptr, register size_t nb)
{
    void *loc_ptr;

    loc_ptr = rmalloc(env_ptr->heap_ptr, nb);

	if (loc_ptr == (void *)0) {
		method_abort("Out of memory in imalloc_chk", env_ptr->env_info);
	}

	return loc_ptr;
}


/*********************************************************************
 *
 *	Name: irealloc_chk()
 *	ShortDesc: reallocates memory in the heap and checks returned pointer
 *
 *	Description:
 *		Reallocate memory in the heap and check for valid pointer.  If the
 *		pointer is NULL, then call method_abort with appropriate message.
 *
 *	Inputs:
 * 		env_ptr - the environment pointer.
 *		ptr		- pointer to existing memory block.
 *		nb		- number of bytes to allocate.
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		pointer to allocated memory.
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void *
irealloc_chk(register ENVIRON *env_ptr, register char *ptr, size_t nb)
{
	void *loc_ptr;
	 
	loc_ptr = rrealloc(env_ptr->heap_ptr, ptr, nb);
		  
	if (loc_ptr == (void *)0) {
		method_abort("Out of memory in irealloc_chk", env_ptr->env_info);
    }

	return loc_ptr;
}
