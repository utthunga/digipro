%{
/************************************
 *
 *	@(#)int_scan.l	30.4  30  21 Apr 1997
 *
 *	int_scan.l - lex input to scan C-language tokens
 ************************************/

/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */
#include    <stdlib.h>
#include	<ctype.h>
#include	<string.h>
#include    <malloc.h>

#include	"std.h"
#include	"ddi_lib.h"
#include	"ddsbltin.h"
#include	"int_loc.h"
#include	"app_xmal.h"
#include	"int_pars.h"
#include    "err_gen.h"
 
/*
 * Pointer into buffer containing method definition. Used to
 * keep track of how much of the buffer has been given to the
 * lexical analyser and what to give it the next time it asks
 * for more data.
 */
char *lexptr = 0;

/*
 * In certain cases, we want to use our own xmalloc
 * where checking for pointer existence is done.
 */

#undef malloc
#define malloc(x) xmalloc(x)

#undef YY_DECL
#ifdef YY_USE_PROTOS
#define YY_DECL int my_yylex(ENVIRON *env_ptr)
#else
#define YY_DECL int my_yylex(env_ptr) ENVIRON *env_ptr;
#endif

#undef YY_READ_BUF_SIZE		/* methods are small */
#define YY_READ_BUF_SIZE	512

#undef YY_BUF_SIZE
#define YY_BUF_SIZE	(YY_READ_BUF_SIZE * 2) /* size of default input buffer */

#undef YY_INPUT
#define YY_INPUT(buf,result,max_size)	\
	fill_lex_buf(buf, &result, max_size)
	
#define YY_USER_ACTION	{ yylval.line = linenum; }
#define YY_USER_INIT	linenum = colnum = 1;

static void fill_lex_buf P(( YY_CHAR *buf, int *result, int max_size ));

static unsigned int linenum = 1;
static int colnum = 1;

%}

 /* Definitions for recognizing floating point constants. */
DIGIT-SEQUENCE		[0-9]+
FLOAT-SUFFIX		([fF])?
EXPONENT			[eE][\+\-]?{DIGIT-SEQUENCE}
FRACTIONAL			({DIGIT-SEQUENCE}?\.{DIGIT-SEQUENCE})|({DIGIT-SEQUENCE}\.)
DOUBLE				({FRACTIONAL}{EXPONENT}?)|({DIGIT-SEQUENCE}{EXPONENT})
FLOAT				{DOUBLE}{FLOAT-SUFFIX}

 /* Definitions for recognizing integer constants. */
UNSIGNED-SUFFIX		([uU])?
LONG-SUFFIX			([lL])?
INT-SUFFIX			({UNSIGNED-SUFFIX}{LONG-SUFFIX})|({LONG-SUFFIX}{UNSIGNED-SUFFIX})
DECIMAL				[1-9][0-9]*{INT-SUFFIX}
OCTAL				0[0-7]*{INT-SUFFIX}
HEX					0[xX][0-9a-fA-F]+{INT-SUFFIX}

 /* Definitions for recognizing escape sequences. */
ESCAPE-SEQUENCE		\\((['\"\?\\abfnrtv])|([0-7]{1,3})|(x[0-9a-fA-F]+))

 /* Definitions for recognizing character constants. */
C-CHAR-SEQUENCE		[^'\\\n]|{ESCAPE-SEQUENCE}
CHARACTER			'{C-CHAR-SEQUENCE}'

 /* Definitions for recognizing string literals. */
S-CHAR-SEQUENCE		([^\"\\\n]|{ESCAPE-SEQUENCE})*
STRING				\"{S-CHAR-SEQUENCE}\"

 /* Definitions for recognizing identifiers. */
IDENTIFIER			[_a-zA-Z][0-9_a-zA-Z]*

%%

signed		{ return SIGNED_TOK; }
unsigned	{ return UNSIGNED_TOK; }
char		{ return CHAR_TOK; }
short		{ return SHORT_TOK; }
int			{ return INT_TOK; }
long		{ return LONG_TOK; }
float		{ return FLOAT_TOK; }
double		{ return DOUBLE_TOK; }

break		{ return BREAK_TOK; }
case		{ return CASE_TOK; }
continue	{ return CONTINUE_TOK; }
default		{ return DEFAULT_TOK; }
do			{ return DO_TOK; }
else		{ return ELSE_TOK; }
for			{ return FOR_TOK; }
if			{ return IF_TOK; }
return		{ return RETURN_TOK; }
switch		{ return SWITCH_TOK; }
while		{ return WHILE_TOK; }

{IDENTIFIER}	{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return IDENTIFIER; }

{STRING}	{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return STRING_CONST; }

{CHARACTER}	{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return CHAR_CONST; }

{OCTAL}		{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return INT_CONST; }

{DECIMAL}	{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return INT_CONST; }

{HEX}		{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return INT_CONST; }

{FLOAT}		{ yylval.str_tok.str = imalloc_chk(env_ptr, strlen(yytext) + 1);
		  strcpy(yylval.str_tok.str, yytext);
		  yylval.str_tok.l = linenum;
		  return FLOAT_CONST; }

"]"		{ return ']'; }
"["		{ return '['; }
"("		{ return '('; }
")"		{ return ')'; }
"?"		{ return '?'; }
":"		{ return ':'; }
","		{ return ','; }
"{"		{ return '{'; }
"}"		{ return '}'; }
";"		{ return ';'; }
"++"	{ return PLUSPLUS; }
"--"	{ return MINUSMINUS; }
"!"		{ return '!'; }
"~"		{ return '~'; }
"&"		{ return '&'; }
"+"		{ return '+'; }
"-"		{ return '-'; }

"*"		{ yylval.op_tok.o = MULT;
		  yylval.op_tok.l = linenum;
		  return MULTOP_TOK; }
"/"		{ yylval.op_tok.o = DIV;
		  yylval.op_tok.l = linenum;
		  return MULTOP_TOK; }
"%"		{ yylval.op_tok.o = MOD;
		  yylval.op_tok.l = linenum;
		  return MULTOP_TOK; }

"<<"	{ yylval.op_tok.o = SHIFT_L;
		  yylval.op_tok.l = linenum;
		  return SHIFT_TOK; }
">>"	{ yylval.op_tok.o = SHIFT_R;
		  yylval.op_tok.l = linenum;
		  return SHIFT_TOK; }

"<"		{ yylval.op_tok.o = LT;
		  yylval.op_tok.l = linenum;
		  return RELOP_TOK; }
">"		{ yylval.op_tok.o = GT;
		  yylval.op_tok.l = linenum;
		  return RELOP_TOK; }
"<="	{ yylval.op_tok.o = LE;
		  yylval.op_tok.l = linenum;
		  return RELOP_TOK; }
">="	{ yylval.op_tok.o = GE;
		  yylval.op_tok.l = linenum;
		  return RELOP_TOK; }

"=="	{ yylval.op_tok.o = EQ;
		  yylval.op_tok.l = linenum;
		  return EQOP_TOK; }
"!="	{ yylval.op_tok.o = NEQ;
		  yylval.op_tok.l = linenum;
		  return EQOP_TOK; }

"^"		{ return '^'; }
"|"		{ return '|'; }
"&&"	{ return LOG_AND_TOK; }
"||"	{ return LOG_OR_TOK; }

"="		{ yylval.op_tok.o = ASSIGN;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"*="	{ yylval.op_tok.o = ASSIGN_MULT;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"/="	{ yylval.op_tok.o = ASSIGN_DIV;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"%="	{ yylval.op_tok.o = ASSIGN_MOD;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"+="	{ yylval.op_tok.o = ASSIGN_ADD;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"-="	{ yylval.op_tok.o = ASSIGN_SUB;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"<<="	{ yylval.op_tok.o = ASSIGN_SHIFT_L;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
">>="	{ yylval.op_tok.o = ASSIGN_SHIFT_R;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"&="	{ yylval.op_tok.o = ASSIGN_BIT_AND;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"^="	{ yylval.op_tok.o = ASSIGN_BIT_XOR;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }
"|="	{ yylval.op_tok.o = ASSIGN_BIT_OR;
		  yylval.op_tok.l = linenum;
		  return ASSIGN_TOK; }

\n		{ ++linenum; colnum = 1; }

\t		{ colnum += 9 - (colnum % 8); }

[ \r\v\f\b]	{ ; }

.		{
		  if ( isprint( *yytext ) )
		    error(env_ptr->env_info,  INVALID_CHARACTER, *yytext );
		  else
		    error(env_ptr->env_info,  INVALID_BIN_CHARACTER, *yytext);
		}

<<EOF>>		{ yy_init = 1; BEGIN 0; yyterminate(); }

%%

/*********************************************************************
 *	fill_lex_buf - gives lex its input depending upon parsing
 *			statements or programs
 *********************************************************************/

static void
fill_lex_buf(YY_CHAR *buf, int *result, int max_size)
{
	strncpy((char *) buf, lexptr, max_size - 1);
	buf[max_size - 1] = 0;
	*result = (int) strlen((char *) buf);
	lexptr += *result;
}

/*********************************************************************
 *
 *	Name: yywrap
 *
 *	ShortDesc: called when yylex() reaches end of file
 *
 *	Description:
 *		When yylex() reaches end of file, it calls yywrap(), which
 *		returns a value of 0 or 1.  
 *
 *		The return value 1 indicates that the program is done 
 *		and that there is no further input.
 *
 *		Returning the value 0, however, indicates continued input.
 *		yylex() assumes that yywrap() has opened another file, and
 *		it continues to read from 'yyin'.
 *
 *	Inputs:
 *		None
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		1 if no further input is available, 0 otherwise.
 *
 *	Author:
 *		Meysman
 *
 *********************************************************************/
int
yywrap()
{
	return 1;
}
