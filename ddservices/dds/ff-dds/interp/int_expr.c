
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      int_expr.c - interpret an expression tree
 ******************************************************/

#include "int_inc.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#ifdef TOK
#define error tok_interp_error
extern void tok_interp_error(va_alist);
#endif

/*
 * Local macros.
 */

/*
 * Macro for casting variable type.
 */
#define CAST_VALUE(nval, ntype, oval, otype, env)			\
	{														\
		switch (otype) {									\
		case METH_CHAR_TYPE:								\
			nval = (ntype) (int)oval.c;						\
			break;											\
		case METH_SHORT_TYPE:								\
			nval = (ntype) (int)oval.s;						\
			break;											\
		case METH_LONG_TYPE:								\
			nval = (ntype) oval.l;							\
			break;											\
		case METH_U_CHAR_TYPE:								\
			nval = (ntype) oval.uc;							\
			break;											\
		case METH_U_SHORT_TYPE:								\
			nval = (ntype) oval.us;							\
			break;											\
		case METH_U_LONG_TYPE:								\
			nval = (ntype) oval.ul;							\
			break;											\
		case METH_FLOAT_TYPE:								\
			nval = (ntype) oval.f;							\
			break;											\
		case METH_DOUBLE_TYPE:								\
			nval = (ntype) oval.d;							\
			break;											\
		default:											\
			CRASH_DBG();									\
			method_abort("Default reached in casting:", 	\
									env->env_info);	\
			/*NOTREACHED*/									\
		}													\
	}

/*
 * Macro for evaluating bitwise binary operator.
 */
#define  APPLY_BIT_BOPER(rval,type,oper,val1,val2)					\
	{																\
		switch (type->typ) {										\
		case METH_SHORT_TYPE:										\
			rval->s = val1.s oper val2.s;							\
			break;													\
		case METH_LONG_TYPE:										\
			rval->l = val1.l oper val2.l;							\
			break;													\
		case METH_U_SHORT_TYPE:										\
			rval->us = val1.us oper val2.us;						\
			break;													\
		case METH_U_LONG_TYPE:										\
			rval->ul = val1.ul oper val2.ul;						\
			break;													\
		default:													\
			CRASH_DBG();											\
			method_abort("Default reached in `oper` expression:",	\
										env_ptr->env_info);	\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating arithmetic binary operator.
 */
#define  APPLY_MATH_BOPER(rval,type,oper,val1,val2)					\
	{																\
		switch (type->typ) {										\
		case METH_SHORT_TYPE:										\
			rval->s = (short)(val1.s oper val2.s);					\
			break;													\
		case METH_LONG_TYPE:										\
			rval->l = (long)(val1.l oper val2.l);					\
			break;													\
		case METH_U_SHORT_TYPE:										\
			rval->us = (unsigned short)(val1.us oper val2.us);		\
			break;													\
		case METH_U_LONG_TYPE:										\
			rval->ul = (unsigned long)(val1.ul oper val2.ul);		\
			break;													\
		case METH_FLOAT_TYPE:										\
			rval->f = (float)(val1.f oper val2.f);					\
			break;													\
		case METH_DOUBLE_TYPE:										\
			rval->d = (double)(val1.d oper val2.d);					\
			break;													\
		default:													\
			CRASH_DBG();											\
			method_abort("Default reached in `oper` expression:",	\
										env_ptr->env_info);	\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating modulo operator.
 */
#define  APPLY_MOD_BOPER(rval,type,val1,val2)						\
	{																\
		switch (type->typ) {										\
		case METH_SHORT_TYPE:										\
			if (val2.s == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->s = val1.s % val2.s;								\
			break;													\
		case METH_U_SHORT_TYPE:										\
			if (val2.us == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->us = val1.us % val2.us;							\
			break;													\
		case METH_LONG_TYPE:										\
			if (val2.l == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->l = val1.l % val2.l;								\
			break;													\
		case METH_U_LONG_TYPE:										\
			if (val2.ul == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->ul = val1.ul % val2.ul;							\
			break;													\
		default:													\
			CRASH_DBG();											\
			method_abort("Default reached in `%` expression:",		\
									env_ptr->env_info);		\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating division operator.
 */
#define  APPLY_DIV_BOPER(rval,type,val1,val2)						\
	{																\
		switch (type->typ) {										\
		case METH_SHORT_TYPE:										\
			if (val2.s == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->s = val1.s / val2.s;								\
			break;													\
		case METH_U_SHORT_TYPE:										\
			if (val2.us == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->us = val1.us / val2.us;							\
			break;													\
		case METH_LONG_TYPE:										\
			if (val2.l == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->l = val1.l / val2.l;								\
			break;													\
		case METH_U_LONG_TYPE:										\
			if (val2.ul == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->ul = val1.ul / val2.ul;							\
			break;													\
		case METH_FLOAT_TYPE:										\
			if (val2.f == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->f = val1.f / val2.f;								\
			break;													\
		case METH_DOUBLE_TYPE:										\
			if (val2.d == 0) {										\
				method_abort("Divide by zero encountered:",			\
									env_ptr->env_info);		\
			}														\
			rval->d = val1.d / val2.d;								\
			break;													\
		default:													\
			CRASH_DBG();											\
			method_abort("Default reached in `%` expression:",		\
									env_ptr->env_info);		\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating bitwise unary operator.
 */
#define APPLY_BIT_UOPER(rval, rtype, stype, val2type, oper, val1, val2)	\
	{																	\
			switch (val2type->typ) {									\
			case METH_SHORT_TYPE:										\
				rval = (rtype) ((stype) val1 oper val2.s);				\
				break;													\
			case METH_U_SHORT_TYPE:										\
				rval = (rtype) ((stype) val1 oper val2.us);				\
				break;													\
			case METH_LONG_TYPE:										\
				rval = (rtype) ((stype) val1 oper (int) val2.l);		\
				break;													\
			case METH_U_LONG_TYPE:										\
				rval = (rtype) ((stype) val1 oper (int) val2.ul);		\
				break;													\
			default:													\
				CRASH_DBG();											\
				method_abort("Default reached in `oper` expression:",	\
												env_ptr->env_info);\
			}															\
	}

/*
 * Macro for evaluating arithmetic pre unary operator.
 */
#define  APPLY_MATH_PRE_UOPER(rval,type,foper,lval1)				\
	{																\
		switch (type->typ) {										\
		case METH_CHAR_TYPE:										\
			rval->c = foper(lval1.c_p);								\
			break;													\
		case METH_SHORT_TYPE:										\
			rval->s = foper(lval1.s_p);								\
			break;													\
		case METH_LONG_TYPE:										\
			rval->l = foper(lval1.l_p);								\
			break;													\
		case METH_U_CHAR_TYPE:										\
			rval->uc = foper(lval1.uc_p);							\
			break;													\
		case METH_U_SHORT_TYPE:										\
			rval->us = foper(lval1.us_p);							\
			break;													\
		case METH_U_LONG_TYPE:										\
			rval->ul = foper(lval1.ul_p);							\
			break;													\
		case METH_FLOAT_TYPE:										\
			rval->f = foper(lval1.f_p);								\
			break;													\
		case METH_DOUBLE_TYPE:										\
			rval->d = foper(lval1.d_p);								\
			break;													\
		default:													\
			CRASH_DBG();											\
	method_abort("Default reached in 'unary foperboper' expression:",\
											env_ptr->env_info);		\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating arithmetic post unary operator.
 */
#define  APPLY_MATH_POST_UOPER(rval,type,boper,lval1)				\
	{																\
		switch (type->typ) {										\
		case METH_CHAR_TYPE:										\
			rval->c = (lval1.c_p)boper;								\
			break;													\
		case METH_SHORT_TYPE:										\
			rval->s = (lval1.s_p)boper;								\
			break;													\
		case METH_LONG_TYPE:										\
			rval->l = (lval1.l_p)boper;								\
			break;													\
		case METH_U_CHAR_TYPE:										\
			rval->uc = (lval1.uc_p)boper;							\
			break;													\
		case METH_U_SHORT_TYPE:										\
			rval->us = (lval1.us_p)boper;							\
			break;													\
		case METH_U_LONG_TYPE:										\
			rval->ul = (lval1.ul_p)boper;							\
			break;													\
		case METH_FLOAT_TYPE:										\
			rval->f = (lval1.f_p)boper;								\
			break;													\
		case METH_DOUBLE_TYPE:										\
			rval->d = (lval1.d_p)boper;								\
			break;													\
		default:													\
			CRASH_DBG();											\
	method_abort("Default reached in 'unary foperboper' expression:",\
											env_ptr->env_info);		\
			/*NOTREACHED*/											\
		}															\
	}

/*
 * Macro for evaluating address-of operator.
 */
#define  APPLY_ADDRESSOF_OPER(rval,type,lval1)							\
	{																	\
		switch (type->typ) {											\
		case METH_CHAR_TYPE:											\
			rval->p.c_p = lval1.c_p;									\
			break;														\
		case METH_SHORT_TYPE:											\
			rval->p.s_p = lval1.s_p;									\
			break;														\
		case METH_LONG_TYPE:											\
			rval->p.l_p = lval1.l_p;									\
			break;														\
		case METH_U_CHAR_TYPE:											\
			rval->p.uc_p = lval1.uc_p;									\
			break;														\
		case METH_U_SHORT_TYPE:											\
			rval->p.us_p = lval1.us_p;									\
			break;														\
		case METH_U_LONG_TYPE:											\
			rval->p.ul_p = lval1.ul_p;									\
			break;														\
		case METH_FLOAT_TYPE:											\
			rval->p.f_p = lval1.f_p;									\
			break;														\
		case METH_DOUBLE_TYPE:											\
			rval->p.d_p = lval1.d_p;									\
			break;														\
		default:														\
			CRASH_DBG();												\
			method_abort("Default reached in `address of` expression:",	\
											env_ptr->env_info);	\
			/*NOTREACHED*/												\
		}																\
	}

/*********************************************************************
 *
 *	Name: cast()
 *	ShortDesc: Convert the variable-type internally.
 *
 *	Description:
 *		Cast changes the internal type of a variable to different type.
 *
 *	Inputs:
 *		val      - Variable value to be converted.
 *		old_type - The original type to be converted.
 *		new_type - The target type to convert to.
 *		env_ptr  - The environment to be used for casting variable.
 *
 *	Outputs:
 *		val - The internal type was changed to new type.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
cast(VALUE *val, TYPE_SPEC **old_type, int new_typ, ENVIRON *env_ptr)
{
	VALUE 		tmp;				/* Temp variable value */

	tmp = *val;

	switch (new_typ) {
	case METH_CHAR_TYPE:
		
		/*
		 * Cast from other types to character type.
		 */
		CAST_VALUE(val->c, char, tmp, (*old_type)->typ, env_ptr);
		*old_type = CHAR_T;
		break;

	case METH_U_CHAR_TYPE:

		/*
		 * Cast from other types to unsigned character type.
		 */
		CAST_VALUE(val->uc, unsigned char, tmp, (*old_type)->typ, env_ptr);
		*old_type = U_CHAR_T;
		break;

	case METH_SHORT_TYPE:
		
		/*
		 * Cast from other types to short type.
		 */
		CAST_VALUE(val->s, short, tmp, (*old_type)->typ, env_ptr);
		*old_type = SHORT_T;
		break;

	case METH_U_SHORT_TYPE:
		
		/*
		 * Cast from other types to unsigned short type.
		 */
		CAST_VALUE(val->us, unsigned short, tmp, (*old_type)->typ, env_ptr);
		*old_type = U_SHORT_T;
		break;

	case METH_LONG_TYPE:

		/*
		 * Cast from other types to long type.
		 */
		CAST_VALUE(val->l, long, tmp, (*old_type)->typ, env_ptr);
		*old_type = LONG_T;
		break;

	case METH_U_LONG_TYPE:

		/*
		 * Cast from other types to unsigned long type.
		 */
		CAST_VALUE(val->ul, unsigned long, tmp, (*old_type)->typ, env_ptr);
		*old_type = U_LONG_T;
		break;

	case METH_FLOAT_TYPE:

		/*
		 * Cast from other types to float type.
		 */
		CAST_VALUE(val->f, float, tmp, (*old_type)->typ, env_ptr);
		*old_type = FLOAT_T;
		break;

	case METH_DOUBLE_TYPE:

		/*
		 * Cast from other types to double type.
		 */
		CAST_VALUE(val->d, double, tmp, (*old_type)->typ, env_ptr);
		*old_type = DOUBLE_T;
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in casting:", env_ptr->env_info);
		/*NOTREACHED*/
	}
}


/*********************************************************************
 *
 *	Name: ieval_array()
 *	ShortDesc: Evaluates an array reference.
 *
 *	Description:
 *		Ieval_array first computes the array offset and then accesses 
 *		to get the value of the array reference. Finally returns the
 *		value and its type.
 *
 *	Inputs:
 *		array_ref  - Expression contained array symbol and values.
 *		subscripts - Expression contained subscripts or indices.
 *		ret_type   - A result type of an array reference.
 *		env_ptr    - The environment to be used for evaluating 
 *						the array reference.
 *		v_p		   - Pointer to an array reference value.
 *
 *	Outputs:
 *		v_p      - The pointer to an array reference value.
 *		ret_type - The type of an array reference value.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static void
ieval_array(EXPRESS *array_ref, EXPRESS *subscripts, TYPE_SPEC **ret_type,
	ENVIRON *env_ptr, VALUE_P *v_p)
{
	long 				mag;		/* for computing array offset */ 
	long 				offset = 0; /* stored offset to an array */
	VALUE 				exp_val;	/* store value of evaluated express */
	VALUE_P 			src_p;		/* ptr to beginning array values */
	TYPE_SPEC 			*exp_type;	/* Type of evaluated express value */
	TYPE_SPEC			*type_ptr; 	/* array dimension type and size */
	TYPE_SPEC			*mag_ptr;

	ASSERT_DBG(array_ref != (EXPRESS *)0);
	ASSERT_DBG(array_ref->type->typ == METH_ARRAY_TYPE);

	/*
	 * Calculate offset of an array reference.
	 */
	for (type_ptr = array_ref->type; (subscripts && type_ptr);
		subscripts = subscripts->next_exp, type_ptr = type_ptr->madeof) {

		/*
		 * Evaluate a subscript-expression to get its value.
		 */
		do_eval_expr(subscripts, &exp_type, env_ptr, &exp_val);

		/*
		 * a array subscript should not be a real or
		 * float number.
		 */
		ASSERT_DBG(INTEGRAL_TYPE(exp_type->typ));

		/*
		 * Cast to integer because array-size is integer.
		 * The subscript should also be the same type. 
		 *  
		 */
		ADJ_CAST(&exp_val, exp_type, METH_LONG_TYPE, env_ptr);

		/*
		 * Check and issue error message if the current 
		 * subscript is greater than array's dimension.
		 */
		if (exp_val.l >= (int) type_ptr->size) {
			error(env_ptr->env_info, SUBSCRIPT_OUT_OF_BOUNDS, exp_val.l, 
									(int)type_ptr->size - 1);
			method_abort("Press any key for stopping execution\n", 
									env_ptr->env_info);
		}

		/*
		 * Check and issue error message if the current
		 * subscript is less than zero.
		 */ 
		if (exp_val.l < 0) {
			error(env_ptr->env_info, NEGATIVE_SUBSCRIPT, exp_val.l);
			method_abort("Press any key for stopping execution\n", 
									env_ptr->env_info);
		}

		/*
		 * Offset multiplier for a single-dimensional array.
		 */
		mag = 1;

		/*
		 * This loop is executed only in the case of multi-dimensional array.
		 */
		for (mag_ptr = type_ptr->madeof;
			(mag_ptr->typ == METH_ARRAY_TYPE); mag_ptr = mag_ptr->madeof) {

			/*
			 * Multi-dimensional array is represented internally as a 
			 * single-dimensional (flattened) array.
			 * Compute the offset multiplier. This multiplier is multiplied 
			 * by the current subscript, to give the offset into
			 * the flattened array.
			 */
			mag *= mag_ptr->size;
		}

		/*
		 * Compute the current offset to the internal array
		 * representation for the current subscript.
		 */
		offset += exp_val.l * mag;
	}

	/*
	 * The number of subscripts should not exceed  
	 * the number of array dimensions.
	 */
	ASSERT_DBG(subscripts == NULL);

	/*
	 * Evaluate the subscript expression
	 * to get the value of the subscript.
	 */ 
	do_eval_expr(array_ref, &exp_type, env_ptr, &exp_val);
	ASSERT_DBG(exp_type->typ == METH_ARRAY_TYPE);
	src_p = exp_val.p;

	exp_type = type_ptr;
	/* 
	 * Find the base type of an array,
	 * if multi-dimensional array.
	 */
	while (type_ptr->madeof != NULL) {
		type_ptr = type_ptr->madeof;
	}

	/*
	 * Get the value and type for array reference.
	 */
	switch (type_ptr->typ) {
	case METH_CHAR_TYPE:
		v_p->c_p = &src_p.c_p[offset];
		break;
	case METH_U_CHAR_TYPE:
		v_p->uc_p = &src_p.uc_p[offset];
		break;
	case METH_SHORT_TYPE:
		v_p->s_p = &src_p.s_p[offset];
		break;
	case METH_U_SHORT_TYPE:
		v_p->us_p = &src_p.us_p[offset];
		break;
	case METH_LONG_TYPE:
		v_p->l_p = &src_p.l_p[offset];
		break;
	case METH_U_LONG_TYPE:
		v_p->ul_p = &src_p.ul_p[offset];
		break;
	case METH_FLOAT_TYPE:
		v_p->f_p = &src_p.f_p[offset];
		break;
	case METH_DOUBLE_TYPE:
		v_p->d_p = &src_p.d_p[offset];
		break;
	default:
		CRASH_DBG();
		method_abort("Default reached in getting value and type for array reference:", env_ptr->env_info);
		/*NOTREACHED*/
	}

	*ret_type = exp_type;
}


/*********************************************************************
 *
 *	Name: get_lvalue()
 *	ShortDesc: Get a symbol value and its type.
 *
 *	Description:
 *		Get_lvalue searchs for a reference symbol and returns its
 *		value and type.
 *
 *	Inputs:
 *		l_exp    - Expression contained symbol to be searched.
 *		env_ptr  - Environment to be used for searching.
 *
 *	Outputs:
 *		ret_type - The symbol type.
 *		v_p      - Pointer to symbol value.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static void
get_lvalue(EXPRESS *l_exp, TYPE_SPEC **ret_type, ENVIRON *env_ptr, VALUE_P *v_p)
{
	SYM 		*sym_ptr;				/* Ptr to symbol found */
	TYPE_SPEC 	*type = NULL;			/* Type of the symbol */

	switch(l_exp->op) {
	case SYMREF:

		/*
		 * Search for the symbol and then set the value and
		 * its type for returning.
		 */ 
		sym_ptr = lookup(l_exp->misc.name, env_ptr);
		ASSERT_DBG(sym_ptr != (SYM *)0);
		type = sym_ptr->type;
		*v_p = sym_ptr->v;
		break;

	case ARRAYREF:

		/*
		 * Evaluate an array reference to get the value and its type.
		 */
		ieval_array(l_exp->op1, l_exp->op2, &type, env_ptr, v_p);
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in getting lvalue:", env_ptr->env_info);
		/*NOTREACHED*/
	}

	*ret_type = type;
}


/*********************************************************************
 *
 *	Name: get_val()
 *	ShortDesc: Fetch a value from a provided value pointer. 
 *
 *	Description:
 *		Get_val Fetchs a value from the provided value pointer
 *		and stores it to the value buffer.
 *
 *	Inputs:
 *		val_p - Ptr to a value pointer to fetch value from.
 *		type  - The type of the value.
 *		env_ptr - Environment pointer to be be passed into method_abort.
 *
 *	Outputs:
 *		ret_val - Contained the fetched value. 
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
get_val(VALUE_P *val_p, TYPE_SPEC *type, VALUE *ret_val, ENVIRON *env_ptr)
{

	switch (type->typ) {
	case METH_CHAR_TYPE:
		ret_val->c = *(val_p->c_p);
		break;

	case METH_U_CHAR_TYPE:
		ret_val->uc = *(val_p->uc_p);
		break;

	case METH_SHORT_TYPE:
		ret_val->s = *(val_p->s_p);
		break;

	case METH_U_SHORT_TYPE:
		ret_val->us = *(val_p->us_p);
		break;

	case METH_LONG_TYPE:
		ret_val->l = *(val_p->l_p);
		break;

	case METH_U_LONG_TYPE:
		ret_val->ul = *(val_p->ul_p);
		break;

	case METH_FLOAT_TYPE:
		ret_val->f = *(val_p->f_p);
		break;

	case METH_DOUBLE_TYPE:
		ret_val->d = *(val_p->d_p);
		break;

	case METH_ARRAY_TYPE:
		memcpy( (char *)&ret_val->p, (char *)val_p, sizeof(*val_p) );
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in getting value:", env_ptr->env_info);
		/*NOTREACHED*/
	}

}


/*********************************************************************
 *	assign_val - assigns a value to a value_p
 *********************************************************************/

/*********************************************************************
 *
 *	Name: assign_val()
 *	ShortDesc: Assign a value to a value pointer.
 *
 *	Description:
 *		Assign_val stores a value to the value pointer base on
 *		the provided type.
 *
 *	Inputs:
 *		val  	 - Value to copy from. 
 *		type 	 - Type of value to copy from.
 *		env_ptr  - Environment to be used for passing to ADJ_CAST.
 *
 *	Outputs:
 *		val_p   - Pointer to storage for holding the value. 
 *		tar_typ - Type of value storing in the storage.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
assign_val(VALUE_P *val_p, int tar_typ, VALUE *val, TYPE_SPEC **type, ENVIRON *env_ptr)
{
	int src_typ = (*type)->typ;
        int uLongSize = sizeof(unsigned long);

	if ((tar_typ != METH_ARRAY_TYPE) &&
		(tar_typ != METH_PTR_TYPE)) {

		/*
		 * Cast the value to the target type.
	 	 */
		ADJ_CAST(val, *type, tar_typ, env_ptr);
	}

	switch (tar_typ) {
	case METH_CHAR_TYPE:
		*(val_p->c_p) = val->c;
		break;

	case METH_U_CHAR_TYPE:
		*(val_p->uc_p) = val->uc;
		break;

	case METH_SHORT_TYPE:
		*(val_p->s_p) = val->s;
		break;

	case METH_U_SHORT_TYPE:
		*(val_p->us_p) = val->us;
		break;

	case METH_LONG_TYPE:
		*(val_p->l_p) = val->l;
		break;

	case METH_U_LONG_TYPE:
#ifdef CPM_64_BIT_OS
                // For 64 bit machine, unsigned long takes upto 8 bytes which corrupts
                // the 32 bit data encoded in dd binary (which assumes unsigned long as 4 bytes)
                // To avoid the above issue, cast data and consume it as 32 bit instead of 64 bit
                // The fix has been made specifically to avoid misinterpretation of object id
                // which is of 4 bytes in length
                if (METH_LONG_TYPE == src_typ)
                {
                    *(val_p->ul_p) = (unsigned int) val->ul;
                }
                else
                {
                    *(val_p->ul_p) = val->ul;
                }
#else
               *(val_p->ul_p) = val->ul;
#endif
		break;

	case METH_FLOAT_TYPE:
		*(val_p->f_p) = val->f;
		break;

	case METH_DOUBLE_TYPE:
		*(val_p->d_p) = val->d;
		break;

	case METH_ARRAY_TYPE:
		*val_p = val->p;
		break;

	case METH_PTR_TYPE:
		*val_p = val->p;
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in assigning value:", env_ptr->env_info);
		/*NOTREACHED*/
	}
}


/*********************************************************************
 *
 *	Name: eval_bltin()
 *	ShortDesc: Evaluate a built-in function call.
 *
 *	Description:
 *		Eval_bltin looks up a built-in function name from the global
 *		symbol table and then goes through the built-in parameters 
 *		and constructs a parameter-list from the built-in function
 *		symbol. The bltin call arguments are evaluated and bonded
 *		to the parameter-list. Finally, the built-in function is 
 *		executed with the constructed parameter-list. 
 *
 *	Inputs:
 *		bltin_name - Built-in function name string.
 *		args	  - Arguments for a built-in function call. 
 *		env_ptr	  - Environment to run the built-in function.
 *		expr_val  - Hold the value for evaluating expression.
 *
 *	Outputs:
 *		env_ptr   - Contained a returned value and type of 
 *						the built-in function.
 *		ret_type  - Contained a return type of the built-in
 *						function.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static void
eval_bltin(char *bltin_name, EXPRESS *args, TYPE_SPEC **ret_type,
	ENVIRON *env_ptr, VALUE *expr_val)
{
	SYM 		*bltin_sym;			/* Ptr to built-in func symbol */
	SYM			*sym_ptr;			/* Para symbol from global table */	
	SYM			*param_ptr;			/* Created Para for binding arg */
	SYM			*param_lst;			/* Contained bonded parameters */ 
	BUILT_IN 	*bltin_ptr; 		/* Ptr to builtin struct in symbol */  
	TYPE_SPEC 	*expr_type;			/* Returned type of eval express */

	/*
	 * Search for a built-in function symbol in a global symbol
	 * table and access a pointer to the built-in prototype in
 	 * global symbol table. 
	 */
	bltin_sym = lookup_global(bltin_name, env_ptr);
	ASSERT_DBG(bltin_sym != NULL);
	bltin_ptr = bltin_sym->v.bltin_p;

	param_lst = (SYM *)0;

	/*
	 * Create a parameter-list from a built-in arguments.
	 */
	for (sym_ptr = bltin_ptr->params; (args && sym_ptr);
			args = args->next_exp, sym_ptr = sym_ptr->next_sym) {

		/*
		 * Create a parameter symbol from the built-in global symbol.
		 */
		param_ptr = cpy_sym(sym_ptr, SC_FUNC, env_ptr);

		/*
		 * Evaluate the built-in call argument to get its value
		 * and type.
		 */
		do_eval_expr(args, &expr_type, env_ptr, expr_val);

		/*
		 * Bind value and type of a built-in function call argument
		 * to a parameter symbol and link to a parameter list.
		 */
		if ((param_ptr->type->typ == METH_PTR_TYPE) && 
								(expr_type->typ == METH_LONG_TYPE)) {
			/*
			 * This parameter is a pointer type and it has value of
			 * zero which is equivalent to null. Assign the parameter 
			 * to be a null pointer.
			 */
			ASSERT_DBG(expr_val->l == 0);
			param_ptr->v.l_p = 0;
		}
		else {
			assign_val(&(param_ptr->v), 
					param_ptr->type->typ, expr_val, &expr_type, env_ptr);
		}
		param_lst = bld_sym_lst(env_ptr, param_ptr, param_lst);
	}

	/*
	 * Lists should be the same size!!! 
	 */
	ASSERT_DBG((void *) args == (void *) sym_ptr);
	ASSERT_DBG(bltin_sym->type->typ == METH_BUILT_IN_TYPE);

	/*
	 * Execute a built-in function with the constructed parameter-
	 * list and then fetch a return-value. The memory occupied 
	 * the parameter-list is finally freed back to the environment
	 * heap.
	 */
	env_ptr->return_value = bltin_ptr->bi(param_lst, env_ptr);
	env_ptr->return_type = bltin_sym->type->madeof;
	del_sym_lst(param_lst, SC_FUNC, env_ptr);			   

	*ret_type = env_ptr->return_type;
	*expr_val = env_ptr->return_value;
}


/*********************************************************************
 *
 *	Name: print_val()
 *	ShortDesc: prints value depending on the type of the value
 *
 *	Inputs:
 *		val  - value to print.
 *		type - type of the value.
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static void
print_val(VALUE *val, int type)
{
	switch (type) {
		case METH_CHAR_TYPE:
			if ((val->c == (char)(val->c & 0x7F)) && isprint(val->c)) {
                                LOGC_INFO(CPM_FF_DDSERVICE, "%3d = '%c' METH_CHAR_TYPE\n", val->c, val->c);
			}
			else {
                                LOGC_INFO(CPM_FF_DDSERVICE, "%3d = 0x%X METH_CHAR_TYPE\n", val->c, val->c);
			}
			break;
		case METH_U_CHAR_TYPE:
			if ((val->uc == (unsigned char) (val->uc & 0x7F))
												&& isprint(val->uc)) {
                                LOGC_INFO(CPM_FF_DDSERVICE, "%3u = '%c' METH_U_CHAR_TYPE\n", val->uc, val->uc);
			}
			else {
                                LOGC_INFO(CPM_FF_DDSERVICE, "%3u = 0x%X METH_U_CHAR_TYPE\n", val->uc, val->uc);
			}
			break;
		case METH_SHORT_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%d = 0x%X METH_SHORT_TYPE\n", val->s, val->s);
			break;
		case METH_U_SHORT_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%u = 0x%X METH_U_SHORT_TYPE\n", val->us, val->us);
			break;
		case METH_LONG_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%ld = 0x%lX METH_LONG_TYPE\n", val->l, val->l);
			break;
		case METH_U_LONG_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%lu = 0x%lX METH_U_LONG_TYPE\n", val->ul, val->ul);
			break;
		case METH_FLOAT_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%#.6g METH_FLOAT_TYPE\n", val->f);
			break;
		case METH_DOUBLE_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "%#.6g METH_DOUBLE_TYPE\n", val->d);
			break;
		case METH_ARRAY_TYPE:
                        LOGC_INFO(CPM_FF_DDSERVICE, "METH_ARRAY_TYPE\n");
			break;
		default:
                        LOGC_INFO(CPM_FF_DDSERVICE, "Unknown type!!!\n");
	}
}


/*********************************************************************
 *
 *	Name: eval_expr()
 *	ShortDesc: Evaluate an expression in structure-presentation.
 *
 *	Description:
 *		Eval_expr first evaluates an operator' sub-expression(s) or
 *		operand(s) and then applies the operator to its operands to
 *		get the result value.  If the operator cause change to its
 * 		operand value,  that operand value should be modified to 
 *		reflect the new value.
 *
 *	Inputs:
 *		exp     - An expression to be evaluated.
 * 		env_ptr - Environment to use for evaluating expression.
 *
 *	Outputs:
 *		type    - The type of the resulted expression.
 *		loc_val - The value of the resulted expression.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
/*
 * If DDSTEST is defined, the wrapper function eval_expr_wrap() is 
 * the entry point to call eval_expr() and there will be no other 
 * eval_expr calls from outside this module. However, if DDSTEST 
 * is not defined, the call to expr_expr() will be made directly(no 
 * wrapper) and may be outside this file. Therefore, the function 
 * should not be static.
 */
static
void
eval_expr(EXPRESS *exp, TYPE_SPEC **type, ENVIRON *env_ptr, VALUE *loc_val)
{
	int 			tmp_typ;			/* Temp var to hold type */
	int 			tmp_typ1;			/* Temp var to hold type of 1st operand */
	int 			tmp_typ2;			/* Temp var to hold type of 2nd operand */
	VALUE 			val1 = {0};				/* Eval value of 1st operand */
	VALUE			val2 = {0};				/* Eval value of 2nd operand */
	VALUE_P 		lvalue = {0};				/* Ptr to value storage */
	TYPE_SPEC		*type1 = NULL;				/* Eval type of 1st operand */
	TYPE_SPEC		*type2 = NULL;				/* Eval type of 2nd operand */
	TYPE_SPEC		*loc_type;			/* Ptr to sub-express type */
	TYPE_SPEC		*ret_type;			/* Ptr to resulted expr type */
	TYPE_SPEC 		*ltype = NULL;				/* Type for call get lvalue */


#ifdef lint
	val1.l = val2.l = 0;
	type1 = type2 = ltype = *type;
	lvalue.c_p = (char *) NULL;
#endif

	loc_type = ret_type = exp->type;

	/*
	 * Evaluate an operator' sub-expressions and if necessary, 
	 * cast the intermediate types of sub-expressions to the 
	 * resulted expression type.
	 */
	switch (exp->op) {
	case ADD:
	case SUB:
	case MULT:
	case DIV:
	case MOD:
	case BIT_OR:
	case BIT_AND:
	case BIT_XOR:

		/*
		 * It is a binary operator. Therefore, two operands are 
		 * evaluated to get the intermediate values. The values
		 * are casted to match the resulted expression type.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		eval_expr(exp->op2, &type2, env_ptr, &val2);

		ADJ_CAST(&val1, type1, loc_type->typ, env_ptr);
		ADJ_CAST(&val2, type2, loc_type->typ, env_ptr);
		break;

	case EQ:
	case NEQ:
	case LT:
	case LE:
	case GT:
	case GE:

		/*
		 * It is a binary operator. Therefore, two operands
		 * are evaluated to get the intermediate values.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		eval_expr(exp->op2, &type2, env_ptr, &val2);


		/*
		 * The intermediate sub-expression types are promoted to have
		 * the same type. The type with a greatest size will be favored
		 * so that a comparison can be made on two same type operands.
		 */
		tmp_typ1 = type1->typ;
		tmp_typ2 = type2->typ;
		INTEGRAL_PROMOTION(tmp_typ1);
		INTEGRAL_PROMOTION(tmp_typ2);
		tmp_typ = SAME_TYPE(tmp_typ1, tmp_typ2);

		RESULT_TYPE_PTR(tmp_typ, loc_type);

		ADJ_CAST(&val1, type1, loc_type->typ, env_ptr);
		ADJ_CAST(&val2, type2, loc_type->typ, env_ptr);
		break;

	case SHIFT_L:
	case SHIFT_R:

		/*
		 * It is a binary operator. Therefore, two operands are 
		 * evaluated to get the intermediate values. The values 
		 * are casted to match the resulted expression type.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		eval_expr(exp->op2, &type2, env_ptr, &val2);

		ADJ_CAST(&val1, type1, loc_type->typ, env_ptr);

		/*
		 * Cast the number of shifts to integer if 
		 * it has a character or short type.
		 */
		if ((type2->typ == METH_CHAR_TYPE) || 
								(type2->typ == METH_U_CHAR_TYPE)) { 
			ADJ_CAST(&val2, type2, METH_LONG_TYPE, env_ptr);
		}
		break;

	case COMMA:

		/*
		 * It is a binary operator. Therefore, two operands 
		 * are evaluated to get the intermediate values.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		eval_expr(exp->op2, &type2, env_ptr, &val2);
		break;

	case LOG_NOT:

		/*
		 * It is an unary operator. Therefore, one operand
		 * is evaluated to get the intermediate value. 
		 */

	case LOG_OR:
	case LOG_AND:

		/*
		 * It is a binary operator. However, only the first operand
		 * is evaluated to get the intermediate value. The second
		 * operand has to be evaluated depending on checking the 
		 * first operand and this step will be performed below.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		break;

	case UNARY_PLUS:
	case UNARY_MINUS:
	case COMPLEMENT:

		/*
		 * It is an unary operator. Therefore, only one operand 
		 * is evaluated to get the intermediate value. The value 
		 * is casted to match the resulted expression type.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		ADJ_CAST(&val1, type1, loc_type->typ, env_ptr);
		break;

	case COND:

		/*
		 * Only a first operand is evaluated to get an intermediate
		 * value.  The second or third operand has to be evaluated
		 * depending on checking the first operand and this step will
		 * be performed below.
		 */
		eval_expr(exp->op1, &type1, env_ptr, &val1);
		break;

	case ASSIGN_ADD:
	case ASSIGN_SUB:
	case ASSIGN_MULT:
	case ASSIGN_DIV:
	case ASSIGN_MOD:
	case ASSIGN_BIT_OR:
	case ASSIGN_BIT_AND:
	case ASSIGN_BIT_XOR:

		/*
		 * It is a binary operator. A second operand is evaluated
		 * to get its value. 
		 */
		eval_expr(exp->op2, &type2, env_ptr, &val2);

		/*
		 * The operator will cause change to its first operand value. 
		 * Therefore, get a first operand value storage for modification.
		 */
		get_lvalue(exp->op1, &ltype, env_ptr, &lvalue);
		ASSERT_DBG(ltype->typ == ret_type->typ);
		type1 = ltype;
		get_val(&lvalue, type1, &val1, env_ptr);

		tmp_typ1 = type1->typ;
		tmp_typ2 = type2->typ;
		INTEGRAL_PROMOTION(tmp_typ1);
		INTEGRAL_PROMOTION(tmp_typ2);
		tmp_typ = SAME_TYPE(tmp_typ1, tmp_typ2);

		RESULT_TYPE_PTR(tmp_typ, loc_type);

		ADJ_CAST(&val1, type1, loc_type->typ, env_ptr);
		ADJ_CAST(&val2, type2, loc_type->typ, env_ptr);
		break;

	case ASSIGN_SHIFT_L:
	case ASSIGN_SHIFT_R:

		/*
		 * It is a binary operator. A second operand is evaluated 
		 * to get its value. 
		 */
		eval_expr(exp->op2, &type2, env_ptr, &val2);

		/*
		 * The operator will cause change to its first operand value. 
		 * Therefore, get a first operand value storage for modification.
		 */
		get_lvalue(exp->op1, &ltype, env_ptr, &lvalue);
		ASSERT_DBG(ltype->typ == ret_type->typ);
		type1 = ltype;
		get_val(&lvalue, type1, &val1, env_ptr);

		/*
		 * Promote to integer types if the operand is character type.
		 */
		if ((type1->typ == METH_CHAR_TYPE) || 
									(type1->typ == METH_U_CHAR_TYPE)) {
			ADJ_CAST(&val1, type1, METH_LONG_TYPE, env_ptr);
		}
		if ((type2->typ == METH_CHAR_TYPE) || 
									(type2->typ == METH_U_CHAR_TYPE)) { 
			ADJ_CAST(&val2, type2, METH_LONG_TYPE, env_ptr);
		}
		break;

	case ASSIGN:

		/*
		 * It is a binary operator. A second operand is evaluated
		 * to get its value. 
		 */
		eval_expr(exp->op2, &type2, env_ptr, &val2);

		/*
		 * The operator will cause change to its first operand value. 
		 * Therefore, get a first operand value storage for modification.
		 */
		get_lvalue(exp->op1, &ltype, env_ptr, &lvalue);
		ASSERT_DBG(ltype->typ == ret_type->typ);
		break;

	case PRE_INCR:
	case PRE_DECR:
	case POST_INCR:
	case POST_DECR:

		/* 
		 * It is an unary operator and the operator will cause 
		 * change to its operand value. Therefore, get a operand 
		 * value storage for modification. 
		 */
		get_lvalue(exp->op1, &ltype, env_ptr, &lvalue);
		ASSERT_DBG(ltype->typ == ret_type->typ);
		break;

	case ADDRESS_OF:

		/* 
		 * It is an unary operator to get the address of 
		 * an operand.
		 */
		get_lvalue(exp->op1, &ltype, env_ptr, &lvalue);
		break;

	case SYMREF:
	case ARRAYREF:
	case CONSTREF:
	case FUNCREF:
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in evaluating operator' sub-expression:", 
														env_ptr->env_info);
		/*NOTREACHED*/
	}

	/*
	 * Produce a resulted value of an expression. The operator' 
	 * sub-expressions have been evaluated at this point. Just
	 * apply the operator to its operands to get the resulted 
	 * value of expression.
	 */
	switch (exp->op) {
	case ADD:
	case ASSIGN_ADD:
		APPLY_MATH_BOPER(loc_val,loc_type,+,val1,val2);
		break;

	case SUB:
	case ASSIGN_SUB:
		APPLY_MATH_BOPER(loc_val,loc_type,-,val1,val2);
		break;

	case MULT:
	case ASSIGN_MULT:
		APPLY_MATH_BOPER(loc_val,loc_type,*,val1,val2);
		break;

	case DIV:
	case ASSIGN_DIV:
		APPLY_DIV_BOPER(loc_val,loc_type,val1,val2);
		break;

	case MOD:
	case ASSIGN_MOD:
		APPLY_MOD_BOPER(loc_val,loc_type,val1,val2);
		break;

	case BIT_OR:
	case ASSIGN_BIT_OR:
		APPLY_BIT_BOPER(loc_val,loc_type,|,val1,val2);
		break;

	case BIT_AND:
	case ASSIGN_BIT_AND:
		APPLY_BIT_BOPER(loc_val,loc_type,&,val1,val2);
		break;

	case BIT_XOR:
	case ASSIGN_BIT_XOR:
		APPLY_BIT_BOPER(loc_val,loc_type,^,val1,val2);
		break;

	case EQ:
		APPLY_MATH_BOPER(loc_val,loc_type,==,val1,val2);
		break;

	case NEQ:
		APPLY_MATH_BOPER(loc_val,loc_type,!=,val1,val2);
		break;

	case LT:
		APPLY_MATH_BOPER(loc_val,loc_type,<,val1,val2);
		break;

	case LE:
		APPLY_MATH_BOPER(loc_val,loc_type,<=,val1,val2);
		break;

	case GT:
		APPLY_MATH_BOPER(loc_val,loc_type,>,val1,val2);
		break;

	case GE:
		APPLY_MATH_BOPER(loc_val,loc_type,>=,val1,val2);
		break;

	case SHIFT_L:
	case ASSIGN_SHIFT_L:
		switch (type1->typ) {
		case METH_SHORT_TYPE:
			APPLY_BIT_UOPER(loc_val->s,short,ushort,type2,<<,val1.s,val2);
			break;
		case METH_U_SHORT_TYPE:
			APPLY_BIT_UOPER(loc_val->us,ushort,ushort,type2,<<,val1.us,val2);
			break;
		case METH_LONG_TYPE:
			APPLY_BIT_UOPER(loc_val->l,long,ulong,type2,<<,val1.l,val2);
			break;
		case METH_U_LONG_TYPE:
			APPLY_BIT_UOPER(loc_val->ul,ulong,ulong,type2,<<,val1.ul,val2);
			break;
		default:
			CRASH_DBG();
			method_abort("Default reached in `<<` expression:",
												 env_ptr->env_info);
			/*NOTREACHED*/
		}
		break;

	case SHIFT_R:
	case ASSIGN_SHIFT_R:
		switch (type1->typ) {
		case METH_SHORT_TYPE:
			APPLY_BIT_UOPER(loc_val->s,short,ushort,type2,>>,val1.s,val2);
			break;
		case METH_U_SHORT_TYPE:
			APPLY_BIT_UOPER(loc_val->us,ushort,ushort,type2,>>,val1.us,val2);
			break;
		case METH_LONG_TYPE:
			APPLY_BIT_UOPER(loc_val->l,long,ulong,type2,>>,val1.l,val2);
			break;
		case METH_U_LONG_TYPE:
			APPLY_BIT_UOPER(loc_val->ul,ulong,ulong,type2,>>,val1.ul,val2);
			break;
		default:
			CRASH_DBG();
			method_abort("Default reached in `>>` expression:", 
												env_ptr->env_info);
			/*NOTREACHED*/
		}
		break;

	case COMMA:
		*loc_val = val2;
		break;

	case LOG_NOT:
		loc_val->l = !LOGICAL(val1, type1->typ);
		break;

	case LOG_OR:
		if (LOGICAL(val1, type1->typ)) {
			loc_val->l = TRUE;
		}
		else {
			eval_expr(exp->op2, &type2, env_ptr, &val2);

			loc_val->l = LOGICAL(val2, type2->typ);
		}
		break;

	case LOG_AND:
		if (!LOGICAL(val1, type1->typ)) {
			loc_val->l = FALSE;
		}
		else {
			eval_expr(exp->op2, &type2, env_ptr, &val2);

			loc_val->l = LOGICAL(val2, type2->typ);
		}
		break;

	case UNARY_PLUS:
		switch (loc_type->typ) {
		case METH_SHORT_TYPE:
			loc_val->s = val1.s;
			break;
		case METH_U_SHORT_TYPE:
			loc_val->us = val1.us;
			break;
		case METH_LONG_TYPE:
			loc_val->l = val1.l;
			break;
		case METH_U_LONG_TYPE:
			loc_val->ul = val1.ul;
			break;
		case METH_FLOAT_TYPE:
			loc_val->f = val1.f;
			break;
		case METH_DOUBLE_TYPE:
			loc_val->d = val1.d;
			break;
		default:
			CRASH_DBG();
			method_abort("Default reached in `unary +` expression:", 
											env_ptr->env_info);
			/*NOTREACHED*/
		}
		break;

	case UNARY_MINUS:
		switch (loc_type->typ) {
		case METH_SHORT_TYPE:
			loc_val->s = -val1.s;
			break;
		case METH_U_SHORT_TYPE:
			loc_val->us = (unsigned short) -((short)val1.us);
			break;
		case METH_LONG_TYPE:
			loc_val->l = -val1.l;
			break;
		case METH_U_LONG_TYPE:
			loc_val->ul = (unsigned long)(-(long)val1.ul);
			break;
		case METH_FLOAT_TYPE:
			loc_val->f = -val1.f;
			break;
		case METH_DOUBLE_TYPE:
			loc_val->d = -val1.d;
			break;
		default:
			CRASH_DBG();
			method_abort("Default reached in `unary -` expression:", 
											env_ptr->env_info);
			/*NOTREACHED*/
		}
		break;

	case COMPLEMENT:
		switch (loc_type->typ) {
		case METH_SHORT_TYPE:
			loc_val->s = (short) ~(unsigned short) val1.s;
			break;
		case METH_U_SHORT_TYPE:
			loc_val->us = ~val1.us;
			break;
		case METH_LONG_TYPE:
			loc_val->l = (long) ~(unsigned long) val1.l;
			break;
		case METH_U_LONG_TYPE:
			loc_val->ul = ~val1.ul;
			break;
		default:
			CRASH_DBG();
			method_abort("Default reached in `~` expression:", 
										env_ptr->env_info);
			/*NOTREACHED*/
		}
		break;

	case COND:
		if (LOGICAL(val1, type1->typ)) {
			eval_expr(exp->op2, &type2, env_ptr, &val2);
		}
		else {
			eval_expr(exp->misc.op3, &type2, env_ptr, &val2);
		}

		if (loc_type->madeof == NULL) {
			ADJ_CAST(&val2, type2, loc_type->typ, env_ptr);
		}
		*loc_val = val2;
		break;

	case PRE_INCR:
		APPLY_MATH_PRE_UOPER(loc_val,ltype,++,*lvalue);
		break;

	case PRE_DECR:
		APPLY_MATH_PRE_UOPER(loc_val,ltype,--,*lvalue);
		break;

	case POST_INCR:
		APPLY_MATH_POST_UOPER(loc_val,ltype,++,*lvalue);
		break;

	case POST_DECR:
		APPLY_MATH_POST_UOPER(loc_val,ltype,--,*lvalue);
		break;

	case ADDRESS_OF:
		APPLY_ADDRESSOF_OPER(loc_val,ltype,lvalue);
		break;

	case ASSIGN:
		*loc_val = val2;
		loc_type = type2;
		break;

	case SYMREF:
	case ARRAYREF:
		get_lvalue(exp, &ltype, env_ptr, &lvalue);
		get_val(&lvalue, ltype, loc_val, env_ptr);

		if ( exp ) {
			if (exp->misc.name == NULL) {
				/*
				 * Sun compiler prints null string as "(null)" but 
				 * some compilers prints null string as nothing. 
				 * Therefore, force both compilers to print "(null)"
				 * for file comparison without differences.
				 */
                                LOGC_INFO(CPM_FF_DDSERVICE, "        %s = ", "(null)");
			}
			else {
                                LOGC_INFO(CPM_FF_DDSERVICE, "        %s = ", exp->misc.name);
			}
		}
		print_val(loc_val, ltype->typ);
		break;

	case CONSTREF:
		get_val(&exp->misc.con_v, loc_type, loc_val, env_ptr);
		break;

	case FUNCREF:
		eval_bltin(exp->misc.name, exp->op1, &loc_type, env_ptr, loc_val);
		break;

	default:
		break;
	}

	/*
	 * Do the assignment for the operator that will cause change to
	 * its operand value. The effected operand is modified to have 
	 * the resulted value after applying the operator to its operands.
	 */
	switch (exp->op) {
	case ASSIGN_ADD:
	case ASSIGN_SUB:
	case ASSIGN_MULT:
	case ASSIGN_DIV:
	case ASSIGN_MOD:
	case ASSIGN_BIT_OR:
	case ASSIGN_BIT_AND:
	case ASSIGN_BIT_XOR:
	case ASSIGN_SHIFT_L:
	case ASSIGN_SHIFT_R:
	case ASSIGN:
		assign_val(&lvalue, ltype->typ, loc_val, &loc_type, env_ptr);


		if ((exp->op1->op == ARRAYREF) && (exp->op1->op1->op == SYMREF)) {
                        LOGC_INFO(CPM_FF_DDSERVICE, "            %s = ",
											exp->op1->op1->misc.name);
		}
		else {
                        LOGC_INFO(CPM_FF_DDSERVICE, "            %s = ",
											exp->op1->misc.name);
		}
		get_val(&lvalue, ltype, &val1, env_ptr);
		print_val(&val1, ltype->typ);
		break;

	default:
		break;
	}

	/*
	 * Set up return value and its type.
	 */
	ADJ_CAST(loc_val, loc_type, ret_type->typ, env_ptr);
	*type = loc_type;
}


/*********************************************************************
 *
 *	Name: eval_expr_wrap()
 *	ShortDesc: Wrapper function for eval_expr()
 *
 *	Description:
 *		Wrapper function for eval_expr().  Used only when DDSTEST is 
 *		defined, for verifying interpreter output.
 *
 *	Inputs:
 *		exp     -  not used
 * 		env_ptr -  not used
 *		type    -  not used
 *		loc_val -  not used
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
void
eval_expr_wrap( EXPRESS *exp, TYPE_SPEC **type, ENVIRON *env_ptr, VALUE *loc_val )
{

        LOGC_INFO(CPM_FF_DDSERVICE, "    eval_expr()\n");

	eval_expr(exp, type, env_ptr, loc_val);

}

