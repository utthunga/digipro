/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#include <malloc.h>
#include <stdio.h>
#include "std.h"
#include "sysparam.h"
#include "int_meth.h"
#include "app_xmal.h"
#include "rtn_code.h"
#include "cm_lib.h"
#include "pc_loc.h"
#include "int_loc.h"
#include "dds_lock.h"
#include "ddi_lib.h"
#include "int_diff.h"
#include "dds_upcl.h"

/*
 *	The storage for the per-method information.
 */

METH_INFO meth_info[MAX_METHODS];

static int close_param_cache(ENV_INFO2 *env_info, int close_option);

/**********************************************************************
 *
 *  Name: meth_assign_tag
 *  ShortDesc: Assign a method tag to a method execution.
 *
 *  Include:
 *		dds_meth.h
 *
 *  Description:
 *
 *		Meth_assign_tag assigns a tag to a method execution.  This tag
 *		is used to index into the meth_info array, which keeps track
 *		of the state of the executing method.
 *
 *  Inputs:
 *		None.
 *
 *  Outputs:
 *		new_tag - The tag assigned to the method.
 *
 *  Returns:
 *		BLTIN_NO_MEMORY, BLTIN_SUCCESS, METH_TOO_MANY_METHODS
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
meth_assign_tag(unsigned short *new_tag)
{

	METH_INFO      *meth_infop, *meth_info_endp;
	ERRLIST        *errlistp;
	int             list_size;

	/*
	 * Search the structures in the array for one that is not in use.
	 */

	meth_infop = meth_info;
	meth_info_endp = meth_infop + MAX_METHODS;
	for (; meth_infop < meth_info_endp; meth_infop++) {
		if (meth_infop->meth_inuse) {
			continue;
		}

		/*
		 * An empty structure!  Initialize it, and return the index.
		 */

		meth_infop->meth_inuse++;
		meth_infop->meth_processing_abort = 0;
		meth_infop->meth_default_comm_err = DEFAULT_IS_RETRY;
		meth_infop->meth_default_response_code = DEFAULT_IS_RETRY;
		errlistp = &meth_infop->meth_abort_comm_errs;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		errlistp = &meth_infop->meth_retry_comm_errs;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		errlistp = &meth_infop->meth_fail_comm_errs;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		errlistp = &meth_infop->meth_abort_response_codes;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		errlistp = &meth_infop->meth_retry_response_codes;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		errlistp = &meth_infop->meth_fail_response_codes;
		errlistp->errlist_count = 0;
		if (errlistp->errlist_list == NULL) {
			errlistp->errlist_limit = ERRLIST_GROWTH_CT;
			list_size = errlistp->errlist_limit *
				sizeof(*errlistp->errlist_list);
			errlistp->errlist_list = (UINT32 *) malloc((unsigned) list_size);
			if (errlistp->errlist_list == NULL) {
				return (BLTIN_NO_MEMORY);
			}
		}
		meth_infop->meth_resolve_nest_count = 0;
		meth_infop->meth_resolve_rc = 0;
		meth_infop->meth_collision_retry = METHOD_COLLISION_RETRY_CHOICE;
		meth_infop->meth_comm_retry = COMM_RETRY_CHOICE;
		meth_infop->meth_abort_list_count = 0;
		if (meth_infop->meth_abort_list == NULL) {
			meth_infop->meth_abort_list_limit = METH_ABORT_GROWTH_CT;
			list_size = meth_infop->meth_abort_list_limit *
				sizeof(*meth_infop->meth_abort_list);
			meth_infop->meth_abort_list = (ITEM_ID *) malloc((unsigned) list_size);
			if (meth_infop->meth_abort_list == NULL) {
				meth_infop->meth_abort_list_limit = 0;
				return (BLTIN_NO_MEMORY);
			}
		}


		/*
		 * Assign the new tag and return BLTIN_SUCCESS
		 */

		*new_tag = (unsigned short) (meth_infop - meth_info + 1);
		return (BLTIN_SUCCESS);
	}

	/*
	 * There were no empty slots. Return an error.
	 */

	return (METH_TOO_MANY_METHODS);
}


/**********************************************************************
 *
 *  Name: valid method_tag
 *  ShortDesc: Check the validity of a method_tag
 *
 *  Include:
 *		int_meth.h
 *
 *  Description:
 *
 *		valid method_tag checks the passed in method tag for validity.
 *
 *  Inputs:
 *		None.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		0 - invalid tag, 1 - valid tag	
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
valid_method_tag (unsigned short method_tag)
{
	return ((method_tag > 0) && (method_tag <= MAX_METHODS) && 
			(meth_info[method_tag-1].meth_inuse != 0));
}


/**********************************************************************
 *
 *  Name: meth start
 *  ShortDesc: Set up the environment for a method execution, execute the
 *				method, and close up the environment.
 *
 *  Include:
 *		dds_meth.h
 *
 *  Description:
 *
 *		Meth_start first sets up the environment for a method execution.
 *		This entails assigning a method tag, setting up the setjmp/longjmp
 *		information for handling aborts, and initializing the Parameter
 *		Cache for a method execution.  Then, meth start parses and
 *		executes the method by calling do_method().  Finally, meth start
 *		closes up the mthod environment.  This entails notifying the
 *		Parameter Cache that the method is complete (which may cause
 *		variables to be sent to the device), and de-assigning the method
 *		tag.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure 
 *		meth_id - The ID of the method to start
 *		do_init - if non-zero, do the method initialization.
 *				if zero, this is a sub-method, do not initialize
 *		close_option - Specifies whether to discard, save, or send values
 *				at the close of the method (this option is only used
 *				if do_init is nonzero)
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		METH_METHOD_ABORT,
 *		and error returns from other method, DDS, and Parameter Cache functions.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
meth_start (ENV_INFO2 *env_info, ITEM_ID meth_id, int do_init, int close_option)
{

	DEVICE_HANDLE	device_handle;
	RHEAP			*heap;
	METH_INFO		*meth_infop;
	NET_TYPE		net_type;
	int				rs, rs2;
	APP_INFO		*app_info;

	app_info = (APP_INFO *)env_info->app_info;

	/*
	 *	If the do_init parameter is zero, then this method start 
	 *	is a sub-method to an active method.  An example of
	 *	a sub-method could be an abort method.
	 *
	 *	If the do_init parameter is non-zero, then a new
	 *	method is starting.  In this case, determine the network type
	 *  and initialize the corresponding builtin symbols, assign a 
	 *	method tag, set up the setjmp/longjmp code for handling aborts,
	 *	and call the Parameter Cache method initialization routine.
	 */

	if (do_init) {

        if (env_info->type == ENV_BLOCK_HANDLE)
        {
	        rs = get_abt_adt_offset (env_info->block_handle, &device_handle);
		    if (rs != CM_SUCCESS) {
			    return (rs);
		    }
        }
        else
            device_handle = env_info->device_handle;

		rs = get_adt_net_type(device_handle, &net_type);
		if (rs != CM_SUCCESS) {
			return (rs);
		}

        init_a_bi_symbols();

		/*
		 * Make sure getting unique method tag.
		 */
		lock(GET_METH_TAG_LOCK);
		rs = meth_assign_tag (&app_info->method_tag);
		unlock(GET_METH_TAG_LOCK);

		if (rs != BLTIN_SUCCESS) {
			return (rs);
		}
		ASSERT_DBG (VALID_METHOD_TAG (app_info->method_tag));
		meth_infop = &meth_info[app_info->method_tag - 1];
		meth_infop->meth_close_option = (uchar)close_option;

        /* If this is a device method, we'll skip the block param. cache initialization */
        if (env_info->type == ENV_BLOCK_HANDLE)
        {
            /* Initialize the LOCAL portion of the parameter cache */
            rs = pc_initialize_meth_locals((ENV_INFO *)env_info);
            if (rs)
                return rs;
        }

		/*
		 *	Set up the setjmp/longjmp handling for processing aborts.
		 */

		if (setjmp (meth_infop->meth_setjmp) != 0) {

			/*
			 *	This is the return from a longjmp, so this is abort
			 *	processing.  Tell the Parameter Cache that this method
			 *	is through, free up the space in the meth_info array and
			 *	free up the method interpreter heap and finally return.
			 */

			ASSERT_DBG (meth_infop->meth_inuse);
			ASSERT_DBG (meth_infop->meth_processing_abort);

			/*
			 *  The bitest did not allocate memory heap. 
			 *  Therefore, check and free heap if it was allocated.
			 */
			heap = meth_infop->meth_environ.heap_ptr;
			if (heap != NULL) {
				rfreeheap(heap);
				xfree((void **) &heap);
				heap = NULL;
			}
            /* The method complete upcall hasn't been called yet due to the longjmp */
            /* so call it now */
            if (method_complete_upcall)
                (*method_complete_upcall)((ENV_INFO *)env_info, meth_id, METH_METHOD_ABORT);

			meth_infop->meth_close_option = DISCARD_VALUES;
			(void) pc_method_close ((ENV_INFO *)env_info, meth_infop->meth_close_option);
			meth_infop->meth_inuse = 0;
			return (METH_METHOD_ABORT);
		}
	} else {
		meth_infop = &meth_info[app_info->method_tag - 1];
#ifdef DEBUG

		/*
		 *	Since there is no method init, then if the request specifies
		 *	USE_METH_VALUE, there must already be a method tag present.
		 *	If the request does not specify using method values, then
		 *	there may not be a method tag.
		 *  In the case of a scaling calls as part of an existing method 
	 	 *  the status_info will not equal USE_METH_VALUE but the  
	  	 *  method tag will be valid.
		 */

		if (app_info->status_info & USE_METH_VALUE) {
			ASSERT_DBG (VALID_METHOD_TAG (app_info->method_tag));
/*
		} else {
			ASSERT_DBG (app_info->method_tag == 0);
*/
		}
#endif  /*DEBUG*/
	}

	/*
	 *	Parse and execute the method.
	 */
    if (do_init && method_start_upcall)
        (*method_start_upcall)((ENV_INFO *)env_info, meth_id);

    rs = do_method (env_info, meth_id);

    if (do_init && method_complete_upcall)
        (*method_complete_upcall)((ENV_INFO *)env_info, meth_id, rs);

	if (do_init) {
		if (rs != BLTIN_SUCCESS) {
			meth_infop->meth_close_option = DISCARD_VALUES;
		}

		/*
		 *	Close up the method in the Parameter Cache, free up the meth_info
		 *	slot, and return the appropriate return code.
		 */

        rs2 = close_param_cache(env_info, meth_infop->meth_close_option);

		meth_infop->meth_inuse = 0;
	} else {
		rs2 = SUCCESS;
	}
	if (rs != BLTIN_SUCCESS) {
		return (rs);
	}
	return (rs2);
}

static int close_param_cache(ENV_INFO2 *env_info, int close_option)
{
    int rs;

    if (env_info->type == ENV_BLOCK_HANDLE)
    {
        rs = pc_method_close ((ENV_INFO *)env_info, close_option);
        return rs;
    }
    /* If we get here, this is a device handle and we need to close the cache for
     * all blocks in the device, since any parameter of any block may have been
     * touched by cross-block referencing.
     */

    {
        FLAT_DEVICE_DIR *fdd;
        BLK_TBL_ELEM *bte;
        int b;
		unsigned long inst, inst_count;

        /* Get the device tables, for the list of block types */
        rs = get_adt_dd_dev_tbls (env_info->device_handle,(void **) &fdd);
        if (rs)
            return rs;
        for (b=0; b < fdd->blk_tbl.count; b++)
        {
            bte = &fdd->blk_tbl.list[b];
            /* Get the number of instances for this block */
            inst_count = 0;
            rs = (*app_get_block_instance_count)((ENV_INFO *)env_info, 
                 bte->blk_id, &inst_count);
            if (rs)
                return rs;

            for (inst=0; inst < inst_count; inst++)
            {
                ENV_INFO env_info_block;
                if (env2_to_env(env_info, &env_info_block, bte->blk_id, inst))
                    continue;
                rs = pc_method_close (&env_info_block, close_option);
            }

        }
        return 0;
    }
}

