%{
/*
 *
 *	@(#)int_pars.y	30.3  30  14 Nov 1996
 *
 *	int_pars.y - Yacc input for C-interpreter
 */

/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

#include	<stdio.h>
#include	<stdlib.h>

#include	"std.h"
#include	"ddi_lib.h"
#include	"ddsbltin.h"
#include	"int_loc.h"
#include	"app_xmal.h"
#include	"err_gen.h"

#ifdef TOK
#define error tok_interp_error
extern void tok_interp_error(va_alist);
#endif

/*
 * This is a pointer to the parsed statement built by the parser.
 */
STATEMENT *parsed_stmt = 0;

#define YYDEBUG 0
#undef YYERROR_VERBOSE	/* defining this sometimes causes strlen to crash */
#define YYLTYPE

#undef yylval
#undef yylex
#define yylex()			my_yylex(env_ptr)

#undef yyparse
#define yyparse()	    	st_parse( ENVIRON *env_ptr )

#define YY_(msg) env_ptr,msg

int my_yylex P((ENVIRON *env_ptr));
void yyerror(ENVIRON *env_ptr, char *msg);

%}

%start one_statement


/* Type specifier keywords. */
%token <line> CHAR_TOK FLOAT_TOK LONG_TOK INT_TOK DOUBLE_TOK UNSIGNED_TOK SHORT_TOK SIGNED_TOK

/* Statement keywords. */
%token <line> BREAK_TOK CASE_TOK CONTINUE_TOK DEFAULT_TOK DO_TOK
%token <line> ELSE_TOK FOR_TOK IF_TOK RETURN_TOK SWITCH_TOK WHILE_TOK

/* Identifiers. */
%token <str_tok> IDENTIFIER

/* Character, floating point, and integer constants. */
%token <str_tok> CHAR_CONST INT_CONST FLOAT_CONST

/* String literals. */
%token <str_tok> STRING_CONST


/* Precedence rules for dangling else problem. */
%nonassoc IF_TOK
%nonassoc ELSE_TOK

/* Defining precedence for operators. */
%right <op_tok> 	ASSIGN_TOK
%right <line> 		'?' ':'
%left <line> 		LOG_OR_TOK
%left <line> 		LOG_AND_TOK
%left <line> 		'|'
%left <line> 		'^'
%left <line> 		'&'
%left <op_tok> 		EQOP_TOK
%left <op_tok> 		RELOP_TOK
%left <op_tok> 		SHIFT_TOK
%left <line> 		'+' '-'
%left <op_tok> 		MULTOP_TOK
%right <line> 		UNARY PLUSPLUS MINUSMINUS
%left <line> 		'(' '['

%union {
    SYM 			*sym_ptr;
    EXPRESS 		*exp_ptr;
    STATEMENT 		*stmt_ptr;
    STMT_PTRS 		*stmt_ptrs;
    unsigned int	line;

	struct {
    	int 		 typ;
		unsigned int l;
    } typ_tok;

	struct {
    	int 		 o;			/* operator */
		unsigned int l;
    } op_tok;

	struct {
    	char 		 *str;
    	unsigned int l;
    } str_tok;

}

%type <exp_ptr> 	unary xexpr postfix constant primary expression
%type <exp_ptr> 	opt_expr constant_expression argument_list
%type <op_tok> 		unop
%type <str_tok> 	identifier string
%type <stmt_ptr> 	statement compound_statement simple_if 
%type <stmt_ptrs>	statement_list
%type <sym_ptr> 	declaration declaration_list declarator declarator_list 
%type <typ_tok> 	type_specifier



%%

/* 
 * Statements. 
 */

one_statement
	: statement
		{ parsed_stmt = $1; }
	;

statement
	: CASE_TOK constant_expression ':' statement
		{ $$ = bld_stmt( $1, CASE, $4, $2, NULL, NULL, env_ptr ); }
		
	| DEFAULT_TOK ':' statement
		{ $$ = bld_stmt( $1, DEFAULT, $3, NULL, NULL, NULL, env_ptr ); }

	| compound_statement
		{ $$ = $1; }

	| ';'
		{ $$ = bld_stmt( $<line>1, NULL_STMT, NULL, NULL, NULL, NULL, env_ptr ); }

	| expression ';'
		{ $$ = bld_stmt( $<line>2, EXPRESSION, NULL, $1, NULL, NULL, env_ptr ); }

	| simple_if ELSE_TOK statement
		{ 
			if( $1 != NULL )
			{
		    	$1->misc._else = $3;
			}
		  	$$ = $1; 
		}

	| simple_if %prec IF_TOK
		{ 
			if( $1 != NULL ) {
		    	$1->misc._else = NULL;
			}
			$$ = $1; 
		}

	| SWITCH_TOK '(' expression ')'
	  	{ 
			add_switch( env_ptr ); 
		}
	statement
		{ 
			del_switch( env_ptr );
			$$ = bld_stmt( $1, SWITCH, $6, $3, NULL, NULL, env_ptr ); 
		}

	| WHILE_TOK '(' expression ')'
		{ 
			env_ptr->in_loop++; 
		}
	statement
		{ 
			env_ptr->in_loop--;
			$$ = bld_stmt( $1, WHILE, $6, $3, NULL, NULL, env_ptr ); 
		}

	| DO_TOK
		{ 
			env_ptr->in_loop++; 
		}
	statement
		{ 
			env_ptr->in_loop--; 
		}
	WHILE_TOK '(' expression ')' ';'
		{ 
			$$ = bld_stmt( $5, DO, $3, $7, NULL, NULL, env_ptr ); 
		}

	| FOR_TOK '(' opt_expr ';' opt_expr ';' opt_expr ')'
		{ 
			env_ptr->in_loop++; 
		}
	statement
		{ 
			env_ptr->in_loop--;
			$$ = bld_stmt( $1, FOR, $10, $3, $5, $7, env_ptr ); 
		}

	| CONTINUE_TOK ';'
		{ $$ = bld_stmt( $1, CONTINUE, NULL, NULL, NULL, NULL, env_ptr ); }

	| BREAK_TOK ';'
		{ $$ = bld_stmt( $1, BREAK, NULL, NULL, NULL, NULL, env_ptr ); }

	| RETURN_TOK ';'
		{ $$ = bld_stmt( $1, RETURN, NULL, NULL, NULL, NULL, env_ptr ); }

	| error ';'
		{ $$ = NULL; }
	;

opt_expr
	: /* empty */
		{ $$ = NULL; }
	| expression
	;
	
simple_if
	: IF_TOK '(' expression ')' statement
		{ $$ = bld_stmt( $1, IF, $5, $3, NULL, NULL, env_ptr ); }
	;

compound_statement
	: '{' '}'
		{ $$ = bld_stmt( $<line>1, COMPOUND, NULL, NULL, NULL, NULL, env_ptr ); }

	| '{' declaration_list '}'
		{ 
			if( ($$ = bld_stmt( $<line>1, COMPOUND, NULL,
					NULL, NULL, NULL, env_ptr)) != NULL ) {
		    	$$->misc.new_vars = $2;
		    }
		}

	| '{' statement_list '}'
		{ $$ = bld_stmt( $<line>1, COMPOUND, $2->head, NULL, NULL, NULL, env_ptr ); }

	| '{' declaration_list
		{ 
			add_scope( $2, env_ptr ); 
		}
	statement_list '}'
		{ 
			del_scope( env_ptr );
			if( ($$ = bld_stmt( $<line>1, COMPOUND, $4->head, NULL, NULL, NULL,
							env_ptr)) != NULL ) {
		    	$$->misc.new_vars = $2;
		    }
		}

	| '{' error '}'
		{ $$ = NULL; }
	;

statement_list
	: statement
		{ $$ = bld_stmt_lst($1, NULL, env_ptr); }
	| statement_list statement
		{ $$ = bld_stmt_lst($2, $1, env_ptr); }
	;


/* 
 * Declarations. 
 */

declaration_list
	: declaration
		{ $$ = bld_sym_lst(env_ptr, $1, NULL ); }
	| declaration_list declaration
		{ $$ = bld_sym_lst(env_ptr, $2, $1 ); }
	;

declaration
	: type_specifier declarator_list ';'
		{ $$ = bld_add_type( $1.typ, $2, env_ptr ); }
	;

declarator_list
	: declarator
		{ $$ = bld_sym_lst(env_ptr, $1, NULL ); }
	| declarator_list ',' declarator
		{ $$ = bld_sym_lst(env_ptr, $3, $1 ); }
	;

declarator
	: identifier
		{ $$ = bld_sym( $1.l, $1.str, env_ptr ); }
	| declarator '[' ']'
		{ $$ = bld_array( $1, NULL, env_ptr ); }
	| declarator '[' constant_expression ']'
		{ $$ = bld_array( $1, $3, env_ptr ); }
	;

type_specifier
	: SIGNED_TOK
		{ $$.typ = METH_LONG_TYPE; $$.l = $1; }
	| UNSIGNED_TOK
		{ $$.typ = METH_U_LONG_TYPE; $$.l = $1; }
	| CHAR_TOK
		{ $$.typ = METH_CHAR_TYPE; $$.l = $1; }
	| SIGNED_TOK CHAR_TOK
		{ $$.typ = METH_CHAR_TYPE; $$.l = $1; }
	| UNSIGNED_TOK CHAR_TOK
		{ $$.typ = METH_U_CHAR_TYPE; $$.l = $1; }
	| SHORT_TOK
		{ $$.typ = METH_SHORT_TYPE; $$.l = $1; }
	| SIGNED_TOK SHORT_TOK
		{ $$.typ = METH_SHORT_TYPE; $$.l = $1; }
	| UNSIGNED_TOK SHORT_TOK
		{ $$.typ = METH_U_SHORT_TYPE; $$.l = $1; }
	| INT_TOK
		{ $$.typ = METH_LONG_TYPE; $$.l = $1; }
	| SIGNED_TOK INT_TOK
		{ $$.typ = METH_LONG_TYPE; $$.l = $1; }
	| UNSIGNED_TOK INT_TOK
		{ $$.typ = METH_U_LONG_TYPE; $$.l = $1; }
	| LONG_TOK
		{ $$.typ = METH_LONG_TYPE; $$.l = $1; }
	| SIGNED_TOK LONG_TOK
		{ $$.typ = METH_LONG_TYPE; $$.l = $1; }
	| UNSIGNED_TOK LONG_TOK
		{ $$.typ = METH_U_LONG_TYPE; $$.l = $1; }
	| FLOAT_TOK
		{ $$.typ = METH_FLOAT_TYPE; $$.l = $1; }
	| DOUBLE_TOK
		{ $$.typ = METH_DOUBLE_TYPE; $$.l = $1; }
	;


/* 
 * Expressions. 
 */

expression
	: xexpr
	| expression ',' xexpr
		{ $$ = bld_exp( $<line>2, COMMA, $1, $3, NULL, env_ptr ); }
	;

xexpr
	: unary
	| xexpr MULTOP_TOK xexpr
		{ $$ = bld_exp( $2.l, $2.o,    $1, $3, NULL, env_ptr ); }
	| xexpr '+' xexpr
		{ $$ = bld_exp( $2,   ADD,     $1, $3, NULL, env_ptr ); }
	| xexpr '-' xexpr
		{ $$ = bld_exp( $2,   SUB,     $1, $3, NULL, env_ptr ); }
	| xexpr SHIFT_TOK xexpr
		{ $$ = bld_exp( $2.l, $2.o,    $1, $3, NULL, env_ptr ); }
	| xexpr RELOP_TOK xexpr
		{ $$ = bld_exp( $2.l, $2.o,    $1, $3, NULL, env_ptr ); }
	| xexpr EQOP_TOK xexpr
		{ $$ = bld_exp( $2.l, $2.o,    $1, $3, NULL, env_ptr ); }
	| xexpr '&' xexpr
		{ $$ = bld_exp( $2,   BIT_AND, $1, $3, NULL, env_ptr ); }
	| xexpr '^' xexpr
		{ $$ = bld_exp( $2,   BIT_XOR, $1, $3, NULL, env_ptr ); }
	| xexpr '|' xexpr
		{ $$ = bld_exp( $2,   BIT_OR,  $1, $3, NULL, env_ptr ); }
	| xexpr LOG_AND_TOK xexpr
		{ $$ = bld_exp( $2,   LOG_AND, $1, $3, NULL, env_ptr ); }
	| xexpr LOG_OR_TOK xexpr
		{ $$ = bld_exp( $2,   LOG_OR,  $1, $3, NULL, env_ptr ); }
	| xexpr '?' expression ':' xexpr
		{ $$ = bld_exp( $2,   COND,    $1, $3, $5,   env_ptr ); }
	| xexpr ASSIGN_TOK xexpr
		{ $$ = bld_exp( $2.l, $2.o,    $1, $3, NULL, env_ptr ); }
	;

unary
	: postfix
	| PLUSPLUS unary %prec UNARY
		{ $$ = bld_exp( $1, PRE_INCR, $2, NULL, NULL, env_ptr ); }
	| MINUSMINUS unary %prec UNARY
		{ $$ = bld_exp( $1, PRE_DECR, $2, NULL, NULL, env_ptr ); }
	| unop xexpr %prec UNARY
		{ $$ = bld_exp( $1.l, $1.o, $2, NULL, NULL, env_ptr ); }
	;

unop
	: '+'
		{ $$.o = UNARY_PLUS; $$.l = $1; }
	| '-'
		{ $$.o = UNARY_MINUS; $$.l = $1; }
	| '~'
		{ $$.o = COMPLEMENT; $$.l = $<line>1; }
	| '!'
		{ $$.o = LOG_NOT; $$.l = $<line>1; }
	| '&'
		{ $$.o = ADDRESS_OF; $$.l = $<line>1; }
	;

postfix
	: primary
	| postfix '[' expression ']'
		{ $$ = bld_arrayref( $2, $1, $3, env_ptr ); }
	| identifier '(' ')'
		{ $$ = bld_bltin_ref( $1.l, $1.str, NULL, env_ptr ); }
	| identifier '(' 
		{
			env_ptr->in_bltin_ref++;
		}
		argument_list ')'
		{ env_ptr->in_bltin_ref--;
			$$ = bld_bltin_ref( $1.l, $1.str, $4, env_ptr ); }
	| postfix PLUSPLUS
		{ $$ = bld_exp( $2, POST_INCR, $1, NULL, NULL, env_ptr ); }
	| postfix MINUSMINUS
		{ $$ = bld_exp( $2, POST_DECR, $1, NULL, NULL, env_ptr ); }
	;

primary
	: identifier
		{ $$ = bld_symref( $1.l, $1.str, env_ptr ); }
	| constant
		{ $$ = $1; }
	| string
		{ $$ = bld_string( $1.l, $1.str, env_ptr ); }
	| '(' expression ')'
		{ $$ = $2; }
	| '(' error ')'
		{ $$ = NULL; }
	;

identifier
	: IDENTIFIER
		{ $$ = $1; }
	;

constant
	: CHAR_CONST
		{ $$ = bld_char( $1.l, $1.str, env_ptr ); }
	| INT_CONST
		{ $$ = bld_int_const( $1.l, $1.str, env_ptr ); }
	| FLOAT_CONST
		{ $$ = bld_float_const( $1.l, $1.str, env_ptr ); }
	;

string
	: STRING_CONST
		{ 
			$$.str = bld_str( $1.str ); 
			$$.l = $1.l; 
		}

argument_list
	: xexpr
		{ $$ = bld_exp_lst( $1, NULL ); }
	| argument_list ',' xexpr
		{ $$ = bld_exp_lst( $3, $1 ); }
	;	

constant_expression
	: xexpr
		{	
			if ( ($1 != NULL) && ($1->op != CONSTREF) ) {
				error(env_ptr->env_info, CONSTANT_MISSING );
				$$ = NULL;
			}
			else {
			    $$ = $1;
			}
		}
	;


/* External defintions. */



%%

void
yyerror(ENVIRON *env_ptr, char *msg)
{
	unsigned int line;

	switch (yychar) {
	case IDENTIFIER:
	case CHAR_CONST:
	case INT_CONST:
	case FLOAT_CONST:
	case STRING_CONST:
		line = yylval.str_tok.l;
		break;

	case ASSIGN_TOK:
	case EQOP_TOK:
	case RELOP_TOK:
	case SHIFT_TOK:
	case MULTOP_TOK:
		line = yylval.op_tok.l;
		break;

	case CHAR_TOK:
	case SHORT_TOK:
	case INT_TOK:
	case LONG_TOK:
	case SIGNED_TOK:
	case UNSIGNED_TOK:
	case FLOAT_TOK:
	case DOUBLE_TOK:
		line = yylval.typ_tok.l;
		break;

	default:
		line = yylval.line;
		break;
	}

	error(env_ptr->env_info, PARSE_ERROR, msg);
}
