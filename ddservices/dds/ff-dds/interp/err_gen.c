/*
 * @(#)err.t	21.4  26 Jul 1996
 *
 * This file is created from a template file and should not
 * be modified directly. This file contains the error structures
 * and the routines needed to access them.
 */
/* 6/12/02 - the translation tool from err.t to this file is unavailable
 *           manually changed error message 636 was |	636,	"%s %s is too large.",|
 */

#ifdef DD_VIEWER
#ifdef WIN32
#include <windows.h>
#endif
#endif

#include	<stdio.h>
#include	<stdlib.h>

#include	<assert.h>
#include	"std.h"
#include	"tok.h"
#include	"options.h"
#include    "dds_upcl.h"
#ifdef STDARGS
#include	<stdarg.h>
#else
#include	<varargs.h>
#endif
/*
 * Local defines
 */
#define	ERR_MSG_BUF_LEN		2000


/*
 * Global data
 */
#ifdef __cplusplus
	extern "C" {
#endif
int			error_count = 0;
int			supp_error_count = 0;
int			warn_count = 0;
int			supp_warn_count = 0;
int			info_count = 0;
int			supp_info_count = 0;
static int	max_errors = 50;
static char	err_msg_buf[ERR_MSG_BUF_LEN+1];
#define error_msg_file stderr
//FILE		*error_msg_file = stderr;
int			error_excl_count = 0;
int			*error_excl_list = NULL;
#ifdef __cplusplus
	}
#endif



/********** START OF GENERATED CODE **********/

/*
 * Generated From Template File: err.t
 */

#include	"err_gen.h"

/*
 * Structures
 */
typedef struct {
	unsigned short	number;
	char			*desc;
} ERROR_DATA;

static ERROR_DATA	system_errors[] = {
	{100,"Memory exhausted."},
	{101,"File system error; seek failed for file %s."},
	{102,"File system error; write failed for file %s."},
	{103,"File system error; read failed for file %s."},
	{104,"File system error; stat failed for file %s."},
	{105,"Cannot open file %s."},
	{106,"Configuration file %s contains input files."},
	{107,"Too many recursions in configuration files."},
	{108,"Multiple default configuration files specified."},
	{109,"No parameter for command-line option %s."},
	{110,"Unknown command-line option %s."},
	{111,"Cannot change default configuration file within configuration file %s."},
	{112,"Invalid ddl source type %s."},
	{113,"Option %s invalid for %s ddl source."},
	{114,"Unable to open Tokenizer setup file: %s"},
	{115,"Input file: %s is same as Output file: %s"},
	{201,"%s was not found in BLOCK %s."},
	{202,"Attributes for item %s too large."},
	{203,"Import definition for device id (%ld %ld %d %d) is too large."},
	{204,"Like definition for item %s is too large."},
	{205,"Device Directory is too large."},
	{206,"Block Directory for block %s is too large."},
	{210,"DDOD binary file %s has incompatible revision number %d.%d."},
	{211,"DDOD binary file %s has incompatible format."},
	{212,"File %s is not a valid DDOD binary."},
	{220,"Enumerations specifier has no enumerators."},
	{221,"Elements specifier has no elements."},
	{222,"Members specifier has no members."},
	{223,"Response codes specifier has no response codes."},
	{224,"Item %s has no attributes."},
	{225,	"Invalid dictionary string [%d,%d]%s."},
	{226,"Command %d iteration count of %d exceeds bound.  Command skipped."},
	{227,"Variable %s: multibyte variable cannot be used as an index in command %d"},
	{228,"Variable %s: cannot be used as an index in command %d"},
	{229,	"Variable %s, used in command %d, is neither parameter nor local variable."},
	{230,"Variable %s has no CLASS attribute specified and is required."},
	{231,	"%s, line %d: Shape file syntax error."},
	{232,	"%s, line %d: Shape file item %s is not a properly defined symbol."},
	{300,	"%s, line %d: Specified MIN_VALUE is too large."},
	{301,	"%s, line %d: Specified MAX_VALUE is too large."},
	{302,	"%s, line %d: Output file %s already open."},
	{303,	"%s, line %d: Output file %s is already closed."},
	{304,	"%s, line %d: Output file %s is not open."},
	{305,	"%s, line %d: Invalid source character: %s."},
	{306,	"%s, line %d: Invalid preprocessor directive."},
	{307,	"%s, line %d: String contains duplicate country codes."},
	{308,	"%s, line %d: %s appears more than once."},
	{309,	"%s, line %d: %s."},
	{310,	"%s, line %d: VARIABLE %s cannot have size zero."},
	{311,	"%s, line %d: Invalid escape sequence."},
	{312,	"%s, line %d: %s of %s is already defined."},
	{313,	"%s, line %d: Invalid reference."},
	{314,"Excess command-line options ignored."},
	{315,"Cannot open source file %s."},
	{316,"Invalid language code %s."},
	{317,	"%s, line %d: Invalid dictionary string name %s."},
	{318,"RELEASE directory %s does not exist."},
	{319,"Creating device-type directory %s."},
	{320,"Cannot create directory %s."},
	{321,	"%s, line %d: Invalid imported device description."},
	{322,"Creating manufacturer directory %s."},
	{323,	"%s, line %d: method has incompatible builtin use."},
	{324,	"%s, line %d: %s is already defined for transaction."},
	{327,	"%s, line %d: String language/country code is missing closing delimiter."},
	{328,	"%s, line %d: String contains Bad language/country code."},
	{400,	"Import error in %s, line %d:\n Could not find item %s%s to import."},
	{401,	"Redefinition error in %s, line %d:\n Could not find enumerator %d of VARIABLE %s%s."},
	{402,	"Redefinition error in %s, line %d:\n Could not find MIN_VALUE %d of VARIABLE %s%s."},
	{403,	"Redefinition error in %s, line %d:\n Could not find MAX_VALUE %d of VARIABLE %s%s."},
	{404,	"Redefinition error in %s, line %d:\n Could not find member %s of item %s%s."},
	{405,	"Redefinition error in %s, line %d:\n Could not find element %d of item %s%s."},
	{406,	"Redefinition error in %s, line %d:\n Could not find RESPONSE_CODE %d of item %s%s."},
	{408,	"Redefinition error in %s, line %d:\n Types mismatched between redefinition and original definition\n of VARIABLE %s%s."},
	{411,	"Redefinition error in %s, line %d:\n Item %s redefined without being imported."},
	{412,	"Redefinition error in %s, line %d:\n Invalid MIN_VALUE redefinition; original VARIABLE %s\n%s had no MIN_VALUE."},
	{413,	"Redefinition error in %s, line %d:\n Invalid MAX_VALUE redefinition; original VARIABLE %s\n%s had no MAX_VALUE."},
	{414,	"Redefinition error in %s, line %d:\n Could not find TRANSACTION %d of COMMAND %s%s."},
	{415,"Import error:\n Import file %s has unresolved %s."},
	{416,"Imported variable %s has no LABEL specified."},
	{417,	"Redefinition error in %s, line %d:\n May not redefine %s attribute."},
	{500,	"%s, line %d:  Invalid use of member bit in ID for %s."},
	{501,	"%s, line %d:  Invalid standard ID 0x%lX or %lu for item %s."},
	{502,	"%s, line %d:  Standard item %s with ID 0x%lX or %lu is not in the standard symbol table."},
	{503,	"Item %s defined in file %s, line %d was previously defined in file %s, line %d."},
	{504,	"%s, line %d:  Item %s must be imported but was defined."},
	{505,"Cannot open symbol table file %s."},
	{506,	"%s, line %d: Syntax error."},
	{507,	"%s, line %d: Invalid flag %s for item %s."},
	{508,"Items %s and %s have the same symbol ID 0x%lX or %lu."},
	{509,"Item %s is used but not defined."},
	{510,"Item %s is defined but not used."},
	{511,"Item %s was used but is not in a symbol file."},
	{512,"Item %s is mandatory but not defined."},
	{513,	"Item %s being imported, is defined in file %s, line %d."},
	{514,"Symbol file includes nested too deep at file %s."},
	{515,	"%s, line %d: No item name specified."},
	{516,	"%s, line %d:  Item %s defined with no type."},
	{517,"Item %s was imported but is not in a symbol file."},
	{518,	"%s, line %d:  The type of item %s was redefined."},
	{519,	"%s, line %d:  Item %s already exists with Item ID 0x%lX or %lu, cannot be redefined."},
	{520,	"%s, line %d:  %s item %s, Item ID 0x%lX or %lu, already defined, %s Item ID 0x%lX or %lu will be ignored."},
	{521,"%s symbol item ID table overflowed."},
	{522,	"%s, line %d:  The item %s item ID 0x%lX or %lu has been defined in an inactive range."},
	{523,"Errors detected in symbol file %s."},
	{530,	"%s, line %d: Syntax error."},	
	{600,"Device imports itself."},
	{601,"%s must have a %s attribute."},
	{602,"%s appears more than once in %s."},
	{603,"%s is the wrong type of item to be used in %s %s as a %s value."},
	{604,"%s is used in %s %s as a %s value but is not in BLOCK %s."},
	{605,"Multiple ELEMENTs of ITEM_ARRAY %s have the index %lu."},
	{606,"Multiple response codes in %s have the same value %lu."},
	{607,"Multiple enumerations of VARIABLE %s have the value %lu."},
	{608,"%s imported multiple times."},
	{609,"Attribute of %s redefined multiple times."},
	{611,"Undefined attribute from %s was deleted."},
	{612,"Original item for LIKE of %s is undefined."},
	{613,"Invalid flags defined for menu item %s in MENU %s."},
	{614,"%s %s is always invalid."},
	{615,"The CLASS for %s is incorrect."},
	{616,"Variable %s has both CONSTANT_UNIT and a UNIT relation defined."},
	{617,"The size for VARIABLE %s is %s."},
	{618,"VARIABLE_LIST %s does not have multiple members."},
	{619,"%s %s is not attached to a BLOCK."},
	{620,"MEMBERS of %s %s have different types."},
	{621,"ELEMENTS of %s %s have different types."},
	{622,"Invalid %s in %s format string for variable %s."},
	{623,"Invalid number of MIN/MAX values defined for VARIABLE %s."},
	{624,"Default not last CASE in SELECT."},
	{625,"BLOCK reference made through a BLOCK %s that has no CHARACTERISTICS."},
	{626,"%s for %s is conditional."},
	{627,"CHARACTERISTICS RECORD %s listed in PARAMETERS for BLOCK %s."},
	{628,	"%s is in PARAMETERS for BLOCK %s, but is not defined."},
	{629,	"Invalid reference found involving %s at line %d of %s\n(ID 0x%lX, type %d)."},	
	{630,"%s Has No %s Defined."},
	{631,"METHOD %s is used by VARIABLE %s as a pre/post read/write action but contains non-scaling built-in routines."},
	{632,"User METHOD %s contains scaling built-in routines."},
	{633,"CHARACTERISTIC RECORD %s has wrong number of members."},
	{634,"MEMBER %s of CHARACTERISTIC RECORD %s has wrong type/size."},
	{635,"VARIABLE %s has duplicate definition of %s%d."},
	{636,"%s %s (%d bytes) is larger than an FMS pdu (%d bytes)."},
	{637,"Multiple transactions are defined for COMMAND %s."},
	{638,"PROGRAM %s arguments cannot have a data item qualifier specified."},
	{639,"PROGRAM %s arguments cannot have a data item mask specified."},
	{640,"MANUFACTURER required for device-specific %d COMMAND %s."},
	{641,	"Manufacturer specified for %s %d COMMAND %s, not permitted."},
	{642,	"COMMAND %s TRANSACTION %d %s, %s is invalid."},	
	{643,"COMMAND %s TRANSACTION %d is multiply defined."},
	{644,"%s value %d is greater than maximum %d."},
	{646,"%s response code %d description not specified."},
	{647,"COMMAND %s TRANSACTION %d %s data item masks leave bit stream gap."},
	{648,"COMMAND %s TRANSACTION %d must have a REPLY."},
	{649,"COMMAND %s TRANSACTION %d %s has no %s operation in the data items."},
	{650,"COMMAND %s TRANSACTION %d %s octet size %s the 25 octet maximum."},
	{651,"COMMAND %s TRANSACTION %d %s variable %s is wrong type for a data item mask."},
	{652,"Redefinition of %s in COMMAND %s not allowed."},
	{653,"Universal command %d %s attribute redefinition in COMMAND %s not allowed."},
	{654,"Common Practice command %d %s attribute redefinition in COMMAND %s occurred."},
	{655,"BLOCK %s PARAMETERS items are undefined."},
	{656,"LOCAL VARIABLE %s not permitted in BLOCK %s."},
	{657,"COMMAND %s TRANSACTION %d data items improperly qualified."},
	{658,"COMMAND %s TRANSACTION %d REQUEST data item qualifier duplicated in REPLY."},
	{659,"Functional class for value 0x%lX or %ld in VARIABLE %s is incorrect."},
	{660,"Bit-enumerated value 0x%lX or %ld specified in VARIABLE %s is incorrect."},
	{661,"Status class for value 0x%lX or %ld in VARIABLE %s is incorrect."},
	{662,"Index VARIABLE %s references undefined ITEM_ARRAY %s."},
	{663,"COMMAND %s NUMBER %d duplicates COMMAND %s NUMBER."},
	{664,"Item %s is used as %s in %s %s but is undefined."},
	{665,"%s %s is invalid item type for %s device."},
	{666,"%s %s: %s is invalid attribute for %s device."},
	{667,"Variable %s: %s is incorrect bit-enumerator status class for %s device."},
	{668,"Variable %s: using bit-enumerator output statuses in %s device."},
	{669,"COMMAND %s NUMBER %d is greater than maximum of 255."},
	{700,"Undeclared identifier \%s\"."},
	{701,"Subscript on non-array."},
	{702,"Non-integral index."},
	{703,"Symbol \%s\" must be of function type."},
	{704,"Multiple definition of \%s\" on line %d."},
	{705,"Array size required."},
	{706,"Subscript (%d) out of bounds (max = %d)."},
	{707,"Negative subscript (%d)."},
	{708,"Constant required."},
	{709,"Invalid Source Character: '%c'."},
	{710,"Invalid Source Character: (0x%X)."},
	{711,"Return type mismatch with decl of \%s\" (see line %d)."},
	{712,"Argument #%d type mismatch with decl of \%s\" (see line %d)."},
	{713,"Argument #%d: type mismatch."},
	{714,"Number of args different from decl of \%s\" (see line %d)."},
	{715,"Wrong number of arguments."},
	{716,"Prototype \%s\" ignored."},
	{717,"Duplicate prototype \%s\" ignored."},
	{718,"Floating to Integral - loss of precision."},
	{719,"Arithmetic type required - Op #1"},
	{720,"Arithmetic type required - Op #2"},
	{721,"Operands #2 & #3 need to be the same type."},
	{722,"Integral type required - Op #1"},
	{723,"Integral type required - Op #2"},
	{724,"Lvalue required."},
	{725,	"Address-of-operator, '&', is legal only in function references."},	
	{726,"Arithmetic type required as second expression in \for\"."},
	{727,"Continue can only occur within a loop."},
	{728,"Break can only occur within a loop or switch."},
	{729,"Case can only occur within a switch."},
	{730,"Default can only occur within a switch."},
	{731,"Only one default statement per switch."},
	{732,"Arithmetic type required in \%s\"."},
	{733,"Integral type required in \%s\"."},
	{734,"Duplicate case value: %ld."},
	{735,"Error while parsing: %s."},

	{0,	""}};

#define NUM_SYS_ERRORS	209
/* was 173 in tok*/

/********** END OF GENERATED CODE **********/



static int error_removed P((int));



/*
 * error_removed
 *
 * Parameters:  code        The error code to validate
 *
 * Returns:     1 if message is in error_excl_list or
 *              0 if it is not
 */
static int
error_removed(int code)

{
	int		*entry;
	int		*last_entry;

	entry = error_excl_list;
	last_entry = entry + error_excl_count;
	for(; entry < last_entry; entry++) {
		if(*entry == code) {
			return 1;
		}
	}

	return 0;
}



/*
 * message
 *
 * Parameters:	code		The code for the error to display
 *				va_alist	variable arguments
 *
 * This routine will display a message and return.
 *
 * Returns:    nothing
 */

#ifdef STDARGS
void
message(int code,...)
{
	int		i;
	va_list	arg_ptr;

	/*
	 * Go to the start of the argument list
	 */
	va_start(arg_ptr,code);

	/*
	 * Return if this code is not being displayed
	 */
	if(error_removed(code)) {
		supp_info_count++;
		return;
	}

	/*
	 * Search for the error code in the table
	 */
	for(i = 0; i < NUM_SYS_ERRORS; i++) {
		if(system_errors[i].number == code) {
			break;
		}
	}

	/*
	 * Verify the code was found in the table
	 */
	assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it.
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
	(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
	fprintf(error_msg_file,"%s\n",err_msg_buf);
	info_count++;
}

#else
void
message(va_alist)

va_dcl
{
	int		i;
	int		code;
	va_list	arg_ptr;

	/*
	 * Go to the start of the argument list and get
	 * the code
	 */
	va_start(arg_ptr);
	code = va_arg(arg_ptr,int);

	/*
	 * Return if this code is not being displayed
	 */
	if(error_removed(code)) {
		supp_info_count++;
		return;
	}

	/*
	 * Search for the error code in the table
	 */
	for(i = 0; i < NUM_SYS_ERRORS; i++) {
		if(system_errors[i].number == code) {
			break;
		}
	}

	/*
	 * Verify the code was found in the table
	 */
	assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
	(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
	fprintf(error_msg_file,"%s\n",err_msg_buf);
	info_count++;
}
#endif


/*
 * info
 *
 * Parameters:	code		The code for the error to display
 *				va_alist	variable arguments
 *
 * This routine will display a info message and return.
 *
 * Returns:    nothing
 */

#ifdef STDARGS
void
info(int code,...)
{
	int		i;
	va_list	arg_ptr;

	/*
	 * Don't process this message if the warning level
	 * is not set correctly
	 */
	if(warning_level >= 2) {
		/*
		 * Go to the start of the argument list
		 */
		va_start(arg_ptr,code);

		/*
		 * Return if this code is not being displayed
		 */
		if(error_removed(code)) {
			supp_info_count++;
			return;
		}

		/*
		 * Search for the error code in the table
		 */
		for(i = 0; i < NUM_SYS_ERRORS; i++) {
			if(system_errors[i].number == code) {
				break;
			}
		}

		/*
		 * Verify the code was found in the table
		 */
		assert(i < NUM_SYS_ERRORS);

		/*
		 * Build the error message and display it
		 * Allocate a buffer four times as long as the source
		 * string to provide space for the arguments to be expanded
		 */
		(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
		va_end(arg_ptr);
		fprintf(error_msg_file,"INFO %d: %s\n",
				system_errors[i].number,err_msg_buf);
		info_count++;
	}
	else
		supp_info_count++;
}
#else
void
info(va_alist)

va_dcl
{
	int		i;
	int		code;
	va_list	arg_ptr;

	/*
	 * Don't process this message if the warning level
	 * is not set correctly
	 */
	if(warning_level >= 2) {
		/*
		 * Go to the start of the argument list and get
		 * the code
		 */
		va_start(arg_ptr);
		code = va_arg(arg_ptr,int);

		/*
		 * Return if this code is not being displayed
		 */
		if(error_removed(code)) {
			supp_info_count++;
			return;
		}

		/*
		 * Search for the error code in the table
		 */
		for(i = 0; i < NUM_SYS_ERRORS; i++) {
			if(system_errors[i].number == code) {
				break;
			}
		}

		/*
		 * Verify the code was found in the table
		 */
		assert(i < NUM_SYS_ERRORS);

		/*
		 * Build the error message and display it
		 * Allocate a buffer four times as long as the source
		 * string to provide space for the arguments to be expanded
		 */
		(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
		va_end(arg_ptr);
		fprintf(error_msg_file,"INFO %d: %s\n",
				system_errors[i].number,err_msg_buf);
		info_count++;
	}
	else
		supp_info_count++;
}
#endif

/*
 * warning
 *
 * Parameters:	code		The code for the error to display
 *				va_alist	variable arguments
 *
 * This routine will display a warning message and return.
 *
 * Returns:    nothing
 */

#ifdef STDARGS
void
warning(int code,...)
{
	int		i;
	va_list	arg_ptr;

	/*
	 * Don't process this message if the warning level
	 * is not set correctly
	 */
	if(warning_level >= 1) {
		/*
		 * Go to the start of the argument list
		 */
		va_start(arg_ptr,code);

		/*
		 * Return if this code is not being displayed
		 */
		if(error_removed(code)) {
			supp_warn_count++;
			return;
		}

		/*
		 * Search for the error code in the table
		 */
		for(i = 0; i < NUM_SYS_ERRORS; i++) {
			if(system_errors[i].number == code) {
				break;
			}
		}

		/*
		 * Verify the code was found in the table
		 */
		assert(i < NUM_SYS_ERRORS);

		/*
		 * Build the error message and display it
		 * Allocate a buffer four times as long as the source
		 * string to provide space for the arguments to be expanded
		 */
		(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
		va_end(arg_ptr);
		fprintf(error_msg_file,"WARNING %d: %s\n",
				system_errors[i].number,err_msg_buf);
		warn_count++;
	}
	else
		supp_warn_count++;
}
#else
void
warning(va_alist)

va_dcl
{
	int		i;
	int		code;
	va_list	arg_ptr;

	/*
	 * Don't process this message if the warning level
	 * is not set correctly
	 */
	if(warning_level >= 1) {
		/*
		 * Go to the start of the argument list and get
		 * the code
		 */
		va_start(arg_ptr);
		code = va_arg(arg_ptr,int);

		/*
		 * Return if this code is not being displayed
		 */
		if(error_removed(code)) {
			supp_warn_count++;
			return;
		}

		/*
		 * Search for the error code in the table
		 */
		for(i = 0; i < NUM_SYS_ERRORS; i++) {
			if(system_errors[i].number == code) {
				break;
			}
		}

		/*
		 * Verify the code was found in the table
		 */
		assert(i < NUM_SYS_ERRORS);

		/*
		 * Build the error message and display it
		 * Allocate a buffer four times as long as the source
		 * string to provide space for the arguments to be expanded
		 */
		(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
		va_end(arg_ptr);
		fprintf(error_msg_file,"WARNING %d: %s\n",
				system_errors[i].number,err_msg_buf);
		warn_count++;
	}
	else
		supp_warn_count++;
}
#endif


/*
 * error
 *
 * Parameters:	code		The code for the error to display
 *				va_alist	variable arguments
 *
 * This routine will display a error message and return.
 *
 * Returns:    nothing
 */

#ifdef STDARGS
void
error(ENV_INFO2 *env_info,...)
{
	int		i;
	va_list	arg_ptr;
    int code;

	/*
	 * Go to the start of the argument list
	 */
	va_start(arg_ptr, env_info);
    code = va_arg(arg_ptr, int);

	/*
	 * Return if this code is not being displayed
	 */
	if(error_removed(code)) {
		supp_error_count++;
		return;
	}

	/*
	 * Search for the error code in the table
	 */
	for(i = 0; i < NUM_SYS_ERRORS; i++) {
		if(system_errors[i].number == code) {
			break;
		}
	}

	/*
	 * Verify the code was found in the table
	 */
	assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
	(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
	fprintf(error_msg_file,"ERROR %d: %s\n",
			system_errors[i].number,err_msg_buf);

    if (method_error_upcall)
    {
        char str[1000];
    	sprintf(str,"ERROR %d: %s\n",
	    		system_errors[i].number,err_msg_buf);
        (*method_error_upcall)((ENV_INFO *)env_info, str);
    }
	/*
	 * Show another error was displayed and terminate if
	 * the maximum number have been displayed
	 */
	error_count++;
	if((max_errors >= 0) && (error_count > max_errors)) {
#if !defined(INTERP) 
		fprintf(error_msg_file,"\nToo Many Errors, Program Aborted\n");
#endif
		exit_gracefully();
	}
}
#else
void
error(va_alist)

va_dcl
{
	int		i;
	int		code;
	va_list	arg_ptr;
    ENV_INFO2 *env_info;

	/*
	 * Go to the start of the argument list and get
	 * the code
	 */
	va_start(arg_ptr);
    env_info = va_arg(arg_ptr, ENV_INFO2*);
	code = va_arg(arg_ptr,int);

	/*
	 * Return if this code is not being displayed
	 */
	if(error_removed(code)) {
		supp_error_count++;
		return;
	}

	/*
	 * Search for the error code in the table
	 */
	for(i = 0; i < NUM_SYS_ERRORS; i++) {
		if(system_errors[i].number == code) {
			break;
		}
	}

	/*
	 * Verify the code was found in the table
	 */
	assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
	(void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
	fprintf(error_msg_file,"ERROR %d: %s\n",
			system_errors[i].number,err_msg_buf);
    
    if (method_error_upcall)
    {
        char str[1000];
    	sprintf(str,"ERROR %d: %s\n",
	    		system_errors[i].number,err_msg_buf);
        (*method_error_upcall)((ENV_INFO *)env_info, str);
    }

	/*
	 * Show another error was displayed and terminate if
	 * the maximum number have been displayed
	 */
	error_count++;
	if((max_errors >= 0) && (error_count > max_errors)) {
#if !defined(INTERP) 
		fprintf(error_msg_file,"\nToo Many Errors, Program Aborted\n");
#endif
		exit_gracefully();
	}
}
#endif

/*
 * fatal
 *
 * Parameters:	code		The code for the fatal message to display
 *				va_alist	variable arguments
 *
 * This routine will display a fatal error message and exit.
 *
 * Returns:    nothing
 */

#ifdef STDARGS
void
fatal(int code,...)
{
	int		i;
	va_list	arg_ptr;

	/*
	 * Go to the start of the argument list
	 */
	va_start(arg_ptr,code);

    /*
     * Search for the error code in the table
     */
    for(i = 0; i < NUM_SYS_ERRORS; i++) {
        if(system_errors[i].number == code) {
            break;
        }
    }

    /*
     * Verify the code was found in the table
     */
    assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
    (void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
    fprintf(error_msg_file,"FATAL ERROR %d: %s\n",
			system_errors[i].number,err_msg_buf);

	error_count++;
	exit_gracefully();
}

#else
void
fatal(va_alist)

va_dcl
{
	int		i;
	int		code;
	va_list	arg_ptr;

	/*
	 * Go to the start of the argument list and get
	 * the code
	 */
	va_start(arg_ptr);
	code = va_arg(arg_ptr,int);

    /*
     * Search for the error code in the table
     */
    for(i = 0; i < NUM_SYS_ERRORS; i++) {
        if(system_errors[i].number == code) {
            break;
        }
    }

    /*
     * Verify the code was found in the table
     */
    assert(i < NUM_SYS_ERRORS);

	/*
	 * Build the error message and display it
	 * Allocate a buffer four times as long as the source
	 * string to provide space for the arguments to be expanded
	 */
    (void)vsprintf(err_msg_buf,system_errors[i].desc,arg_ptr);
	va_end(arg_ptr);
    fprintf(error_msg_file,"FATAL ERROR %d: %s\n",
			system_errors[i].number,err_msg_buf);

	error_count++;
	exit_gracefully();
}
#endif



