
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      int_enty.c - interpreter entry points
 ******************************************************/

#include 	"int_inc.h"

#define		INIT_METH_ENV	1

/*********************************************************************
 *
 *	Name: mi exec_user_meth
 *	ShortDesc: execute a "user" method
 *
 *	Description:
 *		Executes a "user" method. A user method is any method
 *		which is not a pre or post edit methods, nor a post-read
 *		method, nor a pre-write method. In other words, a method
 *		that is invoked by the end user. 
 *
 *		The application support for the builtins supplied with the
 *		method interpreter is quite simplistic, and therefore requires
 *		little initialization. More sophisticated implementations
 *		may need to further initialize the ENV_INFO. For example,
 *		initializing the display_info pointer.
 *
 *	Inputs:
 *		block_handle - block handle
 *		meth_id - id of method to execute
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		METH_METHOD_ABORT,
 *		and error returns from other method, DDS, and Parameter Cache functions.
 *
 *	Also:
 *		mi exec_edit_meth
 *
 *	Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

long
mi_exec_user_meth(ENV_INFO env_info, ITEM_ID meth_id)
{
    ENV_INFO2 env_info2;

    env_to_env2(&env_info, &env_info2);

    return mi_exec_user_meth2(env_info2, meth_id);
}

long
mi_exec_user_meth2(ENV_INFO2 env_info2, ITEM_ID meth_id)
{
    /*
	 *	Set the builtin request structure status_info bit.
	 */
	((APP_INFO*)env_info2.app_info)->status_info = USE_METH_VALUE;

	/*
	 *	Execute the method.
	 */
	return meth_start(&env_info2, meth_id, INIT_METH_ENV, DISCARD_VALUES);
}


#define DONT_REPLICATE_DEV_VALUES 0

/*********************************************************************
 *
 *	Name: mi exec_edit_meth
 *	ShortDesc: execute a pre/post edit method
 *
 *	Description:
 *		Executes a pre/post edit method. This function initializes
 *		the builtins so that the value being edited is available
 *		via the scaling builtins. This is the only difference
 *		between mi exec_user_meth and mi exec_edit_meth.
 *
 *		The application support for the builtins supplied with the
 *		method interpreter is quite simplistic, and therefore requires
 *		little initialization. More sophisticated implementations
 *		may need to further initialize the ENV_INFO. For example,
 *		initializing the display_info pointer.
 *
 *	Inputs:
 *		block_handle - block handle
 *		meth_id - id of method to execute
 *		pc_p_ref - reference to value being edited
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		METH_METHOD_ABORT,
 *		and error returns from DDS, and Parameter Cache functions.
 *
 *	Also:
 *		mi exec_user_meth
 *
 *	Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

long
mi_exec_edit_meth (ENV_INFO env_info, ITEM_ID meth_id, P_REF *p_ref)
{
    ENV_INFO2 env_info2;

    env_to_env2(&env_info, &env_info2);

    return mi_exec_edit_meth2(env_info2, meth_id, p_ref);
}


long
mi_exec_edit_meth2 (ENV_INFO2 env_info2, ITEM_ID meth_id, P_REF *p_ref)
{
	P_REF		ip_ref;
	long		rs;
	long		rs2;

    /* Edit methods must be called with a block handle.  There are no
     * device-level variables.
     */
    if (env_info2.type == ENV_DEVICE_HANDLE)
        return METH_INVALID_BLOCK;
	/*
	 *	Set the builtin request structure status_bit info.
	 */
	((APP_INFO*)env_info2.app_info)->status_info = USE_EDIT_VALUE;

	rs = FmsGetActionStorage(((ENV_INFO *)&env_info2), &ip_ref, p_ref);
	if (rs != SUCCESS) {
		return (rs);
	}
	/*
	 *	Execute the pre/post edit method.
	 */
	rs2 = meth_start(&env_info2, meth_id, INIT_METH_ENV, NO_VALUES);

	rs = FmsPutActionStorage(((ENV_INFO *)&env_info2), &ip_ref, DONT_REPLICATE_DEV_VALUES);

	return (rs2);
}

/*********************************************************************
 *
 *	Name: mi exec_scaling_meth
 *	ShortDesc: execute a pre-write or post-read method
 *
 *	Description:
 *		Executes a pre-write or post-read method, a.k.a. scaling method.
 *		
 *		More sophisticated implementations may need to further initialize 
 *		the ENV_INFO. For example, initializing a display_info pointer.
 *
 *	Inputs:
 *		env_info - a pointer at the internal execution envionment structure
 *		meth_id - id of the method to execute
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		METH_METHOD_ABORT,
 *		and error returns from DDS, and Parameter Cache functions.
 *
 *	Also:
 *		mi exec_user_meth, mi exec_edit_meth
 *
 *	Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

long
mi_exec_scaling_meth (ENV_INFO *env_info, ITEM_ID meth_id)
{
	long	rs;

    ENV_INFO2 env_info2;

    env_to_env2(env_info, &env_info2);

	/*
	 *  Execute the pre-write or post-read method.
	 */
	rs = meth_start(&env_info2, meth_id, INIT_METH_ENV, NO_VALUES);

	return (rs);
}

long
mi_exec_scaling_meth2 (ENV_INFO2 *env_info2, ITEM_ID meth_id)
{
	long	rs;

    /* Not valid in the case of a device handle: there are no device-level
     * variables.
     */
    if (env_info2->type == ENV_DEVICE_HANDLE)
        return METH_INVALID_BLOCK;

	/*
	 *  Execute the pre-write or post-read method.
	 */
	rs = meth_start(env_info2, meth_id, INIT_METH_ENV, NO_VALUES);

	return (rs);
}

/* Convert an old-style ENV_INFO to a new ENV_INFO2 */
void env_to_env2(ENV_INFO *env_info, ENV_INFO2 *env_info2)
{
    env_info2->app_info = env_info->app_info;
    env_info2->block_handle = env_info->block_handle;
    env_info2->device_handle = 0;
    env_info2->type = ENV_BLOCK_HANDLE;
}


/* Given an env_info2 to specify the device, find the block, and create a valid 
 * env_info for the block, including the proper app_info appropriate to that 
 * block.
 */
int env2_to_env(ENV_INFO2 *env_info2, ENV_INFO *env_info, ITEM_ID blk_id, unsigned long instance)
{
    int rc;

    if (!app_func_get_block_handle)
        return METH_INVALID_BLOCK;
    rc = (*app_func_get_block_handle)((ENV_INFO *)env_info2,
        blk_id, instance, &env_info->block_handle);
    if (rc)
        return rc;
    /*  Wrong!!! 
    rc = get_abt_app_info(env_info->block_handle, &env_info->app_info);
    if (rc)
        return rc;
    ***/
    /* Right!!! */
    env_info->app_info = env_info2->app_info;
    return 0;
}

int env2_handle_to_env(ENV_INFO2 *env_info2, ENV_INFO *env_info, BLOCK_HANDLE bh)
{
    env_info->block_handle = bh;
    /*  Wrong!!! 
    rc = get_abt_app_info(env_info->block_handle, &env_info->app_info);
    if (rc)
        return rc;
    */
    
    /* Right!!! */
    env_info->app_info = env_info2->app_info;

    return 0;
}

int env2_handle_to_env2(ENV_INFO2 *env_info2, ENV_INFO2 *env_info, BLOCK_HANDLE bh)
{
    int rc;

    // Fill in the first 2 elements, block_handle and app_info
    rc = env2_handle_to_env(env_info2, (ENV_INFO *)env_info, bh);
    if (rc)
        return rc;
    env_info->device_handle = 0;
    env_info->type = ENV_BLOCK_HANDLE;
    return 0;
}

// Convert a device ENV_INFO2 to a block ENV_INFO2
int env2_to_env2(ENV_INFO2 *env_info2, ENV_INFO2 *env_info, ITEM_ID blockId, unsigned long instance)
{
    int rc;

    // Fill in the first 2 elements, block_handle and app_info
    rc = env2_to_env(env_info2, (ENV_INFO *)env_info, blockId, instance);
    if (rc)
        return rc;
    env_info->device_handle = 0;
    env_info->type = ENV_BLOCK_HANDLE;
    return 0;
}

