
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */
/*
 *      int_vfy.c - validation functions
 *
 ******************************************************/

#include "int_inc.h"
#ifdef TOK
#define error tok_interp_error
extern void tok_interp_error(va_alist);
#endif

/*
 * Use in verify expression to indicate operand type.
 */
#define	ARITHMETIC_OP	1
#define	INTEGRAL_OP		2	
#define	UNUSED_OP		3	

/*********************************************************************
 *
 *	Name: equal_types()
 *	ShortDesc: Check to see if two types are equal.
 *
 *	Description:
 *		Equal_types checks and returns if two types are equal.
 *
 *	Inputs:
 *		type1 - Compare against type2.
 *		type2 - Compare against type1.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		TRUE  - Types are equal.
 *		FALSE - Types are not equal.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/

/*
 * Check that two function-types are not equal.
 */
#define	NEQ_FUNC_TYPES(t1,t2)								\
				( (t1->typ == METH_BUILT_IN_TYPE) && 		\
				  (t2->typ == METH_BUILT_IN_TYPE) &&		\
				  !equal_types(t1->madeof, t2->madeof)		\
				)

/*
 * Check that two array-types are not equal.
 */
#define NEQ_ARRAY_TYPES(t1,t2)								\
				( (t1->typ == METH_ARRAY_TYPE) &&			\
				  (t2->typ == METH_ARRAY_TYPE) &&			\
				  !(equal_types(t1->madeof, t2->madeof) && 	\
				  (t1->madeof->size == t2->madeof->size))	\
				)

static int
equal_types(TYPE_SPEC *type1, TYPE_SPEC *type2)
{
	if ((type1 == NULL) || (type2 == NULL) 
		|| (type1->typ != type2->typ)
		|| NEQ_ARRAY_TYPES(type1, type2)
		|| NEQ_FUNC_TYPES(type1, type2)) {

		return (FALSE);
	}

	return (TRUE);
}


/*********************************************************************
 *
 *	Name: valid_array_sub()
 *	ShortDesc: Verify an array subscript.
 *
 *	Description:
 *		Valid_array_sub verifies an array subscript for having a
 *		legal type and range.  This is called when an array is declared.
 *
 *	Inputs:
 *		subscript - Array subscript to be verified. 
 *		env_ptr  - Environment for passing into ADJ_CAST
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		zero 	 - Error found in array subscript.
 *		non-zero - Size of an array.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
long
valid_array_sub(EXPRESS *subscript, ENVIRON *env_ptr)
{
	VALUE 		size_val;					/* Size of array */
	TYPE_SPEC 	*size_type;					/* Type of array-size */

	ASSERT_DBG(subscript != NULL);
	ASSERT_DBG(subscript->op == CONSTREF);

	/*
	 * Get the size of an array.
	 */
	size_type = subscript->type;
	get_val(&subscript->misc.con_v, size_type, &size_val, env_ptr);

	/*
	 * Issue error message if the type of the array-subscript 
 	 * is not an integral type.
	 */
	if (!INTEGRAL_TYPE(size_type->typ)) {
		error(env_ptr->env_info, NON_INTEGRAL_INDEX);
		return (0);
	}

	/*
	 * Making sure the array-size should be greater than or equal
	 * to zero and less than or equal to the maximum integer allowed.
	 */ 
	if ((size_type->typ == METH_LONG_TYPE) && (size_val.l > LONG_MAX)) {
		error(env_ptr->env_info, SUBSCRIPT_OUT_OF_BOUNDS);
		return (0);
	}
	ADJ_CAST(&size_val, size_type, METH_LONG_TYPE, env_ptr);
	if (size_val.l <= 0) {
		error(env_ptr->env_info, NEGATIVE_SUBSCRIPT, size_val.l);
		return (0);
	}

	return (size_val.l);
}


/*********************************************************************
 *
 *	Name: valid_bltin_call_args()
 *	ShortDesc: Validates built-in call arguments against its prototype.
 *
 *	Description:
 *		valid_bltin_call_args verifies the built-in call arguments for 
 *		correctness in usage. The error or warning message(s) will be 
 *		displayed on the console if an incorrect usage is found in the
 *		arguments of the built-in call. Return false to indicate error
 *		is found. Otherwise, true will be returned.
 *
 *	Inputs:
 *		bltin_ptr - Pointer to expression contained a built-in call.
 *		arg_ptr  - Built-in call arguments to be verified.
 *		env_ptr  - Environment contained prototype symbols.
 *
 *	Outputs:
 *		Error message(s) will be displayed on the console if
 *			found improper usage in the built-in call arguments.
 *
 *	Returns:
 *		TRUE  - no error found.
 *		FALSE - error found.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
int
valid_bltin_call_args(EXPRESS *bltin_ptr, EXPRESS *arg_ptr, ENVIRON *env_ptr)
{
	int	i;								/* Function argument counter */
	int arg_ok;							/* True if sym and arg compare okay */
	int errornum = 0;					/* Error counter  */
	SYM *sym_ptr;						/* Function argument symbol */
	SYM *bltin_sym;						/* Ptr to a built-in symbol */

	/*
	 * Search for a built-in symbol in symbol table.
	 */
	bltin_sym = lookup(bltin_ptr->misc.name, env_ptr);

	/*
	 * If not found the built-in symbol then issue the
	 * error message because no such built-in was defined.
	 */
	if (bltin_sym == NULL) {
		error(env_ptr->env_info, NON_FUNCTION_CALL, bltin_ptr->misc.name);
		return (TRUE);
	}

	/*
	 * Validate the types of a built-in call arguments against 
	 * the argument types of its prototype in symbol table.
	 */
	for (sym_ptr = bltin_sym->v.bltin_p->params, i = 1;
		(sym_ptr && arg_ptr); 
		sym_ptr = sym_ptr->next_sym, arg_ptr = arg_ptr->next_exp, i++) {

		/*
		 * Issue an error message if a type of the built-in call 
		 * argument is not matched with the argument type of a 
		 * prototype in symbol table.
		 */
		arg_ok = FALSE;

		/*
		 * If both are arithmetic, we have a match
		 */
		if (ARITH_TYPE(sym_ptr->type->typ)
			&& ARITH_TYPE(arg_ptr->type->typ)) {
			arg_ok = TRUE;
		}
		/*
		 * If both types are exactly the same, we have a match.
		 */
		else if ( equal_types(sym_ptr->type, arg_ptr->type)) {
			arg_ok = TRUE;
		}
		/*
		 * If the built-in prototype wants a METH_PTR_TYPE and we give it 
		 * a METH_ARRAY_TYPE, and the rest is the same, we have a match.
		 */
		else if ((sym_ptr->type->typ == METH_PTR_TYPE)
				&& (arg_ptr->type->typ == METH_ARRAY_TYPE)
				&& equal_types(sym_ptr->type->madeof, arg_ptr->type->madeof)) {
			arg_ok = TRUE;
		}
#ifndef TOK   /* AR 5686 - only DDS/Interp should allow 0 for a pointer arg; Tokenizer should reject it */
		/*
		 * If the built-in prototype wants a METH_PTR_TYPE and we give it 
		 * a NULL in the form of a LONG 0, then we have a match.
		 */
        else if ((sym_ptr->type->typ == METH_ARRAY_TYPE) &&
                 (arg_ptr->type->typ == METH_LONG_TYPE) &&
                 (*arg_ptr->misc.con_v.l_p == 0)) {
            arg_ok = TRUE;
        }
#endif

		if (arg_ok == FALSE) {
			/* None of the checks above passed */
			error(env_ptr->env_info, ARGUMENT_TYPE_MISMATCH2, i);
			errornum++;
		}

		if (INTEGRAL_TYPE(sym_ptr->type->typ) && FLOATING_TYPE(arg_ptr->type->typ)) {
			warning(LOSS_OF_PRECISION);
		}
	}

	/*
	 * Issue error message if the number of arguments is not equal. 
	 */
	if ((void *) sym_ptr != (void *) arg_ptr) {	
		if (!arg_ptr)
            error(env_ptr->env_info, ARGUMENT_COUNT_MISMATCH2);
        else
		    error(env_ptr->env_info, ARGUMENT_COUNT_MISMATCH, bltin_sym->name, arg_ptr->line);
		errornum++;
	}

	if (errornum) {
		return (FALSE);
	}

	return (TRUE);
}


/*********************************************************************
 *
 *	Name: valid_get_expr_info()
 *	ShortDesc: Get the necessary info for verifying an expression.
 *
 *	Description:
 *		Vfy_get_expr_info checks the operator in the expression and
 *		set the flag(opkind) to indicate the expected types for the 
 *		operands. If the operand is effected its value after applying
 *		the operator, set the flag(lvalue) to indicate effect operand
 *		value. The expected number of operands is also returned to 
 *		the caller.
 *
 *	Inputs:
 *		exp    - Expression to be extracted info for verifying.
 *
 *	Outputs:
 *		opkind - The expected type(s) for an operator operand(s). 
 *		lvalue - Flag to indicate operand value is effected.
 *		env_ptr - Environment pointer used to pass into method_abort. 
 *
 *	Returns:
 *		The expected number of operands is required for the operator.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static int
valid_get_expr_info(EXPRESS *exp, int *opkind, int *lvalue, ENVIRON *env_ptr)
{

	int 		op_cnt;

	*opkind = UNUSED_OP;
	*lvalue = FALSE;

	/*
	 * Run through this switch to set up flags to determine the
	 * requirements of the operator that applies to its operand(s). 
	 * Refer to "int_loc.h" for actual c-operators below.
	 */ 
	switch (exp->op) {
		case PRE_INCR:
		case PRE_DECR:
		case POST_INCR:
		case POST_DECR:
			/*
			 * Set this flag to indicate that this operator applies
			 * to its operand and will change its value. 
			 * Note: Intentionally let execution fall through to include 
			 *       the following case.
			 */
			*lvalue = TRUE;
			/* FALL THROUGH */
		case UNARY_PLUS:
		case UNARY_MINUS:
		case LOG_NOT:
			/*
			 * It was an unary operator and 
			 * it requires only one arithmetic operand.
			 */
			op_cnt = 1;
			*opkind = ARITHMETIC_OP;
			break;

		case COMPLEMENT:
			/*
			 * It was an unary operator and 
			 * it requires only one integral operand.
			 */
			op_cnt = 1;
			*opkind = INTEGRAL_OP;
			break;

		/*
		 * Binary operators.
		 */
		case ASSIGN:
		case ASSIGN_ADD:
		case ASSIGN_SUB:
		case ASSIGN_MULT:
		case ASSIGN_DIV:
			/*
			 * Set this flag to indicate that this operator applies to
			 * its operands and will change its left-hand variable value. 
			 * Note: Intentionally let execution fall through to include 
			 *       the following case.
			 */
			*lvalue = TRUE;
			/* FALL THROUGH */
		case ADD:
		case SUB:
		case MULT:
		case DIV:
		case LOG_OR:
		case LOG_AND:
		case EQ:
		case NEQ:
		case LT:
		case LE:
		case GT:
		case GE:
			/*
			 * It was an binary operator and 
			 * it requires two arithmetic operands.
			 */
			op_cnt = 2;
			*opkind = ARITHMETIC_OP;
			break;

		case ASSIGN_MOD:
		case ASSIGN_BIT_OR:
		case ASSIGN_BIT_AND:
		case ASSIGN_BIT_XOR:
		case ASSIGN_SHIFT_L:
		case ASSIGN_SHIFT_R:
			/*
			 * Set this flag to indicate that this operator applies to
			 * its operands and will change its left-hand variable value. 
			 * Note: Intentionally let execution fall through to include 
			 *       the following case.
			 */
			*lvalue = TRUE;
			/* FALL THROUGH */
		case MOD:
		case BIT_OR:
		case BIT_AND:
		case BIT_XOR:
		case SHIFT_L:
		case SHIFT_R:
			/*
			 * It was an binary operator and 
			 * it requires two integral operands.
			 */
			op_cnt = 2;
			*opkind = INTEGRAL_OP;
			break;

		case COMMA:
			/*
			 * It was a comma between two expressions.
			 * It requires two operands. 
			 */
			op_cnt = 2;
			break;

		case COND:
			/*
			 * It requires three operands for this operator.
			 * ex. "a ? 5 : 4"
			 */
			op_cnt = 3;
			break;
		case ADDRESS_OF:
			op_cnt = 1;
			*lvalue = TRUE;
			break;

		case SYMREF:
		case ARRAYREF:
		case CONSTREF:
		case FUNCREF:
		default:
			CRASH_DBG();
			method_abort("Default reached in setting operator flags:", env_ptr->env_info);
	}

	return (op_cnt);
}


/*********************************************************************
 *
 *	Name: valid_operand_type()
 *	ShortDesc: Verify the operand types.
 *
 *	Description:
 *		Vfy_operand_type checks and issues error message if error
 *		is found for a particular operand type. 
 *
 *	Inputs:
 * 		exp   - Contained error line number if error found.
 *		op_cnt   - Number of operands to be verified.
 *		kind  - The expected operand kind(ARITHMETIC_OP/INTEGRAL_OP).
 * 		type1 -	The type of the first operand.
 *		type2 - The type of the second operand.
 *		type3 - The type of the third operand.
 *
 *	Outputs:
 *		Error messages will be displayed on the console if found errors.
 *
 *	Returns:
 *		Number of errors found.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static int
valid_operand_type(ENVIRON *env_ptr, int op_cnt, int kind, TYPE_SPEC *type1,
	TYPE_SPEC *type2, TYPE_SPEC *type3)
{
	int 			nbr_errs = 0;	/* Error counter for return */

	if ((kind == ARITHMETIC_OP) || (op_cnt == 3)) {

		/*
		 * Issue error message if the first operand 
		 * is not arithmetic type.
		 */
		if (!ARITH_TYPE(type1->typ)) {
			error(env_ptr->env_info, NON_ARITHMETIC_TYPE_OPERAND_1);
			nbr_errs++;
		}

		/*
		 * Issue error message if the second operand 
		 * is not arithmetic type.
		 */
		if ((op_cnt == 2) && !ARITH_TYPE(type2->typ)) {
			error(env_ptr->env_info, NON_ARITHMETIC_TYPE_OPERAND_2);
			nbr_errs++;
		}

		if (op_cnt == 3) {	

			/*
			 * It was "(a == b) ? x : y" statement.  Issue error message
			 * if the second and third operands are not equal.
			 */
			if (!((ARITH_TYPE(type2->typ) && ARITH_TYPE(type3->typ))
			      || equal_types(type2, type3))) {
				error(env_ptr->env_info, OPERAND_MISMATCH_OPER_2_AND_3);
				nbr_errs++;
			}
		}
	}

	if (kind == INTEGRAL_OP) {

		/*
		 * Issue error message if the first operand is not integral type.
		 */
		if (!INTEGRAL_TYPE(type1->typ)) {
			error(env_ptr->env_info, NON_INTEGRAL_TYPE_OPERAND_1);
			nbr_errs++;
		}

		/*
		 * Issue error message if the second operand is not integral type.
		 */
		if ((op_cnt == 2) && !INTEGRAL_TYPE(type2->typ)) {
			error(env_ptr->env_info, NON_INTEGRAL_TYPE_OPERAND_2);
			nbr_errs++;
		}
	}
	return (nbr_errs);
}


/*********************************************************************
 *
 *	Name: valid_determ_promo_type()
 *	ShortDesc: Determine the result type and promote if necessary.
 *
 *	Description:
 *		Determ_promo_type determines the result type of an operator
 *		that applies to its operand(s). In some cases the result type
 *		has to be promoted for the variable to hold a value adequately.
 *
 *	Inputs:
 *		exp   - Expression contained operator for determination. 
 *		type1 - Type of the first operand. 
 *		type2 - Type of the second operand.
 *		type3 - Type of the third operand.
 *		env_ptr - The environment for using in verify.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		The result type of an operator applying to its operands.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static TYPE_SPEC *
valid_determ_promo_type(EXPRESS *exp, TYPE_SPEC *type1,
	TYPE_SPEC *type2, TYPE_SPEC *type3, ENVIRON *env_ptr)
{
	int 			tmp_typ;
	int 			tmp_typ1;
	int 			tmp_typ2;
	int 			tmp_typ3;
	TYPE_SPEC 		*ret_type = NULL;

	/*
	 * Determine and promote the result type if necessary.
	 */
	switch (exp->op) {
		case ADD:
		case SUB:
		case MULT:
		case DIV:
		case MOD:
		case BIT_OR:
		case BIT_AND:
		case BIT_XOR:

			/*
			 * It was a binary operator.
			 * Get type that has greatest size of the binary operands.
			 */
			tmp_typ1 = type1->typ;
			tmp_typ2 = type2->typ;
			INTEGRAL_PROMOTION(tmp_typ1);
			INTEGRAL_PROMOTION(tmp_typ2);
			tmp_typ = SAME_TYPE(tmp_typ1, tmp_typ2);

			RESULT_TYPE_PTR(tmp_typ, ret_type);
			break;

		case SHIFT_L:
		case SHIFT_R:
		case UNARY_PLUS:
		case UNARY_MINUS:
		case COMPLEMENT:

			/*
			 * It was a unary operator.
			 * Promote to integer if it is a character type. Otherwise, 
			 * the result type of the unary operator is type of the only
			 * operand.
			 */
			if ((type1->typ == METH_CHAR_TYPE) || (type1->typ == METH_U_CHAR_TYPE)) {
				ret_type = LONG_T;
			}
			else {
				ret_type = type1;
			}
			break;

		case ASSIGN_ADD:
		case ASSIGN_SUB:
		case ASSIGN_MULT:
		case ASSIGN_DIV:
		case ASSIGN_MOD:
		case ASSIGN_BIT_OR:
		case ASSIGN_BIT_AND:
		case ASSIGN_BIT_XOR:
		case ASSIGN_SHIFT_L:
		case ASSIGN_SHIFT_R:
		case ASSIGN:

			/*
			 * Issue the warning if the float is assigned to the integral
			 * type. The loss of precision will be occurred if attempting
			 * to assign a float or double to a variable that could not
			 * hold precision.
			 */
			if (INTEGRAL_TYPE(type1->typ) && FLOATING_TYPE(type2->typ)) {
				warning(LOSS_OF_PRECISION);
			}

			/*
			 * Like an assign binary operator. The result type of this
			 * operator is the type of the operand that will hold the 
			 * result value. The first(or left hand side) operand.
			 */
			ret_type = type1;
			break;

		case PRE_INCR:
		case PRE_DECR:
		case POST_INCR:
		case POST_DECR:
		
			/*
			 * It was an unary operator. Therefore, 
			 * the result type is the only operand type.
			 */
			ret_type = type1;
			break;

		case COMMA:
			/*
			 * It was a binary operator comma. 
			 * The result type is the type of second/last operand.
			 */
			ret_type = type2;
			break;

		case ADDRESS_OF:	
			/*
			 * Pointer to the operand type.
			 */
			ret_type = bld_type(METH_PTR_TYPE, type1, 0L, env_ptr);
			break;

		case COND:
			if (ARITH_TYPE(type2->typ)) {

				/*
				 * Get type that has a greatest size of the operands.
				 */
				tmp_typ2 = type2->typ;
				tmp_typ3 = type3->typ;
				INTEGRAL_PROMOTION(tmp_typ2);
				INTEGRAL_PROMOTION(tmp_typ3);
				tmp_typ = SAME_TYPE(tmp_typ2, tmp_typ3);
		
				RESULT_TYPE_PTR(tmp_typ, ret_type);

			} else {
		
				/*
				 * If it is not an arithmetic type, the result
				 * type is the second operand type.
				 */ 
				ret_type = type2;
			}
			break;

		case EQ:
		case NEQ:
		case LT:
		case LE:
		case GT:
		case GE:
		case LOG_NOT:
		case LOG_OR:
		case LOG_AND:

			/* 
			 * The result type of the logical operator
			 * is the integer type.
			 */
			ret_type = LONG_T;
			break;

		case SYMREF:
		case ARRAYREF:
		case CONSTREF:
		case FUNCREF:
		default:
			CRASH_DBG();
			method_abort(
				"Default reached in promoting the result type:", env_ptr->env_info);
	}

	return (ret_type);
}


/*********************************************************************
 *
 *	Name: valid_expression()
 *	ShortDesc: Validates an expression and returns resulted type.
 *
 *	Description:
 *		valid_expression validates an expression for correctness and 
 *		returns the result type of an expression. The error or warning
 *		messages will be issued for the following problems:
 *			1) Missing operand(s) in an expression.
 *			2) Improper usage of operand type(s) in an expression.
 *			3) The effected operand is not a changeable symbol.
 *			4) Loss of precision on an operand.
 *
 *	Inputs:
 *		exp - Expression to be validated.
 *		env_ptr - The environment for using in verify.
 *
 *	Outputs:
 *		Error or Warning messages will be displayed on console if 	
 *			improper usage in expression.
 *
 *	Returns:
 *		The resulted type of an expression.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
TYPE_SPEC *
valid_expression(EXPRESS *exp, ENVIRON *env_ptr)
{
	int			op_cnt;				/* The expected number of operands */
	int			opkind;				/* The operand type(arith,integral) */
	int 		lvalue;				/* Flag indicate changeable operand */
	TYPE_SPEC 	*type1;				/* The first operand type */
	TYPE_SPEC	*type2;				/* The second operand type */
	TYPE_SPEC	*type3;				/* The third operand type */
	TYPE_SPEC	*ret_type;			/* The result type of expression */
	int 		nbr_errs = 0;		/* The error counter */


	/*
	 * Get necessary info for verifying an expression.
	 */
	op_cnt = valid_get_expr_info(exp, &opkind, &lvalue, env_ptr);

	type1 = (exp->op1 == NULL) ? NULL : exp->op1->type;
	type2 = (exp->op2 == NULL) ? NULL : exp->op2->type;
	type3 = (exp->misc.op3 == NULL) ? NULL : exp->misc.op3->type;

	/*
	 * Check the number of operands for an expression operator.
	 * Increment error counter if missing expected operand.
	 */
	if ((op_cnt == 1) && (type1 == NULL)) {
		nbr_errs++;
	}
	if ((op_cnt == 2) && ((type1 == NULL) || (type2 == NULL))) {
		nbr_errs++;
	}
    if ((op_cnt == 3) && 
		((type1 == NULL) || (type2 == NULL) || (type3 == NULL))) {
		nbr_errs++;
	}

	if (nbr_errs) {
		return (NULL);
	}

	/*
	 * Verify the types of the operands.
	 */
	nbr_errs = valid_operand_type(env_ptr, op_cnt,opkind,type1,type2,type3);

	if (lvalue) {

		/*
		 * Verify the operand can hold a value.
		 */
		if (!((exp->op1->op == SYMREF)
			|| ((exp->op1->op == ARRAYREF) && (exp->op1->op1->op == SYMREF)))) {

			error(env_ptr->env_info, LVALUE_REQUIRED);
			nbr_errs++;
		}
	}

	if ((exp->op == ADDRESS_OF) && !(env_ptr->in_bltin_ref) ) {
		error(env_ptr->env_info, ADDR_OF_OPERATOR_NOT_IN_FUNC_REF);
		nbr_errs++;
	}

	if (nbr_errs) {
		return (NULL);
	}

	/*
	 * Get the result type of an expression.
	 */
	ret_type = valid_determ_promo_type(exp,type1,type2,type3,env_ptr);

	return(ret_type);
}


/*********************************************************************
 *
 *	Name: valid_stmt()
 *	ShortDesc: Validates and issues error(s) for the statement.
 *
 *	Description:
 *		Valid_stmt verifies the statement for correctness in usage.  An 
 *		error message will be displayed on the console if any incorrect
 *		usage is found in the statement.  Returns false to indicate that
 *		an error is found in the statement.  Otherwise, true is returned.
 *
 *	Inputs:
 *		stmt 	- The statement under verify.
 *		env_ptr - The environment for using in verify.
 *
 *	Outputs:
 *		Error message(s) will be displayed on the console if
 *			found improper usage in the statement.
 *
 *	Returns:
 *		TRUE  - no error found.
 *		FALSE - error found.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
int
valid_stmt(STATEMENT *stmt, ENVIRON *env_ptr)
{
	int 		arith = 0;			/* Flag to indicate arith type */
	int 		integral = 0;		/* Flag to indicate integral type */
	int 		exp_req = 0;		/* Flag to indicate expr required */
	char 		*stmt_name = "";	/* Statement string for error mess */

	switch (stmt->stmt_type) {
		case NULL_STMT:
		case EXPRESSION:
		case RETURN:
		case COMPOUND:
			break;

		case IF:

			/*
			 * Set flags to indicate that the first expression is required
			 * for this statement and its type should be arithmetic type.
			 */ 
			exp_req++;
			arith++;
			stmt_name = "if";
			break;

		case DO:

			/*
			 * Set flags to indicate that the first expression is required
			 * for this statement and its type should be arithmetic type.
			 */ 
			exp_req++;
			arith++;
			stmt_name = "do-while";
			break;

		case WHILE:

			/*
			 * Set flags to indicate that the first expression is required
			 * for this statement and its type should be arithmetic type.
			 */ 
			exp_req++;
			arith++;
			stmt_name = "while";
			break;

		case FOR:
			if (stmt->exp2 != NULL) {
		
				/*
				 * Arithmetic type is required for the second expression.
				 */
				if (!ARITH_TYPE(stmt->exp2->type->typ)) {
					error(env_ptr->env_info, FOR_EXPRESSION_NOT_ARITHMETIC);
					return(FALSE);
				}
			}
			break;

		case CONTINUE:
			
			/*
			 * Verify a continue statement should be found within loop.
			 */ 
			if (!env_ptr->in_loop) {
				error(env_ptr->env_info, MISPLACED_CONTINUE);
				return(FALSE);
			}
			break;

		case BREAK:

			/*
			 * Verify a break statement should be found within
			 * a loop or switch statement.
			 */
			if (!(env_ptr->in_loop || ENV_SWITCH)) {
				error(env_ptr->env_info, MISPLACED_BREAK);
				return(FALSE);
			}
			break;

		case SWITCH:

			/*
			 * Set flags to indicate that the first expression is required
			 * for this statement and its type should be integral type.
			 */ 
			exp_req++;
			integral++;
			stmt_name = "switch";
			break;

		case CASE:

			/*
			 * Set flags to indicate that the first expression is required
			 * for this statement and its type should be integral type.
			 */ 
			exp_req++;
			integral++;
			stmt_name = "case";

			if (!ENV_SWITCH) {
				error(env_ptr->env_info, MISPLACED_CASE);
				return(FALSE);
			}
			break;

		case DEFAULT:
			if (!ENV_SWITCH) {
				error(env_ptr->env_info, MISPLACED_DEFAULT_CASE);
				return(FALSE);

			} else {
				if (ENV_SWITCH->flag == TRUE) {
					error(env_ptr->env_info, MULTIPLE_DEFAULT_CASES);
					return(FALSE);

				} else {
					ENV_SWITCH->flag = TRUE;
				}
			}
			break;

		default:
			CRASH_DBG();
			method_abort("Default reached in validating a statement:", env_ptr->env_info);
			/*NOREACH*/
	}

	if (exp_req && (stmt->exp1 == NULL)) {

		/*
		 * Return false for missing the first expression.
		 */
		return(FALSE);
	}

	if (arith && !ARITH_TYPE(stmt->exp1->type->typ)) {

		/* 
		 * Issue an error message and return false for 
		 * the resulted type of the first expression 
		 * is not an arithmetic type.
 		 */
		error(env_ptr->env_info, STATEMENT_REQUIRES_ARITHMETIC, stmt_name);
		return(FALSE);
	}

	if (integral && !INTEGRAL_TYPE(stmt->exp1->type->typ)) {

		/* 
		 * Issue an error message and return false for 
		 * the resulted type of the first expression 
		 * is not an integral type.
 		 */
		error(env_ptr->env_info, STATEMENT_REQUIRES_INTEGRAL, stmt_name);
		return(FALSE);
	}

	/*
	 * Verifying duplicated case in switch statement.
	 */
	if (stmt->stmt_type == CASE) {
		int i;
		long case_val;

		case_val = case_const(stmt->exp1, env_ptr);

		for (i = 0; i < ENV_SWITCH->size; i++) {
			if ((ENV_SWITCH->case_exp)[i] == case_val) {
				error(env_ptr->env_info, DUPLICATE_CASE_VALUE, case_val);
				return(FALSE);
			}
		}

		/*
		 * No duplicated case found for this case.
		 */
		ENV_SWITCH->case_exp = (long *)irealloc_chk(env_ptr, 
				(char *) ENV_SWITCH->case_exp, sizeof(long) * (size_t) (i + 1));
		(ENV_SWITCH->case_exp)[i] = case_val;
		ENV_SWITCH->size += 1;
	}


	/*
	 * No error found in expression.
	 */
	return (TRUE);
}
