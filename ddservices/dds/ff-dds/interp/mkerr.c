
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	This program will read the errors from a properly formatted input 
 *	file and generate both code and document files for those errors.
 */

#include	<stdio.h>
#include	<stdlib.h>
#ifndef STDARGS
	#define STDARGS
#endif 
#ifdef STDARGS
#	ifndef STDARG_H
#		include <stdarg.h>
#		define 	STDARG_H 1
#	endif
#else
#	include	<varargs.h>
#endif	/* STDARGS */

#include	<string.h>
#include	<ctype.h>
#include	"std.h"

/*character defines*/
#define	SPACE					0x20
#define	TAB						0x09

/*default sizes*/
#define	MAX_LINE_LEN			200
#define	MAX_FILE_NAME_LEN		100
#define	MAX_DEFINE_LEN			5
#define	MAX_SHORT_DESC_LEN		5
#define	MAX_FULL_DESC_LEN		5
#define	MAX_DEFINED_ERRORS		1000

/*string defines*/
#define	DEFAULT_BASE_FILE_NAME		"err_gen"
#define	DEFAULT_TEMPLATE_FILE_NAME	"err.t"

#define COPYRIGHT_MESSAGE		"Copyright 1996 Fieldbus Foundation All Rights Reserved."


/*enumerations*/
enum	{							/*states in parser*/
		CHECK_FOR_SPACE_TAB,CHECK_FOR_SPACE_TAB_CTRL,
		LOOKING_FOR_START_OF_DATA, PROCESSING_NUMBER, 
		PROCESSING_DEFINE, 
		PROCESSING_SHORT_DESC, 
		PROCESSING_FULL_DESC,
		PROCESSING_CONDITIONAL_INCLUDE,
		EXTRACT_CONDITION,
		SKIP_INCLUSION
		} state, next_state;


/*
 * Global variables
 */
FILE	*h_file = NULL;				/*pointer to generated header file*/
FILE	*c_file = NULL;				/*pointer to generated 'C' file*/
FILE	*err_file = NULL;			/*pointer to generated error document file*/
FILE	*template_file = NULL;		/*pointer to template file*/
FILE	*input_file = NULL;			/*pointer to source file for error definitions*/
char	verbose_mode = FALSE;		/*print informational messages*/
int		*processed_errors;			/*list of errors which have been processed*/
int		num_defined_errors;			/*number of errors which have been processed*/
int		next_error_number;			/*next number to be auto-assigned*/
int		auto_inc;					/*amount to auto-increment errors numbers by*/
int		forced_auto_numbering;		/*force auto-numbering*/
char	*error_define;				/*pointer to #define for error being processed*/
char	*error_short_desc;			/*pointer to short desc for error being processed*/
char	*error_full_desc;			/*pointer to full desc for error being processed*/
char	*conditional;				/*pointer to conditional being defined*/
char	base_file_name[MAX_FILE_NAME_LEN+1];	/*base name for generated files*/
char	template_file_name[MAX_FILE_NAME_LEN+1];/*name of template file*/
char	c_file_name[MAX_FILE_NAME_LEN+1];		/*name of generated 'C' file*/
char	h_file_name[MAX_FILE_NAME_LEN+1];		/*name of generated header file*/
char	err_file_name[MAX_FILE_NAME_LEN+1];		/*name of generated error file*/
char	file_line[MAX_LINE_LEN+1];				/*line being processed from input file*/


/*prototypes*/
void		main_xxx P((int,char **));

#ifndef WIN32
extern int	fgetc P((FILE *));
extern int	fclose P((FILE *));
#endif

#ifdef MSDOS
extern void	error_msg (char *,...);
extern void initial_generated_header_code (FILE *);
extern void ending_generated_header_code (FILE *);
extern void initial_generated_c_code (FILE *);
extern void ending_generated_c_code (FILE *);
extern void terminate (int);
extern void display_help (void);
#endif /* MSDOS */
#ifdef _lint
/*lint +fva variable number of arguments */
extern void	error_msg P((char *,...));
extern int	printf P((char *,...));
extern int	fprintf P((FILE *,char *,...));
extern void	vprintf P((char *,...));
/*lint -fva end of varying arguments */
#endif /* _lint */


/*********************************************************************
 *
 *	Name:  error_msg()
 *
 *	Description:
 *		Used to print an error message from mkerr.
 *
 *	Inputs:
 *		variable argument list, with a format string as the first
 *		argument
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
#ifdef STDARGS
void
error_msg (char *format,...)
{
	va_list ap;

	va_start(ap,format);
	vprintf( format, ap );
	va_end(ap);
}
#else
void
error_msg (va_alist)
va_dcl
{
    va_list ap;
    char *format;
 
	va_start(ap);
	format = va_arg(ap, char *);
	vprintf( format, ap );
	va_end(ap);
}
#endif /* STDARGS */


/*********************************************************************
 *
 *	Name:  initial_generated_header_code()
 *
 *	Description:
 *		Writes the initial generated header file code to the .h file.
 *
 *	Inputs:
 *		fptr	pointer to the file to write to
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
initial_generated_header_code(fptr)

FILE	*fptr;
{
	fprintf(fptr,"/*\n");
	fprintf(fptr," * %s.h (GENERATED FILE)\n",h_file_name);
	fprintf(fptr," *\n");
	fprintf(fptr," * %s\n",COPYRIGHT_MESSAGE);
	fprintf(fptr," *\n");
	fprintf(fptr," * This is the header file for the system error messages.\n");
	fprintf(fptr," */\n\n");

	fprintf(fptr,"#ifndef ERR_GEN_H\n");
	fprintf(fptr,"#define ERR_GEN_H\n");

	fprintf(fptr,"\n#ifdef __cplusplus\n");
	fprintf(fptr,"    extern \"C\" { \n");
	fprintf(fptr,"#endif\n\n");

	fprintf(fptr,"/*\n");
	fprintf(fptr," * Prototypes\n");
	fprintf(fptr," */\n");
	fprintf(fptr,"#ifdef STDARGS\n");
	fprintf(fptr,"extern void message (int,...);\n");
	fprintf(fptr,"extern void info (int,...);\n");
	fprintf(fptr,"extern void warning (int,...);\n");
	fprintf(fptr,"extern void error (int,...);\n");
	fprintf(fptr,"extern void fatal (int,...);\n");
	fprintf(fptr,"#else\n");
	fprintf(fptr,"extern void message ();\n");
	fprintf(fptr,"extern void info ();\n");
	fprintf(fptr,"extern void warning ();\n");
	fprintf(fptr,"extern void error ();\n");
	fprintf(fptr,"extern void fatal ();\n");
	fprintf(fptr,"#endif\n");
	fprintf(fptr,"\n");
	fprintf(fptr,"/*\n");
	fprintf(fptr," * Defines\n");
	fprintf(fptr," */\n");
}


/*********************************************************************
 *
 *	Name:  ending_generated_header_code()
 *
 *	Description:
 *		Writes the last of the generated header file code to the .h file.
 *
 *	Inputs:
 *		fptr	pointer to the file to write to
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
ending_generated_header_code(fptr)

FILE	*fptr;
{
	fprintf(fptr,"\n#ifdef __cplusplus\n");
	fprintf(fptr,"    } \n");
	fprintf(fptr,"#endif\n\n");

	fputs("#endif\n",fptr);
}


/*********************************************************************
 *
 *	Name:  initial_generated_header_code()
 *
 *	Description:
 *		Writes the initial generated header file code to the .h file.
 *
 *	Inputs:
 *		fptr	pointer to the file to write to
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
initial_generated_c_code(fptr)

FILE	*fptr;
{
	fputs("/********** START OF GENERATED CODE **********/\n\n",fptr);
	fprintf(fptr,"/*\n");
	fprintf(fptr," * Generated From Template File: %s\n",template_file_name);
	fprintf(fptr," */\n\n");

	fprintf(fptr,"#include	\"%s\"\n\n",h_file_name);

	fprintf(fptr,"/*\n");
	fprintf(fptr," * Structures\n");
	fprintf(fptr," */\n");
	fprintf(fptr,"typedef struct {\n");
	fprintf(fptr,"	unsigned short	number;\n");
	fprintf(fptr,"	char			*desc;\n");
	fprintf(fptr,"} ERROR_DATA;\n\n");

	fprintf(fptr,"static ERROR_DATA	system_errors[] = {\n");
}


/*********************************************************************
 *
 *	Name:  ending_generated_header_code()
 *
 *	Description:
 *		Writes the last of the generated header file code to the .h file.
 *
 *	Inputs:
 *		fptr	pointer to the file to write to
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
ending_generated_c_code(fptr)

FILE	*fptr;
{
	fprintf(fptr,"	0,	\"\"};\n\n");
	fprintf(fptr,"#define NUM_SYS_ERRORS	%d\n\n",num_defined_errors);
	fputs("/********** END OF GENERATED CODE **********/\n\n",fptr);
}


/*********************************************************************
 *
 *	Name:  display_help()
 *
 *	Description:
 *		Writes the help for this program to the screen.
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
display_help()
{

	printf("\nSYNTAX: errors [<options>] <input file>\n\n");
	printf("  Supported Options:\n");
	printf("    -a [starting error number]  force automatic error numbering\n");
	printf("    -c <if char>          		character identifying start of if for conditionally including error data group (DEFAULT: '#')\n");
	printf("    -e <ending char>            character following each error data group (DEFAULT: ';')\n");
	printf("    -h                          display this help screen\n");
	printf("    -i <number>                 automatic numbering increment value (default = 10)\n");
	printf("    -o <base file name>         base file name for generated files\n");
	printf("    -s <starting char>          character proceeding each error data group (DEFAULT: '>')\n");
	printf("    -t <template file name>     name of the template file to use (with extension)\n");
	printf("    -v                          verbose messages\n");
	printf("\n  The default output files are %s.c, %s.h, and %s.err\n",
			DEFAULT_BASE_FILE_NAME,DEFAULT_BASE_FILE_NAME,DEFAULT_BASE_FILE_NAME);
	printf("         \n");
}


/*********************************************************************
 *
 *	Name:  terminate()
 *
 *	Description:
 *		Handles clean-up for terminating the program.
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
terminate(code)

int		code;
{
	/*
	 * Close any open files
	 */
	if(input_file != NULL) {
		(void)fclose(input_file);
	}
	if(template_file != NULL) {
		(void)fclose(template_file);
	}
	if(c_file != NULL) {
		(void)fclose(c_file);
		if(code != 0) {
			c_file = fopen(c_file_name,"w");
			(void)fclose(c_file);
		}
	}
	if(err_file != NULL) {
		(void)fclose(err_file);
		if(code != 0) {
			err_file = fopen(err_file_name,"w");
			(void)fclose(err_file);
		}
	}

	/*
	 * Free any malloc'ed memory
	 */
	if(processed_errors)	free(processed_errors);
	if(error_define)		free(error_define);
	if(error_short_desc)	free(error_short_desc);
	if(error_full_desc)		free(error_full_desc);
	if(conditional)			free(conditional);

	exit(code);
}


/*********************************************************************
 *
 *	Name:  main()
 *
 *	Description:
 *		Starting point of the program.
 *
 *	Inputs:
 *		command line arguments
 *
 *	Outputs:
 *		none
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Nelson
 *
 *********************************************************************/
void
main_xxx(argc,argv)

int			argc;
char		*argv[];
{
	/*
	 * Data items needed for to perform conditional
	 * inclusion based upon DDL type
	 */
	int		include_block;
	int		product_block = 'AMS';
	int		nesting = 0;
	int		next_char = 0;
	int		previous_data = 0;
	int		previous_last_data = 0;
	int		conditional_len = 5;
	int		proc_found_include = FALSE;
	int		proc_prod_include = FALSE;
	/**/
	int		ii;
	int		start_of_error_data_char = '>';
	int		end_of_error_data_char = ';';
	int		conditional_include_char = '#';
	int		multiplier = 0;
	int		data;
	int		last_data = 0;
	int		string_length = 0;
	int		error_number;
	int		max_num_errors;
	int		error_define_len;
	int		error_short_desc_len;
	int		error_full_desc_len;
	int		insert_point_found = FALSE;
	char	skip_next_read;
	char	*ptr;

#ifdef TOK
	product_block = 'TOK';
#endif /*TOK*/
#ifdef SYN
	product_block = 'SYN';
#endif /*SYN*/
	include_block = 'FF';

	/*
	 * Malloc the data arrays and strings to their initial lengths (they
	 * will be realloc'ed to a larger size if needed)
	 */
	/*array for error numbers processed*/
	max_num_errors = MAX_DEFINED_ERRORS;
	processed_errors = (int *)malloc(max_num_errors * sizeof(int));
	if(processed_errors == NULL) {
		error_msg("\nOut Of Memory\n");
		terminate(1);
	}

	/*string for the define*/
	error_define_len = MAX_DEFINE_LEN;
	error_define = malloc(error_define_len);
	if(error_define == NULL) {
		error_msg("\nOut Of Memory\n");
		terminate(1);
	}

	/*string for the short description*/
	error_short_desc_len = MAX_SHORT_DESC_LEN;
	error_short_desc = malloc(error_short_desc_len);
	if(error_short_desc == NULL) {
		error_msg("\nOut Of Memory\n");
		terminate(1);
	}

	/*string for the full description*/
	error_full_desc_len = MAX_FULL_DESC_LEN;
	error_full_desc = malloc(error_full_desc_len);
	if(error_full_desc == NULL) {
		error_msg("\nOut Of Memory\n");
		terminate(1);
	}

	/*string for the conditional*/
	conditional = malloc(conditional_len);
	if(conditional == NULL) {
		error_msg("\nOut Of Memory\n");
		terminate(1);
	}

	/*
	 * Default the file names
	 */
	strcpy(base_file_name,DEFAULT_BASE_FILE_NAME);
	strcpy(template_file_name,DEFAULT_TEMPLATE_FILE_NAME);
	num_defined_errors = 0;			/*no errors defined yet*/
	verbose_mode = FALSE;			/*default message level*/
	input_file = NULL;				/*no input file open*/

	/*
	 * Set-up variables which can be changed via command line options
	 */
	auto_inc = 10;					/*amount to auto-increment errors numbers by*/
	next_error_number = auto_inc;	/*next number to be auto-assigned*/
	forced_auto_numbering = FALSE;	/*use numbers provided*/

	/*
	 * Process each of the command line arguments (except the first one which is
	 * always the program name)
	 */
	for(ii = 1; ii < argc; ii++) {
		/*
		 * if not preceeded by a `-` assume this is the input file name
		 */
		if(isalpha(argv[ii][0])) {
			/*
			 * Verify that only one input file is specified
			 */
			if(input_file != NULL) {
				error_msg("\nMultiple Input Files Defined\n");
			}
			/*
			 * Open the file
			 */
			else {
				input_file = fopen(argv[ii],"r");
				if(input_file == NULL) {
					error_msg("\nCan`t Open Input File \"%s\"\n",argv[ii]);
				}
				if(verbose_mode == TRUE) {
					printf("Input File: %s\n",argv[ii]);
				}
			}
		}

		/*
		 * Process non-input file command line options
		 */
		else {
			switch(argv[ii][1]) {
				/*
				 * Specify starting error number
				 */
				case 'a':
					forced_auto_numbering = TRUE;	/*turn auto-numbering on*/

					/*if starting number immediately follows*/
					if(isdigit(argv[ii][2])) {
						next_error_number = atoi(&argv[ii][2]);
					}
					else if((ii < (argc - 1)) && isdigit(argv[ii+1][1])) {
						error_msg("\nNo Starting Error Number Provided With \"-e\" Argument\n");
						display_help();
						terminate(1);
					}
					break;

				/*
				 * Specify character that starts inclusion block definition
				 */
				case 'c':	
					if(strlen(argv[ii]) == 3) {
						conditional_include_char = argv[ii][2];
					}
					else {
						error_msg(
							"\nInvalid Start Of Inclusion Character Option \"%s\"\n",argv[ii]);
						display_help();
						terminate(0);
					}
					break;

				/*
				 * Specify character that terminates a block of error data
				 */
				case 'e':
					if(strlen(argv[ii]) == 3) {
						end_of_error_data_char = argv[ii][2];
					}
					else {
						error_msg("\nInvalid End Of Data Character Option \"%s\"\n",argv[ii]);
						display_help();
						terminate(0);
					}
					break;

				/*
				 * Display help screen
				 */
				case 'h':
					display_help();
					terminate(0);
					break;

				/*
				 * Specify auto-increment value
				 */
				case 'i':
					/*if starting number immediately follows*/
					if(isdigit(argv[ii][2])) {
						auto_inc = atoi(&argv[ii][2]);
					}
					/*else if there is another arg assume number is there*/
					else if(ii < (argc - 1)) {
						ii++;						/*move to number argument*/
						auto_inc = atoi(argv[ii]);
					}
					else {
						error_msg("\nNo Auto-Increment Number Provided With \"-i\" Argument\n");
						display_help();
						terminate(1);
					}
					break;

				/*
				 * Specify base name of output files
				 */
				case 'o':
					if(ii < (argc - 1)) {
						ii++;						/*move to next argument*/
						if(strlen(argv[ii]) >= MAX_FILE_NAME_LEN) {
							error_msg("\nSpecified Base File Name Is Too Long\n");
							terminate(1);
						}
						strcpy(base_file_name,argv[ii]);
						ptr = strchr(base_file_name,'.');
						if(ptr)	*ptr = 0;
					}
					else {
						error_msg("\nNo Output File Named With \"-o\" Argument\n");
						display_help();
						terminate(1);
					}
					break;

				/*
				 * Specify character that starts a block of error data
				 */
				case 's':
					if(strlen(argv[ii]) == 3) {
						start_of_error_data_char = argv[ii][2];
					}
					else {
						error_msg("\nInvalid Start Of Data Character Option \"%s\"\n",argv[ii]);
						display_help();
						terminate(0);
					}
					break;

				/*
				 * Specify name of the template files
				 */
				case 't':
					if(ii < (argc - 1)) {
						if(strlen(argv[ii+1]) >= MAX_FILE_NAME_LEN) {
							error_msg("\nSpecified Template File Name Is Too Long\n");
							terminate(1);
						}
						strcpy(template_file_name,argv[ii+1]);
						ptr = strchr(template_file_name,'.');
						if(ptr)	*ptr = 0;
						ii++;						/*move past template file argument*/
					}
					else {
						error_msg("\nNo Template File Named With \"-t\" Argument\n");
						display_help();
						terminate(1);
					}
					break;

				/*
				 * Specify verbose mode
				 */
				case 'v':
					verbose_mode = TRUE;
					break;

				default:
					error_msg("\nUnknown Command Line Argument \"%s\" Found\n",argv[ii]);
					display_help();
					terminate(1);
					break;
			}
		}
	}

	/*
	 * Verify an input file was specified
	 */
	if(input_file == NULL) {
		error_msg("\nNo Input File Specified\n");
		display_help();
		terminate(1);
	}

	/*
	 * If in verbose mode, display the base name of the files
	 */
	if(verbose_mode == TRUE) {
		printf("Base File Name: %s\n",base_file_name);
		printf("Template File Name: %s\n",template_file_name);
	}

	/*
	 * open the generated files for writing (erasing them if they exist)
	 */
	/*header file*/
	sprintf(h_file_name,"%s.h",base_file_name);
	h_file = fopen(h_file_name,"w");
	if(h_file == NULL) {
		error_msg("\nCan`t Open Header File \"%s\"\n",h_file_name);
		terminate(1);
	}

	/*code file*/
	sprintf(c_file_name,"%s.c",base_file_name);
	c_file = fopen(c_file_name,"w");
	if(c_file == NULL) {
		error_msg("\nCan`t Open Code File \"%s\"\n",c_file_name);
		terminate(1);
	}

	/*error text file*/
	sprintf(err_file_name,"%s.err",base_file_name);
	err_file = fopen(err_file_name,"w");
	if(err_file == NULL) {
		error_msg("\nCan`t Open Document File \"%s\"\n",err_file_name);
		terminate(1);
	}

	/*
	 * open the template file for reading
	 */
	template_file = fopen(template_file_name,"r");
	if(template_file == NULL) {
		error_msg("\nCan`t Open Template File \"%s\"\n",template_file_name);
		terminate(1);
	}

	/*
	 * Read and copy the template file until the insertion point for the
	 * generated code is found
	 */
	do {
		printf("FIX THE FEOF ERROR HERE\n");
		if((fgets(file_line,MAX_LINE_LEN,template_file) == NULL) && (!feof(template_file))) {
			error_msg("\nError Reading from Template File \"%s\"\n",template_file_name);
			terminate(1);
		}
	    if(strstr(file_line,"GENERATED CODE GOES HERE") == NULL) {
			fputs(file_line,c_file);
		}
		else {
			insert_point_found = TRUE;
			break;
		}
	} while(!feof(template_file) && (insert_point_found == FALSE));

	/*
	 * Verify the insert point was found and terminate if it wasn't
	 */
	if(insert_point_found == FALSE) {
		error_msg("\nNo Insertion Point Found In Template File \"%s\"\n",template_file_name);
		terminate(1);
	}

	/*
	 * Write initial text to the generated files
	 */
	initial_generated_header_code(h_file);
	initial_generated_c_code(c_file);

	skip_next_read = FALSE;				/*set to read new character every loop*/
	state = LOOKING_FOR_START_OF_DATA;	/*set to look for the start of error block*/
	data = 0;
	error_number = 0;					/*initialize for error messages*/

	/*
	 * This loop will process the input file one byte at a time.  This is done using
	 * one character at a time via a state machine.  The characters are processed by
	 * the code for the current state, with each section of code setting the next
	 * state at the appropriate time.
	 */
	while(!feof(input_file)) {
		printf("FIX THE FEOF ERROR HERE\n");
		/*
		 * This is the code to read each data byte.  Typically this is done once per
		 * loop, but if other code has read the first byte of an item (number, define, etc)
		 * it can set the "skip_next_read" flag so that character is not lost.
		 */
		if(skip_next_read == FALSE) {
			last_data = data;
			data = fgetc(input_file);
		}
		else {
			skip_next_read = FALSE;
		}
		switch(state) {
			/*
			 * Check for spaces and tabs in either of these state, and in the
			 * CHECK_FOR_SPACE_TAB_CTRL state check for control characters.  If
			 * any of these characters are found, ignore the character.  If
			 * none of them are found, change to the next state and don't
			 * overwrite the current character.
			 */
			case CHECK_FOR_SPACE_TAB:
			case CHECK_FOR_SPACE_TAB_CTRL:
				if(((data != SPACE) && (data != TAB))
						&& ((state != CHECK_FOR_SPACE_TAB_CTRL) || !iscntrl(data))) {
					skip_next_read = TRUE;
					state = next_state;
					string_length = 0;		/*clear character counter for all strings*/
				}
				break;

			case LOOKING_FOR_START_OF_DATA:
				/*
				 * if start of new error data block is found, set-up
				 * to get number one digit at a time, else check to see
				 * if the ending character is found
				 */
				if (data == start_of_error_data_char) {
					state = PROCESSING_NUMBER;
					error_number = 0;
					multiplier = 10;
					if(verbose_mode == TRUE) {
						printf("   New Error-");
					}
				}
				else if ( (data == conditional_include_char) &&
						  (iscntrl (last_data) ) ) {

					if  ( proc_found_include || proc_prod_include)
					{
						state = PROCESSING_CONDITIONAL_INCLUDE;
                        next_char = 'e';
					}
					else
					{
						state = PROCESSING_CONDITIONAL_INCLUDE;
						next_char = 'i';
					};
					previous_last_data = last_data;
					previous_data = data;
				}
				else if((data == end_of_error_data_char) && iscntrl(last_data)) {
					error_msg("\nStart Of Data Charater Missing After Error %d\n",error_number);
					terminate(1);
				}
				break;

			case PROCESSING_NUMBER:
				/*
				 * process each number, moving the existing ones one
				 * digit and adding the new one in
				 */
				if(isdigit(data)) {
					error_number *= multiplier;
					error_number += (data - '0');
				}
				else {
					/*
					 * If forced automatic numbering is on, or if no number was
					 * provided, set the number
					 */
					if((error_number == 0) || (forced_auto_numbering == TRUE)) {
						error_number = next_error_number;
						next_error_number += auto_inc;
					}
					/*
					 * Else update the automatic next number with the this number
					 * plus the increment value if this is the largest error number
					 * so far
					 */
					else {
						if((error_number + auto_inc) > next_error_number) {
							next_error_number = error_number + auto_inc;
						}
					}

					/*verify this is not a duplicate number*/
					for(ii = 0; ii < num_defined_errors; ii++) {
						if(error_number == processed_errors[ii]) {
							error_msg("\nError Number %d Used Twice\n",error_number);
							terminate(1);
						}
					}
					/*if the array is full, expand it*/
					if(ii >= max_num_errors) {
						max_num_errors += 10;
						processed_errors = (int *)realloc((char *)processed_errors,
								max_num_errors*sizeof(int));
						if(processed_errors == NULL) {
							error_msg("\nOut Of Memory\n");
							terminate(1);
						}
					}
					processed_errors[ii] = error_number;
					if(verbose_mode == TRUE) {
						printf("%d ",error_number);
					}
					/*
					 * Start reading the short description, and skip reading the next
					 * character from the file since the current value of data is
					 * the first character of the short description
					 */
					state = CHECK_FOR_SPACE_TAB;
					next_state = PROCESSING_SHORT_DESC;
					/*
					 * skip the next character in case it is an end-of-line (this
					 * will "confuse" the program into thinking the define is the
					 * short description)
					 */
					skip_next_read = TRUE;
				}
				break;

			case PROCESSING_SHORT_DESC:
				/*verify that the short description was found initially*/
				if((string_length == 0) && (iscntrl(data) || isspace(data))) {
					error_msg("\nShort Description For Error %d Not Found\n",error_number);
					terminate(1);
				}
				/*resize string if it is full*/
				if(string_length >= error_short_desc_len) {
					error_short_desc_len += 50;
					error_short_desc = realloc(error_short_desc,error_short_desc_len);
					if(error_short_desc == NULL) {
						error_msg("\nOut Of Memory\n");
						terminate(1);
					}
				}

				/*if this is not the end of the line, store the data in the string*/
				if(!iscntrl(data)) {
					error_short_desc[string_length++] = (char)data;
				}
				/*
				 * If a control character is found assume it is the end of the line
				 * and process the short description
				 */
				else {
					/*strip white space off end of short description*/
					while((string_length > 0)
							&& isspace(error_short_desc[string_length-1])) {
						string_length--;
					}
					/*terminate line after last alphanumeric character*/
					error_short_desc[string_length] = 0;
					if(verbose_mode == TRUE) {
						printf("Short ");
					}
					/*
					 * Start reading the define, and skip reading the next
					 * character from the file since the current value of data is
					 * the first character of the define
					 */
					state = CHECK_FOR_SPACE_TAB_CTRL;
					next_state = PROCESSING_DEFINE;
					skip_next_read = TRUE;
				}
				break;

			case PROCESSING_DEFINE:
				/*verify the starting character for the define is valid*/
				if((string_length == 0) && !isupper(data)) {
					error_msg("\nDefine For Error %d Not Found\n",error_number);
					terminate(1);
				}
				/*verify the define is not too long*/
				if(string_length >= error_define_len) {
					error_define_len += 50;
					error_define = realloc(error_define,error_define_len);
					if(error_define == NULL) {
						error_msg("\nOut Of Memory\n");
						terminate(1);
					}
				}
				/*
				 * check if this is a legal character for a define, which means either upper
				 * case letter, digit (but not for the first letter), or allowed characters
				 */
				if(isupper(data) || (isdigit(data) && string_length > 0) || strchr("_-",data)) {
					error_define[string_length++] = (char)data;
				}
				/*
				 * if this is the end of the define (i.e. terminated by end-of-line
				 * or white space)
				 */
				else if(isspace(data)) {
					/*terminate define string*/
					error_define[string_length] = 0;
					/*
					 * Start reading the full description, and skip reading the next
					 * character from the file since the current value of data is
					 * the first character of the description
					 */
					state = CHECK_FOR_SPACE_TAB_CTRL;
					next_state = PROCESSING_FULL_DESC;
					skip_next_read = TRUE;
					if(verbose_mode == TRUE) {
						printf("Define ");
					}
				}
				/*else report the error and terminate the program*/
				else {
					if(string_length < 3) {
						error_msg(
							"\nDefine Is Missing and/or Improperly Terminated For Error %d\n",
							error_number);
					}
					else {
						error_msg(
							"\nBad Character \"%c\" Terminating Define Label For Error %d\n",
							data,error_number);
					}
					terminate(1);
				}
				break;

			case PROCESSING_FULL_DESC:
				if((string_length == 0) && ((data == start_of_error_data_char)
						|| (data == end_of_error_data_char))) {
					error_msg("\nFull Description For Error %d Not Found\n",error_number);
					terminate(1);
				}
				/*resize string if it is full*/
				if(string_length >= error_full_desc_len) {
					error_full_desc_len += 50;
					error_full_desc = realloc(error_full_desc,error_full_desc_len);
					if(error_full_desc == NULL) {
						error_msg("\nOut Of Memory\n");
						terminate(1);
					}
				}
				if((data == end_of_error_data_char) && iscntrl(last_data)) {
					/*strip white space off end of full description*/
					while((string_length > 0)
							&& isspace(error_full_desc[string_length-1])) {
						string_length--;
					}
					error_full_desc[string_length] = 0;
					if(verbose_mode == TRUE) {
						printf("Full\n");
					}
					state = LOOKING_FOR_START_OF_DATA;
					fprintf(h_file,"#define %-40s %d\n",error_define,error_number);
					fprintf(c_file,"	%d,	\"%s\",\n",error_number,error_short_desc);
					fprintf(err_file,"%d     %s\n%s\n\n",error_number,error_short_desc,
							error_full_desc);
					num_defined_errors++;
				}
				else if((data == start_of_error_data_char) && iscntrl(last_data)) {
					error_msg("\nEnd Of Data Charater Missing For Error %d\n",error_number);
					terminate(1);
				}
				else {
					error_full_desc[string_length++] = (char)data;
				}
				break;

			case PROCESSING_CONDITIONAL_INCLUDE:

				if (tolower (data) == next_char)

					switch (next_char) {

						case 'i':

							next_char = 'f';
							break;

						case 'f':
							state = CHECK_FOR_SPACE_TAB;
							next_state = EXTRACT_CONDITION;
							string_length = 0;
							break;

						case 'e':
						
							if (--nesting == 0)
							{
								next_char = 'n';
							} 
							else
							{
								/*
					 			 * Seeing an embedded conditional inclusion
					 			 */
								state = SKIP_INCLUSION;
								if (verbose_mode == TRUE)
								{
									printf ("Ending Nested Conditional\n");
									printf ("Returning to SKIP INCLUSION\n");
								};
							};
							break;

						case 'n':
							next_char = 'd';
							break;

						case 'd':
							if (proc_found_include)
								proc_found_include = FALSE;
							else if (proc_prod_include)
								proc_prod_include = FALSE;
							state = LOOKING_FOR_START_OF_DATA;
							break;
					}
				else if ( (tolower (data) == 'i') && (next_char == 'e') )
				{
					if ( proc_prod_include )
					{
						next_char = 'f';
						break;
					}
					else
					{

						/*
					 	* Seeing an embedded conditional inclusion
					 	*/
						state = SKIP_INCLUSION;
						nesting++;
						if (verbose_mode == TRUE)
						{
							printf ("Begining Nested Conditional\n");
							printf ("Returning to SKIP INCLUSION\n");
						};
					};
				}
				else if((previous_data == end_of_error_data_char) &&
						 iscntrl(previous_last_data))
				{
					error_msg(
						"\nStart Of Data Charater Missing After Error %d\n",error_number);
					terminate(1);
				}
				break;

			case EXTRACT_CONDITION:

				/*
				 * Expecting conditional specifier to be 'TOK', 'SYN', 'HART',
				 * 'HARTR', or 'FF'
				 */

				if((string_length == 0) && iscntrl(data) )
				{
					error_msg("\nConditional inclusion identifier Not Found\n");
					terminate(1);
				}
				if ( (string_length >= conditional_len) && !iscntrl (data) )
				{
					conditional_len += 5;
					conditional = realloc (conditional, conditional_len);
					if (conditional == NULL)
					{
						error_msg ("\nOut Of Memory processing conditional\n");
						terminate (1);
					}
				}


				/* if this is not end of the line, store the data in the
				 * conditional string
				 */
				if (!iscntrl (data) ) {
					conditional [string_length++] = toupper (data);
				}
				else {

					/*strip white space off end of full description*/
                    while ( (string_length > 0) &&
                            isspace (conditional [string_length - 1]) ) {
                        string_length--;
                    }
/*=====================*/
printf ("Conditional String = %s\n",conditional);
/*=====================*/

					if ( strstr (conditional, "TOK"))
					{
						nesting++;

						if (product_block == 'TOK')
						{
							if(verbose_mode == TRUE)
								printf ("TOK conditional inclusion processed\n");
							proc_prod_include = TRUE;
							state = LOOKING_FOR_START_OF_DATA;
						}
						else
						{
							if(verbose_mode == TRUE)
								printf ("TOK conditional inclusion is skipped\n");
							state = SKIP_INCLUSION;
						}
					}
					else if ( strstr (conditional, "SYN"))
					{
						nesting++;

						if (product_block == 'SYN')
						{
							if(verbose_mode == TRUE)
								printf ("SYN conditional inclusion processed\n");
							proc_prod_include = TRUE;
							state = LOOKING_FOR_START_OF_DATA;
						}
						else
						{
							if(verbose_mode == TRUE)
								printf ("SYN conditional inclusion is skipped\n");
							state = SKIP_INCLUSION;
						}
					}
					else if ( strstr (conditional, "HART"))
					{
						nesting++;

						if (include_block == 'HART')
						{
							if(verbose_mode == TRUE)
								printf ("HART conditional inclusion processed\n");
							proc_found_include = TRUE;
							state = LOOKING_FOR_START_OF_DATA;
						}
						else
						{
							if(verbose_mode == TRUE)
								printf ("HART conditional inclusion is skipped\n");
							state = SKIP_INCLUSION;
						}
					}
					else if (strstr (conditional, "FF"))
					{
						nesting++;

						if (include_block == 'FF')
						{
							if(verbose_mode == TRUE)
								printf ("FF conditional inclusion processed\n");
							proc_found_include = TRUE;
							state = LOOKING_FOR_START_OF_DATA;
						}
						else
						{
							if(verbose_mode == TRUE)
								printf ("FF conditional inclusion is skipped\n");
							state = SKIP_INCLUSION;
						}
					}
					else
					{
						error_msg ("\nInvalid Conditional inclusion identifier\n");
						state = SKIP_INCLUSION;
					}

					for (ii = 1; ii < string_length; ii++) conditional [ii] = ' ';
				}
				break;

			case SKIP_INCLUSION:

				if ( (data == conditional_include_char) &&
					 (iscntrl (last_data) ) )
				{
					if(verbose_mode == TRUE)
                        printf ("Looking for end of conditional inclusion\n");
					state = PROCESSING_CONDITIONAL_INCLUDE;
                    next_char = 'e';
				}
				break;

			default:
				error_msg("\nParser In Unknown State %d\n",state);
				error_msg("Last Error Processed: %d\n",error_number);
				terminate(1);
				break;
		}
	}

	/*check for correct state for end of program*/
	if(state != LOOKING_FOR_START_OF_DATA) {
		error_msg("\nData For Error %d Was Incomplete\n",error_number);
		terminate(1);
	}

	/*add closing text to generated files*/
	ending_generated_header_code(h_file);
	ending_generated_c_code(c_file);

	/*
	 * Read and copy the rest of the template file until the insertion
	 * point for the generated code is found
	 */
	do {
		if((fgets(file_line,MAX_LINE_LEN,template_file) == NULL) && (!feof(template_file))) {
			error_msg("\nError Reading from Template File \"%s\"\n",template_file_name);
			terminate(1);
		}
		fputs(file_line,c_file);
	} while(!feof(template_file));

	printf("%d Errors Processed\n",num_defined_errors);

	terminate(0);
}
