/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *  This file contains stub functions required for the interpreter.
 */

#include "int_inc.h"

/*********************************************************************
*
*  Name: exit_gracefully
*  ShortDesc: terminate program in a controlled manner
*
*  Description:
*      Terminate the method - currently stubed
*
*  Inputs:
*      None
*
*  Outputs:
*      None
*
*  Returns:
*      Nothing
*
*  Author:
*      Conrad Beaulieu
*
*********************************************************************/

void
exit_gracefully()
{

}
