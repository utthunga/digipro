/**
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *  This file contains the builtin wrapper functions.table
 */
#ifndef INT_BLTIN_TBL
#define INT_BLTIN_TBL

#include "std.h"

/* For the interp files, this file is included and shall define INCLUDE_BLTIN_TBL_RAW before including */

/* For the tokenizer, the BI_PROTO struct is redifined to not include all information since it  */
/* will only require the name of the built-ins, the valid menu flags, and the number of arguments for */
/* the built-in. */


#ifndef INCLUDE_BLTIN_TBL_RAW


#define NUM_ARGS		16


enum {
VOID_T=1,					
CHAR_T,					
SHORT_T,					
LONG_T,					
U_CHAR_T,				
U_SHORT_T,				
U_LONG_T,				
FLOAT_T,					
DOUBLE_T,				
ARRAY_OF_CHAR_T,			
ARRAY_OF_SHORT_T,		
ARRAY_OF_LONG_T,			
ARRAY_OF_U_SHORT_T,		
ARRAY_OF_U_LONG_T,		
PTR_TO_CHAR_T,			
PTR_TO_SHORT_T,			
PTR_TO_LONG_T,			
PTR_TO_U_CHAR_T,			
PTR_TO_U_SHORT_T,		
PTR_TO_U_LONG_T,			
PTR_TO_FLOAT_T,			
PTR_TO_DOUBLE_T
};

/*
 * Built-in prototype structure.
 */
typedef struct bi_proto BI_PROTO;
struct bi_proto {
	char		blkMenu;
	char		devMenu;
	void		*function;			/* Ptr to a built-in function */
	int			ret_type;				/* A type of its return value */
	char 		*bltin_name;			/* ASCII built-in name string */
	int		 	arg_type[NUM_ARGS];	/* The built-in parameter types */
};


#endif // !INCLUDE_BLTIN_TBL_RAW

#if !defined(INCLUDE_BLTIN_TBL_RAW) || defined(USE_NULL_BUILTINS)

//Defines for null functions...

#define r_mhd NULL
#define r_puts NULL
#define r_putchar NULL
#define wrap_add_abort_method NULL
#define wrap_remove_abort_method NULL
#define wrap_remove_all_abort_methods NULL
#define wrap_method_abort NULL
#define wrap_discard_on_exit NULL
#define wrap_send_on_exit NULL
#define wrap_save_on_exit NULL
#define wrap_get_sel_string NULL
#define wrap_get_sel_double NULL
#define wrap_get_sel_double2 NULL
#define wrap_get_float_value NULL
#define wrap_ret_float_value NULL
#define wrap_put_float_value NULL
#define wrap_get_string_lelem NULL
#define wrap_get_string_lelem2 NULL
#define wrap_get_date_lelem NULL
#define wrap_get_date_lelem2 NULL
#define wrap_get_double_lelem NULL
#define wrap_get_double_lelem2 NULL
#define wrap_get_float_lelem NULL
#define wrap_get_float_lelem2 NULL
#define wrap_get_signed_lelem NULL
#define wrap_get_signed_lelem2 NULL
#define wrap_get_unsigned_lelem NULL
#define wrap_get_unsigned_lelem2 NULL
#define wrap_get_double_value NULL
#define wrap_ret_double_value NULL
#define wrap_put_double_value NULL
#define wrap_get_signed_value NULL
#define wrap_ret_signed_value NULL
#define wrap_put_signed_value NULL
#define wrap_get_unsigned_value NULL
#define wrap_ret_unsigned_value NULL
#define wrap_put_unsigned_value NULL
#define wrap_get_string_value NULL
#define wrap_put_string_value NULL
#define wrap_get_date_value NULL
#define wrap_put_date_value NULL
#define wrap_assign NULL
#define wrap_read_value NULL
#define wrap_send_value NULL
#define wrap_send_all_values NULL
#define wrap_get_response_code NULL
#define wrap_get_comm_error NULL
#define wrap_abort_on_all_comm_errors NULL
#define wrap_abort_on_all_response_codes NULL
#define wrap_abort_on_comm_error NULL
#define wrap_abort_on_response_code NULL
#define wrap_fail_on_all_comm_errors NULL
#define wrap_fail_on_all_response_codes NULL
#define wrap_fail_on_comm_error NULL
#define wrap_fail_on_response_code NULL
#define wrap_retry_on_all_comm_errors NULL
#define wrap_retry_on_all_response_codes NULL
#define wrap_retry_on_comm_error NULL
#define wrap_retry_on_response_code NULL
#define wrap_get_status_string NULL
#define wrap_get_comm_error_string NULL
#define wrap_get_response_code_string NULL
#define wrap_display_builtin_error NULL
#define wrap_display_message NULL
#define wrap_display_dynamics NULL
#define wrap_display_comm_error NULL
#define wrap_display_response_code NULL
#define wrap_delayfor NULL
#define wrap_edit_device_value NULL
#define wrap_edit_local_value NULL
#define wrap_select_from_menu NULL
#define wrap_get_acknowledgement NULL
#define wrap_menu_display NULL
#define wrap_list_create_element NULL
#define wrap_list_create_element2 NULL
#define wrap_list_delete_element NULL
#define wrap_list_delete_element2 NULL
#define wrap_is_NaN NULL
#define wrap_NaN_value NULL
#define wrap_get_stddict_string NULL
#define wrap_resolve_block_ref NULL
#define wrap_resolve_list_ref NULL
#define wrap_resolve_param_ref NULL
#define wrap_resolve_local_ref NULL
#define wrap_resolve_param_list_ref NULL
#define wrap_resolve_array_ref NULL
#define wrap_resolve_record_ref NULL
#define wrap_resolve_selector_ref NULL
#define wrap_get_resolve_status NULL
#define wrap_get_dds_error NULL
#define wrap_bi_rcsim_set_response_code NULL
#define wrap_bi_rcsim_set_comm_err NULL
#define wrap_get_float NULL
#define wrap_put_float NULL
#define wrap_get_double NULL
#define wrap_put_double NULL
#define wrap_get_signed NULL
#define wrap_put_signed NULL
#define wrap_get_unsigned NULL
#define wrap_put_unsigned NULL
#define wrap_get_string NULL
#define wrap_put_string NULL
#define wrap_get_date NULL
#define wrap_put_date NULL
#define wrap_debug_line NULL
#define wrap_assign2 NULL
#define wrap_delayfor2 NULL
#define wrap_display_dynamics2 NULL
#define wrap_display_message2 NULL
#define wrap_edit_device_value2 NULL
#define wrap_edit_local_value2 NULL
#define wrap_get_acknowledgement2 NULL
#define wrap_get_block_instance_by_object_index NULL
#define wrap_get_block_instance_by_tag NULL
#define wrap_get_block_instance_count NULL
#define wrap_get_date_value2 NULL
#define wrap_get_double_value2 NULL
#define wrap_get_float_value2 NULL
#define wrap_get_signed_value2 NULL
#define wrap_get_string_value2 NULL
#define wrap_get_unsigned_value2 NULL
#define wrap_ret_double_value2 NULL
#define wrap_ret_float_value2 NULL
#define wrap_ret_signed_value2 NULL
#define wrap_ret_unsigned_value2 NULL
#define wrap_put_date_value2 NULL
#define wrap_put_double_value2 NULL
#define wrap_put_float_value2 NULL
#define wrap_put_signed_value2 NULL
#define wrap_put_string_value2 NULL
#define wrap_put_unsigned_value2 NULL
#define wrap_read_value2 NULL
#define wrap_select_from_menu2 NULL
#define wrap_send_value2 NULL
#define wrap_resolve_param_ref2 NULL
#define wrap_resolve_local_ref2 NULL
#define wrap_resolve_array_ref2 NULL
#define wrap_resolve_record_ref2 NULL

/* This get's rid of type mismatch errors in table init */
#undef NULL
#define NULL 0


#endif  // !defined(INCLUDE_BLTIN_TBL_RAW) || defined(USE_NULL_BUILTINS)




/*lint -save -e785 Too few initializer for aggregate */

static const BI_PROTO built_in[] = {
	{FALSE,FALSE,r_mhd, LONG_T, "mhd", {PTR_TO_LONG_T}},
	{FALSE,FALSE,r_puts, LONG_T, "puts", {PTR_TO_CHAR_T}},
	{FALSE,FALSE,r_putchar, LONG_T, "putchar", {LONG_T}},

	/* Abort Builtins*/
	{TRUE,TRUE,wrap_add_abort_method, LONG_T, "add_abort_method", {U_LONG_T}},
	{TRUE,TRUE,wrap_remove_abort_method, LONG_T, "remove_abort_method", {U_LONG_T}},
	{TRUE,TRUE,wrap_remove_all_abort_methods, VOID_T, "remove_all_abort_methods",	{NULL}},
	{TRUE,TRUE,wrap_method_abort, VOID_T, "method_abort", {ARRAY_OF_CHAR_T}},

	/* Variable Table Builtins */
	{TRUE,TRUE,wrap_discard_on_exit, VOID_T, "discard_on_exit", {NULL}},
	{TRUE,TRUE,wrap_send_on_exit, VOID_T, "send_on_exit", {NULL}},
	{TRUE,TRUE,wrap_save_on_exit, VOID_T, "save_on_exit", {NULL}},
	{TRUE,FALSE,wrap_get_sel_string, LONG_T, "get_sel_string", {ARRAY_OF_CHAR_T, U_LONG_T, U_LONG_T, U_LONG_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_get_sel_double, DOUBLE_T, "get_sel_double", 
			{U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_get_sel_double2, DOUBLE_T, "get_sel_double2", 
			{U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_get_float_value, LONG_T, "get_float_value", 
			{U_LONG_T, U_LONG_T, PTR_TO_FLOAT_T}},
	{TRUE,FALSE,wrap_ret_float_value, FLOAT_T, "ret_float_value", 
			{U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_put_float_value, LONG_T, "put_float_value",
			{U_LONG_T, U_LONG_T, DOUBLE_T}},
    {TRUE,FALSE,wrap_get_string_lelem, LONG_T, "get_string_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
    {TRUE,FALSE,wrap_get_string_lelem2, LONG_T, "get_string_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
    {TRUE,FALSE,wrap_get_date_lelem, LONG_T, "get_date_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
    {TRUE,FALSE,wrap_get_date_lelem2, LONG_T, "get_date_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
    {TRUE,FALSE,wrap_get_double_lelem, DOUBLE_T, "get_double_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_double_lelem2, DOUBLE_T, "get_double_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_float_lelem, FLOAT_T, "get_float_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_float_lelem2, FLOAT_T, "get_float_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_signed_lelem, LONG_T, "get_signed_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_signed_lelem2, LONG_T, "get_signed_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_unsigned_lelem, U_LONG_T, "get_unsigned_lelem",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {TRUE,FALSE,wrap_get_unsigned_lelem2, U_LONG_T, "get_unsigned_lelem2",
            {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_get_double_value, LONG_T, "get_double_value", 
			{U_LONG_T, U_LONG_T, PTR_TO_DOUBLE_T}},
	{TRUE,FALSE,wrap_ret_double_value, DOUBLE_T, "ret_double_value", 
			{U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_put_double_value, LONG_T, "put_double_value",
			{U_LONG_T, U_LONG_T, DOUBLE_T}},
	{TRUE,FALSE,wrap_get_signed_value, LONG_T, "get_signed_value", 
			{U_LONG_T, U_LONG_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_ret_signed_value, LONG_T, "ret_signed_value", 
			{U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_put_signed_value, LONG_T, "put_signed_value",
			{U_LONG_T, U_LONG_T, LONG_T}},
	{TRUE,FALSE,wrap_get_unsigned_value, LONG_T, "get_unsigned_value", 
			{U_LONG_T, U_LONG_T, PTR_TO_U_LONG_T}},
	{TRUE,FALSE,wrap_ret_unsigned_value, U_LONG_T, "ret_unsigned_value", 
			{U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_put_unsigned_value, LONG_T, "put_unsigned_value",
			{U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_get_string_value, LONG_T, "get_string_value", 
			{U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_put_string_value, LONG_T, "put_string_value",
			{U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,FALSE,wrap_get_date_value, LONG_T, "get_date_value", 
			{U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_put_date_value, LONG_T, "put_date_value",
			{U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,FALSE,wrap_assign, LONG_T, "assign", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},

	/* Communication Builtins */
	{TRUE,FALSE,wrap_read_value, LONG_T, "read_value", {U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_send_value, LONG_T, "send_value", {U_LONG_T, U_LONG_T}},
	{TRUE,TRUE,wrap_send_all_values, LONG_T, "send_all_values", {NULL}},
	{TRUE,TRUE,wrap_get_response_code, VOID_T, "get_response_code",
			{PTR_TO_U_LONG_T, PTR_TO_U_LONG_T, PTR_TO_U_LONG_T}},
	{TRUE,TRUE,wrap_get_comm_error, U_LONG_T, "get_comm_error", {NULL}},
	{TRUE,TRUE,wrap_abort_on_all_comm_errors, VOID_T, "abort_on_all_comm_errors", {NULL}},
	{TRUE,TRUE,wrap_abort_on_all_response_codes, VOID_T, "abort_on_all_response_codes", 
			{NULL}},
	{TRUE,TRUE,wrap_abort_on_comm_error, LONG_T, "abort_on_comm_error", {U_LONG_T}},
	{TRUE,TRUE,wrap_abort_on_response_code, LONG_T, "abort_on_response_code", {U_LONG_T}},
	{TRUE,TRUE,wrap_fail_on_all_comm_errors, VOID_T, "fail_on_all_comm_errors", {NULL}},
	{TRUE,TRUE,wrap_fail_on_all_response_codes, VOID_T, "fail_on_all_response_codes", 
			{NULL}},
	{TRUE,TRUE,wrap_fail_on_comm_error, LONG_T, "fail_on_comm_error", {U_LONG_T}},
	{TRUE,TRUE,wrap_fail_on_response_code, LONG_T, "fail_on_response_code", {U_LONG_T}},
	{TRUE,TRUE,wrap_retry_on_all_comm_errors, VOID_T, "retry_on_all_comm_errors", {NULL}},
	{TRUE,TRUE,wrap_retry_on_all_response_codes, VOID_T, "retry_on_all_response_codes", 
			{NULL}},
	{TRUE,TRUE,wrap_retry_on_comm_error, LONG_T, "retry_on_comm_error", {U_LONG_T}},
	{TRUE,TRUE,wrap_retry_on_response_code, LONG_T, "retry_on_response_code", {U_LONG_T}},
	{TRUE,FALSE,wrap_get_status_string, LONG_T, "get_status_string",
			{U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,TRUE,wrap_get_comm_error_string, LONG_T, "get_comm_error_string",
			{U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,TRUE,wrap_get_response_code_string, LONG_T, "get_response_code_string",
			{U_LONG_T, U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},

	/* Display Builtins */
	{TRUE,TRUE,wrap_display_builtin_error, LONG_T, "display_builtin_error", {U_LONG_T}},
	{TRUE,FALSE,wrap_display_message, LONG_T, "display_message",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},
	{TRUE,FALSE,wrap_display_dynamics, LONG_T, "display_dynamics",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},
	{TRUE,TRUE,wrap_display_comm_error, LONG_T, "display_comm_error", {U_LONG_T}},
	{TRUE,TRUE,wrap_display_response_code, LONG_T, "display_response_code",
			{U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_delayfor, LONG_T, "delayfor",
			{LONG_T, ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},
	{TRUE,FALSE,wrap_edit_device_value, LONG_T, "edit_device_value",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T, 
			U_LONG_T, U_LONG_T}},
	{TRUE,FALSE,wrap_edit_local_value, LONG_T, "edit_local_value",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T, 
			ARRAY_OF_CHAR_T}},
	{TRUE,FALSE,wrap_select_from_menu, LONG_T, "select_from_menu",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T,
			ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_get_acknowledgement, LONG_T, "get_acknowledgement",
			{ARRAY_OF_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},

    {TRUE,TRUE,wrap_menu_display, LONG_T, "MenuDisplay", {U_LONG_T, ARRAY_OF_CHAR_T, PTR_TO_U_LONG_T}}, 
/*  Removed as part of update to FF-644 4/2005
	{TRUE,TRUE,wrap_plot_create, LONG_T, "PlotCreate", {U_LONG_T}},
	{TRUE,TRUE,wrap_plot_destroy, LONG_T, "PlotDestroy", {LONG_T}},
	{TRUE,TRUE,wrap_plot_display, LONG_T, "PlotDisplay", {LONG_T}},
	{TRUE,TRUE,wrap_plot_edit, LONG_T, "PlotEdit", {LONG_T, U_LONG_T}},
*/
    {TRUE,TRUE,wrap_list_create_element, LONG_T, "ListInsert", {U_LONG_T, LONG_T, U_LONG_T}},
    {TRUE,TRUE,wrap_list_create_element2, LONG_T, "ListInsert2", {U_LONG_T, LONG_T, U_LONG_T, LONG_T, U_LONG_T}},
    {TRUE,TRUE,wrap_list_delete_element, LONG_T, "ListDeleteElementAt", {U_LONG_T, LONG_T}},
    {TRUE,TRUE,wrap_list_delete_element2, LONG_T, "ListDeleteElementAt2", {U_LONG_T, LONG_T, U_LONG_T, LONG_T}},

	/* Miscellaneous Builtins */
    {TRUE,TRUE,wrap_is_NaN, LONG_T, "is_NaN", {DOUBLE_T}},
    {TRUE,TRUE,wrap_NaN_value, LONG_T, "NaN_value", {PTR_TO_DOUBLE_T}},
	{TRUE,TRUE,wrap_get_stddict_string, LONG_T, "get_stddict_string",
			{U_LONG_T, ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,TRUE,wrap_resolve_block_ref, U_LONG_T, "resolve_block_ref",
			{U_LONG_T}},
	{TRUE,TRUE,wrap_resolve_list_ref, U_LONG_T, "resolve_list_ref",
			{U_LONG_T}},
	{TRUE,FALSE,wrap_resolve_param_ref, U_LONG_T, "resolve_param_ref",
			{U_LONG_T}},
	{TRUE,FALSE,wrap_resolve_local_ref, U_LONG_T, "resolve_local_ref",
			{U_LONG_T}},
	{TRUE,TRUE,wrap_resolve_param_list_ref, U_LONG_T, "resolve_param_list_ref",
			{U_LONG_T}},
	{TRUE,TRUE,wrap_resolve_array_ref, U_LONG_T, "resolve_array_ref",
			{U_LONG_T, U_LONG_T}},
	{TRUE,TRUE,wrap_resolve_record_ref, U_LONG_T, "resolve_record_ref",
			{U_LONG_T, U_LONG_T}},
	{TRUE,TRUE,wrap_resolve_selector_ref, U_LONG_T, "resolve_selector_ref",
			{U_LONG_T, U_LONG_T, U_LONG_T}},
	{TRUE,TRUE,wrap_get_resolve_status, LONG_T, "get_resolve_status", {NULL}},
	{TRUE,TRUE,wrap_get_dds_error, LONG_T, "get_dds_error", {ARRAY_OF_CHAR_T, LONG_T}},

#if defined(DEBUG) || defined(DDSTEST)
	{TRUE,TRUE,wrap_bi_rcsim_set_response_code, LONG_T, "bi_rcsim_set_response_code",
			{LONG_T, LONG_T, U_LONG_T}},
	{TRUE,TRUE,wrap_bi_rcsim_set_comm_err, LONG_T, "bi_rcsim_set_comm_err",
			{LONG_T, LONG_T, U_LONG_T}},
#endif /* defined(DEBUG) || defined(DDSTEST) */

	/* Pre/post Read/write builtins */
	{TRUE,FALSE,wrap_get_float, LONG_T, "get_float", {PTR_TO_FLOAT_T}},
	{TRUE,FALSE,wrap_put_float, LONG_T, "put_float", {DOUBLE_T}},
	{TRUE,FALSE,wrap_get_double, LONG_T, "get_double", {PTR_TO_DOUBLE_T}},
	{TRUE,FALSE,wrap_put_double, LONG_T, "put_double", {DOUBLE_T}},
	{TRUE,FALSE,wrap_get_signed, LONG_T, "get_signed", {PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_put_signed, LONG_T, "put_signed", {LONG_T}},
	{TRUE,FALSE,wrap_get_unsigned, LONG_T, "get_unsigned", {PTR_TO_U_LONG_T}},
	{TRUE,FALSE,wrap_put_unsigned, LONG_T, "put_unsigned", {U_LONG_T}},
	{TRUE,FALSE,wrap_get_string, LONG_T, "get_string", {ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_put_string, LONG_T, "put_string", {ARRAY_OF_CHAR_T, LONG_T}},
	{TRUE,FALSE,wrap_get_date, LONG_T, "get_date", {ARRAY_OF_CHAR_T, PTR_TO_LONG_T}},
	{TRUE,FALSE,wrap_put_date, LONG_T, "put_date", {ARRAY_OF_CHAR_T, LONG_T}},

    /* Debug */
    {TRUE,TRUE,wrap_debug_line, VOID_T, "_l", {U_LONG_T}},

	/* FF-900 5.1 Additions */
	{FALSE,TRUE,wrap_assign2, LONG_T, "assign2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	{FALSE,TRUE,wrap_delayfor2, LONG_T, "delayfor2", {LONG_T, PTR_TO_CHAR_T,PTR_TO_U_LONG_T,
                                           PTR_TO_U_LONG_T, PTR_TO_U_LONG_T, PTR_TO_U_LONG_T, LONG_T}},
	{FALSE,TRUE,wrap_display_dynamics2, LONG_T, "display_dynamics2", {PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, 
	                                                                      ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},
	{FALSE,TRUE,wrap_display_message2, LONG_T, "display_message2", {PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
	                                                                    ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},
	{FALSE,TRUE,wrap_edit_device_value2, LONG_T,  "edit_device_value2", {PTR_TO_CHAR_T,ARRAY_OF_U_LONG_T, 
                                                              ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
															  LONG_T, U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
	{FALSE,TRUE,wrap_edit_local_value2, LONG_T, "edit_local_value2",{PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
	                                                      ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, U_LONG_T, ARRAY_OF_CHAR_T}},  
	{FALSE,TRUE,wrap_get_acknowledgement2, LONG_T, "get_acknowledgement2",{PTR_TO_CHAR_T, ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T,
	                                                                           ARRAY_OF_U_LONG_T, ARRAY_OF_U_LONG_T, LONG_T}},    
	{FALSE,TRUE,wrap_get_block_instance_by_object_index, LONG_T, "get_block_instance_by_object_index",{U_LONG_T, PTR_TO_U_LONG_T}},
	{FALSE,TRUE,wrap_get_block_instance_by_tag, LONG_T, "get_block_instance_by_tag", {U_LONG_T, PTR_TO_CHAR_T, PTR_TO_U_LONG_T}},
	{FALSE,TRUE,wrap_get_block_instance_count, LONG_T, "get_block_instance_count", {U_LONG_T, PTR_TO_U_LONG_T}},
	{FALSE,TRUE,wrap_get_date_value2, LONG_T, "get_date_value2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T,
	                                                   PTR_TO_CHAR_T, PTR_TO_LONG_T }},
	{FALSE,TRUE,wrap_get_double_value2, LONG_T, "get_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_DOUBLE_T}},
	{FALSE,TRUE,wrap_get_float_value2, LONG_T, "get_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_FLOAT_T}},
	{FALSE,TRUE,wrap_get_signed_value2, LONG_T, "get_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_LONG_T}},
	{FALSE,TRUE,wrap_get_string_value2, LONG_T, "get_string_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,PTR_TO_LONG_T}},
	{FALSE,TRUE,wrap_get_unsigned_value2, LONG_T, "get_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_U_LONG_T}},

	{FALSE,TRUE,wrap_ret_double_value2, DOUBLE_T, "ret_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},
	{FALSE,TRUE,wrap_ret_float_value2, FLOAT_T, "ret_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},
	{FALSE,TRUE,wrap_ret_signed_value2, LONG_T, "ret_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},
	{FALSE,TRUE,wrap_ret_unsigned_value2, U_LONG_T, "ret_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},

	{FALSE,TRUE,wrap_put_date_value2, LONG_T, "put_date_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,LONG_T}},
	{FALSE,TRUE,wrap_put_double_value2, LONG_T, "put_double_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,DOUBLE_T}},
	{FALSE,TRUE,wrap_put_float_value2, LONG_T, "put_float_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,DOUBLE_T}},
	{FALSE,TRUE,wrap_put_signed_value2, LONG_T, "put_signed_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,LONG_T}},
	{FALSE,TRUE,wrap_put_string_value2, LONG_T, "put_string_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,PTR_TO_CHAR_T,LONG_T}},
	{FALSE,TRUE,wrap_put_unsigned_value2, LONG_T, "put_unsigned_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},
	{FALSE,TRUE,wrap_read_value2, LONG_T, "read_value2", {U_LONG_T,U_LONG_T,U_LONG_T,U_LONG_T}},
	{FALSE,TRUE,wrap_select_from_menu2, LONG_T, "select_from_menu2", {PTR_TO_CHAR_T, PTR_TO_U_LONG_T, PTR_TO_U_LONG_T, PTR_TO_U_LONG_T, PTR_TO_U_LONG_T,
			                                               LONG_T, PTR_TO_CHAR_T, PTR_TO_LONG_T}},
	{FALSE,TRUE,wrap_send_value2, LONG_T, "send_value2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {FALSE,TRUE,wrap_resolve_param_ref2, U_LONG_T, "resolve_param_ref2", {U_LONG_T, U_LONG_T, U_LONG_T}},
    {FALSE,TRUE,wrap_resolve_local_ref2, U_LONG_T, "resolve_local_ref2", {U_LONG_T, U_LONG_T, U_LONG_T}},
    {FALSE,TRUE,wrap_resolve_array_ref2, U_LONG_T, "resolve_array_ref2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},
    {FALSE,TRUE,wrap_resolve_record_ref2, U_LONG_T, "resolve_record_ref2", {U_LONG_T, U_LONG_T, U_LONG_T, U_LONG_T}},

	/* Table Termination */
	{FALSE,FALSE, NULL, NULL, NULL, {NULL}},
};

#ifndef INCLUDE_BLTIN_TBL_RAW

/* Put NULL back to normal */
#undef  NULL
#define NULL    ((void *)0)

#endif
/*lint -restore */

#endif
