
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  Description : functions that replace calls that differ between 
 *                method executor application and plant server. 
 *
 ******************************************************************************/

#include "int_inc.h"

#ifdef MI
#	include "dci.h"         // DCI OLE wrapper class
#	include "midlg.h"       // MI Method Obj Class description

int warning_level = 1;      // Others find this in ff_sup.cpp
#endif /* MI */

extern SYM *bi_func_symbols_a;	/* built-in symbols list */

/******************************************************************************
 * FmsGetActionStorage
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : 
 *
 ******************************************************************************/
int 
FmsGetActionStorage(ENV_INFO *env_info, P_REF *ip_ref, P_REF *p_ref)
{
#ifdef MI

    return PC_SUCCESS;

#else

    int         param_count;
    int         rs;

    rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, ip_ref);
    if (rs != SUCCESS) {
        return (rs);
    }

    return (pc_get_action_storage (env_info, ip_ref, &param_count, 0));

#endif /* MI */
} /* FmsGetActionStorage */


/******************************************************************************
 * FmsPutActionStorage
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : 
 *
 ******************************************************************************/
int 
FmsPutActionStorage(ENV_INFO *env_info, P_REF *ip_ref, int replicate)
{
#ifdef MI

    return PC_SUCCESS;

#else

    return (pc_put_action_storage(env_info, ip_ref, replicate));

#endif /* MI */
} /* FmsPutActionStorage */


/******************************************************************************
 * FmsInitBiSymbols
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *                IN        BLOCK_HANDLE   Handle to device block.
 *
 *  Returns     : ?
 *
 *  Description : Determines whether type A or type B DDL method and then
 *                creates symbol table. 
 *
 ******************************************************************************/
int
FmsInitBiSymbols(ENVIRON* env, BLOCK_HANDLE block_handle)
{
#ifdef MI
	init_a_bi_symbols();
	if (env != NULL) env->global_scope->symbols = bi_func_symbols_a;

    return CM_SUCCESS;

#else

    DEVICE_HANDLE    device_handle;
    int              rs;
	NET_TYPE         net_type;

    rs = get_abt_adt_offset (block_handle, &device_handle);
    if (rs != CM_SUCCESS) {
        return (rs);
    }

    rs = get_adt_net_type(device_handle, &net_type);
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	init_a_bi_symbols();
	if (env != NULL) env->global_scope->symbols = bi_func_symbols_a;

    return CM_SUCCESS;

#endif /* MI */
} /* FmsInitBiSymbols */


/******************************************************************************
 * FmsCloseMethod
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *                IN        BLOCK_HANDLE   Handle to device block.
 *                OUT       NET_TYPE       Type A or type B device.
 *
 *  Returns     : ?
 *
 *  Description : Determines whether type A or type B DDL method. 
 *
 ******************************************************************************/
int
FmsCloseMethod(ENV_INFO* env_info, int option)
{
#ifdef MI

	return BLTIN_SUCCESS;

#else

    return (pc_method_close (env_info, option));

#endif /* MI */
} /* FmsCloseMethod */


/******************************************************************************
 * FmsCleanItem
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : Cleans up flat structure within DDS
 *
 ******************************************************************************/
int
FmsCleanItem(DDI_GENERIC_ITEM*    generic_item)
{
#ifdef MI

	return SUCCESS;

#else

	int 			retval;

	retval = ddi_clean_item(generic_item);

    return (retval);

#endif /* MI */
} /* FmsCleanItem */


/******************************************************************************
 * FmsGetMethodDefinition
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : Gets method definition
 *
 ******************************************************************************/
int
FmsGetMethodDefinition(DDI_BLOCK_SPECIFIER* block_spec, 
                       DDI_ITEM_SPECIFIER*  item_spec,
                       ENV_INFO*            env_info,
                       DDI_GENERIC_ITEM*    generic_item)
{
#ifdef MI

	void* pObj = ((APP_INFO*)(env_info->app_info))->pMethod;
	generic_item->item = (void*)(&((CFMSMethodObj*)pObj)->m_FlatMethod);
	return SUCCESS;

#else

	int 			retval;

	retval = ddi_get_item(block_spec,
			      item_spec, 
			      env_info,
			      (unsigned long) METHOD_DEF, 
			      generic_item);

	if (retval != SUCCESS) {
		if (retval == DDL_CHECK_RETURN_LIST) {
			ASSERT_DBG(generic_item->errors.count != 0);
			retval = generic_item->errors.list[0].rc;
			ASSERT_DBG(retval != SUCCESS);
		}
		(void) ddi_clean_item(generic_item);
	}

    return (retval);

#endif /* MI */
} /* FmsGetDefinition */


/******************************************************************************
 * FmsPut
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : 
 *
 ******************************************************************************/
void
FmsPut(char* msg)
{
#ifdef MI
	DEBUGOUT(OUTFILE,"%s\n",msg);
	FMSASSERT( 0 );

#else
	(void) fputs( msg, stderr );
	(void) putc( '\n', stderr );
	assert( 1 );

#endif /* MI */
} /* FmsPut */


/******************************************************************************
 * FmsWrite
 *
 *  Parameters  : IN/OUT    Type           Description
 *                -------------------------------------------------------------
 *
 *  Returns     : ?
 *
 *  Description : 
 *
 ******************************************************************************/
void
FmsWrite(const void* buffer, size_t size, size_t count, FILE* stream)
{
#ifdef MI
	TRACE("%s\n",buffer);

#else
	(void) fwrite( (char *) buffer, size, count, stream );

#endif /* MI */
} /* FmsWrite */
