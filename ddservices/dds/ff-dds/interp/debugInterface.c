#include "debugInterface.h"

debugCB_t	globalDebugCallback;
brkpnt_t	*globalBrkPnts;
int			numBrkPnts;

void	SetBP( brkpnt_t *bpts, int numBpts, debugCB_t callback)
{
	if( callback ) globalDebugCallback = callback;
	if( bpts )
	{
		globalBrkPnts = bpts;
		numBrkPnts = numBpts;
	}
}