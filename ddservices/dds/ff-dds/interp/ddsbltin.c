/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*

 #   #    ###    #####   #####           #   #    ###    #####   #####
 #   #   #   #   # # #   #               #   #   #   #   # # #   #
 ##  #   #   #     #     #               ##  #   #   #     #     #
 # # #   #   #     #     ####            # # #   #   #     #     ####
 #  ##   #   #     #     #               #  ##   #   #     #     #
 #   #   #   #     #     #               #   #   #   #     #     #
 #   #    ###      #     #####           #   #    ###      #     #####

                NO BUILTIN MAY RETURN METH_INTERNAL_ERROR
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>		/* only used for UNIX sleep() function */
#include <string.h>
#include <memory.h>
#include <ctype.h>
#include <time.h>

#include "std.h"
#include "sysparam.h"
#include "ddsbltin.h"
#include "ddi_lib.h"
#include "dict.h"
#include "evl_loc.h"
#include "dds_upcl.h"
#include "int_meth.h"
#include "int_lib.h"
#include "int_loc.h"
#include "od_defs.h"
#include "ddldefs.h"
#include "pc_loc.h"
#include "pc_lib.h"
#include "int_diff.h"
#include "debugInterface.h"
#include "rcsim.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#if !defined(MSDOS) && !defined(WIN32) && !defined(__linux__)
extern int fprintf();
#endif /* MSDOS */


/* Function Prototypes for bltin static functions which must be positioned after
 * other routines in this file which call them.
 */
static int bltin_select_from_menu P((char *prompt, ITEM_ID *param_ids,
        SUBINDEX *subindices, char *options, long *selection, BLOCK_HANDLE *bhs, int use_bh,
        ENV_INFO2 *env_info));
static int bltin_get_response_code_string P((ITEM_ID id, SUBINDEX subindex,
    UINT32 code, char *out_buf, long maxlength, ENV_INFO2 *env_info));
static void bltin_free_string P((STRING *dds_str));
static void mi_handle_suppression P((ENV_INFO2 *env_info, char *out_buf));

/*
 *	EUC functions don't currently exist.  Treat EUC strings as
 *	normal ASCII strings.
 */

#define eucncpy(a,b,c) strncpy((a),(b),(c))
#define find_euc_lang_string(a) find_lang_string(a)
#define euc_sprintf(a,b,c) sprintf((a),(b),(c))

#define SELECTOR_NOT_FOUND "Fatal: Invalid selector reference. ID=0x%lx, selector(%lu,%lu), error code=%u"
#define IS_MEMBER(id) ((id) & 0x40000000)

#define MAX_PROMPT_BUFFER_SIZE 2000
/*
 *	Function upcalls
 */

int (*display_upcall) P((ENV_INFO *br, char *buffer)) = NULL;
int (*display_continuous_upcall) P((ENV_INFO *br, char *prompt,
                ITEM_ID *param_ids, SUBINDEX *subindices)) = NULL;
int (*display_get_upcall) P((ENV_INFO *br, char *buffer,
                P_REF *pc_p_ref, EVAL_VAR_VALUE *param_value)) = NULL;
int (*display_menu_upcall) P((ENV_INFO *br, char *buffer,
                SELECT_LIST *select_list, int *selection)) = NULL;
int (*display_menu_upcall2) P((ENV_INFO *br, SELECT_LIST *select_list,
                              int *selection, ITEM_ID menuid)) = NULL;
int (*display_ack_upcall) P((ENV_INFO *br, char *buffer)) = NULL;
long (*list_delete_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index)) = NULL;
long (*list_delete_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
                                      ITEM_ID embedded_list_id, int embedded_index)) = NULL;
long (*list_insert_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
                                     ITEM_ID element)) = NULL;
long (*list_insert_element2_upcall) P((ENV_INFO *env_info, ITEM_ID list_id, int index,
            ITEM_ID embedded_list_id, int embedded_index, ITEM_ID embedded_element)) = NULL;
long (*list_read_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
                                         ITEM_ID subelement, ITEM_ID subsubelement,
                                         EVAL_VAR_VALUE *out_elem_data)) = NULL;
long (*list_read_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
                                         ITEM_ID embedded_list_id, int embedded_index,
                                         ITEM_ID embedded_subelement, ITEM_ID embedded_subsubelement,
                                         EVAL_VAR_VALUE *out_elem_data)) = NULL;
long (*list_count_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, unsigned long *count)) = NULL;
long (*list_count_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
                                     ITEM_ID embedded_list_id, unsigned long *count)) = NULL;
long (*plot_create_upcall) P((ENV_INFO *br, ITEM_ID plot)) = NULL;
long (*plot_destroy_upcall) P((ENV_INFO *br, long plot_handle)) = NULL;
long (*plot_display_upcall) P((ENV_INFO *br, long plot_handle)) = NULL;
long (*plot_edit_upcall) P((ENV_INFO *br, long plot_handle, unsigned long waveform_id)) = NULL;
long (*method_start_upcall) P((ENV_INFO *br, ITEM_ID meth_item_id)) = NULL;
long (*method_complete_upcall) P((ENV_INFO *br, ITEM_ID meth_item_id, int rs)) = NULL;
long (*method_error_upcall) P((ENV_INFO *br, char *errorStr)) = NULL;
int (*app_get_block_instance_by_index) P((ENV_INFO *env_info,
                                         unsigned long objectIndex,
                                         unsigned long *out_instance)) = NULL;
int (*app_get_block_instance_by_tag) P((ENV_INFO *env_info,
                                         ITEM_ID block_id,
                                         char *blockTag,
                                         unsigned long *out_instance)) = NULL;
int (*app_get_block_instance_count) P((ENV_INFO *env_info,
                                             ITEM_ID block_id,
                                             unsigned long *out_count)) = NULL;
int (*display_continuous_upcall2) P((ENV_INFO *br, char *prompt,
                                    ITEM_ID *param_ids, SUBINDEX *subindices,
                                    BLOCK_HANDLE *block_handles)) = NULL;
int (*app_get_selector_upcall) P((ENV_INFO *br, ITEM_ID id, ITEM_ID selector,
                                    ITEM_ID additional_id /* currently unused */,
                                    double *sel_value)) = NULL;


/*
 *	Builtin support routines.
 */

#if defined(WIN32)
#include <windows.h>
/*
 * sleep function required for Microsoft compiler only
 */
/*********************************************************************
 *
 *  Name:	sleep
 *
 *  ShortDesc:	time delay function (MS DOS only)
 *
 *  Description:
 *		Provides a time delay function for MS DOS compilers
 *		that have no built in sleep function
 *
 *  Inputs:
 *		delay -		time delay in seconds
 *
 *  Outputs:
 *		None
 *
 *  Returns:
 *		None
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/
void
sleep(unsigned int delay)
{
    Sleep(delay * 1000);
}
#endif

/**
 * The following structures and data definitions are used by the
 * check_printf_format_string() functions.
 *
 * This table is used to determine whether or not a printf-like format string is
 * valid for the type of variable being used in the string.
 *
 * Across the top of the table are the valid characters for a printf format string.
 * The values in the table represent a parse "level".  Format string are examined
 * character by character. Based on the type of the variable, this table is searched
 * to see what level the character represents for this variable type.  If the table
 * is blank for the type of variable being formated and the character being parsed,
 * the format string is invalid.
 *
 * Other format string rules:
 *     1) All format strings must start with a '%' character.
 *     2) Levels must increment, except in the case where '=' follows the level number.
 *           For instance: the format string "%hld" for an INTEGER variable is invalid
 *           because the level for the character "l" is the same as the level for
 *           character "h".  Note: "%33d" is OK since digits in the table have the '='
 *           following the level number.
 *     3) After parsing the format string, the level must equal 7.
 *
 * In table:  '#' = { '0', '1', '2',...,'9' }
 *
 * Note:  In the table, digits are listed at two different levels.  This is because
 *        digits can appear in format strings in two places: field width and
 *        display precision.
 *
 *						Valid printf Characters
 * DDL Type           %  -  #  .  #  h  l  d  i  x  X  u  c  s  f  e  E  g  G
 *
 * INTEGER            1  2  3= 4  5= 6  6  7  7  7  7  7
 * UNSIGNED           1  2  3= 4  5= 6  6  7  7  7  7  7
 * INDEX              1  2  3= 4  5=                         7
 * ENUMERATED         1  2  3= 4  5=                         7
 * BIT_ENUMERATED     1  2  3= 4  5= 6  6  7  7  7  7  7
 * FLOAT              1  2  3= 4  5= 6  6                       7  7  7  7  7
 * DOUBLE             1  2  3= 4  5= 6  6                       7  7  7  7  7
 * ASCII              1  2  3= 4  5=                      7  7
 * PASSWORD           1  2  3= 4  5=                      7  7
 * BITSTRING          1  2  3= 4  5= 6  6        7  7
 * OCTETSTRING        1  2  3= 4  5= 6  6        7  7
 * VISIBLESTRING      1  2  3= 4  5= 6  6        7  7
 * EUC                1  2  3= 4  5=                         7
 *
 * This table is represented by the use of the PRINTF_LIKE_REC structure and the
 * DDL_PRINTF_DATA structure.  An initialized array of DDL_PRINTF_DATA structures is
 * used to represent the data in this table.
 *
 * Note:  Digits are handled as a special case in the check_printf_format_string()
 *        function.  Because of this digits are not represented in the DDL_PRINTF_DATA
 *        array.
 */

#define BAD_DDL_TYPE			-983848
#define MAX_VALID_PRINTF_CHARS	9
#define NUC						'z' /* Not Used Character in printf array. */
#define FIRST_DIGIT_LEVEL		3
#define SECOND_DIGIT_LEVEL		5
#define REQUIRED_LAST_LEVEL		7

typedef struct {
    char	character;
    long	level;
} PRINTF_LIKE_REC;

typedef struct {
    long	ddl_type;
    long	valid_chars; /* Number of valid chars in the printf_data array. */
    PRINTF_LIKE_REC	printf_data[MAX_VALID_PRINTF_CHARS];
} DDL_PRINTF_DATA;

/*
 * All "{" and "}" removed from initialization to quiet lint.
 */
static DDL_PRINTF_DATA	printf_info[] = {
    { DDS_INTEGER, 			9,	{{'-',2}, {'.',4}, {'h',6}, {'l',6}, {'d',7}, {'i',7}, {'x',7}, {'X',7}, {'u',7}}},
    { DDS_UNSIGNED,			9,	{{'-',2}, {'.',4}, {'h',6}, {'l',6}, {'d',7}, {'i',7}, {'x',7}, {'X',7}, {'u',7}}},
    { DDS_INDEX,			3,	{{'-',2}, {'.',4}, {'s',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_ENUMERATED,		3,	{{'-',2}, {'.',4}, {'s',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_BIT_ENUMERATED,		9,	{{'-',2}, {'.',4}, {'h',6}, {'l',6}, {'d',7}, {'i',7}, {'x',7}, {'X',7}, {'u',7}}},
    { DDS_FLOAT,			9,	{{'-',2}, {'.',4}, {'h',6}, {'l',6}, {'f',7}, {'e',7}, {'E',7}, {'g',7}, {'G',7}}},
    { DDS_DOUBLE,			9,	{{'-',2}, {'.',4}, {'h',6}, {'l',6}, {'f',7}, {'e',7}, {'E',7}, {'g',7}, {'G',7}}},
    { DDS_ASCII,			4,	{{'-',2}, {'.',4}, {'s',7}, {'c',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_PASSWORD,			4,	{{'-',2}, {'.',4}, {'s',7}, {'c',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_BITSTRING,		4,	{{'-',2}, {'.',4}, {'x',7}, {'X',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_OCTETSTRING,		4,	{{'-',2}, {'.',4}, {'x',7}, {'X',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
//	 { DDS_VISIBLESTRING,		5,	{{'-',2}, {'.',4}, {'x',7}, {'X',7}, {'s',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}}.
    { DDS_VISIBLESTRING,		4,	{{'-',2}, {'.',4}, {'x',7}, {'X',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { DDS_EUC,			3,	{{'-',2}, {'.',4}, {'s',7}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}},
    { BAD_DDL_TYPE,			0,	{{NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}, {NUC,0}}}
};


/**********************************************************************
 *
 *  Name: check_printf_format_string
 *  ShortDesc: Checks validity of printf format strings based on a DDL data type.
 *
 *  Description:
 *
 *		This routine parses the printf format string to check whether the string
 *		is valid for the type of DDL parameter passed to the function.
 *
 *		The printf string should contain one "%" print specification only and must
 *		start with a "%" character.
 *
 *		Null format strings are considered OK.  BLTIN_SUCCESS is immediately returned.
 *
 *		Not all DDL types are covered by this routine.  Only the following types are
 *		implemented: INTEGER, UNSIGNED, INDEX, ENUMERATED, BIT_ENUMERATED, FLOAT,
 *		DOUBLE, ASCII, PASSWORD, BITSTRING, OCTETSTRING, VISIBLESTRING, and EUC.
 *
 *		If the data_type does not equal any of the types above, BLTIN_SUCCESS
 *		is returned.
 *
 *  Inputs:
 *		format_str - The '\0' terminated printf string to be examined.
 *		data_type  - DDL data type from ddldefs.h.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		BLTIN_SUCCESS
 *		BLTIN_INVALID_PROMPT
 *
 *  Author:
 *		Don Marquart
 *
 *********************************************************************/

long
check_printf_format_string(char *format_str, long data_type)
{

    long	index;
    char	*ptr;
    long	level, count;

    if (format_str == 0)
        return(BLTIN_SUCCESS); /* Null format strings are OK. */

    ptr = format_str;
    if (*ptr != '%') 		/* String must start with a '%' sign. */
        return(BLTIN_INVALID_PROMPT);

    level = 1;
    ptr++;

    /*
     *	Find the data type in the printf_info array.
     */
    index = 0;
    while ( (data_type != printf_info[index].ddl_type) &&
            (printf_info[index].ddl_type != BAD_DDL_TYPE) )
        index++;

    if (printf_info[index].ddl_type == BAD_DDL_TYPE) {
        return(BLTIN_SUCCESS); /* If type not found, return OK. */
    }

    while ( *ptr != '\0' ) {
        if (isdigit(*ptr)) {
            /*
             * Digits are handled as a special case from the print_info array.
             */
            if (level <= FIRST_DIGIT_LEVEL)
                level = FIRST_DIGIT_LEVEL;
            else if ((level == (SECOND_DIGIT_LEVEL-1)) || (level == SECOND_DIGIT_LEVEL) )
                level = SECOND_DIGIT_LEVEL;
            else
                return(BLTIN_INVALID_PROMPT);

        } else {

            for (count=0; count<printf_info[index].valid_chars; count++)
                if (printf_info[index].printf_data[count].character == *ptr)
                    break;

            if (count == printf_info[index].valid_chars)
                return(BLTIN_INVALID_PROMPT); /* Character not found. */

            if (printf_info[index].printf_data[count].level <= level)
                return(BLTIN_INVALID_PROMPT); /* Levels must increase. */

            level = printf_info[index].printf_data[count].level;
        }
        ptr++;
    }

    if (level != REQUIRED_LAST_LEVEL)
        return(BLTIN_INVALID_PROMPT);

    return(BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: find_lang_string
 *  ShortDesc: Finds the current language string in a text string
 *
 *  Description:
 *
 *      Locates the part of a text string associated with the
 *      country code stored in the global param "language".
 *      A pointer to the located string is returned. This
 *      pointer is a pointer into the original string, i.e.,
 *      no data is ever moved. In order to return a null terminated
 *      string, a null byte may be written into the middle of
 *      the original string. Therefore, the original string is
 *      probably not useful, except when referenced via the
 *      the pointer return by this function.
 *
 *		This function uses the function "strip" in the standard
 *		dictionary file.  However, strip requires the string to
 *		start with a valid country code (all characters preceeding
 *		the first country code are discarded).  Find_lang_string
 *		defaults the string to English by prepending the English
 *		country code (|001) to the string if there is none.
 *
 *  Inputs:
 *		text - The text to be examined.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		A pointer to the requested language string within the provided
 *		text.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

char *
find_lang_string (char *text)
{
    char		*tbuf = NULL, *tptr, *rptr;

    if(text==NULL)
        return NULL;

    if(text[0]!='|')
        return text;

    if(strlen(text) < 4)
        return text;

    if(isdigit(text[1]) && isdigit(text[2]) && isdigit(text[3]))
    {
        text[4] = '|';
        return strip (text);
    }

    if(isalpha(text[1]) && isalpha(text[2]))
    {
        text[3] = '|';
        return strip (text);
    }

//	if(		isalpha(text[1]) && isalpha(text[2]) && (text[3]= ' ')
//		&&  isalpha(text[4]) && isalpha(text[5]) && (text[6]= '|'))
//		return strip (text);

    /*
     *	If the provided text does not start with a valid country
     *	or language/country code, prepend one.
     */
    tbuf = (char *)malloc ((unsigned)(strlen(text)+5));
    if (tbuf == NULL)
        return (NULL);
    tbuf[0] = '|';
    tbuf[1] = 'e';
    tbuf[2] = 'n';
    tbuf[3] = '|';
    (void)strcpy (tbuf + 4, text);
    tptr = strip (tbuf);
    if (tptr)
    {
        rptr = text + (tptr - tbuf - 4);
        rptr[strlen(tptr)] = 0;
    }
    else
        rptr = tptr;
    free (tbuf);
    return (rptr);
}


/**********************************************************************
 *
 *  Name: bltin_format_commerr_string
 *  ShortDesc: Format a string describing the communications error
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_format_commerr_string formats a descriptive string
 *		describing the communications error that was received.
 *		If possible, the description of the error is used, if the
 *		description is not available, the error number is used.
 *		If the tag of the object that was being accessed is
 *		available, it is used in the description, if not, the
 *		object's ID and subindex is used.
 *
 *		Since bltin_format_commerr_string is used to format a
 *		description during an error condition, all work is done
 *		in buffers on the stack.  This leads to some maximum length
 *		conditions, but avoids the problem of failing to malloc memory.
 *
 *  Inputs:
 *		errcode - The communications error number
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer to put the formatted string.
 *
 *  Returns:
 *		None.
 *
 *  Bugs:
 *		Fixed length buffers of size 250 characters limit the length of
 *			strings, some of which may have multiple country codes.
 *		The specified buffer for the final string is assumed to be
 *			"long enough" to hold the final result.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static void
bltin_format_commerr_string (UINT32 errcode, char *out_buf, ENV_INFO2 *env_info)
{

    int				rs;
    char			error_buf[250], format_buf[250], name_buf[250];
    char			*format_ptr, *label_ptr;
    P_REF			pc_p_ref;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Save the ID and subindex of the access causing the error,
     *	since the following calls might destroy them.
     */

    pc_p_ref.id 		= app_info->env_returns.id_in_error;
    pc_p_ref.po 		= app_info->env_returns.po_in_error;
    pc_p_ref.subindex	= app_info->env_returns.subindex_in_error;
    pc_p_ref.type		= PC_ITEM_ID_REF|PC_PARAM_OFFSET_REF;

    /*
     *	Get the description of the communications error.
     */

    rs = get_comm_error_string (errcode, error_buf, (long) sizeof(error_buf),
            env_info);

    /*
     *	Attempt to get the param's tag.
     */

    /* This portion rewritten to use a label_ptr rather than a buffer for AR 3489 */
    rs = pc_get_param_label ((ENV_INFO *)env_info, &pc_p_ref, &label_ptr);
    if (rs != BLTIN_SUCCESS) {
        (void)strncpy (format_buf, DEFAULT_TAG_STRING, sizeof (format_buf));
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (name_buf, format_ptr, pc_p_ref.id, pc_p_ref.subindex);

        (void)strncpy (format_buf, COMM_ERROR_MSG, sizeof (format_buf));
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (out_buf, format_ptr, error_buf, name_buf);
        return;
    }

    /*
     *	Format the string.
     */

    (void)strncpy (format_buf, COMM_ERROR_MSG, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf, format_ptr, error_buf, label_ptr);
}


/**********************************************************************
 *
 *  Name: bltin_abort_commerr
 *  ShortDesc: Abort a method because of a communications error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_abort_commerr formats a descriptive message about the
 *		communication error received, and displays that message
 *		while aborting the method.
 *
 *		This routine does not return to its caller.
 *
 *  Inputs:
 *		errcode - The communications error number
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static void
bltin_abort_commerr (UINT32 errcode, ENV_INFO2 *env_info)
{

    char			out_buf[200], format_buf[50];
    char			*format_ptr;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Attempt to get the description of the communications error.
     *	However, set the flag bit so that errors are not processed.
     */

    app_info->status_info |= NO_ERROR_PROCESSING;
    bltin_format_commerr_string (errcode, out_buf, env_info);
    app_info->status_info &= ~NO_ERROR_PROCESSING;

    /*
     *	Abort.
     */

    (void)strncpy (format_buf, ABORT_ACTION, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf + strlen(out_buf), format_ptr);
    method_abort (out_buf, env_info);
}


/**********************************************************************
 *
 *  Name: bltin_retry_commerr
 *  ShortDesc: Retry a communication because of a communications error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_retry_commerr handles retrying communications that
 *		generated a communications error.  If the communications
 *		retry count is non-zero, bltin_retry_commerr just retries
 *		the communication.  If the retry count is zero, bltin_retry_commerr
 *		formats a descriptive message about the communication error
 *		received, and displays that message.  It then asks the user if
 *		s/he wishes to continue retrying, abort the method, or cause
 *		the builtin to fail while returning control to the method.
 *
 *  Inputs:
 *		errcode - The communications error number
 *		retry_ok - Indication if caller is in a position to do a retry
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		meth_infop - The METH_INFO block containing information about
 *				the current method.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		METH_METHOD_RETRY, BLTIN_FAIL_COMM_ERROR
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_retry_commerr (UINT32 errcode, int retry_ok,
        ENV_INFO2 *env_info, METH_INFO *meth_infop)
{

    char			out_buf[200], format_buf[50];
    char			*format_ptr;
    int				choice, rs;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	If there are retries left on the count, try again without
     *	prompting the user.
     */

    if (retry_ok && meth_infop->meth_comm_retry-- > 0) {
        return (METH_METHOD_RETRY);
    }

    /*
     *	Attempt to get the description of the response_code.
     *	However, set the flag bit so that errors are not processed.
     */

    app_info->status_info |= NO_ERROR_PROCESSING;
    bltin_format_commerr_string (errcode, out_buf, env_info);
    app_info->status_info &= ~NO_ERROR_PROCESSING;

    /*
     *	Format the retry message, and query the user.
     */

    (void)strncpy (format_buf, CHOOSE_PROMPT, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf + strlen(out_buf), format_ptr);
    rs = bltin_select_from_menu (out_buf, (ITEM_ID *)NULL, (SUBINDEX *)NULL,
        retry_ok ? RETRY_MENU : SHORT_RETRY_MENU, (long *) &choice, (BLOCK_HANDLE *)0, FALSE, env_info);
    if (rs != BLTIN_SUCCESS) {
        method_abort (INTERNAL_ERROR_ABORT_MSG, env_info);
    }
    switch (choice) {
        case 1:
            method_abort (ABORT_ACTION, env_info);
        case 2:
            return (BLTIN_FAIL_COMM_ERROR);
        default:
            ASSERT_DBG (choice == 3 && retry_ok);
            meth_infop->meth_comm_retry = COMM_RETRY_CHOICE;
            return (METH_METHOD_RETRY);
    }
}


/**********************************************************************
 *
 *  Name: bltin_format_rcode_string
 *  ShortDesc: Format a string describing the response code
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_format_rcode_string formats a descriptive string
 *		describing the response code that was received.
 *		If possible, the description of the response is used, if the
 *		description is not available, the code number is used.
 *		If the tag of the object that was being accessed is
 *		available, it is used in the description, if not, the
 *		object's ID and subindex is used.
 *
 *		Since bltin_format_rcode_string is used to format a
 *		description during an error condition, all work is done
 *		in buffers on the stack.  This leads to some maximum length
 *		conditions, but avoids the problem of failing to malloc memory.
 *
 *  Inputs:
 *		code - The response code number
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer to put the formatted string.
 *
 *  Returns:
 *		None.
 *
 *  Bugs:
 *		Fixed length buffers of size 250 characters limit the length of
 *			strings, some of which may have multiple country codes.
 *		The specified buffer for the final string is assumed to be
 *			"long enough" to hold the final result.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static void
bltin_format_rcode_string (UINT32 code, char *out_buf, ENV_INFO2 *env_info)
{

    int				rs;
    char			error_buf[250], format_buf[250], name_buf[250];
    char			*format_ptr, *label_ptr;
    P_REF			pc_p_ref;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Save the ID and subindex of the access causing the error,
     *	since the following calls might destroy them.
     */

    pc_p_ref.id 		= app_info->env_returns.id_in_error;
    pc_p_ref.po 		= app_info->env_returns.po_in_error;
    pc_p_ref.subindex	= app_info->env_returns.subindex_in_error;
    pc_p_ref.type		= PC_ITEM_ID_REF|PC_PARAM_OFFSET_REF;

    /*
     *	Attempt to get the description of the communications error.
     */

    (void)bltin_get_response_code_string (pc_p_ref.id, pc_p_ref.subindex,
                code, error_buf, (long) sizeof(error_buf), env_info);

    /*
     *	Attempt to get the param's tag.
     */

    /* AR 3489 - use a char * for label_ptr rather than a buffer */
    rs = pc_get_param_label ((ENV_INFO *)env_info, &pc_p_ref, &label_ptr);
    if (rs != BLTIN_SUCCESS) {
        (void)strncpy (format_buf, DEFAULT_TAG_STRING, sizeof (format_buf));
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (name_buf, format_ptr, pc_p_ref.id, pc_p_ref.subindex);
        (void)strncpy (format_buf, RESP_CODE_MSG, sizeof (format_buf));
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (out_buf, format_ptr, error_buf, name_buf);
        return;
    }

    /*
     *	Format the string.
     */

    (void)strncpy (format_buf, RESP_CODE_MSG, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf, format_ptr, error_buf, label_ptr);
}


/**********************************************************************
 *
 *  Name: bltin_abort_rcode
 *  ShortDesc: Abort a method because of a response code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_abort_rcode formats a descriptive message about the
 *		response code received, and displays that message
 *		while aborting the method.
 *
 *		This routine does not return to its caller.
 *
 *  Inputs:
 *		code - The response code received.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static void
bltin_abort_rcode (UINT32 code, ENV_INFO2 *env_info)
{
    char			out_buf[200], format_buf[50];
    char			*format_ptr;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Attempt to get the description of the response_code.
     *	However, set the flag bit so that errors are not processed.
     */

    app_info->status_info |= NO_ERROR_PROCESSING;
    bltin_format_rcode_string (code, out_buf, env_info);
    app_info->status_info &= ~NO_ERROR_PROCESSING;

    /*
     *	Format the abort message, and abort.
     */

    (void)strncpy (format_buf, ABORT_ACTION, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf + strlen(out_buf), format_ptr);
    method_abort (out_buf, env_info);
}


/**********************************************************************
 *
 *  Name: bltin_retry_rcode
 *  ShortDesc: Retry a communication because of a response code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_retry_rcode handles retrying communications that generated
 *		a response code.  If the communications retry count is non-zero,
 *		bltin_retry_rcode just retries the communication.  If the retry
 *		count is zero, bltin_retry_rcode formats a descriptive message
 *		about the response code received, and displays that message.
 *		It then asks the user if s/he wishes to continue retrying,
 *		abort the method, or cause the builtin to fail while returning
 *		control to the method.
 *
 *  Inputs:
 *		code - The response code received.
 *		retry_ok - Indication if caller is in a position to do a retry
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		meth_infop - The METH_INFO block containing information about
 *				the current method.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		METH_METHOD_RETRY, BLTIN_FAIL_RESPONSE_CODE
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_retry_rcode (UINT32 code, int retry_ok,
        ENV_INFO2 *env_info, METH_INFO *meth_infop)
{
    char			out_buf[200], format_buf[50];
    char			*format_ptr;
    int				choice, rs;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	If there are retries left on the count, try again without
     *	prompting the user.
     */

    if (retry_ok && meth_infop->meth_comm_retry-- > 0) {
        return (METH_METHOD_RETRY);
    }

    /*
     *	Attempt to get the description of the response_code.
     *	However, set the flag bit so that errors are not processed.
     */

    app_info->status_info |= NO_ERROR_PROCESSING;
    bltin_format_rcode_string (code, out_buf, env_info);
    app_info->status_info &= ~NO_ERROR_PROCESSING;

    /*
     *	Format the retry message, and query the user.
     */

    (void)strncpy (format_buf, CHOOSE_PROMPT, sizeof (format_buf));
    format_ptr = find_lang_string (format_buf);
    (void)sprintf (out_buf + strlen(out_buf), format_ptr);
    rs = bltin_select_from_menu (out_buf, (ITEM_ID *)NULL, (SUBINDEX *)NULL,
        retry_ok ? RETRY_MENU : SHORT_RETRY_MENU, (long *) &choice, (BLOCK_HANDLE *)0, FALSE, env_info);
    if (rs != BLTIN_SUCCESS) {
        method_abort (INTERNAL_ERROR_ABORT_MSG, env_info);
    }
    switch (choice) {
        case 1:
            method_abort (ABORT_ACTION, env_info);
        case 2:
            return (BLTIN_FAIL_RESPONSE_CODE);
        default:
            ASSERT_DBG (choice == 3 && retry_ok);
            meth_infop->meth_comm_retry = COMM_RETRY_CHOICE;
            return (METH_METHOD_RETRY);
    }
}


/**********************************************************************
 *
 *  Name: bltin_filter_comm_type_error
 *  ShortDesc: Handle the retry, abort, fail logic for communications
 *				errors and response codes.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_filter_comm_type_error searches the retry, abort, and
 *		fail lists of the current method for the specified
 *		communication error or response code.  If it finds the
 *		error in one of the lists, it does the appropriate action.
 *		if it fails to find the error, it does the default action.
 *
 *  Inputs:
 *		error_code - The indication of whether this is a communications
 *				error or response code.
 *		retry_ok - Indication if caller is in a position to do a retry
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		meth_infop - The METH_INFO block containing information about
 *				the current method.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		BLTIN_FAIL_COMM_ERROR, BLTIN_FAIL_RESPONSE_CODE,
 *		and error returns from other builtin, DDS and Parameter Cache functions.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_filter_comm_type_error (int error_code, int retry_ok,
        ENV_INFO2 *env_info, METH_INFO *meth_infop)
{

    UINT32			*valp, *val_endp, value;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Determine if this action should cause an abort, cause a retry
     *	or cause a failure.
     *	If there is a hidden response code error, then the action must
     *	be retry.
     */

    if (app_info->status_info & HIDDEN_RCODE_ERROR) {
        ASSERT_DBG (error_code == CM_RESPONSE_CODE);
        value = app_info->env_returns.response_code;
        app_info->status_info &= ~HIDDEN_RCODE_ERROR;
        return (bltin_retry_rcode(value, retry_ok, env_info, meth_infop));
    }

    /*
     *	If the error is a comm error, search the comm error lists
     *	to see if an action is specified for this error.
     */

    if (error_code == CM_COMM_ERR) {
        value = app_info->env_returns.comm_err;
        valp = meth_infop->meth_abort_comm_errs.errlist_list;
        val_endp = valp + meth_infop->meth_abort_comm_errs.errlist_count;
        for ( ; valp < val_endp; valp++) {
            if (*valp == value) {
                bltin_abort_commerr(value, env_info);
            }
        }
        valp = meth_infop->meth_retry_comm_errs.errlist_list;
        val_endp = valp + meth_infop->meth_retry_comm_errs.errlist_count;
        for ( ; valp < val_endp; valp++) {
            if (*valp == value) {
                return (bltin_retry_commerr(value, retry_ok, env_info, meth_infop));
            }
        }
        valp = meth_infop->meth_fail_comm_errs.errlist_list;
        val_endp = valp + meth_infop->meth_fail_comm_errs.errlist_count;
        for ( ; valp < val_endp; valp++) {
            if (*valp == value) {
                return (BLTIN_FAIL_COMM_ERROR);
            }
        }
        switch (meth_infop->meth_default_comm_err) {
            case DEFAULT_IS_ABORT:
                bltin_abort_commerr(value, env_info);
            case DEFAULT_IS_RETRY:
                return (bltin_retry_commerr(value, retry_ok, env_info, meth_infop));
            case DEFAULT_IS_FAIL:
                return (BLTIN_FAIL_COMM_ERROR);
            default:
                CRASH_DBG();
                /*NOTREACHED*/
                method_abort (INTERNAL_ERROR_ABORT_MSG, env_info);
        }
    }

    /*
     *	The error is a response code error. search the response code lists
     *	to see if an action is specified for this error.
     */

    value = app_info->env_returns.response_code;
    valp = meth_infop->meth_abort_response_codes.errlist_list;
    val_endp = valp + meth_infop->meth_abort_response_codes.errlist_count;
    for ( ; valp < val_endp; valp++) {
        if (*valp == value) {
            bltin_abort_rcode(value, env_info);
        }
    }
    valp = meth_infop->meth_retry_response_codes.errlist_list;
    val_endp = valp + meth_infop->meth_retry_response_codes.errlist_count;
    for ( ; valp < val_endp; valp++) {
        if (*valp == value) {
            return (bltin_retry_rcode(value, retry_ok, env_info, meth_infop));
        }
    }
    valp = meth_infop->meth_fail_response_codes.errlist_list;
    val_endp = valp + meth_infop->meth_fail_response_codes.errlist_count;
    for ( ; valp < val_endp; valp++) {
        if (*valp == value) {
            return (BLTIN_FAIL_RESPONSE_CODE);
        }
    }
    switch (meth_infop->meth_default_response_code) {
        default:
            CRASH_DBG();
            /*NOTREACHED*/
            method_abort (INTERNAL_ERROR_ABORT_MSG, env_info);
        case DEFAULT_IS_ABORT:
            bltin_abort_rcode(value, env_info);
        case DEFAULT_IS_RETRY:
            return (bltin_retry_rcode(value, retry_ok, env_info, meth_infop));
        case DEFAULT_IS_FAIL:
            return (BLTIN_FAIL_RESPONSE_CODE);
    }

}

/*
 * If the number of PC errors changes, this definition must change cbb
 */

#define PC_ERROR_COUNT	PC_NO_DATA_TO_SEND - PC_ERROR_START + 1

/**********************************************************************
 *
 *  Name: bltin_error_filter
 *  ShortDesc: Filter all possible errors into one of the BLTIN errors
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Builtin writers will only see return codes as defined in builtins.h.
 *		Builtin_error_filter will filter all possible errors that
 *		the builtin functions might see into the specified return
 *		codes.
 *
 *  Inputs:
 *		error_code - The error code.
 *		retry_ok - Indication if caller is in a position to do a retry
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
bltin_error_filter (int error_code, int retry_ok, ENV_INFO2 *env_info)
{

    static 	int		err_tab[PC_ERROR_COUNT][2] = {
        {PC_METHOD_COLLISION,			METH_METHOD_COLLISION},
        {PC_NO_READ_OVER_CHANGED_PARAM,	BLTIN_CANNOT_READ_VARIABLE},
        {PC_INTERNAL_ERROR,				METH_INTERNAL_ERROR},
        {PC_WRONG_DATA_TYPE,			BLTIN_WRONG_DATA_TYPE},
        {PC_NO_MEMORY,					BLTIN_NO_MEMORY},
        {PC_VALUE_NOT_SET,				BLTIN_VALUE_NOT_SET},
        {PC_ENTRY_BUSY,					METH_METHOD_COLLISION},
        {PC_PARAM_NOT_FOUND,			BLTIN_VAR_NOT_FOUND},
        {PC_BUFFER_TOO_SMALL,			BLTIN_BUFFER_TOO_SMALL},
        {PC_BAD_STATUS_INFO,			METH_INTERNAL_ERROR},
        {PC_BAD_REQ_TYPE,				METH_INTERNAL_ERROR},
        {PC_BAD_BLOCK_HANDLE,			METH_INTERNAL_ERROR},
        {PC_NO_DEV_VALUE_WRITE,			METH_INTERNAL_ERROR},
        {PC_NO_ACTION_VALUE,			METH_INTERNAL_ERROR},
        {PC_PARAM_CANNOT_BE_WRITTEN,	METH_INTERNAL_ERROR},
        {PC_PARAM_CANNOT_BE_READ,		BLTIN_CANNOT_READ_VARIABLE},
        {PC_BAD_POINTER,				METH_INTERNAL_ERROR},
        {PC_BAD_METHOD_TAG,				METH_INTERNAL_ERROR},
        {PC_BAD_PARAM_REQUEST,			METH_INTERNAL_ERROR},
        {PC_NO_DATA_TO_SEND,			BLTIN_NO_DATA_TO_SEND}};
    int				i;
    METH_INFO		*meth_infop;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	If errors are not to be processed, just return the error code.
     *	Errors are not processed when attempting to format a string
     *	for a different error.
     */

    if (app_info->status_info & NO_ERROR_PROCESSING) {
        return (error_code);
    }

    /*
     *	Builtin error codes are just passed through.
     *
     *	METH_INTERNAL_ERROR (which is a non-recoverable "this is never
     *	supposed to happen" type error) will cause the method to abort.
     *
     *	METH_METHOD_COLLISION will cause a re-try for the number of times
     *	specified by the system implementor in the
     *	METHOD_COLLISION_RETRY_CHOICE define.
     *
     *	All other errors are translated into BLTIN_DDS_ERROR, and the
     *	actual error code is saved in the ENV_INFO2 structure, where is
     *	can be accessed by the get_dds_error builtin.
     *
     *	This could be coded as a switch statement, switching on error_code,
     *	but since the switch statement would be so sparse, the compiler
     *	would turn it into a series of if statements.  So, code it as if
     *	statements, noting that order is important, and should be from
     *	expected most common to least common.
     */

    if (VALID_METHOD_TAG (app_info->method_tag)) {
        meth_infop = &meth_info[app_info->method_tag - 1];
    } else {
        CRASH_DBG();
        /*NOTREACHED*/
        meth_infop = NULL;
        error_code = METH_INTERNAL_ERROR;
    }
    if (error_code == BLTIN_SUCCESS) {
        meth_infop->meth_comm_retry = COMM_RETRY_CHOICE;
        return (BLTIN_SUCCESS);
    }

    /*
     * Convert Parameter Cache errors to BLTIN errors.
     */

    for ( i = 0; i < PC_ERROR_COUNT; i++) {
        if (err_tab[i][0] == error_code) {
            error_code = err_tab[i][1];
            break;
        }
    }

    if (error_code >= BLTIN_NO_MEMORY && error_code < BLTN_ERROR_END) {
        return (error_code);
    }

    if (error_code == CM_COMM_ERR || error_code == CM_RESPONSE_CODE) {
        return (bltin_filter_comm_type_error (error_code, retry_ok,
                    env_info, meth_infop));
    }

    if (error_code == METH_METHOD_COLLISION) {
        if (retry_ok && meth_infop->meth_collision_retry-- > 0) {
            (void)sleep ((unsigned int)METHOD_COLLISION_SUSPEND_TIME);
            return (METH_METHOD_RETRY);
        }
        method_abort (METHOD_COLLISION_ABORT_MSG, env_info);
    }

    if (error_code == METH_INTERNAL_ERROR) {
        method_abort (INTERNAL_ERROR_ABORT_MSG, env_info);
    }

    if (error_code == CM_IF_USR_ABORT) {
        method_abort (USER_ABORT_MSG, env_info);
    }

    if (error_code == DDL_CHECK_RETURN_LIST) {
        error_code = app_info->env_returns.dds_errors.list[0].rc;
    }

    app_info->env_returns.dds_return_code = error_code;
    return (BLTIN_DDS_ERROR);
}


/**********************************************************************
 *
 *  Name: bltin_format_array_elem
 *  ShortDesc: Format the description string from an ITEM_ARRAY for the
 *			specified value.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_format_array_elem gets the description for the ITEM_ARRAY
 *		element that corresponds to the given value.  It then strips
 *		out the current country code string, and returns it in the
 *		supplied buffer.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		array_id - The ID of the ITEM_ARRAY to access.
 *		value - The value to look for in the ITEM_ARRAY elements.
 *		format - The format specification for "printing" the description
 *				into the buffer.
 *
 *  Outputs:
 *		buf_ptr - The buffer to hold the formatted string.
 *
 *  Returns:
 *		BLTIN_SUCCESS, METH_INTERNAL_ERROR,
 *		and error returns from other builtin, DDS and Parameter Cache functions.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_format_array_elem (ENV_INFO2 *env_info, ITEM_ID array_id,
        unsigned long value, char *buf_ptr, char *format)
{

    FLAT_ITEM_ARRAY		flat_item_array;
    ITEM_ARRAY_ELEMENT	*item_array_elemp, *item_array_elem_endp;
    DDI_ITEM_SPECIFIER	item_spec;
    char				*print_string;
    int					rs, rs2;
    APP_INFO			*app_info;
    char				temp_buf[500];

    DDI_GENERIC_ITEM	generic_item;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Clear the space for ddi to return the info.
     */

    (void)memset ((char *)&generic_item, 0, sizeof (DDI_GENERIC_ITEM));

    /*
     *	Call to get the item array.
     */

    item_spec.type = DDI_ITEM_ID;
    item_spec.subindex = 0;
    item_spec.item.id = array_id;

    app_info->env_returns.dds_errors.count = 0;
    rs = ddi_get_item2(&item_spec, env_info,
              (unsigned long)ITEM_ARRAY_ELEMENTS, &generic_item);

    flat_item_array = *(FLAT_ITEM_ARRAY *)generic_item.item;

    if (rs != SUCCESS) {
        if (rs == DDL_CHECK_RETURN_LIST) {
            rs2 = generic_item.errors.list[0].rc;
            if ((rs2 == SUCCESS) || (generic_item.errors.count == 0)) {
                CRASH_DBG();
                /*NOTREACHED*/
                rs = METH_INTERNAL_ERROR;
            }
        }
        goto lets_go;
    }

    if ((flat_item_array.masks.attr_avail & ITEM_ARRAY_ELEMENTS) == 0) {
        CRASH_DBG();
        /*NOTREACHED*/
        rs = METH_INTERNAL_ERROR;
        goto lets_go;
    }

    /*
     *	Search through the item array values, looking for a match.
     */

    item_array_elemp = flat_item_array.elements.list;
    item_array_elem_endp = item_array_elemp + flat_item_array.elements.count;
    for ( ; item_array_elemp < item_array_elem_endp; item_array_elemp++) {
        if (item_array_elemp->index == value) {
            break;
        }
    }

    /*
     *	Return the description.
     */

    if (item_array_elemp == item_array_elem_endp) {
        (void)strncpy (temp_buf, INVALID_ARRAY_REFERENCE, sizeof (temp_buf));
    } else {
        ASSERT_DBG ((item_array_elemp->evaled & IA_DESC_EVALED) != 0);
        if (item_array_elemp->desc.str == NULL) {
            (void)sprintf (temp_buf, "%08lX", value);
        } else {
            (void)strncpy (temp_buf, item_array_elemp->desc.str, sizeof (temp_buf));
        }
    }
    print_string = find_lang_string (temp_buf);
    (void)sprintf (buf_ptr, format, print_string);

    /*
     *	Clean up the flat structure.
     */

lets_go:
    rs2 = ddi_clean_item (&generic_item);
    if (rs != SUCCESS) {
        return (rs);
    }
    return (rs2);
}


/**********************************************************************
 *
 *  Name: get_local_value
 *  ShortDesc: This function is called to  get the current value
 *				of a local param for the builtins and translating
 *				the SYM structure to a EVAL_VAR_VALUE structure.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *		This function will call the interpreter routine "lookup" to get
 *		the SYM structure for the passed in param name and then
 *		convert this structure to the EVAL_VAR_VALUE structure identified
 *		as param_value.
 *
 *		Note that the local interpreter value for array types is an unformatted
 *		string of characters and can not be assummed by the application to be
 *      of any particular multi-byte type, such as PASSWORD or BIT_ENUMERATED.
 *
 *  Inputs:
 *		localparam - The name of the local param to get info for.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The value param_value is filled in.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
get_local_value (char *localparam, EVAL_VAR_VALUE *param_value, ENV_INFO2 *env_info)
{

    int				rs;
    ENVIRON			*env_ptr;
    SYM				*sym_ptr;
    char            *ptr_to_str_value;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    rs = BLTIN_SUCCESS;

    /*
     *	Get the current information about the localparam.
     */

    env_ptr = &meth_info[app_info->method_tag - 1].meth_environ;
    sym_ptr = lookup(localparam, env_ptr);
    if (sym_ptr == (SYM *) NULL) {
        return(BLTIN_VAR_NOT_FOUND);
    }

    /*
     * Convert from interpreter symbol types to param_value types.
     *
     *	The following table describes the mappings used between the types returned
     *	by the routine lookup and the types accepted by param_value.
     *	METH_VOID_TYPE			no mapping
     *	METH_CHAR_TYPE (c_p)	INTEGER (if not part of an array)
     *	METH_LONG_TYPE			INTEGER
     *	METH_U_CHAR_TYPE		UNSIGNED
     *	METH_U_LONG_TYPE		UNSIGNED
     *	METH_FLOAT_TYPE			FLOAT
     *	METH_DOUBLE_TYPE		DOUBLE
     *	METH_ARRAY_TYPE			ASCII
     *	METH_BUILT_IN_TYPE		no mapping
     *	METH_PTR_TYPE			no mapping
     *	METH_SHORT_TYPE			INTEGER
     *	METH_U_SHORT_TYPE		UNSIGNED
     */
    switch (sym_ptr->type->typ) {
        case METH_VOID_TYPE:
        case METH_BUILT_IN_TYPE:
        case METH_PTR_TYPE:
            return(BLTIN_WRONG_DATA_TYPE);

        case METH_CHAR_TYPE:
            param_value->type = DDS_INTEGER;
            param_value->val.i = *(sym_ptr->v.c_p);
            break;

        case METH_SHORT_TYPE:
            param_value->type = DDS_INTEGER;
            param_value->val.i = *(sym_ptr->v.s_p);
            break;

        case METH_LONG_TYPE:
            param_value->type = DDS_INTEGER;
            param_value->val.i = *(sym_ptr->v.l_p);
            break;

        case METH_U_CHAR_TYPE:
            param_value->type = DDS_UNSIGNED;
            param_value->val.u = *(sym_ptr->v.uc_p);
            break;

        case METH_U_SHORT_TYPE:
            param_value->type = DDS_UNSIGNED;
            param_value->val.u = *(sym_ptr->v.us_p);
            break;

        case METH_U_LONG_TYPE:
            param_value->type = DDS_UNSIGNED;
            param_value->val.u = *(sym_ptr->v.ul_p);
            break;

        case METH_FLOAT_TYPE:
            param_value->type = DDS_FLOAT;
            param_value->val.f = *(sym_ptr->v.f_p);
            break;

        case METH_DOUBLE_TYPE:
            param_value->type = DDS_DOUBLE;
            param_value->val.d = *(sym_ptr->v.d_p);
            break;

    /*	Since only single values or string arrays of type CHAR or U_CHAR are
     *  considered valid display or edit types, other arrays types are restricted.
     */
        case METH_ARRAY_TYPE:
            if ((sym_ptr->type->madeof->typ != METH_CHAR_TYPE) &&
                (sym_ptr->type->madeof->typ != METH_U_CHAR_TYPE)) {
                return(BLTIN_WRONG_DATA_TYPE);
            }

            if (sym_ptr->type->madeof->typ == METH_CHAR_TYPE) {
                ptr_to_str_value = sym_ptr->v.c_p;
            } else {
                ptr_to_str_value = (char *)sym_ptr->v.uc_p;
            }

            /* Make sure any extra unused fields are zeroed */
            (void)memset(&param_value->val.s, 0, sizeof(param_value->val.s));

            param_value->type = DDS_ASCII;
            param_value->val.s.len = (unsigned short)sym_ptr->type->size;

            /*
             * Create a place for the string.
             */

            param_value->val.s.str = (char *)malloc(param_value->val.s.len + 1);
            if (param_value->val.s.str == (char *) 0) {
               return (BLTIN_NO_MEMORY);
            }

            (void)memcpy(param_value->val.s.str, ptr_to_str_value,
                (int) param_value->val.s.len);
            param_value->val.s.flags = FREE_STRING;
            param_value->val.s.str[param_value->val.s.len] = 0;
            /* It is assumed that the pointer to character is pointing
                to the first character of a string  ?? md */
            break;

        default:
            CRASH_RET(METH_INTERNAL_ERROR);
    }
    return (rs);
}


/**********************************************************************
 *
 *  Name: put_local_value
 *  ShortDesc: This function is called to take the value of an EVAL_VAR_VALUE
 *				structure and put it in the SYM structure associated with the
 *				passed in param name.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *		This function will call the interpreter routine "lookup" to get the SYM
 *		structure for the passed in param name and then convert the structure
 *		of type EVAL_VAR_VALUE structure moving the values into the SYM structure.
 *
 *		Note that the local interpreter value for array types is an unformatted
 *		string of characters and can not be assumed by the interpreter to be
 *      of any particular multi-byte type, such as PASSWORD or BIT_ENUMERATED.
 *		ALL MULTI-BYTE VAR_VALUE TYPES ARE FORCED INTO THE EVAL_VAR_VALUE
 *		STRUCTURE IF THEY FIT FOR THE LOCAL VARIABLE SPECIFIED.
 *		NO TYPE CHECKING IS POSSIBLE.
 *
 *  Inputs:
 *		localparam - The name of the local param to get info for.
 *		param_value - 	The EVAL_VAR_VALUE structure containing new values.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
put_local_value (char *localparam, EVAL_VAR_VALUE *param_value, ENV_INFO2 *env_info)
{

    int				rs;
    ENVIRON			*env_ptr;
    SYM				*sym_ptr;
    char            *ptr_to_str_value;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    rs = BLTIN_SUCCESS;

    /*
     *	Get the current information about the localparam.
     */

    env_ptr = &meth_info[app_info->method_tag - 1].meth_environ;
    sym_ptr = lookup(localparam, env_ptr);
    if (sym_ptr == NULL) {
        return(BLTIN_VAR_NOT_FOUND);
    }

    /*
     * Convert from param_value types to interpreter symbol types.  The size
     * of the value is ASSUMED to fit. Only basic type checking and conversion
     * is done.
     */
    switch (param_value->type) {
        case DDS_UNUSED:
            return(BLTIN_WRONG_DATA_TYPE);

        case DDS_INTEGER:
            switch (sym_ptr->type->typ) {
                case METH_CHAR_TYPE:
                    *(sym_ptr->v.c_p) = (char)param_value->val.i;
                    break;
                case METH_SHORT_TYPE:
                    *(sym_ptr->v.s_p) = (short)param_value->val.i;
                    break;
                case METH_LONG_TYPE:
                    *(sym_ptr->v.l_p) = param_value->val.i;
                    break;
                default:
                    return(BLTIN_WRONG_DATA_TYPE);
            }
            break;

        case DDS_UNSIGNED:
            switch (sym_ptr->type->typ) {
                case METH_U_CHAR_TYPE:
                    *(sym_ptr->v.uc_p) = (unsigned char)param_value->val.u;
                    break;
                case METH_U_SHORT_TYPE:
                    *(sym_ptr->v.us_p) = (unsigned short)param_value->val.u;
                    break;
                case METH_U_LONG_TYPE:
                    *(sym_ptr->v.ul_p) = param_value->val.u;
                    break;
                default:
                    return(BLTIN_WRONG_DATA_TYPE);
            }
            break;

        case DDS_FLOAT:
            if (sym_ptr->type->typ != METH_FLOAT_TYPE) {
                return(BLTIN_WRONG_DATA_TYPE);
            }
            *(sym_ptr->v.f_p) = param_value->val.f;
            break;

        case DDS_DOUBLE:
            if (sym_ptr->type->typ != METH_DOUBLE_TYPE) {
                return(BLTIN_WRONG_DATA_TYPE);
            }
            *(sym_ptr->v.d_p) = param_value->val.d;
            break;

    /*   All multi-byte param_value types are shoved into SYM type
     *   METH_ARRAY_TYPE with an element type of CHAR or UCHAR or not at all.
     */
        case DDS_ENUMERATED:
        case DDS_BIT_ENUMERATED:
        case DDS_INDEX:
        case DDS_ASCII:
        case DDS_PASSWORD:
        case DDS_BITSTRING:
        case DDS_OCTETSTRING:
        case DDS_VISIBLESTRING:
        case DDS_TIME:
        case DDS_TIME_VALUE:
        case DDS_DATE_AND_TIME:
        case DDS_DURATION:
        case DDS_EUC:
            if (sym_ptr->type->typ != METH_ARRAY_TYPE) {
                return(BLTIN_WRONG_DATA_TYPE);
            }
            if ((sym_ptr->type->madeof->typ != METH_CHAR_TYPE) &&
                (sym_ptr->type->madeof->typ != METH_U_CHAR_TYPE)) {
                return(BLTIN_WRONG_DATA_TYPE);
            }
            if (param_value->val.s.len != sym_ptr->type->size) {  /* is this a good test ? */
                return(BLTIN_WRONG_DATA_TYPE);
            }

            if (sym_ptr->type->madeof->typ == METH_CHAR_TYPE) {
                ptr_to_str_value = sym_ptr->v.c_p;
            } else {
                ptr_to_str_value = (char *)sym_ptr->v.uc_p;
            }

            (void)memcpy(ptr_to_str_value, param_value->val.s.str,
                (int) param_value->val.s.len);
            /* It is assumed that the pointer to character is pointing t
                the first character of a string  ?? md */
            break;

        default:
            CRASH_RET(METH_INTERNAL_ERROR);
    }
    return (rs);
}


/**********************************************************************
 *
 *  Name: bltin_get_status_string
 *  ShortDesc: Return the description of the member of the specified
 *			ENUMERATED or BIT_ENUMERATED param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_get_status_string searches the members of a ENUMERATED or
 *		BIT_ENUMERATED param, and returns the description of
 *		the member matching the status in the given buffer.
 *
 *  Inputs:
 *		id - The ID of the param
 *		subindex - The member_id of the param.
 *		status - The value to search for.
 *		max_length - The length of the buffer
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer containing the returned description.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_get_status_string (ITEM_ID id, SUBINDEX subindex, UINT32 status,
    char *out_buf, long max_length, ENV_INFO2 *env_info)
{

    ENUM_VALUE			*enum_valuep, *enum_value_endp;
    FLAT_VAR			flat_var;
    DDI_ITEM_SPECIFIER	item_spec;
    char				*print_string;
    int					rs, rs2;
    char				temp_buf[500];
    APP_INFO			*app_info;

    DDI_GENERIC_ITEM	generic_item;

    app_info = (APP_INFO *)env_info->app_info;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: param2: member_id = 0x%x\n", subindex);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: param3: status = %u\n", status);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_status_string: param5: max_length = %ld\n", max_length);

    /*
     *	Clear the defaulted string indicator.
     */

    app_info->env_returns.return_flag &= ~DEFAULTED_STATUS_STRING;

    /*
     *	Clear the space for ddi to return the info.
     */

    (void)memset ((char *)&generic_item, 0, sizeof (DDI_GENERIC_ITEM));

    /*
     *	Call to get the enum values.
     */

    if (subindex) {
        item_spec.type = DDI_ITEM_ID_SI;
        item_spec.subindex = subindex;
    } else {
        item_spec.type = DDI_ITEM_ID;
        item_spec.subindex = 0;
    }
    item_spec.item.id = id;
    app_info->env_returns.dds_errors.count = 0;

    rs = ddi_get_item2(&item_spec, env_info,
              (unsigned long)VAR_ENUMS, &generic_item);

    if (rs != SUCCESS) {
        if (rs == DDL_CHECK_RETURN_LIST) {
            rs2 = generic_item.errors.list[0].rc;
            if ((rs2 == SUCCESS) || (generic_item.errors.count == 0)) {
                CRASH_DBG();
                /*NOTREACHED*/
                rs = METH_INTERNAL_ERROR;
            }
        }
        goto clean_up;
    }

    flat_var = *(FLAT_VAR *)generic_item.item;

    if ((flat_var.masks.attr_avail & VAR_ENUMS) == 0) {
        rs = BLTIN_WRONG_DATA_TYPE;
        goto clean_up;
    }
    /*
     *	Search through the enum values, looking for a match.
     */

    enum_valuep = flat_var.enums.list;
    enum_value_endp = enum_valuep + flat_var.enums.count;
    for ( ; enum_valuep < enum_value_endp; enum_valuep++) {
        if (enum_valuep->val == status) {
            break;
        }
    }

    /*
     *	Return the description.
     */

    if (enum_valuep == enum_value_endp) {
        (void)strncpy (temp_buf, UNKNOWN_ENUMERATOR, sizeof (temp_buf));
        app_info->env_returns.return_flag |= DEFAULTED_STATUS_STRING;
    } else {
        ASSERT_DBG ((enum_valuep->evaled & ENUM_DESC_EVALED) != 0);
        (void)strncpy (temp_buf, enum_valuep->desc.str, sizeof (temp_buf));
    }
    print_string = find_lang_string (temp_buf);
    (void)strncpy (out_buf, print_string, (size_t) max_length);

    /*
     *	Clean up the flat structure.
     */

clean_up:
    rs2 = ddi_clean_item (&generic_item);
    rs = bltin_error_filter (rs, 0, env_info);
    if (rs != SUCCESS) {
        return (rs);
    }
    rs2 = bltin_error_filter (rs2, 0, env_info);
    return (rs2);
}


/**********************************************************************
 *
 *  Name: bltin_format_string
 *  ShortDesc: Format a string into the specified buffer
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_format_string takes the supplied prompt string, strips out the
 *		current language version, and parses the resulting prompt.  This
 *		parsing means putting the taking the printf-like prompt format, and
 *		inserting param values or labels in the appropriate places.
 *
 *		The type of variable is being checked against the assumed type of
 *		variable based on the %[] string.  For instance:  if the user specifies
 *		a printf-like string of "%s" but is using a variable of type FLOAT, an
 *		error will be returned.
 *
 *		Types DATE_AND_TIME, TIME, TIME_VALUE, and DURATION all use a default
 * 		printf format.  The user's format is always ignored.
 *
 *  Inputs:
 *		max_length - The length of the output buffer.
 *		passed_prompt - The prompt to strip and parse.
 *		param_ids - The array of param IDs used to parse the prompt.
 *		subindices - The array of param subindices used to parse
 *					the prompt.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer to put the formatted string.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
bltin_format_string (char *out_buf, int max_length, char *passed_prompt,
         ITEM_ID *param_ids, SUBINDEX *subindices, ENV_INFO *env_info)
{
    ENV_INFO2 env_info2;
    env_to_env2(env_info, &env_info2);

    return bltin_format_string2(out_buf, max_length, passed_prompt,
        param_ids, subindices, NULL, FALSE, &env_info2);
}

int
bltin_format_string2 (char *out_buf, int max_length, char *passed_prompt,
         ITEM_ID *param_ids, SUBINDEX *subindices, BLOCK_HANDLE *bhs,
         int use_bh, ENV_INFO2 *env_info)
{

    char			*out_ptr, *out_endp;
    char 			*prompt, *curr_ptr, *prompt_buf;
    int				copy_length, par_index, rs, j;
    char			prompt_char;
    char			curr_format[20], curr_par[100], *curr_label;
    char			temp_buf[100], t2_buf[200], *temp_ptr, *str_ptr;
    char			*byte_ptr;
    char			*use_format;
    ITEM_ID			array_id;
    P_REF			pc_p_ref = {0};
    EVAL_VAR_VALUE	param_value;
    struct tm		*timep;
    time_t			days, ms, secs;
    unsigned int	msecs, ms32nds;
    char 			temp_char;
    BLOCK_HANDLE	save_bh = 0;
    // APP_INFO        *save_app_info;
    int				bh_saved=FALSE;


    int passed_prompt_size;
    /*
     *	Get the output buffer and the prompt string
     */

    out_endp = out_buf + max_length;

    /*
     *	Find the appropriate country code string in the prompt string.
     */

    passed_prompt_size=strlen(passed_prompt);
    prompt_buf = (char *)malloc ((unsigned)(passed_prompt_size+1));
    memset(prompt_buf,0,passed_prompt_size+1);

    if (prompt_buf == NULL) {
        *out_buf = 0;
        rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
        return (rs);
    }

    (void)strcpy(prompt_buf, passed_prompt);

    passed_prompt_size=strlen(passed_prompt);

    prompt = find_lang_string (prompt_buf);

    /*
     *	Fill the out_buffer, using the prompt string.
     */

    if (prompt == NULL) {
        free (prompt_buf);
        *out_buf = 0;
        rs = bltin_error_filter (BLTIN_NO_LANGUAGE_STRING, 0, env_info);
        return (rs);
    }

    out_ptr = out_buf;
    for (;;) {

        use_format = (char *)0;

        /*
         *	Copy the prompt string up to the first occurance of a "%".
         */

        curr_ptr = strchr (prompt, '%');

        /*
         *	If there are no more occurrences of "%", copy the rest of the
         *	string.
         */

        if (curr_ptr == NULL) {
            if (out_ptr + strlen (prompt) >= out_endp) {
                CRASH_DBG();
                /*NOTREACHED*/
                rs = METH_INTERNAL_ERROR;
                goto err_rtn;
            }
            (void)strcpy (out_ptr, prompt);
            break;
        }

        /*
         *	Copy the prompt up to the next "%".
         */

        ASSERT_DBG (*curr_ptr == '%');
        copy_length = curr_ptr - prompt;
        if (out_ptr + copy_length >= out_endp) {
            CRASH_DBG();
            /*NOTREACHED*/
            rs = METH_INTERNAL_ERROR;
            goto err_rtn;
        }
        (void)strncpy (out_ptr, prompt, copy_length);
        out_ptr += copy_length;
        prompt = curr_ptr + 1;
        prompt_char = *prompt;

        /*
         *	Handle the formatting.  The formatting consists of:
         *
         *	%[format]{param ID array index} for a device param
         *	%[format]{local param name} for a local param
         *
         *	The format is optional, and consists of standard scanf
         *	format string, with the addition of 'L', which specifies
         *	the label of the param.
         */

        if (prompt_char == '[') {

            /*
             *	Get the format string.
             */

            prompt++;
            curr_ptr = strchr (prompt, ']');
            if (curr_ptr == NULL || curr_ptr == prompt) {
                rs = BLTIN_INVALID_PROMPT;
                goto err_rtn;
            }
            copy_length = curr_ptr - prompt;
            *curr_format = '%';
            (void)strncpy (&curr_format[1], prompt, copy_length);
            curr_format[copy_length+1] = 0;
            prompt = curr_ptr + 1;
            prompt_char = *prompt;
        } else {
            *curr_format = 0;
        }

        if (prompt_char == '{') {

            /*
             *	Get the param string.  Should leading or following blanks be removed?
             */

            prompt++;
            curr_ptr = strchr (prompt, '}');
            if (curr_ptr == NULL) {
                rs = BLTIN_INVALID_PROMPT;
                goto err_rtn;
            }
            copy_length = curr_ptr - prompt;
            (void)strncpy (curr_par, prompt, copy_length);
            curr_par[copy_length] = 0;
            prompt = curr_ptr + 1;
            prompt_char = *prompt;
        } else if (isdigit (prompt_char)) {

            /*
             *	Special case of param string... %X is same as %{X}
             *	where X is a single digit number.
             */

            curr_par[0] = prompt_char;
            curr_par[1] = 0;
            prompt++;
        } else if (prompt_char == '%')
        {
            /* AR 3614 */
            prompt++;
            strcpy(temp_buf, "%");
            goto next_iteration;
        } else {
            rs = BLTIN_INVALID_PROMPT;
            goto err_rtn;
        }

        /*
         *	Get the ID to process if a device param.
         */

#ifdef lint	/* Stop lint complaints about pc_p_ref not being initialized for locals */
        pc_p_ref.id 		= (ITEM_ID)0;
        pc_p_ref.subindex	= (SUBINDEX)0;
        pc_p_ref.type		= 0;
        bh_saved = FALSE;
#endif

        if (isdigit(curr_par[0])) {

            /*
             *	The reference is to a device param.  Get the param ID and subindex.
             */

            par_index = atoi (curr_par);
            pc_p_ref.id 		= param_ids[par_index];
            pc_p_ref.subindex	= subindices[par_index];
            pc_p_ref.type		= PC_ITEM_ID_REF;

            // Save current env_info->block_handle, set saved flag so
            // the remaining portion of the for(;;) can restore it when needed
            if( use_bh == TRUE )
            {
                save_bh = env_info->block_handle;
                env_info->block_handle = bhs[par_index];
                // The next four lines removed as part of AR 3480.  We should keep the original APP_INFO.
                // save_app_info = env_info->app_info;
                // rs = get_abt_app_info(env_info->block_handle, &env_info->app_info);
                // if (rs)
                //    goto err_rtn;

                bh_saved = TRUE;
            }
        }

        /*
         *	Process the format string into out_buf.
         */

        if (!strcmp (curr_format, "%L")) {

            /*
             *	Print the label of the param.
             */

            if (isdigit(curr_par[0])) {  /* if device param */
                do {
                    /* AR 3489 - use a char * for curr_label rather than a buffer */
                    rs = pc_get_param_label ((ENV_INFO *)env_info, &pc_p_ref, &curr_label);
                    rs = bltin_error_filter (rs, 1, env_info);
                } while (rs == METH_METHOD_RETRY);
                if (rs != BLTIN_SUCCESS) {
                    goto err_rtn;
                }
                str_ptr = find_lang_string (curr_label);

                /* Perform a length check */
                copy_length = strlen(str_ptr);
                if (out_ptr + copy_length >= out_endp) {
                    CRASH_DBG();
                    /*NOTREACHED*/
                    rs = METH_INTERNAL_ERROR;
                    goto err_rtn;
                }

                /* Perform the copy without using any of the limited-size temporary buffers */
                strcpy(out_ptr, str_ptr);
                out_ptr += copy_length;

                goto next_iteration_skip_copy;
            }
        } else {

            /*
             *	Get the param value.
             */

            if (isdigit(curr_par[0])) {  /* if device param */
                do {
                    rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
                    rs = bltin_error_filter (rs, 1, env_info);
                } while (rs == METH_METHOD_RETRY);
                if (rs != BLTIN_SUCCESS) {
                    goto err_rtn;
                }

                /*
                 *	If no format specified, get format specified in the DDL.
                 */

                if (*curr_format == 0) {
                    curr_format[0] = '%';
                    curr_format[1] = '\0';
                    do {
                        rs = pc_get_param_disp_format ((ENV_INFO *)env_info, &pc_p_ref,
                                &curr_format[1], sizeof(curr_format)-1);
                        rs = bltin_error_filter (rs, 1, env_info);
                    } while (rs == METH_METHOD_RETRY);
                    if (rs != BLTIN_SUCCESS) {
                        goto err_rtn;
                    }
                    if (curr_format[1] != 0) {
                        use_format = curr_format;
                    }
                    else
                        use_format = (char*)NULL;
                } else {
                    use_format = curr_format;
                }
            } else {

                /*
                 * If no format is specified, set the use_format to NULL.
                 * Remember, local variables do not have a default format
                 * string like device parameters.
                 */
                if (*curr_format == 0)
                    use_format = (char*)NULL;
                else
                    use_format = curr_format;

                /*
                 * local param - get value from the interpreter symbol table
                 */
                rs = get_local_value((char *)curr_par, &param_value, env_info);
                if (rs != BLTIN_SUCCESS) {
                    goto err_rtn;
                }
            }

            /*
             *	Format the value into out_buf.  Each type has a default
             *	format to use if none is specified.
             *
             *  First check the validity of the format string.
             */

            rs = check_printf_format_string( use_format, (long)param_value.type );
            if (rs != BLTIN_SUCCESS)
                goto err_rtn;

            switch (param_value.type) {
                case DDS_INTEGER:
                    if (use_format == 0) {
                        use_format = "%d";
                    }
                    (void)sprintf (temp_buf, use_format, param_value.val.i);
                    break;

                case DDS_UNSIGNED:
                    if (use_format == 0) {
                        use_format = "%u";
                    }
                    (void)sprintf (temp_buf, use_format, param_value.val.u);
                    break;

                case DDS_INDEX:
                    if (use_format == 0) {
                        use_format = "%s";
                    }
                    do {
                        rs = pc_get_param_array_id ((ENV_INFO *)env_info, &pc_p_ref,
                                &array_id);
                        rs = bltin_error_filter (rs, 1, env_info);
                    } while (rs == METH_METHOD_RETRY);
                    if (rs != BLTIN_SUCCESS) {
                        goto err_rtn;
                    }
                    do {
                        rs = bltin_format_array_elem (env_info, array_id,
                                param_value.val.u, temp_buf, use_format);
                        rs = bltin_error_filter (rs, 1, env_info);
                    } while (rs == METH_METHOD_RETRY);
                    if (rs != BLTIN_SUCCESS) {
                        goto err_rtn;
                    }
                    break;

                case DDS_ENUMERATED:
                    if (use_format == 0) {
                        use_format = "%s";
                    }
                    do {
                        rs = bltin_get_status_string (pc_p_ref.id, pc_p_ref.subindex,
                                param_value.val.u, t2_buf,
                                (long) sizeof (t2_buf), env_info);
                        rs = bltin_error_filter (rs, 1, env_info);
                    } while (rs == METH_METHOD_RETRY);
                    if (rs != BLTIN_SUCCESS) {
                        goto err_rtn;
                    }
                    str_ptr = find_lang_string (t2_buf);
                    (void)sprintf (temp_buf, use_format, str_ptr);
                    break;

                case DDS_BIT_ENUMERATED:
                    if (use_format == 0) {
                        use_format = "%08X";
                    }
                    (void)sprintf (temp_buf, use_format, param_value.val.u);
                    break;

                case DDS_FLOAT:
                    if (use_format == 0) {
                        use_format = "%f";
                    }
                    (void)sprintf (temp_buf, use_format, param_value.val.f);
                    break;

                case DDS_DOUBLE:
                    if (use_format == 0) {
                        use_format = "%lf";
                    }
                    (void)sprintf (temp_buf, use_format, param_value.val.d);
                    break;

                case DDS_ASCII:
                case DDS_PASSWORD:
                    if (use_format == 0) {
                        use_format = "%s";
                    }
                    (void)strncpy (t2_buf, param_value.val.s.str, sizeof (t2_buf));
                    /* This will free the string if it was allocated */
                    bltin_free_string (&param_value.val.s);
                    str_ptr = find_lang_string (t2_buf);
                    (void)sprintf (temp_buf, use_format, str_ptr);
                    break;

                case DDS_BITSTRING:
                case DDS_OCTETSTRING:
                case DDS_VISIBLESTRING:
                    if (use_format == 0) {
                        use_format = "%02X ";
                    }
                    copy_length = (int)param_value.val.s.len;
                    temp_ptr = temp_buf;
                    byte_ptr = param_value.val.s.str;
                    do {
                        temp_char = (unsigned char)*byte_ptr;
                        (void)sprintf (temp_ptr, use_format, (unsigned char)temp_char);
                        temp_ptr += strlen (temp_ptr);
                        byte_ptr++;
                    } while (--copy_length > 0);
                    /* This will free the string if it was allocated */
                    bltin_free_string (&param_value.val.s);
                    break;

                case DDS_EUC:
                    if (use_format == 0) {
                        use_format = "%s";
                    }
                    eucncpy (t2_buf, param_value.val.s.str, sizeof (t2_buf));
                    /* This will free the string if it was allocated */
                    bltin_free_string (&param_value.val.s);
                    str_ptr = find_euc_lang_string (t2_buf);
                    euc_sprintf (temp_buf, use_format, str_ptr);
                    break;

                case DDS_DATE_AND_TIME:
                    /*
                     * Default the format string.  Documentation should tell the user
                     * that a default is always used.
                     */
                    use_format = "%02d/%02d/%02d %02d:%02d:%02d:%03d";
                    ms = (time_t)(uchar)param_value.val.ca[0] * 256 +
                            (time_t)(uchar)param_value.val.ca[1];
                    (void)sprintf (temp_buf, use_format, param_value.val.ca[6],
                            param_value.val.ca[5] & 0x3F, param_value.val.ca[4] & 0x1F,
                            param_value.val.ca[3] & 0x1F, param_value.val.ca[2] & 0x3F,
                            ms / 1000, ms % 1000);
                    break;

                case DDS_TIME:
                    /*
                     *	The date portion of this param is stored as
                     *	the number of days since 1/1/84.  This must be
                     *	converted into the number of seconds since 1/1/70
                     *	to use the gmtime() conversion.  Note that there
                     *	are 5113 days in the years 1970 - 1983.
                     *
                     *	Get the days as number of seconds since 1/1/70
                     */

                    if (param_value.size > 4) {
                        days = (time_t)(uchar)param_value.val.ca[4] * 256 +
                            (time_t)(uchar)param_value.val.ca[5];
                    } else {
                        days = 0;
                    }
                    days += 5113;
                    days *= 24 * 60 * 60;

                    /*
                     *	Now get the number of ms since midnight.  Add the
                     *	corresponding number of seconds into the day value,
                     *	and remember the partial second in the ms value.
                     */

                    ms = (time_t)(uchar)param_value.val.ca[0];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[1];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[2];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[3];
                    days += ms / 1000;
                    ms %= 1000;

                    /*
                     *	Convert into year, month, day, etc.
                     */

                    timep = gmtime (&days);

                    /*
                     * Default the format string.  Documentation should tell the
                     * user that a default is always used.
                     */
                    use_format = "%02d/%02d/%02d %02d:%02d:%02d:%03d";
                    (void)sprintf (temp_buf, use_format, timep->tm_year,
                        timep->tm_mon+1, timep->tm_mday, timep->tm_hour,
                        timep->tm_min, timep->tm_sec, ms);
                    break;

                case DDS_TIME_VALUE:
                    /*
                     *	The TIME_VALUE data type consists of 8 bytes of unsigned,
                     *	fixed point integer data.  The units of the data is 32nds of
                     *	a millisecond since 1 Jan 1972.
                     */
                    /*
                     *	So here's the problem:
                     *
                     *	The data is presented to DDS as eight unsigned integer bytes.
                     *	The data needs to be divided by 32000 (= 256 * 125) and have
                     *	63072000 (730 * 24 * 60 * 60) added to it to make it meet the
                     *	input format (seconds since 1 Jan 70) for the 'gmtime' time
                     *	function which we will use to do our day/date/time formatting.
                     *
                     *	We can divide the whole 64-bit value by 32000 by shifting off
                     *	eight bits, then dividing by 125.  If we do this as a 5-bit
                     *	shift (divide by 32) followed by a divide by 1000, we can save
                     *	an intermediate value of possible interest (milliseconds).
                     *
                     *	The shifts can be performed on the 64-bit long long as a unit,
                     *	which is quicker and easier than shifting eight chars or four
                     *	shorts, thus the need for an unsigned long long integer data
                     *  type in our union.  The value remaining after the shifts still
                     *  needs to be divided by 1000 to get it to seconds.  Since this
                     *	cannot be accomplished by shifts, we will have to do division.
                     *	Since we have only 32-bit arithmetic functions, we will have to
                     *	do long division.  Since we can't be certain that our long
                     *	division will be confined to 32 bit values, we'll use short
                     *	integers as our constituents, thus the need for a short integer
                     *	data type in our union.
                     */

                    /*
                     *	Save the 5 bits representing fractions of a millisecond, then
                     *	divide the whole 64 bits by 32 by shifting off the least
                     *	significant 5 bits of the long long.
                     */
                    ms32nds = (unsigned int)(param_value.val.ll & 037);
                    param_value.val.ll = param_value.val.ll >> 5;

                    /*
                     *	Using long division on short integers(!), divide the whole
                     *	64-bit long long by 1000 (decimal) to get the number of
                     *	seconds, then add in the seconds from 1 Jan 70 to 1 Jan 72
                     *	to normalize the value to be sent to 'gmtime'.  We'll do the
                     *	calculations right in the input buffer since it isn't used
                     *	for anything afterwards.  Only the lower 32 bits will be sent
                     *	to 'gmtime'; this will suffice until well after the middle of
                     *	the 21st century.
                     */
                    for (j = 0; j <= 2; j++) {
                        param_value.val.sa[j] = param_value.val.sa[j] / 1000;
                        param_value.val.sa[j+1] += (unsigned short)((param_value.val.sa[j] % 1000) * 65536);
                    }

                    msecs = param_value.val.sa[3] % 1000;	/* Save the milliseconds. */
                    param_value.val.sa[3] = param_value.val.sa[3] / 1000;
                    secs = param_value.val.sa[3] + (param_value.val.sa[2] * 65536);
                    secs = secs + (730 * 24 * 60 * 60);

                    /*
                     *	Convert into year, month, day, etc.
                     */
                    timep = gmtime (&secs);	  /* AR 3615 */

                    /*
                     * Default the format string.  Documentation should tell the
                     * user that a default is always used.
                     */
                    use_format =
                        "%02d/%02d/%02d %02d:%02d:%02d.%03d + %d/32 of a millisecond";
                    (void)sprintf (temp_buf, use_format, timep->tm_year,
                        timep->tm_mon+1, timep->tm_mday, timep->tm_hour,
                        timep->tm_min, timep->tm_sec, msecs, ms32nds);
                    break;

                case DDS_DURATION:
                    /*
                     *	Get the days.
                     */

                    if (param_value.size > 4) {
                        days = (time_t)(uchar)param_value.val.ca[4] * 256 +
                            (time_t)(uchar)param_value.val.ca[5];
                    } else {
                        days = 0;
                    }

                    /*
                     *	Now get the number of ms
                     */

                    ms = (time_t)(uchar)param_value.val.ca[0];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[1];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[2];
                    ms *= 256;
                    ms |= (time_t)(uchar)param_value.val.ca[3];

                    /*
                     * Default the format string.  Documentation should tell the user
                     * that a default is always used.
                     */
                    use_format = "%d %d";
                    (void)sprintf (temp_buf, use_format, days, ms);

                    break;

                default:
                    CRASH_DBG();
                    /*NOTREACHED*/
                    rs = METH_INTERNAL_ERROR;
                    goto err_rtn;
            }
        }

next_iteration:
        copy_length = strlen (temp_buf);
        if (out_ptr + copy_length >= out_endp) {
            CRASH_DBG();
            /*NOTREACHED*/
            rs = METH_INTERNAL_ERROR;
            goto err_rtn;
        }
        (void)strcpy (out_ptr, temp_buf);
        out_ptr += copy_length;

next_iteration_skip_copy:
        //End of for(;;) so check if we need to restore env_info->block_handle
        if(bh_saved ==TRUE)
        {
            bh_saved = FALSE;
            env_info->block_handle = save_bh;
        }
    }

    /*  out_buf is now filled in.  We check it for unit code suppression strings before returning.
     */
    mi_handle_suppression(env_info, out_buf);

    /*
     *	Return.
     */

    free (prompt_buf);
    return (BLTIN_SUCCESS);

err_rtn:

    *out_buf = 0;
    free (prompt_buf);
    rs = bltin_error_filter (rs, 0, env_info);

    if(bh_saved ==TRUE)
    {
        env_info->block_handle = save_bh;
    }

    return (rs);
}


/**********************************************************************
 *
 *  Name: bltin_get_acknowledgement
 *  ShortDesc: Display a message that requires operator acknowledgement
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_get_acknowledgement displays a message on the output device
 *		by calling the user supplied upcall (*display_ack_upcall)(), and
 *		continues after receiving operator acknowledgement.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		subindices - The subindices of the params used in
 *				the prompt message.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
bltin_get_acknowledgement (char *prompt, ITEM_ID *param_ids,
        SUBINDEX *subindices, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    char		out_buf[MAX_PROMPT_BUFFER_SIZE];
    int			rs;

    /*
     *	Translate the prompt string and param references into
     *	a displayable string.
     */

    do {
        rs = bltin_format_string2 (out_buf, sizeof (out_buf),
                    prompt, param_ids, subindices, bhs, use_bh, env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the message and wait for acknowledgement.
     */

    do {
        rs = (*display_ack_upcall) ((ENV_INFO *)env_info, out_buf);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/*
 *	Abort Builtins
 */


/**********************************************************************
 *
 *  Name: add_abort_method
 *  ShortDesc: Add an abort method to the abort method list.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Each active method has a list of abort methods.  This list
 *		contains the methods that are to be executed if the primary
 *		method aborts.  Add_abort_method adds another method to the
 *		abort list for the current method.
 *
 *  Inputs:
 *		method_id - The ID of the abort method to add.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		BLTIN_NO_MEMORY, BLTIN_SUCCESS
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
add_abort_method (ITEM_ID method_id, ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    ITEM_ID		*old_list;
    unsigned	list_size;
    int			rs;
    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "add_abort_method: param1: method_id = 0x%lx\n", method_id);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Expand the abort method list, if necessary.
     */

    if (meth_infop->meth_abort_list_count >= meth_infop->meth_abort_list_limit) {
        old_list = meth_infop->meth_abort_list;
        meth_infop->meth_abort_list_limit += METH_ABORT_GROWTH_CT;
        list_size = (unsigned)meth_infop->meth_abort_list_limit *
                sizeof (*meth_infop->meth_abort_list);
        meth_infop->meth_abort_list = (ITEM_ID *)
            realloc ((char *)meth_infop->meth_abort_list, list_size);
        if (meth_infop->meth_abort_list == NULL) {
            meth_infop->meth_abort_list_limit -= METH_ABORT_GROWTH_CT;
            meth_infop->meth_abort_list = old_list;
            rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
            return (rs);
        }
    }

    /*
     *	Add the abort method.
     */

    meth_infop->meth_abort_list[meth_infop->meth_abort_list_count++] = method_id;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: remove_abort_method
 *  ShortDesc: Remove an abort method from the abort method list.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Each active method has a list of abort methods.  This list
 *		contains the methods that are to be executed if the primary
 *		method aborts.  Remove_abort_method removes a method from
 *		the abort list for the current method.
 *
 *  Inputs:
 *		method_id - The ID of the abort method to add.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		BLTIN_BAD_METHOD_ID, BLTIN_SUCCESS
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
remove_abort_method (ITEM_ID method_id, ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    ITEM_ID		*abort_idp, *abort_id_endp;
    int			rs;
    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "remove_abort_method: param1: method_id = 0x%lx\n", method_id);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Find the abort method to remove.
     */

    abort_idp = meth_infop->meth_abort_list;
    abort_id_endp = abort_idp + meth_infop->meth_abort_list_count;
    while (abort_idp < abort_id_endp) {
        if (*abort_idp == method_id) {
            break;
        }
        abort_idp++;
    }

    /*
     *	If the method didn't exist, return an error.
     */

    if (abort_idp == abort_id_endp) {
        rs = bltin_error_filter (BLTIN_BAD_METHOD_ID, 0, env_info);
        return (rs);
    }

    /*
     *	Remove the abort method by sliding the remainder of the list
     *	down.  Since this is an overlapped move of a small amount of
     *	data, do the move an element at a time.
     */

    for (abort_idp++; abort_idp < abort_id_endp; abort_idp++) {
        abort_idp[-1] = *abort_idp;
    }
    meth_infop->meth_abort_list_count--;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: remove_all_abort_methods
 *  ShortDesc: Remove all abort methods from the abort method list.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Each active method has a list of abort methods.  This list
 *		contains the methods that are to be executed if the primary
 *		method aborts.  Remove_all_abort_methods removes all methods
 *		from the abort list for the current method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
remove_all_abort_methods (ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove all the abort methods.
     */
    meth_infop->meth_abort_list_count = 0;
}


/**********************************************************************
 *
 *  Name: method_abort
 *  ShortDesc: Abort the current method.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Method_abort displays the provided message, executes the
 *		abort methods, and does the longjmp to reset the method
 *		execution to the startup state.
 *
 *		Method_abort does not return to its caller.
 *
 *  Inputs:
 *		prompt - The abort message to display.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
method_abort (char *prompt, ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    ITEM_ID		*abort_idp, *abort_id_endp;
    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "method_abort: param1: prompt = %s\n", prompt);

    /*
     *	Get a pointer to the method's information block.
     */

    ASSERT_ALL (VALID_METHOD_TAG (app_info->method_tag)) ;
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	See if this is a nested abort.  If so, abort the abort method
     *	processing, and blow this pop stand! If not, mark this method
     *	as aborting.
     */

    if (meth_infop->meth_processing_abort) {
        goto blow_this_pop_stand;
    }
    meth_infop->meth_processing_abort++;

    /*
     *	Display the message.
     */

    if (prompt == NULL) {
        prompt = DEFAULT_ABORT_MSG;
    }
    (void)bltin_get_acknowledgement (prompt, (ITEM_ID *)0, (SUBINDEX *)0, (BLOCK_HANDLE *)0, FALSE,
                env_info);

    /*
     *	Process the abort methods.  If any errors are generated from the
     *	abort methods, abort the abort method processing, and just get out!
     */

    abort_idp = meth_infop->meth_abort_list;
    abort_id_endp = abort_idp + meth_infop->meth_abort_list_count;
    while (abort_idp < abort_id_endp) {
        if (meth_start (env_info, *abort_idp++, 0, DISCARD_VALUES) != BLTIN_SUCCESS) {
            goto blow_this_pop_stand;
        }
    }

    /*
     *	Return to the top of the method.  That is, take the abort exit.
     */

blow_this_pop_stand:
    longjmp (meth_infop->meth_setjmp, 1);
}


/*
 *	Parameter Cache Builtins
 */


/**********************************************************************
 *
 *  Name: discard_on_exit
 *  ShortDesc: Set the method to discard any local param changes.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		When a method modifies a param's value, it is really just
 *		changing a local copy of that value.  This change is only
 *		effective for the life of the method.  In order to make the
 *		change in the device, the method must send the value to the
 *		device.  All values that have been changed but not sent to
 *		the device will be handled at the termination of the method
 *		by either discarding them, sending them to the device, or
 *		saving the changes locally in a way that survives the end
 *		of the method (they still haven't affected the device).
 *		This choice is specified by the method.
 *
 *		Discard_on_exit sets the termination action to discard locally
 *		changed values.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
discard_on_exit (ENV_INFO2 *env_info)
{

    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    /*
     *	Change the close option in the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_info[app_info->method_tag - 1].meth_close_option = DISCARD_VALUES;
}


/**********************************************************************
 *
 *  Name: send_on_exit
 *  ShortDesc: Set the method to send any local param changes to the device.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		When a method modifies a param's value, it is really just
 *		changing a local copy of that value.  This change is only
 *		effective for the life of the method.  In order to make the
 *		change in the device, the method must send the value to the
 *		device.  All values that have been changed but not sent to
 *		the device will be handled at the termination of the method
 *		by either discarding them, sending them to the device, or
 *		saving the changes locally in a way that survives the end
 *		of the method (they still haven't affected the device).
 *		This choice is specified by the method.
 *
 *		Send_on_exit sets the termination action to send locally
 *		changed values to the device.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
send_on_exit (ENV_INFO2 *env_info)
{
    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Change the close option in the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_info[app_info->method_tag - 1].meth_close_option = SEND_VALUES;
}


/**********************************************************************
 *
 *  Name: save_on_exit
 *  ShortDesc: Set the method to save any local param changes.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		When a method modifies a param's value, it is really just
 *		changing a local copy of that value.  This change is only
 *		effective for the life of the method.  In order to make the
 *		change in the device, the method must send the value to the
 *		device.  All values that have been changed but not sent to
 *		the device will be handled at the termination of the method
 *		by either discarding them, sending them to the device, or
 *		saving the changes locally in a way that survives the end
 *		of the method (they still haven't affected the device).
 *		This choice is specified by the method.
 *
 *		Save_on_exit sets the termination action to save locally
 *		changed values without affecting the device.
 *
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
save_on_exit (ENV_INFO2 *env_info)
{

    APP_INFO	*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    /*
     *	Change the close option in the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_info[app_info->method_tag - 1].meth_close_option = SAVE_VALUES;
}


/**********************************************************************
 *
 *  Name: bltin_resolve_subindex
 *  ShortDesc: resolves the subindex for an item based on item type
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		An item has a id and may have a member_id which is either an
 *		array index, item array index or actual ID of a record member.
 *
 *		This function resolves what item type is being referred to
 *		and then gets the actual subindex for the record member or
 *		item_array member or returns the passed in member_id as the
 *		actual subindex in the case of an array.
 *
 *  Inputs:
 *		id - The ID of the param.
 *		member_id - The member_id of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		subindex - subindex for the item described by id and member_id.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
bltin_resolve_subindex(ITEM_ID *id, ITEM_ID member_id,
        SUBINDEX *subindex, ENV_INFO2 *env_info)
{

    int					rs;
    DDI_BLOCK_SPECIFIER	block_spec;
    DDI_ITEM_SPECIFIER	item_spec;
    ITEM_TYPE			item_type;
    ITEM_ID				res_id;

    block_spec.block.handle = env_info->block_handle;
    block_spec.type       	= DDI_BLOCK_HANDLE;
    item_spec.type		= DDI_ITEM_ID;
    item_spec.item.id	= *id;
    item_spec.subindex	= 0;		/* not used just set for completeness */

    rs = ddi_get_type(&block_spec, &item_spec, &item_type);
    if (rs != DDS_SUCCESS) {
        rs = bltin_error_filter (rs, 0, env_info);
        return(rs);
    }

        switch (item_type) {
            case ARRAY_ITYPE:
                if (member_id < 1) {
                  return (BLTIN_VAR_NOT_FOUND);
                }
                res_id = *id;
                *subindex = (SUBINDEX) member_id;
                break;

            case DOMAIN_ITYPE:
            case PROGRAM_ITYPE:
            case VARIABLE_ITYPE:
                res_id = *id;
                *subindex = (SUBINDEX) 0;
                break;

            case ITEM_ARRAY_ITYPE:
                res_id = resolve_array_ref (*id, member_id, env_info);
                *subindex = 0;
                break;

            case COLLECTION_ITYPE:
            case FILE_ITYPE:
            case CHART_ITYPE:
            case SOURCE_ITYPE:
            case GRAPH_ITYPE:
                res_id = resolve_record_ref (*id, member_id, env_info);
                *subindex = 0;
                break;

            case VAR_LIST_ITYPE:
                if (member_id == 0) {
                    return (BLTIN_VAR_NOT_FOUND);
                }
                res_id = resolve_record_ref (*id, member_id, env_info);
                *subindex = 0;
                break;

            case RECORD_ITYPE:
                if (member_id == 0) {
                    return (BLTIN_VAR_NOT_FOUND);
                }
                rs = ddi_get_subindex (&block_spec, *id, member_id, subindex);
                res_id = *id;
                if (rs != DDS_SUCCESS) {
                    rs = bltin_error_filter (rs, 0, env_info);
                    return(rs);
                }
                break;

            default:
                return (BLTIN_BAD_ITEM_TYPE);
        }

    *id = res_id;			/* Reset or restore id */

    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: get_expr_val_d
 *  ShortDesc: Convert an EXPR attribute to a double
 *
 *  Description:
 *
 *		get_expr_val_d retrieves the current value of the specified
 *		expression, if it is a numeric type, and converts it to a double.
 *
 *  Inputs:
 *		exp - The DDS expression
 *
 *  Outputs:
 *		err - An error code if this EXPR was an invalid type
 *
 *  Return value:
 *      double : the value, or undefined if err is nonzero.
 *
 *********************************************************************/
double get_expr_val_d(EXPR *exp, int *err)
{
    if (err)
        *err = 0;
    switch(exp->type)
    {
    case DDS_INTEGER :
        return (double)exp->val.i;

    case DDS_UNSIGNED :
    case DDS_ENUMERATED :
    case DDS_BIT_ENUMERATED:
    case DDS_INDEX:
        return (double)exp->val.u;

    case DDS_FLOAT 	:
        return (double)exp->val.f;
    case DDS_DOUBLE 	:
        return exp->val.d;
    }
    if (err)
        *err = BLTIN_BAD_ITEM_TYPE;
    return 0.0;
}

/**********************************************************************
 *
 *  Name: copy_ddl_string
 *  ShortDesc: Copy a STRING to a C character buffer
 *
 *  Description:
 *
 *		copy_ddl_string retrieves the specified string and copies
 *		as much of it as possible to the C character buffer.
 *      The character buffer will be null terminated if there is room.
 *
 *  Inputs:
 *		in_str : the DDL string to copy
 *  *inout_len : on input, the length of the C buffer to be used
 *
 *  Outputs:
 *	*inout_len : on completion, the number of bytes copied NOT including NULL
 *
 *  Return value:
 *      int : 0 for success, nonzero for failure
 *
 *********************************************************************/
int copy_ddl_string(char *out_str, STRING in_str, long *inout_len)
{
    long len = in_str.len;
    memset(out_str, 0, *inout_len);
    if (len > *inout_len)
        len = *inout_len;
    memcpy(out_str, in_str.str, len);
    *inout_len = len;
    return 0;
}


/**********************************************************************
 *
 *  Name: get_sel_string
 *  ShortDesc: Get the current value of a string selector.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_string_value retrieves the current value of the specified
 *		selector from DD Services, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		selector - An enumeration for what portion of the item we are accessing
 *		additional - Additional information required for some forms of access
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_str - The returned value of the param.
 *
 *  Return value:
 *      long : error code or 0 for success
 *
 *********************************************************************/

long
get_sel_string(char *out_str, ITEM_ID id, ITEM_ID selector, ITEM_ID additional, long *out_len, ENV_INFO2 *env_info)
{

    int				rs = 0;
    DDI_ITEM_SPECIFIER is;
    DDI_GENERIC_ITEM gi;
    ITEM_TYPE it;


    /* Set up the block and item specifiers */
    is.type = DDI_ITEM_ID;
    is.subindex = 0;
    is.item.id = id;
    memset(&gi, 0, sizeof(gi));
    rs = ddi_get_type2(env_info, &is, &it);
    if (rs)
        goto abort;

    switch(it)
    {
    case AXIS_ITYPE:
        {
            FLAT_AXIS *axis;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, AXIS_HELP, &gi);
                    if (rs)
                        goto abort;
                    axis = (FLAT_AXIS *)gi.item;
                    rs = copy_ddl_string(out_str, axis->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, AXIS_LABEL, &gi);
                    if (rs)
                        goto abort;
                    axis = (FLAT_AXIS *)gi.item;
                    rs = copy_ddl_string(out_str, axis->label, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_CONSTANT_UNIT:
                    rs = ddi_get_item2(&is, env_info, AXIS_CONSTANT_UNIT, &gi);
                    if (rs)
                        goto abort;
                    axis = (FLAT_AXIS *)gi.item;
                    rs = copy_ddl_string(out_str, axis->constant_unit, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case CHART_ITYPE:
        {
            FLAT_CHART *chart;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, CHART_HELP, &gi);
                    if (rs)
                        goto abort;
                    chart = (FLAT_CHART *)gi.item;
                    rs = copy_ddl_string(out_str, chart->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, CHART_LABEL, &gi);
                    if (rs)
                        goto abort;
                    chart = (FLAT_CHART *)gi.item;
                    rs = copy_ddl_string(out_str, chart->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case FILE_ITYPE:
        {
            FLAT_FILE *file;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, FILE_HELP, &gi);
                    if (rs)
                        goto abort;
                    file = (FLAT_FILE *)gi.item;
                    rs = copy_ddl_string(out_str, file->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, FILE_LABEL, &gi);
                    if (rs)
                        goto abort;
                    file = (FLAT_FILE *)gi.item;
                    rs = copy_ddl_string(out_str, file->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case GRAPH_ITYPE:
        {
            FLAT_GRAPH *graph;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, GRAPH_HELP, &gi);
                    if (rs)
                        goto abort;
                    graph = (FLAT_GRAPH *)gi.item;
                    rs = copy_ddl_string(out_str, graph->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, GRAPH_LABEL, &gi);
                    if (rs)
                        goto abort;
                    graph = (FLAT_GRAPH *)gi.item;
                    rs = copy_ddl_string(out_str, graph->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case LIST_ITYPE:
        {
            FLAT_LIST *list;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, LIST_HELP, &gi);
                    if (rs)
                        goto abort;
                    list = (FLAT_LIST *)gi.item;
                    rs = copy_ddl_string(out_str, list->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, LIST_LABEL, &gi);
                    if (rs)
                        goto abort;
                    list = (FLAT_LIST *)gi.item;
                    rs = copy_ddl_string(out_str, list->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case SOURCE_ITYPE:
        {
            FLAT_SOURCE *source;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, SOURCE_HELP, &gi);
                    if (rs)
                        goto abort;
                    source = (FLAT_SOURCE *)gi.item;
                    rs = copy_ddl_string(out_str, source->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, SOURCE_LABEL, &gi);
                    if (rs)
                        goto abort;
                    source = (FLAT_SOURCE *)gi.item;
                    rs = copy_ddl_string(out_str, source->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    case WAVEFORM_ITYPE:
        {
            FLAT_WAVEFORM *waveform;
            switch(selector)
            {
                case SEL_HELP:
                    rs = ddi_get_item2(&is, env_info, WAVEFORM_HELP, &gi);
                    if (rs)
                        goto abort;
                    waveform = (FLAT_WAVEFORM *)gi.item;
                    rs = copy_ddl_string(out_str, waveform->help, out_len);
                    if (rs)
                        goto abort;
                    break;
                case SEL_LABEL:
                    rs = ddi_get_item2(&is, env_info, WAVEFORM_LABEL, &gi);
                    if (rs)
                        goto abort;
                    waveform = (FLAT_WAVEFORM *)gi.item;
                    rs = copy_ddl_string(out_str, waveform->label, out_len);
                    if (rs)
                        goto abort;
                    break;
            }
            break;
        }
    }
    ddi_clean_item(&gi);
    return rs;

abort:
    {
        char outbuf[255];
        ddi_clean_item(&gi);

        sprintf(outbuf, SELECTOR_NOT_FOUND, id, selector, additional, rs);
        method_abort(outbuf, env_info);
        /* Unreachable */
#if (defined (_MSC_VER) && (_MSC_VER <= 1200)) || defined (_DEBUG) || defined (__linux__)
        /* msvc60 does not realize this is unreachable */
        return 0;
#endif
    }
}

/**********************************************************************
 *
 *  Name: get_sel_double
 *  ShortDesc: Get the current value of a floating point selector.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		get_sel_double retrieves the current value of the specified
 *		selector from DD Services, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		selector - An enumeration for what portion of the item we are accessing
 *		additional - Additional information required for some forms of access
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *********************************************************************/

double
get_sel_double(ITEM_ID id, ITEM_ID selector, ITEM_ID additional, ENV_INFO2 *env_info)
{

    int				rs;
    DDI_ITEM_SPECIFIER is;
    DDI_ATTR_REQUEST attr = 0;
    DDI_GENERIC_ITEM gi;
    double retval = 0.0;


    /* Set up the block and item specifiers */
    is.type = DDI_ITEM_ID;
    is.subindex = 0;
    is.item.id = id;
    memset(&gi, 0, sizeof(gi));

    switch(selector)
    {
    case SEL_VIEW_MIN:
    case SEL_VIEW_MAX:
    case SEL_SCALING:
        {
            FLAT_AXIS *axis;
            if (selector == SEL_VIEW_MIN)
                attr = AXIS_MIN_VALUE;
            else if (selector == SEL_VIEW_MAX)
                attr = AXIS_MAX_VALUE;
            else if (selector == SEL_SCALING)
                attr = AXIS_SCALING;
            else
                ASSERT_DBG(0);
            rs = ddi_get_item2(&is, env_info, attr, &gi);
            if (rs)
                goto abort;
            rs = 0;
            axis = (FLAT_AXIS *)gi.item;
            if (selector == SEL_VIEW_MIN)
            {
                if (app_get_selector_upcall)
                {
                    rs = (*app_get_selector_upcall)((ENV_INFO *)env_info, id,
                        selector, 0, &retval);
                }
                else /* Default to the defined minimum for the graph */
                    retval = get_expr_val_d(&axis->min_value, &rs);
            }
            else if (selector == SEL_VIEW_MAX)
            {
                if (app_get_selector_upcall)
                {
                    rs = (*app_get_selector_upcall)((ENV_INFO *)env_info, id,
                        selector, 0, &retval);
                }
                else /* Default to the defined maximum for the graph */
                    retval = get_expr_val_d(&axis->max_value, &rs);
            }
            else if (selector == SEL_SCALING)
                retval = (double)axis->scaling;
            else
                ASSERT_DBG(0);
            if (rs)
                goto abort;
            break;
        }
    case SEL_MIN_VALUE:
    case SEL_MAX_VALUE:
        {
            FLAT_VAR *var;
            if (selector == SEL_MIN_VALUE)
                attr = VAR_MIN_VAL;
            else if (selector == SEL_MAX_VALUE)
                attr = VAR_MAX_VAL;
            else
                ASSERT_DBG(0);
            rs = ddi_get_item2(&is, env_info, attr, &gi);
            if (rs)
                goto abort;
            rs = 0;
            var = (FLAT_VAR *)gi.item;
            /* If an additional field is specified, it is 1-relative, so adjust */
            if (additional)
                --additional;
            if (selector == SEL_MIN_VALUE)
            {
                if (additional >= var->misc->min_val.count)
                {
                    rs = BLTIN_VAR_NOT_FOUND;
                    goto abort;
                }
                retval = get_expr_val_d(&var->misc->min_val.list[additional], &rs);
            }
            else if (selector == SEL_MAX_VALUE)
            {
                if (additional >= var->misc->max_val.count)
                {
                    rs = BLTIN_VAR_NOT_FOUND;
                    goto abort;
                }
                retval = get_expr_val_d(&var->misc->max_val.list[additional], &rs);
            }
            else
                ASSERT_DBG(0);
            if (rs)
                goto abort;
            break;
        }
    case SEL_COUNT:
    case SEL_CAPACITY:
        {
            FLAT_LIST *list;
            if (selector == SEL_COUNT)
                attr = LIST_COUNT;
            else if (selector == SEL_CAPACITY)
                attr = LIST_CAPACITY;
            else
                ASSERT_DBG(0);
            rs = ddi_get_item2(&is, env_info, attr, &gi);
            if (rs)
                goto abort;
            list = (FLAT_LIST *)gi.item;
            /* If the COUNT is requested and unavailable, ask the app for it */
            if ((selector == SEL_COUNT) && !(list->masks.attr_avail & LIST_COUNT))
            {
                unsigned long count;
                /* In the case of a LIST COUNT, ask the application that's storing it */
                rs = (*list_count_element_upcall)((ENV_INFO *)env_info, id, &count);
                list->count = count;
                if (rs)
                    goto abort;
            }
            if (selector == SEL_COUNT)
                retval = (double)list->count;
            else if (selector == SEL_CAPACITY)
                retval = (double)list->capacity;
            else
                ASSERT_DBG(0);
            break;
        }
    }
    ddi_clean_item(&gi);
    return retval;

abort:
    {
        char outbuf[255];
        ddi_clean_item(&gi);

        sprintf(outbuf, SELECTOR_NOT_FOUND, id, selector, additional, rs);
        method_abort(outbuf, env_info);
        /* Unreachable */
#if (defined (_MSC_VER) && (_MSC_VER <= 1200)) || defined (_DEBUG) || defined (__linux__)
        /* msvc60 does not realize this is unreachable */
        return 0;
#endif
    }
}


/*
 * get_sel_double2 - currently this function exists specifically for retrieving
 * the COUNT of an inner list in an embedded list situation.
 */
double
get_sel_double2(ITEM_ID id, int index, ITEM_ID embedded_id, ITEM_ID selector, ITEM_ID additional, ENV_INFO2 *env_info)
{
    int				rs = 0;
    DDI_ITEM_SPECIFIER is;
    DDI_ATTR_REQUEST attr;
    DDI_GENERIC_ITEM gi;
    double retval;


    /* Set up the block and item specifiers */
    is.type = DDI_ITEM_ID;
    is.subindex = 0;
    is.item.id = embedded_id;
    memset(&gi, 0, sizeof(gi));

    switch(selector)
    {
    case SEL_COUNT:
        {
            FLAT_LIST *list;
            attr = LIST_COUNT;
            rs = ddi_get_item2(&is, env_info, attr, &gi);
            if (rs)
                goto abort;
            list = (FLAT_LIST *)gi.item;
            /* If the COUNT is requested and unavailable, ask the app for it */
            if ((selector == SEL_COUNT) && !(list->masks.attr_avail & LIST_COUNT))
            {
                unsigned long count;
                /* In the case of a LIST COUNT, ask the application that's storing it */
                rs = (*list_count_element2_upcall)((ENV_INFO *)env_info, id, index, embedded_id, &count);
                list->count = count;
                if (rs)
                    goto abort;
            }
            retval = (double)list->count;
            break;
        }
    default:
        goto abort;
    }
    ddi_clean_item(&gi);
    return retval;

abort:
    {
        char outbuf[255];
        ddi_clean_item(&gi);

        sprintf(outbuf, SELECTOR_NOT_FOUND, id, selector, additional, rs);
        method_abort(outbuf, env_info);
        /* Unreachable */
#if (defined (_MSC_VER) && (_MSC_VER <= 1200)) || defined (_DEBUG) || defined (__linux__)
        /* msvc60 does not realize this is unreachable */
        return 0;
#endif
    }
}

/**********************************************************************
 *
 *  Name: get_float_value
 *  ShortDesc: Get the current value of a floating point param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_float_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_float_value (ITEM_ID id, ITEM_ID member_id, float *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_float_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE,"get_float_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a float.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_FLOAT) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.f;
    return (BLTIN_SUCCESS);
}

/* Actually return the value */
float
ret_float_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{
    float f = 0.0;
    get_float_value(id, member_id, &f, env_info);

    return f;
}

/* Actually return the value */
double
ret_double_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{
    double f = 0.0;
    get_double_value(id, member_id, &f, env_info);

    return f;
}

/* Actually return the value */
unsigned long
ret_unsigned_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{
    unsigned long f = 0;
    get_unsigned_value(id, member_id, &f, env_info);

    return f;
}

/* Actually return the value */
long
ret_signed_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{
    long f = 0;
    get_signed_value(id, member_id, &f, env_info);

    return f;
}


/**********************************************************************
 *
 *  Name: put_float_value
 *  ShortDesc: Put the specified value of a floating point param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_float_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_float_value (ITEM_ID id, ITEM_ID member_id, double value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_float_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_float_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_float_value: param3: value = %f\n", value);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        param_value.type		= DDS_FLOAT;
        param_value.val.f		= (float)value;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_double_value
 *  ShortDesc: Get the current value of a double param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_double_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_double_value (ITEM_ID id, ITEM_ID member_id, double *valuep,
    ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_double_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_double_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a double.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_DOUBLE) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.d;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_double_value
 *  ShortDesc: Put the specified value of a double param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_double_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_double_value (ITEM_ID id, ITEM_ID member_id, double value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_double_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_double_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_double_value: param3: value = %f\n", value);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        param_value.type	= DDS_DOUBLE;
        param_value.val.d	= value;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_signed_value
 *  ShortDesc: Get the current value of an integer param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_signed_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_signed_value (ITEM_ID id, ITEM_ID member_id, long *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_signed_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_signed_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is an integer.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_INTEGER) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.i;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_signed_value
 *  ShortDesc: Put the specified value of an integer param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_signed_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_signed_value (ITEM_ID id, ITEM_ID member_id, long value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_signed_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_signed_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_signed_value: param3: value = %ld\n", value);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        param_value.type	= DDS_INTEGER;
        param_value.val.i	= value;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_unsigned_value
 *  ShortDesc: Get the current value of an unsigned integer param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_unsigned_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_unsigned_value (ITEM_ID id, ITEM_ID member_id, unsigned long *valuep,
    ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF		pc_p_ref;
    ITEM_TYPE		type;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_unsigned_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is an unsigned value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;

    /* Code addition by LNT; since there is a mismatch between
     * transmitter's data type and DDS data type handling for
     * Bit string, the below code is added
     */
    if(DDS_BITSTRING == param_value.type)
    {
        (void)memcpy((char *)valuep, (char *)param_value.val.s.str, param_value.val.s.len);
        type = DDS_UNSIGNED;
    }

    if (type != DDS_UNSIGNED && type != DDS_INDEX
        && type != DDS_ENUMERATED && type != DDS_BIT_ENUMERATED) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    if(DDS_BITSTRING != param_value.type)
    {
        *valuep = param_value.val.u;
    }
    else
    {
        return BLTIN_WRONG_DATA_TYPE;
    }
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_unsigned_value
 *  ShortDesc: Put the specified value of a unsigned integer param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_unsigned_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_unsigned_value (ITEM_ID id, ITEM_ID member_id, unsigned long value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned_value: param3: value = %lu\n", value);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        param_value.type	= DDS_UNSIGNED;
        param_value.val.u	= value;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_string_value
 *  ShortDesc: Get the current value of a string param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_string_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		lengthp - The maximum size of the string.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *		lengthp - The returned length of the string.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_string_value (ITEM_ID id, ITEM_ID member_id, char *valuep, long *lengthp,
    ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    ITEM_TYPE		type;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_string_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_string_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a string value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;
    if (type != DDS_ASCII && type != DDS_PASSWORD
        && type != DDS_BITSTRING && type != DDS_OCTETSTRING
        && type != DDS_VISIBLESTRING && type != DDS_EUC) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }

    /*
     *	Return as much of the string as possible.  If the supplied
     *	buffer is too small, also return an error.
     */

    if (*lengthp < param_value.val.s.len) {
        param_value.val.s.len = (unsigned short)*lengthp;
        rs = bltin_error_filter (BLTIN_BUFFER_TOO_SMALL, 0, env_info);
    } else {
        rs = BLTIN_SUCCESS;
    }
    (void)memcpy (valuep, param_value.val.s.str, (int)param_value.val.s.len);
    *lengthp = param_value.val.s.len;
    /* This will free the string if it was allocated */
    bltin_free_string (&param_value.val.s);
    return (rs);
}


/**********************************************************************
 *	NEEDS WORK HERE
 *  Name: put_string_value
 *  ShortDesc: Put the specified value of a string param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_string_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		valuep - The new value of the param.
 *		length - The length of the new value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_string_value (ITEM_ID id, ITEM_ID member_id, char *valuep,
        long length, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value: param3: valuep = %s\n", valuep);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_string_value: param4: length = %ld\n", length);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 			= res_id;
        pc_p_ref.subindex		= subindex;
        pc_p_ref.type			= PC_ITEM_ID_REF;
        param_value.type		= DDS_ASCII;
        param_value.val.s.str	= valuep;
        param_value.val.s.len	= (unsigned short)length;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_date_value
 *  ShortDesc: Get the current value of a date param.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_date_value retrieves the current value of the specified
 *		param from the Parameter Cache, and returns it.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		datap - The returned value of the param.
 *		lengthp - The returned length of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_date_value (ITEM_ID id, ITEM_ID member_id, char *datap, long *lengthp,
    ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    ITEM_TYPE		type;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_date_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_date_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a date value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;
    if (type != DDS_DATE_AND_TIME && type != DDS_TIME
        && type != DDS_TIME_VALUE && type != DDS_DURATION) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    (void)memcpy (datap, param_value.val.ca, (int)param_value.size);
    *lengthp = param_value.size;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_date_value
 *  ShortDesc: Put the specified value of a date param
 *			into the Parameter Cache.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_date_value stores the provided value of the specified
 *		param into the Parameter Cache.
 *
 *  Inputs:
 *		id - The ID of the param to access.
 *		member_id - The member_id of the param to access.
 *		datap - The new value of the param.
 *		length - The length of the new value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_date_value (ITEM_ID id, ITEM_ID member_id, char *datap, long length,
    ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value: param2: member_id = 0x%lx\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value: param3: datap = %s\n", datap);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_date_value: param4: length = %ld\n", length);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_id;
        pc_p_ref.subindex	= subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        param_value.type	= DDS_DATE_AND_TIME;
        param_value.size	= (unsigned short)length;
        (void)memcpy (param_value.val.ca, datap, (int)length);
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *	Name: assign
 *  ShortDesc: Set the value of a param equal to another param's
 *			value.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Assign sets the value of a param equal to the value of another
 *		param.  The two params must have the same type.  Assign
 *		is a shorthand way of doing a get_type_value builtin followed by
 *		a put_type_value builtin, where type is any value type.
 *
 *  Inputs:
 *		dest_id - The ID of the destination param.
 *		dest_member_id - The member_id of the destination param.
 *		src_id - The ID of the source param.
 *		src_member_id - The member_id of the source param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The destination param is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
assign (ITEM_ID dest_id, ITEM_ID dest_member_id, ITEM_ID src_id,
        ITEM_ID src_member_id, ENV_INFO2 *env_info)
{

    return bltin_assign( dest_id, dest_member_id, src_id, src_member_id, 0, 0, FALSE, env_info);
}

long
bltin_assign (ITEM_ID dest_id, ITEM_ID dest_member_id, ITEM_ID src_id,
        ITEM_ID src_member_id, BLOCK_HANDLE dstBH, BLOCK_HANDLE srcBH, int use_bh, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    ITEM_TYPE		type;
    SUBINDEX		dest_subindex, src_subindex;
    ITEM_ID			res_dest_id, res_src_id;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "assign: param1: dest_id = 0x%lx\n", dest_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "assign: param2: src_id = 0x%lx\n", src_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "assign: param3: dest_member_id = 0x%lx\n", dest_member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "assign: param4: src_member_id = 0x%lx\n", src_member_id);

    /*
     *  Resolve dest_member_id into dest_subindex
     */

    res_dest_id = dest_id;

    if(use_bh){ save_bh = env_info->block_handle; env_info->block_handle = dstBH; }

    rs = bltin_resolve_subindex(&res_dest_id, dest_member_id, &dest_subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    if(use_bh){ env_info->block_handle = save_bh; }

    /*
     *  Resolve src_member_id into src_subindex
     */

    res_src_id = src_id;

    if(use_bh){ env_info->block_handle = srcBH; }

    rs = bltin_resolve_subindex(&res_src_id, src_member_id, &src_subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        pc_p_ref.id 		= res_src_id;
        pc_p_ref.subindex	= src_subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_get_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Put the value into the Parameter Cache.
     */

    if(use_bh){ env_info->block_handle = dstBH; }

    do {
        pc_p_ref.id 		= res_dest_id;
        pc_p_ref.subindex	= dest_subindex;
        pc_p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_put_param_value ((ENV_INFO *)env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);

    /*
     *	Free memory if necessary
     */

    type = param_value.type;
    if ((type == DDS_ASCII || type == DDS_PASSWORD
        || type == DDS_BITSTRING || type == DDS_OCTETSTRING
        || type == DDS_VISIBLESTRING || type == DDS_EUC)

       && param_value.val.s.flags == FREE_STRING
       ) {
        /* This will free the string if it was allocated */
        bltin_free_string (&param_value.val.s);
    }

    if(use_bh){ env_info->block_handle = save_bh;}

    return (rs);
}


/*
 *	Communication Builtins
 */


/**********************************************************************
 *
 *  Name: read_value
 *  ShortDesc: Update a param value from the device.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Read_value causes the param's value in the Parameter Cache
 *		to be refreshed with the device value.
 *
 *  Inputs:
 *		id - The ID of the param to read.
 *		member_id - The member_id of the param to read.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value in the Parameter Cache is refreshed.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
read_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{

    int			rs;
    P_REF		p_ref;
    SUBINDEX	subindex = 0;
    ITEM_ID		res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "read_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "read_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;

    if ( member_id != 0 ) {
        rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
        if (rs != BLTIN_SUCCESS) {
            return (rs);
        }
    }

    /*
     *	Call the Parameter Cache, forcing an actual read.
     */

    do {
        p_ref.id 		= res_id;
        p_ref.subindex	= subindex;
        p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_read_param_value ((ENV_INFO *)env_info, &p_ref);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: send_value
 *  ShortDesc: Send a modified value to the device.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Send_value causes the param's value in the Parameter Cache
 *		to be sent to the device, thus updating the device's value.
 *
 *  Inputs:
 *		id - The ID of the param to read.
 *		member_id - The member_id of the param to read.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The device's value is changed.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
send_value (ITEM_ID id, ITEM_ID member_id, ENV_INFO2 *env_info)
{

    int			rs;
    P_REF		p_ref;
    SUBINDEX	subindex = 0;
    ITEM_ID		res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "send_value: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "send_value: param2: member_id = 0x%lx\n", member_id);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    if ( member_id != 0 ) {
        rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
        if (rs != BLTIN_SUCCESS) {
            return (rs);
        }
    }

    /*
     *	Call the Parameter Cache, forcing a send of the data to the device.
     */

    do {
        p_ref.id 		= res_id;
        p_ref.subindex	= subindex;
        p_ref.type		= PC_ITEM_ID_REF;
        rs = pc_write_param_value ((ENV_INFO *)env_info, &p_ref, (EVAL_VAR_VALUE *)NULL);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: send_all_values
 *  ShortDesc: Update the device with all of the values that the
 *			method has changed.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Send_all_values causes all of the values in the Parameter Cache
 *		that the method has modified to be sent to the device, thus
 *		changing the device values to match the method's values.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The device values are changed to reflect the method's values.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
send_all_values (ENV_INFO2 *env_info)
{

    int				rs;

    /*
     *	Call the Parameter Cache, forcing a send of the data
     *	that has been modified to the device.
     */

    do {
        rs = pc_write_all_param_values ((ENV_INFO *)env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_response_code
 *  ShortDesc: Get the response code returned on the last communication
 *			request, as well as the ID and subindex being accessed
 *			when the code was returned.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  Response codes
 *		are application messages, such as "value too high".  Response code
 *		return may be handled through the abort or retry mechanism, or they
 *		may cause the builtin to fail.  In the latter case, the response
 *		code information is made available to the method through the
 *		get_response_code call.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		resp_codep - The returned response_code.
 *		itemp - The returned ID of the item generating the response code.
 *		member_id - The returned member_id of the item generating
 *				the response code.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
get_response_code (UINT32 *resp_codep, ITEM_ID *itemp, ITEM_ID *member_idp,
    ENV_INFO2 *env_info)
{
    APP_INFO 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    *resp_codep = app_info->env_returns.response_code;
    *itemp = app_info->env_returns.id_in_error;
    *member_idp = app_info->env_returns.member_id_in_error;
}


/**********************************************************************
 *
 *  Name: get_comm_error
 *  ShortDesc: Get the communication error returned on the last communication
 *			request.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.  Communication
 *		error return may be handled through the abort or retry mechanism, or they
 *		may cause the builtin to fail.  In the latter case, the communication
 *		error information is made available to the method through the
 *		get_comm_error call.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The communication error number.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

UINT32
get_comm_error (ENV_INFO2 *env_info)
{
    APP_INFO 	*app_info;
    app_info = (APP_INFO *)env_info->app_info;
    return (app_info->env_returns.comm_err);
}


/**********************************************************************
 *
 *  Name: abort_on_all_comm_errors
 *  ShortDesc: Set the method to abort upon receiving any communication
 *				error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Abort_on_all_comm_errors sets the method to abort if it
 *		receives any communication error.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
abort_on_all_comm_errors (ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    APP_INFO 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default comm error action to abort, and clear
     *	out the comm error errlists.
     */

    meth_infop->meth_default_comm_err = DEFAULT_IS_ABORT;
    meth_infop->meth_abort_comm_errs.errlist_count = 0;
    meth_infop->meth_retry_comm_errs.errlist_count = 0;
    meth_infop->meth_fail_comm_errs.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: abort_on_all_response_codes
 *  ShortDesc: Set the method to abort upon receiving any response
 *				code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		codes is an application message such as "Value too large".
 *		Abort_on_all_response_codes sets the method to abort if it
 *		receives any response codes.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
abort_on_all_response_codes (ENV_INFO2 *env_info)
{

    METH_INFO	*meth_infop;
    APP_INFO 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default response code action to abort, and clear
     *	out the response code errlists.
     */

    meth_infop->meth_default_response_code = DEFAULT_IS_ABORT;
    meth_infop->meth_abort_response_codes.errlist_count = 0;
    meth_infop->meth_retry_response_codes.errlist_count = 0;
    meth_infop->meth_fail_response_codes.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: remove_errlist_entry
 *  ShortDesc: Remove an entry from the retry, abort, or fail lists.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Remove_errlist_entry searches the specified error list (either the
 *		retry, abort, or fail error list) for a given entry.  If that entry
 *		is found, it is removed by compacting the list and decrementing the
 *		count.  If the entry is not found, no action is taken.
 *
 *  Inputs:
 *		errlistp - The error list to search.
 *		entry - The entry to remove.
 *
 *  Outputs:
 *		The error list is updated.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static void
remove_errlist_entry (ERRLIST *errlistp, UINT32 entry)
{

    UINT32		*valp, *val_endp;

    /*
     *	Search the list for the entry.
     */

    valp = errlistp->errlist_list;
    val_endp = valp + errlistp->errlist_count;
    while (valp < val_endp) {
        if (*valp == entry) {
            break;
        }
        valp++;
    }

    /*
     *	If the entry was not found, return.
     */

    if (valp >= val_endp) {
        return;
    }

    /*
     *	Show the entry removed by decrementing the count.
     */

    errlistp->errlist_count--;

    /*
     *	If the matching entry is the last entry, return.
     */

    if (++valp == val_endp) {
        return;
    }

    /*
     *	Collapse the list to remove the entry.
     */

    do {
        valp[-1] = *valp;
    } while (++valp < val_endp);
}


/**********************************************************************
 *
 *  Name: abort_on_comm_error
 *  ShortDesc: Set the method to abort upon receiving the specified
 *				communication error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Abort_on_comm_error sets the method to abort if it
 *		receives the specified communication error.
 *
 *  Inputs:
 *		errcode - The communication error.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
abort_on_comm_error (UINT32 errcode, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_comm_error: param1: errcode = %u\n", errcode);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the abort
     *	list - this ensures no duplicates), and then add it to the
     *	end of the abort list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_retry_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_fail_comm_errs, errcode);
    errlistp = &meth_infop->meth_abort_comm_errs;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = errcode;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: abort_on_response_code
 *  ShortDesc: Set the method to abort upon receiving the specified response
 *				code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		codes is an application message such as "Value too large".
 *		Abort_on_response_code sets the method to abort if it
 *		receives the specified response code.
 *
 *  Inputs:
 *		code - The response code
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
abort_on_response_code (UINT32 code, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "abort_on_response_code: param1: code = %u\n", code);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the abort
     *	list - this ensures no duplicates), and then add it to the
     *	end of the abort list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_retry_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_fail_response_codes, code);
    errlistp = &meth_infop->meth_abort_response_codes;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = code;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: fail_on_all_comm_errors
 *  ShortDesc: Set the method to fail upon receiving any communication
 *				error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Fail_on_all_comm_errors sets the method to fail if it
 *		receives any communication error.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
fail_on_all_comm_errors (ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default comm error action to fail, and clear
     *	out the comm error errlists.
     */

    meth_infop->meth_default_comm_err = DEFAULT_IS_FAIL;
    meth_infop->meth_abort_comm_errs.errlist_count = 0;
    meth_infop->meth_retry_comm_errs.errlist_count = 0;
    meth_infop->meth_fail_comm_errs.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: fail_on_all_response_codes
 *  ShortDesc: Set the method to fail upon receiving any response
 *				code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		codes is an application message such as "Value too large".
 *		Fail_on_all_response_codes sets the method to fail if it
 *		receives any response codes.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
fail_on_all_response_codes (ENV_INFO2 *env_info)
{
    METH_INFO		*meth_infop;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default response code action to fail, and clear
     *	out the response code errlists.
     */

    meth_infop->meth_default_response_code = DEFAULT_IS_FAIL;
    meth_infop->meth_abort_response_codes.errlist_count = 0;
    meth_infop->meth_retry_response_codes.errlist_count = 0;
    meth_infop->meth_fail_response_codes.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: fail_on_comm_error
 *  ShortDesc: Set the method to fail upon receiving the specified
 *				communication error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Fail_on_comm_error sets the method to fail if it
 *		receives the specified communication error.
 *
 *  Inputs:
 *		Communication requests may return a communication error.
 *		Abort_on_comm_error sets the method to abort if it
 *		receives the specified communication error.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
fail_on_comm_error (UINT32 errcode, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO	 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_comm_error: param1: errcode = %u\n", errcode);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the fail
     *	list - this ensures no duplicates), and then add it to the
     *	end of the fail list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_retry_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_fail_comm_errs, errcode);
    errlistp = &meth_infop->meth_fail_comm_errs;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = errcode;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: fail_on_response_code
 *  ShortDesc: Set the method to fail upon receiving the specified response
 *				code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		codes is an application message such as "Value too large".
 *		Fail_on_response_code sets the method to fail if it
 *		receives the specified response code.
 *
 *  Inputs:
 *		code - The response code
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
fail_on_response_code (UINT32 code, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "fail_on_response_code: param1: code = %u\n", code);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the fail
     *	list - this ensures no duplicates), and then add it to the
     *	end of the fail list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_retry_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_fail_response_codes, code);
    errlistp = &meth_infop->meth_fail_response_codes;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = code;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: retry_on_all_comm_errors
 *  ShortDesc: Set the method to retry upon receiving any communication
 *				error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Retry_on_all_comm_errors sets the method to retry if it
 *		receives any communication error.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
retry_on_all_comm_errors (ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    APP_INFO	 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default comm error action to retry, and clear
     *	out the comm error errlists.
     */

    meth_infop->meth_default_comm_err = DEFAULT_IS_RETRY;
    meth_infop->meth_abort_comm_errs.errlist_count = 0;
    meth_infop->meth_retry_comm_errs.errlist_count = 0;
    meth_infop->meth_fail_comm_errs.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: retry_on_all_response_codes
 *  ShortDesc: Set the method to retry upon receiving any response code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		codes is an application message such as "Value too large".
 *		Retry_on_all_response_codes sets the method to retry if it
 *		receives any response codes.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		None.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

void
retry_on_all_response_codes (ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    APP_INFO	 	*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Set the default response code action to retry, and clear
     *	out the response code errlists.
     */

    meth_infop->meth_default_response_code = DEFAULT_IS_RETRY;
    meth_infop->meth_abort_response_codes.errlist_count = 0;
    meth_infop->meth_retry_response_codes.errlist_count = 0;
    meth_infop->meth_fail_response_codes.errlist_count = 0;
}


/**********************************************************************
 *
 *  Name: retry_on_comm_error
 *  ShortDesc: Set the method to retry upon receiving the specified
 *				communication error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a communication error.
 *		Retry_on_comm_error sets the method to retry if it
 *		receives the specified communication error.
 *
 *  Inputs:
 *		errcode - The communication error.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
retry_on_comm_error (UINT32 errcode, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_comm_error: param1: errcode = %u\n", errcode);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the retry
     *	list - this ensures no duplicates), and then add it to the
     *	end of the retry list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_retry_comm_errs, errcode);
    remove_errlist_entry (&meth_infop->meth_fail_comm_errs, errcode);
    errlistp = &meth_infop->meth_retry_comm_errs;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = errcode;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: retry_on_response_code
 *  ShortDesc: Set the method to retry upon receiving the specified response
 *				code.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Communication requests may return a response code.  A response
 *		code is an application message such as "Value too large".
 *		Retry_on_response_code sets the method to retry if it
 *		receives the specified response code.
 *
 *  Inputs:
 *		code - The response code
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
retry_on_response_code (UINT32 code, ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    ERRLIST			*errlistp;
    UINT32			*old_list;
    APP_INFO 		*app_info;

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "retry_on_response_code: param1: code = %u\n", code);

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	Remove the specified error from the errlists (including the retry
     *	list - this ensures no duplicates), and then add it to the
     *	end of the retry list.
     */

    remove_errlist_entry (&meth_infop->meth_abort_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_retry_response_codes, code);
    remove_errlist_entry (&meth_infop->meth_fail_response_codes, code);
    errlistp = &meth_infop->meth_retry_response_codes;
    if (errlistp->errlist_count >= errlistp->errlist_limit) {
        old_list = errlistp->errlist_list;
        errlistp->errlist_limit += ERRLIST_GROWTH_CT;
        errlistp->errlist_list = (UINT32 *)realloc
                ((char *)errlistp->errlist_list,
                (unsigned)errlistp->errlist_limit * sizeof (errlistp->errlist_list));
        if (errlistp->errlist_list == NULL) {
            errlistp->errlist_limit -= ERRLIST_GROWTH_CT;
            errlistp->errlist_list = old_list;
            return (bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info));
        }
    }
    errlistp->errlist_list[errlistp->errlist_count++] = code;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: get_status_string
 *  ShortDesc: Return the description of the member of the specified
 *			ENUMERATED or BIT_ENUMERATED param by calling bltin_
 *			get_status_string.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_status_string searches the members of a ENUMERATED or
 *		BIT_ENUMERATED param, and returns the description of
 *		the member matching the status in the given buffer.
 *
 *  Inputs:
 *		id - The ID of the param
 *		member_id - The member_id of the param.
 *		status - The value to search for.
 *		max_length - The length of the buffer
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer containing the returned description.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_status_string (ITEM_ID id, ITEM_ID member_id, UINT32 status,
        char *out_buf, long max_length, ENV_INFO2 *env_info)
{

    int				rs;
    SUBINDEX		subindex;
    ITEM_ID			res_id;

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    return(bltin_get_status_string (res_id, subindex, status, out_buf,
        max_length, env_info));
}


/**********************************************************************
 *
 *  Name: get_comm_error_string
 *  ShortDesc: Get the description of the communication error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_comm_error_string formats a descriptive string
 *		describing the communications error that is provided.
 *		If possible, the description of the error is used, if the
 *		description is not available, the error number is used.
 *
 *  Inputs:
 *		errcode - The communications error to find.
 *		max_length - The length of the buffer.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer for the returned response code description.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_comm_error_string (UINT32 errcode, char *out_buf, long max_length,
    ENV_INFO2 *env_info)
{

    int				rs;
    APP_INFO	 	*app_info;
    char			format_buf[sizeof(UNKNOWN_COMM_ERROR)+1], *format_ptr;
    char			error_buf[sizeof(UNKNOWN_COMM_ERROR)+20];

    app_info = (APP_INFO *)env_info->app_info;
    LOGC_INFO(CPM_FF_DDSERVICE, "get_comm_error_string: param1: errcode = %u\n", errcode);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_comm_error_string: param3: max_length = %ld\n", max_length);

    /*
     *	Attempt to get the description of the communications error.
     */

    do {
        app_info->env_returns.return_flag |= DEFAULTED_STATUS_STRING;
        rs = BLTIN_SUCCESS;
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != SUCCESS) {
        return (rs);
    }

    /*
     *	If the error wasn't found, format the default string.
     */

    if (app_info->env_returns.return_flag & DEFAULTED_STATUS_STRING) {
        (void)strncpy (format_buf, UNKNOWN_COMM_ERROR, sizeof (format_buf));
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (error_buf, format_ptr, errcode);
        (void)strncpy (out_buf, error_buf, (size_t) max_length);
    }
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: item_tbl_compare
 *  ShortDesc: Compare routine for searching the item table.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Item_tbl_compare is the comparison routine for the search of the
 *		item table.
 *
 *  Inputs:
 *		elem1, elem2 - Pointers to the two item table elements to compare.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		-1 - if *elem1 is "less" than *elem2
 *		 0 - if *elem1 is "equal" to *elem2
 *		 1 - if *elem1 is "greater" than *elem2
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
item_tbl_compare (ITEM_TBL_ELEM *elem1, ITEM_TBL_ELEM *elem2)
{
    if (elem1->item_id < elem2->item_id) {
        return (-1);
    }
    if (elem1->item_id > elem2->item_id) {
        return (1);
    }
    return (0);
}


typedef union {
    FLAT_VAR		flat_var;
    FLAT_RECORD		flat_record;
    FLAT_ARRAY		flat_array;
    FLAT_VAR_LIST	flat_var_list;
    FLAT_DOMAIN		flat_domain;
    FLAT_PROGRAM	flat_program;
    FLAT_RESP_CODE	flat_resp_codes;
} ALL_FLAT;

/**********************************************************************
 *
 *  Name: bltin_get_response_code_string
 *  ShortDesc: Get the description of the response code for the
 *			specified item.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Bltin_get_response_code_string searches the response codes of the
 *		specified item, and returns the description of the specified
 *		response code.  If the response code is not found, and if the item
 *		is a member of a record or an element of an array, then the record
 *		or array is also searched for the specified code.
 *
 *  Inputs:
 *		id - ID of the item to search.
 *		subindex - subindex of the item to search
 *		code - The response codes to search for.
 *		max_length - The length of the buffer.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer for the returned response code description.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
bltin_get_response_code_string (ITEM_ID id, SUBINDEX subindex, UINT32 code,
        char *out_buf, long max_length, ENV_INFO2 *env_info)
{

    ALL_FLAT			all_flat;
    ITEM_ID				arr_rec_resp_codes = 0;
    FLAT_DEVICE_DIR     *flat_device_dirp;
    char				format_buf[sizeof(UNKNOWN_RESP_CODE)+1], *format_ptr;
    DDI_ITEM_SPECIFIER	item_spec;
    ITEM_TBL_ELEM		*item_tbl_elemp;
    ITEM_TBL			*item_tblp;
    ITEM_TYPE			item_type;
    ITEM_ID				item_resp_codes;
    char				*print_string;
    RESPONSE_CODE		*response_codep, *response_code_endp;
    int					rs, rs2;
    char				temp_buf[500];
    APP_INFO 			*app_info;
    DDI_GENERIC_ITEM	generic_item,generic_item_rsp_code;

    app_info = (APP_INFO *)env_info->app_info;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param1: id = 0x%lx\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param2: member_id = 0x%x\n", subindex);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param3: code = %u\n", code);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_response_code_string: param5: max_length = %ld\n", max_length);

    /*
     *	Find the type of the specified item.
     */

    switch(env_info->type)
    {
    case ENV_BLOCK_HANDLE:
        if (!valid_block_handle (env_info->block_handle)) {
            CRASH_DBG();
            /*NOTREACHED*/
            rs = METH_INTERNAL_ERROR;
            goto misc_error;
        }
        rs = get_abt_dd_dev_tbls (env_info->block_handle, (void **) (&flat_device_dirp));
        if (rs != SUCCESS) {
            return (rs);
        }
        break;
    case ENV_DEVICE_HANDLE:
        if (!valid_device_handle(env_info->device_handle)) {
            CRASH_DBG();
            /*NOTREACHED*/
            rs = METH_INTERNAL_ERROR;
            goto misc_error;
        }
        rs = get_adt_dd_dev_tbls (env_info->device_handle, (void **) (&flat_device_dirp));
        if (rs != SUCCESS) {
            return (rs);
        }
        break;
    default:
        rs = DDI_INVALID_DD_HANDLE_TYPE;
        goto misc_error;
    }

    item_tblp = &flat_device_dirp->item_tbl;
    item_tbl_elemp = (ITEM_TBL_ELEM *)bsearch ((char *)&id,
            (char *)item_tblp->list, (unsigned int)item_tblp->count,
            sizeof (*item_tbl_elemp), (CMP_FN_PTR)item_tbl_compare);
    if (item_tbl_elemp == NULL) {
        rs = BLTIN_BAD_ID;
        goto misc_error;
    }
    item_type = item_tbl_elemp->item_type;

    /*
     *	Get the specified item into the appropriate flat structure.
     */

    (void)memset ((char *)&all_flat, 0, sizeof (all_flat));  /* keep CodeCenter happy */
    (void)memset ((char *)&generic_item, 0, sizeof (DDI_GENERIC_ITEM));
    (void)memset ((char *)&generic_item_rsp_code, 0, sizeof (DDI_GENERIC_ITEM));
    item_spec.item.id = id;
    app_info->env_returns.dds_errors.count = 0;
    switch (item_type) {
        case ARRAY_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;
            do {

              rs = ddi_get_item2(&item_spec, env_info,
                        (unsigned long)ARRAY_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_array = *(FLAT_ARRAY *)generic_item.item;

            if ((all_flat.flat_array.masks.attr_avail & ARRAY_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_array.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }

        /*
         *	If the subindex for the array is zero, then the array's response
         *	codes are all that need to be searched.  If the subindex is not
         *	zero, get the element's response codes, and search them first,
         *	and use the array's response codes only if the element's codes
         *	don't provide the one needed.
         */

            if (subindex == 0) {
                break;
            }
            item_spec.type = DDI_ITEM_ID_SI;
            item_spec.subindex = subindex;
            arr_rec_resp_codes = item_resp_codes;
            goto get_param_resp_codes;

        case RECORD_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;
            do {

                rs = ddi_get_item2(&item_spec, env_info,
                    (unsigned long)RECORD_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_record = *(FLAT_RECORD *)generic_item.item;

            if ((all_flat.flat_record.masks.attr_avail & RECORD_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_record.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }

        /*
         *	If the subindex for the record is zero, then the record's response
         *	codes are all that need to be searched.  If the subindex is not
         *	zero, get the member's response codes, and search them first,
         *	and use the record's response codes only if the member's codes
         *	don't provide the one needed.
         */

            if (subindex == 0) {
                break;
            }
            item_spec.type = DDI_ITEM_ID_SI;
            item_spec.subindex = subindex;
            arr_rec_resp_codes = item_resp_codes;
            goto get_param_resp_codes;

        case VARIABLE_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;

get_param_resp_codes:
            (void)memset ((char *)&generic_item, 0, sizeof (DDI_GENERIC_ITEM));

            do {
              rs = ddi_get_item2(&item_spec, env_info,
                        (unsigned long)VAR_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_var = *(FLAT_VAR *)generic_item.item;

            if ((all_flat.flat_var.masks.attr_avail & VAR_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_var.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }
            break;

        case VAR_LIST_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;
            do {

                rs = ddi_get_item2(&item_spec, env_info,
                        (unsigned long)VAR_LIST_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_var_list = *(FLAT_VAR_LIST *)generic_item.item;

            if ((all_flat.flat_var_list.masks.attr_avail & VAR_LIST_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_var_list.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }
            break;

        case DOMAIN_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;
            do {

                rs = ddi_get_item2(&item_spec, env_info,
                    (unsigned long)DOMAIN_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_domain = *(FLAT_DOMAIN *)generic_item.item;

            if ((all_flat.flat_domain.masks.attr_avail & DOMAIN_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_domain.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }
            break;

        case PROGRAM_ITYPE:
            item_spec.type = DDI_ITEM_ID;
            item_spec.subindex = 0;
            do {

                rs = ddi_get_item2(&item_spec, env_info,
                    (unsigned long)PROGRAM_RESP_CODES, &generic_item);

                rs = bltin_error_filter (rs, 1, env_info);
            } while (rs == METH_METHOD_RETRY);
            if (rs != SUCCESS) {
                goto get_item_error;
            }

            all_flat.flat_program = *(FLAT_PROGRAM *)generic_item.item;

            if ((all_flat.flat_program.masks.attr_avail & PROGRAM_RESP_CODES) == 0) {
                goto resp_codes_not_avail;
            }
            item_resp_codes = all_flat.flat_program.resp_codes;
            rs = ddi_clean_item (&generic_item);
            if (rs != SUCCESS) {
                goto misc_error;
            }
            break;

        default:
            rs = BLTIN_NO_RESP_CODES;
            goto misc_error;
    }

    (void)memset ((char *)&all_flat, 0, sizeof (all_flat));  /* redo for CodeCenter only */

    /*
     *	Get the response code item, and search it for a match.
     */

    if (item_resp_codes) {
        item_spec.type = DDI_ITEM_ID;
        item_spec.subindex = 0;
        item_spec.item.id = item_resp_codes;
        do {

            rs = ddi_get_item2(&item_spec, env_info,
                    (unsigned long)RESP_CODE_MEMBER, &generic_item_rsp_code);

            rs = bltin_error_filter (rs, 1, env_info);
        } while (rs == METH_METHOD_RETRY);
        if (rs != SUCCESS) {
            goto get_item_error;
        }

        all_flat.flat_resp_codes = *(FLAT_RESP_CODE *)generic_item_rsp_code.item;

        if (!(all_flat.flat_resp_codes.masks.attr_avail & RESP_CODE_MEMBER)) {
            goto resp_codes_not_avail;
        }
        response_codep = all_flat.flat_resp_codes.member.list;
        response_code_endp = response_codep +
                all_flat.flat_resp_codes.member.count;
        for ( ; response_codep < response_code_endp; response_codep++) {
            if (response_codep->val == (unsigned short)code) {
                break;
            }
        }

    } else {
        response_codep = response_code_endp = NULL;
    }

    /*
     *	If there was no match, and there is a set of response codes
     *	for the array or record, search those response codes.
     */

    if (response_codep == response_code_endp && arr_rec_resp_codes) {
        rs = ddi_clean_item (&generic_item_rsp_code);
        (void)memset ((char *)&generic_item_rsp_code, 0, sizeof (DDI_GENERIC_ITEM));
        if (rs != SUCCESS) {
            goto misc_error;
        }
        item_spec.type = DDI_ITEM_ID;
        item_spec.subindex = 0;
        item_spec.item.id = arr_rec_resp_codes;
        do {

            rs = ddi_get_item2(&item_spec, env_info,
                    (unsigned long)RESP_CODE_MEMBER, &generic_item_rsp_code);

            rs = bltin_error_filter (rs, 1, env_info);
        } while (rs == METH_METHOD_RETRY);
        if (rs != SUCCESS) {
            goto get_item_error;
        }

        all_flat.flat_resp_codes = *(FLAT_RESP_CODE *)generic_item_rsp_code.item;

        if ((all_flat.flat_resp_codes.masks.attr_avail & RESP_CODE_MEMBER) == 0) {
            goto resp_codes_not_avail;
        }
        response_codep = all_flat.flat_resp_codes.member.list;
        response_code_endp = response_codep + all_flat.flat_resp_codes.member.count;
        for ( ; response_codep < response_code_endp; response_codep++) {
            if (response_codep->val == (unsigned short)code) {
                break;
            }
        }
    }
    if (response_codep == response_code_endp) {
        (void)strcpy (format_buf, UNKNOWN_RESP_CODE);
        format_ptr = find_lang_string (format_buf);
        (void)sprintf (temp_buf, format_ptr, code);
    } else {
        (void)strncpy (temp_buf, response_codep->desc.str, sizeof (temp_buf));
    }
    print_string = find_lang_string (temp_buf);
    (void)strncpy (out_buf, print_string, (size_t) max_length);

    /*
     *	Cleanup and leave
     */

    rs = ddi_clean_item (&generic_item_rsp_code);
    if (rs != SUCCESS) {
        goto misc_error;
    }
    return (BLTIN_SUCCESS);

    /*
     *	Error return when experiencing an error during cleaning
     *	of a flat structure.
     */

misc_error:
    return (bltin_error_filter (rs, 0, env_info));
get_item_error:
    if (rs != BLTIN_DDS_ERROR) {
        CRASH_DBG();
        /*NOTREACHED*/
        rs = METH_INTERNAL_ERROR;
    } else {
        rs = app_info->env_returns.dds_return_code;
    }

    if (rs == DDL_CHECK_RETURN_LIST) {
        rs2 = app_info->env_returns.dds_errors.list[0].rc;
        if ((rs2 == SUCCESS) ||
            (app_info->env_returns.dds_errors.count == 0)) {
            CRASH_DBG();
            /*NOTREACHED*/
            rs = METH_INTERNAL_ERROR;
        }
    }

    goto clean_up_this_mess;
resp_codes_not_avail:
    CRASH_DBG();
    /*NOTREACHED*/
    rs = METH_INTERNAL_ERROR;

clean_up_this_mess:

    /*
     *	Clean up the flat structure.
     */

    (void)ddi_clean_item(&generic_item);
    rs = bltin_error_filter (rs, 0, env_info);
    return (rs);
}


/**********************************************************************
 *
 *  Name: bltin_select_from_menu
 *  ShortDesc: Display a menu of choices, and allow the operator to
 *			select from them.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Select from menu displays a menu of choices on the output device by
 *		calling the user supplied upcall (*display_menu_upcall)(), and
 *		returns the operator's selction to the calling method.
 *
 *  Input:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		subindices - The subindices of the params used in
 *				the prompt message.
 *		options - The options to select from
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Output:
 *		selection - The options selected.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
bltin_select_from_menu (char *prompt, ITEM_ID *param_ids, SUBINDEX *subindices,
        char *options, long *selection, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    char			out_buf[MAX_PROMPT_BUFFER_SIZE];
    int				rs;
    SELECT_LIST		select_list;
    SELECT_OPTION	*select_optionp, *select_option_endp;
    char			*option_end, *option_start;
    int				option_size;

    /*
     *	The option string is a string consisting of the selection options,
     *	each option is seperated from the other options with a colon (':').
     *	Each option may have country codes imbedded in it.  Change this
     *	string to a SELECT_LIST of options, each of which has had the
     *	correct country codes string selected.
     *
     *	First, determine how many option strings there are, so the
     *	correct amount of memory may be allocated.
     */

    select_list.select_count = 0;
    option_start = options;
    do {
        option_end = strchr (option_start, (int)';');
        select_list.select_count++;
        option_start = option_end + 1;
    } while (option_end != NULL);

    select_list.select_option =
        (SELECT_OPTION *) calloc ((unsigned)select_list.select_count,
                    sizeof (*select_list.select_option));
    if (select_list.select_option == NULL) {
        rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
        return (rs);
    }

    /*
     *	For each option string, malloc memory for it, copy it into the
     *	memory, and strip the correct country code string in it.
     */

    option_start = options;
    select_optionp = select_list.select_option;
    select_option_endp = select_optionp + select_list.select_count;
    do {
        option_end = strchr (option_start, (int)';');
        if (option_end == NULL) {
            option_size = strlen (option_start);
        } else {
            option_size = option_end - option_start;
        }
        select_optionp->select_str_head = (char *)malloc ((unsigned)(option_size + 1));
        if (select_optionp->select_str_head == NULL) {
            rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
            goto clean_up_and_leave;
        }
        (void)strncpy (select_optionp->select_str_head, option_start, option_size);
        select_optionp->select_str_head[option_size] = 0;
        select_optionp->select_string =
                find_lang_string (select_optionp->select_str_head);
        if (select_optionp->select_string == NULL) {
            rs = bltin_error_filter (BLTIN_NO_LANGUAGE_STRING, 0, env_info);
            goto clean_up_and_leave;
        }

        /* Check the string for unit codes that need to be suppressed */
        mi_handle_suppression(env_info, select_optionp->select_string);

        option_start = option_end + 1;
        select_optionp++;
    } while (option_end != NULL);

    ASSERT_DBG (select_optionp == select_option_endp);

    /*
     *	Translate the prompt string and param references into
     *	a displayable string.
     */

    do {
        rs = bltin_format_string2 (out_buf, sizeof (out_buf),
                    prompt, param_ids, subindices, bhs, use_bh, env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        goto clean_up_and_leave;
    }

    /*
     *	Display the menu and get the selected value.
     */

    do {
        rs = (*display_menu_upcall) ((ENV_INFO *)env_info, out_buf, &select_list,
                    (int *)selection);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);

    /*
     *	Free the allocated memory.
     */

clean_up_and_leave:
    select_optionp = select_list.select_option;
    while (select_optionp < select_option_endp) {
        if (select_optionp->select_str_head) {
            free (select_optionp->select_str_head);
        }
        select_optionp++;
    }
    free ((char *)select_list.select_option);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_response_code_string
 *  ShortDesc: Get the description of the response code for the
 *			specified item.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_response_code_string searches the response codes of the
 *		specified item, and returns the description of the
 *		specified response code via bltin_get_response_code_string.
 *		If the response code is not found, and if the item is a member
 *		of a record or an element of an array, then the record or
 *		array is also searched for the specified code.
 *
 *  Inputs:
 *		id - ID of the item to search.
 *		member_id - the member_id of the item to search for.
 *		code - The response codes to search for.
 *		max_length - The length of the buffer.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - The buffer for the returned response code description.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_response_code_string (ITEM_ID id, ITEM_ID member_id, UINT32 code,
        char *out_buf, long max_length, ENV_INFO2 *env_info)
{

    int 		rs;
    SUBINDEX	subindex = 0;
    ITEM_ID		res_id;

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;

    if (member_id != 0) {
        rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
        if (rs != BLTIN_SUCCESS) {
            return (rs);
        }
    }

    return (bltin_get_response_code_string (res_id, subindex, code, out_buf,
            max_length, env_info));
}


/*
 *	Display Builtins
 */


/**********************************************************************
 *
 *  Name: display builtin_error
 *  ShortDesc: Display's the text associated with a builtin return code
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Display_builtin_error displays a the text associated with a builtin
 *		return code on the output device by calling the user supplied
 *		upcall (*display_ack_upcall)(), and continues after receiving operator
 *		acknowledgement.
 *
 *  Inputs:
 *		error - The builtin return code.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

#define BUILTIN_ERROR_STRING_LENGTH	81

long
display_builtin_error (long error, ENV_INFO2 *env_info)
{

    char		*desc_string;
    char		string_with_cr[BUILTIN_ERROR_STRING_LENGTH];

    LOGC_INFO(CPM_FF_DDSERVICE, "display_builtin_error: param1: error = %ld\n", error);
    desc_string = dds_error_string ((int)error);
    (void)strncpy (string_with_cr, desc_string, BUILTIN_ERROR_STRING_LENGTH);
    strcat (string_with_cr, "\n");

    return (bltin_get_acknowledgement (string_with_cr, (ITEM_ID *)0,
            (SUBINDEX *)0, (BLOCK_HANDLE *)0, FALSE, env_info));
}

long
list_delete_element (unsigned long id, int index, ENV_INFO2 *env_info)
{
    int rs;
    do {
        rs = (*list_delete_element_upcall) ((ENV_INFO *)env_info, id, index);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}

long
list_delete_element2 (unsigned long id, int index,
                      unsigned long embedded_id, int embedded_index, ENV_INFO2 *env_info)
{
    int rs;
    do {
        rs = (*list_delete_element2_upcall) ((ENV_INFO *)env_info, id, index, embedded_id, embedded_index);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}

long
list_create_element (unsigned long id, int index, unsigned long element, ENV_INFO2 *env_info)
{
    int rs;
    do {
        rs = (*list_insert_element_upcall) ((ENV_INFO *)env_info, id, index, element);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}

/* AR 3457 (document the meaning of the parameters for list_create_element2)
 *
 * list_id : the ID of the "outer" list; the list that contains elements of type LIST
 * index : the index of the item being referred to in the "outer" list
 * embedded_list_id: the ID of the "inner" list; the list that makes up each element in the outer list
 * embedded_index: the index of the item to create in the "inner" list
 * embedded_element: the ID of the element type for the item in the inner list
 * env_info : environment info to be passed to the upcall
 */
long    list_create_element2(unsigned long 	list_id, int index,
            unsigned long embedded_list_id, int embedded_index, unsigned long embedded_element,
            ENV_INFO2 *env_info)
{
    int rs;
    do {
        rs = (*list_insert_element2_upcall) ((ENV_INFO *)env_info, list_id, index,
            embedded_list_id, embedded_index, embedded_element);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


long
plot_create (unsigned long id, ENV_INFO2 *env_info)
{
    long rs;
    rs = (*plot_create_upcall) ((ENV_INFO *)env_info, id);
    return (rs);
}

long
plot_destroy (long handle, ENV_INFO2 *env_info)
{
    long rs;
    rs = (*plot_destroy_upcall) ((ENV_INFO *)env_info, handle);
    return (rs);
}

long
plot_display (long handle, ENV_INFO2 *env_info)
{
    long rs;
    rs = (*plot_display_upcall) ((ENV_INFO *)env_info, handle);
    return (rs);
}

long
plot_edit (long handle, unsigned long waveform_id, ENV_INFO2 *env_info)
{
    long rs;
    rs = (*plot_edit_upcall) ((ENV_INFO *)env_info, handle, waveform_id);
    return (rs);
}

/**********************************************************************
 *
 *  Name: display_message
 *  ShortDesc: Display a message that doesn't require operator acknowledgement
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Display_message displays a message on the output device by
 *		calling the user supplied upcall (*display_upcall)(), and
 *		continues without requiring and operator acknowledgement.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
display_message (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, ENV_INFO2 *env_info)
{
    return bltin_display_message (prompt, param_ids, member_ids,
                                       id_count, (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_display_message (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    char			out_buf[MAX_PROMPT_BUFFER_SIZE];
    int				rs;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "display_message: param1: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE, "display_message: param2: param_ids[%d] = 0x%lx\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,"display_message: param3: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE,
        "display_message: param4: id_count = %ld\n", id_count);

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */

    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    /*
     *	Translate the prompt string and param references into
     *	a displayable string.
     */

    do {
        rs = bltin_format_string2 (out_buf, sizeof (out_buf),
                    prompt, res_ids, subindices, bhs, use_bh, env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the message and return immediately (without waiting
     *	for any acknowledgement).
     */

    do {
        rs = (*display_upcall) ((ENV_INFO *)env_info, out_buf);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: display_dynamics
 *  ShortDesc: Display a message with param values that continue to
 *			update until operator intervention stops the display.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Display_dynamics displays a message on the output device by
 *		calling the user supplied upcall (*display_continuous_upcall)().
 *		This upcall continues to display the information, using
 *		bltin_format_string() to parse the prompt and update the
 *		param values, until operator intervention stops the
 *		display.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
display_dynamics (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, ENV_INFO2 *env_info)
{
    return bltin_display_dynamics (prompt, param_ids, member_ids,
                                   id_count, (BLOCK_HANDLE *)0, FALSE, env_info);
}


long
bltin_display_dynamics (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    int				rs;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "display_dynamics: param1: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "display_dynamics: param2: param_ids[%d] = 0x%lx\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "display_dynamics: param3: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "display_dynamics: param4: id_count = %ld\n", id_count);

    /*
     *  Resolve member_ids into subindices
     */
    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    /* Since this call may output a string, perform (and record in env_info) any unit code suppression */
    mi_handle_suppression(env_info, prompt);

    /*
     *	Call the routine that will display the message continuously
     *	until the user stops the display.
     */

    do {
        if (!use_bh)
            rs = (*display_continuous_upcall) ((ENV_INFO *)env_info, prompt,
                        res_ids, subindices);
        else
            rs = (*display_continuous_upcall2) ((ENV_INFO *)env_info, prompt,
                        res_ids, subindices, bhs);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: display_comm_error
 *  ShortDesc: Display the description of a communications error, and
 *				wait for operator acknowledgement.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Display_comm_error displays a description of the specified
 *		communications error on the output device by calling the
 *		user supplied upcall (*display_ack_upcall)(), and continues
 *		after receiving operator acknowledgement.
 *
 *  Inputs:
 *		errcode - The communications error.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
display_comm_error (UINT32 errcode, ENV_INFO2 *env_info)
{

    char				out_buf[200];
    int					rs;

    LOGC_INFO(CPM_FF_DDSERVICE, "display_comm_error: param1: errcode = %u\n", errcode);
    /*
     *	Get the communications error string.
     */

    rs = get_comm_error_string (errcode, out_buf, (long) sizeof(out_buf), env_info);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the string.
     */

    return (bltin_get_acknowledgement (out_buf, (ITEM_ID *)0,
            (SUBINDEX *)0, (BLOCK_HANDLE *)0, FALSE, env_info));
}


/**********************************************************************
 *
 *  Name: display_response_code
 *  ShortDesc: Display the description of a response code, and
 *				wait for operator acknowledgement.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Display_respone_code displays a description of the specified
 *		response code on the output device by calling the user supplied
 *		upcall (*display_ack_upcall)(), and continues after receiving
 *		operator acknowledgement.
 *
 *  Inputs:
 *		id - The ID of the item generating the response code.
 *		member_id - the member_id of the item generating the response code
 *		code - the response code.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
display_response_code (ITEM_ID id, ITEM_ID member_id, UINT32 code, ENV_INFO2 *env_info)
{

    char		out_buf[200];
    int			rs;
    SUBINDEX	subindex = 0;
    ITEM_ID		res_id;

    LOGC_INFO(CPM_FF_DDSERVICE, "display_response_code: param1: id = %lu\n",  id);
    LOGC_INFO(CPM_FF_DDSERVICE,
        "display_response_code: param2: member_id = %lu\n", member_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "display_response_code: param3: code = %u\n", code);

    /*
     *  Resolve member_id into subindex
     */

    res_id = id;
    if (member_id != 0) {
        rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
        if (rs != BLTIN_SUCCESS) {
            return (rs);
        }
    }

    /*
     *  Get the response code string.
     */

    rs = bltin_get_response_code_string (res_id, subindex, code, out_buf,
                                (long) sizeof(out_buf), env_info);
    if (rs != BLTIN_SUCCESS) {
       return (rs);
    }

    /*
     *	Display the string.
     */

    return (bltin_get_acknowledgement (out_buf, (ITEM_ID *)0,
            (SUBINDEX *)0, (BLOCK_HANDLE *)0, FALSE, env_info));
}


/**********************************************************************
 *
 *  Name: delayfor
 *  ShortDesc: Display a message, and then delay for the specified time.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Delayfor displays a message by calling the user supplied
 *		upcall (*display_message)(), and then delays (suspends itself)
 *		for the specified amount of time.
 *
 *  Inputs:
 *		duration - The number of seconds to delay.
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
delayfor (long duration, char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, ENV_INFO2 *env_info)
{
    return bltin_delayfor (duration, prompt, param_ids, member_ids,
                           id_count, (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_delayfor (long duration, char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    char			out_buf[MAX_PROMPT_BUFFER_SIZE];
    int				rs;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: param1: duration = %ld\n", duration);
    LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: param2: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: param3: param_ids[%d] = 0x%lx\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: param4: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "delayfor: param5: id_count = %ld\n", id_count);

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */

    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    /*
     *	Translate the prompt string and param references into
     *	a displayable string.
     */

    do {
        rs = bltin_format_string2 (out_buf, sizeof (out_buf),
                    prompt, res_ids, subindices, bhs, use_bh, env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the message and return immediately (without waiting
     *	for any acknowledgement).
     */

    do {
        rs = (*display_upcall) ((ENV_INFO *)env_info, out_buf);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Pause for the specified time.
     */

    (void)sleep ((unsigned int)duration);

    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: bltin_edit_value
 *  ShortDesc: Display a device value, and let the operator modify it.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Edit_device_value displays a message on the output device by
 *		calling the user supplied upcall (*display_get_upcall)().
 *		This upcall allows the operator to modify the value of the
 *		displayed param.  The updated value returned from the
 *		upcall is stored in the Parameter Cache.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		subindices - The subindices of the params used in
 *				the prompt message.
 *		param_id - The ID of the param that may be modified.
 *		subindex - The subindex of the param that may be modified.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The value of the param param is modified in the Parameter Cache.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
bltin_edit_value (char *prompt, ITEM_ID *param_ids, SUBINDEX *subindices,
        P_REF *pc_p_ref, EVAL_VAR_VALUE *param_value, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    char			out_buf[MAX_PROMPT_BUFFER_SIZE];
    int				rs;

    /*
     *	Translate the prompt string and param references into
     *	a displayable string.
     */

    do {
        rs = bltin_format_string2 (out_buf, sizeof (out_buf),
                    prompt, param_ids, subindices, bhs, use_bh, env_info);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the message and get the entered value for the parameter.
     */

    do {
        rs = (*display_get_upcall) ((ENV_INFO *)env_info, out_buf, pc_p_ref,
                param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);

    return (rs);
}


/**********************************************************************
 *
 *  Name: edit_device_value
 *  ShortDesc: Display a device value, and let the operator modify it.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Edit_device_value displays a message on the output device by
 *		calling the user supplied upcall (*display_get_upcall)().
 *		This upcall allows the operator to modify the value of the
 *		displayed param.  The updated value returned from the
 *		upcall is stored in the Parameter Cache.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		param_id - The ID of the param that may be modified.
 *		member_id - The member_id of the param that may be modified.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The value of the param param is modified in the Parameter Cache.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis/Conrad Beaulieu
 *
 *********************************************************************/

long
edit_device_value (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
    long id_count, ITEM_ID param_id, ITEM_ID member_id, ENV_INFO2 *env_info)
{
    return bltin_edit_device_value (prompt, param_ids, member_ids,
                                    id_count, param_id, member_id, (BLOCK_HANDLE) 0,
                                    (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_edit_device_value (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
    long id_count, ITEM_ID param_id, ITEM_ID member_id,
    BLOCK_HANDLE bh, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    int				id_counter;
    EVAL_VAR_VALUE	param_value;
    P_REF			pc_p_ref;
    ITEM_ID			res_id;
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				rs;
    unsigned short	save_use_info;
    SUBINDEX		subindex;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_TYPE		type;
    APP_INFO		*app_info;
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;
    ENV_INFO2       edited_value_env_info;

    LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value: param1: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "edit_device_value: param2: param_ids[%d] = 0x%lu\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "edit_device_value: param3: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value: param4: id_count = %ld\n", id_count);
    LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value: param4: param_id = %lu\n", param_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "edit_device_value: param6: member_id = %lu\n", member_id);
    /* If we're using cross-block referencing, get the correct app_info for the block */
    if (use_bh)
    {
        rs = env2_handle_to_env2(env_info, &edited_value_env_info, bh);
        if (rs)
            return rs;
    }
    else
        memcpy(&edited_value_env_info, env_info, sizeof(edited_value_env_info));

    app_info = (APP_INFO *)edited_value_env_info.app_info;

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */
    if(use_bh == TRUE) save_bh = env_info->block_handle;


    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }


    /*
     *  Resolve member_id into subindex
     */

    res_id = param_id;
    if(use_bh == TRUE) env_info->block_handle = bh;
    rs = bltin_resolve_subindex(&res_id, member_id, &subindex, env_info);
    if (rs != BLTIN_SUCCESS) {
        if(use_bh == TRUE) env_info->block_handle = save_bh;
        return (rs);
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    /*
     *	Get the current information about the parameter.
     */

    pc_p_ref.id 		= res_id;
    pc_p_ref.subindex	= subindex;
    pc_p_ref.type		= PC_ITEM_ID_REF;
    do {
        rs = pc_get_param_value ((ENV_INFO *)&edited_value_env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, &edited_value_env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Display the message and get the entered value for the parameter.
     */
    rs = bltin_edit_value (prompt, res_ids, subindices, &pc_p_ref,
                        &param_value, bhs, use_bh, &edited_value_env_info);
    if (rs != BLTIN_SUCCESS) {
        goto clean_and_leave;
    }

    /*
     *	Put the new value into the Parameter Cache.
     */

    save_use_info = app_info->status_info & USE_MASK;
    if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
        app_info->status_info &= ~USE_MASK;
        app_info->status_info |= USE_EDIT_VALUE ;
    }
    do {
        rs = pc_put_param_value ((ENV_INFO *)&edited_value_env_info, &pc_p_ref, &param_value);
        rs = bltin_error_filter (rs, 1, &edited_value_env_info);
    } while (rs == METH_METHOD_RETRY);
    app_info->status_info &= ~USE_MASK;
    app_info->status_info |= save_use_info;

clean_and_leave:
    type = param_value.type;
    if (type == DDS_ASCII || type == DDS_PASSWORD
        || type == DDS_BITSTRING || type == DDS_OCTETSTRING
        || type == DDS_VISIBLESTRING || type == DDS_EUC) {

        /* This will free the string if it was allocated */
        bltin_free_string (&param_value.val.s);
    }
    return (rs);
}


/**********************************************************************
 *
 *  Name: edit_local_value
 *  ShortDesc: Display a value local to a method, and let the
 *			operator modify it.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		This function sends a local interpreter param to the operator to
 *		be edited.
 *
 *		Note that the local interpreter value for array types is an unformatted
 *		string of characters and can not be assumed by the application to be
 *      of any particular multi-byte type, such as PASSWORD or BIT_ENUMERATED.
 *
 *  Inputs:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		localparam - The name of the local param that may be modified.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The value of the local param is modified.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

/*ARGSUSED*/

long
edit_local_value (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, char *localparam, ENV_INFO2 *env_info)
{
    return bltin_edit_local_value (prompt, param_ids, member_ids,
                             id_count, localparam, (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_edit_local_value (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, char *localparam, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "edit_local_value: param1: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "edit_local_value: param2: param_ids[%d] = 0x%lx\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "edit_local_value: param3: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "edit_local_value: param4: id_count = %ld\n", id_count);
    LOGC_INFO(CPM_FF_DDSERVICE, "edit_local_value: param5: localparam = %s\n", localparam);

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */

    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    /*
     *	Get the current information about the localparam.
     */

    rs = get_local_value(localparam, &param_value, env_info);
    if (rs != BLTIN_SUCCESS) {
        return(rs);
    }

    rs = bltin_edit_value(prompt, res_ids, subindices, (P_REF *)NULL,
                    &param_value, bhs, use_bh, env_info);
    if (rs != BLTIN_SUCCESS) {
        return(rs);
    }

    return(put_local_value(localparam, &param_value, env_info));
}


/**********************************************************************
 *
 *  Name: select_from_menu
 *  ShortDesc: Display a menu of choices, and allow the operator to
 *			select from them.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Select from menu displays a menu of choices on the output device by
 *		calling the user supplied upcall (*display_menu_upcall)(), and
 *		returns the operator's selction to the calling method.
 *		This function is performed via bltin_select_from_menu.
 *
 *  Input:
 *		prompt - The message to display.  This message may contain
 *				printf-like formatting which refer to the values of
 *				the provided params.
 *		param_ids - The IDs of the params used in the prompt message.
 *		member_ids - The member_ids of the params used in
 *				the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *		options - The options to select from
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Output:
 *		selection - The options selected.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

 long
select_from_menu (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
    long id_count, char *options, long *selection, ENV_INFO2 *env_info)
{
    return bltin_select_from_menu2 (prompt, param_ids, member_ids,
                                   id_count, options, selection, (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_select_from_menu2 (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
    long id_count, char *options, long *selection, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    int				rs;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr = param_ids;
    ITEM_ID 		*mem_ptr = member_ids;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param1: prompt = %s\n", prompt);
    for (i = 0; i < id_count; i++) {
        LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param2: param_ids[%d] = 0x%lx\n",
                i, *par_ptr++);
    }
    for (i = 0; i < id_count; i++) {
        LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param3: member_ids[%d] = 0x%lx\n",
                i, *mem_ptr++);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param4: id_count = %ld\n", id_count);
    LOGC_INFO(CPM_FF_DDSERVICE, "select_from_menu: param5: options = %s\n", options);

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */

    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }
    if(use_bh == TRUE) env_info->block_handle = save_bh;

    return (bltin_select_from_menu (prompt, res_ids, subindices, options,
            selection, bhs, use_bh, env_info));
}


long menu_display(ITEM_ID menuid, char *options,
                  long *selection, ENV_INFO2 *env_info)
{
    long rs;
    SELECT_LIST		select_list;
    SELECT_OPTION	*select_optionp, *select_option_endp;
    char			*option_end, *option_start;
    int				option_size;

    if (!display_menu_upcall2)
        return BLTIN_NOT_IMPLEMENTED;

    select_list.select_count = 0;
    option_start = options;
    do {
        option_end = strchr (option_start, (int)';');
        select_list.select_count++;
        option_start = option_end + 1;
    } while (option_end != NULL);

    select_list.select_option =
        (SELECT_OPTION *) calloc ((unsigned)select_list.select_count,
                    sizeof (*select_list.select_option));
    if (select_list.select_option == NULL) {
        rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
        return (rs);
    }

    /*
     *	For each option string, malloc memory for it, copy it into the
     *	memory, and strip the correct country code string in it.
     */

    option_start = options;
    select_optionp = select_list.select_option;
    select_option_endp = select_optionp + select_list.select_count;
    do {
        option_end = strchr (option_start, (int)';');
        if (option_end == NULL) {
            option_size = strlen (option_start);
        } else {
            option_size = option_end - option_start;
        }
        select_optionp->select_str_head = (char *)malloc ((unsigned)(option_size + 1));
        if (select_optionp->select_str_head == NULL) {
            rs = bltin_error_filter (BLTIN_NO_MEMORY, 0, env_info);
            goto clean_up_and_leave;
        }
        (void)strncpy (select_optionp->select_str_head, option_start, option_size);
        select_optionp->select_str_head[option_size] = 0;
        select_optionp->select_string =
                find_lang_string (select_optionp->select_str_head);
        if (select_optionp->select_string == NULL) {
            rs = bltin_error_filter (BLTIN_NO_LANGUAGE_STRING, 0, env_info);
            goto clean_up_and_leave;
        }

        /* Check the string for unit codes that need to be suppressed */
        mi_handle_suppression(env_info, select_optionp->select_string);

        option_start = option_end + 1;
        select_optionp++;
    } while (option_end != NULL);

    ASSERT_DBG (select_optionp == select_option_endp);


    rs = (*display_menu_upcall2)((ENV_INFO *)env_info, &select_list, (int *)selection,
        menuid);
clean_up_and_leave:
    select_optionp = select_list.select_option;
    while (select_optionp < select_option_endp) {
        if (select_optionp->select_str_head) {
            free (select_optionp->select_str_head);
        }
        select_optionp++;
    }
    free ((char *)select_list.select_option);

    if (rs < 0)
        method_abort("User selected abort", env_info);

    return rs;
}

/**********************************************************************
 *
 *  Name: get_acknowledgement
 *  ShortDesc: Display a message that requires operator acknowledgement
 *
 *  Include:
 *      ddsbltin.h
 *
 *  Description:
 *
 *      Get_acknowledgement displays a message on the output device by
 *      calling the user supplied upcall (*display_ack_upcall)(), and
 *      continues after receiving operator acknowledgement.
 *
 *  Inputs:
 *      prompt - The message to display.  This message may contain
 *              printf-like formatting which refer to the values of
 *              the provided params.
 *      param_ids - The IDs of the params used in the prompt message.
 *      member_ids - The member_ids of the params used in
 *              the prompt message.
 *		id_count - the number of valid elements in param_ids and member_ids arrays
 *      env_info - The ENV_INFO2 structure containing information
 *                      about the current request.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Any of the error returns specified in builtins.h
 *
 *  Author:
 *      Bruce Davis
 *
 *********************************************************************/

long
get_acknowledgement (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, ENV_INFO2 *env_info)
{
    return bltin_get_acknowledgement2 (prompt, param_ids, member_ids,
                                       id_count, (BLOCK_HANDLE *)0, FALSE, env_info);
}

long
bltin_get_acknowledgement2 (char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids,
        long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info)
{

    int             rs;
    int				id_counter;
    SUBINDEX		subindices[MAX_PROMPT_MSG_PARAMS];
    ITEM_ID			res_ids[MAX_PROMPT_MSG_PARAMS];
    int				i;
    ITEM_ID 		*par_ptr;
    ITEM_ID 		*mem_ptr;
    BLOCK_HANDLE	save_bh = 0;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_acknowledgement: param1: prompt = %s\n", prompt);
    par_ptr = param_ids;
    for (i = 0; i < id_count; i++, par_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "get_acknowledgement: param2: param_ids[%d] = 0x%lx\n", i, *par_ptr);
    }
    mem_ptr = member_ids;
    for (i = 0; i < id_count; i++, mem_ptr++) {
        LOGC_INFO(CPM_FF_DDSERVICE,
            "get_acknowledgement: param3: member_ids[%d] = 0x%lx\n", i, *mem_ptr);
    }
    LOGC_INFO(CPM_FF_DDSERVICE, "get_acknowledgement: param4: id_count = %ld\n", id_count);

    if (id_count > MAX_PROMPT_MSG_PARAMS) {
        return(BLTIN_INVALID_PROMPT);  /* This could really be its own error */
    }

    /*
     *  Resolve member_ids into subindices
     */

    if(use_bh == TRUE) save_bh = env_info->block_handle;

    for (id_counter = 0; id_counter < id_count; id_counter++) {
        res_ids[id_counter] = param_ids[id_counter];
        if(use_bh == TRUE)	env_info->block_handle=bhs[id_counter];
        rs = bltin_resolve_subindex(&(res_ids[id_counter]), member_ids[id_counter],
                &subindices[id_counter], env_info);
        if (rs != BLTIN_SUCCESS) {
            if(use_bh == TRUE) env_info->block_handle = save_bh;
            return (rs);
        }
    }

    if(use_bh == TRUE) env_info->block_handle = save_bh;

    return (bltin_get_acknowledgement (prompt, res_ids, subindices, (BLOCK_HANDLE *)0, FALSE, env_info));
}


/*
 *	Miscellaneous Builtins
 */


/**********************************************************************
 *
 *  Name: is_NaN
 *  ShortDesc: Check double value for not a number.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Check the provided double value for equality to Nan (not
 *		a number).
 *
 *  Inputs:
 *		dvalue - The provided double value.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		1 if equal to Nan, 0 otherwise.
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

long
is_NaN (double dvalue)
{

    unsigned char	NaN_bytes[2];
    unsigned char	*double_p;

    LOGC_INFO(CPM_FF_DDSERVICE, "is_NaN: param1: dvalue = %f\n", dvalue);
    NaN_bytes[0] = 0x7F;
    NaN_bytes[1] = 0xF0;
    double_p = (unsigned char *)&dvalue;
    /* check upper 2 bytes for NaN exponent and not zero in other bytes */
    if (((double_p[0] & NaN_bytes[0]) == NaN_bytes[0]) &&
        ((double_p[1] & NaN_bytes[1]) == NaN_bytes[1]) &&
        ((double_p[1] & 0x0F) | double_p[2] | double_p[3] | double_p[4] |
          double_p[5] | double_p[6] | double_p[7])) {
        return(1);
    }
    else {
        return(0);
    }
}


/**********************************************************************
 *
 *  Name: NaN_value
 *  ShortDesc: Get a "Not a Number"(NaN) double value.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *      Return a NaN double value.
 *
 *  Inputs:
 *		None.
 *
 *  Outputs:
 *		dvaluep - The returned NaN double value.
 *
 *  Returns:
 *		0.
 *
 *  Author:
 *		Don Marquart
 *
 *********************************************************************/

long
NaN_value( double *dvaluep )
{

    unsigned char	*NaN_bytes;

    LOGC_INFO(CPM_FF_DDSERVICE, "NaN_value: \n");

    NaN_bytes = (unsigned char*)dvaluep;
    NaN_bytes[0] = 0x7F;
    NaN_bytes[1] = 0xFF;


    return(0);
}


/**********************************************************************
 *
 *  Name: get_stddict_string
 *  ShortDesc: Get a standard dictionary string
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_stddict_string takes a standard dictionary identifier, and
 *		returns the corresponding text string in the supplied buffer.
 *
 *  Inputs:
 *		id - The standard dictionary identifier
 *		maxlen - The length of the buffer.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		str_buf - The returned string.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_stddict_string (UINT32 id, char *str_buf, long maxlen, ENV_INFO2 *env_info)
{

    STRING			local_string;
    int				rs;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_stddict_string: param1: id = 0x%x\n", id);
    LOGC_INFO(CPM_FF_DDSERVICE, "get_stddict_string: param3: maxlen = %ld\n", maxlen);

    /*
     *	Get the Standard Dictionary string
     */

    do {
        rs = app_func_get_dict_string((ENV_INFO *)env_info, id, &local_string);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Return the string.
     */

    if (local_string.len > maxlen) {
        rs = bltin_error_filter (BLTIN_BUFFER_TOO_SMALL, 0, env_info);
    } else {
        rs = BLTIN_SUCCESS;
    }
    (void)strncpy (str_buf, local_string.str, (size_t) maxlen);
    /* This will free the string if it was allocated */
    bltin_free_string (&local_string);
    return (rs);
}


/**********************************************************************
 *
 *  Name: resolve
 *  ShortDesc: setup for a call to the DDS routine resolve_ref to
 *     			resolve a DDL reference.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve calls resolve_ref which takes a ref_info_list array and
 *		resolves it to the description reference which is the item_id and
 *      type of the reference.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *      ref_info_list - A pointer to the list of reference information
 *                 		used to resolve a reference.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

static ITEM_ID
resolve(ENV_INFO2 *env_info, REF_INFO_LIST *ref_info_list)
{

    METH_INFO		*meth_infop;
    OP_REF_TRAIL	op_ref_trail;
    OP_REF			param_op_ref;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
        return ((ITEM_ID)0);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];

    /*
     *	If this is the top level resolve, initialize the error return
     *	to zero.  If this is not the top level, then if the error return
     *	is non-zero, just return.
     */

    if (meth_infop->meth_resolve_nest_count++ == 0) {
        meth_infop->meth_resolve_rc = BLTIN_SUCCESS;
    } else if (meth_infop->meth_resolve_rc != BLTIN_SUCCESS) {
        meth_infop->meth_resolve_nest_count--;
        return ((ITEM_ID)0);
    }

    /*
     *	Call resolve to resolve the reference.
     */

    meth_infop->meth_resolve_rc =
        resolve_ref (ref_info_list, &op_ref_trail, (OP_REF_LIST *)0,
            env_info, &param_op_ref, RESOLVE_DESC_REF, (unsigned short)0);
    meth_infop->meth_resolve_nest_count--;
    ASSERT_DBG (meth_infop->meth_resolve_nest_count >= 0);
    if (meth_infop->meth_resolve_rc != SUCCESS) {
        return ((ITEM_ID)0);
    }
    return (op_ref_trail.desc_id);
}


/**********************************************************************
 *
 *  Name: resolve_block_ref
 *  ShortDesc: setup for a call to resolve for a block parameter ref.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_block_ref calls resolve which resolves the block
 *		parameter reference to an item id.
 *
 *  Inputs:
 *		member - The id of the member.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

ITEM_ID
resolve_block_ref(ITEM_ID member, ENV_INFO2 *env_info)
{

    REF_INFO_LIST	ref_info_list;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_block_ref: param1: member = 0x%lx\n", member);

    /*
     *	Call resolve to resolve the reference.
     */

    ref_info_list.count = 1;
    ref_info_list.list[0].type = VIA_BLOCK_REF;
    ref_info_list.list[0].val.member = member;

    return resolve(env_info, &ref_info_list);
}

ITEM_ID
resolve_block_ref2(ITEM_ID block_id, unsigned long instance, ITEM_ID member, ENV_INFO2 *env_info)
{

    REF_INFO_LIST	ref_info_list;
    ENV_INFO2 env2;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_block_ref2: param1: block_id = 0x%lx\n", block_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_block_ref2: param2: instance = %lu\n", instance);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_block_ref2: param3: member = 0x%lx\n", member);

    /*
     * Call resolve to resolve the reference.
     */

    ref_info_list.count = 1;
    ref_info_list.list[0].type = VIA_BLOCK_REF;
    ref_info_list.list[0].val.member = member;

    if (env2_to_env2(env_info, &env2, block_id, instance))
        return 0;

    return resolve(&env2, &ref_info_list);
}

/**********************************************************************
 *
 *  Name: resolve_param_ref
 *  ShortDesc: setup for a call to resolve for a parameter ref.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_param_ref calls resolve which resolves the
 *		parameter reference to an item id.
 *
 *  Inputs:
 *		member - The id of the parameter.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

ITEM_ID
resolve_param_ref(ITEM_ID member, ENV_INFO2 *env_info)
{

    REF_INFO_LIST	ref_info_list;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_ref: param1: member = 0x%lx\n", member);

    /*
     * Call resolve to resolve the reference.
     */

    ref_info_list.count = 1;
    ref_info_list.list[0].type = VIA_PARAM_REF;
    ref_info_list.list[0].val.member = member;

    return resolve(env_info, &ref_info_list);
}

ITEM_ID
resolve_param_ref2(ITEM_ID block_id, unsigned long instance, ITEM_ID member, ENV_INFO2 *env_info)
{

    REF_INFO_LIST	ref_info_list;
    ENV_INFO2 env2;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_ref2: param1: block_id = 0x%lx\n", block_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_ref2: param2: instance = %lu\n", instance);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_ref2: param3: block_id = 0x%lx\n", member);

    /*
     * Call resolve to resolve the reference.
     */
    ref_info_list.count = 1;
    ref_info_list.list[0].type = VIA_PARAM_REF;
    ref_info_list.list[0].val.member = member;

    if (env2_to_env2(env_info, &env2, block_id, instance))
        return 0;

    return resolve(&env2, &ref_info_list);
}



/**********************************************************************
 *
 *  Name: resolve_local_ref
 *  ShortDesc: Locate the ITEM ID of a local parameter.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_local_ref resolves the given local
 *		parameter reference to an item id.
 *
 *  Inputs:
 *		member - The member id of the parameter.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *********************************************************************/
ITEM_ID
resolve_local_ref(ITEM_ID member, ENV_INFO2 *env_info)
{

    int rs;
    DDI_GENERIC_ITEM gi;
    FLAT_BLOCK *fb;
    int i;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_local_ref: param1: member = 0x%lx\n", member);

    /*
     * Call DDI to search the local list
     */

    rs = pc_get_locals(env_info->block_handle, &gi);
    if (rs)
        return rs;
    fb = (FLAT_BLOCK *)gi.item;
    for (i=0; i < fb->local_param.count; i++)
    {
        if (fb->local_param.list[i].name == member)
        {
            unsigned long rid = fb->local_param.list[i].ref.id;
            ddi_clean_item(&gi);
            return rid;
        }
    }
    ddi_clean_item(&gi);
    return BLTIN_VAR_NOT_FOUND;
}

ITEM_ID
resolve_local_ref2(ITEM_ID block_id, unsigned long instance, ITEM_ID member, ENV_INFO2 *env_info)
{

    ITEM_ID rs;
    BLOCK_HANDLE bh, save_bh;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_local_ref: param1: block_id = 0x%lx\n", block_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_local_ref: param2: instance = 0x%lx\n", instance);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_local_ref: param3: member = 0x%lx\n", member);

    if (!app_func_get_block_handle)
        return (ITEM_ID)-1;

    /* Find the block handle for the passed-in block */
    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, block_id, instance, &bh);
    if (rs)
        return BLTIN_BLOCK_NOT_FOUND;

    save_bh = env_info->block_handle;
    env_info->block_handle = bh;
    rs = resolve_local_ref(member, env_info);
    env_info->block_handle = save_bh;
    return rs;
}


/**********************************************************************
 *
 *  Name: resolve_param_list_ref
 *  ShortDesc: setup for a call to resolve for a parameter list ref.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_param_list_ref calls resolve which resolves the
 *		parameter_list reference to an item id.
 *
 *  Inputs:
 *		member - The id of the parameter_list.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

ITEM_ID
resolve_param_list_ref(ITEM_ID member, ENV_INFO2 *env_info)
{

    REF_INFO_LIST	ref_info_list;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_param_list_ref: param1: member = 0x%lx\n", member);

    /*
     * Call resolve to resolve the reference.
     */

    ref_info_list.count = 1;
    ref_info_list.list[0].type = VIA_PARAM_LIST_REF;
    ref_info_list.list[0].val.member = member;

    return resolve(env_info, &ref_info_list);
}


/**********************************************************************
 *
 *  Name: resolve_array_ref
 *  ShortDesc: setup for a call to resolve for an array reference.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_array_ref calls resolve which resolves the
 *		array reference to an item id.
 *
 *  Inputs:
 *		array_id - item id of the array to resolve through.
 *		subindex - index of the element in the array.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

ITEM_ID
resolve_array_ref(ITEM_ID array_id, UINT32 subindex, ENV_INFO2 *env_info)
{

    DDI_BLOCK_SPECIFIER	block_spec;
    DDI_ITEM_SPECIFIER	item_spec;
    ITEM_TYPE			item_type;
    int					rs;
    REF_INFO_LIST		ref_info_list;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param1: array_id = 0x%lx\n", array_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param2: subindex = 0x%x\n", subindex);

    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    item_spec.type = DDI_ITEM_ID;
    item_spec.subindex = 0;
    item_spec.item.id = array_id;

    rs = ddi_get_type(&block_spec, &item_spec, &item_type);
    if (rs == DDS_SUCCESS) {
        if (item_type == ARRAY_ITYPE || item_type == ITEM_ARRAY_ITYPE) {
            ref_info_list.count = 2;
            if (item_type == ARRAY_ITYPE) {
                if (!IS_MEMBER(array_id))
                    return array_id;
                ref_info_list.list[1].type = ARRAY_ID_REF;
                ref_info_list.list[0].type = VIA_ARRAY_REF;
            } else {
                ref_info_list.list[1].type = ITEM_ARRAY_ID_REF;
                ref_info_list.list[0].type = VIA_ITEM_ARRAY_REF;
            }
            ref_info_list.list[1].val.id = array_id;
            ref_info_list.list[0].val.index = (unsigned short)subindex;

            return resolve(env_info, &ref_info_list);
        }
        rs = BLTIN_BAD_ITEM_TYPE;
    }

    (void)bltin_error_filter(rs, 0, env_info);

    return ((ITEM_ID)0);
}


ITEM_ID
resolve_array_ref2(ITEM_ID block_id, unsigned long instance, ITEM_ID array_id, unsigned long subindex, ENV_INFO2 *env_info)
{
    ITEM_ID rs;
    BLOCK_HANDLE bh, save_bh;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param1: block_id = 0x%lx\n", block_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param2: instance = %lu\n",  instance);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param3: array_id = 0x%lx\n", array_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_array_ref: param4: subindex = 0x%lx\n", subindex);
    if (!app_func_get_block_handle)
        return (ITEM_ID)-1;

    /* Find the block handle for the passed-in block */
    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, block_id, instance, &bh);
    if (rs)
        return BLTIN_BLOCK_NOT_FOUND;

    save_bh = env_info->block_handle;
    env_info->block_handle = bh;
    rs = resolve_array_ref(array_id, subindex, env_info);
    env_info->block_handle = save_bh;
    return rs;
}

/**********************************************************************
 *
 *  Name: resolve_record_ref
 *  ShortDesc: setup for a call to resolve for a record reference.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_record_ref calls resolve which resolves the
 *		record member reference to an item id.
 *
 *  Inputs:
 *		record_id - item id of the record to resolve through.
 *		member - member id in the record to resolve.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *  Author: Jon Westbrock
 *
 *********************************************************************/

ITEM_ID
resolve_record_ref(ITEM_ID record_id, ITEM_ID member, ENV_INFO2 *env_info)
{

    DDI_BLOCK_SPECIFIER	block_spec;
    DDI_ITEM_SPECIFIER	item_spec;
    ITEM_TYPE			item_type;
    int					rs;
    REF_INFO_LIST		ref_info_list;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param1: record_id = 0x%lx\n", record_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param2: member_id = 0x%lx\n", member);

    /* If the member ID we're searching for has already been resolved to an ITEM ID, */
    /* just return it. */
    if (!IS_MEMBER(member))
        return member;
    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    item_spec.type = DDI_ITEM_ID;
    item_spec.subindex = 0;
    item_spec.item.id = record_id;

    rs = ddi_get_type(&block_spec, &item_spec, &item_type);
    if (rs == DDS_SUCCESS)
    {
        switch(item_type)
        {
            case RECORD_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = RECORD_ID_REF;
                ref_info_list.list[0].type = VIA_RECORD_REF;
                break;
            case COLLECTION_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = COLLECTION_ID_REF;
                ref_info_list.list[0].type = VIA_COLLECTION_REF;
                break;
            case VAR_LIST_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = VAR_LIST_ID_REF;
                ref_info_list.list[0].type = VIA_VAR_LIST_REF;
                break;
            case CHART_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = CHART_ID_REF;
                ref_info_list.list[0].type = VIA_CHART_REF;
                break;
            case FILE_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = FILE_ID_REF;
                ref_info_list.list[0].type = VIA_FILE_REF;
                break;
            case GRAPH_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = GRAPH_ID_REF;
                ref_info_list.list[0].type = VIA_GRAPH_REF;
                break;
            case SOURCE_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = SOURCE_ID_REF;
                ref_info_list.list[0].type = VIA_SOURCE_REF;
                break;
            case LIST_ITYPE:
                ref_info_list.count = 2;
                ref_info_list.list[1].type = LIST_ID_REF;
                ref_info_list.list[0].type = VIA_LIST_REF;
                break;
            default:
                rs = BLTIN_BAD_ITEM_TYPE;
                goto getout;
        }
        ref_info_list.list[1].val.id = record_id;
        ref_info_list.list[0].val.member = member;

        return resolve(env_info, &ref_info_list);
    }
    else
        rs = BLTIN_BAD_ITEM_TYPE;

getout:
    (void)bltin_error_filter(rs, 0, env_info);

    return ((ITEM_ID)0);
}


ITEM_ID
resolve_record_ref2(ITEM_ID block_id, unsigned long instance, ITEM_ID record_id, ITEM_ID member, ENV_INFO2 *env_info)
{
    ITEM_ID rs;
    BLOCK_HANDLE bh, save_bh;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param1: block_id = 0x%lx\n", block_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param2: instance = %lu\n", instance);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param3: record_id = 0x%lx\n", record_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_record_ref: param4: member_id = 0x%lx\n", member);

    if (!app_func_get_block_handle)
        return (ITEM_ID)-1;

    /* Find the block handle for the passed-in block */
    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, block_id, instance, &bh);
    if (rs)
        return BLTIN_BLOCK_NOT_FOUND;

    save_bh = env_info->block_handle;
    env_info->block_handle = bh;
    rs = resolve_record_ref(record_id, member, env_info);
    env_info->block_handle = save_bh;
    return rs;
}

/**********************************************************************
 *
 *  Name: resolve_list_ref
 *  ShortDesc: setup for a call to resolve the type of a LIST reference
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *      Resolve_list_ref calls resolve which resolves the
 *		list reference to an item id.
 *
 *  Inputs:
 *		list_id - item id of the record to resolve through.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		The ITEM_ID of the resolved reference, or zero for an error.
 *
 *********************************************************************/

ITEM_ID
resolve_list_ref(ITEM_ID list_id, ENV_INFO2 *env_info)
{

    DDI_ITEM_SPECIFIER	item_spec;
    int					rs;
    DDI_GENERIC_ITEM    gen_item;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_list_ref: param1: list_id = 0x%lx\n", list_id);

    item_spec.type = DDI_ITEM_ID;
    item_spec.subindex = 0;
    item_spec.item.id = list_id;
    memset(&gen_item, 0, sizeof(gen_item));

    rs = ddi_get_item2(&item_spec, env_info, LIST_TYPE, &gen_item);
    if (rs == DDS_SUCCESS)
    {
        FLAT_LIST *list;
        ITEM_ID id;

        list = (FLAT_LIST *)gen_item.item;
        id = list->type;
        ddi_clean_item(&gen_item);
        return id;
    }
    else
        rs = BLTIN_BAD_ITEM_TYPE;
    (void)bltin_error_filter(rs, 0, env_info);

    return ((ITEM_ID)0);
}


ITEM_ID
resolve_selector_ref(ITEM_ID record_id, ITEM_ID selector, ITEM_ID additional, ENV_INFO2 *env_info)
{

    DDI_BLOCK_SPECIFIER	block_spec;
    DDI_ITEM_SPECIFIER	item_spec;
    ITEM_TYPE			item_type;
    int					rs;

    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_selector_ref: param1: record_id = 0x%lx\n", record_id);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_selector_ref: param2: selector = 0x%lx\n", selector);
    LOGC_INFO(CPM_FF_DDSERVICE, "resolve_selector_ref: param3: additional = 0x%lx\n", additional);

    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    item_spec.type = DDI_ITEM_ID;
    item_spec.subindex = 0;
    item_spec.item.id = record_id;

    rs = ddi_get_type(&block_spec, &item_spec, &item_type);

    (void)bltin_error_filter(rs, 0, env_info);

    return ((ITEM_ID)0);
}


/**********************************************************************
 *
 *  Name: get_resolve_status
 *  ShortDesc: Return the error generated by resolve_itemarray_ref or
 *				resolve_collection_ref
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Resolve_array_ref, resolve_block_ref, resolve_param_list_ref,
 *		resolve_param_ref and resolve_record_ref are calls in methods
 *		that resolve the possibly dynamic ID of an element in an
 *		item such as a record, list or array.  If any of these
 *		routines encounter an error, they return an ITEM_ID of zero.
 *		If the method wishes to know what the error was, then
 *		get_resolve_status is called, which returns the error.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Error returns from other builtin, DDS and Parameter Cache functions.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_resolve_status (ENV_INFO2 *env_info)
{

    METH_INFO		*meth_infop;
    APP_INFO		*app_info;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     *	Get a pointer to the method's information block.
     */

    if (!(VALID_METHOD_TAG (app_info->method_tag))) {
        CRASH_DBG();
        /*NOTREACHED*/
        (void)bltin_error_filter (METH_INTERNAL_ERROR, 0, env_info);
    }
    meth_infop = &meth_info[app_info->method_tag - 1];
    return (meth_infop->meth_resolve_rc);
}


/**********************************************************************
 *
 *  Name: get_dds_error
 *  ShortDesc: Return the DDS error generated by an earlier call, and
 *				optionally return a text string describing the error.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Many builtin functions may end up calling many routines in
 *		the Parameter Cache, connection manager, DDS services (including
 *		fetch and eval routines), and method execution.  Many different
 *		error conditions may be detected from all of these sources.
 *
 *		In order to simplify the writing of methods, builtins are
 *		restricted to returning the relatively small number of
 *		returns defined in builtins.h.  Therefore, many of the error
 *		returns mentioned above are combined into the error return
 *		BLTIN_DDS_ERROR.  If the method wishes to know the error number
 *		(and option description) of the actual error, it calls get_dds_error.
 *
 *  Inputs:
 *		max_length - The length of the optional output buffer.  This
 *					parameter is only meaningful if out_buf is specified.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		out_buf - If this buffer is provided, a text description of the
 *					error is returned.
 *
 *  Returns:
 *		The error code generated by an earlier call.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_dds_error (char *out_buf, long max_length, ENV_INFO2 *env_info)
{

    int				errcode;
    APP_INFO		*app_info;
    char			*desc_string;

    app_info = (APP_INFO *)env_info->app_info;
    errcode = app_info->env_returns.dds_return_code;

    LOGC_INFO(CPM_FF_DDSERVICE, "get_dds_error: param2: max_length = %ld\n", max_length);

    /*
     *	If a buffer was supplied, get the description string for
     *	the error.
     */

    if (out_buf != NULL) {
        desc_string = dds_error_string (errcode);
        (void)strncpy (out_buf, desc_string, (size_t) max_length);
    }
    return (errcode);
}


/*
 *	Pre/post Read/write builtins
 */


/**********************************************************************
 *
 *  Name: get_float
 *  ShortDesc: Get the current value of the floating point param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_float retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_float (float *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a float.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_FLOAT) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.f;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_float
 *  ShortDesc: Store the new value of the floating point param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_float stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_float (double value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_float: param1: value = %f\n", value);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_FLOAT;
        param_value.val.f = (float)value;
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_double
 *  ShortDesc: Get the current value of the double param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_double retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_double (double *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a double.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_DOUBLE) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.d;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_double
 *  ShortDesc: Store the new value of the double param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_double stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_double (double value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_double: param1: value = %f\n", value);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_DOUBLE;
        param_value.val.d = value;
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_signed
 *  ShortDesc: Get the current value of the integer param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_signed retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_signed (long *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is an integer.  If so, return it.
     *	If not, return an error.
     */

    if (param_value.type != DDS_INTEGER) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.i;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_signed
 *  ShortDesc: Store the new value of the signed integer param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_signed stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *		value - The new value of the param.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_signed (long value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_signed: param1: value = %ld\n", value);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_INTEGER;
        param_value.val.i = value;
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_unsigned
 *  ShortDesc: Get the current value of the unsigned integer param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_unsigned retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_unsigned (unsigned long *valuep, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    ITEM_TYPE		type;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is an unsigned value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;
    if (  type != DDS_UNSIGNED
       && type != DDS_INDEX
       && type != DDS_ENUMERATED
       && type != DDS_BIT_ENUMERATED
       ) {
        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    *valuep = param_value.val.u;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_unsigned
 *  ShortDesc: Store the new value of the unsigned integer param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_unsigned stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		value - The new value of the param.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_unsigned (unsigned long value, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_unsigned: param1: value = %lu\n", value);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_UNSIGNED;
        param_value.val.u = value;
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_string
 *  ShortDesc: Get the current value of the string param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_string retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		lengthp - The maximum length of the string.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		valuep - The returned value of the param.
 *		lengthp - The returned length of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_string (char *valuep, long *lengthp, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    ITEM_TYPE		type;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a string value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;
    if (type != DDS_ASCII && type != DDS_PASSWORD
        && type != DDS_BITSTRING && type != DDS_OCTETSTRING
        && type != DDS_VISIBLESTRING && type != DDS_EUC) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }

    /*
     *	Return as much of the string as possible.  If the supplied
     *	buffer is too small, also return an error.
     */

    if (*lengthp < param_value.val.s.len) {
        param_value.val.s.len = (unsigned short)*lengthp;
        rs = bltin_error_filter (BLTIN_BUFFER_TOO_SMALL, 0, env_info);
    } else {
        rs = BLTIN_SUCCESS;
    }
    (void)memcpy (valuep, param_value.val.s.str, (int)param_value.val.s.len);
    *lengthp = param_value.val.s.len;
    /* This will free the string if it was allocated */
    bltin_free_string (&param_value.val.s);
    return (rs);
}


/**********************************************************************
 *
 *  Name: put_string
 *  ShortDesc: Store the new value of the string param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_string stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		value - The new value of the param.
 *		length - The length of the new value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_string (char *valuep, long length, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_string: param1: valuep = %s\n", valuep);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_string: param2: length = %lu\n", length);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_ASCII;
        param_value.val.s.str = valuep;
        param_value.val.s.len = (unsigned short)length;
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}


/**********************************************************************
 *
 *  Name: get_date
 *  ShortDesc: Get the current value of the date param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Get_date retrieves the current value of the specified
 *		param, and returns it.  This is a special purpose
 *		routine that is only intended to be used when scaling or
 *		de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		datap - The returned value of the param.
 *		lengthp - The returned length of the param.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
get_date (char *datap, long *lengthp, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;
    ITEM_TYPE		type;

    /*
     *	Get the value from the Parameter Cache.
     */

    do {
        rs = pc_get_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    if (rs != BLTIN_SUCCESS) {
        return (rs);
    }

    /*
     *	Make sure the param is a date value.  If so, return it.
     *	If not, return an error.
     */

    type = param_value.type;
    if (type != DDS_DATE_AND_TIME && type != DDS_TIME
        && type != DDS_DURATION && type != DDS_TIME_VALUE) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    (void)memcpy (datap, param_value.val.ca, (int)param_value.size);
    *lengthp = param_value.size;
    return (BLTIN_SUCCESS);
}


/**********************************************************************
 *
 *  Name: put_date
 *  ShortDesc: Store the new value of the date param
 *				that is being scaled or de-scaled.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Put_date stores the new value of the specified param.  This is
 *		a special purpose routine that is only intended to be used when
 *		scaling or de-scaling a param in a post-read or pre-write method.
 *
 *  Inputs:
 *		value - The new value of the param.
 *		length - The length of the new value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The param's value is updated.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

long
put_date (char *datap, long length, ENV_INFO2 *env_info)
{

    int				rs;
    EVAL_VAR_VALUE	param_value;

    LOGC_INFO(CPM_FF_DDSERVICE, "put_date: param1: datap = %s\n", datap);
    LOGC_INFO(CPM_FF_DDSERVICE, "put_date: param2: length = %ld\n", length);

    /*
     *	Put the value into the Parameter Cache.
     */

    do {
        param_value.type = DDS_DATE_AND_TIME;
        param_value.size = (unsigned short) length;
        (void)memcpy (param_value.val.ca, datap, (int)length);
        rs = pc_put_action_value ((ENV_INFO *)env_info, &param_value);
        rs = bltin_error_filter (rs, 1, env_info);
    } while (rs == METH_METHOD_RETRY);
    return (rs);
}

int globalMethodAbort=0;

/* For methods that are Tokenized with debug information, each statement
 * that is allowed to contain a breakpoint will be preceded by a call
 * to this function.  The original source file's line number is passed
 * in.
 */
long _l(unsigned long source_line, void *env_info)
{
#ifdef DDIDE
    int x;
    ENVIRON *e=(ENVIRON *)env_info;
    APP_INFO *app_info = (APP_INFO *)((e->env_info)->app_info);

    ENVIRON *env;
    SCOPE   *scope;
    int ret=0;

    if( (globalDebugCallback != 0) && (globalBrkPnts !=0 ) && numBrkPnts > 0 )
    {
        for(x=0;x<numBrkPnts;x++)
        {
            if( globalBrkPnts[x].line == source_line && globalBrkPnts[x].enabled )
            {
                if( globalBrkPnts[x].brk_type == BRK_TYPE_SINGLE )
                    globalBrkPnts[x].enabled = 0;

                env = &meth_info[app_info->method_tag - 1].meth_environ;
                scope = env ? env->cur_scope : 0;

                if(e)
                    scope = e->cur_scope;

                ret=(*globalDebugCallback)( scope, globalBrkPnts[x].decookie,source_line );
                if(ret)
                    globalMethodAbort=1;

                return 0;
            } else if( globalBrkPnts[x].brk_type == BRK_TYPE_STEP && globalBrkPnts[x].enabled && globalDebugCallback != 0)
            {
                globalBrkPnts[x].enabled = 0;
                globalBrkPnts[x].line=source_line;
                env = &meth_info[app_info->method_tag - 1].meth_environ;
                scope = env ? env->cur_scope : 0;

                if(e)
                    scope = e->cur_scope;

                ret=(*globalDebugCallback)( scope, globalBrkPnts[x].decookie,source_line );
                if(ret)
                    globalMethodAbort=1;
            }
        }
    }
#else
    /* Unused parameters */
    (void)source_line;
    (void)env_info;
#endif
    return 0;
}

/**********************************************************************
 *
 *  Name: bi_rcsim_set_response_code
 *  ShortDesc: Set simulator response code value.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Set the simulator response code.  After this call is made, subsequent calls
 *		requiring communication with the simulated network will return this response
 *		code.  Response code class RCSIM_TYPE_RESPONSE_CODE is used (see rcsim.h).
 *
 *  Inputs:
 *		network_handle - Network handle.  Network handles can be determined by referring
 *						 to the network.dat file and the sequence in which the networks
 *						 are listed in that file.  The first network list would have a
 *						 network handle of 0, the second would have a network handle of 1,
 *						 and so on.
 *		device_stn_address - Device Station Address.  Refer to the device simulator data
 *							 file as listed in network.dat.  See the simulator data file
 *							 (example: a_sim02.dat) for the station address of the block
 *							 you want to use.
 *		response_code - The new respone code value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Don Marquart
 *
 *********************************************************************/

long
bi_rcsim_set_response_code(int network_handle, int device_stn_address,
    UINT32 response_code, ENV_INFO2 *env_info )
{

    int				rs;
    int				rcsim_class;
    RCSIM_VALUE		rcsim_value;
    RCSIM_PARAM		rcsim_param;


#ifdef DDSTEST
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_response_code: param1: network_handle = %d\n",
                network_handle);
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_response_code: param1: device_stn_address = %d\n",
                device_stn_address);
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_response_code: param3: response_code = %ld\n",
                response_code);
#endif /* DDSTEST */

    rcsim_class = RCSIM_TYPE_RESPONSE_CODE;

    rcsim_param.network_handle = network_handle;
    rcsim_param.network_type = NT_A_SIM;
    rcsim_param.device_station_address = device_stn_address;
    rcsim_value.type = DDS_UNSIGNED;
    rcsim_value.size = sizeof(unsigned long);
    rcsim_value.value = (void*)&response_code;

    rs = rcsim_set(rcsim_class, &rcsim_param, &rcsim_value);
    rs = bltin_error_filter(rs, 0, env_info);

    return (rs);
}


/**********************************************************************
 *
 *  Name: bi_rcsim_set_comm_err
 *  ShortDesc: Set simulator comm error value.
 *
 *  Include:
 *		ddsbltin.h
 *
 *  Description:
 *
 *		Set the simulator comm error.  After this call is made, subsequent calls
 *		requiring communication with the simulated network will return this comm error.
 *		Response code class RCSIM_TYPE_COMM_ERR is used (see rcsim.h).
 *
 *  Inputs:
 *		network_handle - Network handle.  Network handles can be determined by referring
 *						 to the network.dat file and the sequence in which the networks
 *						 are listed in that file.  The first network listed would have a
 *						 network handle of 0, the second would have a network handle of 1,
 *						 and so on.
 *		device_stn_address - Device Station Address.  Refer to the device simulator data
 *							 file as listed in network.dat.  See the simulator data file
 *							 (example: a_sim02.dat) for the station address of the block
 *							 you want to use.
 *		comm_err - The new comm error value.
 *		env_info - The ENV_INFO2 structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		none
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h
 *
 *  Author:
 *		Don Marquart
 *
 *********************************************************************/

long
bi_rcsim_set_comm_err(int network_handle, int device_stn_address, UINT32 comm_err, ENV_INFO2 *env_info )
{

    int				rs;
    int				rcsim_class;
    RCSIM_VALUE		rcsim_value;
    RCSIM_PARAM		rcsim_param;

#ifdef DDSTEST
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_comm_err: param1: network_handle = %d\n",
                network_handle);
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_comm_err: param1: device_stn_address = %d\n",
                device_stn_address);
    LOGC_INFO(CPM_FF_DDSERVICE, "bi_rcsim_set_comm_err: param3: comm_err = %ld\n",
                comm_err);
#endif /* DDSTEST */

    rcsim_class = RCSIM_TYPE_COMM_ERR;
    rcsim_param.network_handle = network_handle;
    rcsim_param.network_type = NT_A_SIM;
    rcsim_param.device_station_address = device_stn_address;
    rcsim_value.type = DDS_UNSIGNED;
    rcsim_value.size = sizeof(unsigned long);
    rcsim_value.value = (void*)&comm_err;

    rs = rcsim_set(rcsim_class, &rcsim_param, &rcsim_value);
    rs = bltin_error_filter(rs, 0, env_info);

    return (rs);
}

/* Get a date-format portion of a list element */
long get_date_lelem(ITEM_ID list_id, ITEM_ID list_index, ITEM_ID element_id, ITEM_ID subelement_id,
                    char *datap, long *lengthp, ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE  param_value;
    long rs;
    int type;

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    type = param_value.type;
    if (type != DDS_DATE_AND_TIME && type != DDS_TIME
        && type != DDS_TIME_VALUE && type != DDS_DURATION) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    (void)memcpy (datap, param_value.val.ca, (int)param_value.size);
    *lengthp = param_value.size;
    return (BLTIN_SUCCESS);
}

/* Get a date-format portion of a list element in an embedded list */
long get_date_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                    ITEM_ID embedded_list_id, ITEM_ID embedded_list_index, ITEM_ID element_id, ITEM_ID subelement_id,
                    char *datap, long *lengthp, ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE  param_value;
    long rs;
    int type;

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    type = param_value.type;
    if (type != DDS_DATE_AND_TIME && type != DDS_TIME
        && type != DDS_TIME_VALUE && type != DDS_DURATION) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }
    (void)memcpy (datap, param_value.val.ca, (int)param_value.size);
    *lengthp = param_value.size;
    return (BLTIN_SUCCESS);
}

/* Get a string-format portion of a list element */
long get_string_lelem(ITEM_ID list_id, ITEM_ID list_index, ITEM_ID element_id, ITEM_ID subelement_id,
                    char *valuep, long *lengthp, ENV_INFO2 *env_info)
{

    EVAL_VAR_VALUE	param_value;
    long rs;
    int type;

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    type = param_value.type;
    if (type != DDS_ASCII && type != DDS_PASSWORD
        && type != DDS_BITSTRING && type != DDS_OCTETSTRING
        && type != DDS_VISIBLESTRING && type != DDS_EUC) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }

    /*
     *	Return as much of the string as possible.  If the supplied
     *	buffer is too small, also return an error.
     */

    if (*lengthp < param_value.val.s.len) {
        param_value.val.s.len = (unsigned short)*lengthp;
        rs = bltin_error_filter (BLTIN_BUFFER_TOO_SMALL, 0, env_info);
    } else {
        rs = BLTIN_SUCCESS;
    }
    (void)memcpy (valuep, param_value.val.s.str, (int)param_value.val.s.len);
    *lengthp = param_value.val.s.len;
    /* This will free the string if it was allocated */
    bltin_free_string (&param_value.val.s);
    return (rs);
}

/* Get a string-format portion of a list element in an embedded list */
long get_string_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                       ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                       ITEM_ID element_id, ITEM_ID subelement_id,
                    char *valuep, long *lengthp, ENV_INFO2 *env_info)
{

    EVAL_VAR_VALUE	param_value;
    long rs;
    int type;

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    type = param_value.type;
    if (type != DDS_ASCII && type != DDS_PASSWORD
        && type != DDS_BITSTRING && type != DDS_OCTETSTRING
        && type != DDS_VISIBLESTRING && type != DDS_EUC) {

        rs = bltin_error_filter (BLTIN_WRONG_DATA_TYPE, 0, env_info);
        return (rs);
    }

    /*
     *	Return as much of the string as possible.  If the supplied
     *	buffer is too small, also return an error.
     */

    if (*lengthp < param_value.val.s.len) {
        param_value.val.s.len = (unsigned short)*lengthp;
        rs = bltin_error_filter (BLTIN_BUFFER_TOO_SMALL, 0, env_info);
    } else {
        rs = BLTIN_SUCCESS;
    }
    (void)memcpy (valuep, param_value.val.s.str, (int)param_value.val.s.len);
    *lengthp = param_value.val.s.len;
    /* This will free the string if it was allocated */
    bltin_free_string (&param_value.val.s);
    return (rs);
}

void list_ref_error(ITEM_ID list_id, ITEM_ID element_id, ENV_INFO2 *env_info)
{
    char buf[255];
    sprintf(buf, LIST_INVALID_REF, list_id, element_id);
    method_abort(buf, env_info);
}

float get_float_lelem(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.f;
}

float get_float_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                       ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.f;
}

double get_double_lelem(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.d;
}

double get_double_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.d;
}


long get_signed_lelem(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.i;
}

long get_signed_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.i;
}


unsigned long get_unsigned_lelem(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.u;
}

unsigned long get_unsigned_lelem2(ITEM_ID list_id, ITEM_ID list_index,
                             ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id,
                             ENV_INFO2 *env_info)
{
    EVAL_VAR_VALUE	param_value;
    long rs;

    if ((!list_id) || (!element_id))
        list_ref_error(list_id, element_id, env_info);

    rs = (*list_read_element2_upcall)((ENV_INFO *)env_info, list_id, list_index,
                                     embedded_list_id, embedded_list_index,
                                     element_id, subelement_id,
                                     &param_value);
    if (rs)
        method_abort (LIST_READ_ERROR, env_info);

    return param_value.val.u;
}

/* 5.1 Builtins */
long   assign2(unsigned long   dstBlockId,
               unsigned long   dstBlockInstance,
               unsigned long   dstParamId,
               unsigned long   dstMemberId,
               unsigned long   srcBlockId,
               unsigned long   srcBlockInstance,
               unsigned long   srcParamId,
               unsigned long   srcMemberId, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE dstBH, srcBH;

    if (!app_func_get_block_handle)
        return -1;

    /* Find the block handle for the passed-in blocks */
    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, dstBlockId, dstBlockInstance, &dstBH);
    if (rs) return BLTIN_BLOCK_NOT_FOUND;
    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, srcBlockId, srcBlockInstance, &srcBH);
    if (rs) return BLTIN_BLOCK_NOT_FOUND;

    return bltin_assign( dstParamId, dstMemberId, srcParamId, srcMemberId, dstBH, srcBH, TRUE, env_info);

}

long   delayfor2(long            duration,
                 char           *prompt,
                 unsigned long   blockIds[],
                 unsigned long   blockInstances[],
                 unsigned long   paramIds[],
                 unsigned long   memberIds[],
                 long            count, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_delayfor (duration, prompt, paramIds, memberIds,
                           count, bhs, TRUE, env_info);
}

long   display_dynamics2(char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_display_dynamics (prompt, paramIds, memberIds,
                                   count, bhs, TRUE, env_info);
}

long   display_message2(char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_display_message (prompt, paramIds, memberIds, count,
                                  bhs, TRUE, env_info);
}

long   edit_device_value2(char           *prompt,
                          unsigned long   blockIds[],
                          unsigned long   blockInstances[],
                          unsigned long   paramIds[],
                          unsigned long   memberIds[],
                          long            count,
                          unsigned long   blockId,
                          unsigned long   blockInstance,
                          unsigned long   paramId,
                          unsigned long   memberId, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS],bh;
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockId, blockInstance, &bh);
    if (rs)
        return BLTIN_BLOCK_NOT_FOUND;

    return bltin_edit_device_value (prompt, paramIds, memberIds,
                                    count, paramId, memberId, bh,
                                    bhs, TRUE, env_info);
}

long   edit_local_value2(char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count,
                         char           *localVariable, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_edit_local_value (prompt, paramIds, memberIds,
                             count, localVariable, bhs, TRUE, env_info);
}

long   get_acknowledgement2(char           *prompt,
                            unsigned long   blockIds[],
                            unsigned long   blockInstances[],
                            unsigned long   paramIds[],
                            unsigned long   memberIds[],
                            long            count, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_get_acknowledgement2 (prompt, paramIds, memberIds,
                                       count, bhs, TRUE, env_info);

}

long   get_block_instance_by_object_index(unsigned long   objectIndex,
                                          unsigned long  *instance, ENV_INFO2 *env_info)
{
    if (!app_get_block_instance_by_index)
        return -1;

    return (*app_get_block_instance_by_index)((ENV_INFO *)env_info, objectIndex, instance);
}

long   get_block_instance_by_tag(ITEM_ID   blockId,
                                 char           *blockTag,
                                 unsigned long  *instance, ENV_INFO2 *env_info)
{
    if (!app_get_block_instance_by_tag)
        return -1;

    return (*app_get_block_instance_by_tag)((ENV_INFO *)env_info, blockId, blockTag, instance);
}

long   get_block_instance_count(ITEM_ID   blockId,
                                unsigned long  *count, ENV_INFO2 *env_info)
{
    if (!app_get_block_instance_count)
        return -1;

    return (*app_get_block_instance_count)((ENV_INFO *)env_info, blockId, count);
}

long   get_date_value2(unsigned long   blockId,
                       unsigned long   blockInstance,
                       unsigned long   paramId,
                       unsigned long   memberId,
                       char           *value,
                       long           *length, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = get_date_value( paramId, memberId, value, length, &env_info_block);

    return rs;
}

long   get_double_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         double         *value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = get_double_value( paramId, memberId, value, &env_info_block);

    return rs;
}

long   get_float_value2(unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        float          *value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = get_float_value( paramId, memberId, value, &env_info_block);
    return rs;
}

long   get_signed_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         long           *value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;


    rs = get_signed_value( paramId, memberId, value, &env_info_block);

    return rs;
}

long   get_string_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         char           *value,
                         long           *length, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;


    rs = get_string_value( paramId, memberId, value, length, &env_info_block);

    return rs;
}

long   get_unsigned_value2(unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           unsigned long  *value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = get_unsigned_value( paramId, memberId, value, &env_info_block);

    return rs;
}


double   ret_double_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         ENV_INFO2 *env_info)
{
    double	d = 0.0;
    get_double_value2( blockId, blockInstance, paramId, memberId, &d, env_info);

    return d;
}

float   ret_float_value2(unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        ENV_INFO2 *env_info)
{
    float	f = 0.0;
    get_float_value2( blockId, blockInstance, paramId, memberId, &f, env_info);

    return f;
}

long   ret_signed_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         ENV_INFO2 *env_info)
{
    long	l;
    get_signed_value2( blockId, blockInstance, paramId, memberId, &l, env_info);

    return l;
}

unsigned long   ret_unsigned_value2(unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           ENV_INFO2 *env_info)
{
    unsigned long	ul;
    get_unsigned_value2( blockId, blockInstance, paramId, memberId, &ul, env_info);

    return ul;
}


long   put_date_value2(unsigned long   blockId,
                       unsigned long   blockInstance,
                       unsigned long   paramId,
                       unsigned long   memberId,
                       char           *value,
                       long            length, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_date_value( paramId, memberId, value, length, &env_info_block);

    return rs;
}

long   put_double_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         double          value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_double_value( paramId, memberId, value, &env_info_block);

    return rs;
}


long   put_float_value2(unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        double          value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_float_value( paramId, memberId, value, &env_info_block);

    return rs;
}

long   put_signed_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         long            value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_signed_value( paramId, memberId, value, &env_info_block);

    return rs;
}

long   put_string_value2(unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         char           *value,
                         long            length, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_string_value( paramId, memberId, value, length, &env_info_block);

    return rs;
}

long   put_unsigned_value2(unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           unsigned long   value, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = put_unsigned_value( paramId, memberId, value, &env_info_block);

    return rs;
}

long   read_value2(unsigned long   blockId,
                   unsigned long   blockInstance,
                   unsigned long   paramId,
                   unsigned long   memberId, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = read_value( paramId, memberId, &env_info_block);

    return rs;
}

long   select_from_menu2(char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count,
                         char           *options,
                         long           *selection, ENV_INFO2 *env_info)
{
    long rs;
    BLOCK_HANDLE	bhs[MAX_PROMPT_MSG_PARAMS];
    int i;

    if (!app_func_get_block_handle)
    return -1;

    for(i=0;i<count;i++)
    {
        /* get block_handles */
        rs = (*app_func_get_block_handle)((ENV_INFO *)env_info, blockIds[i], blockInstances[i], &bhs[i]);
        if (rs)
            return BLTIN_BLOCK_NOT_FOUND;
    }

    return bltin_select_from_menu2 (prompt, paramIds, memberIds,
                                   count, options, selection, bhs, TRUE, env_info);
}

long   send_value2(unsigned long   blockId,
                   unsigned long   blockInstance,
                   unsigned long   paramId,
                   unsigned long   memberId, ENV_INFO2 *env_info)
{
    long rs;
    ENV_INFO2 env_info_block;

    rs = env2_to_env2(env_info, &env_info_block, blockId, blockInstance);
    if (rs)
        return -1;

    rs = send_value( paramId, memberId, &env_info_block);

    return rs;
}

/* end 5.1 additions */

static void bltin_free_string(STRING *dds_str)
{
    if (dds_str->flags == FREE_STRING)
    {
        free (dds_str->str);
    }
    if (dds_str->wflags == FREE_STRING)
    {
        free (dds_str->wstr);
    }
}

static void mi_handle_suppression(ENV_INFO2 *env_info, char *out_buf)
{
    char			*suppression_str;

    /* Start by clearing out the suppression flag */
    ((APP_INFO *)env_info->app_info)->status_info &= ~MI_SUPPRESS;
    acquire_suppression_str(env_info, &suppression_str);
    if (suppression_str)
    {
        STRING tmp_dds_str;
        memset(&tmp_dds_str, 0, sizeof(STRING));
        tmp_dds_str.str = out_buf;
        tmp_dds_str.len = (unsigned short)strlen(out_buf);
        if (enum_check_for_suppression(&tmp_dds_str, suppression_str))
        {
            /* This string needs to be suppressed.  Tell the host through a flag bit. */
            ((APP_INFO *)env_info->app_info)->status_info |= MI_SUPPRESS;
        }
        /* Remove any unused suppression flags from the string */
        remove_all_suppression_flags(&tmp_dds_str);
    }
}
