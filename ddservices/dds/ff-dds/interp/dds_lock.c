/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	dds_lock.c - The locking mechanism provided is a simple example.
 *		  	 The example code should be replaced by operating system 
 *			 calls (for light weight processes) if multi threaded 
 *			 applications are used.
 ********************************************************************/

#include "int_inc.h"
#include "unistd.h"

/*
 * Static data used to maintain the locks
 */
static int	locks[MAX_LOCKS] = {UNLOCKED, UNLOCKED, UNLOCKED};

/***********************************************************************
 *
 *  Name:  unlock
 *  ShortDesc:  Unlock a process.
 *
 *  Description:
 *
 *		A lock identifier is specified.  The specified lock
 *		is then set to unlocked.
 *
 *  Inputs:
 *		lock_id -	The unique identifier of a lock.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		None
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

void
unlock(int lock_id)
{
	ASSERT_DBG(lock_id >= 0 && lock_id < MAX_LOCKS) ;
	locks[lock_id] = UNLOCKED ;
	return ;
}


/***********************************************************************
 *
 *  Name:  islocked
 *  ShortDesc:  Return the state of a lock 
 *
 *  Description:
 *
 *		A lock identifier is specified.  The specified lock
 *		is then checked to see if it is locked.
 *
 *  Inputs:
 *		lock_id -	The unique identifier of a lock.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		zero if the lock is not locked.  
 *		Non-zero if the lock is locked.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
islocked(int lock_id)
{
	ASSERT_DBG(lock_id >= 0 && lock_id < MAX_LOCKS) ;
	return ((locks[lock_id] == LOCKED)) ;
}


/***********************************************************************
 *
 *  Name:  waitfor
 *  ShortDesc:  Wait for a lock to unlock.
 *
 *  Description:
 *
 *		A lock identifier is specified.  The specified lock is checked
 *		to see if is unlocked yet.  If not, it sleeps for awhile and 
 *		checks until the specified lock is unlocked.
 *
 *  Inputs:
 *		lock_id -	The unique identifier of a lock.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		None
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static void
waitfor(int lock_id)
{
	ASSERT_DBG(lock_id >= 0 && lock_id < MAX_LOCKS) ;
	while (islocked(lock_id)) {

		(void) sleep(1) ;

	}
	return ;
}


/***********************************************************************
 *
 *  Name:  lock
 *  ShortDesc:  Lock a process.
 *
 *  Description:
 *
 *		A lock identifier is specified. If the specified lock is unlocked,
 *		then set to locked. Otherwise, waits for the specified lock is 
 *		unlocked then set to locked.
 *
 *  Inputs:
 *		lock_id -	The unique identifier of a lock.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		None
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

void
lock(int lock_id)
{
	ASSERT_DBG(lock_id >= 0 && lock_id < MAX_LOCKS) ;

	if (islocked(lock_id)) {
		waitfor(lock_id) ;
	}

	locks[lock_id] = LOCKED ;
	return ;
}
