
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      int_exec.c - interpret the tree
 ******************************************************/

#include    "int_inc.h"
#include    "flk_log4cplus_clogger.h"
#include    "flk_log4cplus_defs.h"
#include    "device_access.h"

/*
 * Macro to check if searching for case of switch statement.
 */ 
#define FIND_CASE	((ENV_SWITCH != NULL) && (ENV_SWITCH->flag == FALSE))

/*
 * Externals.
 */
extern int globalMethodAbort;

static int exec_stmt_lst P((STATEMENT *, ENVIRON *));

/*********************************************************************
 *
 *	Name: add_switch()
 *	ShortDesc: Allocates switch-scope and adds to the environment.
 *
 *	Description:
 *		Add_switch Allocates memory from the environment heap for storing 
 * 		information to be used during the execution of switch statement. 
 *		Attach switch-scope to the environment so that other functions 
 *		can fetch and use during the execution.
 *
 *	Inputs:
 * 		env_ptr - the environment pointer.
 *
 *	Outputs:
 *		env_ptr - the environment pointer.
 *
 *	Returns:
 *		nothing.
 *
 *	Author:
 *		Mike Dieter/Bac Truong.
 *
 *********************************************************************/
void
add_switch(ENVIRON *env_ptr)
{
	SWITCH_SCOPE	*new_swi;

	/*
	 * Allocate memory and set switch-scope to default values.
	 * Attach the switch-scope to the environment.
	 */
	new_swi = (SWITCH_SCOPE *) imalloc_chk(env_ptr, sizeof(SWITCH_SCOPE));
	new_swi->case_exp = (long *)imalloc_chk(env_ptr, sizeof(long));

	new_swi->size = 0;
	new_swi->flag = FALSE;
	new_swi->next_switch = ENV_SWITCH;

	ENV_SWITCH = new_swi;
}


/*********************************************************************
 *
 *	Name: del_switch()
 *	ShortDesc: Free memory of switch-scope from the environment.
 *
 *	Description:
 *		Del_switch Frees the switch-scope memory from the environment
 *		heap.
 *
 *	Inputs:
 * 		env_ptr - the environment pointer.
 *
 *	Outputs:
 *		The switch-scope from the environment was freed.
 *
 *	Returns:
 *		nothing.
 *
 *	Author:
 *		Mike Dieter/Bac Truong.
 *
 *********************************************************************/
void
del_switch(ENVIRON *env_ptr)
{
	SWITCH_SCOPE	*old_swi;			/* Switch-scope to be freed */ 

	old_swi = ENV_SWITCH;

	ENV_SWITCH = ENV_SWITCH->next_switch;

	rfree(env_ptr->heap_ptr, (char *) old_swi->case_exp);
	rfree(env_ptr->heap_ptr, (char *) old_swi);
}


/*********************************************************************
 *
 *	Name: case_const()
 *	ShortDesc: Gets the value of a case expression.
 *
 *	Description:
 *		Case_const gets the case expression value and casts to type
 *		long.
 *
 *	Inputs:
 *		con_exp - Expression contained case value.
 *		env_ptr - Environment ptr to be passed into ADJ_CAST.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		Value of case expression.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
long
case_const(EXPRESS *con_exp, ENVIRON *env_ptr)
{
	VALUE 		con_val;				/* The case expression value */
	TYPE_SPEC	*con_type;				/* Type of expression var */

	/*
	 * Get value from expression and cast to type long.
	 */
	con_type = con_exp->type;
	get_val(&con_exp->misc.con_v, con_type, &con_val, env_ptr);

	ADJ_CAST(&con_val, con_type, METH_LONG_TYPE, env_ptr);

	return (con_val.l);
}


/*********************************************************************
 *
 *	Name: exec_stmt()
 *	ShortDesc: Execute the method statement.
 *
 *	Description:
 *		Exec_stmt sets up the environment and then execute the method
 *		statement. During the search for a case in switch statement, 
 *		statement will not be executed, but instead skipping over to 
 *		the next statment.
 *
 *	Inputs:
 *		cur_stmt - the statement may be executed.
 *		env_ptr  - the environment for using during the execution.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		BREAK     - Break statement was executed.
 *		CONTINUE  - Continue statement was executed. 
 *		RETURN	  - Return statement was executed.
 *		NULL_STMT - Statement was successfully executed.
 *
 *	Author:
 *		Mike Dieter/Bac Truong.
 *
 *********************************************************************/
int
exec_stmt(STATEMENT *cur_stmt, ENVIRON *env_ptr)
{
	VALUE 			val = {0};				/* result value of condition */			
	TYPE_SPEC 		*type = NULL;				/* result type of condition */
	int ret_val = 	NULL_STMT;			/* return value after execut */

#ifdef lint
	type = (TYPE_SPEC *)0;
	val.l = 0;
#endif

	if (cur_stmt == (STATEMENT *)0) {
		return ret_val;
	}

	if (SUCCESS == (*getConnectionState)())
	{
		method_abort("Abort has been called by host", env_ptr->env_info);
		return ret_val;
	}

	switch (cur_stmt->stmt_type) {
	case COMPOUND:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = COMPOUND\n" );

		/* 
		 * If there are variables within compound statement,
		 * attach them to a current scope of the environment. 
		 * During the execution of the compound statement,
		 * these variables will be fetched and modified by 
		 * the statement execution.
		 */   
		if (cur_stmt->misc.new_vars != (SYM *)0) {
			add_scope(cur_stmt->misc.new_vars, env_ptr);
		}

		/*
		 * Call to execute a list of statements.
		 */
		ret_val = exec_stmt_lst(cur_stmt->stmt, env_ptr);

		if(globalMethodAbort)	{
			method_abort("Abort has been called by host", env_ptr->env_info);
		}
		else	{
			/*
			 * If there are variables within the compound statement then
 			 * they have been attached to the current scope for execution.
			 * These variables have to to be removed from the current scope.  
			 */ 
			if (cur_stmt->misc.new_vars != (SYM *)0) {
				del_scope(env_ptr);
			}
		}
		break;

	case EXPRESSION:
                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = EXPRESSION\n" );

		/* 
		 * If not in the process of finding a case-value that matchs 
		 * a switch-value then evaluate the expression. Otherwise,
		 * just skip this expression.
		 */
		if (!FIND_CASE) {
			do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
		}
		break;

	case RETURN:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = RETURN\n" );

		/* 
		 * If not in the process of finding a case-value that matchs 
		 * a switch-value then execute the return statement. Otherwise,
		 * just skip this statement.
		 */
		if (!FIND_CASE) {
			ASSERT_DBG(cur_stmt->exp1 == NULL);

			/*
			 * default to zero and integer type.
			 */
			env_ptr->return_value.l = 0;
			env_ptr->return_type = LONG_T;
			ret_val = RETURN;
		}
		break;

	case IF:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = IF\n" );

		{
			/*
			 * If this flag is true then  the if-condition 
			 * has been evaluated and the else-part should
			 * be executed if the if-condition is evaluated 
			 * to false.
			 */
			int val_good = FALSE;

			if (!FIND_CASE) {

				/*
				 * If not in the process of searching for the case,
				 * evaluate the if-condition to get the result.
				 * Set the flag to indicate that the if-condition
				 * have been evaluated. This flag can be checked
				 * to determine the execution of the else-part. 
				 */ 
				do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
				val_good = TRUE;
			}

			if (FIND_CASE || LOGICAL(val, type->typ)) {

				/*
				 * 1) FIND_CASE == TRUE.
				 *  If searching for case within the switch statement, 
				 *  call exec_stmt() for skipping through the compound 
				 *  statement of the if-part.
				 *
				 * 2) LOGICAL(val, type->typ) == TRUE.
				 *  the condition is evaluated to true, execute
				 * 	the compound statement of the if-part.
				 */
				ret_val = exec_stmt(cur_stmt->stmt, env_ptr);
			}

			if (FIND_CASE || (val_good && !LOGICAL(val, type->typ))) {

				/*
				 * 1) FIND_CASE == TRUE.
				 *  If searching for case within the switch statement, 
				 *  call exec_stmt() for skipping through the compound 
				 *  statement within the else-part.
				 *
				 * 2)(val_good==TRUE) && (LOGICAL(val, type->typ) == FALSE).
				 *  Execute the compound statement of the else-part
				 *  if the flag indicated that the if-condition have
				 *  been evaluated and the result is false.
				 */
				ret_val = exec_stmt(cur_stmt->misc._else, env_ptr);
			}
		}
		break;

	case DO:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = DO\n" );

		do {

			/*
			 * Attempt to execute the compound statement 
			 * within the do-loop.
			 */
			ret_val = exec_stmt(cur_stmt->stmt, env_ptr);

			if (FIND_CASE) {

				/*
				 * If in the process of looking for a case
				 * within a switch, just skip over this loop.
				 * Don't need to check for the do-condition,
				 * just want to skip over statements to locate
				 * the case-value that matchs the switch-value.
				 */
				break;
			}

			if (ret_val == BREAK) {

				/*
				 * The BREAK statement was encountered, 
				 * stop the do-loop execution without 
				 * exiting from the condition of the
				 * do statement.
				 */
				ret_val = NULL_STMT;
				break;
			}
			else if (ret_val == CONTINUE) {
				/*
				 * The CONTINUE statement was encountered,
				 * continue the do-loop from the top and
				 * reset the ret_val to NULL_STMT.
				 */
				ret_val = NULL_STMT;
			}
			else if (ret_val) {

				/*
				 *  Stop executing this loop if RETURN or 
				 *  error was encountered. 
				 */ 
				break;
			}

			/*
			 * Evaluate the do-condition and then check for 
			 * the result. If it is evaluated to true then
			 * continue the execution. Otherwise, stop. 
			 */
			do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
		} while (LOGICAL(val, type->typ));
		break;

	case WHILE:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = WHILE\n" );

		if (!FIND_CASE) {

			/*
			 * If not in the process of searching for the case
			 * then evaluate the condition of while-loop to get 
			 * the result.
			 */ 
			do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
		}

		while (FIND_CASE || LOGICAL(val, type->typ)) {

			/*
			 * 1) FIND_CASE == TRUE.
			 *  If searching for case within the switch statement, 
			 *  call exec_stmt() for skipping through the compound 
			 *  statement within the while-loop.
			 *
			 * 2) LOGICAL(val, type->typ) == TRUE.
			 *  the condition is evaluated to true, execute
			 * 	the compound statement of the while-loop.
			 */
			ret_val = exec_stmt(cur_stmt->stmt, env_ptr);

			if (FIND_CASE) {

				/*
				 * If in the process of looking for a case
				 * within a switch, just skip over this loop.
				 * Don't need to check for the while-condition,
				 * just want to skip over statements to locate
				 * a case that matchs the switch expression.
				 */
				break;
			}

			if (ret_val == BREAK) {

				/*
				 * The BREAK statement was encountered, 
				 * stop the while-loop execution without 
				 * exiting from the while-condition.
				 */
				ret_val = NULL_STMT;
				break;
			}
			else if (ret_val == CONTINUE) {
				/*
				 * The CONTINUE statement was encountered,
				 * continue the while-loop from the top and
				 * reset the ret_val to NULL_STMT.
				 */
				ret_val = NULL_STMT;
			}
			else if (ret_val) {

				/*
				 *  Stop executing this loop if RETURN or 
				 *  error was encountered. 
				 */ 
				break;
			}

			/*
			 * Evaluate the while-condition and then check 
			 * for the result. If it is evaluated to true
			 * then continue the execution. Otherwise, stop. 
			 */
			do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
		}
		break;

	case FOR:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = FOR\n" );

		if (!FIND_CASE) {
			if (cur_stmt->exp1) {
				do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);
			}

			if (cur_stmt->exp2) {
				do_eval_expr(cur_stmt->exp2, &type, env_ptr, &val);
			}
			else {
				val.l = TRUE;
				type = LONG_T;
			}
		}

		while (FIND_CASE || LOGICAL(val, type->typ)) {

			/*
			 * 1) FIND_CASE == TRUE.
			 *  If searching for case within the switch statement, 
			 *  call exec_stmt() for skipping through the compound 
			 *  statement within the for-loop.
			 *
			 * 2) LOGICAL(val, type->typ) == TRUE.
			 *  the condition is evaluated to true, execute
			 * 	the compound statement of the for-loop.
			 */
			ret_val = exec_stmt(cur_stmt->stmt, env_ptr);

			if (FIND_CASE) {

				/*
				 * If in the process of looking for a case
				 * within a switch, just skip over this loop.
				 * Don't need to check for the condition,
				 * just want to  skip over statements to 
				 * locate the case that matchs the switch-
				 * value.
				 */
				break;
			}

			if (ret_val == BREAK) {

				/*
				 * The BREAK statement was encountered, 
				 * stop the for-loop execution without 
				 * exiting from the condition of the for
				 * statement.
				 */
				ret_val = NULL_STMT;
				break;
			}
			else if (ret_val == CONTINUE) {
				/*
				 * The CONTINUE statement was encountered,
				 * continue the for-loop from the top and
				 * reset the ret_val to NULL_STMT.
				 */
				ret_val = NULL_STMT;
			}
			else if (ret_val) {

				/*
				 *  Stop executing this loop if RETURN or 
				 *  error was encountered. 
				 */ 
				break;
			}

			if (cur_stmt->misc.exp3) {
				do_eval_expr(cur_stmt->misc.exp3, &type, env_ptr, &val);
			}
			if (cur_stmt->exp2) {
				do_eval_expr(cur_stmt->exp2, &type, env_ptr, &val);
			}
			else {
				val.l = TRUE;
				type = LONG_T;
			}
		}
		break;

	case SWITCH:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = SWITCH\n" );

		if (!FIND_CASE) {

			/*
			 * If not in the process of searching for the case
			 * then evaluate the condition of the switch statement.
			 */ 
			do_eval_expr(cur_stmt->exp1, &type, env_ptr, &val);

		 	/*
			 * 1)Cast the value in switch statement to long.
			 * 2)Allocate and add SWITCH_SCOPE to the environment. 
			 * 3)Put the value of switch statement to the environment 
			 *   SWITCH_SCOPE for later comparing against the
			 *   case expression.
			 */
			ADJ_CAST(&val, type, METH_LONG_TYPE, env_ptr);
			add_switch(env_ptr);
			(ENV_SWITCH->case_exp)[0] = val.l;

			/*
			 * Execute the compound statement within the switch
			 * statement and then remove the SWITCH_SCOPE from 
			 * the environment.
			 */
			ret_val = exec_stmt(cur_stmt->stmt, env_ptr);

			del_switch(env_ptr);

			if (ret_val == BREAK) {
				ret_val = NULL_STMT;
			}
		}
		break;

	case BREAK:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = BREAK\n" );

		if (!FIND_CASE) {

			/*
			 * If not in the process of searching for the case
			 * then set ret_val to BREAK for returning.
			 */ 
			ret_val = BREAK;
		}
		break;

	case CONTINUE:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = CONTINUE\n" );

		if (!FIND_CASE) {

			/*
			 * If not in the process of searching for the case
			 * then set ret_val to CONTINUE for returning.
			 */ 
			ret_val = CONTINUE;
		}
		break;

	case NULL_STMT:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = NULL_STMT\n" );

		/*
		 * Just return null statement.
		 */ 
		ret_val = NULL_STMT;
		break;

	case DEFAULT:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = DEFAULT\n" );

		ASSERT_DBG(ENV_SWITCH != (SWITCH_SCOPE *)0);

		/*
		 * Set flag to indicate executing the default
		 * of the switch. This will enable execution
		 * of all statements in the default. Otherwise,
		 * statements will not be executed because we
		 * are in searching for the case that matchs
		 * the switch value.
		 */ 
		ENV_SWITCH->flag = TRUE;
		ret_val = exec_stmt(cur_stmt->stmt, env_ptr);
		break;

	case CASE:

                LOGC_INFO(CPM_FF_DDSERVICE,"exec_stmt(), statement type = CASE\n" );

		ASSERT_DBG(ENV_SWITCH != (SWITCH_SCOPE *)0);

		/*
		 * If the case and switch values are equal then 
		 * set the flag to indicate executing mode. 
		 * This will enable execution of all statements 
		 * in this case. Otherwise, statements will not
		 * be executed because we are searching for the
		 * case that matchs the switch value.
		 */ 
		if ((ENV_SWITCH->case_exp)[0] == case_const(cur_stmt->exp1, env_ptr)) {
			ENV_SWITCH->flag = TRUE;
		}
		ret_val = exec_stmt(cur_stmt->stmt, env_ptr);
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in executing statement:", 
												env_ptr->env_info);
	}

	return (ret_val);
}


/*********************************************************************
 *
 *	Name: exec_stmt_lst()
 *	ShortDesc: Execute a list of statements.
 *
 *	Description: 
 *		Exec_stmt_lst loops through a chain of statements and executes
 *		a single statement at a time until the end of list. If the
 *		execution control flow statement(BREAK, CONTINUE, RETURN...) 
 *		is executed, the execution will be stopped and returned to 
 *		the caller. Otherwise, the entire statement list will be 
 *		executed.
 *
 *	Inputs:
 *		cur_stmt - Pointer to beginning of the statement list.
 *		env_ptr  - Environment to be used during execution.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		BREAK     - Break statement was encountered.
 *		CONTINUE  - Continue statement was encountered. 
 *		RETURN	  - Return statement was encountered.
 *		NULL_STMT - Successfully execute the entire statement list. 	
 *		
 *
 *	Author:
 *		Mike Dieter/Bac Truong.
 *
 *********************************************************************/
static int
exec_stmt_lst(STATEMENT *cur_stmt, ENVIRON *env_ptr)
{
	int 		ret_val;	/* Returned value of statement execution */

	while (cur_stmt != (STATEMENT *)0) {

		/*
		 * Execute a single statement and check for returned error. 
		 * The execution will be stopped and returned to the caller 
		 * if error is found in the statement.
		 */
		if(globalMethodAbort)
			return NULL_STMT;

		ret_val = exec_stmt(cur_stmt, env_ptr);
		if (ret_val) {
			return (ret_val);
		}

		/*
		 * Fetch the next statement for execution.
		 */
		cur_stmt = cur_stmt->next_stmt;
	}

	return (NULL_STMT);
}
