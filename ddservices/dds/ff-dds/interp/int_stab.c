
/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      int_stab.c - manipulate the symbol table
 ******************************************************/

#include "int_inc.h"

/*********************************************************************
 *
 *	Name: alloc_scope()
 *	ShortDesc: Allocate and initialize a scope.
 *
 *	Description:
 *		Alloc_scope allocates a scope from the environment heap and
 *		initialize a scope.
 *
 *	Inputs:
 *		stype	- A scope type.
 *		env_ptr - Environment contained memory heap to allocate from. 
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		A allocated and initialized scope.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static SCOPE *
alloc_scope(int stype, ENVIRON *env_ptr)
{
	SCOPE 		*new_scope;			/* Ptr to a new allocated  scope */

	new_scope = (SCOPE *)imalloc_chk(env_ptr, sizeof(SCOPE));

	new_scope->type = stype;
	new_scope->symbols = NULL;
	new_scope->next_scope = NULL;

	return (new_scope);
}


/*********************************************************************
 *
 *	Name: environ_init()
 *	ShortDesc: Allocate and initialize the environment.
 *
 *	Description:
 *		Environ_init initializes an environment. It attachs the provided
 * 		memory heap to the allocated environment. The current and global
 *		scope are also allocated and initialized. 
 *
 *	Inputs:
 *		env  - Environment to initialize.
 *		heap - Heap for attaching to the allocated environment.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong/Jon Westbrock
 *
 *********************************************************************/
void
environ_init(ENVIRON *env, RHEAP *heap)
{				

	/*
	 * Clear the environment.
	 */
	memset((char *) env, 0, sizeof (ENVIRON));

	/*
	 * Attach the heap to the environment.
	 * Allocate scope and attach to current and global
	 * scope of the allocated environment.
	 */
	env->heap_ptr = heap;
	env->global_scope = alloc_scope(SC_OTHER, env);
	env->cur_scope = env->global_scope;
}



/*********************************************************************
 *
 *	Name: cpy_sym()
 *	ShortDesc: Copy symbol.
 *
 *	Description:
 *		Cpy_sym allocates memory from the environment heap and copies 
 *		specified source symbol to the allocated memory. Base on the
 *		specified type of source symbol, copies its value and returns 
 *		the duplicated symbol.
 *
 *	Inputs:
 *		src_sym - Source symbol to be copied from.
 *		stype   - Scope type.
 *		env_ptr - Environment contained heap memory 
 *					to allocate from.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		Duplicated symbol.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
cpy_sym(SYM *src_sym, int stype, ENVIRON *env_ptr)
{
	SYM *target_sym;

	ASSERT_DBG(src_sym != NULL);

	/*
	 * Allocate and copy symbol.
	 */
	target_sym = (SYM *)imalloc_chk(env_ptr, sizeof(SYM));

	*target_sym = *src_sym;
	target_sym->next_sym = NULL;

	switch (target_sym->type->typ) {
	case METH_ARRAY_TYPE:
	case METH_PTR_TYPE:

		/*
		 * don't allocate arrays.
		 */
		if (stype == SC_FUNC) {

			/*
			 * for built-in parameters.
			 */
			break;
		}
		/* FALL THROUGH */
	case METH_CHAR_TYPE:
	case METH_U_CHAR_TYPE:
	case METH_SHORT_TYPE:
	case METH_U_SHORT_TYPE:
	case METH_LONG_TYPE:
	case METH_U_LONG_TYPE:
	case METH_FLOAT_TYPE:
	case METH_DOUBLE_TYPE:

		/*
		 * Copy value sub-structure.
		 */
		bld_val_p(target_sym->type, (size_t)1, env_ptr, &target_sym->v);
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in allocating and copying symbols:", 
												env_ptr->env_info);
	}

	return (target_sym);
}


/*********************************************************************
 *
 *	Name: attach_scope()
 *	ShortDesc: attaches the scope to the scope list.
 *
 *	Description:
 *		Attach_scope attaches the provided scope to the beginning of
 *		the scope list in the environment.
 *
 *	Inputs:
 *		new_scope - The scope to be attached to list.
 *		env_ptr   - Environment contained the scope list to be used
 *					for attaching.
 *
 *	Outputs:
 *		One more scope was added to the scope list.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static void
attach_scope(SCOPE *new_scope, ENVIRON *env_ptr)
{
	/*
	 * Attach to the beginning of the list.
	 */
	new_scope->next_scope = ENV_SCOPE;

	ENV_SCOPE = new_scope;
}


/*********************************************************************
 *
 *	Name: add_scope()
 *	ShortDesc: Add a new scope to the scope list.
 *
 *	Description:
 *		Add_scope allocates a scope and makes a copy of the provided 
 *		symbol list then attachs this list to the allocated scope. 
 * 		Finally, adds this scope to the scope list.
 *
 *	Inputs:
 *		sym_list - A symbol list to be duplicated and attach to scope.
 *		env_ptr  - Environment contained scope list for adding to.
 *
 *	Outputs:
 *		Additional scope was attached to the scope list.
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
add_scope(SYM *sym_list, ENVIRON *env_ptr)
{
	SYM 		*last_sym;					/* Symbol trailing ptr */
	SYM 		*target_sym;				/* Duplicated symbol */
	SCOPE 		*target_scope;				/* New allocated scope */

	last_sym = NULL;						/* lint kludge */

	/*
	 * Allocate scope from environment heap.
	 */
	target_scope = alloc_scope(SC_OTHER, env_ptr);

	/*
	 * Go through the symbol-list and make copy of the symbol
	 * list then attach this list to the new allocated scope.
	 */
	while (sym_list) {

		/*
		 * Make a copy of symbol.
		 */
		target_sym = cpy_sym(sym_list, SC_OTHER, env_ptr);

		if (target_scope->symbols == NULL) {

			/*
			 * Attach the beginning of the symbol-list
			 * to scope.
			 */
			target_scope->symbols = target_sym;
		}
		else {
			last_sym->next_sym = target_sym;
		}

		last_sym = target_sym;
		sym_list = sym_list->next_sym;
	}

	/*
	 * Attach this scope to the scope list.
	 */
	attach_scope(target_scope, env_ptr);
}


/*********************************************************************
 *
 *	Name: del_sym_lst()
 *	ShortDesc: Deletes a list of symbols and their sub-structure values.
 *
 *	Description:
 *		Del_sym_lst loops through the symbol-list and free the memory
 *		of each sub-structure value and then the symbol itself. 
 *
 *	Inputs:
 *		sym_lst - Ptr to the beginning of the symbol-list.
 *		stype   - The scope type.
 *		env_ptr - Environment contained heap for free up memory.
 *
 *	Outputs:
 *	 	Memory occupied the symbol-list was freed. 
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
del_sym_lst(SYM *sym_lst, int stype, ENVIRON *env_ptr)
{
	SYM 		*sym;					/* Current symbol in list */

	while (sym_lst) {	
		
		/*
		 * Free them one by one.
		 */
		switch (sym_lst->type->typ) {
		case METH_CHAR_TYPE:
		case METH_U_CHAR_TYPE:
		case METH_SHORT_TYPE:
		case METH_U_SHORT_TYPE:
		case METH_LONG_TYPE:
		case METH_U_LONG_TYPE:
		case METH_FLOAT_TYPE:
		case METH_DOUBLE_TYPE:
	
			/*
			 * Free the value sub-structure.
			 */
			rfree(env_ptr->heap_ptr, sym_lst->v.c_p);
			break;

		case METH_ARRAY_TYPE:
		case METH_PTR_TYPE:

			/* if SC_FUNC: Nothing to free, this array
			 * was passed in! 
			 */
			if (stype != SC_FUNC) {

				/*
				 * Free array buffer.
				 */
				rfree(env_ptr->heap_ptr, sym_lst->v.c_p);
			}
			break;

		default:
			CRASH_DBG();
			method_abort("Default reached in deleting symbols:", 
										env_ptr->env_info);
		}

		/*
		 * Free the symbol and go to the next one.
		 */
		sym = sym_lst;
		sym_lst = sym_lst->next_sym;
		rfree(env_ptr->heap_ptr, (char *) sym);
	}
}


/*********************************************************************
 *
 *	Name: del_scope()
 *	ShortDesc: Deletes the current scope.
 *
 *	Description:
 *		Del_scope frees the symbol-list of the current scope and then
 *		frees the current scope itself. 
 *
 *	Inputs:
 *		env_ptr - Environment contained current scope to be freed. 
 *
 *	Outputs:
 *	 	Memory occupied the symbol-list was freed. 
 *
 *	Returns:
 *		nothing
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/

void
del_scope(ENVIRON *env_ptr)
{
	SCOPE 	*old_scope;				/* Ptr to the scope to be freed */

	/*
	 * Un-attach the current scope from the environment.
	 */
	old_scope = ENV_SCOPE;
	ENV_SCOPE = ENV_SCOPE->next_scope;

	/*
	 * Delete the symbol-list of the scope.
	 */
	del_sym_lst(old_scope->symbols, old_scope->type, env_ptr);

	/*
	 * Free up the scope structure itself.
	 */
	rfree(env_ptr->heap_ptr, (char *) old_scope);
}


/*********************************************************************
 *
 *	Name: lookup()
 *	ShortDesc: Looks up a symbol in the symbol table.
 *
 *	Description:
 *		Lookup first searchs the symbol-list of each current scope.
 *		If the symbol was not found, searchs for the symbol in 
 *		the global-scope. The symbol will be returned if it is found.
 *		Otherwise, Null will be returned. 
 *
 *	Inputs:
 *		sym_name - The symbol to be searched. 
 *		env_ptr  - Environment contained symbol-lists for searching.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		If the symbol was found, that symbol will be returned. 
 *		Otherwise, NULL will be returned.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
lookup(char *sym_name, ENVIRON *env_ptr)
{
	SYM 	*this_sym;						/* Current symbol in list */
	SCOPE 	*this_scope;					/* Current scope in list */

	/*
	 * Get the first scope in the current scope list.
	 */
	this_scope = ENV_SCOPE;					

	while (this_scope) {

		/*
		 * Search for the symbol in the symbol-list of 
		 * the current scope.
		 */
		for (this_sym = this_scope->symbols; 
				this_sym; this_sym = this_sym->next_sym) {

			if (strcmp(sym_name, this_sym->name) == 0) {
		
				return (this_sym);
			}
		}

		/* 
		 * Go to the next scope in the current scope list.
		 */
		this_scope = this_scope->next_scope;
	}

	/*
	 * The symbol was not found in the symbol-list of current-scope.
	 * Therefore, try to search in the global symbol-list.
	 */
	return (lookup_global(sym_name, env_ptr));
}


/*********************************************************************
 *
 *	Name: lookup_global()
 *	ShortDesc: Looks up a symbol in the global scope only.
 *
 *	Description:
 *		Lookup_global searchs the global symbol table for the specified
 *		symbol. Null will be returned if the symbol is not found. 
 *		Otherwise, returns the symbol found.
 *
 *	Inputs:
 *		sym_name - The symbol to be searched. 
 *		env_ptr  - Environment contained global symbol-list for searching.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		If the symbol was found, that symbol will be returned. 
 *		Otherwise, NULL will be returned.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
lookup_global(char *sym_name, ENVIRON *env_ptr)
{
	SYM 		*this_sym;				/* The global symbol list */

	/*
	 * Loop until the end of the symbol-list(NULL) or 
	 * found the symbol.
	 */
	for (this_sym = ENV_GLOB_SYMS;
			(this_sym && strcmp(sym_name, this_sym->name));  ) {

		/*
		 * Saber did not like for-loop with empty body.
		 */
		this_sym = this_sym->next_sym;
	}

	/*
	 * Return NULL(end of list) or the symbol found.
 	 */
	return this_sym;
}
