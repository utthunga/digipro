/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      @(#)int_bld.c	30.3  30  14 Nov 1996
 *
 *      int_bld.c - build the tree
 ******************************************************/

#if defined(SUN) || defined(CODECENTER)
#include <floatingpoint.h>
#endif /* SUN */

#include	"int_inc.h"

#ifdef TOK
#define error tok_interp_error
extern void tok_interp_error(va_alist);
#endif

/*
 *	Global variable types for use in 
 *	the build parsed tree sub-system.
 */

const TYPE_SPEC v_type = { METH_VOID_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC c_type = { METH_CHAR_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC uc_type = { METH_U_CHAR_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC s_type = { METH_SHORT_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC us_type = { METH_U_SHORT_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC l_type = { METH_LONG_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC ul_type = { METH_U_LONG_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC f_type = { METH_FLOAT_TYPE, 0, (TYPE_SPEC *)0 };
const TYPE_SPEC d_type = { METH_DOUBLE_TYPE, 0, (TYPE_SPEC *)0 };

const TYPE_SPEC aofc_type = { METH_ARRAY_TYPE, 0, (TYPE_SPEC *)&c_type };
const TYPE_SPEC aoful_type = { METH_ARRAY_TYPE, 0, (TYPE_SPEC *)&ul_type };

const TYPE_SPEC ptoc_type = { METH_PTR_TYPE, 0, (TYPE_SPEC *)&c_type };
const TYPE_SPEC ptol_type = { METH_PTR_TYPE, 0, (TYPE_SPEC *)&l_type };
const TYPE_SPEC ptoul_type = { METH_PTR_TYPE, 0, (TYPE_SPEC *)&ul_type };
const TYPE_SPEC ptof_type = { METH_PTR_TYPE, 0, (TYPE_SPEC *)&f_type };
const TYPE_SPEC ptod_type = { METH_PTR_TYPE, 0, (TYPE_SPEC *)&d_type };

/*
 * The following types are not used by any builtin functions now.
 * However, they may be used when adding new builtin functions.
 */

/*********************************************************************
 *
 *	Name: bld_type()
 *	ShortDesc: Builds a type structure.
 *
 *	Description:
 *		Bld_type allocates a TYPE_SPEC structure from the provided
 *		environment heap and initializes its fields with the provided 
 *		information. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		new_typ    - Supplied value for field type.
 *		new_madeof - Supplied value for field madeof.
 *		new_size   - Supplied value for field size.
 *		env_ptr    - Environment for allocating memory from.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		A pointer to an allocated and initialized TYPE_SPEC structure.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
TYPE_SPEC *
bld_type(int new_typ, TYPE_SPEC *new_madeof, long int new_size, ENVIRON *env_ptr)
{
	TYPE_SPEC *type_ptr;

	type_ptr = (TYPE_SPEC *) imalloc_chk(env_ptr, sizeof(TYPE_SPEC));

	type_ptr->typ = new_typ;
	type_ptr->madeof = new_madeof;
	type_ptr->size = new_size;

	return (type_ptr);
}


/*********************************************************************
 *
 *	Name: bld_val_p()
 *	ShortDesc: Builds a VALUE_P structure.
 *
 *	Description:
 *		Bld_val_p allocates memory storage base on a specified number 
 *		of data-objects of the specified type and sets an appropriate 
 *		pointer in VALUE_P structure to point to this allocated storage.
 *		See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		type_ptr - Type of data-storage to be allocated.
 *		size   	 - Number of data objects to be allocated.
 *		env_ptr  - Environment for allocating memory from.
 *
 *	Outputs:
 *		val_p   - Contained a pointer to storage of the specified type.
 *
 *	Returns:
 *		nothing.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
void
bld_val_p(TYPE_SPEC *type_ptr, size_t size, ENVIRON *env_ptr, VALUE_P *val_p)
{

	switch (type_ptr->typ) {
	case METH_CHAR_TYPE:
		val_p->c_p = (char *) imalloc_chk(env_ptr, sizeof(char) * size);
		break;

	case METH_U_CHAR_TYPE:
		val_p->uc_p = (unsigned char *) imalloc_chk(env_ptr, 
										sizeof(unsigned char) * size);
		break;

	case METH_SHORT_TYPE:
		val_p->s_p = (short *) imalloc_chk(env_ptr, sizeof(short) * size);
		break;

	case METH_U_SHORT_TYPE:
		val_p->us_p = (unsigned short *) imalloc_chk(env_ptr, 
										sizeof(unsigned short) * size);
		break;

	case METH_LONG_TYPE:
		val_p->l_p = (long *) imalloc_chk(env_ptr, sizeof(long) * size);
		break;

	case METH_U_LONG_TYPE:
		val_p->ul_p = (unsigned long *) imalloc_chk(env_ptr, 
										sizeof(unsigned long) * size);
		break;

	case METH_FLOAT_TYPE:
		val_p->f_p = (float *) imalloc_chk(env_ptr, sizeof(float) * size);
		break;

	case METH_DOUBLE_TYPE:
		val_p->d_p = (double *) imalloc_chk(env_ptr, sizeof(double) * size);
		break;

	case METH_ARRAY_TYPE:
		{
			long sumsize = 1;

			/*
			 * Adding up the number of elements in
			 * the  multi-dimensional array.
			 */ 
			for ( ;(type_ptr->typ == METH_ARRAY_TYPE);
								type_ptr = type_ptr->madeof) {
				sumsize *= type_ptr->size;
			}
			bld_val_p(type_ptr, (size_t)sumsize, env_ptr, val_p);
		}
		break;

	default:
		CRASH_DBG();
		method_abort("Default reached in allocating memory depending on type:",
			env_ptr->env_info);
		/*NOTREACHED*/
	}

}


/*********************************************************************
 *
 *	Name: bld_const()
 *	ShortDesc: Builds an constant expression.
 *
 *	Description:
 *		Bld_const creates an constant EXPRESS structure containing
 *		the provided constant value and its type. See int_lib.h 
 *		for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where the parser encountered the constant.
 *		new_val - Constant value for building expression.
 *		new_typ - Type of the constant value.
 *		env_ptr - Environment for building the constant expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS structure which is a constant.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static EXPRESS *
bld_const(unsigned int line, VALUE *new_val, int new_typ, ENVIRON *env_ptr)
{
	EXPRESS 		*exp_ptr;				/* Ptr to express build */
	TYPE_SPEC		*type_ptr;				/* Temp pointer to type */

	/*
	 * Create a constant expression.
	 */
	exp_ptr = bld_exp(line, CONSTREF, (EXPRESS *)NULL, (EXPRESS *)NULL,
						(EXPRESS *)NULL, env_ptr);

	if (exp_ptr != NULL) {
		/*
		 * Create memory for storing an constant value and 
		 * its type then attach them to the constant expression.
		 */
		type_ptr = bld_type(new_typ, (TYPE_SPEC *)NULL, 0L, env_ptr);
		bld_val_p(type_ptr, (size_t)1, env_ptr, &exp_ptr->misc.con_v);
		assign_val(&(exp_ptr->misc.con_v), new_typ, new_val, &type_ptr, env_ptr);
		exp_ptr->type = type_ptr;
	}

	return (exp_ptr);
}


/*********************************************************************
 *
 *	Name: bld_string()
 *	ShortDesc: Builds a string constant expression.
 *
 *	Description:
 *		Bld_string creates an constant EXPRESS structure containing
 *		the provided constant string. See int_lib.h for commenting
 *		on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered the string.
 *		s 		- Constant string for building expression.
 *		env_ptr - Environment for building the string expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS structure which is a constant string.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_string(unsigned int line, char *s, ENVIRON *env_ptr)
{
	EXPRESS 	*exp_ptr;				/* Ptr to express build */
	TYPE_SPEC	*type_ptr;				/* Temp pointer to type */

	/*
	 * Create a constant expression.
	 */
	exp_ptr = bld_exp(line, CONSTREF, (EXPRESS *)NULL, (EXPRESS *)NULL,
						(EXPRESS *)NULL, env_ptr);

	if (exp_ptr != NULL) {
		/*
		 * String is internally stored as array of characters.
		 * First build the type for array elements which are characters
		 * and then build the type for an array itself. Finally set a
		 * constant character pointer in the expression to point to 
		 * the provided string.
		 */ 
		type_ptr = bld_type(METH_CHAR_TYPE, (TYPE_SPEC *)NULL, 0L, env_ptr);
		exp_ptr->type = bld_type(METH_ARRAY_TYPE, 
								type_ptr, (long)strlen(s) + 1, env_ptr);
		exp_ptr->misc.con_v.c_p = s;
	}

	return (exp_ptr);
}


/*********************************************************************
 *
 *	Name: bld_exp()
 *	ShortDesc: Builds an expression structure.
 *
 *	Description:
 *		Bld_exp creates an EXPRESS structure containing the provided
 *		operator and its operands. If an expression contained constant
 *		operands that may be evaluated to get another constant value,
 *		then evaluates this expression to get the resulted value(This
 *		is done at parsing for saving the execution-time during the
 *		interpreting).
 *
 *	Inputs:
 *		line    - Line number where parser encountered expression.
 *		new_op  - An operator for the created expression.
 *		new_op1 - First operand for the created expression.
 *		new_op2 - Second operand for the created expression.
 *		new_op3 - Third operand for the created expression.
 *		env_ptr - Environment for building the expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		The pointer to a created EXPRESS.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
#define OPERATOR_CHK_FREE(op, env_ptr) { \
				if (ARITH_TYPE(op->type->typ)) { \
					rfree(env_ptr->heap_ptr, (char *) op->misc.con_v.c_p);	\
				} \
				rfree(env_ptr->heap_ptr, (char *) op); \
			}

EXPRESS *
bld_exp(unsigned int line, int new_op, EXPRESS *new_op1, EXPRESS *new_op2, EXPRESS *new_op3, ENVIRON *env_ptr)
{
	EXPRESS 		*new_exp;			/* Ptr to expr built */
	VALUE 			const_val;			/* Fold in constant value */
	TYPE_SPEC 		*const_type;		/* Fold in constant type */

	/*
	 * Allocate the EXPRESS structure and initialize 
	 * the operator and its operands with the provided parameters.
	 */
	new_exp = (EXPRESS *) imalloc_chk(env_ptr, sizeof(EXPRESS));
	new_exp->op = new_op;
	new_exp->op1 = new_op1;
	new_exp->op2 = new_op2;
	new_exp->misc.op3 = new_op3;
	new_exp->next_exp = NULL;

	/*
	 * Line was removed from this structure to save space.
	 */
	new_exp->line = line;

	/* 
	 * If an expression is not a REF expression then call
	 * to validate and get the type of the expression.  
	 * note: build REF expressions set their own type.
	 */
	if ((new_op != CONSTREF) && (new_op != SYMREF) && 
		(new_op != FUNCREF) && (new_op != ARRAYREF)) {

		new_exp->type = valid_expression(new_exp, env_ptr);
		if (new_exp->type == NULL) {
			/* 
			 * Error encountered free the expression.
			 */
			rfree(env_ptr->heap_ptr, (char *) new_exp);
			return (NULL);
		}

		/* 
		 * Check if an expression contained an operator that if applies 
		 * to its constant operands will be evaluated to another constant
		 * then evaluate this expression to get a resulted constant value.
		 */
		if ((new_op1 != NULL)
		    && (new_op1->op == CONSTREF)
		    && ((new_op2 == NULL) || (new_op2->op == CONSTREF))
		    && ((new_op3 == NULL) || (new_op3->op == CONSTREF))) {
	
			do_eval_expr(new_exp, &const_type, env_ptr, &const_val);
	
			/* 
			 * The sub-expressions/operands have been felt in one expression. 
			 * Therefore, we are no longer need to keep the sub-expressions 
			 * around. Check and free the expressions occupied the operands. 
			 */
	
			/* 
			 * NEVER FREE a type, it might be an external!! 
			 */
			OPERATOR_CHK_FREE(new_op1, env_ptr);
	
			if (new_op2 != NULL) {
				/* 
				 * NEVER FREE a type, it might be an external!! 
				 * free_type( (char *)new_op2->type ); 
				 */
				OPERATOR_CHK_FREE(new_op2, env_ptr);
			}
	
			if (new_op3 != NULL) {
	
				/* 
				 * NEVER FREE a type, it might be an external!! 
				 * free_type( new_op3->type );
				 */
				OPERATOR_CHK_FREE(new_op3, env_ptr);
			}
	
			/* 
			 * Create new constant type 
			 */
			new_exp->type = const_type;
			new_exp->op = CONSTREF;
			new_exp->op1 = NULL;
			new_exp->op2 = NULL;
	
			if (ARITH_TYPE(const_type->typ)) {
				bld_val_p(const_type, (size_t)1, env_ptr, &new_exp->misc.con_v);
			}
	
			assign_val(&(new_exp->misc.con_v), 
									const_type->typ, &const_val, &const_type, env_ptr);
		}
	}

	return (new_exp);
}


/*********************************************************************
 *
 *	Name: bld_symref()
 *	ShortDesc: Builds a symbol reference expression.
 *
 *	Description:
 *		Bld_const creates an EXPRESS structure containing a symbol 
 *		reference. Issues error message if the symbol is not found
 * 		in the symbol table. Otherwise, returns a created expression.
 *		See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered symbol reference.
 *		name 	- Symbol name in character string.
 *		env_ptr - Environment for building the symbol reference expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained a symbol reference.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_symref(unsigned int line, char *name, ENVIRON *env_ptr)
{
	SYM 		 *sym_ptr;				/* Ptr to symbol found */
	EXPRESS 	 *exp_ptr;				/* Expression built */

	/*
	 * Create a symbol reference expression and 
	 * assign the symbol type to expression type.
	 */
	exp_ptr = bld_exp(line, SYMREF, (EXPRESS *)NULL, (EXPRESS *)NULL,
						(EXPRESS *)NULL, env_ptr);

	if (exp_ptr != NULL) {

		sym_ptr = lookup(name, env_ptr);
		if (sym_ptr == NULL) {
			error(env_ptr->env_info, UNDECLARED_IDENTIFIER, name);
			return (NULL);
		}
	
		exp_ptr->type = sym_ptr->type;
		exp_ptr->misc.name = name;
	}

	return (exp_ptr);
}


/*********************************************************************
 *
 *	Name: bld_arrayref()
 *	ShortDesc: Builds an array reference expression structure.
 *
 *	Description:
 *		Bld_const creates an EXPRESS structure containing an array 
 *		reference. Issues an error message if error is found in the
 *		array reference. Otherwise, returns the created expression.
 *		See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line      - Line number where parser encountered array reference.
 *		array_ref - Expression contained an array symbol reference.
 *		subscript - Expression contained an array subscript.
 *		env_ptr   - Environment for building the array reference expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained an array reference.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_arrayref(unsigned int line, EXPRESS *array_ref, EXPRESS *subscript, ENVIRON *env_ptr)
{
	EXPRESS 			*exp_ptr;		/* An array subscript list */
	EXPRESS 			*new_array;		/* A created array reference */

	new_array = NULL;

	/*
	 * Verify and issue error message if found error.
	 */
	if ((array_ref != NULL) && (subscript != NULL))	{

		if (array_ref->type->typ != METH_ARRAY_TYPE) {
			error(env_ptr->env_info, NON_ARRAY_SUBSCRIPTED);
			return (NULL);
		}
		if (!INTEGRAL_TYPE(subscript->type->typ)) {
			error(env_ptr->env_info, NON_INTEGRAL_INDEX);
			return (NULL);
		}
	
		if (array_ref->op != ARRAYREF) {
	
			/*
			 * Build an expression contained the array reference to a 
			 * single dimensional array. The array symbol is stored in
			 * the first operand and the array subscript is stored in
			 * the second operand.
			 */
			new_array = bld_exp(line, ARRAYREF, array_ref, subscript,
								(EXPRESS *)NULL, env_ptr);
	
		}
		else {
	
			/*
			 * For a multi-dimensional array, the second operand points
			 * to a link list of array subscripts.
			 */
			new_array = array_ref;
	
			exp_ptr = array_ref->op2;
	
			/*
			 * Add the next subscript to the end of list.
			 * SPEED COULD BE IMPROVED HERE
			 */
			while (exp_ptr->next_exp) {
				exp_ptr = exp_ptr->next_exp;
			}
	
			exp_ptr->next_exp = subscript;
		}
	
		if (new_array != NULL) {
			new_array->type = array_ref->type->madeof;
			ASSERT_DBG(new_array->type != NULL);
		}	
	}

	return (new_array);
}
	
	
/*********************************************************************
 *
 *	Name: bld_bltin_ref()
 *	ShortDesc: Builds a built-in call expression structure.
 *
 *	Description:
 *		Bld_bltin_ref creates an EXPRESS structure containing a built-in 
 *		call. Issues an error message if error is found in the built-in
 *		call. Otherwise, returns the created built-in-call expression.
 *		See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered built-in call.
 *		name    - Name of the built-in call in character string.
 *		arg_ptr - Argument-list of the built-in call.
 *		env_ptr - Environment for building the built-in call expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained a built-in call.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_bltin_ref(unsigned int line, char *name, EXPRESS *arg_ptr, ENVIRON *env_ptr)
{
	EXPRESS 	*sym_ref;			/* Function call expression */

	sym_ref = bld_symref(line, name, env_ptr);

	/*
	 * Validate for correctness of built-in call. 
	 */
	if (sym_ref == NULL) {
		return (NULL);
	}
	if (sym_ref->type->typ != METH_BUILT_IN_TYPE) {
		error(env_ptr->env_info, NON_FUNCTION_CALL, sym_ref->misc.name);
		return (NULL);
	}
	if (valid_bltin_call_args(sym_ref, arg_ptr, env_ptr) == FALSE) {
		return (NULL);
	}

	/*
	 * The built-in call is stored in the expression. The operator
	 * indicates built-in call(FUNCREF) and the first operand points
	 * to the argument-list.
	 */
	sym_ref->op = FUNCREF;
	sym_ref->op1 = arg_ptr;

	sym_ref->type = sym_ref->type->madeof;

	return (sym_ref);
}


/*********************************************************************
 *
 *	Name: bld_exp_lst()
 *	ShortDesc: Inserts an expression into the expression-list.
 *
 *	Description:
 *		Bld_exp_lst inserts an expression into the expression-list.
 *		An expression is always inserted at the end of the list.
 *
 *	Inputs:
 *		exp_ptr - An expression to be inserted into the list.
 *		arg_lst - An expression list for insertion.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		A pointer to expression-list contained additional 
 *			inserted expression.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_exp_lst(EXPRESS *exp_ptr, EXPRESS *arg_lst)
{
	EXPRESS 	*arg_ptr;			/* Expression list traversal ptr */ 

	if (arg_lst == NULL) {

		/*
		 * Insert the first expression into the empty list.
		 */
		arg_lst = exp_ptr;
	}
	else {

		/*
		 * Insert an expression into the end of expression-list.
		 */
		arg_ptr = arg_lst;	/* Find end of list */

		/* 
		 * SPEED COULD BE IMPROVED HERE
		 */
		while (arg_ptr->next_exp) {
			arg_ptr = arg_ptr->next_exp;
		}
		arg_ptr->next_exp = exp_ptr;
	}

	return (arg_lst);
}


/*********************************************************************
 *
 *			Statements
 *
 *********************************************************************/

/*********************************************************************
 *
 *	Name: bld_stmt()
 *	ShortDesc: Builds a statement structure.
 *
 *	Description:
 *		Bld_stmt creates a STATEMENT structure containing the statement
 *		type and the provided expressions.  Issues an error message if 
 *		error is found in the statement. Otherwise, returns the created
 *		statement. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line     - Line number where parser encountered statement.
 *		stmt_typ - The statement type.
 *		stmt_ptr - Pointer to a list of statements/compound statement.
 *		exp1     - The first expression in statement.
 *		exp2     - The second expression in statement.
 *		exp3     - The third expression in statement.
 *		env_ptr   - Environment for building the statement.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a created STATEMENT.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
STATEMENT *
bld_stmt(unsigned int line, int stmt_typ, STATEMENT *stmt_ptr, EXPRESS *exp1,
	EXPRESS *exp2, EXPRESS *exp3, ENVIRON *env_ptr)
{
	STATEMENT 		*new_stmt;			/* Ptr to created statement */

	new_stmt = (STATEMENT *) imalloc_chk(env_ptr, sizeof(STATEMENT));

	/*
	 * Line was removed from this structure to save space.
	 */
	new_stmt->line = line;

	/*
	 * Add in the provided statement components and
	 * validate the created statement.
	 */
	new_stmt->stmt_type = stmt_typ;
	new_stmt->stmt = stmt_ptr;
	new_stmt->exp1 = exp1;
	new_stmt->exp2 = exp2;
	new_stmt->misc.exp3 = exp3;
	new_stmt->next_stmt = NULL;

	if (valid_stmt(new_stmt, env_ptr) == FALSE) {
		return (NULL);
	}

	return (new_stmt);
}


/*********************************************************************
 *
 *	Name: bld_stmt_lst()
 *	ShortDesc: Inserts a statement into the statement-list.
 *
 *	Description:
 *		Bld_stmt_lst inserts a statement into the statement-list.
 *		A statement is always inserted at the end of the list.
 *
 *	Inputs:
 *		stmt      - A statement to be inserted into the list.
 *		stmt_ptrs - A struct of head and tail to the list.
 *		env_ptr   - Pointer to environment struct.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		A pointer to statement-list contained additional 
 *			inserted statement.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
STMT_PTRS *
bld_stmt_lst(STATEMENT *stmt, STMT_PTRS *stmt_ptrs, ENVIRON *env_ptr)
{
	/*
	 * If the statement structure passed in is NULL,
	 * there must have been a syntax error and it
	 * is recorded by the error counter.  Don't do
	 * anything here. 
	 */
	if (stmt == NULL) {
		return(stmt_ptrs);
	}

	if (stmt_ptrs == NULL) {
		/*
	 	 * Create a new structure to hold head and tail
	 	 * for a statement list.
	 	 */
		stmt_ptrs = (STMT_PTRS *) imalloc_chk(env_ptr, sizeof(STMT_PTRS));
		stmt_ptrs->head = NULL;
		stmt_ptrs->tail = NULL;
	}

	if (stmt_ptrs->head == NULL) {
		/*
		 * Insert the first statement into the empty list.
		 */
		stmt_ptrs->head = stmt;
		stmt_ptrs->tail = stmt;
	}
	else {
		/*
		 * Insert a statement at the end of statement-list.
		 */
		stmt_ptrs->tail->next_stmt = stmt;
		stmt_ptrs->tail = stmt;
	}

	return (stmt_ptrs);
}


/*********************************************************************
 *
 *			Declarations
 *
 *********************************************************************/

/*********************************************************************
 *
 *	Name: bld_sym_lst()
 *	ShortDesc: Builds a list of unique symbols.
 *
 *	Description:
 *		Bld_sym_lst merges two symbol list into one. During the merging
 *		process, Error message will be issued if the duplicated symbol
 *		found. At any time the combined list always contained unique
 *		symbols. 
 *
 *	Inputs:
 *		this_sym - Head of list of symbols for merging into a unique-symbol list.
 *		sym_lst  - Head of list containing unique symbols.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a combined list of unique symbols.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_sym_lst(ENVIRON *env_ptr, SYM *this_sym, SYM *sym_lst)
{
	SYM 	*sym_ptr;					/* Tmp ptr for list traversal */
	SYM 	*cur_sym;					/* Current insertion symbol */
	int 	err;						/* Error counter */

	/*
	 * First symbol in the list.
	 */
	if (sym_lst == NULL) {
		return (this_sym);
	}

	if (this_sym == NULL) {
		return (sym_lst);
	}

	/*
	 * Merging two symbol-lists into one. During the merging
	 * process, maintain the symbol list so that all symbols 
	 * in the list are unique.
	 */ 
	while (this_sym) {

		/*
		 * Detach only one symbol from the list for insertion.
		 */
		cur_sym = this_sym;
		this_sym = this_sym->next_sym;
		cur_sym->next_sym = NULL;

		/*
		 * Checking for a duplicated symbol in the list.
		 */ 
		err = 0;
		for (sym_ptr = sym_lst; sym_ptr->next_sym;
								     sym_ptr = sym_ptr->next_sym) {

			/*
			 * Issue error message if found a duplicated symbol.
			 * COULD IMPROVE SPEED BY USING INT ID`S INSTEAD OF STRCMP
			 */
			if (cur_sym->name && !strcmp(cur_sym->name, sym_ptr->name)) {
				error(env_ptr->env_info, IDENTIFIER_RE_DEFINITION, cur_sym->name, 
						GET_SYM_LINE(sym_ptr));
				err++;
				break;
			}
		}

		/*
		 * If the current insertion symbol is unique, 
		 * add the current symbol into the end of list.
		 */
		if (!err) {
			sym_ptr->next_sym = cur_sym;
		}
	}

	return (sym_lst);
}


/*********************************************************************
 *
 *	Name: bld_add_type()
 *	ShortDesc: Adds base type to a list of declarators.
 *
 *	Description:
 *		bld_add_type loops through a list of declarators and creates a
 *		type structure using the base type then adds the created type
 *		to each symbol. Finally returns the symbol-list. See int_lib.h 
 *		for commenting on data fields.
 *
 *	Inputs:
 *		new_typ	  - New type to add.
 *		decl_lst  - List of declarator symbols.
 *		env_ptr   - Environment for building a list of built-in 
 *						 function symbols.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A list of declarator symbols with types.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_add_type(int new_typ, SYM *decl_lst, ENVIRON *env_ptr)
{
	SYM 		*sym_ptr;			/* Ptr to a current symbol */
	TYPE_SPEC 	*type_ptr;			/* Ptr to array type list */
	TYPE_SPEC	*new_type;			/* Type for the symbol */

	/*
	 * Go through the list of declarator symbols and 
	 * build the type for each symbol.
	 */
	for (sym_ptr = decl_lst; 
			sym_ptr != NULL; sym_ptr = sym_ptr->next_sym) {

		/*
		 * Create a type structure and attach to the symbol.
		 */
		new_type = bld_type(new_typ, (TYPE_SPEC *)NULL, 0L, env_ptr);

		if (sym_ptr->type == NULL) {
			sym_ptr->type = new_type;
		}
		else {

			/*
			 * Add an array-element type to the end of list.
			 */
			type_ptr = sym_ptr->type;
			while (type_ptr->madeof != NULL) {
				type_ptr = type_ptr->madeof;
			}
			type_ptr->madeof = new_type;
		}
	}

	return (decl_lst);
}


/*********************************************************************
 *
 *	Name: bld_sym()
 *	ShortDesc: Builds a symbol.
 *
 *	Description:
 *		Bld_sym creates an SYM structure containing a symbol name. 
 *		Other fields are initialized to nulls. See int_lib.h for 
 *		commenting on data fields.
 *
 *	Inputs:
 *		line     - Line number where parser encountered a symbol.
 *		sym_name - Symbol name in character string.
 *		env_ptr  - Environment for using to build a symbol. 
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a created symbol.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_sym(unsigned int line, char *sym_name, ENVIRON *env_ptr)
{
	SYM 		*sym_ptr;

	sym_ptr = (SYM *)imalloc_chk(env_ptr, sizeof(SYM));

    /*
     * Line was removed from this structure to save space.
     */
    sym_ptr->line = line;

    /*
     * Initialize the symbol-name. Other fields are unknown yet.
     */
    sym_ptr->name = sym_name;
    sym_ptr->type = NULL;
    sym_ptr->v.c_p = NULL;
    sym_ptr->next_sym = NULL;

	return (sym_ptr);
}


/*********************************************************************
 *
 *	Name: bld_abstr()
 *	ShortDesc: Builds an abstract type.
 *
 *	Description:
 *		Bld_abstr creates an abstract type. If an array dimension 
 *		is undefined, default the array-size to a maximum integer.
 *		If the provided subscript is not a subscript of the first 
 *		dimension of the multi-dimensional array then link the 
 * 		created type to the end of the madeof list of an array.
 *		The created type will be returned to the caller.
 *		See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		type_ptr  - Pointer to a created abstract type.
 *		subscript - Size of an array dimension.
 *		env_ptr   - Environment for using to build an 
 *						abstract type. 
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a created abstract type.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static TYPE_SPEC *
bld_abstr(TYPE_SPEC *type_ptr, EXPRESS *subscript, ENVIRON *env_ptr)
{
	long 		array_size;			/* The size of an array */
	TYPE_SPEC 	*new_type;			/* Ptr to created type */
	TYPE_SPEC 	*tmp_ptr;			/* Tmp ptr for traversal */
	
	ASSERT_DBG(subscript != NULL);

	/*
	 * Validate array subscript and get array size.
	 */
	array_size = valid_array_sub(subscript, env_ptr);

	/*
	 * An error has occurred.
	 */
	if (array_size == 0) {
		return (NULL);
	}

	/*
	 * Create a type for an array.
	 */
	new_type = bld_type(METH_ARRAY_TYPE, (TYPE_SPEC *)NULL,
								array_size, env_ptr);

	if (type_ptr == NULL) {
		type_ptr = new_type;
	}
	else {

		/*
	 	 * If it is a multi-dimensional array, link 
		 * the created type to the end of the madeof list.
		 */ 
		tmp_ptr = type_ptr;
		while (tmp_ptr->madeof != NULL) {
			tmp_ptr = tmp_ptr->madeof;
		}

		tmp_ptr->madeof = new_type;
	}

	return (type_ptr);
}


/*********************************************************************
 *
 *	Name: bld_array()
 *	ShortDesc: Builds an array declaration.
 *
 *	Description:
 *		Bld_array creates a SYM structure representing an array
 *		declaration. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		sym_ptr - Pointer to an array declaration symbol.
 *		exp_ptr - Expression contained array dimension size.
 *		env_ptr - Environment for using to build array declaration. 
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an array declaration symbol.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_array(SYM *sym_ptr, EXPRESS *exp_ptr, ENVIRON *env_ptr)
{
	if (sym_ptr != NULL) {

		if (exp_ptr == NULL) {
			error(env_ptr->env_info, ARRAY_SIZE_MISSING);
			return (NULL);
		}
	
		sym_ptr->type = bld_abstr(sym_ptr->type, exp_ptr, env_ptr);
	
		if (sym_ptr->type == NULL) {
			return (NULL);
		}
	}

	return (sym_ptr);
}


/*********************************************************************
 *
 *	Name: bld_bltin_decl()
 *	ShortDesc: Builds a built-in declaration symbol.
 *
 *	Description:
 *		Bld_bltin_decl builds the  built-in symbol with the provided 
 *		built-in name and parameter-list. See int_lib.h for commenting 
 *		on data fields.
 *
 *	Inputs:
 *		line      - Line where parser encountered built-in symbol.
 *		name      - The name of a built-in in ASCII string.
 *		param_lst - The list of built-in parameters.
 *		env_ptr   - Environment for building the built-in symbol.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a built-in symbol.
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_bltin_decl(unsigned int line, char *name, SYM *param_lst, ENVIRON *env_ptr)
{
	SYM 			*sym_ptr; 			/* Ptr to created symbol */
	BUILT_IN 		*bltin_ptr;			/* Ptr to created built-in */

	/*
	 * Build a built-in symbol then attach its type and
	 * the parameter-list.
	 */
	sym_ptr = bld_sym(line, name, env_ptr);

	sym_ptr->type = bld_type(METH_BUILT_IN_TYPE, (TYPE_SPEC *)NULL,
								0L, env_ptr);

	sym_ptr->v.bltin_p = (BUILT_IN *) imalloc_chk(env_ptr, sizeof(BUILT_IN));

	bltin_ptr = sym_ptr->v.bltin_p;
	bltin_ptr->params = param_lst;

	return (sym_ptr);
}


/*********************************************************************
 *
 *	Name: bld_abstr_sym()
 *	ShortDesc: Builds an abstract symbol.
 *
 *	Description:
 *		Bld_abstr_sym calls to create a symbol and attachs the 
 *		specified symbol type.
 *
 *	Inputs:
 *		line     - Line number where parser encountered a symbol.
 *		type_ptr - Pointer to the type of symbol.
 *		env_ptr  - Environment for building the symbol.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to the created symbol.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
SYM *
bld_abstr_sym(unsigned int line, TYPE_SPEC *type_ptr, ENVIRON *env_ptr)
{
	SYM 		 *sym_ptr;

	/*
	 * Build symbol with the specified type.
	 */
    sym_ptr = bld_sym(line, (char *)NULL, env_ptr);
    sym_ptr->type = type_ptr;

	return (sym_ptr);
}


/*********************************************************************
 *
 *	Routines to create constants
 *
 *********************************************************************/


/*********************************************************************
 *
 *	Name: bld_cnv_c()
 *	ShortDesc: Converts a constant character to its internal form.
 *
 *	Description:
 *		Bld_cnv_c converts a character to its internal representation.
 *		The conversion will be performed only to the escape sequence
 *		characters. Other characters will not be changed.
 *
 *	Inputs:
 *		text - Character to be converted.
 *		endp - Next character to be converted.
 *
 *	Outputs:
 *		nothing.	
 *
 *	Returns:
 *		A pointer to a converted character.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
static char
bld_cnv_c(char *text, char **endp)
{
	char 	c;				/* Converted character */


	if (*text != '\\') {

		/*
		 * A character is not an escape sequence character.
		 * It is a simple character. No conversion needed. 
		 * 
		 */
		c = *text;
		*endp = text + 1;
	}
	else {	

		/*
		 * A character is an escape sequence character.
		 * Convert a character to its internal representation.
		 */

		text++;
		switch (*text) {
		case '\'':
			c = '\'';
			*endp = text + 1;
			break;
		case '\"':
			c = '\"';
			*endp = text + 1;
			break;
		case '?':
			c = '?';
			*endp = text + 1;
			break;
		case '\\':
			c = '\\';
			*endp = text + 1;
			break;
		case 'a':
			c = '\a';
			*endp = text + 1;
			break;
		case 'b':
			c = '\b';
			*endp = text + 1;
			break;
		case 'f':
			c = '\f';
			*endp = text + 1;
			break;
		case 'n':
			c = '\n';
			*endp = text + 1;
			break;
		case 'r':
			c = '\r';
			*endp = text + 1;
			break;
		case 't':
			c = '\t';
			*endp = text + 1;
			break;
		case 'v':
			c = '\v';
			*endp = text + 1;
			break;

		case 'x':
			c = (char) strtol(++text, endp, 16);
			break;
		default:
			c = (char) strtol(text, endp, 8);
			break;
		}
	}
	return (c);
}


/*********************************************************************
 *
 *	Name: bld_str()
 *	ShortDesc: Builds a string.
 *
 *	Description:
 *		Bld_str scans through the provided string and converts the
 *		escape characters in string to the internal representation
 *		of these special characters.
 *
 *	Inputs:
 *		text - String to be converted its escape characters.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to a converted string.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
char *
bld_str(char *text)
{
	char *endp;			/* The next character to be converted */
	char *next_c;		/* Next location for storing converted char */
	char *last_c;		/* Ptr pointing to the end of string */ 

	/*
	 * Point to the end of string and toss last '"'.
	 */ 
	last_c = &(text[strlen(text) - 2]);

	next_c = text;
	endp = text + 1;					/* skip of the first '"' */
	while (endp <= last_c) {
		*next_c++ = bld_cnv_c(endp, &endp);
	}
	*next_c = '\0';

	return (text);
}


/*********************************************************************
 *
 *	Name: bld_char()
 *	ShortDesc: Builds an character constant expression.
 *
 *	Description:
 *		Bld_char builds an EXPRESS structure containing an character
 *		constant. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered constant.
 *		text 	- String representation of an constant character. 
 *		env_ptr - Environment for building the constant expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained an character constant.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_char(unsigned int line, char *text, ENVIRON *env_ptr)
{
	char 		 *endp = (char *)0;	/* Next char to be converted in bld_cnv_c
										routine, but not used here */
	VALUE 		new_val;			/* VALUE struct contained constant */
	EXPRESS		*exp_ptr;			/* A character constant expression */

	/*
	 * Convert a string representation of an constant character into 
	 * an integer representation and put the value and its type into 
	 * the VALUE structure.
	 */
	new_val.l = (int) bld_cnv_c(text + 1, &endp);

	rfree(env_ptr->heap_ptr, text);

	/*
	 * Build an expression contained an character constant.
	 */
	exp_ptr = bld_const(line, &new_val, METH_LONG_TYPE, env_ptr);

	return (exp_ptr);
}


/*********************************************************************
 *
 *	Name: bld_int_const()
 *	ShortDesc: Builds an integer constant expression.
 *
 *	Description:
 *		Bld_int_const builds an EXPRESS structure containing an 
 *		integer constant. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered a constant.
 *		text 	- String representation of a constant integer. 
 *		env_ptr - Environment for building the constant expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained an integer constant.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_int_const(unsigned int line, char *text, ENVIRON *env_ptr)
{
	unsigned long 	ul;					/* Numeric of integer constant */
	char *			u_tmp;				/* Signed or unsigned indicator */
	char *			l_tmp;				/* Long or integer indicator */
	char 			*suffix;			/* Suffix of returned numeric */
	int 			new_typ;			/* Type in the value structure */ 
	VALUE 			new_val;			/* VALUE struct contained constant */
	EXPRESS			*exp_ptr;			/* A integer constant expression */

	/*
	 * Convert the string representation of an integer constant into a
	 * numeric and put the value and its type into the VALUE structure.
	 */
	ul = strtoul(text, &suffix, 0);

	u_tmp = strchr(suffix, 'u');
	if (u_tmp == NULL) {
		u_tmp = strchr(suffix, 'U');
	}

	l_tmp = strchr(suffix, 'l');
	if (l_tmp == NULL) {
		l_tmp = strchr(suffix, 'L');
	}

	/*
	 * Determine the type of a numeric value by checking
	 * to see a constant falls into which range. Start from 
	 * the largest range down to smallest range(integer).
	 */
	if ((ul > LONG_MAX) || ((u_tmp != NULL) && (l_tmp != NULL))) {
		new_typ = METH_U_LONG_TYPE;
		new_val.ul = ul;
	}
	else {
		new_typ = METH_LONG_TYPE;
		new_val.l = (long) ul;
	}

	rfree(env_ptr->heap_ptr, text);

	/*
	 * Build an expression contained an integer constant.
	 */
	exp_ptr = bld_const(line, &new_val, new_typ, env_ptr);

	return (exp_ptr);
}


/*********************************************************************
 *
 *	Name: bld_float_const()
 *	ShortDesc: Builds a float constant expression.
 *
 *	Description:
 *		Bld_float_const builds an EXPRESS structure containing a float
 *		constant. See int_lib.h for commenting on data fields.
 *
 *	Inputs:
 *		line    - Line number where parser encountered a constant.
 *		text 	- String representation of a constant float. 
 *		env_ptr - Environment for building the constant expression.
 *
 *	Outputs:
 *		nothing.
 *
 *	Returns:
 *		A pointer to an EXPRESS contained a float constant.	
 *
 *	Author:
 *		Mike Dieter/Bac Truong
 *
 *********************************************************************/
EXPRESS *
bld_float_const(unsigned int line, char *text, ENVIRON *env_ptr)
{
	double		d;				/* Numeric of float constant */
	char 		*suffix;		/* double or float indicator */
	int 		new_typ;		/* Type in the value structure */ 
	VALUE 		new_val;		/* VALUE contained a constant */
	EXPRESS		*exp_ptr;		/* A float constant expression */

	/*
	 * Convert the string representation of a float constant into a
	 * numeric and put the value and its type into the VALUE structure.
	 */
	d = strtod(text, &suffix);
	if ((*suffix == 'f') || (*suffix == 'F')) {
		new_typ = METH_FLOAT_TYPE;
		new_val.f = (float) d;
	}
	else {
		new_typ = METH_DOUBLE_TYPE;
		new_val.d = d;
	}
	rfree(env_ptr->heap_ptr, text);

	/*
	 * Build an expression contained the float constant.
	 */
	exp_ptr = bld_const(line, &new_val, new_typ, env_ptr);

	return (exp_ptr);
}
