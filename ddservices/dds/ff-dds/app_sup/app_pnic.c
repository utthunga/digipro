/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */


#if defined(__BORLANDC__) || defined(_MSC_VER) || defined(SVR4) || defined(__linux__)
#include	<stdarg.h>
#else
#include    <varargs.h>
#endif

#include	<stdlib.h>
#include	<stdio.h>
#include    "panic.h"

int (*dds_panic)(char *msg)=NULL;


#if defined(__BORLANDC__) || defined(_MSC_VER) || defined(SVR4) || defined(__linux__)
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
/* VARARGS */
void 
panic(const char *format, ...)
{
	va_list         ap;

	va_start(ap, format);

	(void) vprintf(format, ap);

#ifdef DDIDE
	if(dds_panic)
	{
    	char buf[1000];

		memset(buf,0,sizeof(char)*1000);
		vsprintf(buf,format,ap);
		va_end(ap);
		if((*dds_panic)(buf))
			exit(1); 
	}
#else
	va_end(ap);
	exit(1);
#endif
}

#else
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
/* VARARGS */
void
panic(va_alist)
va_dcl
{
	va_list         ap;
	char           *format;

	va_start(ap);

	format = va_arg(ap, char *);

	(void) vprintf(format, ap);

	va_end(ap);
	exit(1);
}

#endif

#if defined(__BORLANDC__) || defined(_MSC_VER) || defined(SVR4) || defined(__linux__)
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
/* VARARGS */
void
panic_but_return(const char *format, ...)
{
	va_list         ap;

	va_start(ap, format);

	(void) vprintf(format, ap);

#ifdef DDIDE
	if(dds_panic)
	{
    	char buf[1000];

		memset(buf,0,sizeof(char)*1000);
		vsprintf(buf,format,ap);
		va_end(ap);
	}
#else
	va_end(ap);
#endif
}

#else
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
/* VARARGS */
void
panic_but_return(va_alist)
va_dcl
{
	va_list         ap;
	char           *format;

	va_start(ap);

	format = va_arg(ap, char *);

	(void) vprintf(format, ap);

	va_end(ap);
}

#endif



