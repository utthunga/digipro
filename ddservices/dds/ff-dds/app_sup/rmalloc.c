/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/**
 *
 *	rmalloc.c
 *
 *	Robust Malloc routines:
 *
 *	These routines use a heap gotten from the operating system via standard
 *	malloc.  This heap is then used by the Robust routines to allocate and
 *	deallocate memory chunks with the usual routines:
 *
 *		 void *rmalloc (RHEAP * heap, size_t bytes_needed)
 *		 void *rcalloc (RHEAP * heap, size_t nelem, size_t elsize)
 *		 void *rrealloc (RHEAP * heap, char *p, size_t bytes_needed)
 *		 void rfree (RHEAP * heap, char *p)
 *
 *	In addition the Robust versions have the following functions:
 *
 *		 void rheapinit (RHEAP * heap, long size) - Initialize the heap.
 *		 long rsizeof (char *p) - Returns the size of the space
 *					allocated at this address.
 *		 int rmemberof(RHEAP * heap,char *p) - is p a member of heap ?
 *		 void rdump (RHEAP * heap, verbose) - Dumps the heap for
 *					diagnostic purposes (not on 331).
 *
 *	The include file, "rmalloc.h", is required to use these functions.
 *
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#ifdef SUN
#include <memory.h>     /* K&R */
#else
#include <string.h>     /* ANSI */
#endif

#include "std.h"
#include "rmalloc.h"

#define RM_USED		1L
#define RM_NEXT		(~RM_USED)

#define RM_BLKUSED(addr)	((((RBLKHEAD *)(addr))->ma_nextblk & RM_USED) != 0)
#define RM_NEXTBLK(addr)	((RBLKHEAD *)(((RBLKHEAD *)(addr))->ma_nextblk & RM_NEXT))
#define RM_BYTES_IN_BLK(addr) ((size_t)((long)RM_NEXTBLK(addr) - (long)(addr)))

#if defined(MSDOS) || defined(WIN32) || defined(_MSC_VER)
#define RM_ALIGN_MASK	3L
#else
#define RM_ALIGN_MASK	7L
#endif /* MSDOS || WIN32 || _MSC_VER */

#define RM_ALIGN_OK(addr)	(((long)(addr) & RM_ALIGN_MASK) == 0)
#define RM_SIZE_OK(size)	RM_ALIGN_OK(size)
#define RM_BLK_OVERHEAD		(sizeof (RBLKHEAD))
#define RM_MIN_BLK_SIZE		(RM_BLK_OVERHEAD+RM_ALIGN_MASK+1)

static void subheap_init P((RSUBHEAP *subptr, long size));

#ifdef DEBUG
static int rm_debug_level = 2;
static void rm_checkit P((RHEAP *heap, RSUBHEAP *checkptr));
#define RM_CHECKIT(heap,thissub) rm_checkit (heap, thissub)
#else
#define RM_CHECKIT(heap,thissub)
#endif


/******************************************************************
 *	find_subheap - Find the subheap of "heap" that "ptr" is in.
 */

static RSUBHEAP *
find_subheap (RHEAP *heap, char *ptr)
{
	RSUBHEAP *subptr;

	subptr = heap->ma_currsub;
	if (  (char *)subptr->ma_top <= ptr
	   && (char *)subptr->ma_bot >= ptr
	   ) {
		return (subptr);
	}
	for (subptr = heap->ma_firstsub; subptr; subptr = subptr->ma_nextsub) {
		if (  (char *)subptr->ma_top <= ptr
		   && (char *)subptr->ma_bot >= ptr
		   ) {
			return (subptr);
		}
	}
	return ((RSUBHEAP *)0);
}


/******************************************************************
 *	create_subheap - Create a new subheap and initialize it.
 */

static RSUBHEAP *
create_subheap (long size)
{
	RSUBHEAP *subptr;

	/*
	 *	Allocate the memory, allowing for the RSUBHEAP overhead.
	 */

	size += SUBHEAP_OVERHEAD;
	ASSERT_DBG (RM_SIZE_OK (size));
	subptr = (RSUBHEAP *)malloc (size);
	if (!subptr)
		return ((RSUBHEAP *)0);
	ASSERT_DBG (RM_ALIGN_OK (subptr));

	/*
	 *	Initialize the subheap.
	 */

	subheap_init (subptr, size);

	return (subptr);
}



/*******************************************************************
 *	rsubmalloc - Malloc memory from the specified subheap
 */

static void *
rsubmalloc (RSUBHEAP *subptr, size_t bytes_needed)
{
	register char *rtn;		/* Pointer to the newly allocated block */
	register RBLKHEAD *this_blk, *next_blk;		/* Working block addresses */
	register size_t size;	/* Current block's size */
	long try_again = 1 ;	/* Retry counter */
	RBLKHEAD *last;			/* Address of most recently allocated chunk */
	RBLKHEAD *bottom;		/* Last block in the heap */

	/*
	 *	Allocate something.  It is not logal to call rmalloc and not
	 *	allocate anything.
	 */

	if (!bytes_needed)
		bytes_needed = 1;

	/**
	 *	Allocate blocks in correctly aligned chunks
	 */

	bytes_needed += RM_BLK_OVERHEAD;
	bytes_needed += RM_ALIGN_MASK;
	bytes_needed &= ~RM_ALIGN_MASK;

	last = subptr->ma_last;
	bottom = subptr->ma_bot;
	this_blk = subptr->ma_last;
	do {
		for (;;) {

			/* Look for an idle block */
			while (RM_BLKUSED(this_blk) && this_blk < bottom)
				this_blk = RM_NEXTBLK(this_blk);

			if (this_blk >= bottom) {
				ASSERT_DBG (RM_NEXTBLK (this_blk) == NULL);
				goto no_memory;
			}
			next_blk = RM_NEXTBLK (this_blk);

			/* Check for enough memory here */
			size = RM_BYTES_IN_BLK (this_blk);
			if (size >= bytes_needed) {

				/* Check for bytes_needed almost the same as size */
				if (size - bytes_needed < RM_MIN_BLK_SIZE)
					bytes_needed = size;
				subptr->ma_usage += bytes_needed;
				subptr->ma_free -= bytes_needed;
#ifdef DEBUG
				if (subptr->ma_usage > subptr->ma_maxused)
					subptr->ma_maxused = subptr->ma_usage;
#endif

				/* Set this block's size and RM_USED flag */
				this_blk->ma_nextblk = ((long)this_blk + bytes_needed) | RM_USED;
				rtn = (char *) this_blk;
				subptr->ma_last = this_blk;

				/* Find the first space after this block */
				this_blk = RM_NEXTBLK(this_blk);

				/* Check for a new, smaller empty block */
				if (this_blk != next_blk) {
					this_blk->ma_nextblk = (long)next_blk;
					subptr->ma_nblks++;
#ifdef DEBUG
					if (subptr->ma_nblks > subptr->ma_maxblks)
						subptr->ma_maxblks = subptr->ma_nblks;
#endif
				}

				/* return a pointer to usable memory */
				return ((void *) (rtn + RM_BLK_OVERHEAD));
			}
			else {
				/* Check for end-of-memory array */
				if (next_blk >= subptr->ma_bot)
					goto no_memory;

				/* Combine w/ next block if both are free */
				if (!RM_BLKUSED(next_blk)) {
					ASSERT_DBG (!RM_BLKUSED(next_blk));
					this_blk->ma_nextblk = next_blk->ma_nextblk;
					subptr->ma_nblks--;
					if (next_blk == last) {
						subptr->ma_last = this_blk;
					}
					continue;
				}

				/* No space here, bump pointer to next block */
				this_blk = next_blk;
			}
		}

no_memory:
		this_blk = subptr->ma_top ;
	} while (try_again--) ;

	return ((void *) 0);
}




/*******************************************************************
 *	rmalloc - The MAIN guy that does all the memory management for
 *	the system.  Everything is organized as pointer/flags followed
 *	by data.  The size returned does NOT include the bookkeeping,
 */
void *
rmalloc(register RHEAP *heap, register size_t bytes_needed)
{
	RSUBHEAP *subptr, *currptr;
	void *return_ptr;
	long size;

	/*
	 *	Try to malloc from the current subheap
	 */

	currptr = heap->ma_currsub;
	return_ptr = rsubmalloc (currptr, bytes_needed);
	if (return_ptr != NULL) {
		RM_CHECKIT (heap, currptr);
		return (return_ptr);
	}

	/*
	 *	We couldn't get the memory from the current subheap.
	 *	Try all other subheaps.  Skip the current subheap, since
	 *	we have already tried that.  Also, skip any subheaps that
	 *	have less free space than we are looking for.
	 *
	 *	When we find another subheap to check, we only want to
	 *	walk the chain once, so disable the optimization that
	 *	starts checking after the last allocated chunk.  We do
	 *	this by setting the address of the last allocated chunk
	 *	such that RM_NEXTBLK() on that pointer points to the end of
	 *	the heap.  See rsubmalloc() for use of the ma_last value.
	 */

	for (subptr = heap->ma_firstsub; subptr; subptr = subptr->ma_nextsub) {
		if (subptr == currptr || (size_t)subptr->ma_free < bytes_needed)
			continue;
		subptr->ma_last = subptr->ma_bot;
		return_ptr = rsubmalloc (subptr, bytes_needed);
		if (return_ptr != NULL) {
			heap->ma_currsub = subptr;
			RM_CHECKIT (heap, subptr);
			return (return_ptr);
		}
	}

	/*
	 *	We were unable to find space in an existing subheap.
	 *	If the requested number of bytes is less than the
	 *	subheap size, create a new subheap, and use it.
	 *	If not, we are unable to allocate the memory.
	 */

	if (  (heap->ma_subsize & DYNAMIC_HEAP)
	   && (bytes_needed <= (size_t)(size = ~DYNAMIC_HEAP & heap->ma_subsize))
	   ) {
		subptr = create_subheap (size);
		if (!subptr)
			panic("%s (%d): Out of memory in rmalloc\n", __FILE__, __LINE__);
		heap->ma_lastsub->ma_nextsub = subptr;
		heap->ma_lastsub = subptr;
		return_ptr = rsubmalloc (subptr, bytes_needed);
		ASSERT_DBG (return_ptr != NULL);
		heap->ma_currsub = subptr;
	}

	RM_CHECKIT (heap, subptr);
	return (return_ptr);
}


/*******************************************************************
 *	rcalloc -
 *	Simulates the system calloc().
 *	The size does NOT include the bookkeeping,
 */

void *
rcalloc(register RHEAP *heap, register size_t nelem, register size_t elsize)
{
	register void *p;
	register ulong total_size;

	total_size = (ulong)(nelem * elsize);
	p = rmalloc(heap, (size_t)total_size);

	if (!p)
		return (p);

	(void)memset(p, 0x00, (int) total_size);

	return (p);
}


/********************************************************************
 *	rfreeheap - free an entire heap structure.
 */

void
rfreeheap (RHEAP *heap)
{
	RSUBHEAP *subptr, *next;

	/*
	 *	If this is a static heap, just free the entire buffer.
	 */

	if ((heap->ma_subsize & DYNAMIC_HEAP) == 0) {
		free ((void *)heap);
		return;
	}

	/*
	 *	Step through the subheaps, freeing each.  Do not free
	 *	the heap pointer, since that may not be malloced.
	 */

	for (subptr = heap->ma_firstsub; subptr; subptr = next) {
		next = subptr->ma_nextsub;
		free ((void *)subptr);
	}
	heap->ma_firstsub = NULL;
	heap->ma_lastsub = NULL;
	heap->ma_currsub = NULL;
}


/*******************************************************************
 *	Return this block to the system.  If any blocks immediately
 *	following are already free, combine consecutive free blocks.
 */
void
rfree(register RHEAP *heap, register char *p)
{
	RBLKHEAD *freeblk;
	RSUBHEAP *subptr;
	register long bytes_returned;
	register RBLKHEAD *next_blk;
	RBLKHEAD *last;

	/*
	 *	If no block pointer, just return.
	 */

	if (!p)
		return;

	/*
	 *	Find the subheap.
	 */

	subptr = find_subheap (heap, p);
	if (subptr == NULL) {
		CRASH_DBG();
		/*NOTREACHED*/
		return;
	}
	last = subptr->ma_last;

	/* 
	 *	Point to link pointer, not to memory
	 */

	freeblk = (RBLKHEAD *)(p - RM_BLK_OVERHEAD);
	ASSERT_ALL(freeblk != NULL);

	/**
	 *	If this isn't busy, we are through
	 */
	if (!RM_BLKUSED (freeblk)) {
		RM_CHECKIT (heap, subptr);
		return;
	}

	next_blk = RM_NEXTBLK (freeblk);
	ASSERT_DBG (next_blk != NULL);
	bytes_returned = RM_BYTES_IN_BLK (freeblk);

	ASSERT_ALL(bytes_returned >= 0);

	subptr->ma_usage -= bytes_returned;
	subptr->ma_free += bytes_returned;

	/* Free this block */
	freeblk->ma_nextblk &= ~RM_USED;

	/* Combine up all free blocks following this one */
	while (next_blk != NULL && !RM_BLKUSED(next_blk)) {
		freeblk->ma_nextblk = next_blk->ma_nextblk;
		subptr->ma_nblks--;
		if (next_blk == last) {
			subptr->ma_last = freeblk;
		}
		next_blk = RM_NEXTBLK (next_blk);
	}
	RM_CHECKIT (heap, subptr);
}


/*******************************************************************
 *	Return the size of this memory block.  The size of the USER area
 *	is returned, bookkeeping data space is withheld.
 */
long
rsizeof(register char *p)
{
	p -= RM_BLK_OVERHEAD;
	return (RM_BYTES_IN_BLK(p) - RM_BLK_OVERHEAD);
}


/*******************************************************************
 *	Determine if the block passed is a member of the heap.  1 is YES.
 */
int
rmemberof(register RHEAP *heap, register char *p)
{
	return ((find_subheap (heap, p) != NULL));
}


/*******************************************************************
 *	Assume the given block exists and is allocated.  If the new size
 *	requested is smaller than the current size, free up the extra size.
 *	Otherwise, try to extend this block into immediately following
 *	free blocks.  Next, try to malloc a new larger block, copy into it
 *	and free the old block.
 */
void *
rrealloc(register RHEAP *heap, register char *ptr, register size_t bytes_requested)
{
	RSUBHEAP *subptr;
	RBLKHEAD *last;			/* Address of last allocated block */
	RBLKHEAD *orig_blk;		/* Pointer to head of existing memory block */
	size_t orig_size;		/* Size of original block */
	RBLKHEAD *next_blk;		/* Pointer to head of next block */
	size_t extended_size;	/* Size of extended block */
	size_t bytes_needed;	/* New size of block (including overhead) */
	long *dest_ptr, *src_ptr; /* Pointers for copying data */
	char *new_ptr;			/* Pointer to new buffer */
	size_t copy_count;		/* Loop counter for copying */

	/*
	 *	If a NULL pointer is passed in, do an rmalloc.
	 */

	if (!ptr)
		return (rmalloc(heap, bytes_requested));

	/*
	 *	If zero bytes are requested, free the pointer.
	 */

	if (!bytes_requested) {
		rfree(heap, ptr);
		return ((void *) 0);
	}

	ASSERT_ALL(bytes_requested > 0);

	/*
	 *	Find the subheap
	 */

	subptr = find_subheap (heap, ptr);
	if (subptr == NULL) {
		CRASH_DBG();
		/*NOTREACHED*/
		return ((void *)0);
	}
	last = subptr->ma_last;

	/*
	 *	Set pointers to original block, and next block.
	 */

	orig_blk = (RBLKHEAD *)(ptr - RM_BLK_OVERHEAD);
	next_blk = RM_NEXTBLK (orig_blk);
	orig_size = RM_BYTES_IN_BLK (orig_blk);

	/*
	 *	Convert requested size into correct alignment size
	 */

	bytes_needed = bytes_requested + RM_BLK_OVERHEAD;
	bytes_needed += RM_ALIGN_MASK;
	bytes_needed &= ~RM_ALIGN_MASK;

	/*
	 *	Compare the requested size to the real size
	 */

	if (orig_size == bytes_needed)
		return (ptr);

	/*
	 *	Prepare to possibly extend the original block.
	 */

	subptr->ma_usage -= (long)orig_size;
	subptr->ma_free += (long)orig_size;

	/* 
	 *	Look for more space by combining the current block with any
	 *	following unused block.
	 */

	for (;;) {

		/*
		 *	Check to see if the current block is now large enough.
		 */

		extended_size = RM_BYTES_IN_BLK (orig_blk);
		if (extended_size >= bytes_needed) {
			/**
			 *	We can extend this block to be big enough
			 *	for the new size of the block.
			 *
			 *	If the block size is just a little larger
			 *	than requested, use the entire block.
			 */

			if (extended_size - bytes_needed <= RM_MIN_BLK_SIZE)
				bytes_needed = extended_size;

			/*
			 * Set this block's size and RM_USED flag.
			 */
			orig_blk->ma_nextblk = ((long)orig_blk + bytes_needed) | RM_USED;
			if (extended_size != bytes_needed) {

				/*
				 *	Make a new, smaller empty block
				 */

				orig_blk = RM_NEXTBLK (orig_blk);
				orig_blk->ma_nextblk = (long)next_blk;
				subptr->ma_nblks++;
#ifdef DEBUG
				if (subptr->ma_nblks > subptr->ma_maxblks)
					subptr->ma_maxblks = subptr->ma_nblks;
#endif
			}
			subptr->ma_usage += bytes_needed;
			subptr->ma_free -= bytes_needed;
#ifdef DEBUG
			if (subptr->ma_usage > subptr->ma_maxused)
				subptr->ma_maxused = subptr->ma_usage;
#endif
			RM_CHECKIT (heap, subptr);
			return (ptr);
		}

		/*
		 *	As soon as we reach the bottom of memory
		 *	or a busy block, give up because we can not
		 *	add any more blocks to this memory chunk.
		 */

		if (RM_BLKUSED (next_blk))
			break;

		/*
		 *	We will merge these two contiguous unused blocks together,
		 *	hence decreasing the total block count.
		 */

		subptr->ma_nblks--;
		if (next_blk == last) {
			subptr->ma_last = orig_blk;
		}

		next_blk = RM_NEXTBLK (next_blk);

		/*
		 *	Extend our block to include a contiguous, unused block.
		 */
		orig_blk->ma_nextblk = (long)next_blk | RM_USED;
	}

	/*
	 *	Now a large segment is marked busy, because we've extended
	 *	the original block of memory by adding to it all subsequent
	 *	unused blocks.  However, this large segment is still not
	 *	big enough for the requested block. Take care of bookkeeping.
	 */

	extended_size = RM_BYTES_IN_BLK (orig_blk);

	subptr->ma_usage += extended_size;
	subptr->ma_free -= extended_size;
#ifdef DEBUG
	if (subptr->ma_usage > subptr->ma_maxused)
		subptr->ma_maxused = subptr->ma_usage;
#endif

	/*
	 *	Couldn't extend block, allocate new block and do copy
	 */

	src_ptr = (long *) ptr;
	dest_ptr = (long *) rmalloc(heap, bytes_requested);
	ASSERT_DBG (RM_ALIGN_OK (dest_ptr));
	if (!dest_ptr)
		return ((void *) 0);
	new_ptr = (char *)dest_ptr;
	copy_count = (orig_size / (sizeof(long)) - 1);
	while (copy_count--) {
		/**
		 *	Copy contents of block of memory ...
		 */
		*dest_ptr++ = *src_ptr++;
	}
	/* Free the old block */
	rfree(heap, ptr);

	return ((void *) new_ptr);
}


static void
subheap_init (RSUBHEAP *subptr, long size)
{
	register RBLKHEAD *blkhead;
	register char *addr = (char *) subptr;	/* The memory control struct */
	char *original_addr;
	int adjustedBytes;

	/*
	 *	Allocate the RSUBHEAP structure in the beginning memory of the subheap.
	 */

	addr += sizeof (RSUBHEAP);
	size -= sizeof (RSUBHEAP);
	
	/*
	 * AR 4505: added allocation of the first (of two) RBLKHEAD after the RSUBHEAP
	 */
	addr += RM_BLK_OVERHEAD;
	size -= RM_BLK_OVERHEAD;

	/*
	 *	Set the address to correct alignment, and adjust the size for
	 *	the required RBLKHEAD at the end, and for any lost bytes
	 *	caused by adjusting the address.  Also, size must be a multiple
	 *	of the alignment size.
	 */

	/* 
	 * AR 4505: made this calculation less complicated by explicitly counting
	 * the bytes that we adjust "addr" by, and using that to adjust "size".
	 */
	original_addr = addr;
	addr += RM_ALIGN_MASK;
	addr = (char *) ((long) addr & ~RM_ALIGN_MASK);
	adjustedBytes = addr - original_addr;
	size -= (RM_BLK_OVERHEAD + adjustedBytes);

	size &= ~RM_ALIGN_MASK;

	/**
	 *	Initialize the RHEAP structure and set up the pointers in the
	 *	memory.
	 */

	subptr->ma_nextsub = NULL;
	subptr->ma_top = (RBLKHEAD *)addr;
	subptr->ma_bot = (RBLKHEAD *)(addr + size);
	subptr->ma_usage = 0;
	subptr->ma_maxused = 0;
	subptr->ma_nblks = 1;
	subptr->ma_maxblks = 1;
	subptr->ma_free = size;
	subptr->ma_osize = size;
	subptr->ma_pseudotop.ma_nextblk = (long)addr | RM_USED ;
	subptr->ma_last = &subptr->ma_pseudotop ;

	blkhead = (RBLKHEAD *) addr;
	blkhead->ma_nextblk = (long)addr + size;
	blkhead = RM_NEXTBLK (blkhead);
	blkhead->ma_nextblk = 0L | RM_USED;
}


void
rcreateheap (RHEAP *heap, long osize, long subsize)
{
	RSUBHEAP *subptr;


	/*
	 *	Malloc the subheap, and set up RHEAP.
	 */

	osize += SUBHEAP_OVERHEAD;
	subptr = (RSUBHEAP *)malloc (osize);
	if (!subptr)
		panic("%s (%d): Out of memory in rcreateheap\n", __FILE__, __LINE__);
	ASSERT_DBG (RM_ALIGN_OK (subptr));
	heap->ma_firstsub = subptr;
	heap->ma_lastsub = subptr;
	heap->ma_currsub = subptr;
	subsize += RM_ALIGN_MASK;
	subsize &= ~RM_ALIGN_MASK;
	heap->ma_subsize = subsize | DYNAMIC_HEAP;

	/*
	 *	Initialize the subheap
	 */

	subheap_init (subptr, osize);
	
	RM_CHECKIT (heap, subptr);
}


void
rheapinit(register RHEAP *heap, register long size)
{
	register char *addr = (char *) heap;	/* The memory control struct */


	/* Subtract off the malloc control structure */
	addr += sizeof(RHEAP);
	size -= sizeof(RHEAP);

	/*
	 *	Set the address to correct alignment, and adjust the size for
	 *	for any lost bytes caused by adjusting the address. 
	 */

	if (!RM_ALIGN_OK (addr)) {
		addr += RM_ALIGN_MASK;
		addr = (char *) ((long) addr & ~RM_ALIGN_MASK);
		size -= RM_ALIGN_MASK;
		ASSERT_DBG (RM_ALIGN_OK (addr));
	}

	/*
	 *	Initialize the RHEAP structure.  Subsize is set to zero because
	 *	this is a static heap structure.
	 */

	heap->ma_firstsub = (RSUBHEAP *)addr;
	heap->ma_lastsub = (RSUBHEAP *)addr;
	heap->ma_currsub = (RSUBHEAP *)addr;
	heap->ma_subsize = 0;

	/*
	 *	Initialize the subheap
	 */

	subheap_init (heap->ma_firstsub, size);
	
	RM_CHECKIT (heap, heap->ma_firstsub);
}



void
rdump(register RHEAP *heap, int verbose)
{
	RSUBHEAP *subptr;
	int i;
	RBLKHEAD *this_blk, *next_blk;
	RBLKHEAD *top, *bottom;
	long *display_ptr;

	if (!(heap->ma_subsize & DYNAMIC_HEAP)) {
		printf ("\n\nStatic heap\n");
	}
	else {
		printf ("\n\nDynamic heap with sub-heap growth size %ld\n", 
			(heap->ma_subsize & ~DYNAMIC_HEAP));
	}
	i = 1;
	subptr = heap->ma_firstsub;
	for ( ; subptr; subptr = subptr->ma_nextsub, i++) {
		printf ("\nSubheap number %d\n", i);
		printf("   Top = 0x%06lx, Bottom = 0x%06lx, Size = %ld\n", 
				(long) subptr->ma_top, (long) subptr->ma_bot,
				subptr->ma_osize);
		printf("   Free = %ld, Used = %ld, Max Used= %ld, No. Blks = %ld, Maxblks = %ld\n",
		   subptr->ma_free, subptr->ma_usage, subptr->ma_maxused, 
		   subptr->ma_nblks, subptr->ma_maxblks);

		if (verbose) {
			top = subptr->ma_top;
			bottom = subptr->ma_bot;
			this_blk = top;
			while (this_blk != NULL) {
				if (this_blk < top || this_blk > bottom) {
					printf("   ADDRESS ERROR 0x%06lx\n", (long)this_blk);
					break;
				}
				next_blk = RM_NEXTBLK (this_blk);

				printf("     Addr : 0x%06lx - 0x%06lx, size = 0x%06lx, %s : ",
					(long)this_blk, (long)next_blk,
					RM_BYTES_IN_BLK (this_blk),
					(RM_BLKUSED (this_blk) ? "busy" : "free"));

				display_ptr = (long *)((long)this_blk + RM_BLK_OVERHEAD);
				printf("%08lx ", *display_ptr++);
				printf("%08lx ", *display_ptr++);
				printf("%08lx", *display_ptr++);
				printf("\n");

				this_blk = next_blk;
			}
		}
	}
}



/*********************************************************************
 *	Rmalloc routines that will panic if the standard functions
 *	can't allocate the memory.
 */


/*** rmalloc_chk ***/

void *
rmalloc_chk(register RHEAP *heap, register size_t bytes_needed)
{
	void *loc_ptr;

	loc_ptr = rmalloc(heap, bytes_needed);

	if (loc_ptr == (void *)0) {
		panic("%s (%d): Out of memory in rmalloc_chk\n", __FILE__, __LINE__);
	}
	ASSERT_DBG (RM_ALIGN_OK (loc_ptr));

	return loc_ptr;
}


/*** rcalloc_chk ***/

void *
rcalloc_chk(register RHEAP *heap, register size_t nelem, register size_t elsize)
{
	void *loc_ptr;

	loc_ptr = rcalloc(heap, nelem, elsize);

	if (loc_ptr == (void *)0) {
		panic("%s (%d): Out of memory in rcalloc_chk\n", __FILE__, __LINE__);
	}
	ASSERT_DBG (RM_ALIGN_OK (loc_ptr));

	return loc_ptr;
}


/*** rrealloc_chk ***/

void *
rrealloc_chk(register RHEAP *heap, register char *ptr, register size_t bytes_needed)
{
	void *loc_ptr;

	loc_ptr = rrealloc(heap, ptr, bytes_needed);

	if (loc_ptr == (void *)0) {
		panic("%s (%d): Out of memory in rrealloc_chk\n", __FILE__, __LINE__);
	}
	ASSERT_DBG (RM_ALIGN_OK (loc_ptr));

	return loc_ptr;
}



#ifdef DEBUG
static void
rm_checkit (RHEAP *heap, RSUBHEAP *checkptr)
{
	RSUBHEAP *subptr, *firstptr, *lastptr;
	long used;
	long unused;
	long nblks;
	long size, subsize;
	RBLKHEAD *this_blk, *next_blk;
	RBLKHEAD *top, *bottom;

	/*
	 *	Step through the subheaps.
	 *	Validate the current subheap, and validate all subheaps if
	 *	rm_debug_level > 1
	 */

	firstptr = heap->ma_firstsub;
	lastptr = heap->ma_lastsub;
	subsize = heap->ma_subsize & ~DYNAMIC_HEAP;
	ASSERT_ALL (RM_SIZE_OK (subsize));

	/*
	 *	If this isn't a dynamic heap, there must only be one subheap.
	 */

	if ((heap->ma_subsize & DYNAMIC_HEAP) == 0) {
		ASSERT_ALL (firstptr == lastptr);
		ASSERT_ALL (heap->ma_currsub == firstptr);
	}

	for (subptr = heap->ma_firstsub; subptr; subptr = subptr->ma_nextsub) {

		/*
		 *	The subheap must be correctly alligned.
		 */

		ASSERT_ALL (RM_ALIGN_OK (subptr));

		/*
		 *	The next pointer should be zero iff it is the last subheap.
		 */

		ASSERT_ALL (subptr->ma_nextsub != NULL || subptr == lastptr);

		/*
		 *	For all subheaps except the first, the size must be subsize.
		 */

		if (subptr != firstptr) {
			ASSERT_ALL (subptr->ma_osize == subsize);
		}

		/*
		 *	If this isn't the subheap that was just changed, and if 
		 *	rm_debug_level <= 1, continue for the next subheap.
		 */

		if (subptr != checkptr && rm_debug_level <= 1)
			continue;

		/*
		 *	Validate the subheap.
		 */

		top = subptr->ma_top;
		bottom = subptr->ma_bot;
		ASSERT_ALL (RM_BLKUSED (bottom));
		ASSERT_ALL (RM_NEXTBLK (bottom) == NULL);
		this_blk = RM_NEXTBLK (&subptr->ma_pseudotop);

		/*
		 *	Is this pointer valid?
		 */

		ASSERT_ALL (this_blk >= top);
		ASSERT_ALL (this_blk <= bottom);

		used = 0;
		unused = 0;
		nblks = 0;
		next_blk = RM_NEXTBLK (this_blk);
		while (next_blk != NULL) {

			/*
			 *	Is the next pointer valid?
			 */

			ASSERT_ALL (next_blk > this_blk);
			ASSERT_ALL (next_blk >= top);
			ASSERT_ALL (next_blk <= bottom);

			/*
			 *	Add the block size into the correct counter.
			 */

			size = RM_BYTES_IN_BLK (this_blk);
			ASSERT_ALL (size >= RM_MIN_BLK_SIZE);
			if (RM_BLKUSED(this_blk))
				used += size;
			else
				unused += size;

			/*
			 *	Increment number of blocks counter.
			 */

			nblks++;

			/*
			 *	Check next block.
			 */

			this_blk = next_blk;
			next_blk = RM_NEXTBLK (this_blk);
			ASSERT_ALL (next_blk != NULL || this_blk == bottom);
		}

		/*
		 *	Validate the counters.
		 */

		ASSERT_ALL (nblks == subptr->ma_nblks);
		ASSERT_ALL (used == subptr->ma_usage);
		ASSERT_ALL (unused == subptr->ma_free);
		ASSERT_ALL (used + unused == subptr->ma_osize);
	}
}
#endif /*DEBUG*/
