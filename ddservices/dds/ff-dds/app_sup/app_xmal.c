/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#include    <stddef.h>
#include    <stdio.h>
#include    <malloc.h>

#include    "app_xmal.h"

/***********************************************************************
 *
 * Name: xmalloc
 *
 * ShortDesc: Malloc with extra checking.
 *
 * Descripton:
 *		Checks if malloc actually allocates memory.
 *
 * Inputs:
 *		size -- size of memory to allocate.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
void           *
xmalloc(size_t size)
{
	void           *ptr;

	ptr = (void *) malloc((unsigned int) size);
	if (!ptr)
		printf("Memory exhausted xmalloc.\n");

	return (ptr);
}


/***********************************************************************
 *
 * Name: xrealloc
 *
 * ShortDesc: Realloc with extra checking.
 *
 * Descripton:
 *		Checks if realloc actually allocates memory.
 *
 * Inputs:
 *		ptr -- pointer to memory to be reallocated.
 *		size -- size of the memory to be reallocated.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
void           *
xrealloc(void *ptr, size_t size)
{
	if (!ptr)
		return xmalloc(size);

	if (!size) {
		free((char *) ptr);
		return (NULL);
	}
	ptr = (void *) realloc((char *) ptr, (unsigned int) size);
	if (!ptr)
		printf("Memory exhausted xrealloc.\n");

	return (ptr);
}


/***********************************************************************
 *
 * Name: xcalloc
 *
 * ShortDesc: Calloc with extra checking.
 *
 * Descripton:
 *		Checks if calloc actually allocates memory.
 *
 * Inputs:
 *		num - number of items
 *		size -- size of each item
 *
 * Returns:
 *		void *
 *
 **********************************************************************/
void           *
xcalloc(size_t num, size_t size)
{
	void           *ptr;

	ptr = (void *) calloc((unsigned) num, (unsigned) size);
	if (!ptr) {
		panic("Memory exhausted in xcalloc.\n");
	}

	return ptr;
}

/******************************************************************
 *
 * Name:
 *
 * ShortDesc:
 *
 * Descripton:
 *
 *
 *
 * Inputs:
 *
 *
 * Returns:
 *		void.
 *
 ******************************************************************/
void
xfree(void **ptr)
{
	if (!(*ptr))
		panic("%s (%d): Pointer already freed", __FILE__, __LINE__);

	free((char *) *ptr);
	*ptr = (void *) 0;
}
