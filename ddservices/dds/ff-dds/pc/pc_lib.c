/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */


#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "std.h"
#include "table.h"
#include "ddi_tab.h"
#include "ddi_lib.h"
#include "int_meth.h"
#include "int_lib.h"
#include "pc_loc.h"
#include "app_xmal.h"
#include "int_diff.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

/*
 *	max length of line read from sym file
 */

#define MAX_BLK_TAG_SIZE    64
#define MAX_LINE_LEN        200

/*
 * Function Prototypes for PC static functions which must be
 * positioned  after other routines in this file which call them.
 */

static int pc_get_param_info P((ENV_INFO *env_info, PC_ELEM *pc_elemp, 
			unsigned long req_mask));
static int pc_read_param P((ENV_INFO *env_info, P_REF *ip_ref));
static int pc_discard_table P((ENV_INFO *env_info));
static int pc_check_lock P((PC_ELEM *pc_elemp));
static void pc_release_lock P((PC_ELEM *pc_elemp));
static int pc_get_one_var(BLOCK_HANDLE block_handle, ITEM_ID id, DDI_GENERIC_ITEM *gi);
static int pc_find_param(ENV_INFO *env_info, P_REF *ip_ref, PC_ELEM **pc_elempp,
	PC_VALUE **pc_valuepp);

#ifdef DEBUG
extern char     release_path[128];

/*
 *	Symbol table stuff for table printout.
 */

#define MAX_SYMBOLS	2048

typedef struct {
	ITEM_ID	sym_item_id;
	char	sym_name[50];
} SYM_ENTRY;
static int       num_symbols;
static SYM_ENTRY sym_entry[MAX_SYMBOLS];

#endif /* DEBUG */


/**********************************************************************
 *
 *  Name: pc count_entries
 *  ShortDesc: Calculate the number of Parameter Cache entries
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc count_entries calculates the number of Parameter Cache
 *		entries that will be needed to store the information about
 *		the block referenced by the FLAT_BLOCK_DIR pointer.
 *
 *  Inputs:
 *		block_handle -  handle for the block being added
 *		flat_block_dirp - Pointer to the FLAT_BLOCK_DIR structure
 *							for the block
 *
 *  Outputs:
 *		param_count - The count of the number of parameters including
 *						the characteristic record.
 *		record_count - The number of records among all the parameters
 *
 *  Returns:
 *		The count of the number of entries.
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_count_entries(BLOCK_HANDLE block_handle, FLAT_DEVICE_DIR *flat_device_dirp,
	int *param_count, int *record_count)
{

	BLK_TBL_ELEM   	*blk_tbl_elemp;
	int				blk_tbl_offset;
	int				entry_count;
	FLAT_BLOCK_DIR	*flat_block_dirp;
	PARAM_TBL_ELEM	*param_tbl_elemp, *param_tbl_elem_endp;
	int				rs;


	/*
	 * Calculate the number of entries.  There is one entry for simple
	 * variables.  For arrays and records, there is one entry for the
	 * record or array, and one entry for each array or record element.
	 */

	rs = get_abt_dd_blk_tbl_offset (block_handle, &blk_tbl_offset);
	if (rs != CM_SUCCESS) {
		return (rs);
	}
	blk_tbl_elemp	= &flat_device_dirp->blk_tbl.list[blk_tbl_offset];
	flat_block_dirp	= &blk_tbl_elemp->flat_block_dir;

	/*
	 * First, the characteristics record.
	 */

	entry_count = flat_block_dirp->char_mem_tbl.count + 1;
	*param_count = 1;

	/*
	 * Now, for each parameter, add the number of entries that are needed.
	 * The param_mem_count field is zero for variables and arrays, and
	 * contains the number of members in a record for records.
	 */

	*record_count = 0;
	param_tbl_elemp = flat_block_dirp->param_tbl.list;
	*param_count += flat_block_dirp->param_tbl.count;
    if (*param_count < 0)
    {
         return(CM_BAD_BLOCK_HANDLE) ;
    }
	param_tbl_elem_endp = param_tbl_elemp + flat_block_dirp->param_tbl.count;
	for (; param_tbl_elemp < param_tbl_elem_endp; param_tbl_elemp++) {
		if (param_tbl_elemp->array_elem_count) {
			entry_count += param_tbl_elemp->array_elem_count + 1;
		}
		else {
			entry_count += param_tbl_elemp->param_mem_count + 1;
			if (param_tbl_elemp->param_mem_count) {
				*record_count += 1;
			}
		}
	}



	return (entry_count);
}


/**********************************************************************
 *
 *  Name: pc get_locals
 *  ShortDesc:	 Get local_param list for a block
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_locals gets the local parameters for the specified block.
 *      The caller is responsible for freeing the DDI_GENERIC_ITEM record
 *      by calling ddi_clean_item.
 *
 *  Inputs:
 *		block_handle
 *
 *  Outputs:
 *      A DDI_GENERIC_ITEM structure for the block.
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/
int pc_get_locals(BLOCK_HANDLE block_handle, DDI_GENERIC_ITEM *gi)
{
    DDI_BLOCK_SPECIFIER bs;
    DDI_ITEM_SPECIFIER is;
    ENV_INFO env;

    bs.type = DDI_BLOCK_HANDLE;
    bs.block.handle = block_handle;
    is.type = DDI_ITEM_BLOCK;
    env.block_handle = block_handle;
    env.app_info = NULL;

    is.type = DDI_ITEM_BLOCK;
    memset(gi, 0, sizeof(DDI_GENERIC_ITEM));

    return ddi_get_item(&bs, &is, &env, BLOCK_LOCAL_PARAM, gi);
}


/**********************************************************************
 *
 *  Name: pc get_one_var
 *  ShortDesc:	 Get a single VARIABLE for a block
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_one_var gets the specified variable for the block.
 *      The caller is responsible for freeing the DDI_GENERIC_ITEM record
 *      by calling ddi_clean_item.
 *
 *  Inputs:
 *		block_handle, and the ID of the VARIABLE
 *
 *  Outputs:
 *      A DDI_GENERIC_ITEM structure for the VARIABLE.
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/
int pc_get_one_var(BLOCK_HANDLE block_handle, ITEM_ID id, DDI_GENERIC_ITEM *gi)
{
    DDI_BLOCK_SPECIFIER bs;
    DDI_ITEM_SPECIFIER is;
    ENV_INFO env;
    int attr;

    bs.type = DDI_BLOCK_HANDLE;
    bs.block.handle = block_handle;
    is.type = DDI_ITEM_ID;
    is.item.id = id;
    env.block_handle = block_handle;
    env.app_info = NULL;
    memset(gi, 0, sizeof(DDI_GENERIC_ITEM));
    attr = VAR_TYPE_SIZE | VAR_CLASS;

    return ddi_get_item(&bs, &is, &env, attr, gi);
}

/**********************************************************************
 *
 *  Name: pc id_compare
 *  ShortDesc:	 Comparison Routine for sort of id xref table
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc compare is the comparison routine for the qsort of
 *		the id xref table.
 *
 *  Inputs:
 *		elem1, elem2 - Pointers to the two id xref table elements
 *			to compare.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		< 0 if *elem1 is "less" than *elem2
 *		= if *elem1 and *elem2 are "equal"
 *		> 0 if *elem1 is "greater" than *elem2
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_id_compare(PC_ID_XREF *elem1, PC_ID_XREF *elem2)
{

	/*
	 * id xref table elements are ordered by pc_id.
	 */

	if (elem1->pc_id < elem2->pc_id) {
		return (-1);
	}

	if (elem1->pc_id > elem2->pc_id) {
		return (1);
	}

	return (0);
}


/**********************************************************************
 *
 *  Name: pc po_compare
 *  ShortDesc:	 Comparison Routine for sorting the po_xref array
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc po_compare is the comparison routine for the qsort of
 *		the po_xref array.
 *
 *  Inputs:
 *		elem1, elem2 - Pointers to the two parameter cache elements
 *			to compare.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		< 0 if *elem1 is "less" than *elem2
 *		= if *elem1 and *elem2 are "equal"
 *		> 0 if *elem1 is "greater" than *elem2
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_po_compare(PC_PO_XREF *elem1, PC_PO_XREF *elem2)
{

	/*
	 * po_xref elements are ordered by pc_po
	 */

	if (elem1->pc_po < elem2->pc_po) {
		return (-1);
	}
	if (elem1->pc_po > elem2->pc_po) {
		return (1);
	}
	return (0);
}


/**********************************************************************
 *
 *  Name: pc check_env_info
 *  ShortDesc: Check all fields of env_info for validity.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc check_env_info checks all the fields of env_info as they
 *		must be set by any caller of a parameter cache i/f function.
 *
 *  Inputs:
 *		env_info - environment info pointer to be checked
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_BAD_ENV_INFO
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_check_env_info(ENV_INFO *env_info)
{
	APP_INFO	*app_info;

	if (env_info == (ENV_INFO *) NULL) {
		return (PC_BAD_POINTER);
	}
	
	app_info = env_info->app_info;

	if (!valid_block_handle (env_info->block_handle)) {
		return (PC_BAD_BLOCK_HANDLE);
	 }

	if (!(valid_method_tag (app_info->method_tag)) &&
		((app_info->status_info & USE_MASK) == USE_METH_VALUE)) {
		return (PC_BAD_METHOD_TAG);
	}

	if (((app_info->status_info & USE_MASK) != USE_DEV_VALUE) &&
		((app_info->status_info & USE_MASK) != USE_METH_VALUE) &&
		((app_info->status_info & USE_MASK) != USE_EDIT_VALUE)) {
		return (PC_BAD_STATUS_INFO);
	}

	app_info->env_returns.dds_return_code = 0;
	app_info->env_returns.dds_errors.count = 0;
	app_info->env_returns.comm_err = 0;
	app_info->env_returns.device_status = 0;
	app_info->env_returns.response_code = 0;
	app_info->env_returns.id_in_error = 0;
	app_info->env_returns.po_in_error = 0;
	app_info->env_returns.subindex_in_error = 0;
	app_info->env_returns.member_id_in_error = 0;
	app_info->env_returns.return_flag = 0;

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc conv_p_ref_to_ip_ref
 *  ShortDesc: Convert a p_ref to a common PC p_ref, ip_ref.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc conv_p_ref_to_ip_ref takes a p_ref as supplied by any PC i/f
 *		routine and converts it to a standardized form for internal
 *		PC usage.
 */
/*
 *  Inputs:
 *		p_ref - a p_ref structure
 *
 *  Outputs:
 *		ip_ref - the standardized p_ref.
 *
 *  Returns:
 *		PC_SUCCESS, PC_BAD_ENV_INFO
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_conv_p_ref_to_ip_ref(BLOCK_HANDLE block_handle, P_REF *p_ref, P_REF *ip_ref)
{

	PC_ID_XREF		pc_id_key;
	PC_ID_XREF		*pc_idxp;
	PC				*pc_p;
	PC_ELEM			*pc_elemp;
	int				rs;

	if ((p_ref == (P_REF *) NULL) || (ip_ref == (P_REF *) NULL)) {
		return (PC_BAD_POINTER);
	}

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(block_handle, (void **)(&pc_p));
	rs = get_abt_app_info(block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

    if (!pc_p)
    {
        rs = pc_open_block(block_handle);
        if (rs)
            return rs;
        // CPMHACK - Use right method to get pc info instead of app info APIs
    	//rs = get_abt_app_info(block_handle, (void **)(&pc_p));
    	rs = get_abt_pc_info(block_handle, (void **)(&pc_p));
        if (rs)
            return rs;
    }

	if ((p_ref->type & (PC_PARAM_OFFSET_REF|PC_ITEM_ID_REF)) == 0) {
		return (PC_PARAM_NOT_FOUND);
	}

	ip_ref->type		= p_ref->type;
	ip_ref->subindex	= p_ref->subindex;

	/*
	 * If id is not supplied, get it
	 */

	if (!(p_ref->type & PC_ITEM_ID_REF)) {		/* id not spec'ed */
		if ((p_ref->po > pc_p->pc_po_xref_count - 1) || (p_ref->po < 0)) {
			return (PC_PARAM_NOT_FOUND);
		}
		ip_ref->po = p_ref->po;
		pc_elemp = pc_p->pc_po_xref[p_ref->po].pc_elemp;
		ip_ref->id = pc_elemp->pc_id;
		ip_ref->type |= PC_ITEM_ID_REF;
	} else {
		ip_ref->id = p_ref->id;
	}

	/*
	 * Convert id to po if po is not set.
	 */
	if (
		(p_ref->type & PC_PARAM_OFFSET_REF) == 0) {
		pc_id_key.pc_id = ip_ref->id;
		pc_idxp = (PC_ID_XREF *) bsearch ((char *) &pc_id_key,
			(char *) pc_p->pc_id_xref, (unsigned) pc_p->pc_id_xref_count,
			sizeof (*pc_p->pc_id_xref), (CMP_FN_PTR) pc_id_compare);
		if (pc_idxp == (PC_ID_XREF *) NULL) {
			return (PC_PARAM_NOT_FOUND);
		}
		ip_ref->po = pc_idxp->pc_po;
		pc_elemp = pc_p->pc_po_xref[ip_ref->po].pc_elemp;
		ip_ref->type |= PC_PARAM_OFFSET_REF;


		/*
		 * Check the value of subindex.
		 */
			/*
			 * return po for non-local parameters 
			 */

			if (!(pc_elemp->pc_class & LOCAL_CLASS)) {
				p_ref->po = ip_ref->po;  
				p_ref->type |= PC_PARAM_OFFSET_REF;
			}

	} else {
		ip_ref->po = p_ref->po;
		pc_elemp = pc_p->pc_po_xref[ip_ref->po].pc_elemp;
	}

	if (ip_ref->subindex != 0) {
		if (((pc_elemp->pc_flags & PC_HEAD_ELEMENT) == 0) ||
			(ip_ref->subindex > pc_elemp->pc_size) || (ip_ref->subindex < 0)) {
			return (PC_PARAM_NOT_FOUND);
		}
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc conv_pc_p_ref_to_ip_ref
 *  ShortDesc: Convert a pc_p_ref to a common PC p_ref, ip_ref.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc conv_pc_p_ref_to_ip_ref takes a pc_p_ref as supplied by any 
 *		PC i/f routine and converts it to a standardized form for 
 *		internal PC usage.
 */
/*  Inputs:
 *		pc_p_ref - a pc_p_ref parameter reference structure
 *
 *  Outputs:
 *		ip_ref - the internal p_ref.
 *
 *  Returns:
 *		PC_SUCCESS, PC_BAD_ENV_INFO
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_conv_pc_p_ref_to_ip_ref(BLOCK_HANDLE block_handle, P_REF *pc_p_ref, P_REF *ip_ref)
{

	int			rs;
	P_REF		p_ref;

	if (pc_p_ref == (P_REF *) NULL) {
		return (PC_BAD_POINTER);
	}

	if (pc_p_ref->type & PC_ITEM_ID_REF) {
		p_ref.id = pc_p_ref->id;
	}

	if (pc_p_ref->type & PC_PARAM_OFFSET_REF) {
		p_ref.po = pc_p_ref->po;
	}

	p_ref.subindex	= pc_p_ref->subindex;
	p_ref.type		= pc_p_ref->type;

	/*
	 * Note: When subsubindex is implemented additional conversion
	 * 		 will have to be done here.
	 */

	rs = pc_conv_p_ref_to_ip_ref (block_handle, &p_ref, ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc conv_ip_ref_to_p_ref
 *  ShortDesc: Convert the internal p_ref back to an external p_ref
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc conv_ip_ref_to_p_ref takes an internal p_ref and converts
 *		it to a p_ref which is usable by a comm. stack or even an
 *		application.
 */
/*  Inputs:
 *		ip_ref - a internal p_ref structure
 *
 *  Outputs:
 *		p_ref - an externally usable p_ref.
 *
 *  Returns:
 *		PC_SUCCESS, PC_BAD_ENV_INFO
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_conv_ip_ref_to_p_ref(BLOCK_HANDLE block_handle, P_REF *ip_ref, P_REF *p_ref)
{

	PC				*pc_p;
	int				rs;

	p_ref->id		= ip_ref->id;
	p_ref->po		= ip_ref->po;
	p_ref->subindex	= ip_ref->subindex;
	p_ref->type		= ip_ref->type;

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}


	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc set_up_string_var
 *  ShortDesc: Initialize a Parameter Cache string element
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc set_up_string_var initializes a Parameter Cache element
 *		for a string variable.  String variables consist of types
 *		ASCII, PACKED_ASCII, PASSWORD, BITSTRING, OCTETSTRING,
 *		VISIBLESTRING, and EUC.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to read.
 *
 *  Outputs:
 *		The Parameter Cache element is initialized.
 *
 *  Returns:
 *		PC_NO_MEMORY, PC_SUCCESS
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_set_up_string_var(PC_ELEM *pc_elemp)
{

	unsigned short  string_length;
	PC_STRING    *pc_string;

	/*
	 * Initialize a string variable by mallocing the space for the string,
	 * and initializing it.
	 */

	switch (pc_elemp->pc_type) {
	case DDS_ASCII:
	case DDS_PASSWORD:
		string_length = pc_elemp->pc_size;
		break;
	case DDS_PACKED_ASCII:
		string_length = (unsigned short) ((pc_elemp->pc_size+1)*3)/4;
		break;
	case DDS_BITSTRING:
		ASSERT_DBG ((pc_elemp->pc_size % 8) == 0);
		string_length = pc_elemp->pc_size / 8;
		break;
	case DDS_OCTETSTRING:
	case DDS_VISIBLESTRING:
		string_length=pc_elemp->pc_size;
//		ASSERT_DBG ((pc_elemp->pc_size % 8) == 0);
//		string_length = pc_elemp->pc_size / 8;
		break;
	case DDS_EUC:
		string_length = (unsigned short) (pc_elemp->pc_size * 2);
		break;
	default:
		return (PC_SUCCESS); 	/* if not a string type return */
	}
	pc_string = &pc_elemp->pc_dev_value.pc_value.pc_string;
	pc_string->str_ptr = calloc (1, (unsigned) string_length);
	if (pc_string->str_ptr == NULL) {
		return (PC_NO_MEMORY);
	}
	pc_string->str_len = string_length;
	pc_string = &pc_elemp->pc_meth_value.pc_value.pc_string;
	pc_string->str_ptr = calloc (1, (unsigned) string_length);
	if (pc_string->str_ptr == NULL) {
		return (PC_NO_MEMORY);
	}
	pc_string->str_len = string_length;
	pc_string = &pc_elemp->pc_edit_value.pc_value.pc_string;
	pc_string->str_ptr = calloc (1, (unsigned) string_length);
	if (pc_string->str_ptr == NULL) {
		return (PC_NO_MEMORY);
	}
	pc_string->str_len = string_length;

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc set_up_relations
 *  ShortDesc: Initialize a Parameter Cache relations list
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc set_up_relations creates a Parameter Cache relations list.
 *		For each Parameter Cache entry, the Parameter Cache keeps track
 *		of all variables which are dependent on the variable.  This
 *		allows quick checking for marking values as stale when the
 *		controlling variable is written.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to update.
 *		update_tblp - Pointer into Relations Table for this element
 *		rel_tbl_elemp - Pointer into the Relation Table
 *				for this element.
 *
 *  Outputs:
 *		The Parameter Cache element's relations list is set up.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_set_up_relations(PC_ELEM *pc_elemp, REL_TBL_ELEM *rel_tbl_elemp,
	UPDATE_TBL *update_tblp, ITEM_TBL *item_tblp)
{
	int					update_count;
	PC_RELATION_ELEM	*update_list;
	UPDATE_TBL_ELEM		*update_tbl_elemp, *update_tbl_elem_endp;
	ITEM_TBL_ELEM		*item_tbl_elemp;

	/*
	 * Allocate memory for the Parameter Cache relations.
	 */

	update_count = RTE_U_COUNT (rel_tbl_elemp);
	if (update_count == 0) {
		return (PC_SUCCESS);
	}
	update_list = (PC_RELATION_ELEM *)
		malloc ((unsigned)update_count * sizeof (PC_RELATION_ELEM));
	if (update_list == (PC_RELATION_ELEM *) NULL) {

		return (PC_NO_MEMORY);
	}
	pc_elemp->pc_relation.pc_rel_count = update_count;
	pc_elemp->pc_relation.pc_rel_list = update_list;

	/*
	 * For each relation element, create an entry in the PC_RELATION.
	 */

	update_tbl_elemp = &update_tblp->list[RTE_UT_OFFSET(rel_tbl_elemp)];
	update_tbl_elem_endp = update_tbl_elemp + update_count;
	for (; update_tbl_elemp < update_tbl_elem_endp; update_tbl_elemp++, update_list++) {
		item_tbl_elemp = ITE(item_tblp, update_tbl_elemp->op_it_offset);
		update_list->pc_rel_id = ITE_ITEM_ID(item_tbl_elemp);
		update_list->pc_rel_subindex =
			update_tbl_elemp->op_subindex;
		update_list->pc_rel_pointer = (PC_ELEM *) 0;
	}

	/*
	 * Return SUCCESS.
	 */
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc set_up_table
 *  ShortDesc: Set up the values in the Parameter Cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc set_up_table sets up the default values in the variable
 *		table.  To do this, it gathers information from the DDS tables 
 *		pointed to by the FLAT_BLOCK_DIR pointer. pc set_up_table is 
 *		called from pc open_block to initialize the Parameter Cache 
 *		after pc open_block has created a space for the Parameter Cache.
 *
 *  Inputs:
 *		block_handle -  handle for the block being added
 *		flat_device_dirp - Pointer to FLAT_DEVICE_DIR entry for this device
 *		pc_p - Pointer to Parameter Cache to fill in
 */
/*  Outputs:
 *		The parameter cache pointed to by pc_p is filled in.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_INTERNAL_ERROR
 *
 *	Also:
 *		pc open_block
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_set_up_table(BLOCK_HANDLE block_handle, FLAT_DEVICE_DIR *flat_device_dirp,
	PC *pc_p
	)

{
	unsigned long   		arr_rec_dyn_flag;
	ITEM_ID         		arr_rec_id;
	int						arr_rec_po;
	unsigned long 	 		array_class;
	unsigned short  		array_size;
	ITEM_TYPE       		array_type;
	BLK_ITEM_NAME_TBL_ELEM 	*blk_item_name_tbl_elemp;
	BLK_ITEM_NAME_TBL 		*blk_item_name_tblp;
	BLK_TBL_ELEM   			*blk_tbl_elemp;
	int						blk_tbl_offset;
	CHAR_MEM_NAME_TBL_ELEM 	*char_mem_name_tbl_elemp, 
							*char_mem_name_tbl_elem_endp;
	CHAR_MEM_TBL_ELEM 		*char_mem_tbl_elem_endp;
	CHAR_MEM_TBL_ELEM 		*char_mem_tbl_elemp;
	FLAT_BLOCK_DIR 			*flat_block_dirp;
	PC_ELEM      			*head_elemp;
	int						highest_po;			/* highest param offset in block */
	ITEM_TBL       			*item_tblp;
	ITEM_TBL_ELEM  			*item_tbl_elemp;
	SUBINDEX	    		offset;
	PARAM_ELEM_TBL_ELEM 	*param_elem_tbl_elemp;
	PARAM_MEM_TBL  			*param_mem_tblp;
	PARAM_MEM_TBL_ELEM 		*param_mem_tbl_elemp, 
							*param_mem_tbl_elem_endp;
	PARAM_MEM_NAME_TBL 		*param_mem_name_tblp;
	PARAM_MEM_NAME_TBL_ELEM *param_mem_name_tbl_elemp,
							*param_mem_name_tbl_elem_endp;
	PARAM_TBL_ELEM 			*param_tbl_elemp, *param_tbl_elem_endp;
	PC_ELEM      			*pc_elemp, 
							*pc_elem_endp;
	PC_ID_XREF				*pc_xidp;
	PC_PO_XREF				*pc_xpop;
	REL_TBL_ELEM   			*rel_tbl_elemp;
	REL_TBL        			*rel_tblp;
	int             		rs;
	SUBINDEX	    		subindex;
	UPDATE_TBL     			*update_tblp;

	/*
	 * Initialize pointers
	 */

	pc_elemp			= pc_p->pc_list;
	pc_xidp				= pc_p->pc_id_xref;
	pc_xpop				= pc_p->pc_po_xref;
	rs = get_abt_dd_blk_tbl_offset (block_handle, &blk_tbl_offset);
	if (rs != CM_SUCCESS) {
		return (rs);
	}
	blk_tbl_elemp		= &flat_device_dirp->blk_tbl.list[blk_tbl_offset];
	flat_block_dirp		= &blk_tbl_elemp->flat_block_dir;
	item_tblp			= &flat_device_dirp->item_tbl;
	blk_item_name_tblp	= &flat_block_dirp->blk_item_name_tbl;
	rel_tblp			= &flat_block_dirp->rel_tbl;
	update_tblp			= &flat_block_dirp->update_tbl;

	/*
	 * First, add the entry for the characteristics record.
	 */

	item_tbl_elemp		= &item_tblp->list[blk_tbl_elemp->char_rec_item_tbl_offset];
	pc_elemp->pc_po		= 0;	/* The param offset is fixed by def. to 0. */
	pc_elemp->pc_id		= item_tbl_elemp->item_id;
	pc_xidp->pc_id		= item_tbl_elemp->item_id;
	pc_xidp->pc_po		= 0;
	pc_xpop->pc_po		= 0;
	pc_xpop->pc_elemp	= pc_elemp;
	pc_elemp->pc_subindex = 0;
	pc_elemp->pc_size	= (unsigned short) flat_block_dirp->char_mem_tbl.count;
	pc_elemp->pc_flags	= PC_RECORD | PC_FLAG_INIT;
	arr_rec_id			= pc_elemp->pc_id;
	arr_rec_po			= 0;
	arr_rec_dyn_flag	= 0;
	head_elemp			= pc_elemp;
	highest_po 			= 0;

	/*
	 * Get the Relation Table Element pointer for the characteristic
	 * record, and add the relation items.
	 */

	blk_item_name_tbl_elemp = BINTE (BTE_BINT(blk_tbl_elemp),
		BTE_CR_BINT_OFFSET (blk_tbl_elemp));

        if (blk_item_name_tbl_elemp == 0)
        {
            return -1;
        }

    // CPMHACK: Validating the block table offset
	if (blk_tbl_offset > DEFAULT_OFFSET && BINTE_RT_OFFSET (blk_item_name_tbl_elemp) > DEFAULT_OFFSET) {
		rel_tbl_elemp =
			RTE (rel_tblp, BINTE_RT_OFFSET (blk_item_name_tbl_elemp));
		rs = pc_set_up_relations (pc_elemp, rel_tbl_elemp, update_tblp, item_tblp);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	/*
	 * Now, add an entry for each member in the characteristics record.
	 */

	char_mem_tbl_elemp = flat_block_dirp->char_mem_tbl.list;
	char_mem_tbl_elem_endp =
		char_mem_tbl_elemp + pc_elemp->pc_size;
	char_mem_name_tbl_elemp = flat_block_dirp->char_mem_name_tbl.list;
	char_mem_name_tbl_elem_endp = char_mem_name_tbl_elemp + pc_elemp->pc_size; 
	subindex = 1;
	while (char_mem_tbl_elemp < char_mem_tbl_elem_endp) {
		offset = subindex - 1;
		pc_elemp++;
		ASSERT_DBG (pc_elemp - pc_p->pc_list < pc_p->pc_count);
		pc_elemp->pc_id = arr_rec_id;
		while (char_mem_name_tbl_elemp <  char_mem_name_tbl_elem_endp) {
			if (char_mem_name_tbl_elemp->char_mem_offset == offset) {
				pc_elemp->pc_member_id = char_mem_name_tbl_elemp->char_mem_name;
				break;
			}
			char_mem_name_tbl_elemp++;
		}
		pc_elemp->pc_po = arr_rec_po;
		pc_elemp->pc_subindex = subindex;
		pc_elemp->pc_subsubindex = 0;
		pc_elemp->pc_type = (ITEM_TYPE) char_mem_tbl_elemp->char_mem_type;
		pc_elemp->pc_size =
			(unsigned short) char_mem_tbl_elemp->char_mem_size;
		pc_elemp->pc_class	= char_mem_tbl_elemp->char_mem_class;
		if (char_mem_tbl_elemp->char_mem_class & DYNAMIC_CLASS) {
			pc_elemp->pc_flags = PC_DYN_FLAG_INIT;
			arr_rec_dyn_flag++;
		}
		else {
			pc_elemp->pc_flags = PC_FLAG_INIT;
		}

		rs = pc_set_up_string_var (pc_elemp);
		if (rs != PC_SUCCESS) {
			return (rs);
		}

		/*
		 * Get the Relation Table Element pointer for the
		 * characteristic record member, and add the relation items.
		 */

		char_mem_tbl_elemp = CMTE (BTE_CMT(blk_tbl_elemp), offset);
		if (CMTE_RT_OFFSET (char_mem_tbl_elemp) > DEFAULT_OFFSET) {
			rel_tbl_elemp = RTE (rel_tblp,
				CMTE_RT_OFFSET (char_mem_tbl_elemp));
			rs = pc_set_up_relations (pc_elemp, rel_tbl_elemp, update_tblp, 
									item_tblp);
			if (rs != PC_SUCCESS) {
				return (rs);
			}
		}

		char_mem_tbl_elemp++;
		subindex++;
	}
	ASSERT_DBG (subindex == flat_block_dirp->char_mem_tbl.count + 1);
	if (arr_rec_dyn_flag) {
		head_elemp->pc_flags |= PC_DYNAMIC_VAR;
	}

	/*
	 * For each parameter, add an entry.
	 */

	param_tbl_elemp = flat_block_dirp->param_tbl.list;
	param_tbl_elem_endp = param_tbl_elemp + flat_block_dirp->param_tbl.count;

	for (; param_tbl_elemp < param_tbl_elem_endp; param_tbl_elemp++) {
		pc_elemp++;
		ASSERT_DBG (pc_elemp - pc_p->pc_list < pc_p->pc_count);

		/*
		 * Get the item table pointer for the variable or record or array.
		 */

		blk_item_name_tbl_elemp =
			&blk_item_name_tblp->list[param_tbl_elemp->blk_item_name_tbl_offset];
		item_tbl_elemp =
			&item_tblp->list[blk_item_name_tbl_elemp->item_tbl_offset];

		/*
		 * Save the item_id, operational id and param offset in the entry.
		 */

		pc_elemp->pc_id		= item_tbl_elemp->item_id;
		pc_xidp++;
		pc_xidp->pc_id		= item_tbl_elemp->item_id;
		pc_xidp->pc_po		= (int)blk_item_name_tbl_elemp->param_tbl_offset
								+ 1;  /* delete the + 1 when DDS is fixed cbb */
		pc_xpop++;
		pc_xpop->pc_po		= pc_xidp->pc_po;
		pc_xpop->pc_elemp	= pc_elemp;
		pc_elemp->pc_po		= pc_xidp->pc_po;
		if (highest_po < pc_xidp->pc_po) {
			highest_po = pc_xidp->pc_po;
		}

		/*
		 * Add the relation items.
		 */

		if (BINTE_RT_OFFSET (blk_item_name_tbl_elemp) > DEFAULT_OFFSET) {
			rel_tbl_elemp = RTE (rel_tblp,
				BINTE_RT_OFFSET (blk_item_name_tbl_elemp));
			rs = pc_set_up_relations (pc_elemp, rel_tbl_elemp, update_tblp, 
									item_tblp);
			if (rs != PC_SUCCESS) {
				return (rs);
			}
		}

		/*
		 * If the parameter is an array, add an entry for the array,
		 * and an entry for each element of the array.
		 */

		if (param_tbl_elemp->array_elem_count) {
			ASSERT_DBG (param_tbl_elemp->param_mem_tbl_offset == -1);
			ASSERT_DBG (param_tbl_elemp->param_mem_count == 0);

			/*
			 * Add an entry for the array itself.
			 */

			arr_rec_id = pc_elemp->pc_id;
			arr_rec_po = pc_elemp->pc_po;
			array_type =
				(ITEM_TYPE) param_tbl_elemp->array_elem_type_or_var_type;
			array_size =
				(unsigned short) param_tbl_elemp->array_elem_size_or_var_size;
			array_class = param_tbl_elemp->array_elem_class_or_var_class;
			pc_elemp->pc_size =
				(unsigned short) param_tbl_elemp->array_elem_count;
			if (array_class & DYNAMIC_CLASS) {
				arr_rec_dyn_flag = PC_DYNAMIC_VAR;
			}
			else {
				arr_rec_dyn_flag = 0;
			}
			pc_elemp->pc_flags =
				PC_ARRAY | PC_FLAG_INIT | arr_rec_dyn_flag;

			/*
			 * Add an entry for each array element
			 */

			subindex = 1;
			pc_elem_endp = pc_elemp + param_tbl_elemp->array_elem_count;
			while (++pc_elemp <= pc_elem_endp) {
				ASSERT_DBG (pc_elemp - pc_p->pc_list < pc_p->pc_count);
				pc_elemp->pc_id = arr_rec_id;
				pc_elemp->pc_member_id = (ITEM_ID)subindex;
				pc_elemp->pc_po = arr_rec_po;
				pc_elemp->pc_subindex = subindex;
				pc_elemp->pc_subsubindex = 0;
				pc_elemp->pc_type = array_type;
				pc_elemp->pc_size = array_size;
				pc_elemp->pc_class = array_class;
				pc_elemp->pc_flags = PC_FLAG_INIT | arr_rec_dyn_flag;
				rs = pc_set_up_string_var (pc_elemp);
				if (rs != PC_SUCCESS) {
					return (rs);
				}

				/*
				 * Get the Relation Table Element pointer for
				 * the array element, and add the relation items.
				 */

				if ((subindex < PTE_PE_MX_COUNT (param_tbl_elemp)) &&
							(PTE_PET_OFFSET (param_tbl_elemp) != -1)) {

					param_elem_tbl_elemp = PETE (BTE_PET(blk_tbl_elemp),
						PTE_PET_OFFSET (param_tbl_elemp));

					if((PETE_PE_SUBINDEX(param_elem_tbl_elemp) == subindex) &&
						 (PETE_RT_OFFSET (param_elem_tbl_elemp) > DEFAULT_OFFSET)) {

						rel_tbl_elemp = RTE (rel_tblp, PETE_RT_OFFSET (param_elem_tbl_elemp));

						rs = pc_set_up_relations (pc_elemp, rel_tbl_elemp, 
												update_tblp, item_tblp);
						if (rs != PC_SUCCESS) {
							return (rs);
						}
					}
				}
				subindex++;
			}
			ASSERT_DBG (subindex == param_tbl_elemp->array_elem_count + 1);
			pc_elemp--;
			continue;
		}
		ASSERT_DBG (param_tbl_elemp->array_elem_item_tbl_offset == -1);

		/*
		 * If this parameter is a record, add an entry for that record,
		 * and an entry for each member of the record.
		 */

		if (param_tbl_elemp->param_mem_count) {

			/*
			 * Add the entry for the record itself
			 */

			arr_rec_id = pc_elemp->pc_id;
			arr_rec_po = pc_elemp->pc_po;
			pc_elemp->pc_size =
				(unsigned short) param_tbl_elemp->param_mem_count;
			pc_elemp->pc_flags = PC_RECORD | PC_FLAG_INIT;
			arr_rec_dyn_flag = 0;
			head_elemp = pc_elemp;

			/*
			 * Add an entry for each member of the record
			 */

			param_mem_tblp = &flat_block_dirp->param_mem_tbl;
			param_mem_tbl_elemp =
				&param_mem_tblp->list[param_tbl_elemp->param_mem_tbl_offset];
			param_mem_tbl_elem_endp =
				param_mem_tbl_elemp + pc_elemp->pc_size;
			param_mem_name_tblp = &flat_block_dirp->param_mem_name_tbl;
			param_mem_name_tbl_elemp =
				&param_mem_name_tblp->list[param_tbl_elemp->param_mem_tbl_offset];
			param_mem_name_tbl_elem_endp = param_mem_name_tbl_elemp +
				pc_elemp->pc_size;
			subindex = 1;


			while (param_mem_tbl_elemp < param_mem_tbl_elem_endp) {
				offset = subindex - 1;
				pc_elemp++;
				ASSERT_DBG (pc_elemp - pc_p->pc_list < pc_p->pc_count);
				pc_elemp->pc_id = arr_rec_id;
				while (param_mem_name_tbl_elemp < param_mem_name_tbl_elem_endp) {
					if(param_mem_name_tbl_elemp->param_mem_offset == offset) {
						pc_elemp->pc_member_id =
							param_mem_name_tbl_elemp->param_mem_name;
						break;
					}
					param_mem_name_tbl_elemp++;
				}
				pc_elemp->pc_po = arr_rec_po;
				pc_elemp->pc_subindex = subindex;
				pc_elemp->pc_subsubindex = 0;
				pc_elemp->pc_type =
					(ITEM_TYPE) param_mem_tbl_elemp->param_mem_type;
				pc_elemp->pc_size =
					(unsigned short) param_mem_tbl_elemp->param_mem_size;
				pc_elemp->pc_class = param_mem_tbl_elemp->param_mem_class;
				if (param_mem_tbl_elemp->param_mem_class & DYNAMIC_CLASS) {
					pc_elemp->pc_flags = PC_DYN_FLAG_INIT;
					arr_rec_dyn_flag++;
				}
				else {
					pc_elemp->pc_flags = PC_FLAG_INIT;
				}

				rs = pc_set_up_string_var (pc_elemp);
				if (rs != PC_SUCCESS) {
					return (rs);
				}

				/*
				 * Get the Relation Table Element pointer for the
				 * parameter record member, and add the relation items.
				 */

				if (PMTE_RT_OFFSET (param_mem_tbl_elemp) >
					DEFAULT_OFFSET) {
					rel_tbl_elemp = RTE (BTE_RT(blk_tbl_elemp),
						PMTE_RT_OFFSET (param_mem_tbl_elemp));
					rs = pc_set_up_relations (pc_elemp,
						rel_tbl_elemp, update_tblp, item_tblp);
					if (rs != PC_SUCCESS) {
						return (rs);
					}
				}

				param_mem_tbl_elemp++;
				subindex++;
			}
			ASSERT_DBG (subindex == param_tbl_elemp->param_mem_count + 1);
			if (arr_rec_dyn_flag) {
				head_elemp->pc_flags |= PC_DYNAMIC_VAR;
			}
			continue;
		}
		ASSERT_DBG (param_tbl_elemp->param_mem_tbl_offset == -1);

		/*
		 * The parameter is a simple variable.  Add one entry.
		 */

		pc_elemp->pc_type =
			(ITEM_TYPE) param_tbl_elemp->array_elem_type_or_var_type;
		pc_elemp->pc_size =
			(unsigned short) param_tbl_elemp->array_elem_size_or_var_size;
		if (param_tbl_elemp->array_elem_class_or_var_class & DYNAMIC_CLASS) {
			pc_elemp->pc_flags = PC_DYN_FLAG_INIT;
		}
		else {
			pc_elemp->pc_flags = PC_FLAG_INIT;
		}

		rs = pc_set_up_string_var (pc_elemp);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	ASSERT_RET ((pc_elemp - pc_p->pc_list == pc_p->pc_count - 1),
		PC_INTERNAL_ERROR);
	ASSERT_RET ((pc_xpop - pc_p->pc_po_xref == pc_p->pc_po_xref_count - 1),
		PC_INTERNAL_ERROR);
	(void) qsort ((char *) pc_p->pc_po_xref, pc_p->pc_po_xref_count,
		sizeof (*pc_p->pc_po_xref), (CMP_FN_PTR) pc_po_compare);

	ASSERT_RET ((pc_xidp - pc_p->pc_id_xref == pc_p->pc_id_xref_count - 1),
		PC_INTERNAL_ERROR);
	(void) qsort ((char *) pc_p->pc_id_xref, pc_p->pc_id_xref_count,
		sizeof (*pc_p->pc_id_xref), (CMP_FN_PTR) pc_id_compare);

    /* 
     * Process local parameters - they can be VARIABLES, RECORDS or ARRAYS.
     * Look through the entries and locate the existing LOCAL entries.
     * Set each one's class to LOCAL
     */
     {
         DDI_GENERIC_ITEM gi;
         if (!pc_get_locals(block_handle, &gi))
         {
             FLAT_BLOCK *fb = (FLAT_BLOCK *)gi.item;
             int i;
             for (i=0; i < fb->local_param.count; i++)
             {
                 //if (!pc_get_one_local(block_handle, &fb->local_param.list[i], &mem_gi))
                 //{
                     /* Search for an element */
                 P_REF ip_ref, p_ref;
                 ENV_INFO env_info;

                 env_info.block_handle = block_handle;
                 // CPMHACK - Use right method to get pc info instead of app info APIs
             	 // rs = set_abt_app_info(block_handle, (void *)pc_p);
             	 rs = set_abt_pc_info(block_handle, (void *)pc_p);
                 if (rs)
                     return rs;
                 p_ref.id = fb->local_param.list[i].ref.id;
                 p_ref.type = PC_ITEM_ID_REF;
                 p_ref.subindex = 0;
                 rs = pc_conv_p_ref_to_ip_ref(env_info.block_handle, &p_ref, &ip_ref);
                 if (rs)
                     return rs;
                 rs = pc_find_param(&env_info, &ip_ref, &pc_elemp, NULL);
                 if (rs)
                     return rs;
                 
                 if (pc_elemp->pc_flags & PC_HEAD_ELEMENT)
                 {
                     /* Process the next N elements */
                     int j, omax;
                     omax = pc_elemp->pc_size;
                     for (j=1; j <= omax; j++)
                     {
                         p_ref.id = fb->local_param.list[i].ref.id;
                         p_ref.type = PC_ITEM_ID_REF;
                         p_ref.subindex = j;
                         rs = pc_conv_p_ref_to_ip_ref(env_info.block_handle, &p_ref, &ip_ref);
                         if (rs)
                             return rs;
                         rs = pc_find_param(&env_info, &ip_ref, &pc_elemp, NULL);
                         if (rs)
                             return rs;
                         pc_elemp->pc_flags |= LOCAL_CLASS;
                     }
                 }
                 else
                    pc_elemp->pc_class |= LOCAL_CLASS;

                  //ddi_clean_item(&mem_gi);
                 //}  /* if !get_one_local(...) */
             }
             ddi_clean_item(&gi);
         }
     }

	/*
	 * We have added all the parameter info and characteristics record
	 * info.  Sort the po xref table and id xref table for faster access.
	 * Note: DO NOT SORT THE PARAMETER CACHE, po_xref is dependent on the
	 *       order of the elements since it contains pc_elemp.
	 */

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc open_block
 *  ShortDesc: Initialize the parameter caches for a block
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc open_block creates and initializes the parameter cache
 *		information for the specified block.  Use is made of the
 *		DDS tables that are set up by the connection manager.
 *		Therefore, the connection manager block open routines
 *		(ct_block_open, bl_block_load, and ds_dd_block_load) must be
 *		called prior to calling this routine.
 *
 *		The Parameter Cache is composed of three tables:
 *		The parameter cache data itself consisting of PC_ELEM 
 *			structures and not sorted.
 *		The id xref table composed of PC_ID_XREF structures, sorted by
 *			id and having ids for parameters and local variables
 */
/*		The po xref table composed of PC_PO_XREF structures, sorted by
 *			po and having a direct pointer into the cache for instant
 *			reference for users using po for parameter referencing.
 *
 *  Inputs:
 *		block_handle - The block handle returned by ct_block_open.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_INTERNAL_ERROR,
 *
 *  Also:
 *		ct_block_open, bl_block_load, ds_dd_block_load,
 *		pc close_block
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_open_block(BLOCK_HANDLE block_handle)
{

	int             	pc_entry_count;
	DEVICE_HANDLE		device_handle;
	FLAT_DEVICE_DIR		*flat_device_dirp;
	NET_TYPE			net_type;
	int					record_count;
	int					param_count;
	PC           		*pc_p;
	int             	rs;
	void				*blk_app_infop;
	int					b_var_count = 0;

	/*
	 * Validate the block handle
	 */

	if (!valid_block_handle (block_handle)) {
		return (PC_BAD_BLOCK_HANDLE);
	}

	/*
	 * Get the pointer into the Active Block Table, and validate the entry.
	 * Then see if the Parameter Caches are already set up.  If so, return
	 * successfully.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(block_handle, &blk_app_infop);
	rs = get_abt_pc_info(block_handle, &blk_app_infop);
	if (rs != CM_SUCCESS) {
		return (rs);
	}
	if (blk_app_infop != NULL) {
		return (PC_SUCCESS);
	}

	rs = get_abt_dd_dev_tbls (block_handle, (void **)(&flat_device_dirp));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	/*
	 * The table needs to be built.  First, figure out how many entries the
	 * table will need.
	 */

	pc_entry_count = pc_count_entries(block_handle, flat_device_dirp,
										&param_count, &record_count);

    if (param_count < 0)
    {
         return(CM_BAD_BLOCK_HANDLE) ;
    }

	/*
	 * Create the Parameter Cache
	 */

	pc_p = (PC *) malloc (sizeof *pc_p);
	if (pc_p == (PC *) NULL) {
		return (PC_NO_MEMORY);
	}
	pc_p->pc_count = pc_entry_count;
	pc_p->pc_list = (PC_ELEM *) calloc ((unsigned) pc_entry_count, 
										sizeof (*pc_p->pc_list));
	if (pc_p->pc_list == (PC_ELEM *) NULL) {
		free ((char *)pc_p);
		return (PC_NO_MEMORY);
	}

	rs = get_abt_adt_offset (block_handle, &device_handle);
	if (rs != CM_SUCCESS) {
		free((char *)pc_p->pc_list);
		free((char *)pc_p);
		return (rs);
	}
	rs = get_adt_net_type(device_handle, &net_type);
	if (rs != CM_SUCCESS) {
		free((char *)pc_p->pc_list);
		free((char *)pc_p);
		return (rs);
	}


	/*
	 * Create the ID xref list
	 */

	pc_p->pc_id_xref_count = param_count + b_var_count;
	pc_p->pc_id_xref = (PC_ID_XREF *) calloc
		((unsigned)(param_count + b_var_count), sizeof (*pc_p->pc_id_xref));
	if (pc_p->pc_id_xref == (PC_ID_XREF *) NULL) {
		free ((char *)pc_p->pc_list);
		free ((char *)pc_p);
		return (PC_NO_MEMORY);
	}

	/*
	 * Create the po_xref list
	 */

	pc_p->pc_po_xref_count = param_count;
	pc_p->pc_po_xref = (PC_PO_XREF *) calloc ((unsigned)param_count, 
											sizeof (*pc_p->pc_po_xref));
	if (pc_p->pc_po_xref == (PC_PO_XREF *) NULL) {
		free ((char *)pc_p->pc_id_xref);
		free ((char *)pc_p->pc_list);
		free ((char *)pc_p);
		return (PC_NO_MEMORY);
	}

	/*
	 * Fill in the Parameter Cache
	 */

	rs = pc_set_up_table (block_handle, flat_device_dirp, pc_p
		);
	if (rs != PC_SUCCESS) {
		(void) pc_close_block (block_handle);
		return (rs);
	}

	/*
	 * Link the Parameter Cache to the Active Block Table entry and return.
	 */
    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = set_abt_app_info(block_handle, (void *)pc_p);
	rs = set_abt_pc_info(block_handle, (void *)pc_p);
	if (rs != PC_SUCCESS) {
		(void) pc_close_block (block_handle);
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc find_param
 *  ShortDesc: Find the variable's Parameter Cache information
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc find_param_value finds the parameter cache ntry, and returns 
 *		pointers to the requested param's table entry, and its value 
 *		structure.  It is used by the Parameter Cache interface routines 
 *		that receive a request to find the appropriate place 
 *		in the Parameter Cache.  These routines include pc get_param_value,
 *		pc put_param_value, pc write_param_value, and 
 *		pc discard_put_param_value.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		ip_ref - P_REF specifies po, ID and subindex of 
 *				requested param.
 *
 *  Outputs:
 *		pc_elempp - Where to return pointer to parameter cache element
 *		pc_valuepp - Where to return pointer to param value structure.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, 
 *		PC_METHOD_COLLISION, PC_INTERNAL_ERROR,
 *		and errors from 
 *
 *	Also:
 *		pc get_param_value, pc put_param_value, pc write_param_value,
 *		pc discard_put_param_value, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_find_param(ENV_INFO *env_info, P_REF *ip_ref, PC_ELEM **pc_elempp,
	PC_VALUE **pc_valuepp)
{

	PC_ELEM			*pc_elemp;
	PC_VALUE		*pc_valuep;
	PC				*pc_p;
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;
	/*
	 * Get the pointer into the Active Block Table, and validate the entry.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

    if (!pc_p)
    {
        rs = pc_open_block(env_info->block_handle);
        if (rs)
            return rs;
        // CPMHACK - Use right method to get pc info instead of app info APIs
    	//rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
    	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
        if (rs)
            return rs;
    }

	/*
	 * Get the table entry.
	 */

	pc_elemp = pc_p->pc_po_xref[ip_ref->po].pc_elemp;
	if (ip_ref->subindex != 0) {
		pc_elemp += ip_ref->subindex;
	}

	*pc_elempp = pc_elemp;

	if (pc_valuepp != NULL) {
		if ((pc_elemp->pc_flags & PC_HEAD_ELEMENT) && (ip_ref->subindex == 0)) {
			return (PC_BAD_PARAM_REQUEST);
		}

		if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
			pc_valuep = &pc_elemp->pc_dev_value;
		} else if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			pc_valuep = &pc_elemp->pc_meth_value;
	
			/*
			 * Make sure some other method has not already updated it.  why???
			 */
	
			if (pc_valuep->pc_value_changed != 0
				&& pc_valuep->pc_method_tag != app_info->method_tag) {
				return (PC_METHOD_COLLISION);
			}

		} else {
			pc_valuep = &pc_elemp->pc_edit_value;
		}
		*pc_valuepp = pc_valuep;
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc find_param_value
 *  ShortDesc: Find the variable's Parameter Cache information
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc find_param_value searches the parameter cache, and returns 
 *		pointers to the requested param's table entry, and its value 
 *		structure.  It is used by the external routines 
 *		to find the appropriate place in the Parameter Cache.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		pc_p_ref - P_REF fields po or ID may be set if they are
 *						not specified.  This improves performance.
 *		pc_elempp - Where to return pointer to parameter cache element
 *		pc_valuepp - Where to return pointer to param value structure.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, 
 *		PC_METHOD_COLLISION, PC_INTERNAL_ERROR,
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_find_param_value(ENV_INFO *env_info, P_REF *pc_p_ref, PC_ELEM **pc_elempp,
	PC_VALUE **pc_valuepp)
{

	P_REF			ip_ref;
	int				rs;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_pc_p_ref_to_ip_ref(env_info->block_handle, pc_p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_find_param(env_info, &ip_ref, pc_elempp, pc_valuepp);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc return_value
 *  ShortDesc: Return a Parameter Cache value in an EVAL_VAR_VALUE structure.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc return_value takes a specified Parameter Cache element, and
 *		fills an EVAL_VAR_VALUE structure with information from that
 *		Parameter Cache element.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to read.
 *		pc_valuep - Pointer to value bucket in Parameter Cache element
 *					to read.
 *
 *  Outputs:
 *		param_value - EVAL_VAR_VALUE structure to return information
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_return_value(PC_ELEM *pc_elemp, PC_VALUE *pc_valuep, EVAL_VAR_VALUE *param_value)
{

	/*
	 * Return the value.
	 */

	param_value->type = pc_elemp->pc_type;
	param_value->size = pc_elemp->pc_size;
	switch (param_value->type) {
		case DDS_INTEGER:
            param_value->val.i = pc_valuep->pc_value.pc_long;
			break;

		case DDS_UNSIGNED:
		case DDS_INDEX:
		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
        case DDS_BOOLEAN_T:
            param_value->val.u = pc_valuep->pc_value.pc_ulong;
			break;

		case DDS_FLOAT:
            param_value->val.f = pc_valuep->pc_value.pc_float;
			break;

		case DDS_DOUBLE:
            param_value->val.d = pc_valuep->pc_value.pc_double;
			break;

		case DDS_ASCII:
		case DDS_PACKED_ASCII:
		case DDS_PASSWORD:
		case DDS_BITSTRING:
		case DDS_OCTETSTRING:
		case DDS_VISIBLESTRING:
		case DDS_EUC:
			/*
			 * Create a place for the string.
			 */

			param_value->val.s.str =
				malloc (pc_valuep->pc_value.pc_string.str_len + 1);
			if (param_value->val.s.str == (char *) NULL) {
				return (PC_NO_MEMORY);
			}

			(void)memcpy (param_value->val.s.str,
				pc_valuep->pc_value.pc_string.str_ptr,
				(int) pc_valuep->pc_value.pc_string.str_len);
			param_value->val.s.flags = FREE_STRING;
			param_value->val.s.len = pc_valuep->pc_value.pc_string.str_len;
			param_value->val.s.str[param_value->val.s.len] = 0;
			break;

		case DDS_DATE:
		case DDS_TIME:
        case DDS_TIME_VALUE:
		case DDS_DATE_AND_TIME:
		case DDS_DURATION:
			(void)memcpy (param_value->val.ca, pc_valuep->pc_value.pc_chararray,
				(int) param_value->size);
			break;

		default:
#ifdef DDIDE
			return PC_INTERNAL_ERROR;
#else
			CRASH_RET (PC_INTERNAL_ERROR);
#endif
			break;
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc get_action_value
 *  ShortDesc: Return the value from the special storage for pre/post
 *				read/write actions.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Pre/post actions for reads and writes must work in special
 *		temporary storage, rather than in the Parameter Cache itself.
 *		This is so that these actions don't leave "de-scaled" values
 *		in the Parameter Cache for others to mistakenly access.
 *
 *		pc get_action_value accesses the special storage created for
 *		these actions, and returns the variable information in an
 *		EVAL_VAR_VALUE structure.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		param_value - EVAL_VAR_VALUE structure to return information
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY,
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION,
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_action_value(ENV_INFO *env_info, EVAL_VAR_VALUE *param_value)
{

	PC_ELEM     *pc_elemp;
	PC_VALUE    *pc_valuep;
	int			rs;
	APP_INFO	*app_info;

	app_info = env_info->app_info;

	/*
	 * If there is no pointer to an action value, return an error.
	 */

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}
	ASSERT_RET (param_value != (EVAL_VAR_VALUE *) NULL, PC_BAD_POINTER);

	pc_elemp = (PC_ELEM *)(app_info->action_storage);

	/*
	 * Find the value pointer. The value pointer may be either the device
	 * value or the method value or the edit value.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		pc_valuep = &pc_elemp->pc_dev_value;
	}
	else if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
		pc_valuep = &pc_elemp->pc_meth_value;

		/*
		 * Make sure some other method has not already updated it.
		 */

		if (pc_valuep->pc_value_changed != 0
			&& pc_valuep->pc_method_tag != app_info->method_tag
			) {
			return (PC_METHOD_COLLISION);
		}

	} else {
		ASSERT_RET ((app_info->status_info & USE_MASK) == USE_EDIT_VALUE, 
					PC_BAD_STATUS_INFO);
		pc_valuep = &pc_elemp->pc_edit_value;
	}

	return (pc_return_value (pc_elemp, pc_valuep, param_value));
}


/**********************************************************************
 *
 *  Name: app func_get_param_value
 *  ShortDesc: Return the param value from the parameter cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		app func_get_param_value is a wrapper around
 *		pc get_param_value.  It is needed because DDS
 *		requires that this service be called
 *		app_func_get_param_value.
 *
 *  Inputs:
 *
 *		env_info - environment information
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		param_value - EVAL_VAR_VALUE structure to return information
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_READ_OVER_CHANGED_PARAM, PC_NO_MEMORY, 
 *		BLTIN_VALUE_NOT_SET, PC_PARAM_NOT_FOUND, PC_INTERNAL_ERROR,
 *		PC_METHOD_COLLISION and errors from cr_param_read.
 *
 *	Also:
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
app_func_get_param_value(ENV_INFO *env_info, OP_REF *op_ref, EVAL_VAR_VALUE *param_value)
{

	P_REF		pc_p_ref;
	int			rs;
    BLOCK_HANDLE save_blk_handle = env_info->block_handle;
    APP_INFO    *save_app_info = env_info->app_info;

	if (op_ref == (OP_REF *) NULL) {
		return (PC_BAD_POINTER);
	}

    if (op_ref->block_id)
    {
        ENV_INFO2 env2;
        // CPMHACK - Added app_info variable
        APP_INFO *app_info = NULL;

        /* This is a crossblock reference.  Store this block handle and
         * ask the application to give us the correct one. Put the new one
         * in the env_info parameter.
         */
        if (!app_func_get_block_handle)
            return PC_BAD_BLOCK_HANDLE;
        
        dds_env_to_env2(env_info, &env2);

        rs = (*app_func_get_block_handle)((ENV_INFO *)&env2, op_ref->block_id, op_ref->block_instance,
            &env_info->block_handle);
        if (rs)
            return rs;

        /* Find in the app_info so we can find the cache */
        // CPMHACK - Check if app_info for block already exists, 
        // If yes, use the block app info, otherwise application provided app info will be used
        rs = get_abt_app_info(env_info->block_handle, &app_info);
        if (app_info != NULL)
        {
            env_info->app_info = app_info;
        }
    }
	/*
	 * The conversion to pc_p_ref MUST be removed and the parameter
	 * op_ref changed to pc_p_ref if and when DDS is changed.
	 */

	pc_p_ref.id			= op_ref->id;
	pc_p_ref.subindex	= op_ref->subindex;
	pc_p_ref.type		= PC_ITEM_ID_REF;

    rs = pc_get_param_value (env_info, &pc_p_ref, param_value);

    /* Restore the block handle */
    env_info->block_handle = save_blk_handle;
    env_info->app_info = save_app_info;

	return (rs);
}

int pc_get_param_elem(ENV_INFO *env_info, P_REF *pc_p_ref, 	void **pc)
{

	P_REF		ip_ref;
	PC_VALUE	*pc_valuep;
	int			rs;

        PC_ELEM** pc_elemp = (PC_ELEM**)pc;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_find_param (env_info, &ip_ref, pc_elemp, &pc_valuep);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return rs;
}

/**********************************************************************
 *
 *  Name: pc get_param_value
 *  ShortDesc: Return the variable value from the parameter cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_param_value gets the Parameter Cache entry for a specified
 *		param/subindex.  If found, pc get_param_value returns the
 *		type, size, operational id, and value of the param.
 *      The value will be read if it has not
 *      already been read, or if it is a dynamic variable
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		param_value - EVAL_VAR_VALUE structure to return information
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_READ_OVER_CHANGED_PARAM, PC_NO_MEMORY, 
 *		BLTIN_VALUE_NOT_SET, PC_PARAM_NOT_FOUND, PC_INTERNAL_ERROR,
 *		PC_METHOD_COLLISION and from cr_param_read 
 *
 *	Also:
 *		pc put_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_value(ENV_INFO *env_info, P_REF *pc_p_ref, EVAL_VAR_VALUE *param_value)
{

	P_REF		ip_ref;
	PC_ELEM		*pc_elemp;
	PC_VALUE	*pc_valuep;
	int			rs;
	APP_INFO	*app_info;
	NET_TYPE	net_type;

	rs = get_nt_net_type(0, &net_type);
	if(rs != PC_SUCCESS){
		return (rs);
	}

	switch(net_type)
	{
	case NT_OFF_LINE:
		return (PC_SUCCESS);
		break;

	case NT_A:
		rs = pc_check_env_info (env_info);
		if (rs != PC_SUCCESS) {
			return (rs);
		}

		app_info = env_info->app_info;
		rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
		if (rs != PC_SUCCESS) {
			return (rs);
		}

		if (param_value == (EVAL_VAR_VALUE *) NULL) {
			return (PC_BAD_POINTER);
		}

		rs = pc_find_param (env_info, &ip_ref, &pc_elemp, &pc_valuep);
		if (rs != PC_SUCCESS) {
			return (rs);
		}

        /*
		 * Read the value if needed.  The value needs to be read if it has not
		 * already been read, or if it is a dynamic variable. However, if the
		 * value has been changed locally or is of class LOCAL, it is not
		 * necessary to do the read.
		 */

		if (pc_valuep->pc_value_changed == 0
			&& (pc_elemp->pc_flags & (PC_VAL_NOT_READ | PC_DYNAMIC_VAR)) != 0) {
			
            if (pc_elemp->pc_class & LOCAL_CLASS) {               
                LOGC_INFO(CPM_FF_DDSERVICE,"Local variables cannot read from the device");
			}
            else
            {
                if (app_info->status_info & NO_COMM_ON_GET) {
                    return (PC_VALUE_NOT_SET);
                }

                rs = pc_read_param (env_info, &ip_ref);
                if (rs != PC_SUCCESS) {
                    return (rs);
                }
            }
		}

		rs = pc_return_value (pc_elemp, pc_valuep, param_value);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
		break;

	default:
			return(CM_NO_COMM) ;
	}

	return (PC_SUCCESS);
}

/**********************************************************************
 *
 *  Name: pc pc_get_param_cache_value
 *  ShortDesc: Return the variable value from the parameter cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_param_value gets the Parameter Cache entry for a specified
 *		param/subindex.  If found, pc get_param_value returns the
 *		type, size, operational id, and value of the param.
 *      The value WILL NOT BE READ FROM DEVICE if it has not
 *      already been read, or if it is a dynamic variable
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of
 *						requested param.
 *
 *  Outputs:
 *		param_value - EVAL_VAR_VALUE structure to return information
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_READ_OVER_CHANGED_PARAM, PC_NO_MEMORY,
 *		BLTIN_VALUE_NOT_SET, PC_PARAM_NOT_FOUND, PC_INTERNAL_ERROR,
 *		PC_METHOD_COLLISION and from cr_param_read
 *
 *	Also:
 *		pc put_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/
int
pc_get_param_cache_value(ENV_INFO *env_info, P_REF *pc_p_ref, EVAL_VAR_VALUE *param_value)
{

    P_REF		ip_ref;
    PC_ELEM		*pc_elemp;
    PC_VALUE	*pc_valuep;
    int			rs;
    APP_INFO	*app_info;
    NET_TYPE	net_type;

    rs = get_nt_net_type(0, &net_type);
    if(rs != PC_SUCCESS){
        return (rs);
    }

    switch(net_type)
    {
    case NT_OFF_LINE:
        return (PC_SUCCESS);
        break;

    case NT_A:
        rs = pc_check_env_info (env_info);
        if (rs != PC_SUCCESS) {
            return (rs);
        }

        app_info = env_info->app_info;
        rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
        if (rs != PC_SUCCESS) {
            return (rs);
        }

        if (param_value == (EVAL_VAR_VALUE *) NULL) {
            return (PC_BAD_POINTER);
        }

        rs = pc_find_param (env_info, &ip_ref, &pc_elemp, &pc_valuep);
        if (rs != PC_SUCCESS) {
            return (rs);
        }

        rs = pc_return_value (pc_elemp, pc_valuep, param_value);
        if (rs != PC_SUCCESS) {
            return (rs);
        }
        break;

    default:
            return(CM_NO_COMM) ;
    }

    return (PC_SUCCESS);


}

/**********************************************************************
 *
 *  Name: pc read_param
 *  ShortDesc: Update the Parameter Cache with the current device value.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc read_param finds the Parameter Cache entry for a specified
 *		param/subindex.  pc read_param_value then updates
 *		the value by reading the current value of the device.  Note
 *		that it is valid to force a read for an entire record or array.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		ip_ref - the internal p_ref.
 *
 *  Outputs:
 *		The Parameter Cache element is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_READ_OVER_CHANGED_PARAM, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_read.
 *
 *	Also:
 *		pc put_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc get_param_value, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_read_param(ENV_INFO *env_info, P_REF *ip_ref)
{

	P_REF		p_ref;
	PC_ELEM		*pc_elem_endp;
	PC_ELEM		*pc_elemp;
	PC_ELEM		*pc_saved_elemp;
	PC_VALUE	*pc_subvaluep;
	PC_VALUE	*pc_valuep;
	int			rs;
	APP_INFO	*app_info;

	/*
	 * Get the Parameter Cache entry, and the param's value pointer.
	 */

	rs = pc_find_param (env_info, ip_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}
	app_info = env_info->app_info;

	/*
	 * Block waiting for param to be unlocked.
	 */

	rs = pc_check_lock (pc_elemp);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * If the whole param if being read, (record or array) and any sub-part has
	 * been changed it cannot be read as well.
	 */

	if ((pc_elemp->pc_flags & PC_HEAD_ELEMENT) &&
		(ip_ref->subindex == 0)) {
		pc_saved_elemp = pc_elemp;
		pc_elem_endp = pc_elemp + pc_elemp->pc_size;
		pc_elemp++;
		for (; pc_elemp < pc_elem_endp; pc_elemp++) {
			if (app_info->status_info & USE_EDIT_VALUE) {
				pc_subvaluep = &pc_elemp->pc_edit_value;
			}
			else if (app_info->status_info & USE_METH_VALUE) {
				pc_subvaluep = &pc_elemp->pc_meth_value;
			} else {
				/*
				 * The device value is not checked since the pc_value_changed
				 * flag is not supported for it.  It is protected by the lock.
				 */
				continue;
			}

			if (pc_subvaluep->pc_value_changed) {
				return (PC_NO_READ_OVER_CHANGED_PARAM);
			}
		}
		pc_elemp = pc_saved_elemp;
		pc_valuep = &pc_elemp->pc_dev_value;	/* the dev value is never changed */

	} else {
		if (app_info->status_info & USE_EDIT_VALUE) {
			pc_valuep = &pc_elemp->pc_edit_value;
		}
		else if (app_info->status_info & USE_METH_VALUE) {
			pc_valuep = &pc_elemp->pc_meth_value;
		} else {
			pc_valuep = &pc_elemp->pc_dev_value;
		}

		if (pc_valuep->pc_value_changed != 0) {
		/*
		 * If this single param value has been modified by another method, it
		 * can't be overwritten. Return an error.
		 */
			if ((app_info->status_info & USE_METH_VALUE) != 0
				&& pc_valuep->pc_method_tag != app_info->method_tag
				) {
				pc_release_lock (pc_elemp);
				return (PC_METHOD_COLLISION);
			}
			pc_release_lock (pc_elemp);
			return (PC_NO_READ_OVER_CHANGED_PARAM);
		}
	}

	/*
	 * If this value is of class LOCAL and has never been set, then return an
	 * error; otherwise just return - read does nothing for a LOCAL variable.
	 */
	
	if (pc_elemp->pc_class & LOCAL_CLASS) {
		if (pc_valuep->pc_value_changed == 0
			&& (pc_elemp->pc_flags & PC_VAL_NOT_READ) != 0) {
			return (PC_VALUE_NOT_SET);
		} else {
			return (PC_SUCCESS);
		}
	}

	/*
	 * Read the value.
	 */

	rs = pc_conv_ip_ref_to_p_ref (env_info->block_handle, ip_ref, &p_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = cr_param_read (env_info, &p_ref);
	if (rs != PC_SUCCESS) {
		app_info->env_returns.id_in_error = pc_elemp->pc_id;
		app_info->env_returns.po_in_error = pc_elemp->pc_po;
		app_info->env_returns.subindex_in_error = pc_elemp->pc_subindex;
		app_info->env_returns.member_id_in_error = pc_elemp->pc_member_id;
        pc_release_lock(pc_elemp);
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc read_param_value
 *  ShortDesc: Interface function to read a parameter
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc read_param_value sets up a call to pc read_param after checking
 *		caller supplied parameters.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		p_ref - P_REF specifies po or ID and subindex of the
 *						requested param.
 *
 *  Outputs:
 *		The Parameter Cache element is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_READ_OVER_CHANGED_PARAM, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_read
 *
 *	Also:
 *		pc put_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc get_param_value,
 *		pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_read_param_value(ENV_INFO *env_info, P_REF *p_ref)
{
	P_REF			ip_ref;
    int				rs;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_read_param (env_info, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc get_param_label
 *  ShortDesc: Get the label for the specified param.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_param_label searches the Parameter Cache for a specified
 *		param/subindex.  If found, pc get_param_label returns the
 *		param's label into the supplied buffer.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *		max_len - Length of the buffer
 *
 *  Outputs:
 *		label_buf - Pointer to buffer to store the label
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from 
 *		ddi clean_item and ddi get_item 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_label(ENV_INFO *env_info, P_REF *pc_p_ref, char **label_ptr)
{

	int		rs;
	PC_ELEM	*pc_elemp;

	/*
	 * Get the Parameter Cache entry, and the param's value pointer.
	 */

	rs = pc_find_param_value (env_info, pc_p_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (label_ptr == (char **) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * Get the label if it has never been fetched, or if it is dynamic.
	 */

	if ((pc_elemp->pc_flags & (PC_LABEL_NOT_READ | PC_LABEL_DYN)) != 0) {
		rs = pc_get_param_info (env_info, pc_elemp,
			(unsigned long) VAR_LABEL);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	*label_ptr = pc_elemp->pc_label;
	return (rs);
}


/**********************************************************************
 *
 *  Name: pc get_param_disp_format
 *  ShortDesc: Get the display format for the specified param.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc_get_param_edit_format searches the Parameter Cache for a specified
 *		param/subindex.  If found, pc get_param_disp_format returns the
 *		param's display format into the supplied buffer.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *		max_len - Length of the buffer
 *
 *  Outputs:
 *		format_buf - Pointer to buffer to store the display format
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from 
 *		ddi_clean_item,  and ddi get_item 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_disp_format(ENV_INFO *env_info, P_REF *pc_p_ref, char *format_buf,
	int max_len)
{
	int             rs;
	PC_ELEM      *pc_elemp;

	/*
	 * Get the Parameter Cache entry, and the param's value pointer.
	 */

	rs = pc_find_param_value (env_info, pc_p_ref, &pc_elemp, 
				(PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (format_buf == (char *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * Get the display format if it has never been fetched, or if it is
	 * dynamic.
	 */

	if ((pc_elemp->pc_flags & (PC_DISP_FMT_NOT_READ | PC_DISP_FMT_DYN)) != 0) {
		rs = pc_get_param_info(env_info, pc_elemp, (unsigned long)VAR_DISPLAY);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	/*
	 * If the specified buffer is too small, return an error
	 */

	if (pc_elemp->pc_disp_fmt == NULL) {
		*format_buf = 0;
	}
	else if ((size_t)max_len < strlen (pc_elemp->pc_disp_fmt) + 1) {
		rs = PC_BUFFER_TOO_SMALL;
	}
	else {
		(void)strcpy (format_buf, pc_elemp->pc_disp_fmt);
	}
	return (rs);
}


#if 0
/**********************************************************************
 *
 *  Name: pc get_param_edit_format
 *  ShortDesc: Get the edit format for the specified param.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_param_edit_format searches the Parameter Cache for a specified
 *		param/subindex.  If found, pc get_param_edit_format returns the
 *		param's edit format into the supplied buffer.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *		max_len - Length of the buffer
 *
 *  Outputs:
 *		format_buf - Pointer to buffer to store the edit format
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from 
 *		ddi_clean_item,  and ddi get_item 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_edit_format(ENV_INFO *env_info, P_REF *pc_p_ref, char *format_buf,
	int max_len)
{

	int		rs;
	PC_ELEM	*pc_elemp;

	/*
	 * Get the Parameter Cache entry, and the param's value pointer.
	 */

	rs = pc_find_param_value (env_info, pc_p_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (format_buf == (char *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * Get the edit format if it has never been fetched, or if it is
	 * dynamic.
	 */

	if ((pc_elemp->pc_flags & (PC_EDIT_FMT_NOT_READ | PC_EDIT_FMT_DYN)) != 0) {
		rs = pc_get_param_info (env_info, pc_elemp,
			(unsigned long) VAR_EDIT);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	/*
	 * If the specified buffer is too small, return an error
	 */

	if (pc_elemp->pc_edit_fmt == NULL) {
		*format_buf = 0;

	} else if ((size_t)max_len < strlen (pc_elemp->pc_edit_fmt) + 1) {
		rs = PC_BUFFER_TOO_SMALL;

	} else {
		(void)strcpy (format_buf, pc_elemp->pc_edit_fmt);
	}
	return (rs);
}
#endif


/**********************************************************************
 *
 *  Name: pc get_param_array_id
 *  ShortDesc: Get the ID of the associated array for the specified index param.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc get_param_array_id searches the Parameter Cache for a specified
 *		param/subindex.  If found and the element is an INDEX variable,
 *		pc get_param_array_id returns the ITEM_ID of the associated item_array.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		item_idp - Pointer to where to return the item_array's ITEM_ID
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from 
 *		ddi_clean_item and ddi get_item 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_array_id(ENV_INFO *env_info, P_REF	*pc_p_ref, ITEM_ID *item_idp)
{

	int		rs;
	PC_ELEM	*pc_elemp;

	/*
	 * Get the Parameter Cache entry, and the variable's value pointer.
	 */

	rs = pc_find_param_value (env_info, pc_p_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (item_idp == (ITEM_ID *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * If the variable is not type INDEX, return an error.
	 */

	if (pc_elemp->pc_type != DDS_INDEX) {
		return (PC_WRONG_DATA_TYPE);
	}

	/*
	 * Get the item_array's ID if it has never been fetched.
	 * Note: Indices are not dynamic.
	 */

	if ((pc_elemp->pc_flags & PC_ARRAY_ID_NOT_READ) != 0) {
		rs = pc_get_param_info (env_info, pc_elemp,
			(unsigned long) VAR_INDEX_ITEM_ARRAY);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	/*
	 * Return the ID.
	 */

	*item_idp = pc_elemp->pc_array_id;
	return (rs);
}


/**********************************************************************
 *
 *  Name: pc store_value
 *  ShortDesc: Store an EVAL_VAR_VALUE value into the Parameter Cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc store_value takes a specified EVAL_VAR_VALUE structure, and
 *		fills a Parameter cache element with information from that
 *		structure.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		param_value - EVAL_VAR_VALUE structure containing the value
 *
 *  Outputs:
 *		pc_elemp - Pointer to Parameter Cache element to update.
 *		pc_valuep - Pointer to value bucket in Parameter Cache element
 *					to update.
 *
 *  Returns:
 *		PC_SUCCESS, PC_WRONG_DATA_TYPE, PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_store_value(ENV_INFO *env_info, PC_ELEM *pc_elemp, PC_VALUE *pc_valuep,
	EVAL_VAR_VALUE *param_value)
{

	PC_VALUE	*edit_valuep = (PC_VALUE *) NULL, 
				*meth_valuep = (PC_VALUE *) NULL;
	int			type_ok;
	APP_INFO	*app_info;


	app_info = env_info->app_info;
	ASSERT_DBG ((pc_elemp->pc_flags & PC_HEAD_ELEMENT) == 0);

	/*
	 * If the put is into the device value, the edit value will also need
	 * to be modified (if it has not been changed).  And, since the method
	 * value tracks the edit value, if the edit value is changed, the method
	 * value needs to be modified (if it, the method value has not changed).
	 */

	if (app_info->status_info & USE_DEV_VALUE) {
		edit_valuep = &pc_elemp->pc_edit_value;
		if (edit_valuep->pc_value_changed) {
			edit_valuep = NULL;

		} else {
			meth_valuep = &pc_elemp->pc_meth_value;
			if (meth_valuep->pc_value_changed) {
				meth_valuep = NULL;
			}
		}

	} else if (app_info->status_info & USE_EDIT_VALUE) {
		meth_valuep = &pc_elemp->pc_meth_value;
		if (meth_valuep->pc_value_changed) {
			meth_valuep = NULL;
		}
	}

	/*
	 * Make sure the provided variable type matches the stored type.
	 */

	switch (param_value->type) {

		case DDS_UNSIGNED:
			type_ok = (pc_elemp->pc_type == DDS_UNSIGNED
				|| pc_elemp->pc_type == DDS_INDEX
				|| pc_elemp->pc_type == DDS_ENUMERATED
				|| pc_elemp->pc_type == DDS_BIT_ENUMERATED
                || pc_elemp->pc_type == DDS_BOOLEAN_T);
			break;

		case DDS_ASCII:
			type_ok = (pc_elemp->pc_type == DDS_ASCII
				|| pc_elemp->pc_type == DDS_PACKED_ASCII
				|| pc_elemp->pc_type == DDS_PASSWORD
				|| pc_elemp->pc_type == DDS_BITSTRING
				|| pc_elemp->pc_type == DDS_OCTETSTRING
				|| pc_elemp->pc_type == DDS_VISIBLESTRING
				|| pc_elemp->pc_type == DDS_EUC);
			break;

		case DDS_DATE_AND_TIME:
			type_ok = (pc_elemp->pc_type == DDS_DATE
				|| pc_elemp->pc_type == DDS_TIME
				|| pc_elemp->pc_type == DDS_TIME_VALUE
				|| pc_elemp->pc_type == DDS_DATE_AND_TIME
				|| pc_elemp->pc_type == DDS_DURATION);
			break;

		default:
			type_ok = (param_value->type == pc_elemp->pc_type);
	}
	if (!type_ok) {
		return (PC_WRONG_DATA_TYPE);
	}

	/*
	 * Update the parameter cache value.
	 */

	switch (param_value->type) {
		case DDS_INTEGER:
			pc_valuep->pc_value.pc_long = param_value->val.i;
			if (edit_valuep != (PC_VALUE *) NULL) {
				edit_valuep->pc_value.pc_long =
					pc_valuep->pc_value.pc_long;
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				meth_valuep->pc_value.pc_long =
					pc_valuep->pc_value.pc_long;
			}
			break;

		case DDS_UNSIGNED:
		case DDS_INDEX:
		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
        case DDS_BOOLEAN_T:
			pc_valuep->pc_value.pc_ulong = param_value->val.u;
			if (edit_valuep != (PC_VALUE *) NULL) {
				edit_valuep->pc_value.pc_ulong =
					pc_valuep->pc_value.pc_ulong;
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				meth_valuep->pc_value.pc_ulong =
					pc_valuep->pc_value.pc_ulong;
			}
			break;

		case DDS_FLOAT:
			pc_valuep->pc_value.pc_float = param_value->val.f;
			if (edit_valuep != (PC_VALUE *) NULL) {
				edit_valuep->pc_value.pc_float =
					pc_valuep->pc_value.pc_float;
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				meth_valuep->pc_value.pc_float =
					pc_valuep->pc_value.pc_float;
			}
			break;

		case DDS_DOUBLE:
			pc_valuep->pc_value.pc_double = param_value->val.d;
			if (edit_valuep != (PC_VALUE *) NULL) {
				edit_valuep->pc_value.pc_double =
					pc_valuep->pc_value.pc_double;
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				meth_valuep->pc_value.pc_double =
					pc_valuep->pc_value.pc_double;
			}
			break;

		case DDS_ASCII:
		case DDS_PACKED_ASCII:
		case DDS_PASSWORD:
		case DDS_BITSTRING:
		case DDS_OCTETSTRING:
		case DDS_VISIBLESTRING:
		case DDS_EUC:
			if (param_value->val.s.len != pc_valuep->pc_value.pc_string.str_len) {
				return (PC_WRONG_DATA_TYPE);
			}
			(void)memcpy (pc_valuep->pc_value.pc_string.str_ptr,
				param_value->val.s.str, (int) param_value->val.s.len);
			if (edit_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (edit_valuep->pc_value.pc_string.str_ptr,
					pc_valuep->pc_value.pc_string.str_ptr,
					(int) pc_valuep->pc_value.pc_string.str_len);
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (meth_valuep->pc_value.pc_string.str_ptr,
					pc_valuep->pc_value.pc_string.str_ptr,
					(int) pc_valuep->pc_value.pc_string.str_len);
			}
			break;

		case DDS_DATE:
		case DDS_TIME:
		case DDS_TIME_VALUE:
		case DDS_DATE_AND_TIME:
		case DDS_DURATION:
			if (param_value->size != pc_elemp->pc_size) {
				return (PC_WRONG_DATA_TYPE);
			}
			(void)memcpy (pc_valuep->pc_value.pc_chararray,
				param_value->val.ca, (int) param_value->size);
			if (edit_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (edit_valuep->pc_value.pc_chararray,
					pc_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
			}
			if (meth_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (meth_valuep->pc_value.pc_chararray,
					pc_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
			}
			break;

		default:
			CRASH_RET (PC_INTERNAL_ERROR);
	}
	if ((app_info->status_info & USE_DEV_VALUE) == 0) {
		pc_valuep->pc_value_changed++;
		pc_valuep->pc_method_tag = app_info->method_tag;
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc put_action_value
 *  ShortDesc: Update the value in the special storage for pre/post
 *				read/write actions.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Pre/post actions for reads and writes must work in special
 *		temporary storage, rather than in the Parameter Cache itself.
 *		This is so that these actions don't leave "de-scaled" values
 *		in the Parameter Cache for others to mistakenly access.
 *
 *		pc put_action_value accesses the special storage created for
 *		these actions, and updates the variable from information in an
 *		EVAL_VAR_VALUE structure.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		param_value - EVAL_VAR_VALUE structure with the new information
 *
 *  Outputs:
 *		The special storage is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_WRONG_DATA_TYPE, PC_INTERNAL_ERROR,
 *		PC_METHOD_COLLISION
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_put_action_value(ENV_INFO *env_info, EVAL_VAR_VALUE *param_value)
{

	PC_ELEM     *pc_elemp;
	PC_VALUE    *pc_valuep;
	int			rs;
	APP_INFO	*app_info;

	app_info = env_info->app_info;
	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (param_value == (EVAL_VAR_VALUE *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * If there is no pointer to an action value, return an error.
	 */
	ASSERT_RET (app_info->action_storage != NULL, PC_NO_ACTION_VALUE);

	pc_elemp = (PC_ELEM *)(app_info->action_storage);

	/*
	 * Find the value pointer. The value pointer is either the device value
	 * or the method value or the edit value.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		pc_valuep = &pc_elemp->pc_dev_value;
	}
	else if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
		pc_valuep = &pc_elemp->pc_meth_value;

		/*
		 * Make sure some other method has not already updated it.
		 * Note: This check may be done in the interpreter.
		 */

		if (pc_valuep->pc_value_changed != 0
			&& pc_valuep->pc_method_tag != app_info->method_tag) {
			return (PC_METHOD_COLLISION);
		}

	} else {
		ASSERT_RET ((app_info->status_info & USE_MASK) == USE_EDIT_VALUE,
					PC_BAD_STATUS_INFO);
		pc_valuep = &pc_elemp->pc_edit_value;
	}

	return (pc_store_value (env_info, pc_elemp, pc_valuep, param_value));
}


/**********************************************************************
 *
 *  Name: pc put_param
 *  ShortDesc: Store the param value into the parameter cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc put_param finds the entry in the Parameter Cache
 *		and if found, stores the supplied value for the param into 
 *		the Parameter Cache.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		ip_ref - internal P_REF specifies po or ID and subindex of 
 *						requested param.
 *		param_value - EVAL_VAR_VALUE structure supplying the new value.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE,
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION 
 *		
 *
 *	Also:
 *		pc get_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_put_param(ENV_INFO *env_info, P_REF *ip_ref, EVAL_VAR_VALUE *param_value)
{

	PC_ELEM		*pc_elemp;
	PC_VALUE	*pc_valuep;
	int			rs;

	/*
	 * Get the Parameter Cache entry, and the param's value pointer.
	 */

	rs = pc_find_param (env_info, ip_ref, &pc_elemp, &pc_valuep);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_store_value (env_info, pc_elemp, pc_valuep, param_value);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc put_param_value
 *  ShortDesc: Interface function to store the param value
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc put_param_value sets up to call pc put_param
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *		param_value - EVAL_VAR_VALUE structure supplying the new value.
 *
 *  Outputs:
 *		The Parameter Cache is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE,
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION 
 *		
 *
 *	Also:
 *		pc get_param_value, pc write_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_put_param_value(ENV_INFO *env_info, P_REF *pc_p_ref, EVAL_VAR_VALUE *param_value)
{

	P_REF		ip_ref;
	int			rs;

    char str[99];

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
        sprintf(str, "1: id=0x%lx  rs=%d", pc_p_ref->id, rs);
		return (rs);
	}

	rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
        sprintf(str, "1: id=0x%lx  rs=%d", pc_p_ref->id, rs);
		return (rs);
	}

	if (param_value == (EVAL_VAR_VALUE *) NULL) {
        sprintf(str, "1: id=0x%lx  rs=%d", pc_p_ref->id, rs);
		return (PC_BAD_POINTER);
	}

	rs = pc_put_param (env_info, &ip_ref, param_value);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc write_param
 *  ShortDesc: Send value(s) to the device
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc write_param updates a value or values in a device.
 *		It takes an optional EVAL_VAR_VALUE structure pointer.  If the
 *		structure pointer is non_null, the value in the param
 *		table is updated to reflect the provided value.
 *
 *		After possibly updating the Parameter Cache, pc write_param
 *		calls the comm. router (CR) to write the parameter
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		ip_ref - A P_REF structure which specifies po, ID and subindex of 
 *						requested param.
 *		param_value - EVAL_VAR_VALUE structure supplying the new value.  If
 *						this value is null, the values currently in the
 *						Parameter Cache are sent to the device.
 *
 *  Outputs:
 *		The Parameter Cache is updated if a param_value pointer is provided.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_write.
 *
 *	Also:
 *		pc get_param_value, pc put_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_write_param(ENV_INFO *env_info, P_REF *ip_ref, EVAL_VAR_VALUE *param_value)
{

	COMM_VALUE_LIST		comm_value_list;
	P_REF				p_ref;
	PC_ELEM				*pc_elemp, *pc_elem_endp;
	PC_ELEM				*pc_ep, *pc_e_endp;
	PC_ID_XREF			pc_id_key, *pc_idxp;
	PC					*pc_p;
	PC_RELATION_ELEM	*pc_relation_elemp, *pc_relation_elem_endp;
	PC_VALUE			*pc_valuep;
	int					rs;
	APP_INFO			*app_info;

	app_info = env_info->app_info;	

	rs = pc_find_param (env_info, ip_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * Block waiting for param to be unlocked. 
	 */

	rs = pc_check_lock (pc_elemp);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * If a new value is provided, update the Parameter Cache.
	 */

	if (param_value != (EVAL_VAR_VALUE *) NULL) {
		rs = pc_put_param (env_info, ip_ref, param_value);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	rs = pc_conv_ip_ref_to_p_ref (env_info->block_handle, ip_ref, &p_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * LOCAL class variables are read and written like device variables but
	 * without sending them to the device.
	 */

	if (pc_elemp->pc_class & LOCAL_CLASS) {
		comm_value_list.cmvl_count = 4;
		comm_value_list.cmvl_limit = comm_value_list.cmvl_count ;
		comm_value_list.cmvl_list =
				(COMM_VALUE *)calloc((unsigned)comm_value_list.cmvl_count,
				sizeof(COMM_VALUE));
		if (comm_value_list.cmvl_list == (COMM_VALUE *) NULL) {
			return (PC_NO_MEMORY);
		}

		rs = pc_get_param_value_list (env_info, &p_ref, &comm_value_list);
		if (rs != PS_SUCCESS) {
			free ((char *)comm_value_list.cmvl_list);
			return (rs);
		}

		rs = pc_put_param_value_list (env_info, &p_ref, &comm_value_list, 0);
		if (rs != PS_SUCCESS) {
			free ((char *)comm_value_list.cmvl_list);
			return (rs);
		}
		free ((char *)comm_value_list.cmvl_list);

	} else {

		/*
		 * Send the data
		 */

		rs = cr_param_write (env_info, &p_ref);
		if (rs != PC_SUCCESS) {
			app_info->env_returns.id_in_error = pc_elemp->pc_id;
			app_info->env_returns.po_in_error = pc_elemp->pc_po;
			app_info->env_returns.subindex_in_error = pc_elemp->pc_subindex;
			app_info->env_returns.member_id_in_error = pc_elemp->pc_member_id;
            pc_release_lock(pc_elemp);
			return (rs);
		}
	}

	/*
	 * Get a pointer to the Parameter Cache.  These pointers were validated
	 * in pc find_param_value, so they don't need to be re-validated.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	/*
	 * The entire record, array, or variable has been sent, and therefore
	 * the device value (and maybe edit or method value) needs to reflect
	 * the new value.
	 */

	if ((pc_elemp->pc_flags & PC_HEAD_ELEMENT) != 0) {
		pc_elem_endp = pc_elemp + pc_elemp->pc_size;
		pc_elemp++;

	} else {
		pc_elem_endp = pc_elemp;
	}

	do {

		/*
		 * If the value was not changed, or if the method value was
		 * changed by a different method, skip this entry.
		 */

		if (app_info->status_info & USE_METH_VALUE) {
			pc_valuep = &pc_elemp->pc_meth_value;
			if (pc_valuep->pc_method_tag != app_info->method_tag) {
				continue;
			}

		} else {
			pc_valuep = &pc_elemp->pc_edit_value;
		}
		if (pc_valuep->pc_value_changed == 0) {
			continue;
		}

		/*
		 * Search refresh relations here and mark any values on right
		 * side as stale.
		 */

		pc_relation_elemp = pc_elemp->pc_relation.pc_rel_list;
		pc_relation_elem_endp = pc_relation_elemp +
			pc_elemp->pc_relation.pc_rel_count;
		for (; pc_relation_elemp < pc_relation_elem_endp; pc_relation_elemp++) {

			/*
			 * Find the Parameter Cache entry corresponding to the
			 * relation if it hasn't already been found.
			 */

			pc_ep = pc_relation_elemp->pc_rel_pointer;
			if (pc_ep == (PC_ELEM *) NULL) {   /* why not use pc_find_param_value?*/
				pc_id_key.pc_id = pc_relation_elemp->pc_rel_id;
				pc_idxp = (PC_ID_XREF *) bsearch ((char *) &pc_id_key,
					(char *) pc_p->pc_id_xref, (unsigned) pc_p->pc_id_xref_count,
					sizeof (*pc_p->pc_id_xref), (CMP_FN_PTR) pc_id_compare);

				pc_relation_elemp->pc_rel_pointer =
					pc_p->pc_po_xref[pc_idxp->pc_po].pc_elemp + 
					pc_relation_elemp->pc_rel_subindex;
				pc_ep = pc_relation_elemp->pc_rel_pointer;
				ASSERT_RET (pc_ep != (PC_ELEM *) NULL, PC_INTERNAL_ERROR);
			}
			ASSERT_DBG (pc_ep->pc_id == pc_relation_elemp->pc_rel_id);
			ASSERT_DBG (pc_ep->pc_subindex == pc_relation_elemp->pc_rel_subindex);
			pc_ep->pc_flags |= PC_VAL_NOT_READ;

			/*
			 * If this is not an array or record, continue to the
			 * next relation.
			 */

			if ((pc_ep->pc_flags & PC_HEAD_ELEMENT) == 0) {
				continue;
			}

			/*
			 * Indicate that all of the record members or array
			 * elements are stale.
			 */

			pc_e_endp = pc_ep + pc_ep->pc_size;
			for (pc_ep++; pc_ep < pc_e_endp; pc_ep++) {
				pc_ep->pc_flags |= PC_VAL_NOT_READ;
			}
		}

		/*
		 * Indicate that this value has been sent by clearing the
		 * changed indicator.
		 */

		pc_valuep->pc_value_changed = 0;
		pc_valuep->pc_method_tag = 0;
	} while (++pc_elemp <= pc_elem_endp);

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc write_param_value
 *  ShortDesc: Send value(s) to the device
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc write_param_value updates a value or values in a device.
 *		It takes an optional EVAL_VAR_VALUE structure pointer.  If the
 *		structure pointer is non_null, the value in the param
 *		table is updated to reflect the provided value.
 *
 *		After possibly updating the Parameter Cache, pc write_param_value
 *		translates the Parameter Cache value(s) into a byte stream,
 *		and sends the byte stream to the device.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *		param_value - EVAL_VAR_VALUE structure supplying the new value.  If
 *						this value is null, the values currently in the
 *						Parameter Cache are sent to the device.
 *
 *  Outputs:
 *		The Parameter Cache is updated if a param_value pointer is provided.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_write.
 *
 *	Also:
 *		pc get_param_value, pc put_param_value, pc discard_put_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_write_param_value(ENV_INFO *env_info, P_REF *p_ref, EVAL_VAR_VALUE *param_value)
{

	P_REF				ip_ref;
	int					rs;
	APP_INFO			*app_info;

	app_info = env_info->app_info;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * It is not valid to "write" a device value to the device.
	 */

	if (app_info->status_info & USE_DEV_VALUE) {
		return (PC_NO_DEV_VALUE_WRITE);
	}

	rs = pc_write_param (env_info, &ip_ref, param_value);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc discard_put_param_value
 *  ShortDesc: Discard local modifications to a param
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc discard put param  value is called to discard a change to a param
 *		that a method or application (screen)  made.
 *
 *		When a method or screen modifies a param's value, it actually is
 *		just modifying a local table.  The device does not see any
 *		changes.  If it is then determined that the modification is
 *		not wanted, the changed value must be discarded.
 *		pc discard_put_param_value is then called to discard the value.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		The locally modified value of the param in the Parameter Cache is 
 *		discarded.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_INTERNAL_ERROR, 
 *		PC_METHOD_COLLISION 
 *
 *	Also:
 *		pc get_param_value, pc put_param_value, pc write_param_value,
 *		pc write_all_param_values, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_discard_put_param_value(ENV_INFO *env_info, P_REF *pc_p_ref)
{

	P_REF		ip_ref;
	int         rs;
	PC_ELEM     *pc_elemp;
	PC_VALUE    *pc_valuep;
	PC_VALUE    *init_valuep;
	PC_VALUE    *discard_valuep = (PC_VALUE *) 0;
	APP_INFO	*app_info;

	app_info = env_info->app_info;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_find_param (env_info, &ip_ref, &pc_elemp, &pc_valuep);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * If an edited value has not been modified, or a method value hasn't
	 * been modified by this method, return SUCCESS.
	 */

	if (pc_valuep->pc_value_changed == 0) {
		return (PC_SUCCESS);
	}

	if (app_info->status_info & USE_EDIT_VALUE) {
		ASSERT_DBG ((app_info->status_info & USE_MASK) == USE_EDIT_VALUE);
		init_valuep = &pc_elemp->pc_dev_value;
		discard_valuep = &pc_elemp->pc_edit_value;
		if (discard_valuep->pc_value_changed != 0) {
			discard_valuep = NULL;
		}

	} else {
		ASSERT_DBG ((app_info->status_info & USE_MASK) == USE_METH_VALUE);
		if (pc_valuep->pc_method_tag != app_info->method_tag) {
			return (PC_METHOD_COLLISION);
		}
		pc_valuep->pc_method_tag = 0;
		discard_valuep = &pc_elemp->pc_meth_value;
		init_valuep = &pc_elemp->pc_edit_value;
	}

	/*
	 * Discard the modified value.
	 */

	switch (pc_elemp->pc_type) {
		case DDS_INTEGER:
			pc_valuep->pc_value.pc_long = init_valuep->pc_value.pc_long;
			if (discard_valuep != (PC_VALUE *) NULL) {
				discard_valuep->pc_value.pc_long =
					init_valuep->pc_value.pc_long;
			}
			break;

		case DDS_UNSIGNED:
		case DDS_INDEX:
		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
        case DDS_BOOLEAN_T:
			pc_valuep->pc_value.pc_ulong = init_valuep->pc_value.pc_ulong;
			if (discard_valuep != (PC_VALUE *) NULL) {
				discard_valuep->pc_value.pc_ulong =
					init_valuep->pc_value.pc_ulong;
			}
			break;

		case DDS_FLOAT:
			pc_valuep->pc_value.pc_float = init_valuep->pc_value.pc_float;
			if (discard_valuep != (PC_VALUE *) NULL) {
				discard_valuep->pc_value.pc_float =
					init_valuep->pc_value.pc_float;
			}
			break;

		case DDS_DOUBLE:
			pc_valuep->pc_value.pc_double =
				init_valuep->pc_value.pc_double;
			if (discard_valuep != (PC_VALUE *) NULL) {
				discard_valuep->pc_value.pc_double =
					init_valuep->pc_value.pc_double;
			}
			break;

		case DDS_ASCII:
		case DDS_PACKED_ASCII:
		case DDS_PASSWORD:
		case DDS_BITSTRING:
		case DDS_OCTETSTRING:
		case DDS_VISIBLESTRING:
		case DDS_EUC:
			(void)memcpy (pc_valuep->pc_value.pc_string.str_ptr,
				init_valuep->pc_value.pc_string.str_ptr,
				(int) pc_valuep->pc_value.pc_string.str_len);
			if (discard_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (discard_valuep->pc_value.pc_string.str_ptr,
					init_valuep->pc_value.pc_string.str_ptr,
					(int) discard_valuep->pc_value.pc_string.str_len);
			}
			break;

		case DDS_DATE:
		case DDS_TIME:
		case DDS_TIME_VALUE:
		case DDS_DATE_AND_TIME:
		case DDS_DURATION:
			(void)memcpy (pc_valuep->pc_value.pc_chararray,
				init_valuep->pc_value.pc_chararray,
				(int) pc_elemp->pc_size);
			if (discard_valuep != (PC_VALUE *) NULL) {
				(void)memcpy (discard_valuep->pc_value.pc_chararray,
					init_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
			}
			break;

		default:
			CRASH_RET (PC_INTERNAL_ERROR);
	}
	pc_valuep->pc_value_changed = 0;

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc write_table
 *  ShortDesc: Send all values modified by a method to the device.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc write_table is called to write all of the param values that
 *		the method has modified to the device.
 *
 *		When a method modifies a param's value, it actually is
 *		just modifying a local table.  The device does not see any
 *		changes.  In order for a device to see the change, the method
 *		must send the value to the device.  It may do this by
 *		explicitly sending a specific param to the device, by
 *		using the send_on_exit builtin, which sends all modified
 *		values at method completion, or it may send all of the
 *		values it has modified, which sends all of the values it
 *		has modified to the device.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		Values in the device have been modified.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_write.
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_write_table(ENV_INFO *env_info)
{

	PC_ELEM		*pc_elemp, *pc_elem_endp;
	int			rs;
	P_REF		ip_ref;
	PC			*pc_p;
	APP_INFO	*app_info;

	app_info = env_info->app_info;

	/*
	 * The write command may not use the device values.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		return (PC_BAD_STATUS_INFO);
	}

	/*
	 * Search the parameter cache for any params with values that have
	 * been modified by this method.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	//rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}
	if (pc_p == (PC *) NULL) {
		return (PC_BAD_BLOCK_HANDLE);
	}

	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	for (; pc_elemp < pc_elem_endp; pc_elemp++) {

		/*
		 * If the method or edit value has not been changed, or if it was
		 * changed by another method or application, skip it.
		 */

		if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			if (pc_elemp->pc_meth_value.pc_value_changed == 0
				|| pc_elemp->pc_meth_value.pc_method_tag != 
				app_info->method_tag) {
				continue;
			}
		} else {
		/*
		 * CHANGE app_info->method_tag to app_infoappl_tag when it exists
		 */
			if (pc_elemp->pc_edit_value.pc_value_changed == 0
#if 0
				|| pc_elemp->pc_edit_value.pc_appl_tag != 
				app_info->method_tag
#endif
				) {
				continue;
			}
		}

		/*
		 * Write the value to the device.
		 */

		ip_ref.id		= pc_elemp->pc_id;
		ip_ref.po		= pc_elemp->pc_po;
		ip_ref.subindex	= pc_elemp->pc_subindex;
		ip_ref.type		= PC_PARAM_OFFSET_REF|PC_ITEM_ID_REF;

		rs = pc_write_param (env_info, &ip_ref, (EVAL_VAR_VALUE *) NULL);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc write_all_param_values
 *  ShortDesc: Send all values modified by a method to the device.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc write_all_param_values is called from the method builtin
 *		function send_all_values to send all of the param values that
 *		the method has modified to the device.
 *
 *		When a method modifies a param's value, it actually is
 *		just modifying a local table.  The device does not see any
 *		changes.  In order for a device to see the change, the method
 *		must send the value to the device.  It may do this by
 *		explicitly sending a specific param to the device, by
 *		using the send_on_exit builtin, which sends all modified
 *		values at method completion, or it may send all of the
 *		values it has modified.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		Values in the device have been modified.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_write.
 *
 *	Also:
 *		pc get_param_value, pc put_param_value, pc discard_put_param_value,
 *		pc write_param_value, pc method_close, pc read_param_value
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_write_all_param_values(ENV_INFO *env_info)
{

	PC				*pc_p;
	int				rs;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	rs = pc_write_table (env_info);
	return (rs);
}


/**********************************************************************
 *
 *  Name: pc param_value_modified
 *  ShortDesc: Determine if the method or edit value has changed.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc param_value_modified will check if the value for the
 *		usage specified in env_info by the status_info field has been
 *		modified.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p_ref - The parameter reference record, a ref to a param.
 *
 *  Outputs:
 *		A flag indicating if the specified param value has been modified.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_INTERNAL_ERROR, 
 *		PC_METHOD_COLLISION 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_param_value_modified(ENV_INFO *env_info, P_REF *pc_p_ref, int *flag)
{

	P_REF	ip_ref;
	PC_ELEM	*pc_elemp;
	int		rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_pc_p_ref_to_ip_ref (env_info->block_handle, pc_p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * param_modified may not use the device value.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		return (PC_BAD_STATUS_INFO);
	}

	if (flag == (int *) NULL) {
		return (PC_BAD_POINTER);
	}

    rs = pc_find_param (env_info, &ip_ref, &pc_elemp, (PC_VALUE **) NULL);
    if (rs != PC_SUCCESS) {
        return (rs);
    }

	ASSERT_DBG ((pc_elemp->pc_flags & PC_HEAD_ELEMENT) == 0);

	/*
	 * If the method value has not been changed, or if it was
	 * changed by another method, skip it.
	 */

	*flag = 0;
	if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
		if (pc_elemp->pc_meth_value.pc_value_changed != 0) {
			*flag = 1;
		}
	} else {								/* USE_EDIT_VALUE */
		if (pc_elemp->pc_edit_value.pc_value_changed != 0) {
			*flag = 1;
		}
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc any_param_value_modified
 *  ShortDesc: Determine if any method or edit values changed for a block.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc any_param_value_modified will check if any of the values of the
 *		usage specified in env_info by the status_info field.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p - Pointer to Parameter Cache to fill in
 *
 *  Outputs:
 *		The Parameter Cache is updated to discard the modified values.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_any_param_value_modified(ENV_INFO *env_info, int *flag)
{

	PC_ELEM	*pc_elemp, *pc_elem_endp;
	PC		*pc_p;
	int		rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (flag == (int *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * any_param_modified may not use the device value.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		return (PC_BAD_STATUS_INFO);
	}

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	/*
	 * Search the parameter cache for any params with values that have
	 * been modified.
	 */

	*flag = 0;
	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	for (; pc_elemp < pc_elem_endp; pc_elemp++) {

		/*
		 * If the method value has not been changed, or if it was
		 * changed by another method, skip it.
		 */

		if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			if (pc_elemp->pc_meth_value.pc_value_changed != 0) {
				*flag = 1;
				break;
			}
		} else {
			if (pc_elemp->pc_edit_value.pc_value_changed != 0) {
				*flag = 1;
				break;
			}
		}
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc save_table
 *  ShortDesc: Save all method modified values past the method completion
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc save_table is called to save all of the variable values that
 *		the method has modified past the completion of the method.
 *
 *		When a method modifies a variable's value, it actually is
 *		just modifying a local table.  The device does not see any
 *		changes.  This value modification is valid only for the
 *		duration of the method.  In order for the modified values to
 *		survive the completion of the method, the save_on_exit builtin
 *		must be used.  pc save_table is then called at method completion
 *		to save the values that the method modified.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache is updated to save the method modified values.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_save_table(ENV_INFO *env_info)
{

	PC_ELEM			*pc_elemp, *pc_elem_endp;
	PC				*pc_p;
	PC_VALUE		*meth_valuep, *edit_valuep;
#if 0
	unsigned short	method_tag;
#endif
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	ASSERT_RET ((app_info->status_info & USE_MASK) == USE_METH_VALUE,
				PC_BAD_STATUS_INFO);

	/*
	 * Search the parameter cache for any params with values that have
	 * been modified by this method.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	for (; pc_elemp < pc_elem_endp; pc_elemp++) {

		/*
		 * If the method value has not been changed, or if it was
		 * changed by another method, skip it.
		 */

		if (pc_elemp->pc_meth_value.pc_value_changed == 0
#if 0	/* Should be controlled by method interpreter */
			|| pc_elemp->pc_meth_value.pc_method_tag != method_tag
#endif
			) {
			continue;
		}

		/*
		 * Save the value into the edit value.
		 */

		meth_valuep = &pc_elemp->pc_meth_value;
		edit_valuep = &pc_elemp->pc_edit_value;
		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				edit_valuep->pc_value.pc_long =
					meth_valuep->pc_value.pc_long;
				break;

			case DDS_UNSIGNED:
			case DDS_INDEX:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
            case DDS_BOOLEAN_T:
				edit_valuep->pc_value.pc_ulong =
					meth_valuep->pc_value.pc_ulong;
				break;

			case DDS_FLOAT:
				edit_valuep->pc_value.pc_float =
					meth_valuep->pc_value.pc_float;
				break;

			case DDS_DOUBLE:
				edit_valuep->pc_value.pc_double =
					meth_valuep->pc_value.pc_double;
				break;

			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				ASSERT_DBG (meth_valuep->pc_value.pc_string.str_len ==
					edit_valuep->pc_value.pc_string.str_len);
				ASSERT_DBG (edit_valuep->pc_value.pc_string.str_ptr != NULL);
				ASSERT_DBG (meth_valuep->pc_value.pc_string.str_ptr != NULL);
				(void)memcpy (edit_valuep->pc_value.pc_string.str_ptr,
					meth_valuep->pc_value.pc_string.str_ptr,
					(int) meth_valuep->pc_value.pc_string.str_len);
				break;

			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				(void)memcpy (edit_valuep->pc_value.pc_chararray,
					meth_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
				break;

		default:
			CRASH_RET (PC_INTERNAL_ERROR);
		}
		edit_valuep->pc_value_changed++;
#if 0	/* Why is the method_tag for an edit_value set??? */
		edit_valuep->pc_method_tag = method_tag;
#endif
		meth_valuep->pc_value_changed = 0;
		meth_valuep->pc_method_tag = 0;
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc discard_all_put_params
 *  ShortDesc: Discard modified edit values.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc discard_all_put_params is called to discard all of the param values 
 *		that the application has modified.
 *
 *		When an application modifies a param's value via 
 *		put param, it actually is just modifying a local table.  The 
 *		device does not see any changes.  
 * 
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache's modified edit values are deleted.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_discard_all_put_params(ENV_INFO *env_info)
{
	int	rs;

	rs = pc_check_env_info (env_info);
	if (rs == PC_SUCCESS) {
		rs = pc_discard_table (env_info);
	}

	return (rs);
}


/**********************************************************************
 *
 *  Name: pc discard_meth_table
 *  ShortDesc: Discard modified method values.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc discard_meth_table is called to discard all of the param values 
 *		that the method has modified.
 *		The discard command must use method values.
 *
 *		When a method modifies a param's value via put param, it actually 
 *		is just modifying a local table.  The device does not see any 
 *		changes.  
 * 
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		The Parameter Cache's modified meth values are deleted.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_discard_meth_table(ENV_INFO *env_info)
{
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	ASSERT_RET ((app_info->status_info & USE_MASK) == USE_METH_VALUE, 
				PC_BAD_STATUS_INFO);

	rs = pc_discard_table (env_info);

	return (rs);
}


/**********************************************************************
 *
 *  Name: pc discard_table
 *  ShortDesc: Discard modified method or edit values.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc discard_table is called to discard all of the param values 
 *		that the method or application has modified.
 *
 *		When a method or application modifies a param's value via get and 
 *		put param, it actually is just modifying a local table.  The 
 *		device does not see any changes.  
 * 
 *		The value modification made by a method is valid only for the 
 *		duration of the method.  pc discard_table is then called
 *		at method completion to discard the values that the method
 *		modified and re-initialize them.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_p - Pointer to Parameter Cache to fill in
 *
 *  Outputs:
 *		The Parameter Cache is updated to discard the modified values.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_discard_table(ENV_INFO *env_info)
{

	PC_VALUE	*old_valuep, *new_valuep;
	PC			*pc_p;
	PC_ELEM		*pc_elemp, *pc_elem_endp;
	int			rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	/*
	 * discard may not use device values.
	 */

	if ((app_info->status_info & USE_MASK) == USE_DEV_VALUE) {
		return (PC_BAD_STATUS_INFO);
	}

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = get_abt_app_info(env_info->block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(env_info->block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	/*
	 * Search the parameter cache for any params with values that have
	 * been modified.
	 */

	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	for (; pc_elemp < pc_elem_endp; pc_elemp++) {

		/*
		 * If the method value has not been changed, or if it was
		 * changed by another method, skip it.
		 */

		if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			if (pc_elemp->pc_meth_value.pc_value_changed == 0
				|| pc_elemp->pc_meth_value.pc_method_tag != 
					app_info->method_tag) {
				continue;
			}
		} else {
			if (pc_elemp->pc_edit_value.pc_value_changed == 0) {
				continue;
			}
		}

		/*
		 * Discard the value by re-initializing it.
		 */
		if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			old_valuep = &pc_elemp->pc_meth_value;
			new_valuep = &pc_elemp->pc_edit_value;
		} else { 
			old_valuep = &pc_elemp->pc_edit_value;
			new_valuep = &pc_elemp->pc_dev_value;
		}
		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				old_valuep->pc_value.pc_long =
					new_valuep->pc_value.pc_long;
				break;

			case DDS_UNSIGNED:
			case DDS_INDEX:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
            case DDS_BOOLEAN_T:
				old_valuep->pc_value.pc_ulong =
					new_valuep->pc_value.pc_ulong;
				break;

			case DDS_FLOAT:
				old_valuep->pc_value.pc_float =
					new_valuep->pc_value.pc_float;
				break;

			case DDS_DOUBLE:
				old_valuep->pc_value.pc_double =
					new_valuep->pc_value.pc_double;
				break;

			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				(void)memcpy (old_valuep->pc_value.pc_string.str_ptr,
					new_valuep->pc_value.pc_string.str_ptr,
					(int) old_valuep->pc_value.pc_string.str_len);
				break;

			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				(void)memcpy (old_valuep->pc_value.pc_chararray,
					new_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
				break;

			default:
				CRASH_RET (PC_INTERNAL_ERROR);
		}
		old_valuep->pc_value_changed = 0;
		if ((app_info->status_info & USE_MASK) == USE_METH_VALUE) {
			pc_elemp->pc_meth_value.pc_method_tag = 0;
		} else {
			pc_elemp->pc_meth_value.pc_appl_tag = 0;
		}
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc method_close
 *  ShortDesc: Close up a method's entries in the parameter cache
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc method_close closes up a method's entries in the Parameter
 *		Cache.  When a method modifies a param's value, it actually is
 *		just modifying a local table.  The device does not see any
 *		changes.  This value modification is valid only for the
 *		duration of the method.  Therefore, when a method terminates,
 *		the values that the method modified may be sent the device),
 *		saved (the local modification will survive the
 *		method termination in the Parameter Cache, but still will not
 *		be sent to the device), or discarded.
 *
 *		pc method_close will handle the modified values in the
 *		manner specified by the option parameter, and then update
 *		the Parameter Cache to show that there is one less active
 *		method.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		option - send, save, or discard modified values.
 *
 *  Outputs:
 *		The values in the device may be updated, the Parameter Cache has
 *		one less active method.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_WRONG_DATA_TYPE, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION and from cr_param_write.
 *
 *	Also:
 *		pc_send_values
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_method_close(ENV_INFO *env_info, int option)
{
	int				rs;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if ((option != SEND_VALUES) && (option != SAVE_VALUES) &&
		(option != DISCARD_VALUES) && (option != NO_VALUES)) {
		return (PC_BAD_REQ_TYPE);
	}

	/*
	 * Handle the method modified values in the specified way.
	 */

	if (option & SEND_VALUES) {
		ASSERT_RET (((option & (SAVE_VALUES | DISCARD_VALUES)) == 0), 
						PC_INTERNAL_ERROR);
		rs = pc_write_table (env_info);
	}
	else if (option & SAVE_VALUES) {
		ASSERT_RET (((option & DISCARD_VALUES) == 0), PC_INTERNAL_ERROR);
		rs = pc_save_table (env_info);
	}
	else if (option & DISCARD_VALUES) {
		rs = pc_discard_meth_table (env_info);
	}
	else if (option & NO_VALUES) {
		rs = PC_SUCCESS;
	}
	else {
		CRASH_RET (PC_INTERNAL_ERROR);
	}

	if (rs != PC_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc close_block
 *  ShortDesc: Remove a parameter cache entry for a block
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		pc close_block frees up all of the memory associated with
 *		a parameter cache for a block.
 *
 *  Inputs:
 *		block_handle - The block handle returned by ct_block_open.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, 
 *
 *	Also:
 *		pc_open_block
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_close_block(BLOCK_HANDLE block_handle)
{

	PC			*pc_p;
	PC_ELEM		*pc_elemp, *pc_elem_endp;
	ITEM_TYPE	item_type;
	int			rs;

	/*
	 * Get the pointer into the Active Block Table, and validate the entry.
	 * Then see if the Parameter Caches are already set up.  If not, return
	 * successfully.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = get_abt_app_info(block_handle, (void **)(&pc_p));
	rs = get_abt_pc_info(block_handle, (void **)(&pc_p));
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	if (pc_p == (PC *) NULL) {
		return (PC_SUCCESS);
	}

	/*
	 * Go through the parameter cache entries.  Each string has allocated
	 * memory, and each entry may have allocated memory in the
	 * PC_RELATION, PC_ACTIONs, pc_label, pc_disp_fmt, and pc_edit_fmt.
	 */

	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	for (; pc_elemp < pc_elem_endp; pc_elemp++) {
		item_type = pc_elemp->pc_type;
		switch (item_type) {
			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				ASSERT_DBG (pc_elemp->pc_dev_value.pc_value.pc_string.str_ptr);
				free (pc_elemp->pc_dev_value.pc_value.pc_string.str_ptr);
				ASSERT_DBG (pc_elemp->pc_meth_value.pc_value.pc_string.str_ptr);
				free (pc_elemp->pc_meth_value.pc_value.pc_string.str_ptr);
				ASSERT_DBG (pc_elemp->pc_edit_value.pc_value.pc_string.str_ptr);
				free (pc_elemp->pc_edit_value.pc_value.pc_string.str_ptr);
				break;

			default:
				break;
		}

		if (pc_elemp->pc_relation.pc_rel_list) {
			ASSERT_DBG (pc_elemp->pc_relation.pc_rel_count != 0);
			free ((char *)(pc_elemp->pc_relation.pc_rel_list));
		}
		if (pc_elemp->pc_postread.pc_action_list) {
			ASSERT_DBG (pc_elemp->pc_postread.pc_action_count != 0);
			free ((char *)(pc_elemp->pc_postread.pc_action_list));
		}
		if (pc_elemp->pc_prewrite.pc_action_list) {
			ASSERT_DBG (pc_elemp->pc_prewrite.pc_action_count != 0);
			free ((char *)(pc_elemp->pc_prewrite.pc_action_list));
		}
		if (pc_elemp->pc_label) {
			free (pc_elemp->pc_label);
		}
		if (pc_elemp->pc_disp_fmt) {
			free (pc_elemp->pc_disp_fmt);
		}
#if 0		/* Add a generic pointer later for this and possible label & disp */
		if (pc_elemp->pc_edit_fmt) {
			free (pc_elemp->pc_edit_fmt);
		}
#endif
	}

	/*
	 * Free the parameter cache entries, id xref, and the parameter 
	 * cache structure itself.
	 */

	free ((char *)(pc_p->pc_list));
	free ((char *)(pc_p->pc_id_xref));
	free ((char *)(pc_p->pc_po_xref));
	free ((char *)(pc_p));

	/*
	 * Show that there is no longer any parameter cache tied to the
	 * block, and return.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
	// rs = set_abt_app_info(block_handle, (void *) NULL);
	rs = set_abt_pc_info(block_handle, (void *) NULL);
	if (rs != CM_SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}




/**********************************************************************
 *
 *  Name: pc get_action_list
 *  ShortDesc: Get the action list for a parameter.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Parameters may have pre or post read methods or pre or post write
 *		methods attached to them.  These methods (called "actions") are
 *		intended to be executed just before or after a read or a write.
 *		pc get_action_list attaches a list of methods for one of the
 *		above cases to a Parameter Cache entry.
 *
 *  Inputs:
 *		flat_actionp - Pointer into the FLAT_VAR actions area for the
 *				current action.
 *		p_ref - Reference structure for the param for which the
 *					action list is being sought.
 *		req_type - Type of action list request: either post-read or pre-write
 *
 *  Outputs:
 *		pc_actionp - Pointer into the Parameter Cache actions area for
 *				the current action.
 *		The actions area for the Parameter Cache entry is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, CM_COMM_ERR, CM_RESPONSE_CODE, 
 *		PC_INTERNAL_ERROR, and from ddi_clean_item and ddi get_item
 *
 *  NOTE: THE CALLER OF THIS ROUTINE MUST SAVE AND RESET THE CONTENTS
 *  	  OF ENV_INFO->ACTION_STORAGE BEFORE AND AFTER EACH CALL.
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_action_list(ENV_INFO *env_info, P_REF *p_ref, unsigned long req_type,
	PC_ACTION **pc_actionpp)
{

	unsigned long	info_type;
	P_REF			ip_ref;
	PC_ELEM			*pc_elemp;
	unsigned long	pc_get_info_flags;
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;	

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (pc_actionpp == (PC_ACTION **) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 * Get the Parameter Cache entry
	 */

	pc_elemp = (PC_ELEM *)(app_info->action_storage);
	pc_elemp += ip_ref.subindex;

	/*
	 * Get the action if it has never been fetched, or if it is dynamic.
	 */

	if (req_type == PC_PRE_WRITE) {
		info_type = VAR_PRE_WRITE_ACT;
		pc_get_info_flags = (unsigned long)(PC_METH_NOT_READ|PC_PREWRITE_DYN);
	} else if (req_type == PC_POST_READ) {
		info_type = VAR_POST_READ_ACT;
		pc_get_info_flags = (unsigned long)(PC_METH_NOT_READ|PC_POSTREAD_DYN);
	} else {
		return (PC_BAD_REQ_TYPE);
	}

	if ((pc_elemp->pc_flags & pc_get_info_flags) != 0) {
		rs = pc_get_param_info (env_info, pc_elemp, info_type);
		if (rs != SUCCESS) {
			if (app_info->env_returns.comm_err) {
				ASSERT_DBG (!app_info->env_returns.response_code);
				return (CM_COMM_ERR);
			}
			if (app_info->env_returns.response_code) {
				app_info->status_info |= HIDDEN_RCODE_ERROR;
				return (CM_RESPONSE_CODE);
			}
			return (rs);
		}
	}

	if (req_type == (unsigned long)PC_PRE_WRITE) {
		*pc_actionpp = &(pc_elemp->pc_prewrite);
	} else {
		*pc_actionpp = &(pc_elemp->pc_postread);
	}

	/*
	 * NOTE: THE CALLER OF THIS ROUTINE MUST SAVE AND RESET THE CONTENTS
	 *	     OF ENV_INFO->ACTION_STORAGE BEFORE AND AFTER EACH CALL.
	 */
	app_info->action_storage = (void *)pc_elemp;

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc add_action_list
 *  ShortDesc: Attach action list to Parameter Cache entry.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Parameters may have pre or post read methods or pre or post write
 *		methods attached to them.  These methods (called "actions") are
 *		intended to be executed just before or after a read or a write.
 *		pc add_action_list attaches a list of methods for one of the
 *		above cases to a Parameter Cache entry.
 *
 *  Inputs:
 *		flat_actionp - Pointer to a list of method ids to add to the action list
 *  Outputs:
 *		pc_actionp - The actions area for the Parameter Cache entry is updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_add_action_list(ITEM_ID_LIST *flat_actionp, PC_ACTION *pc_actionp)
{
	int				needed_size;

	/*
	 *	If there are no methods to add, free any existing list, and return.
	 */

	if (flat_actionp->count == 0) {
		if (pc_actionp->pc_action_count) {
			ASSERT_RET (pc_actionp->pc_action_list != NULL, PC_INTERNAL_ERROR);
			free ((char *)pc_actionp->pc_action_list);
			pc_actionp->pc_action_list = (ITEM_ID *) NULL;
			pc_actionp->pc_action_count = 0;
		}
		return (PC_SUCCESS);
	}

	/*
	 *	Create a space for the list.
	 */

	needed_size = flat_actionp->count * sizeof (*pc_actionp->pc_action_list);
	if (pc_actionp->pc_action_list) {
		pc_actionp->pc_action_list = (ITEM_ID *)
			realloc ((char *)pc_actionp->pc_action_list, (unsigned)needed_size);
	} else {
		pc_actionp->pc_action_list = (ITEM_ID *) malloc ((unsigned)needed_size);
	}
	if (pc_actionp->pc_action_list == NULL) {
		return (PC_NO_MEMORY);
	}
	pc_actionp->pc_action_count = flat_actionp->count;

	/*
	 *	Transfer the data.
	 */

	(void)memcpy ((char *)pc_actionp->pc_action_list, 
			(char *)flat_actionp->list, needed_size);
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc get_param_info
 *  ShortDesc: Find the actions, edit format, array id  and label for 
 *				a parameter.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Parameters may have post-read methods or pre-write methods 
 *		attached to them.  These methods (called "actions") are
 *		intended to be executed just before or after a read or a write.
 *		pc get_param_info calls DDS routines to search the DDL to find 
 *		the appropriate methods for the current parameter, as well as 
 *		the label, edit format and associated array id for the parameter.
 *		It then attaches a list of those methods to the Parameter Cache
 *		entry by calling pc_add_action_list.  The label, array ID, and
 *		edit format are also attached to the Parameter Cache entry.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *		pc_elemp - Pointer to Parameter Cache element to read.
 *		req_mask - mask value specifying which of the four actions
 *						are desired.
 *
 *  Outputs:
 *		The appropriate Parameter Cache entry field is updated.
 *
 *  Returns:
 *		PC_NO_MEMORY, PC_INTERNAL_ERROR, and from ddi_clean_item and
 *		ddi get_item
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

#define METHOD_REQUEST	(VAR_POST_READ_ACT | VAR_PRE_WRITE_ACT )

static int
pc_get_param_info(ENV_INFO *env_info, PC_ELEM *pc_elemp, unsigned long req_mask)
{

	FLAT_VAR			flat_var;
	DDI_ITEM_SPECIFIER	item_spec;
	int					read_all_methods;
	int					rs, rs2, old_length;
	DDI_BLOCK_SPECIFIER block_spec;
	DDI_GENERIC_ITEM	generic_item;
	APP_INFO 			*app_info;

	app_info = env_info->app_info;	

	block_spec.type = DDI_BLOCK_HANDLE;
	block_spec.block.handle = env_info->block_handle;

	/*
	 *	Make sure this is not an array or record "head".
	 */

	if (pc_elemp->pc_flags & PC_HEAD_ELEMENT) {
		return (PC_INTERNAL_ERROR);
	}

	/*
	 *	If the methods haven't been read, if any method is
	 *	requested, try to read all the methods.
	 */

	read_all_methods = 0;
	if (  (req_mask & METHOD_REQUEST) != 0
	   && (pc_elemp->pc_flags & PC_METH_NOT_READ) != 0) {
		req_mask |= METHOD_REQUEST;
		read_all_methods = 1;
	}

	/*
	 *	Clear the space for ddi to return the info.
	 */
	(void)memset ((char *)&generic_item, 0, sizeof (DDI_GENERIC_ITEM)); 
	(void)memset ((char *)&flat_var, 0, sizeof (flat_var));

	/*
	 *	Call to get the method IDs and/or the label.
	 */

	if (pc_elemp->pc_subindex) {
		item_spec.type = DDI_ITEM_ID_SI;
		item_spec.subindex = pc_elemp->pc_subindex;
	} else {
		item_spec.type = DDI_ITEM_ID;
		item_spec.subindex = 0;
	}
	item_spec.item.id = pc_elemp->pc_id;
	app_info->env_returns.dds_errors.count = 0;

	rs = ddi_get_item(&block_spec,&item_spec,env_info,req_mask,&generic_item);

	if (rs != SUCCESS) {
		if (rs == DDL_CHECK_RETURN_LIST) {
			rs = generic_item.errors.list[0].rc;
			if ((rs == SUCCESS) || 
				(app_info->env_returns.dds_errors.count == 0)) {
				CRASH_DBG ();
				/*NOTREACHED*/
				rs = PC_INTERNAL_ERROR;
			}
		}
		rs2 = ddi_clean_item (&generic_item);

		return (rs);
	}
	
	flat_var = *(FLAT_VAR *)generic_item.item;

	/*
	 *	Update the Parameter Cache with the information for whichever
	 *	methods have been requested.
	 *  Note: The order of the cases in the switch for methods cannot be changed
	 *		without changing the use of read_all_methods.
	 */

	if (read_all_methods) {
		req_mask = VAR_POST_READ_ACT;
	}

	switch (req_mask) {
		/*
		 *	Post-read method
		 */

		case VAR_POST_READ_ACT:
			if ((flat_var.masks.attr_avail & VAR_POST_READ_ACT) == 0) {
				CRASH_DBG ();
				/*NOTREACHED*/
				rs = PC_INTERNAL_ERROR;
				break;
			}
			rs = pc_add_action_list (&flat_var.actions->post_read_act, 
					&pc_elemp->pc_postread);
			if (rs != PC_SUCCESS) {
				break;
			}
			pc_elemp->pc_flags &= ~PC_METH_NOT_READ;
			if (flat_var.masks.dynamic & VAR_POST_READ_ACT) {
				pc_elemp->pc_flags |= PC_POSTREAD_DYN;
			}

		if (!read_all_methods) {
			break;
		}

		/*
		 *	Pre-write method
		 */

		case VAR_PRE_WRITE_ACT:
			if ((flat_var.masks.attr_avail & VAR_PRE_WRITE_ACT) == 0) {
				CRASH_DBG ();
				/*NOTREACHED*/
				rs = PC_INTERNAL_ERROR;
				break;
			}
			rs = pc_add_action_list (&flat_var.actions->pre_write_act, 
					&pc_elemp->pc_prewrite);
			if (rs != PC_SUCCESS) {
				break;
			}
			pc_elemp->pc_flags &= ~PC_METH_NOT_READ;
			if (flat_var.masks.dynamic & VAR_PRE_WRITE_ACT) {
				pc_elemp->pc_flags |= PC_PREWRITE_DYN;
			}
			break;

		/*
		 *	Label.
		 */

		case VAR_LABEL:
			if ((flat_var.masks.attr_avail & VAR_LABEL) == 0) {
				CRASH_DBG ();
				/*NOTREACHED*/
				rs = PC_INTERNAL_ERROR;
				break;
			}
			if (pc_elemp->pc_label) {
				old_length = strlen (pc_elemp->pc_label);
				if (old_length < flat_var.label.len) {
					pc_elemp->pc_label = realloc (pc_elemp->pc_label,
							flat_var.label.len + 1);
					if (pc_elemp->pc_label == NULL) {
						rs = PC_NO_MEMORY;
						break;
					}
				}
			} else {
				pc_elemp->pc_label = malloc (flat_var.label.len + 1);
				if (pc_elemp->pc_label == NULL) {
					rs = PC_NO_MEMORY;
					break;
				}
			}
			(void)strcpy (pc_elemp->pc_label, flat_var.label.str);
			pc_elemp->pc_flags &= ~PC_LABEL_NOT_READ;
			if (flat_var.masks.dynamic & VAR_LABEL) {
				pc_elemp->pc_flags |= PC_LABEL_DYN;
			}
			break;

		/*
		 *	Display format.
		 */

		case VAR_DISPLAY:
			if ((flat_var.masks.attr_avail & VAR_DISPLAY) == 0) {
				if (pc_elemp->pc_disp_fmt) {
					free (pc_elemp->pc_disp_fmt);
					pc_elemp->pc_disp_fmt = (char *) NULL;
				}
			} else {
				if (pc_elemp->pc_disp_fmt) {
					old_length = strlen (pc_elemp->pc_disp_fmt);
					if (old_length < flat_var.display.len) {
						pc_elemp->pc_disp_fmt = realloc 
							(pc_elemp->pc_disp_fmt, flat_var.display.len + 1);
						if (pc_elemp->pc_disp_fmt == NULL) {
							rs = PC_NO_MEMORY;
							break;
						}
					}
				} else {
					pc_elemp->pc_disp_fmt = malloc (flat_var.display.len + 1);
					if (pc_elemp->pc_disp_fmt == NULL) {
						rs = PC_NO_MEMORY;
						break;
					}
				}
				(void)strcpy (pc_elemp->pc_disp_fmt, flat_var.display.str);
			}
			pc_elemp->pc_flags &= ~PC_DISP_FMT_NOT_READ;
			if (flat_var.masks.dynamic & VAR_DISPLAY) {
				pc_elemp->pc_flags |= PC_DISP_FMT_DYN;
			}
			break;

	#if 0
		/*
		 *	Edit format.
		 */

		case VAR_EDIT:
			if ((flat_var.masks.attr_avail & VAR_EDIT) == 0) {
				if (pc_elemp->pc_edit_fmt) {
					free (pc_elemp->pc_edit_fmt);
					pc_elemp->pc_edit_fmt = (char *)0;
				}
			} else {
				if (pc_elemp->pc_edit_fmt) {
					old_length = strlen (pc_elemp->pc_edit_fmt);
					if (old_length < flat_var.edit.len) {
						pc_elemp->pc_edit_fmt = realloc 
							(pc_elemp->pc_edit_fmt, flat_var.edit.len + 1);
						if (pc_elemp->pc_edit_fmt == NULL) {
							rs = PC_NO_MEMORY;
							break;
						}
					}
				} else {
					pc_elemp->pc_edit_fmt = malloc (flat_var.edit.len + 1);
					if (pc_elemp->pc_edit_fmt == NULL) {
						rs = PC_NO_MEMORY;
						break;
					}
				}
				(void)strcpy (pc_elemp->pc_edit_fmt, flat_var.edit.str);
			}
			pc_elemp->pc_flags &= ~PC_EDIT_FMT_NOT_READ;
			if (flat_var.masks.dynamic & VAR_EDIT) {
				pc_elemp->pc_flags |= PC_EDIT_FMT_DYN;
			}
			break;
	#endif

		/*
		 *	ITEM_ID of the associated array (for index types only)
		 */

		case VAR_INDEX_ITEM_ARRAY:
			if (  (flat_var.masks.attr_avail & VAR_INDEX_ITEM_ARRAY) == 0
			   || (flat_var.masks.dynamic & VAR_INDEX_ITEM_ARRAY) != 0
			   || pc_elemp->pc_type != DDS_INDEX
			   ) {
				CRASH_DBG ();
				/*NOTREACHED*/
				rs = PC_INTERNAL_ERROR;
				break;
			}
			pc_elemp->pc_array_id = flat_var.index_item_array;
			pc_elemp->pc_flags &= ~PC_ARRAY_ID_NOT_READ;
			break;

		default:
			rs = PC_BAD_REQ_TYPE;
			break;
	}

	/*
	 *	Clean up the flat structure.
	 */

	rs2 = ddi_clean_item (&generic_item);
	if (rs != SUCCESS) {
		return (rs);
	}
	return (rs2);
}


/**********************************************************************
 *
 *  Name: pc write_values_to_storage
 *  ShortDesc: Write device data into the action storage
 *
 *  Include:
 *		pc_loc.h
 *		cm_lib.h
 *
 *  Description:
 *		pc write_values_to_storage writes values from the value struct
 *		to the device value in the action storage.
 *
 *  Inputs:
 *      env_info - The ENV_INFO structure containing information
 *                      about the current request.
 *		cm_value_listp - The data returned from the device.
 *
 *  Outputs:
 *		pc_elemp - Pointer into Parameter Cache where to store the data.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_write_values_to_storage(ENV_INFO *env_info, COMM_VALUE_LIST *cm_value_listp)
{

	COMM_VALUE		*cm_valuep, *cm_value_endp;
	PC_ELEM			*pc_elemp;
	PC_VALUE		*pc_valuep;
	int				pc_count;
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (cm_value_listp == (COMM_VALUE_LIST *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 *	Make sure the value list count corresponds to the count in
	 *	the Parameter Cache.
	 */

	pc_elemp = (PC_ELEM *)(app_info->action_storage);
	if (pc_elemp->pc_flags & PC_HEAD_ELEMENT) {
		pc_count = pc_elemp++->pc_size;
	} else {
		pc_count = 1;
	}
	ASSERT_RET (pc_count == cm_value_listp->cmvl_count, PC_INTERNAL_ERROR);

	/*
	 *	Step through the value list, updating the Parameter Cache.
	 */

	cm_valuep = cm_value_listp->cmvl_list;
	cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;
	for ( ; cm_valuep < cm_value_endp; cm_valuep++, pc_elemp++) {

		/*
		 *	Verify that the value list type and size match the
		 *	Parameter Cache's.
		 */
		if (pc_elemp->pc_size != cm_valuep->cm_size) { 
			if (pc_elemp->pc_type == DDS_EUC) {
				if (pc_elemp->pc_size * 2 != cm_valuep->cm_size) {
					CRASH_DBG ();
					/*NOTREACHED*/
					return (PC_INTERNAL_ERROR);
				} 
			} else if (pc_elemp->pc_type == DDS_PACKED_ASCII) {
				if (((pc_elemp->pc_size+1)*3)/4 != cm_valuep->cm_size) {
					CRASH_DBG ();
					/*NOTREACHED*/
					return (PC_INTERNAL_ERROR);
				} 
			} else if ( (pc_elemp->pc_type == DDS_BITSTRING) ||
						(pc_elemp->pc_type == DDS_OCTETSTRING) ||
						(pc_elemp->pc_type == DDS_VISIBLESTRING) ) {
				if (pc_elemp->pc_size != cm_valuep->cm_size * 8) {
					CRASH_DBG ();
					/*NOTREACHED*/
					return (PC_INTERNAL_ERROR);
				} 
			} else {
				CRASH_DBG ();
				/*NOTREACHED*/
				return (PC_INTERNAL_ERROR);
			}	
		}

        /* CPM Hack : Code addition by Utthunga;
           Added BIT_STRING type along with UNSIGNED
           since BIT_STRING is handling separately in PC
        */
		if (pc_elemp->pc_type != cm_valuep->cm_type) {
			if ((cm_valuep->cm_type != DDS_UNSIGNED) &&
                (cm_valuep->cm_type != DDS_ASCII) && 
                ((cm_valuep->cm_type == DDS_OCTETSTRING) &&
                 (pc_elemp->pc_type != DDS_ASCII) &&
                 (pc_elemp->pc_type != DDS_PASSWORD))) {
				CRASH_DBG ();
				/*NOTREACHED*/
				return (PC_INTERNAL_ERROR);
			} else if ((cm_valuep->cm_type == DDS_UNSIGNED) &&
				(pc_elemp->pc_type != DDS_UNSIGNED) &&
				(pc_elemp->pc_type != DDS_ENUMERATED) &&
                (pc_elemp->pc_type != DDS_BIT_ENUMERATED) &&
                (pc_elemp->pc_type != DDS_BITSTRING) &&
				(pc_elemp->pc_type != DDS_INDEX) &&
                (pc_elemp->pc_type != DDS_BOOLEAN_T)) {
				CRASH_DBG ();
				/*NOTREACHED*/
				return (PC_INTERNAL_ERROR);
			} else if ((cm_valuep->cm_type == DDS_ASCII) &&
				(pc_elemp->pc_type != DDS_ASCII) &&
                (pc_elemp->pc_type != DDS_PACKED_ASCII) &&
				(pc_elemp->pc_type != DDS_OCTETSTRING) &&
				(pc_elemp->pc_type != DDS_VISIBLESTRING) &&
				(pc_elemp->pc_type != DDS_PASSWORD)) {
				CRASH_DBG ();
				/*NOTREACHED*/
				return (PC_INTERNAL_ERROR);
			}
		}

		/*
		 *	Transfer the value to the device "bucket".
		 *  Note: in FMS DDS type and CM type are identical.
		 */

		pc_valuep = &pc_elemp->pc_dev_value;
		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				pc_valuep->pc_value.pc_long = cm_valuep->cm_val.cm_long;
				break;
			case DDS_UNSIGNED:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
			case DDS_INDEX:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				pc_valuep->pc_value.pc_ulong = cm_valuep->cm_val.cm_ulong;
				break;
			case DDS_BOOLEAN_T:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				pc_valuep->pc_value.pc_ulong = cm_valuep->cm_val.cm_boolean;
				break;
			case DDS_FLOAT:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				ASSERT_DBG (pc_elemp->pc_size == 4);
				pc_valuep->pc_value.pc_float = cm_valuep->cm_val.cm_float;
				break;
			case DDS_DOUBLE:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				ASSERT_DBG (pc_elemp->pc_size == 8);
				pc_valuep->pc_value.pc_double = cm_valuep->cm_val.cm_double;
				break;
			case DDS_ASCII:
			case DDS_PACKED_ASCII:
            case DDS_PASSWORD:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
            case DDS_EUC:
                ASSERT_DBG (pc_valuep->pc_value_changed == 0);
                ASSERT_DBG (pc_valuep->pc_value.pc_string.str_ptr != NULL
                                   && cm_valuep->cm_val.cm_stream != NULL);
                (void)memcpy (pc_valuep->pc_value.pc_string.str_ptr,
                    cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;
            case DDS_BITSTRING:
                ASSERT_DBG (pc_valuep->pc_value_changed == 0);
                ASSERT_DBG (pc_valuep->pc_value.pc_string.str_ptr != NULL
                                       && cm_valuep->cm_val.cm_ca != NULL);
                if (cm_valuep->cm_type == DDS_BITSTRING)
                {
                    (void)memcpy (pc_valuep->pc_value.pc_string.str_ptr,
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                }
                else
                {
                    (void)memcpy (pc_valuep->pc_value.pc_string.str_ptr,
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                }
                break;
			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				ASSERT_DBG (pc_valuep->pc_value_changed == 0);
				(void)memcpy (pc_valuep->pc_value.pc_chararray,
					cm_valuep->cm_val.cm_ca, (int)pc_elemp->pc_size);
				break;
			default:
				CRASH_RET (PC_INTERNAL_ERROR);
		}
	}

	return (PC_SUCCESS);
}


/*
 * pc_write_locals_to_storage
 * Write the locals from the device bucket into the "method bucket" of the
 * parameter cache.
 */
static int
pc_copy_locals_to_storage(ENV_INFO *env_info)
{

	PC_ELEM			*pc_elemp, *pc_endp;
	PC_VALUE		*pc_valuep;
	int				pc_count;
	int				rs;
	APP_INFO		*app_info;
    PC              *pc;
	app_info = env_info->app_info;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 *	Make sure the value list count corresponds to the count in
	 *	the Parameter Cache.
	 */

    // CPMHACK - Use right method to get pc info instead of app info APIs
    //get_abt_app_info(env_info->block_handle, (void **)&pc);
    get_abt_pc_info(env_info->block_handle, (void **)&pc);

	pc_elemp = (PC_ELEM *)(pc->pc_list);
	pc_count = pc->pc_count;
    pc_endp = pc_elemp + pc_count;

	/*
	 *	Step through the value list, updating the Parameter Cache.
	 */

	for ( ; pc_elemp < pc_endp; pc_elemp++) {
        /* If this is not a LOCAL, don't do anything */
		if (!(pc_elemp->pc_class & LOCAL_CLASS))
            continue;
		/*
		 *	Transfer the value to the method "bucket".
		 *  Note: in FMS DDS type and CM type are identical.
		 */
		pc_valuep = &pc_elemp->pc_meth_value;
        pc_valuep->pc_value_changed++;
        pc_valuep->pc_method_tag = app_info->method_tag;
        pc_elemp->pc_flags &= ~PC_VAL_NOT_READ;

		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				pc_valuep->pc_value.pc_long = pc_elemp->pc_dev_value.pc_value.pc_long;
				break;
			case DDS_UNSIGNED:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
			case DDS_INDEX:
            case DDS_BOOLEAN_T:
				pc_valuep->pc_value.pc_ulong = pc_elemp->pc_dev_value.pc_value.pc_ulong;
				break;
			case DDS_FLOAT:
				ASSERT_DBG (pc_elemp->pc_size == 4);
				pc_valuep->pc_value.pc_float = pc_elemp->pc_dev_value.pc_value.pc_float;
				break;
			case DDS_DOUBLE:
				ASSERT_DBG (pc_elemp->pc_size == 8);
				pc_valuep->pc_value.pc_double = pc_elemp->pc_dev_value.pc_value.pc_double;
				break;
			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				ASSERT_DBG (pc_valuep->pc_value.pc_string.str_ptr != NULL);
                if ((pc_elemp->pc_dev_value.pc_value.pc_string.str_len) &&
                    (pc_elemp->pc_dev_value.pc_value.pc_string.str_ptr))
                {
				    (void)memcpy (pc_valuep->pc_value.pc_string.str_ptr, 
					            pc_elemp->pc_dev_value.pc_value.pc_string.str_ptr, 
                                (int)pc_elemp->pc_dev_value.pc_value.pc_string.str_len);
                }
				break;
			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				(void)memcpy (pc_valuep->pc_value.pc_chararray,
					pc_elemp->pc_dev_value.pc_value.pc_chararray, 
                    (int)pc_elemp->pc_size);
				break;
			default:
				CRASH_RET (PC_INTERNAL_ERROR);
		}
	}

	return (PC_SUCCESS);
}


int pc_initialize_meth_locals(ENV_INFO *env_info)
{
    int rs;


    rs = pc_copy_locals_to_storage(env_info);
    return rs;
}


/**********************************************************************
 *
 *  Name: pc replicate_device_values
 *  ShortDesc: Replicate the data in the device bucket of the
 *			Parameter Cache into the edit and method buckets.
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		The Parameter Cache device value bucket has just been updated with
 *		data from the device.  All post_read methods have also been run. 
 *		pc replicate_device_values takes this new, freshly scaled data from
 *		the device bucket of the Parameter Cache, and updates the edit and
 *		method buckets.
 *
 *  Inputs:
 *		pc_elemp - Pointer into Parameter Cache where to store the data.
 *
 *  Outputs:
 *		The edit and method buckets are updated.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_replicate_device_values (ENV_INFO	*env_info,PC_ELEM		*pc_elemp)
{
	PC_ELEM		*pc_elem_endp, *arr_rec_elemp;
	PC_VALUE	*pc_valuep, *edit_valuep, *meth_valuep;
	APP_INFO	*app_info;

	app_info = env_info->app_info;	

	/*
	 *	Setup the pointers to the first and last entries
	 */

	if (pc_elemp->pc_flags & (PC_ARRAY|PC_RECORD)) {
		arr_rec_elemp = pc_elemp;
		pc_elem_endp = pc_elemp + pc_elemp->pc_size;
		pc_elemp++;
	} else {
		arr_rec_elemp = NULL;
		pc_elem_endp = pc_elemp;
	}

	/*
	 *	Step through the entries, transferring the values in the cache.
	 * The value may be over-written even if changed because a POST_READ
	 * method may have changed the value read but in the comm. stack the
	 * value_changed flag cannot be reset.
	 */

	for ( ; pc_elemp <= pc_elem_endp; pc_elemp++) {
		pc_valuep = &pc_elemp->pc_dev_value;
		edit_valuep = &pc_elemp->pc_edit_value;
		meth_valuep = &pc_elemp->pc_meth_value;
		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					edit_valuep->pc_value.pc_long = 
							pc_valuep->pc_value.pc_long;
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					meth_valuep->pc_value.pc_long = 
							pc_valuep->pc_value.pc_long;
				}
				break;
			case DDS_UNSIGNED:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
			case DDS_INDEX:
            case DDS_BOOLEAN_T:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					edit_valuep->pc_value.pc_ulong = 
							pc_valuep->pc_value.pc_ulong;
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					meth_valuep->pc_value.pc_ulong = 
							pc_valuep->pc_value.pc_ulong;
				}
				break;
			case DDS_FLOAT:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					edit_valuep->pc_value.pc_float = 
							pc_valuep->pc_value.pc_float;
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					meth_valuep->pc_value.pc_float = 
							pc_valuep->pc_value.pc_float;
				}
				break;
			case DDS_DOUBLE:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					edit_valuep->pc_value.pc_double = 
							pc_valuep->pc_value.pc_double;
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					meth_valuep->pc_value.pc_double = 
							pc_valuep->pc_value.pc_double;
				}
				break;
			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					ASSERT_DBG (edit_valuep->pc_value.pc_string.str_len == 
						pc_valuep->pc_value.pc_string.str_len);
					ASSERT_DBG (edit_valuep->pc_value.pc_string.str_ptr != NULL);
					(void)memcpy (edit_valuep->pc_value.pc_string.str_ptr, 
						pc_valuep->pc_value.pc_string.str_ptr, 
						(int)pc_valuep->pc_value.pc_string.str_len);
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					ASSERT_DBG (meth_valuep->pc_value.pc_string.str_len == 
						pc_valuep->pc_value.pc_string.str_len);
					ASSERT_DBG (meth_valuep->pc_value.pc_string.str_ptr != NULL);
					(void)memcpy (meth_valuep->pc_value.pc_string.str_ptr, 
						pc_valuep->pc_value.pc_string.str_ptr, 
						(int)pc_valuep->pc_value.pc_string.str_len);
				}
				break;
			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				if ((edit_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_EDIT_VALUE)) {
					(void)memcpy (edit_valuep->pc_value.pc_chararray,
						pc_valuep->pc_value.pc_chararray, 
						(int)pc_elemp->pc_size);
				}
				if ((meth_valuep->pc_value_changed == 0) ||
					(app_info->status_info & USE_METH_VALUE)) {
					(void)memcpy (meth_valuep->pc_value.pc_chararray,
						pc_valuep->pc_value.pc_chararray, 
						(int)pc_elemp->pc_size);
				}
				break;
			default:
				CRASH_RET (PC_INTERNAL_ERROR);
		}
		pc_elemp->pc_flags &= ~PC_VAL_NOT_READ;
	}

	/*
	 *	Show that the entire record or array has been read.
	 */

	if (arr_rec_elemp) {
		arr_rec_elemp->pc_flags &= ~PC_VAL_NOT_READ;
	}

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc set_lock
 *  ShortDesc: Sets a flag indicating that an entry is being updated.
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc set_lock sets a param entry's flags indicating that it is being
 *		updated by a comm. stack.  Either the whole param is being updated
 *		or a part thereof.  A part can only be updated in the case of
 *		records and arrays.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to lock.
 *
 *  Outputs:
 *      The flags in the param cache are set appropriately.
 *
 *  Returns:
 *		PC_SUCCESS, PC_ENTRY_BUSY
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_set_lock(PC_ELEM *pc_elemp)
{

	if (pc_elemp->pc_flags & PC_ENTRY_LOCKED) {
		return (PC_ENTRY_BUSY);
	}

	/*
	 * Check whether complete param or just subparam being accessed
	 */
	if (pc_elemp->pc_subindex == 0) {
		pc_elemp->pc_flags |= PC_PARAM_LOCKED_FOR_COMM;	
	} else {
		pc_elemp->pc_flags |= PC_SUBPARAM_LOCKED_FOR_COMM;	
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc check_lock
 *  ShortDesc: Checks a flag indicating whether an entry is being updated.
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc check_lock tests a param entry's flags indicating that it
 *		is being updated by a comm. stack.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to test.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_ENTRY_BUSY
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

static int
pc_check_lock(PC_ELEM *pc_elemp)
{
	int	i;

	i = 0;
	while(pc_elemp->pc_flags & PC_ENTRY_LOCKED) {
		i++;
		if (i > 99) {
			return (PC_ENTRY_BUSY);
		}
	}
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc release_lock
 *  ShortDesc: Resets flags which indicated that an entry was being
 *				updated.
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc release_lock resets a param entry's flags indicating that it
 *		was being updated by a comm. stack.  Either the whole param
 *		was being updated or a part thereof.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Parameter Cache element to lock.
 *
 *  Outputs:
 *      The flags in the param cache are set appropriately.
 *
 *  Returns:
 *		None
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

static void
pc_release_lock(PC_ELEM *pc_elemp)
{

	pc_elemp->pc_flags &= ~PC_ENTRY_LOCKED;
	return;
}


/**********************************************************************
 *
 *  Name: pc get_action_storage
 *  ShortDesc: Allocate memory to hold a copy of a param's entry
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc get_action_storage finds the param's entry, locks it and
 *		allocates space for a duplicate entry and then copies the
 *		existing entry to the new entry.
 *
 *  Inputs:
 *      env_info - The ENV_INFO structure containing information
 *                      about the current request.
 *		p_ref - P_REF specifies po or ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *		param_count - the number of params allocated.
 *      The action_storage pointer is set to point at the temporary 
 *		param entry.
 *
 *  Returns:
 *      PC_SUCCESS, PC_NO_MEMORY, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION
 *		
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_action_storage(ENV_INFO *env_info, P_REF *ip_ref, int *param_count, int lock)
{

	PC_ELEM		*pc_start_elemp;
	PC_ELEM		*temp_pc;
	PC_ELEM		*pc_elemp;
	int			needed_count, needed_size;
	int			param_counter;
	int			rs;
	APP_INFO	*app_info;

	app_info = env_info->app_info;

	*param_count = 0;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (param_count == (int *) NULL) {
		return (PC_BAD_POINTER);
	}

    rs = pc_find_param (env_info, ip_ref, &pc_elemp, (PC_VALUE **) NULL);
    if (rs != PC_SUCCESS) {
        return (rs);
    }
	if(!lock)
	{
		rs = pc_set_lock (pc_elemp);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}
	pc_start_elemp = pc_elemp;

	if (pc_start_elemp->pc_flags & PC_HEAD_ELEMENT) {
		needed_count = pc_start_elemp->pc_size + 1;
		param_counter = pc_start_elemp->pc_size;
	} else {
		needed_count = 1;
		param_counter = 1;
	}

	needed_size = needed_count * sizeof (*temp_pc);
	temp_pc = (PC_ELEM *)malloc ((unsigned)needed_size);
	if (temp_pc == NULL) {
		pc_release_lock (pc_elemp);
		return (PC_NO_MEMORY);
	}
	(void)memcpy ((char *)temp_pc, (char *)pc_start_elemp, needed_size);

	app_info->action_storage = (void *)temp_pc;
	*param_count = param_counter;
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc put_action_storage
 *  ShortDesc: Copy the action storage into the cache, release lock
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc put_action_storage makes a call to update the other param
 *		value "buckets" and then copies the new param entries over the
 *		existing entries.  The lock on the param entry is released after
 *		successful execution.  In the case of pre/post edit methods no
 *		replication of values should take place.
 *
 *  Inputs:
 *      env_info - The ENV_INFO structure containing information
 *                      about the current request.
 *		ip_ref - A P_REF structure that specifies po, ID and subindex of 
 *						requested param.
 *		replicate_device_values - a flag if set causes the other device buckets
 *						to be updated from the device bucket.
 *
 *  Outputs:
 *      The existing param entry in the cache is updated and param lock
 *		released.
 *
 *  Returns:
 *      PC_SUCCESS, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION
 *		
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_put_action_storage(ENV_INFO *env_info, P_REF *ip_ref, int replicate_device_values)
{

	PC_ELEM		*pc_start_elemp;
	int			needed_count, needed_size;
	int			rs;
	APP_INFO	*app_info;

	app_info = env_info->app_info;	

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	/*
	 * Get the pointer to the entry in the Parameter Cache
	 */

    rs = pc_find_param (env_info, ip_ref, &pc_start_elemp, (PC_VALUE **) NULL);
	ASSERT_DBG (rs == PC_SUCCESS);
    if (rs != PC_SUCCESS) {
    /* 
     * This error condition is problematic because we cannot find the entry which
	 * needs to be unlocked, therefore the release_lock won't necessarily work. 
     */ 
		pc_release_lock (pc_start_elemp);
        return (rs);
    }

	if (replicate_device_values) {
		/*
		 * Update the edit and method values with the device value
		 */

		rs = pc_replicate_device_values(env_info, (PC_ELEM *)(app_info->action_storage));
		if (rs != PC_SUCCESS) {
			pc_release_lock (pc_start_elemp);
			return (rs);
		}
    }

	/*
	 *	Copy the updated action storage into the Parameter Cache.
	 */

	if (pc_start_elemp->pc_flags & PC_HEAD_ELEMENT) {
		needed_count = pc_start_elemp->pc_size + 1;
	} else {
		needed_count = 1;
	}
	needed_size = needed_count * sizeof (*pc_start_elemp);

	(void)memcpy ((char *)pc_start_elemp, (char *)app_info->action_storage, 
				needed_size);

	free ((char *)app_info->action_storage);
	app_info->action_storage = NULL;

	pc_release_lock (pc_start_elemp);
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc free_action_storage
 *  ShortDesc: Free the action storage and release lock
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc free_action_storage frees the action_storage and release
 *		the lock on the param.
 *
 *  Inputs:
 *      env_info - The ENV_INFO structure containing information
 *                      about the current request.
 *		ip_ref - A P_REF structure which specifies po, ID and subindex of 
 *						requested param.
 *
 *  Outputs:
 *      The param lock is released.
 *
 *  Returns:
 *      PC_SUCCESS, PC_PARAM_NOT_FOUND, 
 *		PC_INTERNAL_ERROR, PC_METHOD_COLLISION
 *		
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_free_action_storage(ENV_INFO *env_info, P_REF *ip_ref)
{

	PC_ELEM		*pc_elemp;
	int			rs;
	APP_INFO	*app_info;

	app_info = env_info->app_info;

	/*
	 * Get the pointer to the entry in the Parameter Cache
	 */

    rs = pc_find_param (env_info, ip_ref, &pc_elemp, (PC_VALUE **) NULL);
    if (rs != PC_SUCCESS) {
		pc_release_lock (pc_elemp);
        return (rs);
    }

	free ((char *)app_info->action_storage);
	app_info->action_storage = NULL;

	pc_release_lock (pc_elemp);
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc read_values_from_storage
 *  ShortDesc: Read the values out of action storage, put in value struct
 *
 *  Include:
 *		pc_loc.h
 *		cm_lib.h
 *
 *  Description:
 *		pc read_values_from_storage copies the proper values out of the
 *		Parameter Cache, and packs them into a COMM_VALUE_LIST
 *		structure in preparation for sending the data to the
 *		device.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *						about the current request.
 *
 *  Outputs:
 *		cm_value_listp - The Parameter Cache data formatted to send to the
 *					device.
 *
 *  Returns:
 *		PC_SUCCESS, PC_NO_MEMORY, PC_METHOD_COLLISION, 
 *		PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_read_values_from_storage(ENV_INFO *env_info, COMM_VALUE_LIST *cm_value_listp)
{

	COMM_VALUE		*cm_valuep;
	DEVICE_HANDLE	device_handle;
	int				found_count;
	int				needed_count;
	NET_TYPE		net_type;
	PC_ELEM			*pc_elemp;
	PC_ELEM			*pc_elem_endp;
	PC_VALUE		*pc_valuep;
	int				rs;
	APP_INFO		*app_info;

	app_info = env_info->app_info;

	rs = pc_check_env_info (env_info);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (cm_value_listp == (COMM_VALUE_LIST *) NULL) {
		return (PC_BAD_POINTER);
	}

	/*
	 *	Calculate the number of elements needed in the value list,
	 *	and malloc the memory.
	 */

	pc_elemp = (PC_ELEM *)(app_info->action_storage);
	if (pc_elemp->pc_flags & PC_HEAD_ELEMENT) {
		needed_count = pc_elemp++->pc_size;
	} else {
		needed_count = 1;
	}

	if (needed_count > cm_value_listp->cmvl_limit) {
		cm_value_listp->cmvl_list = (COMM_VALUE *) xrealloc (
			(void *)cm_value_listp->cmvl_list, 
			(sizeof (*cm_value_listp->cmvl_list) * needed_count));
		cm_value_listp->cmvl_limit = (int)needed_count;
	}
	cm_value_listp->cmvl_count = (int)needed_count;

	/*
	 *	Step through the Parameter Cache, filling in the values list.
	 */

	pc_elem_endp = pc_elemp + needed_count;
	cm_valuep = cm_value_listp->cmvl_list;
	found_count = 0;
	for ( ; pc_elemp < pc_elem_endp; pc_elemp++, cm_valuep++) {
		if (app_info->status_info & USE_METH_VALUE) {
			/*
			 *	Use the method value.  If there is a value that has been
			 *	changed by this method, indicated that there is something
			 *	that should be sent (found_count != 0).  If the method value
			 *	has not been modified by any method, use it.  If it has been
			 *	modified by a different method, return an error.
			 */

			pc_valuep = &pc_elemp->pc_meth_value;
			if (pc_valuep->pc_value_changed) {
				if (pc_valuep->pc_method_tag == app_info->method_tag) {
					found_count++;
				} else {
					return (PC_METHOD_COLLISION);
				}
			}

		} else {
			ASSERT_DBG ((app_info->status_info & USE_MASK) == USE_EDIT_VALUE);
			pc_valuep = &pc_elemp->pc_edit_value;
			if (pc_valuep->pc_value_changed) {
				found_count++;
			}
		}

		/*
		 *	Transfer the data.
		 */

		cm_valuep->cm_size = pc_elemp->pc_size;
		cm_valuep->cm_type = pc_elemp->pc_type;

		switch (pc_elemp->pc_type) {
			case DDS_INTEGER:
				cm_valuep->cm_val.cm_long = pc_valuep->pc_value.pc_long;
				break;
			case DDS_UNSIGNED:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
			case DDS_INDEX:
            case DDS_BOOLEAN_T:
				cm_valuep->cm_val.cm_ulong = pc_valuep->pc_value.pc_ulong;
				break;
			case DDS_FLOAT:
				cm_valuep->cm_val.cm_float = pc_valuep->pc_value.pc_float;
				break;
			case DDS_DOUBLE:
				cm_valuep->cm_val.cm_double = pc_valuep->pc_value.pc_double;
				break;
			case DDS_ASCII:
			case DDS_PACKED_ASCII:
			case DDS_PASSWORD:
			case DDS_BITSTRING:
			case DDS_OCTETSTRING:
			case DDS_VISIBLESTRING:
			case DDS_EUC:
				cm_valuep->cm_size = pc_valuep->pc_value.pc_string.str_len;
				cm_valuep->cm_val.cm_stream = malloc (cm_valuep->cm_size);
				if (cm_valuep->cm_val.cm_stream == NULL) {
					cr_free_value_list (cm_value_listp);
					return (PC_NO_MEMORY);
				}
				(void)memcpy (cm_valuep->cm_val.cm_stream, 
						pc_valuep->pc_value.pc_string.str_ptr, 
						(int)cm_valuep->cm_size);
				break;
			case DDS_DATE:
			case DDS_TIME:
			case DDS_TIME_VALUE:
			case DDS_DATE_AND_TIME:
			case DDS_DURATION:
				(void)memcpy (cm_valuep->cm_val.cm_ca, pc_valuep->pc_value.pc_chararray,
					(int) pc_elemp->pc_size);
				break;
			default:
				CRASH_RET (PC_INTERNAL_ERROR);
		}
	}

    rs = get_abt_adt_offset (env_info->block_handle, &device_handle);
    if (rs != CM_SUCCESS) {
        return (rs);
    }
    rs = get_adt_net_type(device_handle, &net_type);
    if (rs != CM_SUCCESS) {
        return (rs);
    }

    if ((net_type <= NT_A_SIM) || (net_type >= NT_ETHER)) {
		if (found_count == 0) {
			return (PC_NO_DATA_TO_SEND);
		}
    }

	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc set_param_flag
 *  ShortDesc: Sets a flag used by the application only.
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc set_param_flag sets the internal flag associated with each
 *		param.  This flag is only used by the application or
 *		applications.  It not reserved to an application after being
 *		set.
 *
 *  Inputs:
 *		block_handle - block_handle where the param is located.
 *		param_ref - param reference of param for flags to be set.
 *		param_flag - param flag settings
 *
 *  Outputs:
 *      The flag in the param cache is set appropriately.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_METHOD_COLLISION, 
 *		PC_INTERNAL_ERROR
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_set_param_flag(BLOCK_HANDLE block_handle, P_REF *pc_p_ref, unsigned long param_flag)
{

	ENV_INFO	env_info;
	APP_INFO	app_info;
	PC_ELEM		*pc_elemp;
	int			rs;
	
	(void)memset ((char *)&env_info, 0, sizeof (env_info));
	(void)memset ((char *)&app_info, 0, sizeof (app_info));
	env_info.app_info = &app_info;
	env_info.block_handle = block_handle;
	app_info.status_info = USE_DEV_VALUE;

	rs = pc_find_param_value (&env_info, pc_p_ref, &pc_elemp, 
							(PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	pc_elemp->pc_appl_flags |= param_flag;
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc test_param_flag
 *  ShortDesc: Tests a flag needed by the application only.
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc test_param_flag tests the internal flag associated with each
 *		param.  This flag is only used by the application or
 *		applications.
 *
 *  Inputs:
 *		block_handle - block_handle where the param is located.
 *		param_ref - param reference of param for flags to be set.
 *		param_flag - param flag settings
 *
 *  Outputs:
 *      The flag in the param cache is set appropriately.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_METHOD_COLLISION, 
 *		PC_INTERNAL_ERROR
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_test_param_flag(BLOCK_HANDLE block_handle, P_REF *pc_p_ref, unsigned long test_flag,
	unsigned long *set_flag)
{

	ENV_INFO	env_info;
	APP_INFO	app_info;
	PC_ELEM		*pc_elemp;
	int			rs;
	
	(void)memset ((char *)&env_info, 0, sizeof (env_info));
	(void)memset ((char *)&app_info, 0, sizeof (app_info));
	env_info.app_info = &app_info;
	env_info.block_handle = block_handle;
	app_info.status_info = USE_DEV_VALUE;

	rs = pc_find_param_value (&env_info, pc_p_ref, &pc_elemp, (PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	*set_flag = pc_elemp->pc_appl_flags &= test_flag;
	return (PC_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pc clear_param_flag
 *  ShortDesc: Clear the bits set in the passed flag in the cache
 *
 *  Include:
 *      pc_loc.h
 *
 *  Description:
 *		pc clear_param_flag resets the internal flag associated with each
 *		param.  This flag is only used by the application or
 *		applications.
 *
 *  Inputs:
 *		block_handle - block_handle where the param is located.
 *		param_ref - param reference of param for flags to be set.
 *		param_flag - param flag re-settings
 *
 *  Outputs:
 *      The flag in the param cache is set appropriately.
 *
 *  Returns:
 *		PC_SUCCESS, PC_PARAM_NOT_FOUND, PC_METHOD_COLLISION, 
 *		PC_INTERNAL_ERROR
 *
 *  Author:
 *      Conrad Beaulieu
 *
 *********************************************************************/

int
pc_clear_param_flag(BLOCK_HANDLE block_handle, P_REF *pc_p_ref, unsigned long param_flag)
{

	ENV_INFO	env_info;
	APP_INFO	app_info;
	PC_ELEM		*pc_elemp;
	int			rs;
	
	(void)memset ((char *)&env_info, 0, sizeof (env_info));
	(void)memset ((char *)&app_info, 0, sizeof (app_info));
	env_info.app_info = &app_info;
	env_info.block_handle = block_handle;
	app_info.status_info = USE_DEV_VALUE;

	rs = pc_find_param_value (&env_info, pc_p_ref, &pc_elemp, 
							(PC_VALUE **) NULL);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	pc_elemp->pc_appl_flags &= ~param_flag;
	return (PC_SUCCESS);
}

	
/**********************************************************************
 *
 *  Name: pc get_param_value_list
 *  ShortDesc: Get the un-scaled parameter values to write to the device.
 *
 *  Include:
 *		pc_loc.h		
 *
 *  Description:
 *		Get the values associated with the passed param reference.
 *		This is done by getting the necessary temporary storage,
 *		a list of any pre-write methods which may exist,
 *		executing the pre-write methods and finally transfering the
 *		scaled values to the value_list.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *					about the current request.
 *		p_ref - The P_REF structure containing a unique reference
 *				to a param by id or po.
 *
 *  Outputs:
 *		comm_value_list - a value list structure including value
 *			size, type, and typed value.  The list value count and
 *			maximum count is also included.
 *
 *  Returns:
 *		Any PC error condition.	
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_get_param_value_list(ENV_INFO *env_info, P_REF *p_ref,
	COMM_VALUE_LIST *comm_value_list)
{

	PC_ACTION		*actionsp;
	int				count;
	P_REF			ip_ref;
	ITEM_ID			*itemp, *item_endp;
	int				param_count;
	int				rs, rs2;
	void			*save_action_storage;
	P_REF			subparam_p_ref;
	ENV_INFO		tenv_info;
	APP_INFO		*app_info;
	APP_INFO		tapp_info;

	app_info = env_info->app_info;
	(void)memset((char *) &tenv_info, 0, sizeof (ENV_INFO));
	(void)memset((char *) &tapp_info, 0, sizeof (APP_INFO));
	tenv_info.block_handle = env_info->block_handle;
	tenv_info.app_info = &tapp_info;
	tapp_info.status_info = app_info->status_info;
	tapp_info.method_tag = app_info->method_tag;

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_get_action_storage (&tenv_info, &ip_ref, &param_count, 0);
	if (rs != PC_SUCCESS) {
		return (rs);
	}
	
	/*
	 * If value list count is greater than one, then the whole record or
	 * array is being dealt with.  Start subindex at first param value,
	 * otherwise the param has a single value and p_ref has the subindex.
	 */

	subparam_p_ref.type	= ip_ref.type;
	subparam_p_ref.id	= ip_ref.id;
	subparam_p_ref.po	= ip_ref.po;
	if (param_count > 1) {
		ASSERT_DBG (p_ref->subindex == 0);
		subparam_p_ref.subindex = 1;
	} else {
		subparam_p_ref.subindex = 0;
	}

	save_action_storage = tapp_info.action_storage;
    tapp_info.status_info &= ~USE_MASK;
    tapp_info.status_info |= USE_DEV_VALUE;
	for (count = 1; count <= param_count; count++) {
		rs = pc_get_action_list (&tenv_info, &subparam_p_ref, 
							(unsigned long)PC_PRE_WRITE, &actionsp);
		if (rs != PC_SUCCESS) {
            tapp_info.action_storage = save_action_storage;
			rs2 = pc_free_action_storage (&tenv_info, &ip_ref);
			return (rs);
		}
	
		if (actionsp->pc_action_count > 0) {
			itemp		= actionsp->pc_action_list;
			item_endp	= itemp + actionsp->pc_action_count;
            for ( ; itemp < item_endp; itemp++) {
                rs2 = mi_exec_scaling_meth (&tenv_info, *itemp);
                if (rs2 != PC_SUCCESS) {
					rs = pc_free_action_storage (&tenv_info, &ip_ref);
					app_info->env_returns.dds_return_code = tapp_info.env_returns.dds_return_code;
					app_info->env_returns.dds_errors = tapp_info.env_returns.dds_errors;
					app_info->env_returns.comm_err = tapp_info.env_returns.comm_err;
					app_info->env_returns.device_status = tapp_info.env_returns.device_status;
					app_info->env_returns.response_code = tapp_info.env_returns.response_code;
					app_info->env_returns.id_in_error = tapp_info.env_returns.id_in_error;
					app_info->env_returns.po_in_error = tapp_info.env_returns.po_in_error;
					app_info->env_returns.subindex_in_error = tapp_info.env_returns.subindex_in_error;
					app_info->env_returns.member_id_in_error = tapp_info.env_returns.member_id_in_error;
					app_info->env_returns.return_flag = tapp_info.env_returns.return_flag;

                    return (rs2);
                }
            }
		}
		tapp_info.action_storage = save_action_storage;
		subparam_p_ref.subindex++;
	}

    tapp_info.status_info = app_info->status_info;
	/* whoa Nellie - cbb */
	tapp_info.method_tag = app_info->method_tag;
	app_info->action_storage = tapp_info.action_storage;
	rs2 = pc_read_values_from_storage (&tenv_info, comm_value_list);
	if (rs2 != PC_SUCCESS) {
		rs = pc_free_action_storage (&tenv_info, &ip_ref);
		app_info->action_storage = (void *)NULL;
#if 0
		app_info->env_returns.dds_return_code = tapp_info.env_returns.dds_return_code;
		app_info->env_returns.dds_errors = tapp_info.env_returns.dds_errors;
		app_info->env_returns.comm_err = tapp_info.env_returns.comm_err;
		app_info->env_returns.device_status = tapp_info.env_returns.device_status;
		app_info->env_returns.response_code = tapp_info.env_returns.response_code;
		app_info->env_returns.id_in_error = tapp_info.env_returns.id_in_error;
		app_info->env_returns.po_in_error = tapp_info.env_returns.po_in_error;
		app_info->env_returns.subindex_in_error = tapp_info.env_returns.subindex_in_error;
		app_info->env_returns.member_id_in_error = tapp_info.env_returns.member_id_in_error;
		app_info->env_returns.return_flag = tapp_info.env_returns.return_flag;
#endif
		return (rs2);
	}


	return (PS_SUCCESS);
}

#define REPLICATE_DEV_VALUES 1

/**********************************************************************
 *
 *  Name:  pc put_param_value_list
 *  ShortDesc: Scale and put the unscaled values from the device in the
 *				parameter cache.
 *
 *  Include:
 *		pc_loc.h		
 *
 *  Description:
 *		Create a temporary param cache entry, call the necessary post-read
 *		methods and then update the parameter cache.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *					about the current request.
 *		p_ref - The P_REF structure containing a unique reference
 *				to a param by id or po.
 *		comm_value_list - a value list structure including value
 *			size, type, and typed value.  The list value count and
 *			maximum count is also included.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any PC error condition.	
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_put_param_value_list(ENV_INFO *env_info, P_REF *p_ref,
	COMM_VALUE_LIST *comm_value_list, int lock)
{

	P_REF			ip_ref;
	int				rs, rs2;
	P_REF			subparam_p_ref;
	PC_ACTION		*actionsp;
	ITEM_ID			*itemp, *item_endp;
	void			*save_action_storage;
	int				param_count;
	int				param_size;
	ENV_INFO		tenv_info;
	APP_INFO		tapp_info;
	APP_INFO		*app_info;

	app_info = env_info->app_info;
    (void)memset((char *) &tenv_info, 0, sizeof (ENV_INFO));
    (void)memset((char *) &tapp_info, 0, sizeof (APP_INFO));
	tenv_info.app_info = &tapp_info;
	tenv_info.block_handle = env_info->block_handle;
	tapp_info.status_info = app_info->status_info;
	tapp_info.action_storage = app_info->action_storage;
	tapp_info.method_tag = app_info->method_tag;

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	if (app_info->action_storage == (void *)NULL) {
		rs = pc_get_action_storage(&tenv_info, &ip_ref, &param_size, lock);
		if (rs != PC_SUCCESS) {
			return (rs);
		}
	}

	/*
	 * REMEMBER to free action_storage if any error occurs!
	 */

	rs = pc_write_values_to_storage (&tenv_info, comm_value_list);
	if (rs != PC_SUCCESS) {
		rs = pc_free_action_storage (&tenv_info, &ip_ref);
		app_info->action_storage = tapp_info.action_storage;
		return (rs);
	}

	/*
	 * if value list count is greater than one then the whole record or
	 * array is being dealt with start subindex at first param value
	 * otherwise the param has a single value and p_ref has the subindex
	 */

	subparam_p_ref.type	= ip_ref.type;
	subparam_p_ref.id	= ip_ref.id;
	subparam_p_ref.po	= ip_ref.po;
	if (comm_value_list->cmvl_count > 1) {
		ASSERT_DBG (ip_ref.subindex == 0);
		subparam_p_ref.subindex = 1;
	} else {
		subparam_p_ref.subindex = 0; 
	}

	save_action_storage = tapp_info.action_storage;
    tapp_info.status_info &= ~USE_MASK;
    tapp_info.status_info |= USE_DEV_VALUE;
	for (param_count = 1; param_count <= comm_value_list->cmvl_count; 
			param_count++) {
		rs2 = pc_get_action_list (&tenv_info, &subparam_p_ref,
							(unsigned long)PC_POST_READ, &actionsp);
		if (rs2 != PC_SUCCESS) {
            tapp_info.action_storage = save_action_storage;
			rs = pc_free_action_storage (&tenv_info, &ip_ref);
			app_info->action_storage = tapp_info.action_storage;
			return (rs2);
		}

		if (actionsp->pc_action_count > 0) {
			itemp = actionsp->pc_action_list;
			item_endp = itemp + actionsp->pc_action_count;
            for ( ; itemp < item_endp; itemp++) {
                rs2 = mi_exec_scaling_meth (&tenv_info, *itemp);
                if (rs2 != PC_SUCCESS) {
					rs = pc_free_action_storage (&tenv_info, &ip_ref);
					app_info->env_returns.dds_return_code =
						tapp_info.env_returns.dds_return_code;
					app_info->env_returns.dds_errors = tapp_info.env_returns.dds_errors;
					app_info->env_returns.comm_err = tapp_info.env_returns.comm_err;
					app_info->env_returns.device_status = tapp_info.env_returns.device_status;
					app_info->env_returns.response_code = tapp_info.env_returns.response_code;
					app_info->env_returns.id_in_error = tapp_info.env_returns.id_in_error;
					app_info->env_returns.po_in_error = tapp_info.env_returns.po_in_error;
					app_info->env_returns.subindex_in_error =
						tapp_info.env_returns.subindex_in_error;
					app_info->env_returns.member_id_in_error =
						tapp_info.env_returns.member_id_in_error;
					app_info->env_returns.return_flag = tapp_info.env_returns.return_flag;
                    return (rs2);
                }
            }
		}
		tapp_info.action_storage = save_action_storage;
		subparam_p_ref.subindex++;
	}

    tapp_info.status_info = app_info->status_info;
    tapp_info.method_tag = app_info->method_tag;
	rs = pc_put_action_storage (&tenv_info, &ip_ref, REPLICATE_DEV_VALUES);
	if (rs != PC_SUCCESS) {
		rs = pc_free_action_storage (&tenv_info, &ip_ref);
		app_info->action_storage = (void *)NULL;
#if 0
		app_info->env_returns.dds_return_code = tapp_info.env_returns.dds_return_code;
		app_info->env_returns.dds_errors = tapp_info.env_returns.dds_errors;
		app_info->env_returns.comm_err = tapp_info.env_returns.comm_err;
		app_info->env_returns.device_status = tapp_info.env_returns.device_status;
		app_info->env_returns.response_code = tapp_info.env_returns.response_code;
		app_info->env_returns.id_in_error = tapp_info.env_returns.id_in_error;
		app_info->env_returns.po_in_error = tapp_info.env_returns.po_in_error;
		app_info->env_returns.subindex_in_error = tapp_info.env_returns.subindex_in_error;
		app_info->env_returns.member_id_in_error = tapp_info.env_returns.member_id_in_error;
		app_info->env_returns.return_flag = tapp_info.env_returns.return_flag;
#endif
		return (rs);
	}

	return (PS_SUCCESS);
}


/**********************************************************************
 *
 *  Name:  pc release_param_value_list
 *  ShortDesc: 
 *
 *  Include:
 *		pc_loc.h		
 *
 *  Description:
 *		Release storage associated with a pc get_param_value_list call. 
 *		After a pc get_param_value_list call either pc put_param_
 *		value_list or pc release_param_value_list MUST be called.
 *		The associated entry in the param cache will remain in
 *		a pending read state if this is not done.
 *
 *  Inputs:
 *		env_info - The ENV_INFO structure containing information
 *					about the current request.
 *		p_ref - The P_REF structure containing a unique reference
 *				to a param by id or po.
 * 
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		Any returns associated with the PC function pc free_action_storage	
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

int
pc_release_param_value_list(ENV_INFO *env_info, P_REF *p_ref)
{
	int		rs;
	P_REF	ip_ref;

	rs = pc_conv_p_ref_to_ip_ref (env_info->block_handle, p_ref, &ip_ref);
	if (rs != PC_SUCCESS) {
		return (rs);
	}

	rs = pc_free_action_storage (env_info, &ip_ref);
	return (rs);
}
