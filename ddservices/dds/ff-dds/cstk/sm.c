/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Device System management module.

    Device System Management basic functions are as follows:
 
 	1.  Read the physical tag of device.
 	2.  Clear the the physical tag of device.
 	3.  Set the physical tag of device.
 	4.  Read the Node address of device.
 	5.  Set the Node address of device.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "sm.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
/***********************************************************************
 *
 *	Name: softing_sm_identify
 *
 *	ShortDesc:  Read the Physical tag and Decice ID of the Device.
 *
 *	Description:
 *		The softing_sm_identify function takes a node address
 *		and gets the PD tag and Device ID of the device
 *
 *	Inputs:
 *		dev_addr - node address of the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *		
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_sm_identify (
						 ENV_INFO* env_info,
						 unsigned short dev_addr, 
						 CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
	int		r_code;
	APP_INFO *app_info;
    SM_REQ sm_req;
    int len;
    CR_SM_IDENTIFY_CNF temp_identify;

	app_info = (APP_INFO *)env_info->app_info;

    memset((char *)&temp_identify, '\0', sizeof(CR_SM_IDENTIFY_CNF));

    sm_req.dev_addr = dev_addr;
	r_code = send_wait_receive_cmd(env_info, SM_IDENTIFY_REQ, NULL, &sm_req);
	if (CM_SUCCESS != r_code)
    {
        return (r_code);
    }
    
	if (POS == get_service_result())
	{
        LOGC_TRACE(CPM_FF_DDSERVICE,"Recv Identify: Layer:SM - Service:SM_IDENTIFY = %d",
            sizeof(CR_SM_IDENTIFY_CNF) +
            get_size_of_desc_header()
            );

		(void *)memcpy(
			(char *)&temp_identify, 
			(CR_SM_IDENTIFY_CNF *)global_res_buf,
			sizeof(CR_SM_IDENTIFY_CNF)
		);
		
        /* 
         * Device Address:
         *      0x00 to 0x09 -- Reserved
         *      0x10 to 0xF7 -- Operational Device Address
         *      0xF8 to 0xFB -- Un initialized/Initialized Device address
         *                      and not operational.
         *      0xFC -- 0xFF -- Handheld/Secondary Host Address
         */
        if ((dev_addr >= 0x10) && (dev_addr <= 0xFF) && (dev_addr != 0))
        {
            /* Store the Device ID information */
            len = (size_t) temp_identify.dev_id[0];
            if (len > SM_TAG_LEN)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Device Uses invalid length %d for device ID\n", len);
                len = SM_TAG_LEN;
            }
            else if(len < SM_TAG_LEN)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Device uses invalid length %d for Device ID\n", len);
                LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
                return CS_SM_WRONG_PD_TAG;
            }
            memcpy((char *)sm_identify_cnf->dev_id, &temp_identify.dev_id[1], len);
            memcpy((char *)sm_identify_cnf->dup_dev_id, &temp_identify.dev_id[0], 34);

            /* Store the PD Tag Information */
            len = (size_t) temp_identify.pd_tag[0];
            if (len > SM_TAG_LEN)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Device Uses invalid length %d for PD Tag\n", len);
                len = SM_TAG_LEN;
            }
            else if(len < SM_TAG_LEN)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Device uses invalid length %d for PD Tag\n", len);
                LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
                return CS_SM_WRONG_DEVICE_ID;
            }
            memcpy(sm_identify_cnf->pd_tag, &temp_identify.pd_tag[1], len);
            memcpy((char *)sm_identify_cnf->dup_pd_tag, &temp_identify.pd_tag[0], 34);

            /* A cleared device should not occur for devices at an operational
             * address, but accept this so that the code also could be used to
             * Administer default addresses
             */
	    }

		app_info->env_returns.comm_err = CM_SUCCESS; 
        app_info->env_returns.response_code = 0;
		r_code = CM_SUCCESS;
    }
   	else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;
		
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
        
        return CS_SM_ERR(app_info->env_returns.response_code); 
    }

	return r_code;
}

/***********************************************************************
 *
 *	Name: softing_sm_clr_tag
 *
 *	ShortDesc:  Clear the Physical Device Tag of the remote device
 *
 *	Description:
 *		The softing_sm_clr_tag function takes a current node address
 *		and clears the Tag of the device
 *
 *	Inputs:
 *		dev_addr - node address of the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_sm_clr_tag (
						 ENV_INFO* env_info,
						 unsigned short dev_addr,
                         CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
	int		r_code;
	APP_INFO *app_info;
    SM_REQ sm_req;

	app_info = env_info->app_info;

	(void *)memcpy((char *)sm_req.pd_tag, (char *)sm_identify_cnf->dup_pd_tag, SM_PD_TAG_SIZE);
    (void *)memcpy((char *)sm_req.dev_id, (char *)sm_identify_cnf->dup_dev_id, SM_DEV_ID_SIZE);

    app_info->env_returns.comm_err = CM_SUCCESS; 
	app_info->env_returns.response_code = 0;
    
    sm_req.dev_addr = dev_addr;
    sm_req.clear = FB_TRUE;
    
    r_code = send_wait_receive_cmd(env_info, SM_CLR_TAG_REQ, NULL, &sm_req);
	if (CM_SUCCESS != r_code)
    {
        return (r_code);
    }

	if (POS == get_service_result())
	{
                LOGC_DEBUG(CPM_FF_DDSERVICE,"Clear PD TAG con (+)\n");
		app_info->env_returns.comm_err = CM_SUCCESS; 
        app_info->env_returns.response_code = 0;
	    r_code = CM_SUCCESS;
    }
   	else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;
		
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
        
        return CS_SM_ERR(app_info->env_returns.response_code); 
    }

	return r_code;
}

/***********************************************************************
 *
 *	Name: softing_sm_assign_tag
 *
 *	ShortDesc:  Assigns the Physical Device Tag to the remote device
 *
 *	Description:
 *		The softing_sm_assign_tag function takes a current node address
 *		and Sets the New Tag to the device
 *
 *	Inputs:
 *		dev_addr - node address of the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *		
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_sm_assign_tag (
						 ENV_INFO* env_info,
						 unsigned short dev_addr,
                         CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
	int		r_code;
	APP_INFO *app_info;
    SM_REQ sm_req;

	app_info = env_info->app_info;

	(void *)memcpy((char *)sm_req.pd_tag, (char *)sm_identify_cnf->dup_pd_tag, SM_PD_TAG_SIZE);
    (void *)memcpy((char *)sm_req.dev_id, (char *)sm_identify_cnf->dup_dev_id, SM_DEV_ID_SIZE);

    app_info->env_returns.comm_err = CM_SUCCESS; 
	app_info->env_returns.response_code = 0;
    
    sm_req.dev_addr = dev_addr;
    sm_req.clear = FB_FALSE;
    
    r_code = send_wait_receive_cmd(env_info, SM_SET_TAG_REQ, NULL, &sm_req);
	if (CM_SUCCESS != r_code)
    {
        return r_code;
    }

	if (POS == get_service_result())
	{
                LOGC_DEBUG(CPM_FF_DDSERVICE,"Set PD TAG con (+)\n");
		app_info->env_returns.comm_err = CM_SUCCESS; 
        app_info->env_returns.response_code = 0;
	    r_code = CM_SUCCESS;
    }
   	else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;
		
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
        
        return CS_SM_ERR(app_info->env_returns.response_code); 
    }

	return r_code;
}


/***********************************************************************
 *
 *	Name: softing_sm_clr_node
 *
 *	ShortDesc:  Clear the Node address of the remote device
 *
 *	Description:
 *		The softing_sm_clr_node function takes a current node address
 *		and clears the Node or Poll or Device address of the device
 *
 *	Inputs:
 *		dev_addr - node address of the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *		
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_sm_clr_node (
						 ENV_INFO* env_info,
						 unsigned short dev_addr,
                         CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
	int		r_code;
	APP_INFO *app_info;
    SM_REQ sm_req;

	app_info = env_info->app_info;

	(void *)memcpy((char *)sm_req.pd_tag, (char *)sm_identify_cnf->dup_pd_tag, SM_PD_TAG_SIZE);
    (void *)memcpy((char *)sm_req.dev_id, (char *)sm_identify_cnf->dup_dev_id, SM_DEV_ID_SIZE);

    app_info->env_returns.comm_err = CM_SUCCESS; 
	app_info->env_returns.response_code = 0;
    
    sm_req.dev_addr = dev_addr;
    
    r_code = send_wait_receive_cmd(env_info, SM_CLR_NODE_REQ, NULL, &sm_req);
	if (CM_SUCCESS != r_code)
    {
       return r_code;
    }

	if (POS == get_service_result())
	{
                LOGC_DEBUG(CPM_FF_DDSERVICE,"Clear Address con (+)\n");
		app_info->env_returns.comm_err = CM_SUCCESS; 
        app_info->env_returns.response_code = 0;
	    r_code = CM_SUCCESS;
    }
   	else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;
		
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
        
        return CS_SM_ERR(app_info->env_returns.response_code); 
    }

	return r_code;
}

/***********************************************************************
 *
 *	Name: softing_sm_set_node
 *
 *	ShortDesc:  Set the Node address to the remote device
 *
 *	Description:
 *		The softing_sm_set_node function takes a pd_tag as input
 *		compares with the remote device and sets the new Node or Poll or 
 *      Device address to the device
 *
 *	Inputs:
 *		pd_tag - PD TAG of the remote device for comparison
 *      new_dev_addr - New Node address to the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *		
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_sm_set_node (
						 ENV_INFO* env_info,
						 unsigned short new_dev_addr,
                         char* pd_tag
)
{
	int		r_code;
	APP_INFO *app_info;
    SM_REQ sm_req;

	app_info = env_info->app_info;

    sm_req.dev_addr = new_dev_addr;
    
    (void *)memcpy((char *)sm_req.pd_tag, (char *)pd_tag, SM_PD_TAG_SIZE);
    r_code = send_wait_receive_cmd(env_info, SM_SET_NODE_REQ, NULL,  &sm_req);
	if (CM_SUCCESS != r_code)
    {
        return r_code;
    }

	if (POS == get_service_result())
	{
                LOGC_DEBUG(CPM_FF_DDSERVICE,"Set Address con (+)\n");
		app_info->env_returns.comm_err = CM_SUCCESS; 
        app_info->env_returns.response_code = 0;
	    r_code = CM_SUCCESS;
    }
   	else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;
		
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Identify (-)");
        
        return CS_SM_ERR(app_info->env_returns.response_code); 
    }

	return r_code;
}

