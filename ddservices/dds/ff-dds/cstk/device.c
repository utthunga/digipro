/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Device Simulator module and Actual device adminstration module.

    This file contains all functions pertaining to the simulation and
    also the actual remote transmitter.

    1.  Read a data file of devices.
    2.  Build the simulated devices ODs.
    3.  Handle any communication with the simulated devices.
    4.  Read the data from the remote device.
    5.  Populate the Block and the device table.
    6.  Read and write values and object description of the device.

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>        // For gettimeofday()
#include <pthread.h>        // For gettimeofday()

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "cm_loc.h"
#include "rcsim.h"
#include "int_diff.h"
#include "obj_desc.h"
#include "fakexmtr.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"
#include "os_helper.h"

#define BLK_TAG_CNT_LEN 4
#define BLK_TAG_MAX_LEN 33

/*
 *  The Simulated Network Table
 */

NETWK_TBL   netwk_tbl;

/***********************************************************************
 *
 *  Name:  log_error
 *
 *  ShortDesc:  Log the FBAP and NMA response error in clogger file.
 *
 *  Description:
 *      The response for FBAP request is of data type T_ERROR
 *      and the error response codes are logged in the clogger
 *      file. LOGC_ERROR is the macro called to write the error
 *      code into file.
 *
 *  Inputs:
 *      error - T_ERROR pointer to global_appl_data_buf.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      None
 *
 *  Author:
 *      Jayanth Mahadeva
 **********************************************************************/
void log_error(T_ERROR *error)
{
    LOGC_ERROR(CPM_FF_DDSERVICE,"Error Class code = %d\n",
        error->class_code);
    LOGC_ERROR(CPM_FF_DDSERVICE,"Additional Detail = %d\n",
        error->add_detail);
    LOGC_ERROR(CPM_FF_DDSERVICE,"Additional Description = %s\n",
        error->add_description);
}

static int read_mode_block(ENV_INFO *env_info,
        unsigned char *mode_block, FBAP_NMA_REQ *req)
{
    int r_code;
    APP_INFO        *app_info = (APP_INFO *)env_info->app_info;

    req->index = req->index + 5;
    req->sub_index = 2;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
            req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }

    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
	    log_error(error);
        app_info->env_returns.response_code = error->class_code;

        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        *mode_block = global_res_buf[sizeof(T_VAR_READ_CNF)];
    }

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name:  save_od_header
 *
 *  ShortDesc:  Save the OD description header for FBAP indecis
 *
 *  Description:
 *      The save_od_header saves the read function block
 *      application process Object Dictionary into the structure
 *      fbap_od_desc of type OD_DESCRIPTION_SPECIFIC
 *
 *  Inputs:
 *      cr - communication reference.
 *      data - OD information data.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *  Author:
 *      Jayanth Mahadeva
 *      Shivaji Tikkireddy
 **********************************************************************/
int save_od_header(USIGN16 cr, UINT8 *data)
{
    NT_COUNT nt_count;
    int r_code;
    NETWORK_HANDLE network_handle;
    OD_DESCRIPTION_SPECIFIC fbap_od_desc;
    OD_DESCRIPTION_SPECIFIC mib_od_desc;
    USIGN8        vfd_nr;

    OD_DESCRIPTION_HDR *p_od_header = (OD_DESCRIPTION_HDR *)(data + 5);

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    /*
     *  Get the network handle for the established connection to the
     *  transmitter device
    */
    r_code = get_nt_net_handle_by_cr(nt_count, cr, &network_handle);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    /*
     *  Save the OD header inforantion into the structure
     *  converting it into little endian format
     */
    vfd_nr = get_device_admin_vfd_nr(cr);
    if(VFD_NR_FBAP == vfd_nr)
    {
        fbap_od_desc.ram_rom_flag = p_od_header->ram_rom_flag;
        fbap_od_desc.name_length = p_od_header->name_length;
        fbap_od_desc.access_protection_flag =
                (p_od_header->access_protection_flag);
        fbap_od_desc.version =
                (p_od_header->version >> 8 ) | (p_od_header->version << 8 );
        fbap_od_desc.stod_object_count = 
                (p_od_header->stod_object_count  >> 8 ) |
                (p_od_header->stod_object_count  << 8 );
        fbap_od_desc.sod_first_index =
                (p_od_header->sod_first_index >> 8 ) |
                (p_od_header->sod_first_index << 8 );
        fbap_od_desc.sod_object_count =
                (p_od_header->sod_object_count >> 8 ) |
                (p_od_header->sod_object_count << 8 );
        fbap_od_desc.dvod_first_index =
                (p_od_header->dvod_first_index >> 8 ) |
                (p_od_header->dvod_first_index << 8);
        fbap_od_desc.dvod_object_count =
                (p_od_header->dvod_object_count >> 8) |
                (p_od_header->dvod_object_count << 8);
        fbap_od_desc.dpod_first_index =
                (p_od_header->dpod_first_index >> 8) |
                (p_od_header->dpod_first_index << 8);
        fbap_od_desc.dpod_object_count =
                (p_od_header->dpod_object_count >> 8) |
                (p_od_header->dpod_object_count << 8);

        // CPMHACK: TODO: In some device stod_object_count is provided with very less value
        // and this creates issue with accessing basic structure / type elements.
        // For these devices, stod_object_count has been manupulated to have a minimum count of
        // atleast SOD_STARTING_INDEX
        if ((fbap_od_desc.stod_object_count < SOD_STARTING_INDEX) &&
            (fbap_od_desc.stod_object_count < fbap_od_desc.sod_first_index))
        {
            LOGC_INFO(CPM_FF_DDSERVICE, "STOD object count (%d) may not be correct and so, it will be resetted to SOD_STARTING_INDEX", fbap_od_desc.stod_object_count);
            fbap_od_desc.stod_object_count = SOD_STARTING_INDEX - 1;
        }

        /*
         *  Save the FBAP Header
         */
        r_code = set_nt_net_fbap_od(network_handle, &fbap_od_desc);
        if (r_code != SUCCESS) {
            return (r_code) ;
        }
    }
    else if(VFD_NR_MIB == vfd_nr)
    {
        mib_od_desc.ram_rom_flag = p_od_header->ram_rom_flag;
        mib_od_desc.name_length = p_od_header->name_length;
        mib_od_desc.access_protection_flag =
                (p_od_header->access_protection_flag);
        mib_od_desc.version =
                (p_od_header->version >> 8 ) | (p_od_header->version << 8 );
        mib_od_desc.stod_object_count =
                (p_od_header->stod_object_count  >> 8 ) |
                (p_od_header->stod_object_count  << 8 );
        mib_od_desc.sod_first_index =
                (p_od_header->sod_first_index >> 8 ) |
                (p_od_header->sod_first_index << 8 );
        mib_od_desc.sod_object_count =
                (p_od_header->sod_object_count >> 8 ) |
                (p_od_header->sod_object_count << 8 );
        mib_od_desc.dvod_first_index =
                (p_od_header->dvod_first_index >> 8 ) |
                (p_od_header->dvod_first_index << 8);
        mib_od_desc.dvod_object_count =
                (p_od_header->dvod_object_count >> 8) |
                (p_od_header->dvod_object_count << 8);
        mib_od_desc.dpod_first_index =
                (p_od_header->dpod_first_index >> 8) |
                (p_od_header->dpod_first_index << 8);
        mib_od_desc.dpod_object_count =
                (p_od_header->dpod_object_count >> 8) |
                (p_od_header->dpod_object_count << 8);
        /*
         *  Save the MIB Header
         */
        r_code = set_nt_net_mib_od(network_handle, &mib_od_desc);
        if (r_code != SUCCESS) {
            return (r_code) ;
        }
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Invalid CR");
    }
    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name:  convert_to_ltl_endian
 *
 *  ShortDesc:  Convert to little endian format.
 *
 *  Description:
 *      The convert_to_ltl_endian takes unsigned int data as input
 *      and converts it to little endian format.
 *
 *  Inputs:
 *      data - Integer value to be converted to little endian.
 *
 *  Outputs:
 *      data - Converted to little endian.
 *
 *  Returns:
 *      data - Converted to little endian format.
 *
 *  Author:
 *      Rohit Bhosale
 *
 **********************************************************************/
unsigned int convert_to_ltl_endian(unsigned int data)
{
    /*
     * Convertion for two byte data
     */
    if(data<=0xffff)
    {
        data = ((data & 0xff) << 8) |
               ((data & 0xff00) >> 8);
    }
    /*
     * Conversion for four byte data
     */
    else{
        data = ((data & 0xff) << 24) | ((data & 0xff00) << 8) |
               ((data & 0xff0000) >> 8) | ((data & 0xff000000) >> 24);
    }
    return data;
}

/***********************************************************************
 *
 *  Name:  bit_rev
 *
 *  ShortDesc:  Reversing the bit string data.
 *
 *  Description:
 *      The bit_rev function takes an bit string data and its size
 *      and reverse its bits.
 *
 *  Inputs:
 *      val - Bit string data.
 *      size - size of bit string data.
 *
 *  Outputs:
 *      val - Bit string reversed data.
 *
 *  Returns:
 *      none.
 *
 *  Author:
 *      Rohit Bhosale
 *
 **********************************************************************/
void bit_rev(unsigned char *val,unsigned int size)
{
    unsigned int i=0;
    unsigned long   temp_value=0x00,temp=0;
    (void *)memcpy((unsigned long *)&temp_value, (char *)val, size);

    /*
     * Loop through number of bits in val and reverse the the bit
     * Swapping first to last bit through the count
     */
    for(i = 0; i < (size * 8); i++){
        unsigned long temp1 = 0;
        temp1 = (0x1) & temp_value >>  i;
        temp |= temp1 << (((size * 8) -1) - i);
    }
    (void *)memcpy((char *)val, (unsigned long *)&temp, size);
}

/***********************************************************************
 *
 *  Name:  put_integer
 *
 *  ShortDesc:  Put an integer into a byte stream.
 *
 *  Description:
 *      The put_integer function takes an integer and puts it into
 *      a byte stream.
 *
 *  Inputs:
 *      value - integer value to be put into byte stream.
 *      bin_ptr - pointer to byte stream.
 *      bin_size - binary size of integer.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_PUT_INT_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
put_integer(UINT32 value, UINT8 *bin_ptr, int bin_size)
{

    UINT32  remainder;
    UINT32  divisor;
    UINT32  quotient;
    int     byte_num;
    int     power;

    if ((bin_size <= 0) || (bin_size > 4)) {
        return(FFDS_PUT_INT_ERR);
    }

    remainder = value;

    for (byte_num = 0; byte_num < bin_size; byte_num ++) {
        divisor = 1;
        for (power = 0; power < (bin_size - byte_num -1); power ++) {
            divisor <<= 8;
        }

        if (remainder >= divisor) {
            quotient = (UINT32)(remainder/divisor);
            if (quotient > 256) {
                return(FFDS_PUT_INT_ERR);
            }
            bin_ptr[byte_num] = (unsigned char)(quotient);
            remainder -= (UINT32)(bin_ptr[byte_num] * divisor);
        }
        else {
            bin_ptr[byte_num] = (UINT8)0;
        }
    }
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  get_integer
 *
 *  ShortDesc:  Get an integer from a byte stream.
 *
 *  Description:
 *      The get_integer function takes byte stream pointer and size and
 *      returns the integer value.
 *
 *  Inputs:
 *      bin_ptr - pointer to byte stream.
 *      bin_size - binary size of integer.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Integer value.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static UINT32
get_integer(UINT8 *bin_ptr, int bin_size)
{
    UINT32  value = 0;
    int     byte_num;

    /*
     *  Go through each byte and total up the value.
     */

    for (byte_num = 0; byte_num < bin_size; byte_num ++) {

        value <<= 8;
        value += bin_ptr[byte_num];
    }
    return(value);
}

/***********************************************************************
 *
 *  Name:  build_block_param_objects
 *
 *  ShortDesc:  Build a block's parameter objects in a simulated
 *              Type A device.
 *
 *  Description:
 *      The build_block_param_objects function takes device data
 *      (Device Data Table element) and a block number and builds the
 *      corresponding block parameter objects in a simulated Type A device.
 *      These objects are built in the ROD heap (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *      blk_num - the number of the block whose parameter objects are
 *                to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_block_param_objects(DEV_DATA *dev_data, int blk_num)
{
    int      param_num;
    OBJECT  *object;
    int      r_code;

    /*
     *  Go through each parameter in the block.
     */

    for (param_num = 1; param_num < dev_data->
            blk_data_tbl.list[blk_num].param_count; param_num ++) {

        /*
         *  Allocate space in the ROD heap for an object, and set it
         *  up as a null object.
         */

        object = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
        if (!object) {
            return(FFDS_ROD_ERR);
        }

        object->index = (OBJECT_INDEX)
          (dev_data->blk_data_tbl.list[blk_num].op_index + param_num);
        object->code = OC_NULL;

        /*
         *  Put the null object (parameter object) into the OPOD.
         */

        r_code = rod_put(dev_data->opod_handle,object->index,
                object);
        if (r_code) {
            return(FFDS_ROD_ERR);
        }
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  build_block_object
 *
 *  ShortDesc:  Build a Block Object in a simulated Type A device.
 *
 *  Description:
 *      The build_block_object function takes device data (Device Data
 *      Table element) and a block number and builds the corresponding
 *      block object in a simulated Type A device.  This object is built
 *      in the ROD heap (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *      blk_num - the number of the block whose object is to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_block_object(DEV_DATA *dev_data, int blk_num)
{

    OBJECT          *object;
    UINT8           *value;
    int              r_code;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as a record object.
     */

    object = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
    if (!object) {
        return(FFDS_ROD_ERR);
    }

    object->index = dev_data->blk_data_tbl.list[blk_num].op_index;
    object->code = OC_RECORD;

    /*
     *  Allocate space in the ROD heap for a record object's specific
     *  information, and set it up as a Block Object.
     */

    object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object->specific) {
        return(FFDS_ROD_ERR);
    }

    object->specific->record.type = DT_BLOCK;

    /*
     *  Put the record object (Block Object) into the OPOD.
     */

    r_code = rod_put(dev_data->opod_handle,object->index,object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    /*
     *  Set up the Block Object's value according to information
     *  in the Device's Block Data Table.
     */

    value = (UINT8 *) calloc(1,BLK_SIZE);
    if (!value) {
        return(FFDS_MEMORY_ERR);
    }
    r_code = put_integer(
            (UINT32)dev_data->blk_data_tbl.list[blk_num].class_type,
            (value + BLK_CLASS_OFFSET),BLK_CLASS_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }
    r_code = put_integer(
            dev_data->blk_data_tbl.list[blk_num].dd_blk_id,
            (value + BLK_DD_REFERENCE_OFFSET),BLK_DD_REFERENCE_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }
    r_code = put_integer(
            (UINT32)dev_data->blk_data_tbl.list[blk_num].param_count,
            (value + BLK_PARAM_COUNT_OFFSET),BLK_PARAM_COUNT_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    /*
     *  Write the record object's value (Block Object's value)
     *  into the OPOD.
     */

    r_code = rod_write(dev_data->opod_handle,object->index,
            (SUBINDEX)0,value);
    if (r_code) {
        free((char*)value);
        return(FFDS_ROD_ERR);
    }

    free((char *)value);
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  build_block_reference_object
 *
 *  ShortDesc:  Build a Block Reference Object in a simulated Type A
 *              device.
 *
 *  Description:
 *      The build_block_reference_object function takes device data
 *      (Device Data Table element) and a block number and builds the
 *      corresponding block reference object in a simulated Type A device.
 *      This object is built in the ROD heap (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *      blk_num - the number of the block whose reference object is to
 *                be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_block_reference_object(DEV_DATA *dev_data, int blk_num, NET_TYPE net_type)
{

    OBJECT                  *object;
    UINT8                   *value;
    int                      r_code;
    OBJECT_DICTIONARY                       *rod;
    OD_DESCRIPTION_SPECIFIC         *od_description;

    rod = rod_tbl.list[dev_data->opod_handle];
    od_description = &rod->od_description->specific->od_description;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as a record object.
     */

    object = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
    if (!object) {
        return(FFDS_ROD_ERR);
    }

    object->code = OC_RECORD;

    /*
     *  Read the value of the OPOD Directory Object and set the
     *  object index of the Block Reference Object.
     */

    r_code = rod_read(dev_data->opod_handle,
            ((net_type != NT_A && (UINT16)SOD_STARTING_INDEX > od_description->stod_object_count) ? (UINT16)SOD_STARTING_INDEX : od_description->sod_first_index),(SUBINDEX)0,&value,NULL,NULL);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    switch (dev_data->blk_data_tbl.list[blk_num].class_type) {

        case BC_PHYSICAL:
            object->index = (OBJECT_INDEX)
                    (get_integer((value + PHYS_BLK_REF_INDEX_OFFSET),
                    PHYS_BLK_REF_INDEX_SIZE));
            break;

        case BC_INPUT:
            object->index = (OBJECT_INDEX)
                    (get_integer((value + FUNCT_BLK_REF_INDEX_OFFSET),
                    FUNCT_BLK_REF_INDEX_SIZE) + (UINT32)
                    dev_data->blk_data_tbl.list[blk_num].class_num);
            break;

        case BC_OUTPUT:
            object->index = (OBJECT_INDEX)
                    (get_integer((value + FUNCT_BLK_REF_INDEX_OFFSET),
                    FUNCT_BLK_REF_INDEX_SIZE) + (UINT32)
                    dev_data->blk_data_tbl.list[blk_num].class_num);
            break;

        case BC_CONTROL:
            object->index = (OBJECT_INDEX)
                    (get_integer((value + FUNCT_BLK_REF_INDEX_OFFSET),
                    FUNCT_BLK_REF_INDEX_SIZE) + (UINT32)
                    dev_data->blk_data_tbl.list[blk_num].class_num);
            break;

        case BC_TRANSDUCER:
            object->index = (OBJECT_INDEX)
                    (get_integer((value + TRANS_BLK_REF_INDEX_OFFSET),
                    TRANS_BLK_REF_INDEX_SIZE) + (UINT32)
                    dev_data->blk_data_tbl.list[blk_num].class_num);
            break;

        default:
            return(FFDS_BLK_CLASS_ERR);
    }

    /*
     *  Allocate space in the ROD heap for a record object's specific
     *  information, and set it up as a Block Reference Object.
     */

    object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object->specific) {
        return(FFDS_ROD_ERR);
    }

    object->specific->record.type = DT_BLOCK_REFERENCE;

    /*
     *  Put the record object (Block Reference Object) into the OPOD.
     */

    r_code = rod_put(dev_data->opod_handle,object->index, object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    /*
     *  Set up the Block Reference Object's value according to
     *  information in the Device's Block Data Table.
     */

    value = (UINT8 *) calloc(1, BLK_REF_SIZE);
    if (!value) {
        return(FFDS_MEMORY_ERR);
    }

    (void)strcpy((char *)(value + BLK_REF_BLK_TAG_OFFSET),
            dev_data->blk_data_tbl.list[blk_num].blk_tag);

    r_code = put_integer(dev_data->blk_data_tbl.list[blk_num].dd_blk_id,
            (value + BLK_REF_DD_NAME_OFFSET),
            BLK_REF_DD_NAME_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    r_code = put_integer(
            (UINT32)dev_data->blk_data_tbl.list[blk_num].op_index,
            (value + BLK_REF_STARTING_INDEX_OFFSET),
            BLK_REF_STARTING_INDEX_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    /*
     *  Write the record object's value (Block Reference Object's
     *  value) into the OPOD.
     */

    r_code = rod_write(dev_data->opod_handle, object->index,
            (SUBINDEX)0, value);
    if (r_code) {
        free((char*)value);
        return(FFDS_ROD_ERR);
    }

    free((char *)value);
    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  build_directory_object
 *
 *  ShortDesc:  Build the Directory Object in a simulated Type A device.
 *
 *  Description:
 *      The build_directory_object function takes device data
 *      (Device Data Table element) and builds the directory object in
 *      a simulated Type A device.  This object is built in the ROD heap
 *      (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_directory_object(DEV_DATA *dev_data, NET_TYPE net_type)
{

    OBJECT          *object;
    UINT8           *value;
    int              r_code;
    OBJECT_DICTIONARY                       *rod;
    OD_DESCRIPTION_SPECIFIC         *od_description;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as an array object.
     */

    object = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
    if (!object) {
        return(FFDS_ROD_ERR);
    }
 

    rod = rod_tbl.list[dev_data->opod_handle];
    od_description = &rod->od_description->specific->od_description;
	
    object->index = (net_type !=  NT_A && (UINT16)SOD_STARTING_INDEX > od_description->stod_object_count) ? (UINT16)SOD_STARTING_INDEX : od_description->sod_first_index;
    object->code = OC_ARRAY;

    /*
     *  Allocate space in the ROD heap for an array object's specific
     *  information, and set it up as a Directory Object.
     */

    object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object->specific) {
        return(FFDS_ROD_ERR);
    }

    object->specific->array.type = DIRECTORY_ELEM_TYPE;
    object->specific->array.size = DIRECTORY_ELEM_SIZE;
    object->specific->array.elem_count = DIRECTORY_ELEM_COUNT;

    /*
     *  Put the array object (Directory Object) into the OPOD.
     */

    r_code = rod_put(dev_data->opod_handle, object->index, object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    /*
     *  Set up the Directory Object's value according to information
     *  in the Device Data Table.
     */

    value = (UINT8 *) calloc(1, DIRECTORY_SIZE);
    if (!value) {
        return(FFDS_MEMORY_ERR);
    }

    /* Physical Block Reference Object */

    r_code = put_integer(
            (UINT32)((net_type != NT_A && (UINT16)SOD_STARTING_INDEX > od_description->stod_object_count) ? (UINT16)SOD_STARTING_INDEX : od_description->sod_first_index) + 1,
            (value + PHYS_BLK_REF_INDEX_OFFSET),
            PHYS_BLK_REF_INDEX_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    /* Function Block Reference Objects */

    if (dev_data->funct_blk_count > 0) {

        r_code = put_integer(
                get_integer((value + PHYS_BLK_REF_INDEX_OFFSET),
                PHYS_BLK_REF_INDEX_SIZE) + 1,
                (value + FUNCT_BLK_REF_INDEX_OFFSET),
                FUNCT_BLK_REF_INDEX_SIZE);
        if (r_code != FFDS_SUCCESS) {
            free((char*)value);
            return(r_code);
        }
    }
    else {

        r_code = put_integer((UINT32)0,
                (value + FUNCT_BLK_REF_INDEX_OFFSET),
                FUNCT_BLK_REF_INDEX_SIZE);
        if (r_code != FFDS_SUCCESS) {
            free((char*)value);
            return(r_code);
        }
    }

    r_code = put_integer((UINT32)dev_data->funct_blk_count,
            (value + FUNCT_BLK_COUNT_OFFSET),
            FUNCT_BLK_COUNT_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    /* Transducer Block Reference Objects */

    if (dev_data->trans_blk_count > 0) {

        r_code = put_integer(
                get_integer((value + PHYS_BLK_REF_INDEX_OFFSET),
                PHYS_BLK_REF_INDEX_SIZE) + 1 +
                get_integer((value + FUNCT_BLK_COUNT_OFFSET),
                FUNCT_BLK_COUNT_SIZE),
                (value + TRANS_BLK_REF_INDEX_OFFSET),
                TRANS_BLK_REF_INDEX_SIZE);
        if (r_code != FFDS_SUCCESS) {
            free((char*)value);
            return(r_code);
        }
    }
    else {

        r_code = put_integer((UINT32)0,
                (value + TRANS_BLK_REF_INDEX_OFFSET),
                TRANS_BLK_REF_INDEX_SIZE);
        if (r_code != FFDS_SUCCESS) {
            free((char*)value);
            return(r_code);
        }
    }

    r_code = put_integer((UINT32)dev_data->trans_blk_count,
            (value + TRANS_BLK_COUNT_OFFSET),
            TRANS_BLK_COUNT_SIZE);
    if (r_code != FFDS_SUCCESS) {
        free((char*)value);
        return(r_code);
    }

    /*
     *  Write the array object's value (Directory Object's value)
     *  into the OPOD.
     */

    r_code = rod_write(dev_data->opod_handle, object->index,
            (SUBINDEX)0, value);
    if (r_code) {
        free((char*)value);
        return(FFDS_ROD_ERR);
    }

    free((char *)value);
    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  build_block_ref_type_object
 *
 *  ShortDesc:  Build a data type structure object for a Block (record)
 *              Reference Object (Type 50).
 *
 *  Description:
 *      The build_block_ref_type_object function takes device data
 *      (Device Data Table element) and builds a data type structure
 *      object for a Block (record) Reference Object (Type 32) in a
 *      simulated Type A device.  This object is built in the ROD heap
 *      (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_block_ref_type_object(DEV_DATA *dev_data, NET_TYPE net_type)
{

    OBJECT          *object;
    OBJECT_INDEX    *type_list;
    UINT8           *size_list;
    int              r_code;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as a data type structure object.
     */

    object = (OBJECT *) rod_calloc(1, sizeof(OBJECT));
    if (!object) {
        return(FFDS_ROD_ERR);
    }

    object->index = DT_BLOCK_REFERENCE;
    object->code = OC_DATA_TYPE_STRUCTURE;

    /*
     *  Allocate space in the ROD heap for a data type structure
     *  object's specific information, its type list, and size list.
     */

    object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object->specific) {
        return(FFDS_ROD_ERR);
    }

    if(NT_OFF_LINE == net_type)
    {
        type_list = (OBJECT_INDEX *) rod_calloc(DT_SIM_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!type_list) {
            return(FFDS_ROD_ERR);
        }
        size_list = (UINT8 *) rod_calloc(DT_SIM_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!size_list) {
            return(FFDS_ROD_ERR);
        }
    }
    else
    {
        type_list = (OBJECT_INDEX *) rod_calloc(DT_ACTUAL_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!type_list) {
            return(FFDS_ROD_ERR);
        }
        size_list = (UINT8 *) rod_calloc(DT_ACTUAL_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!size_list) {
            return(FFDS_ROD_ERR);
        }
    }

    /*
     *  Set up the type list and size list according to definitions
     *  of the Block Reference Object (Type 50).
     */

    type_list[BLK_REF_BLK_TAG_NUM] = BLK_REF_BLK_TAG_TYPE;
    size_list[BLK_REF_BLK_TAG_NUM] = BLK_REF_BLK_TAG_SIZE;

    type_list[BLK_REF_DD_NAME_NUM] = BLK_REF_DD_NAME_TYPE;
    size_list[BLK_REF_DD_NAME_NUM] = BLK_REF_DD_NAME_SIZE;

    type_list[BLK_REF_STARTING_INDEX_NUM] = BLK_REF_STARTING_INDEX_TYPE;
    size_list[BLK_REF_STARTING_INDEX_NUM] = BLK_REF_STARTING_INDEX_SIZE;

    /*
     *  Set the data type structure object's element count, and
     *  assign its type list and size list.
     */

    object->specific->data_type_structure.elem_count =
            DT_BLOCK_REFERENCE_COUNT;
    object->specific->data_type_structure.type_list = type_list;
    object->specific->data_type_structure.size_list = size_list;

    /*
     *  Put the data type structure object into the OPOD.
     */

    r_code = rod_put(dev_data->opod_handle, object->index, object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  build_block_type_object
 *
 *  ShortDesc:  Build a data type structure object for a Block (record)
 *              Object (Type 32).
 *
 *  Description:
 *      The build_block_type_object function takes device data (Device
 *      Data Table element) and builds a data type structure object for
 *      a Block (record) Object (Type 32) in a simulated Type A device.
 *      This object is built in the ROD heap (using the ROD Manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose block type object
 *                 is to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_block_type_object(DEV_DATA *dev_data, NET_TYPE net_type)
{

    OBJECT          *object;
    OBJECT_INDEX    *type_list;
    UINT8           *size_list;
    int              r_code;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as a data type structure object.
     */

    object = (OBJECT *) rod_calloc(1, sizeof(OBJECT));
    if (!object) {
        return(FFDS_ROD_ERR);
    }

    object->index = DT_BLOCK;
    object->code = OC_DATA_TYPE_STRUCTURE;

    /*
     *  Allocate space in the ROD heap for a data type structure
     *  object's specific information, its type list, and size list.
     */

    object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object->specific) {
        return(FFDS_ROD_ERR);
    }

    if(NT_OFF_LINE == net_type)
    {
        type_list = (OBJECT_INDEX *) rod_calloc(DT_SIM_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!type_list) {
            return(FFDS_ROD_ERR);
        }
        size_list = (UINT8 *) rod_calloc(DT_SIM_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!size_list) {
            return(FFDS_ROD_ERR);
        }

        type_list[BLK_SIM_PARAM_COUNT_NUM] = BLK_PARAM_COUNT_TYPE;
        size_list[BLK_SIM_PARAM_COUNT_NUM] = BLK_PARAM_COUNT_SIZE;

        type_list[BLK_SIM_DSPLY_LIST_INDEX_NUM] = BLK_DSPLY_LIST_INDEX_TYPE;
        size_list[BLK_SIM_DSPLY_LIST_INDEX_NUM] = BLK_DSPLY_LIST_INDEX_SIZE;

        type_list[BLK_SIM_DSPLY_LIST_COUNT_NUM] = BLK_DSPLY_LIST_COUNT_TYPE;
        size_list[BLK_SIM_DSPLY_LIST_COUNT_NUM] = BLK_DSPLY_LIST_COUNT_SIZE;
    }
    else
    {
        type_list = (OBJECT_INDEX *) rod_calloc(DT_ACTUAL_BLOCK_COUNT,
                sizeof(OBJECT_INDEX));
        if (!type_list) {
            return(FFDS_ROD_ERR);
        }
        size_list = (UINT8 *) rod_calloc(DT_ACTUAL_BLOCK_COUNT,
             sizeof(OBJECT_INDEX));
        if (!size_list) {
            return(FFDS_ROD_ERR);
        }

        type_list[BLK_ACTUAL_PARAM_COUNT_NUM] = BLK_PARAM_COUNT_TYPE;
        size_list[BLK_ACTUAL_PARAM_COUNT_NUM] = BLK_PARAM_COUNT_SIZE;

        type_list[BLK_ACTUAL_DSPLY_LIST_INDEX_NUM] = BLK_DSPLY_LIST_INDEX_TYPE;
        size_list[BLK_ACTUAL_DSPLY_LIST_INDEX_NUM] = BLK_DSPLY_LIST_INDEX_SIZE;

        type_list[BLK_ACTUAL_DSPLY_LIST_COUNT_NUM] = BLK_DSPLY_LIST_COUNT_TYPE;
        size_list[BLK_ACTUAL_DSPLY_LIST_COUNT_NUM] = BLK_DSPLY_LIST_COUNT_SIZE;
    }

    /*
     *  Set up the type list and size list according to definitions
     *  of the Block Object (Type 32).
     */

    type_list[BLK_CLASS_NUM] = BLK_CLASS_TYPE;
    size_list[BLK_CLASS_NUM] = BLK_CLASS_SIZE;

    type_list[BLK_PARENT_CLASS_NUM] = BLK_PARENT_CLASS_TYPE;
    size_list[BLK_PARENT_CLASS_NUM] = BLK_PARENT_CLASS_SIZE;

    type_list[BLK_DD_REFERENCE_NUM] = BLK_DD_REFERENCE_TYPE;
    size_list[BLK_DD_REFERENCE_NUM] = BLK_DD_REFERENCE_SIZE;

    type_list[BLK_DD_REVISION_NUM] = BLK_DD_REVISION_TYPE;
    size_list[BLK_DD_REVISION_NUM] = BLK_DD_REVISION_SIZE;

    type_list[BLK_PROFILE_NUM] = BLK_PROFILE_TYPE;
    size_list[BLK_PROFILE_NUM] = BLK_PROFILE_SIZE;

    type_list[BLK_PROFILE_REVISION_NUM] = BLK_PROFILE_REVISION_TYPE;
    size_list[BLK_PROFILE_REVISION_NUM] = BLK_PROFILE_REVISION_SIZE;

    type_list[BLK_EXECUTION_TIME_NUM] = BLK_EXECUTION_TIME_TYPE;
    size_list[BLK_EXECUTION_TIME_NUM] = BLK_EXECUTION_TIME_SIZE;

    /*
     *  Set the data type structure object's element count, and
     *  assign its type list and size list.
     */

    if(NT_OFF_LINE == net_type)
    {
        object->specific->data_type_structure.elem_count =
                DT_SIM_BLOCK_COUNT;
    }
    else
    {
        object->specific->data_type_structure.elem_count =
                DT_ACTUAL_BLOCK_COUNT;
    }
    object->specific->data_type_structure.type_list = type_list;
    object->specific->data_type_structure.size_list = size_list;

    /*
     *  Put the data type structure object into the OPOD.
     */

    r_code = rod_put(dev_data->opod_handle, object->index, object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  build_device
 *
 *  ShortDesc:  Build a simulated Type A device.
 *
 *  Description:
 *      The build_device function takes device data (Device Data Table
 *      element) and builds a simulated device in the ROD heap (using
 *      the ROD manager).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose simulated device
 *                 is to be built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      Return values from build_directory_object function.
 *      Return values from build_block_object function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
build_device(DEV_DATA *dev_data, NETWORK_HANDLE nh)
{

    OBJECT      *object_0;
    int          blk_num;
    int          r_code;
    NET_TYPE     net_type;
    OD_DESCRIPTION_SPECIFIC fbap_od_desc;

    /*
     *  Allocate space in the ROD heap for an object, and set it up
     *  as an OD description object.
     */

    object_0 = (OBJECT *) rod_calloc(1, sizeof(OBJECT));
    if (!object_0) {
        return(FFDS_ROD_ERR);
    }

    object_0->index = 0;
    object_0->code = OC_OD_DESCRIPTION;

    /*
     *  Allocate space in the ROD heap for an OD description object's
     *  specific information, and set it up to describe the OPOD.
     */

    object_0->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
            sizeof(OBJECT_SPECIFIC));
    if (!object_0->specific) {
        return(FFDS_ROD_ERR);
    }

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch(net_type)
    {
        case NT_A_SIM:
        case NT_OFF_LINE:

            object_0->specific->od_description.ram_rom_flag = 0;
            object_0->specific->od_description.name_length = 0;
            object_0->specific->od_description.access_protection_flag = 0;
            object_0->specific->od_description.version = 0;
            object_0->specific->od_description.stod_object_count =
                    (UINT16)(SOD_STARTING_INDEX - 1);
            object_0->specific->od_description.sod_first_index =
                    (OBJECT_INDEX)SOD_STARTING_INDEX;
            object_0->specific->od_description.sod_object_count =
                    (UINT16)dev_data->sod_size;
            object_0->specific->od_description.dvod_first_index = 0;
            object_0->specific->od_description.dvod_object_count = 0;
            object_0->specific->od_description.dpod_first_index = 0;
            object_0->specific->od_description.dpod_object_count = 0;

            break;

        case NT_A:

            r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
            if (r_code != SUCCESS) {
                return (r_code) ;
            }

            object_0->specific->od_description.ram_rom_flag =
                        fbap_od_desc.ram_rom_flag;
            object_0->specific->od_description.name_length =
                        fbap_od_desc.name_length;
            object_0->specific->od_description.access_protection_flag =
                        fbap_od_desc.access_protection_flag;
            object_0->specific->od_description.version =
                        fbap_od_desc.version;
            object_0->specific->od_description.stod_object_count =
                        fbap_od_desc.stod_object_count;
            object_0->specific->od_description.sod_first_index =
                        fbap_od_desc.sod_first_index;
            object_0->specific->od_description.sod_object_count =
                        (UINT16)dev_data->sod_size;
            object_0->specific->od_description.dvod_first_index =
                        fbap_od_desc.dvod_first_index;
            object_0->specific->od_description.dvod_object_count =
                        fbap_od_desc.dvod_object_count;
            object_0->specific->od_description.dpod_first_index =
                        fbap_od_desc.dpod_first_index;
            object_0->specific->od_description.dpod_object_count =
                        fbap_od_desc.dpod_object_count;

            break;
    }

    /*
     *  Open up the OPOD with Object 0 (using the ROD Manager).
     */

    dev_data->opod_handle = rod_open(object_0);
    if (dev_data->opod_handle < 0) {
        return(FFDS_ROD_ERR);
    }

    /*
     *  Build the OPOD's data type structure objects for block
     *  (record) objects and block reference (record) objects.
     */

    r_code = build_block_type_object(dev_data, net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    r_code = build_block_ref_type_object(dev_data, net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    /*
     *  Build the OPOD's Directory Object.
     */

    r_code = build_directory_object(dev_data, net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    /*
     *  Go through each block in the device's Block Data Table.
     */

    for (blk_num = 0; blk_num < dev_data->blk_data_tbl.count;
            blk_num ++) {

        /*
         *  Build the Block Reference Object.
         */

        r_code = build_block_reference_object(dev_data, blk_num, net_type);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }

        /*
         *  Build the Block Object (The Characteristics Record).
         */

        r_code = build_block_object(dev_data, blk_num);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }

        /*
         *  Build the block's parameter objects.
         */

        r_code = build_block_param_objects(dev_data, blk_num);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  calculate_sod_size
 *
 *  ShortDesc:  Calculate the size of the device's SOD.
 *
 *  Description:
 *      The calculate_sod_size function takes device data (Device Data
 *      Table element) and calculates the size of the device's Static
 *      Object Dictionary (SOD).
 *
 *  Inputs:
 *      dev_data - the Device Data Table element whose SOD size is to
 *                 be calculated.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Void.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
calculate_sod_size(DEV_DATA *dev_data, NETWORK_HANDLE nh)
{
    int     blk_num;
    int     max_blk_num = 0;
    NET_TYPE    net_type;
    OD_DESCRIPTION_SPECIFIC fbap_od_desc;
    int r_code;

    /*
     *  Go through each block in the device's Block Data Table.
     */

    for (blk_num = 0; blk_num < dev_data->blk_data_tbl.count;
            blk_num ++) {

        /*
         *  Check if the block has the highest operational index.
         */

        if (dev_data->blk_data_tbl.list[blk_num].op_index >
                dev_data->blk_data_tbl.list[max_blk_num].op_index) {
            max_blk_num = blk_num;
        }
    }

    /*
     *  Calculate the size of the device's SOD.  The +1 is listed because
     *  the param_count that is part of dev_data does not seem to include
     *  the block characteristic object, so we must account for that here.
     */

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }


    switch(net_type)
    {
        case NT_A_SIM:
        case NT_OFF_LINE:

            dev_data->sod_size =
                (dev_data->blk_data_tbl.list[max_blk_num].op_index +
                dev_data->blk_data_tbl.list[max_blk_num].param_count + 1) -
                SOD_STARTING_INDEX;
            break;

        case NT_A:


            r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
            if (r_code != SUCCESS) {
                return (r_code) ;
            }


            dev_data->sod_size =
                (dev_data->blk_data_tbl.list[max_blk_num].op_index +
                dev_data->blk_data_tbl.list[max_blk_num].param_count + 1) -
                fbap_od_desc.sod_first_index;

            break;
    }

    return SUCCESS;
}


/***********************************************************************
 *
 *  Name:  check_od_conflict
 *
 *  ShortDesc:  Check for OD conflict within a device.
 *
 *  Description:
 *      The check_od_conflict function takes device data (Device Data
 *      Table element) and checks for any OD conflict within the
 *      device.
 *
 *  Inputs:
 *      dev_data - the Device Data Table element to be checked.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_OD_CONFLICT_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
check_od_conflict(DEV_DATA *dev_data)
{

    int     blk_num;
    int     other_blk_num;
    int     lowest_possible_blk_index;

    /*
     *  Calculate the lowest possible index that any block can
     *  start at.  This is based upon reserving room for the
     *  Directory Object, the Physical Block Reference Object,
     *  the Function Block Reference Objects, and the Transducer
     *  Block Reference Objects.
     */

    lowest_possible_blk_index = SOD_STARTING_INDEX + 1 +
            dev_data->phys_blk_count + dev_data->funct_blk_count +
            dev_data->trans_blk_count;

    /*
     *  Go through each block in the device's Block Data Table.
     */

    for (blk_num = 0; blk_num < dev_data->blk_data_tbl.count;
            blk_num ++) {

        /*
         *  Check to make sure that the operational index of the
         *  block is not lower than the lowest possible index that
         *  any block can start at.
         */

        if (dev_data->blk_data_tbl.list[blk_num].op_index <
                lowest_possible_blk_index) {
            return(FFDS_OD_CONFLICT_ERR);
        }

        /*
         *  Go through each of the other blocks in the device's
         *  Block Data Table.
         */

        for (other_blk_num = 0; other_blk_num <
                dev_data->blk_data_tbl.count; other_blk_num ++) {

            if (other_blk_num == blk_num) {
                continue;
            }

            /*
             *  Check to make sure that the operational indices of
             *  the two blocks are not in conflict.
             */

            if ((dev_data->
                    blk_data_tbl.list[other_blk_num].op_index >=
                    dev_data->
                    blk_data_tbl.list[blk_num].op_index) &&
                    (dev_data->
                    blk_data_tbl.list[other_blk_num].op_index <
                    dev_data->
                    blk_data_tbl.list[blk_num].op_index +
                    dev_data->
                    blk_data_tbl.list[blk_num].param_count)) {

                return(FFDS_OD_CONFLICT_ERR);
            }
        }
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  validate_device_data
 *
 *  ShortDesc:  Validate the device data.
 *
 *  Description:
 *      The validate_device_data function takes device data (Device
 *      Data Table element) and validates the data, making sure that
 *      there is one and only one Physical Block, and that there are
 *      no OD conflicts.
 *
 *  Inputs:
 *      dev_data - the Device Data Table element to be validated.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_PHYS_BLK_ERR.
 *      Return values from check_od_conflict function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
validate_device_data(DEV_DATA *dev_data)
{

    int     r_code;

    /*
     *  Check to make sure that there is one and only one Physical
     *  Block in the device.
     */

    if (dev_data->phys_blk_count != 1) {
        return(FFDS_PHYS_BLK_ERR);
    }

    /*
     *  Check to make sure that there are no OD conflicts.
     */

    r_code = check_od_conflict(dev_data);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  build_devices
 *
 *  ShortDesc:  Build the simulated Type A devices for the specified
 *              network.
 *
 *  Description:
 *      The build_devices function takes a network number and builds
 *      the simulated Type A devices for that network.
 *
 *  Inputs:
 *      netwk_num - the number of the network whose devices are to be
 *                  built.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from validate_device_data function.
 *      Return values from build_device function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
build_devices(int netwk_num)
{

    int          dev_num;
    int          r_code;

    /*
     *  Go through each device in the Device Data Table.
     */

    for (dev_num = 0; dev_num < netwk_tbl.list[netwk_num].
            dev_data_tbl.count; dev_num ++) {

        /*
         *  Validate the device's data.
         */

        r_code = validate_device_data(
            &netwk_tbl.list[netwk_num].dev_data_tbl.list[dev_num]);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }

        /*
         *  Calculate the size of the device's SOD.
         */

        calculate_sod_size(
            &netwk_tbl.list[netwk_num].dev_data_tbl.list[dev_num], netwk_num);

        /*
         *  Build the device.
         */

        r_code = build_device(
            &netwk_tbl.list[netwk_num].dev_data_tbl.list[dev_num], netwk_num);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  add_block_to_device_data
 *
 *  ShortDesc:  Add a block to an element in the Device Data Table.
 *
 *  Description:
 *      The add_block_to_device_data function takes a block entry
 *      and device data (Device Data Table Element) and adds the
 *      block entry's information to the table element.
 *
 *  Inputs:
 *      blk_entry - the block entry to be added.
 *      dev_data - the Device Data Table element receiving the block
 *                 entry data.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *      Return values from ds_blk_name_to_item_id function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
add_block_to_device_data(BLK_ENTRY *blk_entry,
    DEV_DATA *dev_data,
    int net_type
)
{

    int     blk_num;
    int     r_code;

    /*
     *  Increase the Device's Block Data Table by one element.
     */

    dev_data->blk_data_tbl.list = (BLK_DATA *) realloc(
            (char *)dev_data->blk_data_tbl.list, (unsigned int)
            (sizeof(BLK_DATA) * (dev_data->blk_data_tbl.count + 1)));
    if (!dev_data->blk_data_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }
    dev_data->blk_data_tbl.count ++;

    /*
     *  Initialize the Device's Block Data Table element with data
     *  from the block entry.
     */

    blk_num = dev_data->blk_data_tbl.count - 1;

    (void)strcpy(dev_data->blk_data_tbl.list[blk_num].blk_tag,
            blk_entry->blk_tag);
    dev_data->blk_data_tbl.list[blk_num].op_index =
            (OBJECT_INDEX)blk_entry->op_index;
    dev_data->blk_data_tbl.list[blk_num].param_count =
            blk_entry->param_count;
    dev_data->blk_data_tbl.list[blk_num].class_type = blk_entry->class_type;
    dev_data->blk_data_tbl.list[blk_num].dd_blk_id = blk_entry->dd_item_id;
    dev_data->blk_data_tbl.list[blk_num].profile = blk_entry->profile;
    dev_data->blk_data_tbl.list[blk_num].mode_block = blk_entry->mode_block;


    /*
     *  Increment the count of the corresponding block class_type.
     */
    switch (blk_entry->class_type) {
        case BC_PHYSICAL:
            dev_data->blk_data_tbl.list[blk_num].class_num =
                    dev_data->phys_blk_count;
            dev_data->phys_blk_count ++;
            break;

        case BC_INPUT:
            dev_data->blk_data_tbl.list[blk_num].class_num =
                    dev_data->funct_blk_count;
            dev_data->funct_blk_count ++;
            break;

        case BC_OUTPUT:
            dev_data->blk_data_tbl.list[blk_num].class_num =
                    dev_data->funct_blk_count;
            dev_data->funct_blk_count ++;
            break;

        case BC_CONTROL:
            dev_data->blk_data_tbl.list[blk_num].class_num =
                    dev_data->funct_blk_count;
            dev_data->funct_blk_count ++;
            break;

        case BC_TRANSDUCER:
            dev_data->blk_data_tbl.list[blk_num].class_num =
                    dev_data->trans_blk_count;
            dev_data->trans_blk_count ++;
            break;

        default:
            return(FFDS_BLK_CLASS_ERR);
        }


    /*
     *  Convert the DD Block Name into an Item ID for the DD Block.
     */


    if(net_type == NT_A_SIM)
    {
        r_code = ds_blk_name_to_item_id(
                &dev_data->dd_device_id, SM_LATEST_DDREV, blk_entry->dd_blk_name,
                &dev_data->blk_data_tbl.list[blk_num].dd_blk_id);
        if (r_code) {
            dev_data->blk_data_tbl.list[blk_num].dd_blk_id = 0;
        }
    }

    /*
     *  Convert the Item ID into an DD Block Name for the DD Block.
     */

    else if(net_type == NT_A || net_type == NT_OFF_LINE)
    {
        r_code = ds_blk_item_id_to_name(
                &dev_data->dd_device_id, SM_LATEST_DDREV, blk_entry->dd_blk_name,
                &dev_data->blk_data_tbl.list[blk_num].dd_blk_id);
        if (r_code) 
        {
            if (CM_FILE_NOT_FOUND == r_code)
                r_code = ds_blk_item_id_to_name(
                        &dev_data->dd_device_id, SM_EXPLICIT_DDREV, blk_entry->dd_blk_name,
                        &dev_data->blk_data_tbl.list[blk_num].dd_blk_id);
                if (r_code) 
                    dev_data->blk_data_tbl.list[blk_num].dd_blk_id = 0;
        }
        if (CM_SUCCESS == r_code )
        {
            if (0 == blk_entry->dd_blk_name[0])
            {
                strncpy(blk_entry->dd_blk_name, blk_entry->blk_tag, BLK_TAG_MAX_LEN);
            }
            int entry_num;

            /*
             *  Go through each entry in the Block Entry Table.
             */

            for (entry_num = 0; entry_num < dev_data->blk_data_tbl.count; entry_num ++)
            {
                /*
                 *  Check if the new block entry's dd tag matches an existing
                 *  block entry's tag.
                 */
                if (strcmp(blk_entry->dd_blk_name,
                           dev_data->blk_data_tbl.list[entry_num].dd_tag) == 0)
                {
                    /*
                     *  Append counter in increment order to the device DD tag
                     *  for each duplicate occurence
                     */
                    char countstr[BLK_TAG_CNT_LEN];
                    memset(countstr, '\0', BLK_TAG_CNT_LEN);
                    sprintf(countstr, "_%d",dev_data->blk_data_tbl.count );
                    for (int index = 0; index < DD_BLK_NAME_LEN; index++)
                    {
                        if (blk_entry->dd_blk_name[index] == 0)
                        {
                            /*
                             *  Replace counter of 3 character length to device DD tag
                             *  starts from the 29th character position, so that total
                             *  block tag length will be 32
                             */
                            if (index > (BLK_TAG_MAX_LEN - BLK_TAG_CNT_LEN))
                                index = BLK_TAG_MAX_LEN - BLK_TAG_CNT_LEN;
                            for (int i = 0; i < BLK_TAG_CNT_LEN; i++)
                            {
                                blk_entry->dd_blk_name[index + i] = countstr[i];
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            (void)strcpy(dev_data->blk_data_tbl.list[blk_num].dd_tag,
                         blk_entry->dd_blk_name);
            (void)strcpy(dev_data->blk_data_tbl.list[blk_num].blk_tag,
                         blk_entry->blk_tag);
        }
    }
    else
    {
         LOGC_DEBUG(CPM_FF_DDSERVICE," Network Type Error\n");
        return (FFDS_COMM_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  new_device_data
 *
 *  ShortDesc:  Create a new element in a Device Data Table.
 *
 *  Description:
 *      The new_device_data function takes a block entry and Device
 *      Data Table and creates a new element in the table.
 *
 *  Inputs:
 *      blk_entry - the block entry whose data will be used.
 *      dev_data_tbl - the Device Data Table that will get a new
 *                     element.
 *
 *  Outputs:
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
new_device_data(BLK_ENTRY *blk_entry, DEV_DATA_TBL *dev_data_tbl)
{

    /*
     *  Increase the Device Data Table by one element.
     */

    dev_data_tbl->list = (DEV_DATA *) realloc(
            (char *)dev_data_tbl->list, (unsigned int)
            (sizeof(DEV_DATA) * (dev_data_tbl->count + 1)));
    if (!dev_data_tbl->list) {
        return(FFDS_MEMORY_ERR);
    }
    dev_data_tbl->count ++;

    /*
     *  Initialize the new Device Data Table element with data from
     *  the block entry.
     */

    dev_data_tbl->list[dev_data_tbl->count - 1].
            station_address = blk_entry->station_address;
    dev_data_tbl->list[dev_data_tbl->count - 1].
            dd_device_id.ddid_manufacturer =
            (UINT32)blk_entry->manufacturer;
    dev_data_tbl->list[dev_data_tbl->count - 1].
            dd_device_id.ddid_device_type =
            (UINT16)blk_entry->dev_type;
    dev_data_tbl->list[dev_data_tbl->count - 1].
            dd_device_id.ddid_device_rev =
            (UINT8)blk_entry->dev_rev;
    dev_data_tbl->list[dev_data_tbl->count - 1].
            dd_device_id.ddid_dd_rev =
            (UINT8)blk_entry->dd_rev;

    /*
     *  Initialize the Device's Block Class counts.
     */

    dev_data_tbl->list[dev_data_tbl->count - 1].phys_blk_count = 0;
    dev_data_tbl->list[dev_data_tbl->count - 1].funct_blk_count = 0;
    dev_data_tbl->list[dev_data_tbl->count - 1].trans_blk_count = 0;

    /*
     *  Initialize the Device's Block Data Table.
     */

    dev_data_tbl->list[dev_data_tbl->count - 1].
            blk_data_tbl.count = 0;
    dev_data_tbl->list[dev_data_tbl->count - 1].
            blk_data_tbl.list = (BLK_DATA *) malloc(1);
    if (!dev_data_tbl->list[dev_data_tbl->count -1].blk_data_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }
    return(FFDS_SUCCESS);
}



/***********************************************************************
 *
 *  Name:  store_device_data
 *
 *  ShortDesc:  Store device data from the block entry data.
 *
 *  Description:
 *      The store_device_data_function takes a block entry and Device
 *      Data Table and stores applicable information from the block
 *      entry into the Device Data Table.
 *
 *  Inputs:
 *      blk_entry - the block entry whose information is to be stored.
 *      dev_data_tbl - the Device Data Table that will hold the
 *                         block entry's information.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
store_device_data(BLK_ENTRY *blk_entry, DEV_DATA_TBL *dev_data_tbl, int net_type)
{

    int     dev_exists_flag = FALSE;
    int     dev_num;
    int     r_code;

    /*
     *  Go through each device in the Device Data Table.
     */

    for (dev_num = 0; dev_num < dev_data_tbl->count; dev_num ++ ) {

        if (blk_entry->station_address ==
                dev_data_tbl->list[dev_num].station_address) {
            dev_exists_flag = TRUE;
            break;
        }
    }

    /*
     *  Check if the device does not yet exist.
     */

    if (dev_exists_flag == FALSE) {

        /*
         *  Create a new device in the Device Data Table.
         */

        r_code = new_device_data(blk_entry, dev_data_tbl);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }
    }

    /*
     *  Add the block entry to the device data.
     */

    r_code = add_block_to_device_data(blk_entry,
            &dev_data_tbl->list[dev_num], net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  store_find_confirm
 *
 *  ShortDesc:  Store a block entry into the Find Confirm Table.
 *
 *  Description:
 *      The store_find_confirm function takes a block entry and Find
 *      Confirm Table and stores applicable information from the block
 *      entry into the Find Confirm Table.
 *
 *  Inputs:
 *      blk_entry - the block entry whose information is to be stored.
 *      find_confirm_tbl - the Find Confirm Table that will hold the
 *                         block entry's information.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
store_find_confirm(BLK_ENTRY *blk_entry, FIND_CNF_TBL *find_cnf_tbl,
		NET_TYPE net_type)
{

    /*
     *  Increase the Find Confirm Table by one element.
     */

    find_cnf_tbl->find_cnf_list = (FIND_CNF_A *) realloc(
            (char *)find_cnf_tbl->find_cnf_list, (unsigned int)
            (sizeof(FIND_CNF_A) * (find_cnf_tbl->count + 1)));
    if (!find_cnf_tbl->find_cnf_list) {
        return(FFDS_MEMORY_ERR);
    }
    find_cnf_tbl->blk_tag_list = (BLK_TAG *) realloc(
            (char *)find_cnf_tbl->blk_tag_list, (unsigned int)
            (sizeof(BLK_TAG) * (find_cnf_tbl->count + 1)));
    if (!find_cnf_tbl->blk_tag_list) {
        return(FFDS_MEMORY_ERR);
    }
    find_cnf_tbl->count ++;

    /*
     *  Store the block DD tag.
     */

    (void)strcpy(find_cnf_tbl->blk_tag_list[find_cnf_tbl->count - 1],
            blk_entry->dd_blk_name);

    /*
     *  Store the applicable information from the block entry into
     *  the Find Confirm.
     */

    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            op_index =
            (OBJECT_INDEX)blk_entry->op_index;
    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            station_address =
            (UINT16)blk_entry->station_address;
    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            dd_device_id.ddid_manufacturer =
            (UINT32)blk_entry->manufacturer;
    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            dd_device_id.ddid_device_type =
            (UINT16)blk_entry->dev_type;
    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            dd_device_id.ddid_device_rev =
            (UINT8)blk_entry->dev_rev;
    find_cnf_tbl->
            find_cnf_list[find_cnf_tbl->count - 1].
            dd_device_id.ddid_dd_rev=
            (UINT8)blk_entry->dd_rev;
    if(NT_OFF_LINE == net_type)
    {
    	find_cnf_tbl->
    		find_cnf_list[find_cnf_tbl->count - 1].
    		network_type = NT_OFF_LINE;
    }
    else
    {
    	find_cnf_tbl->
       		find_cnf_list[find_cnf_tbl->count - 1].
       		network_type = NT_A;
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  store_block_entry
 *
 *  ShortDesc:  Store a block Entry into the Block Entry Table.
 *
 *  Description:
 *      The store_block_entry function takes a block entry and Block
 *      Entry Table and stores the block entry into the Block Entry
 *      Table.
 *
 *  Inputs:
 *      blk_entry - the block entry to be stored.
 *      blk_entry_tbl - the Block Entry Table that will hold the
 *                      block entry.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
store_block_entry(BLK_ENTRY *blk_entry, BLK_ENTRY_TBL *blk_entry_tbl)
{

    /*
     *  Increase the Block Entry Table by one element.
     */

    blk_entry_tbl->list = (BLK_ENTRY **) realloc(
            (char *)blk_entry_tbl->list, (unsigned int)
            (sizeof(BLK_ENTRY *) * (blk_entry_tbl->count + 1)));
    if (!blk_entry_tbl->list) {
        return(FFDS_MEMORY_ERR);
    }
    blk_entry_tbl->count ++;

    /*
     *  Store the Block Entry data into the Block Entry table.
     */

    blk_entry_tbl->list[blk_entry_tbl->count - 1] = blk_entry;

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  validate_block_entry
 *
 *  ShortDesc:  Validate a block entry in the Device Simulator data
 *              file.
 *
 *  Description:
 *      The validate_block_entry function takes a block entry and Block
 *      Entry Table and performs one-to-many relationship validation.
 *
 *  Inputs:
 *      blk_entry - the block entry to be validated.
 *      blk_entry_tbl - the Block Entry Table to be validated against.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MULT_BLK_ERR.
 *      FFDS_MULT_DEV_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
validate_block_entry(BLK_ENTRY *blk_entry, BLK_ENTRY_TBL *blk_entry_tbl)
{

    int     entry_num;

    /*
     *  Go through each entry in the Block Entry Table.
     */

    for (entry_num = 0; entry_num < blk_entry_tbl->count; entry_num ++) {

        /* CPMHACK: Commented below condition
         * Instead of returning error, ignore the duplicate block tag
         * since all the operation is handled by using device dd tag,
         * in this case block tag is considered as block label
         */

//        /*
//         *  Check if the new block entry's tag matches an existing
//         *  block entry's tag.
//         */
//        if (strcmp(blk_entry->blk_tag,
//                blk_entry_tbl->list[entry_num]->blk_tag) == 0) {
//            return(FFDS_MULT_BLK_ERR);
//        }

        /*
         *  Check if the new block entry's device matches an existing
         *  block entry's device.
         */

        if (blk_entry->station_address ==
                blk_entry_tbl->list[entry_num]->station_address) {

            /*
             *  Check if the two matching devices have different
             *  device types.
             */

            if ((blk_entry->manufacturer !=
                    blk_entry_tbl->list[entry_num]->manufacturer) ||
                    (blk_entry->dev_type !=
                    blk_entry_tbl->list[entry_num]->dev_type) ||
                    (blk_entry->dev_rev !=
                    blk_entry_tbl->list[entry_num]->dev_rev)) {

                return(FFDS_MULT_DEV_ERR);
            }
        }
    }
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  scan_block_entry
 *
 *  ShortDesc:  Scan a block entry in the Device Simulator data file.
 *
 *  Description:
 *      The scan_block_entry function takes a file line and Block
 *      Entry Table and scans in and stores the block entry.
 *
 *  Inputs:
 *      file_line - the entry line of the data file.
 *      blk_entry_tbl - the block entry table of a specific simulated
 *                      Type A Network.
 *
 *  Outputs:
 *      blk_entry - the scanned in block entry.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_FILE_READ_ERR.
 *      Return values from validate_block_entry function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
scan_block_entry(USIGN8 node_address, char *file_line,
                 BLK_ENTRY *blk_entry,
                 BLK_ENTRY_TBL *blk_entry_tbl,
                 int net_type, SOFTING_BLOCK_INFO *softing_block)
{

    int     r_code;

    /*
     *  Scan in the following information for each entry:
     *
     *  1. tag of the block
     *  2. station address of the device
     *  3. DD device ID of the device type
     *      a. manufacturer
     *      b. device type
     *      c. device revision
     *  4. operational index of the block
     *  5. parameter count of the block
     *  6. name of the DD block
     *  7. class_type of the block
     */
    if(NT_A_SIM == net_type)
    {
        r_code = sscanf(file_line,
                "%s %d %d %d %c %d %d %s %d",
                blk_entry->blk_tag,
                &blk_entry->station_address,
                &blk_entry->manufacturer,
                &blk_entry->dev_type,
                &blk_entry->dev_rev,
                &blk_entry->op_index,
                &blk_entry->param_count,
                blk_entry->dd_blk_name,
                &blk_entry->class_type);

        if (r_code != 9) {
            return(FFDS_FILE_READ_ERR);
        }

        /*
         *  Validate the information of the entry.
         */

        r_code = validate_block_entry(blk_entry, blk_entry_tbl);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }
    }
    else if((NT_A == net_type) || (NT_OFF_LINE == net_type))
    {
        strncpy(blk_entry->blk_tag, (char *)softing_block->block_tag,
                                 BLK_TAG_LEN);
        blk_entry->station_address = node_address;
        blk_entry->manufacturer = (int)get_device_admin_manufacturer();
        blk_entry->dev_type = (int)get_device_admin_device_type();
        blk_entry->dev_rev = (unsigned char)get_device_admin_device_rev();
        blk_entry->dd_rev = (unsigned char)get_device_admin_dd_rev();
        blk_entry->op_index = softing_block->op_index;
        blk_entry->param_count = softing_block->no_params;
        blk_entry->dd_item_id = softing_block->DD_Item_ID;
        blk_entry->mode_block = softing_block->mode_block;

        if(softing_block->class_type == BC_UNKNOWN)
        {
            blk_entry->class_type = get_class_vlaue(softing_block->Profile) ;
        }
        else
        {
            blk_entry->class_type = softing_block->class_type;
            softing_block->class_type = -1;
        }
        /*
         *  Validate the information of the entry.
         */

        r_code = validate_block_entry(blk_entry, blk_entry_tbl);
        if (r_code != FFDS_SUCCESS) {
            return(r_code);
        }
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  cfg_file_id_to_network_num.
 *
 *  ShortDesc:  Convert a Configuration File ID to a network number.
 *
 *  Description:
 *      The cfg_file_id_to_network_num function takes a Configuration
 *      File ID and converts it to a corresponding network number.
 *
 *      Note:   The Type A Device/Network Simulator uses its own Network
 *              Table.  The Simulator's Network Table is local to the
 *              Simulator as it is a table of simulated networks only.
 *              This is a different table from the Network Table that
 *              is used in the "cm_comm.c" module.  The Network Table
 *              that is used in the "cm_comm.c" module is a global
 *              table of all networks in the system.
 *
 *              The Configuration File ID is used to identify a
 *              particular simulated network (entry in the Simulator's
 *              Network Table).
 *
 *              The network number is an offset into the Simulator's
 *              Network Table.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network.
 *
 *  Outputs:
 *      netwk_num - the number of the network.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_CFG_FILE_ID_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
cfg_file_id_to_network_num(int config_file_id, int *netwk_num)
{

    int     temp_netwk_num;

    /*
     *  Go through each network in the (simulated) Network Table.
     */

    for (temp_netwk_num = 0; temp_netwk_num < netwk_tbl.count;
            temp_netwk_num ++) {

        /*
         *  Check if the Configuration File ID matches.
         */

        if (netwk_tbl.list[temp_netwk_num].config_file_id ==
                config_file_id) {

            *netwk_num = temp_netwk_num;
            return(FFDS_SUCCESS);
        }
    }

    return(FFDS_CFG_FILE_ID_ERR);
}


/***********************************************************************
 *
 *  Name:  add_block_entry
 *
 *  ShortDesc:  add a block entry in the Device data file.
 *
 *  Description:
 *      The add_block_entry function takes a Block
 *      Entry Table and adds in and stores the block entry.
 *
 *  Inputs:
 *      block_info - the block header data.
 *      blk_entry_tbl - the block entry table of a specific
 *                      Type A Network.
 *      blk_entry - the block entry data
 *      dev_data_tbl - device data table to be stored with
 *                      block entry information
 *  Outputs:
 *      none.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from scan_block_entry function.
 *      Return values from store_block_entry function.
 *      Return values from store_find_confirm function.
 *      Return values from store_device_data function.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/


int add_block_entry(SOFTING_BLOCK_INFO *block_info,
                           BLK_ENTRY *blk_entry,
                           BLK_ENTRY_TBL *blk_entry_tbl,
                           FIND_CNF_TBL *find_cnf_tbl,
                           DEV_DATA_TBL *dev_data_tbl,
                           NETWORK_HANDLE nh
)
{
    USIGN8           device_addr;
    int              r_code;
    char             file_line[256];
    NET_TYPE         net_type;

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != FFDS_SUCCESS) {
        return(r_code) ;
    }

    r_code = get_nt_device_addr(nh, &device_addr);
    if (r_code != FFDS_SUCCESS) {
        return(r_code) ;
    }


    r_code = scan_block_entry(device_addr,
                    file_line,
                    blk_entry,
                    blk_entry_tbl,
                    net_type,
                    block_info);
    if (r_code != FFDS_SUCCESS) {
        free(blk_entry);
        return(r_code);
    }

    /*
     *  Store the block entry data into the Block Entry Table.
     */

    r_code = store_block_entry(blk_entry, blk_entry_tbl);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }


    /*
     *  Store the block entry data into the Device Data Table.
     */

    r_code = store_device_data(blk_entry, dev_data_tbl, net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    /*
     *  Store the block entry data into the Find Confirm Table.
     */

    r_code = store_find_confirm(blk_entry, find_cnf_tbl, net_type);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  del_device_entry
 *
 *  ShortDesc:  cleanup the memory allocated for device
 *
 *  Description:
 *      The del_device_entry function takes a network handle and
 *      clears the memory for the deivce in the active device list
 *
 *  Inputs:
 *      NETWORK_HANDLE - The network handle of the device
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      CM_RESPONSE_CODE
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
del_device_entry(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    BLK_ENTRY_TBL   *blk_entry_tbl;
    FIND_CNF_TBL    *find_cnf_tbl;
    DEV_DATA_TBL    *dev_data_tbl;
    DEV_DATA        *dev_data;
    int              r_code, i;
    USIGN8          node_address;
    FBAP_NMA_REQ    fbap_nma_req;
    UINT16          cr;
    BLK_ENTRY       *blk_entry;
    NET_TYPE         net_type;

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != FFDS_SUCCESS) {
        return(r_code) ;
    }


    blk_entry_tbl = &netwk_tbl.list[nh].blk_entry_tbl;
    find_cnf_tbl = &netwk_tbl.list[nh].find_cnf_tbl;
    dev_data_tbl = &netwk_tbl.list[nh].dev_data_tbl;

    r_code = get_nt_device_addr(nh, &node_address);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    /*
     *  Free the table list created for the
     *  connected device
     */

    for(i = 0; i < (blk_entry_tbl->count); i++)
    {
        blk_entry = blk_entry_tbl->list[i];
        free(blk_entry);
    }

    free(blk_entry_tbl->list);
    free(find_cnf_tbl->blk_tag_list);
    free(find_cnf_tbl->find_cnf_list);

    for(i = 0; i < dev_data_tbl->count; i++)
    {
        dev_data = &dev_data_tbl->list[i];
        free(dev_data->blk_data_tbl.list);
    }
    free(dev_data_tbl->list);
    dev_data_tbl->count = 0;    

    //free the network table<to void memory leak>
    free(netwk_tbl.list);
    netwk_tbl.list = NULL;

    if(NT_A == net_type)
    {
		r_code = get_nt_net_mib_cr(nh, &cr);
		if (r_code != SUCCESS) {
			return(r_code) ;
		}

		/*
		 * Abort the MIB CR
		 */

		fbap_nma_req.cr_index = cr;
		set_device_admin_state(DEVACC_APPL_STATE_ABORT);
        set_device_admin_conn(FB_FALSE);
		r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ, &fbap_nma_req,
				NULL);

		if(CM_SUCCESS != r_code) {
            if (CM_IF_DLL_ABT_RC7 != r_code) //Just Ignore this cr time out error
                return(r_code) ;
		}

		r_code = get_nt_net_fbap_cr(nh, &cr);
		if (r_code != CM_SUCCESS) {
            if (CM_IF_DLL_ABT_RC7 != r_code) //Just Ignore this cr time out error
                return(r_code) ;
		}

		/*
		 * Abort the FBAP CR
		 */

		fbap_nma_req.cr_index = cr;
		r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ,
				&fbap_nma_req, NULL);
		if(CM_SUCCESS != r_code) {
            if (CM_IF_DLL_ABT_RC7 != r_code) //Just Ignore this cr time out error
                return(r_code) ;

		}
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  read_data_device
 *
 *  ShortDesc:  Read the Device
 *
 *  Description:
 *      The read_data_device function takes a ENV_INFO and NETWORK_HANDLE and
 *      reads all the blocks information from the device and builds the
 *      block table for future reference.
 *
 *  Inputs:
 *      nh - NETWORK_HANDLE of Type A Network.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_FILE_OPEN_ERR.
 *      Return values from cfg_file_id_to_netwk_num function.
 *      Return values from scan_block_entry function.
 *      Return values from store_block_entry function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
read_data_device(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    BLK_ENTRY       *blk_entry;
    BLK_ENTRY_TBL   *blk_entry_tbl;
    FIND_CNF_TBL    *find_cnf_tbl;
    DEV_DATA_TBL    *dev_data_tbl;
    int             r_code;
    USIGN8          node_address;
    NET_TYPE        net_type;
    int i;
    SOFTING_BLOCK_INFO *block;
    FBAP_NMA_REQ    fbap_nma_req;
    UINT16          cr;
    UINT16          n_entries;
    APP_INFO        *app_info = (APP_INFO *)env_info->app_info;
    unsigned char   mode_block;

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code =  get_nt_net_fbap_cr(nh, &cr) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    /*
     *  Obtain the network's tables.
     */

    blk_entry_tbl = &netwk_tbl.list[nh].blk_entry_tbl;
    find_cnf_tbl = &netwk_tbl.list[nh].find_cnf_tbl;
    dev_data_tbl = &netwk_tbl.list[nh].dev_data_tbl;

    r_code = get_nt_device_addr(nh, &node_address);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    /*
     *  Read the Resource block
     */

    fbap_nma_req.index = get_device_admin_rb_index();
    fbap_nma_req.cr_index = cr;
    r_code = read_mode_block(env_info,&mode_block, &fbap_nma_req);
    if(r_code != CM_SUCCESS)
    {
        return r_code;
    }
    bit_rev(&mode_block, 1);

    fbap_nma_req.index = get_device_admin_rb_index();
    fbap_nma_req.sub_index = 0;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
            &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }

    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
	    log_error(error);
        app_info->env_returns.response_code = error->class_code;

        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
        if (!blk_entry) {
            free(blk_entry);
            return(FFDS_MEMORY_ERR);
        }
        block = (SOFTING_BLOCK_INFO *)global_res_buf;
        block = convert_ltl_endian(block);
        block->op_index = get_device_admin_rb_index();
        block->class_type = BC_PHYSICAL;
        block->mode_block = mode_block;

        // Fill in blk_entry from softing_block
        add_block_entry(block, blk_entry, blk_entry_tbl,
                    find_cnf_tbl, dev_data_tbl, nh);
        block->class_type = -1;

    }

    /*
     *  Read the Transducer blocks
     */
    n_entries = get_device_admin_tb_entries();

    for (i = 0; i < n_entries; i++)
    {
        fbap_nma_req.index = get_device_admin_tb_index(i);
        fbap_nma_req.cr_index = cr;
        r_code = read_mode_block(env_info, &mode_block, &fbap_nma_req);
        if(r_code != CM_SUCCESS)
        {
            return r_code;
        }
        bit_rev(&mode_block, 1);

        fbap_nma_req.index = get_device_admin_tb_index(i);
        fbap_nma_req.sub_index = 0;
        fbap_nma_req.cr_index = cr;

        r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
                &fbap_nma_req, NULL);
        if(CM_SUCCESS != r_code) {
        	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
        			NULL);
        	if(CM_SUCCESS != r_code)
        		return r_code;
        }
        if(NEG == get_service_result())
        {
            T_ERROR *error = (T_ERROR *)global_res_buf;
            log_error(error);
            app_info->env_returns.response_code = error->class_code;

            return CS_FMS_ERR(error->class_code);
        }
        else if(POS == get_service_result())
        {
            blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
            if (!blk_entry) {
                free(blk_entry);
                return(FFDS_MEMORY_ERR);
            }
            block = (SOFTING_BLOCK_INFO *)global_res_buf;
            block = convert_ltl_endian(block);
            block->op_index = get_device_admin_tb_index(i);
            block->class_type = BC_TRANSDUCER;
            block->mode_block = mode_block;
            /*
             *  Add transducer block data into device table
             */
            add_block_entry(block, blk_entry, blk_entry_tbl,
                        find_cnf_tbl, dev_data_tbl, nh);
            block->class_type = -1;

        }
    }

    /*
     *  Read the Function blocks
     */
    n_entries = get_device_admin_fb_entries();
    for (i = 0; i < n_entries; i++)
    {
        fbap_nma_req.index = get_device_admin_fb_index(i);
        fbap_nma_req.cr_index = cr;
        r_code = read_mode_block(env_info, &mode_block, &fbap_nma_req);
        if(r_code != CM_SUCCESS)
        {
            return r_code;
        }
        bit_rev(&mode_block, 1);

        fbap_nma_req.index = get_device_admin_fb_index(i);
        fbap_nma_req.sub_index = 0;
        fbap_nma_req.cr_index = cr;

        r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
                &fbap_nma_req, NULL);
        if(CM_SUCCESS != r_code) {
        	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
        			NULL);
        	if(CM_SUCCESS != r_code)
        		return r_code;
        }
        if(NEG == get_service_result())
        {
            T_ERROR *error = (T_ERROR *)global_res_buf;
            log_error(error);
            app_info->env_returns.response_code = error->class_code;
            return CS_FMS_ERR(error->class_code);
        }
        else if(POS == get_service_result())
        {
            blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
            if (!blk_entry) {
                free(blk_entry);
                return(FFDS_MEMORY_ERR);
            }
            block = (SOFTING_BLOCK_INFO *)global_res_buf;
            block = convert_ltl_endian(block);
            block->op_index = get_device_admin_fb_index(i);
            block->class_type = BC_UNKNOWN;
            block->mode_block = mode_block;

            /*
             *  Add function block data into device table
             */
            add_block_entry(block, blk_entry, blk_entry_tbl,
                        find_cnf_tbl, dev_data_tbl, nh);
            block->class_type = -1;


        }
    }
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  initialize_tables
 *
 *  ShortDesc:  Initialize the tables used in a network of simulated
 *              Type A devices.
 *
 *  Description:
 *      The initialize_tables function takes a network number and
 *      initializes all of the tables used in the network of
 *      simulated devices.  Note that the network number is
 *      different from the network ID.  The network number is
 *      a handle into the Simulated Network Table.  The network
 *      ID is a handle into the Connection Manager's Network
 *      Table.
 *
 *  Inputs:
 *      netwk_num - the number of the simulated Type A Network.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERROR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
initialize_tables(int netwk_num)
{

    NETWK       *netwk;

    netwk = &netwk_tbl.list[netwk_num];

    /*
     *  Initialize the Block Entry Table.
     */

    netwk->blk_entry_tbl.count = 0;
    netwk->blk_entry_tbl.list = (BLK_ENTRY **) malloc(1);
    if (!netwk->blk_entry_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }

    /*
     *  Initialize the Find Confirm Table.
     */

    netwk->find_cnf_tbl.count = 0;
    netwk->find_cnf_tbl.blk_tag_list = (BLK_TAG *) malloc(1);
    if (!netwk->find_cnf_tbl.blk_tag_list) {
        return(FFDS_MEMORY_ERR);
    }
    netwk->find_cnf_tbl.find_cnf_list = (FIND_CNF_A *) malloc(1);
    if (!netwk->find_cnf_tbl.find_cnf_list) {
        return(FFDS_MEMORY_ERR);
    }

    /*
     *  Initialize the Device Data Table.
     */

    netwk->dev_data_tbl.count = 0;
    netwk->dev_data_tbl.list = (DEV_DATA *) malloc(1);
    if (!netwk->dev_data_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  new_network
 *
 *  ShortDesc:  Create a new network for simulated Type A devices.
 *
 *  Description:
 *      The new_network function takes a Configuratin File ID and
 *      creates a new network for simulated Type A devices.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be created.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
new_network(int config_file_id)
{

    /*
     *  Increase the (simulated) Network Table by one element.
     */

    netwk_tbl.list = (NETWK *) realloc(
            (char *)netwk_tbl.list, (unsigned int)
            (sizeof(NETWK) * (netwk_tbl.count + 1)));
    if (!netwk_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }
    netwk_tbl.count ++;

    /*
     *  Initialize the new (simulated) Network Table element with
     *  the Configuration File ID.
     */

    netwk_tbl.list[netwk_tbl.count - 1].config_file_id = config_file_id;

    return(FFDS_SUCCESS);
}

/*
 * Conversion to intel format/ little endian format
 */

SOFTING_BLOCK_INFO * convert_ltl_endian(SOFTING_BLOCK_INFO *softing_block)
{
    unsigned int num32;
    unsigned short num16;
    // move byte 3 to byte 0
    softing_block->DD_Item_ID = ((softing_block->DD_Item_ID>>24)&0xff) |
                    // move byte 1 to byte 2
                    ((softing_block->DD_Item_ID<<8)&0xff0000) |
                    // move byte 2 to byte 1
                    ((softing_block->DD_Item_ID>>8)&0xff00) |
                    // byte 0 to byte 3
                    ((softing_block->DD_Item_ID<<24)&0xff000000);

    num32 = softing_block->DD_member_ID;
    softing_block->DD_member_ID = ((num32>>24)&0xff) |// move byte 3 to byte 0
                    ((num32<<8)&0xff0000) | // move byte 1 to byte 2
                    ((num32>>8)&0xff00) | // move byte 2 to byte 1
                    ((num32<<24)&0xff000000); // byte 0 to byte 3


    num16 = softing_block->DD_Revision;
    softing_block->DD_Revision = (num16>>8) | (num16<<8);

    num16 = softing_block->Profile;
    softing_block->Profile = (num16>>8) | (num16<<8);

    num16 = softing_block->Profile_revision;
    softing_block->Profile_revision = (num16>>8) | (num16<<8);

    num32 = softing_block->Execution_time;
    softing_block->Execution_time = ((num32>>24)&0xff) |
                    // move byte 3 to byte 0
                    ((num32<<8)&0xff0000) | // move byte 1 to byte 2
                    ((num32>>8)&0xff00) | // move byte 2 to byte 1
                    ((num32<<24)&0xff000000); // byte 0 to byte 3

    num32 = softing_block->Period_exec;
    softing_block->Period_exec = ((num32>>24)&0xff) | // move byte 3 to byte 0
                    ((num32<<8)&0xff0000) | // move byte 1 to byte 2
                    ((num32>>8)&0xff00) | // move byte 2 to byte 1
                    ((num32<<24)&0xff000000); // byte 0 to byte 3

    num16 = softing_block->no_params;
    softing_block->no_params = (num16>>8) | (num16<<8);

    num16 = softing_block->next_fb_to_exec;
    softing_block->next_fb_to_exec = (num16>>8) | (num16<<8);

    num16 = softing_block->starting_index_views;
    softing_block->starting_index_views = (num16>>8) | (num16<<8);

    return softing_block;
}

/***********************************************************************
 *
 *  Name: softing_detach_device
 *
 *  ShortDesc: Detach the device from the live list.
 *
 *  Description:
 *      The softing_detach_device function deletes the memory for the
 *      device in the live list.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from read_data_device funciton.
 *      Return values from build_devices function.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int softing_detach_device(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int         r_code;
    DEV_TBL     dev_table;

    r_code = del_device_entry(env_info, nh);
    if(r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    r_code =  get_nt_dev_tbl (nh, &dev_table);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    dev_table.phys_blk_count = 0;
    dev_table.funct_blk_count = 0;
    dev_table.trans_blk_count = 0;

    dev_table.blk_data_tbl.count = 0;

    r_code =  set_nt_dev_tbl(nh, &dev_table) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    return(FFDS_SUCCESS);
}


static int
build_init_network(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int     r_code;

    APP_INFO *app_info = (APP_INFO *)env_info->app_info;
    /*
     *  Create a new network.
     */

    /*
     *  Initialize the Network Table (for simulated Type A devices).
     */

    netwk_tbl.count = 0;
    netwk_tbl.list = (NETWK *) malloc(1);
    if (!netwk_tbl.list) {
        app_info->env_returns.response_code = FFDS_MEMORY_ERR;
        return(FFDS_MEMORY_ERR);
    }

    r_code = new_network(nh);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    /*
     *  Initialize all of the tables used in the network.
     */

    r_code = initialize_tables(netwk_tbl.count - 1);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: validate_device_entry
 *
 *  ShortDesc:  Validates the multiple initialization of same device
 *              from the list of connected device of Type A devices.
 *
 *  Description:
 *      The validate_device_entry function takes a network handle
 *      and validates whter the device count has been initialized
 *
 *  Inputs:
 *      nh - the network handle of the device to be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MULT_DEV_INIT_ERR
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int validate_device_entry(ENV_INFO *env_info, NT_COUNT nt_count)
{
    int count;

    for(count = 0; count < nt_count; count++)
    {
        if(netwk_tbl.list[count].dev_data_tbl.count > 0)
        {
            return(FFDS_MULT_DEV_INIT_ERR);
        }
    }
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: softing_device_table
 *
 *  ShortDesc:  After Scanning, this function build the device table
 *              of list of connected device of Type A devices.
 *
 *  Description:
 *      The softing_device_table function takes a Configuration File
 *      ID and scans the list of connected device list.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from read_data_device funciton.
 *      Return values from build_devices function.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int softing_device_table(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int     r_code;
    NT_COUNT     nt_count;
    DEV_TBL     dev_table;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = build_init_network(env_info, nh);
    if(CM_SUCCESS != r_code)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initilaize new network ");
    	return r_code;
    }

    r_code = validate_device_entry(env_info, nt_count);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    r_code = read_data_device(env_info, nh);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Build the network of simulated Type A devices.
     */

    r_code = build_devices(nh);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    dev_table.station_address =
                netwk_tbl.list[nh].dev_data_tbl.list->station_address;
    dev_table.phys_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->phys_blk_count;
    dev_table.funct_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->funct_blk_count;
    dev_table.trans_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->trans_blk_count;
    dev_table.blk_data_tbl.count =
                netwk_tbl.list[nh].dev_data_tbl.list->blk_data_tbl.count;
    dev_table.blk_data_tbl.list =
    (SCAN_BLK_DATA *) netwk_tbl.list[nh].dev_data_tbl.list->blk_data_tbl.list;

    dev_table.dd_device_id.ddid_dd_rev =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_dd_rev;
    dev_table.dd_device_id.ddid_device_rev =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_device_rev;
    dev_table.dd_device_id.ddid_device_type =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_device_type;
    dev_table.dd_device_id.ddid_manufacturer =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_manufacturer;

    r_code =  set_nt_dev_tbl(nh, &dev_table) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    return(FFDS_SUCCESS);

}

/***********************************************************************
 *
 *  Name: softing_init_network
 *
 *  ShortDesc:  Initialize a single network of simulated Type A devices.
 *
 *  Description:
 *      The softing_init_network function takes a Configuration File ID and
 *      initializes a single network of Type A devices.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from new_network function.
 *      Return values from initialize_tables funciton.
 *      Return values from read_data_file funciton.
 *      Return values from build_devices function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
softing_init_network(int config_file_id)
{

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: softing_stack_init
 *
 *  ShortDesc:  Initialize the Softing Stack.
 *
 *  Description:
 *      The softing_stack_init function initializes the Softing Stack
 *
 *  Inputs:
 *      None.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Reference:
 *      Softing stack interface Demo code
 *  Author:
 *      Jayanth
 *
 **********************************************************************/
int
softing_stack_init(ENV_INFO *env_info, HOST_CONFIG *host_config)
{
    int r_code = CM_SUCCESS;
    char *node_addr_str;
    unsigned char host_address = host_config->host_addr;

    /* Get Node address from Environmental Variable */
    if(host_address <= 0)
    {
        node_addr_str = getenv(WALLACE_FBK2_NODE);
        if(NULL == node_addr_str)
        {
            LOGC_DEBUG(CPM_FF_DDSERVICE,"No Environment variable WALLACE_FBK2_NODE found\n");
            host_address = 252;
            LOGC_TRACE(CPM_FF_DDSERVICE,"Using default Host Node Address %d\n", host_address);
        }
        else
        {
            host_address = atoi(node_addr_str);
        }
    }

    set_device_admin_state(DEVACC_APPL_STATE_START);
    set_device_admin_conn(FB_FALSE);
    set_device_admin_local_nma_read(0);
    set_device_admin_local_nma_write(0);
    set_device_admin_invokeid(0);
    set_device_admin_proc_con(NULL);
    clear_device_admin_live_list();
    set_device_admin_channel(0);
    set_device_admin_host_type(host_config->host_type);

    mgmt_service_loc = FB_FALSE;

    set_device_admin_host_addr(host_address);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Host is using node address 0x%x\n\n",
        host_config->host_addr);

    switch(host_config->host_type)
    {
    	case HOST_STANDALONE:
    	case HOST_SERVER:

    		r_code = initialize_host(host_config);

    		if(CM_SUCCESS != r_code)
    		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to configure host");
    			return (r_code);
    		}

    		break;

    	case HOST_CLIENT:

    		/*
    		 * Do nothing
    		 */
    		r_code = CM_SUCCESS;
    		break;

    	default:
    		r_code = CM_COMM_ERR;
                LOGC_ERROR(CPM_FF_DDSERVICE,"start_stack_init: Host Type not defined");
    		break;
    }

    if(CM_SUCCESS == r_code)
    {
    	r_code = init_comm_thread (host_config->host_type);
    	if(r_code != CM_SUCCESS) {
    		return r_code;
    	}
    }

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: sim_init
 *
 *  ShortDesc:  Initialize the Type A Network/Device simulator.
 *
 *  Description:
 *      The sim_init function initializes the Type A Network/Device
 *      simulator.
 *
 *  Inputs:
 *      None.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/
int
sim_init(void)
{

    /*
     *  Initialize the Network Table (for simulated Type A devices).
     */

    netwk_tbl.count = 0;
    netwk_tbl.list = (NETWK *) malloc(1);
    if (!netwk_tbl.list) {
        return(FFDS_MEMORY_ERR);
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  softing_initiate_comm
 *
 *  ShortDesc:  Initiate communication with a real Type A device.
 *
 *  Description:
 *      The softing_initiate_comm function takes a Configuration File ID
 *      and station address of a device and initiates communication by
 *      establishing the communication references (CREFs) for the
 *      device's VFDs (OPOD, DDOD, MIB).
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network.
 *      station_address - the station address of the device.
 *
 *  Outputs:
 *      opod_cref - the communication reference for the device's OPOD.
 *      ddod_cref - the communication reference for the device's DDOD.
 *      mib_cref - the communicaiton reference for the device's MIB.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_COMM_ERR.
 *      Return values from cfg_file_id_to_network_num function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/
int
softing_initiate_comm(NETWORK_HANDLE nh, int station_address, CREF *opod_cref,
    CREF *ddod_cref, CREF *mib_cref)
{

    USIGN16         cr;
    int             r_code;

    r_code = get_nt_net_fbap_cr(nh, &cr);
    if(r_code != CM_SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting FBAP CR value\n");
        return r_code;
    }

    *opod_cref = cr;

    /*
     *  The DDOD is not supported yet.
     */

    *ddod_cref = -1;

    r_code = get_nt_net_mib_cr(nh, &cr);
    if(r_code != CM_SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting FBAP CR value\n");
        return r_code;
    }

    *mib_cref = cr;

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  sim_initiate_comm
 *
 *  ShortDesc:  Initiate communication with a simulated Type A device.
 *
 *  Description:
 *      The sim_initiate_comm function takes a Configuration File ID
 *      and station address of a device and initiates communication by
 *      establishing the communication references (CREFs) for the
 *      device's VFDs (OPOD, DDOD, MIB).
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network.
 *      station_address - the station address of the device.
 *
 *  Outputs:
 *      opod_cref - the communication reference for the device's OPOD.
 *      ddod_cref - the communication reference for the device's DDOD.
 *      mib_cref - the communicaiton reference for the device's MIB.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_COMM_ERR.
 *      Return values from cfg_file_id_to_network_num function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/
int
sim_initiate_comm(int config_file_id, int station_address, CREF *opod_cref,
    CREF *ddod_cref, CREF *mib_cref)
{

    int              netwk_num;
    int              dev_num;
    DEV_DATA_TBL    *dev_data_tbl;
    int              r_code;

    /*
     *  Get the network number of the device, and the corresponding
     *  Device Data Table.
     */

    r_code = cfg_file_id_to_network_num(config_file_id, &netwk_num);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }
    dev_data_tbl = &netwk_tbl.list[netwk_num].dev_data_tbl;

    /*
     *  Go through each device in the network's Device Data Table.
     */

    for (dev_num = 0; dev_num < dev_data_tbl->count; dev_num ++) {

        /*
         *  Check if the station address matches.
         */

        if (dev_data_tbl->list[dev_num].station_address ==
                station_address) {

            /*
             *  Set the device's OPOD CREF to the simulated FF
             *  device's opod handle.
             */

            *opod_cref = dev_data_tbl->list[dev_num].opod_handle;

            /*
             *  The DDOD and MIB is not supported yet.
             */

            *ddod_cref = -1;
            *mib_cref = -1;

            return(FFDS_SUCCESS);
        }
    }

    return(FFDS_COMM_ERR);
}

/***********************************************************************
 *
 *  Name:  softing_object_copy
 *
 *  ShortDesc:  Copy an object into system memory.
 *
 *  Description:
 *      The sim_object_copy function takes an object (allocated
 *      anywhere) and makes a copy of it (along with any of its
 *      allocated components) in system memory.
 *
 *  Inputs:
 *      destination_object - the copy of the object (in system memory).
 *      source_object - the object to be copied.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_OBJECT_ERR.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/
int
softing_object_copy(OBJECT **destination_object, OBJECT *source_object)
{

    OBJECT  *temp_object;

    /*
     *  Check the object, and allocate and copy if necessary.
     */

    if (!source_object) {
        return(FFDS_OBJECT_ERR);
    }

    temp_object = (OBJECT *) calloc(1, sizeof(OBJECT));
    if (!temp_object) {
        return(FFDS_MEMORY_ERR);
    }
    (void) memcpy((char *)temp_object, (char *)source_object, sizeof(OBJECT));

    /*
     *  Null the value pointer.
     */

    temp_object->value = 0;
    temp_object->name = NULL;
    temp_object->extension = NULL;
    temp_object->specific = NULL;

    /*
     *  Check the name, and allocate and copy if necessary.
     */

    if (source_object->name) {
        temp_object->name = (char *) calloc(1, ROD_OBJECT_NAME_LENGTH);
        if (!temp_object->name) {
            goto error_cleanup;
        }
        (void)strcpy(temp_object->name, source_object->name);
    }

    /*
     *  Check the extension, and allocate and copy if necessary.
     */

    if (source_object->extension) {
        temp_object->extension = (UINT8 *) calloc(1,
                (unsigned int)source_object->extension[0]);
        if (!temp_object->extension) {
            goto error_cleanup;
        }
        (void) memcpy((char *)temp_object->extension,
                (char *)source_object->extension,
                (int)source_object->extension[0]);
    }

    /*
     *  Check the specific information, and allocate and copy if
     *  necessary.
     */

    if (source_object->specific) {

        temp_object->specific = (OBJECT_SPECIFIC *) calloc(1,
                sizeof(OBJECT_SPECIFIC));
        if (!temp_object->specific) {
            goto error_cleanup;
        }
        (void) memcpy((char *)temp_object->specific,
                (char *)source_object->specific,
                sizeof(OBJECT_SPECIFIC));

        /*
         *  Check the type of the object and take appropriate action.
         */

        switch (source_object->code) {

            case OC_PROGRAM_INVOCATION:

                /*
                 *  For a program invocation object, the domain list
                 *  must be checked and copied if necessary.
                 */

                if (source_object->specific->program_invocation.domain_list) {
                    temp_object->specific->program_invocation.domain_list =
                        (OBJECT_INDEX *) calloc(
                        (unsigned)source_object->specific->
                        program_invocation.domain_count,
                        sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->program_invocation.domain_list)
                    {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                        (char *)temp_object->specific->program_invocation.domain_list,
                        (char *)source_object->specific->program_invocation.domain_list,
                        (int)(source_object->specific->program_invocation.domain_count *
                        sizeof(OBJECT_INDEX)));
                }
                break;

            case OC_DATA_TYPE:

                /*
                 *  For a data type object, the symbol must be
                 *  checked and copied if necessary.
                 */

                if (source_object->specific->data_type.symbol) {
                    temp_object->specific->data_type.symbol =
                            (char *) calloc(
                            (unsigned)source_object->specific->
                            data_type.symbol_size,
                            sizeof(char));
                    if (!temp_object->specific->
                            data_type.symbol) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                        (char *)temp_object->specific->data_type.symbol,
                        (char *)source_object->specific->data_type.symbol,
                        (int)(source_object->specific->data_type.symbol_size *
                        sizeof(char)));
                }
                break;

            case OC_DATA_TYPE_STRUCTURE:

                /*
                 *  For a data type structure object, the type list and
                 *  size list must be checked and copied if necessary.
                 */

                if (source_object->specific->data_type_structure.type_list) {
                    temp_object->specific->data_type_structure.type_list =
                            (OBJECT_INDEX *) calloc(
                            (unsigned)source_object->specific->
                            data_type_structure.elem_count,
                            sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->
                            data_type_structure.type_list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->data_type_structure.type_list,
                            (char *)source_object->specific->data_type_structure.type_list,
                            (int)(source_object->specific->data_type_structure.elem_count *
                            sizeof(OBJECT_INDEX)));
                }

                if (source_object->specific->data_type_structure.size_list) {
                    temp_object->specific->data_type_structure.size_list =
                            (UINT8 *) calloc(
                            (unsigned)source_object->specific->
                            data_type_structure.elem_count,
                            sizeof(UINT8));
                    if (!temp_object->specific->data_type_structure.size_list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->data_type_structure.size_list,
                            (char *)source_object->specific->data_type_structure.size_list,
                            (int)(source_object->specific->data_type_structure.elem_count *
                            sizeof(UINT8)));
                }
                break;

            case OC_VARIABLE_LIST:

                /*
                 *  For a variable list object, the list must be
                 *  checked and copied if necessary.
                 */

                if (source_object->specific->variable_list.list) {
                    temp_object->specific->variable_list.list =
                            (OBJECT_INDEX *) calloc(
                            (unsigned)source_object->specific->
                            variable_list.elem_count,
                            sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->variable_list.list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->variable_list.list,
                            (char *)source_object->specific->variable_list.list,
                            (int)(source_object->specific->variable_list.elem_count *
                            sizeof(OBJECT_INDEX)));
                }
                break;

            default:
                break;
        }
    }

    *destination_object = temp_object;
    return(FFDS_SUCCESS);

error_cleanup:
    if (temp_object->name)
    {
        free(temp_object->name);
    }
    if (temp_object->extension)
    {
        free(temp_object->extension);
    }
    if (temp_object->specific)
    {
        free(temp_object->specific);
    }
    free(temp_object);

    return FFDS_MEMORY_ERR;
}

/***********************************************************************
 *
 *  Name:  sim_object_copy
 *
 *  ShortDesc:  Copy an object into system memory.
 *
 *  Description:
 *      The sim_object_copy function takes an object (allocated
 *      anywhere) and makes a copy of it (along with any of its
 *      allocated components) in system memory.
 *
 *  Inputs:
 *      destination_object - the copy of the object (in system memory).
 *      source_object - the object to be copied.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_OBJECT_ERR.
 *      FFDS_MEMORY_ERR.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/
static int
sim_object_copy(OBJECT **destination_object, OBJECT *source_object)
{

    OBJECT  *temp_object;

    /*
     *  Check the object, and allocate and copy if necessary.
     */

    if (!source_object) {
        return(FFDS_OBJECT_ERR);
    }

    temp_object = (OBJECT *) calloc(1, sizeof(OBJECT));
    if (!temp_object) {
        return(FFDS_MEMORY_ERR);
    }
    (void) memcpy((char *)temp_object, (char *)source_object, sizeof(OBJECT));

    /*
     *  Null the value pointer.
     */

    temp_object->value = 0;
    temp_object->name = NULL;
    temp_object->extension = NULL;
    temp_object->specific = NULL;

    /*
     *  Check the name, and allocate and copy if necessary.
     */

    if (source_object->name) {
        temp_object->name = (char *) calloc(1, ROD_OBJECT_NAME_LENGTH);
        if (!temp_object->name) {
            goto error_cleanup;
        }
        (void)strcpy(temp_object->name, source_object->name);
    }

    /*
     *  Check the extension, and allocate and copy if necessary.
     */

    if (source_object->extension) {
        temp_object->extension = (UINT8 *) calloc(1,
                (unsigned int)source_object->extension[0]);
        if (!temp_object->extension) {
            goto error_cleanup;
        }
        (void) memcpy((char *)temp_object->extension,
                (char *)source_object->extension,
                (int)source_object->extension[0]);
    }

    /*
     *  Check the specific information, and allocate and copy if
     *  necessary.
     */

    if (source_object->specific) {

        temp_object->specific = (OBJECT_SPECIFIC *) calloc(1,
                sizeof(OBJECT_SPECIFIC));
        if (!temp_object->specific) {
            goto error_cleanup;
        }
        (void) memcpy((char *)temp_object->specific,
                (char *)source_object->specific,
                sizeof(OBJECT_SPECIFIC));

        /*
         *  Check the type of the object and take appropriate action.
         */

        switch (source_object->code) {

            case OC_PROGRAM_INVOCATION:

                /*
                 *  For a program invocation object, the domain list
                 *  must be checked and copied if necessary.
                 */

                if (source_object->specific->program_invocation.domain_list) {
                    temp_object->specific->program_invocation.domain_list =
                            (OBJECT_INDEX *) calloc(
                            (unsigned)source_object->specific->
                            program_invocation.domain_count,
                            sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->program_invocation.domain_list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->program_invocation.domain_list,
                            (char *)source_object->specific->program_invocation.domain_list,
                            (int)(source_object->specific->program_invocation.domain_count *
                            sizeof(OBJECT_INDEX)));
                }
                break;

            case OC_DATA_TYPE:

                /*
                 *  For a data type object, the symbol must be
                 *  checked and copied if necessary.
                 */

                if (source_object->specific->data_type.symbol) {
                    temp_object->specific->data_type.symbol =
                            (char *) calloc(
                            (unsigned)source_object->specific->
                            data_type.symbol_size,
                            sizeof(char));
                    if (!temp_object->specific->
                            data_type.symbol) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->data_type.symbol,
                            (char *)source_object->specific->data_type.symbol,
                            (int)(source_object->specific->data_type.symbol_size *
                            sizeof(char)));
                }
                break;

            case OC_DATA_TYPE_STRUCTURE:

                /*
                 *  For a data type structure object, the type list and
                 *  size list must be checked and copied if necessary.
                 */

                if (source_object->specific->data_type_structure.type_list) {
                    temp_object->specific->data_type_structure.type_list =
                            (OBJECT_INDEX *) calloc(
                            (unsigned)source_object->specific->
                            data_type_structure.elem_count,
                            sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->
                            data_type_structure.type_list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->data_type_structure.type_list,
                            (char *)source_object->specific->data_type_structure.type_list,
                            (int)(source_object->specific->data_type_structure.elem_count *
                            sizeof(OBJECT_INDEX)));
                }

                if (source_object->specific->data_type_structure.size_list) {
                    temp_object->specific->data_type_structure.size_list =
                            (UINT8 *) calloc(
                            (unsigned)source_object->specific->
                            data_type_structure.elem_count,
                            sizeof(UINT8));
                    if (!temp_object->specific->data_type_structure.size_list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->data_type_structure.size_list,
                            (char *)source_object->specific->data_type_structure.size_list,
                            (int)(source_object->specific->data_type_structure.elem_count *
                            sizeof(UINT8)));
                }
                break;

            case OC_VARIABLE_LIST:

                /*
                 *  For a variable list object, the list must be
                 *  checked and copied if necessary.
                 */

                if (source_object->specific->variable_list.list) {
                    temp_object->specific->variable_list.list =
                            (OBJECT_INDEX *) calloc(
                            (unsigned)source_object->specific->
                            variable_list.elem_count,
                            sizeof(OBJECT_INDEX));
                    if (!temp_object->specific->variable_list.list) {
                        goto error_cleanup;
                    }
                    (void) memcpy(
                            (char *)temp_object->specific->variable_list.list,
                            (char *)source_object->specific->variable_list.list,
                            (int)(source_object->specific->variable_list.elem_count *
                            sizeof(OBJECT_INDEX)));
                }
                break;

            default:
                break;
        }
    }

    *destination_object = temp_object;
    return(FFDS_SUCCESS);

error_cleanup:
    if (temp_object->name)
    {
        free(temp_object->name);
    }
    if (temp_object->extension)
    {
        free(temp_object->extension);
    }
    if (temp_object->specific)
    {
        free(temp_object->specific);
    }
    free(temp_object);

    return FFDS_MEMORY_ERR;

}

/***********************************************************************
 *
 *  Name:  softing_comm_get
 *
 *  ShortDesc:  Get an object from a simulated Type A device.
 *
 *  Description:
 *      The softing_comm_get function takes a communication reference of a
 *      VFD (OD) and object index and gets the corresponding object.
 *      The requested object output is a pointer to an object.  This
 *      object and any of its allocated contents reside in system
 *      memory.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *
 *  Outputs:
 *      object - the requested object.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *      Return values from sim_object_copy function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
softing_comm_get(CREF cref, OBJECT_INDEX object_index, OBJECT **object)
{

    int      sim_od_handle;
    OBJECT  *od_object;
    int      r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Get the object from the OD and copy it into system memory.
     */

    r_code = rod_get(sim_od_handle, object_index, &od_object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    r_code = sim_object_copy(object, od_object);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }
    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  sim_comm_get
 *
 *  ShortDesc:  Get an object from a simulated Type A device.
 *
 *  Description:
 *      The sim_comm_get function takes a communication reference of a
 *      VFD (OD) and object index and gets the corresponding object.
 *      The requested object output is a pointer to an object.  This
 *      object and any of its allocated contents reside in system
 *      memory.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *
 *  Outputs:
 *      object - the requested object.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *      Return values from sim_object_copy function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_comm_get(CREF cref, OBJECT_INDEX object_index, OBJECT **object)
{

    int      sim_od_handle;
    OBJECT  *od_object;
    int      r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Get the object from the OD and copy it into system memory.
     */

    r_code = rod_get(sim_od_handle, object_index, &od_object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    r_code = sim_object_copy(object, od_object);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  sim_comm_put
 *
 *  ShortDesc:  Put an object into a simulated Type A device.
 *
 *  Description:
 *      The sim_comm_put function takes a communication reference of a
 *      VFD (OD), object index, and object and puts the object into
 *      the corresponding VFD (OD).
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *      object - the object to be put.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_comm_put(CREF cref, OBJECT_INDEX object_index, OBJECT *object)
{

    int      sim_od_handle;
    OBJECT  *od_object;
    int      r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Copy the object into the OD heap and put it into the OD.
     */

    r_code = rod_object_copy(&od_object, object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    r_code = rod_put(sim_od_handle, object_index, od_object);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  softing_comm_read
 *
 *  ShortDesc:  Read an object's value from a Type A device.
 *
 *  Description:
 *      The softing_comm_read function takes a communication reference of
 *      a VFD (OD), object index, and sub-index and reads the object's
 *      value from the corresponding VFD (OD).  A non-zero sub-index
 *      implies a particular array or record member will be read.  If
 *      the sub-index is zero, the object's entire value will be read.
 *      The requested value output is a pointer to a byte stream.  The
 *      object that describes the value is also an output of the
 *      function.  Both the value and type object are allocated in
 *      system memory.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *      sub_index - the sub-index of the (array or record) member.
 *
 *  Outputs:
 *      value - the requested value.
 *      type_object - the object describing the structure of the value.
 *                    (This may be NULL.)
 *      value_size - the length of the data.  (This may be NULL).
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/
int
softing_comm_read (
        ENV_INFO *env_info,
        UINT16 cr,
        OBJECT_INDEX object_index,
        SUBINDEX sub_index,
        UINT8 **value,
        OBJECT **type_object,
        int *value_size)
{
    int             r_code = CM_SUCCESS;
    USIGN8          obj_code;
    NT_COUNT        nt_count;
    APP_INFO        *app_info;
    FBAP_NMA_REQ    fbap_nma_req;
    OD_DESCRIPTION_SPECIFIC fbap_od_desc;
    NETWORK_HANDLE   nh;

    app_info = (APP_INFO *)env_info->app_info;

    /*
     * Get the OD descriptor of the communicating device
     */

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    /*
     *  Read the value from the OPOD and copy it into system memory.
     *  (Also copy the type object into system memory.)
     */

    if(object_index)
    {
        fbap_nma_req.index = object_index;
        fbap_nma_req.sub_index = sub_index;
        fbap_nma_req.cr_index = cr;
        r_code = send_wait_receive_cmd(env_info, GET_OD_SECOND_CL,
                &fbap_nma_req, NULL);
        if(CM_SUCCESS != r_code) {
        	r_code = handle_cr_timeout(r_code, env_info, GET_OD_SECOND_CL, &fbap_nma_req,
        			NULL);
        	if(CM_SUCCESS != r_code)
        		return r_code;
        }

        if(NEG == get_service_result())
        {
            T_ERROR *error = (T_ERROR *)global_res_buf;
            log_error(error);
            app_info->env_returns.response_code = error->class_code;
            return CS_FMS_ERR(error->class_code);
        }
        else if (POS == get_service_result())
        {
            obj_code = global_res_buf[OBJ_CODE_LOC];

            switch(obj_code)
            {
                case NULL_OBJECT:

                    break;
                case OD_OBJECT:

                    break;

                case DOMAIN_OBJECT:
                    r_code = obj_domain_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);
                    break;

                case INVOCATION_OBJECT:
                    r_code = obj_invocation_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);

                    break;

                case EVENT_OBJECT:
                    r_code = obj_event_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);

                    break;

                case TYPE_OBJECT:
                    r_code = obj_type_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);

                    break;

                case TYPE_STRUCT_OBJECT:
                    r_code = obj_type_struct_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);

                    break;

                case SIMPLE_VAR_OBJECT:
                    r_code = obj_simple_var_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);
                    break;

                case ARRAY_OBJECT:
                    r_code = obj_array_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);
                    break;

                case RECORD_OBJECT:
                    r_code = obj_record_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);
                    break;

                case VAR_LIST_OBJECT:
                    r_code = obj_var_list_desc(env_info, cr, object_index, sub_index, type_object, value, value_size);

                    break;

                case VAR_OBJECT:


                    break;

                case ALL_OBJECT:


                    break;
            } /* End of Switch */
        } /* End of if */

    }

    return r_code;
}

/***********************************************************************
 *
 *  Name:  softing_comm_write
 *
 *  ShortDesc:  Write an object's value into a simulated Type A device.
 *
 *  Description:
 *      The softing_comm_write function takes a communication reference of
 *      a VFD (OD), object index, sub-index, and value and writes the
 *      object's value into the corresponding VFD (OD).  A non-zero
 *      sub-index implies a particular array or record member will be
 *      written.  If the sub-index is zero, the object's entire value
 *      will be written.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *      sub_index - the sub-index of the (array or record) member.
 *      value - the value to be written.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
softing_comm_write (
    ENV_INFO *env_info,
    UINT16 cr,
    OBJECT_INDEX object_index,
    SUBINDEX sub_index,
    UINT8 *value,
    unsigned short size)
{
    int     r_code;
    int     bitStringType = FALSE;
    USIGN8          obj_code;
    int count;
    T_DEVICE_ERROR dev_err;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    T_H1_OD_DATA_TYPE_DESCRIPTION               obj_data_type_description;
    T_H1_OD_DATA_TYPE_STRUCTURE                 obj_type_struct;
    T_H1_SIMPLE_VAR_OBJECT                      obj_simple_var;
    T_H1_ARRAY_OBJECT                           obj_array;
    T_H1_RECORD_OBJECT                          obj_record;

    FBAP_NMA_REQ fbap_nma_req;

    memset((char*) &dev_err, 0, sizeof(T_DEVICE_ERROR));

    if(object_index)
    {
        fbap_nma_req.index = object_index;
        fbap_nma_req.sub_index = sub_index;
        fbap_nma_req.cr_index = cr;
        r_code = send_wait_receive_cmd(env_info, GET_OD_SECOND_CL,
                &fbap_nma_req, NULL);
        if(CM_SUCCESS != r_code) {
        	r_code = handle_cr_timeout(r_code, env_info, GET_OD_SECOND_CL, &fbap_nma_req,
        			NULL);
        	if(CM_SUCCESS != r_code)
        		return r_code;
        }

        if(NEG == get_service_result())
        {
            T_ERROR *error = (T_ERROR *)global_res_buf;

            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Error Class code = %d\n",
                error->class_code);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Detail = %d\n",
                error->add_detail);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Description = %s\n",
                error->add_description);
            app_info->env_returns.response_code = error->class_code;
            return CS_FMS_ERR(error->class_code);
        }

        else if(POS == get_service_result())
        {
            obj_code = global_res_buf[OBJ_CODE_LOC];

            switch(obj_code)
            {
                case NULL_OBJECT:
                case OD_OBJECT:
                case DOMAIN_OBJECT:
                case INVOCATION_OBJECT:
                case EVENT_OBJECT:
                case VAR_LIST_OBJECT:
                case VAR_OBJECT:
                case ALL_OBJECT:
                    break;

                case TYPE_OBJECT:
                        (void *)memcpy(
                            (T_H1_OD_DATA_TYPE_DESCRIPTION* )&obj_data_type_description,
                            (T_H1_OD_DATA_TYPE_DESCRIPTION *)(global_res_buf + 3),
                            sizeof(T_H1_OD_DATA_TYPE_DESCRIPTION)
                            );
                        obj_data_type_description.ObjectIndex       =   convert_to_ltl_endian(obj_data_type_description.ObjectIndex);
                    break;

                case TYPE_STRUCT_OBJECT:
                        (void *)memcpy(
                            (T_H1_OD_DATA_TYPE_STRUCTURE* )&obj_type_struct,
                            (T_H1_OD_DATA_TYPE_STRUCTURE* )(global_res_buf + 3),
                            sizeof(T_H1_OD_DATA_TYPE_STRUCTURE)
                            );

                        obj_type_struct.ObjectIndex =   convert_to_ltl_endian(obj_type_struct.ObjectIndex);
                        LOGC_TRACE(CPM_FF_DDSERVICE,"No: of elements: =   %d\n",obj_type_struct.NumberOfElements);

                        obj_type_struct.list = (T_H1_DATA_TYPE_ELEMENT *)calloc(1,sizeof(T_H1_DATA_TYPE_ELEMENT) * obj_type_struct.NumberOfElements);
                        (void *) memcpy(
                            (T_H1_DATA_TYPE_ELEMENT *)obj_type_struct.list,
                            (T_H1_DATA_TYPE_ELEMENT *)(global_res_buf + 7),
                            (sizeof(T_H1_DATA_TYPE_ELEMENT) * obj_type_struct.NumberOfElements)
                        );

                        for(int index = 0; index < obj_type_struct.NumberOfElements; index++)
                        {
                            obj_type_struct.list[index].DataTypeIndex = convert_to_ltl_endian(obj_type_struct.list[index].DataTypeIndex);
                            LOGC_TRACE(CPM_FF_DDSERVICE,"Datatype index of %d Element =   %x\tLength  =   %d\n",
                                count,obj_type_struct.list[index].DataTypeIndex,
                                obj_type_struct.list[index].Length);
                        }

                        if ((sub_index <= obj_type_struct.NumberOfElements) &&
                            (DT_BIT_STRING == obj_type_struct.list[sub_index - 1].DataTypeIndex))
                        {
                            LOGC_INFO(CPM_FF_DDSERVICE, "Variable is bit string");
                            bitStringType = TRUE;
                        }

                        free(obj_type_struct.list);

                    break;

                case SIMPLE_VAR_OBJECT:
                        (void* ) memcpy((T_H1_SIMPLE_VAR_OBJECT *)&obj_simple_var, (char *)(global_res_buf + 3), sizeof(T_H1_SIMPLE_VAR_OBJECT));
                        obj_simple_var.ObjectIndex      =   convert_to_ltl_endian(obj_simple_var.ObjectIndex);
                        obj_simple_var.DataTypeIndex    =   convert_to_ltl_endian(obj_simple_var.DataTypeIndex);
                        if(DT_BIT_STRING == obj_simple_var.DataTypeIndex)
                        {
                            bitStringType = TRUE;
                        }

                    break;

                case ARRAY_OBJECT:
                        (void* ) memcpy(
                            (T_H1_ARRAY_OBJECT *)&obj_array,
                            (char *)(global_res_buf + 3),
                            sizeof(T_H1_ARRAY_OBJECT)
                        );

                        obj_array.ObjectIndex       =   convert_to_ltl_endian(obj_array.ObjectIndex);
                        obj_array.DataTypeIndex =   convert_to_ltl_endian(obj_array.DataTypeIndex);
                        if(DT_BIT_STRING == obj_array.DataTypeIndex)
                        {
                            bitStringType = TRUE;
                        }

                    break;

                case RECORD_OBJECT:
                    {
                        (void *) memcpy(
                            (T_H1_RECORD_OBJECT* )&obj_record,
                            (T_H1_RECORD_OBJECT* )(global_res_buf + 3),
                            sizeof(T_H1_RECORD_OBJECT)
                        );
                        obj_record.ObjectIndex      =   convert_to_ltl_endian(obj_record.ObjectIndex);
                        obj_record.DataTypeIndex    =   convert_to_ltl_endian(obj_record.DataTypeIndex);

                        fbap_nma_req.index = obj_record.DataTypeIndex;
                        fbap_nma_req.sub_index = sub_index;
                        fbap_nma_req.cr_index = cr;
                        r_code = send_wait_receive_cmd(env_info, GET_OD_SECOND_CL,
                                    &fbap_nma_req, NULL);
                        if(CM_SUCCESS != r_code) {
                        	r_code = handle_cr_timeout(r_code, env_info, GET_OD_SECOND_CL, &fbap_nma_req,
                        			NULL);
                        	if(CM_SUCCESS != r_code)
                        		return r_code;
                        }
                        if(NEG == get_service_result())
                        {
                            T_ERROR *error = (T_ERROR *)global_res_buf;

                            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Error Class code = %d\n",
                                error->class_code);
                            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Detail = %d\n",
                                error->add_detail);
                            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Description = %s\n",
                                error->add_description);
                            app_info->env_returns.response_code = error->class_code;
                            return CS_FMS_ERR(error->class_code);
                        }
                        else if(POS == get_service_result())
                        {
                            (void *)memcpy(
                            (T_H1_OD_DATA_TYPE_STRUCTURE* )&obj_type_struct,
                            (T_H1_OD_DATA_TYPE_STRUCTURE* )(global_res_buf + 3),
                            sizeof(T_H1_OD_DATA_TYPE_STRUCTURE)
                            );

                            obj_type_struct.ObjectIndex =   convert_to_ltl_endian(obj_type_struct.ObjectIndex);
                            LOGC_TRACE(CPM_FF_DDSERVICE,"No: of elements: =   %d\n",obj_type_struct.NumberOfElements);

                            obj_type_struct.list = (T_H1_DATA_TYPE_ELEMENT *)calloc(1,sizeof(T_H1_DATA_TYPE_ELEMENT) * obj_type_struct.NumberOfElements);
                            (void) memcpy(
                                (T_H1_DATA_TYPE_ELEMENT *)obj_type_struct.list,
                                (T_H1_DATA_TYPE_ELEMENT *)(global_res_buf + 7),
                                (sizeof(T_H1_DATA_TYPE_ELEMENT) * obj_type_struct.NumberOfElements)
                                );
                            for(int index = 0; index < obj_type_struct.NumberOfElements; index++)
                            {
                                obj_type_struct.list[index].DataTypeIndex = convert_to_ltl_endian(obj_type_struct.list[index].DataTypeIndex);
                                LOGC_TRACE(CPM_FF_DDSERVICE,"Datatype index of %d Element =   %x\tLength  =   %d\tType  =   %d\n",
                                    index, obj_type_struct.list[index].DataTypeIndex,
                                    obj_type_struct.list[index].Length,
                                    obj_type_struct.list[index].DataTypeIndex);
                            }
                            if ((sub_index <= obj_type_struct.NumberOfElements) &&
                                (DT_BIT_STRING == obj_type_struct.list[sub_index - 1].DataTypeIndex))
                            {
                                LOGC_INFO(CPM_FF_DDSERVICE, "Variable is bit string");
                                bitStringType = TRUE;
                            }
                            free(obj_type_struct.list);
                        }
                    }
                    break;
            } /* End of Switch */
        } /* End of if */
    }

    if(TRUE == bitStringType)
    {
        (void) bit_rev(value, size);
    }

    /*
     *  Write the value into the Device.
     */
    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.size = size;
    fbap_nma_req.cr_index = cr;
    
    if(fbap_nma_req.size > 64)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Fixit, Size of the value is more than memory assigned\n");
    }
    (void *)memcpy((char *)fbap_nma_req.value, (char *)value, size);

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_WRITE_REQ,
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_WRITE_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }

    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_comm_write: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;
        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        app_info->env_returns.comm_err = CM_SUCCESS;
        app_info->env_returns.response_code = CM_SUCCESS;
        return(CM_SUCCESS);
    }
    return(CM_SUCCESS);
}
/*end*/

/***********************************************************************
 *
 *  Name:  sim_comm_read
 *
 *  ShortDesc:  Read an object's value from a simulated Type A device.
 *
 *  Description:
 *      The sim_comm_read function takes a communication reference of
 *      a VFD (OD), object index, and sub-index and reads the object's
 *      value from the corresponding VFD (OD).  A non-zero sub-index
 *      implies a particular array or record member will be read.  If
 *      the sub-index is zero, the object's entire value will be read.
 *      The requested value output is a pointer to a byte stream.  The
 *      object that describes the value is also an output of the
 *      function.  Both the value and type object are allocated in
 *      system memory.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *      sub_index - the sub-index of the (array or record) member.
 *
 *  Outputs:
 *      value - the requested value.
 *      type_object - the object describing the structure of the value.
 *                    (This may be NULL.)
 *      value_size - the length of the data.  (This may be NULL).
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_comm_read(CREF cref, OBJECT_INDEX object_index, SUBINDEX sub_index,
    UINT8 **value, OBJECT **type_object, int *value_size)
{

    int      sim_od_handle;
    UINT8   *od_value;
    OBJECT  *od_type_object;
    int      r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Read the value from the OPOD and copy it into system memory.
     *  (Also copy the type object into system memory.)
     */

    r_code = rod_read(sim_od_handle, object_index, sub_index, &od_value,
            &od_type_object, value_size);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
    if (!*value) {
        return(FFDS_MEMORY_ERR);
    }
    (void) memcpy((char *)*value, (char *)od_value, *value_size);

    r_code = sim_object_copy(type_object, od_type_object);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  sim_comm_write
 *
 *  ShortDesc:  Write an object's value into a simulated Type A device.
 *
 *  Description:
 *      The sim_comm_write function takes a communication reference of
 *      a VFD (OD), object index, sub-index, and value and writes the
 *      object's value into the corresponding VFD (OD).  A non-zero
 *      sub-index implies a particular array or record member will be
 *      written.  If the sub-index is zero, the object's entire value
 *      will be written.
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of the requested object.
 *      sub_index - the sub-index of the (array or record) member.
 *      value - the value to be written.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_comm_write(CREF cref, OBJECT_INDEX object_index, SUBINDEX sub_index,
    UINT8 *value)
{

    int     sim_od_handle;
    int     r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Write the value into the OD.
     */

    r_code = rod_write(sim_od_handle, object_index, sub_index, value);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  sim_comm_upload
 *
 *  ShortDesc:  Upload an object's data into a simulated Type A device.
 *
 *  Description:
 *      The sim_comm_upload function takes a communication reference
 *      of a VFD (OD), object index, and data and uploads the (domain)
 *      data into the corresponding VFD (OD).
 *
 *  Inputs:
 *      cref - the cref of the VFD that the object exists in.
 *      object_index - the index of object to be uploaded.
 *      data - the data to be uploaded.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_ROD_ERR.
 *      FFDS_COMM_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_comm_upload(CREF cref, OBJECT_INDEX object_index, UINT8 *data)
{

    int     sim_od_handle;
    int     r_code;

    /*
     *  Check to make sure that the communication reference is valid.
     */

    sim_od_handle = cref;

    if (!valid_rod_handle(sim_od_handle)) {
        return(FFDS_COMM_ERR);
    }

    /*
     *  Write (upload) the data into the OD.
     */

    r_code = rod_write(sim_od_handle, object_index, (SUBINDEX)0, data);
    if (r_code) {
        return(FFDS_ROD_ERR);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  softing_find_block
 *
 *  ShortDesc:  Find a block with the specified tag.
 *
 *  Description:
 *      The sim_find_block functions takes a block tag and Configuration
 *      File ID and searches the corresponding network for the tag.  If
 *      found, a Find Confirm structure will be returned as an output
 *      parameter of the function.  This structure contains location
 *      information for the block and its device.
 *
 *  Inputs:
 *      blk_tag - the tag of the block to be searched for.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_FIND_BLK_ERR.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
softing_find_block(char *blk_tag, NETWORK_HANDLE nh, FIND_CNF_A **find_cnf)
{

    int              netwk_num;
    int              blk_num;
    FIND_CNF_TBL    *find_cnf_tbl;

    /*
     *  Get the (simulated) network number that corresponds to the
     *  network, and get the corresponding Find Confirm Table.
     */

    netwk_num = nh;

    find_cnf_tbl = &netwk_tbl.list[netwk_num].find_cnf_tbl;

    /*
     *  Go through each block in the Find Confirm Table.
     */

    for (blk_num = 0; blk_num < find_cnf_tbl->count; blk_num ++) {

        /*
         *  Check if the block tag matches.
         */

        if (strncmp(blk_tag, find_cnf_tbl->blk_tag_list[blk_num], strlen(blk_tag)) == 0) {
            *find_cnf = &find_cnf_tbl->find_cnf_list[blk_num];
            return(FFDS_SUCCESS);
        }
    }
    return(FFDS_FIND_BLK_ERR);
}


/***********************************************************************
 *
 *  Name:  sim_find_block
 *
 *  ShortDesc:  Find a block with the specified tag.
 *
 *  Description:
 *      The sim_find_block functions takes a block tag and Configuration
 *      File ID and searches the corresponding network for the tag.  If
 *      found, a Find Confirm structure will be returned as an output
 *      parameter of the function.  This structure contains location
 *      information for the block and its device.
 *
 *  Inputs:
 *      blk_tag - the tag of the block to be searched for.
 *      config_file_id - the Configuration File ID of the network.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_FIND_BLK_ERR.
 *      Return values from cfg_file_id_to_network_num function.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_find_block(char *blk_tag, int config_file_id, FIND_CNF_A **find_cnf)
{

    int              netwk_num;
    int              blk_num;
    FIND_CNF_TBL    *find_cnf_tbl;
    int              r_code;

    /*
     *  Get the (simulated) network number that corresponds to the
     *  Configuration File ID, and get the corresponding Find Confirm Table.
     */

    r_code = cfg_file_id_to_network_num(config_file_id, &netwk_num);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }
    find_cnf_tbl = &netwk_tbl.list[netwk_num].find_cnf_tbl;

    /*
     *  Go through each block in the Find Confirm Table.
     */

    for (blk_num = 0; blk_num < find_cnf_tbl->count; blk_num ++) {

        /*
         *  Check if the block tag matches.
         */

        if (strcmp(blk_tag, find_cnf_tbl->blk_tag_list[blk_num]) == 0) {
            *find_cnf = &find_cnf_tbl->find_cnf_list[blk_num];
            return(FFDS_SUCCESS);
        }
    }
    return(FFDS_FIND_BLK_ERR);
}


/***********************************************************************
 *
 *  Name:  check_sim_params
 *
 *  ShortDesc:  Check if the simulated parameters exist for a block.
 *
 *  Description:
 *
 *  Inputs:
 *      block_handle - handle of the block.
 *
 *  Outputs:
 *      exist_flag - 1 if simulated parameters exist.
 *                   0 if simulated parameters do not exist.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from get_abt_param_count function.
 *      Return values from get_abt_adt_offset function.
 *      Return values from get_adt_a_opod_cref function.
 *      Return values from get_abt_op_index function.
 *      Return values from rod_get function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
check_sim_params(BLOCK_HANDLE block_handle, int *exist_flag)
{

    unsigned int    param_count;
    DEVICE_HANDLE   device_handle;
    CREF            opod_cref;
    ROD_HANDLE      rod_handle;
    OBJECT_INDEX    op_index;
    OBJECT *        object;
    int             r_code;

    /*
     *  Check to make sure that the block has parameters.
     */

    r_code = get_abt_param_count(block_handle, &param_count);
    if (r_code) {
        return(r_code);
    }
    if (param_count == 0) {
        *exist_flag = 1;
        return(FFDS_SUCCESS);
    }

    /*
     *  The simulated parameters are stored in a ROD.
     *  Get the corresponding ROD Handle.
     */

    r_code = get_abt_adt_offset(block_handle, &device_handle);
    if (r_code) {
        return(r_code);
    }
    r_code = get_adt_a_opod_cref(device_handle, &opod_cref);
    if (r_code) {
        return(r_code);
    }
    rod_handle = (ROD_HANDLE)opod_cref;

    /*
     *  Get the first parameter object and check if it is NULL.
     */

    r_code = get_abt_op_index(block_handle, &op_index);
    if (r_code) {
        return(r_code);
    }
    op_index ++;
    r_code = rod_get(rod_handle, op_index, &object);
    if (r_code) {
        return(r_code);
    }
    if (object->code == OC_NULL) {
        *exist_flag = 0;
    }
    else {
        *exist_flag = 1;
    }
    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  make_sim_params
 *
 *  ShortDesc:  Make the simulated parameters for a block.
 *
 *  Description:
 *
 *  Inputs:
 *      block_handle - handle of the block.
 *
 *  Outputs:
 *      exist_flag - 1 if simulated parameters exist.
 *                   0 if simulated parameters do not exist.
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from get_abt_app_infofunction.
 *      Return values from get_abt_dd_dev_tbls function.
 *      Return values from get_adt_dd_blk_tbl_offset function.
 *      Return values from vtsim_make_sims_params function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

static int
make_sim_params(BLOCK_HANDLE block_handle)
{

    PC *                pcp;
    FLAT_DEVICE_DIR *   flat_device_dirp;
    int                 dd_blk_tbl_offset;
    BLK_TBL_ELEM *      blk_tbl_elemp;
    FLAT_BLOCK_DIR *    flat_block_dirp;
    int                 r_code;

    /*
     *  Get the pointer to the Parameter Cache.
     */

    // CPMHACK - Added right function to pc info instead of app info API
    //r_code = get_abt_app_info(block_handle, (void **)&pcp);
    r_code = get_abt_pc_info(block_handle, (void **)&pcp);
    if (r_code) {
        return(r_code);
    }

    /*
     *  Get the pointer to the Flat Block Directory.
     */

    r_code = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dirp);
    if (r_code) {
        return(r_code);
    }
    r_code = get_abt_dd_blk_tbl_offset(block_handle, &dd_blk_tbl_offset);
    if (r_code) {
        return(r_code);
    }
    blk_tbl_elemp = &flat_device_dirp->blk_tbl.list[dd_blk_tbl_offset];
    flat_block_dirp = &blk_tbl_elemp->flat_block_dir;

    /*
     *  Make the simulated parameters.
     */

    r_code = vtsim_make_sim_params(block_handle, pcp, flat_block_dirp);
    if (r_code) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  sim_param_load
 *
 *  ShortDesc:  Read a parameter's value from a device.
 *
 *  Description:
 *      The cr_param_load function takes a block handle,
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *      cm_value_listp - The requested value(s).
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
softing_param_load(ENV_INFO *env_info,
    int po, SUBINDEX si, COMM_VALUE_LIST *comm_value_list)
{
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    CM_VALUE_LIST   cm_value_list ;


    /*
     *  Get the value of the Block Object from the device.
     */

    r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_read(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    r_code = type_to_ddl_type(&cm_value_list, comm_value_list) ;
    free_value_list(&cm_value_list) ;

    return(r_code) ;
}


/***********************************************************************
 *
 *  Name:  sim_param_load
 *
 *  ShortDesc:  Read a parameter's value from a device.
 *
 *  Description:
 *      The cr_param_load function takes a block handle,
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *      cm_value_listp - The requested value(s).
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
sim_param_load(ENV_INFO *env_info,
    int po, SUBINDEX si, COMM_VALUE_LIST *comm_value_list)
{

    int             exist_flag ;
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    CM_VALUE_LIST   cm_value_list ;

    /*
     *  Check for the simulated parameters and make them if necessary.
     */

    if (po != 0) {
        r_code = check_sim_params(env_info->block_handle, &exist_flag);
        if (r_code) {
            return(r_code);
        }
        if (!exist_flag) {
            r_code = make_sim_params(env_info->block_handle);
            if (r_code) {
                return(r_code);
            }
        }
    }

    /*
     *  Get the value of the Block Object from the device.
     */

    r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_read(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    r_code = type_to_ddl_type(&cm_value_list, comm_value_list) ;
    free_value_list(&cm_value_list) ;

    return(r_code) ;
}


/***********************************************************************
 *
 *  Name:  sim_param_unload
 *
 *  ShortDesc:  Read a parameter's value from a device.
 *
 *  Description:
 *      The cr_param_unload function takes a block handle,
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be written.
 *            cache and it does not update the parameter cache.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *      cm_value_listp - The requested value(s).
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
sim_param_unload(ENV_INFO *env_info,
    int po, SUBINDEX si, COMM_VALUE_LIST *comm_value_list)
{

    int             exist_flag ;
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    CM_VALUE_LIST   cm_value_list ;

    r_code = type_from_ddl_type(comm_value_list, &cm_value_list) ;
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Check for the simulated parameters and make them if necessary.
     */

    if (po != 0) {
        r_code = check_sim_params(env_info->block_handle, &exist_flag);
        if (r_code) {
            return(r_code);
        }
        if (!exist_flag) {
            r_code = make_sim_params(env_info->block_handle);
            if (r_code) {
                return(r_code);
            }
        }
    }

    /*
     *  Write the value of the Block Object to the device.
     */

    r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_write(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);

    free_value_list(&cm_value_list) ;
    return(r_code) ;
}

/***********************************************************************
 *
 *  Name:  softing_param_read
 *
 *  ShortDesc:  Read a parameter's value from a device and
 *              put it into the parameter cache.
 *
 *  Description:
 *      The cr_param_read function takes a block handle,
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
softing_param_read(ENV_INFO *env_info, int po, SUBINDEX si)
{
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    COMM_VALUE_LIST comm_value_list ;
    CM_VALUE_LIST   cm_value_list ;
    P_REF           p_ref ;
    APP_INFO        *app_info;
    int lock = NO_LOCK;
    ENV_INFO        tenv_info;
    APP_INFO        tapp_info;

    app_info = env_info->app_info;
    if(app_info->status_info == 0)
        //app_info->status_info = USE_DEV_VALUE;
    (void)memset((char *) &tenv_info, 0, sizeof (ENV_INFO));
    (void)memset((char *) &tapp_info, 0, sizeof (APP_INFO));
    tenv_info.app_info = &tapp_info;
    tenv_info.block_handle = env_info->block_handle;
    tapp_info.status_info = app_info->status_info;
    tapp_info.action_storage = app_info->action_storage;
    tapp_info.method_tag = app_info->method_tag;

    /*
     *  Get the value of the Object from the device.
     */

    p_ref.type = PC_PARAM_OFFSET_REF ;
    p_ref.po = po ;
    p_ref.subindex = si ;

    r_code = get_abt_adt_offset(env_info->block_handle,
            &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_read(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     * Put the value into the parameter cache
     */
    r_code = type_to_ddl_type(&cm_value_list, &comm_value_list) ;
    free_value_list(&cm_value_list) ;

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }
    /*
     * The env_info->action_storage must be preset to NULL here because
     * ps get_param_value_list is not called to initialize it (and can't)
     * This is what the comm. stack must do prior to calling
     * ps put_param_value_list,
     */

    app_info->action_storage = (void *)NULL;

    r_code = pc_put_param_value_list (env_info, &p_ref, &comm_value_list, lock);
    cr_free_value_list(&comm_value_list) ;

    return(r_code) ;
}

/***********************************************************************
 *
 *  Name:  softing_param_write
 *
 *  ShortDesc:  Write a parameter's value to a device
 *              taking it from the parameter cache.
 *
 *  Description:
 *      The softing_param_write function takes a environment info ptr,
 *      param ref, and reads an parameter's value from the
 *      corresponding device.  If the sub-index iszero, the
 *      object's entire value will be read.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
softing_param_write(ENV_INFO *env_info, int po, SUBINDEX si)
{

    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    CM_VALUE_LIST   cm_value_list ;
    COMM_VALUE_LIST comm_value_list ;
    P_REF           p_ref ;

    /*
     * Put the value into the parameter cache
     */

    p_ref.type = PC_PARAM_OFFSET_REF ;
    p_ref.po = po ;
    p_ref.subindex = si ;

    comm_value_list.cmvl_limit = 4;  /* 4 is completely arbitrary */
    comm_value_list.cmvl_count = 0;
    comm_value_list.cmvl_list = (COMM_VALUE *)malloc (comm_value_list.cmvl_limit *
                                    sizeof(*comm_value_list.cmvl_list));
    if (comm_value_list.cmvl_list == (COMM_VALUE *)NULL) {
        return (CM_NO_MEMORY);
    }
    r_code = pc_get_param_value_list (env_info, &p_ref, &comm_value_list);

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     * Convert the structure containing DDL value types in a structure
     * containing Type A value types.
     */

    r_code = type_from_ddl_type(&comm_value_list, &cm_value_list) ;
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Put the value of the Object from the device.
     */

    r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_write(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);

    if (r_code != CM_SUCCESS) {
        cr_free_value_list(&comm_value_list) ;
        return(r_code);
    }

    /*
     * Put the param_value_list into the param cache
     */

    r_code = pc_put_param_value_list (env_info, &p_ref, &comm_value_list, 0);

    cr_free_value_list(&comm_value_list) ;
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    free_value_list(&cm_value_list) ;
    return(CM_SUCCESS) ;
}

/***********************************************************************
 *
 *  Name:  sim_param_read
 *
 *  ShortDesc:  Read a parameter's value from a device and
 *              put it into the parameter cache.
 *
 *  Description:
 *      The cr_param_read function takes a block handle,
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
sim_param_read(ENV_INFO *env_info, int po, SUBINDEX si)
{

    int             exist_flag ;
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    COMM_VALUE_LIST comm_value_list ;
    CM_VALUE_LIST   cm_value_list ;
    P_REF           p_ref ;
    APP_INFO        *app_info;

    app_info = env_info->app_info;

    /*
     *  Check for the simulated parameters and make them if necessary.
     */

    if (po != 0) {
        r_code = check_sim_params(env_info->block_handle, &exist_flag);
        if (r_code) {
            return(r_code);
        }
        if (!exist_flag) {
            r_code = make_sim_params(env_info->block_handle);
            if (r_code) {
                return(r_code);
            }
        }
    }

    /*
     *  Get the value of the Object from the device.
     */

    p_ref.type = PC_PARAM_OFFSET_REF ;
    p_ref.po = po ;
    p_ref.subindex = si ;

    r_code = get_abt_adt_offset(env_info->block_handle,
            &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_read(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     * Put the value into the parameter cache
     */
    r_code = type_to_ddl_type(&cm_value_list, &comm_value_list) ;
    free_value_list(&cm_value_list) ;

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }
    /*
     * The env_info->action_storage must be preset to NULL here because
     * ps get_param_value_list is not called to initialize it (and can't)
     * This is what the comm. stack must do prior to calling
     * ps put_param_value_list,
     */
    app_info->action_storage = (void *)NULL;
    r_code = pc_put_param_value_list (env_info, &p_ref, &comm_value_list, 0);

    cr_free_value_list(&comm_value_list) ;

    return(r_code) ;
}

/***********************************************************************
 *
 *  Name:  sim_param_write
 *
 *  ShortDesc:  Write a parameter's value to a device
 *              taking it from the parameter cache.
 *
 *  Description:
 *      The cr_param_write function takes a environment info ptr,
 *      param ref, and reads an parameter's value from the
 *      corresponding device.  If the sub-index iszero, the
 *      object's entire value will be read.
 *
 *  Inputs:
 *      env_info -  the block_handle is passed into the function
 *                  in this structure.
 *      po -        the parameter offset of the parameter to be read.
 *      si -        the subindex  of the parameter to be read.
 *
 *  Outputs:
 *      env_info -  contains the response code and communicaiion
 *                  error (if any exist).
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
sim_param_write(ENV_INFO *env_info, int po, SUBINDEX si)
{

    int             exist_flag ;
    DEVICE_HANDLE   device_handle ;
    int             r_code ;
    OBJECT_INDEX    object_index ;
    CM_VALUE_LIST   cm_value_list ;
    COMM_VALUE_LIST comm_value_list ;
    P_REF           p_ref ;

    /*
     *  Check for the simulated parameters and make them if necessary.
     */

    if (po != 0) {
        r_code = check_sim_params(env_info->block_handle, &exist_flag);
        if (r_code) {
            return(r_code);
        }
        if (!exist_flag) {
            r_code = make_sim_params(env_info->block_handle);
            if (r_code) {
                return(r_code);
            }
        }
    }

    /*
     * Put the value into the parameter cache
     */

    p_ref.type = PC_PARAM_OFFSET_REF ;
    p_ref.po = po ;
    p_ref.subindex = si ;

    comm_value_list.cmvl_limit = 4;  /* 4 is completely arbitrary */
    comm_value_list.cmvl_count = 0;
    comm_value_list.cmvl_list = (COMM_VALUE *)malloc (comm_value_list.cmvl_limit *
                                    sizeof(*comm_value_list.cmvl_list));
    if (comm_value_list.cmvl_list == (COMM_VALUE *)NULL) {
        return (CM_NO_MEMORY);
    }
    r_code = pc_get_param_value_list (env_info, &p_ref, &comm_value_list);

    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     * Convert the structure containing DDL value types in a structure
     * containing Type A value types.
     */

    r_code = type_from_ddl_type(&comm_value_list, &cm_value_list) ;
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Put the value of the Object from the device.
     */

    r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = get_abt_op_index(env_info->block_handle, &object_index) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    object_index = object_index + (OBJECT_INDEX)po ;
    r_code = comm_write(device_handle, (COMM_TYPE)CT_OPOD,
            object_index, si, &cm_value_list, env_info);

    if (r_code != CM_SUCCESS) {
        cr_free_value_list(&comm_value_list) ;
        return(r_code);
    }

    /*
     * Put the param_value_list into the param cache
     */

    r_code = pc_put_param_value_list (env_info, &p_ref, &comm_value_list, 0);

    cr_free_value_list(&comm_value_list) ;
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    free_value_list(&cm_value_list) ;
    return(CM_SUCCESS) ;
}

/***********************************************************************
 *
 *  Name:  sim_comm_get_sim_response_code
 *
 *  ShortDesc:  Get simulated device response code.
 *
 *  Description:
 *      The sim_comm_get_sim_response_code function calls the
 *      response code simulation module (rcsim) in an attempt to
 *      find a simulated response code for a device.
 *
 *      Function expects to find and return only DDS_INTEGER simulated
 *      response code value types.
 *
 *  Inputs:
 *      rcsim_msg_type - type of simulated response code; RCSIM_TYPE_xxx.
 *      network_handle - network handle.
 *      network_type - type of network (NT_xxxx.)
 *      device_station_address - address of device.

 *  Outputs:
 *      sim_response_code - pointer to UINT32 buffer which will
 *      contain response code, if one is found.
 *
 *  Returns:
 *      0 - if no match on network and device found.
 *      1 - if match on network and device found.
 *
 *  Authors:
 *      Joe Fisher
 *
 **********************************************************************/

int
sim_comm_get_sim_response_code(int rcsim_msg_type, NETWORK_HANDLE network_handle,
    NET_TYPE network_type, UINT16 device_station_address, UINT32 *sim_response_code)
{

    int                 r_code=0;
    char                value_buf[128];
    RCSIM_PARAM         rcsim_param;
    RCSIM_VALUE         rcsim_value;

    /* get simulated response code for rcsim_msg_type, if any */
    rcsim_value.value = value_buf;
    rcsim_param.network_handle = network_handle;
    rcsim_param.network_type = network_type;
    rcsim_param.device_station_address = (int)device_station_address;
    r_code = rcsim_get (rcsim_msg_type, &rcsim_param,
                    &rcsim_value, sizeof(value_buf));
    if (r_code != RCSIM_MATCH) {
        return 0;   /* no match on network / device */
    }

    /* convert value to integer comm err. */
    switch (rcsim_value.type) {
    case DDS_INTEGER:
        *sim_response_code = 0;
        *sim_response_code = (UINT32)(*((int *)(rcsim_value.value)));
        break;
    case DDS_UNSIGNED:
        *sim_response_code = (*((UINT32 *)(rcsim_value.value)));
        break;
    default:
        return 0;   /* do not convert other types of values. */
    }

    return 1;   /* return match indication. */
}

/***********************************************************************
 *
 *  Name:  softing_cm_exit
 *
 *  ShortDesc:  Close the communication manager.
 *
 *  Description:
 *      Call the function fbapi_exit api to exit the from the stack
 *      and reset the fbk device
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
softing_cm_exit(ENV_INFO *env_info)
{
    int r_code = CM_SUCCESS;
    APP_INFO *app_info = (APP_INFO *)env_info->app_info;
    int channel = get_device_admin_channel();

    set_device_admin_conn(FB_FALSE);

    // Acquire fbk data thread lock before start processing any request
    acquire_fbk_data_thread_lock();

	// Set the exit status of fbk data thread	
	set_fbk_data_thread_state(THREAD_STATE_STOP);

    LOGC_DEBUG(CPM_FF_DDSERVICE,"FFH1 Modem Read Write Thread canceled\n");

    r_code = fbapi_exit (channel, TRIGGER_HW_RESET);
    if (r_code != E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in reset of FBKhost\n");
        app_info->env_returns.response_code = CM_BAD_RESET;

        release_fbk_data_thread_lock();
        return CM_BAD_RESET;
    }

    // Release fbk data thread lock
    release_fbk_data_thread_lock();

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name:  softing_firmware_download
 *
 *  ShortDesc:  Download the firmware.
 *
 *  Description:
 *      Call the function fbapi_exit api to exit the from the stack
 *      and reset the fbk device and the call fbkhost_firmware_download
 *      to load the firmware spcecified in the path
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *      path: Path to Firmware
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

// TODO - fix this - look at  git@bitbucket.org:flukept/commmodule.git
//   SwUpdate.c - software update is done by external application.
int
softing_firmware_download(ENV_INFO *env_info, char *path)
{
#if 0
    int r_code = CM_SUCCESS;
    STRINGV    *comport_name = NULL;
    T_FBAPI_INIT  ffboard_init;
    T_DOWNLOAD_FIRMWARE firmware;
    int channel = get_device_admin_channel();

    // Save the start time.
    struct timeval start,end;
    gettimeofday(&start, NULL);

    comport_name = getenv(WALLACE_FBK2_PORT);
    if(NULL == comport_name)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to get comport env variable\n");
        return (CM_COMM_ERR);
    }
    memset(ffboard_init.comport_name, '\0', 20);
    (void)strcpy(ffboard_init.serial.ComPortName, comport_name);
    LOGC_ERROR(CPM_FF_DDSERVICE,"Using COM PORT %s\n", ffboard_init.serial.ComPortName);

// parameters initialized in fbapi_init
#if 0
    ffboard_init.baudrate     = CBR_38400;
    ffboard_init.parity       = NOPARITY;
    ffboard_init.stop_bits    = ONESTOPBIT;
    ffboard_init.data_bits    = 8;
#endif
// TODO: verify that these are initialized "elsewhere"
//  see demo_start in FFTestApp/SwUpdate.c in above git repo
#if 0
    ffboard_init.ack_timeout  = 100;
    ffboard_init.read_timeout = 100;
#endif

    sleep(1);

    r_code = fbkhost_init (channel, PROTOCOL_FF, &ffboard_init);
    if (r_code == E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Successful initialization of API interface\n");
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Initialization of API interface fails with error code %d\n",
            r_code);
        return r_code;
    }

    (void)strcpy(firmware.file_name, path);

    LOGC_ERROR(CPM_FF_DDSERVICE,"FFH1 Modem Firmware Download Start %s\n", firmware.file_name);

    /*
     * Downloading the firmware and the firmware file in the filename
     * variable
     */
    r_code = fbkhost_download_firmware (channel, &firmware);
    if (r_code == E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"FBK host Firmware Download success %d\n", r_code);
    }
    else {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in Firmware download to FBKhost\n");
        return CM_COMM_ERR;
    }

    sleep(5);

    /*
     * resetting the board after downloading the firmware
     */
    r_code = fbapi_exit (channel, FBK_HW_RESET);
    if (r_code == E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"FBK host reset success %d\n", r_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Successful de-initialization of API interface\n");
    }
    else {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in reset of FBKhost\n");
        return CM_COMM_ERR;
    }

    // Only report with resolution of seconds.
    gettimeofday(&end, NULL);
    LOGC_ERROR(CPM_FF_DDSERVICE,"Elapsed time of %ld seconds\n",
        end.tv_sec - start.tv_sec);

    return CM_SUCCESS;
#endif
}


/***********************************************************************
 *
 *  Name:  softing_get_host_info
 *
 *  ShortDesc:  Get the information of the Host Device (FBK board).
 *
 *  Description:
 *      Call the function fbkhost_get_device_info to get the device
 *      information of the FBK board.
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *      host_info: Pointer to the Device info structure
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      return from fbkhost_get_device_info function
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
softing_get_host_info(ENV_INFO *env_info, T_FBAPI_DEVICE_INFO *host_info)
{
    int r_code = CM_SUCCESS;
    get_fbk_info(host_info);
    return r_code;
}

/***********************************************************************
 *
 *  Name:  softing_reset_fbk
 *
 *  ShortDesc:  Reset the FBK board.
 *
 *  Description:
 *      Abort the CR's of FBAP and MIB and re-initiate the CR's
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      return from fbkhost_get_device_info function
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

// TODO - fix this - look at  git@bitbucket.org:flukept/commmodule.git

int
softing_reset_fbk(ENV_INFO *env_info, NETWORK_HANDLE nh, HOST_CONFIG *host_config)
{
    int r_code = CM_SUCCESS;
    FBAP_NMA_REQ    fbap_nma_req;
    APP_INFO *app_info = (APP_INFO *)env_info->app_info;
    SM_REQ          sm_req;
    FBAP_NMA_REQ    dll_req;

    STRINGV         *comport_name = NULL;
    T_FBAPI_INIT  ffboard_init;
    const int       MAX_TRIES = 10;
    int             tries = 0;
    int             channel = 0;

    USIGN16         add_detail;

    memset((char *)&fbap_nma_req, 0, sizeof(FBAP_NMA_REQ));
    memset((char *)&sm_req, 0, sizeof(SM_REQ));
    memset((char *)&dll_req, 0, sizeof(FBAP_NMA_REQ));

    /*
     * Acquire the lock for thread safe and block all other requests, if
     * any from other modules
     */
    acquire_fbk_read_lock();

    channel = get_device_admin_channel();

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: init request. Size of the Data in bytes = %d",
            sizeof(ffboard_init) + sizeof(channel));

    /* Reset the FBK2 Hardware */
    r_code = fbapi_exit (channel, FBK_HW_RESET);
    if (r_code != E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in reset of FBKhost (r_code=%d)\n", r_code);
        return r_reason(r_code);
    }

    LOGC_TRACE(CPM_FF_DDSERVICE,"Recv: Reset success. Size of the Data in bytes = %d\n",
            sizeof(r_code));

    memset (&ffboard_init, '\0', sizeof (T_FBAPI_INIT));
    ffboard_init.serial.ComportName = malloc (sizeof (char) * MAX_COM_PORT_NAME_LENGTH);
    memset (ffboard_init.serial.ComportName, '\0', MAX_COM_PORT_NAME_LENGTH);

    if(NULL == host_config->tty_port)
    {
        comport_name = getenv(WALLACE_FBK2_PORT);
        if(NULL == comport_name)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to get comport env variable");
            return (CM_COMM_ERR);
        }
        (void)strcpy(ffboard_init.serial.ComportName, comport_name);
    }
    else
    {
        (void)strcpy(ffboard_init.serial.ComportName, host_config->tty_port);
    }

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Using the COM Port: %s", ffboard_init.serial.ComportName);

    while (tries < MAX_TRIES)
    {
        r_code = fbapi_init (channel, PROTOCOL_FF, &ffboard_init);
        if (r_code == E_OK)
        {
            break;
        }
        tries++;
    }

    if (r_code == E_OK)
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Successful initialization of API interface after %d tries\n",
            tries);
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Initialization of API interface fails with error code %d\n",
            r_code);
        return r_reason(r_code);
    }

// TODO: handle add_detail (e.g. START_ERR_FW_TYPE or START_ERR_FW_INVALID)
    r_code = fbapi_start (channel, &add_detail);
    if(E_OK != r_code)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Initialization of API interface fails with error code %d",
            r_code);
        return r_reason(r_code);
    }

    release_fbk_read_lock();
    /*
     * Configure the System mangement configuration of the
     * FBK board.
     */
    strcpy((char *)&sm_req.host_pd_tag,
            (char *)&host_config->sm_config.host_pd_tag);
    strcpy((char *)&sm_req.vendor_name,
            (char *)&host_config->sm_config.vendor_name);
    strcpy((char *)&sm_req.model_name,
            (char *)&host_config->sm_config.model_name);
    strcpy((char *)&sm_req.revision_string,
            (char *)&host_config->sm_config.revision_string);

    r_code = send_wait_receive_cmd(env_info, HNDL_SM_CONF_REQ, NULL, &sm_req);
    if(CM_SUCCESS != r_code) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in SM config request");
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"SM Config positive confirmation");
    }
    else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"SM set Configuration: Error Class code = %d\n",
            sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM set Configuration: Additional Detail = %d\n",
            sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Config confirmation (-)");

        return CS_SM_ERR(sm_err->rc);
    }

    /*
     * Configure the DLL configuration of the device
     */

    (void *)memcpy((char *)&dll_req.dll_config, 
            (char *)&host_config->dll_config,
             sizeof(T_DLL_CONFIG_REQ ));

    r_code = send_wait_receive_cmd(env_info, HNDL_DLL_CONF_REQ, &dll_req, NULL);

    if(CM_SUCCESS != r_code) {
        return (r_code);
    }

    if (POS == get_service_result())
    {
       /*
        if(NULL != nma_info)
        {
            (void *)memcpy(nma_info, (CM_NMA_INFO *)global_appl_data_buf,
                sizeof(T_NMA_INFO));
        }
        */
        LOGC_TRACE(CPM_FF_DDSERVICE,"DLL Config positive confirmation");
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
	    log_error(error);
        app_info->env_returns.response_code = error->class_code;
        LOGC_TRACE(CPM_FF_DDSERVICE,"DLL Config con (-)");
        return CS_NMA_ERR(error->class_code);
    }

    /* Initialization of VCRL request of FBK board */
    r_code = send_wait_receive_cmd(env_info, HNDL_VCRL_INIT_REQ, NULL, NULL);

    if(CM_SUCCESS != r_code) {
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"VCRL Init con (+)");
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
	    log_error(error);
        app_info->env_returns.response_code = error->class_code;
        LOGC_TRACE(CPM_FF_DDSERVICE,"DLL Config con (-)");
        return CS_NMA_ERR(error->class_code);
    }

    r_code = send_wait_receive_cmd(env_info, HNDL_VCRL_TERM_REQ, NULL, NULL);

    if(CM_SUCCESS != r_code) {
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"VCRL Terminate con (+)");
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
        log_error(error);
        app_info->env_returns.response_code = error->class_code;
        LOGC_TRACE(CPM_FF_DDSERVICE,"DLL Config con (-)");
        return CS_NMA_ERR(error->class_code);
    }

    if (DEVACC_APPL_STATE_END == get_device_admin_state())
    {
        return CM_COMM_ERR;
    }
    else
    {
        set_device_admin_state(DEVACC_APPL_STATE_READY);
        LOGC_INFO(CPM_FF_DDSERVICE,"Stack initialized");
    }

    return r_code;
}

int softing_set_appl_state(ENV_INFO *env_info, int *curr_state, int nw_state)
{
	int state = 0;
	state = get_device_admin_appl_state();

	if(((APPL_BUSY == nw_state) && (APPL_NORMAL == state)) || (APPL_NORMAL == nw_state))
	{
		set_device_admin_appl_state(nw_state);
	}

	else
	{
		*curr_state = state;
		return CM_DPC_BUSY;
	}

	return CM_SUCCESS;
}

int softing_get_actual_index(unsigned short *index, int *si, unsigned short *cr)
{
	get_fbap_current_index(index, si, cr);
	if((*index == 0) || (*si == 0) || (*cr == 0))
		return CM_COMM_ERR;

	return CM_SUCCESS;
}

int softing_read_abs_index(ENV_INFO *env_info, unsigned short index, int si,
		unsigned short cr, void *val)
{
	int 								r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    FBAP_NMA_REQ                        fbap_nma_req;
    int									value_size;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    fbap_nma_req.index = index;
    fbap_nma_req.sub_index = si;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }
    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
        log_error(error);
        app_info->env_returns.response_code = error->class_code;

        r_code = CS_FMS_ERR(error->class_code);
        goto error_code;
    }

    else if(POS == get_service_result())
    {
    	unsigned int data;
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        (void) memcpy((char *)&data, (char *)data_ptr, value_size);
        data = convert_to_ltl_endian(data);
        (void) memcpy((char *)val, (char *)&data, value_size);

        r_code = CM_SUCCESS;
    }

error_code:
	return r_code;
}
