/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file cff_parser.c
    File contains the CFF file data parsing code.

    Contains the code to get the Deive informations from the capability
    file and code to populate the device tree based on the device
    contents and data in the .CFF file.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cff_parse.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! @fn void *accept_function(void *arg)
    @brief Thread function to listen for the client socket connection.
*/
void get_defaults(FILE *data_file)
{
	int count = 0;
	while (!feof(data_file)) {

		/*
		 *	Read in one line of the data file.
		 */

		(void) fgets(file_line, 256, data_file);

		if((strstr(file_line, "[VFD 2 Resource Block Defaults]")) != NULL){
		while (count<3){
			(void) fgets(file_line, 256, data_file);
			if((strstr(file_line, "MANUFAC_ID")) != NULL){
			
				pos = strchr(file_line,'=');
				manufacturer = strtoul (pos+1,NULL,0);
				count++;

			}else if((strstr(file_line, "DEV_TYPE")) != NULL){
					
				pos = strchr(file_line,'=');
				dev_type = strtoul (pos+1,NULL,0);
				count++;

			}/*else if((strstr(file_line, "DD_REV")) != NULL){
					
					pos = strchr(file_line,'=');
					blk_entry-> = strtoul (pos+1,NULL,0);

			}*/else if((strstr(file_line, "DEV_REV")) != NULL){
					
				pos = strchr(file_line,'=');
				dev_rev = strtoul (pos+1,NULL,0);
				count++;
				}
			}
			break;
		}
	}
	fseek(data_file, 0,SEEK_SET);
	return;
}
/***********************************************************************
 *
 *	Name:  store_find_confirm_offline
 *
 *	ShortDesc:  Store a block entry into the Find Confirm Table.
 *
 *	Description:
 *		The store_find_confirm function takes a block entry and Find
 *		Confirm Table and stores applicable information from the block
 *		entry into the Find Confirm Table.
 *
 *	Inputs:
 *		blk_entry - the block entry whose information is to be stored.
 *		find_confirm_tbl - the Find Confirm Table that will hold the
 *						   block entry's information.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *		FFDS_MEMORY_ERR.
 *
 *	Author:
 *		LNT
 *
 **********************************************************************/

int
store_find_confirm_offline(BLK_ENTRY *blk_entry, FIND_CNF_TBL *find_cnf_tbl)
{

	/*
	 *	Increase the Find Confirm Table by one element.
	 */

	find_cnf_tbl->find_cnf_list = (FIND_CNF_A *) realloc(
			(char *)find_cnf_tbl->find_cnf_list, (unsigned int)
			(sizeof(FIND_CNF_A) * (find_cnf_tbl->count + 1)));
	if (!find_cnf_tbl->find_cnf_list) {
		return(FFDS_MEMORY_ERR);
	}
	find_cnf_tbl->blk_tag_list = (BLK_TAG *) realloc(
			(char *)find_cnf_tbl->blk_tag_list, (unsigned int)
			(sizeof(BLK_TAG) * (find_cnf_tbl->count + 1)));
	if (!find_cnf_tbl->blk_tag_list) {
		return(FFDS_MEMORY_ERR);
	}
	find_cnf_tbl->count ++;

	/*
	 *	Store the block tag.
	 */

	(void)strcpy(find_cnf_tbl->blk_tag_list[find_cnf_tbl->count - 1],
			blk_entry->blk_tag);

	/*
	 *	Store the applicable information from the block entry into
	 *	the Find Confirm.
	 */

	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			op_index =
			(OBJECT_INDEX)blk_entry->op_index;
	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			station_address =
			(UINT16)blk_entry->station_address;
	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			dd_device_id.ddid_manufacturer =
			(UINT32)blk_entry->manufacturer;
	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			dd_device_id.ddid_device_type =
			(UINT16)blk_entry->dev_type;
	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			dd_device_id.ddid_device_rev =
			(UINT8)blk_entry->dev_rev;
	find_cnf_tbl->
			find_cnf_list[find_cnf_tbl->count - 1].
			network_type = NT_OFF_LINE;

	return(FFDS_SUCCESS);
}

int get_data(BLK_ENTRY *blk_entry, int netwk_num, DEV_DATA_TBL *dev_data_tbl)
{
	int				 r_code;
	int				 count = 0;
	BLK_ENTRY_TBL	*blk_entry_tbl;
	FIND_CNF_TBL	*find_cnf_tbl;


	blk_entry_tbl = &netwk_tbl.list[netwk_num].blk_entry_tbl;
	find_cnf_tbl = &netwk_tbl.list[netwk_num].find_cnf_tbl;

	while(!feof(data_file) && count < 4)
	{
		(void) fgets(file_line, 256, data_file);
		if (((file_line[0] == '/') && (file_line[1] == '/')) || (file_line[0] == '\n')) {

			/*
			 *	Go on to read the next line of the data file.
			 */

			continue;

		}else{
			if((strstr(file_line, "Block_Index") || (strstr(file_line, "BLOCK_INDEX"))) != 0 ){

				pos = strchr(file_line,'=');
				blk_entry->op_index = strtoul (pos+1,NULL,0);
				count++;

			}else if((strstr(file_line, "DD_Item") || (strstr(file_line, "DD_ITEM"))) != 0 ){
				
				pos = strchr(file_line,'=');
				blk_entry->dd_item_id = strtoul (pos+1,NULL,0);
				count++;

			}else if((strstr(file_line, "NUM_OF_PARMS")||(strstr(file_line, "Num_Of_Parms"))) != 0 ){
				
				pos = strchr(file_line,'=');
				blk_entry->param_count = strtoul (pos+1,NULL,0);
				count++;
            }
			
			else if((strstr(file_line, "PROFILE")||(strstr(file_line, "Profile"))) != 0 ){
				if((strstr(file_line, "REVISION")||(strstr(file_line, "Revision"))) == 0 ){
				    pos = strchr(file_line,'=');
				    if(blk_entry->class_type == BC_UNKNOWN)
				    	blk_entry->class_type = get_class_vlaue(strtoul (pos+1,NULL,0));
				    count++;
                }
			}
    
		}
	}
	if(count != 4){
                LOGC_ERROR(CPM_FF_DDSERVICE,"Cff parser variable missing in file %s \n",cff_file_line);
		return(CM_BAD_FILE_OPEN);
	}
	blk_entry->manufacturer	=	manufacturer;
	blk_entry->dev_type	=	dev_type;
	blk_entry->dev_rev	=	dev_rev;
	/*
	 *	Store the block entry data into the Block Entry Table.
	 */

	r_code = store_block_entry(blk_entry, blk_entry_tbl);
	if (r_code != FFDS_SUCCESS) {
		fclose(data_file);
		return(r_code);
	}

	/*
	 *	Store the block entry data into the Find Confirm Table.
	 */

	r_code = store_find_confirm_offline(blk_entry, find_cnf_tbl);
	if (r_code != FFDS_SUCCESS) {
		fclose(data_file);
		return(r_code);
	}

	/*
	 *	Store the block entry data into the Device Data Table.
	 */

	r_code = store_device_data(blk_entry, dev_data_tbl, NT_OFF_LINE);
	if (r_code != FFDS_SUCCESS) {
		fclose(data_file);
		return(r_code);
	}
	return(FFDS_SUCCESS);
}

int scan_cff_file(BLK_ENTRY *blk_entry, int config_file_id)
{
	int					i=0, len=0, count=0;
	char				*str;
	int					 netwk_num;
	int					r_code;
	DEV_DATA_TBL	*dev_data_tbl;
	int funct_blk_count  = 1;
	int trans_blk_count = 1;

	data_file = infileptr;
    strcpy(cff_file_line, infilename);
	if (!data_file) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Cff parser invalid file %s \n",cff_file_line);
		fprintf(errfileptr, "Cff parser invalid file %s \n",cff_file_line);
		return(CM_BAD_FILE_OPEN);
	}

	get_defaults(data_file);

	r_code = cfg_file_id_to_network_num(config_file_id, &netwk_num);
	if (r_code != FFDS_SUCCESS) {
		fclose(data_file);
		return(r_code);
	}
    
	dev_data_tbl = &netwk_tbl.list[netwk_num].dev_data_tbl;
	dev_data_tbl->list = (DEV_DATA *) calloc(1, sizeof(DEV_DATA));
	while (!feof(data_file)) {

		/*
		 *	Read in one line of the data file.
		 */

		(void) fgets(file_line, 256, data_file);

		/*
		 *	Check for comments or blank lines.
		 */

		if ((file_line[0] == '[')) {

			if((strstr(file_line, "[VFD 2 Resource Block]")) != NULL){
			
				(void)sprintf(blk_entry->blk_tag, "%s", "RESOURCE");
				blk_entry->class_type = BC_PHYSICAL;
				r_code = get_data(blk_entry, netwk_num, dev_data_tbl);
				if (r_code != FFDS_SUCCESS) {
					fclose(data_file);
					return(r_code);
				}
			}else if(((strstr(file_line,"[VFD")) || (strstr(file_line,"[vfd"))) != 0 ){

				pos = strchr(file_line, ']'); // Check for delimiter
				len = pos-&file_line[0];
				str = malloc(len+1);
				memcpy(str, file_line, len+1);
				for(i=0,count = 0;i<=len;i++) //finding the numbers of words in the string
				{  
				   if(str[i]==' ') 
					  count++; 
				}
                free(str);
				/* Checking for string which contains "[VFD 2 (Function or Transducer) Block No:] */
				if(count == 4){  
					if(strstr(file_line, "Function Block") != NULL){

						
						(void)sprintf(blk_entry->blk_tag, "%s%d", "FUNCTION_", 
							funct_blk_count++);
						blk_entry->class_type = BC_UNKNOWN;
						r_code = get_data(blk_entry, netwk_num, dev_data_tbl);
						if (r_code != FFDS_SUCCESS) {
							fclose(data_file);
							return(r_code);
						}

					}else if(strstr(file_line, "Transducer Block") != NULL){

						(void)sprintf(blk_entry->blk_tag, "%s%d", "TRANSDUCER_", 
							trans_blk_count++);
						blk_entry->class_type = BC_TRANSDUCER;
						r_code = get_data(blk_entry, netwk_num, dev_data_tbl);
						if (r_code != FFDS_SUCCESS) {
							fclose(data_file);
							return(r_code);
						}
					}
				}
			}
		}else{

			/*
			*	Go on to read the next line of the data file.
			*/

			continue;
				
		}
	}
	return(FFDS_SUCCESS);
}

int
offline_init(NETWORK_HANDLE network_handle)
{
	int			r_code = CM_SUCCESS ;
	NET_TYPE	net_type ;
	int			config_file_id;
	BLK_ENTRY	*blk_entry;

	blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
	if (!blk_entry) {
			free(blk_entry);
			return(FFDS_MEMORY_ERR);
	}

	r_code =  get_nt_net_type(network_handle, &net_type) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}
	r_code =  get_nt_cfg_file_id(network_handle, &config_file_id) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	/*
	 *	Create a new network.
	 */

	r_code = new_network(config_file_id);
	if (r_code != FFDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Initialize all of the tables used in the network.
	 */

	r_code = initialize_tables(netwk_tbl.count - 1);
	if (r_code != FFDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Read the data file for the particular network.
	 */

	r_code = scan_cff_file(blk_entry, config_file_id);
	if (r_code != FFDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Build the network of simulated Type A devices.
	 */

	r_code = build_devices(netwk_tbl.count -1);
	if (r_code != FFDS_SUCCESS) {
		return(r_code);
	}

	return(FFDS_SUCCESS);
}

#ifdef __cplusplus
}
#endif
