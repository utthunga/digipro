/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file host_server.c
    Server module to receive data from the FF host client Device.

    This file contains all functions pertaining server host code
    management. This module creates the socket

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"
#include "device_access.h"
#include "cm_lib.h"
#include "os_helper.h"
#include "devsim.h"
#include "dds_comm.h"
#include "ddi_lib.h"
#include "int_diff.h"

#define SOCKET_LISTEN   1
#define SOCKET_ACCEPT   2

/*! @fn int get_socket_fd(int type, int *socket)
    @brief Get the socket file descriptor based on type or request
    either created fd or connection fd.
    @param type The socket type
    @return socket The value of socket fd.
*/
static int get_socket_fd(int type, int *socket)
{
	int r_code;
    int fd = 0;
    char *shm = NULL;

    if(SOCKET_LISTEN == type)
    {
        r_code = read_from_shm(shm, LISTEN_FD_DATA_TYPE, &fd);
        if(r_code != CM_SUCCESS)
        {
            return r_code;
        }
	    *socket = fd;
    }
    else if(SOCKET_ACCEPT == type)
    {
        r_code = read_from_shm(shm, ACCEPT_FD_DATA_TYPE, &fd);
        if(r_code != CM_SUCCESS)
        {
            return r_code;
        }
        *socket = fd;
    }

	return CM_SUCCESS;
}

/*! @fn int is_socket_valid(int socket)
    @brief Validate the socket file descriptor
    @param socket The scoket fd to validate.
*/
static int is_socket_valid(int socket)
{
	if(socket > 0)
	{
		return VALID_SOCKET;
	}
	else
		return INVALID_SOCKET;
}

/*! @fn void *accept_function(void *arg)
    @brief Thread function to listen for the client socket connection.
*/
static void *accept_function(void *arg)
{
	int conn_fd, listen_fd, socket_fd;
	socklen_t client_len;
	struct sockaddr_in client_addr;
    int *temp = (int *)arg;
    int r_code;
    char *shm = NULL;

	listen_fd = (*temp);

    r_code = get_socket_fd(SOCKET_LISTEN, &socket_fd);
	if(r_code != CM_SUCCESS)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to read shared memory");
	}
	listen_fd = socket_fd;

    client_len = sizeof(client_addr);
	conn_fd = accept (listen_fd, (struct sockaddr *) &client_addr,
			&client_len);

	if(conn_fd < 0)
	{
		perror("Error is");
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to accept socket %d", errno);
		close(listen_fd);
	}

	else
	{
		write_to_shm(shm, ACCEPT_FD_DATA_TYPE, &conn_fd);
	}
	usleep(500);
	return NULL;
}

/*! @fn int create_server_socket(int *socket_fd)
    @brief Thread function to listen for the client socket connection.
*/
int create_server_socket(char *shm, int *socket_fd)
{
	int r_code;
	int listen_fd;
	int option = 1;
	struct sockaddr_in server_addr;

	/*Create TCP socket*/
	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(listen_fd < 0)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create socket");
		return CM_BAD_SOCKET;
	}

	(void *)memset(&server_addr, 0, sizeof(server_addr));

	/*Configure settings in address struct*/
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT_NUMBER);
	server_addr.sin_addr.s_addr = inet_addr(ADDRESS);

	r_code = setsockopt(listen_fd, SOL_SOCKET,
			(SO_REUSEPORT | SO_REUSEADDR),
			(char*)&option,sizeof(option));
	if(r_code < 0)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to set the socket option");
		close(listen_fd);
		return CM_BAD_SOCKET;
	}

	/*Bind socket with address struct*/
	r_code = bind(listen_fd,
			(struct sockaddr *) &server_addr, sizeof(server_addr));
	if(r_code < 0)
	{
		perror("Error is");
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to bind with the udp socket %d", errno);
		close(listen_fd);
		return CM_BAD_BIND_SOCKET;
	}

	r_code = listen (listen_fd, LISTENQ);
	if(r_code < 0)
	{
		perror("Error is");
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in listening the socket %d", errno);
		close(listen_fd);
		return CM_BAD_BIND_SOCKET;
	}

        LOGC_DEBUG(CPM_FF_DDSERVICE,"Waiting to start the client application");

	write_to_shm(shm, LISTEN_FD_DATA_TYPE, &listen_fd);

	r_code = create_thread (10/*&accept_function*/, (void *)&listen_fd);
	if(r_code != CM_SUCCESS)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create socket accept thread");
		return r_code;
	}

	return CM_SUCCESS;
}

int send_data_to_client(unsigned short code, T_DEVICE_ERROR *dev_err,
                        T_FIELDBUS_SERVICE_DESCR *devacc_sdb, unsigned char *data)
{
	/*Send message to client*/
	int socket;
	SOCKET_DATA  sock_data;
	int r_code;

	sock_data.code = code;

    sock_data.dev_err.reason_code = dev_err->reason_code;
    sock_data.dev_err.host_state = dev_err->host_state;
    sock_data.dev_err.service = dev_err->service;
    sock_data.dev_err.layer = dev_err->layer;
    sock_data.dev_err.primitive = dev_err->primitive;
    sock_data.dev_err.result = dev_err->result;

	sock_data.devacc_sdb.comm_ref = devacc_sdb->comm_ref;
	sock_data.devacc_sdb.invoke_id = devacc_sdb->invoke_id;
	sock_data.devacc_sdb.layer = devacc_sdb->layer;
	sock_data.devacc_sdb.primitive = devacc_sdb->primitive;
	sock_data.devacc_sdb.result = devacc_sdb->result;
	sock_data.devacc_sdb.service = devacc_sdb->service;

	(void *)memcpy((char *)sock_data.data, (char *)data, MAX_FF_DATA_LEN);

	r_code = get_socket_fd(SOCKET_ACCEPT, &socket);
	if(r_code != CM_SUCCESS)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to read shared memory");
	}

	if(is_socket_valid(socket))
	{
		r_code = send(socket, sock_data.buffer, MAX_RX_SOCKET_BUFF_SIZE, 0);
		if(r_code < 0)
		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to send the client data over socket %d", errno);
		}
	}

	return CM_SUCCESS;

}

void *handle_socket_data(void *arg)
{
	char 	buffer[MAX_RX_SOCKET_BUFF_SIZE];
	int 	n_bytes;
	int 	socket = 0;
	SOCKET_DATA  sock_data;
	T_FIELDBUS_SERVICE_DESCR devacc_sdb;
	T_DEVICE_ERROR dev_err = {0};
	int r_code, appl_state;
	unsigned char data[MAX_FF_DATA_LEN];
    int listen_fd = 0;
    char *shm = NULL;
    unsigned int condition;
    FBAP_NMA_REQ fbap_nma_req = {0};
    SM_REQ sm_req = {0};
    ENV_INFO	env_info = {0};
    APP_INFO	app_info = {0};

    env_info.app_info = &app_info;

	while(1){

		(void *)memset((char *)&fbap_nma_req, 0, sizeof(FBAP_NMA_REQ));
		(void *)memset((char *)&sm_req, 0, sizeof(SM_REQ));
		(void *)memset((char *)&devacc_sdb, 0, sizeof(T_FIELDBUS_SERVICE_DESCR));
		(void *)memset((char *)&dev_err, 0, sizeof(T_DEVICE_ERROR));
		condition = 0;

		r_code = get_socket_fd(SOCKET_ACCEPT, &socket);
		if(r_code != CM_SUCCESS)
		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to read shared memory");
		}

		if(is_socket_valid(socket))
		{
            (void *)memset(buffer, 0, MAX_RX_SOCKET_BUFF_SIZE);
            (void *)memset(data, 0, MAX_FF_DATA_LEN);
			/* Try to receive any incoming TCP socket. Address and port of
			  requesting client will be stored on serverStorage variable */
			n_bytes = recv(socket, buffer, MAX_RX_SOCKET_BUFF_SIZE, 0);
            if(n_bytes == 0)
            {
                socket = 0;
                printf("Client closed\n");
		        write_to_shm(shm, ACCEPT_FD_DATA_TYPE, &socket);
	            r_code = create_thread (10/*&accept_function*/, (void *)&listen_fd);
            	if(r_code != CM_SUCCESS)
	            {
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create socket accept thread");
	            }

            }
            else if(n_bytes < 0)
			{
                                LOGC_ERROR(CPM_FF_DDSERVICE,"Error in receiving data over socket");
			}
			else
			{
				(void *)memcpy(sock_data.buffer, buffer, MAX_RX_SOCKET_BUFF_SIZE);

				(void *)memcpy((char *)data, (char *)sock_data.data, MAX_FF_DATA_LEN);

				(void *)memcpy((char *)&fbap_nma_req, (char *)&sock_data.comm_req.fbap_nma_req, sizeof(FBAP_NMA_REQ));
				(void *)memcpy((char *)&sm_req, (char *)&sock_data.comm_req.sm_req, sizeof(SM_REQ));
				condition = sock_data.comm_req.condition;

				appl_state = get_device_admin_appl_state();
				if(APPL_BUSY == appl_state)
				{
					dev_err.reason_code = CM_DPC_BUSY;
	                r_code = send_data_to_client((unsigned short)CM_DPC_BUSY, &dev_err, &devacc_sdb, data);
	                if(CM_SUCCESS != r_code)
	                {
                            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to send data to client application");
	                }
				}

				else
				{
					r_code = send_wait_receive_cmd(&env_info, condition, &fbap_nma_req, &sm_req);

					get_device_error(&dev_err);
					devacc_sdb.comm_ref = get_service_comm_ref();
					devacc_sdb.layer = get_service_layer();
					devacc_sdb.primitive = get_service_primitive();
					devacc_sdb.result = get_service_result();
					devacc_sdb.service = get_service_ref();

					(void *)memcpy((char *)data, (char *)global_res_buf, MAX_FF_DATA_LEN);
					r_code = send_data_to_client((unsigned short)r_code, &dev_err, &devacc_sdb, data);
					if(CM_SUCCESS != r_code)
					{
                                                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to send data to client application");
					}
				}
			}
		}
		else
			usleep(500);
	}
	return NULL;
}

int create_thread (int test/*void *(*function)(void *argc)*/, void *arg)
{
    int r_code = CM_SUCCESS;
    int listen_fd = 4;

    pthread_t handle;
    pthread_attr_t thread_attr;

    /*
        Create threads in detached state (i.e., not joinable).

        The use model in DDS Communication manager is to start a thread
        and have it run independently (detached) from the thread that
        started it -- i.e., the calling thread does not wait on the new
        thread to finish execution before doing something else.

        See:
          * http://www.cs.cf.ac.uk/Dave/C/node30.html
    */

    if (0 == pthread_attr_init(&thread_attr))
    {
        if (0 != pthread_attr_setdetachstate(
                &thread_attr,
                PTHREAD_CREATE_DETACHED
            )
        )
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; detached state config %d",
                    errno);
            return (CM_BAD_THREAD_HANDLE);
        }
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; attribute initialization errno:%d",
                errno);
        return (CM_BAD_THREAD_HANDLE);
    }

    /*
       Create and start the thread.
    */
    r_code = pthread_create(
    		&handle,
    		&thread_attr,
    		&accept_function,//function,
    		(void *)&listen_fd);//arg);

    if (CM_SUCCESS != r_code)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE, "Failed thread creation with status: %d",errno );
        r_code = CM_BAD_THREAD_HANDLE;
    }

    usleep(500);
    pthread_attr_destroy(&thread_attr);

    return r_code;
}

