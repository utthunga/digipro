/*****************************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Handling Object Description data from the transmitter.

    This file contains all functions pertaining to the Object description
    of all Object Types of the data received from the transmitter.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "devsim.h"
#include "pc_loc.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "obj_desc.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "pc_lib.h"

static int null_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    /*
     *    Nothing to Implement as no data with Null object
     */
    return CM_SUCCESS;
}
static int od_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    /*
     *  Nothing to implement
     */
    return CM_SUCCESS;
}

static int domain_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
        T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT *domain_desc = 
            (T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT *)obj_desc;

        od_type_object->access.groups = domain_desc->AccessGroup;
        od_type_object->access.password = domain_desc->Password;
        od_type_object->access.rights = domain_desc->AccessRights;
        od_type_object->specific->domain.size = domain_desc->MaxOctets;
        od_type_object->specific->domain.local_address = 
                                          domain_desc->LocalAddres;
        od_type_object->specific->domain.state = 
                                          domain_desc->DomainState;
        od_type_object->specific->domain.upload_state = 
                                          domain_desc->UploadState;
        od_type_object->specific->domain.usage = 
                                          domain_desc->Extension[0];
        return CM_SUCCESS;
}

static int invocation_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_PI_DESCRIPTION_LONG_FORMAT *pi_desc =
        (T_H1_PI_DESCRIPTION_LONG_FORMAT *) obj_desc;

    od_type_object->access.groups = pi_desc->AccessGroup;
    od_type_object->access.password = pi_desc->Password;
    od_type_object->access.rights = pi_desc->AccessRights;
    od_type_object->specific->program_invocation.deleteable_flag = 
        pi_desc->Deletable;
    od_type_object->specific->program_invocation.domain_count =
        pi_desc->NumberOfDomains;
    od_type_object->specific->program_invocation.reuseable_flag =
        pi_desc->Reusable;
    od_type_object->specific->program_invocation.state =
        pi_desc->PIState;
    return CM_SUCCESS;
}

static int event_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_EVENT_DESCRIPTION_LONG_FORMAT *ev_desc =
        (T_H1_EVENT_DESCRIPTION_LONG_FORMAT *)obj_desc;

    od_type_object->access.groups = ev_desc->AccessGroup;
    od_type_object->access.password = ev_desc->Password;
    od_type_object->access.rights = ev_desc->AccessRights;
    od_type_object->specific->event.data = 
        ev_desc->IndexEventData;
    od_type_object->specific->event.enabled_flag =
        ev_desc->Enabled;
    od_type_object->specific->event.size =
        ev_desc->Length;
   
    return CM_SUCCESS;
}

static int type_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
   T_H1_OD_DATA_TYPE_DESCRIPTION *type_desc = 
       (T_H1_OD_DATA_TYPE_DESCRIPTION *) obj_desc;
   
   od_type_object->specific->data_type.symbol_size =  
        type_desc->SymbolicNameLength;

    return CM_SUCCESS;
}

static int type_struct_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    int i; 
    T_H1_OD_DATA_TYPE_STRUCTURE *type_struct_desc = 
        (T_H1_OD_DATA_TYPE_STRUCTURE *)obj_desc;

    T_H1_DATA_TYPE_ELEMENT *temp_list;
    OBJECT_INDEX *temp_type_list;
    UINT8 *temp_size_list;

    temp_type_list = 
        od_type_object->specific->data_type_structure.type_list; 
    temp_size_list = 
        od_type_object->specific->data_type_structure.size_list;
    temp_list = type_struct_desc->list;

    if(BIT_STRING == temp_list->DataTypeIndex)
    {
        bit_rev(*value, *value_size);
    }
    for(i = 0; i < type_struct_desc->NumberOfElements; i++)
    {
        if(BIT_STRING == temp_list->DataTypeIndex)
        {
            switch(temp_list->Length)
            {
                case 1:
                    temp_list->DataTypeIndex = UNSIGNED8;
                    break;
                case 2:
                    temp_list->DataTypeIndex = UNSIGNED16;
                    break;
                case 4:
                    temp_list->DataTypeIndex = UNSIGNED32;
                    break;
                default:
                    LOGC_ERROR(CPM_FF_DDSERVICE, "WARNING: WRONG SIZE OF THE DATA TYPE");
                    break;
            }
        }
        *temp_type_list = temp_list->DataTypeIndex;
        *temp_size_list = temp_list->Length;
        temp_list++;
        temp_size_list++;
        temp_type_list++;
    }

    return CM_SUCCESS;
}

static int simple_var_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT *simple_var_desc = 
        (T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT *)obj_desc;

    od_type_object->access.groups = simple_var_desc->AccessGroup;
    od_type_object->access.password = simple_var_desc->Password;
    od_type_object->access.rights = simple_var_desc->AccessRights;
  
    if(BIT_STRING == simple_var_desc->DataTypeIndex)
    {
        bit_rev(*value,*value_size);
        switch(simple_var_desc->Length)
        {
            case 1:
                simple_var_desc->DataTypeIndex = UNSIGNED8;
                break;
            case 2:
                simple_var_desc->DataTypeIndex = UNSIGNED16;
                break;
            case 4:
                simple_var_desc->DataTypeIndex = UNSIGNED32;
                break;
            default:
                LOGC_ERROR(CPM_FF_DDSERVICE, "WARNING: WRONG SIZE OF THE DATA TYPE");
                break;
        }
    }
    od_type_object->specific->simple_variable.type = 
        simple_var_desc->DataTypeIndex;
    od_type_object->specific->simple_variable.size = 
        simple_var_desc->Length;
        
    return CM_SUCCESS;
}

static int array_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_ARRAY_DESCRIPTION_LONG_FORMAT  *array_desc = 
        (T_H1_ARRAY_DESCRIPTION_LONG_FORMAT *)obj_desc;

    od_type_object->access.groups = array_desc->AccessGroup;
    od_type_object->access.password = array_desc->Password;
    od_type_object->access.rights = array_desc->AccessRights;
    od_type_object->specific->array.elem_count = 
        array_desc->NumberOfElements;
  
    if(BIT_STRING == array_desc->DataTypeIndex)
    {
        bit_rev(*value, *value_size);
        switch(array_desc->Length)
        {
            case 1:
                array_desc->DataTypeIndex = UNSIGNED8;
                break;
            case 2:
                array_desc->DataTypeIndex = UNSIGNED16;
                break;
            case 4:
                array_desc->DataTypeIndex = UNSIGNED32;
                break;
            default:
                LOGC_ERROR(CPM_FF_DDSERVICE, "WARNING: WRONG SIZE OF THE DATA TYPE");
                break;
        }
    }
    od_type_object->specific->array.type = array_desc->DataTypeIndex;
    od_type_object->specific->array.size = array_desc->Length;
        
    return CM_SUCCESS;
}

static int record_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_RECORD_DESCRIPTION_LONG_FORMAT *record_desc = 
        (T_H1_RECORD_DESCRIPTION_LONG_FORMAT *)obj_desc;

    od_type_object->access.groups = record_desc->AccessGroup;
    od_type_object->access.password = record_desc->Password;
    od_type_object->access.rights = record_desc->AccessRights;
    od_type_object->specific->record.type   =   record_desc->DataTypeIndex;

    return CM_SUCCESS;
}

static int var_list_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT *var_list_desc = 
        (T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT *)obj_desc;

    od_type_object->access.groups = var_list_desc->AccessGroup;
    od_type_object->access.password = var_list_desc->Password;
    od_type_object->access.rights = var_list_desc->AccessRights;
    od_type_object->specific->variable_list.deleteable_flag =
        var_list_desc->Deletable;
    od_type_object->specific->variable_list.elem_count =
        var_list_desc->NumberOfElements;

    return CM_SUCCESS;
}

static int var_obj_data_copy(OBJECT *od_type_object, void *obj_desc,
        UINT8 **value, int *value_size)
{
    /*
     * Not required
     */
    return CM_SUCCESS;
}
static int all_obj_data_copy(OBJECT *od_type_object, void *obj_desc, 
        UINT8 **value, int *value_size)
{
    /*
     * Nothing to implement
     */
    return CM_SUCCESS;
}

static int (*object_data_copy[MAX_OBJECT_DESC])(OBJECT *, 
        void *, UINT8 **, int *) = {
    null_obj_data_copy,
    od_obj_data_copy,
    domain_obj_data_copy,
    invocation_obj_data_copy,
    event_obj_data_copy,
    type_obj_data_copy,
    type_struct_obj_data_copy,
    simple_var_obj_data_copy,
    array_obj_data_copy,
    record_obj_data_copy,
    var_list_obj_data_copy,
    var_obj_data_copy,
    all_obj_data_copy
};

int obj_domain_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                              ext_len;
    OBJECT                              *od_type_object;
    int                                 r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT *domain_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    ext_len = global_res_buf[19 + fbap_od_desc.name_length];
    domain_desc = (T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT *) calloc
                 (1, sizeof(T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT));
    if(NULL == domain_desc)
    {
        r_code = CM_NO_MEMORY;
        goto domain_desc_mem_err;
    }
    
    (void) memcpy((char *)domain_desc, 
                  (char *)(global_res_buf + 3),
                  sizeof(T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT));
    
    domain_desc->ObjectIndex = convert_to_ltl_endian(
                                 domain_desc->ObjectIndex);
    domain_desc->MaxOctets = convert_to_ltl_endian(
                                 domain_desc->MaxOctets);
    domain_desc->AccessRights = convert_to_ltl_endian(
                                 domain_desc->AccessRights);
    domain_desc->LocalAddres = convert_to_ltl_endian(
                                 domain_desc->LocalAddres);
                    
    domain_desc->Name = (char *)calloc( 1, 
                            (fbap_od_desc.name_length) ? 
                            (fbap_od_desc.name_length * sizeof(char)) : 
                            1);
    if(NULL == domain_desc->Name)
    {
	    r_code = CM_NO_MEMORY;
        goto name_mem_error;
    }                
    domain_desc->Extension = (USIGN8 *)calloc(1, 
                               ext_len ? 
                               (sizeof(USIGN8) * ext_len) : 
                               1);
    if(NULL == domain_desc->Extension)
    {
	    r_code = CM_NO_MEMORY;
        goto ext_mem_err;
    }

    (void) memcpy(
        (char *)domain_desc->Name, 
        (char *)(global_res_buf + 19),
        fbap_od_desc.name_length );

    (void) memcpy(
        (USIGN8 *)domain_desc->Extension, 
        (USIGN8 *)(global_res_buf + 20 + fbap_od_desc.name_length),
        ext_len );

    LOGC_TRACE(CPM_FF_DDSERVICE,
       "Object Index    =   %x\nObject Code =   %x\n"
       "Octets  =   %x\nPassword    =   %x\n"
       "Access Group    =   %x\nAccess Rights   =   %d\n"
       "Local Address   =   %x\nDomain State    =   %d\n"
       "Upload State    =   %d\nCounter =   %c\n"
                           "Extension Length    =   %d\nUsage   =   %d",
    domain_desc->ObjectIndex, domain_desc->ObjectCode,
    domain_desc->MaxOctets,domain_desc->Password,
    domain_desc->AccessGroup,domain_desc->AccessRights,
    domain_desc->LocalAddres,domain_desc->DomainState,
    domain_desc->UploadState,domain_desc->Counter,
    ext_len,domain_desc->Extension[0]);


    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   DOMAIN_OBJECT;

        r_code = (*object_data_copy[DOMAIN_OBJECT])(od_type_object, domain_desc,
                    value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto od_specific_mem_err;
        }

        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto obj_copy_err;
        }
    }
    
obj_copy_err:
    free(od_type_object->specific);

od_specific_mem_err:
    free(od_type_object);

od_obj_mem_err:
send_err:
    free(domain_desc->Extension);

ext_mem_err:
    free(domain_desc->Name);

name_mem_error:
    free(domain_desc);
 
domain_desc_mem_err:
    return r_code;    
 
}


int obj_invocation_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                              ext_len;
    USIGN8                              ext_len_index;
    OBJECT                              *od_type_object;
    int                                 r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_PI_DESCRIPTION_LONG_FORMAT     *pi_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    pi_desc = (T_H1_PI_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_PI_DESCRIPTION_LONG_FORMAT));
    if(NULL == pi_desc)
    {
        r_code = CM_NO_MEMORY;
        goto pi_mem_err;
    }

    (void)memcpy((char *) pi_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_PI_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 15 + 2 * pi_desc->NumberOfDomains +
        fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    pi_desc->ObjectIndex = 
        convert_to_ltl_endian(pi_desc->ObjectIndex);
    pi_desc->AccessRights = 
        convert_to_ltl_endian(pi_desc->AccessRights);

    pi_desc->DomainIndex = (USIGN16 *)calloc(1,
            (pi_desc->NumberOfDomains) ?
            (pi_desc->NumberOfDomains * sizeof(USIGN16)) : 1);
    if(NULL == pi_desc->DomainIndex)
    {
        r_code = CM_NO_MEMORY;
        goto pi_domain_mem_err;
    }

    pi_desc->Name = (char *)calloc(1, 
            (fbap_od_desc.name_length) ? 
            (fbap_od_desc.name_length * sizeof(char)) : 1);
    if(NULL == pi_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto pi_name_err;
    }

    pi_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == pi_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto pi_ext_err;
    }

    (void)memcpy((USIGN16 *)pi_desc->DomainIndex, 
            (USIGN16 *) (global_res_buf + 14),
            2 * pi_desc->NumberOfDomains);

    (void)memcpy((char *)pi_desc->Name, 
            (char *) (global_res_buf + 14 + sizeof(USIGN16) * pi_desc->NumberOfDomains),
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)pi_desc->Extension, 
            (USIGN8 *) (global_res_buf + 15 + sizeof(USIGN16) *
                pi_desc->NumberOfDomains + fbap_od_desc.name_length), 
            ext_len);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "No: of Domains =   %d\nPassword    =   %x\n"
        "Access Group   =   %x\nAccess Rights   =   %d\n"
        "Deletable  =   %d\nReusable    =   %d\n"
        "PIState    =   %d\n"
        "Extension Length   =   %d\nUsage   =   %d",
        pi_desc->ObjectIndex,
        pi_desc->ObjectCode,
        pi_desc->NumberOfDomains,
        pi_desc->Password,
        pi_desc->AccessGroup,
        pi_desc->AccessRights,
        pi_desc->Deletable,
        pi_desc->Reusable,
        pi_desc->PIState,
        ext_len ,
        pi_desc->Extension[0]);

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto pi_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto pi_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto pi_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto pi_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto pi_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   INVOCATION_OBJECT;

        r_code = (*object_data_copy[INVOCATION_OBJECT])(od_type_object, pi_desc,
                    value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto pi_od_specific_mem_err;
        }

        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto pi_obj_copy_err;
        }
    }
    
pi_obj_copy_err:
    free(od_type_object->specific);

pi_od_specific_mem_err:
    free(od_type_object);

pi_od_obj_mem_err:
pi_send_err:
    free(pi_desc->Extension);

pi_ext_err:
    free(pi_desc->Name);

pi_name_err:
    free(pi_desc->DomainIndex);

pi_domain_mem_err:
    free(pi_desc);

pi_mem_err:
    return r_code;    
 
}


int obj_event_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                              ext_len;
    USIGN8                              ext_len_index;
    OBJECT                              *od_type_object;
    int                                 r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_EVENT_DESCRIPTION_LONG_FORMAT  *ev_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    ev_desc = (T_H1_EVENT_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_EVENT_DESCRIPTION_LONG_FORMAT));
    if(NULL == ev_desc)
    {
        r_code = CM_NO_MEMORY;
        goto ev_mem_err;
    }

    (void)memcpy((char *) ev_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_EVENT_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 17 + fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    ev_desc->ObjectIndex = 
        convert_to_ltl_endian(ev_desc->ObjectIndex);
    ev_desc->IndexEventData = 
        convert_to_ltl_endian(ev_desc->IndexEventData);
    ev_desc->AccessRights = 
        convert_to_ltl_endian(ev_desc->AccessRights);
    ev_desc->Length = 
        convert_to_ltl_endian(ev_desc->Length);

    ev_desc->Name = (char *)calloc(1, 
            (fbap_od_desc.name_length) ? 
            (fbap_od_desc.name_length * sizeof(char)) : 1);
    if(NULL == ev_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto ev_name_err;
    }

    ev_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == ev_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto ev_ext_err;
    }

    (void)memcpy((char *)ev_desc->Name, 
            (char *) (global_res_buf + 17),
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)ev_desc->Extension, 
            (USIGN8 *) (global_res_buf + 18 + fbap_od_desc.name_length),
            ext_len);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
    "Index Event Data    =   %x\nLength  =   %d\n"
    "Password    =   %x\nAccess Group    =   %x\n"
    "Access Rights   =   %d\nEnabled     =   %d\n"
    "Extension Length    =   %d\nUsage   =   %d",
    ev_desc->ObjectIndex,
    ev_desc->ObjectCode,
    ev_desc->IndexEventData,
    ev_desc->Length,
    ev_desc->Password,
    ev_desc->AccessGroup,
    ev_desc->AccessRights,
    ev_desc->Enabled,
    ext_len,
    ev_desc->Extension[0]);

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto ev_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto ev_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto ev_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto ev_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto ev_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   EVENT_OBJECT;

        r_code = (*object_data_copy[EVENT_OBJECT])(od_type_object,
                ev_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto ev_od_specific_mem_err;
        }
        
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto ev_obj_copy_err;
        }
    }
    
ev_obj_copy_err:
    free(od_type_object->specific);

ev_od_specific_mem_err:
    free(od_type_object);

ev_od_obj_mem_err:
ev_send_err:
    free(ev_desc->Extension);

ev_ext_err:
    free(ev_desc->Name);

ev_name_err:
    free(ev_desc);

ev_mem_err:
    return r_code;    
 
}



int obj_type_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    OBJECT                              *od_type_object;
    int                                 r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_OD_DATA_TYPE_DESCRIPTION       *type_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    type_desc = (T_H1_OD_DATA_TYPE_DESCRIPTION *) calloc (
            1, sizeof(T_H1_OD_DATA_TYPE_DESCRIPTION));
    if(NULL == type_desc)
    {
        r_code = CM_NO_MEMORY;
        goto type_mem_err;
    }

    (void)memcpy((char *) type_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_OD_DATA_TYPE_DESCRIPTION));

    type_desc->ObjectIndex = convert_to_ltl_endian(type_desc->ObjectIndex);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "Name Length = %d\n",
        type_desc->ObjectIndex,
        type_desc->ObjectCode,
        type_desc->SymbolicNameLength);

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto type_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto type_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto type_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto type_od_obj_mem_err;
        }
        
        od_type_object->specific = (OBJECT_SPECIFIC *)calloc(
                1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto type_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   TYPE_OBJECT;

        r_code = (*object_data_copy[TYPE_OBJECT])(od_type_object,
                type_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto type_od_specific_mem_err;
        }
        
   
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto type_obj_copy_err;
        }
    }
    
type_obj_copy_err:
    free(od_type_object->specific);

type_od_specific_mem_err:
    free(od_type_object);

type_od_obj_mem_err:
type_send_err:
    free(type_desc);

type_mem_err:
    return r_code;    
} 

int obj_type_struct_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    OBJECT                              *od_type_object;
    int                                 r_code;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_OD_DATA_TYPE_STRUCTURE         *type_struct_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    type_struct_desc = (T_H1_OD_DATA_TYPE_STRUCTURE *) calloc (
            1, sizeof(T_H1_OD_DATA_TYPE_STRUCTURE));
    if(NULL == type_struct_desc)
    {
        r_code = CM_NO_MEMORY;
        goto type_struct_mem_err;
    }

    (void)memcpy((char *) type_struct_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_OD_DATA_TYPE_STRUCTURE));

    type_struct_desc->list = (T_H1_DATA_TYPE_ELEMENT *)calloc(1,
            (type_struct_desc->NumberOfElements) ?
            (type_struct_desc->NumberOfElements * 
             sizeof(T_H1_DATA_TYPE_ELEMENT)) : 1);
    if(NULL == type_struct_desc->list)
    {
        r_code = CM_NO_MEMORY;
        goto type_struct_list_mem_err;
    }

    (void)memcpy((T_H1_DATA_TYPE_ELEMENT *)type_struct_desc->list, 
            (T_H1_DATA_TYPE_ELEMENT *) (global_res_buf + 7),
            sizeof(T_H1_DATA_TYPE_ELEMENT) * type_struct_desc->NumberOfElements);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "No: of Elements =  %d\n",
        type_struct_desc->ObjectIndex,
        type_struct_desc->ObjectCode,
        type_struct_desc->NumberOfElements);

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto type_struct_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto type_struct_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto type_struct_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto type_struct_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto type_struct_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   TYPE_STRUCT_OBJECT;

        od_type_object->specific->data_type_structure.elem_count =  
            type_struct_desc->NumberOfElements;
        
        od_type_object->specific->data_type_structure.type_list = 
            (OBJECT_INDEX *)calloc(type_struct_desc->NumberOfElements,
                sizeof(OBJECT_INDEX));
        if(NULL == od_type_object->specific->data_type_structure.type_list)
        {
            r_code = CM_NO_MEMORY;
            goto type_struct_type_list_err;
        }

        od_type_object->specific->data_type_structure.size_list = 
            (UINT8 *)calloc(type_struct_desc->NumberOfElements,
                sizeof(UINT8));
        if(NULL == od_type_object->specific->data_type_structure.size_list)
        {
            r_code = CM_NO_MEMORY;
            goto type_struct_type_size_err;
        }

        r_code = (*object_data_copy[TYPE_STRUCT_OBJECT])(od_type_object,
                type_struct_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto type_struct_type_size_err;
        }
        
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto type_struct_obj_copy_err;
        }
    }
    
type_struct_obj_copy_err:
    free(od_type_object->specific->data_type_structure.size_list);

type_struct_type_size_err:
    free(od_type_object->specific->data_type_structure.type_list);

type_struct_type_list_err:
    free(od_type_object->specific);

type_struct_od_specific_mem_err:
    free(od_type_object);

type_struct_od_obj_mem_err:
type_struct_send_err:
    free(type_struct_desc->list);

type_struct_list_mem_err:
    free(type_struct_desc);

type_struct_mem_err:
    return r_code;    
 
}


int obj_simple_var_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                                  ext_len;
    USIGN8                                  ext_len_index;
    OBJECT                                  *od_type_object;
    int                                     r_code, i;
    T_VAR_READ_CNF                          *read_ptr;
    USIGN8                                  *data_ptr;
    T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT *simple_var_desc;
    OD_DESCRIPTION_SPECIFIC                 fbap_od_desc;
    FBAP_NMA_REQ                            fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    simple_var_desc = (T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT));
    if(NULL == simple_var_desc)
    {
        r_code = CM_NO_MEMORY;
        goto simple_var_mem_err;
    }

    (void)memcpy((char *) simple_var_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 17 + fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    simple_var_desc->ObjectIndex = 
        convert_to_ltl_endian(simple_var_desc->ObjectIndex);
    simple_var_desc->AccessRights = 
        convert_to_ltl_endian(simple_var_desc->AccessRights);
    simple_var_desc->DataTypeIndex = 
        convert_to_ltl_endian(simple_var_desc->DataTypeIndex);
    simple_var_desc->LocalAddres = 
        convert_to_ltl_endian(simple_var_desc->LocalAddres);
    
    // CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    simple_var_desc->Name =
        (char *)calloc(1,
                (fbap_od_desc.name_length) ? 
                (fbap_od_desc.name_length * sizeof(char)):1);

    if(NULL == simple_var_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto simple_var_name_err;
    }

    simple_var_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == simple_var_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto simple_var_ext_err;
    }

    (void)memcpy((char *)simple_var_desc->Name, 
            (char *) (global_res_buf + 17),
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)simple_var_desc->Extension, 
            (USIGN8 *) (global_res_buf + 18 + fbap_od_desc.name_length),
            ext_len);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nData Type Index =   %x\n"
        "Object Code =   %x\nLength=   %d\nPassword    =   %x\n"
        "Access Group    =   %x\nAccess Rights   =   %x\n"
        "Name        =       %c\nLocal Address   =   %x\n"
        "Extension Length=   %x\nUsage       =   %x",
        simple_var_desc->ObjectIndex,
        simple_var_desc->DataTypeIndex,
        simple_var_desc->ObjectCode,
        simple_var_desc->Length,
        simple_var_desc->Password,
        simple_var_desc->AccessGroup,
        simple_var_desc->AccessRights,
        simple_var_desc->Name[0],
        simple_var_desc->LocalAddres,
        ext_len ,
        simple_var_desc->Extension[0]);

    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Member Id   =   ");
    for(i = 1; i < ext_len -4; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x", simple_var_desc->Extension[i]);
    
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Item Id =   ");
    for(i = 5; i < ext_len ; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x", simple_var_desc->Extension[i]);
#else
    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nData Type Index =   %x\n"
        "Object Code =   %x\nLength=   %d\n",
        simple_var_desc->ObjectIndex,
        simple_var_desc->DataTypeIndex,
        simple_var_desc->ObjectCode,
        simple_var_desc->Length);
#endif    

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto simple_var_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto simple_var_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto simple_var_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto simple_var_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto simple_var_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   SIMPLE_VAR_OBJECT;

        r_code = (*object_data_copy[SIMPLE_VAR_OBJECT])(od_type_object,
                simple_var_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto simple_var_od_specific_mem_err;
        }
        

        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto simple_var_obj_copy_err;
        }
    }
    
simple_var_obj_copy_err:
    free(od_type_object->specific);

simple_var_od_specific_mem_err:
    free(od_type_object);

simple_var_od_obj_mem_err:
simple_var_send_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(simple_var_desc->Extension);
#endif

simple_var_ext_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT  
    free(simple_var_desc->Name);
#endif

simple_var_name_err:
    free(simple_var_desc);

simple_var_mem_err:
    return r_code;    
}


int obj_array_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                              ext_len;
    USIGN8                              ext_len_index;
    OBJECT                              *od_type_object;
    int                                 r_code, i;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_ARRAY_DESCRIPTION_LONG_FORMAT  *array_desc;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    array_desc = (T_H1_ARRAY_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_ARRAY_DESCRIPTION_LONG_FORMAT));
    if(NULL == array_desc)
    {
        r_code = CM_NO_MEMORY;
        goto array_mem_err;
    }

    (void)memcpy((char *) array_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_ARRAY_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 18 + fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    array_desc->ObjectIndex = 
        convert_to_ltl_endian(array_desc->ObjectIndex);
    array_desc->AccessRights = 
        convert_to_ltl_endian(array_desc->AccessRights);
    array_desc->DataTypeIndex = 
        convert_to_ltl_endian(array_desc->DataTypeIndex);
    array_desc->LocalAddres = 
        convert_to_ltl_endian(array_desc->LocalAddres);
    
    // CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    array_desc->Name =
        (char *)calloc(1,
                (fbap_od_desc.name_length) ? 
                (fbap_od_desc.name_length * sizeof(char)):1);

    if(NULL == array_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto array_name_err;
    }

    array_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == array_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto array_ext_err;
    }

    (void)memcpy((char *)array_desc->Name, 
            (char *) (global_res_buf + 18),
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)array_desc->Extension, 
            (USIGN8 *) (global_res_buf + 19 + fbap_od_desc.name_length),
            ext_len);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "Data Type Index =   %x\nLength  =   %d\nNo: of Elements =   %d\n"
        "Password    =   %x\nAccess Group    =   %x\n"
        "Access Rights   =   %d\nLocal Address   =   %x\n"
        "Extension Length    =   %d\nUsage   =   %d",
        array_desc->ObjectIndex,
        array_desc->ObjectCode,
        array_desc->DataTypeIndex,
        array_desc->Length,
        array_desc->NumberOfElements,
        array_desc->Password,
        array_desc->AccessGroup,
        array_desc->AccessRights,
        array_desc->LocalAddres,
        ext_len,
        array_desc->Extension[0]);
    
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Member Id   =   ");
    for(i = 1; i < ext_len - 4; i++)
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x",array_desc->Extension[i]);
    }
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Item Id =   ");
    for(i = 5; i < ext_len; i++)
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x",array_desc->Extension[i]);
    }
#else
    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "Data Type Index =   %x\nLength  =   %d\n No: of Elements =   %d\n",
        array_desc->ObjectIndex,
        array_desc->ObjectCode,
        array_desc->DataTypeIndex,
        array_desc->Length,
        array_desc->NumberOfElements);
#endif

    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto array_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto array_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto array_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto array_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto array_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   ARRAY_OBJECT;

        r_code = (*object_data_copy[ARRAY_OBJECT])(od_type_object,
                array_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto array_od_specific_mem_err;
        }
        
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto array_obj_copy_err;
        }
    }
    
array_obj_copy_err:
    free(od_type_object->specific);

array_od_specific_mem_err:
    free(od_type_object);

array_od_obj_mem_err:
array_send_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(array_desc->Extension);
#endif

array_ext_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(array_desc->Name);
#endif

array_name_err:
    free(array_desc);

array_mem_err:
    return r_code;    
}


int obj_record_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                              ext_len;
    USIGN8                              ext_len_index;
    OBJECT                              *od_type_object;
    int                                 r_code, i;
    T_VAR_READ_CNF                      *read_ptr;
    USIGN8                              *data_ptr;
    T_H1_RECORD_DESCRIPTION_LONG_FORMAT *record_desc;
    T_H1_OD_DATA_TYPE_STRUCTURE         *type_struct;
    OD_DESCRIPTION_SPECIFIC             fbap_od_desc;
    FBAP_NMA_REQ                        fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    record_desc = (T_H1_RECORD_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_RECORD_DESCRIPTION_LONG_FORMAT));
    if(NULL == record_desc)
    {
        r_code = CM_NO_MEMORY;
        goto record_mem_err;
    }

    (void)memcpy((char *) record_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_RECORD_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 12 + fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    record_desc->ObjectIndex = 
        convert_to_ltl_endian(record_desc->ObjectIndex);
    record_desc->AccessRights = 
        convert_to_ltl_endian(record_desc->AccessRights);
    record_desc->DataTypeIndex = 
        convert_to_ltl_endian(record_desc->DataTypeIndex);
    
    // CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    record_desc->Name =
        (char *)calloc(1,
                (fbap_od_desc.name_length) ? 
                (fbap_od_desc.name_length * sizeof(char)):1);

    if(NULL == record_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto record_name_err;
    }

    record_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == record_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto record_ext_err;
    }

    (void)memcpy((char *)record_desc->Name, 
            (char *) (global_res_buf + 12),
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)record_desc->Extension, 
            (USIGN8 *) (global_res_buf + 13 + fbap_od_desc.name_length),
            ext_len);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nData Type Index =   %x\n"
        "Object Code =   %x\nPassword    =   %x\n"
        "Access Group    =   %x\nAccess Rights   =   %x\n"
        "Name        =       %c\nExtension Length=   %x\n"
        "Usage       =   %x",
        record_desc->ObjectIndex,
        record_desc->DataTypeIndex,
        record_desc->ObjectCode,
        record_desc->Password,
        record_desc->AccessGroup,
        record_desc->AccessRights,
        record_desc->Name[0],
        ext_len,
        record_desc->Extension[0]);

    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Member Id   = ");
    for(i = 1; i < ext_len - 4; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x", record_desc->Extension[i]);
                        
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Item Id = ");
    for(i = 5; i < ext_len ; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x", record_desc->Extension[i]);
#else
    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nData Type Index =   %x\n"
        "Object Code =   %x\n",
        record_desc->ObjectIndex,
        record_desc->DataTypeIndex,
        record_desc->ObjectCode);
#endif

    fbap_nma_req.index = record_desc->DataTypeIndex;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, GET_OD_SECOND_CL, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, GET_OD_SECOND_CL, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto record_send_err1;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
        log_error(error); 
        app_info->env_returns.response_code = error->class_code;
        r_code = CS_FMS_ERR(error->class_code);
        goto record_send_err1;
    }
    else if(POS == get_service_result())
    {
        type_struct = (T_H1_OD_DATA_TYPE_STRUCTURE *)calloc(1
                    ,sizeof(T_H1_OD_DATA_TYPE_STRUCTURE));
        if(NULL == type_struct)
        {
            r_code = CM_NO_MEMORY;
            goto record_type_struct_mem_err;
        }

        (void) memcpy((char *)type_struct, (char *)(global_res_buf + 3),
                sizeof(T_H1_OD_DATA_TYPE_STRUCTURE));

        LOGC_TRACE(CPM_FF_DDSERVICE,"\nNo: of elements:   =   %d\n",
                type_struct->NumberOfElements);
        
        type_struct->list = (T_H1_DATA_TYPE_ELEMENT *) calloc(1,
                sizeof(T_H1_DATA_TYPE_ELEMENT) * 
                type_struct->NumberOfElements);
        if(NULL == type_struct->list)
        {
            r_code = CM_NO_MEMORY;
            goto record_type_struct_list_mem_err;
        }

        (void) memcpy((char *)type_struct->list, 
                (char *)(global_res_buf + 7),
                sizeof(T_H1_DATA_TYPE_ELEMENT) * 
                type_struct->NumberOfElements);


        for(i = 0; i < type_struct->NumberOfElements; i++)
        {
            type_struct->list[i].DataTypeIndex = 
                convert_to_ltl_endian(type_struct->list[i].DataTypeIndex);

            LOGC_INFO(CPM_FF_DDSERVICE,"Datatype index of %d Element =   %x\tLength  =   %d\n",
                i, type_struct->list[i].DataTypeIndex,
                type_struct->list[i].Length);
        }
    }
                    
    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    save_fbap_current_index(object_index, sub_index,cr);

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto record_send_err2;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
        log_error(error);    
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto record_send_err2;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto record_send_err2;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto record_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto record_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   type_struct->ObjectCode;

        od_type_object->access.groups = record_desc->AccessGroup;
        od_type_object->access.password = record_desc->Password;
        od_type_object->access.rights = record_desc->AccessRights;
        od_type_object->specific->data_type_structure.elem_count = 
            type_struct->NumberOfElements;
        
        od_type_object->specific->data_type_structure.type_list = 
            (OBJECT_INDEX *)calloc(type_struct->NumberOfElements, 
                    sizeof(OBJECT_INDEX));
        if(NULL == od_type_object->specific->data_type_structure.type_list)
        {
            r_code = CM_NO_MEMORY;
            goto record_type_list_err;
        }
        
        od_type_object->specific->data_type_structure.size_list = 
            (UINT8 *)calloc(type_struct->NumberOfElements, sizeof(UINT8));
        if(NULL == od_type_object->specific->data_type_structure.size_list)
        {
            r_code = CM_NO_MEMORY;
            goto record_size_list_err;
        }

        r_code = (*object_data_copy[type_struct->ObjectCode])(od_type_object,
                type_struct, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto record_obj_copy_err;
        }
  
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto record_obj_copy_err;
        }
    }

record_obj_copy_err:
    free(od_type_object->specific->data_type_structure.size_list);

record_size_list_err:
    free(od_type_object->specific->data_type_structure.type_list);

record_type_list_err:
    free(od_type_object->specific);

record_od_specific_mem_err:
    free(od_type_object);

record_od_obj_mem_err:
record_send_err2:
    free(type_struct->list);

record_type_struct_list_mem_err:
    free(type_struct);

record_type_struct_mem_err:
record_send_err1:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT  
    free(record_desc->Extension);
#endif

record_ext_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT  
    free(record_desc->Name);
#endif

record_name_err:
   free(record_desc);

record_mem_err:
    return r_code;    
}



int obj_var_list_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size)
{
    USIGN8                                      ext_len;
    USIGN8                                      ext_len_index;
    OBJECT                                      *od_type_object;
    int                                         r_code, i;
    T_VAR_READ_CNF                              *read_ptr;
    USIGN8                                      *data_ptr;
    T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT  *var_list_desc;
    OD_DESCRIPTION_SPECIFIC                     fbap_od_desc;
    FBAP_NMA_REQ                                fbap_nma_req;
    NT_COUNT        					nt_count;
    NETWORK_HANDLE						nh;
    APP_INFO    *app_info = (APP_INFO *)env_info->app_info;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = get_nt_net_handle_by_cr(nt_count, cr, &nh);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }


    r_code = get_nt_net_fbap_od(nh, &fbap_od_desc);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    
    var_list_desc = (T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT *) calloc (
            1, sizeof(T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT));
    if(NULL == var_list_desc)
    {
        r_code = CM_NO_MEMORY;
        goto var_list_mem_err;
    }

    (void)memcpy((char *) var_list_desc, 
            (char *)(global_res_buf + 3),
            sizeof(T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT));

    ext_len_index = 11 +  (2 * var_list_desc->NumberOfElements) + 
        fbap_od_desc.name_length;
    ext_len = global_res_buf[ext_len_index];

    var_list_desc->ObjectIndex = 
        convert_to_ltl_endian(var_list_desc->ObjectIndex);
    var_list_desc->AccessRights = 
        convert_to_ltl_endian(var_list_desc->AccessRights);
    
    // CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    var_list_desc->Name =
        (char *)calloc(1,
                (fbap_od_desc.name_length) ? 
                (fbap_od_desc.name_length * sizeof(char)):1);

    if(NULL == var_list_desc->Name)
    {
        r_code = CM_NO_MEMORY;
        goto var_list_name_err;
    }

    var_list_desc->Extension = (USIGN8 *)calloc(1, 
            ext_len ? (sizeof(USIGN8) * ext_len) : 1);
    if(NULL == var_list_desc->Extension)
    {
        r_code = CM_NO_MEMORY;
        goto var_list_ext_err;
    }
    
    var_list_desc->ElementIndex = (USIGN16 *)calloc(1, 
            var_list_desc->NumberOfElements ? 
            (sizeof(USIGN16) * var_list_desc->NumberOfElements) : 1);
    if(NULL == var_list_desc->ElementIndex)
    {
        r_code = CM_NO_MEMORY;
        goto var_list_index_err;
    }


    (void)memcpy((char *)var_list_desc->Name, 
            (char *) (global_res_buf + 11 +
                sizeof(USIGN16) * var_list_desc->NumberOfElements), 
            fbap_od_desc.name_length);
    
    (void)memcpy((USIGN8 *)var_list_desc->Extension, 
            (USIGN8 *) (global_res_buf + 12 +
                sizeof(USIGN16) * var_list_desc->NumberOfElements + 
                fbap_od_desc.name_length),
            ext_len);

    (void) memcpy((USIGN16 *)var_list_desc->ElementIndex, 
            (USIGN16 *)(global_res_buf + 11),
            2 * var_list_desc->NumberOfElements);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "No: of Elements =   %d\nPassword    =   %x\n"
        "Access Group    =   %x\nAccess Rights   =   %d\n"
        "Deletable   =   %d\n"
        "Extension Length    =   %d\nUsage   =   %d",
        var_list_desc->ObjectIndex,
        var_list_desc->ObjectCode,
        var_list_desc->NumberOfElements,
        var_list_desc->Password,
        var_list_desc->AccessGroup,
        var_list_desc->AccessRights,
        var_list_desc->Deletable,
        ext_len,
        var_list_desc->Extension[0]);
    
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Member Id   = ");
    for(i = 1; i < ext_len - 4; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x", var_list_desc->Extension[i]);
                        
    LOGC_TRACE(CPM_FF_DDSERVICE,"DD Item Id =   ");
    for(i = 5; i < ext_len; i++)
        LOGC_TRACE(CPM_FF_DDSERVICE,"%3x",var_list_desc->Extension[i]);
#else

    LOGC_TRACE(CPM_FF_DDSERVICE,"Object Index =   %x\nObject Code =   %x\n"
        "No: of Elements =   %d\n" "Deletable   =   %d\n",
        var_list_desc->ObjectIndex,
        var_list_desc->ObjectCode,
        var_list_desc->NumberOfElements,
        var_list_desc->Deletable);
#endif    
                    
    fbap_nma_req.index = object_index;
    fbap_nma_req.sub_index = sub_index;
    fbap_nma_req.cr_index = cr;

    r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ, 
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		goto var_list_send_err;
    }

    if(NEG == get_service_result())        
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
            
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
        error->add_description);
        app_info->env_returns.response_code = error->class_code;
        
        r_code = CS_FMS_ERR(error->class_code);
        goto var_list_send_err;
    }
        
    else if(POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;
        *value_size = read_ptr->length;

        data_ptr = (USIGN8*)(read_ptr + 1);

        *value = (UINT8 *) calloc(1, (unsigned int)*value_size);
        if (!*value) {
            r_code = CM_NO_MEMORY;
            goto var_list_send_err;
        }

        (void) memcpy((char *)*value, (char *)data_ptr, *value_size);
        od_type_object = (OBJECT *)calloc(1,sizeof(OBJECT));
        if(NULL == od_type_object)
        {
            r_code = CM_NO_MEMORY;
            goto var_list_od_obj_mem_err;
        }
        
        od_type_object->specific = 
            (OBJECT_SPECIFIC *)calloc(1,sizeof(OBJECT_SPECIFIC));
        if(NULL == od_type_object->specific)
        {
            r_code = CM_NO_MEMORY;
            goto var_list_od_specific_mem_err;
        }
        od_type_object->index       =   object_index;
        od_type_object->code        =   VAR_LIST_OBJECT;

        r_code = (*object_data_copy[VAR_LIST_OBJECT])(od_type_object,
                var_list_desc, value, value_size);
        if(r_code != CM_SUCCESS)
        {
            goto var_list_od_specific_mem_err;
        }
  
        r_code = softing_object_copy(type_object, od_type_object);
        if (r_code != CM_SUCCESS) {
            goto var_list_obj_copy_err;
        }
    }
    
var_list_obj_copy_err:
    free(od_type_object->specific);

var_list_od_specific_mem_err:
    free(od_type_object);

var_list_od_obj_mem_err:
var_list_send_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(var_list_desc->ElementIndex);
#endif
    
var_list_index_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(var_list_desc->Extension);
#endif

var_list_ext_err:
// CPMHACK: Extension is applicable only for Long format OD
#ifdef SUPPORT_LONG_FORMAT
    free(var_list_desc->Name);
#endif

var_list_name_err:
   free(var_list_desc);

var_list_mem_err:
    return r_code;    
}
