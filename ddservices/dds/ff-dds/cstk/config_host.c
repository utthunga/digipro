/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Host configuration module.

    This file contains functions pertaining to configuring the host
    either as client mode or server mode

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "fakexmtr.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

int softing_config_stack(ENV_INFO *env_info, CM_NMA_INFO *nma_info, HOST_CONFIG *hc)
{

    int             r_code;
    APP_INFO        *app_info;
    SM_REQ          sm_req;
    FBAP_NMA_REQ    dll_req;
    int curr_state = 0;

    memset((char *)&sm_req, 0, sizeof(SM_REQ));
    memset((char *)&dll_req, 0, sizeof(FBAP_NMA_REQ));
    app_info = (APP_INFO *)env_info->app_info;

    cr_set_appl_state_busy(env_info, &curr_state);

    /*
     * Configure the System management configuration of the
     * FBK board.
     */
    strcpy((char *)&sm_req.host_pd_tag,
    	            (char *)&hc->sm_config.host_pd_tag);
    strcpy((char *)&sm_req.vendor_name,
    	            (char *)&hc->sm_config.vendor_name);
    strcpy((char *)&sm_req.model_name,
    	            (char *)&hc->sm_config.model_name);
    strcpy((char *)&sm_req.revision_string,
    	            (char *)&hc->sm_config.revision_string);

    r_code = send_wait_receive_cmd(env_info, HNDL_SM_CONF_REQ, NULL, &sm_req);
    if(CM_SUCCESS != r_code) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in SM config request");
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_INFO(CPM_FF_DDSERVICE,"SM Config positive confirmation");
    }
    else if(NEG == get_service_result())
    {
        T_SM_ERROR *sm_err = (T_SM_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"SM set Configuration: Error Class code = %d\n",
        sm_err->rc);
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM set Configuration: Additional Detail = %d\n",
        sm_err->add_detail);
        app_info->env_returns.response_code = sm_err->rc;
        LOGC_ERROR(CPM_FF_DDSERVICE,"SM Config confirmation (-)");

        return CS_SM_ERR(sm_err->rc);
    }

    /*
     * Configure the DLL configuration of the device
     */

    (void *)memcpy((char *)&dll_req.dll_config, (char *)&hc->dll_config,
            sizeof(T_DLL_CONFIG_REQ ));

    r_code = send_wait_receive_cmd(env_info, HNDL_DLL_CONF_REQ, &dll_req, NULL);

    if(CM_SUCCESS != r_code) {
         return (r_code);
    }

    if (POS == get_service_result())
    {
        if(NULL != nma_info)
        {
            (void *)memcpy(nma_info, (CM_NMA_INFO *)global_res_buf,
 	                sizeof(T_NMA_INFO));
        }
        LOGC_INFO(CPM_FF_DDSERVICE,"DLL Config positive confirmation");
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
             error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
             error->add_detail);
                LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
             error->add_description);
        app_info->env_returns.response_code = error->class_code;

        LOGC_INFO(CPM_FF_DDSERVICE,"DLL Config con (-)");
        return CS_NMA_ERR(error->class_code);
    }

    /* Initialization of VCRL request of FBK board */
    r_code = send_wait_receive_cmd(env_info, HNDL_VCRL_INIT_REQ, NULL, NULL);

    if(CM_SUCCESS != r_code) {
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"VCRL Init con (+)");
    }
    else if(NEG == get_service_result())
    {
       T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Error Class code = %d\n",
           error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Detail = %d\n",
           error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"read_data_device: Additional Description = %s\n",
    	   error->add_description);
    	app_info->env_returns.response_code = error->class_code;

        LOGC_INFO(CPM_FF_DDSERVICE,"DLL Config con (-)");
    	return CS_NMA_ERR(error->class_code);
    }

    r_code = send_wait_receive_cmd(env_info, HNDL_VCRL_TERM_REQ, NULL, NULL);

    if(CM_SUCCESS != r_code) {
        return (r_code);
    }

    if (POS == get_service_result())
    {
        LOGC_INFO(CPM_FF_DDSERVICE,"VCRL Terminate con (+)");
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;
        log_error(error);
    	app_info->env_returns.response_code = error->class_code;

        LOGC_INFO(CPM_FF_DDSERVICE,"DLL Config con (-)");
    	return CS_NMA_ERR(error->class_code);
    }

    if (DEVACC_APPL_STATE_END == get_device_admin_state())
    {
        return CM_COMM_ERR;
    }
    else
    {
        set_device_admin_state(DEVACC_APPL_STATE_READY);
        LOGC_INFO(CPM_FF_DDSERVICE,"Stack initialized");
    }

    cr_set_appl_state_normal(env_info);
    return CM_SUCCESS;
}

int softing_get_stack_config(ENV_INFO *env_info, CM_NMA_INFO *nma_info)
{

    int             r_code;

    /*
     * Configure the System mangement configuration of the
     * FBK board.
     */
    
    r_code = get_nma_info_from_server(env_info, nma_info);
    if(r_code != CM_SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to get NMA information fron server device");
        return r_code;
    }

    return CM_SUCCESS;
}
    
