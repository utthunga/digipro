/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Threading and synchronization logic between read and write operations.

    This file contains all functions pertaining to the creating
    thread, locking using mutex and synchronization between
    read from and write to operations to the FF H1 modem.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <mqueue.h>

#include "devsim.h"
#include "dds_comm.h"
#include "ddi_lib.h"
#include "int_diff.h"
#include "device_access.h"
#include "message_q.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"
#include "os_helper.h"

#define FLAG_OFF	0
#define FLAG_ON		1
#define	FLAG_CHECK	2
#define MAX_READ_TIME   90
#define MAX_WRITE_TIME  30

/*
 * Synchronization locks
 */
volatile    int     read_lock = 0;
volatile    int     write_lock = 0;

COMM_REQ comm_req;


int (*getConnectionState) P(()) = NULL;


static int flag_timeout(int flag)
{
	static int timeout = 0;

	if(FLAG_CHECK != flag)
	{
		timeout = flag;
	}

	return timeout;
}

/***********************************************************************
 *
 * Name: set_error_code
 *
 * ShortDesc: Sets the error code.
 *
 *
 * Description:
 *      This is the routine that sets the error reason code,
 *      state of the device and error caused service layers.
 *
 * Returns:
 *      none
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

void set_error_code(int state, int r_code)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();

    pthread_mutex_lock(m_mutex);
    set_device_error(state, r_code);
    pthread_mutex_unlock(m_mutex);
}


/***********************************************************************
 *
 * Name: get_error_code
 *
 * ShortDesc: Reads the error code from the global error structure
 *
 * Description:
 *      This is the routine reads the error set in the global device
 *      error data structure.
 *
 * Returns:
 *      error code
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

static int get_error_code(T_DEVICE_ERROR *dev_err)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"set_error_code: Error in getting the mutex\n");
        return CM_BAD_MUTEX;
    }
    pthread_mutex_lock(m_mutex);

    get_device_error(dev_err);

    pthread_mutex_unlock(m_mutex);

    return SUCCESS;
}


/***********************************************************************
 *
 * Name: set_return_code
 *
 * ShortDesc: Sets the error code.
 *
 *
 * Description:
 *      This is the routine that sets the error reason code,
 *      state of the device and error caused service layers.
 *
 * Returns:
 *      none
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

void set_return_code(int state, int r_code)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();

    pthread_mutex_lock(m_mutex);
    set_device_return_error(state, r_code);
    pthread_mutex_unlock(m_mutex);
}


/***********************************************************************
 *
 * Name: get_return_code
 *
 * ShortDesc: Reads the error code from the global error structure
 *
 * Description:
 *      This is the routine reads the error set in the global device
 *      error data structure.
 *
 * Returns:
 *      error code
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

static int get_return_code(T_DEVICE_ERROR *dev_err)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"set_error_code: Error in getting the mutex\n");
        return CM_BAD_MUTEX;
    }
    pthread_mutex_lock(m_mutex);

    get_device_return_error(dev_err);

    pthread_mutex_unlock(m_mutex);

    return SUCCESS;
}

/***********************************************************************
 *
 * Name: get_command_request
 *
 * ShortDesc: Gets the requested command to write into the FFH1 modem
 *
 *
 * Description:
 *      This is the routine that reads the comm_req staructue for the
 *      set command by the application.
 *
 * Returns:
 *      none
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

void get_command_request
(
    unsigned int *condition,
    FBAP_NMA_REQ *fbap_nma_req,
    SM_REQ *sm_req
)
{

    if(NULL != fbap_nma_req)
    {
        (void *)memcpy((char *)fbap_nma_req,
                (char *)&comm_req.fbap_nma_req,
                sizeof(FBAP_NMA_REQ));
    }
    if(NULL != sm_req)
    {
        (void *)memcpy((char *)sm_req,
                (char *)&comm_req.sm_req,
                sizeof(SM_REQ));
    }
    *condition = comm_req.condition;

    (void *)memset((char *)&comm_req, 0, sizeof(COMM_REQ));
}

/***********************************************************************
 *
 * Name: set_command_request
 *
 * ShortDesc: sets the command request for writing into the FFH1 modem
 *              (FF stack)
 *
 * Description:
 *      This is the routine sets the command to write into the
 *      FF communcation stack. This function sets the global structure
 *      comm_req
 *
 * Returns:
 *      none
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int set_command_request
(
    unsigned int condition,
    FBAP_NMA_REQ *fbap_nma_req,
    SM_REQ *sm_req,
	int host_type
)
{
	int r_code;

    if(NULL != fbap_nma_req)
    {
        (void *)memcpy((char *)&comm_req.fbap_nma_req,
                (char *)fbap_nma_req,
                sizeof(FBAP_NMA_REQ));
    }
    if(NULL != sm_req)
    {
        (void *)memcpy((char *)&comm_req.sm_req,
                (char *)sm_req,
                sizeof(SM_REQ));
    }
    comm_req.condition = condition;

    /*
     * Send Request command Data to TCP server/DPC
     */
    if(HOST_CLIENT == host_type)
    {
  		r_code = send_data_to_server(&comm_req);
    	if(r_code != CM_SUCCESS)
    	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in sending data over socket");
    		return r_code;
    	}
    }
    return CM_SUCCESS;
}

/***********************************************************************
 *
 * Name: is_read_lock_acquired
 *
 * ShortDesc: Checking for the read lock
 *
 *
 * Description:
 *      This is the routine that verifies for the read lock whether
 *      acquired or not.
 *
 * Returns:
 *      One of the following codes is returned.
 *      SUCCESS -- main test performed correctly.
 *      FAILURE -- main test failed.
 *      CM_COMM_ERROR  -- On failure to perform the action
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int is_read_lock_acquired(void)
{
    int r_code;
    pthread_mutex_t *m_mutex = NULL;


    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }

    pthread_mutex_lock(m_mutex);

    if(read_lock)
        r_code = LOCK_ACQUIRED;
    else
        r_code = LOCK_RELEASED;
    pthread_mutex_unlock(m_mutex);

    return r_code;
}


/***********************************************************************
 *
 * Name: is_write_lock_acquired
 *
 * ShortDesc: Verifying the write lock
 *
 *
 * Description:
 *      This is the routine that performs the icheck for write lock
 *      for locked or released.
 *
 * Returns:
 *      One of the following codes is returned.
 *      LOCK_ACQUIRED -- If locked.
 *      LOCK_RELEASED -- If Released.
 *      CM_COMM_ERR  -- On failure to get the mutex variable
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int is_write_lock_acquired(void)
{
    int r_code;
    pthread_mutex_t *m_mutex = NULL;


    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }

    pthread_mutex_lock(m_mutex);

    if(write_lock)
        r_code = LOCK_ACQUIRED;
    else
        r_code = LOCK_RELEASED;
    pthread_mutex_unlock(m_mutex);

    return r_code;
}

/***********************************************************************
 *
 * Name: aquire_write_lock
 *
 * ShortDesc: Acquire the lock before write operation
 *
 *
 * Description:
 *      This is the routine that acquires the write lock by assigning
 *      LOCK_ACQUIRED to write_lock.
 *
 * Returns:
 *      One of the following codes is returned.
 *      CM_SUCCESS-- On successful Lock acquire.
 *      CM_COMM_ERR -- On failure to acquire lock.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int acquire_write_lock(void)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }
    pthread_mutex_lock(m_mutex);

    write_lock = LOCK_ACQUIRED;
    pthread_mutex_unlock(m_mutex);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 * Name: release_write_lock
 *
 * ShortDesc: Release the acquired write lock
 *
 *
 * Description:
 *      This is the routine that releases the write lock by assigning
 *      LOCK_RELEASED to write_lock
 *
 * Returns:
 *      One of the following codes is returned.
 *      CM_SUCCESS -- On successful lock release.
 *      CM_COMM_ERR -- On failure to release write lock.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int release_write_lock(void)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }
    pthread_mutex_lock(m_mutex);

    write_lock = LOCK_RELEASED;
    pthread_mutex_unlock(m_mutex);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 * Name: acquire_read_lock
 *
 * ShortDesc: Acquire the lock before read operation
 *
 * Description:
 *      This is the routine that acquires the read lock by assigning
 *      LOCK_ACQUIRED to read_lock.
 *
 * Returns:
 *      One of the following codes is returned.
 *      CM_SUCCESS-- On successful Lock acquire.
 *      CM_COMM_ERR -- On failure to acquire lock.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int acquire_read_lock(void)
{
    pthread_mutex_t *m_mutex = NULL;

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }
    pthread_mutex_lock(m_mutex);

    read_lock = 1;
    pthread_mutex_unlock(m_mutex);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 * Name: release_read_lock
 *
 * ShortDesc: Release the acquired read lock
 *
 *
 * Description:
 *      This is the routine that releases the read lock by assigning
 *      LOCK_RELEASED to read_lock
 *
 * Returns:
 *      One of the following codes is returned.
 *      CM_SUCCESS -- On successful lock release.
 *      CM_COMM_ERR -- On failure to release read lock.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int release_read_lock(void)
{
    pthread_mutex_t *m_mutex = NULL;


    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting  the mutex\n");
        return(CM_BAD_MUTEX);
    }
    pthread_mutex_lock(m_mutex);
    if(LOCK_ACQUIRED == read_lock)
    	read_lock = LOCK_RELEASED;
    pthread_mutex_unlock(m_mutex);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 * Name: handle_request
 *
 * ShortDesc: Handle the request command
 *
 *
 * Description:
 *      This is the routine that gets the command request from
 *      global structure COMM_REQ and sets/frames the request packet
 *      and sends it to the FBK2 board.
 *
 * Returns:
 *      One of the following codes is returned.
 *      CM_SUCCESS -- On successful lock release.
 *      Error return from device_send_req_res function.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int handle_request(int *cond)
{
    int r_code;
    unsigned int condition;
	FBAP_NMA_REQ fbap_nma_req;
    SM_REQ sm_req;
    USIGN8 tag_type = 0;
    FB_BOOL   with_param = 0;
    USIGN16 index;
	
	set_service_comm_ref(0);
    set_service_layer(0);
    set_service_ref(0);
    set_service_primitive(0);
    set_service_invoke_id(0);
	set_service_result(0);
	
	/*
	 * Get the command request from COMM_REQ data structure
	 */
    get_command_request(&condition, &fbap_nma_req, &sm_req);
    *cond = condition;

    LOGC_TRACE(CPM_FF_DDSERVICE, "Processing command: %d" , condition);

    switch(condition)
    {
        case INITIATE_REQUEST:
            {
                LOGC_TRACE(CPM_FF_DDSERVICE, "Communication reference index for FBAP NMA request: %d", fbap_nma_req.cr_index);
                r_code = handle_initiate_req(fbap_nma_req.cr_index);
            }
            break;
        case GET_OD_REQUEST:
            {
                LOGC_TRACE(CPM_FF_DDSERVICE, "Communication reference index for FBAP NMA request: %d", fbap_nma_req.cr_index);
                r_code = handle_get_od_req(&fbap_nma_req, (USIGN16)fbap_nma_req.index);
            }
            break;
        case GET_OD_SECOND_CL:
            {
                LOGC_TRACE(CPM_FF_DDSERVICE, "Communication reference index for FBAP NMA request: %d", fbap_nma_req.cr_index);
                r_code = handle_get_od_req(&fbap_nma_req, (USIGN16)fbap_nma_req.index);
            }
            break;
        case HNDL_MGMT_READ_REQ:
            {
                LOGC_TRACE(CPM_FF_DDSERVICE, "Communication reference index for FBAP NMA request: %d", fbap_nma_req.cr_index);
                r_code = handle_mgmt_read_req(&fbap_nma_req);
            }
            break;
        case HNDL_MGMT_WRITE_REQ:
            {
                LOGC_TRACE(CPM_FF_DDSERVICE, "Communication reference index for FBAP NMA request: %d", fbap_nma_req.cr_index);
                r_code = handle_mgmt_write_req(&fbap_nma_req);
            }
            break;
        case SM_IDENTIFY_REQ:
            r_code = handle_identify_req((USIGN8)sm_req.dev_addr);
            break;
        case SM_CLR_NODE_REQ:
            r_code = handle_clear_addr_req(&sm_req);
            break;
        case SM_SET_NODE_REQ:
            r_code = handle_set_addr_req(&sm_req);
            break;
        case SM_CLR_TAG_REQ:
            r_code = handle_set_pd_tag_req(&sm_req);
            break;
        case SM_SET_TAG_REQ:
            r_code = handle_set_pd_tag_req(&sm_req);
            break;
        case HNDL_SM_CONF_REQ:
            r_code = handle_sm_config_req(&sm_req);
            break;
        case HNDL_DLL_CONF_REQ:
            r_code = handle_dll_config_req(&fbap_nma_req);
            break;
        case HNDL_VCRL_INIT_REQ:
            r_code = handle_vcrl_init_req();
            break;
        case HNDL_VCRL_TERM_REQ:
            r_code = handle_vcrl_term_req();
            break;
        case HNDL_VCRL_LOAD_REQ:
         /*
          * The Load request is initiated if and only
          * if the VCR information of the remote device
          * is known to the host and which is not
          * possible by the host device without
          * talking to remote device unless this
          * service is configured by the operator.
          */
            break;
        case HNDL_LOCAL_VCR_WR_REQ:
            if(TRUE == fbap_nma_req.nma_wr_flag)
            {
                set_device_admin_vfd_nr(fbap_nma_req.rem_add,
                    fbap_nma_req.cr_index);
                set_device_admin_device_addr(fbap_nma_req.rem_add,
                    fbap_nma_req.cr_index);

                set_device_admin_type_role(0xA2u, fbap_nma_req.cr_index);
                set_device_admin_local_nma_write(TRUE);
            }

            r_code = handle_load_vcr_local_write_req(&fbap_nma_req);
            break;
        case HNDL_NMA_READ_REQ:
            r_code = handle_nma_read_req(fbap_nma_req.index);
            break;
        case HNDL_FMS_READ_REQ:
            r_code = handle_read_req(fbap_nma_req.cr_index,
                        fbap_nma_req.index);
            break;
        case HNDL_FMS_VCR_WR_REQ:
            r_code = handle_vcr_write_req(&fbap_nma_req);
            break;
        case HNDL_ABORT_CR_REQ:
            r_code = handle_abort_req(fbap_nma_req.cr_index);
            break;
#ifdef ALL_FF_SERVICES_SUPPORTED
        case HNDL_NMA_RESET_REQ:
            r_code = handle_nma_reset_req();
            break;
#endif
        case HNDL_SCAN_LIST_REQ:
            index = get_index_to_scan();
            r_code = handle_nma_read_req(index);
            break;
        case HNDL_FIND_TAG_QUERY_REQ:
            r_code = handle_find_tag_query_req(tag_type, with_param);
            break;
        case HNDL_NMA_DATA_REQ:
            r_code = handle_nma_data_req();
            break;
        default:
            r_code = E_OK;
            break;
    }

    if(E_OK != r_code)
    {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Fatal Error **: %d", r_code);

		if (DEVACC_APPL_STATE_END == get_device_admin_state() )
		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Fatal Error: Not recoverable: %d", r_code);
		}
    }

    set_return_code(get_device_admin_state(), r_code);

    return r_code;
}


/***********************************************************************
 *
 * Name: wait_tcp_data_thread
 *
 * ShortDesc: the thread function to wait for the read and write
 *              completion to the communcation FF stack
 *
 *
 * Description:
 *      This is the routine that performs getting command request
 *      and calling the respective request function and waits till the
 *      response is recieved from the FF Softing stack.
 *
 * Returns:
 *      none.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

static void *wait_tcp_data_thread(void *arg)
{
#if 0

    int r_code = 0;
    int condition;
    ACTIVE_THREAD_DATA *th_data = (ACTIVE_THREAD_DATA *) arg;
    int host_type = th_data->host_type;
    static int static_count = 0;

    while(1)
    {
    	r_code = handle_tcp_response(&condition);
        if(CM_SUCCESS == r_code)
        {
        	acquire_read_lock();
        }

        if(ACK)
        {
        	release_write_lock();
        }

        else if(RES)
        {
        	release_read_lock();

        }


			/* Release the write lock */

		}
		else
		{
			acquire_fbk_read_lock();
			//printf("Handling READ RESPONSE %d\n", static_count);
            static_count++;
            r_code = handle_read_response(host_type);
            if(CM_SUCCESS != r_code)
            {
                if(CM_IF_NO_DATA_RECVD == r_code)
                {
                    if((HNDL_ABORT_CR_REQ == condition) ||
                        (HNDL_NMA_RESET_REQ == condition))
                    {
                        r_code = CM_SUCCESS;
                        /* Release the read lock */
                        release_read_lock();
                        printf("Host state is in ack 0x%x\n", get_device_admin_state());
                        set_error_code(get_device_admin_state(), r_code);
                    }
                }
                else
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in Handling error from FBK :%d",
                        r_code);

                    /* Release the read lock */
                    release_read_lock();
                    set_error_code(get_device_admin_state(), r_code);
                }
            }
            else if(CM_SUCCESS == r_code)
            {
                if(DEVACC_APPL_STATE_READY == get_device_admin_state())
                {
                    release_read_lock();
                    set_error_code(get_device_admin_state(), r_code);
                }
                else {
                    if((DEVACC_APPL_STATE_ABORT == get_device_admin_state()) ||
                        (DEVACC_APPL_STATE_NM_RESET == get_device_admin_state()))
                    {
                        r_code = CM_SUCCESS;
                        /* Release the read lock */
                        release_read_lock();
                        printf("Host state is in ack 0x%x\n", get_device_admin_state());
                        set_error_code(get_device_admin_state(), r_code);
                    }
                }
            }

            release_fbk_read_lock();
            usleep(500);
		}
    }
#endif

    return NULL;
}


/***********************************************************************
 *
 * Name: wait_thread_function
 *
 * ShortDesc: the thread function to wait for the read and write
 *              completion to the communcation FF stack
 *
 *
 * Description:
 *      This is the routine that performs getting command request
 *      and calling the respective request function and waits till the
 *      response is recieved from the FF Softing stack.
 *
 * Returns:
 *      none.
 *
 * Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

static void *wait_fbk_data_thread(void *arg)
{
    int r_code = 0;
    int condition;
    ACTIVE_THREAD_DATA *th_data = (ACTIVE_THREAD_DATA *) arg;
    int host_type = th_data->host_type;

	set_fbk_data_thread_state (THREAD_STATE_RUN);

    while(1)
    {
        // Acquire fbk data thread lock before processing any request
        acquire_fbk_data_thread_lock();

        // Stop processing any further request and exit the thread
        if (THREAD_STATE_STOP == get_fbk_data_thread_state())
        {
            if (is_write_lock_acquired())
                release_write_lock();

            if (is_read_lock_acquired())
                release_read_lock();

            release_fbk_data_thread_lock();
            break;
        }

        if(is_write_lock_acquired())
        {
        	r_code = handle_request(&condition);
        	if(CM_SUCCESS == r_code)
        	{
        		acquire_read_lock();
        	}
			/* Release the write lock */
			release_write_lock();
		}
		else
		{
			acquire_fbk_read_lock();

            if(HNDL_NMA_DATA_REQ  == condition)
            {
                release_read_lock();
                set_error_code(get_device_admin_state(), CM_SUCCESS);
            }
            r_code = handle_read_response(host_type);
            if(CM_SUCCESS != r_code)
            {
                if(CM_IF_NO_DATA_RECVD == r_code)
                {
                    if(HNDL_ABORT_CR_REQ == condition)
                    {
                        r_code = CM_SUCCESS;
                        /* Release the read lock */
                        release_read_lock();
                        set_error_code(get_device_admin_state(), r_code);
                    }
                }
                /*
                 * DLL Timeout condition
                 *
                 */
                else if(CM_IF_DLL_ABT_RC7 == r_code)
                {
                	flag_timeout(FLAG_ON);
                }
                else
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in Handling error from FBK :%d",
                        r_code);

                    /* Release the read lock */
                    release_read_lock();
                    set_error_code(get_device_admin_state(), r_code);
                }
            }
            else if(CM_SUCCESS == r_code)
            {
                if(DEVACC_APPL_STATE_READY == get_device_admin_state())
                {
                    release_read_lock();
                    set_error_code(get_device_admin_state(), r_code);
                }
                else {
                    if(DEVACC_APPL_STATE_ABORT == get_device_admin_state())
                    {
                        r_code = CM_SUCCESS;
                        /* Release the read lock */
                        release_read_lock();
                        set_error_code(get_device_admin_state(), r_code);
                    }
                }
            }

            release_fbk_read_lock();
		}

        // Release fbk data thread lock after processing any fbk request
        release_fbk_data_thread_lock();

        // Let's spare other thread to run !!!
        usleep(500);
    }

    return NULL;
}

/***********************************************************************
 *
 *  Name: init_comm_thread
 *
 *  ShortDesc:  Thread creation for reading the data from the transmitter device
 *
 *  Description:
 *      Thread keeps waiting for the data from the transmitter and calls
 *      the function depending upon the service layer
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_COMM_ERR
 *
 *  Author:
 *      Jayanth M
 *
 **********************************************************************/

int init_comm_thread (int host_type)
{
    int socket;
    int r_code = CM_SUCCESS;

    pthread_t *thread_handle = NULL;
    pthread_t socket_listen_handle;
    pthread_mutex_t *m_mutex = NULL;
    ACTIVE_THREAD_DATA *active_th_data = NULL;
    char *shm = NULL;
    pthread_attr_t fbk_threadAttr;
    pthread_attr_t tcp_threadAttr;

    /*
        Create the the mutex to lock and sync between
        write and read operations to the transmitter device
    */

    m_mutex = get_adt_thread_mutex();
    if(NULL == m_mutex)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the mutex\n");
        return(CM_BAD_MUTEX);
    }

    else {
        r_code = pthread_mutex_init(m_mutex, NULL);
        if(r_code != CM_SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in initialization of mutex");
            return (CM_BAD_MUTEX);
        }
    }

    /*
        Create threads in detached state (i.e., not joinable).

        The use model in DDS Communication manager is to start a thread
        and have it run independently (detached) from the thread that
        started it -- i.e., the calling thread does not wait on the new
        thread to finish execution before doing something else.

        See:
          * http://www.cs.cf.ac.uk/Dave/C/node30.html
    */
    if (0 == pthread_attr_init(&fbk_threadAttr))
    {
        if (0 != pthread_attr_setdetachstate(
                &fbk_threadAttr,
                PTHREAD_CREATE_DETACHED
            )
        )
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; detached state config %d",
                    errno);
            return (CM_BAD_THREAD_HANDLE);
        }
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; attribute initialization errno:%d",
                errno);
        return (CM_BAD_THREAD_HANDLE);
    }

    thread_handle = get_adt_thread_handle();
    if(NULL == thread_handle)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the thread ID\n");
        return(CM_BAD_THREAD_HANDLE);
    }


    active_th_data = get_adt_thread_data();
    if(NULL == active_th_data)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the thread data\n");
        return(CM_BAD_THREAD_HANDLE);
    }

    active_th_data->host_type = host_type;

    switch(host_type)
    {
    case HOST_SERVER:

		/*
		   Create and start the thread to handle the data received over
		   UDP socket.
		 */

		r_code = create_server_socket(shm, &socket);
		if(CM_SUCCESS != r_code)
		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create socket");
			return r_code;
		}
		set_device_admin_socket(socket);


	    if (0 == pthread_attr_init(&tcp_threadAttr))
	    {
	        if (0 != pthread_attr_setdetachstate(
	                &tcp_threadAttr,
	                PTHREAD_CREATE_DETACHED
	            )
	        )
	        {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; detached state config %d",
	                    errno);
	            return (CM_BAD_THREAD_HANDLE);
	        }
	    }
	    else
	    {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; attribute initialization errno:%d",
	                errno);
	        return (CM_BAD_THREAD_HANDLE);
	    }

		active_th_data->socket = socket;
		r_code = pthread_create(
				&socket_listen_handle,
				&tcp_threadAttr,
				&handle_socket_data,
				(void *)active_th_data
		);

		if (CM_SUCCESS != r_code)
		{
			thread_handle = NULL;
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Failed thread creation with status: %d",errno );
			r_code = CM_BAD_THREAD_HANDLE;
		}

        /*
           Create and start the thread.
           All threads call our _WaitThreadFunction() function and that
           in turn calls the wait handler function through the wait object
           that the thread parameter will be set up to point to.
        */
        r_code = pthread_create(
        		thread_handle,
        		&fbk_threadAttr,
        		&wait_fbk_data_thread,
        		(void *)active_th_data
        );


        if (CM_SUCCESS != r_code)
        {
            thread_handle = NULL;
            LOGC_ERROR(CPM_FF_DDSERVICE, "Failed thread creation with status: %d",errno );
            r_code = CM_BAD_THREAD_HANDLE;
        }

    	break;

    case HOST_STANDALONE:
        /*
           Create and start the thread.
           All threads call our _WaitThreadFunction() function and that
           in turn calls the wait handler function through the wait object
           that the thread parameter will be set up to point to.
        */
        r_code = pthread_create(
        		thread_handle,
        		&fbk_threadAttr,
        		&wait_fbk_data_thread,
        		(void *)active_th_data
        );


        if (CM_SUCCESS != r_code)
        {
            thread_handle = NULL;
            LOGC_ERROR(CPM_FF_DDSERVICE, "Failed thread creation with status: %d",errno );
            r_code = CM_BAD_THREAD_HANDLE;
        }

    	break;

    case HOST_CLIENT:

		r_code = create_client_socket(&socket);
		if(CM_SUCCESS != r_code)
		{
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create socket");
			return r_code;
		}

		set_device_admin_socket(socket);

        /*
           Create and start the thread.
           All threads call our _WaitThreadFunction() function and that
           in turn calls the wait handler function through the wait object
           that the thread parameter will be set up to point to.
        */
	    if (0 == pthread_attr_init(&tcp_threadAttr))
	    {
	        if (0 != pthread_attr_setdetachstate(
	                &tcp_threadAttr,
	                PTHREAD_CREATE_DETACHED
	            )
	        )
	        {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; detached state config %d",
	                    errno);
	            return (CM_BAD_THREAD_HANDLE);
	        }
	    }
	    else
	    {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create thread; attribute initialization errno:%d",
	                errno);
	        return (CM_BAD_THREAD_HANDLE);
	    }

        r_code = pthread_create(
        		thread_handle,
        		&tcp_threadAttr,
        		&wait_tcp_data_thread,
        		(void *)active_th_data
        );


        if (CM_SUCCESS != r_code)
        {
            thread_handle = NULL;
            LOGC_ERROR(CPM_FF_DDSERVICE, "Failed thread creation with status: %d",errno );
            r_code = CM_BAD_THREAD_HANDLE;
        }

    	break;

	default:
		/*
		 * Do nothing
		 */
                LOGC_ERROR(CPM_FF_DDSERVICE,"Host type not defined");
		break;

    }

    /*
     * Delay to make sure that thread has got enough time to spawn.
     */
    usleep(1000);
    pthread_attr_destroy(&fbk_threadAttr);
    if((host_type == HOST_SERVER) || (host_type == HOST_CLIENT))
    	pthread_attr_destroy(&tcp_threadAttr);

    return r_code;

}

/***********************************************************************
 *
 * Name: send_wait_recieve_cmd
 *
 * ShortDesc: send the command wait for the response and recieve the
 *              response
 *
 * Description:
 *      This is the routine that performs sending command operation and
 *      waits till write lock release, read lock release so that write
 *      and read operation shall get complete.
 *
 * Returns:
 *    device_error -- Pointer to the T_DEVICE_ERROR structure.
 *
 * Author:
 *      Jayanth
 *
 **********************************************************************/

int send_wait_receive_cmd(
        ENV_INFO *env_info,
        unsigned int condition,
        FBAP_NMA_REQ *fbap_nma_req,
        SM_REQ *sm_req)
{
    T_DEVICE_ERROR dev_err = {0};
    T_FIELDBUS_SERVICE_DESCR devacc_sdb = {0};
    int r_code = CM_SUCCESS;
    int host_type;
    int conn_check = 0;
    unsigned short ret_code = 0;
    unsigned char data[MAX_FF_DATA_LEN];
    time_t start_t, end_t;
    double diff_t;
    
    APP_INFO *app_info = (APP_INFO *)env_info->app_info;

    memset((char *)data, 0, MAX_FF_DATA_LEN);

    acquire_dpc_sync_lock();
    acquire_write_lock();

    host_type = get_device_admin_host_type();
    switch(host_type)
    {
    case HOST_CLIENT:
        r_code = set_command_request(condition, fbap_nma_req, sm_req, host_type);
        if(r_code != CM_SUCCESS){
        	goto err_ret;
        }

        r_code = recv_data_from_server(&ret_code, &dev_err, &devacc_sdb, data);
        if(r_code != CM_SUCCESS){
           	goto err_ret;
        }
   	    app_info->env_returns.response_code = dev_err.reason_code;
   	    app_info->env_returns.host_state = dev_err.host_state;
   	    app_info->env_returns.stack_service = dev_err.service;
   	    app_info->env_returns.stack_primitive = dev_err.primitive;
   	    app_info->env_returns.stack_layer = dev_err.layer;
   	    app_info->env_returns.stack_result = dev_err.result;

   	    (void *)memcpy((char *)global_res_buf, (char *)data, MAX_FF_DATA_LEN);
        set_desc_header(&devacc_sdb);
        if(CM_IF_OK == r_reason(app_info->env_returns.response_code)){
        	r_code = CM_SUCCESS;
            goto err_ret;
        }
    	r_code = r_reason(app_info->env_returns.response_code);
    	goto err_ret;

        break;

        case HOST_SERVER:
        case HOST_STANDALONE:

        	r_code = flag_timeout(FLAG_CHECK);
        	if(r_code == FLAG_ON)
        	{
        		flag_timeout(FLAG_OFF);
        		r_code = CM_IF_DLL_ABT_RC7;
        		goto err_ret;
        	}

            r_code = set_command_request(condition, fbap_nma_req, sm_req, host_type);
            if(r_code != CM_SUCCESS) {
                goto err_ret;
            }

            time(&start_t);
            while((is_write_lock_acquired()))
            {
                usleep(500);
                time(&end_t);
                diff_t = difftime(end_t, start_t);
                if(diff_t > MAX_WRITE_TIME) {
                    r_code = CM_WRITE_TIMEOUT;
                    goto err_ret;
                }
                if (SUCCESS == (*getConnectionState)())
				{
                	r_code = CM_SUCCESS;
					goto err_ret;
					break;
				}
            }

            r_code = get_return_code(&dev_err);
            if(r_code != CM_SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "Failed in sending request to field device");
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Response code: %d\n", dev_err.reason_code);
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Host state: %d\n", dev_err.host_state);
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack service: %d\n", dev_err.service);
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack primitive: %d\n", dev_err.primitive);
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack layer: %d\n", dev_err.layer);
                LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack result: %d\n", dev_err.result);
            	goto err_ret;
            }
            else
            {
                if(CM_SUCCESS != dev_err.reason_code)
                {
                    app_info->env_returns.response_code = dev_err.reason_code;
                    app_info->env_returns.host_state = dev_err.host_state;
                    app_info->env_returns.stack_service = dev_err.service;
                    app_info->env_returns.stack_primitive = dev_err.primitive;
                    app_info->env_returns.stack_layer = dev_err.layer;
                    app_info->env_returns.stack_result = dev_err.result;

                    r_code =  r_reason(app_info->env_returns.response_code);
                    goto err_ret;
                }
                else
                {
                    time(&start_t);
                    while((is_read_lock_acquired()))
                    {
                        usleep(500);
                        time(&end_t);
                        diff_t = difftime(end_t, start_t);
                        // Waiting for a maximum of 90 seconds as commissioning takes about 72 seconds
                        if(diff_t > MAX_READ_TIME) {
                            r_code = CM_READ_TIMEOUT;
                            goto err_ret;
                        }                      
                        if (SUCCESS == (*getConnectionState)())
						{
							r_code = CM_SUCCESS;
							goto err_ret;
							break;
						}
                    }
                    
                    r_code = get_error_code(&dev_err);
                    if(r_code != CM_SUCCESS) {
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Failed in receiving response from field device");
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Response code: %d\n", dev_err.reason_code);
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Host state: %d\n", dev_err.host_state);
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack service: %d\n", dev_err.service);
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack primitive: %d\n", dev_err.primitive);
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack layer: %d\n", dev_err.layer);
                        LOGC_ERROR(CPM_FF_DDSERVICE, "Error: Stack result: %d\n", dev_err.result);
                    	goto err_ret;
                    }

                    app_info->env_returns.response_code = dev_err.reason_code;
                    app_info->env_returns.host_state = dev_err.host_state;
                    app_info->env_returns.stack_service = dev_err.service;
                    app_info->env_returns.stack_primitive = dev_err.primitive;
                    app_info->env_returns.stack_layer = dev_err.layer;
                    app_info->env_returns.stack_result = dev_err.result;
                    if(CM_IF_OK == r_reason(app_info->env_returns.response_code))
                    {
                    	r_code = CM_SUCCESS;
                        goto err_ret;
                    }

                    r_code =  (app_info->env_returns.response_code);
                    goto err_ret;
                }
            }

            break;
    }
    
err_ret:
	release_write_lock();
	release_dpc_sync_lock();
	return r_code;
}

