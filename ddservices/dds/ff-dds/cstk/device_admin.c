/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Administration of device information.

    This file contains handling and maintaining of the device
    administration data structures device_admin and device_error

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ff_if.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"
#include "devsim.h"
#include "ffh1_util.h"
#include "cm_loc.h"

int global_ret_code;

/*
 * Device administration global variable
 */
static T_APPL_ADMIN                device_admin;
static T_DEVICE_ERROR              device_error, dev_ret_err;

/*
 * FF Service Descriptor Header object
 */
T_FIELDBUS_SERVICE_DESCR    devacc_sdb;

extern USIGN8                      global_res_buf[MAX_FF_DATA_LEN];

/* This call is for inform about device disconnected */
int (*getCurrentStatus) P(()) = NULL;

/* Macro Definitions */
#define NM_T_DIR_HEAD_SIZE  12 // The size of T_DIR_HEAD structure
#define NM_MAX_DIR_SIZE     36 // Maximum allowed directory entries size (9 Directory Entries each of 4 bytes)
#define NM_DIR_SIZE         4  // Size of each directory entry

void set_fbk_info(T_FBAPI_DEVICE_INFO *fbk_info)
{
    memcpy((char *)&device_admin.fbk_info,
            (char *)fbk_info, sizeof(T_FBAPI_DEVICE_INFO));
}

void get_fbk_info(T_FBAPI_DEVICE_INFO *fbk_info)
{
    memcpy((char *)fbk_info, (char *)&device_admin.fbk_info,
            sizeof(T_FBAPI_DEVICE_INFO));
}

/*
 * Set the error codes for the response packets
 */
void set_device_error(int state, int code)
{
    device_error.reason_code = code;
    device_error.host_state = state;
    device_error.service = devacc_sdb.service;
    device_error.layer = devacc_sdb.layer;
    device_error.primitive = devacc_sdb.primitive;
    device_error.result = devacc_sdb.primitive;
}

void set_rcv_error(T_DEVICE_ERROR *error)
{
    device_error.reason_code = error->reason_code;
    device_error.host_state = error->host_state;
    device_error.service = error->service;
    device_error.layer = error->layer;
    device_error.primitive = error->primitive;
    device_error.result = error->result;
}

void get_device_error(T_DEVICE_ERROR *error)
{
    error->reason_code = device_error.reason_code;
    error->host_state = device_error.host_state;
    error->service = device_error.service;
    error->layer = device_error.layer;
    error->primitive = device_error.primitive;
    error->result = device_error.result;
}

/*
 * Set the error codes for the returns
 */
void set_device_return_error(int state, int code)
{
	dev_ret_err.reason_code = code;
	dev_ret_err.host_state = state;
	dev_ret_err.service = devacc_sdb.service;
	dev_ret_err.layer = devacc_sdb.layer;
	dev_ret_err.primitive = devacc_sdb.primitive;
	dev_ret_err.result = devacc_sdb.primitive;
}

void get_device_return_error(T_DEVICE_ERROR *error)
{
    error->reason_code = dev_ret_err.reason_code;
    error->host_state = dev_ret_err.host_state;
    error->service = dev_ret_err.service;
    error->layer = dev_ret_err.layer;
    error->primitive = dev_ret_err.primitive;
    error->result = dev_ret_err.result;
}


/*
 * Set the Service Descriptor global device administration
 * variable
 */
void set_service_comm_ref(USIGN16 comm_ref)
{
    devacc_sdb.comm_ref   = comm_ref;
}

void set_service_layer(USIGN8 layer)
{
    devacc_sdb.layer      = layer;
}

void set_service_ref(USIGN8 service)
{
     devacc_sdb.service = service;
}

void set_service_primitive(USIGN8 primitive)
{
    devacc_sdb.primitive = primitive;
}

void set_service_result(FB_INT16 result)
{
    devacc_sdb.result = result;
}

void set_service_invoke_id(FB_INT8 invoke_id)
{
    devacc_sdb.invoke_id = invoke_id;
}

/*
 * Get the Service Descriptor Header data from the recieved
 * Data packet
 */
int get_size_of_desc_header(void)
{
    return(sizeof(devacc_sdb));
}

FB_INT16 get_service_result(void)
{
    return  devacc_sdb.result;
}

FB_INT16 get_service_primitive(void)
{
    return  devacc_sdb.primitive;
}

USIGN16 get_service_comm_ref(void)
{
    return devacc_sdb.comm_ref;
}

USIGN8 get_service_layer(void)
{
    return devacc_sdb.layer;
}

USIGN8 get_service_ref(void)
{
     return devacc_sdb.service;
}

FB_INT8 get_service_invoke_id(void)
{
    return devacc_sdb.invoke_id;
}

T_FIELDBUS_SERVICE_DESCR *get_desc_header(void)
{
    return (&devacc_sdb);
}

void set_desc_header(T_FIELDBUS_SERVICE_DESCR *sdb)
{
    devacc_sdb.comm_ref = sdb->comm_ref;
    devacc_sdb.layer = sdb->layer;
    devacc_sdb.service = sdb->service;
    devacc_sdb.primitive = sdb->primitive;
    devacc_sdb.invoke_id = sdb->invoke_id;
    devacc_sdb.result = sdb->result;
}

/*
 * Set the device administration state
 */
void set_device_admin_appl_state(int state)
{
    device_admin.appl_state = state;
}

void set_device_admin_state(int state)
{
    device_admin.state = state;
}

int get_device_admin_appl_state(void)
{
    return device_admin.appl_state;
}

int get_device_admin_state(void)
{
    return device_admin.state;
}

void set_device_admin_host_type(int type)
{
	device_admin.host_type = type;
}

int get_device_admin_host_type(void)
{
	return (device_admin.host_type);
}

void set_device_admin_invokeid(FB_INT8 invoke_id)
{
    device_admin.invoke_id = invoke_id;
}

FB_INT8 get_device_admin_invokeid(void)
{
    return device_admin.invoke_id;
}

void set_device_admin_proc_con(void *proc_mgmt_con)
{
    device_admin.proc_mgmt_con = (PROCESS_CON_FNC )proc_mgmt_con;
}

PROCESS_CON_FNC device_admin_proc_con(void)
{
    return device_admin.proc_mgmt_con;
}

void clear_device_admin_live_list(void)
{
    memset((char *)&device_admin.dev_list, 0, sizeof(device_admin.dev_list));
    memset(device_admin.vcr_list, 0, sizeof(device_admin.vcr_list));
}
/*
 * Set the local read and write global variable to
 * differentiate between local and remote access
 */
void set_device_admin_local_nma_read(USIGN16 local_nma_read)
{
    device_admin.local_nma_read = local_nma_read;
}

USIGN16 get_device_admin_local_nma_read(void)
{
    return(device_admin.local_nma_read);
}

void set_device_admin_channel(int channel)
{
    device_admin.channel = channel;
}


int get_device_admin_channel(void)
{
    return(device_admin.channel);
}

void set_device_admin_local_nma_write(USIGN16 local_nma_write)
{
    device_admin.local_nma_write = local_nma_write;
}

USIGN16 get_device_admin_local_nma_write(void)
{
    return (device_admin.local_nma_write);
}

void set_device_admin_vfd_nr(USIGN16 addr_val, USIGN16 cr_index)
{
    /*
     * 0xF8 is the address of the VCR reference for remote MIB
     * address, that has been written by the host device while
     * loading the MIB VCR
     */

    if(0xF8 == (addr_val & 0x00FF))
    {
        device_admin.vcr_list[cr_index].vfd_nr = VFD_NR_MIB;
    }
    else
    {
        device_admin.vcr_list[cr_index].vfd_nr = VFD_NR_FBAP;
    }
}

USIGN8 get_device_admin_vfd_nr(USIGN16 cr_index)
{
    return(device_admin.vcr_list[cr_index].vfd_nr);
}
/*
 * Set the remote device address in the device administration
 * table
 */

void set_device_admin_device_addr(USIGN16 device_addr, USIGN16 cr_index)
{
    device_admin.vcr_list[cr_index].node_address =
        (USIGN8) (device_addr / 0x100u);
}

/*
 * Get the remote device address from the device administration
 * table
 */

USIGN8 get_device_admin_device_addr(USIGN16 cr_index)
{
    return(device_admin.vcr_list[cr_index].node_address);
}

void set_device_admin_type_role(USIGN8 type_role, USIGN16 cr_index)
{
    device_admin.vcr_list[cr_index].type_and_role = type_role;
}

/*
 * Set the serial device connection state
 */
void set_device_admin_conn(FB_BOOL conn)
{
    device_admin.conn_open = conn;
}

/*
 * Set the address of the Host device
 */
void set_device_admin_host_addr(USIGN16 device_addr)
{
    device_admin.host_node_addr = device_addr;
}

/*
 * Get the address of the Host device
 */
USIGN8 get_device_admin_host_addr(void)
{
    return(device_admin.host_node_addr);
}


/*
 * Get the VFD reference of the remote device
 */

USIGN32 get_device_admin_remote_vfd(void)
{
    return(device_admin.dev_list.vfd_list[1].vfd_ref);
}

/*
 * Get the stored remote device information from the device_admin data
 * structure
 */
USIGN32 get_device_admin_manufacturer(void)
{
    return(device_admin.dev_list.dev_info.manufacturer_id);
}

USIGN16 get_device_admin_device_type(void)
{
    return(device_admin.dev_list.dev_info.device_type);
}

USIGN8 get_device_admin_device_rev(void)
{
    return(device_admin.dev_list.dev_info.device_rev);
}

USIGN8 get_device_admin_dd_rev(void)
{
    return(device_admin.dev_list.dev_info.dd_rev);
}

USIGN16 get_device_admin_rb_index(void)
{
    return(device_admin.dev_list.rb.index);
}

USIGN16 get_device_admin_tb_index(int count)
{
    return(device_admin.dev_list.tb[count].index);
}

USIGN16 get_device_admin_max_entries(void)
{
    return(device_admin.dev_list.vcr_info.max_entries);
}

USIGN16 get_device_admin_tb_entries(void)
{
    return(device_admin.dev_list.block_list[FBAP_DIR_TB_DIR_INDEX].n_entries);
}

USIGN16 get_device_admin_fb_entries(void)
{
    return(device_admin.dev_list.block_list[FBAP_DIR_FB_DIR_INDEX].n_entries);
}

USIGN16 get_device_admin_fb_index(int count)
{
    return(device_admin.dev_list.fb[count].index);
}

/*
 * Set the state of the connected device
 */
void set_admin_dev_state(T_DEVACC_STATE device_state)
{
    device_admin.dev_list.device_state = device_state;
}

/*
 * Get the state of the connected device
 */
T_DEVACC_STATE get_admin_dev_state(void)
{
    return(device_admin.dev_list.device_state);
}

/*
 * Get the type and role of the remote device
 */
USIGN8 get_admin_dev_type_role(USIGN16 cr_index)
{
    return(device_admin.dev_list.vcr_entry[cr_index].type_and_role);
}

/*
 * Get the NM base directory index of the remote device
 */
USIGN16 get_admin_dev_nm_first_index(void)
{
    return(device_admin.dev_list.nm_directory[NM_DIR_VCR_LIST].first_index);
}

/*
 * Get the local address stored at the remote devce
 */
USIGN16 get_admin_dev_loc_addr(USIGN16 cr_index)
{
    return(device_admin.dev_list.vcr_entry[cr_index].loc_addr);
}

/*
 * Get the remote address stored at the remote devce
 */
USIGN16 get_admin_dev_rem_addr(USIGN16 cr_index)
{
    return(device_admin.dev_list.vcr_entry[cr_index].rem_addr);
}

/*
 * Get the remote sdap stored at the remote devce
 */
USIGN8 get_admin_dev_sdap(USIGN16 cr_index)
{
    return(device_admin.dev_list.vcr_entry[cr_index].sdap);
}

/*
 * Get the idex to read/scan the list of the devices connected in the network
 */
USIGN16 get_index_to_scan(void)
{
    return(device_admin.host_stack_mib_directory.dlme_lm_idx + 3);
}

/*
 * Save the Host Stack MIB directory information inf the global
 * Data structure.
 */

void save_device_admin_stack_mib_dir(T_NMA_INFO *nma_info)
{
    memcpy((char *)&device_admin.host_stack_mib_directory,
            (char *)nma_info,
            sizeof(T_NMA_INFO));
}

void get_device_admin_stack_mib_dir(T_NMA_INFO *nma_info)
{
    memcpy((char *)nma_info, (char *)&device_admin.host_stack_mib_directory,
            sizeof(T_NMA_INFO));
}


void set_device_admin_aux(USIGN8 val)
{
    device_admin.aux = val;
}

USIGN8 get_device_admin_aux(void)
{
    return(device_admin.aux);
}

void set_device_admin_socket(int socket)
{
	device_admin.socket = socket;
}

int get_device_admin_socket(void)
{
	return(device_admin.socket);
}

/*
 * Send the request command to FBK library for accessing
 * Device information or send the response data to the
 * device which has queried to the host by the remote device
 */

/***********************************************************************
 *
 *  Name: device_send_req_res
 *
 *  ShortDesc:  The function to Send the data packet either
 *  of request or response to FBK FF library.
 *
 *  Description:
 *      The device_send_req_res takes the input send_data,
 *      the data packet formed to transmit to the device.
 *      This function calls fbk_host_snd_req_res, Softing
 *      FF FBK API to talk to FBK hardware.
 *
 *  Inputs:
 *      send_data -- Data Packet to transmit to the device
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return Errors from function fbkhost_snd_req_res
 *
 *  Author:
 *      Softing Solution
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int device_send_req_res(unsigned char *send_data)
{
    FB_INT16 r_code;

    r_code = fbapi_snd_req_res(
            device_admin.channel, &devacc_sdb, send_data);
    if(r_code != CM_SUCCESS)
    {
        log_interface_error(REQ, r_code);

        if((r_code != E_IF_SERVICE_CONSTR_CONFLICT) &&
             (r_code != E_IF_INVALID_COMM_REF))
         {
             set_device_admin_state(DEVACC_APPL_STATE_END);
         }
    }

    memset((char *)send_data, 0, MAX_FF_DATA_LEN);
    return r_code;
}


/***********************************************************************
 *
 *  Name: check_device_list
 *
 *  ShortDesc:  Check the device list before adding or deleting device address
 *              from the device list
 *
 *  Description:
 *       Check the device list and log accordingly if the device is previously
 *       found while adding into the device list or deleting the device
 *       from the device list.
 *
 *  Inputs:
 *      dev_tbl -- Device table
 *      node_address -- Address of the remote device.
 *
 *  Outputs:
 *      Logs ibto the logger file
 *
 *  Returns:
 *      None
 *
 *  Author:
 *      Softing Solution
 *      Jayanth Mahadeva
 *
 **********************************************************************/

void check_device_list(USIGN8 node_address, SCAN_DEV_TBL *dev_tbl, int alive)
{
    int count;
    SCAN_TBL *list = dev_tbl->list;
    int avail = 0;

    if ((node_address >= 0x10))/* && (node_address <= 247))*/
    {
        if (alive)
        {
            for(count = 0; count < dev_tbl->count; count++)
            {
                if((list->station_address != 0) &&
                        (list->station_address == node_address))
                {
                    printf("Device with node address 0x%02x already \
                            in device list", node_address);
                }
            }
        }
        else
        {
            for(count = 0; count < dev_tbl->count; count++)
            {
                if((list->station_address != 0) &&
                        (list->station_address == node_address))
                {
                    printf("Device removed from the list 0x%02x", \
                            node_address);
                    avail = 1;
                }
            }

            if(avail)
            {
                printf("Device with node address 0x%02x was not in \
                        list", node_address);
            }
        }
    }
}

/***********************************************************************
 *
 *  Name: devadm_save_od_header_info
 *
 *  ShortDesc:  Save the OD header of the device
 *
 *  Description:
 *       Save the OD header for the further access of the device using the
 *       index's of the OD header for the FBAP directory and MIB directory.
 *
 *  Inputs:
 *      comm_ref -- Communication reference number
 *      node_address -- Address of the remote device.
 *      vfd_id -- MIB or FBAP VFD reference
 *      p_packed_od_descr -- Object description
 *
 *  Outputs:
 *      Save the information in global data structure device_admin
 *
 *  Returns:
 *      None
 *
 *  Author:
 *      Softing Solution
 *
 **********************************************************************/

int devadm_save_od_header_info(
        USIGN16 comm_ref,
        USIGN8 node_address,
        USIGN8 vfd_id,
        T_PACKED_OBJECT_DESCR *p_packed_od_descr,
        USIGN16 *index
)
{
    T_H1_OD_DESCRIPTION    *p_od_header;
    USIGN16                 first_index = 0;
    int                     r_code;

    p_od_header = (T_H1_OD_DESCRIPTION *)(p_packed_od_descr + 1);

    if ((node_address >= 0x10) && (node_address <= 247))
    {
        /* convert value to intel format */
        first_index = (p_od_header->FirstIndex_S_OD / 0x100) +
                      ((p_od_header->FirstIndex_S_OD & 0xFFu) * 0x100u);

        if ((vfd_id == VFD_NR_MIB) &&
            (device_admin.dev_list.device_state ==
             DEVACC_STATE_GET_MGMT_OD_HDR))
        {
            device_admin.dev_list.start_s_od_mib = first_index;
            r_code = save_od_header(devacc_sdb.comm_ref, global_res_buf);
            if(r_code != CM_SUCCESS)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Error saving MIB OD header\n");
                return r_code;
            }

            device_admin.dev_list.device_state   = DEVACC_STATE_READ_SM_DIR;
        }
        else if ((vfd_id == VFD_NR_FBAP) &&
                 (device_admin.dev_list.device_state ==
                  DEVACC_STATE_GET_FBAP_OD_HDR))
        {

            r_code = save_od_header(devacc_sdb.comm_ref, global_res_buf);
            if(r_code != CM_SUCCESS)
            {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Error saving FBAP OD header\n");
                return r_code;
            }

            device_admin.dev_list.start_s_od_fbap_vfd = first_index;
            device_admin.dev_list.device_state  = DEVACC_STATE_READ_AP_DIR;
        }
        else
        {
            first_index = 0;
        }
    }

    if (first_index != 0)
    {
        device_admin.dev_list.index = 0;
    }

    *index = first_index;

    return CM_SUCCESS;
}

// --------------------------------------------------------------------------
int device_admin_read_sm_dir(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index,
        USIGN16 cr_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;
    USIGN8              temp;
    NT_COUNT            nt_count;
    NETWORK_HANDLE      nh;
    USIGN8              node_address;
    int                 i,j;

    node_address = get_device_admin_device_addr(cr_index);
    r_code = get_nt_net_count(&nt_count);
    if(CM_SUCCESS != r_code)
    {
        return CM_COMM_ERR;
    }

    r_code = get_nt_net_handle_by_addr(nt_count, node_address, &nh);
    if(CM_SUCCESS != r_code)
    {
        return CM_COMM_ERR;
    }

    p_data = (USIGN8*)(p_read_con + 1);

    if ((p_read_con->length == 12 + 20) || (p_read_con->length == 12 + 28))
    {
        device_admin.dev_list.sm_dir_header.dir_rev_number =
                                        p_data[2]*0x100u + p_data[3];
        device_admin.dev_list.sm_dir_header.n_dir_objs     =
                                        p_data[4]*0x100u + p_data[5];
        device_admin.dev_list.sm_dir_header.n_dir_entries  =
                                        p_data[6]*0x100u + p_data[7];
        device_admin.dev_list.sm_dir_header.n_first_comp_list_dir_index =
                                        p_data[8]*0x100u + p_data[9];
        device_admin.dev_list.sm_dir_header.n_comp_lists   =
                                       p_data[10]*0x100u + p_data[11];

        for (i = 12, j = 0; i < p_read_con->length; i += 4, j++)
        {
            if (j >= N_SM_DIR_ENTRIES)
            {
                break;
            }
            device_admin.dev_list.sm_directory[j].first_index =
                                    p_data[i]*0x100u + p_data[i+1];
            device_admin.dev_list.sm_directory[j].n_entries   =
                                    p_data[i+2]*0x100u + p_data[i+3];
        }

        for(i = 2; i < p_read_con->length; i+=2)
        {
            temp        =   p_data[i];
            p_data[i]   =   p_data[i+1];
            p_data[i+1] =   temp;
        }

        r_code = set_nt_net_sm_dir(nh, (T_SM_DIR *)&p_data[0]);

        device_admin.dev_list.device_state = DEVACC_STATE_READ_NM_DIR;
        *next_index = device_admin.dev_list.start_s_od_mib+1;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid SM directory length %d\n",
                                                p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_INVALID_SM_DIR;
    }

    return r_code;
}

// ---------------------------------------------------------------------------
int device_admin_read_nm_dir (T_VAR_READ_CNF *p_read_con,
        USIGN16 *next_index,  USIGN16 cr_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;
    USIGN8              temp;
    NT_COUNT            nt_count;
    NETWORK_HANDLE      nh;
    USIGN8              node_address;
    int                 i,j;

    node_address = get_device_admin_device_addr(cr_index);
    r_code = get_nt_net_count(&nt_count);
    if(CM_SUCCESS != r_code)
    {
        return CM_COMM_ERR;
    }

    r_code = get_nt_net_handle_by_addr(nt_count, node_address, &nh);
    if(CM_SUCCESS != r_code)
    {
        return CM_COMM_ERR;
    }

    p_data = (USIGN8*)(p_read_con + 1);

    //if (p_read_con->length == 12 + 36)
        LOGC_INFO(CPM_FF_DDSERVICE,"FTK: NM directory length: %d", p_read_con->length);

    // TODO: The length is getting changed depending upon the directory entries
    // For example: 
    //      FTK (Test Kit) - 
    //          Has 8 directory entries (each of 4 bytes - 2 bytes (Index) + 2 bytes (Number of entries)) + Header 12 bytes = 44 bytes
    //      Some transmitters - 
    //          Has 9 directory entries (each of 4 bytes - 2 bytes (Index) + 2 bytes (Number of entries)) + Header 12 bytes = 48 bytes
    // In the below code, 12 (in bytes) - indicates the size of T_DIR_HEAD structure 
    //                    36 (in bytes) - indicates currently maximum allowed directory entries (9 Directory Entries) each of 4 bytes size 

    if ((p_read_con->length >= NM_T_DIR_HEAD_SIZE) && (p_read_con->length <= (NM_T_DIR_HEAD_SIZE + NM_MAX_DIR_SIZE)))
    {
        device_admin.dev_list.nm_dir_header.dir_rev_number =
                                        p_data[2]*0x100u + p_data[3];
        device_admin.dev_list.nm_dir_header.n_dir_objs     =
                                        p_data[4]*0x100u + p_data[5];
        device_admin.dev_list.nm_dir_header.n_dir_entries  =
                                        p_data[6]*0x100u + p_data[7];
        device_admin.dev_list.nm_dir_header.n_first_comp_list_dir_index =
                                        p_data[8]*0x100u + p_data[9];
        device_admin.dev_list.nm_dir_header.n_comp_lists   =
                                       p_data[10]*0x100u + p_data[11];

        for (i = NM_T_DIR_HEAD_SIZE, j = 0; i < p_read_con->length; i += NM_DIR_SIZE, j++)
        {
            if (j >= N_NM_DIR_ENTRIES)
            {
                break;
            }
            device_admin.dev_list.nm_directory[j].first_index =
                                    p_data[i]*0x100u + p_data[i+1];
            device_admin.dev_list.nm_directory[j].n_entries   =
                                    p_data[i+2]*0x100u + p_data[i+3];
        }

        for(i = 2; i < p_read_con->length; i+=2)
        {
            temp        =   p_data[i];
            p_data[i]   =   p_data[i+1];
            p_data[i+1] =   temp;
        }

        r_code = set_nt_net_nm_dir(nh, (T_NM_DIR *)&p_data[0]);

        device_admin.dev_list.device_state =
                                       DEVACC_STATE_READ_VFD_REF_LIST;
        device_admin.dev_list.index = 0;

        *next_index = device_admin.dev_list.sm_directory[SM_DIR_VFD_LIST].first_index;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid NM directory length %d\n",
                                                p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_INVALID_NM_DIR;
    }
    return r_code;
}

// -----------------------------------------------------------------------------
int device_admin_read_vfd_ref (T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    device_admin.dev_list.vfd_list[device_admin.dev_list.index].vfd_ref =
        p_data[0]*0x1000000uL + p_data[1]*0x10000uL + p_data[2]*0x100uL + p_data[3];

    memcpy (device_admin.dev_list.vfd_list[device_admin.dev_list.index].vfd_tag,
            &p_data[4], SM_TAG_LEN);
    device_admin.dev_list.index++;
    if (device_admin.dev_list.index <
        device_admin.dev_list.sm_directory[SM_DIR_VFD_LIST].n_entries)
    {
        *next_index = device_admin.dev_list.sm_directory[SM_DIR_VFD_LIST].first_index +
              device_admin.dev_list.index;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_VFD_REF_LIST;
    }
    else
    {
        device_admin.dev_list.index = 0;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_VCR_CHARACT;
        *next_index = device_admin.dev_list.nm_directory[NM_DIR_VCR_LIST].first_index + 1;
    }

    return r_code;
}

// -----------------------------------------------------------------------------
int device_admin_read_vcr_char (T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 16)
    {
        device_admin.dev_list.vcr_info.max_entries =
            p_data[4]*0x100u + p_data[5];
        device_admin.dev_list.vcr_info.num_perm_entries =
            p_data[6]*0x100u + p_data[7];
        device_admin.dev_list.vcr_info.num_config_entries =
            p_data[8]*0x100u + p_data[9];
        device_admin.dev_list.vcr_info.first_unconfig_entry =
            p_data[10]*0x100u + p_data[11];

        device_admin.dev_list.index = 0;
        device_admin.dev_list.device_state = DEVACC_STATE_SCAN_VCR_LIST;

        *next_index = device_admin.dev_list.nm_directory[NM_DIR_VCR_LIST].first_index +
                      device_admin.dev_list.index + 2;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid VCR characteristic length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_INVALID_VCR_CHAR;
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_scan_vcr_list (T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;
    unsigned short      remote_address;
    int rc;

    p_data = (USIGN8*)(p_read_con + 1);

    if (device_admin.dev_list.index < N_VCR_ENTRIES)
    {
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].type_and_role =
            p_data[0];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].loc_addr =
            p_data[3]*0x100uL + p_data[4];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].rem_addr =
            p_data[7]*0x100uL + p_data[8];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].sdap =
            p_data[9];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].features[0] =
            p_data[36];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].features[1] =
            p_data[37];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].features[2] =
            p_data[38];
        device_admin.dev_list.vcr_entry[device_admin.dev_list.index].features[3] =
            p_data[39];

        /* Copying the VCR entry value when the object is missing in some cases like
           uncommission, Utthunga has made changes by Subtracting previous local
           address with 0x79 to match the next VCR entry
        */
    
        if (device_admin.dev_list.index == 1)
		{
		    if (device_admin.dev_list.vcr_entry[device_admin.dev_list.index].sdap == 0)
            {
                device_admin.dev_list.vcr_entry[device_admin.dev_list.index].type_and_role
                    = device_admin.dev_list.vcr_entry[0].type_and_role;
                device_admin.dev_list.vcr_entry[device_admin.dev_list.index].sdap
                    = device_admin.dev_list.vcr_entry[0].sdap;
                device_admin.dev_list.vcr_entry[device_admin.dev_list.index].loc_addr
                    = device_admin.dev_list.vcr_entry[0].loc_addr - DEVACC_APPL_ADD_VCR_ENTRY;
            }
		}

        // CPMHACK: It's in assumption that MIB VCR is configured properly in remote device with both Node Designator and Selector address
        // For FBAP VCR, some devices doesn't have node designator configured and for this case, we are getting node address from MIB VCR and
        // copying it to FBAP VCR's
        rc = get_nt_net_mib_addr(network_tbl.count - 1, &remote_address);
        if ((AR_TYPE_SERVER == device_admin.dev_list.vcr_entry[device_admin.dev_list.index].type_and_role) &&
            (0 == (device_admin.dev_list.vcr_entry[device_admin.dev_list.index].loc_addr >> 8)) && 
            (CM_SUCCESS == rc))
        {
             device_admin.dev_list.vcr_entry[device_admin.dev_list.index].loc_addr |= (remote_address & 0xFF00);
             LOGC_INFO(CPM_FF_DDSERVICE, "Modified local address to %x as the remote address is not configued in VCR", 
                device_admin.dev_list.vcr_entry[device_admin.dev_list.index].loc_addr);
        }
    }

    device_admin.dev_list.index++;
    if (device_admin.dev_list.index < device_admin.dev_list.vcr_info.max_entries)
    {
        *next_index = device_admin.dev_list.nm_directory[NM_DIR_VCR_LIST].first_index +
                        device_admin.dev_list.index + 2;
        device_admin.dev_list.device_state = DEVACC_STATE_SCAN_VCR_LIST;
    }
    else
    {
        device_admin.dev_list.index = 0;
        device_admin.dev_list.device_state = DEVACC_STATE_WRITE_VCR_ENTRY;
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_appl_dir(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;
    int                 copy_index = 0;
    USIGN16             dir_index;
    int                 i,j;

    p_data = (USIGN8*)(p_read_con + 1);

    if (device_admin.dev_list.index == 0)
    {
        /* Directory header, to findout out how many directory objects to be read */
        if (p_read_con->length >= 12 + 20)
        {
            device_admin.dev_list.fbap_dir_header.dir_rev_number =
                            p_data[2]*0x100u + p_data[3];
            device_admin.dev_list.fbap_dir_header.n_dir_objs     =
                            p_data[4]*0x100u + p_data[5];
            device_admin.dev_list.fbap_dir_header.n_dir_entries  =
                            p_data[6]*0x100u + p_data[7];
            device_admin.dev_list.fbap_dir_header.n_first_comp_list_dir_index =
                            p_data[8]*0x100u + p_data[9];
            device_admin.dev_list.fbap_dir_header.n_comp_lists   =
                            p_data[10]*0x100u + p_data[11];
            device_admin.dev_list.fbap_dir_entry = 0;
            copy_index = 12;

        }
    }

    /* copy the directory object content */
    for (i = copy_index, j = device_admin.dev_list.fbap_dir_entry; i < p_read_con->length; i += 4, j++)
    {
        if (j < N_FBAP_DIR_ENTRIES)
        {
            device_admin.dev_list.fbap_directory[j].first_index = p_data[i]*0x100u + p_data[i+1];
            device_admin.dev_list.fbap_directory[j].n_entries   = p_data[i+2]*0x100u + p_data[i+3];
        }
    }

    /* store destination index for next copy action if additional directory object */
    device_admin.dev_list.fbap_dir_entry = j;

    /* increment counter for directory objects */
    device_admin.dev_list.index++;

    if (device_admin.dev_list.index >= device_admin.dev_list.fbap_dir_header.n_dir_objs)
    {
        /* n_first_comp_list_dir_index is the array index in directory object array -> calculate
           directory entry index from it: header uses 6 array entries, directory entry uses 2 */
        if (((device_admin.dev_list.fbap_dir_header.n_first_comp_list_dir_index % 2) != 1) ||
            (device_admin.dev_list.fbap_dir_header.n_first_comp_list_dir_index <= 7u))
        {
            printf("invalid index n_first_comp_list_dir_index %d\n", device_admin.dev_list.fbap_dir_header.n_first_comp_list_dir_index);
            return CM_COMM_ERR;
        }
        else
        {
            dir_index = (device_admin.dev_list.fbap_dir_header.n_first_comp_list_dir_index - 7u) / 2u;
            /* read the directory indices for resource block, transducer blocks, function blocks */
            for (i = 0; i <= FBAP_DIR_FB_DIR_INDEX; i++)
            {
                device_admin.dev_list.block_list[i].first_index =
                    (device_admin.dev_list.fbap_directory[dir_index+i].first_index - 7u) / 2u;
                device_admin.dev_list.block_list[i].n_entries =
                    device_admin.dev_list.fbap_directory[dir_index+i].n_entries;
            }

            dir_index = device_admin.dev_list.block_list[FBAP_DIR_RB_DIR_INDEX].first_index;
            device_admin.dev_list.rb.index   = device_admin.dev_list.fbap_directory[dir_index].first_index;
            device_admin.dev_list.rb.n_param = device_admin.dev_list.fbap_directory[dir_index].n_entries;

            /* now read transducer block and function block indices */
            dir_index = device_admin.dev_list.block_list[FBAP_DIR_TB_DIR_INDEX].first_index;
            for (i = 0; i < device_admin.dev_list.block_list[FBAP_DIR_TB_DIR_INDEX].n_entries; i++)
            {
                device_admin.dev_list.tb[i].index   = device_admin.dev_list.fbap_directory[dir_index+i].first_index;
                device_admin.dev_list.tb[i].n_param = device_admin.dev_list.fbap_directory[dir_index+i].n_entries;
            }

            dir_index = device_admin.dev_list.block_list[FBAP_DIR_FB_DIR_INDEX].first_index;
            for (i = 0; i < device_admin.dev_list.block_list[FBAP_DIR_FB_DIR_INDEX].n_entries; i++)
            {
                device_admin.dev_list.fb[i].index   = device_admin.dev_list.fbap_directory[dir_index+i].first_index;
                device_admin.dev_list.fb[i].n_param = device_admin.dev_list.fbap_directory[dir_index+i].n_entries;
            }
        }

        /* start to read the block headers */
        device_admin.dev_list.index = 0;
        *next_index = device_admin.dev_list.tb[0].index;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_TB_HDR;
    }
    else
    {
        *next_index = device_admin.dev_list.start_s_od_fbap_vfd + device_admin.dev_list.index;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_AP_DIR;
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_tb(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 62)
    {
        device_admin.dev_list.tb[device_admin.dev_list.index].dd_member_id =
            p_data[32]*0x1000000uL + p_data[33]*0x10000uL + p_data[34]*0x100uL + p_data[35];
        device_admin.dev_list.tb[device_admin.dev_list.index].dd_item_id =
            p_data[36]*0x1000000uL + p_data[37]*0x10000uL + p_data[38]*0x100uL + p_data[39];
        device_admin.dev_list.tb[device_admin.dev_list.index].profile_number =
            p_data[42]*0x100uL + p_data[43];
        device_admin.dev_list.tb[device_admin.dev_list.index].profile_rev =
            p_data[44]*0x100uL + p_data[45];

        device_admin.dev_list.index++;
        if (device_admin.dev_list.index <
                device_admin.dev_list.block_list[FBAP_DIR_TB_DIR_INDEX].n_entries)
        {
            *next_index = device_admin.dev_list.tb[device_admin.dev_list.index].index;
            device_admin.dev_list.device_state = DEVACC_STATE_READ_TB_HDR;
        }
        else
        {
            device_admin.dev_list.index = 0;
            *next_index = device_admin.dev_list.fb[0].index;
            device_admin.dev_list.device_state = DEVACC_STATE_READ_FB_HDR;
        }
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_fb(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 62)
    {
        device_admin.dev_list.fb[device_admin.dev_list.index].dd_member_id =
            p_data[32]*0x1000000uL + p_data[33]*0x10000uL + p_data[34]*0x100uL + p_data[35];
        device_admin.dev_list.fb[device_admin.dev_list.index].dd_item_id =
            p_data[36]*0x1000000uL + p_data[37]*0x10000uL + p_data[38]*0x100uL + p_data[39];
        device_admin.dev_list.fb[device_admin.dev_list.index].profile_number =
            p_data[42]*0x100uL + p_data[43];
        device_admin.dev_list.fb[device_admin.dev_list.index].profile_rev =
            p_data[44]*0x100uL + p_data[45];

        device_admin.dev_list.index++;
        if (device_admin.dev_list.index <
                device_admin.dev_list.block_list[FBAP_DIR_FB_DIR_INDEX].n_entries)
        {
            *next_index = device_admin.dev_list.fb[device_admin.dev_list.index].index;
            device_admin.dev_list.device_state = DEVACC_STATE_READ_FB_HDR;
        }
        else
        {
            device_admin.dev_list.index = 0;
            *next_index = device_admin.dev_list.rb.index;
            device_admin.dev_list.device_state = DEVACC_STATE_READ_RB_HDR;
        }
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_rb(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    int                 r_code = CM_SUCCESS;

    if (p_read_con->length == 62)
    {
        device_admin.dev_list.index = 0;
        *next_index = device_admin.dev_list.rb.index + 10;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_MFG_ID;
    }
    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_manufac(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 4)
    {
        device_admin.dev_list.dev_info.manufacturer_id =
            p_data[0]*0x1000000uL + p_data[1]*0x10000uL + p_data[2]*0x100uL + p_data[3];

        *next_index = device_admin.dev_list.rb.index + 11;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_DEV_TYPE;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid MFG ID length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_BAD_MANUFACTURER;
    }
    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_dev_type(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 2)
    {
        device_admin.dev_list.dev_info.device_type = p_data[0]*0x100uL + p_data[1];

        *next_index = device_admin.dev_list.rb.index + 12;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_DEV_REV;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid Dev Type length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_BAD_DEV_TYPE;
    }

    return r_code;
}


// --------------------------------------------------------------------------------
int device_admin_read_dev_rev(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 1)
    {
        device_admin.dev_list.dev_info.device_rev = p_data[0];

        *next_index = device_admin.dev_list.rb.index + 13;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_DD_REV;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid Dev Rev length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_BAD_DEV_REV;
    }
    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_dd_rev(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);
    if (p_read_con->length == 1)
    {
        device_admin.dev_list.dev_info.dd_rev = p_data[0];

        *next_index = device_admin.dev_list.rb.index + 21;
        device_admin.dev_list.device_state = DEVACC_STATE_READ_MIN_CYCLE_T;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid DD Rev length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_BAD_DD_REV;
    }

    return r_code;
}

// --------------------------------------------------------------------------------
int device_admin_read_cycle(T_VAR_READ_CNF *p_read_con, USIGN16 *next_index)
{
    USIGN8              *p_data;
    int                 r_code = CM_SUCCESS;

    p_data = (USIGN8*)(p_read_con + 1);

    if (p_read_con->length == 4)
    {
        device_admin.dev_list.dev_info.min_cycle_time =
            p_data[0]*0x1000000uL + p_data[1]*0x10000uL + p_data[2]*0x100uL + p_data[3];

        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        device_admin.state = DEVACC_APPL_STATE_READY;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"invalid MIN_CYCLE_TIME length %d\n", p_read_con->length);
        device_admin.dev_list.device_state = DEVACC_STATE_IDLE;
        return CM_BAD_CYCLE_TIME;
    }

    return r_code;
}

void set_device_admin_manufac(int manufacturer)
{
	device_admin.dev_list.dev_info.manufacturer_id =
			(USIGN32)manufacturer;
}
void set_device_admin_dev_type(int dev_type)
{
	device_admin.dev_list.dev_info.device_type = (USIGN16)dev_type;
}
void set_device_admin_dd_rev(int dd_rev)
{
	device_admin.dev_list.dev_info.dd_rev = (USIGN8)dd_rev;
}
void set_device_admin_dev_rev(int dev_rev)
{
	device_admin.dev_list.dev_info.device_rev = (USIGN8)dev_rev;
}

void get_device_admin_data(T_APPL_ADMIN *appl_admin)
{
	(void *)memcpy((char *) appl_admin, (char *)&device_admin, sizeof(T_APPL_ADMIN));
}

void set_device_admin_data(T_APPL_ADMIN *appl_admin)
{
	int socket;
	int host_type;

	socket = get_device_admin_socket();
	host_type = get_device_admin_host_type();

	(void *)memcpy((char *)&device_admin, (char *)appl_admin, sizeof(T_APPL_ADMIN));

	set_device_admin_socket(socket);
	set_device_admin_host_type(host_type);
}

void  save_fbap_current_index(OBJECT_INDEX object_index, SUBINDEX sub_index, USIGN16 cr)
{
	device_admin.curr_param_index.index = object_index;
	device_admin.curr_param_index.sub_index = sub_index;
	device_admin.curr_param_index.cr = cr;
}

void  get_fbap_current_index(OBJECT_INDEX *object_index, SUBINDEX *sub_index, USIGN16 *cr)
{
	*object_index = device_admin.curr_param_index.index;
	*sub_index = device_admin.curr_param_index.sub_index;
	*cr = device_admin.curr_param_index.cr;
}


int handle_cr_timeout(int code, ENV_INFO *env_info, unsigned int condition,
		FBAP_NMA_REQ *fbap_nma_req, SM_REQ *sm_req)
{
	int 			r_code;
	FBAP_NMA_REQ 	cr_abt_nma_req = { 0 };
	UINT16          mib_cr = 0;
	UINT16          fbap_cr = 0;

	/* From DDSController to inform that device has disconnected. */
	 if (0 ==(*getCurrentStatus)())
	 {
		 // send a error code device connection has aborted.
		 return CM_IF_USR_ABT_RC1;
	 }

	if(CM_IF_DLL_ABT_RC7 == code)
	{
        r_code = get_nt_net_mib_cr(0, &mib_cr);
		if (r_code != SUCCESS) {
			return(r_code) ;
		}

		cr_abt_nma_req.cr_index = mib_cr;
        set_device_admin_state(DEVACC_APPL_STATE_ABORT);
        
        r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ, 
                &cr_abt_nma_req, NULL);

        if(CM_SUCCESS != r_code) {
            return(r_code) ;
        }

		r_code = get_nt_net_fbap_cr(0, &fbap_cr);
		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}

        cr_abt_nma_req.cr_index = fbap_cr;
        set_device_admin_state(DEVACC_APPL_STATE_ABORT);
        r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ, 
                &cr_abt_nma_req, NULL);
        if(CM_SUCCESS != r_code) {
            return(r_code) ;
        }      
        
        
		/*
		 * Initiate the MIB CR
		 */

		cr_abt_nma_req.cr_index = mib_cr;
		set_device_admin_state(DEVACC_APPL_STATE_FMS_INITIATE);
        set_device_admin_conn(FB_FALSE);
		r_code = send_wait_receive_cmd(env_info, INITIATE_REQUEST, &cr_abt_nma_req,
				NULL);

		if(CM_SUCCESS != r_code) {
			return(r_code) ;
		}

		/*
		 * Initiate the FBAP CR
		 */

		set_device_admin_state(DEVACC_APPL_STATE_FMS_INITIATE);
		cr_abt_nma_req.cr_index = fbap_cr;
		r_code = send_wait_receive_cmd(env_info, INITIATE_REQUEST,
				&cr_abt_nma_req, NULL);
		if(CM_SUCCESS != r_code) {
			return((r_code)) ;
		}

		/*
		 * Transmit the command after re-initialization
		 */
		r_code = send_wait_receive_cmd(env_info, condition, fbap_nma_req, sm_req);
		if(CM_SUCCESS != r_code) {
			return((r_code)) ;
		}
    }

	return CM_SUCCESS;
}

