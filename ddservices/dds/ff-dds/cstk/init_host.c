/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Host configuration module.

    This file contains functions pertaining to configuring the host
    either as client mode or server mode

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "fakexmtr.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

int initialize_host(HOST_CONFIG *hc)
{
    T_FBAPI_DEVICE_INFO   device_info;
    STRINGV         *comport_name = NULL;
    T_FBAPI_INIT  ffboard_init;
    const int       MAX_TRIES = 10;
    int             tries = 0;
    int             channel = 0;
    int 			r_code;
    USIGN16         add_detail = 0;

    memset (&ffboard_init, '\0', sizeof(T_FBAPI_INIT));
    ffboard_init.serial.ComportName = (char *) malloc (TTY_PORT_BUFFER_LEN * sizeof(char));
    memset(ffboard_init.serial.ComportName, '\0', TTY_PORT_BUFFER_LEN);

    if(NULL == hc)
    {
        comport_name = getenv(WALLACE_FBK2_PORT);
        if(NULL == comport_name)
        {
            printf("Failed to get comport env variable\n");
            return (CM_COMM_ERR);
        }
        (void)strcpy(ffboard_init.serial.ComportName, comport_name);
    }
    else
    {
        (void)strcpy(ffboard_init.serial.ComportName, hc->tty_port);
    }

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Using the COM Port: %s", ffboard_init.serial.ComportName);

	channel = get_device_admin_channel();

        LOGC_INFO(CPM_FF_DDSERVICE,"Sent: init request. Size of the Data in bytes = %d",
			sizeof(ffboard_init) + sizeof(channel));
 
    // Initialize the communication channel with FBK board
	r_code = fbapi_init (channel, PROTOCOL_FF, &ffboard_init);
	if (r_code != E_OK)
	{
        if (NULL != ffboard_init.serial.ComportName)
            free (ffboard_init.serial.ComportName);
        LOGC_ERROR(CPM_FF_DDSERVICE," Failure in initialization of API interface r_code = %d\n",
                r_code);
        return r_reason(r_code);
	}

    // Reset FBK hardware
    r_code = fbapi_exit (channel, TRIGGER_HW_RESET);
    if (r_code != E_OK)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in reset of FBKhost (r_code=%d)\n", r_code);
        return r_reason(r_code);
    }

    LOGC_INFO(CPM_FF_DDSERVICE,"Recv: Reset success. Size of the Data in bytes = %d",
            sizeof(r_code));

    // Retry to initialize the communication channel once again
    // after reset
    while (tries < MAX_TRIES)
    {
        r_code = fbapi_init (channel, PROTOCOL_FF, &ffboard_init);
        if (r_code == E_OK)
        {
            break;
        }
        tries++;
    }

    if (NULL != ffboard_init.serial.ComportName)
        free (ffboard_init.serial.ComportName);

    if (r_code == E_OK)
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Successful initialization of API interface after %d tries\n",
            tries);
    }
    else
    {
                LOGC_ERROR(CPM_FF_DDSERVICE," Failure in initialization of API interface (r_code=%d)",
			r_code);
        return r_reason(r_code);
    }

        LOGC_INFO(CPM_FF_DDSERVICE,"Recv: init success. Size of the Data in bytes = %d",
			sizeof(r_code));

        LOGC_INFO(CPM_FF_DDSERVICE,"Sent: Get Device Info request: Size = %d",
			sizeof(channel));

	r_code = fbapi_get_device_info(channel, PROTOCOL_FF, &device_info);

	if(r_code != CM_SUCCESS) {
		return r_reason(r_code);
	}

	set_fbk_info(&device_info);

        LOGC_INFO(CPM_FF_DDSERVICE,"Recv: Get Device Info response. Size of the Data in bytes = %d",
			sizeof(device_info) + sizeof(channel));

        LOGC_DEBUG(CPM_FF_DDSERVICE,"Starting service sequence to setup the H1 host stack");

        LOGC_INFO(CPM_FF_DDSERVICE,"Sent: Start the FF Stack. Size of the Data in bytes = %d",
			sizeof(channel));

	r_code = fbapi_start (channel, &add_detail);
	if(E_OK != r_code)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Initialization of API interface fails with error code %d", r_code);
        if (add_detail != 0)
            LOGC_ERROR(CPM_FF_DDSERVICE,"Starting FW failed with error code %d", add_detail);
		return r_reason(r_code);
	}

        LOGC_INFO(CPM_FF_DDSERVICE,"Recv: Start the FF Stack. Size of the Data in bytes = %d",
			sizeof(r_code));

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Stack service has been started successfully.");

	return CM_SUCCESS;
}
