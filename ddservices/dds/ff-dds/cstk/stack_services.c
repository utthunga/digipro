/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file stack_services.c
    FF Stack services provided by softing.

    This file contains all functions pertaining to the services
    of FF protocol.

    The basic services functions are as follows:

    1.  Network management Agent Services
    2.  System Management Services.
    3.  Field Message Specification services.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"

/* Process diconnection */
int (*connectdisconnNotification) P((unsigned char)) = NULL;
unsigned int (*setLASNodeAddress) P((unsigned int)) = NULL;

unsigned int  activeLAS = 0;

#ifdef ALL_FF_SERVICES_SUPPORTED
/***********************************************************************
 *
 *  Name: handle_config_req
 *
 *  ShortDesc:  The function to configure memory requirement of FBK stack.
 *
 *  Description:
 *      The handle_config_req configures the memory requirements using
 *      data structure of type T_LOAD_CONFIG_REQ and loads with the
 *      service request NMA_LOAD_CONFIG
 *      NOTE: This service is not called in this application.
 *      FBK uses the default configuration. Default configuration
 *      stored at??
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return Errors from function device_send_req_res
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

/*! @fn int handle_config_req (void)
    @brief Handle the request command for configuring the stack.
    	   Note: This request command is not necessary as the FBK is using
    	   its default configured values.
    @param None.
*/
int handle_config_req (void)
{
    T_LOAD_CONFIG_REQ *   set_cfg;
    int r_code = E_OK;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    set_cfg = (T_LOAD_CONFIG_REQ *) req_buffer;

    /*
     * The below are the default values provided by Softing inc
     */
    set_cfg->fal_cap.max_fal_sdb_blocks  = 100;
    set_cfg->fal_cap.max_data_blocks     = 120;
    set_cfg->dll_cap.max_no_of_dlsaps    = 20;
    set_cfg->dll_cap.max_no_of_dlceps    = 20;
    set_cfg->dll_cap.max_no_send         = 30;
    set_cfg->dll_cap.max_no_recv         = 30;
    set_cfg->dll_cap.max_dll_sdb_blocks  = 120;
    set_cfg->dll_cap.max_dll_fifo_descr  = 120;
    set_cfg->dll_cap.max_dll_timer_elems = 180;

    /*
     * Set the service Descriptive Header
     */
    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(NMA_LOAD_CONFIG);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device communication state
     */
    set_device_admin_state(DEVACC_APPL_STATE_CFG);

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Load Memory Configuration");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:NMA_LOAD_CONFIG size = %d",
            sizeof(T_LOAD_CONFIG_REQ) +
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return (r_code);
}

/***********************************************************************
 *
 *  Name: handle_config_con
 *
 *  ShortDesc:  Confirmation information from remote Device for request
 *
 *  Description:
 *      The handle_config_con function logs the error recieved
 *      for the request handle_config_req and proceed further to
 *      set SM configuration.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn int handle_config_con (VOID)
    @brief Handle the response command for configuring the stack.
    	   Note: The request command is not necessary as the FBK is using
    	   its default configured values.
    @param None.
*/
static int handle_config_con (VOID)
{
    /*
     * Get the application state
     */
    int state = get_device_admin_state();
    int result = get_service_result();

    if (state != DEVACC_APPL_STATE_CFG)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Fatal Error, FBK Board not able to recover");

        set_device_admin_state(DEVACC_APPL_STATE_END);
    }

    if (result != POS)
    {
        T_ERROR *err_ptr = (T_ERROR *) global_res_buf;
        LOGC_ERROR(CPM_FF_DDSERVICE,"Negative result for the cinfig request");

        set_device_admin_state(DEVACC_APPL_STATE_END);
        return err_ptr->class_code;
    }

    return SUCCESS;
}
#endif

/***********************************************************************
 *
 *  Name: handle_sm_config_req
 *
 *  ShortDesc:  Configuration of the System management parameters.
 *
 *  Description:
 *      The handle_sm_config_req function sets the configurations of the
 *      host device, which describe its properties and identification.
 *
 *  Inputs:
 *      SM_REQ
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS.
 *      Return values from device_send_req_res function.
 *
 *  Author:
 *      Softing Solution
 *      Jayanth M
 *
 **********************************************************************/
/*! @fn handle_sm_config_req (SM_REQ *sm_req)
    @brief The handle_sm_config_req function sets the configurations of the
       	   host device, which describe its properties and identification.
    @param SM_REQ.
*/
int handle_sm_config_req (SM_REQ *sm_req)
{
    int r_code;
    T_SM_SET_CONFIGURATION_REQ *   set_cfg;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    set_cfg = (T_SM_SET_CONFIGURATION_REQ *) req_buffer;

    set_cfg->current_time.high_u32       = 0;
    set_cfg->current_time.low_u32        = 0;
    set_cfg->local_time_diff             = 0;
    set_cfg->ap_clock_sync_interval      = AP_CLOCK_SYNC_INTERVAL;
    set_cfg->primary_time_publisher      = PRIMARY_TIME_PUB;
    set_cfg->macrocycle_duration         = 0;

    /*
     * All roles supporting manager role for SM services, SM Identify as agent
     */

    /*
      sm_support: Enables SM services as manager/agent
       � Set PD Tag (Agent) 		Bit 0 		-> Byte 0: 0x80
       � Set Address (Agent) 		Bit 1 		-> Byte 0: 0x40
       � Clear Address (Agent) 		Bit 2 		-> Byte 0: 0x20
       � SM Identify (Agent) 		Bit 3 		-> Byte 0: 0x10
       � Locate FB blocks (Agent) 	Bit 4 		-> Byte 0: 0x08
       � Set PD Tag (Manager) 		Bit 5 		-> Byte 0: 0x04
       � Set Address (Manager) 		Bit 6 		-> Byte 0: 0x02
       � Clear Address (Manager) 	Bit 7 		-> Byte 0: 0x01
       � SM Identify (Manager) 		Bit 8 		-> Byte 1: 0x80
      � Locate FB blocks (Manager) 	Bit 9 		-> Byte 1: 0x40
       � FMS Server Bit 10 						-> Byte 1: 0x20
       � Application Clock Synchronization (Pub/Sub) Bit 11/13 -> Byte 1: 0x10/Byte 1: 0x04
       � FB Scheduling 				Bit 12 		-> Byte 1: 0x08
    */

    set_cfg->sm_support[0]               = SM_SUPPORT_SM_IDENT |
    									   SM_SUPPORT_SET_PD_TAG |
										   SM_SUPPORT_SET_ADDR |
										   SM_SUPPORT_CLR_ADDR; // (0x17)
    set_cfg->sm_support[1]               = SM_SUPPORT_SM_IDENT_MNGR |
    									   SM_SUPPORT_LOC_FB_BLOCKS; //(0xc0 - FBK3)
										   // SM_SUPPORT_APP_SUB_CLK_SYNC; //(0xc4 - FBK2)
    set_cfg->sm_support[2]               = 0x00;
    set_cfg->sm_support[3]               = 0x00;

    /* For T1 T2 and T3 please refer document: FF-940 section 5.4 */

    set_cfg->t1                          = SM_SUPPORT_SET_ADDR_TIMER_1; //(480000)
    set_cfg->t2                          = SM_SUPPORT_SET_ADDR_TIMER_3; //(3840000)
    set_cfg->t3                          = SM_SUPPORT_SET_ADDR_TIMER_2; //(1440000)
    set_cfg->max_fb_elems                = 0;

    /* FBK sets the device id internally in the stack to
       FBK manufacturer id + dev type + S/N */
    strcpy(&set_cfg->pd_tag[1], &sm_req->host_pd_tag[1]);
    set_cfg->pd_tag [0] = (USIGN8) sm_req->host_pd_tag[0];

    set_cfg->op_powerup = FB_TRUE;

    /*
     * Set the service Descriptive Header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_SET_CONFIGURATION);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device communication state
     */
    set_device_admin_state(DEVACC_APPL_STATE_LOAD_SM);

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Load System Management Configuration");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:SM - Service:SM_SET_CONFIGURATION size = %d",
            (sizeof(T_SM_SET_CONFIGURATION_REQ) +
            get_device_admin_channel() +
            get_size_of_desc_header()
            ));

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_sm_config_con
 *
 *  ShortDesc:  Confirmation information from remote Device for
 *              SM request
 *
 *  Description:
 *      The handle_sm_config_con handles the confirmation from
 *      transmitter for the request handle_sm_config_req and reports
 *      either Negative confirmation with error code or positive
 *      confirmation to the application if no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_sm_config_con (void)
    @brief The response for SM configuration request.
    @param void.
*/
int handle_sm_config_con (void)
{
    /*
     * Get the application state
     */
    int state = get_device_admin_state();

    if (state != DEVACC_APPL_STATE_LOAD_SM)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Fatal Error, FBK Board not able to recover");
        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);
    }

    if (POS != get_service_result())
    {
        print_sm_err();
        LOGC_TRACE(CPM_FF_DDSERVICE,"Recv: Layer:SM - Service:SM_SET_CONFIGURATION size ");

        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);
    }

    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return SUCCESS;
}


/***********************************************************************
 *
 *  Name: handle_dll_config_req
 *
 *  ShortDesc:  Configuration of DLL paramters of FBK before starting
 *              stack
 *
 *  Description:
 *      Sets the local Data Link Layer (DLL) parameters. The parameters
 *      are checked by DLL. If they are accepted the station tries to go
 *      online i.e. it tries to join the network. The confirmation for
 *      the service is issued when the station has joined the network
 *      and has a valid, non-default station address.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return code from function device_send_req_res
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_dll_config_req (void)
    @brief Sets the local Data Link Layer (DLL) parameters. The parameters
       are checked by DLL. If they are accepted the station tries to go
       online i.e. it tries to join the network. The confirmation for
       the service is issued when the station has joined the network
       and has a valid, non-default station address.
    @param dll_config_req.
*/
int handle_dll_config_req(FBAP_NMA_REQ *dll_config_req)
{
    int r_code = SUCCESS;
    T_LOAD_DLL_CONFIG_REQ *dll_ptr;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    T_DLL_CONFIG_REQ *dll_req = (T_DLL_CONFIG_REQ *)&dll_config_req->dll_config;
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    dll_ptr = (T_LOAD_DLL_CONFIG_REQ*)req_buffer;

    /*
     * Default Configured DLL Settings
     */
    dll_ptr->type = NMA_LINK_MASTER;
    dll_ptr->lm.this_link = dll_req->this_link;//0;
    dll_ptr->lm.primary_las = FB_FALSE;
    dll_ptr->lm.use_decided = FB_TRUE;
    dll_ptr->lm.decided_val.slot_time = dll_req->slot_time; //DFLT_SLOT_TIME;
    dll_ptr->lm.decided_val.phl_overhead = dll_req->phlo; //DFLT_PHL_OVERHEAD;
    dll_ptr->lm.decided_val.max_reply_delay = dll_req->mrd; //DFLT_MAX_RPLY_DELAY;
    dll_ptr->lm.decided_val.min_inter_pdu_delay = dll_req->mipd; //DFLT_MIN_INTER_PDU_DELAY;
    dll_ptr->lm.decided_val.time_sync_class = dll_req->tsc; //DFLT_TIME_SYNC_CLASS;
    dll_ptr->lm.decided_val.max_inter_chan_signal_skew = dll_req->phis_skews;
                                                //DFLT_MAX_INTER_CHAN_SIG_SKEW;
    dll_ptr->lm.decided_val.phl_preamble_extension = dll_req->phpe;//DFLT_PHL_PREAMBLE_EXTN;
    dll_ptr->lm.decided_val.phl_gap_extension = dll_req->phge; //DFLT_PHL_GAP_EXTN;
    dll_ptr->lm.decided_val.time_distribution_period = DFLT_TIME_DIST_PERIOD;
    dll_ptr->lm.decided_val.dtb_sta_spdu_distr_period =
                                                DFLT_DTB_STA_SPDU_DIST_PERIOD;
    dll_ptr->lm.decided_val.default_token_hold_time = DFLT_TOKEN_HOLD_TIME;
    dll_ptr->lm.decided_val.min_token_delegation_time =
                                                DFLT_MIN_TOKEN_DELEGATE_TIME;
    dll_ptr->lm.decided_val.target_token_rot_time =
                                                DFLT_TARGET_TOKEN_ROT_TIME;
    dll_ptr->lm.decided_val.first_unpolled_node = dll_req->fun; //DFLT_FIRST_UNPOLLED_NODE;
    dll_ptr->lm.decided_val.num_unpolled_node = dll_req->nun; //DFLT_NUM_UNPOLLED_NODE;
    dll_ptr->lm.decided_val.max_link_maint_time = DFLT_MAX_LINK_MAINT_TIME;
    dll_ptr->lm.decided_val.max_inact_claim_las_delay =
                                               DFLT_MAX_INACT_CLAIM_LAS_DELAY;

    dll_ptr->this_node = get_device_admin_host_addr();

    /*
     * Service Description of loading DLL settings
     */

    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(NMA_LOAD_DLL_CONFIG);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device communication state
     */
    set_device_admin_state(DEVACC_APPL_STATE_LOAD_DLL);

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Load DLL Configuration");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NM - Service:NMA_LOAD_DLL_CONFIG size = %d",
            sizeof(T_LOAD_DLL_CONFIG_REQ) +
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_dll_config_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              config dll
 *  Description:
 *      The handle_dll_config_con function prints the error recieved
 *      for the request handle_config_req and if no errors the function
 *      outputs the index of NMA directory for read further information.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      nma_info -- The index of the NMA Dierctory
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_dll_config_req (T_NMA_INFO *nma_info)
    @brief The handle_dll_config_con function prints the error recieved
 *      for the request handle_config_req and if no errors the function
 *      outputs the index of NMA directory for read further information.
    @param nma_info.
*/
int handle_dll_config_con(T_NMA_INFO *nma_info)
{
    if (DEVACC_APPL_STATE_LOAD_DLL != get_device_admin_state())
    {
        //devacc_appl_print_error(__LINE__);
        //set_device_admin_state(DEVACC_APPL_STATE_END);
        return SUCCESS;
    }

    if (POS != get_service_result())
    {
        //print_service_err();
        //set_device_admin_state(DEVACC_APPL_STATE_END);
        return SUCCESS;
    }

    /* The MIB has been created internally in the host stack now.
    The confirmation contains the layout of the SM and NM directory.
    This is stored now. It also would be possible to do a local
    NMA read to SM directory (index 256) and NM directory (index 257)
    to gain this information. The directory objects also contain the
    number of the objects in the various sections, while nma_info
    contains only the index of the first object in a section */

    save_device_admin_stack_mib_dir(nma_info);
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_vcrl_init_req
 *
 *  ShortDesc:  Initiation Request for VCRL
 *
 *  Description:
 *      Service starts the load VCRL (virtual communication reference
 *      list) sequence and loads the VCRL entry of VCR 1 (VCR for
 *      Management access)send service requests.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return code from function device_send_req_res
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_vcrl_init_req(void)
    @brief Service starts the load VCRL (virtual communication reference
       list) sequence and loads the VCRL entry of VCR 1 (VCR for
       Management access)send service requests..
    @param nma_info.
*/
int handle_vcrl_init_req(void)
{
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    /*
     * Service Description of VCRL initialization
     */

    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(NMA_INIT_LOAD_VCRL);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device communication state
     */
    set_device_admin_state(DEVACC_APPL_STATE_VCRL_INIT);

    LOGC_DEBUG(CPM_FF_DDSERVICE," Init Load VCRL ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:NMA_INIT_LOAD_VCR size = %d",
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_vcrl_init_con
 *
 *  ShortDesc:  Confirmation information from remote Device for request
 *              vcrl_init
 *  Description:
 *      The handle_vcrl_init_con function prints the error received
 *      for the request handle_vcrl_init_req and sets the state
 *      depending upon the success in received data or failure in
 *      performing the action
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_vcrl_init_req(void)
    @brief The handle_vcrl_init_con function prints the error received
 *      for the request handle_vcrl_init_req and sets the state
 *      depending upon the success in received data or failure in
 *      performing the action
    @param void.
*/
int handle_vcrl_init_con(void)
{
    if (DEVACC_APPL_STATE_VCRL_INIT != get_device_admin_state())
    {
        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);
    }

    if (POS != get_service_result())
    {
        print_service_err();
        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);;
    }

    set_device_admin_aux(TRUE);
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_vcrl_term_req
 *
 *  ShortDesc:  Service terminates the loading of VCR entries
 *
 *  Description:
 *      The handle_vcrl_term_req funtion sets the service descriptor for
 *      for terminating the loading of VCr entries and sets the
 *      application state to DEVACC_APPL_STATE_VCRL_TERM
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      return from funtion device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_vcrl_term_req(void)
    @brief The handle_vcrl_term_req funtion sets the service descriptor for
       for terminating the loading of VCr entries and sets the
       application state to DEVACC_APPL_STATE_VCRL_TERM
    @param void.
*/
int handle_vcrl_term_req(void)
{
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    /*
     * Service Description of Terminating VCRL initialization
     */

    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(NMA_TERM_LOAD_VCRL);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device communication state
     */
    set_device_admin_state(DEVACC_APPL_STATE_VCRL_TERM);

    LOGC_DEBUG(CPM_FF_DDSERVICE," Terminate loading VCR table ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:NMA_TERM_LOAD_VCRL size = %d",
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_vcrl_term_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_vcrl_term_req
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and calls the function
 *      devacc_stack_startup_finished indicating the completion
 *      of stack initialization
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_vcrl_term_con(void)
    @brief The handle_config_con function prints the error recieved
           for the request handle_config_req and calls the function
           devacc_stack_startup_finished indicating the completion
           of stack initialization
    @param void.
*/
int handle_vcrl_term_con (void)
{
    set_device_admin_aux(FALSE);

    if (DEVACC_APPL_STATE_VCRL_TERM != get_device_admin_state())
    {
        //devacc_appl_print_error (__LINE__);
        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);
    }
    else if (POS != get_service_result())
    {
        print_service_err ();
        set_device_admin_state(DEVACC_APPL_STATE_END);
        return (FAILURE);
    }
    else
    {
        set_device_admin_state(DEVACC_APPL_STATE_READY);
    }
    return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_nma_read_req
 *
 *  ShortDesc:  Service request to read local NMA directiory
 *
 *  Description:
 *      Service is used to read the value of a local MIB object.
 *      No parallel invocation of Management Read and/or Management
 *      Write service requests are possible.
 *
 *  Inputs:
 *      index -- The index of NMA object to read
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_nma_read_req(USIGN16 index)
    @brief  Service is used to read the value of a local MIB object.
       No parallel invocation of Management Read and/or Management
       Write service requests are possible
    @param index.
*/
int handle_nma_read_req(USIGN16 index)
{
    static unsigned char invoke_id;
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    T_VAR_READ_LOC_REQ *read_ptr;
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    read_ptr = (T_VAR_READ_LOC_REQ*)req_buffer;

    read_ptr->acc_spec.tag = ACCESS_INDEX;
    read_ptr->acc_spec.id.index = index;
    read_ptr->subindex = 0;

    /*
     * Service Description of Local NMA read
     */
    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(MGMT_READ);
    set_service_primitive(REQ);
    set_service_invoke_id(invoke_id);

    invoke_id++;
    // Max invok ID is 125
    if (invoke_id > MAX_INVOKE_ID)
    {
        invoke_id = 0;
    }

    /*
     * Set the Device administration parameters
     */
    set_device_admin_state(DEVACC_APPL_STATE_MGMT_READ);
    set_device_admin_proc_con(NULL);
    set_device_admin_local_nma_read(TRUE);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:MGMT_READ size = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_READ_LOC_REQ) +
            get_size_of_desc_header()
            );


    r_code = device_send_req_res(req_buffer);
    if(E_OK != r_code)
    {
        set_device_admin_local_nma_read(FALSE);
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_nma_write_req
 *
 *  ShortDesc:  Service request to read local NMA directiory
 *
 *  Description:
 *      Service is used to write the value of a local MIB object.
 *      No parallel invocation of Management Read and/or Management
 *      Write service requests are possible.
 *
 *  Inputs:
 *      index -- NMA index
 *      subindex -- Sub index
 *      len     -- Length of the data to be written
 *      data    -- Data to be written
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_nma_write_req (index,subindex,len,*data)
    @brief  Service is used to write the value of a local MIB object.
       No parallel invocation of Management Read and/or Management
       Write service requests are possible
    @param index, subindex, length, data ptr.
*/
int handle_nma_write_req (
            USIGN16 index,
            USIGN8 subindex,
            USIGN8 len,
            USIGN8 *data
)
{
    T_VAR_WRITE_REQ *write_ptr;
    USIGN8 *data_buf;
    USIGN8 i;
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    write_ptr = (T_VAR_WRITE_REQ*)req_buffer;
    data_buf = (USIGN8*)(write_ptr + 1);

    write_ptr->acc_spec.tag = ACCESS_INDEX;
    write_ptr->acc_spec.id.index = (USIGN16) index;
    write_ptr->subindex = (USIGN8) subindex;
    write_ptr->length = (USIGN8) len;

    /*
     * Copy all the data to req_buffer
     * which are to written into the transmitter
     */

    for (i = 0; i < write_ptr->length; i++)
    {
        data_buf[i] = data[i];
    }

    /*
     * Service Description of Write local NMA
     */
    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(MGMT_WRITE);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */
    set_device_admin_state(DEVACC_APPL_STATE_MGMT_WRITE);
    set_device_admin_local_nma_write(TRUE);

    LOGC_DEBUG(CPM_FF_DDSERVICE," Local NMA Write Request ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:MGMT_WRITE size = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_WRITE_REQ) +
            write_ptr->length +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_load_vcr_local_write_req
 *
 *  ShortDesc:  Load the VCR using MGMT Write service
 *
 *  Description:
 *      The handle_load_vcr_local_write_req configures the local
 *      MIB index with the VCR entries and writes it into the FBK
 *      host.
 *
 *  Inputs:
 *      fbap_nma_req -- Which contains NMA index, local and
 *                      remote address
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_load_vcr_local_write_req(FBAP_NMA_REQ *fbap_nma_req)
    @brief  The handle_load_vcr_local_write_req configures the local
 *      MIB index with the VCR entries and writes it into the FBK
 *      host
    @param FBAP_NMA_REQ.
*/
int handle_load_vcr_local_write_req(FBAP_NMA_REQ *fbap_nma_req)
{
    T_VAR_WRITE_REQ *write_ptr;
    USIGN8 *data_buf;
    int r_code = -1;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    write_ptr = (T_VAR_WRITE_REQ*)req_buffer;
    data_buf = (USIGN8*)(write_ptr + 1);

    write_ptr->acc_spec.tag      = ACCESS_INDEX;
    write_ptr->acc_spec.id.index = fbap_nma_req->cr_index +
                                   fbap_nma_req->nma_info.vcr_list_idx + 1;
    write_ptr->subindex          = 0;
    write_ptr->length            = 44;

    /*
     * Data descriptor for the service request. Please refer FF-940 Section 4.4
     * and traning Document from Softing
     */

    data_buf[0] = 0xA2u; /* type and role ->client*/
    data_buf[1] = 0x00; /* local add */
    data_buf[2] = 0x00;
    data_buf[3] = (USIGN8) (fbap_nma_req->loc_add / 0x100u);
    data_buf[4] = (USIGN8) fbap_nma_req->loc_add;
    data_buf[5] = 0x00; /* rem add */
    data_buf[6] = 0x00;
    data_buf[7] = (USIGN8) (fbap_nma_req->rem_add / 0x100u);
    data_buf[8] = (USIGN8) fbap_nma_req->rem_add;
    data_buf[9] = 0x2b; /* SDAP */
    data_buf[10] = 0xea; /* Connect Delay */
    data_buf[11] = 0x60;
    data_buf[12] = 0xea; /* Data Delay */
    data_buf[13] = 0x60;
    data_buf[14] = 0x00; /* DLL size */
    data_buf[15] = 0x80;
    data_buf[16] = 0x01; /* residual act */
    data_buf[17] = 0x00; /* timeliness class */
    data_buf[18] = 0x00; /* PUB time window size */
    data_buf[19] = 0x00;
    data_buf[20] = 0x00; /* PUB synchronizing DLCEP */
    data_buf[21] = 0x00;
    data_buf[22] = 0x00;
    data_buf[23] = 0x00;
    data_buf[24] = 0x00; /* SUB time window size */
    data_buf[25] = 0x00;
    data_buf[26] = 0x00; /* SUB synchronizing DLCEP */
    data_buf[27] = 0x00;
    data_buf[28] = 0x00;
    data_buf[29] = 0x00;
    data_buf[30] = 0x00; /* FMS Vfd Id, not used for client VCR */
    data_buf[31] = 0x00;
    data_buf[32] = 0x00;
    data_buf[33] = 0x00;
    data_buf[34] = 0x01; /* outstanding calling */
    data_buf[35] = 0x00; /* outstanding called  */
    // CPMHACK: In FF Specification FF-870, it has been mentioned that its not mandatory
    // to have GET_OD long format service always enabled in field device
#ifdef SUPPORT_LONG_FORMAT
    data_buf[36] = 0x80; /* supported features */
#else
    data_buf[36] = 0x00; /* supported features */
#endif
    data_buf[37] = 0x30;
    data_buf[38] = 0x00;
    data_buf[39] = 0x00;
    data_buf[40] = 0x00;
    data_buf[41] = 0x00;
    data_buf[42] = 0x00;
    data_buf[43] = 0x00;

    set_device_admin_vfd_nr(fbap_nma_req->rem_add,
            fbap_nma_req->cr_index);
    set_device_admin_device_addr(fbap_nma_req->rem_add,
            fbap_nma_req->cr_index);
    /*
     * Type and Role:
     * 0xA2 sets the client role for the device
     * For more settings of the Type and Role see
     * FF-801: Network management Section 5.2.3
     */
    set_device_admin_type_role(0xA2u, fbap_nma_req->cr_index);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Remote Device address = %x", fbap_nma_req->rem_add);

    /*
     * Service Description of Write local NMA
     */
    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(MGMT_WRITE);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */
    set_device_admin_state(DEVACC_APPL_STATE_VCR_LOCAL_WR);

    LOGC_DEBUG(CPM_FF_DDSERVICE," Local VCR Load Request ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:MGMT_WRITE size = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_WRITE_REQ) +
            write_ptr->length +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_initiate_req
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      service is used to establish a connection. If the VCR connection
 *      type is QUB, FAL and DLL part of the VCR parameters are exchanged
 *      between two communication partners: The initiate requester sends
 *      context information  such  as  supported services, the supported
 *      options, the maximum PDU length and the current version of the
 *      OD to its communication partner. Upon receipt of an
 *      INITIATE_REQ_PDU the FAL of the communication partner checks
 *      whether the context ofthe initiate requester is compatible with
 *      its own context.
 *
 *  Inputs:
 *      cr -- Communication Reference number
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return code from device_send_req_res function
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_initiate_req(IN USIGN16 cr)
    @brief  Service is used to establish a connection. If the VCR connection
       type is QUB, FAL and DLL part of the VCR parameters are exchanged
       between two communication partners: The initiate requester sends
       context information  such  as  supported services, the supported
       options, the maximum PDU length and the current version of the
       OD to its communication partner. Upon receipt of an
       INITIATE_REQ_PDU the FAL of the communication partner checks
       whether the context ofthe initiate requester is compatible with
       its own context
    @param CR.
*/
int handle_initiate_req(USIGN16 cr)
{
    T_CTXT_INIT_REQ *init_ptr;
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    init_ptr = (T_CTXT_INIT_REQ*)req_buffer;

    init_ptr->profile_number[0] = 'M';
    init_ptr->profile_number[1] = 'G';

    init_ptr->password = 0;
    init_ptr->access_groups = 0xFF;

    /* if remote DLCEP == 0, then it is possible to set the address here */

    /*
     * Service Description of Handle initiate request
     */
    set_service_comm_ref(cr);
    set_service_layer(FMS);
    set_service_ref(FMS_INITIATE);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */
    set_device_admin_state(DEVACC_APPL_STATE_FMS_INITIATE);

    LOGC_DEBUG(CPM_FF_DDSERVICE," Handle Initiate Request ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_INITIATE size = %d",
            get_device_admin_channel() +
            sizeof(T_CTXT_INIT_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_initiate_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_initiate_req
 *  Description:
 *      The handle_initiate_con function prints the error recieved
 *      for the request handle_initiate_req and sets the state to
 *      DEVACC_APPL_STATE_READY if no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_initiate_con (void)
    @brief  The handle_initiate_con function prints the error recieved
       for the request handle_initiate_req and sets the state to
       DEVACC_APPL_STATE_READY if no error received
    @param void.
*/
int handle_initiate_con (void)
{
    int r_code;

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Initiate.con (+) --> CR %d\n", get_service_comm_ref());
        r_code = SUCCESS;

    }
    else
    {
        LOGC_ERROR ("Initiate.con (-) --> CR %d\n", get_service_comm_ref());
        r_code = FAILURE;
    }
    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_INITIATE size = %d",
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return r_code;
}

/*! @fn handle_nma_data_query_con (void)
    @brief  The handle_nma_data_query_con function prints the error received
       for the request handle_nma_data_query and sets the state to
       DEVACC_APPL_STATE_READY if no error received
    @param void.
*/
int handle_nma_data_query_con (void)
{
    int r_code;

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "handle_nma_data_query.con (+) --> CR %d\n", get_service_comm_ref());
        r_code = SUCCESS;

    }
    else
    {
        LOGC_ERROR ("handle_nma_data_query_con.con (-) --> CR %d\n", get_service_comm_ref());
        r_code = FAILURE;
    }
    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:NMA_DATA_QUERY size = %d",
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return r_code;
}


/***********************************************************************
 *
 *  Name: handle_abort_req
 *
 *  ShortDesc: Service request to release an open connection.
 *
 *  Description:
 *      With the ABORT service, the user may release an open connection.
 *      The ABORT service is also called by the protocol stack as
 *      reaction to an error situation e.g. a connection timeout occurs.
 *      It is also possible that the remotestation sends an Abort
 *      telegram which also leads to an Abort indication. These
 *      indications also cause the connection to be closed.
 *
 *  Inputs:
 *      cr -- Communication reference number
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_abort_req (USIGN16   cr )
    @brief  With the ABORT service, the user may release an open connection.
       The ABORT service is also called by the protocol stack as
       reaction to an error situation e.g. a connection timeout occurs.
       It is also possible that the remote station sends an Abort
       telegram which also leads to an Abort indication. These
       indications also cause the connection to be closed
    @param CR.
*/
int handle_abort_req ( USIGN16   cr )
{
    T_CTXT_ABORT_REQ *  abt_ptr;
    int r_code = E_OK;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    abt_ptr = (T_CTXT_ABORT_REQ *) req_buffer;

    abt_ptr->abort_id      = USR;
    /* ---- disconnect ----- */
    abt_ptr->reason        = USR_ABT_RC1;
    abt_ptr->detail_length = 10;
    strcpy((char *) abt_ptr->detail,"USER_ABORT");

    /*
     * Service Description of Handle initiate request
     */
    set_service_comm_ref(cr);
    set_service_layer(FMS);
    set_service_ref(FMS_ABORT);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Handle Abort Request ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_ABORT size = %d",
            get_device_admin_channel() +
            sizeof(T_CTXT_ABORT_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);
    if(E_OK == r_code)
    {
        set_device_admin_state(DEVACC_APPL_STATE_ABORT);
        set_device_admin_conn(FB_FALSE);
    }
    return r_code;
}

#ifdef ALL_FF_SERVICES_SUPPORTED
/***********************************************************************
 *
 *  Name: handle_nma_reset_req
 *
 *  ShortDesc:  Service request to reset the FBK firmware/Hardware stack
 *
 *  Description:
 *      This local service aborts all open connections and sets the Data
 *      Link Layer to the offline state i.e. the station leaves the
 *      network. No other service can be performed after the RESET
 *      service is issued.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_nma_reset_req ( void )
    @brief  This local service aborts all open connections and sets the Data
       Link Layer to the offline state i.e. the station leaves the
       network. No other service can be performed after the RESET
       service is issued
    @param void.
*/
int handle_nma_reset_req ( void )
{
    int r_code = E_OK;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    /*
     * Service Description of NMA reset request
     */
    set_service_comm_ref(0);
    set_service_layer(NMA);
    set_service_ref(NMA_RESET);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Handle Abort Request ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:NMA - Service:NMA_RSEST size = %d",
            get_device_admin_channel() +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);
    if(E_OK == r_code)
    {
        set_device_admin_state(DEVACC_APPL_STATE_NM_RESET);
        set_device_admin_conn(FB_FALSE);
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_nma_reset_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_nma_reset_req
 *  Description:
 *      The handle_nma_reset_con function prints the error recieved
 *      for the request handle_nma_reset_req and if no error sets the
 *      state to DEVACC_APPL_STATE_READY
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_nma_reset_con ( void )
    @brief  The handle_nma_reset_con function prints the error recieved
       for the request handle_nma_reset_req and if no error sets the
       state to DEVACC_APPL_STATE_READY
    @param void.
*/
int handle_nma_reset_con ( void )
{
    int r_code;

    if (POS == get_service_result())
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"NMA Reset.con (+) \n");
        r_code = SUCCESS;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"NMA Reset.con (-) \n");
        r_code = FAILURE;
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return r_code;
}
#endif

/***********************************************************************
 *
 *  Name: handle_abort_ind
 *
 *  ShortDesc:  Abort Indication infromation from remote Device
 *
 *  Description:
 *      The handle_abort_ind function prints the indication recieved
 *      for the transmitter and the CR from where it has received
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_abort_ind(void)
    @brief  The handle_abort_ind function prints the indication recieved
       for the transmitter and the CR from where it has received
    @param void.
*/
int handle_abort_ind(void)
{
    T_CTXT_ABORT_REQ *abt_ptr;
    int r_code;

    abt_ptr = (T_CTXT_ABORT_REQ*)global_res_buf;
    LOGC_INFO(CPM_FF_DDSERVICE, "Abort.ind --> CR : %d", get_service_comm_ref());
    if (abt_ptr->local)
    {
    	LOGC_INFO(CPM_FF_DDSERVICE, " Local Generated");
    }
    else
    {
    	LOGC_INFO(CPM_FF_DDSERVICE, " Remote Generated");
    }

    LOGC_INFO(CPM_FF_DDSERVICE, "Instance:  ");
    switch (abt_ptr->abort_id)
    {

        case 1:
        	LOGC_INFO(CPM_FF_DDSERVICE, "User\n");
            r_code = CM_IF_USR_ABORT + abt_ptr->reason;
            break;

        case 2:
        	LOGC_INFO(CPM_FF_DDSERVICE, "FMS\n");
            r_code = CM_IF_FMS_ABT + abt_ptr->reason;
            break;

        case 3:
        	LOGC_INFO(CPM_FF_DDSERVICE, "FAS\n");
            r_code = CM_IF_FAS_ABT + abt_ptr->reason;
            break;

        case 4:
        	LOGC_INFO(CPM_FF_DDSERVICE, "DLL\n");
            r_code = CM_IF_DLL_ABT + abt_ptr->reason;
            break;

        case 5:
        	LOGC_INFO(CPM_FF_DDSERVICE, "NMA\n");
            r_code = CM_IF_DLL_ABT;
            break;

        default:
        	LOGC_INFO(CPM_FF_DDSERVICE, "Unknown\n");
            r_code = CM_IF_DLL_ABT;
            break;

    }
    LOGC_INFO(CPM_FF_DDSERVICE, "ReasonCode:    %x\n", abt_ptr->reason);
    set_device_admin_conn(FB_FALSE);
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return (r_code);
}

/***********************************************************************
 *
 *  Name: handle_get_od_req
 *
 *  ShortDesc:  Get the Object Description from the Communication
 *              Reference
 *
 *  Description:
 *      One or more Object Descriptions are read with the GET-OD service.
 *      The service distinguishes between a short and a long form of
 *      Object Descriptions. The long form of the GET-OD service is
 *      optional.
 *
 *  Inputs:
 *      fbap_nma_req -- Request data structure
 *      index       -- Index to read OD
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_get_od_req(FBAP_NMA_REQ *fbap_nma_req, USIGN16 first_index)
    @brief  One or more Object Descriptions are read with the GET-OD service.
       The service distinguishes between a short and a long form of
       Object Descriptions. The long form of the GET-OD service is
       optional.
    @param FBAP_NMA_REQ, index.
*/
int handle_get_od_req(FBAP_NMA_REQ *fbap_nma_req, USIGN16 first_index)
{
    T_GET_OD_REQ *u_get_od;
    int r_code;
    USIGN8        device_addr;
    USIGN8        vfd_no;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    u_get_od = (T_GET_OD_REQ*)req_buffer;

    u_get_od->format = FB_FALSE; /* short format */
    /*
     * FB_FALSE = Short Format
     * FB_TRUE = Long Format
    */
    u_get_od->acc_spec.tag      = INDEX_ACCESS;
    u_get_od->acc_spec.id.index = first_index;

    /*
     * Service Description Header
     */
    set_service_comm_ref(fbap_nma_req->cr_index);
    set_service_layer(FMS);
    set_service_ref(FMS_GET_OD);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Get Object Description request from the device ");

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_GET_OD size = %d",
            get_device_admin_channel() +
            sizeof(T_GET_OD_REQ) +
            get_size_of_desc_header()
            );
    
    // CPMHACK: Use always short format OD as its mandatory feature
#ifdef SUPPORT_LONG_FORMAT
    if(!first_index)
    {
        u_get_od->format = FB_FALSE;  /* short format */
        LOGC_TRACE(CPM_FF_DDSERVICE,"Read the Short format of Object dictionary");
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Read the Long format of Object dictionary");
        u_get_od->format = FB_TRUE;  /* long format */
    }
#else
    u_get_od->format = FB_FALSE;  /* short format */
    LOGC_TRACE(CPM_FF_DDSERVICE,"Read the Short format of Object dictionary");
#endif

    device_addr = get_device_admin_device_addr(fbap_nma_req->cr_index);

    if(device_addr)
    {
        vfd_no = get_device_admin_vfd_nr(fbap_nma_req->cr_index);
        if(VFD_NR_MIB == vfd_no)
        {
            set_device_admin_state(DEVACC_STATE_GET_MGMT_OD_HDR);
            set_admin_dev_state(DEVACC_STATE_GET_MGMT_OD_HDR);
        }
        else if(VFD_NR_FBAP == vfd_no)
        {
            set_device_admin_state(DEVACC_STATE_GET_FBAP_OD_HDR);
            set_admin_dev_state(DEVACC_STATE_GET_FBAP_OD_HDR);
        }
    }
	
    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_get_od_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_get_od_req
 *  Description:
 *      The handle_get_od_con function decode the OD information and
 *      if OD of block header is read, it saves the header information
 *      by calling devadm_save_od_header_info function and if its not
 *      the block header, it prints the value for positive response
 *      from the remote device. If the data reception is not complete
 *      the function handle_get_od_req is called to receive all the
 *      OD information.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_get_od_con ( void )
    @brief   The handle_get_od_con function decode the OD information and
       if OD of block header is read, it saves the header information
       by calling devadm_save_od_header_info function and if its not
       the block header, it prints the value for positive response
       from the remote device. If the data reception is not complete
       the function handle_get_od_req is called to receive all the
       OD information
    @param void
*/
int handle_get_od_con ( void )
{
	int i;
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_read_req
 *
 *  ShortDesc:  The FMS read request
 *
 *  Description:
 *      The values of Simple Variables, Arrays, Records and Variable
 *      Lists of the communication partner are read by using this
 *      service. Single elements of Arrays, Records and Variable Lists
 *      are accessed with a subindex.
 *
 *  Inputs:
 *      fbap_nma_req -- Read request data structure
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_read_req(FBAP_NMA_REQ *fbap_nma_req)
    @brief   The values of Simple Variables, Arrays, Records and Variable
       Lists of the communication partner are read by using this
       service. Single elements of Arrays, Records and Variable Lists
       are accessed with a subindex
    @param FBAP_NMA_REQ
*/
int handle_mgmt_read_req(FBAP_NMA_REQ *fbap_nma_req)
{
    int r_code;
	int i;
    T_VAR_READ_REQ *read_ptr;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    read_ptr = (T_VAR_READ_REQ*)req_buffer;

    read_ptr->acc_spec.tag = ACCESS_INDEX;
    read_ptr->acc_spec.id.index = fbap_nma_req->index;
    read_ptr->subindex = fbap_nma_req->sub_index;

    /*
     * Service Description of FMS Read request
     */
    set_service_comm_ref(fbap_nma_req->cr_index);
    set_service_layer(FMS);
    set_service_ref(FMS_READ);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Read the Data from Device using FMS Service ");

    set_device_admin_state(DEVACC_APPL_STATE_FMS_READ);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_READ = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_READ_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_write_req
 *
 *  ShortDesc:  FMS Write request
 *
 *  Description:
 *      With this service, values are written in objects of the
 *      communication partner. The service may be usedfor Simple
 *      Variables, Arrays, Records and Variable Lists. Single elements
 *      of Arrays, Records and Variable Lists may be accessed with a
 *      subindex.
 *
 *  Inputs:
 *      fbap_nma_req -- Read and Write request data structure
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_write_req(FBAP_NMA_REQ *fbap_nma_req)
    @brief   With this service, values are written in objects of the
       communication partner. The service may be usedfor Simple
       Variables, Arrays, Records and Variable Lists. Single elements
       of Arrays, Records and Variable Lists may be accessed with a
       subindex.
    @param FBAP_NMA_REQ
*/
int handle_mgmt_write_req(FBAP_NMA_REQ *fbap_nma_req)
{
    T_VAR_WRITE_REQ *    write_ptr;
    USIGN8 *             data_buf;
    USIGN8               i;
    int r_code;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);


    write_ptr = (T_VAR_WRITE_REQ *) req_buffer;
    data_buf = (USIGN8 *) (write_ptr + 1);

    write_ptr->acc_spec.tag = ACCESS_INDEX;

    write_ptr->acc_spec.id.index = fbap_nma_req->index;
    write_ptr->subindex = fbap_nma_req->sub_index;
    write_ptr->length = fbap_nma_req->size;

    /*
     * Copy the data to req_buffer transmission
     * buffer
     */

    for (i = 0; i < write_ptr->length; i++)
    {
        data_buf [i] = fbap_nma_req->value[i];
    }

    /*
     * Service Description of FMS Read request
     */
    set_service_comm_ref(fbap_nma_req->cr_index);
    set_service_layer(FMS);
    set_service_ref(FMS_WRITE);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Write the Data to Device using FMS Service ");

    set_device_admin_state(DEVACC_APPL_STATE_FMS_WRITE);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_WRITE = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_WRITE_REQ) +
            write_ptr->length +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_read_req
 *
 *  ShortDesc:  The FMS read request
 *
 *  Description:
 *      The values of Simple Variables, Arrays, Records and Variable
 *      Lists of the communication partner are read by using this
 *      service. Single elements of Arrays, Records and Variable Lists
 *      are accessed with a subindex.
 *
 *  Inputs:
 *      fbap_nma_req -- Read request data structure
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_read_req(USIGN16 cr, USIGN16 index)
    @brief   The values of Simple Variables, Arrays, Records and Variable
       Lists of the communication partner are read by using this
       service. Single elements of Arrays, Records and Variable Lists
       are accessed with a subindex.
    @param CR, index
*/
int handle_read_req(USIGN16 cr, USIGN16 index)
{
    T_VAR_READ_REQ *read_ptr;
    FB_INT8 invoke_id;
    int r_code, i;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
	
	memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);
	
    read_ptr = (T_VAR_READ_REQ*)req_buffer;

    read_ptr->acc_spec.tag = ACCESS_INDEX;
    read_ptr->acc_spec.id.index = index;
    read_ptr->subindex          = 0;

    invoke_id = get_device_admin_invokeid();

    /*
     * Service Description of FMS Read request
     */
    set_service_comm_ref(cr);
    set_service_layer(FMS);
    set_service_ref(FMS_READ);
    set_service_primitive(REQ);
    set_service_invoke_id(invoke_id++ & 0x7f);

    set_device_admin_invokeid(invoke_id);
    
    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE," Read the rest of data from Device using FMS Service ");

    set_device_admin_state(DEVACC_APPL_STATE_FMS_READ);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS - Service:FMS_READ = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_READ_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_read_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_read_req
 *
 *  Description:
 *      The handle_read_con function on positive response from remote
 *      device, calls the function devadm_read_con for processing read
 *      confirmation for device access sequence.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
/*! @fn handle_read_con(void)
    @brief   The handle_read_con function on positive response from remote
 *      device, calls the function devadm_read_con for processing read
 *      confirmation for device access sequence.
    @param void
*/
int handle_read_con(void)
{
    int i;
 
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_vcr_write_req
 *
 *  ShortDesc:  Writing/Configuring the remote device VCR by using
 *              FMS Write service
 *
 *  Description:
 *      Configuring the Server FAS type VCR at the remote device
 *      along with other required configuration for establishing
 *      communication over VCR
 *
 *  Inputs:
 *      fbap_nma_req -- Data structure to configure remote VCR
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_vcr_write_req(FBAP_NMA_REQ* fbap_nma_req)
    @brief   Configuring the Server FAS type VCR at the remote device
       along with other required configuration for establishing
       communication over VCR.
    @param FBAP_NMA_REQ
*/
int handle_vcr_write_req(FBAP_NMA_REQ* fbap_nma_req)
{
    T_VAR_WRITE_REQ *write_ptr;
    USIGN8          *data_buf;
    USIGN8          device_addr;
    USIGN32         vfd_ref;
    int             r_code = E_OK;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    device_addr = (USIGN8) (fbap_nma_req->loc_add / 0x100u);

    vfd_ref = (USIGN32)fbap_nma_req->vfd_ref;

    write_ptr = (T_VAR_WRITE_REQ*)req_buffer;
    data_buf = (USIGN8*)(write_ptr + 1);

    write_ptr->acc_spec.tag = ACCESS_INDEX;

    write_ptr->acc_spec.id.index = fbap_nma_req->index;
    write_ptr->subindex = 0;
    write_ptr->length = 44;
    data_buf[0] = 0x32u; /* type and role SERVER*/
    data_buf[1] = 0x00; /* loc add */
    data_buf[2] = 0x00;
    data_buf[3] = (USIGN8) (fbap_nma_req->loc_add >> 8);
    data_buf[4] = (USIGN8) (fbap_nma_req->loc_add);
    data_buf[5] = 0x00; /* rem add */
    data_buf[6] = 0x00;
    data_buf[7] = 0x00;
    data_buf[8] = 0x00;
    data_buf[9] = 0x2b; /* SDAP */
    data_buf[10] = 0xea; /* Connect Delay */
    data_buf[11] = 0x60;
    data_buf[12] = 0xea; /* Data Delay */
    data_buf[13] = 0x60;
    data_buf[14] = 0x00; /* DLL size */
    data_buf[15] = 0x80;
    data_buf[16] = 0x00; /* residual act */
    data_buf[17] = 0x00; /* timeliness class */
    data_buf[18] = 0x00; /* PUB time window size */
    data_buf[19] = 0x00;
    data_buf[20] = 0x00; /* PUB synchronizing DLCEP */
    data_buf[21] = 0x00;
    data_buf[22] = 0x00;
    data_buf[23] = 0x00;
    data_buf[24] = 0x00; /* SUB time window size */
    data_buf[25] = 0x00;
    data_buf[26] = 0x00; /* SUB synchronizing DLCEP */
    data_buf[27] = 0x00;
    data_buf[28] = 0x00;
    data_buf[29] = 0x00;
    data_buf[30] = (USIGN8) (vfd_ref / 0x1000000uL); /* FMS Vfd Id */
    data_buf[31] = (USIGN8) (vfd_ref / 0x10000uL);
    data_buf[32] = (USIGN8) (vfd_ref / 0x100uL);
    data_buf[33] = (USIGN8) (vfd_ref);
    data_buf[34] = 0x00; /* outstanding calling */
    data_buf[35] = 0x01; /* outstanding called  */
    data_buf[36] = 0x00; /* supported features */
    data_buf[37] = 0x00;
    data_buf[38] = 0x00;
    data_buf[39] = 0x00;
    data_buf[40] = 0x80;
    data_buf[41] = 0x30;
    data_buf[42] = 0x00;
    data_buf[43] = 0x00;

    /*
     * Service Description of FMS Write request
     */
    set_service_comm_ref(fbap_nma_req->cr_index);
    set_service_layer(FMS);
    set_service_ref(FMS_WRITE);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Write the VCR of remote device using FMS Write service");

    set_device_admin_state(DEVACC_APPL_STATE_FMS_WRITE);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Remote VCR Write: Layer:FMS - Service:FMS_WRITE = %d",
            get_device_admin_channel() +
            sizeof(T_VAR_WRITE_REQ) +
            write_ptr->length +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);

    LOGC_TRACE(CPM_FF_DDSERVICE,"write server vcr %d for device 0x%02x, using VFD ref: %08x\n",
            fbap_nma_req->index, device_addr, vfd_ref);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_write_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_write_req
 *  Description:
 *      The handle_write_con function prints the error if recieved
 *      for the request handle_write_req and sets the application
 *      state to DEVACC_APPL_STATE_READY.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_write_con ( void )
    @brief   The handle_write_con function prints the error if recieved
       for the request handle_write_req and sets the application
       state to DEVACC_APPL_STATE_READY.
    @param void
*/
int handle_write_con ( void )
{
    int r_code;

    if (POS == get_service_result())
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"Write.con (+)\n");
        r_code = SUCCESS;
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Write.con (-)\n");
        print_service_err();
        r_code =  (FAILURE);
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_initiate_ind
 *
 *  ShortDesc:  Indicating the other Host/Remote device which tries to
 *              access local MIB of host
 *  Description:
 *      Sets the sender descriptive for the requested service and
 *      sends the requested host/remote device over the CR.
 *
 *  Inputs:
 *      layer -- Service layer
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_initiate_ind(USIGN8 layer)
    @brief   Sets the sender descriptive for the requested service and
       sends the requested host/remote device over the CR.
    @param Service Layer
*/
int handle_mgmt_initiate_ind(USIGN8 layer)
{
    int r_code = CM_SUCCESS;
    LOGC_DEBUG(CPM_FF_DDSERVICE,"MGMT Initiate.ind\n");

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_abort_ind
 *
 *  ShortDesc:  Abort Indication handling
 *
 *  Description:
 *      Decodes the type of abort indication received from the remote
 *      device
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      None
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_abort_ind(void)
    @brief   Decodes the type of abort indication received from the remote
       device.
    @param void
*/
int handle_mgmt_abort_ind(void)
{

    T_CTXT_ABORT_REQ *abt_ptr;
    abt_ptr = (T_CTXT_ABORT_REQ*)global_res_buf;

    if (abt_ptr->local)
    {
        printf(" Local Generated\n");
    }
    else
    {
        printf(" Remote Generated\n");
    }
    printf("Instance:  %d\t", abt_ptr->abort_id);
    printf("Reason:    %d\n", abt_ptr->reason);
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_read_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_mgmt_read_req
 *  Description:
 *      The handle_mgmt_read_con function handles the data recieved
 *      in response for the request handle_mgmt_read_con and inteprets
 *      the data received. if devacc_appl_admin.local_nma_read = 1,
 *      which means the read confirmation from local MIB read
 *      devacc_appl_admin.local_nma_read = 0 indicates the read
 *      request from other Host, if devacc_appl_admin.local_nma_read
 *      = 0, then it sends the service layer information with positive
 *      response.
 *
 *  Inputs:
 *      layer -- Service layer
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_read_con(USIGN8 layer)
    @brief   The handle_mgmt_read_con function handles the data recieved
       in response for the request handle_mgmt_read_con and inteprets
       the data received. if devacc_appl_admin.local_nma_read = 1,
       which means the read confirmation from local MIB read
       devacc_appl_admin.local_nma_read = 0 indicates the read
       request from other Host, if devacc_appl_admin.local_nma_read
       = 0, then it sends the service layer information with positive
       response.
    @param Service Layer
*/
int handle_mgmt_read_con(USIGN8 layer)
{
    T_ERROR *error_ptr;
    int r_code = SUCCESS;
    USIGN16 local_nma_read;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    local_nma_read = get_device_admin_local_nma_read();

    if (local_nma_read)
    {
        if (NULL != device_admin_proc_con())
        {
            /*
             * Do Nothing
             */
        }
        if(NEG == get_service_result())
        {
            error_ptr = (T_ERROR*)global_res_buf;
            printf("error class=%x",error_ptr->class_code);
            printf("Local Read.con (-) \n");
            r_code = (FAILURE);
        }
        set_device_admin_local_nma_read(0);
    }
    else
    {
		set_service_layer(layer);
		set_service_primitive(RES);

        r_code = device_send_req_res( req_buffer);
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_read_ind
 *
 *  ShortDesc:  Indicating the other Host/Remote device which tries to
 *              access local MIB of host
 *  Description:
 *      Sets the sender descriptive for the requested service and
 *      sends the data to requested host/remote device over the CR.
 *
 *  Inputs:
 *      layer -- Service layer
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_read_ind(USIGN8 layer)
    @brief   Sets the sender descriptive for the requested service and
       sends the data to requested host/remote device over the CR
    @param Service Layer
*/
int handle_mgmt_read_ind(USIGN8 layer)
{
    int r_code = CM_SUCCESS;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    set_service_layer(layer);
	set_service_primitive(REQ);

    r_code = device_send_req_res( req_buffer);
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_write_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_mgmt_write_req
 *  Description:
 *      The handle_mgmt_write_con function handles the data recieved
 *      in response for the request handle_mgmt_write_req and inteprets
 *      the data received. if devacc_appl_admin.local_nma_write = 1,
 *      which means the write confirmation from local MIB write.
 *      devacc_appl_admin.local_nma_write = 0 indicates the write
 *      request from other Host, if devacc_appl_admin.local_nma_write
 *      = 0, then it sends the service layer information with positive
 *      response.
 *
 *  Inputs:
 *      layer -- Service layer
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_write_con(IN USIGN8 layer)
    @brief   The handle_mgmt_write_con function handles the data received
 *      in response for the request handle_mgmt_write_req and interprets
 *      the data received. if devacc_appl_admin.local_nma_write = 1,
 *      which means the write confirmation from local MIB write.
 *      devacc_appl_admin.local_nma_write = 0 indicates the write
 *      request from other Host, if devacc_appl_admin.local_nma_write
 *      = 0, then it sends the service layer information with positive
 *      response.
    @param Service Layer
*/
int handle_mgmt_write_con(USIGN8 layer)
{
    int r_code = CM_SUCCESS;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    USIGN16 local_nma_write = get_device_admin_local_nma_write();

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);
    if (local_nma_write)
    {
        if (POS == get_service_result())
        {
            LOGC_TRACE(CPM_FF_DDSERVICE,"Recv:NMA Service:MGMT_WRITE: Size = %d",
            get_size_of_desc_header()
            );

        }
        else
        {
            LOGC_TRACE(CPM_FF_DDSERVICE,"Local Write.con (-) \n");
            print_service_err();
            r_code = FAILURE;
        }
        set_device_admin_local_nma_write(0);
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"handle_mgmt_write_con: remote NMA write **\n");
        if (mgmt_service_loc == FB_TRUE)
        {
            mgmt_service_loc = FB_FALSE;
            r_code = SUCCESS ;
        }

		set_service_layer(layer);
		set_service_primitive(RES);
		r_code = device_send_req_res( req_buffer);

        if(r_code != E_OK)
        {
            r_code = -(r_code);
        }
    }

    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_mgmt_write_ind
 *
 *  ShortDesc:  Indicating the other Host/Remote device which tries to
 *              write local MIB of host
 *  Description:
 *      Sets the sender descriptive for the requested service and
 *      sends the data to requested host/remote device over the CR.
 *
 *  Inputs:
 *      layer -- Service layer
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_mgmt_write_ind(USIGN8 layer)
    @brief    Sets the sender descriptive for the requested service and
       sends the data to requested host/remote device over the CR
    @param Service Layer
*/
int handle_mgmt_write_ind(USIGN8 layer)
{

    int r_code;
    unsigned char req_buffer[MAX_FF_DATA_LEN];
    printf("MGMT Write.ind\n");

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);
    /*
     * A host usually does not permit that its FF configuration is
     * changed, so write access is not granted, return error
     */

	set_service_layer(layer);
	set_service_primitive(RES);
	set_service_result(NEG);
	r_code = device_send_req_res( req_buffer);

    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

#ifdef ALL_FF_SERVICES_SUPPORTED
/***********************************************************************
 *
 *  Name: handle_ack_event_req
 *
 *  ShortDesc:  Acknwoleding an event
 *
 *  Description:
 *      Sets the sender descriptive for the requested service and
 *      sends the data to requested host/remote device over the CR.
 *
 *  Inputs:
 *      cr -- communication reference
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
/*! @fn handle_ack_event_req (USIGN16 cr, USIGN16 index, USIGN8 event_no)
    @brief    Sets the sender descriptive for the requested service and
       sends the data to requested host/remote device over the CR
    @param Service Layer
*/
int handle_ack_event_req (USIGN16 cr, USIGN16 index, USIGN8 event_no)
{
    T_ACK_EVN_NOTIFY_REQ *    ack_evt_ptr;
    int r_code;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    ack_evt_ptr = (T_ACK_EVN_NOTIFY_REQ *) req_buffer;

    ack_evt_ptr->acc_spec.tag = ACCESS_INDEX;
    ack_evt_ptr->acc_spec.id.index = (USIGN16) index;
    ack_evt_ptr->event_number = (USIGN8) event_no;

    /*
     * Service Description header
     */
    set_service_comm_ref(cr);
    set_service_layer(FMS);
    set_service_ref(FMS_ACK_EVN_NOTIFY);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Acknowledge the Event");

    set_device_admin_state(DEVACC_APPL_STATE_EVENT_REQ);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Remote VCR Write: Layer:FMS - Service:FMS_ACK_EVN_NOTIFY = %d",
            get_device_admin_channel() +
            sizeof(T_ACK_EVN_NOTIFY_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_ack_event_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_ack_event_con function prints the error recieved
 *      for the request handle_ack_event_req.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
int handle_ack_event_con (void)
{

    int r_code;
    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "AckEvent.con (+)\n");
        r_code = SUCCESS;
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"AckEvent.con (-)\n");
        r_code  = (FAILURE);
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_event_ind
 *
 *  ShortDesc:  Indication inforamtion about event from remote Device
 *
 *  Description:
 *      The handle_event_ind function decodes the event received
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
int handle_event_ind (void)
{
    T_EVENT_NOTIFY_REQ *    event_ptr;

    event_ptr = (T_EVENT_NOTIFY_REQ *) global_res_buf;

    LOGC_TRACE(CPM_FF_DDSERVICE, "Event.ind --> Nr %d  ", event_ptr->event_number);
    LOGC_TRACE(CPM_FF_DDSERVICE, "DLAddr: 0x%x, index: %d  ", event_ptr->dl_address,
             event_ptr->acc_spec.id.index);
    LOGC_TRACE(CPM_FF_DDSERVICE, "Length: %d\n", event_ptr->data_length);

    set_device_admin_state( DEVACC_APPL_STATE_READY);

    return SUCCESS;
}
#endif

/***********************************************************************
 *
 *  Name: handle_reject_ind
 *
 *  ShortDesc:  Indication infromation from remote Device
 *
 *  Description:
 *      The handle_reject_ind function decodes reject indication
 *      either from remote device or local.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int  handle_reject_ind(void)
{
    T_CTXT_REJECT_IND *rej_ptr;

    rej_ptr = (T_CTXT_REJECT_IND*)global_res_buf;

    LOGC_TRACE(CPM_FF_DDSERVICE, "Reject.ind / ");

    if (rej_ptr->detected_here)
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Local Generated\n");
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Remote Generated\n");
    }

    LOGC_TRACE(CPM_FF_DDSERVICE, "   invoke_id: %d\t", rej_ptr->orig_invoke_id);
    LOGC_TRACE(CPM_FF_DDSERVICE, "   pdu_type:  %d\t", rej_ptr->pdu_type);
    LOGC_TRACE(CPM_FF_DDSERVICE, "   reason:    %d\n", rej_ptr->reject_code);

    set_device_admin_aux(0);
    set_device_admin_state( DEVACC_APPL_STATE_READY);

    return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_set_pd_tag_req
 *
 *  ShortDesc:  The service can be used by a System Manager to assign a
 *              physical device tag to a remote device or to clear it.
 *
 *  Description:
 *              If the physical device tag should be assigned, the
 *              remote device has to be in the SM state UNINITIALIZED,
 *              it has to support the agent part of the Set-PD-Tag
 *              service and the device id, given in the request has to
 *              match the device id of the remote device. Upon
 *              successful completion the remote device will be in
 *              the INITIALIZED state. If the physical device tag
 *              should be cleared, the remote device has to be in the
 *              SM state INITIALIZED,it has to support the agent part of
 *              the Set-PD-Tag serviceand the device id and the physical
 *              device tag, given in the request has to match the device
 *              id and physical device tag of the remote device. Upon
 *              successful completion the remote device will be in the
 *              UNINITIALIZED status.
 *              set_pd_tag->clear =  FB_TRUE:  clear the PD tag of remote
 *              device and set its  SM state to UNINITIALIZED.
 *              set_pd_tag->clear =  FB_FALSE:  set the given PD tag
 *              for the remote device.
 *
 *  Inputs:
 *      sm_req -- System Management data request structure
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return Values from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_set_pd_tag_req(SM_REQ* sm_req)
{
    T_SET_PD_TAG_REQ *set_pd_tag;
    int r_code;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    set_pd_tag = (T_SET_PD_TAG_REQ*)req_buffer;
    set_pd_tag->clear = (FB_BOOL)sm_req->clear;
    memcpy (set_pd_tag->dev_id, sm_req->dev_id, SM_TAG_SIZE);
    memcpy (set_pd_tag->pd_tag, sm_req->pd_tag, SM_TAG_SIZE);

    set_pd_tag->node = (USIGN8)sm_req->dev_addr;

    /*
     * Service Description header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_SET_PD_TAG);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Set PD tag of the remote device");

    set_device_admin_state(DEVACC_APPL_STATE_SM_SET_CLR_TAG);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Set PD tag: Layer:SM - Service:SM_SET_PD_TAG = %d",
            get_device_admin_channel() +
            sizeof(T_SET_PD_TAG_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    if (FB_FALSE == set_pd_tag->clear)
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"Set Physical Device Tag");
    }
    else
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"Clear Physical Device Tag");
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_set_pd_tag_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_set_pd_tag_req
 *  Description:
 *      If the request is clearing the PD Tag, then it clears the PD
 *      tag of remote device and set its SM state to UNINITIALIZED.
 *      If the request is for setting the tag, then it sets the given
 *      PD tag for the remote device.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_set_pd_tag_con(void)
{
    int r_code;
    T_SM_ERROR *sm_error;

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Set PD Tag.con (+)\n");
        r_code = SUCCESS;

    }
    else
    {
        sm_error = (T_SM_ERROR*)global_res_buf;
        LOGC_TRACE(CPM_FF_DDSERVICE,"SET PD Tag.con (-) %d", sm_error->rc);
        r_code = sm_error->rc;
    }
    set_device_admin_state( DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_set_addr_req
 *
 *  ShortDesc:  Service request to set the device address
 *
 *  Description:
 *      The service used by a System Manager to assign a node
 *      address to a remote device that is identified by the given
 *      physical device tag and to set values  for application clock
 *      synchronization interval and  primary time publisher address.
 *      The service will fail if the given physical device tag is not
 *      unique, if the selected node address is already used,if the
 *      remote device is not in the SM state INITIALIZED or if the
 *      remote device does not support agentpart of the address
 *      assignment protocol.
 *      Upon successful completion the SM state of the remote device
 *      will be changed to OPERATIONAL.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return value from function device_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_set_addr_req(SM_REQ* sm_req)
{
    int r_code;
    T_SET_ADDRESS_REQ *set_addr;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    set_addr = (T_SET_ADDRESS_REQ*)req_buffer;

    memcpy (set_addr->pd_tag, sm_req->pd_tag, SM_TAG_SIZE);

    /*
     * Default values for clock_sync_interval and time publisher
     */
    set_addr->ap_clock_sync_interval = AP_CLOCK_SYNC_INTERVAL;
    set_addr->primary_time_publisher = 0x10;
    set_addr->node = (USIGN8)sm_req->dev_addr;

    /*
     * Service Description header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_SET_ADDRESS);
    set_service_primitive(REQ);
    set_service_invoke_id(1);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Set Address of the remote device");

    set_device_admin_state(DEVACC_APPL_STATE_SM_SET_ADDR);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Set address: Layer:SM - Service:SM_SET_ADDRESS = %d",
            get_device_admin_channel() +
            sizeof(T_SET_ADDRESS_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_set_addr_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_set_addr_req
 *
 *  Description:
 *      handle_set_addr_con function decodes the data received from
 *      remote device and sets the error based negative response from
 *      remote device.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_set_addr_con(void)
{
    int r_code;

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Set Address.con (+)\n");
        r_code = SUCCESS;
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Set Address.con (-)\n");
        r_code  = (FAILURE);
    }
    set_device_admin_state( DEVACC_APPL_STATE_READY);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_clear_addr_req
 *
 *  ShortDesc: Service request to clear the node address
 *
 *  Description:
 *      The service can be used by a System Manager to clear the node
 *      address of a remote device. The service will fail if the remote
 *      device is not in the SM state OPERATIONAL, if it does not
 *      support the agent part for the clear address service or if the
 *      deviceid or physical device tag do not match the device id
 *      and PD tag of the remote device. Upon successful completion
 *      the SM state of the remote device will be changed to INITIALIZED
 *      and its node address will be set to a default address. The VCR
 *      list will be deleted so that no VCR entry will be available
 *      after a new node address has been assigned to the device.
 *
 *  Inputs:
 *      sm_req -- System manager request data
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return Values from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_clear_addr_req (SM_REQ* sm_req)
{
    T_CLEAR_ADDRESS_REQ *   clear_addr;
    int r_code;
    unsigned short invid = 0;

    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    clear_addr = (T_CLEAR_ADDRESS_REQ *) req_buffer;

    memcpy (clear_addr->dev_id, sm_req->dev_id, SM_TAG_SIZE);
    memcpy (clear_addr->pd_tag, sm_req->pd_tag, SM_TAG_SIZE);

    clear_addr->node = (USIGN8) sm_req->dev_addr;

    /*
     * Service Description header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_CLEAR_ADDRESS);
    set_service_primitive(REQ);
    set_service_invoke_id(1);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Clear Address of the remote device");

    set_device_admin_state(DEVACC_APPL_STATE_SM_CLR_ADDR);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Clear address: Layer:SM - Service:SM_CLEAR_ADDRESS = %d",
            get_device_admin_channel() +
            sizeof(T_CLEAR_ADDRESS_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_clear_addr_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_clear_addr_req
 *
 *  Description:
 *      handle_clear_addr_con function decodes the data received from
 *      remote device and sets the error based negative response from
 *      remote device.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
int handle_clear_addr_con (VOID)
{
    int r_code;
    T_SM_ERROR *sm_error;
    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "Clear Address.con (+)\n");
        LOGC_ERROR(CPM_FF_DDSERVICE, "handle_clear_addr_con Clear Address.con (+)\n");
        r_code = SUCCESS;
    }
    else
    {
        sm_error = (T_SM_ERROR*)global_res_buf;
        LOGC_ERROR(CPM_FF_DDSERVICE,"Clear ADDR.con (-) %d", sm_error->rc);
        r_code = sm_error->rc;
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_identify_req
 *
 *  ShortDesc:  Service request to get PD tag and Device ID from remote
 *              device.
 *
 *  Description:
 *      The service is used to read the physical device tag and device
 *      identifier of a remote device. The remote device has to support
 *      the agent part of the Identify service.
 *
 *  Inputs:
 *      node -- Node address of the remote device
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Return values from function devacc_appl_send_req_res
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/

int handle_identify_req (USIGN8 node)
{
    int r_code;
    T_SM_IDENTIFY_REQ *identify;
	unsigned char req_buffer[MAX_FF_DATA_LEN];
    memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    identify = (T_SM_IDENTIFY_REQ*)req_buffer;

    identify->node = node;

    /*
     * Service Description header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_IDENTIFY);
    set_service_primitive(REQ);
    //set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Identify the remote device");

    set_device_admin_state(DEVACC_APPL_STATE_SM_IDENTIFY);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Identify: Layer:SM - Service:SM_IDENTIFY = %d",
            get_device_admin_channel() +
            sizeof(T_SM_IDENTIFY_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res( req_buffer);

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_identify_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *              handle_identify_req
 *
 *  Description:
 *      handle_identify_con function decodes the data received from
 *      remote device and sets the error based negative response from
 *      remote device.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing Team
 *
 **********************************************************************/
int handle_identify_con(void)
{
	int r_code = CM_SUCCESS;
    if (POS == get_service_result())
    {
        r_code = CM_SUCCESS;
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Identify.con (-)\n");
        r_code = (FAILURE);
    }
    set_device_admin_state(DEVACC_APPL_STATE_READY);
    return r_code;
}


/***********************************************************************
 *
 *  Name: handle_nma_data_req
 *
 *  ShortDesc:  Request command to get the NAM information from FBK board
 *
 *  Description:
 *      handle_nma_data_req copies the NMA information stored in global
 *      structure device_admin and returns to the application
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int handle_nma_data_req(void)
{
	T_NMA_INFO nma_info = {0};

    set_device_admin_state(DEVACC_APPL_STATE_NMA_DATA_QUERY);
    get_device_admin_stack_mib_dir(&nma_info);

    (void *)memcpy((char *)global_res_buf, (char *)&nma_info, sizeof(T_NMA_INFO));

    set_device_admin_state(DEVACC_APPL_STATE_READY);

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_find_tag_query_req
 *
 *  ShortDesc:  Query the tag description from the device
 *
 *  Description:
 *      The handle_find_tag_query_req function query the tag fro the presence
 *      in the remote device.
 *
 *  Note: This is just the example code provided by the Softing solution
 *        This function is never used in the PV reader application
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

int handle_find_tag_query_req(USIGN8 tag_type, FB_BOOL
    with_param)

{
    T_TAG_QUERY_REQ *tag_query;
    USIGN32 n;
    unsigned char req_buffer[MAX_FF_DATA_LEN];

    (void *)memset((char *)req_buffer, 0, MAX_FF_DATA_LEN);

    /*
     * Hard coded is the example code and is provided by the softing to test
     * the find tag query service.
     */
    USIGN32 param = 0x12345678uL;
    static USIGN8 i = 0;
    int r_code = CM_SUCCESS;

    tag_query = (T_TAG_QUERY_REQ*)req_buffer;
    tag_query->tag_type = tag_type;

    if (tag_type == QUERY_VFD_TAG)
    {
        strcpy(&tag_query->query.vfd_tag.vfd_tag[1], "This is VFD TAG");
        strcpy(&tag_query->query.vfd_tag.pd_tag[1], "PD_TAG_SM_AGENT");
        tag_query->query.vfd_tag.pd_tag[0] = (USIGN8)strlen("PD_TAG_SM_AGENT");

        tag_query->query.vfd_tag.vfd_tag[0] = (USIGN8)strlen("This is VFD TAG");
    }
    else if (tag_type == QUERY_FB_TAG)
    {
        switch (i % 5)
        {

            case 0:
                strcpy(&tag_query->query.fb_tag.fb_tag[1], "AI_1");
                tag_query->query.fb_tag.fb_tag[0] = (USIGN8)strlen("AI_1");
                break;

            case 1:
                strcpy(&tag_query->query.fb_tag.fb_tag[1], "ANALOG_IN_101");
                tag_query->query.fb_tag.fb_tag[0] = (USIGN8)strlen(
                    "ANALOG_IN_101");
                break;

            case 2:
                strcpy(&tag_query->query.fb_tag.fb_tag[1], "ANALOG_OUT_103");
                tag_query->query.fb_tag.fb_tag[0] = (USIGN8)strlen(
                    "ANALOG_OUT_103");
                break;

            case 3:
                strcpy(&tag_query->query.fb_tag.fb_tag[1], "DIGITAL_IN_102");
                tag_query->query.fb_tag.fb_tag[0] = (USIGN8)strlen(
                    "DIGITAL_IN_102");
                break;

            default:
                strcpy(&tag_query->query.fb_tag.fb_tag[1], "DIGITAL_OUT_104");
                tag_query->query.fb_tag.fb_tag[0] = (USIGN8)strlen(
                    "DIGITAL_OUT_104");
                break;
        }

        i++;
        tag_query->query.fb_tag.with_param = with_param;

        if (with_param == FB_TRUE)
        {
            printf("Enter PARAMETER (hex) --> \n");
            scanf("%x", &param);
            printf("\n");
            ;

        }

        tag_query->query.fb_tag.param_id = param;
    }
    else if (tag_type == QUERY_PD_TAG)
    {
        strcpy(&tag_query->query.pd_tag.pd_tag[1], "PD_TAG_SM_AGENT");
        tag_query->query.pd_tag.pd_tag[0] = (USIGN8)strlen("PD_TAG_SM_AGENT");
    }
    else
    {
        return CM_SUCCESS;
    }

    printf("Enter SMA Node (HEX) --> \n");
    scanf("%x", &n);
    printf("\n");
    ;
    if (n < 0x100)
    {
        tag_query->dest_addr = (USIGN16)(n *0x100 + 2);
    }
    else
    {
        tag_query->dest_addr = (USIGN16) n;
    }

    /*
     * Service Description header
     */
    set_service_comm_ref(0);
    set_service_layer(SM);
    set_service_ref(SM_FIND_TAG_QUERY);
    set_service_primitive(REQ);
    set_service_invoke_id(0);

    /*
     * Set the Device administration parameters
     */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Find the block tag in the remote device");

    set_device_admin_state(DEVACC_APPL_STATE_SM_IDENTIFY);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Sent Query: Layer:SM - Service:SM_FIND_TAG_QUERY = %d",
            get_device_admin_channel() +
            sizeof(T_TAG_QUERY_REQ) +
            get_size_of_desc_header()
            );

    r_code = device_send_req_res(req_buffer);
    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_config_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and proceed further if
 *      no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

static int handle_find_tag_query_con(VOID)
{
    T_SM_ERROR *sm_error;

    if (POS == get_service_result())
    {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"Find Tag Query.con (+)");
    }
    else
    {
        sm_error = (T_SM_ERROR*)global_res_buf;
        LOGC_ERROR(CPM_FF_DDSERVICE,"Find Tag Query.con (-) %d", sm_error->rc);
        return FAILURE;
    }

    return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_config_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and proceed further if
 *      no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

static int handle_find_tag_reply_ind(VOID)
{

    T_TAG_REPLY_IND *tag_reply;

    tag_reply = (T_TAG_REPLY_IND*)global_res_buf;

    LOGC_TRACE(CPM_FF_DDSERVICE,"Find Tag Reply.ind  tag_type: %d\n", tag_reply->tag_type);
    LOGC_TRACE(CPM_FF_DDSERVICE," remote address: %x\n", tag_reply->src_addr);

    switch (tag_reply->tag_type)
    {
        case QUERY_PD_TAG:
            printf("    Dev-Id: %s\n", tag_reply->reply.pd_tag.dev_id);
            break;

        case QUERY_VFD_TAG:
            printf("    Vfd Ref: %x  ", tag_reply->reply.vfd_tag.vfd_ref);
            printf(" OD-Version: %d", tag_reply->reply.vfd_tag.od_version);
            printf(" Number of VCRs: %d\n", tag_reply->reply.vfd_tag.n_cr);
            break;

        case QUERY_FB_TAG:
            printf("    Vfd Ref: %x  ", tag_reply->reply.fb_tag.vfd_ref);
            printf(" OD-Version: %d  ", tag_reply->reply.fb_tag.od_version);
            printf(" Index: %d", tag_reply->reply.fb_tag.index);
            printf(" Number of VCRs: %d\n", tag_reply->reply.fb_tag.n_cr);
            break;

        default:
            break;
    }


    return SUCCESS;
}
/***********************************************************************
 *
 *  Name: handle_config_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and proceed further if
 *      no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

static int handle_sm_dll_event_ind (VOID)
{

    T_SM_DLL_EVENT * event_ind;
    USIGN8           i;
    USIGN8           node_number;
    int 			 testChannelID = 0;
    int 			 rc;

    event_ind = (T_SM_DLL_EVENT *) global_res_buf;

    LOGC_INFO(CPM_FF_DDSERVICE, "Start processing DLL Event.ind");

    /* Get the LAS information */
    if (fbapi_get_device_parameter(testChannelID, PARAM_ID_LAS_ADDRESS, 4, (USIGN8*) &activeLAS) == E_OK)
    {	 
	   if(event_ind->evt_code == EVT_LL_CHANGED)
	   {
		   // send LAS to connection manager.
		   (*setLASNodeAddress)(activeLAS);
	   }
    }
    if (event_ind->evt_code == EVT_LL_CLEARED)
    {
        LOGC_INFO (CPM_FF_DDSERVICE, "Life list cleared");
    }
    else if ((event_ind->evt_code == EVT_LL_CHANGED))
    {
        LOGC_INFO (CPM_FF_DDSERVICE, "Life list changed --> elems:%d", event_ind->data.ll_chg.elems);

        /* Notification to connection manager that device has been disconnected */
        int status ;
        for (i = 0; i < event_ind->data.ll_chg.elems; i++)
        {
        	 if (event_ind->data.ll_chg.list[i].status == 0)
        	 {
        		 status = event_ind->data.ll_chg.list[i].status;
        		 node_number = event_ind->data.ll_chg.list[i].node;

        		 LOGC_INFO(CPM_FF_DDSERVICE, "status  == %d\n",status);

				 if ((SUCCESS) == (*connectdisconnNotification)(node_number))
				 {
					 LOGC_INFO(CPM_FF_DDSERVICE, "Device with node address \"%d\" is disconnected\n",node_number);
				 }
				 else
				 {
					 LOGC_INFO(CPM_FF_DDSERVICE, "Device with node address \"%d\" is not connected\n",node_number);
				 }
        	 }

             LOGC_INFO (CPM_FF_DDSERVICE, "node: %d is now %d\n", event_ind->data.ll_chg.list[i].node, event_ind->data.ll_chg.list[i].status);
        }
    }
    //CPMHACK-> to notify disconnection of LAS device.
    else if (event_ind->evt_code == EVT_LAS_ROLE_CHANGED)
   {
        if((SUCCESS) == (*connectdisconnNotification)(activeLAS))
		{
			 LOGC_INFO(CPM_FF_DDSERVICE, "Device with address =%d is disconnected\n",node_number);
		}
   }
   else
   {
        LOGC_ERROR ( CPM_FF_DDSERVICE, "   Illegal event %d", event_ind->evt_code);
   }

   LOGC_INFO (CPM_FF_DDSERVICE, "DLL Event.ind processed");

   return SUCCESS;
}

/***********************************************************************
 *
 *  Name: handle_config_con
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and proceed further if
 *      no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

static int handle_sm_event_ind (VOID)
{

    T_SM_EVENT_IND *    event_ind;
    USIGN8              str_len;

    event_ind = (T_SM_EVENT_IND *) global_res_buf;
    switch (event_ind->evt_code)
    {
        case SM_EVT_DUPLICATE_NODE_ADDR:
            printf("!!!Duplicate Address event, FFusb runnning at a default address 0x%x!!!\n", event_ind->description[0]);
            printf("!!!Please select a different node address and restart the application!!!\n");
            break;

        case SM_EVT_PD_TAG_SET:
            /* this would occur only if the host would sm_support for SetPDTag as an agent */
            /* description has length of string as first byte, add terminating 0 for printf */
            str_len = event_ind->description[1];
            if (str_len <= (USIGN8) sizeof(event_ind->description) - 2u)
            {
                event_ind->description[str_len+1] = 0;
            }
            else
            {
                event_ind->description[sizeof(event_ind->description)-1] = 0;
            }
            printf("PD tag changed via FF to be %s", &event_ind->description[1]);
            break;

        case SM_EVT_PD_TAG_CLEARED:
            /* this would occur only if the host would sm_support for ClearPDTag as an agent */
            printf("PD tag has been cleared via FF");
            break;

        case SM_EVT_NODE_ADDR_SET:
            /* this would occur only if the host would sm_support for SetAddress as an agent */
            printf("Address set to %x via FF", event_ind->description[0]);
            break;

        case SM_EVT_NODE_ADDR_CLEARED:
            /* this would occur only if the host would sm_support for ClearAddress as an agent */
            printf("Address cleared to default address %x via FF", event_ind->description[0]);
            break;

    }

    return SUCCESS;
}

void print_service_err (VOID)
{
    T_ERROR *       err_ptr;


    err_ptr = (T_ERROR *) global_res_buf;
    printf ("     Negative Confirmation (class_code): %x\n", err_ptr->class_code);
    printf ("     Negative Confirmation (class_code): %s\n", err_ptr->add_description);

}


/***********************************************************************
 *
 *  Name: print_sm_err
 *
 *  ShortDesc:  Confirmation infromation from remote Device for request
 *
 *  Description:
 *      The handle_config_con function prints the error recieved
 *      for the request handle_config_req and proceed further if
 *      no error received.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      SUCCESS on success
 *      FAILURE
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

void print_sm_err (VOID)
{
    T_SM_ERROR *       err_ptr;

    err_ptr = (T_SM_ERROR *) global_res_buf;
    LOGC_ERROR ("Negative Confirmation: %x\n", err_ptr->rc);

}

/***********************************************************************
 *
 *  Name: process_con_ind_handler
 *
 *  ShortDesc:  Handle the Confirmation or Indication response.
 *
 *  Description:
 *      The process_con_ind_handler function handles the confirmation or
 *      indication response from the FBK borad. It checks for the type of the
 *      service and calls the respective handler function.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from handler functions..
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/

int process_con_ind_handler (unsigned char *res_data, unsigned short len)
{
    int r_code = CM_SUCCESS;
    USIGN8 service;

    (void *)memcpy((char *)global_res_buf, (char *)res_data, len);

    if (FMS_USR == get_service_layer())
    {
        service = get_service_ref();
        LOGC_TRACE(CPM_FF_DDSERVICE,"CON_IND: FMS Layer: Service %d", service);
        switch (service)
        {
            case FMS_READ:
                if (CON == get_service_primitive())
                {
                    r_code = handle_read_con();
                }
                break;

            case FMS_WRITE:
                if (CON == get_service_primitive())
                {
                    r_code = handle_write_con();
                }
                 break;

#ifdef ALL_FF_SERVICES_SUPPORTED
            case FMS_INFO_RPT:
                    r_code = SUCCESS;
                break;

            case FMS_EVN_NOTIFY:
                r_code = handle_event_ind();
                break;
#endif

            case FMS_INITIATE:
                if (CON == get_service_primitive())
                {
                    r_code = handle_initiate_con();
                }
                break;

            case FMS_ABORT:
                r_code = handle_abort_ind();
                break;

            case FMS_REJECT:
                r_code = handle_reject_ind();
                break;

#ifdef ALL_FF_SERVICES_SUPPORTED
            case FMS_CREATE_VFD_LOC:
            case FMS_INIT_LOAD_OD_LOC:
            case FMS_LOAD_OD_LOC:
            case FMS_TERM_LOAD_OD_LOC:
                {
                /* not used as not necessary to create a VFD + OD for an host application */
                    r_code = SUCCESS;
                break;
                }
#endif

            case FMS_GET_OD:
                if (CON == get_service_primitive())
                {
                    r_code = handle_get_od_con();
                }
                break;

#ifdef ALL_FF_SERVICES_SUPPORTED
            case FMS_GEN_INIT_DOWNL_SEQ:
            case FMS_GEN_TERM_DOWNL_SEQ:
            case FMS_GEN_DOWNL_SEG:
            case FMS_INIT_UPL_SEQ:
            case FMS_UPL_SEG:
            case FMS_TERM_UPL_SEQ:
                /* our application does not act as a configuration host, so
                GenericDomain Download is not required (no LAS schedule download.
                It would be necessary to implement if SW Download for field devices
                would be supported */
                r_code = SUCCESS;
                break;

            case FMS_OD_READ_LOC:
                r_code = SUCCESS;
                break;

            case FMS_ACK_EVN_NOTIFY:
                r_code = handle_ack_event_con();
                break;
#endif

            default:
                set_device_admin_state(DEVACC_APPL_STATE_END);
                break;
        }
    }

    else if (NMA_USR == get_service_layer())
    {
        service = get_service_ref();
        LOGC_TRACE(CPM_FF_DDSERVICE, "CON_IND: NMA Layer: Service %d", service);
        switch (service)
        {
#ifdef ALL_FF_SERVICES_SUPPORTED
            case NMA_LOAD_CONFIG:
                r_code = handle_config_con();
                break;
#endif

            case NMA_LOAD_DLL_CONFIG:
                r_code = handle_dll_config_con((T_NMA_INFO*)global_res_buf);
                break;

            case NMA_INIT_LOAD_VCRL:
                r_code = handle_vcrl_init_con();
                break;

            case NMA_LOAD_VCRL_ENTRY:
                                    /*
                     * This service shall be called if and anly if
                     * VCR informations of the remote device
                     * are known to the Host device.
                     */
                break;

            case NMA_TERM_LOAD_VCRL:
                r_code = handle_vcrl_term_con();
                break;

            case MGMT_INITIATE:
                r_code = handle_mgmt_initiate_ind(NMA);
                break;

            case MGMT_ABORT:
               // r_code = handle_mgmt_abort_ind();
                break;

            case MGMT_READ:
                if (CON == get_service_primitive())
                {
                    r_code = handle_mgmt_read_con(NMA);

                }
                else
                {
                    r_code = handle_mgmt_read_ind(NMA);
                }

                break;

            case MGMT_WRITE:
                if (CON == get_service_primitive())
                {
                    r_code = handle_mgmt_write_con(NMA);
                }
                else if(IND == get_service_primitive())
                {
                    r_code = handle_mgmt_write_ind(NMA);
                }

                break;

#ifdef ALL_FF_SERVICES_SUPPORTED
            case NMA_INIT_LOAD_SCHED_LOC:
            case NMA_LOAD_SCHED_LOC:
            case NMA_TERM_LOAD_SCHED_LOC:

                /* our host does not act as a configurator which would have to download
                an LAS schedule. Here this is not necessary */
                r_code = SUCCESS;
                break;

            case NMA_SCHED_UPDATE:

                /* no use case for a standard host:
                The service would indicate when an LAS schedule is downloaded
                to the MIB via FF by a remote host which a host will not support/permit */
                r_code = SUCCESS;
                break;

            case NMA_INIT_UPL_SCHED_LOC:
            case NMA_UPL_SCHED_LOC:
            case NMA_TERM_UPL_SCHED_LOC:

                /* no use case for a standard host:
                The service could be used to upload an LAS schedule that is
                remotely loaded in order to store. But as a host would not support/permit
                the remote download via FF this service is not required. */

                r_code = SUCCESS;
                break;

            case NMA_RESET:
                r_code =  handle_nma_reset_con( );
                break;

            case NMA_READ_IDENT:
                r_code = SUCCESS;
                break;

            case NMA_DATA_READ_QUERY:
            	r_code = handle_nma_data_query_con();
            	break;
#endif

            default:
                set_device_admin_state( DEVACC_APPL_STATE_END);
                r_code = SUCCESS;
                break;
        }
    }

    else if (SM_USR == get_service_layer())
    {
        service = get_service_ref();
        LOGC_TRACE(CPM_FF_DDSERVICE, "CON_IND: SM Layer: Service %d", service);
        switch (service)
        {
            case SM_SET_CONFIGURATION:
                r_code = handle_sm_config_con();
                break;

#ifdef ALL_FF_SERVICES_SUPPORTED
            case SM_LOAD_SCHEDULE:
            case SM_FB_START:
                r_code = SUCCESS;
                break;
#endif

            case SM_IDENTIFY:
                    r_code = handle_identify_con();
                break;

            case SM_SET_PD_TAG:
                r_code = handle_set_pd_tag_con();
                break;

            case SM_SET_ADDRESS:
                r_code = handle_set_addr_con();
                break;

            case SM_CLEAR_ADDRESS:
                r_code = handle_clear_addr_con();
                break;

            case SM_FIND_TAG_QUERY:

                if (CON == get_service_primitive())
                {
                    r_code = handle_find_tag_query_con();
                }
                else
                {
                    /* our host will not respond to find tag query indications */
                }
                break;

            case SM_FIND_TAG_REPLY:
                if (IND == get_service_primitive())
                {
                    r_code = handle_find_tag_reply_ind();
                }
                break;

            case SM_EVENT:
                r_code = handle_sm_event_ind();
                break;

            case SM_DLL_EVENT:
                r_code = handle_sm_dll_event_ind();
                break;

            default:
                set_device_admin_state( DEVACC_APPL_STATE_END);
                break;
        }
    }
    else
    {
        LOGC_TRACE(CPM_FF_DDSERVICE, "CON_IND: Unknown layer: %d", get_service_layer());
        set_device_admin_state( DEVACC_APPL_STATE_END);
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name: handle_read_response
 *
 *  ShortDesc:  handle the read response from the FBK library
 *
 *  Description:
 *      The handle_read_response calls the function fbapi_rcv_con_ind and
 *      handles the data recieved from the device in global_res_buf,
 *      based on the confirmation or indication recieved from the FBK board it
 *      calls process_con_ind_handler function.
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      Sets the error code in global error structure device_error
 *
 *  Returns:
 *      return code from the function process_con_ind_handler()
 *
 *  Author:
 *      Softing
 *
 **********************************************************************/
int handle_read_response(int host_type)
{
    FB_INT16 ret_val,i;
    int r_code = CM_SUCCESS;
    T_FIELDBUS_SERVICE_DESCR devacc_sdb = {0};
	FB_INT8 invoke_id = 0;
    int channel = get_device_admin_channel();
    unsigned char res_buffer[MAX_FF_DATA_LEN];
    unsigned short res_buff_len;

	invoke_id = get_device_admin_invokeid();

	res_buff_len = sizeof(res_buffer);
	memset((char *)res_buffer, 0, MAX_FF_DATA_LEN);

	ret_val = fbapi_rcv_con_ind(channel, &devacc_sdb,
			res_buffer, &res_buff_len);
	
    set_desc_header(&devacc_sdb);

    r_code = ret_val;

    if (ret_val == CON_BUSY_RECEIVED)
    {
        set_device_admin_state(DEVACC_APPL_STATE_READY);
        return r_code;
    }
    else if (ret_val == CON_IND_RECEIVED)
    {
        r_code = process_con_ind_handler(res_buffer, res_buff_len);

        if (DEVACC_APPL_STATE_READY == get_device_admin_state())
        {
            return r_code;
        }
        else
        {
            return CM_IF_NO_DATA_RECVD;
        }
    }
    else if (ret_val != NO_CON_IND_RECEIVED)
    {
        log_interface_error(IND, ret_val);
        if (ret_val != E_IF_SERVICE_CONSTR_CONFLICT)
        {
            return r_code;
        }
    }

    if (DEVACC_APPL_STATE_READY == get_device_admin_state())
    {
        return CM_IF_NO_DATA_RECVD;
    }

    return r_code;
}

/***********************************************************************
 *
 *  Name: log_interface_error
 *
 *  ShortDesc:  Log the FBK interface error information.
 *
 *  Description:
 *      The log_if_error function takes a primitive and error code and logs
 *      in the information in the log file.
 *
 *  Inputs:
 *      primitive --  Service primitive.
 *      error     --  Error code from the FBK intreface library.
 *
 *  Outputs:
 *      Logs in the log file
 *
 *  Returns:
 *      None.
 *
 *  Author:
 *      Jayanth M
 *
 **********************************************************************/
void log_interface_error(uint8_t primitive, int16_t error)
{
    if (primitive == REQ || primitive == RES)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"fbkhost_snd_req_res failed");
    }
    else
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"fbapi_rcv_con_ind returns with error");
    }

    switch (error)
    {
        case E_IF_FATAL_ERROR:
            LOGC_ERROR(CPM_FF_DDSERVICE,"unrecoverable error on board");
            break;

        case E_IF_INVALID_LAYER:
            LOGC_ERROR(CPM_FF_DDSERVICE,"invalid layer                   ");
            break;

        case E_IF_INVALID_SERVICE:
            LOGC_ERROR(CPM_FF_DDSERVICE,"invalid service identifier      ");
            break;

        case E_IF_INVALID_PRIMITIVE:
            LOGC_ERROR(CPM_FF_DDSERVICE,"invalid service primitive      ");
            break;

        case E_IF_INVALID_DATA_SIZE:
            LOGC_ERROR(CPM_FF_DDSERVICE,"not enough cmi data block memory");
            break;

        case E_IF_INVALID_COMM_REF:
            LOGC_ERROR(CPM_FF_DDSERVICE,"invalid communication reference ");
            break;

        case E_IF_RESOURCE_UNAVAILABLE:
            LOGC_ERROR(CPM_FF_DDSERVICE,"no resource available           ");
            break;

        case E_IF_NO_PARALLEL_SERVICES:
            LOGC_ERROR(CPM_FF_DDSERVICE,"no parallel services allowed    ");
            break;

        case E_IF_SERVICE_CONSTR_CONFLICT:
            LOGC_ERROR(CPM_FF_DDSERVICE,"serv. tempor. not executable    ");
            break;

        case E_IF_SERVICE_NOT_SUPPORTED:
            LOGC_ERROR(CPM_FF_DDSERVICE,"service not supported           ");
            break;

        case E_IF_SERVICE_NOT_EXECUTABLE:
            LOGC_ERROR(CPM_FF_DDSERVICE,"service not executable          ");
            break;

        default:
            LOGC_ERROR(CPM_FF_DDSERVICE,"unknown IF error : %d", error);
            break;
    }

    return ;
}
