/*
 * os_helper.c
 *
 *      Author: 20034597
 */


#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

#include "os_helper.h"
#include "cm_lib.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "socket_comm.h"

#define SHARED_MEM_SIZE     1024
#define SHARED_KEY		  5678

static pthread_mutex_t mem_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t dpc_sync_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t fbk_read_mutex = PTHREAD_MUTEX_INITIALIZER;

static char test_sh_mem[1024] = {0};

static pthread_mutex_t g_fbk_data_thread_mutex = PTHREAD_MUTEX_INITIALIZER;
static int g_FbkDataThreadStatus = THREAD_STATE_NONE;

int write_to_shm(char *shm, int type, void *data)
{
    int r_code;
	pthread_mutex_lock(&mem_mutex);
	{
		switch(type)
		{
			case NMA_DATA_TYPE:
				{
				CM_NMA_INFO *nma_info = (CM_NMA_INFO *)data;
				(void *)memcpy(&test_sh_mem[CM_NMA_LOC], (char *)nma_info, sizeof(CM_NMA_INFO));
				test_sh_mem[CM_NMA_VALID_LOC] = 1;


				}
				break;

			case LISTEN_FD_DATA_TYPE:
				{
				int *listen_fd = (int *)data;
				(void *)memcpy(&test_sh_mem[LISTEN_FD_LOC], (char *)listen_fd, sizeof(int));
				}
				break;

			case ACCEPT_FD_DATA_TYPE:
				{
				int *accept_fd = (int *)data;
				(void *)memcpy(&test_sh_mem[ACCEPT_FD_LOC], (char *)accept_fd, sizeof(int));
				}
				break;
		}

		r_code = CM_SUCCESS;
	}

	pthread_mutex_unlock(&mem_mutex);
    return r_code;
}

int read_from_shm(char *shm, int type, void *data)
{
	int r_code;
	pthread_mutex_lock(&mem_mutex);

	{
		switch(type)
		{
			case NMA_DATA_TYPE:
				{
				CM_NMA_INFO *nma_info = (CM_NMA_INFO *)data;
				(void *)memcpy((char *)nma_info, (char *)&test_sh_mem[CM_NMA_LOC], sizeof(CM_NMA_INFO));
				}
				break;

			case NMA_DATA_VALID_TYPE:
				{
				unsigned char *valid = (unsigned char *) data;
				(void *)memcpy((char *)valid, (char *)&test_sh_mem[CM_NMA_VALID_LOC], sizeof(char));
				}
				break;

			case LISTEN_FD_DATA_TYPE:
				{
				int *listen_fd = (int *)data;
				(void *)memcpy((char *)listen_fd, (char *)&test_sh_mem[LISTEN_FD_LOC],  sizeof(int));
				}
				break;

			case ACCEPT_FD_DATA_TYPE:
				{
				int *accept_fd = (int *)data;
				(void *)memcpy((char *)accept_fd, (char *)&test_sh_mem[ ACCEPT_FD_LOC], sizeof(int));
				}
				break;
		}

		r_code = CM_SUCCESS;

	}

	pthread_mutex_unlock(&mem_mutex);

	return r_code;
}

void acquire_dpc_sync_lock(void)
{
    pthread_mutex_lock(&dpc_sync_mutex);
}

void release_dpc_sync_lock(void)
{
    pthread_mutex_unlock(&dpc_sync_mutex);
}


void acquire_fbk_read_lock(void)
{
    pthread_mutex_lock(&fbk_read_mutex);
}

void release_fbk_read_lock(void)
{
    pthread_mutex_unlock(&fbk_read_mutex);
}

void set_fbk_data_thread_state(int threadState)
{
	g_FbkDataThreadStatus = threadState;
}

int get_fbk_data_thread_state()
{
	return g_FbkDataThreadStatus;
}

void acquire_fbk_data_thread_lock(void)
{
    pthread_mutex_lock(&g_fbk_data_thread_mutex);
}

void release_fbk_data_thread_lock(void)
{
    pthread_mutex_unlock(&g_fbk_data_thread_mutex);
}
