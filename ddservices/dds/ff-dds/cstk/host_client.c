/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file host_client.c
    Client model to open the socket and reading the data.

    This file contains all functions pertaining client host code
    management. This module creates the socket

*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "cm_lib.h"
#include "device_access.h"
#include "socket_comm.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "os_helper.h"
#include "devsim.h"
#include "dds_comm.h"
#include "cm_rod.h"

static int socket_fd;

int create_client_socket(int *socket_id)
{
	int r_code;
	struct sockaddr_in server_addr;

	/*Create UDP socket*/
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd < 0)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to create clien socket");
		return CM_BAD_SOCKET;
	}

	(void *)memset(&server_addr, 0, sizeof(server_addr));
	/*Configure settings in address struct*/
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT_NUMBER);
	server_addr.sin_addr.s_addr = inet_addr(ADDRESS);


	*socket_id = socket_fd;

	/*
	 * Connection of the client to the socket
	 */
	r_code = connect(socket_fd,
			(struct sockaddr *) &server_addr, sizeof(server_addr));
	if(r_code < 0)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in connecting to the server");
		return CM_BAD_BIND_SOCKET;
	}

	return CM_SUCCESS;
}

int send_data_to_server(COMM_REQ *comm_req)
{
	/*Send message to server*/

	int socket;
	SOCKET_DATA  sock_data;
	int r_code;

	(void *)memcpy((COMM_REQ *)&sock_data.comm_req, (COMM_REQ *)comm_req, sizeof(COMM_REQ));

	socket = get_device_admin_socket();

	r_code = send(socket, sock_data.buffer, MAX_RX_SOCKET_BUFF_SIZE, 0);
	if(r_code < 0)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to send the client data over socket %d", errno);
		return CM_BAD_CLIENT_SOCKET;
	}

	return CM_SUCCESS;
}

int recv_data_from_server(unsigned short *code, T_DEVICE_ERROR *dev_err,
                          T_FIELDBUS_SERVICE_DESCR *devacc_sdb, unsigned char *data_buf)
{
	char buffer[MAX_RX_SOCKET_BUFF_SIZE];
	int n_bytes;
	int socket;
	SOCKET_DATA  sock_data;
    fd_set recv_fd;
    struct timeval st_tmv;
    int r_code, errno_save;

    (void *)memset( &st_tmv, 0, sizeof (struct timeval) );
    st_tmv.tv_sec = 5;

	socket = get_device_admin_socket();

    do {
        FD_ZERO( &recv_fd );
        FD_SET( socket, &recv_fd );
        errno = 0;
        r_code = select( socket + 1, &recv_fd, NULL, NULL, &st_tmv );
        errno_save = errno;
    } while ((r_code < 0) && ((errno_save == EINTR) || (errno_save == EAGAIN)));

    if(r_code < 0)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"select returns error code %d", errno_save);
    }
    else if(r_code == 0)
    {
        printf("Recv timeout\n");
        return CM_BAD_RECVFROM;
    }
    else{
        if(FD_ISSET(socket, &recv_fd))
        {
            n_bytes = recv(socket, buffer, MAX_RX_SOCKET_BUFF_SIZE, 0);
	        if(n_bytes < 0)
	        {
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in receiving data over socket");
	  	        return CM_BAD_RECVFROM;
	        }
	        else
	        {
	        	//printf("RECEIVED RESPONSE MESSAGE\n");
		        (void *)memcpy((char *)sock_data.buffer, buffer, MAX_RX_SOCKET_BUFF_SIZE);

                devacc_sdb->comm_ref = sock_data.devacc_sdb.comm_ref;
		        devacc_sdb->invoke_id = sock_data.devacc_sdb.invoke_id;
        		devacc_sdb->layer = sock_data.devacc_sdb.layer;
        		devacc_sdb->primitive = sock_data.devacc_sdb.primitive;
        		devacc_sdb->result = sock_data.devacc_sdb.result;
        		devacc_sdb->service = sock_data.devacc_sdb.service;
        		(void *)memcpy((char *) data_buf, (char *)sock_data.data, MAX_FF_DATA_LEN);
                (void *)memcpy((char *)dev_err, (char *)&sock_data.dev_err, sizeof(T_DEVICE_ERROR));
        		set_rcv_error(dev_err);
        	}
        }
        else
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in receiveing the data");
        }
    }

	return CM_SUCCESS;
}

int get_nma_info_from_server(ENV_INFO *env_info, CM_NMA_INFO *nma_info)
{
	int r_code;

    r_code = send_wait_receive_cmd(env_info, HNDL_NMA_DATA_REQ,
                NULL, NULL);

    if(r_code != CM_SUCCESS) {
		return(r_code);
    }

    if (POS == get_service_result())
    {
       (void *)memcpy((char *)nma_info, (char *)global_res_buf, sizeof(CM_NMA_INFO));

        save_device_admin_stack_mib_dir((T_NMA_INFO *)nma_info);

        return(r_code);
    }
    else if(NEG == get_service_result())
    {
        return CS_FMS_ERR(r_code);
    }

    return CM_SUCCESS;
}

