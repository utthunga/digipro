/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Support module to get the profile class of the device.

    This file contains the supporting function to build the device 
    table with its profile class value.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/
#include "devsim.h"

int get_class_vlaue(int code)
{
    int class_val;
	switch(code)
	{
		case 0x010B:
		case 0x0133:
		case 0x0143:
			class_val = 0;
			break;

		case 0x0101:
		case 0x0103:
		case 0x012E:
		case 0x0130:
		case 0x1002:
		case 0x014D:
		case 0x014F:
            class_val = 1;
			break;

		case 0x0102:
		case 0x0104:
		case 0x012F:
		case 0x0131:
		case 0x1004:
		case 0x014E:
		case 0x0150:
            class_val = 2;
			break;

		case 0x0148:
		case	0x0115:
		case	0x0158:
		case	0x010C:
		case	0x013D:
		case	0x0113:
		case	0x011A:
		case	0x0160:
		case	0x0119:
		case	0x0159:
		case	0x0145:
		case	0x0146:
		case	0x0161:
		case	0x014C:
		case	0x0156:
		case	0x0157:
		case	0x0117:
		case	0x0147:
		case	0x014B:
		case	0x0151:
		case	0x0164:
		case	0x0165:
		case	0x0166:
			class_val = 4;
				break;

		default:
            class_val = 3;
			break;
	}

    return class_val;
}
