/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Fake the transmitter.

    This file contains the code to fake the transmitter and simulates the
    value of the process variable.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "sm.h"
#include "device_access.h"
#include "fakexmtr.h"
#include "tst_cmn.h"

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

/***********************************************************************
 *
 *  Name:  sim_get_host_info
 *
 *  ShortDesc:  Get the information of the Host Simulator.
 *
 *  Description:
 *      Read the simulator information from the fake transmitter file.
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *      host_info: Pointer to the Device info structure
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
sim_get_host_info(ENV_INFO *env_info, T_FBAPI_DEVICE_INFO *hi)
{
	hi->DeviceId = 0x10;
	hi->HardwareRevision = 0;
	hi->SerialDeviceNumber = 0;
	hi->BootblockVersion = 0;
	strcpy((char *)hi->BootblockVersionString, "Simulator");
	hi->FirmwareVersion = 0;
	strcpy((char *)hi->FirmwareVersionString, "Simulator firmware");

	return CM_SUCCESS;
}

/***********************************************************************
 *
 *	Name: sim_scan_network
 *
 *
 *	Description:
 *		The sim_scan_network function reads the fakexmtr.txt file and
 *		populates the SCAN_DEV_TABLE.
 *
 *	Inputs:
 *		scan_dev_table - the Table is copied with the scanned device list.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int sim_scan_network(ENV_INFO* env_info, SCAN_DEV_TBL *scan_dev_table)
{
	APP_INFO *app_info;
	FILE *data_file = NULL;
	char file_line[256+1];
	const char *s = "=";
	char *token;
	char p[256];
	SCAN_TBL *temp = NULL;
	const char *fakexmtr_file = XMTR_FILE;
	char       fakexmtr_file_path[FILE_PATH_LEN];
	char       *env_path;

    app_info = (APP_INFO *)env_info->app_info;

   	env_path = getenv(NTOK_PATH_ENV_NAME);
    if(NULL == env_path)
    {
         LOGC_ERROR(CPM_FF_DDSERVICE,"No FAKE Transmitter file found");
         return CM_BAD_DD_PATH;
    }

    (void)sprintf(fakexmtr_file_path, "%s/%s", env_path, fakexmtr_file);

    /*
     *	Open the fake transmitter data file
     */

    data_file = fopen(fakexmtr_file_path, "r");

    if (!data_file) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to open fakexmtr.txt file \n");
    	app_info->env_returns.response_code = CM_SIM_NO_FILE;
    	return CM_SIM_NO_FILE;
    }

    while (!feof(data_file))
    {

    	/*
    	 *	Read in one line of the data file.
    	 */

    	(void) fgets(file_line, 256, data_file);

    	/*
    	 *	Check for comments or blank lines.
    	 */

    	if((strstr(file_line, "Device Address")) != NULL)
    	{
    		(void *)memcpy(p, file_line, sizeof(file_line));
    	    token = strtok(p, s);
    	    token = strtok(NULL, s);
    	    if(NULL != token)
    	    {
    	    	scan_dev_table->count++;
    	    	if(NULL == scan_dev_table->list)
    	    	{
    	    		scan_dev_table->list = (SCAN_TBL *) malloc(sizeof(SCAN_TBL));
    	    	    scan_dev_table->list->station_address =
    	    	    		(UINT8)atoi(token);

    	    	    temp = scan_dev_table->list;
    	    	    temp->next = NULL;
    	    	}
    	    	else
    	    	{
    	    		temp = scan_dev_table->list;
    	    	    while(temp->next != NULL)
    	    	    	temp = temp->next;

    	    	    temp->next = (SCAN_TBL *) malloc(sizeof(SCAN_TBL));
    	    	    temp->next->station_address = (UINT8)atoi(token);
    	    	    temp->next->next = NULL;
    	    	}
    	    }
    	}
    	else
    		continue;
    }

    fclose(data_file);

    return CM_SUCCESS;
}


/***********************************************************************
 *
 *	Name: sim_sm_identify
 *
 *	ShortDesc:  Read the Physical tag and Decice ID from fake xmtr file.
 *
 *	Description:
 *		The sim_sm_identify function takes a node address
 *		and gets the PD tag and Device ID from the fake transmitter
 *		file.
 *
 *	Inputs:
 *		dev_addr - node address of the device
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		FFDS_SUCCESS.
 *
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int sim_sm_identify (
						 ENV_INFO* env_info,
						 unsigned short dev_addr,
						 CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
	APP_INFO *app_info;
    FILE *data_file = NULL;
    char file_line[256+1];
    const char *s = "=";
    char *token;
    char p[256];

	const char *fakexmtr_file = XMTR_FILE;
	char       fakexmtr_file_path[FILE_PATH_LEN];
	char       *env_path;

    app_info = (APP_INFO *)env_info->app_info;

   	env_path = getenv(NTOK_PATH_ENV_NAME);
    if(NULL == env_path)
    {
         LOGC_ERROR(CPM_FF_DDSERVICE,"No FAKE Transmitter file found");
         return CM_BAD_DD_PATH;
    }

    (void)sprintf(fakexmtr_file_path, "%s/%s", env_path, fakexmtr_file);

    /*
     *	Open the fake transmitter data file
     */

    data_file = fopen(fakexmtr_file_path, "r");

	if (!data_file) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to open fakexmtr.txt file \n");
	   	app_info->env_returns.response_code = CM_SIM_NO_FILE;
	  	return CM_SIM_NO_FILE;
	}

	while (!feof(data_file))
	{
		/*
	 	 *	Read in one line of the data file.
	     */

	    (void) fgets(file_line, 256, data_file);

	    if((strstr(file_line, "Device ID")) != NULL)
	    {
	    	(void *)memcpy(p, file_line, sizeof(file_line));
	        token = strtok(p, s);
	        token = strtok(NULL, s);
	        if(NULL != token)
	        {
	        	if(dev_addr == (unsigned short)atoi(token) )
	        	{
	        		token = strtok(NULL, s);
	        		if(NULL != token)
	        			strcpy((char *)sm_identify_cnf->dev_id, token);
	        	}
	        }
	        else
	        	strcpy((char *)sm_identify_cnf->dev_id, "No String Found");
    	}
	    if((strstr(file_line, "PD Tag")) != NULL)
	    {
	      	(void *)memcpy(p, file_line, sizeof(file_line));
	        token = strtok(p, s);
	        token = strtok(NULL, s);
	        if(NULL != token)
	        {
	          	if(dev_addr == (unsigned short)atoi(token) )
	          	{
	          		token = strtok(NULL, s);
	           		if(NULL != token)
	           			strcpy((char *)sm_identify_cnf->pd_tag, token);
	           	}
	        }
	        else
	           	strcpy((char *)sm_identify_cnf->pd_tag, "No String Found");
	    }
	}

    app_info->env_returns.comm_err = CM_SUCCESS;
    app_info->env_returns.response_code = 0;

    fclose(data_file);

    return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name: sim_init_network
 *
 *  ShortDesc:  Initialize a single network of simulated Type A devices.
 *
 *  Description:
 *      The sim_init_network function takes a Configuration File ID and
 *      initializes a single network of simulated Type A devices.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from new_network function.
 *      Return values from initialize_tables funciton.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/

int
sim_init_network(int config_file_id, int net_type)
{
    int     r_code;

    /*
     *  Create a new network.
     */

    r_code = new_network(config_file_id);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    /*
     *  Initialize all of the tables used in the network.
     */

    r_code = initialize_tables(netwk_tbl.count - 1);
    if (r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  read_mode_block
 *
 *  ShortDesc:  Read the Mode/status of the block
 *
 *  Description:
 *      This function fakes the mode of the block. It assigns
 *      status=good, non cascaded and good quality to the mode of
 *      the block.
 *
 *  Inputs:
 *      env_info
 *      	Environmental variable
 *
 *  Outputs:
 *      mode_block - Mode of the block.
 *
 *  Returns:
 *      CM_SUCCESS.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
static void get_device_details(FILE *data_file)
{
	int count = 0;
	char file_line[256 + 1];
	int manufacturer;
	int dev_type;
	int dev_rev;
	int dd_rev;
	char *pos;

	while (!feof(data_file)) {

		/*
		 *	Read in one line of the data file.
		 */

		(void) fgets(file_line, 256, data_file);

		if((strstr(file_line, "[VFD 2 Resource Block Defaults]")) != NULL){
		while (count < 4){
			(void) fgets(file_line, 256, data_file);
			if((strstr(file_line, "MANUFAC_ID")) != NULL){

				pos = strchr(file_line,'=');
				manufacturer = strtoul (pos+1,NULL,0);
				set_device_admin_manufac(manufacturer);
				count++;

			}else if((strstr(file_line, "DEV_TYPE")) != NULL){

				pos = strchr(file_line,'=');
				dev_type = strtoul (pos+1,NULL,0);
				set_device_admin_dev_type(dev_type);
				count++;

			}else if((strstr(file_line, "DD_REV")) != NULL){
				pos = strchr(file_line,'=');
				dd_rev = strtoul (pos+1,NULL,0);
				set_device_admin_dd_rev(dd_rev);
				count++;

			}else if((strstr(file_line, "DEV_REV")) != NULL){
				pos = strchr(file_line,'=');
				dev_rev = strtoul (pos+1,NULL,0);
				set_device_admin_dev_rev(dev_rev);
				count++;
				}
			}
			break;
		}
	}
	fseek(data_file, 0, SEEK_SET);
	return;
}


/***********************************************************************
 *
 *  Name:  read_mode_block
 *
 *  ShortDesc:  Read the Mode/status of the block
 *
 *  Description:
 *      This function fakes the mode of the block. It assigns
 *      status=good, non cascaded and good quality to the mode of
 *      the block.
 *
 *  Inputs:
 *      env_info
 *      	Environmental variable
 *
 *  Outputs:
 *      mode_block - Mode of the block.
 *
 *  Returns:
 *      CM_SUCCESS.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
static int read_mode_block(ENV_INFO *env_info,unsigned char *mode_block)
{
	APP_INFO        *app_info = (APP_INFO *)env_info->app_info;
	/*
	 * Fake the mode of the block
	 */
	*mode_block = 0x80;
	app_info->env_returns.response_code = 0;

	return CM_SUCCESS;
}

/***********************************************************************
 *
 *  Name:  get_dd_path
 *
 *  ShortDesc:  Read the Mode/status of the block
 *
 *  Description:
 *      This function fakes the mode of the block. It assigns
 *      status=good, non cascaded and good quality to the mode of
 *      the block.
 *
 *  Inputs:
 *      env_info
 *      	Environmental variable
 *
 *  Outputs:
 *      mode_block - Mode of the block.
 *
 *  Returns:
 *      CM_SUCCESS.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
static int get_dd_path(char *dd_path)
{
	char *env_var_path = NULL;

	env_var_path = getenv("TOK_RELEASE_DIR");
	if(NULL == env_var_path)
	{
		return CM_BAD_DD_PATH;
	}
	else
	{
		(void)strcpy(dd_path, env_var_path);
		return CM_SUCCESS;
	}
}



static int get_block_data(SOFTING_BLOCK_INFO *block, FILE *data_file)
{
	int				 count = 0;
	char 			file_line[256+1];
	char 			*pos;

	while(!feof(data_file) && count < 4)
	{
		(void) fgets(file_line, 256, data_file);
		if (((file_line[0] == '/') && (file_line[1] == '/')) || (file_line[0] == '\n')) {

			/*
			 *	Go on to read the next line of the data file.
			 */

			continue;

		}else{
			if((strstr(file_line, "Block_Index") || (strstr(file_line, "BLOCK_INDEX"))) != 0 ){

				pos = strchr(file_line,'=');
				block->op_index = strtoul (pos+1,NULL,0);
				count++;

			}else if((strstr(file_line, "DD_Item") || (strstr(file_line, "DD_ITEM"))) != 0 ){

				pos = strchr(file_line,'=');
				block->DD_Item_ID = strtoul (pos+1,NULL,0);
				count++;

			}else if((strstr(file_line, "NUM_OF_PARMS")||(strstr(file_line, "Num_Of_Parms"))) != 0 ){

				pos = strchr(file_line,'=');
				block->no_params = strtoul (pos+1,NULL,0);
				count++;
            }

			else if((strstr(file_line, "PROFILE")||(strstr(file_line, "Profile"))) != 0 ){
				if((strstr(file_line, "REVISION")||(strstr(file_line, "Revision"))) == 0 ){
				    pos = strchr(file_line,'=');
				    block->Profile = get_class_vlaue(strtoul (pos+1,NULL,0));
				    count++;
                }
			}

		}
	}
	if(count != 4){
                LOGC_ERROR(CPM_FF_DDSERVICE,"Cff parser variable missing in file \n");
		return(CM_BAD_FILE_OPEN);
	}

	return CM_SUCCESS;
}
/***********************************************************************
 *
 *  Name:  read_cff_file
 *
 *  ShortDesc:  Read the CFF file
 *
 *  Description:
 *      The read_cff_file function takes a ENV_INFO and NETWORK_HANDLE
 *      and reads in and 'stores' the corresponding data.
 *      This data file contains all
 *      necessary information pertaining to blocks and their associated
 *      devices that will be used in a connection session.
 *
 *  Inputs:
 *      nh - NETWORK_HANDLE
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      FFDS_FILE_OPEN_ERR.
 *      Return values from cfg_file_id_to_netwk_num function.
 *      Return values from scan_block_entry function.
 *      Return values from store_block_entry function.
 *
 *  Author:
 *      Jon Reuter
 *
 **********************************************************************/
int read_cff_file(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    BLK_ENTRY       *blk_entry;
    BLK_ENTRY_TBL   *blk_entry_tbl;
    FIND_CNF_TBL    *find_cnf_tbl;
    DEV_DATA_TBL    *dev_data_tbl;
    int             r_code;
    USIGN8          node_address;
    NET_TYPE        net_type;
    unsigned char   mode_block;
    FILE 		    *data_file = NULL;
    char 			file_line[256+1];
    const char 		*s = "=";
    char 			*token;
    char 			p[256];
    APP_INFO        *app_info = (APP_INFO *)env_info->app_info;
    char			cff_file_path[256+1];
    char 			dd_path[512+1];
    SOFTING_BLOCK_INFO	block = {0};
    int 			funct_blk_count = 0;
    int				trans_blk_count = 0;
	int					i=0, len=0, count=0;
	char				*str, *pos;

	const char *fakexmtr_file = XMTR_FILE;
	char       fakexmtr_file_path[FILE_PATH_LEN];
	char       *env_path;


    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    /*
     *  Obtain the network's tables.
     */

    blk_entry_tbl = &netwk_tbl.list[nh].blk_entry_tbl;
    find_cnf_tbl = &netwk_tbl.list[nh].find_cnf_tbl;
    dev_data_tbl = &netwk_tbl.list[nh].dev_data_tbl;

    r_code = get_nt_device_addr(nh, &node_address);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    r_code = read_mode_block(env_info,&mode_block);
    if(r_code != CM_SUCCESS)
    {
        return r_code;
    }
    /*
     *	Open the fake transmitter data file
     */

   	env_path = getenv(NTOK_PATH_ENV_NAME);
    if(NULL == env_path)
    {
         LOGC_ERROR(CPM_FF_DDSERVICE,"No FAKE Transmitter file found");
         return CM_BAD_DD_PATH;
    }

    (void)sprintf(fakexmtr_file_path, "%s/%s", env_path, fakexmtr_file);

    /*
     *	Open the fake transmitter data file
     */

    data_file = fopen(fakexmtr_file_path, "r");

    if (!data_file) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to open fakexmtr.txt file \n");
       	app_info->env_returns.response_code = CM_SIM_NO_FILE;
      	return CM_SIM_NO_FILE;
    }


    r_code = get_dd_path(dd_path);
    if(r_code != CM_SUCCESS)
    {
    	return r_code;
    }

    while (!feof(data_file))
    {
    	/*
     	 *	Read in one line of the data file.
         */

        (void) fgets(file_line, 256, data_file);

        if((strstr(file_line, "Device Address")) != NULL)
        {
        	(void *)memcpy(p, file_line, sizeof(file_line));
            token = strtok(p, s);
            token = strtok(NULL, s);
            if(NULL != token)
            {
            	if(node_address == (unsigned short)atoi(token) )
            	{
            		token = strtok(NULL, s);
            		if(NULL != token)
            			strcpy((char *)cff_file_path, token);
            	}
            }
            else
            	return CM_SIM_NO_FILE;
       	}
    }

    fclose(data_file);

    sprintf(dd_path, "%s/%s",dd_path, cff_file_path);

    LOGC_INFO(CPM_FF_DDSERVICE,"CFF FILE PATH = %s\n", dd_path);

    for(i = 0; i < 256; i++)
    {
    	if('\n' == dd_path[i])
    	{
    		dd_path[i] = '\0';
    		break;
    	}
    }

    /*
     *	Open the CFF data file
     */

    data_file = fopen(dd_path, "r");
	if (!data_file) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to open CFF file");
		return(CM_BAD_FILE_OPEN);
	}

	/*
	 * Get the Device manufacturer, Device type, Device Revision
	 * Device Type and Device Revision details from the .CFF file
	 * and store it in device_admin data structure.
	 */
	get_device_details(data_file);

	/*
	 * Get the Block details of the transmitter from the
	 * CFF file such as,
	 * Block tag, Block operational index, Parameter count and
	 * Block type.
	 */
	while (!feof(data_file)) {

		/*
		 *	Read in one line of the data file.
		 */

		(void) fgets(file_line, 256, data_file);

		/*
		 *	Check for comments or blank lines.
		 */

		if ((file_line[0] == '[')) {

			if((strstr(file_line, "[VFD 2 Resource Block]")) != NULL){

				(void)sprintf(block.block_tag, "%s", "RESOURCE");
				r_code = get_block_data(&block, data_file);
				if(r_code != CM_SUCCESS)
				{
					fclose(data_file);
					return r_code;
				}
				blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
				if (!blk_entry) {
				    free(blk_entry);
				    fclose(data_file);
				    return(FFDS_MEMORY_ERR);
				}

				block.class_type = BC_PHYSICAL;
				block.mode_block = mode_block;

				// Fill in blk_entry from softing_block
				r_code = add_block_entry(&block, blk_entry, blk_entry_tbl,
				             find_cnf_tbl, dev_data_tbl, nh);
				if(r_code != CM_SUCCESS)
				{
					fclose(data_file);
					return r_code;
				}

				block.class_type = -1;
			}
			else if(((strstr(file_line,"[VFD")) || (strstr(file_line,"[vfd"))) != 0 ){

				pos = strchr(file_line, ']'); // Check for delimiter
				len = pos-&file_line[0];
				str = malloc(len+1);
				memcpy(str, file_line, len+1);
				for(i=0,count = 0;i<=len;i++) //finding the numbers of words in the string
				{
				   if(str[i]==' ')
					  count++;
				}
                free(str);
				/* Checking for string which contains "[VFD 2 (Function or Transducer) Block No:] */
				if(count == 4){
					if(strstr(file_line, "Function Block") != NULL){
						(void)sprintf(block.block_tag, "%s%d", "FUNCTION_",
							funct_blk_count+1);
						r_code = get_block_data(&block, data_file);
						if(r_code != CM_SUCCESS)
						{
							fclose(data_file);
							return r_code;
						}
						funct_blk_count++;

						blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
						if (!blk_entry) {
						    free(blk_entry);
						    fclose(data_file);
						    return(FFDS_MEMORY_ERR);
						}
						block.class_type = BC_UNKNOWN;
						block.mode_block = mode_block;

						/*
						 *  Add function block data into device table
						 */
						r_code = add_block_entry(&block, blk_entry, blk_entry_tbl,
								find_cnf_tbl, dev_data_tbl, nh);
						if(r_code != CM_SUCCESS)
						{
							fclose(data_file);
							return r_code;
						}
						block.class_type = -1;
					}else if(strstr(file_line, "Transducer Block") != NULL){

						(void)sprintf(block.block_tag, "%s%d", "TRANSDUCER_",
							trans_blk_count+1);
						r_code = get_block_data(&block, data_file);
						if (r_code != FFDS_SUCCESS) {
							fclose(data_file);
							return(r_code);
						}
						trans_blk_count++;

						blk_entry = (BLK_ENTRY *) calloc(1, sizeof(BLK_ENTRY));
						if (!blk_entry) {
						    free(blk_entry);
						    fclose(data_file);
						    return(FFDS_MEMORY_ERR);
						}

						block.class_type = BC_TRANSDUCER;
						block.mode_block = mode_block;

						/*
						 *  Add function block data into device table
						 */
						r_code = add_block_entry(&block, blk_entry, blk_entry_tbl,
								find_cnf_tbl, dev_data_tbl, nh);
						if(r_code != CM_SUCCESS)
						{
							fclose(data_file);
							return r_code;
						}
						block.class_type = -1;
					}
				}
			}
		}else{

			/*
			*	Go on to read the next line of the data file.
			*/

			continue;

		}
	}

	fclose(data_file);
	return(FFDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: sim_device_table
 *
 *  ShortDesc:  After Scanning, this function build the device table
 *              of list of connected device of Type A devices.
 *
 *  Description:
 *      The softing_device_table function takes a Configuration File
 *      ID and scans the list of connected device list.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from read_data_device funciton.
 *      Return values from build_devices function.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int sim_device_table(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int     r_code;
    NT_COUNT     nt_count;
    DEV_TBL     dev_table;

    r_code = get_nt_net_count(&nt_count);
    if (r_code != SUCCESS) {
        return (r_code) ;
    }

    r_code = validate_device_entry(env_info, nt_count);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

	/*
	 *	Read the data file for the particular network.
	 */

	r_code = read_cff_file(env_info, nh);
	if (r_code != FFDS_SUCCESS) {
		return(r_code);
	}

    /*
     *  Build the network of simulated Type A devices.
     */

    r_code = build_devices(nh);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    dev_table.station_address =
                netwk_tbl.list[nh].dev_data_tbl.list->station_address;
    dev_table.phys_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->phys_blk_count;
    dev_table.funct_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->funct_blk_count;
    dev_table.trans_blk_count =
                netwk_tbl.list[nh].dev_data_tbl.list->trans_blk_count;
    dev_table.blk_data_tbl.count =
                netwk_tbl.list[nh].dev_data_tbl.list->blk_data_tbl.count;
    dev_table.blk_data_tbl.list =
    (SCAN_BLK_DATA *) netwk_tbl.list[nh].dev_data_tbl.list->blk_data_tbl.list;

    dev_table.dd_device_id.ddid_dd_rev =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_dd_rev;
    dev_table.dd_device_id.ddid_device_rev =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_device_rev;
    dev_table.dd_device_id.ddid_device_type =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_device_type;
    dev_table.dd_device_id.ddid_manufacturer =
        netwk_tbl.list[nh].dev_data_tbl.list->dd_device_id.ddid_manufacturer;

    r_code =  set_nt_dev_tbl(nh, &dev_table) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    return(FFDS_SUCCESS);

}

/***********************************************************************
 *
 *  Name: sim_detach_device
 *
 *  ShortDesc: Detach the device from the live list.
 *
 *  Description:
 *      The softing_detach_device function deletes the memory for the
 *      device in the live list.
 *
 *  Inputs:
 *      config_file_id - the Configuration File ID of the network to
 *                       be initialized.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      FFDS_SUCCESS.
 *      Return values from read_data_device funciton.
 *      Return values from build_devices function.
 *
 *  Author:
 *      Jayanth Mahadeva
 *
 **********************************************************************/
int sim_detach_device(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int         r_code;
    DEV_TBL     dev_table;

    r_code = del_device_entry(env_info, nh);
    if(r_code != FFDS_SUCCESS) {
        return(r_code);
    }

    r_code =  get_nt_dev_tbl (nh, &dev_table);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    dev_table.phys_blk_count = 0;
    dev_table.funct_blk_count = 0;
    dev_table.trans_blk_count = 0;

    dev_table.blk_data_tbl.count = 0;

    r_code =  set_nt_dev_tbl(nh, &dev_table) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    return(FFDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  sim_cm_exit
 *
 *  ShortDesc:  Close the communication manager.
 *
 *  Description:
 *      Call the function fbkhost_exit api to exit the from the stack
 *      and reset the fbk device
 *
 *  Inputs:
 *      env_info: Environmental variable for returning the status
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int
sim_cm_exit(ENV_INFO *env_info)
{

    return CM_SUCCESS;
}
