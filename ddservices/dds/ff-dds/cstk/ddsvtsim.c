/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#include "std.h"
#include "cm_rod.h"
#include "pc_loc.h"
#include "ddsvtsim.h"

/*
 *	Local functions
 */

/***********************************************************************
 *
 *	Name:  get_int
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static UINT32
get_int(UINT8 *bin_ptr, int bin_size)
{

	UINT32	value = 0;
	UINT8	*bin_endp;

	/*
	 *	Go through each byte and total up the integer value.
	 */

	for (bin_endp = bin_ptr + bin_size; bin_ptr < bin_endp; ){

		value <<= 8;
		value += (UINT32)*bin_ptr++;
	}

	return(value);
}


/**********************************************************************
 *
 *  Name: vtsim_init_device
 *  ShortDesc: Initialize the Device table for Parameter Cache simulation
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *
 *		Vtsim_init_device initializes the Active Device Table
 *		application pointer slot for use with Parameter Cache
 *		testing using a simulated device.
 *
 *  Inputs:
 *		device_handle - The device handle for the durrent block.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR.
 *
 *  Also:
 *		ct_block_open, bl_block_load, ds_dd_block_load, pc_open_block,
 *		vtsim_destroy_device
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
vtsim_init_device(DEVICE_HANDLE device_handle)
{
	int	rs;

	rs = set_adt_app_info (device_handle, (void *)100);

	return (rs);
}


/**********************************************************************
 *
 *  Name: vtsim_destroy_device
 *  ShortDesc: Shut down the Parameter Cache simulation
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *
 *		Vtsim_destroy_device clears out the counter used for
 *		Parameter Cache testing using a simulated device.
 *
 *  Inputs:
 *		device_handle - The device handle for the durrent block.
 *
 *  Outputs:
 *		None.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR.
 *
 *  Also:
 *		ct_block_open, bl_block_load, ds_dd_block_load, pc_open_block,
 *		vtsim_destroy_device
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
vtsim_destroy_device(DEVICE_HANDLE device_handle)
{
	int	rs;

	rs = set_adt_app_info (device_handle, (void *)0);

	return (rs);
}


/**********************************************************************
 *
 *  Name: comflt_xlate_types
 *  ShortDesc: Translate from DDL Variable Types to Variable Types
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *
 *		DDL has a set of variable types (defined in ddldefs.h).  Type
 *		A has a different set of variable types (defined in cm_rod.h).
 *		Comflt_xlate_types translates types from DDL types to type A.
 *
 *  Inputs:
 *		pc_elemp - Pointer to Variable Table element.
 *
 *  Outputs:
 *		typep - Pointer where to store type A.
 *		lengthp - Pointer where to store type A variable length.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR
 *
 *  Author:
 *		Conrad Beaulieu
 *
 *********************************************************************/

static int
comflt_xlate_types(const PC_ELEM *pc_elemp, OBJECT_INDEX *typep, UINT8 *lengthp)
{

    int r_code = CM_SUCCESS;
	/*
	 *	Return the length and type as defined for an OPOD, depending
	 *	on the type and length as defined for the Variable Table.
	 */

	switch (pc_elemp->pc_type) {
		case DDS_BOOLEAN_T:
			ASSERT_DBG (pc_elemp->pc_size == DT_BOOLEAN_SIZE);
			*typep = DT_BOOLEAN;
			*lengthp = DT_BOOLEAN_SIZE;
			break;

		case DDS_INTEGER:
			if (pc_elemp->pc_size == DT_INTEGER32_SIZE) {
				*typep = DT_INTEGER32;
				*lengthp = DT_INTEGER32_SIZE;
			} else if (pc_elemp->pc_size == DT_INTEGER16_SIZE) {
				*typep = DT_INTEGER16;
				*lengthp = DT_INTEGER16_SIZE;
			} else {
				ASSERT_DBG (pc_elemp->pc_size == DT_INTEGER8_SIZE);
				*typep = DT_INTEGER8;
				*lengthp = DT_INTEGER8_SIZE;
			}
			break;

		case DDS_UNSIGNED:
		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
		case DDS_INDEX:
			if (pc_elemp->pc_size == DT_UNSIGNED32_SIZE) {
				*typep = DT_UNSIGNED32;
				*lengthp = DT_UNSIGNED32_SIZE;
			} else if (pc_elemp->pc_size == DT_UNSIGNED16_SIZE) {
				*typep = DT_UNSIGNED16;
				*lengthp = DT_UNSIGNED16_SIZE;
			} else {
				ASSERT_DBG (pc_elemp->pc_size == DT_UNSIGNED8_SIZE);
				*typep = DT_UNSIGNED8;
				*lengthp = DT_UNSIGNED8_SIZE;
			}
			break;

		case DDS_FLOAT:
			ASSERT_DBG (pc_elemp->pc_size == DT_FLOATING_POINT_SIZE);
			*typep = DT_FLOATING_POINT;
			*lengthp = DT_FLOATING_POINT_SIZE;
			break;

		case DDS_DOUBLE:
			ASSERT_DBG (pc_elemp->pc_size == DT_DOUBLE_SIZE);
			*typep = DT_DOUBLE;
			*lengthp = DT_DOUBLE_SIZE;
			break;

		case DDS_ASCII:
		case DDS_PASSWORD:
			*typep = DT_VISIBLE_STRING;
			ASSERT_RET (pc_elemp->pc_dev_value.pc_value.pc_string.str_len < 256,
					PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_dev_value.pc_value.pc_string.str_len;
			break;

		case DDS_BITSTRING:
			*typep = DT_BIT_STRING;
			ASSERT_RET (pc_elemp->pc_dev_value.pc_value.pc_string.str_len < 256,
					PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_dev_value.pc_value.pc_string.str_len;
			break;

		case DDS_OCTETSTRING:
			*typep = DT_OCTET_STRING;
			ASSERT_RET (pc_elemp->pc_dev_value.pc_value.pc_string.str_len < 256,
					PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_dev_value.pc_value.pc_string.str_len;
			break;

		case DDS_TIME:
			*typep = DT_TIME_OF_DAY;
			ASSERT_RET (pc_elemp->pc_size < 256, PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_size;
			break;

		case DDS_TIME_VALUE:
			*typep = DT_TIME_VALUE;
			ASSERT_RET (pc_elemp->pc_size < 256, PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_size;
			break;

		case DDS_DATE_AND_TIME:
			*typep = DT_DATE;
			ASSERT_RET (pc_elemp->pc_size < 256, PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_size;
			break;

		case DDS_DURATION:
			*typep = DT_TIME_DIFFERENCE;
			ASSERT_RET (pc_elemp->pc_size < 256, PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_size;
			break;

		case DDS_EUC:
			*typep = DT_ISO_10646_STRING;
			ASSERT_RET (pc_elemp->pc_dev_value.pc_value.pc_string.str_len < 256,
					PC_INTERNAL_ERROR);
			*lengthp = (UINT8)pc_elemp->pc_dev_value.pc_value.pc_string.str_len;
			break;

		default:
            r_code = PC_INTERNAL_ERROR; 
			/*NOTREACHED*/
			break;
	}
	return r_code;
}


/**********************************************************************
 *
 *  Name: vtsim_make_sim_params
 *  ShortDesc: Create parameter OBJECTs for the simluated device
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *
 *		To faciliate testing, there exists a simulated device which
 *		exists on the ROD heap.  In order to read and write values
 *		to and from this "device", there must be parameters OBJECTs
 *		existing for this simulated device.  Vtsim_make_sim_params
 *		uses the information set up in the Parameter Cache for a
 *		block in this device, and creates the appropriate parameter
 *		OBJECTs.
 *
 *  Inputs:
 *		block_handle - The block handle returned by ct_block_open.
 *		pc_p - Pointer to Parameter Cache for the block.
 *		flat_block_dirp - Pointer to the FLAT_BLOCK_DIR structure
 *							for the block
 *
 *  Outputs:
 *		The simulated device in the ROD heap has valid parameter
 *		OBJECTs for this block.
 *
 *  Returns:
 *		PC_SUCCESS, PC_INTERNAL_ERROR, PC_NO_MEMORY,
 *		and error returns from other DDS and Parameter Cache function.
 *
 *  Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
vtsim_make_sim_params(BLOCK_HANDLE block_handle, const PC *pc_p,
	const FLAT_BLOCK_DIR *flat_block_dirp)
{

	void							*ai;
	DEVICE_HANDLE					device_handle;
	ROD_HANDLE						rod_handle;
	PC_ELEM							*pc_elemp, *pc_elem_endp;
	PC_ELEM							*pc_elem_rec_endp;
	OBJECT							*objectp, *type_objectp;
	OBJECT_INDEX					block_op_index;
	SIMPLE_VARIABLE_SPECIFIC		*var_spec;
	ARRAY_SPECIFIC					*array_spec;
	RECORD_SPECIFIC					*record_spec;
	DATA_TYPE_STRUCTURE_SPECIFIC	*data_type_spec;
	UINT8							*value_read;
	UINT16							block_param_count;
	OBJECT_INDEX					*typep, char_rec_type;
	ulong							data_type_index;
	UINT8							*sizep, char_rec_length = 0;
	UINT32							value_size;
    ENV_INFO                        env_info;
 
	char							*value_ptr;
	int								rs;

    (void)memset(&env_info, 0, sizeof(ENV_INFO));

	rs = get_abt_adt_offset (block_handle, &device_handle);
	if (rs != SUCCESS) {
		return (rs);
	}

	/*
	 *	Get the ROD handle
	 */

	rs = get_adt_a_opod_cref (device_handle, &rod_handle);

	/*
	 *	Step through the Parameter Cache, and create a parameter
	 *	in the simulated device for each parameter.
	 */

	pc_elemp = pc_p->pc_list;
	pc_elem_endp = pc_elemp + pc_p->pc_count;
	while (pc_elemp < pc_elem_endp) {

		/*
		 *	Get the OBJECT and OBJECT_SPECIFIC areas from the ROD heap
		 *	that will be needed.
		 */

		objectp = (OBJECT *)rod_calloc (1, sizeof (*objectp));
		if (objectp == NULL) {
			return (PC_NO_MEMORY);
		}
		objectp->specific = 
			(OBJECT_SPECIFIC *)rod_calloc (1, sizeof (*objectp->specific));
		if (objectp->specific == NULL) {
			return (PC_NO_MEMORY);
		}

		/*
		 *	If this is a Record "head", then add a record parameter.  This
		 *	entails creating a new Data Type Structure object which represents
		 *	the record, and then creating the record object.
		 */

		rs = get_abt_op_index (block_handle, &block_op_index);
		if (rs != SUCCESS) {
			return (rs);
		}

		if (pc_elemp->pc_flags & PC_RECORD) {

			/*
			 *	If this is the characteristics record, it is not necessary to
			 *	modify the simulated device, since this object has already
			 *	been set up.  However, verify that the record as defined in
			 *	the DDL matches what is required in the OPROD.
			 */

			if (pc_elemp->pc_po == 0) {

				/*
				 *	Free the object stuff that was malloced on the ROD heap.
				 */

				rod_object_free (objectp);

				/*
				 *	Make sure this is the first time that a characteristic
				 *	record has been found (there can be only one characteristic
				 *	record).
				 */

				ASSERT_RET (char_rec_length == 0, PC_INTERNAL_ERROR);

				/*
				 *	Verify that each member is the expected type and size.
				 */

				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_CLASS_TYPE) {
					panic ("Characteristic record member 1 has wrong type\n");
				}
				if (char_rec_length != BLK_CLASS_SIZE) {
					panic ("Characteristic record member 1 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, 
						&char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_PARENT_CLASS_TYPE) {
					panic ("Characteristic record member 2 has wrong type\n");
				}
				if (char_rec_length != BLK_PARENT_CLASS_SIZE) {
					panic ("Characteristic record member 2 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, 
						&char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_DD_REFERENCE_TYPE) {
					panic ("Characteristic record member 3 has wrong type\n");
				}
				if (char_rec_length != BLK_DD_REFERENCE_SIZE) {
					panic ("Characteristic record member 3 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_DD_REVISION_TYPE) {
					panic ("Characteristic record member 4 has wrong type\n");
				}
				if (char_rec_length != BLK_DD_REVISION_SIZE) {
					panic ("Characteristic record member 4 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_PROFILE_TYPE) {
					panic ("Characteristic record member 5 has wrong type\n");
				}
				if (char_rec_length != BLK_PROFILE_SIZE) {
					panic ("Characteristic record member 5 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_PROFILE_REVISION_TYPE) {
					panic ("Characteristic record member 6 has wrong type\n");
				}
				if (char_rec_length != BLK_PROFILE_REVISION_SIZE) {
					panic ("Characteristic record member 6 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_EXECUTION_TIME_TYPE) {
					panic ("Characteristic record member 7 has wrong type\n");
				}
				if (char_rec_length != BLK_EXECUTION_TIME_SIZE) {
					panic ("Characteristic record member 7 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_PARAM_COUNT_TYPE) {
					panic ("Characteristic record member 8 has wrong type\n");
				}
				if (char_rec_length != BLK_PARAM_COUNT_SIZE) {
					panic ("Characteristic record member 8 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_DSPLY_LIST_INDEX_TYPE) {
					panic ("Characteristic record member 9 has wrong type\n");
				}
				if (char_rec_length != BLK_DSPLY_LIST_INDEX_SIZE) {
					panic ("Characteristic record member 9 has wrong size\n");
				}
				pc_elemp++;
				rs = comflt_xlate_types (pc_elemp, &char_rec_type, &char_rec_length);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				if (char_rec_type != BLK_DSPLY_LIST_COUNT_TYPE) {
					panic ("Characteristic record member 10 has wrong type\n");
				}
				if (char_rec_length != BLK_DSPLY_LIST_COUNT_SIZE) {
					panic ("Characteristic record member 10 has wrong size\n");
				}
				pc_elemp++;
				ASSERT_RET (pc_elemp == pc_elem_endp ||
						pc_elemp->pc_po != 0,
						PC_INTERNAL_ERROR);

				/*
				 *	Verify that the number of parameters in the OPOD
				 *	and in the Parameter Cache agree.
				 */

				rs = rod_read (rod_handle, block_op_index,
						(SUBINDEX) BLK_SIM_PARAM_COUNT_SUBINDEX, &value_read,
						NULL, NULL);
				if (rs != SUCCESS) {
					return (rs);
				}
				block_param_count = (UINT16) get_int(value_read,
						BLK_PARAM_COUNT_SIZE);
				ASSERT_RET (block_param_count == flat_block_dirp->param_tbl.count,
						PC_INTERNAL_ERROR);
				continue;
			}

			/*
			 *	Get the OBJECT and OBJECT_SPECIFIC areas from the ROD heap
			 *	for the Data Type Structure object.
			 */

			type_objectp = (OBJECT *)rod_calloc (1, sizeof (*type_objectp));
			if (type_objectp == NULL) {
				return (PC_NO_MEMORY);
			}
			type_objectp->specific = 
				(OBJECT_SPECIFIC *)rod_calloc (1, sizeof (*type_objectp->specific));
			if (type_objectp->specific == NULL) {
				return (PC_NO_MEMORY);
			}

			record_spec = &objectp->specific->record;
			data_type_spec = &type_objectp->specific->data_type_structure;

			/*
			 *	Get the index of the new data type structure from the
			 *	up-counter that is stored in the Active Device Table.
			 */

			rs = get_adt_app_info (device_handle, &ai);
			if (rs != SUCCESS) {
				return (rs);
			}
			data_type_index = (OBJECT_INDEX) (unsigned long)ai;
			ASSERT_RET (data_type_index >= 100 && data_type_index < 200,
					PC_INTERNAL_ERROR);
			data_type_index++;
			ai = (void *)data_type_index;
			rs = set_adt_app_info (device_handle, ai);
			if (rs != SUCCESS) {
				return (rs);
			}
			type_objectp->index = (OBJECT_INDEX)data_type_index;
			record_spec->type = (OBJECT_INDEX)data_type_index;

			type_objectp->code = OC_DATA_TYPE_STRUCTURE;
			objectp->code = OC_RECORD;
			objectp->index = (unsigned short)pc_elemp->pc_po + block_op_index;
			ASSERT_DBG (pc_elemp->pc_size <= 255);
			data_type_spec->elem_count = (UINT8)pc_elemp->pc_size;

			data_type_spec->type_list = (OBJECT_INDEX *) rod_calloc (
					(int)data_type_spec->elem_count,
					sizeof (*data_type_spec->type_list));
			data_type_spec->size_list = (UINT8 *) rod_calloc (
					(int)data_type_spec->elem_count,
					sizeof (*data_type_spec->size_list));
			if (  data_type_spec->type_list == NULL
			   || data_type_spec->size_list == NULL
			   ) {
				return (PC_NO_MEMORY);
			}

			/*
			 *	Step through the members of the record, filling in
			 *	the Data Type Structure object specific part, and
			 *	keeping track of the total length.
			 */

			value_size = 0;
			pc_elem_rec_endp = pc_elemp + (UINT8)pc_elemp->pc_size;
			typep = data_type_spec->type_list;
			sizep = data_type_spec->size_list;
			while (++pc_elemp <= pc_elem_rec_endp) {
				rs = comflt_xlate_types (pc_elemp, typep, sizep);
				if (rs != PC_SUCCESS) {
					return (rs);
				}	
				value_size += *sizep;
				typep++;
				sizep++;
			}

			/*
			 *	Put the Data Type Structure object into the ROD.
			 */

			rs = rod_put (rod_handle, type_objectp->index, type_objectp);
			if (rs != SUCCESS) {
				return (rs);
			}

			/*
			 *	Put the object into the ROD, and then put a value into the
			 *	ROD for the OBJECT.
			 */

			rs = rod_put (rod_handle, objectp->index, objectp);
			if (rs != SUCCESS) {
				return (rs);
			}
			value_ptr = rod_calloc (1, (int)value_size);
			if (value_ptr == NULL) {
				return (PC_NO_MEMORY);
			}
			rs = rod_write (rod_handle, objectp->index,
						(SUBINDEX) 0, (UINT8 *)value_ptr);
			if (rs != SUCCESS) {
				return (rs);
			}

			continue;
		}

		/*
		 *	If this is an Array "head", then add an array parameter.
		 */

		if (pc_elemp->pc_flags & PC_ARRAY) {
			objectp->index = (unsigned short)pc_elemp->pc_po + block_op_index;
			objectp->code = OC_ARRAY;
			array_spec = &objectp->specific->array;
			ASSERT_DBG (pc_elemp->pc_size < 256);
			array_spec->elem_count = (UINT8)pc_elemp->pc_size;
			rs = comflt_xlate_types (&pc_elemp[1], &array_spec->type,
					&array_spec->size);
			if (rs != PC_SUCCESS) {
				return (rs);
			}	
			value_size = array_spec->elem_count * array_spec->size;

			/*
			 *	Put the object into the ROD, and then put a value into the
			 *	ROD for the OBJECT.
			 */

			rs = rod_put (rod_handle, objectp->index, objectp);
			if (rs != SUCCESS) {
				return (rs);
			}
			value_ptr = rod_calloc (1, (int)value_size);
			if (value_ptr == NULL) {
				return (PC_NO_MEMORY);
			}
			rs = rod_write (rod_handle, objectp->index,
						(SUBINDEX) 0, (UINT8 *)value_ptr);
			if (rs != SUCCESS) {
				return (rs);
			}
			pc_elemp += pc_elemp->pc_size + 1;
			continue;
		}

		/*
		 *	Add a simple variable parameter.
		 */

		objectp->index = (unsigned short)pc_elemp->pc_po + block_op_index;
		objectp->code = OC_SIMPLE_VARIABLE;
		var_spec = &objectp->specific->simple_variable;
		rs = comflt_xlate_types (pc_elemp, &var_spec->type, &var_spec->size);
		if (rs != PC_SUCCESS) {
			return (rs);
		}	
		value_size = var_spec->size;

		/*
		 *	Put the object into the ROD, and then put a value into the
		 *	ROD for the OBJECT.
		 */

		rs = rod_put (rod_handle, objectp->index, objectp);
		if (rs != SUCCESS) {
			return (rs);
		}
		value_ptr = rod_calloc (1, (int)value_size);
		if (value_ptr == NULL) {
			return (PC_NO_MEMORY);
		}
		rs = rod_write (rod_handle, objectp->index,
					(SUBINDEX) 0, (UINT8 *)value_ptr);
		if (rs != SUCCESS) {
			return (rs);
		}
		pc_elemp++;
	}

	/*
	 *	Re-load the block, so that all of the changes to the simulated
	 *	device get reflected in the OPROD.
	 */

	rs = bl_block_load (block_handle, &env_info);
	if (rs != SUCCESS) {
		return (rs);
	}

	return (PC_SUCCESS);
}

