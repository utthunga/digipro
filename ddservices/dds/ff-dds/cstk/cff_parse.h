#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "tst_cmn.h"
#include "devsim.h"
/*
 * Name of program for cff_parse.c.
 */
#define CFF_FILE_PATH  "D:/working_dir/Data/001151/3051/200101.cff"
extern FILE*		infileptr;
extern FILE*		errfileptr;
FILE				*data_file;
char    			file_line[256];
char	    		cff_file_line[256];
char	    		*pos;
int					station_address = 0;
int					manufacturer = 0;
int					dev_type = 0;
int					dev_rev = 0;
extern	char     			infilename[MAX_FILE_NAME_LEN];

#ifdef __cplusplus
}
#endif /* __cplusplus */
