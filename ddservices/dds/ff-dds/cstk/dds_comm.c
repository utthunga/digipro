/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Comm Stack module.

    This file contains functions pertaining to the communication
    of devices on a network.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef SUN
#include <memory.h>     /* K&R only */
#endif

#include "devsim.h"
#include "sm.h"
#include "nm.h"
#include "cm_rod.h"
#include "cm_loc.h"
#include "rcsim.h"
#include "ddi_lib.h"
#include "int_diff.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "host.h"
#include "fakexmtr.h"

int (*appl_event_indication) (ENV_INFO *env_info, char *buffer) = NULL;
int (*softing_stack_events) (ENV_INFO *env_info, char *buffer) = NULL;


/*
 * The Network Simulator Pointers
 */

static int  (*sim_init_ptr) P((void)) ;
static int  (*sim_init_network_ptr) P((int, int)) ;
static int  (*sim_find_block_ptr) P((char *, int, FIND_CNF_A **)) ;
static int  (*sim_initiate_comm_ptr) P((int, int, CREF *, CREF *, CREF *)) ;
static int  (*sim_comm_get_ptr) P((CREF, OBJECT_INDEX, OBJECT **)) ;
static int  (*sim_comm_put_ptr) P((CREF, OBJECT_INDEX, OBJECT *)) ;
static int  (*sim_comm_read_ptr) P((CREF, OBJECT_INDEX, SUBINDEX, UINT8 **, OBJECT **,
            int *)) ;
static int  (*sim_comm_write_ptr) P((CREF, OBJECT_INDEX, SUBINDEX, UINT8 *)) ;
static int  (*sim_comm_upload_ptr) P((CREF, OBJECT_INDEX, UINT8 *)) ;
static int  (*sim_param_read_ptr) P((ENV_INFO *, int, SUBINDEX)) ;
static int  (*sim_param_write_ptr) P((ENV_INFO *, int, SUBINDEX)) ;
static int  (*sim_param_unload_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
static int  (*sim_param_load_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
static int  (*sim_get_host_info_ptr) P((ENV_INFO*, T_FBAPI_DEVICE_INFO *)) ;
static int  (*sim_scan_network_ptr) P((ENV_INFO*,  SCAN_DEV_TBL *));
static int  (*sim_sm_identify_ptr) P((ENV_INFO* ,unsigned short ,
            CR_SM_IDENTIFY_CNF* )) ;
static int  (*sim_device_table_ptr) P((ENV_INFO* , NETWORK_HANDLE));
static int  (*sim_detach_device_ptr) P((ENV_INFO *, NETWORK_HANDLE));
static int  (*sim_cm_exit_ptr) P((ENV_INFO* )) ;
/*
 * The Network stack Pointers
 */

static int  (*real_init_ptr) P((ENV_INFO *, HOST_CONFIG * )) ;
static int  (*real_config_stack_ptr) P((ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG * )) ;
static int  (*real_get_stack_config_ptr) P((ENV_INFO *, CM_NMA_INFO * )) ;
static int  (*real_init_network_ptr) P((int )) ;
static int  (*real_initiate_comm_ptr) P((NETWORK_HANDLE, int, CREF *, CREF *, CREF *)) ;
static int  (*real_comm_get_ptr) P((CREF, OBJECT_INDEX, OBJECT **)) ;
static int  (*real_comm_put_ptr) P((CREF, OBJECT_INDEX, OBJECT *)) ;
static int  (*real_comm_read_ptr) P((ENV_INFO *, UINT16, OBJECT_INDEX, SUBINDEX, UINT8 **, OBJECT **,
            int *)) ;
static int  (*real_comm_write_ptr) P((ENV_INFO *, UINT16, OBJECT_INDEX, SUBINDEX, UINT8 *, unsigned short)) ;
static int  (*real_comm_upload_ptr) P((CREF, OBJECT_INDEX, UINT8 *)) ;
static int  (*real_find_block_ptr) P((char *, NETWORK_HANDLE, FIND_CNF_A **)) ;
static int  (*real_param_read_ptr) P((ENV_INFO *, int, SUBINDEX)) ;
static int  (*real_param_write_ptr) P((ENV_INFO *, int, SUBINDEX)) ;
static int  (*real_param_unload_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
static int  (*real_param_load_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
static int  (*real_scan_network) P((ENV_INFO*,  SCAN_DEV_TBL *));
static int  (*real_device_table) P((ENV_INFO *, NETWORK_HANDLE));
static int  (*real_detach_device) P((ENV_INFO *, NETWORK_HANDLE));
static int  (*real_get_actual_index_ptr) P((unsigned short *, int *, unsigned short *));
static int  (*real_read_absolute_index_ptr) P((ENV_INFO *, unsigned short, int,
		unsigned short, void *));
static int  (*real_abort_communication) P((ENV_INFO *, unsigned short, unsigned short));

/*
 *  System Management pointers
 */

static int  (*real_sm_identify) P((ENV_INFO* ,unsigned short ,
            CR_SM_IDENTIFY_CNF* )) ;
static int (*real_sm_clr_node) P((ENV_INFO* ,unsigned short,
                                 CR_SM_IDENTIFY_CNF* )) ;
static int (*real_sm_set_node) P((ENV_INFO* ,unsigned short,
                                 char* )) ;
static int (*real_sm_clr_tag) P((ENV_INFO* ,unsigned short,
                                 CR_SM_IDENTIFY_CNF* )) ;
static int (*real_sm_assign_tag) P((ENV_INFO* ,unsigned short,
                                 CR_SM_IDENTIFY_CNF* )) ;

/*
 *  Network Management functional pointers
 */
static int (*real_nm_wr_local_vcr) P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
static int (*real_nm_fms_initiate) P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
static int (*real_nm_get_od) P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
static int (*real_nm_read) P((ENV_INFO* , NETWORK_HANDLE, USIGN16, int, int )) ;
static int (*real_nm_dev_boot) P((ENV_INFO* , NETWORK_HANDLE, USIGN16, int )) ;
static int (*real_cm_exit) P((ENV_INFO* )) ;
static int (*real_firmware_download) P((ENV_INFO*, char * )) ;
static int (*real_get_host_info) P((ENV_INFO*, T_FBAPI_DEVICE_INFO *)) ;
static int (*real_reset_fbk) P((ENV_INFO*, NETWORK_HANDLE, HOST_CONFIG * )) ;
static int (*real_set_appl_state) P((ENV_INFO *, int *, int));


/**********************************************************************
 *
 *  Name: free_value_list
 *  ShortDesc: Free a value list and all of its allocated contents
 *
 *  Include:
 *      cm_lib.h
 *
 *  Description:
 *
 *      free_value_list frees all of the memory allocated for a
 *      COMM_VALUE_LIST.  Note that the COMM_VALUE_LIST structure
 *      itself is not freed.
 *
 *  Inputs:
 *      cm_value_listp = The value list to free
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      None.
 *
 *  Author:
 *      Bruce Davis
 *
 *********************************************************************/

void
free_value_list (CM_VALUE_LIST *cm_value_listp)
{

    CM_VALUE    *cm_valuep, *cm_value_endp;

    /*
     *  Step through the list, freeing all of the strings.
     */

    cm_valuep = cm_value_listp->cmvl_list ;
    cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;

    for ( ; cm_valuep < cm_value_endp; cm_valuep++) {

        switch (cm_valuep->cm_type) {
            case 0xff:
            case DT_VISIBLE_STRING:
            case DT_OCTET_STRING:
            case DT_BIT_STRING:
            case DT_ISO_10646_STRING:
                free (cm_valuep->cm_val.cm_stream);
                break;
            default:
                break;
        }
    }

    /*
     *  Free the list.
     */

    free ((char *)cm_value_listp->cmvl_list);
    cm_value_listp->cmvl_list = NULL;
    cm_value_listp->cmvl_count = 0;
}


/**********************************************************************
 *
 *  Name: type_to_ddl_type
 *  ShortDesc:  Convert the Value structure types to DDL types.
 *
 *  Description:
 *
 *  Inputs:
 *
 *      value_listp - The data returned from the device that
 *                    contains data types.
 *
 *  Outputs:
 *
 *      cm_value_listp - Pointer to the structure containing DDL types.
 *
 *  Returns:
 *
 *      Zero for success and non-zero for failure.
 *
 *  Author:
 *      Bruce Davis
 *      Kent Anderson
 *
 *********************************************************************/

int
type_to_ddl_type (CM_VALUE_LIST *cm_value_listp, COMM_VALUE_LIST *comm_value_listp)
{

    COMM_VALUE          *comm_valuep ;
    CM_VALUE            *cm_valuep ;
    CM_VALUE            *cm_value_endp ;

    /*
     *  Step through the value list, updating the DDL value list.
     */

    comm_value_listp->cmvl_list = (COMM_VALUE *)calloc(
            (unsigned)cm_value_listp->cmvl_count, sizeof(COMM_VALUE)) ;
    comm_value_listp->cmvl_count = cm_value_listp->cmvl_count ;
    comm_value_listp->cmvl_limit = cm_value_listp->cmvl_count ;
    comm_valuep = comm_value_listp->cmvl_list;

    cm_valuep = cm_value_listp->cmvl_list;
    cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;

    for ( ; cm_valuep < cm_value_endp; cm_valuep++, comm_valuep++) {

        /*
         *  Transfer the value.
         */

        comm_valuep->cm_size = cm_valuep->cm_size;

        switch (cm_valuep->cm_type) {
            case DT_BOOLEAN:
                comm_valuep->cm_type = DDS_BOOLEAN_T ;
                comm_valuep->cm_val.cm_boolean = cm_valuep->cm_val.cm_boolean;
                break;

            case DT_INTEGER8:
            case DT_INTEGER16:
            case DT_INTEGER32:
                comm_valuep->cm_type = DDS_INTEGER ;
                comm_valuep->cm_val.cm_long = cm_valuep->cm_val.cm_long;
                break;

            case DT_UNSIGNED8:
            case DT_UNSIGNED16:
            case DT_UNSIGNED32:
                comm_valuep->cm_type = DDS_UNSIGNED;
                comm_valuep->cm_val.cm_ulong = cm_valuep->cm_val.cm_ulong;
                break;

            case DT_FLOATING_POINT:
                comm_valuep->cm_type = DDS_FLOAT ;
                comm_valuep->cm_val.cm_float = cm_valuep->cm_val.cm_float;
                break;

            case DT_DOUBLE:
                comm_valuep->cm_type = DDS_DOUBLE ;
                comm_valuep->cm_val.cm_double = cm_valuep->cm_val.cm_double;
                break;

            case DT_VISIBLE_STRING:
                comm_valuep->cm_type = DDS_VISIBLESTRING ;
                comm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (comm_valuep->cm_val.cm_stream, 
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DT_BIT_STRING:
                comm_valuep->cm_type = DDS_BITSTRING ;
                comm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (comm_valuep->cm_val.cm_stream, 
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DT_OCTET_STRING:
                comm_valuep->cm_type = DDS_OCTETSTRING ;
                comm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (comm_valuep->cm_val.cm_stream, 
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DT_ISO_10646_STRING:
                comm_valuep->cm_type = DDS_EUC ;
                comm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (comm_valuep->cm_val.cm_stream, 
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DT_TIME_OF_DAY:
                comm_valuep->cm_type = DDS_TIME;
                (void)memcpy (comm_valuep->cm_val.cm_ca, 
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;

            case DT_TIME_VALUE:
                comm_valuep->cm_type = DDS_TIME_VALUE;
                (void)memcpy (comm_valuep->cm_val.cm_time_value, 
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;

            case DT_DATE:
                comm_valuep->cm_type = DDS_DATE_AND_TIME;
                (void)memcpy (comm_valuep->cm_val.cm_ca, 
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;

            case DT_TIME_DIFFERENCE:
                comm_valuep->cm_type = DDS_DURATION;
                (void)memcpy (comm_valuep->cm_val.cm_ca, 
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;

            default:
                CRASH_BUT_RET_CODE (CM_INTERNAL_ERROR);
        }
    }

    return (CM_SUCCESS);
}


/**********************************************************************
 *
 *  Name: type_from_ddl_type
 *  ShortDesc: Translate from DDL Variable Types to Variable Types
 *
 *  Include:
 *      dds_vtab.h
 *
 *  Description:
 *
 *  Inputs:
 *
 *      cm_value_listp - Pointer to the value list containing DDL data
 *                       types.
 *
 *  Outputs:
 *
 *      value_listp - Pointer to the value list containing 
 *                        data types.
 *
 *  Returns:
 *
 *      Zero for failure and non-zero for success.
 *
 *  Author:
 *      Bruce Davis
 *      Kent Anderson
 *
 *********************************************************************/

int
type_from_ddl_type (COMM_VALUE_LIST *comm_value_listp, CM_VALUE_LIST *cm_value_listp)
{

    COMM_VALUE          *comm_valuep ;
    COMM_VALUE          *comm_value_endp ;
    CM_VALUE            *cm_valuep ;

    /*
     *  Step through the DDL value list, updating the value list.
     */

    cm_value_listp->cmvl_list = (CM_VALUE *)calloc(
            (unsigned)comm_value_listp->cmvl_count, sizeof(CM_VALUE)) ;
    cm_value_listp->cmvl_count = comm_value_listp->cmvl_count ;
    cm_valuep = cm_value_listp->cmvl_list;

    comm_valuep = comm_value_listp->cmvl_list;
    comm_value_endp = comm_valuep + comm_value_listp->cmvl_count;

    for ( ; comm_valuep < comm_value_endp; comm_valuep++, cm_valuep++ ) {

        switch (comm_valuep->cm_type) {
            case DDS_BOOLEAN_T:
                cm_valuep->cm_val.cm_boolean = comm_valuep->cm_val.cm_boolean;
                cm_valuep->cm_type = DT_BOOLEAN;
                cm_valuep->cm_size = DT_BOOLEAN_SIZE;
                break;

            case DDS_INTEGER:
                if (comm_valuep->cm_size == DT_INTEGER32_SIZE) {
                    cm_valuep->cm_type = DT_INTEGER32;
                    cm_valuep->cm_size = DT_INTEGER32_SIZE;
                } else if (comm_valuep->cm_size == DT_INTEGER16_SIZE) {
                    cm_valuep->cm_type = DT_INTEGER16;
                    cm_valuep->cm_size = DT_INTEGER16_SIZE;
                } else {
                    cm_valuep->cm_type = DT_INTEGER8;
                    cm_valuep->cm_size = DT_INTEGER8_SIZE;
                }
                cm_valuep->cm_val.cm_long = comm_valuep->cm_val.cm_long;
                break;

            case DDS_UNSIGNED:
            case DDS_ENUMERATED:
            case DDS_BIT_ENUMERATED:
            case DDS_INDEX:
                if (comm_valuep->cm_size == DT_UNSIGNED32_SIZE) {
                    cm_valuep->cm_type = DT_UNSIGNED32;
                    cm_valuep->cm_size = DT_UNSIGNED32_SIZE;
                } else if (comm_valuep->cm_size == DT_UNSIGNED16_SIZE) {
                    cm_valuep->cm_type = DT_UNSIGNED16;
                    cm_valuep->cm_size = DT_UNSIGNED16_SIZE;
                } else {
                    cm_valuep->cm_type = DT_UNSIGNED8;
                    cm_valuep->cm_size = DT_UNSIGNED8_SIZE;
                }
                cm_valuep->cm_val.cm_ulong = comm_valuep->cm_val.cm_ulong;
                break;

            case DDS_FLOAT:
                cm_valuep->cm_type = DT_FLOATING_POINT;
                cm_valuep->cm_size = DT_FLOATING_POINT_SIZE;
                cm_valuep->cm_val.cm_float = comm_valuep->cm_val.cm_float;
                break;
        
            case DDS_DOUBLE:
                cm_valuep->cm_type = DT_DOUBLE;
                cm_valuep->cm_size = DT_DOUBLE_SIZE;
                cm_valuep->cm_val.cm_double = comm_valuep->cm_val.cm_double;
                break;

            case DDS_VISIBLESTRING:
                cm_valuep->cm_type = DT_VISIBLE_STRING;
                cm_valuep->cm_size = comm_valuep->cm_size;
                cm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (cm_valuep->cm_val.cm_stream, 
                        comm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DDS_BITSTRING:
                cm_valuep->cm_type = DT_BIT_STRING;
                cm_valuep->cm_size = comm_valuep->cm_size;
                cm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (cm_valuep->cm_val.cm_stream, 
                        comm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DDS_ASCII:
            case DDS_PASSWORD:
            case DDS_PACKED_ASCII:
            case DDS_OCTETSTRING:
                cm_valuep->cm_type = DT_OCTET_STRING;
                cm_valuep->cm_size = comm_valuep->cm_size;
                cm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (cm_valuep->cm_val.cm_stream, 
                        comm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;

            case DDS_TIME:
                cm_valuep->cm_type = DT_TIME_OF_DAY;
                cm_valuep->cm_size = comm_valuep->cm_size;
                (void)memcpy (cm_valuep->cm_val.cm_ca, 
                        comm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;

            case DDS_TIME_VALUE:
                cm_valuep->cm_type = DT_TIME_VALUE;
                cm_valuep->cm_size = comm_valuep->cm_size;
                (void)memcpy (cm_valuep->cm_val.cm_time_value, 
                        comm_valuep->cm_val.cm_time_value, (int)cm_valuep->cm_size);
                break;

            case DDS_DATE_AND_TIME:
            case DDS_DATE:
                cm_valuep->cm_type = DT_DATE;
                cm_valuep->cm_size = comm_valuep->cm_size;
                (void)memcpy (cm_valuep->cm_val.cm_ca, 
                        comm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;
    
            case DDS_DURATION:
                cm_valuep->cm_type = DT_TIME_DIFFERENCE;
                cm_valuep->cm_size = comm_valuep->cm_size;
                (void)memcpy (cm_valuep->cm_val.cm_ca, 
                        comm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                break;
    
            case DDS_EUC:
                cm_valuep->cm_type = DT_ISO_10646_STRING;
                cm_valuep->cm_size = comm_valuep->cm_size;
                cm_valuep->cm_val.cm_stream = malloc(cm_valuep->cm_size) ;
                (void)memcpy (cm_valuep->cm_val.cm_stream, 
                        comm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                break;
    
            default:
            	CRASH_BUT_RET_CODE (CM_INTERNAL_ERROR);
                /*NOTREACHED*/
                break;
        }
    }
    return (CM_SUCCESS);
}


/**********************************************************************
 *
 *  Name: pack_value_list
 *  ShortDesc: Create a bytestream to send to a device
 *
 *  Include:
 *      cm_lib.h
 *
 *  Description:
 *
 *      pack_value_list takes the data in the CM_VALUE_LIST
 *      (which is a network independent format), and packs it to
 *      send to a network device.  The bytestream is packed
 *      into the format required for communication given the
 *      data types and sizes specified in the value list.
 *
 *  Inputs:
 *      cm_value_listp - List containing the size, type, and data to
 *              be send to the device.
 *
 *  Outputs:
 *      valuesp - The data bytestream that is created and packed.
 *
 *  Returns:
 *      CM_NO_MEMORY, CM_BAD_OBJECT, CM_SUCCESS,
 *      and error returns from other Connection Manager functions
 *
 *  Author:
 *      Bruce Davis
 *
 *********************************************************************/

static int
pack_value_list (CM_VALUE_LIST *cm_value_listp, UINT8 **valuesp)
{

    CM_VALUE                *cm_valuep, *cm_value_endp;
    UINT8                   *values;
    unsigned int            size_needed;
    unsigned long           data_ulong;
    UINT8                   *datap;

    /*
     *  Calculate the total size of the data bytestream, and malloc
     *  the memory.
     */

    size_needed = 0;
    cm_valuep = cm_value_listp->cmvl_list;
    cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;
    do {
        size_needed += cm_valuep++->cm_size;
    } while (cm_valuep < cm_value_endp) ;
    values = (UINT8 *)malloc (size_needed);
    if (values == NULL) {
        return (CM_NO_MEMORY);
    }
    *valuesp = values;

    /*
     *  Now step through the value list, packing the bytestream from
     *  the values.
     */

    cm_valuep = cm_value_listp->cmvl_list;
    for ( ; cm_valuep < cm_value_endp; cm_valuep++) {
        switch (cm_valuep->cm_type) {
            case DT_BOOLEAN:
            case DT_INTEGER8:
            case DT_INTEGER16:
            case DT_INTEGER32:
                data_ulong = (unsigned long)cm_valuep->cm_val.cm_long;
                goto packit;
            case DT_UNSIGNED8:
            case DT_UNSIGNED16:
            case DT_UNSIGNED32:
                data_ulong = cm_valuep->cm_val.cm_ulong;
                goto packit;
            case DT_FLOATING_POINT:
                ASSERT_DBG (cm_valuep->cm_size == 4);
                data_ulong = *(unsigned long *)(void *)&cm_valuep->cm_val.cm_float;
packit:
                if (cm_valuep->cm_size == 4) {
                    values[3] = (UINT8)(data_ulong & 0xFF);
                    data_ulong >>= 8;
                    values[2] = (UINT8)(data_ulong & 0xFF);
                    data_ulong >>= 8;
                }
                if (cm_valuep->cm_size > 1) {
                    values[1] = (UINT8)(data_ulong & 0xFF);
                    data_ulong >>= 8;
                }
                values[0] = (UINT8)(data_ulong & 0xFF);
                values += cm_valuep->cm_size;
                break;

            case DT_DOUBLE:
                ASSERT_DBG (cm_valuep->cm_size == 8);
#ifdef DDS_BIG_ENDIAN
                datap = (UINT8 *)&cm_valuep->cm_val.cm_double;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
                *values++ = *datap++;
#endif
#ifdef DDS_LITTLE_ENDIAN
                datap = (UINT8 *)&cm_valuep->cm_val.cm_double + 
                        sizeof(cm_valuep->cm_val.cm_double);
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
                *values++ = *--datap;
#endif
                break;

            case DT_VISIBLE_STRING:
            case DT_OCTET_STRING:
            case DT_BIT_STRING:
            case DT_ISO_10646_STRING:
                (void)memcpy ((char *)values,
                        cm_valuep->cm_val.cm_stream, (int)cm_valuep->cm_size);
                values += cm_valuep->cm_size;
                break;

            case DT_TIME_OF_DAY:
            case DT_DATE:
            case DT_TIME_VALUE:
            case DT_TIME_DIFFERENCE:
                (void)memcpy ((char *)values,
                        cm_valuep->cm_val.cm_ca, (int)cm_valuep->cm_size);
                values += cm_valuep->cm_size;
                break;

            default:
                CRASH_DBG();
                /*NOTREACHED*/
                free ((char *)*valuesp);
                return (CM_BAD_OBJECT);
        }
    }

    return (CM_SUCCESS);
}


/***********************************************************************
 *
 *  Name:  object_free
 *
 *  ShortDesc:  Free an object and all of its allocated contents from
 *              system memory.
 *
 *  Description:
 *      The object_free function takes an object in system memory and
 *      frees it along with all of its allocated contents.
 *
 *  Inputs:
 *      object - the object to be freed.
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      Void.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

static void
object_free(OBJECT *object)
{

    if (object->name) {
        free((char *)object->name);
    }
    if (object->extension) {
        free((char *)object->extension);
    }
    if (object->value) {
        free((char *)object->value);
    }
    if (object->specific) {

        switch (object->code) {

            case OC_NULL:
                break;

            case OC_OD_DESCRIPTION:
                break;

            case OC_DOMAIN:
                break;

            case OC_PROGRAM_INVOCATION:
                if (object->specific->program_invocation.domain_list) {
                    free((char *)object->specific->
                            program_invocation.domain_list);
                }
                break;

            case OC_EVENT:
                break;

            case OC_DATA_TYPE:
                if (object->specific->data_type.symbol) {
                    free((char *)object->specific-> data_type.symbol);
                }
                break;

            case OC_DATA_TYPE_STRUCTURE:
                if (object->specific->data_type_structure.type_list) {
                    free((char *)object->specific->
                            data_type_structure.type_list);
                }
                if (object->specific->data_type_structure.size_list) {
                    free((char *)object->specific->
                            data_type_structure.size_list);
                }
                break;

            case OC_SIMPLE_VARIABLE:
                break;

            case OC_ARRAY:
                break;

            case OC_RECORD:
                break;

            case OC_VARIABLE_LIST:
                if (object->specific->variable_list.list) {
                    free((char *)object->specific-> variable_list.list);
                }
                break;

            default:
                break;
        }

        free((char *)object->specific);
    }

    free((char *)object);
}


/**********************************************************************
 *
 *  Name: unpack_values
 *  ShortDesc: Translate device values from a network
 *
 *  Include:
 *      cm_lib.h
 *
 *  Description:
 *
 *      unpack_values takes a data bytestream from a
 *      network device and unpacks it into a CM_VALUE_LIST, which
 *      is a network independent format.  The bytestream is parsed
 *      into the appropriate data types as defined by the types and
 *      sizes in the CM_VALUE_LIST.
 *
 *  Inputs:
 *      cm_value_listp - List containing the size and type of the data
 *              elements.  It is also where the unpacked data is put.
 *      values - The data bytestream
 *      value_length - The length of the data bytestream.
 *
 *  Outputs:
 *      cm_value_listp - List containing the size and type of the data
 *              elements.  It is also where the unpacked data is put.
 *
 *  Returns:
 *      CM_NO_MEMORY, CM_BAD_OBJECT, CM_SUCCESS,
 *      and error returns from other Connection Manager functions
 *
 *  Author:
 *      Bruce Davis
 *
 *********************************************************************/

static int
unpack_values (CM_VALUE_LIST *cm_value_listp, UINT8 *values, int value_length)
{

    CM_VALUE                *cm_valuep, *cm_value_endp;
    UINT8                   *values_endp;
    unsigned char           *destp;
    unsigned long           *ldestp;

    /*
     *  Step the the value list, unpacking the values.
     */

    values_endp = values + value_length;
    cm_valuep = cm_value_listp->cmvl_list;
    cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;
    for ( ; cm_valuep < cm_value_endp; cm_valuep++) {
        switch (cm_valuep->cm_type) {
            case DT_BOOLEAN:
            case DT_INTEGER8:
            case DT_INTEGER16:
            case DT_INTEGER32:
                ldestp = &cm_valuep->cm_val.cm_ulong;
                if ((*values & 0x80) != 0) {
                    *ldestp = 0xffffff00 | *values++;
                } else {
                    *ldestp = *values++;
                }
                if (cm_valuep->cm_size > 1) {
                    *ldestp <<= 8;
                    *ldestp |= *values++;
                    if (cm_valuep->cm_size > 2) {
                        ASSERT_DBG (cm_valuep->cm_size == 4);
                        *ldestp <<= 8;
                        *ldestp |= *values++;
                        *ldestp <<= 8;
                        *ldestp |= *values++;
                    }
                }
                break;

            case DT_UNSIGNED8:
            case DT_UNSIGNED16:
            case DT_UNSIGNED32:
                ldestp = &cm_valuep->cm_val.cm_ulong;
                *ldestp = *values++;
                if (cm_valuep->cm_size > 1) {
                    *ldestp <<= 8;
                    *ldestp |= *values++;
                    if (cm_valuep->cm_size > 2) {
                        ASSERT_DBG (cm_valuep->cm_size == 4);
                        *ldestp <<= 8;
                        *ldestp |= *values++;
                        *ldestp <<= 8;
                        *ldestp |= *values++;
                    }
                }
                break;

            case DT_FLOATING_POINT:
                ASSERT_DBG (cm_valuep->cm_size == 4);
                ldestp = &cm_valuep->cm_val.cm_ulong;
                *ldestp = *values++;
                *ldestp <<= 8;
                *ldestp |= *values++;
                *ldestp <<= 8;
                *ldestp |= *values++;
                *ldestp <<= 8;
                *ldestp |= *values++;
                break;

            case DT_DOUBLE:
                ASSERT_DBG (cm_valuep->cm_size == 8);
#ifdef DDS_BIG_ENDIAN
                destp = (unsigned char *)&cm_valuep->cm_val.cm_double;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
                *destp++ = *values++;
#endif
#ifdef DDS_LITTLE_ENDIAN
                destp = (unsigned char *)&cm_valuep->cm_val.cm_double + 
                        sizeof(cm_valuep->cm_val.cm_double);
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
                *--destp = *values++;
#endif
                break;

            case DT_VISIBLE_STRING:
            case DT_OCTET_STRING:
            case DT_BIT_STRING:
            case DT_ISO_10646_STRING:
                cm_valuep->cm_val.cm_stream = malloc (cm_valuep->cm_size);
                if ((cm_valuep->cm_val.cm_stream == NULL))
                {
                    return (CM_NO_MEMORY);
                }
                if (value_length <= 0)
                {
                	return (CM_NO_VALUE);
                }

                (void)memcpy (cm_valuep->cm_val.cm_stream,
                        (char *)values, (int)cm_valuep->cm_size);
                values += cm_valuep->cm_size;
                break;

            case DT_TIME_OF_DAY:
            case DT_DATE:
            case DT_TIME_VALUE:
            case DT_TIME_DIFFERENCE:
                (void)memcpy (cm_valuep->cm_val.cm_ca,
                        (char *)values, (int)cm_valuep->cm_size);
                values += cm_valuep->cm_size;
                break;

            default:
            	CRASH_BUT_RET_CODE (CM_BAD_OBJECT);
        }
    }

    ASSERT_RET (values == values_endp, CM_BAD_OBJECT);

    return (CM_SUCCESS);
}


/**********************************************************************
 *
 *  Name: unpack_value_list
 *  ShortDesc: Create and fill a value list from device values
 *
 *  Include:
 *      cm_lib.h
 *
 *  Description:
 *
 *      unpack_value_list takes an OBJECT pointer which defines
 *      the format of the device data, and creates a CM_VALUE_LIST
 *      structure to hold the unpacked data.  It then initializes the
 *      value list with the appropriate type and size for each element.
 *      The device data is then unpacked into the value list by calling
 *      unpack_values.
 *
 *  Inputs:
 *      values - The data bytestream
 *      type_objectp - The object that defines the format of the 
 *              data bytestream.
 *      sub_index - The subindex for referencing into an array or
 *                  record.
 *      value_length - The length of the data bytestream.
 *
 *  Outputs:
 *      cm_value_listp - List header for the value list that is created
 *              and filled in.
 *
 *  Returns:
 *      CM_NO_MEMORY, CM_SUCCESS, CM_BAD_OBJECT,
 *      and error returns from other Connection Manager functions
 *
 *  Author:
 *      Bruce Davis
 *
 *********************************************************************/

static int
unpack_value_list (CM_VALUE_LIST *cm_value_listp, UINT8 *values, OBJECT *type_objectp,
        SUBINDEX sub_index, int value_length)
{

    CM_VALUE            *cm_valuep, *cm_value_endp;
    unsigned int        needed_size;
    int                 rs;
    OBJECT_INDEX        array_type, *typep;
    UINT8               array_size, *sizep;

    /*
     *  Create an appropriate COMM_VALUE_LISI, depending on whether the
     *  object is a domain, simple variable, array, or record.
     */

    if (NULL == type_objectp)
    {
        return (CM_OBJECT_NOT_IN_HEAP);
    }

    switch (type_objectp->code) {
        case OC_DOMAIN:

            /*
             *  The object is a DOMAIN.  The list consists of one element
             *  which points to the domain's value.
             */

            cm_value_listp->cmvl_list = (CM_VALUE *)calloc (1,
                        sizeof (*cm_value_listp->cmvl_list));
            if (cm_value_listp->cmvl_list == NULL) {
                return (CM_NO_MEMORY);
            }
            cm_value_listp->cmvl_count = 1;
            cm_valuep = cm_value_listp->cmvl_list;
            cm_valuep->cm_val.cm_stream = malloc (value_length);
            if (cm_valuep->cm_val.cm_stream == NULL) {
                free ((char *)cm_valuep);
                cm_value_listp->cmvl_list = NULL;
                cm_value_listp->cmvl_count = 0;
                return (CM_NO_MEMORY);
            }
            (void)memcpy (cm_valuep->cm_val.cm_stream, (char *)values,
                    value_length);
            return (CM_SUCCESS);

        case OC_SIMPLE_VARIABLE:

            /*
             *  The object is a SIMPLE VARIABLE.  The list consists of
             *  one element, which contains the variable's value.
             */

            cm_value_listp->cmvl_list = (CM_VALUE *)calloc (1,
                        sizeof (*cm_value_listp->cmvl_list));
            if (cm_value_listp->cmvl_list == NULL) {
                return (CM_NO_MEMORY);
            }
            cm_value_listp->cmvl_count = 1;
            cm_valuep = cm_value_listp->cmvl_list;
            cm_valuep->cm_type =
                    type_objectp->specific->simple_variable.type;
            cm_valuep->cm_size =
                    type_objectp->specific->simple_variable.size;
            break;

        case OC_ARRAY:

            /*
             *  The object is an ARRAY.  If the sub_index is non-zero, there
             *  is only one element of the array.  If the sub_index is zero,
             *  then the whole array is being accessed.  Create a value
             *  list of appropriate size.
             */

            needed_size = (sub_index == 0) ? 
                    type_objectp->specific->array.elem_count : 1;
            cm_value_listp->cmvl_list = (CM_VALUE *)calloc (
                        needed_size, sizeof (*cm_value_listp->cmvl_list));
            if (cm_value_listp->cmvl_list == NULL) {
                return (CM_NO_MEMORY);
            }
            cm_value_listp->cmvl_count = (unsigned short)needed_size;
            cm_valuep = cm_value_listp->cmvl_list;
            cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;
            array_type = type_objectp->specific->array.type;
            array_size = type_objectp->specific->array.size;
            for ( ; cm_valuep < cm_value_endp; cm_valuep++) {
                cm_valuep->cm_type = array_type;
                cm_valuep->cm_size = array_size;
            }
            break;
            
        case OC_DATA_TYPE_STRUCTURE:

            /*
             *  The object is a RECORD (this object is the data type
             *  structure definition object defining the record).  If the
             *  sub_index is non-zero, there is only one member of the
             *  record.  If the sub_index is zero, then the whole record
             *  is being accessed.  Create a value list of appropriate size.
             */

            needed_size = (sub_index == 0) ? 
                type_objectp->specific->data_type_structure.elem_count : 1;
            cm_value_listp->cmvl_list = (CM_VALUE *)calloc (
                        needed_size, sizeof (*cm_value_listp->cmvl_list));
            if (cm_value_listp->cmvl_list == NULL) {
                return (CM_NO_MEMORY);
            }
            cm_value_listp->cmvl_count = (unsigned short)needed_size;
            cm_valuep = cm_value_listp->cmvl_list;
            cm_value_endp = cm_valuep + cm_value_listp->cmvl_count;
            typep = type_objectp->specific->data_type_structure.type_list;
            sizep = type_objectp->specific->data_type_structure.size_list;
            if (sub_index) {
                typep += --sub_index;
                sizep += sub_index;
            }
            for ( ; cm_valuep < cm_value_endp; cm_valuep++) {
                cm_valuep->cm_type = *typep++;
                cm_valuep->cm_size = *sizep++;
            }
            break;
            
        default:
            return (CM_BAD_OBJECT);
    }

    /*
     *  Unpack the bytestream into the CM_VALUE_LIST structure.
     */

    rs = unpack_values (cm_value_listp, values, value_length);
    if (rs != CM_SUCCESS) {
        free_value_list (cm_value_listp);
    }
    return (rs);
}


/***********************************************************************
 *
 *  Name:  get_cref
 *
 *  ShortDesc:   Get the cref for the device handle and comm type.
 *
 *  Description:
 *      The get_cref function takes a device handle and communication
 *      type and gets the corresponding communication reference.
 *
 *  Inputs:
 *      device_handle - the handle of the device.
 *      comm_type - the particular OD (OPOD, DDOD, or MIB).
 *
 *  Outputs:
 *      cref - the communication reference.
 *
 *  Returns:
 *      CM_SUCCESS.
 *      CM_BAD_COMM_TYPE.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *
 **********************************************************************/

static int
get_cref(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, CREF *cref)
{

    int         r_code = CM_SUCCESS ;
    /*
     *  Check which type of OD (communication type), and set the
     *  corresponding CREF (communication reference).
     */

    switch (comm_type) {

        case CT_OPOD:

            r_code = get_adt_a_opod_cref(device_handle, cref);
            break;

        case CT_DDOD:

            r_code = get_adt_a_ddod_cref(device_handle, cref);
            break;

        case CT_MIB:

            r_code = get_adt_a_mib_cref(device_handle, cref);
            break;

        default:
            return(CM_BAD_COMM_TYPE);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  comm_read
 *
 *  ShortDesc:  Read an object's value from a device.
 *
 *  Description:
 *      The comm_read function read a value from a device
 *      using a device_handle, object_index, and subindex.
 *
 *  Inputs:
 *      device_handle - unique identifier of the device to 
 *                      receive the read.
 *      comm_type -     the communication type of the read
 *                      request--normally for the operational
 *                      part.
 *      object_index -  the index of the value to read from the
 *                      device.
 *      sub_index -     the subindex of the value to read to
 *                      the device.
 *
 *  Outputs:
 *      cm_value_listp - The requested value(s).
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/
int 
comm_read (DEVICE_HANDLE device_handle, COMM_TYPE comm_type, OBJECT_INDEX object_index,
    SUBINDEX sub_index, CM_VALUE_LIST *cm_value_listp, ENV_INFO *env_info)
{

    int         r_code ;
    UINT8       *value = NULL;
    CREF        cref ;
    OBJECT      *type_object = NULL;
    int         value_size ;
    NET_TYPE    net_type ;
    NETWORK_HANDLE  network_handle;
    UINT16      station_address;
    UINT16      cr;
    UINT32      sim_response_code;
    APP_INFO    *app_info;

    app_info = env_info->app_info;
    app_info->env_returns.comm_err  = 0 ;
    app_info->env_returns.response_code  = 0 ;

    /*
     *  Read an object's value from a device on a Network.
     *
     *  Get the proper CREF, call the simulator to read
     *  the value, and unpack the byte stream.
     */

    r_code = get_cref(device_handle,comm_type,&cref);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    r_code = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {

    	case NT_OFF_LINE:

            ASSERT_RET(sim_comm_read_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_comm_read_ptr)(cref, object_index, sub_index,
                    &value, &type_object, &value_size);
            break;

        case NT_A_SIM:
        
            /*
             *  Get the Network Handle and Station Address.
             */

            r_code = get_adt_network(device_handle, &network_handle);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }
            r_code = get_adt_station_address(device_handle, &station_address);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }

            /*
             * Check to see if simulated comm_errors or device
             * response codes have been set.
             */

            r_code = sim_comm_get_sim_response_code(RCSIM_TYPE_COMM_ERR,
                    network_handle, NT_A_SIM, station_address, &sim_response_code);
            if (r_code != 0) {

                /* check if simulated response provides good response */

                if (sim_response_code != 0) {

                    /* simulate error */

                    app_info->env_returns.comm_err  = sim_response_code ;
                    return (CM_COMM_ERR);
                }
            }
        
            r_code = sim_comm_get_sim_response_code(RCSIM_TYPE_RESPONSE_CODE,
                    network_handle, NT_A_SIM, station_address, &sim_response_code);
            if (r_code != 0) {

                /* check if simulated response provides good response */

                if (sim_response_code != 0) {

                    /* simulate error */

                    app_info->env_returns.response_code  = sim_response_code ;
                    return (CM_RESPONSE_CODE);
                }
            }

            /*
             * Issue the simulated Comm Read
             */

            ASSERT_RET(sim_comm_read_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_comm_read_ptr)(cref, object_index, sub_index,
                    &value, &type_object, &value_size);
            break ;

        case NT_A:

            /*
             *  Get the Network Handle and Station Address.
             */

            r_code = get_adt_network(device_handle, &network_handle);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }
            r_code = get_adt_station_address(device_handle, &station_address);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }

            r_code = get_nt_net_fbap_cr(network_handle, &cr);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }

            /*
             * Issue the Real Comm Read
             */

            ASSERT_RET(real_comm_read_ptr != NULL, CM_NO_COMM) ;
            r_code = (*real_comm_read_ptr)(env_info, cr, object_index, sub_index,
                    &value, &type_object, &value_size);
            break ;

        default:
            return(CM_NO_COMM) ;
    }

    if (r_code) {
        return(r_code);
    }

    r_code = unpack_value_list(cm_value_listp, value, type_object,
            sub_index, value_size);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    free((char *)value);
    object_free(type_object);
    return(CM_SUCCESS) ;
}


/***********************************************************************
 *
 *  Name:  comm_write
 *
 *  ShortDesc:  Write an object's value to a device.
 *
 *  Description:
 *      The comm_write function writes a value to a device
 *      using a device_handle, object_index, and subindex.
 *
 *  Inputs:
 *      device_handle - unique identifier of the device to 
 *                      receive the write.
 *      comm_type -     the communication type of the write
 *                      request--normally for the operational
 *                      part.
 *      object_index -  the index of the value to write to the
 *                      device.
 *      sub_index -     the subindex of the value to write to
 *                      the device.
 *
 *  Outputs:
 *      cm_value_listp - The requested value(s).
 *      env_info - Returns any communication errors or response codes.
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/
int 
comm_write(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, OBJECT_INDEX object_index,
    SUBINDEX sub_index, CM_VALUE_LIST *cm_value_listp, ENV_INFO *env_info)
{

    CREF        cref;
    UINT8       *value;
    unsigned short      size;
    int         r_code;
    NET_TYPE    net_type ;
    NETWORK_HANDLE  network_handle;
    UINT16      station_address;
    UINT32      sim_response_code;
    APP_INFO    *app_info;
    USIGN8     cr; 

    app_info = env_info->app_info;

    app_info->env_returns.comm_err  = 0 ;
    app_info->env_returns.response_code  = 0 ;

    /*
     *  Write an object's value from a device on a Simulated Network.
     *
     *  Get the proper CREF, call the simulator to read the
     *  value, and unpack the byte stream.
     */

    r_code = get_cref(device_handle, comm_type, &cref);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }
    r_code = pack_value_list(cm_value_listp, &value);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }
    r_code = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A_SIM:

            /*
             *  Get the Network Handle and Station Address.
             */

            r_code = get_adt_network(device_handle, &network_handle);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }
            r_code = get_adt_station_address(device_handle, &station_address);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }

            /*
             * Check to see if simulated comm_errors or device
             * response codes have been set.
             */

            r_code = sim_comm_get_sim_response_code(RCSIM_TYPE_COMM_ERR,
                    network_handle, NT_A_SIM, station_address, &sim_response_code);
            if (r_code != 0) {

                /* check if simulated response provides good response */

                if (sim_response_code != 0) {

                    /* simulate error */

                    app_info->env_returns.comm_err  = sim_response_code ;
                    return (CM_COMM_ERR);
                }
            }

            r_code = sim_comm_get_sim_response_code(RCSIM_TYPE_RESPONSE_CODE,
                    network_handle, NT_A_SIM, station_address, &sim_response_code);
            if (r_code != 0) {

                /* check if simulated response provides good response */

                if (sim_response_code != 0) {

                    /* simulate error */

                    app_info->env_returns.response_code  = sim_response_code ;
                    return (CM_RESPONSE_CODE);
                }
            }

            /*
             * Issue the simulated Comm Write
             */

            ASSERT_RET(sim_comm_write_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_comm_write_ptr)(cref, object_index, sub_index,
                    value);
            break ;

        case NT_A:

            /*
             * Issue the Real Comm Write
             */
            get_adt_fbap_cr(device_handle, &cr);
            if (r_code != SUCCESS) {
                return(r_code) ;
            }

            size = cm_value_listp->cmvl_list->cm_size;
            ASSERT_RET(real_comm_write_ptr != NULL, CM_NO_COMM) ;
            r_code = (*real_comm_write_ptr)(env_info, cr,object_index, sub_index,
                    value, size);
            break ;

        default:
            return(CM_NO_COMM) ;
    }

    if (r_code) {
        return(r_code);
    }

    free((char *)value);
    return(CM_SUCCESS) ;
}


/***********************************************************************
 *
 *  Name:  initialize
 *
 *  ShortDesc:  Initialize the communications
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
initialize(ENV_INFO *env_info,
        HOST_CONFIG *host_config)
{

    int         r_code = CM_SUCCESS ;

    switch (host_config->host_mode) {

        case CMM_OFF_LINE:

            sim_init_ptr = sim_init ;
            sim_init_network_ptr = sim_init_network ;
            sim_initiate_comm_ptr = sim_initiate_comm ;
            sim_comm_get_ptr = sim_comm_get ;
            sim_comm_put_ptr = sim_comm_put ;
            sim_comm_read_ptr = sim_comm_read ;
            sim_comm_write_ptr = sim_comm_write ;
            sim_comm_upload_ptr = sim_comm_upload ;
            sim_find_block_ptr = sim_find_block ;
            sim_param_read_ptr = sim_param_read ;
            sim_param_write_ptr = sim_param_write ;
            sim_param_unload_ptr = sim_param_unload ;
            sim_param_load_ptr = sim_param_load ;
            sim_get_host_info_ptr = sim_get_host_info;
            sim_scan_network_ptr = sim_scan_network;
            sim_sm_identify_ptr = sim_sm_identify;
            sim_device_table_ptr = sim_device_table;
            sim_detach_device_ptr = sim_detach_device;
            sim_cm_exit_ptr = sim_cm_exit;
            /*
             * Initialize the Simulated Network Comm Stack (if we're using one)
             */

            if (sim_init_ptr != NULL) {
                r_code = (*sim_init_ptr)() ;
                if (r_code != CM_SUCCESS) {
                    return(r_code) ;
                }
            }

            break;

        case CMM_ON_LINE:

            real_init_ptr = softing_stack_init;
            real_config_stack_ptr = softing_config_stack;
            real_get_stack_config_ptr = softing_get_stack_config;
            real_init_network_ptr = softing_init_network;
            real_initiate_comm_ptr = softing_initiate_comm ;
            real_comm_get_ptr = softing_comm_get ;
            real_comm_put_ptr = NULL ;
            real_comm_read_ptr = softing_comm_read ;
            real_comm_write_ptr = softing_comm_write ;
            real_comm_upload_ptr = NULL ;
            real_find_block_ptr = softing_find_block ;
            real_param_read_ptr = softing_param_read ;
            real_param_write_ptr = softing_param_write ;
            real_param_unload_ptr = NULL ;
            real_param_load_ptr = softing_param_load;
            real_scan_network = softing_scan_network;
            real_device_table = softing_device_table;
            real_detach_device = softing_detach_device;

            /*
             *  Network management and System management
             *  pointers in communication manager
             */
            real_sm_identify = softing_sm_identify;
            real_sm_clr_node = softing_sm_clr_node;
            real_sm_set_node = softing_sm_set_node;
            real_sm_clr_tag = softing_sm_clr_tag;
            real_sm_assign_tag = softing_sm_assign_tag;
            real_nm_wr_local_vcr = softing_nm_wr_local_vcr;
            real_nm_fms_initiate = softing_nm_fms_initiate;
            real_nm_get_od = softing_nm_get_od;
            real_nm_read = softing_nm_read;
            real_nm_dev_boot = softing_nm_dev_boot;
            real_cm_exit = softing_cm_exit;
            real_firmware_download = softing_firmware_download;
            real_get_host_info = softing_get_host_info;
            real_reset_fbk = softing_reset_fbk;
            real_set_appl_state = softing_set_appl_state;
            real_get_actual_index_ptr = softing_get_actual_index;
            real_read_absolute_index_ptr = softing_read_abs_index;
            real_abort_communication = softing_abort_communication;

            softing_stack_events = appl_event_indication;

            /*
             * Initialize the Real Network Comm Stack (if we're using one)
             */
            
             if (real_init_ptr != NULL) {
                r_code = (*real_init_ptr)(env_info,  
                        host_config) ;
                if (r_code != CM_SUCCESS) {
                    return(r_code) ;
                }
            } 

 //           (*appl_event_indication)(env_info, "Hello there");  
           
            LOGC_DEBUG(CPM_FF_DDSERVICE,"Initialization Done");

            break;
    }
  
    return(CM_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  scan_network
 *
 *  ShortDesc:  Scan for the for live list of the connected Device
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int scan_network(ENV_INFO* env_info, SCAN_DEV_TBL *scan_dev_table)
{
    int         r_code = CM_SUCCESS ;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Net Type is 0 as there will always be one network active, either online
     * or simulator mode.
     */
    switch (NT_NET_TYPE(0)) {

    	case NT_OFF_LINE:
    		ASSERT_RET(sim_scan_network_ptr != NULL, CM_NO_COMM) ;
    		r_code = (*sim_scan_network_ptr)(env_info, scan_dev_table) ;
        break;

        case NT_A:
        	ASSERT_RET(real_scan_network != NULL, CM_NO_COMM) ;
        	r_code = (*real_scan_network)(env_info, scan_dev_table) ;

        break;

        default:
        	r_code = CM_BAD_COMM_TYPE;
        break;
    }

    return (r_code);
}


/***********************************************************************
 *
 *  Name:  build_dev_tbl
 *
 *  ShortDesc:  Build the device table for the for live list of the 
 *              connected Device
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int build_dev_tbl( ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Populate the device table reading the data from actual
             * transmitter.
             */

        	ASSERT_RET(real_device_table != NULL, CM_NO_COMM) ;
        	r_code = (*real_device_table)(env_info, nh) ;
        	if (r_code != SUCCESS) {
        		return(r_code) ;
        	}

        	break;

        case NT_OFF_LINE:
        	/*
        	 * Populate the device table reading the data from .CFF file
        	 */

        	ASSERT_RET(sim_device_table_ptr != NULL, CM_NO_COMM) ;
        	r_code = (*sim_device_table_ptr)(env_info, nh);
        	if(r_code != SUCCESS) {
        		return(r_code);
        	}
        	break;
    }

    return (r_code);
}

/***********************************************************************
 *
 *  Name:  delete_dev_tbl
 *
 *  ShortDesc:  Clean up the memory form the device table for the for  
 *              the connected Device
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int delete_dev_tbl( ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;

    r_code =  get_nt_net_type(nh, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    
    switch (net_type) {
        case NT_A:
			/*
			 * Initialize the Real network
			 */

			ASSERT_RET(real_detach_device != NULL, CM_NO_COMM) ;
			r_code = (*real_detach_device)(env_info, nh) ;
			if (r_code != CM_SUCCESS) {
				return(r_code) ;
			}
			break;

        case NT_OFF_LINE:
			/*
			 * Initialize the Real network
			 */

			ASSERT_RET(sim_detach_device_ptr != NULL, CM_NO_COMM) ;
			r_code = (*sim_detach_device_ptr)(env_info, nh) ;
			if (r_code != CM_SUCCESS) {
				return(r_code) ;
			}
			break;
    }

    return (r_code);
}



/***********************************************************************
 *
 *  Name:  sm_clr_node
 *
 *  ShortDesc:  Clears the Node or Poll or Device address of the device
 *
 *  Description: Takes device address as input and clears the node
 *               address of the Device 
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Node address set to one of the uninitialized  set of address.
 *      between (0xF8 to 0xFB)
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int sm_clr_node(
                ENV_INFO* env_info, 
                unsigned short dev_addr,
                CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
    int         r_code = CM_SUCCESS ;

	ASSERT_RET(real_sm_clr_node != NULL, CM_NO_COMM) ;
	r_code = (*real_sm_clr_node)(env_info, dev_addr, sm_identify_cnf) ;

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  sm_clr_tag
 *
 *  ShortDesc:  Clears the Physical Device Tag of the device in the network
 *
 *  Description: Takes device address as input and clears the TAG
 *               of the Device 
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Node address set to one of the uninitialized  set of address.
 *      between (0xF8 to 0xFB) and Tag set to NULL
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int sm_clr_tag(
                ENV_INFO* env_info, 
                unsigned short dev_addr,
                CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
    int         r_code = CM_SUCCESS ;

	/*
	 * Clear the Physical Device Tag of the remote device
	 */

	ASSERT_RET(real_sm_clr_tag != NULL, CM_NO_COMM) ;
	r_code = (*real_sm_clr_tag)(env_info, dev_addr, sm_identify_cnf) ;

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  sm_assign_tag
 *
 *  ShortDesc:  Assigns the Physical Device Tag to the device in the 
 *              network
 *
 *  Description: Takes device address as input and Assigns the new
 *               TAG to the Device 
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Node address set to one of the uninitialized  set of address.
 *      between (0xF8 to 0xFB) and Tag set to New PD Tag Value
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int sm_assign_tag(
                ENV_INFO* env_info, 
                unsigned short dev_addr,
                CR_SM_IDENTIFY_CNF* sm_identify_cnf
)
{
    int         r_code = CM_SUCCESS ;
    
	ASSERT_RET(real_sm_assign_tag != NULL, CM_NO_COMM) ;
	r_code = (*real_sm_assign_tag)(env_info, dev_addr, sm_identify_cnf) ;

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  sm_set_node
 *
 *  ShortDesc:  Sets the operational node address to the device
 *
 *  Description: Takes PD TAG as input and comapres with the device
 *               and sets the new node address to the device
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Sets operational address to the device.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int sm_set_node(
                ENV_INFO* env_info, 
                unsigned short new_dev_addr,
                char *pd_tag
)
{
    int         r_code = CM_SUCCESS ;

	/*
	 * Set the Node address to the remote device in the Real network
	 */

	ASSERT_RET(real_sm_set_node != NULL, CM_NO_COMM) ;
	r_code = (*real_sm_set_node)(env_info, new_dev_addr, pd_tag) ;

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  sm_identify
 *
 *  ShortDesc:  identifies the device in the network
 *
 *  Description: Takes device address as input and returns PD Tag
 *               and Device ID of the device
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Physical Tag of the device.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int sm_identify(
                ENV_INFO* env_info, 
                unsigned short dev_addr,
                CR_SM_IDENTIFY_CNF* sm_indentify_cnf
)
{
    int         r_code = CM_SUCCESS ;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    switch (NT_NET_TYPE(0)) {

        	case NT_OFF_LINE:
        		ASSERT_RET(sim_sm_identify_ptr != NULL, CM_NO_COMM) ;
        		r_code = (*sim_sm_identify_ptr)(env_info, dev_addr, sm_indentify_cnf) ;
            break;

            case NT_A:
            	ASSERT_RET(real_sm_identify != NULL, CM_NO_COMM) ;
            	r_code = (*real_sm_identify)(env_info, dev_addr, sm_indentify_cnf) ;
            break;

            default:
            	r_code = CM_BAD_COMM_TYPE;
            break;
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  init_network
 *
 *  ShortDesc:  Initialize the communications network
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
init_network(NETWORK_HANDLE network_handle)
{

    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;
    int         config_file_id ;

    r_code =  get_nt_net_type(network_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    r_code =  get_nt_cfg_file_id(network_handle, &config_file_id) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Initialize the Real network
             */

            ASSERT_RET(real_init_network_ptr != NULL, CM_NO_COMM) ;
            r_code = (*real_init_network_ptr)(config_file_id );
            break ;

        case NT_OFF_LINE:
    
            /*
             * Initialize the Simulated network
             */

            ASSERT_RET(sim_init_network_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_init_network_ptr)(config_file_id, net_type) ;
            break ;

        default:
            return(CM_NO_COMM) ;
    }
    return(r_code);
}


/***********************************************************************
 *
 *  Name:  find_block
 *
 *  ShortDesc:  Find a block with the specified tag.
 *
 *  Description:
  *
 *  Inputs:
 *      tag - the tag of the block that is sought.
 *
 *  Outputs:
 *      find_cnf - the corresponding Find Confirm.
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Rick Sharpe
 *      Jon Reuter
 *      Kent Anderson
 *
 **********************************************************************/

int
find_block(char *block_tag, NETWORK_HANDLE network_handle, FIND_CNF_A **find_cnf,
    ENV_INFO *env_info)
{

    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;
    int         config_file_id ;
    APP_INFO    *app_info;

    app_info = env_info->app_info;

    ASSERT_RET(block_tag && find_cnf, CM_BAD_POINTER) ;

        /*
         *  Check the type of the network.
         */

        r_code = get_nt_net_type(network_handle, &net_type) ;
        if (r_code != SUCCESS) {
            return(r_code) ;
        }

        switch (net_type) {
            case NT_A:

                /*
                 *  Call the Real Network to find the block.
                 */

                ASSERT_RET(real_find_block_ptr != NULL, CM_NO_COMM) ;
                
                r_code = (*real_find_block_ptr)(block_tag, network_handle,
                        find_cnf);
                if (r_code == CM_SUCCESS) {
                    (*find_cnf)->network = network_handle;
                }
                app_info->env_returns.comm_err  = 0 ;
                app_info->env_returns.response_code  = 0 ;
                break;

            case NT_A_SIM:
            case NT_OFF_LINE: //LNT

                /*
                 *  Call the Simulator to find the block.
                 */

                ASSERT_RET(sim_find_block_ptr != NULL, CM_NO_COMM) ;
                r_code = get_nt_cfg_file_id(network_handle, &config_file_id);
                if (r_code != CM_SUCCESS) {
                    return(r_code);
                }
                r_code = (*sim_find_block_ptr)(block_tag, config_file_id,
                        find_cnf);
                if (r_code == CM_SUCCESS) {
                    (*find_cnf)->network = network_handle;
                }
                app_info->env_returns.comm_err  = 0 ;
                app_info->env_returns.response_code  = 0 ;

                break;

            default:

                break;
        }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  param_read
 *
 *  ShortDesc:  Read a parameter's value into the parameter cache
 *
 *  Description:
 *      The param_read function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      po -        contains the reference (id or offset) of the 
 *                  parameter to be read 
 *      subindex -  the sub-index of the (array or record) member.
 *
 *  Outputs:
 *      env_info -  contains returns of any communication errors and it
 *                  contains returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
param_read(ENV_INFO *env_info, int po, SUBINDEX subindex)
{

    DEVICE_HANDLE   device_handle ;
    int             cm_r_code = CM_SUCCESS ;
    NET_TYPE        net_type ;


    ASSERT_RET(env_info, CM_BAD_POINTER) ; 

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(valid_block_handle(env_info->block_handle), 
            CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    cm_r_code = get_abt_adt_offset(env_info->block_handle, 
            &device_handle) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    if (!valid_device_handle(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    cm_r_code = get_adt_net_type(device_handle, &net_type) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Read a parameter from a Real network
             */

            ASSERT_RET(real_param_read_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*real_param_read_ptr)(env_info,po, subindex) ;
            break ;

        case NT_OFF_LINE:

            /*
             * Read a parameter from a Real network
             */

            ASSERT_RET(sim_param_read_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*sim_param_read_ptr)(env_info,po, subindex) ;
            break ;

        case NT_A_SIM:

            /*
             * Read a parameter from a Simulated network
             */

            ASSERT_RET(sim_param_read_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*sim_param_read_ptr)(env_info,po, subindex) ;
            break ;

        default:
            return(CM_NO_COMM);
    }
    return(cm_r_code) ;
}


/***********************************************************************
 *
 *  Name:  param_write
 *
 *  ShortDesc:  Write a parameter's value from the parameter cache
 *          to a device.
 *
 *  Description:
 *      The param_write function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      po -        contains the reference (id or offset) of the 
 *                  parameter 
 *                  to be read, 
 *      subindex -  it contains the sub-index of the 
 *                  (array or record) member.
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and it contains returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
param_write(ENV_INFO *env_info, int po, SUBINDEX subindex)
{

    DEVICE_HANDLE   device_handle ;
    int             cm_r_code = CM_SUCCESS ;
    NET_TYPE        net_type ;

    ASSERT_RET(env_info, CM_BAD_POINTER) ;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(valid_block_handle(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    cm_r_code = get_abt_adt_offset(env_info->block_handle,
            &device_handle) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    if (!valid_device_handle(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    cm_r_code = get_adt_net_type(device_handle, &net_type) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Write a parameter from a Real network
             */

            ASSERT_RET(real_param_write_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*real_param_write_ptr)(env_info, po, subindex) ;
            break ;

        case NT_A_SIM:

            /*
             * Write a parameter from a Simulated network
             */

            ASSERT_RET(sim_param_write_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*sim_param_write_ptr)(env_info, po, subindex) ;
            break ;

        default:
            return(CM_NO_COMM);
    }
    return(cm_r_code) ;
}


/***********************************************************************
 *
 *  Name:  param_load
 *
 *  ShortDesc:  Read a parameter's value from a device.
 *
 *  Description:
 *      The param_load function takes a block handle, 
 *      param ref, and sub-index and reads an parameter's
 *      value from the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be read.  The (requested)
 *      value output is a pointer to a value strucure which resides in
 *      system memory and should be freed after it is used.
 *
 *      Note: This function does not take values from the parameter
 *            cache and it does not update the parameter cache.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      po -    contains  a reference (offset) of the parameter 
 *                  to be read.
 *      subindex - the sub-index of the (array or record) member.
 *
 *  Outputs:
 *      cm_value_listp - The requested value(s).
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
param_load(ENV_INFO *env_info, int po, SUBINDEX subindex, COMM_VALUE_LIST *cm_value_listp)
{

    DEVICE_HANDLE   device_handle ;
    int             cm_r_code = CM_SUCCESS ;
    NET_TYPE        net_type ;

    ASSERT_RET(env_info && cm_value_listp, CM_BAD_POINTER) ;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(valid_block_handle(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    cm_r_code = get_abt_adt_offset(env_info->block_handle,
            &device_handle) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    if (!valid_device_handle(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    cm_r_code = get_adt_net_type(device_handle, &net_type) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Read a parameter from a Real network
             */

            ASSERT_RET(real_param_load_ptr != NULL, CM_NO_COMM) ;

            cm_r_code = (*real_param_load_ptr) (env_info, po,
                    subindex, cm_value_listp) ;
            break ;

        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             * Read a parameter from a Simulated network
             */

            ASSERT_RET(sim_param_load_ptr != NULL, CM_NO_COMM) ;

            cm_r_code = (*sim_param_load_ptr) (env_info, po,
                    subindex, cm_value_listp) ;
            break ;

        default:
            return(CM_NO_COMM);
    }
    return(cm_r_code);
}


/***********************************************************************
 *
 *  Name:  param_unload
 *
 *  ShortDesc:  Write a parameter's value to a device.
 *
 *  Description:
 *      The param_unload function takes a block handle, 
 *      param ref, and sub-index and writes a parameter's
 *      value to the corresponding device.  If the sub-index is
 *      zero, the object's entire value will be written.  
 *
 *      Note: This function does not take values from the parameter
 *            cache and it does not update the parameter cache.
 *
 *  Inputs:
 *      env_info - contains the handle of the block that the parameter
 *                  exists in.
 *      po - contains  a reference (offset) of the parameter 
 *                  to be read
 *      subindex - sub-index of the (array or record) member.
 *      cm_value_listp - The requested value(s).
 *
 *  Outputs:
 *      env_info - contains the returns of any communication errors
 *                  and the returns of any response codes 
 *                  (application errors and warnings)
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
param_unload(ENV_INFO *env_info, int po,  SUBINDEX subindex, COMM_VALUE_LIST *cm_value_listp)
{

    DEVICE_HANDLE   device_handle ;
    int             cm_r_code = CM_SUCCESS ;
    NET_TYPE        net_type ;

    ASSERT_RET(env_info && cm_value_listp, CM_BAD_POINTER) ;

    /*
     * Check to make sure that the block handle is valid
     */

    ASSERT_RET(valid_block_handle(env_info->block_handle), 
        CM_BAD_BLOCK_HANDLE) ;

    /*
     *  Check to make sure that the device handle is valid.
     */

    cm_r_code = get_abt_adt_offset(env_info->block_handle, &device_handle) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    if (!valid_device_handle(device_handle)) {
        return(CM_BAD_DEVICE_HANDLE);
    }

    cm_r_code = get_adt_net_type(device_handle, &net_type) ;
    if (cm_r_code != SUCCESS) {
        return(cm_r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             * Write a parameter to a Real network
             */

            ASSERT_RET(real_param_unload_ptr != NULL,CM_NO_COMM) ;

            cm_r_code = (*real_param_unload_ptr) (env_info, po,
                    subindex, cm_value_listp) ;
            break ;

        case NT_A_SIM:

            /*
             * Write a parameter to a Simulated network
             */

            ASSERT_RET(sim_param_unload_ptr != NULL, CM_NO_COMM) ;

            cm_r_code = (*sim_param_unload_ptr) (env_info, po,
                    subindex, cm_value_listp) ;
            break ;

        default:
            return(CM_NO_COMM);
    }

    return(cm_r_code);
}


/***********************************************************************
 *
 *  Name:  comm_get
 *
 *  ShortDesc:   Get an object from a device.
 *
 *  Description:
 *      The comm_get function takes a device handle, communication
 *      type, and object index and gets the corresponding object.
 *      The (requested) object output is a pointer to an object.
 *      This object and any of its allocated contents reside in
 *      system memory and should be freed after it is used.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      exists in.
 *      comm_type - the particular OD that the object exists in
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the requested object.
 *
 *  Outputs:
 *      object - the requested object.
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
comm_get(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, OBJECT_INDEX object_index,
    OBJECT **object, ENV_INFO *env_info)
{

    int         r_code;
    CREF        cref ;
    NET_TYPE    net_type ;
    APP_INFO    *app_info;

    app_info = env_info->app_info;

    ASSERT_RET(valid_device_handle(device_handle), CM_BAD_DEVICE_HANDLE);

    /*
     *  Get the proper CREF and call the Simulator to get the object.
     *  The returned object is allocated in system memory.
     */

    r_code = get_cref(device_handle, comm_type, &cref);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    r_code = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             *  Get an object from a device on a Real Network.
             */

            ASSERT_RET(real_comm_get_ptr != NULL, CM_NO_COMM) ;
            r_code = (*real_comm_get_ptr)(cref, object_index, object);

            app_info->env_returns.comm_err  = 0 ; /* To be verified Jayanth */
            app_info->env_returns.response_code  = 0 ;
            break ;

        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             *  Get an object from a device on a Simulated Network.
             */

            ASSERT_RET(sim_comm_get_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_comm_get_ptr)(cref, object_index, object);

            app_info->env_returns.comm_err  = 0 ;
            app_info->env_returns.response_code  = 0 ;

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  comm_put
 *
 *  ShortDesc:   Put an object into a device.
 *
 *  Description:
 *      The comm_put function takes a device handle, communication
 *      type, object index, and object and puts the object into the
 *      corresponding device/OD.  The object being put may be allocated
 *      anywhere as its information is copied before it is transmitted.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      is to be put in.
 *      comm_type - the particular OD that the object will be put in
 *                  (OPOD, DDOD, or MIB).
 *      object_index - the index of the object that is to be put.
 *      object - the object that is to be put.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
comm_put(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, OBJECT_INDEX object_index,
    OBJECT *object, ENV_INFO *env_info)
{

    CREF        cref;
    int         r_code;
    NET_TYPE    net_type ;
    APP_INFO    *app_info;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    ASSERT_RET(valid_device_handle(device_handle), CM_BAD_DEVICE_HANDLE);

    /*
     *  Get the proper CREF and call the Simulator to get the object.
     *  The returned object is allocated in system memory.
     */

    r_code = get_cref(device_handle,comm_type,&cref);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    r_code = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             *  Put an object into a device on a Real Network.
             */

            ASSERT_RET(real_comm_put_ptr != NULL,CM_NO_COMM) ;
            r_code = (*real_comm_put_ptr)(cref,object_index,object);
            break;

        case NT_A_SIM:

            /*
             *  Put an object into a device on a Simulated Network.
             */

            ASSERT_RET(sim_comm_put_ptr != NULL,CM_NO_COMM) ;
            r_code = (*sim_comm_put_ptr)(cref,object_index,object);

            app_info->env_returns.comm_err  = 0 ;
            app_info->env_returns.response_code  = 0 ;

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  comm_upload
 *
 *  ShortDesc:   Upload an object's data to a device.
 *
 *  Description:
 *      The comm_upload function takes a device handle, commun-
 *      ication type, object index, and data and uploads the object's
 *      data into the corresponding device/OD.  The data being up-
 *      loaded may be allocated anywhere as its information is copied
 *      before it is transmitted.
 *
 *  Inputs:
 *      device_handle - the handle of the device that the object
 *                      exists in.
 *      comm_type - the particular OD that the object will be uploaded
 *                  to (OPOD, DDOD, or MIB).
 *      object_index - the index of the object to be uploaded.
 *      data - the data to be uploaded.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
comm_upload(DEVICE_HANDLE device_handle, COMM_TYPE comm_type, OBJECT_INDEX object_index,
    UINT8 *data, ENV_INFO *env_info)
{

    CREF        cref;
    int         r_code;
    NET_TYPE    net_type ;
    APP_INFO    *app_info;

    app_info = env_info->app_info;

    /*
     *  Check to make sure that the device handle is valid.
     */

    ASSERT_RET(valid_device_handle(device_handle), CM_BAD_DEVICE_HANDLE);

    /*
     *  Get the proper CREF and call the Simulator to upload the data.
     */

    r_code = get_cref(device_handle, comm_type, &cref);
    if (r_code != CM_SUCCESS) {
        return(r_code);
    }

    /*
     *  Check which type of network the device is connected to.
     */

    r_code = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             *  Upload an object's data into a device on a Real Network.
             */

            ASSERT_RET(real_comm_upload_ptr != NULL, CM_NO_COMM) ;
            r_code = (*real_comm_upload_ptr)(cref, object_index, data) ;
                    
            break ;

        case NT_A_SIM:

            /*
             *  Upload an object's data into a device on a Simulated Network.
             */

            ASSERT_RET(sim_comm_upload_ptr != NULL, CM_NO_COMM) ;
            r_code = (*sim_comm_upload_ptr)(cref, object_index, data);

            app_info->env_returns.comm_err  = 0 ;
            app_info->env_returns.response_code  = 0 ;

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  initiate_comm
 *
 *  ShortDesc:   Initiate communication with an device.
 *
 *  Description:
 *      The initiate_comm function takes a device handle and
 *      initiates communication with the corresponding device.
 *
 *  Inputs:
 *      device_handle - handle of the device to initialize comm with
 *
 *  Outputs:
 *      opod_cref   - CREF of the operational Object Dictionary
 *      ddod_cref   - CREF of the Device Description Object Dictionary
 *      mib_cref    - CREF of the MIB Object Dictionary
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Kent Anderson
 *
 **********************************************************************/

int
initiate_comm(DEVICE_HANDLE device_handle, CREF *opod_cref, CREF *ddod_cref,
    CREF *mib_cref, ENV_INFO *env_info)
{

    int             r_code ;
    NETWORK_HANDLE  network_handle ;
    UINT16          station_address ;
    NET_TYPE        net_type ;
    int             config_file_id ;
    APP_INFO        *app_info;

    app_info = env_info->app_info;

    ASSERT_RET(valid_device_handle(device_handle), CM_NO_COMM);

    /*
     *  Get the Network Handle and Station Address.
     */

    r_code = get_adt_network(device_handle, &network_handle);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    r_code = get_adt_station_address(device_handle, &station_address);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    /*
     *  Check which type of network the device is connected to.
     */

    r_code  = get_adt_net_type(device_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }

    switch (net_type) {
        case NT_A:

            /*
             *  Initiate communication with a device on a Real Network.
             */

            ASSERT_RET(real_initiate_comm_ptr != NULL,CM_NO_COMM) ;
            r_code = get_nt_cfg_file_id(network_handle, &config_file_id);
            if (r_code != CM_SUCCESS) {
                return(r_code);
            }
            r_code = (*real_initiate_comm_ptr)(network_handle,
                    station_address, opod_cref, ddod_cref, mib_cref) ;

            app_info->env_returns.comm_err  = 0 ;
            app_info->env_returns.response_code  = 0 ;

            break;

        case NT_A_SIM:
        case NT_OFF_LINE:  //LNT

            /*
             *  Initiate communication with a device on a Simulated Network.
             */

            ASSERT_RET(sim_initiate_comm_ptr != NULL, CM_NO_COMM) ;
            r_code = get_nt_cfg_file_id(network_handle, &config_file_id);
            if (r_code != CM_SUCCESS) {
                return(r_code);
            }
            r_code = (*sim_initiate_comm_ptr)(config_file_id,
                    (int)station_address, opod_cref, ddod_cref, mib_cref);

            app_info->env_returns.comm_err  = 0 ;
            app_info->env_returns.response_code  = 0 ;

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  terminate_comm
 *
 *  ShortDesc:   Terminate communication with a device.
 *
 *  Description:
 *      The terminate_comm function takes a device handle and
 *      terminates communication with the corresponding device.
 *
 *  Inputs:
 *      device_handle - handle of the device to terminate comm with
 *
 *  Outputs:
 *      env_info - 
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jon Reuter
 *
 **********************************************************************/

int
terminate_comm(DEVICE_HANDLE device_handle, ENV_INFO *env_info)
{

    NET_TYPE        net_type;
    int             r_code;
    APP_INFO        *app_info;

    app_info = env_info->app_info;

    ASSERT_RET(valid_device_handle(device_handle), CM_NO_COMM);

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     *  Check which type of network the device is connected to.
     */

    r_code = get_adt_net_type(device_handle, &net_type);
    if (r_code) {
        return(r_code);
    }

    switch (net_type) {
        case NT_A:

            /*
             *  Terminate communication with a device on a Real Network.
             */

            break;

        case NT_A_SIM:

            /*
             *  Terminate communication with a device on a Simulated Network.
             */

            break;

        default:
            return(CM_NO_COMM);
    }

    return(r_code);
}


/***********************************************************************
 *
 *  Name:  nm_wr_local_vcr
 *
 *  ShortDesc:  Writes the VCR to local NMA address
 *
 *  Description: Takes CR number as input and writes the VCR 
 *               configurations to the input CR
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      Writes Configuration settings to the local VCR
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int nm_wr_local_vcr(
                ENV_INFO* env_info, 
                NETWORK_HANDLE network_handle, 
                CM_NMA_VCR_LOAD* nma_vcr_load
)
{
    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;
    
    r_code =  get_nt_net_type(network_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    
    switch (net_type) {
        case NT_A:

            /*
             *  Write the VCR configurations to local address in NMA in the Real network
             */

            ASSERT_RET(real_nm_wr_local_vcr != NULL, CM_NO_COMM) ;
            r_code = (*real_nm_wr_local_vcr)(env_info, nma_vcr_load) ;
            break ;

        case NT_A_SIM:


            break ;

        default:
            return(CM_NO_COMM) ;
    }
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  nm_fms_initiate
 *
 *  ShortDesc:  Initiates the FMS services
 *
 *  Description: Takes CR number as input and Initiate FMS service 
 *               on the input CR
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int nm_fms_initiate(
                ENV_INFO* env_info, 
                NETWORK_HANDLE network_handle, 
                CM_NMA_VCR_LOAD* nma_vcr_load
)
{
    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;
    
    r_code =  get_nt_net_type(network_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    
    switch (net_type) {
        case NT_A:

            /*
             * Initiate the FMS service on the CR in the Real network
             */

            ASSERT_RET(real_nm_fms_initiate != NULL, CM_NO_COMM) ;
            r_code = (*real_nm_fms_initiate)(env_info, nma_vcr_load) ;
            break ;

        case NT_A_SIM:


            break ;

        default:
            return(CM_NO_COMM) ;
    }
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  nm_get_od
 *
 *  ShortDesc:  Gets Object Description
 *
 *  Description: Takes CR number as input and gets Object Description
 *               on the input CR
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int nm_get_od(
                ENV_INFO* env_info, 
                NETWORK_HANDLE network_handle, 
                CM_NMA_VCR_LOAD* nma_vcr_load
)
{
    int         r_code = CM_SUCCESS ;
    NET_TYPE    net_type ;
    
    r_code =  get_nt_net_type(network_handle, &net_type) ;
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    
    switch (net_type) {
        case NT_A:

            /*
             * Get OD on the CR in the Real network
             */

            ASSERT_RET(real_nm_get_od != NULL, CM_NO_COMM) ;
            r_code = (*real_nm_get_od)(env_info, nma_vcr_load) ;
            break ;

        case NT_A_SIM:


            break ;

        default:
            return(CM_NO_COMM) ;
    }
    return(r_code);
}


/***********************************************************************
 *
 *  Name:  nm_dev_boot
 *
 *  ShortDesc: Writes value in remote device boot operational class.
 *
 *  Description:
 *          This function takes network handle, index of the NM object and
 *          value and writes the value in remote device boot operational 
 *          class.
 *   
 *  Inputs:
 *          NETWORK_HANDLE - network handle of the connected device.
 *           index - index of remote device boot operational class.
 *           value - valuet to be written.
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *          
 *  Returns:
 *      
 *
 *  Authors:Shivaji Tikkireddy
 *
 **********************************************************************/

int nm_dev_boot(
                ENV_INFO* env_info, NETWORK_HANDLE nh, USIGN16 index, int value)
{
    int         r_code = CM_SUCCESS ;

    ASSERT_RET(real_nm_dev_boot != NULL, CM_NO_COMM) ;
    r_code = (*real_nm_dev_boot)(env_info, nh, index, value) ;
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  nm_read
 *
 *  ShortDesc: Reads NM objects of host or remote device
 *
 *  Description:
 *          This function takes network handle, index of the NM object and
 *          device_select and reads the NM object.
 *   
 *  Inputs:
 *          NETWORK_HANDLE - network handle of the connected device.
 *           index - index of NM Object.
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *          
 *  Returns:
 *      
 *
 *  Authors:Shivaji Tikkireddy
 *
 **********************************************************************/

int nm_read(
                ENV_INFO* env_info, NETWORK_HANDLE nh,              
                USIGN16 index, int service, int device_select
)
{
    int         r_code = CM_SUCCESS ;

    ASSERT_RET(real_nm_read != NULL, CM_NO_COMM) ;
    r_code = (*real_nm_read)(env_info, nh, index, service, device_select) ;
    return(r_code);
}


/***********************************************************************
 *
 *  Name:  cm_exit
 *
 *  ShortDesc:  close of communication manager
 *
 *  Description: disconnects the communication manager and resets the 
 *               FBK board
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_exit( ENV_INFO* env_info, NET_TYPE nt )
{
    int         r_code = CM_SUCCESS ;
    APP_INFO        *app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    /*
     * Close and reset the real network
     */
    switch(nt)
    {
    	case NT_A:
    
    		ASSERT_RET(real_cm_exit != NULL, CM_NO_COMM) ;
    		r_code = (*real_cm_exit)(env_info) ;
    		break;

    	case NT_OFF_LINE:
    		ASSERT_RET(sim_cm_exit_ptr != NULL, CM_NO_COMM) ;
    		r_code = (*sim_cm_exit_ptr)(env_info) ;
    		break;
    }
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cm_firmware_download
 *
 *  ShortDesc:  Firmware Download
 *
 *  Description: Downlaods the firmware specified by the path into the 
 *               FBK board
 *  Inputs:
 *      env_info -- Environmental variable which will catch the returns
 *      path --     Path to the Firmware to be loaded
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_firmware_download( ENV_INFO* env_info, char *path )
{
    int         r_code = CM_SUCCESS ;
    

    /*
     * Close and reset the real network
     */

    ASSERT_RET(real_firmware_download != NULL, CM_NO_COMM) ;
    r_code = (*real_firmware_download)(env_info, path) ;
    
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cm_get_host_info
 *
 *  ShortDesc:  Get FBK Board device information
 *
 *  Description: Reads the FBK Board information
 *
 *  Inputs:
 *      env_info -- Environmental variable which will catch the returns
 *      path --     Path to the Firmware to be loaded
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_get_host_info( ENV_INFO* env_info, HOST_INFO *host_info, HOST_CONFIG *hc )
{
    int         r_code = CM_SUCCESS ;
   
    T_FBAPI_DEVICE_INFO   device_info;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;


    memset((char *)&device_info, 0, sizeof(T_FBAPI_DEVICE_INFO));

    /*
     * Get the Device formation of FBK Board
     */

    switch(hc->host_mode)
    {
        case CMM_ON_LINE:

            ASSERT_RET(real_get_host_info != NULL, CM_NO_COMM) ;
            r_code = (*real_get_host_info)(env_info, &device_info) ;

            break;

        case CMM_OFF_LINE:
            ASSERT_RET(sim_get_host_info_ptr != NULL,
                    CM_NO_COMM);
            r_code = (*sim_get_host_info_ptr)(env_info,
                    &device_info) ;

            break;
    }

    // The T_FBAPI_DEVICE_INFO was renamed to T_FBAPI_DEVICE_INFO
    //  and its members were renamed
    host_info->device_id = device_info.DeviceId;
    host_info->hardware_revision = device_info.HardwareRevision;
    host_info->serial_device_number = device_info.SerialDeviceNumber;
    host_info->boot_version = device_info.BootblockVersion;
    strcpy((char *)host_info->boot_version_str, 
            (char*)device_info.BootblockVersionString);
    host_info->firmware_version = device_info.FirmwareVersion;
    strcpy((char *)host_info->firmware_version_str, 
            (char*)device_info.FirmwareVersionString);
  
    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cm_reset_fbk
 *
 *  ShortDesc:  Reset the FBK board
 *
 *  Description: Issues the Abort command on FBAP and MIB CR and 
 *               re-initiates the CR's.
 *
 *  Inputs:
 *      env_info -- Environmental variable which will catch the returns
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *      
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_reset_fbk( ENV_INFO* env_info, NETWORK_HANDLE nh, HOST_CONFIG *hc)
{
    int         r_code = CM_SUCCESS ;
   
    /*
     * Get the Device formation of FBK Board
     */

    ASSERT_RET(real_reset_fbk != NULL, CM_NO_COMM) ;
    r_code = (*real_reset_fbk)(env_info, nh, hc) ;

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  cm_set_appl_state
 *
 *  ShortDesc:  Set the connection manager application state
 *
 *  Description: Sets the state to busy to block the device connection
 *  			 by Tab application over TCP.
 *
 *  Inputs:
 *      env_info -- Environmental variable which will catch the returns
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_set_appl_state( ENV_INFO* env_info, int *curr_state, int new_state)
{
    int         r_code = CM_SUCCESS ;

    /*
     * Get the Device formation of FBK Board
     */

    ASSERT_RET(real_set_appl_state != NULL, CM_NO_COMM) ;
    r_code = (*real_set_appl_state)(env_info, curr_state, new_state);

    return(r_code);
}

/***********************************************************************
 *
 *  Name:  configure_stack
 *
 *  ShortDesc:  Scan for the for live list of the connected Device
 *
 *  Description:
 *
 *  Inputs:
 *      None
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *      Zero for success and non-zero for failure.
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int configure_stack(ENV_INFO* env_info, CM_NMA_INFO *nma_info, HOST_CONFIG *hc)
{
    int         r_code = CM_SUCCESS ;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    switch (NT_NET_TYPE(0)) {

    	case NT_OFF_LINE:
            /*
             * Do Nothing
             */
        break;

        case NT_A:
        	ASSERT_RET(real_config_stack_ptr != NULL, CM_NO_COMM) ;
        	r_code = (*real_config_stack_ptr)(env_info, nma_info, hc) ;

        break;

        default:
        	r_code = CM_BAD_COMM_TYPE;
        break;
    }

    return (r_code);
}

int get_stack_configuration(ENV_INFO *env_info, CM_NMA_INFO *nma_info)
{
    int r_code = CM_SUCCESS;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

    switch (NT_NET_TYPE(0)) {

    	case NT_OFF_LINE:
            /*
             * Do Nothing
             */
        break;

        case NT_A:
        	ASSERT_RET(real_get_stack_config_ptr != NULL, CM_NO_COMM) ;
        	r_code = (*real_get_stack_config_ptr)(env_info, nma_info) ;

        break;

        default:
        	r_code = CM_BAD_COMM_TYPE;
        break;
    }

    return (r_code);
}


int cm_get_actual_index(unsigned short *index, int *si, unsigned short *cr)
{
	int r_code;

	ASSERT_RET(real_get_actual_index_ptr != NULL, CM_NO_COMM) ;
	r_code = (*real_get_actual_index_ptr)(index, si, cr) ;

	return r_code;
}

int cm_dds_bypass_read(ENV_INFO *env_info, unsigned short index, int si,
		unsigned short cr, void *val)
{
	int r_code;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;

	ASSERT_RET(real_read_absolute_index_ptr != NULL, CM_NO_COMM) ;
	r_code = (*real_read_absolute_index_ptr)(env_info, index, si, cr, val) ;

	return r_code;
}

/***********************************************************************
 *
 *  Name:  cm_abort_cr_fn
 *
 *  ShortDesc:  Abort the CR communication
 *
 *  Description: Issues Abort command to transducer device to disconnect the
 *  			 communication with the device.
 *
 *  Inputs:
 *      env_info -- Environmental variable which will catch the returns
 *      mib_cr --   MIB CR number
 *      fbap_cr --  FBAP CR number
 *
 *  Outputs:
 *      SUCCESS
 *      FAILURE
 *  Returns:
 *
 *
 *  Authors:
 *      Jayanth Mahadeva
 *
 **********************************************************************/

int cm_abort_cr_fn( ENV_INFO* env_info, unsigned short mib_cr, unsigned short
		fbap_cr)
{
    int         r_code = CM_SUCCESS ;

    T_FBAPI_DEVICE_INFO   device_info;
    APP_INFO        *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    app_info->env_returns.comm_err = 0;
    app_info->env_returns.response_code = 0;


    /*
     * Get the Device formation of FBK Board
     */

    ASSERT_RET(real_abort_communication != NULL, CM_NO_COMM) ;
    r_code = (*real_abort_communication)(env_info, mib_cr, fbap_cr) ;

    return(r_code);
}
