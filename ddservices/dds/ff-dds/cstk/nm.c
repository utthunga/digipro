/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file
    Device Network management module.

    This file contains all functions pertaining to the network
  	management of devices on Network.

 	The Device Network Management functions are as follows:

 	1.  Creation and Inititialization of VCR
 	2.  Loading VCR
 	3.  Termination of loading VCR
 	4.  Reading the data from specifiv CR
 	5.  Reading NM object Header and its data.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "devsim.h"
#include "ddsvtsim.h"
#include "dds_comm.h"
#include "cm_rod.h"
#include "rcsim.h"
#include "int_diff.h"
#include "nm.h"
#include "cm_loc.h"
#include "device_access.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

/***********************************************************************
 *
 *	Name: softing_nm_wr_local_vcr
 *
 *	ShortDesc:  Write the VCR configurations to local address in NMA
 *
 *	Description:
 *		The softing_nm_wr_local_vcr function takes a CR number and
 *      VCR configurations as input and writes configuration the value
 *      to the local VCR
 *
 *	Inputs:
 *		cr_no - CR number to write VCR Configurations
 *      nma_vcr_load - configuration settings to write into CR
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS on success and respective communication error code
 *      on failure
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/

int softing_nm_wr_local_vcr (
    ENV_INFO* env_info,
    CM_NMA_VCR_LOAD* nma_vcr_load
)
{
	int		r_code;
	APP_INFO *app_info;
    FBAP_NMA_REQ fbap_nma_req;
    USIGN8          device_addr;
    USIGN32         vfd_ref;

	app_info = (APP_INFO *)env_info->app_info;

    fbap_nma_req.cr_index = nma_vcr_load->cr_index;
    fbap_nma_req.loc_add = nma_vcr_load->loc_add;
    fbap_nma_req.rem_add = nma_vcr_load->rem_add;
    fbap_nma_req.index = nma_vcr_load->index;
    fbap_nma_req.nma_wr_flag = nma_vcr_load->nma_wr_flag;
    
    (void *)memcpy((char *)&fbap_nma_req.nma_info, (char *)&nma_vcr_load->nma_info,
                sizeof(T_NMA_INFO));

    if(TRUE == nma_vcr_load->nma_wr_flag)
    {
        set_device_admin_vfd_nr(fbap_nma_req.rem_add,
            fbap_nma_req.cr_index);
        set_device_admin_device_addr(fbap_nma_req.rem_add,
            fbap_nma_req.cr_index);

        set_device_admin_type_role(0xA2u, fbap_nma_req.cr_index);
        set_device_admin_local_nma_write(TRUE);
        /*
         * Send the request command and acquire write lock
         */
        r_code = send_wait_receive_cmd(env_info, HNDL_LOCAL_VCR_WR_REQ,
                    &fbap_nma_req, NULL);

        if(r_code != CM_SUCCESS) {
            app_info->env_returns.comm_err = CM_COMM_ERR;
		    return(r_code);
        }

       /*
        * Checking for the Positive or Negative output from the
        * remote device
        */
        if (POS == get_service_result())
        {
            LOGC_TRACE(CPM_FF_DDSERVICE,"NMA VCR local write success");
            return(r_code);
        }
        else if(NEG == get_service_result())
        {
            T_ERROR *nm_error = (T_ERROR *)global_res_buf;

            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Error Class code = %d\n",
                nm_error->class_code);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Additional Detail = %d\n",
                nm_error->add_detail);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Additional Description = %s\n",
                nm_error->add_description);
            app_info->env_returns.response_code = nm_error->class_code;

            return CS_NMA_ERR(app_info->env_returns.response_code);
        }
    }
    else
    {
        device_addr = (USIGN8) (fbap_nma_req.loc_add / 0x100u);
        if ((device_addr >= 16) && (device_addr <= 247))
        {
            vfd_ref = get_device_admin_remote_vfd();
        }
        else
        {
            vfd_ref = 0;
        }

        fbap_nma_req.vfd_ref = vfd_ref;

        r_code = send_wait_receive_cmd(env_info, HNDL_FMS_VCR_WR_REQ,
                &fbap_nma_req, NULL);
        if(r_code != CM_SUCCESS) {
		    return(r_code);
        }

        if (POS == get_service_result())
        {
            return(r_code);
        }
        else if(NEG == get_service_result())
        {
            T_ERROR *nm_error = (T_ERROR *)global_res_buf;

            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Error Class code = %d\n",
                nm_error->class_code);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Additional Detail = %d\n",
                nm_error->add_detail);
            LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_wr_local_vcr: Additional Description = %s\n",
                nm_error->add_description);
            app_info->env_returns.response_code = nm_error->class_code;

            return CS_NMA_ERR(app_info->env_returns.response_code);
        }

/*      else if(IND == get_service_primitive())
        {
            abt_ptr = (T_CTXT_ABORT_REQ*)global_res_buf;
            app_info->env_returns.response_code = abt_ptr->reason;
            LOGC_ERROR(CPM_FF_DDSERVICE,"Abort ID = 0x%x\n", abt_ptr->abort_id);
            LOGC_ERROR(CPM_FF_DDSERVICE,"Local/Remote = 0x%x\n", abt_ptr->local);
            LOGC_ERROR(CPM_FF_DDSERVICE,"Additional Detail = %s\n", abt_ptr->detail);
		    return(r_code);
        }
*/
    }
	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name: softing_nm_fms_initiate
 *
 *	ShortDesc:  Initiate the FMS service on the CR
 *
 *	Description:
 *		The softing_nm_fms_initiate function takes a CR number and
 *      Initiate the FMS service on the CR
 *
 *	Inputs:
 *		cr_no - CR number to initiate FMS
 *      nma_vcr_load - configuration settings to write into CR
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS on success and respective communication error code
 *      on failure
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/

int softing_nm_fms_initiate (
                             ENV_INFO* env_info,
                             CM_NMA_VCR_LOAD* nma_vcr_load
)
{
	int		r_code;
	APP_INFO *app_info;
    FBAP_NMA_REQ fbap_nma_req;

	app_info = (APP_INFO *)env_info->app_info;

    fbap_nma_req.cr_index = nma_vcr_load->cr_index;
    fbap_nma_req.loc_add = nma_vcr_load->loc_add;
    fbap_nma_req.rem_add = nma_vcr_load->rem_add;

    (void *)memcpy((char *)&fbap_nma_req.nma_info, (char *)&nma_vcr_load->nma_info,
                   sizeof(T_NMA_INFO ));

    r_code = send_wait_receive_cmd(env_info, INITIATE_REQUEST,
                &fbap_nma_req, NULL);

    if(r_code != CM_SUCCESS) {
		return(r_code);
    }

    if (POS == get_service_result())
    {
        return(r_code);
    }
    else if(NEG == get_service_result())
    {
        T_CTXT_INIT_ERR_CNF *fms_err = (T_CTXT_INIT_ERR_CNF *) global_res_buf;
        app_info->env_returns.response_code = fms_err->class_code;

        LOGC_ERROR(CPM_FF_DDSERVICE,"FMS_INITIATE: Response code = 0x%x\n",
            fms_err->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"FMS_INITIATE: snd_len = 0x%x\n",
            fms_err->snd_len);
        LOGC_ERROR(CPM_FF_DDSERVICE,"FMS_INITIATE: rcv_len = 0x%x\n",
            fms_err->rcv_len);
        LOGC_ERROR(CPM_FF_DDSERVICE,"FMS_INITIATE: Additional Detail = %s\n",
            fms_err->supported_features);

        return CS_FMS_ERR(app_info->env_returns.response_code);
    }

    /*
    else if(IND == get_service_primitive())
    {
        abt_ptr = (T_CTXT_ABORT_REQ*)global_res_buf;
        app_info->env_returns.response_code = abt_ptr->reason;
        LOGC_ERROR(CPM_FF_DDSERVICE,"Abort ID = 0x%x\n", abt_ptr->abort_id);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Local/Remote = 0x%x\n", abt_ptr->local);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Additional Detail = %s\n", abt_ptr->detail);
		return(r_code);
    }
    */
	return(CM_SUCCESS);
}

static void set_nma_vcr_entries(CM_NMA_VCR_LOAD* nma_vcr_load)
{
    int i;

    nma_vcr_load->nma_no_vcr_entries = get_device_admin_max_entries();

    for (i = 0; i < nma_vcr_load->nma_no_vcr_entries; i++)
    {
        if (0 != get_admin_dev_type_role(i))
        {
            nma_vcr_load->nma_vcr_entry[i].index =
                i + get_admin_dev_nm_first_index() + 2;
            nma_vcr_load->nma_vcr_entry[i].type_and_role =
                get_admin_dev_type_role(i);
            nma_vcr_load->nma_vcr_entry[i].loc_addr =
                get_admin_dev_loc_addr(i) ;
            nma_vcr_load->nma_vcr_entry[i].rem_addr =
                get_admin_dev_rem_addr(i);
            nma_vcr_load->nma_vcr_entry[i].sdap =
                get_admin_dev_sdap(i);
        }
    }
}

static int device_adminstration(ENV_INFO *env_info, USIGN16 cr_index,
                USIGN16 first_index, USIGN16 *next_index,
                T_DEVACC_STATE device_state)
{
    FBAP_NMA_REQ        fbap_nma_req;
    T_VAR_READ_CNF      *read_ptr;
    int                 r_code = CM_SUCCESS;
    USIGN8              node_address;
    USIGN8              vfd_nr;
	APP_INFO            *app_info;

    memset((char *)&fbap_nma_req, 0, sizeof(FBAP_NMA_REQ));
	app_info = (APP_INFO *)env_info->app_info;

    fbap_nma_req.cr_index = cr_index;
    fbap_nma_req.index = first_index;

    r_code = send_wait_receive_cmd(env_info, HNDL_FMS_READ_REQ,
                &fbap_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_FMS_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }

    node_address = get_device_admin_device_addr(cr_index);
    vfd_nr = get_device_admin_vfd_nr(cr_index);
    device_state = get_admin_dev_state();

    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;

        LOGC_ERROR(CPM_FF_DDSERVICE,"read error for device 0x%02x, vfd: %d state=%d\n",
                node_address,
                vfd_nr,
                device_state);

        return CS_FMS_ERR(app_info->env_returns.response_code);
    }
    else if (POS == get_service_result())
    {
        read_ptr = (T_VAR_READ_CNF*)global_res_buf;

        LOGC_TRACE(CPM_FF_DDSERVICE,"Recv: Confirmation: FMS: FMS_READ: Size=%d",
            get_size_of_desc_header() +
            sizeof(T_VAR_READ_CNF) +
            read_ptr->length);
        if ((node_address == 0) && ((vfd_nr == VFD_NR_MIB) || (vfd_nr == VFD_NR_FBAP)))
        {
            return CM_BAD_VCR;
        }

        switch(device_state)
        {
            case DEVACC_STATE_READ_SM_DIR:
                /* Get the SM Directory entries */
                r_code = device_admin_read_sm_dir(read_ptr, next_index, cr_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get SM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_NM_DIR:
                /* Get the SM Directory entries */
                r_code = device_admin_read_nm_dir(read_ptr, next_index, cr_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_VFD_REF_LIST:
                /* Get the VFD refernce list */
                r_code = device_admin_read_vfd_ref(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_VCR_CHARACT:
                /* Read the VCR Characteristics */
                r_code = device_admin_read_vcr_char(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_SCAN_VCR_LIST:
                /* Get the VCR List of the remote device */
                r_code = device_admin_scan_vcr_list(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_WRITE_VCR_ENTRY:
                /* Write the VCR entry in the remote device */
                //r_code = device_admin_wr_vcr_entry(read_ptr, next_index);
                //if(r_code != CM_SUCCESS)
                //{
                  //  LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                   // return r_code;
                //}
                break;

            case DEVACC_STATE_READ_AP_DIR:
                /* Get the Application Directory entries */
                r_code = device_admin_read_appl_dir(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_TB_HDR:
                /* Read transducer Block Header */
                r_code = device_admin_read_tb(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_FB_HDR:
                /* Read Function Block Header */
                r_code = device_admin_read_fb(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_RB_HDR:
                /* Read Function Block Header */
                r_code = device_admin_read_rb(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_MFG_ID:
                /* Get the manufacturer ID of the remote device */
                r_code = device_admin_read_manufac(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_DEV_TYPE:
                /* Get the Device Type of the remote device */
                r_code = device_admin_read_dev_type(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_DEV_REV:
                /* Get the Device Revision of the remote device */
                r_code = device_admin_read_dev_rev(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_DD_REV:
                /* Get the Device DD Revision of the remote device */
                r_code = device_admin_read_dd_rev(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_READ_MIN_CYCLE_T:
                /* Get the Minimum Read Cycle of the remote device */
                r_code = device_admin_read_cycle(read_ptr, next_index);
                if(r_code != CM_SUCCESS)
                {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"device_adminstration: Failed to get NM DIR entries\n");
                    return r_code;
                }
                break;

            case DEVACC_STATE_IDLE:
                return CM_SUCCESS;
                break;

            default:
                LOGC_ERROR(CPM_FF_DDSERVICE,"State not Handled %d\n", device_state);
                break;
        }
    }

    return r_code;
}

static int handle_get_od_pos(ENV_INFO* env_info, CM_NMA_VCR_LOAD* nma_vcr_load)
{
    int		            r_code;
	APP_INFO            *app_info;
    UINT8               node_address;
    int                 i;
    T_GET_OD_CNF        *get_od_cnf = NULL;
    T_PACKED_OBJECT_DESCR *lppkdobjdsc;
    T_USR_GET_OD_EVAL   obj_descr;
    USIGN8              vfd_nr;
    USIGN16             cr_index;
    USIGN16             first_index = 0;
    USIGN16             next_index = 0;
    T_DEVACC_STATE      device_state;
    USIGN8        device_addr;
    USIGN8        vfd_no;

	app_info = (APP_INFO *)env_info->app_info;

    get_od_cnf = (T_GET_OD_CNF *) global_res_buf;
    cr_index = get_service_comm_ref();

    if (POS == get_service_result())
    {
        LOGC_TRACE(CPM_FF_DDSERVICE,"Sent: Layer:FMS_USR - Service:FMS_GET_OD = %d",
            sizeof(T_GET_OD_CNF) +
            sizeof(T_PACKED_OBJECT_DESCR) +
            get_size_of_desc_header()
            );

        if (get_od_cnf->more_follows == FB_TRUE)
        {
            LOGC_DEBUG(CPM_FF_DDSERVICE," more_follows: TRUE\n");
        }
        else
        {
            LOGC_DEBUG(CPM_FF_DDSERVICE," more_follows: FALSE\n");
        }

        for (i = 0, lppkdobjdsc = (T_PACKED_OBJECT_DESCR *)(get_od_cnf + 1);
             i < get_od_cnf->no_of_od_descr;
             i++, lppkdobjdsc =
              (T_PACKED_OBJECT_DESCR *)((USIGN8 *)(lppkdobjdsc + 1) + obj_descr.length))
        {
            obj_descr.length = lppkdobjdsc->length;
            obj_descr.id.val[1] = ((USIGN8 *)(lppkdobjdsc + 1))[0];
            obj_descr.id.val[0] = ((USIGN8 *)(lppkdobjdsc + 1))[1];
            obj_descr.obj_type = ((USIGN8 *)(lppkdobjdsc + 1))[2];

            if (!(obj_descr.id.index))
            {
                node_address = get_device_admin_device_addr(cr_index);
                /* Device administration */
                if (node_address != 0)
                {
                    vfd_nr = get_device_admin_vfd_nr(cr_index);
                    r_code = devadm_save_od_header_info(
                        cr_index,
                        node_address,
                        vfd_nr,
                        lppkdobjdsc,
                        &first_index);
                    if(r_code != CM_SUCCESS)
                    {
                        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to cache OD header");
                        return r_code;
                    }

                    if (first_index != 0)
                    {
                        device_state = get_admin_dev_state();
                        while(device_state != DEVACC_STATE_IDLE)
                        {
                            r_code = device_adminstration(env_info, cr_index,
                                    first_index, &next_index, device_state);
                            if(r_code != CM_SUCCESS)
                            {
                                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to configure remote device \n");
                                return r_code;
                            }
                            first_index = next_index;
                            device_state = get_admin_dev_state();
                            if(DEVACC_STATE_WRITE_VCR_ENTRY == device_state)
                            {
                                set_nma_vcr_entries(nma_vcr_load);
                                device_state = DEVACC_STATE_IDLE;
                                set_admin_dev_state(device_state);
                            }
                        } /* End of While -- Loop till the completion of device administration */
                    } /* End of condition first_index != 0     */
                } /* End of condition check for valid node address */
            } /* End of condition for obj_descr.id.index */
        } /* End of For loop */

        /* if requested with start index, some descriptions may follow, request them */
        if (get_od_cnf->more_follows == FB_TRUE)
        {
            FBAP_NMA_REQ fbap_nma_req;
            fbap_nma_req.cr_index = get_service_comm_ref();
            fbap_nma_req.index = (USIGN16)(obj_descr.id.index + 1u);
            LOGC_DEBUG(CPM_FF_DDSERVICE,"requested with start index, some descriptions may follow\n");

            device_addr = get_device_admin_device_addr(fbap_nma_req.cr_index);

            if(device_addr)
            {
                vfd_no = get_device_admin_vfd_nr(fbap_nma_req.cr_index);
                if(VFD_NR_MIB == vfd_no)
                {
                    set_device_admin_state(DEVACC_STATE_GET_MGMT_OD_HDR);
                    set_admin_dev_state(DEVACC_STATE_GET_MGMT_OD_HDR);
                }
                else if(VFD_NR_FBAP == vfd_no)
                {
                    set_device_admin_state(DEVACC_STATE_GET_FBAP_OD_HDR);
                    set_admin_dev_state(DEVACC_STATE_GET_FBAP_OD_HDR);
                }
            }

            r_code = send_wait_receive_cmd(env_info, GET_OD_REQUEST,
                &fbap_nma_req, NULL);
            if(CM_SUCCESS != r_code) {
            	r_code = handle_cr_timeout(r_code, env_info, GET_OD_REQUEST, &fbap_nma_req,
            			NULL);
            	if(CM_SUCCESS != r_code)
            		return r_code;
            }

            if (POS == get_service_result())
            {
                r_code = handle_get_od_pos(env_info, nma_vcr_load);
            }
            else if(NEG == get_service_result())
            {
                T_ERROR *error = (T_ERROR *)global_res_buf;

                LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Error Class code = %d\n",
                    error->class_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Detail = %d\n",
                    error->add_detail);
                LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Description = %s\n",
                    error->add_description);
                app_info->env_returns.response_code = error->class_code;

                return CS_FMS_ERR(app_info->env_returns.response_code);
            }
        }// End of condition for more description follows
    } // End of Positive response handling

    return r_code;
}

/***********************************************************************
 *
 *	Name: softing_nm_get_od
 *
 *	ShortDesc:  Get OD on the CR
 *
 *	Description:
 *		The softing_nm_get_od function takes a CR number and
 *      gets Object Description on the CR
 *
 *	Inputs:
 *		cr_no - CR number to get OD
 *      nma_vcr_load - configuration settings to write into CR
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS on success and respective communication error code
 *      on failure
 *
 *	Author:
 *		Jayanth Mahadeva
 *      Shivaji Tikkireddy
 *
 **********************************************************************/

int softing_nm_get_od(ENV_INFO* env_info, CM_NMA_VCR_LOAD* nma_vcr_load)
{
	int		            r_code;
	APP_INFO            *app_info;
    FBAP_NMA_REQ        fbap_nma_req;
    USIGN8        device_addr;
    USIGN8        vfd_no;

	app_info = (APP_INFO *)env_info->app_info;

    fbap_nma_req.cr_index = nma_vcr_load->cr_index;
    fbap_nma_req.index = 0;

    device_addr = get_device_admin_device_addr(fbap_nma_req.cr_index);

    if(device_addr)
    {
        vfd_no = get_device_admin_vfd_nr(fbap_nma_req.cr_index);
        if(VFD_NR_MIB == vfd_no)
        {
            set_device_admin_state(DEVACC_STATE_GET_MGMT_OD_HDR);
            set_admin_dev_state(DEVACC_STATE_GET_MGMT_OD_HDR);
        }
        else if(VFD_NR_FBAP == vfd_no)
        {
            set_device_admin_state(DEVACC_STATE_GET_FBAP_OD_HDR);
            set_admin_dev_state(DEVACC_STATE_GET_FBAP_OD_HDR);
        }
    }

    r_code = send_wait_receive_cmd(env_info, GET_OD_REQUEST,
                &fbap_nma_req, NULL);

    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, GET_OD_REQUEST, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }
    if (POS == get_service_result())
    {
        r_code = handle_get_od_pos(env_info, nma_vcr_load);
    }
    else if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"Get OD: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;

        return CS_FMS_ERR(app_info->env_returns.response_code);
    }

    return r_code;
}

/***********************************************************************
 *
 *	Name: softing_nm_dev_boot
 *
 *	ShortDesc: Assigns cr value, index, sub-index, size and value to be
 *          written in Boot operational class of remote device.
 *
 *	Description:
 *          This function calls send_wait_receive_cmd() with
 *          HNDL_MGMT_WRITE_REQ service.
 *
 *	Inputs:
 *          NETWORK_HANDLE - Network Handle of the connected device.
 *          index - Index of the boot operational class of device.
 *          value - value to be written in  boot operational class of device.
 *
 *	Outputs:
 *      None
 *
 *	Returns:
 *		CM_SUCCESS on success and respective communication error code
 *      on failure
 *
 *	Author:Shivaji Tikkireddy
 *
 **********************************************************************/
int softing_nm_dev_boot(ENV_INFO* env_info,
    NETWORK_HANDLE nh,
    USIGN16 index,
    int value
)
{
	int		r_code;
    FBAP_NMA_REQ vfd_nma_req;
	APP_INFO *app_info;
    app_info = (APP_INFO *)env_info->app_info;

    vfd_nma_req.index = index;
    vfd_nma_req.sub_index = 0;
    vfd_nma_req.size = 1;
    vfd_nma_req.value[0] = (USIGN8 )value;

    r_code = get_nt_net_mib_cr(nh, &vfd_nma_req.cr_index);
    if (r_code != SUCCESS) {
        return(r_code) ;
    }
    r_code = send_wait_receive_cmd(env_info,
            HNDL_MGMT_WRITE_REQ, &vfd_nma_req, NULL);
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_WRITE_REQ, &vfd_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }
    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;
        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        app_info->env_returns.comm_err = CM_SUCCESS;
        app_info->env_returns.response_code = CM_SUCCESS;
        return(CM_SUCCESS);
    }
    return (CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name: softing_nm_read
 *
 *	ShortDesc: Reads NM objects of Host or Remote device.
 *
 *	Description: This function reads NM objects of Host or Remote device
 *          based on device_select and index of NM object
 *
 *	Inputs:
 *          NETWORK_HANDLE - Network Handle of the connected device.
 *          index - Index of NM object.
 *
 *	Outputs:
 *      None
 *
 *	Returns:
 *		CM_SUCCESS on success and respective communication error code
 *      on failure
 *	Author:
 *      Shivaji Tikkireddy
 *
 **********************************************************************/
int softing_nm_read(ENV_INFO* env_info, NETWORK_HANDLE nh, USIGN16 index,
                        int service, int device_select)
{
	int		r_code;
    FBAP_NMA_REQ fbap_nma_req;
	APP_INFO *app_info;

    app_info = (APP_INFO *)env_info->app_info;
    fbap_nma_req.index = index;

    /*
     * Set the Device administration parameters
     */
    set_device_admin_proc_con(NULL);
    set_device_admin_local_nma_read(TRUE);

    if(device_select == HOST)
    {
        // MGMT_READ
        r_code = send_wait_receive_cmd(env_info, HNDL_NMA_READ_REQ,
                &fbap_nma_req, NULL);
    }
    else    /*device_select = REMOTE*/
    {
        r_code = get_nt_net_mib_cr(nh, &fbap_nma_req.cr_index);
        if (r_code != SUCCESS) {
            return(r_code) ;
        }
        fbap_nma_req.sub_index = 0;
        // FMS_READ
        r_code = send_wait_receive_cmd(env_info, HNDL_MGMT_READ_REQ,
                &fbap_nma_req, NULL);
    }
    if(CM_SUCCESS != r_code) {
    	r_code = handle_cr_timeout(r_code, env_info, HNDL_MGMT_READ_REQ, &fbap_nma_req,
    			NULL);
    	if(CM_SUCCESS != r_code)
    		return r_code;
    }
    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_dev_boot: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;
        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        r_code = softing_nm_read_data (
             global_res_buf, nh, service, device_select);
        return(r_code) ;
    }

    return (CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name: softing_scan_network
 *
 *
 *	Description:
 *		The softing_scan_network function takes a Configuration File ID and
 *		scans the list of connected device list.
 *
 *	Inputs:
 *		scan_dev_table - the Table is copied with the scanned device list.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS.
 *      CM_COMM_ERR.
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_scan_network(ENV_INFO* env_info, SCAN_DEV_TBL *scan_dev_table)
{
	int		r_code;
	APP_INFO *app_info;

    app_info = (APP_INFO *)env_info->app_info;

    r_code = send_wait_receive_cmd(env_info, HNDL_SCAN_LIST_REQ, NULL, NULL);
	if(r_code != CM_SUCCESS) {
	   return(r_code);
    }
    if(NEG == get_service_result())
    {
        T_ERROR *error = (T_ERROR *)global_res_buf;

        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_scan_network: Error Class code = %d\n",
            error->class_code);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_scan_network: Additional Detail = %d\n",
            error->add_detail);
        LOGC_ERROR(CPM_FF_DDSERVICE,"softing_scan_network: Additional Description = %s\n",
            error->add_description);
        app_info->env_returns.response_code = error->class_code;
        return CS_FMS_ERR(error->class_code);
    }
    else if(POS == get_service_result())
    {
        r_code = softing_nm_read_live_list (
            global_res_buf, scan_dev_table);
        if(CM_SUCCESS != r_code)
        {
            app_info->env_returns.comm_err = CM_RESPONSE_CODE;
            app_info->env_returns.response_code = r_code;
        }
    }

    return r_code;
}

/***********************************************************************
 *
 *	Name: softing_nm_read_live_list
 *
 *	ShortDesc:  Read the live list from the read buffer from FF modem
 *
 *	Description:
 *		The softing_nm_read_live_list function read data buffer from
 *		FF modem and update the list of connected device count and
 *		their address into the data structure pointer scan_dev_table
 *
 *	Inputs:
 *		scan_dev_table - the Table is copied with the scanned device list.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Error code on particular error
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int  softing_nm_read_live_list (
    USIGN8 *p_con_data,
    SCAN_DEV_TBL *scan_dev_table )
{
    T_VAR_READ_CNF *p_read_con;
    USIGN8         *p_read_data;
    int             i, j;
	SCAN_TBL *temp = NULL;

    p_read_con  = (T_VAR_READ_CNF *) p_con_data;
    p_read_data = (USIGN8 *)(p_read_con + 1);

    if (p_read_con->length == 256/8)
    {
        /* read out all bits, skip first 16 because no valid node addresses */
        for (i = 16/8; i < 256/8; i++)
        {
            for (j = 0; j < 8; j++)
            {
                if (((USIGN8) i * 8 + j) == get_device_admin_host_addr())
                {
                    if ((p_read_data[i] & (0x80 >> j)) == 0)
                    {
                        LOGC_ERROR(CPM_FF_DDSERVICE,"is kicked from life list 0x%x\n", i * 8 + j);
                    }
                }
                else
                {
                    if((p_read_data[i] & (0x80 >> j)) != 0)
                    {
                        LOGC_TRACE(CPM_FF_DDSERVICE,"Device using node address: 0x%02x alive\n", i * 8 + j);
                        check_device_list((USIGN8) (i*8 + j), scan_dev_table, 1);
                        if((((i * 8) +j) >= 0x10)) /*&& (((i * 8) +j) <= 0xfb))*/
                        {
                            scan_dev_table->count++;
                            if(NULL == scan_dev_table->list)
                            {
                                scan_dev_table->list = (SCAN_TBL *) malloc(sizeof(SCAN_TBL));
                                scan_dev_table->list->station_address = (i*8 + j);
                                temp = scan_dev_table->list;
                                temp->next = NULL;
                            }
                            else
                            {
                                temp = scan_dev_table->list;
                                while(temp->next != NULL)
                                    temp = temp->next;

                                temp->next = (SCAN_TBL *) malloc(sizeof(SCAN_TBL));
                                temp->next->station_address = (i*8 + j);
                                temp->next->next = NULL;
                            }
                        }

                        else {
                            LOGC_INFO(CPM_FF_DDSERVICE,"Found Unconfigured or another host ");
                        }
                    }
                }
            } /* End of j For loop to check each bits in a byte */
        } /* End of i loop to check the 32 bytes */
    } /* End of if loop */

	return CM_SUCCESS;
}

/***********************************************************************
 *
 *	Name: softing_nm_read_data
 *
 *	ShortDesc: Saves the NM object.
 *
 *	Description: This function saves the NM object values in global
 *          variables.
 *
 *	Inputs:
 *
 *	Outputs:
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Error code on particular error
 *
 *	Author:Shivaji Tikkiredddy
 *
 **********************************************************************/
int  softing_nm_read_data (
    USIGN8 *p_con_data,
    NETWORK_HANDLE nh, int service, int device_select)
{
    T_VAR_READ_CNF          *p_read_con;
    USIGN8                  *p_read_data;
    NM_OBJ_STACK_CAP        *p_nm_stck_cap_data;
    NM_OBJ_VCR_LST_CHARAC   *p_nm_vcrl_charac;
    NM_OBJ_VCR_STATIC_ENRY  *p_nm_vcr_static_ent;
    NM_OBJ_VCR_DYN_ENTRY    *p_nm_vcr_dyn_entry;
    NM_OBJ_VCR_STAT_ENTRY   *p_nm_vcr_stat_entry;
    NM_OBJ_DLME_BSC_CHARAC  *p_nm_dlme_bsc_charc;
    NM_OBJ_DLME_BSC_INFO    *p_nm_dlme_bsc_info;
    NM_OBJ_DLME_STAT        *p_nm_dlme_stat;
    NM_OBJ_DLME_LM_INFO     *p_nm_dlme_lm_info;
    NM_OBJ_DLME_LM_LINKSETT *p_nm_dlme_lm_linksett;
    NM_OBJ_LS_CHARAC        *p_nm_ls_charac;

    int r_code = CM_SUCCESS;

    p_read_con  = (T_VAR_READ_CNF *) p_con_data;
    switch (service) {

        case STACK_CAP_REC:
            p_nm_stck_cap_data  =   (NM_OBJ_STACK_CAP *)(p_read_con + 1);
            p_nm_stck_cap_data->FasArTypeAndRoleSupp    =   convert_to_ltl_endian(p_nm_stck_cap_data->FasArTypeAndRoleSupp);
            p_nm_stck_cap_data->MaxDlsapAddressesSupp   =   convert_to_ltl_endian(p_nm_stck_cap_data->MaxDlsapAddressesSupp);
            p_nm_stck_cap_data->MaxDlcepAddressesSupp   =   convert_to_ltl_endian(p_nm_stck_cap_data->MaxDlcepAddressesSupp);
            p_nm_stck_cap_data->VersionOfNmSpecSupp     =   convert_to_ltl_endian(p_nm_stck_cap_data->VersionOfNmSpecSupp);

            if(device_select == HOST)
            {
                r_code = set_nmo_stack_cap_rec(p_nm_stck_cap_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_stack_cap_rec(nh, p_nm_stck_cap_data);
            }
            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case VCR_LST_CTRL_VAR:
            p_read_data = (USIGN8 *)(p_read_con + 1);

            if(device_select == HOST)
            {
                r_code = set_nmo_vcr_lst_ctrl(p_read_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_vcr_lst_ctrl(nh, p_read_data);
            }
            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case VCR_LST_CHARAC_REC:
            p_nm_vcrl_charac    =   (NM_OBJ_VCR_LST_CHARAC *)(p_read_con + 1);
            p_nm_vcrl_charac->Version                   =   convert_to_ltl_endian(p_nm_vcrl_charac->Version);
            p_nm_vcrl_charac->MaxEntries                =   convert_to_ltl_endian(p_nm_vcrl_charac->MaxEntries);
            p_nm_vcrl_charac->NumPermanentEntries       =
                                            convert_to_ltl_endian(p_nm_vcrl_charac->NumPermanentEntries);
            p_nm_vcrl_charac->NumCurrentlyConfigured    =
                                            convert_to_ltl_endian( p_nm_vcrl_charac->NumCurrentlyConfigured);
            p_nm_vcrl_charac->FirstUnconfiguredEntry    =
                                            convert_to_ltl_endian(p_nm_vcrl_charac->FirstUnconfiguredEntry);
            p_nm_vcrl_charac->NumOfStatisticsEntries    =
                                            convert_to_ltl_endian( p_nm_vcrl_charac->NumOfStatisticsEntries);

            if(device_select == HOST)
            {
                r_code = set_nmo_vcr_lst_charac(p_nm_vcrl_charac);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_vcr_lst_charac(nh, p_nm_vcrl_charac);
            }


            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case VCR_STATIC_ENTRY_REC:
            p_nm_vcr_static_ent =   (NM_OBJ_VCR_STATIC_ENRY *)(p_read_con + 1);
            p_nm_vcr_static_ent->FasDllLocalAddr                    =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllLocalAddr);
            p_nm_vcr_static_ent->FasDllConfiguredRemoteAddr         =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllConfiguredRemoteAddr);
            p_nm_vcr_static_ent->FasDllMaxConfirmDelayOnConnect     =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllMaxConfirmDelayOnConnect);
            p_nm_vcr_static_ent->FasDllMaxConfirmDelayOnData        =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllMaxConfirmDelayOnData);
            p_nm_vcr_static_ent->FasDllMaxDlsduSize                 =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllMaxDlsduSize);
            p_nm_vcr_static_ent->FasDllPublisherTimeWindowSize      =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllPublisherTimeWindowSize);
            p_nm_vcr_static_ent->FasDllPublisherSynchronizingDlcep  =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllPublisherSynchronizingDlcep);
            p_nm_vcr_static_ent->FasDllSubscriberTimeWindowSize     =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllSubscriberTimeWindowSize);
            p_nm_vcr_static_ent->FasDllSubscriberSynchronizingDlcep =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FasDllSubscriberSynchronizingDlcep);
            p_nm_vcr_static_ent->FmsVfdId   =
                                convert_to_ltl_endian(p_nm_vcr_static_ent->FmsVfdId);

            if(device_select == HOST)
            {
                r_code = set_nmo_vcr_lst_static(p_nm_vcr_static_ent);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_vcr_lst_static(nh, p_nm_vcr_static_ent);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case VCR_DYNC_ENTY_REC:
            p_nm_vcr_dyn_entry  =   (NM_OBJ_VCR_DYN_ENTRY *)(p_read_con + 1);
            p_nm_vcr_dyn_entry->FasDllActualRemoteAddress   =
                                convert_to_ltl_endian(p_nm_vcr_dyn_entry->FasDllActualRemoteAddress);


            if(device_select == HOST)
            {
                r_code = set_nmo_vcr_lst_dyn(p_nm_vcr_dyn_entry);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_vcr_lst_dyn(nh, p_nm_vcr_dyn_entry);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case VCR_STAT_ENTY_REC:
            p_nm_vcr_stat_entry =   (NM_OBJ_VCR_STAT_ENTRY *)(p_read_con + 1);
            p_nm_vcr_stat_entry->VcrStaticEntryOdIndex  =   convert_to_ltl_endian(p_nm_vcr_stat_entry->VcrStaticEntryOdIndex);
            p_nm_vcr_stat_entry->FasNumOfAbortsCtr      =   convert_to_ltl_endian(p_nm_vcr_stat_entry->FasNumOfAbortsCtr);
            p_nm_vcr_stat_entry->FasReasonLastAborted   =   convert_to_ltl_endian(p_nm_vcr_stat_entry->FasReasonLastAborted);
            p_nm_vcr_stat_entry->DllNumOfDtPdusSent     =   convert_to_ltl_endian(p_nm_vcr_stat_entry->DllNumOfDtPdusReceived);
            p_nm_vcr_stat_entry->DllNumOfDlDataTransferTimeoutFailures   =
                                    convert_to_ltl_endian(p_nm_vcr_stat_entry->DllNumOfDlDataTransferTimeoutFailures);
            p_nm_vcr_stat_entry->DllNumOfRcvrQuFullDlDataFailures    =
                                    convert_to_ltl_endian(p_nm_vcr_stat_entry->DllNumOfRcvrQuFullDlDataFailures);

            if(device_select == HOST)
            {
                r_code = set_nmo_vcr_stat_entry(p_nm_vcr_stat_entry);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_vcr_stat_entry(nh, p_nm_vcr_stat_entry);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_BASIC_CHARAC_REC:
            p_nm_dlme_bsc_charc  =   (NM_OBJ_DLME_BSC_CHARAC  *)(p_read_con + 1);
            p_nm_dlme_bsc_charc->DlDeviceConformance = convert_to_ltl_endian(p_nm_dlme_bsc_charc->DlDeviceConformance);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_bsc_charac(p_nm_dlme_bsc_charc);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_bsc_charac(nh, p_nm_dlme_bsc_charc);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_BASIC_INFO_REC:
            p_nm_dlme_bsc_info  =   (NM_OBJ_DLME_BSC_INFO  *)(p_read_con + 1);
            p_nm_dlme_bsc_info->SlotTime    =   convert_to_ltl_endian(p_nm_dlme_bsc_info->SlotTime);
            p_nm_dlme_bsc_info->ThisLink    =   convert_to_ltl_endian(p_nm_dlme_bsc_info->ThisLink);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_bsc_info(p_nm_dlme_bsc_info);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_bsc_info(nh, p_nm_dlme_bsc_info);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_BASIC_STAT_REC:
            p_nm_dlme_stat  =   (NM_OBJ_DLME_STAT *)(p_read_con + 1);
            p_nm_dlme_stat->NumOfDlpduTransmitted  =   convert_to_ltl_endian(p_nm_dlme_stat->NumOfDlpduTransmitted);
            p_nm_dlme_stat->NumOfGoodDlpduReceived =   convert_to_ltl_endian(p_nm_dlme_stat->NumOfGoodDlpduReceived);
            p_nm_dlme_stat->NumOfPartialReceivedDlpdu  =
                                    convert_to_ltl_endian(p_nm_dlme_stat->NumOfPartialReceivedDlpdu);
            p_nm_dlme_stat->NumOfFcsFailures           =
                                    convert_to_ltl_endian(p_nm_dlme_stat->NumOfFcsFailures);
            p_nm_dlme_stat->NumOfNodeTimeOffsetDiscontinuousChanges    =
                                    convert_to_ltl_endian(p_nm_dlme_stat->NumOfNodeTimeOffsetDiscontinuousChanges);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_bsc_stat(p_nm_dlme_stat);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_bsc_stat(nh, p_nm_dlme_stat);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_LM_CAP_VAR:
            p_read_data = (USIGN8 *)(p_read_con + 1);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_cap(p_read_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_cap(nh, p_read_data);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_LM_INFO_REC:
            p_nm_dlme_lm_info  =   (NM_OBJ_DLME_LM_INFO  *)(p_read_con + 1);
            p_nm_dlme_lm_info->DefMinTokenDelegTime  =  convert_to_ltl_endian(p_nm_dlme_lm_info->DefMinTokenDelegTime);
            p_nm_dlme_lm_info->DefTokenHoldTime  =  convert_to_ltl_endian(p_nm_dlme_lm_info->DefTokenHoldTime);
            p_nm_dlme_lm_info->TargetTokenRotTime  =   convert_to_ltl_endian(p_nm_dlme_lm_info->TargetTokenRotTime);
            p_nm_dlme_lm_info->LinkMaintTokHoldTime  =  convert_to_ltl_endian(p_nm_dlme_lm_info->LinkMaintTokHoldTime);
            p_nm_dlme_lm_info->TimeDistributionPeriod  =  convert_to_ltl_endian(p_nm_dlme_lm_info->TimeDistributionPeriod);
            p_nm_dlme_lm_info->MaximumInactivityToClaimLasDelay  = convert_to_ltl_endian(p_nm_dlme_lm_info->MaximumInactivityToClaimLasDelay);
            p_nm_dlme_lm_info->LasDatabaseStatusSpduDistributionPeriod  = convert_to_ltl_endian(p_nm_dlme_lm_info->LasDatabaseStatusSpduDistributionPeriod);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_info(p_nm_dlme_lm_info);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_info(nh, p_nm_dlme_lm_info);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case PRIMARY_LM_FLAG_VAR:
            p_read_data = (USIGN8 *)(p_read_con + 1);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_pri_flag(p_read_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_pri_flag(nh, p_read_data);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case LIVE_LST_ARR_VAR:
            p_read_data = (USIGN8 *)(p_read_con + 1);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_live_list_arr(p_read_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_live_list_arr(nh, p_read_data);
            }
            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case MAX_TOK_HOLD_TIME_ARR:
        break;

        case BOOT_OPERAT_FUNC:
            p_read_data = (USIGN8 *)(p_read_con + 1);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_boot_op(p_read_data);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_boot_op(nh, p_read_data);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case CURR_LINK_SETT_REC:
            p_nm_dlme_lm_linksett   =   (NM_OBJ_DLME_LM_LINKSETT *)(p_read_con + 1);
            p_nm_dlme_lm_linksett->SlotTime =   convert_to_ltl_endian(p_nm_dlme_lm_linksett->SlotTime);
            p_nm_dlme_lm_linksett->ThisLink =   convert_to_ltl_endian(p_nm_dlme_lm_linksett->ThisLink);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_curr_link_sett(p_nm_dlme_lm_linksett);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_curr_link_sett(nh, p_nm_dlme_lm_linksett);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case CONF_LINK_SETT_REC:
            p_nm_dlme_lm_linksett   =   (NM_OBJ_DLME_LM_LINKSETT *)(p_read_con + 1);
            p_nm_dlme_lm_linksett->SlotTime =   convert_to_ltl_endian(p_nm_dlme_lm_linksett->SlotTime);
            p_nm_dlme_lm_linksett->ThisLink =   convert_to_ltl_endian(p_nm_dlme_lm_linksett->ThisLink);

            if(device_select == HOST)
            {
                r_code = set_nmo_dlme_lm_config_link_sett(p_nm_dlme_lm_linksett);
            }
            else    /*device_select = REMOTE*/
            {
                r_code = set_nmo_dev_dlme_lm_config_link_sett(nh, p_nm_dlme_lm_linksett);
            }

            if (r_code != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: rs = %d\n", r_code);
                LOGC_ERROR(CPM_FF_DDSERVICE,"softing_nm_read_data: %s\n", dds_error_string(r_code));
                return r_code;
            }

        break;

        case DLME_LM_STAT_REC:
        break;

        case LST_VAL_REC:
        break;

        case LS_ACT_VAR:
        break;

        case LS_CHARAC_REC:
            p_nm_ls_charac  =   (NM_OBJ_LS_CHARAC *)(p_read_con + 1);
            p_nm_ls_charac->ActiveScheduleVersion    =   convert_to_ltl_endian(p_nm_ls_charac->ActiveScheduleVersion);
            p_nm_ls_charac->ActiveScheduleOdIndex    =   convert_to_ltl_endian(p_nm_ls_charac->ActiveScheduleOdIndex);
        break;

        case SCH_DES_REC:
        break;

        case SCH_DOMAIN:
        break;

        case PLME_BSC_CHARAC_REC:
        break;

        case CHAN_STATUS_VAR:
        break;

        case PLME_BASIC_INFO_REC:
        break;

        case MME_WIRE_STAT_REC:
        break;

        default:
            LOGC_ERROR(CPM_FF_DDSERVICE,"Invalid service\n");
            return(FAILURE);
    }

	return (CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name: softing_abort_communication
 *
 *	ShortDesc:  Abort the device communication
 *
 *	Description:
 *		Aborts the communication by issuing ABORT command on both MIB
 *		and FBAP VCR
 *
 *	Inputs:
 *		mib_cr - MIB communication reference number.
 *		fbap_cr - FBAP communication reference number.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Error code on particular error
 *
 *	Author:
 *		Jayanth Mahadeva
 *
 **********************************************************************/
int softing_abort_communication (
	ENV_INFO* env_info,
    unsigned short mib_cr,
	unsigned short fbap_cr
)
{
	int r_code;
	FBAP_NMA_REQ    fbap_nma_req = {0};

	fbap_nma_req.cr_index = mib_cr;
	r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ,
			&fbap_nma_req, NULL);
	if(CM_SUCCESS != r_code) {
		return((r_code)) ;
	}

	/*
	 * Delay added because, abort has no response and delay is given to make sure
	 * ABORT is executed in FBK2 board
	 */

	sleep(3);
	fbap_nma_req.cr_index = fbap_cr;
	r_code = send_wait_receive_cmd(env_info, HNDL_ABORT_CR_REQ,
			&fbap_nma_req, NULL);
	if(CM_SUCCESS != r_code) {
		return((r_code)) ;
	}
	/*
	 * Delay added because, abort has no response and delay is given to make sure
	 * ABORT is executed in FBK2 board
	 */

	sleep(3);
}
