/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)panic.h	30.3  30  14 Nov 1996
 */
#ifndef _PANIC_H
#define _PANIC_H

#include "std.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int (*dds_panic)(char *msg);

/*lint +fva variable number of arguments */
#if !defined(NO_P4_DOS)
#if defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4)

extern void panic P((char *,...));

#else // defined(__linux__)
extern void panic P((const char *,...));

#endif /* defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4) */

#if defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4)

extern void panic_but_retrun P((char *,...));

#else // defined(__linux__)
extern void panic_but_retrun P((const char *,...));

#endif /* defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4) */



#endif /* !defined(NO_P4_DOS) */
/*lint -fva end of varying arguments */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _PANIC_H */
