 /*
  *	@(#)display.h	30.3  30  14 Nov 1996
  */

/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#define FILE_PATH_LEN 				256 	/* Length of release dir */
#define MAX_DICT_TABLE_SIZE 		20000	/* Dictionary table size */
#define MAX_NAME_LENGTH 			60
#define	DDI_COMMAND					2
#define	MAX_ID_TABLE_SIZE			10000	/* Size of the id table */
#define	MAX_LINE_LEN				200		/* Max length of line */
#define	MAX_LABEL					32		/* Max length of label */
#define	MAX_ENUM					32		/* Max length of enum strg */
#define	MAX_UNIT					32		/* Max length const unit */
#define	MAX_VAR_FMT					32		/* Max edit/display format */ 
#define	BLOCKTAGSIZE				100		/* Max tag length of block */ 
#define	MAXBLOCKS					10		/* Max number of blocks */
#define NTOK_PATH_ENV_NAME          "TOK_RELEASE_DIR"

#define	QUIT						0		/* Exit application program */
#define	CLASS						1		/* Select variable class */
#define	READ						2		/* Read in the new value */
#define	WRITTEN						3		/* Write out the value */

#define	ST_STATIC					0		/* Macros for status */
#define	ST_READ						1	
#define	ST_WRITE					2		
#define	ST_DYNAMIC					4
#define	ST_LOCAL					8

/*
 * Macro for checking variable mathematic type.
 */

#define	ISMATH_TYPE(type) \
					(((type == DDS_DOUBLE) || (type == DDS_FLOAT) || \
       			     (type == DDS_INTEGER) || (type == DDS_UNSIGNED))\
					   ? TRUE : FALSE)

/*
 * Macro for all variable classes.
 */

#define ALL_CLASS	(DIAGNOSTIC_CLASS | DYNAMIC_CLASS |\
					SERVICE_CLASS | CORRECTION_CLASS |\
					COMPUTATION_CLASS | INPUT_BLOCK_CLASS |\
					ANALOG_OUTPUT_CLASS | \
					LOCAL_DISPLAY_CLASS | FREQUENCY_CLASS |\
					DISCRETE_CLASS | DEVICE_CLASS |\
					LOCAL_CLASS | INPUT_CLASS |\
					OUTPUT_CLASS | CONTAINED_CLASS |\
					OPERATE_CLASS | ALARM_CLASS	| TUNE_CLASS)

/*
 * Entry in dictionary table.
 */

typedef struct {
	ulong           ref;
	unsigned short  len;
	char           *str;
} DICT_TABLE_ENTRY;

/*
 * Entry info in the id-table.
 */

typedef struct {
	ITEM_TYPE       item_type;
	ITEM_ID         item_id;
	OBJECT_INDEX    object_index;
	char            item_name[MAX_NAME_LENGTH];
} ID_TABLE_ENTRY;


/*
 * Header of id tables.
 */

typedef struct {
	DD_DEVICE_ID    id;	
	unsigned int    num_entries;
	ID_TABLE_ENTRY  table[MAX_ID_TABLE_SIZE];
} ID_TABLE_HEADER;


/*
 * Info about application entry for use in DDS calls.
 */

typedef struct {
	ITEM_TYPE			item_type;		/* Item type of entry */
	DDI_BLOCK_SPECIFIER	block_spec;		/* Block spec. of entry */
	DDI_ITEM_SPECIFIER	item_spec;		/* Item spec. of entry */
	void 				*flat;			/* Flat struct of entry */
	ulong	 			mask;			/* Using in DDS calls  */
} GET_ITEM_ARGS ;


/*
 * Application info for a value entry.
 */

typedef struct {
	GET_ITEM_ARGS	label_args;
	GET_ITEM_ARGS	typ_siz_args;		/* Info for using in DDS */
	OP_REF			op_ref;				/* Operational reference */
	EVAL_VAR_VALUE	var_value;			/* Appl. entry value */
	int				status;				/* Appl. entry status */
	int				unit_index;
} DISP_VALUE_ELEM ;


/*
 * A list of infos about application values.
 */

typedef struct {
	int				count; 	
	int				limit;
	DISP_VALUE_ELEM	*list;
} DISP_VALUE ;


/*
 * Block entry.
 */

typedef	struct{
	char			tag[BLOCKTAGSIZE];		/* Block tag */
	BLOCK_HANDLE	block_handle;			/* Block handle */
	DISP_VALUE		var_struct;				/* Block appl. values */
} BLOCK_STRUCT_ELEM ;


/*
 * A list of blocks.
 */

typedef	struct {
	int					count;				/* Number of blocks */
	BLOCK_STRUCT_ELEM	list[MAXBLOCKS]; 	/* List of blocks */
} BLOCK_STRUCT ;

/*
 * Function prototypes
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int do_reads P((DISP_VALUE *));
extern int do_writes P((DISP_VALUE *));
extern int get_message P((int *, int * , BLOCK_STRUCT *));
extern int get_value P((const DISP_VALUE *, int));
extern int show_values P((BLOCK_HANDLE, DISP_VALUE *, ulong, int, char *));
extern void apply_scale_factor P((EVAL_VAR_VALUE *, EXPR *, int));
extern int allocate_flat_item P((void **, ITEM_TYPE, unsigned long, unsigned int));
extern void select_class P((ulong *, int *, char *));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
