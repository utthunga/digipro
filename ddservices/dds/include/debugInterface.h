#pragma once
#include "int_meth.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct brkpnt{
        int brk_type;
#define BRK_TYPE_PERM   1
#define BRK_TYPE_SINGLE 2
#define BRK_TYPE_STEP   3
	unsigned int line;
	int enabled;
	void *decookie;
} brkpnt_t;

/*
<<<<<<< debugInterface.h
typedef void (*debugCB_t)( SCOPE *vals, void *decookie);

extern debugCB_t        globalDebugCallback;
extern brkpnt_t *globalBrkPnts;
extern int                      numBrkPnts;
=======
typedef struct pvalue{
	int  value_type;
	int  int_value;
	char char_value;
	void *avalue;
} pvalue_t;
*/

#define DEBUG_CONTINUE 0
#define DEBUG_ABORT 1

typedef int (*debugCB_t)( SCOPE *vals, void *decookie,int line);

extern debugCB_t	globalDebugCallback;
extern brkpnt_t		*globalBrkPnts;
extern int			numBrkPnts;
//>>>>>>> 1.3

void    SetBP( brkpnt_t *bpts, int numBpts, debugCB_t callback);

#ifdef __cplusplus
}
#endif /* __cplusplus */