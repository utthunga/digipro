/**
 *	@(#)dds_upcl.h	30.3  30  14 Nov 1996
 */
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	These are the function upcalls that the application must provide!
 */

#ifndef DDS_UPCL_H
#define DDS_UPCL_H

#include "cm_lib.h"
#include "attrs.h"
#include "evl_lib.h"
#include "ddldefs.h"
#include "ddsbltin.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	extern int (*display_upcall) P((ENV_INFO *env_info, char *buffer));

	extern int (*display_continuous_upcall) P((ENV_INFO *env_info, char *prompt, 
					ITEM_ID *var_ids, SUBINDEX *var_subindices));

	extern int (*display_continuous_upcall2) P((ENV_INFO *env_info, char *prompt, 
					ITEM_ID *var_ids, SUBINDEX *var_subindices, 
                    BLOCK_HANDLE *block_handles));

	extern int (*display_get_upcall) P((ENV_INFO *env_info, char *buffer,
					P_REF *pc_p_ref, EVAL_VAR_VALUE *var_value));

	extern int (*display_menu_upcall) P((ENV_INFO *env_info, char *buffer,
					SELECT_LIST *select_list, int *selection));

    /* For the MenuDisplay() builtin */
	extern int (*display_menu_upcall2) P((ENV_INFO *env_info,
					SELECT_LIST *select_list, int *selection, ITEM_ID menuid));

	extern int (*display_ack_upcall) P((ENV_INFO *env_info, char *buffer));
    extern long (*list_delete_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index));
    extern long (*list_delete_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index,
        ITEM_ID embedded_id, int embedded_index));
    extern long (*list_insert_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index, 
                                         ITEM_ID element));
    extern long (*list_insert_element2_upcall) P((ENV_INFO *env_info, ITEM_ID list_id, int index, 
            ITEM_ID embedded_list_id, int embedded_index, ITEM_ID embedded_element));
    extern long (*list_read_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index, 
                                         ITEM_ID subelement, ITEM_ID subsubelement,
                                         EVAL_VAR_VALUE *out_elem_data));
    extern long (*list_read_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index, 
                                         ITEM_ID embedded_list_id, int embedded_list_index,
                                         ITEM_ID subelement, ITEM_ID subsubelement,
                                         EVAL_VAR_VALUE *out_elem_data));
    extern long (*list_count_element_upcall) P((ENV_INFO *br, ITEM_ID list_id, unsigned long *count));
    extern long (*list_count_element2_upcall) P((ENV_INFO *br, ITEM_ID list_id, int index, 
                                                 ITEM_ID embedded_list_id, unsigned long *count));
    extern long (*plot_create_upcall) P((ENV_INFO *br, ITEM_ID plot));
    extern long (*plot_destroy_upcall) P((ENV_INFO *br, long plot_handle));
    extern long (*plot_display_upcall) P((ENV_INFO *br, long plot_handle));
    extern long (*plot_edit_upcall) P((ENV_INFO *br, long plot_handle, unsigned long waveform_id));
    extern long (*method_start_upcall) P((ENV_INFO *br, ITEM_ID meth_item_id));
    extern long (*method_complete_upcall) P((ENV_INFO *br, ITEM_ID meth_item_id, int rs));
    extern long (*method_error_upcall) P((ENV_INFO *br, char *errorStr));

    /* DDS 5.1 upcalls */
    extern int (*app_get_block_instance_by_index) P((ENV_INFO *env_info, 
                                                    unsigned long objectIndex,
                                                    unsigned long *out_instance));
    extern int (*app_get_block_instance_by_tag) P((ENV_INFO *env_info, 
                                                    ITEM_ID block_id,
                                                    char *blockTag,
                                                    unsigned long *out_instance));
    extern int (*app_get_block_instance_count) P((ENV_INFO *env_info, 
                                                 ITEM_ID block_id,
                                                 unsigned long *out_count));
    extern int (*app_get_selector_upcall) P((ENV_INFO *br, ITEM_ID id, ITEM_ID selector, 
				                            ITEM_ID additional_id /* currently unused */, 
                                            double *sel_value));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /*DDS_UPCL_H*/
