/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)tst_dds.h	30.4	27 Nov 1996
 */

#ifndef TST_DDS_H
#define TST_DDS_H

#include "fch_lib.h"
#include "ddi_lib.h"
#include "cm_lib.h"
#include "tst_cmn.h"
#include "host.h"

#define MAX_NUM_TEST_DEVICES        100
#define DO_ALL_DEVICES              -5
#define MAX_NUM_NEW_VALUES          30
#define MAX_ITEM_NAME_LENGTH		50
#define MAX_FUNC_NAME_LENGTH		40

#define	DT_HANDLE	BLOCK_HANDLE

#define	MAXTESTBLKS		100
#define	MAX_DEV_TYPES	10
#define	OPENBLOCKCMD	10
#define	MAX_SP_PTRS		1024

#define MAX_BLK_TAG_SIZE    64

/*
 * Language codes
 */

#define ENGLISH		1
#define FRENCH		33
#define SPANISH		34
#define ITALIAN		39
#define GERMAN		49

#define ISO_to_PC	0
#define PC_to_ISO	1

/*
 * Number of arguments for script directives
 */

#define	FETCH_ITEM_ARG_CNT			3
#define	FETCH_DIR_ARG_CNT			2
#define	ROD_FETCH_ITEM_ARG_CNT		4
#define	ROD_FETCH_DIR_ARG_CNT		3

#define	EVAL_ITEM_ARG_CNT			3
#define	EVAL_DIR_ARG_CNT			2
#define	CHANGE_VAR_VALUE_ARG_CNT	3
 
#define EXEC_METHOD_ARG_CNT         3
 
#define TRANSLATE_ARG_CNT			3

#define	BDIR_BHANDLE_ARG_CNT		2
#define	BDIR_DDHANDLE_DDREF_ARG_CNT	2
#define	BDIR_DTHANDLE_BNAME_ARG_CNT	2
#define	DDIR_DHANDLE_ARG_CNT		2
#define	DDIR_DDHANDLE_DDREF_ARG_CNT	2
#define	DDIR_DTHANDLE_ARG_CNT		2
#define	BTAB_DDHANDLE_DDREF_ARG_CNT	2
#define	DTAB_DDHANDLE_DDREF_ARG_CNT	2

#define	ITEM_BLOCK_ARG_CNT			3
#define	ITEM_BLK_NAME_ARG_CNT		4
#define	ITEM_BLK_PARM_ARG_CNT		4
#define	ITEM_BLK_OP_IDX_ARG_CNT		4
#define	ITEM_BLK_DDID_ARG_CNT		4
#define	ITEM_BLK_CHAR_ARG_CNT		4
#define	ITEM_BLK_LIST_ARG_CNT		4
#define	ITEM_BLK_NAME_SUB_ARG_CNT	5
#define	ITEM_BLK_PARM_SUB_ARG_CNT	5
#define	ITEM_BLK_DDID_SUB_ARG_CNT	5
#define	ITEM_BLK_OP_IDX_SUB_ARG_CNT	5

#define	OP_IDX_NAME_ARG_CNT			3
#define	OP_IDX_PARM_ARG_CNT			3
#define	OP_IDX_DDID_ARG_CNT			3
#define	OP_IDX_CHAR_ARG_CNT			3
#define	OP_IDX_LIST_ARG_CNT			3
#define	OP_IDX_NAME_SUB_ARG_CNT		4
#define	OP_IDX_PARM_SUB_ARG_CNT		4
#define	OP_IDX_DDID_SUB_ARG_CNT		4

#define	TYPE_NAME_ARG_CNT			3
#define	TYPE_PARM_ARG_CNT			3
#define	TYPE_OP_IDX_ARG_CNT			3
#define	TYPE_DDID_ARG_CNT			3
#define	TYPE_NAME_SUB_ARG_CNT		4
#define	TYPE_PARM_SUB_ARG_CNT		4
#define	TYPE_OP_IDX_SUB_ARG_CNT		4
#define	TYPE_DDID_SUB_ARG_CNT		4

#define	RIGHT_NAME_ARG_CNT			3
#define	RIGHT_PARM_ARG_CNT			3
#define	RIGHT_OP_IDX_ARG_CNT		3
#define	RIGHT_DDID_ARG_CNT			3
#define	RIGHT_NAME_SUB_ARG_CNT		4
#define	RIGHT_PARM_SUB_ARG_CNT		4
#define	RIGHT_OP_IDX_SUB_ARG_CNT	4
#define	RIGHT_DDID_SUB_ARG_CNT		4
#define	RIGHT_CHAR_ARG_CNT			2
#define	RIGHT_CHAR_SUB_ARG_CNT		3

#define	UNIT_NAME_ARG_CNT			3
#define	UNIT_PARM_ARG_CNT			3
#define	UNIT_OP_IDX_ARG_CNT			3
#define	UNIT_DDID_ARG_CNT			3
#define	UNIT_NAME_SUB_ARG_CNT		4
#define	UNIT_PARM_SUB_ARG_CNT		4
#define	UNIT_OP_IDX_SUB_ARG_CNT		4
#define	UNIT_DDID_SUB_ARG_CNT		4
#define	UNIT_CHAR_ARG_CNT			2
#define	UNIT_CHAR_SUB_ARG_CNT		3

#define	WAO_NAME_ARG_CNT			3
#define	WAO_PARM_ARG_CNT			3
#define	WAO_OP_IDX_ARG_CNT			3
#define	WAO_DDID_ARG_CNT			3
#define	WAO_NAME_SUB_ARG_CNT		4
#define	WAO_PARM_SUB_ARG_CNT		4
#define	WAO_OP_IDX_SUB_ARG_CNT		4
#define	WAO_DDID_SUB_ARG_CNT		4
#define	WAO_CHAR_ARG_CNT			2
#define	WAO_CHAR_SUB_ARG_CNT		3

#define	CMDS_NAME_ARG_CNT			4
#define	CMDS_PARM_ARG_CNT			4
#define	CMDS_OP_IDX_ARG_CNT			4
#define	CMDS_DDID_ARG_CNT			4
#define	CMDS_NAME_SUB_ARG_CNT		5
#define	CMDS_PARM_SUB_ARG_CNT		5
#define	CMDS_OP_IDX_SUB_ARG_CNT		5
#define	CMDS_DDID_SUB_ARG_CNT		5
#define	CMDS_CHAR_SUB_ARG_CNT		4

#define GET_CMD_ID_ARG_CNT			3

#define	ITEM_ID_NAME_ARG_CNT		3
#define	ITEM_ID_LIST_NAME_ARG_CNT	4
#define	ITEM_ID_CHAR_ARG_CNT		2

#define	SUBINDEX_ARG_CNT			4

#define	CHK_ENUM_VAR_VAL_ARG_CNT	5

#define	FAIL_COMMAND_ARG_CNT		3

#define	PIO_OPEN_ARG_CNT			2
#define	PIO_CLOSE_ARG_CNT			2
#define	PIO_GET_ID_SUB_ARG_CNT		5
#define	PIO_GET_PO_SUB_ARG_CNT		5
#define	PIO_PUT_ID_SUB_ARG_CNT		6
#define	PIO_PUT_PO_SUB_ARG_CNT		6
#define	PIO_READ_ID_SUB_ARG_CNT		5
#define	PIO_READ_PO_SUB_ARG_CNT		5
#define PIO_READ_ALL_ARG_CNT		3
#define	PIO_WRITE_ID_SUB_ARG_CNT	6
#define	PIO_WRITE_PO_SUB_ARG_CNT	6
#define PIO_WRITE_ALL_ARG_CNT		3

#define	PSRV_DSCRD_ID_SUB_ARG_CNT	5
#define	PSRV_DSCRD_PO_SUB_ARG_CNT	5
#define	PSRV_DSCRD_ALL_ARG_CNT		3
#define	PSRV_MOD_ID_SUB_ARG_CNT		5
#define	PSRV_MOD_PO_SUB_ARG_CNT		5
#define	PSRV_ANY_MOD_ARG_CNT		3
#define	PSRV_CLR_ID_SUB_ARG_CNT		5
#define	PSRV_CLR_PO_SUB_ARG_CNT		5
#define	PSRV_SET_ID_SUB_ARG_CNT		5
#define	PSRV_SET_PO_SUB_ARG_CNT		5
#define	PSRV_TST_ID_SUB_ARG_CNT		5
#define	PSRV_TST_PO_SUB_ARG_CNT		5

#define	CRG_READ_ID_SUB_ARG_CNT		5
#define	CRG_READ_PO_SUB_ARG_CNT		5
#define	CRG_WRITE_ID_SUB_ARG_CNT	5
#define	CRG_WRITE_PO_SUB_ARG_CNT	5
#define	CRG_LOAD_ID_SUB_ARG_CNT		5
#define	CRG_LOAD_PO_SUB_ARG_CNT		5
#define	CRG_UNLOAD_ID_SUB_ARG_CNT	5
#define	CRG_UNLOAD_PO_SUB_ARG_CNT	5

#define	CRS_IDENT_SHORT_ARG_CNT		2
#define	CRS_IDENT_LONG_ARG_CNT		4
#define	CRS_ASSIGN_TAG_ARG_CNT		3
#define	CRS_READ_TAG_ARG_CNT		2
#define	CRS_SEND_CMD_ARG_CNT		4
#define	CRS_SET_POLL_ADDR_ARG_CNT	3

#define RCSIM_SET_GENERAL_ARG_CNT	3
#define RCSIM_SET_NETWORK_ARG_CNT	5
#define RCSIM_SET_DEVICE_ARG_CNT	6


/*
 * General types of script directives
 */

#define FETCH_COMMAND	0
#define EVAL_COMMAND	1
#define DDI_COMMAND		2

/*
 * Types of fetch script directive for getting scratch pad size
 */

#define SPAD_FUNC		0
#define FETCH_FUNC		1
#define START_BYTE		2


/*
 * Console User input directive
 */
typedef enum _CONSOLE_CMD {
    INVALID_CMD = 0,
    START_SCAN_CMD,
    STOP_SCAN_CMD,
    INIT_DEV_CMD,
    CONNECT_DEV_CMD,
    DISCONNECT_DEV_CMD,
    OPENCMD,
    CLOSECMD,			
    PARAMETERSCMD,
    READPARMCMD,			
    WRITEPARMCMD,		
    READMENUCMD,			
    READMETHODCMD,		
    READ_UNIT_CMD,			
    READ_REFRESH_CMD,		
    READ_WAO_CMD,			
    READCOLLECTIONCMD,		
    READITEMARRCMD,		
    EXECMETHODCMD,		
    LANGCMD,
    IDENTIFY_CMD,		
    CLR_NODE_ADDR_CMD,
    CLR_PD_TAG_CMD,
    SET_PD_TAG_CMD,
    SET_NODE_ADDR_CMD,
    WR_NMA_LOCAL_VCR_CMD,
    FMS_INITIATE_CMD,
    WR_FMS_LOCAL_VCR_CMD,
    GET_OD_CMD,
    LOAD_VCR_CMD,
    READ_NMA_OBJECTS,
    LINK_MASTER_CAP,
    CLOSE_COMM_MNGR,
    FIRMWARE_DOWNLOAD_CMD,
    MENU_OFF_CMD,
    MENU_ON_CMD,
    EXITCMD,
}CONSOLE_CMD;

#define    START_SCAN_STATE            (1 << 0)                    
#define    STOP_SCAN_STATE             (1 << 1)                     
#define    INIT_DEV_STATE              (1 << 2)                      
#define    CONNECT_DEV_STATE           (1 << 3)                  
#define    DISCONNECT_DEV_STATE        (1 << 4)                     
#define    OPEN_STATE                  (1<< 5) 
#define    CLOSE_STATE                 (1 << 6)                                  
#define    PARAMETERS_STATE            (1 << 7)                               
#define    READPARM_STATE              (1 << 8)
#define    WRITEPARM_STATE             (1 << 9)                      
#define    READMENU_STATE              (1 << 10)                       
#define    READMETHOD_STATE            (1 << 11)                     
#define    READ_UNIT_STATE             (1 << 12)                 
#define    READ_REFRESH_STATE          (1 << 13)              
#define    READ_WAO_STATE              (1 << 14)                  
#define    READCOLLECTION_STATE        (1 << 15)                  
#define    READITEMARR_STATE           (1 << 16)                    
#define    EXECMETHOD_STATE            (1 << 17)                     
#define    LANG_STATE                  (1 << 18)                                        
#define    IDENTIFY_STATE             (1 << 19)                              
#define    CLR_NODE_ADDR_STATE        (1 << 20)                    
#define    CLR_PD_TAG_STATE           (1 << 21)                          
#define    SET_PD_TAG_STATE           (1 << 22)                          
#define    SET_NODE_ADDR_STATE        (1 << 23)                    
#define    WR_NMA_LOCAL_VCR_STATE     (1 << 24)              
#define    FMS_INITIATE_STATE         (1 << 25)                      
#define    WR_FMS_LOCAL_VCR_STATE     (1 << 26)              
#define    GET_OD_STATE               (1 << 27)                                  
#define    READ_NMA_OBJECTS_STATE         (1 << 28)                      
#define    LINK_MASTER_CAP_STATE          (1 << 29)                        
#define    CLOSE_COMM_MNGR_STATE          (1 << 30)                        
#define    FIRMWARE_DOWNLOAD_STATE    (1 << 31)            


typedef struct {
	unsigned int	count;
	unsigned char *	list[MAX_SP_PTRS];
}				SP_PTR_TABLE;

#define	MAXCMD 233

typedef struct {
	char       *cmdstr;			/* Command string found in the test input file */
	int        (*cmdfunc) ();	/* Function executed when the command is found */
}               CMD;

typedef struct {
	char 			func_name[MAX_FUNC_NAME_LENGTH];
	int             return_status;	/* status to return when fail */
	int				iteration;		/* number of iterations to go before fail */
	int				curr_count;		/* number of times this function has been hit */
	int				turned_on;		/* flag that says whether to count or not */
}               FAIL_FUNC;

#define	MAXRTNSTAT 100

typedef struct {
	char 			rs_name[MAX_FUNC_NAME_LENGTH];
	int             rs_number;
}               RETURN_STATUS;

/*
 * Extern declarations
 */

extern DEVICE_HANDLE global_dhandle;
extern DEVICE_TYPE_HANDLE global_dthandle;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * Routines that handle command directives
 */


extern int ddi_get_block P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_op_index P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_char_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_parmlist_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_op_index_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_get_item_parm_offset_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_parm_offset_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_opix_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_op_idx P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_parm_offset_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_op_idx_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_unit_char_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_op_idx P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_parm_offset_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_op_idx_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_wao_char_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_op_idx P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_parm_off_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_op_idx_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_update_char_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_cmds_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_cmds_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_cmds_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_cmds_op_idx P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_parm_offset P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_ddid P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_op_idx P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_name_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_parm_offset_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_ddid_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_type_op_idx_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_vtype P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_item_id_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_item_id_list_name P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_item_id_list_id P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_item_id_char P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_subindex P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_conv_check_enum_var_value P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_block_dir_bhandle P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_block_dir_ddhandle_ddref P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_block_dir_dthandle_bname P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_device_dir_dhandle P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_device_dir_ddhandle_ddref P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_device_dir_dthandle P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_block_dir_all P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_device_dir_all P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int item_eval P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int dir_eval P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int item_fetch P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int dir_fetch P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int item_rod_fetch P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int dir_rod_fetch P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int var_value_change P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

extern int pc_read_parm_val_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_read_all_params P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_write_all_parm_vals P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_discard_put_parm_val_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_discard_put_parm_val_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_discard_all_put_parms P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_parm_val_mod_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_parm_val_mod_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_any_parm_val_mod P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_clear_parm_flag_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_clear_parm_flag_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_set_parm_flag_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_set_parm_flag_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_test_parm_flag_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_test_parm_flag_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_get_parm_val_with_bad_type P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_read_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_read_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_write_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_write_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_load_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_load_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_unload_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_param_unload_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_identify_with_short_addr P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_identify_with_long_addr P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_read_tag_from_device P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int cr_send_cmd_to_device P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

extern int rcsim_set_general P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int rcsim_set_network P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int rcsim_set_device  P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int tstrc_initialize  P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int tstrc_free P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int rcsim_dump P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));


extern int ddi_conv_param_info P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));



/*
 * Support test routines
 */

extern void init_test_blocks P(());
extern int add_test_block P((char *, BLOCK_HANDLE));
extern int remove_test_block P((char *));
extern int item_id_to_ob_idx P((ITEM_ID, OBJECT_INDEX *));
extern int item_id_to_type P((ITEM_ID, ITEM_TYPE *));
extern int ob_idx_to_name P((OBJECT_INDEX, char **));
extern void init_sp_ptr_table P(());
extern int save_sp_ptr P((unsigned char *));
extern int fill_id_table P((ITEM_TBL *, DD_DEVICE_ID *, ID_TABLE_HEADER *));
extern int get_string_tbl_handle P((BLOCK_HANDLE, STRING_TBL **));
extern int set_eval_var_value P((unsigned short, unsigned short, char *, EVAL_VAR_VALUE *));
extern int get_dd_ref P((DDI_ITEM_SPECIFIER *, BLOCK_HANDLE, DD_REFERENCE *, BLK_TBL_ELEM *));
extern int allocate_flat_item P((void **, ITEM_TYPE, unsigned long, unsigned int));
extern int init_fail_func P((char *, char *, int));

/*
 * Fetch test routines
 */

extern int item_rfch_incr P((DEVICE_HANDLE, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, unsigned long, void *, ITEM_TYPE));
extern int item_rfch_fchsize P((DEVICE_HANDLE, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, void *, ITEM_TYPE));
extern int item_rfch_spadsize P((DEVICE_HANDLE, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, void *, ITEM_TYPE));
extern int ddir_rfch_incr P((ROD_HANDLE, SCRATCH_PAD *, unsigned long, unsigned long, BIN_DEVICE_DIR *));
extern int ddir_rfch_fchsize P((ROD_HANDLE, SCRATCH_PAD *, unsigned long, BIN_DEVICE_DIR *));
extern int ddir_rfch_spadsize P((ROD_HANDLE, SCRATCH_PAD *, unsigned long, BIN_DEVICE_DIR *));
extern int bdir_rfch_incr P((ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, unsigned long, BIN_BLOCK_DIR *));
extern int bdir_rfch_fchsize P((ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, BIN_BLOCK_DIR *));
extern int bdir_rfch_spadsize P((ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long, BIN_BLOCK_DIR *));
extern int item_gfch_spadsize P((DEVICE_HANDLE, ITEM_ID, SCRATCH_PAD *, unsigned long, void *, ITEM_TYPE));
extern int ddir_gfch_spadsize P((DEVICE_HANDLE, SCRATCH_PAD *, unsigned long, BIN_DEVICE_DIR *));
extern int bdir_gfch_spadsize P((DEVICE_HANDLE, ITEM_ID, SCRATCH_PAD *, unsigned long, BIN_BLOCK_DIR *));

/*
 * Softing Network SM directory routines
 */
#ifdef DDS_TEST_MODE

extern int open_block P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int close_block P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

extern int pc_open_blk P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));  
extern int pc_close_blk P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *)); 
extern int pc_get_parm_val_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

extern int pc_get_parm_val_with_po_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_put_parm_val_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int exec_method P(( void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int * ));
extern int dump_block P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int ddi_translate P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int fail_command P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

extern int pc_read_parm_val_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
extern int pc_write_parm_val_with_id_sub P((void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));

#else // else of DDS_TEST_MODE
extern int pc_open_blk P((char *));
extern int pc_get_parm_val_with_id_sub P((ENV_INFO *, ITEM_ID ,unsigned int, EVAL_VAR_VALUE *));
extern int pc_get_parm_val_with_po_sub P((ENV_INFO *, int, unsigned int, EVAL_VAR_VALUE *));
extern int pc_put_parm_val_with_id_sub P((ENV_INFO *, ITEM_ID ,unsigned int, EVAL_VAR_VALUE *));
extern int pc_put_parm_val_with_po_sub P((ENV_INFO *, int ,unsigned int, EVAL_VAR_VALUE *));
extern int pc_read_parm_val_with_id_sub P((ENV_INFO *, ITEM_ID ,unsigned int, EVAL_VAR_VALUE *));
extern int pc_write_parm_val_with_id_sub P((ENV_INFO *, ITEM_ID ,unsigned int, EVAL_VAR_VALUE *));
extern int pc_write_parm_val_with_po_sub P((ENV_INFO *, int ,unsigned int, EVAL_VAR_VALUE *));

int dds_get_user_data(EVAL_VAR_VALUE *var_value);
extern void conv_user_datentime_to_dds_datentime(int *date_array, char char_array[]);
extern void conv_dds_datentime_user_datentime(char char_array[], int *date_array);
extern void conv_user_date_to_dds_date(int *date_array, char char_array[]);
extern void conv_dds_date_to_user_date(char char_array[], int *date_array);
extern void conv_user_duration_to_dds_duration(unsigned long long mSec, char char_array[]);
extern void conv_dds_duration_to_user_duration(char char_array[], unsigned long long *mSec);
extern void conv_dds_time_user_time(char char_array[], int *date_array);
extern void conv_user_time_dds_time(int *date_array, char char_array[]);
extern void conv_dds_timeval_to_user_timeval(char char_array[], int *date_array);
extern void conv_user_timeval_to_dds_timeval(unsigned long epochtime, int *date_array, char* char_array);

/*
 *	System Management related functioanlities
 */

extern int cr_sm_identify_with_device_addr P((ENV_INFO *, unsigned short, CR_SM_IDENTIFY_CNF *));
extern int cr_clr_device_poll_addr P((ENV_INFO *, unsigned short int));
extern int cr_set_device_poll_addr P((ENV_INFO *, unsigned short int, unsigned short int));
extern int cr_assign_tag_to_device P((ENV_INFO *, unsigned short int, char *));
extern int cr_clr_tag_of_device P((ENV_INFO *, unsigned short int ));

/*
 *  Network Management related functions
 */

extern int cr_nma_build_net_tbl P((ENV_INFO *, NETWORK_HANDLE ));
extern int cr_set_appl_state_busy P((ENV_INFO *, int *));
extern int cr_set_appl_state_normal P((ENV_INFO *));
extern int cr_wr_nma_local_vcr P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ));
extern int cr_nma_fms_initiate P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ));
extern int cr_nma_get_od P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ));
extern int cr_nma_read_objects P((ENV_INFO *, CM_NMA_VCR_LOAD*, NETWORK_HANDLE, int));
extern int cr_nma_boot_oper P((ENV_INFO *, NETWORK_HANDLE));
extern int cr_comm_mngr_exit P((ENV_INFO *, unsigned char));
extern int cr_firmware_download P((ENV_INFO *, char *));
extern int cr_get_host_info P((ENV_INFO *, HOST_INFO *, HOST_CONFIG *));
extern int cr_cm_reset_fbk P((ENV_INFO *, NETWORK_HANDLE, HOST_CONFIG *));
extern int cr_get_actual_index P((unsigned short *, int *, unsigned short *));
extern int cr_dds_bypass_read P((ENV_INFO *,unsigned short, int,
		unsigned short, void *val));
extern int cr_abort_mib_fbap_cr P((ENV_INFO *, unsigned short ,unsigned short ));

#endif // DDS_TEST_MODE

int build_device_table( ENV_INFO *, NETWORK_HANDLE);
int delete_device_table( ENV_INFO *, NETWORK_HANDLE);
int dds_connect_device(ENV_INFO *, NETWORK_HANDLE);
int dds_dis_connect_device(ENV_INFO *, NETWORK_HANDLE);
int dds_open_block(ENV_INFO *, NETWORK_HANDLE, char *);
int dds_close_block(ENV_INFO *, NETWORK_HANDLE, char *);
int dds_get_param_list(char *);
int dds_get_variable_list(char *);
//int dds_get_variable_info(char *, ITEM_ID);
extern int dds_get_param_value(char *, ITEM_ID, int, EVAL_VAR_VALUE *, ENV_INFO *, int *);
int dds_put_param_value(char *, ITEM_ID, EVAL_VAR_VALUE *, ENV_INFO *, unsigned int);
int dds_read_param_value(char *, ITEM_ID, int, EVAL_VAR_VALUE *, ENV_INFO *, unsigned int);
int dds_write_param_value(char *, ITEM_ID, EVAL_VAR_VALUE *, ENV_INFO *, unsigned int);
int dds_get_menu_info(char *);
int dds_get_method_info(ENV_INFO *, char *);
int dds_get_unit_info(ENV_INFO *, char *);
int dds_get_view_info(char *);
int dds_get_refresh_info(ENV_INFO *, char *);
int dds_get_wao_info(char *);
int dds_get_collec_info(char *);
int dds_get_itemarr_info(char *);
int dds_get_param_info(char *);
extern int dds_exec_method P((ENV_INFO, OP_REF, DDI_ITEM_SPECIFIER, DDI_BLOCK_SPECIFIER));
int dds_ddi_translate(char *, char *);
extern int dds_dump_block P((char *, void **, OBJECT_INDEX *, ITEM_TYPE *, unsigned int *));
int get_network_handle(void);

extern void reference_type_to_name(ITEM_TYPE, char **);
extern void ref_item_id_to_name(ITEM_ID, char **);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TST_DDS_H */
