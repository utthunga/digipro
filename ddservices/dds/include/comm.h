/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)comm.h	30.3  30.3  14 Nov 1996
 *
 *  This file contains common definitions needed for the FMS
 *	communication sub-system.
 *
 */
#ifndef COMM_H
#define COMM_H

#include "ddldefs.h"
#include "od_defs.h"

/*
 * Mask values for type in structures PC_P_REF and P_REF
 */

#define PC_PARAM_OFFSET_REF 0x1
#define PC_ITEM_ID_REF      0x2

 /*
 * The following structures are used to define references to parameters
 * when accessing the Parameter Cache.
 */

typedef struct {
    ITEM_ID     id;
    int         po;
    char	*name;	
    SUBINDEX    subindex;
    SUBINDEX    subsubindex;
    int         type;
} P_REF;

typedef int		DEVICE_HANDLE;
typedef int		BLOCK_HANDLE;

#ifndef BOOLEANDECLARED
#define BOOLEANDECLARED
typedef	UINT8	BOOLEAN_T;
#endif

typedef struct {
    unsigned short      cm_type;
    unsigned short      cm_size;
    union {
		BOOLEAN_T			cm_boolean;
        float           cm_float;
        double          cm_double;
        unsigned long   cm_ulong;
        long            cm_long;
        unsigned long	cm_time_value[2];
        char            *cm_stream;
        char            cm_ca[8];
        unsigned short	cm_sa[4];
    } cm_val;
} COMM_VALUE;

typedef struct {
    int         cmvl_count;
    int         cmvl_limit;
    COMM_VALUE  *cmvl_list;
} COMM_VALUE_LIST;

#endif /* COMM_H */
