#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#ifndef RET_INFO_H
#include "ret_info.h"
#endif

#ifndef HOST_INFO_H
#include "host.h"
#endif

/* DD Scan Demo Interface */
int ff_initialize(RET_INFO*, const char *, HOST_CONFIG *);
int ff_configure(RET_INFO*, HOST_CONFIG *);
int ff_get_host_info(RET_INFO *, HOST_INFO *, HOST_CONFIG *);
extern int ff_connect(RET_INFO*, unsigned short, void*);
int ff_offline_connect(RET_INFO*, unsigned short);

int ff_scan_begin(RET_INFO*);
unsigned char ff_scan_has_next(void);
void ff_scan_end(void);

int ff_connect_block(RET_INFO *, const char *);
int ff_disconnect_block(RET_INFO *, const char *);
int ff_disconnect(RET_INFO* , unsigned short );
int ff_uninitialize(RET_INFO *, unsigned char);
int ff_get_mode_block(RET_INFO *, char *, char *);
int ff_reset_fbkboard(RET_INFO *, unsigned short, HOST_CONFIG *);
int ff_exec_method(RET_INFO *ret_info, const char *, unsigned long );
int ff_set_pd_tag(RET_INFO *ret_info, unsigned short device_addr);

/* Notify to device admin  about diconnection state  change */
extern int (*getCurrentStatus) P(());

#ifdef __cplusplus
}
#endif
