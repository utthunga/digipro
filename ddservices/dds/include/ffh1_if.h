/*  Filename    : fbkhost_if.h                                                  */
/*                                                                           */
/*  Description : This file contains the global function declarations,       */
/*                defines and types of the FBK Host IF communcation interface      */
/*                                                                           */
/*****************************************************************************/

#ifndef __FBKHOST_IF_H__
#define __FBKHOST_IF_H__

// renamed APIs
#define fbkhost_download_firmware fbapi_download_firmware
#define fbkhost_exit              fbapi_exit
#define fbkhost_get_device_info   fbapi_get_device_info
#define fbkhost_init              fbapi_init
#define fbkhost_rcv_con_ind       fbapi_rcv_con_ind
#define fbkhost_snd_req_res       fbapi_snd_req_res
#define fbkhost_start             fbapi_start

#define T_DOWNLOAD_FIRMWARE       T_FBAPI_DOWNLOAD_FIRMWARE
#define T_DEVICE_INFO             T_FBAPI_DEVICE_INFO

#define T_FBKHOST_INIT            T_FBAPI_INIT

// T_FIELDBUS_SERVICE_DESCR comes from ff_if.h ( which is included from fbapi_if.h )

#include "api_linux.h"  // for LPCTSTR ...

#include "fbapi_if.h"



#pragma pack(push, 2)

#include "keywords.h"
#include "fbkhost_conf.h"
#include "fbkhost_type.h"
#include "fbkhost_error.h"
#include "fbkhost_fms.h"
#include "fbkhost_nma.h"
#include "fbkhost_sm.h"


/*****************************************************************************/
/* INTERFACE DEFINES                                                         */
/*****************************************************************************/

/* --- contants to receive an indication or confirmation ------------------- */
#define CON_IND_RECEIVED    0x0001  /* ind. or con. has been received        */
#define NO_CON_IND_RECEIVED 0x0000  /* nothing has been received             */

/* --- values > CON_IND_RECEIVED are error returns (see fb_err.h) ---------- */

/* --- OK return value ----------------------------------------------------- */
#define E_OK                0        /* no errors                            */
/* --- values != E_OK are error returns (see fb_err.h) --------------------- */

/* --- service result ------------------------------------------------------ */
#define NEG                 0x01    /* result for negative response          */
#define POS                 0x00    /* result for positive response          */

/* --- service primitives -------------------------------------------------- */
#define REQ                 0x00   /* request primitive                      */
#define CON                 0x01   /* confirmation primitive                 */
#define IND                 0x02   /* indication primitive                   */
#define RES                 0x03   /* response primitive                     */


/* --- layer identifiers --------------------------------------------------- */
#define USR                 0x01      /* identifier USER                     */
#define FMS                 0x02      /* identifier FMS                      */
#define FAS                 0x03      /* identifier FAS                      */
#define DLL                 0x04      /* identifier DLL                      */
#define NMA                 0x05      /* identifier NMA                      */
#define SM                  0x06      /* identifier SM                       */
#define FMS_USR             0x07      /* identifier FMS-USER                 */
#define NMA_USR             0x08      /* identifier NMA-USER                 */
#define SM_USR              0x09      /* identifier SM-USER                  */

#define TRACE_ID_DLL        1
#define TRACE_ID_MSG        2

#pragma pack(pop)



/* The following ifdef block is the standard way of creating macros which make exporting
 * from a DLL simpler. All files within this DLL are compiled with the DIAGDEV_EXPORTS
 * symbol defined on the command line. this symbol should not be defined on any project
 * that uses this DLL. This way any other project whose source files include this file see
 * DIAGDEV_API functions as being imported from a DLL, whereas this DLL sees symbols
 * defined with this macro as being exported. */

#ifndef API_LINUX
#    ifdef FBKHOSTAPI_EXPORTS
#        define FBKHOST_API __declspec(dllexport)
#    else
#        define FBKHOST_API __declspec(dllimport)
#    endif
#    define FBKHOST_CALL_CONV __stdcall
#else
#    define FBKHOST_API       // KT OFC 2014-02-18 We need to remove the Win32 definition modifiers
#    define FBKHOST_CALL_CONV
#endif

#endif /* __FBKHOST_IF_H__ */
