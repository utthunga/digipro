#ifndef HOST_INFO_H
#define HOST_INFO_H

#ifdef __cplusplus
extern "C" {
#endif

#define HOST_ONLINE 1
#define HOST_SIMULATOR 2
#define TTY_PORT_BUFFER_LEN (32)
#define SYSTEM_CONFIG_MEMBERS_SIZE (34)

#define		HOST_CLIENT		1
#define		HOST_SERVER		2
#define		HOST_STANDALONE	3

#define MAX_COM_PORT_NAME_LENGTH    20

/* Version information of the FBK board. Softing has released two
 * versions of the FBK board firmware to Fluke. This info helps
 * the developer load the correct version.
 */
// TODO - removed duplicated HOST_INFO structure
typedef struct 
{
  unsigned short device_id;      /* device id: 0x0055 = FBK Host IF */
  unsigned short hardware_revision;    /* hardware revision */
  unsigned int serial_device_number;   /* serial number */
  unsigned int boot_version;
  unsigned char boot_version_str[128]; /* bootblock version string */
  unsigned int  firmware_version;
  unsigned char firmware_version_str[128]; /*firmware version string */
}HOST_INFO;

typedef struct
{
    char vendor_name[SYSTEM_CONFIG_MEMBERS_SIZE];
    char model_name[SYSTEM_CONFIG_MEMBERS_SIZE];
    char revision_string[SYSTEM_CONFIG_MEMBERS_SIZE];
    char host_pd_tag[SYSTEM_CONFIG_MEMBERS_SIZE];

}SYSTEM_CONFIG;

/* 
 * Please see FF-801.pdf section 5.3.1.3 for DLME configuration paramters 
 * Please see FF-940.pdf section 5.4 for default configuration values
 */

typedef struct
{
   unsigned short this_link;  /* Represnts the node of the FBK */
   unsigned short slot_time;  
   unsigned char phlo;        /* Per DL PDU PHL Overload */
   unsigned char mrd;         /* Max Response delay */
   unsigned char mipd;        /* Min Inter PDU delay */
   unsigned char tsc;         /* Time Sync Class */
   unsigned char phis_skews;  /* Max Inter channel Signal Skews */
   unsigned char phpe;        /* PHL Preamble Extension */
   unsigned char phge;        /* PHL Gap Extension */
   unsigned char fun;         /* First unpolled node */
   unsigned char nun;         /* Number of iconsecutive unpolled nodes */
}DLL_CONFIG;


typedef struct
{
    char            tty_port[TTY_PORT_BUFFER_LEN];
    unsigned char   host_addr;
    SYSTEM_CONFIG   sm_config;
    DLL_CONFIG      dll_config;
    unsigned char   host_mode;
    int				host_type;
}HOST_CONFIG;


#ifdef __cplusplus
}
#endif


#endif // HOST_INFO_H

