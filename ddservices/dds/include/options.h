/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)options.h	30.8  30  05 Mar 1997
 *
 */

#ifndef OPTIONS_H
#define OPTIONS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * Global options.
 */

/* Name of the input file. */
extern char *input_file;

/* If non-zero, append new names to the symbol table file (-a). */
extern int append_symbols;

/* Name of the standard dictionary file (-d). */
/*extern char *dictionary_file;*/

/* If non-zero, process all import constructs (-i). */
extern int process_imports;

/* If non-zero, process all like constructs (-l). */
extern int process_likes;

/* If non-zero, require all names to be registered in a symbol table (-r). */
extern int registration;

/* If non-zero, allow symbols to be used but not defined (-u). */
extern int partial_symbols;

/* If non-zero, menu item images are stored internally in the DD */
extern int inline_images;

/* If non-zero, a Tokenizer 4.x - style binary will be generated, resulting in
 * smaller length limits and fewer parser features
 */
extern int use_40_format;

/* If non-zero, the Tokenizer will put line number and other debug information
 * into the output DD for the purposes of method debugging.
 */
extern int debug_methods;

/* Global path to the images directory */
#define MAX_IMAGE_PATH 256
extern char image_path[];  

/* Global path to the standard.sym directory */
extern char std_sym_path[];  

extern int num_vendor_dictionaries;

/* Treat 5.1 imported binaries as having Latin-1 format strings (as opposed to UTF-8 strings) */
extern int treat_51_as_latin1;

#ifdef TOK


/* Language in which error messages are generated (-L). */
/*extern int errors_language;*/

#endif /*TOK*/

/* Location of release directory (-R). */
extern char *release_directory;

/* the following line is not in the DDS file */
extern char	dir_sep_char;

/* Warning level (-W). */
extern int warning_level;

#ifdef __cplusplus
}
#endif /* __cplusplus */

/*
 * Language codes.
 */

#define ENGLISH 1

#endif  /* OPTIONS_H */
