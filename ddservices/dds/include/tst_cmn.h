/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)tst_cmn.h	30.3	14 Nov 1996
 */

#ifndef TEST_CMN_H
#define TEST_CMN_H

#include "table.h"
#include "evl_lib.h"
#include "ddi_lib.h"

#define MAX_NAME_LENGTH				60
#define MAX_ID_TABLE_SIZE           10000
#define MAX_VAR_TABLE_SIZE          1000
#define MAX_DEV_SPEC_TABLE_SIZE			5000
#define MAX_DICT_TABLE_SIZE			20000
#define MAX_LINE_LEN                200
#define MAX_FILE_NAME_LEN           256
#define NUM_ITEMS					20
#define NUM_ATTRIBUTES				544
#define MAX_NUM_ATTRIBUTES			32
#define ABSURD_NUMBER				9999999
#define DEVICE_PATH_ENV_NAME        "DEVICE_PATH"
#define NTOK_PATH_ENV_NAME          "TOK_RELEASE_DIR"
#define NTOK_FIRMWARE_PATH_ENV_NAME          "TOK_FIRMWARE_PATH"
#define NTOK_CFF_PATH               "TOK_CFF_PATH"
#define FILE_PATH_LEN 				128
#define	MIN_SCRATCH_PAD_SIZE		220
#define	MEMORY_ALLOCATION_ERROR		5
#define	GET_SP_SIZE_ERROR			7

#define ENV_COMM_MODE				"TOK_COMM_MODE"

#define	CLEANED			0
#define	NOT_CLEANED		1

#define	IMASK	unsigned	long

#define DDS_MAX_TYPES			(MAX_ITYPE+3)

/* define the below, if In-put file support is required */
#undef  IN_FILE_SUPPORT

typedef struct {
	ITEM_TYPE		item_type;
	ITEM_ID         item_id;
	OBJECT_INDEX	object_index;
	char            item_name[MAX_NAME_LENGTH];
}               ID_TABLE_ENTRY;

typedef struct {
	DD_DEVICE_ID	id;
	unsigned int	num_entries;
	ID_TABLE_ENTRY	table[MAX_ID_TABLE_SIZE];
}				ID_TABLE_HEADER;

typedef union {
	long            S;	/* DDS_INTEGER */
	unsigned long   U;	/* DDS_UNSIGNED, DDS_ENUMERATED, DDS_INDEX, DDS_BIT_ENUMERATED */
	float           F;	/* DDS_FLOAT  */
	double          D;	/* DDS_DOUBLE */
	char           *C;	/* DDS_ASCII */
}               ITEM_VALUE;

typedef struct {
	unsigned short  type;
	unsigned short  size;
	ITEM_VALUE      value;
}               VAR_VALUE;

typedef struct {
	ulong           ref;
	unsigned short  len;
	char           *str;
}               DICT_TABLE_ENTRY;

typedef struct {
	char           *attrstr;
	ulong           attrmask;
}               ATTR;

typedef struct {
	char           *type_name;
	ATTR           *attrs;
	long            max_attrs;
	ITEM_TYPE       type_value;
}               ITEM_ATTR;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int 		init_var_table P(( ));
extern int		check_to_clean P((void **, OBJECT_INDEX, OBJECT_INDEX, ITEM_TYPE, unsigned int, unsigned int));
extern void 	dict_table_install P((ulong ref, char *name, char *str));
extern void 	dict_table_uninstall ();
extern void 	free_dict_table P(());
extern int 		set_upcalls P(());
extern int 		item_id_to_name P((ITEM_ID, char **));
extern int 		item_name_to_id P((char *, ITEM_ID *));
extern char * 	item_type_to_name P((ITEM_TYPE));





/*
 * Print value functions
 */

extern void	    print_op_desc_list P((OP_DESC_LIST * op_desc_list, char * name));
extern void     print_item_id_list P((ITEM_ID_LIST * item_id_list, char * name));
extern void     print_item_id P((ITEM_ID *, char *));
extern void     print_type P((ITEM_TYPE));
extern void     print_errors P((ITEM_ID id, RETURN_LIST * errors));
extern void		print_var_value P((EVAL_VAR_VALUE *, char *));
extern void		print_comm_value_list P((COMM_VALUE_LIST *, char *));
extern void		print_env_info P((ENV_INFO *, char *));
extern void		print_identify_cnf P((IDENTIFY_CNF *, char *));

/*
 * Print flat functions
 */

extern void		print_flat_item P((void *, unsigned long, char *, ITEM_TYPE, BLOCK_HANDLE));
extern void		print_flat_device_dir P((FLAT_DEVICE_DIR *, unsigned long, unsigned short));
extern void		print_flat_block_dir P((FLAT_BLOCK_DIR *, unsigned long, char *));


/*
 *	Utility routines
 */

extern void packed_ascii_to_ascii P(( char *, char *, int ));
extern void ascii_to_packed_ascii P(( char *, char *, int ));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TEST_CMN_H */
