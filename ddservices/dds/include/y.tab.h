#define CHAR_TOK 257
#define FLOAT_TOK 258
#define LONG_TOK 259
#define INT_TOK 260
#define DOUBLE_TOK 261
#define UNSIGNED_TOK 262
#define SHORT_TOK 263
#define SIGNED_TOK 264
#define BREAK_TOK 265
#define CASE_TOK 266
#define CONTINUE_TOK 267
#define DEFAULT_TOK 268
#define DO_TOK 269
#define ELSE_TOK 270
#define FOR_TOK 271
#define IF_TOK 272
#define RETURN_TOK 273
#define SWITCH_TOK 274
#define WHILE_TOK 275
#define IDENTIFIER 276
#define CHAR_CONST 277
#define INT_CONST 278
#define FLOAT_CONST 279
#define STRING_CONST 280
#define ASSIGN_TOK 281
#define LOG_OR_TOK 282
#define LOG_AND_TOK 283
#define EQOP_TOK 284
#define RELOP_TOK 285
#define SHIFT_TOK 286
#define MULTOP_TOK 287
#define UNARY 288
#define PLUSPLUS 289
#define MINUSMINUS 290
typedef union {
    SYM 			*sym_ptr;
    EXPRESS 		*exp_ptr;
    STATEMENT 		*stmt_ptr;
    STMT_PTRS 		*stmt_ptrs;
    unsigned int	line;

	struct {
    	int 		 typ;
		unsigned int l;
    } typ_tok;

	struct {
    	int 		 o;			/* operator */
		unsigned int l;
    } op_tok;

	struct {
    	char 		 *str;
    	unsigned int l;
    } str_tok;

} YYSTYPE;
extern YYSTYPE yylval;
