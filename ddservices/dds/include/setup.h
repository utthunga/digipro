/*
 *   @(#)setup.h	30.2 09 Sep 1996
 */
/*
 *  Copyright 1994-2002 - Fieldbus Foundation
 *  All rights reserved.
 */

/*
 * Define the Software Revision numbers for the Pre-Processor and
 * the Tokenizer.  Both should be the same even though two separate
 * values are defined.
 * The Tokenizer Version number is a 3-part id defined as follows:
 *
 * 		<major ddod revision>.<minor ddod revision>.<software revision>
 *
 * The major and minor ddod revisions are shared with DDS and are
 * assigned in another header file.
 */
#define DDP_REVISION	    2
#define DDP_ALPHA_REVISION	0
#define DDP_TEXT		"FF Pre-Processor"

#define SYN_REVISION	    0
#define SYN_ALPHA_REVISION	0

#define DDT_REVISION	    1
#define DDT_ALPHA_REVISION	1
#define DDT_SPECIAL_REVISION "[BETA] Build 6"


#if defined(TOK) || defined(SYN)

#  ifdef TOK

#  define DDT_TEXT		"FF Tokenizer"

#  endif /*TOK*/

#  ifdef SYN

#  define SYN_TEXT		"FF Synthesizer"

#  endif /*SYN*/
#else
#  ifndef _SETUPDEF

#  define _SETUPDEF

   char *Setup_Filename = "toksetup.bin";

#  endif /* _SETUPDEF */
#endif /*DDS*/

