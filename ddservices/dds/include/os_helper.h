/*
 * os_helper.h
 *
 *      Author: Jayanth Mahadeva
 */

#ifndef OS_HELPER_H_
#define OS_HELPER_H_

/*
 * Maintain the fbk data thread status
 */
#define THREAD_STATE_NONE  -1
#define THREAD_STATE_RUN   0
#define THREAD_STATE_STOP  1   

typedef struct {
	int listen_fd;
	int accept_fd;

}SHARED_MEMORY;


int write_to_shm(char *, int type, void *data);
int read_from_shm(char *, int type, void *data);
//int create_shared_memory(char **);
//int get_shared_memory_region(char **);

void acquire_dpc_sync_lock(void);
void release_dpc_sync_lock(void);
void acquire_fbk_read_lock(void);
void release_fbk_read_lock(void);

void set_fbk_data_thread_status(int);
int get_fbk_data_thread_status(void);

#endif /* OS_HELPER_H_ */
