/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)ddldefs.h	30.7 30.7 10 Jan 1997
 */

#ifndef DDLDEFS_H
#define DDLDEFS_H

typedef unsigned long	ITEM_ID; 
typedef unsigned short	ITEM_TYPE;
typedef unsigned long	DDL_UINT;	/* Unparsed integer from binary */
typedef	unsigned short	OFFSET;
typedef unsigned short   ITEM_SIZE;
#define DDL_HUGE_INTEGER ULONG_MAX  /* defines the maximum size of an */
									/* unsigned integer               */

/*
 *  Parse integer macro.
 *  This macro can handle parsing simple (one byte) integers.
 *  If the Most Significant Bit of CHUNK is set, the integer occupies
 *  multiple bytes.  In this case "ddl_parse_integer_func()" should
 *  be called.
 */

#if defined (_MSC_VER) && (_MSC_VER >= 1300)
	#define DDL_PARSE_INTEGER(C,L,V)  \
		__pragma(warning(push)) \
		__pragma(warning(disable:4127)) \
		{   \
			if (**(C) & 0x80) { \
				rc = ddl_parse_integer_func((C), (L), (V)); \
				if (rc != DDL_SUCCESS)  \
					return rc;  \
			} else {    \
				if ((V) != 0) {*(V) = **(C);}   \
				++(*(C));   \
				--(*(L));   \
			}   \
		} \
		__pragma(warning(pop))
#else
	#define DDL_PARSE_INTEGER(C,L,V)    \
		{   \
			if (**(C) & 0x80) { \
				rc = ddl_parse_integer_func((C), (L), (V)); \
				if (rc != DDL_SUCCESS)  \
					return rc;  \
			} else {    \
				if ((V) != 0) {*(V) = **(C);}   \
				++(*(C));   \
				--(*(L));   \
			}   \
		}
#endif

/*
 * Major and minor DDOD Binary revision numbers.
 * These are used by the Tokenizer and DDS to form a Version number:
 *
 * 		<major ddod revision>.<minor ddod revision>.<software revision>
 *
 * The major and minor binary revision numbers are assigned here. 
 * The software revision numbers are assigned independently by the
 * Tokenizer and DDS in separate header files.
 */

#define DDOD_REVISION_MAJOR		5
#define DDOD_REVISION_MINOR		2

/*
 * Value assigned to unused table offsets in the Block Directories.
 */
#define UNUSED_OFFSET   ((OFFSET)~0)

/*
 *  Item Types.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the item_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */
#define	RESERVED_ITYPE1			0
#define VARIABLE_ITYPE 	       	1
#define COMMAND_ITYPE           2
#define MENU_ITYPE              3
#define EDIT_DISP_ITYPE         4
#define METHOD_ITYPE            5
#define REFRESH_ITYPE           6
#define UNIT_ITYPE              7
#define WAO_ITYPE 	       		8
#define ITEM_ARRAY_ITYPE        9
#define COLLECTION_ITYPE        10
#define	RESERVED_ITYPE2			11
#define BLOCK_ITYPE             12
#define PROGRAM_ITYPE           13
#define RECORD_ITYPE            14
#define ARRAY_ITYPE             15
#define VAR_LIST_ITYPE          16
#define RESP_CODES_ITYPE        17
#define DOMAIN_ITYPE            18
#define MEMBER_ITYPE            19
#define AXIS_ITYPE              20
#define CHART_ITYPE             21
#define FILE_ITYPE              22
#define GRAPH_ITYPE             23
#define LIST_ITYPE              24
#define SOURCE_ITYPE            25
#define WAVEFORM_ITYPE          26
#define GRID_ITYPE              27
#define IMAGE_ITYPE             28
#define MAX_ITYPE				29	/* must be last in list */

/*
 *	Special object type values
 */

#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130

#define DEVICE_DIR_TYPE_LARGE	150

 /* these additional ITYPES are used by resolve to build resolve trails */

#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202
#define LOCAL_PARAM_ITYPE       203
#define BLOCK_XREF_ITYPE        204  /* Cross-block reference */

/* Selectors used for direct DDL construct referencing in methods */
#define SEL_VIEW_MIN        16
#define SEL_VIEW_MAX        17
#define SEL_MIN_VALUE       18
#define SEL_MAX_VALUE       19
#define SEL_CONSTANT_UNIT   20
#define SEL_LABEL           21
#define SEL_HELP            22
#define SEL_CAPACITY        23
#define SEL_COUNT           24
#define SEL_SCALING         25
#define SEL_X_AXIS          26
#define SEL_Y_AXIS          27


/* List-referencing selectors */
#define LIST_REF_FIRST 0xfffffffe
#define LIST_REF_LAST  0xffffffff

/*
 * Types of variables.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the var_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define DDS_UNUSED 		    0
#define DDS_INTEGER 		2
#define DDS_UNSIGNED 		3
#define DDS_FLOAT 			4
#define DDS_DOUBLE 			5
#define DDS_ENUMERATED 		6
#define DDS_BIT_ENUMERATED 	7
#define DDS_INDEX 			8
#define DDS_ASCII 			9


#define DDS_PACKED_ASCII 	10
#define DDS_PASSWORD 		11
#define DDS_BITSTRING 		12


#define DDS_DATE 			13
#define DDS_TIME 			14
#define DDS_DATE_AND_TIME 	15
#define DDS_DURATION 		16
#define DDS_EUC				17
#define DDS_OCTETSTRING		18
#define DDS_VISIBLESTRING	19
#define DDS_TIME_VALUE		20
#define DDS_BOOLEAN_T		21
#define DDS_MAX_TYPE		22	/* must be last in list */

/*
 * Lnt Required types.
 *  As we are not supposed to change or modify the above
 *  list, we are just adding our own data types after  
 *  last type in the list("DDS_MAX_TYPE").
 */
#define DDS_ACK_TYPE        (DDS_MAX_TYPE + 1)

/*
 * Types of members.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the mem_type in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define	COLLECTION_MEM		1
#define	PARAM_MEM			2
#define	PARAM_LIST_MEM		3
#define	VAR_LIST_MEM		4
#define	RECORD_MEM			5
#define	CHARACTERISTIC_MEM	6	
#define CHART_MEM           7
#define FILE_MEM            8
#define GRAPH_MEM           9
#define LIST_MEM            10
#define SOURCE_MEM          11
#define	LOCAL_PARAM_MEM		12
#define GRID_MEM            13
#define MENU_MEM            14
#define METHOD_MEM          15

/*
 * Classes of variables and methods.
 */

#define DIAGNOSTIC_CLASS			0x000001
#define DYNAMIC_CLASS				0x000002
#define SERVICE_CLASS				0x000004
#define CORRECTION_CLASS			0x000008
#define COMPUTATION_CLASS			0x000010
#define INPUT_BLOCK_CLASS			0x000020
#define ANALOG_OUTPUT_CLASS			0x000040


#define LOCAL_DISPLAY_CLASS			0x000100
#define FREQUENCY_CLASS				0x000200
#define DISCRETE_CLASS				0x000400
#define DEVICE_CLASS				0x000800
#define LOCAL_CLASS					0x001000
#define INPUT_CLASS					0x002000
#define OUTPUT_CLASS				0x004000
#define CONTAINED_CLASS				0x008000
#define OPERATE_CLASS				0x010000
#define ALARM_CLASS					0x020000
#define TUNE_CLASS					0x040000



/*
 * Handling of variables.
 */

#define READ_HANDLING 				0x01
#define WRITE_HANDLING 				0x02

/*
 * Ordinary status classes of bit enumerated variables.
 */

#define HARDWARE_STATUS				0x000001
#define SOFTWARE_STATUS				0x000002
#define PROCESS_STATUS				0x000004
#define MODE_STATUS					0x000008
#define DATA_STATUS					0x000010
#define MISC_STATUS					0x000020
#define EVENT_STATUS				0x000040
#define STATE_STATUS				0x000080
#define SELF_CORRECTING_STATUS		0x000100
#define CORRECTABLE_STATUS			0x000200
#define UNCORRECTABLE_STATUS		0x000400
#define SUMMARY_STATUS				0x000800
#define DETAIL_STATUS				0x001000
#define MORE_STATUS					0x002000
#define COMM_ERROR_STATUS			0x004000
#define IGNORE_IN_TEMPORARY_STATUS	0x008000
#define BAD_OUTPUT_STATUS			0x010000

/*
 * ALTERNATIVE output status classes.
 */

#define OC_NORMAL					0	/* must be 0, see ddl_parse_one_enum() */



/*
 * Data item types.
 */

#define DATA_CONSTANT 					0
#define DATA_REFERENCE 					1


#define DATA_FLOATING 					5

/*
 * Vector item types.
 */

#define VECTOR_CONSTANT 				0
#define VECTOR_REFERENCE 				1


#define VECTOR_FLOATING 				5
#define VECTOR_STRING                   6

/*
 * Response code types.
 */

#define SUCCESS_RSPCODE 				1
#define MISC_WARNING_RSPCODE 			2
#define DATA_ENTRY_WARNING_RSPCODE 		3
#define DATA_ENTRY_ERROR_RSPCODE 		4
#define MODE_ERROR_RSPCODE 				5
#define PROCESS_ERROR_RSPCODE 			6
#define MISC_ERROR_RSPCODE 				7
#define INVALID_RSPCODE 				8



/*
 * data item flags.
 */

#define WIDTH_PRESENT 					0x8000

/*
 * Menu item flags.
 */

#define READ_ONLY_ITEM 					0x0001
#define DISPLAY_VALUE_ITEM 				0x0002
#define REVIEW_ITEM 					0x0004
#define HIDDEN_ITEM                     0x0008
#define NO_LABEL_ITEM                   0x0010
#define NO_UNIT_ITEM                    0x0020
#define SEPARATOR_ITEM                  0x0040
#define STRING_ITEM                     0x0080
#define IMAGE_ITEM                      0x0100
#define INLINE_ITEM                     0x0200
#define ROWBREAK_ITEM                   0x0400
#define COLUMNBREAK_ITEM                0x0800
#define BIT_ENUM_WITH_MASK_ITEM         0x1000

/*
 * Method type flags
 */
#define SCALING_TYPE_METHOD			0x00000001
#define USER_TYPE_METHOD			0x00000010


/*
 * Attribute Identifier Tags for DDOD Item Objects.
 * These tags are also used as bit positions for the
 * Item Attribute Masks defined below.
 */

/* BLOCK attributes */

#define BLOCK_CHARACTERISTIC_ID     0
#define BLOCK_LABEL_ID              1
#define BLOCK_HELP_ID               2
#define BLOCK_PARAM_ID              3
#define BLOCK_MENU_ID               4
#define BLOCK_EDIT_DISP_ID          5
#define BLOCK_METHOD_ID             6
#define BLOCK_REFRESH_ID            7
#define BLOCK_UNIT_ID               8
#define BLOCK_WAO_ID                9
#define BLOCK_COLLECT_ID            10
#define BLOCK_ITEM_ARRAY_ID         11
#define BLOCK_PARAM_LIST_ID         12
#define BLOCK_AXIS_LIST_ID          13
#define BLOCK_CHART_LIST_ID         14
#define BLOCK_FILE_LIST_ID          15
#define BLOCK_GRAPH_LIST_ID         16
#define BLOCK_LIST_LIST_ID          17
#define BLOCK_SOURCE_LIST_ID        18
#define BLOCK_WAVEFORM_LIST_ID      19
#define BLOCK_LOCAL_PARAM_ID        20
#define BLOCK_GRID_LIST_ID          21
#define BLOCK_IMAGE_LIST_ID         22
#define BLOCK_CHART_MEMBERS_ID      23
#define BLOCK_FILE_MEMBERS_ID       24
#define BLOCK_GRAPH_MEMBERS_ID      25
#define BLOCK_GRID_MEMBERS_ID       26
#define BLOCK_MENU_MEMBERS_ID       27
#define BLOCK_METHOD_MEMBERS_ID     28
#define BLOCK_LIST_MEMBERS_ID       29
#define MAX_BLOCK_ID                30  /* must be (largest index in list + 1) */
#define MAX_BLOCK_ID_PRE_5          13	/* For 4.x compatibility: do not change */

/* VARIABLE attributes */

#define VAR_CLASS_ID                0
#define VAR_HANDLING_ID             1
#define VAR_UNIT_ID                 2
#define VAR_LABEL_ID                3
#define VAR_HELP_ID                 4
#define VAR_READ_TIME_OUT_ID        5
#define VAR_WRITE_TIME_OUT_ID       6
#define VAR_VALID_ID                7
#define VAR_PRE_READ_ACT_ID         8
#define VAR_POST_READ_ACT_ID        9
#define VAR_PRE_WRITE_ACT_ID        10
#define VAR_POST_WRITE_ACT_ID       11
#define VAR_PRE_EDIT_ACT_ID         12
#define VAR_POST_EDIT_ACT_ID        13
#define VAR_RESP_CODES_ID           14
#define VAR_TYPE_SIZE_ID            15
#define VAR_DISPLAY_ID              16
#define VAR_EDIT_ID                 17
#define VAR_MIN_VAL_ID              18
#define VAR_MAX_VAL_ID              19
#define VAR_SCALE_ID                20
#define VAR_ENUMS_ID                21
#define VAR_INDEX_ITEM_ARRAY_ID     22
#define VAR_REFRESH_ACT_ID          23
#define VAR_WRITE_MODE_ID           24
#define MAX_VAR_ID                  25	/* must be last in list */

/* MENU attributes */

#define MENU_LABEL_ID               0
#define MENU_ITEMS_ID               1
#define MENU_STYLE_ID               2
#define MENU_STYLE_STRING_ID        3
#define MENU_VALID_ID               4
#define MAX_MENU_ID                 5	/* must be last in list */

/* EDIT_DISPLAY attributes */

#define EDIT_DISPLAY_LABEL_ID           0
#define EDIT_DISPLAY_EDIT_ITEMS_ID      1
#define EDIT_DISPLAY_DISP_ITEMS_ID      2
#define EDIT_DISPLAY_PRE_EDIT_ACT_ID    3
#define EDIT_DISPLAY_POST_EDIT_ACT_ID   4
#define MAX_EDIT_DISPLAY_ID             5	/* must be last in list */

/* METHOD attributes */

#define METHOD_CLASS_ID             0
#define METHOD_LABEL_ID             1
#define METHOD_HELP_ID              2
#define METHOD_DEF_ID               3
#define METHOD_VALID_ID             4
#define METHOD_SCOPE_ID             5
#define MAX_METHOD_ID               6	/* must be last in list */

/* REFRESH attributes */

#define REFRESH_ITEMS_ID            0
#define MAX_REFRESH_ID              1	/* must be last in list */

/* UNIT attributes */

#define UNIT_ITEMS_ID               0
#define MAX_UNIT_ID                 1	/* must be last in list */

/* WRITE AS ONE attributes */

#define WAO_ITEMS_ID                0
#define MAX_WAO_ID                  1	/* must be last in list */

/* ITEM_ARRAY attributes */

#define ITEM_ARRAY_ELEMENTS_ID      0
#define ITEM_ARRAY_LABEL_ID         1
#define ITEM_ARRAY_HELP_ID          2
#define ITEM_ARRAY_VALID_ID         3
#define MAX_ITEM_ARRAY_ID           4	/* must be last in list */

/* COLLECTION attributes */

#define COLLECTION_MEMBERS_ID       0
#define COLLECTION_LABEL_ID         1
#define COLLECTION_HELP_ID          2
#define MAX_COLLECTION_ID           3	/* must be last in list */

/* PROGRAM attributes */

#define PROGRAM_ARGS_ID             0
#define PROGRAM_RESP_CODES_ID       1
#define MAX_PROGRAM_ID              2	/* must be last in list */

/* RECORD attributes */

#define RECORD_MEMBERS_ID           0
#define RECORD_LABEL_ID             1
#define RECORD_HELP_ID              2
#define RECORD_RESP_CODES_ID        3
#define RECORD_VALID_ID             4
#define RECORD_WRITE_MODE_ID        5
#define MAX_RECORD_ID               6	/* must be last in list */

/* ARRAY attributes */

#define ARRAY_LABEL_ID              0
#define ARRAY_HELP_ID               1
#define ARRAY_TYPE_ID               2
#define ARRAY_NUM_OF_ELEMENTS_ID    3
#define ARRAY_RESP_CODES_ID         4
#define ARRAY_VALID_ID              5
#define ARRAY_WRITE_MODE_ID         6
#define MAX_ARRAY_ID                7	/* must be last in list */

/* VARIABLE LIST attributes */

#define VAR_LIST_MEMBERS_ID         0
#define VAR_LIST_LABEL_ID           1
#define VAR_LIST_HELP_ID            2
#define VAR_LIST_RESP_CODES_ID      3
#define MAX_VAR_LIST_ID             4	/* must be last in list */

/* RESPONSE CODE attributes */

#define RESP_CODE_MEMBER_ID         0
#define MAX_RESP_CODE_ID            1	/* must be last in list */

/* DOMAIN attributes */

#define DOMAIN_HANDLING_ID          0
#define DOMAIN_RESP_CODES_ID        1
#define MAX_DOMAIN_ID               2	/* must be last in list */

/* COMMAND attributes */

#define COMMAND_NUMBER_ID           0
#define COMMAND_OPER_ID             1
#define COMMAND_TRANS_ID            2
#define COMMAND_RESP_CODES_ID       3
#define MAX_COMMAND_ID              4	/* must be last in list */

/* AXIS attributes */
#define AXIS_LABEL_ID              0
#define AXIS_HELP_ID               1
#define AXIS_MIN_VALUE_ID          2
#define AXIS_MAX_VALUE_ID          3
#define AXIS_SCALING_ID            4
#define AXIS_CONSTANT_UNIT_ID      5
#define MAX_AXIS_ID                6	/* must be last in list */

#define AXIS_LABEL                (1<<AXIS_LABEL_ID)
#define AXIS_HELP                 (1<<AXIS_HELP_ID)
#define AXIS_MIN_VALUE            (1<<AXIS_MIN_VALUE_ID)
#define AXIS_MAX_VALUE            (1<<AXIS_MAX_VALUE_ID)
#define AXIS_SCALING              (1<<AXIS_SCALING_ID)
#define AXIS_CONSTANT_UNIT        (1<<AXIS_CONSTANT_UNIT_ID)

#define AXIS_ATTR_MASKS		      (AXIS_LABEL | AXIS_HELP | AXIS_MIN_VALUE | \
                                   AXIS_MAX_VALUE | AXIS_SCALING | AXIS_CONSTANT_UNIT)

/* AXIS constants */
#define AXIS_SCALING_LINEAR        0
#define AXIS_SCALING_LOGARITHMIC   1

/* CHART attributes */
#define CHART_LABEL_ID              0
#define CHART_HELP_ID               1
#define CHART_MEMBER_ID             2
#define CHART_CYCLE_TIME_ID         3
#define CHART_HEIGHT_ID             4
#define CHART_LENGTH_ID             5
#define CHART_TYPE_ID               6
#define CHART_WIDTH_ID              7
#define CHART_VALID_ID              8
#define MAX_CHART_ID                9	/* must be last in list */

#define CHART_LABEL                 (1<<CHART_LABEL_ID)
#define CHART_HELP                  (1<<CHART_HELP_ID)
#define CHART_MEMBER                (1 << CHART_MEMBER_ID)
#define CHART_CYCLE_TIME            (1 << CHART_CYCLE_TIME_ID)
#define CHART_WIDTH                 (1 << CHART_WIDTH_ID)
#define CHART_HEIGHT                (1 << CHART_HEIGHT_ID)
#define CHART_LENGTH                (1 << CHART_LENGTH_ID)
#define CHART_TYPE                  (1 << CHART_TYPE_ID)

// Post 5.1
#define CHART_VALID                 (1 << CHART_VALID_ID)

#define CHART_ATTR_MASKS            (CHART_LABEL | CHART_HELP | CHART_MEMBER | \
                                     CHART_CYCLE_TIME | CHART_HEIGHT | CHART_WIDTH | \
                                     CHART_LENGTH | CHART_TYPE | CHART_VALID)

/* DISPLAY constants: used by CHART and GRAPH */
#define DISPLAY_SIZE_XX_SMALL         0
#define DISPLAY_SIZE_X_SMALL          1
#define DISPLAY_SIZE_SMALL            2
#define DISPLAY_SIZE_MEDIUM           3
#define DISPLAY_SIZE_LARGE            4
#define DISPLAY_SIZE_X_LARGE          5
#define DISPLAY_SIZE_XX_LARGE         6

/* FILE attributes */
#define FILE_LABEL_ID              0
#define FILE_HELP_ID               1
#define FILE_MEMBER_ID             2
#define MAX_FILE_ID                3

#define FILE_LABEL                 (1<<FILE_LABEL_ID)
#define FILE_HELP                  (1<<FILE_HELP_ID)
#define FILE_MEMBER                (1 << FILE_MEMBER_ID)
#define FILE_ATTR_MASKS            (FILE_LABEL | FILE_HELP | FILE_MEMBER)

/* GRAPH attributes */
#define GRAPH_LABEL_ID          0
#define GRAPH_HELP_ID           1
#define GRAPH_MEMBER_ID         2
#define GRAPH_HEIGHT_ID         3
#define GRAPH_X_AXIS_ID         4
#define GRAPH_WIDTH_ID          5
#define GRAPH_VALID_ID          6
#define GRAPH_CYCLE_TIME_ID     7
#define MAX_GRAPH_ID            8

#define GRAPH_LABEL                 (1 << GRAPH_LABEL_ID)
#define GRAPH_HELP                  (1 << GRAPH_HELP_ID)
#define GRAPH_MEMBER                (1 << GRAPH_MEMBER_ID)
#define GRAPH_HEIGHT                (1 << GRAPH_HEIGHT_ID)
#define GRAPH_X_AXIS                (1 << GRAPH_X_AXIS_ID)
#define GRAPH_WIDTH                 (1 << GRAPH_WIDTH_ID)
#define GRAPH_VALID                 (1 << GRAPH_VALID_ID)
#define GRAPH_CYCLE_TIME			(1 << GRAPH_CYCLE_TIME_ID)
#define GRAPH_ATTR_MASKS            (GRAPH_LABEL | GRAPH_HELP | GRAPH_MEMBER | \
                                     GRAPH_HEIGHT | GRAPH_WIDTH | GRAPH_X_AXIS |\
                                     GRAPH_VALID | GRAPH_CYCLE_TIME)

/* GRID constants */
#define GRID_ORIENTATION_VERTICAL       0
#define GRID_ORIENTATION_HORIZONTAL     1

/* GRID attributes */
#define GRID_LABEL_ID          0
#define GRID_HELP_ID           1
#define GRID_VECTOR_ID         2
#define GRID_HEIGHT_ID         3
#define GRID_WIDTH_ID          4
#define GRID_HANDLING_ID       5
#define GRID_VALIDITY_ID       6
#define GRID_ORIENTATION_ID    7

#define MAX_GRID_ID            8

#define GRID_LABEL                 (1 << GRID_LABEL_ID)
#define GRID_HELP                  (1 << GRID_HELP_ID)
#define GRID_VECTOR                (1 << GRID_VECTOR_ID)
#define GRID_HEIGHT                (1 << GRID_HEIGHT_ID)
#define GRID_WIDTH                 (1 << GRID_WIDTH_ID)
#define GRID_HANDLING              (1 << GRID_HANDLING_ID)
#define GRID_VALIDITY              (1 << GRID_VALIDITY_ID)
#define GRID_ORIENTATION           (1 << GRID_ORIENTATION_ID)
#define GRID_ATTR_MASKS            (GRID_LABEL | GRID_HELP | GRID_VECTOR | \
                                    GRID_HEIGHT | GRID_WIDTH | GRID_HANDLING | \
                                    GRID_VALIDITY | GRID_ORIENTATION)


/* IMAGE attributes */
#define IMAGE_LABEL_ID          0
#define IMAGE_HELP_ID           1
#define IMAGE_PATH_ID           2
#define IMAGE_LINK_ID           3 
#define IMAGE_VALIDITY_ID       4
#define IMAGE_BINARY_ID         5
#define MAX_IMAGE_ID            6

#define IMAGE_LABEL                 (1 << IMAGE_LABEL_ID)
#define IMAGE_HELP                  (1 << IMAGE_HELP_ID)
#define IMAGE_PATH                  (1 << IMAGE_PATH_ID)
#define IMAGE_LINK                  (1 << IMAGE_LINK_ID)
#define IMAGE_VALIDITY              (1 << IMAGE_VALIDITY_ID)
#define IMAGE_BINARY                (1 << IMAGE_BINARY_ID)
#define IMAGE_ATTR_MASKS            (IMAGE_LABEL | IMAGE_HELP | IMAGE_PATH | \
                                    IMAGE_LINK | IMAGE_VALIDITY | IMAGE_BINARY)

/* LIST attributes */
#define LIST_LABEL_ID          0
#define LIST_HELP_ID           1
#define LIST_TYPE_ID           2
#define LIST_COUNT_ID          3
#define LIST_CAPACITY_ID       4
#define LIST_VALID_ID          5
#define MAX_LIST_ID            6

#define LIST_LABEL          (1 << LIST_LABEL_ID)
#define LIST_HELP           (1 << LIST_HELP_ID)
#define LIST_TYPE           (1 << LIST_TYPE_ID)
#define LIST_COUNT          (1 << LIST_COUNT_ID)
#define LIST_CAPACITY       (1 << LIST_CAPACITY_ID)
#define LIST_VALID          (1 << LIST_VALID_ID)
#define LIST_ATTR_MASKS     (LIST_LABEL | LIST_HELP | LIST_TYPE | LIST_COUNT | LIST_CAPACITY | LIST_VALID)

/* SOURCE constants */
#define LINE_COLOR_DEFAULT  0xffffffff

/* SOURCE attributes */
#define SOURCE_LABEL_ID       0
#define SOURCE_HELP_ID        1
#define SOURCE_EMPHASIS_ID    2
#define SOURCE_LINE_TYPE_ID   3
#define SOURCE_MEMBER_ID      4
#define SOURCE_Y_AXIS_ID      5
#define SOURCE_LINE_COLOR_ID      6
#define SOURCE_INIT_ACTIONS_ID    7
#define SOURCE_REFRESH_ACTIONS_ID 8
#define SOURCE_EXIT_ACTIONS_ID    9
#define SOURCE_VALID_ID             10
#define MAX_SOURCE_ID             11

#define SOURCE_LABEL          (1 << SOURCE_LABEL_ID)
#define SOURCE_HELP           (1 << SOURCE_HELP_ID)
#define SOURCE_EMPHASIS       (1 << SOURCE_EMPHASIS_ID)
#define SOURCE_LINE_TYPE      (1 << SOURCE_LINE_TYPE_ID)
#define SOURCE_MEMBER         (1 << SOURCE_MEMBER_ID)
#define SOURCE_Y_AXIS         (1 << SOURCE_Y_AXIS_ID)
#define SOURCE_LINE_COLOR     (1 << SOURCE_LINE_COLOR_ID)
#define SOURCE_INIT_ACTIONS   (1 << SOURCE_INIT_ACTIONS_ID)
#define SOURCE_REFRESH_ACTIONS (1 << SOURCE_REFRESH_ACTIONS_ID)
#define SOURCE_EXIT_ACTIONS     (1 << SOURCE_EXIT_ACTIONS_ID)
#define SOURCE_VALID            (1 << SOURCE_VALID_ID)
#define SOURCE_ATTR_MASKS     (SOURCE_LABEL | SOURCE_HELP | SOURCE_EMPHASIS | \
                               SOURCE_LINE_TYPE | SOURCE_MEMBER | SOURCE_Y_AXIS | \
                               SOURCE_LINE_COLOR | SOURCE_INIT_ACTIONS | \
                               SOURCE_REFRESH_ACTIONS | SOURCE_EXIT_ACTIONS |\
                               SOURCE_VALID)

/* WAVEFORM attributes */
#define WAVEFORM_LABEL_ID               0
#define WAVEFORM_HELP_ID                1
#define WAVEFORM_EMPHASIS_ID            2
#define WAVEFORM_LINE_TYPE_ID           3
#define WAVEFORM_INIT_ACTIONS_ID        5
#define WAVEFORM_REFRESH_ACTIONS_ID     6
#define WAVEFORM_TYPE_ID                7
#define WAVEFORM_X_INITIAL_ID           8
#define WAVEFORM_X_INCREMENT_ID         9
#define WAVEFORM_NUMBER_OF_POINTS_ID    10
#define WAVEFORM_Y_VALUES_ID            11
#define WAVEFORM_X_VALUES_ID            12
#define WAVEFORM_HANDLING_ID            13
#define WAVEFORM_LINE_COLOR_ID          14
#define WAVEFORM_KEY_Y_VALUES_ID        15
#define WAVEFORM_KEY_X_VALUES_ID        16
#define WAVEFORM_Y_AXIS_ID              17
#define WAVEFORM_EXIT_ACTIONS_ID        18
#define WAVEFORM_VALID_ID               19

#define WAVEFORM_LABEL              (1 << WAVEFORM_LABEL_ID              )
#define WAVEFORM_HELP               (1 << WAVEFORM_HELP_ID               )
#define WAVEFORM_EMPHASIS           (1 << WAVEFORM_EMPHASIS_ID           )
#define WAVEFORM_LINE_TYPE          (1 << WAVEFORM_LINE_TYPE_ID          )
#define WAVEFORM_INIT_ACTIONS       (1 << WAVEFORM_INIT_ACTIONS_ID       )
#define WAVEFORM_REFRESH_ACTIONS    (1 << WAVEFORM_REFRESH_ACTIONS_ID    )
#define WAVEFORM_TYPE               (1 << WAVEFORM_TYPE_ID               )
#define WAVEFORM_X_INITIAL          (1 << WAVEFORM_X_INITIAL_ID          )
#define WAVEFORM_X_INCREMENT        (1 << WAVEFORM_X_INCREMENT_ID        )
#define WAVEFORM_NUMBER_OF_POINTS    (1 << WAVEFORM_NUMBER_OF_POINTS_ID    )
#define WAVEFORM_Y_VALUES            (1 << WAVEFORM_Y_VALUES_ID            )
#define WAVEFORM_X_VALUES            (1 << WAVEFORM_X_VALUES_ID            )
#define WAVEFORM_HANDLING            (1 << WAVEFORM_HANDLING_ID            )
#define WAVEFORM_LINE_COLOR          (1 << WAVEFORM_LINE_COLOR_ID          )
#define WAVEFORM_KEY_X_VALUES        (1 << WAVEFORM_KEY_X_VALUES_ID          )
#define WAVEFORM_KEY_Y_VALUES        (1 << WAVEFORM_KEY_Y_VALUES_ID          )
#define WAVEFORM_Y_AXIS             (1 << WAVEFORM_Y_AXIS_ID          )
#define WAVEFORM_EXIT_ACTIONS       (1 << WAVEFORM_EXIT_ACTIONS_ID       )
#define WAVEFORM_VALID              (1 << WAVEFORM_VALID_ID)
#define WAVEFORM_ATTR_MASKS (WAVEFORM_LABEL | WAVEFORM_HELP | WAVEFORM_EMPHASIS | \
                             WAVEFORM_LINE_TYPE | WAVEFORM_INIT_ACTIONS | \
                             WAVEFORM_REFRESH_ACTIONS | WAVEFORM_TYPE | WAVEFORM_X_INITIAL | \
                             WAVEFORM_X_INCREMENT | WAVEFORM_NUMBER_OF_POINTS | \
                             WAVEFORM_Y_VALUES | WAVEFORM_X_VALUES | WAVEFORM_HANDLING | \
                             WAVEFORM_LINE_COLOR | WAVEFORM_KEY_X_VALUES | \
                             WAVEFORM_KEY_Y_VALUES | WAVEFORM_Y_AXIS | WAVEFORM_EXIT_ACTIONS | \
                             WAVEFORM_VALID)

/* CHART constants */
#define CHART_TYPE_GAUGE            0
#define CHART_TYPE_HORIZONTAL_BAR   1
#define CHART_TYPE_SCOPE            2
#define CHART_TYPE_STRIP            3
#define CHART_TYPE_SWEEP            4
#define CHART_TYPE_VERTICAL_BAR     5

/* LINE_TYPE constants : used by SOURCE and WAVEFORM */
#define LINE_TYPE_LOW_LOW_LIMIT     0
#define LINE_TYPE_LOW_LIMIT         1
#define LINE_TYPE_HIGH_LIMIT        2
#define LINE_TYPE_HIGH_HIGH_LIMIT   3
#define LINE_TYPE_TRANSPARENT       4
#define LINE_TYPE_DATA_N            16  /* all higher values are reserved */

/* WAVEFORM types */
#define WAVEFORM_TYPE_XY            0
#define WAVEFORM_TYPE_YT            1
#define WAVEFORM_TYPE_HORIZONTAL    2
#define WAVEFORM_TYPE_VERTICAL      3


/* MENU constants */
#define MENU_STYLE_DIALOG           0
#define MENU_STYLE_WINDOW           1
#define MENU_STYLE_PAGE             2
#define MENU_STYLE_GROUP            3
#define MENU_STYLE_MENU             4
#define MENU_STYLE_USER_STRING      5
#define MENU_STYLE_TABLE            6

#define MENU_STYLE_DEFAULT      MENU_STYLE_MENU

/*
 * Item Attribute Masks for DDOD Item Objects.
 */

/* BLOCK attribute masks */

#define BLOCK_CHARACTERISTIC        (1<<BLOCK_CHARACTERISTIC_ID)
#define BLOCK_LABEL                 (1<<BLOCK_LABEL_ID)
#define BLOCK_HELP                  (1<<BLOCK_HELP_ID)
#define BLOCK_PARAM                 (1<<BLOCK_PARAM_ID)

#define BLOCK_MENU                  (1<<BLOCK_MENU_ID)
#define BLOCK_EDIT_DISP             (1<<BLOCK_EDIT_DISP_ID)
#define BLOCK_METHOD                (1<<BLOCK_METHOD_ID)
#define BLOCK_UNIT                  (1<<BLOCK_UNIT_ID)

#define BLOCK_REFRESH               (1<<BLOCK_REFRESH_ID)
#define BLOCK_WAO                   (1<<BLOCK_WAO_ID)
#define BLOCK_COLLECT               (1<<BLOCK_COLLECT_ID)
#define BLOCK_ITEM_ARRAY            (1<<BLOCK_ITEM_ARRAY_ID)

#define BLOCK_PARAM_LIST            (1<<BLOCK_PARAM_LIST_ID)
#define BLOCK_AXIS                  (1<<BLOCK_AXIS_LIST_ID)
#define BLOCK_CHART                 (1<<BLOCK_CHART_LIST_ID)
#define BLOCK_FILE                  (1<<BLOCK_FILE_LIST_ID)
#define BLOCK_GRAPH                 (1<<BLOCK_GRAPH_LIST_ID)
#define BLOCK_LIST                  (1<<BLOCK_LIST_LIST_ID)
#define BLOCK_SOURCE                (1<<BLOCK_SOURCE_LIST_ID)
#define BLOCK_WAVEFORM              (1<<BLOCK_WAVEFORM_LIST_ID)
#define BLOCK_LOCAL_PARAM           (1<<BLOCK_LOCAL_PARAM_ID)
#define BLOCK_GRID                  (1<<BLOCK_GRID_LIST_ID)
#define BLOCK_IMAGE                 (1<<BLOCK_IMAGE_LIST_ID)
#define BLOCK_CHART_MEMBERS         (1<<BLOCK_CHART_MEMBERS_ID)
#define BLOCK_FILE_MEMBERS          (1<<BLOCK_FILE_MEMBERS_ID)
#define BLOCK_GRAPH_MEMBERS         (1<<BLOCK_GRAPH_MEMBERS_ID)
#define BLOCK_GRID_MEMBERS          (1<<BLOCK_GRID_MEMBERS_ID)
#define BLOCK_MENU_MEMBERS          (1<<BLOCK_MENU_MEMBERS_ID)
#define BLOCK_METHOD_MEMBERS        (1<<BLOCK_METHOD_MEMBERS_ID)
#define BLOCK_LIST_MEMBERS          (1<<BLOCK_LIST_MEMBERS_ID)


#define BLOCK_ATTR_MASKS		   (BLOCK_CHARACTERISTIC | \
									BLOCK_LABEL | \
									BLOCK_HELP | \
									BLOCK_PARAM | \
									BLOCK_MENU | \
									BLOCK_EDIT_DISP | \
									BLOCK_METHOD | \
									BLOCK_UNIT | \
									BLOCK_REFRESH | \
									BLOCK_WAO | \
									BLOCK_COLLECT | \
									BLOCK_ITEM_ARRAY | \
									BLOCK_PARAM_LIST | \
                                    BLOCK_AXIS | \
                                    BLOCK_CHART | \
                                    BLOCK_FILE | \
                                    BLOCK_GRAPH | \
                                    BLOCK_LIST | \
                                    BLOCK_SOURCE | \
                                    BLOCK_WAVEFORM | \
                                    BLOCK_LOCAL_PARAM | \
                                    BLOCK_GRID | \
                                    BLOCK_IMAGE | \
                                    BLOCK_CHART_MEMBERS | \
                                    BLOCK_FILE_MEMBERS | \
                                    BLOCK_GRAPH_MEMBERS | \
                                    BLOCK_GRID_MEMBERS | \
                                    BLOCK_MENU_MEMBERS | \
                                    BLOCK_METHOD_MEMBERS | \
									BLOCK_LIST_MEMBERS)

/* VARIABLE attribute masks */

#define VAR_CLASS                   (1<<VAR_CLASS_ID)
#define VAR_HANDLING                (1<<VAR_HANDLING_ID)
#define VAR_UNIT                    (1<<VAR_UNIT_ID)
#define VAR_LABEL                   (1<<VAR_LABEL_ID)

#define VAR_HELP                    (1<<VAR_HELP_ID)
#define VAR_READ_TIME_OUT           (1<<VAR_READ_TIME_OUT_ID)
#define VAR_WRITE_TIME_OUT          (1<<VAR_WRITE_TIME_OUT_ID)
#define VAR_VALID                   (1<<VAR_VALID_ID)

#define VAR_PRE_READ_ACT            (1<<VAR_PRE_READ_ACT_ID)
#define VAR_POST_READ_ACT           (1<<VAR_POST_READ_ACT_ID)
#define VAR_PRE_WRITE_ACT           (1<<VAR_PRE_WRITE_ACT_ID)
#define VAR_POST_WRITE_ACT          (1<<VAR_POST_WRITE_ACT_ID)
#define VAR_REFRESH_ACT             (1<<VAR_REFRESH_ACT_ID)

#define VAR_PRE_EDIT_ACT            (1<<VAR_PRE_EDIT_ACT_ID)
#define VAR_POST_EDIT_ACT           (1<<VAR_POST_EDIT_ACT_ID)
#define VAR_RESP_CODES              (1<<VAR_RESP_CODES_ID)
#define VAR_TYPE_SIZE               (1<<VAR_TYPE_SIZE_ID)

#define VAR_DISPLAY                 (1<<VAR_DISPLAY_ID)
#define VAR_EDIT                    (1<<VAR_EDIT_ID)
#define VAR_MIN_VAL                 (1<<VAR_MIN_VAL_ID)
#define VAR_MAX_VAL                 (1<<VAR_MAX_VAL_ID)

#define VAR_SCALE                   (1<<VAR_SCALE_ID)
#define VAR_ENUMS                   (1<<VAR_ENUMS_ID)
#define VAR_INDEX_ITEM_ARRAY        (1<<VAR_INDEX_ITEM_ARRAY_ID)
#define VAR_WRITE_MODE              (1<<VAR_WRITE_MODE_ID)

#define VAR_MAIN_MASKS			   (VAR_CLASS | \
									VAR_HANDLING | \
									VAR_HELP | \
									VAR_LABEL | \
									VAR_TYPE_SIZE | \
									VAR_DISPLAY | \
									VAR_EDIT | \
									VAR_ENUMS | \
									VAR_INDEX_ITEM_ARRAY | \
									VAR_RESP_CODES)

#define VAR_MISC_MASKS			   (VAR_UNIT | \
									VAR_READ_TIME_OUT | \
									VAR_WRITE_TIME_OUT | \
									VAR_MIN_VAL | \
									VAR_MAX_VAL | \
									VAR_SCALE | \
									VAR_VALID | \
                                    VAR_WRITE_MODE)

#define	VAR_ACT_MASKS			   (VAR_PRE_EDIT_ACT | \
									VAR_POST_EDIT_ACT | \
									VAR_PRE_READ_ACT | \
									VAR_POST_READ_ACT | \
									VAR_PRE_WRITE_ACT | \
									VAR_POST_WRITE_ACT | \
                                    VAR_REFRESH_ACT)

#define	VAR_ATTR_MASKS			   (VAR_MAIN_MASKS | \
									VAR_MISC_MASKS | \
									VAR_ACT_MASKS)

/* MENU attribute masks */

#define MENU_LABEL                  (1<<MENU_LABEL_ID)
#define MENU_ITEMS                  (1<<MENU_ITEMS_ID)
#define MENU_STYLE                  (1<<MENU_STYLE_ID)
#define MENU_STYLE_STRING           (1<<MENU_STYLE_STRING_ID)
#define MENU_VALID                  (1<<MENU_VALID_ID)

#define	MENU_ATTR_MASKS			   (MENU_LABEL | \
									MENU_ITEMS | \
                                    MENU_STYLE | \
                                    MENU_VALID | \
                                    MENU_STYLE_STRING)

/* EDIT_DISPLAY attribute masks */

#define EDIT_DISPLAY_LABEL          (1<<EDIT_DISPLAY_LABEL_ID)
#define EDIT_DISPLAY_EDIT_ITEMS     (1<<EDIT_DISPLAY_EDIT_ITEMS_ID)
#define EDIT_DISPLAY_DISP_ITEMS     (1<<EDIT_DISPLAY_DISP_ITEMS_ID)
#define EDIT_DISPLAY_PRE_EDIT_ACT   (1<<EDIT_DISPLAY_PRE_EDIT_ACT_ID)
#define EDIT_DISPLAY_POST_EDIT_ACT  (1<<EDIT_DISPLAY_POST_EDIT_ACT_ID)

#define EDIT_DISP_ATTR_MASKS	   (EDIT_DISPLAY_LABEL | \
									EDIT_DISPLAY_EDIT_ITEMS | \
									EDIT_DISPLAY_DISP_ITEMS | \
									EDIT_DISPLAY_PRE_EDIT_ACT | \
									EDIT_DISPLAY_POST_EDIT_ACT)

/* METHOD attribute masks */

#define METHOD_CLASS                (1<<METHOD_CLASS_ID)
#define METHOD_LABEL                (1<<METHOD_LABEL_ID)
#define METHOD_HELP                 (1<<METHOD_HELP_ID)
#define METHOD_DEF                  (1<<METHOD_DEF_ID)
#define METHOD_VALID                (1<<METHOD_VALID_ID)
#define METHOD_SCOPE                (1<<METHOD_SCOPE_ID)

#define	METHOD_ATTR_MASKS		   (METHOD_CLASS | \
									METHOD_LABEL | \
									METHOD_HELP | \
									METHOD_DEF | \
									METHOD_VALID | \
									METHOD_SCOPE)

/* REFRESH attribute masks */

#define REFRESH_ITEMS               (1<<REFRESH_ITEMS_ID)

#define REFRESH_ATTR_MASKS			REFRESH_ITEMS

/* UNIT attribute masks */

#define UNIT_ITEMS                  (1<<UNIT_ITEMS_ID)

#define UNIT_ATTR_MASKS				UNIT_ITEMS

/* WRITE AS ONE attribute masks */

#define WAO_ITEMS                   (1<<WAO_ITEMS_ID)

#define WAO_ATTR_MASKS				WAO_ITEMS

/* ITEM_ARRAY attribute masks */

#define ITEM_ARRAY_ELEMENTS         (1<<ITEM_ARRAY_ELEMENTS_ID)
#define ITEM_ARRAY_LABEL            (1<<ITEM_ARRAY_LABEL_ID)
#define ITEM_ARRAY_HELP             (1<<ITEM_ARRAY_HELP_ID)
#define ITEM_ARRAY_VALID            (1<<ITEM_ARRAY_VALID_ID)

#define	ITEM_ARRAY_ATTR_MASKS	   (ITEM_ARRAY_ELEMENTS | \
									ITEM_ARRAY_LABEL | \
									ITEM_ARRAY_HELP | \
                                    ITEM_ARRAY_VALID)

/* COLLECTION attribute masks */

#define COLLECTION_MEMBERS          (1<<COLLECTION_MEMBERS_ID)
#define COLLECTION_LABEL            (1<<COLLECTION_LABEL_ID)
#define COLLECTION_HELP             (1<<COLLECTION_HELP_ID)

#define	COLLECTION_ATTR_MASKS	   (COLLECTION_MEMBERS | \
									COLLECTION_LABEL | \
									COLLECTION_HELP)

/* PROGRAM attribute masks */

#define PROGRAM_ARGS                (1<<PROGRAM_ARGS_ID)
#define PROGRAM_RESP_CODES          (1<<PROGRAM_RESP_CODES_ID)

#define PROGRAM_ATTR_MASKS		   (PROGRAM_ARGS | \
									PROGRAM_RESP_CODES)

/* RECORD attribute masks */

#define RECORD_MEMBERS              (1<<RECORD_MEMBERS_ID)
#define RECORD_LABEL                (1<<RECORD_LABEL_ID)
#define RECORD_HELP                 (1<<RECORD_HELP_ID)
#define RECORD_RESP_CODES           (1<<RECORD_RESP_CODES_ID)
#define RECORD_VALID                (1<<RECORD_VALID_ID)
#define RECORD_WRITE_MODE           (1<<RECORD_WRITE_MODE_ID)

#define	RECORD_ATTR_MASKS		   (RECORD_MEMBERS | \
									RECORD_LABEL | \
									RECORD_HELP | \
									RECORD_RESP_CODES | \
                                    RECORD_VALID | \
                                    RECORD_WRITE_MODE)

/* ARRAY attribute masks */

#define ARRAY_LABEL                 (1<<ARRAY_LABEL_ID)
#define ARRAY_HELP                  (1<<ARRAY_HELP_ID)
#define ARRAY_TYPE                  (1<<ARRAY_TYPE_ID)
#define ARRAY_NUM_OF_ELEMENTS       (1<<ARRAY_NUM_OF_ELEMENTS_ID)
#define ARRAY_RESP_CODES            (1<<ARRAY_RESP_CODES_ID)
#define ARRAY_VALID                 (1<<ARRAY_VALID_ID)
#define ARRAY_WRITE_MODE            (1<<ARRAY_WRITE_MODE_ID)

#define	ARRAY_ATTR_MASKS		   (ARRAY_LABEL | \
									ARRAY_HELP | \
									ARRAY_TYPE | \
									ARRAY_NUM_OF_ELEMENTS | \
									ARRAY_RESP_CODES |\
                                    ARRAY_VALID | \
                                    ARRAY_WRITE_MODE)

/* VARIABLE LIST attribute masks */

#define VAR_LIST_MEMBERS            (1<<VAR_LIST_MEMBERS_ID)
#define VAR_LIST_LABEL              (1<<VAR_LIST_LABEL_ID)
#define VAR_LIST_HELP               (1<<VAR_LIST_HELP_ID)
#define VAR_LIST_RESP_CODES         (1<<VAR_LIST_RESP_CODES_ID)

#define	VAR_LIST_ATTR_MASKS		   (VAR_LIST_MEMBERS | \
									VAR_LIST_LABEL | \
									VAR_LIST_HELP | \
									VAR_LIST_RESP_CODES)

/* RESPONSE CODE attribute masks */

#define RESP_CODE_MEMBER            (1<<RESP_CODE_MEMBER_ID)

#define RESP_CODE_ATTR_MASKS		RESP_CODE_MEMBER

/* DOMAIN attribute masks */

#define DOMAIN_HANDLING             (1<<DOMAIN_HANDLING_ID)
#define DOMAIN_RESP_CODES           (1<<DOMAIN_RESP_CODES_ID)

#define	DOMAIN_ATTR_MASKS		   (DOMAIN_HANDLING | \
									DOMAIN_RESP_CODES)

/* COMMAND attribute masks */

#define COMMAND_NUMBER              (1<<COMMAND_NUMBER_ID)
#define COMMAND_OPER                (1<<COMMAND_OPER_ID)
#define COMMAND_TRANS               (1<<COMMAND_TRANS_ID)
#define COMMAND_RESP_CODES          (1<<COMMAND_RESP_CODES_ID)

#define	COMMAND_ATTR_MASKS		   (COMMAND_NUMBER | \
									COMMAND_OPER | \
									COMMAND_TRANS | \
									COMMAND_RESP_CODES)

/*
 * Table Identifiers for DDOD Directory Objects.
 * These are also used as bit positions for the
 * Directory Table Masks defined below.
 */

#define	BLOCK_NAME_SIZE		32	/* This is the max size of a block name */

/* DEVICE DIRECTORY tables */

#define	BLK_TBL_ID			 			0
#define	ITEM_TBL_ID					 	1
#define	PROG_TBL_ID					 	2
#define	DOMAIN_TBL_ID				 	3
#define	STRING_TBL_ID				 	4
#define	DICT_REF_TBL_ID				 	5
#define	LOCAL_VAR_TBL_ID				6

#define	RESERVED1_TBL_ID				7
#define AXIS_TBL_ID                     8
#define CHART_TBL_ID                    9
#define FILE_TBL_ID                     10
#define GRAPH_TBL_ID                    11
#define LIST_TBL_ID                     12
#define SOURCE_TBL_ID                   13
#define WAVEFORM_TBL_ID                 14
#define GRID_TBL_ID                     15
#define IMAGE_TBL_ID                    16
#define BINARY_TBL_ID                   17
#define	MAX_DEVICE_TBL_ID			 	18	/* must be last in list and one
                                               greater than last table offset */
#define	MAX_DEVICE_TBL_ID_PRE_5		 	8	/* For 4.x compatibility: do not change */


/* BLOCK DIRECTORY tables */

#define	BLK_ITEM_TBL_ID				 	0
#define	BLK_ITEM_NAME_TBL_ID		 	1
#define PARAM_TBL_ID				 	2
#define	PARAM_MEM_TBL_ID			 	3
#define	PARAM_MEM_NAME_TBL_ID			4
#define PARAM_ELEM_TBL_ID				5
#define	PARAM_LIST_TBL_ID			 	6
#define	PARAM_LIST_MEM_TBL_ID		 	7
#define PARAM_LIST_MEM_NAME_TBL_ID		8
#define	CHAR_MEM_TBL_ID				 	9
#define CHAR_MEM_NAME_TBL_ID			10
#define	REL_TBL_ID					 	11
#define UPDATE_TBL_ID					12

#define	MAX_BLOCK_TBL_ID				13	/* must be last in list and 1 greater than last offset */
#define	MAX_BLOCK_TBL_ID_PRE_5			13	/* For 4.x compatibility: do not change */


/*
 *	Directory Table Masks.
 */

/* DEVICE DIRECTORY table masks */

#define	BLK_TBL_MASK					(1<<BLK_TBL_ID)
#define	ITEM_TBL_MASK					(1<<ITEM_TBL_ID)
#define	PROG_TBL_MASK					(1<<PROG_TBL_ID)
#define	DOMAIN_TBL_MASK					(1<<DOMAIN_TBL_ID)
#define	AXIS_TBL_MASK					(1<<AXIS_TBL_ID)
#define	CHART_TBL_MASK					(1<<CHART_TBL_ID)
#define	FILE_TBL_MASK					(1<<FILE_TBL_ID)
#define	GRAPH_TBL_MASK					(1<<GRAPH_TBL_ID)
#define	LIST_TBL_MASK					(1<<LIST_TBL_ID)
#define	SOURCE_TBL_MASK					(1<<SOURCE_TBL_ID)
#define	WAVEFORM_TBL_MASK				(1<<WAVEFORM_TBL_ID)
#define	STRING_TBL_MASK					(1<<STRING_TBL_ID)
#define	DICT_REF_TBL_MASK				(1<<DICT_REF_TBL_ID)
#define	LOCAL_VAR_TBL_MASK				(1<<LOCAL_VAR_TBL_ID)
#define	GRID_TBL_MASK				    (1<<GRID_TBL_ID)
#define	IMAGE_TBL_MASK				    (1<<IMAGE_TBL_ID)
#define	BINARY_TBL_MASK				    (1<<BINARY_TBL_ID)

#define RESV_DEV_MASK					0

#define DEVICE_TBL_MASKS		BLK_TBL_MASK | \
								ITEM_TBL_MASK | \
								PROG_TBL_MASK | \
								DOMAIN_TBL_MASK | \
								STRING_TBL_MASK | \
								DICT_REF_TBL_MASK | \
								LOCAL_VAR_TBL_MASK | \
                                AXIS_TBL_MASK | \
                                CHART_TBL_MASK | \
                                FILE_TBL_MASK | \
                                GRAPH_TBL_MASK | \
                                LIST_TBL_MASK | \
                                SOURCE_TBL_MASK | \
                                WAVEFORM_TBL_MASK | \
                                GRID_TBL_MASK | \
                                IMAGE_TBL_MASK | \
                                BINARY_TBL_MASK | \
								RESV_DEV_MASK

/* BLOCK DIRECTORY table masks */

#define	BLK_ITEM_TBL_MASK				(1<<BLK_ITEM_TBL_ID)
#define	BLK_ITEM_NAME_TBL_MASK			(1<<BLK_ITEM_NAME_TBL_ID)
#define PARAM_TBL_MASK					(1<<PARAM_TBL_ID)
#define	PARAM_MEM_TBL_MASK				(1<<PARAM_MEM_TBL_ID)
#define PARAM_MEM_NAME_TBL_MASK			(1<<PARAM_MEM_NAME_TBL_ID)
#define PARAM_ELEM_TBL_MASK				(1<<PARAM_ELEM_TBL_ID)
#define	PARAM_LIST_TBL_MASK				(1<<PARAM_LIST_TBL_ID)
#define	PARAM_LIST_MEM_TBL_MASK			(1<<PARAM_LIST_MEM_TBL_ID)
#define PARAM_LIST_MEM_NAME_TBL_MASK	(1<<PARAM_LIST_MEM_NAME_TBL_ID)
#define	CHAR_MEM_TBL_MASK				(1<<CHAR_MEM_TBL_ID)
#define CHAR_MEM_NAME_TBL_MASK			(1<<CHAR_MEM_NAME_TBL_ID)
#define	REL_TBL_MASK					(1<<REL_TBL_ID)
#define	UPDATE_TBL_MASK					(1<<UPDATE_TBL_ID)


#ifndef RESV_BLK_MASK
#define RESV_BLK_MASK			0
#endif /* RESV_BLK_MASK */

#define BLOCK_TBL_MASKS			BLK_ITEM_TBL_MASK | \
								BLK_ITEM_NAME_TBL_MASK | \
								PARAM_TBL_MASK | \
								PARAM_MEM_TBL_MASK | \
								PARAM_MEM_NAME_TBL_MASK | \
								PARAM_ELEM_TBL_MASK | \
								PARAM_LIST_TBL_MASK | \
								PARAM_LIST_MEM_TBL_MASK | \
								PARAM_LIST_MEM_NAME_TBL_MASK | \
								CHAR_MEM_TBL_MASK | \
								CHAR_MEM_NAME_TBL_MASK | \
								REL_TBL_MASK | \
								UPDATE_TBL_MASK  | \
								RESV_BLK_MASK


#endif	/* DDLDEFS_H */
