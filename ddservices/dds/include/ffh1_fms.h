/*  Filename    : ffh1_fms.h                                                 */
/*                                                                           */
/*  Description : This file contains the types and defines for using FMS     */
/*                services, ( now via fbkhost_fms.h )                        */ 
/*                                                                           */
/*****************************************************************************/


#ifndef __FFH1_FMS__
#define __FFH1_FMS__

#include "fbkhost_fms.h"

#endif /* __FFH1_FMS__ */
