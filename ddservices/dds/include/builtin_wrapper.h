#ifndef _CBUILTIN_WRAPPER_H_
#define _CBUILTIN_WRAPPER_H_

#include <evl_lib.h>


extern int display_upcall_cppwrap(ENV_INFO *env_info, char *buffer);
extern int display_continuous_upcall_cppwrap(ENV_INFO *env_info, char *prompt,
                                             ITEM_ID *var_ids, 
                                             SUBINDEX *var_subindices);
extern int display_get_upcall_cppwrap(ENV_INFO *env_info, char *buffer,
                                      P_REF *pc_p_ref,
                                      EVAL_VAR_VALUE *var_value);
extern int display_menu_upcall_cppwrap(ENV_INFO *env_info, char *buffer,
                                       SELECT_LIST *select_list,
                                       int *selection);
extern int display_ack_upcall_cppwrap(ENV_INFO *env_info, char *buffer);
extern long method_start_upcall_cppwrap(ENV_INFO *env_info, ITEM_ID meth_item_id);
extern long method_complete_upcall_cppwrap(ENV_INFO *env_info, ITEM_ID meth_item_id,
                                           int meth_rs);
extern long method_error_upcall_cppwrap(ENV_INFO *env_info, char *errorStr);

#endif
