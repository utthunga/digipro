
/**
 *	@(#)hrtbltin.h	30.2  30  16 Aug 1996
 */

#ifndef HRTBLTIN_H
#define HRTBLTIN_H

#include "env_info.h"

#define NORMAL_CMD		0
#define MORE_STATUS_CMD	1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern long rspcode_string P((long cmdnum, long trans, UINT32 code,
	char *out_buf, long len, ENV_INFO *env_info));

extern void SET_NUMBER_OF_RETRIES P((long num, ENV_INFO *env_info));

extern void set_char_mask P((unsigned char *abort_mask, unsigned char *retry_mask,
	long status, long type));
extern void set_all_resp_mask P((unsigned char *resp_abort_array,
	unsigned char *resp_retry_array, long type));
extern void set_resp_mask P((unsigned char *resp_abort_array,
	unsigned char *resp_retry_array, long status, long type));

extern void set_xmtr_all_data P((long type, ENV_INFO *env_info));
extern void set_xmtr_data P((long byte_num, long bit_num, long type, ENV_INFO *env_info));

extern long send_command P((long cmdnum, long trans, unsigned char *cmd_sts,
	unsigned char *xmtr_cmd_sts, unsigned char *xmtr_sts, int cmd_type,
	int moredata_flag, ENV_INFO *env_info));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /*HRTBLTIN_H*/
