/***********************************************************************
    Copyright (c) 2015 Fluke Corporation, Inc. All rights reserved.
************************************************************************

    Repository URL:    igdev/projects/ace/sw/thirdparty/ff_dds.git
    Authored By:       Jayanth Mahadeva
    Origin:            Hutch
*/

/** @file devsim.h
 * @brief Function prototypes to interact with simulator host or actual host

    This file contains all function prototype which explains the functional
    properties of the host device.
*/

/***********************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
***********************************************************************/

#ifndef	DEVSIM_H
#define	DEVSIM_H


#include "cm_lib.h"
#include "fbapi_if.h"
#include "ff_if.h"
#include "device_access.h"
#include "host.h"

/*!
  \def WALLACE_FBK2_PORT
  Environmental variable to set the tty port to communicate with FBK board.
*/

#define WALLACE_FBK2_PORT				"WALLACE_FBK2_PORT"

/*!
  \def WALLACE_FBK2_NODE
  Environmental variable to set the host address of FBK board.
*/
#define WALLACE_FBK2_NODE               "WALLACE_FBK2_NODE"

#define     NO_LOCK         1

/*
 *	Block Classes
 */

/*!
  \def BC_PHYSICAL
  \def BC_INPUT
  \def BC_OUTPUT
  \def BC_CONTROL
  \def BC_TRANSDUCER
  \def BC_UNKNOWN
  Block classes types.
*/
#define BC_PHYSICAL					  0
#define BC_INPUT					  1
#define BC_OUTPUT					  2
#define BC_CONTROL					  3
#define BC_TRANSDUCER				  4
#define BC_UNKNOWN  				  5

/*!
  \def SOD_STARTING_INDEX
  Static OD starting OD index for the simulator host.
*/
#define SOD_STARTING_INDEX			201

/*!
  \def BLK_TAG_LEN
  Max length of the Block tag name.
*/
#define	BLK_TAG_LEN					 64
#define DD_BLK_NAME_LEN				 32

#define LOCK_ACQUIRED               1
#define LOCK_RELEASED               0

typedef char	BLK_TAG[BLK_TAG_LEN + 1];
typedef char	DD_BLK_NAME[DD_BLK_NAME_LEN + 1];


/*
 *	The Block Entry Table
 */

/*! Block Entry table */
/*
 * \typedef BLK_ENTRY
 * Block Entry table
 */
typedef struct {
	BLK_TAG			blk_tag;				/**< Block tag of length 64 bytes */
	int				station_address;		/**< Node address of the remote FF transmitter */
	int				manufacturer;			/**< Manufacturer of the FF transmitter */
	int				dev_type;				/**< Device Type of the FF transmitter */
	unsigned char   dev_rev;				/**< Device Revision of the FF transmitter */
    unsigned char	dd_rev;					/**< DD Revision of the FF transmitter */
	int				op_index;				/**< Operational Index of the block */
	int				param_count;			/**< Parameter Count of the block */
	DD_BLK_NAME		dd_blk_name;			/**< DD name of the block */
	int				class_type;				/**< Class type of the block */
	unsigned int	dd_item_id;				/**< DD Item ID of the block */
	unsigned int	profile;				/**< Profile/Type of the block */
	unsigned char	mode_block;				/**< Actual mode of the block */
} BLK_ENTRY;

/*
 * \typedef BLK_ENTRY_TBL
 * List of Block Entry table
 */
typedef struct {
	int			  count;					/**< Count of the blocks in transmitter */
	BLK_ENTRY	**list;						/**< pointer to access the block */
} BLK_ENTRY_TBL;


/*
 *	The Find Confirm Table
 */
/*
 * \typedef FIND_CNF_TBL
 * Confirmed blocks in the transmitter
 */
typedef struct {
	int			 count;						/**< Count of the blocks in transmitter */
	BLK_TAG		*blk_tag_list;				/**< Block tag name */
	FIND_CNF_A	*find_cnf_list;				/**< pointer to access the found block in transmitter */
} FIND_CNF_TBL;


/*
 *	The Device Data Table
 */

/*
 * \typedef BLK_DATA
 * Operational elements of the block
 */
typedef struct {
	BLK_TAG			blk_tag;				/**< Block tag name */
    BLK_TAG			dd_tag;			    	/**< DD tag name */
	OBJECT_INDEX 	op_index;				/**< Operational Index of the block */
	int			 	class_type;				/**< Class type of the block */
	int				class_num;				/**< Class type represented in terms of identifier number */
	UINT32	 		dd_blk_id;				/**< DD Item ID of the block */
	int		 		param_count;			/**< Parameter Count of the block */
	unsigned int	profile;				/**< Profile/Type of the block */
	unsigned char	mode_block;				/**< Actual mode of the block */
} BLK_DATA;

typedef struct {
	int			 count;
	BLK_DATA	*list;
} BLK_DATA_TBL;

/*
 * \typedef DEV_DATA
 * Operational elements of the Device
 */
typedef struct {
	int	 			station_address;		/**< Node address of the device */
	DD_DEVICE_ID	dd_device_id;			/**< Device Identification, contains manufacture ID,
	 	 	 	 	 	 	 	 	 	 	 	 Device type and revision of the device */
	int				phys_blk_count;			/**< Number of physical/Resource blocks in device */
	int				funct_blk_count;		/**< Number of Functional blocks in device */
	int				trans_blk_count;		/**< Number of transducer blocks in device */
	BLK_DATA_TBL	blk_data_tbl;			/**< Entry to Block data table */
	int				sod_size;				/**< Size of the static Object description */
	ROD_HANDLE		opod_handle;			/**< handler to operate on operation OD (MIB/FBAP) */
} DEV_DATA;

typedef struct {
	int			 count;
	DEV_DATA	*list;
} DEV_DATA_TBL;


/*
 *	The Network Table
 */

typedef struct {
	int				config_file_id;
	BLK_ENTRY_TBL	blk_entry_tbl;
	FIND_CNF_TBL	find_cnf_tbl;
	DEV_DATA_TBL	dev_data_tbl;
} NETWK;

typedef struct {
	int			 count;
	NETWK		*list;
} NETWK_TBL;

extern NETWK_TBL	netwk_tbl;

/*
 *	Device Simulator Function Prototypes
 *
 *	Note:	These functions are not considered part of the
 *			Connection Manager Library, but are currently
 *			used in the Communication Support module
 *			(cm_comm.c) to communicate with simulated devices.
 *
 */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

# pragma pack (1)
/*
 * \typedef SOFTING_BLOCK_INFO
 * Block header as read from Softing provided FF stack
 */
typedef struct {
	unsigned short dummy;					/**< Dummy parameter */
	char block_tag[DD_BLK_NAME_LEN];		/**< Block Name as found in device */
	unsigned int DD_member_ID;				/**< Device Identification, contains manufacture ID,
	 	 	 	 	 	 	 	 	 	 	 	 Device type and revision of the device */
	unsigned int DD_Item_ID;				/**< DD Item ID of the block */
	unsigned short DD_Revision;				/**< DD Revision of the block */
	unsigned short Profile;					/**< Profile/Type of the block */
	unsigned short Profile_revision;		/**< Revision of the profile, which says custom block or standard */
	unsigned int Execution_time;			/**< execution time of the block */
	unsigned int Period_exec;				/**< Period to execute the block */
	unsigned short no_params;				/**< number of parameters in block */
	unsigned short next_fb_to_exec;			/**< Next function block to execute */
	unsigned short starting_index_views;	/**< Starting index of views */
	unsigned char no_view_3;				/**< Number of View3 parameters */
	unsigned char no_view_4;				/**< number of View4 parameters */
	unsigned short op_index;				/**< operational index of the block */
	char class_type;						/**< Block type (Resource/Transducer/Function) */
	unsigned char mode_block;				/**< Mode of the block */
}SOFTING_BLOCK_INFO;
# pragma pack ()


/*!
  \def INITIATE_REQUEST
  Request Command from application to initiate the Communication reference.
*/
#define INITIATE_REQUEST        1
/*!
  \def GET_OD_REQUEST
  Request Command from application to get Object description.
*/
#define GET_OD_REQUEST          2
/*!
  \def WRITE_VCRL_LOCAL
  Request Command from application to initiate the Communication reference.
*/
#define WRITE_VCRL_LOCAL        3
/*!
  \def GET_OD_SECOND_CL
  Request Command from application to get Object description from FBAP OD.
*/
#define GET_OD_SECOND_CL        5
/*!
  \def SM_IDENTIFY_REQ
  Request Command from application to get Identification packets from device.
*/
#define SM_IDENTIFY_REQ         6
/*!
  \def SM_CLR_NODE_REQ
  Request Command from application to clear the node address of the device.
*/
#define SM_CLR_NODE_REQ         7
/*!
  \def SM_SET_NODE_REQ
  Request Command from application to Set the node address of the device.
*/
#define SM_SET_NODE_REQ         8
/*!
  \def SM_CLR_TAG_REQ
  Request Command from application to clear the PD tag of the device.
*/
#define SM_CLR_TAG_REQ          9
/*!
  \def SM_SET_TAG_REQ
  Request Command from application to set the PD tag of the device.
*/
#define SM_SET_TAG_REQ          10
/*!
  \def WAIT_FOR_COMPLETION
  Request Command from application to wai for the complete transfer of data to FBK2 board.
*/
#define WAIT_FOR_COMPLETION     11
/*!
  \def HNDL_MGMT_READ_REQ
  Request Command from application to read the MIB data.
*/
#define HNDL_MGMT_READ_REQ      12
/*!
  \def HNDL_MGMT_WRITE_REQ
  Request Command from application to write the data to MIB.
*/
#define HNDL_MGMT_WRITE_REQ     13
/*!
  \def HNDL_SM_CONF_REQ
  Request Command from application to Configure System Management of host.
*/
#define HNDL_SM_CONF_REQ        14
/*!
  \def HNDL_DLL_CONF_REQ
  Request Command from application to configure DLL settings.
*/
#define HNDL_DLL_CONF_REQ       15
/*!
  \def HNDL_VCRL_INIT_REQ
  Request Command from application to initiate virtual communication reference in host.
*/
#define HNDL_VCRL_INIT_REQ      16
/*!
  \def HNDL_VCRL_TERM_REQ
  Request Command from application to terminate the VCR initiated in host.
*/
#define HNDL_VCRL_TERM_REQ      17
/*!
  \def HNDL_VCRL_LOAD_REQ
  Request Command from application to load the VCR informations in host.
*/
#define HNDL_VCRL_LOAD_REQ      18
/*!
  \def HNDL_LOCAL_VCR_WR_REQ
  Request Command from application to write VCR configurations in host.
*/
#define HNDL_LOCAL_VCR_WR_REQ   19
/*!
  \def HNDL_NMA_READ_REQ
  Request Command from application to read FBAP parameters.
*/
#define HNDL_NMA_READ_REQ       20
/*!
  \def HNDL_FMS_VCR_WR_REQ
  Request Command from application to write VCR informations in device.
*/
#define HNDL_FMS_VCR_WR_REQ     21
/*!
  \def HNDL_ABORT_CR_REQ
  Request Command from application to abort the CR initiated.
*/
#define HNDL_ABORT_CR_REQ       22
/*!
  \def HNDL_NMA_RESET_REQ
  Request Command from application to reset the NMA.
*/
#define HNDL_NMA_RESET_REQ      23
/*!
  \def HNDL_SCAN_LIST_REQ
  Request Command from application to read the devices connected in FF bus.
*/
#define HNDL_SCAN_LIST_REQ      24
/*!
  \def HNDL_FIND_TAG_QUERY_REQ
  Request Command from application to find the block tag in device.
*/
#define HNDL_FIND_TAG_QUERY_REQ 25
/*!
  \def HNDL_FMS_READ_REQ
  Request Command from application to read FBAP parameters.
*/
#define HNDL_FMS_READ_REQ       26
/*!
  \def HNDL_NMA_DATA_REQ
  Request Command from application to read Network Management access parameters of the device.
*/
#define HNDL_NMA_DATA_REQ       27

enum {
	OBJ_INDEX_ID_HIGH = 3,
	OBJ_INDEX_ID_LOW,
	OBJ_CODE_LOC,
	OBJ_DATA_TYPE,
};

/*
  sm_support: Enables SM services as manager/agent
   � Set PD Tag (Agent) 		Bit 0 		-> Byte 0: 0x80
   � Set Address (Agent) 		Bit 1 		-> Byte 0: 0x40
   � Clear Address (Agent) 		Bit 2 		-> Byte 0: 0x20
   � SM Identify (Agent) 		Bit 3 		-> Byte 0: 0x10
   � Locate FB blocks (Agent) 	Bit 4 		-> Byte 0: 0x08
   � Set PD Tag (Manager) 		Bit 5 		-> Byte 0: 0x04
   � Set Address (Manager) 		Bit 6 		-> Byte 0: 0x02
   � Clear Address (Manager) 	Bit 7 		-> Byte 0: 0x01
   � SM Identify (Manager) 		Bit 8 		-> Byte 1: 0x80
  � Locate FB blocks (Manager) 	Bit 9 		-> Byte 1: 0x40
   � FMS Server Bit 10 						-> Byte 1: 0x20
   � Application Clock Synchronization (Pub/Sub) Bit 11/13 -> Byte 1: 0x10/Byte 1: 0x04
   � FB Scheduling 				Bit 12 		-> Byte 1: 0x08
*/

/*!
  \def SM_IDENTIFY_AGENT
  Application SM Identify settings.
*/
#define SM_IDENTIFY_AGENT      		0x10

/*!
  \def SM_IDENTIFY_MANAGER
  Application SM Identify settings.
*/
#define SM_IDENTIFY_MANAGER      		0x80

/*!
  \def SM_IDENTIFY_MANAGER
  Application SM Identify settings.
*/
#define SM_PDTAG__MANAGER      		0x04

/*!
  \def SM_SET_ADDR_MANAGER
  Application SM Identify settings.
  */
#define SM_SET_ADDR_MANAGER      		0x02

/*!
  \def SM_CLR_ADDR_MANAGER
  Application SM Identify settings.
*/
#define SM_CLR_ADDR_MANAGER      		0x01

/*!
  \def AP_CLOCK_SYNC_INTERVAL
   Application clock interval settings.
*/
#define AP_CLOCK_SYNC_INTERVAL      5

/*!
  \def PRIMARY_TIME_PUB
  Application Time Publisher settings.
*/
#define PRIMARY_TIME_PUB			0x10

/*!
  \def SM_SUPPORT_SM_IDENT
  SM Support Identification setting.
*/
#define SM_SUPPORT_SM_IDENT			0x10

/*!
  \def SM_SUPPORT_SET_PD_TAG
  SM Support Set PD Tag setting.
*/
#define SM_SUPPORT_SET_PD_TAG		0x04

/*!
  \def SM_SUPPORT_SET_ADDR
  SM Support Set Address setting.
*/
#define SM_SUPPORT_SET_ADDR			0x02

/*!
  \def SM_SUPPORT_CLR_ADDR
  SM Support to Clear Address setting.
*/
#define SM_SUPPORT_CLR_ADDR			0x01


/*!
  \def SM_SUPPORT_SM_IDENT_MNGR
  SM Support to SM Identify Manager setting.
*/
#define SM_SUPPORT_SM_IDENT_MNGR	0x80

/*!
  \def SM_SUPPORT_LOC_FB_BLOCKS
  SM Support to Locate FB Blocks in device.
*/
#define SM_SUPPORT_LOC_FB_BLOCKS	0x40

/*!
  \def SM_SUPPORT_APP_SUB_CLK_SYNC
  SM Support to Application subscription clock synchronization setting.
*/
#define SM_SUPPORT_APP_SUB_CLK_SYNC	0x04


/*!
  \def SM_SUPPORT_SET_ADDR_TIMER_1
  SM Support to SM Set timer 1.
*/
#define SM_SUPPORT_SET_ADDR_TIMER_1	480000

/*!
  \def SM_SUPPORT_SET_ADDR_TIMER_2
  SM Support to SM Set timer 2.
*/
#define SM_SUPPORT_SET_ADDR_TIMER_2	1440000

/*!
  \def SM_SUPPORT_SET_ADDR_TIMER_3
  SM Support to SM Set timer 3.
*/
#define SM_SUPPORT_SET_ADDR_TIMER_3	3840000

/*!
  \def MAX_INVOKE_ID
  max Invoke ID.
*/
#define MAX_INVOKE_ID		125

/* FBKHOST reset */
/*!
  \def FBK_FIRMWARE_RESET
   Reset only the firmware of the FBK2 board.
*/
#define FBK_FIRMWARE_RESET          1
/*!
  \def FBK_HW_RESET
   Reset FBK2 hardware.
*/
#define FBK_HW_RESET                0

/*!
 \def r_reason(x)
  Get the corresponding reason code for the response from FBK2 board.
*/
#define r_reason(x) (x + 2900)

/* Get the absolute error code */

#define CS_SM_ERR(x) (x + 0x30000)
#define CS_FMS_ERR(x) (x + 0x10000)
#define CS_NMA_ERR(x) (x + 0x20000)

/*!
 \def CON_BUSY_RECEIVED
  Busy confirmation from the FBK board.
*/
#define CON_BUSY_RECEIVED 255

# pragma pack (1)
/*
 * \typedef OD_DESCRIPTION_HDR
 * Object Dictionary description Header
 */
typedef struct	{
	UINT8			ram_rom_flag;					/**< This attribute of type Boolean describes whether modifications in the OD are allowed */
	UINT8			name_length;					/**< This field gives the length of the name and may take only the values */
	UINT8			access_protection_flag;			/**< This attribute states, whether access rights for password, access groups and all communication partners are supported */
	UINT16			version;						/**< Gives the version of the OD */
	UINT16			stod_object_count;				/**< This attribute gives the maximal number of available entries in the Static List of Types */
	OBJECT_INDEX	sod_first_index;				/**< The first available index in the Static Object Dictionary */
	UINT16			sod_object_count;				/**< This attribute gives the maximum number of available entries in the Static Object Dictionary */
	OBJECT_INDEX	dvod_first_index;				/**< First available index in the Dynamic List of Variable Lists. */
	UINT16			dvod_object_count;				/**< This attribute gives the maximum number of available entries in the Dynamic List of Variable Lists */
	OBJECT_INDEX	dpod_first_index;				/**< First available index in the Dynamic List of Program Invocations */
	UINT16			dpod_object_count;				/**< This attribute gives the maximum number of available entries in the Dynamic List of Program Invocations */
} OD_DESCRIPTION_HDR;
# pragma pack ()

typedef struct _T_USR_GET_OD_EVAL
{
    USIGN8      length;
    union
    {
         USIGN16    index;
         USIGN8     val [2];

    }    id;

    USIGN8      obj_type;

} T_USR_GET_OD_EVAL;

/*
 * \var global_res_buf
 * \brief The buffer to receive the response data from FBK board.
 */
USIGN8                      global_res_buf[512];

FB_BOOL                     mgmt_service_loc;

extern int sim_init P((void));
extern int sim_init_network P((int, int));
extern int sim_initiate_comm P((int,int,CREF *,CREF *,CREF *));
extern int sim_comm_get P((CREF,OBJECT_INDEX,OBJECT **));
extern int sim_comm_put P((CREF,OBJECT_INDEX,OBJECT *));
extern int sim_comm_read P((CREF,OBJECT_INDEX,SUBINDEX,UINT8 **,OBJECT **,int *));
extern int sim_comm_write P((CREF,OBJECT_INDEX,SUBINDEX,UINT8 *));
extern int sim_comm_upload P((CREF,OBJECT_INDEX,UINT8 *));
extern int sim_find_block P((char *,int,FIND_CNF_A **));
extern int sim_param_read P((ENV_INFO *, int, SUBINDEX)) ;
extern int sim_param_write P((ENV_INFO *, int, SUBINDEX)) ;
extern int sim_param_load P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
extern int sim_param_unload P((ENV_INFO *,int, SUBINDEX, COMM_VALUE_LIST *)) ;
extern int sim_comm_get_sim_response_code P((int, NETWORK_HANDLE,
		NET_TYPE, UINT16, UINT32 *)) ;
extern int sim_device_table P((ENV_INFO *, NETWORK_HANDLE));
extern int sim_get_host_info(ENV_INFO *, T_FBAPI_DEVICE_INFO *);
extern int sim_detach_device P((ENV_INFO *, NETWORK_HANDLE));
extern int sim_cm_exit P((ENV_INFO *));


int  softing_nm_read_live_list (USIGN8 *, SCAN_DEV_TBL* );
int  softing_nm_read_data (USIGN8 *, NETWORK_HANDLE, int, int );

int send_wait_receive_cmd(ENV_INFO *, unsigned int , FBAP_NMA_REQ *, SM_REQ *);

extern int softing_stack_init P((ENV_INFO *, HOST_CONFIG *));
extern int softing_config_stack P((ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG *));
extern int softing_get_stack_config P((ENV_INFO *, CM_NMA_INFO *));
extern int softing_cm_exit P((ENV_INFO *));
extern int softing_firmware_download P((ENV_INFO *, char *));
extern int softing_init_network P((int ));
extern int softing_initiate_comm P((NETWORK_HANDLE,int,CREF *,CREF *,CREF *));
extern int softing_device_table P((ENV_INFO *, NETWORK_HANDLE));
extern int softing_detach_device P((ENV_INFO *, NETWORK_HANDLE));
extern int softing_find_block P((char *,int,FIND_CNF_A **));
extern int softing_comm_get P((CREF,OBJECT_INDEX,OBJECT **));
extern int softing_get_host_info P((ENV_INFO *, T_FBAPI_DEVICE_INFO *));
extern int softing_reset_fbk P((ENV_INFO *, NETWORK_HANDLE, HOST_CONFIG *));
extern int softing_set_appl_state P((ENV_INFO *, int *, int));
extern int softing_get_actual_index P((unsigned short *, int *, unsigned short *));
extern int softing_read_abs_index P((ENV_INFO *, unsigned short, int,
		unsigned short, void *));
extern int softing_abort_communication P((ENV_INFO *, unsigned short, unsigned short));

extern int softing_object_copy P((OBJECT **, OBJECT *));
extern void bit_rev(unsigned char *val,unsigned int size);

int init_comm_thread P((int));


SOFTING_BLOCK_INFO * convert_ltl_endian(SOFTING_BLOCK_INFO *softing_block);

int get_class_vlaue(int code);
int read_data_device(ENV_INFO *, NETWORK_HANDLE);
void print_block_info(SOFTING_BLOCK_INFO *);
int add_block_entry P((SOFTING_BLOCK_INFO *, BLK_ENTRY *, BLK_ENTRY_TBL *,
                FIND_CNF_TBL *, DEV_DATA_TBL *,	NETWORK_HANDLE));
int del_device_entry(ENV_INFO *, NETWORK_HANDLE);


extern int softing_param_load P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
extern int softing_comm_read P((ENV_INFO *, UINT16, OBJECT_INDEX,SUBINDEX,UINT8 **,OBJECT **,int *));
extern int softing_comm_write P((ENV_INFO *, UINT16, OBJECT_INDEX , SUBINDEX , UINT8 *, unsigned short ));
extern int softing_param_read P((ENV_INFO *, int, SUBINDEX)) ;
extern int softing_param_write P((ENV_INFO *, int, SUBINDEX)) ;

extern int new_network P((int));
extern int initialize_tables P((int));
extern int build_devices P((int));
extern int store_block_entry P((BLK_ENTRY *, BLK_ENTRY_TBL *));
extern int store_find_confirm P((BLK_ENTRY *, FIND_CNF_TBL *, NET_TYPE));
extern int store_device_data P((BLK_ENTRY *, DEV_DATA_TBL *, int));
extern int cfg_file_id_to_network_num P((int , int *));
extern int validate_device_entry P((ENV_INFO *, NT_COUNT));
unsigned int convert_to_ltl_endian(unsigned int);
int configure_host(HOST_CONFIG *);
int initialize_host(HOST_CONFIG *);

extern void log_error P((T_ERROR *));
/*
 * Synchronization Locking helper functions
 */
int is_sync_lock_acquired (void);
int is_read_lock_acquired (void);
int is_write_lock_acquired (void);
int acquire_write_lock (void);
int release_write_lock (void);
int acquire_read_lock (void);
int release_read_lock (void);

/*
 * Events and Indication return codes.
 */

extern int(*softing_stack_events)(ENV_INFO *, char *);

extern int handle_cr_timeout(int, ENV_INFO *, unsigned int, FBAP_NMA_REQ *,
		SM_REQ *);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* DEVSIM_H */

