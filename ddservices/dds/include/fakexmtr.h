/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/* 
 *	@(#)fakexmtr.h	30.3  30  14 Nov 1996
 */

#ifndef _FAKEXMTR_H
#define _FAKEXMTR_H

#include "responsa.h"
#include "cm_lib.h"
#include "fbapi_if.h"
#include "ff_if.h"

/* global NAN variable */
#define NAN_LEN     sizeof(float)
#define NAN_STRING  "NaN"

/* global mode variable */
#define NORMAL 1
#define SIMULATION 2

/* global connection variables */
#define CONNECTED	0
#define DISCONNECTED	1

#define MAX_SIM_DEVICES 16
#define MAX_SIM_NETWORKS 16

#define XMTR_FILE "fakexmtr.txt"
 
typedef union {
    unsigned char   s[NAN_LEN];
    float           f;
}	NAN_TYPE;


typedef struct {	/* fake xmtr's copy of vars */
	ITEM_ID id;		/* variable's identifier */
	COMM_VALUE cv;	/* variable's value */
} SIM_VAR;	

typedef struct {	/* fake xmtr's copy of vars */
	int state;		/* CONNECTED or DISCONNECTED */
	int count;
	int limit;
	SIM_VAR *list;	/* malloc'd list of simulated device variables */
} SIM_DEVICE;	

typedef struct {
	int cfg_id;		/* config_file_id gotten from conn_mgr via network handle */
	int dev_count;		/* num of devices on network */
	SIM_DEVICE dev_list[MAX_SIM_DEVICES];
} SIM_NETWORK;

typedef struct {
	int net_count;
	SIM_NETWORK net_list[MAX_SIM_NETWORKS];
} SIM_NETWORK_LIST;

int get_sim_info (T_FBAPI_DEVICE_INFO *);
int sim_scan_network(ENV_INFO* , SCAN_DEV_TBL *);
int sim_sm_identify (ENV_INFO* ,unsigned short , CR_SM_IDENTIFY_CNF* );

#endif /* _FAKEXMTR_H */
