#ifndef DDL_TAGS_H
#define DDL_TAGS_H

/*
 *	Disable the header strings feature for the time being.
 */
 /* note:  the following line is not in the DDS version of this file */
#undef FMS_DD_STRS

/*******************************************
 *******************************************
 **										  **
 **		Attribute Related Definitions	  **
 **										  **
 *******************************************
 *******************************************/

/*
 * Item tags.
 */

#define VARIABLE_TAG 1
#define COMMAND_TAG 2
#define MENU_TAG 3
#define EDIT_DISPLAY_TAG 4
#define METHOD_TAG 5
#define REFRESH_TAG 6
#define UNIT_TAG 7
#define WRITE_AS_ONE_TAG 8
#define ITEM_ARRAY_TAG 9
#define COLLECTION_TAG 10
#define RELATION_TAG 11


/*
 * Attribute tags.
 */

#define MIN_ATTR	(CLASS_TAG-1)	/* This must be < Minimum Attr number */

#define CLASS_TAG 20
#define CONSTANT_UNIT_TAG 21
#define HANDLING_TAG 22
#define HELP_TAG 23
#define LABEL_TAG 24
#define PRE_EDIT_TAG 25
#define POST_EDIT_TAG 26
#define PRE_READ_TAG 27
#define POST_READ_TAG 28
#define PRE_WRITE_TAG 29
#define POST_WRITE_TAG 30
#define READ_TIMEOUT_TAG 31
#define WRITE_TIMEOUT_TAG 32
#define TYPE_TAG 33
#define DISPLAY_FORMAT_TAG 34
#define EDIT_FORMAT_TAG 35
#define MAX_VALUE_TAG 36
#define MIN_VALUE_TAG 37
#define SCALING_FACTOR_TAG 38
#define VALIDITY_TAG 39
#define NUMBER_TAG 40
#define OPERATION_TAG 41
#define TRANSACTION_TAG 42
#define RESPONSE_CODES_TAG 43
#define MENU_ITEMS_TAG 44
#define DISPLAY_ITEMS_TAG 45
#define EDIT_ITEMS_TAG 46
#define DEFINITION_TAG 47
#define ELEMENTS_TAG 48
#define MEMBERS_TAG 49
#define ITEM_ARRAYNAME_TAG 50

#define MAX_ATTR	(ITEM_ARRAYNAME_TAG+1)	/* This must be > Maximum Attr
                         * number */

/*
 * Miscellaneous tags.
 */

#define REFERENCE_SEQLIST_TAG 75
#define ENUMERATOR_SEQLIST_TAG 76
#define ENUMERATOR_TAG 77
#define DATA_ITEMS_SEQLIST_TAG 78
#define RSPCODES_SEQLIST_TAG 79
#define RESPONSE_CODE_TAG 80
#define MENU_ITEM_SEQLIST_TAG 81
#define ELEMENT_SEQLIST_TAG 82
#define ITEM_ARRAY_ELEMENT_TAG 83
#define MEMBER_SEQLIST_TAG 84
#define MEMBER_TAG 85
#define EXPRESSION_TAG 86
#define RANGE_LIST_TAG 87
#define MENU_ITEM_50_SEQLIST_TAG 88 /* New form of menu items */
#define VECTORS_SEQLIST_TAG 89
#define VECTOR_SEQLIST_TAG 90
#define VECTOR_ITEM_TAG 91
#define BINARY_LIST_TAG 92

/*
 * Import tags.
 */

#define IMPORT_TAG 100
#define ALL_IMPORT_TAG 101
#define TYPE_IMPORT_TAG 102
#define OBJECT_IMPORT_TAG 103
#define DELETE_TAG 104
#define REDEFINE_TAG 105
#define ADD_TAG 106

#define ATTR_DELETE_TAG 0
#define ATTR_REDEFINE_TAG 1
#define ATTR_ADD_TAG 2

/*
 * Expression opcodes.
 */

#define NOT_OPCODE 1
#define NEG_OPCODE 2
#define BNEG_OPCODE 3
#define ADD_OPCODE 4
#define SUB_OPCODE 5
#define MUL_OPCODE 6
#define DIV_OPCODE 7
#define MOD_OPCODE 8
#define LSHIFT_OPCODE 9
#define RSHIFT_OPCODE 10
#define AND_OPCODE 11
#define OR_OPCODE 12
#define XOR_OPCODE 13
#define LAND_OPCODE 14
#define LOR_OPCODE 15
#define LT_OPCODE 16
#define GT_OPCODE 17
#define LE_OPCODE 18
#define GE_OPCODE 19
#define EQ_OPCODE 20
#define NEQ_OPCODE 21
#define INTCST_OPCODE 22
#define FPCST_OPCODE 23
#define VARID_OPCODE 24
#define MAXVAL_OPCODE 25
#define MINVAL_OPCODE 26
#define VARREF_OPCODE 27
#define MAXREF_OPCODE 28
#define MINREF_OPCODE 29
#define BLOCK_OPCODE 30
#define BLOCKID_OPCODE 31
#define BLOCKREF_OPCODE 32
#define SELECTORVAL_OPCODE 33
/*
 * Import by type flags.
 */

#define IMPORT_VARIABLES 1
#define IMPORT_COMMANDS 2
#define IMPORT_METHODS 4
#define IMPORT_MENUS 8
#define IMPORT_EDIT_DISPLAYS 16
#define IMPORT_RELATIONS 32
#define IMPORT_ITEM_ARRAYS 64
#define IMPORT_COLLECTIONS 128


/*
 *	conditional tags
 */

#define IF_TAG 		0
#define SELECT_TAG 	1
#define CASE_TAG	0
#define DEFAULT_TAG	1
#define OBJECT_TAG	2


/*
 * 	STRING tags
 */

#define DEV_SPEC_STRING_TAG     0	/* string device specific id */
#define VARIABLE_STRING_TAG     1	/* string variable id */
#define ENUMERATION_STRING_TAG  2	/* enumeration string information */
#define DICTIONARY_STRING_TAG   3	/* dictionary string id */
#define VAR_REF_STRING_TAG      4	/* variable_reference_string */
#define ENUM_REF_STRING_TAG     5	/* enumerated reference string */


/*
 *	Enumeration tags
 */

#define	ENUM_VALUE_TAG		0
#define	ENUM_STATUS_TAG		1
#define	ENUM_ACTIONS_TAG	2
#define	ENUM_DESC_TAG		3
#define	ENUM_HELP_TAG		4
#define	ENUM_CLASS_TAG		5


/*
 *	Transaction tags
 */

#define	TRANS_REQ_TAG			0
#define	TRANS_REPLY_TAG			1
#define	TRANS_RESP_CODES_TAG	2


/*
 *	Block Item Name Table tags
 */

#define BINT_BLK_ITEM_NAME_TAG				0
#define BINT_ITEM_TBL_OFFSET_TAG			1
#define BINT_PARAM_TBL_OFFSET_TAG			2
#define BINT_PARAM_LIST_TBL_OFFSET_TAG		3
#define BINT_REL_TBL_OFFSET_TAG				4
#define BINT_READ_CMD_TBL_OFFSET_TAG		5
#define BINT_READ_CMD_TBL_COUNT_TAG			6
#define BINT_WRITE_CMD_TBL_OFFSET_TAG		7
#define BINT_WRITE_CMD_TBL_COUNT_TAG		8


/*
 *	Parameter Table tags
 */

#define PT_BLK_ITEM_NAME_TBL_OFFSET_TAG		0
#define PT_PARAM_MEM_TBL_OFFSET_TAG			1
#define PT_PARAM_MEM_COUNT_TAG				2
#define PT_PARAM_ELEM_TBL_OFFSET_TAG		3
#define PT_PARAM_ELEM_COUNT_TAG				4
#define PT_PARAM_ELEM_MAX_COUNT_TAG			5
#define PT_ARRAY_ELEM__ITEM_TBL_OFFSET_TAG	6
#define PT_ARRAY_ELEM_TYPE_OR_VAR_TYPE_TAG	7
#define PT_ARRAY_ELEM_SIZE_OR_VAR_SIZE_TAG	8
#define PT_ARRAY_ELEM_CLASS_VAR_CLASS_TAG	9


/*
 * Reference types.
 * NOTE- An array is created as part of the tokenizer verify
 * subsystem which is indexed by these reference type numbers.
 * For this reason, the numbers must be positive numbers which
 * start at or near zero and are reasonably consecutive.  The
 * The array which is indexed is created at run-time, so the
 * order/values of the defines may be changed without reflecting
 * the change elsewhere.
 */

#define ITEM_ID_REF				0
#define ITEM_ARRAY_ID_REF		1
#define COLLECTION_ID_REF		2

#define VIA_ITEM_ARRAY_REF		3
#define VIA_COLLECTION_REF		4
#define VIA_RECORD_REF			5
#define VIA_ARRAY_REF			6
#define VIA_VAR_LIST_REF		7
#define VIA_PARAM_REF			8
#define VIA_PARAM_LIST_REF		9
#define VIA_BLOCK_REF			10

#define BLOCK_ID_REF			11
#define VARIABLE_ID_REF			12
#define MENU_ID_REF				13
#define EDIT_DISP_ID_REF		14
#define METHOD_ID_REF			15
#define REFRESH_ID_REF			16
#define UNIT_ID_REF				17
#define WAO_ID_REF				18
#define RECORD_ID_REF			19
#define ARRAY_ID_REF			20
#define VAR_LIST_ID_REF			21
#define PROGRAM_ID_REF			22
#define DOMAIN_ID_REF			23
#define RESP_CODES_ID_REF		24
#define AXIS_ID_REF             25
#define CHART_ID_REF            26
#define FILE_ID_REF             27
#define GRAPH_ID_REF            28
#define LIST_ID_REF             29
#define SOURCE_ID_REF           30
#define WAVEFORM_ID_REF         31

#define VIA_SELECTOR_REF        32
#define VIA_CHART_REF           33
#define VIA_FILE_REF            34
#define VIA_GRAPH_REF           35
#define VIA_SOURCE_REF          36
#define VIA_LOCAL_PARAM_REF     37

#define GRID_ID_REF             38
#define IMAGE_ID_REF            39

#define VIA_BIT_ENUM_REF         40
#define VIA_LIST_REF             41
#define BLOCK_ID_XREF            42  /* Crossblock reference (block ID) */
#define VIA_BLOCK_XREF           43  /* Crossblock reference (instance index) */
#define VIA_BCHART_REF           44  /* Block->Chart reference */
#define VIA_BFILE_REF            45  /* Block->File reference */
#define VIA_BGRAPH_REF           46  /* Block->Graph reference */
#define VIA_BGRID_REF            47  /* Block->Grid reference */
#define VIA_BMENU_REF            48  /* Block->Menu reference */
#define VIA_BMETHOD_REF          49  /* Block->Method reference */

/*******************************************
 *******************************************
 **										  **
 **		 Object Related Definitions		  **
 **										  **
 *******************************************
 *******************************************/

/*
 *	Definitions pertaining to the Header.
 */

    /*   Sizes of the Header data fields (in octets).   */

#define	MAGIC_NUMBER_SIZE			4
#define	HEADER_SIZE_SIZE			4
#define	OBJECTS_SIZE_SIZE			4
#define	DATA_SIZE_SIZE				4
#define	MANUFACTURER_SIZE			3
#define	DEVICE_TYPE_SIZE			2
#define	DEVICE_REV_SIZE				1
#define	DD_REV_SIZE					1
#define	RESERVED1_SIZE				4
#define	RESERVED2_SIZE				4
#define	RESERVED3_SIZE				4
#define	RESERVED4_SIZE				4


#define	HEADER_SIZE					(MAGIC_NUMBER_SIZE +	\
									 HEADER_SIZE_SIZE +		\
									 OBJECTS_SIZE_SIZE +	\
									 DATA_SIZE_SIZE +		\
									 MANUFACTURER_SIZE +	\
									 DEVICE_TYPE_SIZE +		\
									 DEVICE_REV_SIZE +		\
									 DD_REV_SIZE +			\
									 RESERVED1_SIZE +		\
									 RESERVED2_SIZE +		\
									 RESERVED3_SIZE +		\
									 RESERVED4_SIZE)

	/*   Offsets of the Header data fields (in octets).   */

#define	MAGIC_NUMBER_OFFSET			0

#define	HEADER_SIZE_OFFSET			(MAGIC_NUMBER_OFFSET +	\
									 MAGIC_NUMBER_SIZE)

#define	OBJECTS_SIZE_OFFSET			(HEADER_SIZE_OFFSET +	\
									 HEADER_SIZE_SIZE)

#define	DATA_SIZE_OFFSET			(OBJECTS_SIZE_OFFSET +	\
									 OBJECTS_SIZE_SIZE)

#define	MANUFACTURER_OFFSET			(DATA_SIZE_OFFSET +		\
									 DATA_SIZE_SIZE)

#define	DEVICE_TYPE_OFFSET			(MANUFACTURER_OFFSET +	\
									 MANUFACTURER_SIZE)

#define	DEVICE_REV_OFFSET			(DEVICE_TYPE_OFFSET +	\
									 DEVICE_TYPE_SIZE)

#define	DD_REV_OFFSET				(DEVICE_REV_OFFSET +	\
									 DEVICE_REV_SIZE)

#define	RESERVED1_OFFSET			(DD_REV_OFFSET +		\
									 DD_REV_SIZE)


#define	RESERVED2_OFFSET			(RESERVED1_OFFSET +		\
									 RESERVED1_SIZE)

#define	RESERVED3_OFFSET			(RESERVED2_OFFSET +		\
									 RESERVED2_SIZE)

#define	RESERVED4_OFFSET			(RESERVED3_OFFSET +		\
									 RESERVED3_SIZE)

/*
 *	Definitions pertaining to Format Object extension.
 *
 *   Sizes of the extension data fields (in octets).
 */

#define FORMAT_OBJ_TYPE_SIZE		1
#define	CODING_FMT_MAJOR_SIZE		1
#define	CODING_FMT_MINOR_SIZE		1
#define	DD_REVISION_SIZE			1
#define	PROFILE_NUM_SIZE			2
#define	NUM_IMPORTS_SIZE			2
#define	NUM_LIKES_SIZE				2

/*
 *  Tokenizer type data field (in bits).  It's the upper bit(s) in the
 *  minor rev number byte; create a handy mask to ease data extraction.
 */

#define	TOK_TYPE_SIZE		1
#define	TOK_TYPE_MASK		(unsigned char) \
                                (~0 << ((CODING_FMT_MINOR_SIZE * 8) - TOK_TYPE_SIZE))

#define FORMAT_EXTENSION_SIZE		(EXTEN_LENGTH_SIZE +		\
                                     FORMAT_OBJ_TYPE_SIZE +		\
                                     CODING_FMT_MAJOR_SIZE +	\
                                     CODING_FMT_MINOR_SIZE +	\
                                     DD_REVISION_SIZE +			\
                                     PROFILE_NUM_SIZE +			\
                                     NUM_IMPORTS_SIZE +			\
                                     NUM_LIKES_SIZE)

    /*   Offsets of the extension data fields (in octets).   */

#define FORMAT_OBJ_TYPE_OFFSET		(EXTEN_LENGTH_OFFSET +		\
                                     EXTEN_LENGTH_SIZE)
#define	CODING_FMT_MAJOR_OFFSET		(FORMAT_OBJ_TYPE_OFFSET +	\
                                     FORMAT_OBJ_TYPE_SIZE)
#define	CODING_FMT_MINOR_OFFSET		(CODING_FMT_MAJOR_OFFSET +	\
                                     CODING_FMT_MAJOR_SIZE)
#define	DD_REVISION_OFFSET			(CODING_FMT_MINOR_OFFSET +	\
                                     CODING_FMT_MINOR_SIZE)
#define	PROFILE_NUM_OFFSET			(DD_REVISION_OFFSET +		\
                                     DD_REVISION_SIZE)
#define	NUM_IMPORTS_OFFSET			(PROFILE_NUM_OFFSET +		\
                                     PROFILE_NUM_SIZE)
#define	NUM_LIKES_OFFSET			(NUM_IMPORTS_OFFSET +		\
                                     NUM_IMPORTS_SIZE)

/*
 *	Definitions pertaining to directory object extensions.
 */

    /*   Sizes of the extension data fields (in octets).   */

#define DIR_TYPE_SIZE				1
#define	FORMAT_CODE_SIZE			1
#define	DATAFIELD_OFFSET_SIZE		2
#define	DATAFIELD_SIZE_SIZE			2

#define	DATAFIELD_OFFSET_SIZE2		4
#define	DATAFIELD_SIZE_SIZE2		4

#define TABLE_REF_LEN       		(DATAFIELD_OFFSET_SIZE + \
                                     DATAFIELD_SIZE_SIZE)

#define TABLE_REF_LEN2       		(DATAFIELD_OFFSET_SIZE2 + \
                                     DATAFIELD_SIZE_SIZE2)

    /*   Offsets of the extension data fields (in octets).   */

#define DIR_TYPE_OFFSET				(EXTEN_LENGTH_OFFSET +	\
                                     EXTEN_LENGTH_SIZE)
#define	FORMAT_CODE_OFFSET			(DIR_TYPE_OFFSET +	\
                                     DIR_TYPE_SIZE)
#define	DIRECTORY_DATA_OFFSET		(FORMAT_CODE_OFFSET +	\
                                     FORMAT_CODE_SIZE)

#define BLK_REF_OFFSET					DIRECTORY_DATA_OFFSET
#define ITEM_REF_OFFSET					(BLK_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PROG_REF_OFFSET					(ITEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define DOMAIN_REF_OFFSET				(PROG_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define STRING_REF_OFFSET				(DOMAIN_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define DICT_REF_REF_OFFSET				(STRING_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define LOCAL_VAR_REF_OFFSET			(DICT_REF_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define CMD_NUM_ID_REF_OFFSET			(LOCAL_VAR_REF_OFFSET + \
                                         TABLE_REF_LEN)

#define BLK_REF_OFFSET2					DIRECTORY_DATA_OFFSET
#define ITEM_REF_OFFSET2					(BLK_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PROG_REF_OFFSET2					(ITEM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define DOMAIN_REF_OFFSET2				(PROG_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define STRING_REF_OFFSET2				(DOMAIN_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define DICT_REF_REF_OFFSET2				(STRING_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define LOCAL_VAR_REF_OFFSET2			(DICT_REF_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define CMD_NUM_ID_REF_OFFSET2			(LOCAL_VAR_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define AXIS_REF_OFFSET2				(CMD_NUM_ID_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define CHART_REF_OFFSET2				(AXIS_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define FILE_REF_OFFSET2				(CHART_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define GRAPH_REF_OFFSET2				(FILE_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define LIST_REF_OFFSET2				(GRAPH_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define SOURCE_REF_OFFSET2				(LIST_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define WAVEFORM_REF_OFFSET2			(SOURCE_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define GRID_REF_OFFSET2			    (WAVEFORM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define IMAGE_REF_OFFSET2			    (GRID_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define BINARY_REF_OFFSET2			    (IMAGE_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)

/////////////////////////////////////////////////////////////

#define BLK_ITEM_REF_OFFSET				DIRECTORY_DATA_OFFSET
#define BLK_ITEM_NAME_REF_OFFSET		(BLK_ITEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_REF_OFFSET				(BLK_ITEM_NAME_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_MEM_REF_OFFSET			(PARAM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_MEM_NAME_REF_OFFSET		(PARAM_MEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_ELEM_REF_OFFSET			(PARAM_MEM_NAME_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_LIST_REF_OFFSET			(PARAM_ELEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_LIST_MEM_REF_OFFSET		(PARAM_LIST_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define PARAM_LIST_MEM_NAME_REF_OFFSET	(PARAM_LIST_MEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define CHAR_MEM_REF_OFFSET				(PARAM_LIST_MEM_NAME_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define CHAR_MEM_NAME_REF_OFFSET		(CHAR_MEM_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define REL_REF_OFFSET					(CHAR_MEM_NAME_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define UPDATE_REF_OFFSET				(REL_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define COMMAND_REF_OFFSET				(UPDATE_REF_OFFSET + \
                                         TABLE_REF_LEN)

/* Long references for above constants */
#define BLK_ITEM_REF_OFFSET2			DIRECTORY_DATA_OFFSET
#define BLK_ITEM_NAME_REF_OFFSET2		(BLK_ITEM_REF_OFFSET + \
                                         TABLE_REF_LEN2)
#define PARAM_REF_OFFSET2				(BLK_ITEM_NAME_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_MEM_REF_OFFSET2			(PARAM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_MEM_NAME_REF_OFFSET2		(PARAM_MEM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_ELEM_REF_OFFSET2			(PARAM_MEM_NAME_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_LIST_REF_OFFSET2			(PARAM_ELEM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_LIST_MEM_REF_OFFSET2		(PARAM_LIST_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define PARAM_LIST_MEM_NAME_REF_OFFSET2	(PARAM_LIST_MEM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define CHAR_MEM_REF_OFFSET2				(PARAM_LIST_MEM_NAME_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define CHAR_MEM_NAME_REF_OFFSET2		(CHAR_MEM_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define REL_REF_OFFSET2					(CHAR_MEM_NAME_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define UPDATE_REF_OFFSET2				(REL_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)
#define COMMAND_REF_OFFSET2				(UPDATE_REF_OFFSET2 + \
                                         TABLE_REF_LEN2)


/* this line is not in the original DDS version of this file  */
#define CRIT_PARAM_REF_OFFSET		    (COMMAND_REF_OFFSET + \
                                         TABLE_REF_LEN)


/*
 *	Definitions pertaining to item object extensions.
 */

    /*   Sizes of the extension data fields (in octets).   */

#define	ITEM_TYPE_SIZE				1
#define	ITEM_SUBTYPE_SIZE			1
#define	ITEM_ID_SIZE				4
#define	MIN_ATTR_MASK_SIZE			1
#define	BLOCK_ATTR_MASK_SIZE		4
#define	BLOCK_ATTR_MASK_SIZE_PRE_5  2
#define WAVEFORM_ATTR_MASK_SIZE     3
#define SOURCE_ATTR_MASK_SIZE       2
#define	VAR_ATTR_MASK_SIZE			3
#define CHART_ATTR_MASK_SIZE        1

/* Expanded attribute mask sizes */
#define	VAR_EXPANDED_ATTR_MASK_SIZE			4
#define CHART_EXPANDED_ATTR_MASK_SIZE		4

    /*   Offsets of the extension data fields (in octets).   */

#define	ITEM_TYPE_OFFSET			(EXTEN_LENGTH_OFFSET +	\
                                     EXTEN_LENGTH_SIZE)

#define	ITEM_SUBTYPE_OFFSET			(ITEM_TYPE_OFFSET +		\
                                     ITEM_TYPE_SIZE)

#define	ITEM_ID_OFFSET				(ITEM_SUBTYPE_OFFSET +	\
                                     ITEM_SUBTYPE_SIZE)

#define	ATTR_MASK_OFFSET			(ITEM_ID_OFFSET +		\
                                     ITEM_ID_SIZE)


/*
 *	Definitions pertaining to attribute identifiers.
 */

    /*   Sizes of the attribute ID data fields (in octets).   */
    /*	 NOTE -- EXTERNAL_REF_SIZE must be <= LOCAL_REF_SIZE */

#define	ATTR_RI_TAG_SIZE			1		/* size of reference indicator/tag */
#define	LOCAL_REF_SIZE				2		/* size of local data field reference */
#define	EXTERNAL_REF_SIZE			2		/* size of external object reference */
#define MAX_OBJ_EXTN_LEN            180		/* maximum size of object extension */

    /*   Offsets of the attribute ID data fields (in octets).   */

#define	ATTR_RI_TAG_OFFSET			0

#define	ATTR_LENGTH_OFFSET			(ATTR_RI_TAG_OFFSET +	\
                                     ATTR_RI_TAG_SIZE)

    /*   Shifts of the attribute ID data fields.   */

#define	REF_INDICATOR_SHIFT			6
#define	LENGTH_SHIFT				7

    /*   Masks of the attribute ID data fields.   */

#define	REF_INDICATOR_MASK			0x03
#define	ATTR_TAG_MASK				0x3F
#define	TERMINATING_BIT_MASK		0x80
#define	LENGTH_MASK					0x7F

    /*   Reference Indicators of the attribute ID data fields.   */

#define	RI_IMMEDIATE				0
#define	RI_LOCAL					1
#define	RI_EXTERNAL_SINGLE			2
#define	RI_EXTERNAL_ALL				3


/*
 *	Definitions pertaining to object indices.
 */

#define	FIRST_OBJECT_INDEX			100
#define FORMAT_OBJECT_INDEX			FIRST_OBJECT_INDEX
#define	DEVICE_DIRECTORY_INDEX		(FORMAT_OBJECT_INDEX + 1)

/*
 * Definitions relating to menu item encoding
 */
#define MENU_CODE_REFERENCE     0x01
#define MENU_CODE_USER_STRING   0x08
#define MENU_CODE_BIT_ENUM_MASK 0x10

/*
 * Reserved values for special formatting information
 */
#define ENHANCED_FORMAT_50   0x80FD

extern unsigned long calc_version(unsigned int access_rights, unsigned char usage);

#endif /* DDL_TAGS_H */
