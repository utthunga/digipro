/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)dds_tab.h	21.5  21.5  14 Nov 1996
 */

#ifndef DDS_TAB_H
#define DDS_TAB_H

#include "table.h"

/*
 * Macros used in dds_tab
 */

#define PARAM_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex) (subindex-1)
#define PARAM_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset) (table_offset+1)

#define CHAR_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex) (subindex-1)
#define CHAR_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset) (table_offset+1)


#define	CHECK_CHAR_MEMBER_OFFSET(cmt, offset)(((offset >= CMT_COUNT(cmt)) || \
									(offset < 0)) ? \
									(DDI_TAB_BAD_CHAR_OFFSET) \
									: (DDS_SUCCESS))


#define	CHECK_PARAM_MEMBER_OFFSET(pt_elem, offset)(((((offset >= PTE_PM_COUNT(pt_elem)) && \
									(offset >= PTE_AE_COUNT(pt_elem)))) || \
									(offset < 0)) ? \
									(DDI_TAB_BAD_PARAM_MEMBER) \
									: (DDS_SUCCESS))


#define CHECK_PARAM_LIST_OFFSET(plt, offset)(((offset >= PLT_COUNT(plt)) || \
								(offset < 0)) ? \
								(DDI_TAB_BAD_PARAM_LIST_OFFSET) \
								: (DDS_SUCCESS))


#define CHECK_PARAM_OFFSET(pt, offset)(((offset >= PT_COUNT(pt)) || \
								(offset < 0)) ? \
								(DDI_TAB_BAD_PARAM_OFFSET) \
								: (DDS_SUCCESS))

#define REQ_TABLE_CHK(x, p)((x > 0) && (p != NULL))

#define OPT_TABLE_CHK(x, p)(((x > 0) && (p != NULL)) || \
						((x <= 0) && (p == NULL)))

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*	
 * 	function prototypes needed for conversion to ddid 
 */

extern int	tr_param_si_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, int, SUBINDEX,
											ITEM_TBL_ELEM **)) ;

extern int	tr_resolve_name_to_id_type P((BLOCK_HANDLE, BLK_TBL_ELEM *, ITEM_ID, ITEM_ID *,
											ITEM_TYPE *, int)) ;

extern int	tr_resolve_char_rec_to_ddid P((BLOCK_HANDLE, BLK_TBL_ELEM *, ITEM_ID *)) ;

extern int	tr_resolve_lname_pname_to_ddid P((BLOCK_HANDLE, BLK_TBL_ELEM *, ITEM_ID,
											ITEM_ID, ITEM_ID *)) ;

extern int	tr_characteristics_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, SUBINDEX,
											ITEM_TBL_ELEM **)) ;

extern int	tr_id_si_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, ITEM_ID, SUBINDEX,
											ITEM_TBL_ELEM **)) ;

extern int	tr_resolve_lid_pname_to_id_type P((BLOCK_HANDLE, BLK_TBL_ELEM *, ITEM_ID,
											ITEM_ID,ITEM_ID *, ITEM_TYPE *));

/*	
 * 	function prototypes needed for conversion to subindex 
 */

extern int	tr_resolve_ddid_mem_to_si P((BLK_TBL_ELEM *, ITEM_ID, ITEM_ID, SUBINDEX *)) ;


/*	
 * 	function prototypes needed for conversion to find table elements 
 */

extern int	find_bint_elem P((BLK_ITEM_NAME_TBL *, ITEM_ID, BLK_ITEM_NAME_TBL_ELEM **)) ;
extern int	find_bit_elem P((BLK_ITEM_TBL *, ITEM_ID, BLK_ITEM_TBL_ELEM **)) ;
extern int	find_pmnt_elem P((PARAM_MEM_NAME_TBL *, PARAM_TBL_ELEM *, ITEM_ID,
											PARAM_MEM_NAME_TBL_ELEM **));


/*
 * Function prototype for converting the block handle into the block
 * table element.
 */

extern int block_handle_to_bt_elem P((BLOCK_HANDLE, BLK_TBL_ELEM **));


/*
 * Funciton prototype needed for the table checker
 */

extern void	dds_check_table P((DEVICE_TYPE_HANDLE)) ;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif				/* DDS_TAB_H */
