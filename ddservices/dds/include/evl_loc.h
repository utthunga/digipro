/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)evl_loc.h	30.3  30  14 Nov 1996
 */

#include "evl_lib.h"
#include "fch_lib.h"

#ifndef EVL_LOC_H
#define EVL_LOC_H


/*
 * TYPEINFO - Contains the tag and a pointer to a generic value
 */

typedef struct {
	int             tag;
	void           *ptr;
}               TYPEINFO;

/*
 *	Flags for ddl_parse_ref() and resolve to indicate which reference
 *	types are requested.
 */

#define	RESOLVE_OP_REF  	0X01
#define	RESOLVE_DESC_REF	0X02
#define	RESOLVE_TRAIL		0X04

/*
 * Structure used by ddl_parse_ref to save the ref info
 */

typedef struct {
	unsigned short  type;

	union {
		ITEM_ID         id;	/* */
		unsigned long   index;	/* */
		unsigned long   member;	/* member name id */

	}               val;

}               REF_INFO;

#define REF_INFO_LIST_SIZE	32

typedef struct {

	unsigned short  count;
	REF_INFO        list[REF_INFO_LIST_SIZE];

}               REF_INFO_LIST;

/*
 * Chunk list stuff for ddl_cond() and ddl_cond_list().
 */

typedef struct {
	DDL_UINT        size;	/* # of chars assoc. w/ chunk to be parsed */
	unsigned char  *chunk;	/* ptr to chunk to be parsed */
}               CHUNK;

typedef struct {
	unsigned short  size;	/* index to the currently active element of */
	/* chunk list */
	unsigned short  limit;	/* number of elements in chunk list.  This is */
	/* dynamic */
	CHUNK          *list;	/* pointer to first element of chunk list */
}               CHUNK_LIST;

#define DEFAULT_CHUNK_LIST_SIZE  100
#define INCR_CHUNK_LIST_SIZE  40

#define ddl_create_chunk_list( chunk_list_ptr, chunk_list )	\
	{	\
		chunk_list_ptr.list = chunk_list;	\
		chunk_list_ptr.size = 0;	\
		chunk_list_ptr.limit = DEFAULT_CHUNK_LIST_SIZE;	\
	}

/*
 * The number ITEM_ID_LIST is incremented each time the list needs more space
 */
#define REF_LIST_INC	16

/*
 * The number VECTOR_LIST is incremented each time the list needs more space
 */
#define VECTOR_LIST_INC	16

/*
 * The number ENUM_VALUE_LIST is incremented each time the list needs more space
 */
#define ENUM_INC	32

/*
 * The number MENU_THE LISTEM_LIST is incremented each time the list needs more space
 */
#define MENU_INC		16

/*
 * The number ITEM_ARRAY_ELEMENT_LIST is incremented each time the list needs more space
 */
#define ITEM_ARRAY_INC 32

/*
 * The number MEMBER_LIST is incremented each time the list needs more space
 */
#define MEMBERS_INC	32

/*
 * The number RESPONSE_CODE_LIST is incremented each time the list needs more space
 */
#define RESPONSE_CODE_INC	8

/*
 * The number RANGE_DATA_LIST is incremented each time the list needs more space
 */
#define RANGE_DATA_INC	4

/*
 * The number OUTPUT_STATUS_LIST is incremented each time the list needs more space
 */
#define OUTPUT_STATUS_LIST_INCSZ 6

/*
 * The number DATA_ITEM_LIST is incremented each time the list needs more space
 */
#define DATA_ITEM_LIST_INCSZ    16

/*
 * The number VECTOR is incremented each time the list needs more space
 */
#define VECTOR_INCSZ    16

/**
 ** Standard dictionary default string indexes.
 ** If an upcall (for a string) returns "!DDL_SUCESS" or "DDL_DEFAULT_ATTR"
 ** one of these default strings is to be used.
 **/

#define DEFAULT_STD_DICT_STRING         (unsigned long)((400 << 16) + 0)
#define DEFAULT_DEV_SPEC_STRING          (unsigned long)((400 << 16) + 1)
#define DEFAULT_STD_DICT_HELP           (unsigned long)((400 << 16) + 2)
#define DEFAULT_STD_DICT_LABEL          (unsigned long)((400 << 16) + 3)
#define DEFAULT_STD_DICT_DESC           (unsigned long)((400 << 16) + 4)
#define DEFAULT_STD_DICT_DISP_INT       (unsigned long)((400 << 16) + 5)
#define DEFAULT_STD_DICT_DISP_UINT      (unsigned long)((400 << 16) + 6)
#define DEFAULT_STD_DICT_DISP_FLOAT     (unsigned long)((400 << 16) + 7)
#define DEFAULT_STD_DICT_DISP_DOUBLE    (unsigned long)((400 << 16) + 8)
#define DEFAULT_STD_DICT_EDIT_INT       (unsigned long)((400 << 16) + 9)
#define DEFAULT_STD_DICT_EDIT_UINT      (unsigned long)((400 << 16) + 10)
#define DEFAULT_STD_DICT_EDIT_FLOAT     (unsigned long)((400 << 16) + 11)
#define DEFAULT_STD_DICT_EDIT_DOUBLE    (unsigned long)((400 << 16) + 12)

/*
 *	Macro to silence complaints from SABER and CODECENTER
 */

#define NULL_PTR	NULL

/*
 *	Parse tag macro.
 *  This macro can handle parsing simple tags.
 * 	If the Most Significant Bit of CHUNK is set, an explicit length
 *  is stored with the tag ( call ddl_parse_tag_func() ).
 *  if the tag occupies multiple bytes, (ie. **chunk == 127),
 *  ddl_parse_tag_func() must be called.
 */

#define DDL_PARSE_TAG(C,S,T,L)	\
	{	\
		if ( (**(C) & 0x80) || ( (**(C) & 0x7f) == 127 )){	\
			rc = ddl_parse_tag_func((C), (S), (T), (L));	\
			if (rc != DDL_SUCCESS)	\
				return rc;	\
		} else {	\
			(L) ? *(L) = 0 : 0;	\
			(T) ? *(T) = **(C) : 0;	\
			 ++(*(C));	\
			--(*(S));	\
		}	\
	}

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 *	flat interface functions
 */

extern int eval_item_array P((FLAT_ARRAY *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_block P((FLAT_BLOCK *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_collection 
P((FLAT_COLLECTION *, unsigned long, ENV_INFO2 *,
		RETURN_LIST *));


extern int eval_item_domain P((FLAT_DOMAIN *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_axis P((FLAT_AXIS *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_chart P((FLAT_CHART *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_file P((FLAT_FILE *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_graph P((FLAT_GRAPH *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_list P((FLAT_LIST *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_source P((FLAT_SOURCE *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_waveform P((FLAT_WAVEFORM *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_grid P((FLAT_GRID *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_image P((FLAT_IMAGE *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_edit_display 
	P((FLAT_EDIT_DISPLAY *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_itemarray 
	P((FLAT_ITEM_ARRAY *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_menu P((FLAT_MENU *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_method P((FLAT_METHOD *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_program P((FLAT_PROGRAM *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_record P((FLAT_RECORD *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_refresh P((FLAT_REFRESH *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_response_code 
	P((FLAT_RESP_CODE *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_unit P((FLAT_UNIT *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_var P((FLAT_VAR *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_var_list 
	P((FLAT_VAR_LIST *, unsigned long, ENV_INFO2 *, RETURN_LIST *));
extern int eval_item_wao P((FLAT_WAO *, unsigned long, ENV_INFO2 *, RETURN_LIST *));

/*
 * flat destructor functions
 */

extern void eval_clean_array P((FLAT_ARRAY *));
extern void eval_clean_block P((FLAT_BLOCK *));
extern void eval_clean_collection P((FLAT_COLLECTION *));


extern void eval_clean_domain P((FLAT_DOMAIN *));
extern void eval_clean_edit_display P((FLAT_EDIT_DISPLAY *));
extern void eval_clean_itemarray P((FLAT_ITEM_ARRAY *));
extern void eval_clean_menu P((FLAT_MENU *));
extern void eval_clean_method P((FLAT_METHOD *));
extern void eval_clean_program P((FLAT_PROGRAM *));
extern void eval_clean_record P((FLAT_RECORD *));
extern void eval_clean_refresh P((FLAT_REFRESH *));
extern void eval_clean_resp_code P((FLAT_RESP_CODE *));
extern void eval_clean_unit P((FLAT_UNIT *));
extern void eval_clean_var_list P((FLAT_VAR_LIST *));
extern void eval_clean_var P((FLAT_VAR *));
extern void eval_clean_wao P((FLAT_WAO *));

/*
 * Evaluation functions
 */

extern int eval_attr_bitstring 
	P((uchar *, DDL_UINT, DDL_UINT *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_dataitems 
	P((unsigned char *, DDL_UINT, DATA_ITEM_LIST * items, OP_REF_LIST *,
	ENV_INFO2 *, OP_REF *));

extern int eval_attr_definition 
	P((uchar *, DDL_UINT, DEFINITION *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_desc_ref 
	P((uchar *, DDL_UINT, DESC_REF *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_display_format 
	P((uchar *, DDL_UINT, STRING *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_edit_format 
	P((uchar *, DDL_UINT, STRING *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_enum 
	P((uchar *, DDL_UINT, ENUM_VALUE_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_int_expr 
	P((unsigned char *chunk, DDL_UINT size, unsigned long *value,
	OP_REF_LIST * depinfo, ENV_INFO2 *, OP_REF *));

extern int eval_attr_itemarray 
	P((uchar *, DDL_UINT, ITEM_ARRAY_ELEMENT_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_arrayname 
	P((unsigned char *, DDL_UINT, ITEM_ID *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_menuitems 
	P((uchar *, DDL_UINT, MENU_ITEM_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_min_values 
	P((uchar *, DDL_UINT, RANGE_DATA_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_max_values 
	P((uchar *, DDL_UINT, RANGE_DATA_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_members 
	P((uchar *, DDL_UINT, MEMBER_LIST *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_op_members 
	P((uchar *, DDL_UINT, OP_MEMBER_LIST *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_op_ref_trail_list 
	P((uchar *, DDL_UINT, OP_REF_TRAIL_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_ref 
	P((uchar *, DDL_UINT, ITEM_ID *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_reflist 
	P((uchar *, DDL_UINT, ITEM_ID_LIST *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_refresh 
	P((uchar *, DDL_UINT, REFRESH_RELATION *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_resp_codes 
	P((uchar *, DDL_UINT, RESPONSE_CODE_LIST *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_scaling_factor 
	P((uchar *, DDL_UINT, EXPR *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_string 
	P((unsigned char *chunk, DDL_UINT, STRING *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int eval_attr_type 
	P((uchar *, DDL_UINT, TYPE_SIZE *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_ulong 
	P((uchar *, DDL_UINT, DDL_UINT *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_unit 
	P((uchar *, DDL_UINT, UNIT_RELATION *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_expr 
	P((unsigned char *, DDL_UINT, EXPR *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int eval_attr_vectors 
	P((uchar *, DDL_UINT, VECTOR_LIST *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));


/*
 * Free functions
 */

#define CLEAN_ATTR  0
#define FREE_ATTR   1

extern void ddl_free_reflist P((ITEM_ID_LIST *, uchar));

extern void ddl_free_dataitems P((DATA_ITEM_LIST *, uchar));

extern void ddl_free_vector P((VECTOR *, uchar));

extern void ddl_free_resp_codes P((RESPONSE_CODE_LIST *, uchar));

extern void ddl_free_depinfo P((OP_REF_LIST * depinfo, uchar));

extern void ddl_free_menuitems_list P((MENU_ITEM_LIST * items, uchar));

extern void ddl_free_refresh P((REFRESH_RELATION *, uchar));

extern void ddl_free_unit P((UNIT_RELATION *, uchar));

extern void ddl_free_range_list P((RANGE_DATA_LIST *, uchar));


extern void ddl_free_enum_list P((ENUM_VALUE_LIST *, uchar));

extern void ddl_free_itemarray_list P((ITEM_ARRAY_ELEMENT_LIST *, uchar));

extern void ddl_free_members_list P((MEMBER_LIST *, uchar));

extern void ddl_free_op_members_list P((OP_MEMBER_LIST *, uchar));


extern void ddl_free_op_ref_trail_list P((OP_REF_TRAIL_LIST *, unsigned char));

extern void ddl_free_op_ref_trail P((OP_REF_TRAIL *));

extern void ddl_free_string P((STRING *));




/*
 *	Defined in evl_base.c
 */
extern int ddl_shrink_depinfo P((OP_REF_LIST *));

extern int ddl_add_depinfo P((OP_REF *, OP_REF_LIST *));

extern int ddl_parse_tag_func
	P((unsigned char **, DDL_UINT *, DDL_UINT *, DDL_UINT *));

extern int ddl_parse_integer_func P((unsigned char **, DDL_UINT *, DDL_UINT *));

extern int ddl_parse_float P((unsigned char **, DDL_UINT *, float *));

extern int ddl_parse_bitstring P((unsigned char **, DDL_UINT *, DDL_UINT *));

extern int ddl_parse_string 
	P((unsigned char **, DDL_UINT *, STRING *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_string_choice 
	P((unsigned char **, DDL_UINT *, STRING *, OP_REF_LIST *, int *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_parse_item_id 
	P((unsigned char **, DDL_UINT *, ITEM_ID *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_parse_desc_ref 
	P((unsigned char **, DDL_UINT *, DESC_REF *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_parse_op_ref 
	P((unsigned char **, DDL_UINT *, OP_REF *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *, unsigned long *));

extern int ddl_parse_op_ref_trail 
	P((unsigned char **, DDL_UINT *, OP_REF_TRAIL *, OP_REF_LIST *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_op_ref_trail_choice 
	P((unsigned char **, DDL_UINT *, OP_REF_TRAIL *, OP_REF_LIST *, int *,
	ENV_INFO2 *, OP_REF *));

extern int ddl_op_ref_trail_list_choice 
	P((unsigned char **, DDL_UINT *, OP_REF_TRAIL_LIST *, OP_REF_LIST *, int *,
	ENV_INFO2 *, OP_REF *));

/*
 *	Defined in evl_resp.c
 */

extern int ddl_rspcodes_choice 
	P((unsigned char **, DDL_UINT *, RESPONSE_CODE_LIST *, OP_REF_LIST *, int *,
	ENV_INFO2 *, OP_REF *));

extern int ddl_shrink_resp_codes P((RESPONSE_CODE_LIST *));

/*
 *	Defined in evl_expr.c
 */

extern int ddl_expr_choice 
	P((unsigned char **chunkp, DDL_UINT *, EXPR *, OP_REF_LIST *, int *,
	ENV_INFO2 *, OP_REF *));

extern int ddl_eval_expr 
	P((unsigned char **, DDL_UINT *, EXPR *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int ddl_expr_nonzero P((EXPR *));

extern int ddl_promote_to P((EXPR *, unsigned short, unsigned short));

extern int ddl_exprs_equal P((EXPR *, EXPR *));

extern int retrieve_param_value P((ENV_INFO *, OP_REF *, EVAL_VAR_VALUE *));

/*
 *	Defined in evl_enum.c
 */

extern int ddl_insert_enum P((ENUM_VALUE_LIST *, unsigned short, ENUM_VALUE *));

extern int ddl_enums_choice 
	P((unsigned char **, DDL_UINT *, TYPEINFO *, OP_REF_LIST *, int *, ENV_INFO2 *,
	OP_REF *));

extern int ddl_shrink_enum_list P((ENUM_VALUE_LIST *));

extern int ddl_shrink_op_ref_trail_list P((OP_REF_TRAIL_LIST *));

extern int ddl_parse_one_enum 
	P((unsigned char **, DDL_UINT *, TYPEINFO *, ENUM_VALUE *, OP_REF_LIST *,
	ENV_INFO2 *, OP_REF *));

/*
 * Defined in evl_cond.c
 */

extern int ddl_cond
	P((unsigned char **, DDL_UINT *, CHUNK *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *));

extern int ddl_cond_list 
	P((unsigned char **, DDL_UINT *, CHUNK_LIST *, OP_REF_LIST *, int, ENV_INFO2 *,
	OP_REF *));

extern void ddl_delete_chunk_list P((CHUNK_LIST *));

/*
 * Defined in evl_rslv.c
 */

extern int resolve_fetch 
	P((ITEM_ID, unsigned long, void *, unsigned short, BLOCK_HANDLE, SCRATCH_PAD *));

extern void resolve_fetch_free
	P((void *, unsigned short, SCRATCH_PAD *, BLOCK_HANDLE));

extern int resolve_ref 
	P((REF_INFO_LIST *, OP_REF_TRAIL *, OP_REF_LIST *, ENV_INFO2 *, OP_REF *,
	unsigned int, unsigned short));

extern int append_depinfo P((OP_REF_LIST *, OP_REF_LIST *));

/*
 * Other functions
 */

extern void
ddl_free_vector_list(VECTOR_LIST *vector_list, uchar dest_flag);
extern int ddl_shrink_vector_list(VECTOR_LIST *vector_list);

extern unsigned char enum_check_for_suppression(STRING* enum_str, char *suppression_str);
extern void acquire_suppression_str(ENV_INFO2 *env_info, char **suppression_str);
extern void remove_all_suppression_flags(STRING *enum_str);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EVL_LOC_H */
