/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)od_defs.h	30.3	 30  14 Nov 1996
 */

#ifndef OD_DEFS_H
#define OD_DEFS_H


typedef	unsigned short	OBJECT_INDEX;
typedef	int	SUBINDEX;


/*
 *	Definitions pertaining to the OD description object.
 */

/* Sizes of the data fields (in octets) */

#define	ODES_INDEX_SIZE				2
#define	ROM_RAM_FLAG_SIZE			1
#define	NAME_LENGTH_SIZE			1
#define	ACCSS_PROTECT_SIZE			1
#define	VERSION_OD_SIZE				2
#define	LOC_ADDR_ODES_SIZE			4
#define	STOD_LENGTH_SIZE			2
#define	LOC_ADDR_STOD_SIZE			4
#define	FIRST_IN_SOD_SIZE			2
#define	SOD_LENGTH_SIZE				2
#define	LOC_ADDR_SOD_SIZE			4
#define	FIRST_IN_DVOD_SIZE			2
#define	DVOD_LENGTH_SIZE			2
#define	LOC_ADDR_DVOD_SIZE			4
#define	FIRST_IN_DPOD_SIZE			2
#define	DPOD_LENGTH_SIZE			2
#define	LOC_ADDR_DPOD_SIZE			4

#define	ODES_SIZE					(ODES_INDEX_SIZE +		\
									 ROM_RAM_FLAG_SIZE +	\
									 NAME_LENGTH_SIZE +		\
									 ACCSS_PROTECT_SIZE +	\
									 VERSION_OD_SIZE +		\
									 LOC_ADDR_ODES_SIZE +	\
									 STOD_LENGTH_SIZE +		\
									 LOC_ADDR_STOD_SIZE +	\
									 FIRST_IN_SOD_SIZE +	\
									 SOD_LENGTH_SIZE +		\
									 LOC_ADDR_SOD_SIZE +	\
									 FIRST_IN_DVOD_SIZE +	\
									 DVOD_LENGTH_SIZE +		\
									 LOC_ADDR_DVOD_SIZE +	\
									 FIRST_IN_DPOD_SIZE +	\
									 DPOD_LENGTH_SIZE +		\
									 LOC_ADDR_DPOD_SIZE)

/* Offsets of the data fields (in octets) */

#define	ODES_INDEX_OFFSET			0

#define	ROM_RAM_FLAG_OFFSET			(ODES_INDEX_OFFSET +	\
									 ODES_INDEX_SIZE)

#define	NAME_LENGTH_OFFSET			(ROM_RAM_FLAG_OFFSET +	\
									 ROM_RAM_FLAG_SIZE)

#define	ACCSS_PROTECT_OFFSET		(NAME_LENGTH_OFFSET +	\
									 NAME_LENGTH_SIZE)

#define	VERSION_OD_OFFSET			(ACCSS_PROTECT_OFFSET +	\
									 ACCSS_PROTECT_SIZE)

#define	LOC_ADDR_ODES_OFFSET		(VERSION_OD_OFFSET +	\
									 VERSION_OD_SIZE)

#define	STOD_LENGTH_OFFSET			(LOC_ADDR_ODES_OFFSET +	\
									 LOC_ADDR_ODES_SIZE)

#define	LOC_ADDR_STOD_OFFSET		(STOD_LENGTH_OFFSET +	\
									 STOD_LENGTH_SIZE)

#define	FIRST_IN_SOD_OFFSET			(LOC_ADDR_STOD_OFFSET +	\
									 LOC_ADDR_STOD_SIZE)

#define	SOD_LENGTH_OFFSET			(FIRST_IN_SOD_OFFSET +	\
									 FIRST_IN_SOD_SIZE)

#define	LOC_ADDR_SOD_OFFSET			(SOD_LENGTH_OFFSET +	\
									 SOD_LENGTH_SIZE)

#define	FIRST_IN_DVOD_OFFSET		(LOC_ADDR_SOD_OFFSET +	\
									 LOC_ADDR_SOD_SIZE)

#define	DVOD_LENGTH_OFFSET			(FIRST_IN_DVOD_OFFSET +	\
									 FIRST_IN_DVOD_SIZE)

#define	LOC_ADDR_DVOD_OFFSET		(DVOD_LENGTH_OFFSET +	\
									 DVOD_LENGTH_SIZE)

#define	FIRST_IN_DPOD_OFFSET		(LOC_ADDR_DVOD_OFFSET +	\
									 LOC_ADDR_DVOD_SIZE)

#define	DPOD_LENGTH_OFFSET			(FIRST_IN_DPOD_OFFSET +	\
									 FIRST_IN_DPOD_SIZE)

#define	LOC_ADDR_DPOD_OFFSET		(DPOD_LENGTH_OFFSET +	\
									 DPOD_LENGTH_SIZE)


/*
 *	Definitions pertaining to the fixed part of a domain object.
 */

/* Sizes of the data fields (in octets) */

#define	OBJECT_INDEX_SIZE			2
#define	OBJECT_CODE_SIZE			1
#define	MAX_OCTETS_SIZE				2
#define	MAX_OCTETS_SIZE2			4
#define	PASSWORD_SIZE				1
#define	ACCESS_GROUP_SIZE			1
#define	ACCESS_RIGHTS_SIZE			2
#define	LOCAL_ADDRESS_SIZE			4
#define	DOMAIN_STATE_SIZE			1
#define	UPLOAD_STATE_SIZE			1
#define	COUNTER_SIZE				1

#define	FIXED_SIZE					(OBJECT_INDEX_SIZE +	\
									 OBJECT_CODE_SIZE +		\
									 MAX_OCTETS_SIZE +		\
									 PASSWORD_SIZE +		\
									 ACCESS_GROUP_SIZE +	\
									 ACCESS_RIGHTS_SIZE +	\
									 LOCAL_ADDRESS_SIZE +	\
									 DOMAIN_STATE_SIZE +	\
									 UPLOAD_STATE_SIZE +	\
									 COUNTER_SIZE)

/* Offsets of the data fields (in octets) */

#define	OBJECT_INDEX_OFFSET			0

#define	OBJECT_CODE_OFFSET			(OBJECT_INDEX_OFFSET +	\
									 OBJECT_INDEX_SIZE)

#define	MAX_OCTETS_OFFSET			(OBJECT_CODE_OFFSET +	\
									 OBJECT_CODE_SIZE)

#define	PASSWORD_OFFSET				(MAX_OCTETS_OFFSET +	\
									 MAX_OCTETS_SIZE)

#define	ACCESS_GROUP_OFFSET			(PASSWORD_OFFSET +		\
									 PASSWORD_SIZE)

#define	ACCESS_RIGHTS_OFFSET		(ACCESS_GROUP_OFFSET +	\
									 ACCESS_GROUP_SIZE)

#define	LOCAL_ADDRESS_OFFSET		(ACCESS_RIGHTS_OFFSET +	\
									 ACCESS_RIGHTS_SIZE)

#define	DOMAIN_STATE_OFFSET			(LOCAL_ADDRESS_OFFSET +	\
									 LOCAL_ADDRESS_SIZE)

#define	UPLOAD_STATE_OFFSET			(DOMAIN_STATE_OFFSET +	\
									 DOMAIN_STATE_SIZE)

#define	COUNTER_OFFSET				(UPLOAD_STATE_OFFSET +	\
									 UPLOAD_STATE_SIZE)


/*
 *	Definitions pertaining to the extension of a domain object.
 */

/* Sizes of the data fields (in octets) */

#define	EXTEN_LENGTH_OFFSET			0

/* Offsets of the data fields (in octets) */

#define	EXTEN_LENGTH_SIZE			1

/* Offsets for the expanded attributes-formatted extension */

#define EXP_MASK_SIZE_SIZE			1  /* Size of the mask to follow */

#endif	/* OD_DEFS_H */
