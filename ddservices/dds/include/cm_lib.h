/**
 *      Device Description Services Rel. 4.3
 *      Copyright 1994-2002 - Fieldbus Foundation
 *      All rights reserved.
 */

/*
 *  @(#)cm_lib.h    30.4  30  14 Nov 1996
 *
 *  cm_lib.h - Connection Manager Library Definitions
 *
 *  This file contains all interface definitions for
 *  the Connection Manager Library.
 */


#ifndef CM_LIB_H
#define CM_LIB_H

#ifdef SUN
#include <memory.h>     /* K&R */
#else
#include <string.h>     /* ANSI */
#endif /* SUN */

#if defined(__linux__)
#include <pthread.h>
#endif

#include "std.h"
#include "rtn_code.h"
#include "ddldefs.h"
#include "env_info.h"
#include "od_defs.h"
#include "host.h"

/*
 *  Connection Manager Modes
 */

#define CMM_ON_LINE     1
#define CMM_OFF_LINE    2

#define HOST    1
#define REMOTE  2

#define		APPL_NORMAL	0
#define		APPL_BUSY   1


#ifndef BOOLEAN_TDECLARED
#define BOOLEAN_TDECLARED
typedef UINT8          BOOLEAN_T;
#endif

#define DFLT_LO_ADDR  0xF8
#define DFLT_HI_ADDR  0xFB

/*
 *  Connection Manager Typedefs
 */

typedef int     DEVICE_TYPE_HANDLE;
typedef int     NETWORK_HANDLE;
typedef int     NT_COUNT;
typedef int     ROD_HANDLE;
typedef int     FLAT_HANDLE;

typedef struct {
    UINT8   password;
    UINT8   groups;
    UINT16  rights;
} ACCESS;

typedef struct  {
    BOOLEAN_T         ram_rom_flag;
    UINT8           name_length;
    BOOLEAN_T         access_protection_flag;
    UINT16          version;
    UINT16          stod_object_count;
    OBJECT_INDEX    sod_first_index;
    UINT16          sod_object_count;
    OBJECT_INDEX    dvod_first_index;
    UINT16          dvod_object_count;
    OBJECT_INDEX    dpod_first_index;
    UINT16          dpod_object_count;
} OD_DESCRIPTION_SPECIFIC;

typedef struct _T_DIR_HEAD
{
    UINT16     reserved;
    UINT16     dir_rev_number;
    UINT16     n_dir_objs;
    UINT16     n_dir_entries;
    UINT16     n_first_comp_list_dir_index;
    UINT16     n_comp_lists;

} T_DIR_HEAD;

typedef struct _T_SM_DIR
{
    T_DIR_HEAD     sm_header;
    UINT16         sm_agent_obj;
    UINT16         n_sm_agent_obj;
    UINT16         sync_schedul_obj;
    UINT16         n_sync_schedule_obj;
    UINT16         addr_assig_obj;
    UINT16         n_addr_assig_obj;
    UINT16         vfd_list_obj;
    UINT16         n_vfd_list_obj;
    UINT16         fb_sch_obj;
    UINT16         n_fb_sch_obj;
    UINT16         soft_download_obj;
    UINT16         n_soft_download_obj;
    UINT16         soft_dwnl_domain_desc;
    UINT16         n_soft_dwnl_domain_desc;
} T_SM_DIR;

typedef struct _T_NM_DIR
{
    T_DIR_HEAD     nm_header;
    UINT16         sm_obj;
    UINT16         n_sm_obj;
    UINT16         vcr_list_obj;
    UINT16         n_vcr_list_obj;
    UINT16         dlme_basic_obj;
    UINT16         n_dlme_basic_obj;
    UINT16         dlme_lm_obj;
    UINT16         n_dlme_lm_obj;
    UINT16         ls_list_obj;
    UINT16         n_ls_list_obj;
    UINT16         dlme_bridge_obj;
    UINT16         n_dlme_bridge_obj;
    UINT16         plme_basic;
    UINT16         n_plme_basic;
    UINT16         mme_ref;
    UINT16         n_mme_ref;
} T_NM_DIR;

typedef struct {
    UINT32          size;
    UINT32          local_address;
    UINT8           state;
    UINT8           upload_state;
    UINT8           usage;
} DOMAIN_SPECIFIC;

typedef struct {
    UINT8            domain_count;
    BOOLEAN_T          deleteable_flag;
    BOOLEAN_T          reuseable_flag;
    UINT8            state;
    OBJECT_INDEX    *domain_list;
} PROGRAM_INVOCATION_SPECIFIC;

typedef struct {
    OBJECT_INDEX    data;
    UINT8           size;
    BOOLEAN_T         enabled_flag;
} EVENT_SPECIFIC;

typedef struct {
    UINT8            symbol_size;
    char            *symbol;
} DATA_TYPE_SPECIFIC;

typedef struct {
    UINT8            elem_count;
    OBJECT_INDEX    *type_list;
    UINT8           *size_list;
} DATA_TYPE_STRUCTURE_SPECIFIC;

typedef struct {
    OBJECT_INDEX    type;
    UINT8           size;
} SIMPLE_VARIABLE_SPECIFIC;

typedef struct {
    OBJECT_INDEX    type;
    UINT8           size;
    UINT8           elem_count;
} ARRAY_SPECIFIC;

typedef struct {
    OBJECT_INDEX    type;
} RECORD_SPECIFIC;

typedef struct {
    UINT8            elem_count;
    BOOLEAN_T          deleteable_flag;
    OBJECT_INDEX    *list;
} VARIABLE_LIST_SPECIFIC;

typedef union {
    OD_DESCRIPTION_SPECIFIC         od_description;
    DOMAIN_SPECIFIC                 domain;
    PROGRAM_INVOCATION_SPECIFIC     program_invocation;
    EVENT_SPECIFIC                  event;
    DATA_TYPE_SPECIFIC              data_type;
    DATA_TYPE_STRUCTURE_SPECIFIC    data_type_structure;
    SIMPLE_VARIABLE_SPECIFIC        simple_variable;
    ARRAY_SPECIFIC                  array;
    RECORD_SPECIFIC                 record;
    VARIABLE_LIST_SPECIFIC          variable_list;
} OBJECT_SPECIFIC;

typedef struct {
    OBJECT_INDEX         index;
    UINT8                code;
    char                *name;
    ACCESS               access;
    OBJECT_SPECIFIC     *specific;
    UINT8               *extension;
    UINT8               *value;
} OBJECT;

typedef struct {
    ITEM_ID                 block_id ;
    BLOCK_HANDLE            block_handle ;
} CM_BLOCK_LIST_ELEM;

typedef struct {
    int                     count ;
    CM_BLOCK_LIST_ELEM      *list ;
} CM_BLOCK_LIST ;

typedef struct {
    ITEM_ID                 block_type_id ;
} CM_BLOCK_TYPE_LIST_ELEM ;

typedef struct {
    int                     count ;
    CM_BLOCK_TYPE_LIST_ELEM *list ;
} CM_BLOCK_TYPE_LIST ;

typedef struct {
    unsigned short      cm_type;
    unsigned short      cm_size;
    union {
        BOOLEAN_T         cm_boolean;
        float           cm_float;
        double          cm_double;
        unsigned long   cm_ulong;
        long            cm_long;
        unsigned long   cm_time_value[2];
        char            *cm_stream;
        char            cm_ca[8];
    } cm_val;
} CM_VALUE;

typedef struct {
    int         cmvl_count;
    CM_VALUE    *cmvl_list;
} CM_VALUE_LIST;


#define DDHT_ROD  1     /* This DD handle is a ROD handle */
#define DDHT_FLAT 2     /* This DD handle is a FLAT handle */

typedef struct {
    int     dd_handle_type;
    int     dd_handle_number;
} DD_HANDLE;

typedef int     CREF;

typedef struct {
    UINT32          ddid_manufacturer;
    UINT16          ddid_device_type;
    UINT8           ddid_device_rev;
    UINT8           ddid_dd_rev;
} DD_DEVICE_ID;

typedef struct {
    int     (*build_dd_dev_tbl_fn_ptr) (DEVICE_HANDLE);
    int     (*build_dd_blk_tbl_fn_ptr) (BLOCK_HANDLE);
    int     (*remove_dd_dev_tbl_fn_ptr) (DEVICE_HANDLE);
    int     (*remove_dd_blk_tbl_fn_ptr) (BLOCK_HANDLE);
} DDS_TBL_FN_PTRS;

typedef struct {
    int     (*build_blk_app_info_fn_ptr) (BLOCK_HANDLE);
    int     (*build_dev_app_info_fn_ptr) (DEVICE_HANDLE);
    int     (*build_dev_type_app_info_fn_ptr) (DEVICE_TYPE_HANDLE);
    int     (*remove_blk_app_info_fn_ptr) (BLOCK_HANDLE);
    int     (*remove_dev_app_info_fn_ptr) (DEVICE_HANDLE);
    int     (*remove_dev_type_app_info_fn_ptr) (DEVICE_TYPE_HANDLE);
} APP_INFO_FN_PTRS;

/* Inform the DDS user of the filename (including path)we've successfully loaded */
extern int (*dd_loaded_callback)(char *fileName);

/* Active Block Table */


typedef struct {
    OBJECT_INDEX    op_index ;
} ABT_SPEC_A ;

typedef struct {


    ABT_SPEC_A  abt_sp_a ;

} ABT_NET_SPEC ;

typedef struct {
    char            *tag;
    ABT_NET_SPEC    abt_net_spec ;
    unsigned int    param_count;
    UINT32           dd_blk_id;
    int              dd_blk_tbl_offset;
    void            *app_info;
    DEVICE_HANDLE    active_dev_tbl_offset;
    int              usage;
    void            *pc_info;
} ACTIVE_BLK_TBL_ELEM;

typedef struct {
    int                       count;
    ACTIVE_BLK_TBL_ELEM     **list;
} ACTIVE_BLK_TBL;


typedef struct {
    pthread_t   thread_id;
    pthread_mutex_t m_mutex;
    int	host_type;
    int socket;
} ACTIVE_THREAD_DATA;



/* Active Device Table */


typedef int ADDR_TYPE ;
typedef int BAUD_RATE ;

typedef struct {
    UINT8           mfrid ;
    UINT8           mfr_dev_type ;
    UINT8           device_id[3] ;
} LONG_ADDR ;


typedef struct {
    ADDR_TYPE       addr_type ;
    LONG_ADDR       long_addr ;
    int             short_addr ;
} DEVICE_ADDRESS ;


typedef struct {
    CREF            opod_cref;
    CREF            ddod_cref;
    CREF            mib_cref;
} ADT_SPEC_A ;


typedef union {

    ADT_SPEC_A      adt_spec_a ;


} ADT_NET_SPEC ;

typedef struct {
    UINT16              station_address ;
    NETWORK_HANDLE       network;
    ADT_NET_SPEC         adt_net_spec ;
    ROD_HANDLE           oprod_handle;
    void                *app_info;
    DEVICE_TYPE_HANDLE   active_dev_type_tbl_offset;
    int                  usage;
} ACTIVE_DEV_TBL_ELEM;

typedef struct {
    int                       count;
    ACTIVE_DEV_TBL_ELEM     **list;
} ACTIVE_DEV_TBL;


/* Active Device Type Table */

typedef struct {
    DD_DEVICE_ID    dd_device_id;
    DD_HANDLE       dd_handle;
    void           *dd_dev_tbls;
    void           *app_info;
    int             usage;
} ACTIVE_DEV_TYPE_TBL_ELEM;

typedef struct {
    int                           count;
    ACTIVE_DEV_TYPE_TBL_ELEM    **list;
} ACTIVE_DEV_TYPE_TBL;


/* Network Table */
/*
 *  Connection Manager Tables
 */

/*
 *  Connection Manager Convenience Macros
 *
 *  The following macros provide access to Active Block, Active Device,
 *  Active Device Type, and Network information.  They may need
 *  replacement for systems that do not have direct access to the
 *  network.
 */

typedef int     NET_TYPE;

#define NT_A            1   /* Type A Network */
#define NT_A_SIM        2   /* Simulated Type A Network */
#define NT_OFF_LINE     3   /* Off-Line Mode */

#define NT_ETHER        5   /* Ethernet Network (connected to FF) */

#define NT_CONTROL      6   /* Control Network */

#define NO_CONFIG_ID    0

typedef union {
    struct {
        UINT32          ip_address;
        UINT16          socket_address;
        UINT16          board;
    } ether_network;
    struct {
        UINT16          controller_address;
        UINT16          controller_port;
        UINT16          controller_device_address;
    } control_network;
} NET_ROUTE;

#define DF_FF               1   /* FF Device Family type */


/****************************************************************************/
/* Network Management Objects Tables                                        */
/****************************************************************************/
#define FEAT_SUPP_LEN 8
#pragma pack(1)
typedef struct _NM_OBJ_STACK_CAP
{
    UINT32 FasArTypeAndRoleSupp;
    UINT16 MaxDlsapAddressesSupp;
    UINT16 MaxDlcepAddressesSupp;
    UINT8  DlcepDeliveryFeaturesSupp;
    UINT16 VersionOfNmSpecSupp;
    UINT8  AgentFunctionsSupp;
    UINT8  FmsFeaturesSupp[FEAT_SUPP_LEN];
} NM_OBJ_STACK_CAP;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_VCR_LST_CHARAC
{
    UINT32 Version;
    UINT16 MaxEntries;
    UINT16 NumPermanentEntries;
    UINT16 NumCurrentlyConfigured;
    UINT16 FirstUnconfiguredEntry;
    UINT8  DynamicsSupportedFlag;
    UINT8  StatisticsSupported;
    UINT16 NumOfStatisticsEntries;
} NM_OBJ_VCR_LST_CHARAC;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_VCR_STATIC_ENRY
{
    UINT8  FasArTypeAndRole;
    UINT32 FasDllLocalAddr;
    UINT32 FasDllConfiguredRemoteAddr;
    UINT8  FasDllSDAP;
    UINT16 FasDllMaxConfirmDelayOnConnect;
    UINT16 FasDllMaxConfirmDelayOnData;
    UINT16 FasDllMaxDlsduSize;
    UINT8  FasDllResidualActivitySupported;
    UINT8  FasDllTimelinessClass;
    UINT16 FasDllPublisherTimeWindowSize;
    UINT32 FasDllPublisherSynchronizingDlcep;
    UINT16 FasDllSubscriberTimeWindowSize;
    UINT32 FasDllSubscriberSynchronizingDlcep;
    UINT32 FmsVfdId;
    UINT8  FmsMaxOutstandingServicesCalling;
    UINT8  FmsMaxOutstandingServicesCalled;
    UINT8  FmsFeaturesSupported[FEAT_SUPP_LEN];
} NM_OBJ_VCR_STATIC_ENRY;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_VCR_DYN_ENTRY
{
    UINT8 FmsState;
    UINT8 FmsActualMaxOutstandingServicesCalling;
    UINT8 FmsActualMaxOutstandingServicesCalled;
    UINT8 FmsOutstandingServicesCounterCalling;
    UINT8 FmsOutstandingServicesCounterCalled;
    UINT8 FasState;
    UINT32 FasDllActualRemoteAddress;
    UINT8 FasDllMaxSendingQueueDepth;
    UINT8 FasDllMaxReceivingQueueDepth;
} NM_OBJ_VCR_DYN_ENTRY;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_VCR_STAT_ENTRY
{
    UINT16 VcrStaticEntryOdIndex;
    UINT8 ClearVcrStatistics;
    UINT16 FasNumOfAbortsCtr;
    UINT8 FasLocallyGeneratedLastAborted;
    UINT16 FasReasonLastAborted;
    UINT16 DllNumOfDtPdusSent;
    UINT16 DllNumOfDtPdusReceived;
    UINT16 DllNumOfDlDataTransferTimeoutFailures;
    UINT16 DllNumOfRcvrQuFullDlDataFailures;
} NM_OBJ_VCR_STAT_ENTRY;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_DLME_BSC_CHARAC
{
    UINT8  Version;
    UINT8  BasicStatisticsSupportedFlag;
    UINT8  DlOperatFunctionalClass;
    UINT32 DlDeviceConformance;
} NM_OBJ_DLME_BSC_CHARAC;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_DLME_BSC_INFO
{
    UINT16 SlotTime;
    UINT8  PerDlpduPhlOverhead;
    UINT8  MaxResponseDelay;
    UINT8  ThisNode;
    UINT16 ThisLink;
    UINT8  MinInterPduDelay;
    UINT8  TimeSyncClass;
    UINT8  PreambleExtension;
    UINT8  PostTransGapExtension;
    UINT8  MaxInterChanSignalSkew;
} NM_OBJ_DLME_BSC_INFO;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_DLME_STAT
{
    UINT32 NumOfDlpduTransmitted;
    UINT32 NumOfGoodDlpduReceived;
    UINT32 NumOfPartialReceivedDlpdu;
    UINT32 NumOfFcsFailures;
    UINT32 NumOfNodeTimeOffsetDiscontinuousChanges;
} NM_OBJ_DLME_STAT;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_DLME_LM_INFO
{
    UINT8  MaxSchedulingOverhead;
    UINT16 DefMinTokenDelegTime;
    UINT16 DefTokenHoldTime;
    UINT16 TargetTokenRotTime;
    UINT16 LinkMaintTokHoldTime;
    UINT32 TimeDistributionPeriod;
    UINT16 MaximumInactivityToClaimLasDelay;
    UINT16 LasDatabaseStatusSpduDistributionPeriod;
} NM_OBJ_DLME_LM_INFO;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_DLME_LM_LINKSETT
{
    UINT16 SlotTime;
    UINT8  PerDlpduPhlOverhead;
    UINT8  MaxResponseDelay;
    UINT8  FirstUnpolledNodeId;
    UINT16 ThisLink;
    UINT8  MinInterPduDelay;
    UINT8  NumConsecUnpolledNodeId;
    UINT8  PreambleExtension;
    UINT8  PostTransGapExtension;
    UINT8  MaxInterChanSignalSkew;
    UINT8  TimeSyncClass;
} NM_OBJ_DLME_LM_LINKSETT;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_LS_CHARAC
{
    UINT8   NumOfSchedules;
    UINT8   NumOfSubSchedulesPerSchedule;
    UINT16  ActiveScheduleVersion;
    UINT16  ActiveScheduleOdIndex;
    UINT8   ActiveScheduleStartingTime[8];
} NM_OBJ_LS_CHARAC;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_SCH_DES
{
    UINT16 Version;
    UINT32 MacrocycleDuration;
    UINT16 TimeResolution;
} NM_OBJ_SCH_DES;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_PLME_BSC_CHARAC
{
    UINT8   ChannelStatisticsSupported;
    UINT8   MediumAndDataRatesSupported;
    UINT16  IecVersion;
    UINT8   NumOfChannels;
    UINT8   PowerMode;
} NM_OBJ_PLME_BSC_CHARAC;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_PLME_BSC_INFO
{
    UINT8 InterfaceMode;
    UINT8 LoopBackMode;
    UINT8 XmitEnabled;
    UINT8 RcvEnabled;
    UINT8 PreferredReceiveChannel;
    UINT8 MediaTypeSelected;
    UINT8 ReceiveSelect;
} NM_OBJ_PLME_BSC_INFO;
#pragma pack()

#pragma pack(1)
typedef struct _NM_OBJ_MME_WIRE_STAT
{
    UINT8 ChannelNumber;
    UINT32 TotalGoodMsgsSent;
    UINT32 TotalGoodMsgsRcvd;
    UINT16 NumOfJabberFaults;
    UINT16 NumOfInternAndJabberFaults;
    UINT16 NumEndActivityBeforeEndData;
} NM_OBJ_MME_WIRE_STAT;
#pragma pack()

typedef struct _NM_VCR {
    UINT8                   vcr_lst_ctrl;
    NM_OBJ_VCR_LST_CHARAC   vcr_lst_charac;
    NM_OBJ_VCR_STATIC_ENRY  vcr_static_entry;
    NM_OBJ_VCR_DYN_ENTRY    vcr_dyn_entry;
    NM_OBJ_VCR_STAT_ENTRY   vcr_stat_entry;
} NM_VCR;
#pragma pack()

#pragma pack(1)
typedef struct _NM_DLME_BASIC {
    NM_OBJ_DLME_BSC_CHARAC  dlme_basic_charc;
    NM_OBJ_DLME_BSC_INFO    dlme_basic_info;
    NM_OBJ_DLME_STAT        dlme_stat_entry;
} NM_DLME_BASIC;
#pragma pack()

#pragma pack(1)
typedef struct _NM_DLME_LM {
    UINT8                   dlme_lm_cap;
    NM_OBJ_DLME_LM_INFO     dlme_lm_info;
    UINT8                   primary_lm_flag;
    UINT8                   live_lst_arr[32];
    /*MAX_TOK_HOLD_TIME_ARR,*/
    UINT8                   boot_operat;
    NM_OBJ_DLME_LM_LINKSETT curr_link_sett;
    NM_OBJ_DLME_LM_LINKSETT conf_link_sett;
    /*DLME_LM_STAT_REC,
    LST_VAL_REC,*/
    NM_OBJ_LS_CHARAC        ls_charac;
    NM_OBJ_SCH_DES          sch_descriptor;
} NM_DLME_LM;
#pragma pack()

#pragma pack(1)
typedef struct _NM_PLME
{
    NM_OBJ_PLME_BSC_CHARAC  plme_bsc_charac;
    NM_OBJ_PLME_BSC_INFO    plme_bsc_info;
    NM_OBJ_MME_WIRE_STAT    mme_wire_stat;
} NM_PLME;
#pragma pack()

#pragma pack(1)
typedef struct _CM_NM_OBJ {
    NM_OBJ_STACK_CAP    stack_mgmt;
    NM_VCR              var_list;
    NM_DLME_BASIC       dlme_basic;
    NM_DLME_LM          dlme_lm;
    NM_PLME             plme;
} CM_NM_OBJ;
#pragma pack()

/* Added by jayanth for Scanning list */

/*
 *  The Device Data Table
 */

typedef struct {
    char        blk_tag[65];
    char        dd_tag[65];
    OBJECT_INDEX    op_index;
    int             class_type;
    int             class_num;
    UINT32          dd_blk_id;
    int             param_count;
    int             profile;
    unsigned char   mode_block;
} SCAN_BLK_DATA;

typedef struct {
    int          count;
    SCAN_BLK_DATA   *list;
} SCAN_BLK_DATA_TBL;


typedef struct _MIB_ADDR {
    UINT8           cr;
    UINT16          remote;
    UINT16          local;
    OD_DESCRIPTION_SPECIFIC od_desc;
    T_SM_DIR        sm_dir;
    T_NM_DIR        nm_dir;
    CM_NM_OBJ       remote_nm_obj;
} MIB_ADDR;

typedef struct _FBAP_ADDR {
    UINT8           cr;
    UINT16          remote;
    UINT16          local;
    OD_DESCRIPTION_SPECIFIC od_desc;
} FBAP_ADDR;

typedef struct _SCAN_TBL {
    UINT8           station_address;
    DD_DEVICE_ID    dd_device_id;
    int             phys_blk_count;
    int             funct_blk_count;
    int             trans_blk_count;
    SCAN_BLK_DATA_TBL   blk_data_tbl;
    struct _SCAN_TBL        *next;
}SCAN_TBL;

typedef struct _SCAN_DEV_TBL{
    int             count;
    SCAN_TBL        *list;

}SCAN_DEV_TBL;

typedef struct _DEV_TBL {
    UINT8           station_address;
    DD_DEVICE_ID    dd_device_id;
    int             phys_blk_count;
    int             funct_blk_count;
    int             trans_blk_count;
    SCAN_BLK_DATA_TBL   blk_data_tbl;
    struct _SCAN_TBL        *next;
}DEV_TBL;

typedef struct {
    NET_ROUTE    network_route;
    int          device_family;
    int          config_file_id;
    MIB_ADDR     mib_addr;
    FBAP_ADDR    fbap_addr;
    DEV_TBL      dev_table;
} NETWORK_TBL_ELEM;

typedef struct {
    int                  count;
    NET_TYPE     network_type;
    NETWORK_TBL_ELEM    *list;
} NETWORK_TBL;


typedef struct {
    int             network;
    NET_TYPE        network_type;
    NET_ROUTE       network_route;
    UINT16          station_address;
    DD_DEVICE_ID    dd_device_id;
} IDENTIFY_CNF_A ;



typedef struct {
    int                     type ;
    union {

        IDENTIFY_CNF_A  cnf_a ;


    } identify_cnf ;
} IDENTIFY_CNF ;


typedef struct {
    OBJECT_INDEX    op_index;
    int             network;
    NET_TYPE        network_type;
    NET_ROUTE       network_route;
    UINT16          station_address;
    DD_DEVICE_ID    dd_device_id;
} FIND_CNF_A ;

typedef struct {
    ADDR_TYPE       addr_type ;
    LONG_ADDR       long_addr ;
    int             short_addr ;
} FF_ADDRESS ;


typedef struct {
    int                 type ;
    union {


        FIND_CNF_A  find_cnf_a ;

    } find_cnf ;
} FIND_CNF;

#define CT_OPOD     1
#define CT_DDOD     2
#define CT_MIB      3

typedef int COMM_TYPE;


#define MAX_VCR 5
#define MAX_VCR_ENTRIES       128

/* FAS Application relationship Type and Role */
#define AR_TYPE_CLIENT                0xA2
#define AR_TYPE_SERVER                0x32
#define AR_TYPE_SOURCE                0x44
#define AR_TYPE_SINK                  0x54



/*
 *  System Manager Tables
 */

typedef struct {

    char     pd_tag [34];       /* physical device tag      */
    char     dev_id [34];       /* device id                */
    char	 dup_pd_tag [34];   /* PD tag with full length of 34 bytes */
    char 	 dup_dev_id [34];   /* Device ID with full length of 34 bytes */

} CR_SM_IDENTIFY_CNF;


/****************************************************************************/
/* Confirmation for Load-DLL-Configuration Service                          */
/****************************************************************************/

typedef struct _CM_NMA_INFO
{
    UINT16         stack_mgmt_idx;     /* first index: StackManagement objects    */
    UINT16         vcr_list_idx;       /* first index: VCR List objects           */
    UINT16         dlme_basic_idx;     /* first index: DlmeBasic objects          */
    UINT16         dlme_lm_idx;        /* first index: DlmeLinkMaster objects     */
    UINT16         link_sched_idx;     /* first index: LinkScheduleList objects   */
    UINT16         dlme_bridge_idx;    /* reserved for future use                 */
    UINT16         plme_basic_idx;     /* first index: PlmeBasic objects          */
    UINT16         sm_agent_idx;       /* first index: SM agent objects           */
    UINT16         sync_sched_idx;     /* first index: SyncAndSchedule objects    */
    UINT16         addr_assign_idx;    /* first index: Address Assignment objects */
    UINT16         vfd_ref_idx;        /* first index: VFD reference list objects */
    UINT16         fb_sched_idx;       /* first index: FB schedule objects        */
    UINT16         unused_memory;      /* free memory, not used by block manager  */

} CM_NMA_INFO;

typedef struct _CM_NMA_VCR_ENTRY
{
    UINT16         index;
    UINT8          type_and_role;
    UINT16         loc_addr;
    UINT16         rem_addr;
    UINT8          sdap;
    UINT8          features[4];

} CM_NMA_VCR_ENTRY;

typedef struct _CM_NMA_VCR_LOAD
{
    UINT16              cr_index;
    UINT16              index;
    UINT16              loc_add;
    UINT16              rem_add;
    UINT8               nma_wr_flag;
    UINT16              nma_no_vcr_entries;
    CM_NMA_INFO         nma_info;
    CM_NMA_VCR_ENTRY    nma_vcr_entry[MAX_VCR_ENTRIES];
} CM_NMA_VCR_LOAD;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern char dd_path[];

/*
 *  Function Prototypes that may be used in the Convenience Macros.
 */

/* Function Prototypes for Active Block Information */

extern void clr_abt_tbls (void) ;

extern int set_abt_dd_blk_tbl_offset (BLOCK_HANDLE, int);
extern int set_abt_param_count (BLOCK_HANDLE, unsigned int);
extern int set_abt_dd_blk_id (BLOCK_HANDLE, UINT32);

extern int set_abt_tag (BLOCK_HANDLE, char *);
extern int set_abt_op_index (BLOCK_HANDLE, OBJECT_INDEX);

extern int set_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE) ;
extern int set_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ;
extern int set_abt_app_info (BLOCK_HANDLE, void *);
// CPMHACK - Declare set method for pc info
extern int set_abt_app_info (BLOCK_HANDLE, void *);
extern int set_abt_adt_offset (BLOCK_HANDLE, DEVICE_HANDLE);
extern int set_abt_usage (BLOCK_HANDLE, int);
extern int set_abt_dd_handle (BLOCK_HANDLE, DD_HANDLE *);
extern int set_abt_dd_dev_tbls (BLOCK_HANDLE, void *);
extern int set_abt_dev_family (BLOCK_HANDLE, int);

/* Function Prototypes for Active Device Information */

extern void clr_adt_tbls (void) ;

extern int set_adt_network (DEVICE_HANDLE, NETWORK_HANDLE);
extern int set_adt_station_address (DEVICE_HANDLE, UINT16);
extern int set_adt_oprod_handle (DEVICE_HANDLE, ROD_HANDLE);
extern int set_adt_adtt_offset (DEVICE_HANDLE, DEVICE_TYPE_HANDLE);
extern int set_adt_app_info (DEVICE_HANDLE, void *);
extern int set_adt_usage (DEVICE_HANDLE, int);


extern int set_adt_a_opod_cref (DEVICE_HANDLE, CREF);
extern int set_adt_a_ddod_cref (DEVICE_HANDLE, CREF);
extern int set_adt_a_mib_cref (DEVICE_HANDLE, CREF);


extern int set_adt_dd_handle (DEVICE_HANDLE, DD_HANDLE *);
extern int set_adt_dd_dev_tbls (DEVICE_HANDLE, void *);
extern int set_adt_dd_device_id (DEVICE_HANDLE, DD_DEVICE_ID *) ;
extern int set_adt_dev_family (DEVICE_HANDLE, int);
extern int set_adt_net_type (DEVICE_HANDLE, NET_TYPE);
extern int set_adt_net_route (DEVICE_HANDLE, NET_ROUTE *);

/* Function Prototype for Active Thread Information */
extern int set_adt_thread_id( pthread_t);

/* Function Prototypes for Active Device Type Information */

extern void clr_adtt_tbls (void) ;

extern int set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE, void *);
extern int set_adtt_dd_device_id (DEVICE_TYPE_HANDLE, DD_DEVICE_ID *);
extern int set_adtt_dd_handle (DEVICE_TYPE_HANDLE, DD_HANDLE *);
extern int set_adtt_app_info (DEVICE_TYPE_HANDLE, void *);
extern int set_adtt_usage (DEVICE_TYPE_HANDLE, int);

/* Function Prototypes for Network Information */

extern int set_nt_net_type (NETWORK_HANDLE, NET_TYPE);
extern int set_nt_net_route (NETWORK_HANDLE, NET_ROUTE *);
extern int set_nt_dev_family (NETWORK_HANDLE, int);
extern int set_nt_cfg_file_id (NETWORK_HANDLE, int);

extern int set_nt_net_fbap_cr(NETWORK_HANDLE, UINT16);
extern int set_nt_net_mib_cr(NETWORK_HANDLE, UINT16);
extern int set_nt_net_fbap_addr(NETWORK_HANDLE, UINT16 );
extern int set_nt_net_mib_addr(NETWORK_HANDLE, UINT16 );
extern int set_nt_net_fbap_local_addr(NETWORK_HANDLE, UINT16 );
extern int set_nt_net_mib_local_addr(NETWORK_HANDLE, UINT16 );
extern int set_nt_dev_tbl (NETWORK_HANDLE, DEV_TBL *);
extern int set_nt_net_fbap_od(NETWORK_HANDLE , OD_DESCRIPTION_SPECIFIC *);
extern int set_nt_net_mib_od(NETWORK_HANDLE , OD_DESCRIPTION_SPECIFIC *);
extern int set_nt_net_sm_dir(NETWORK_HANDLE ,T_SM_DIR *);
extern int set_nt_net_nm_dir(NETWORK_HANDLE ,T_NM_DIR *);

/*Function Prototypes for Network Management Objects*/

extern int set_nmo_stack_cap_rec(NM_OBJ_STACK_CAP *);
extern int set_nmo_vcr_lst_ctrl(UINT8 *);
extern int set_nmo_vcr_lst_charac(NM_OBJ_VCR_LST_CHARAC *);
extern int set_nmo_vcr_lst_static(NM_OBJ_VCR_STATIC_ENRY *);
extern int set_nmo_dlme_bsc_charac(NM_OBJ_DLME_BSC_CHARAC *);
extern int set_nmo_dlme_bsc_info(NM_OBJ_DLME_BSC_INFO *);
extern int set_nmo_dlme_lm_cap(UINT8 *);
extern int set_nmo_dlme_lm_info(NM_OBJ_DLME_LM_INFO *);
extern int set_nmo_dlme_lm_pri_flag(UINT8 *);
extern int set_nmo_dlme_lm_boot_op(UINT8 *);
extern int set_nmo_dlme_lm_conf_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_dlme_lm_curr_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_dev_stack_cap_rec(NETWORK_HANDLE , NM_OBJ_STACK_CAP *);
extern int set_nmo_dev_vcr_lst_ctrl(NETWORK_HANDLE , UINT8 *);
extern int set_nmo_dev_vcr_lst_charac(NETWORK_HANDLE , NM_OBJ_VCR_LST_CHARAC *);
extern int set_nmo_dev_vcr_lst_static(NETWORK_HANDLE , NM_OBJ_VCR_STATIC_ENRY *);
extern int set_nmo_dev_dlme_bsc_charac(NETWORK_HANDLE , NM_OBJ_DLME_BSC_CHARAC *);
extern int set_nmo_dev_dlme_bsc_info(NETWORK_HANDLE , NM_OBJ_DLME_BSC_INFO *);
extern int set_nmo_dev_dlme_lm_cap(NETWORK_HANDLE , UINT8 *);
extern int set_nmo_dev_dlme_lm_info(NETWORK_HANDLE , NM_OBJ_DLME_LM_INFO *);
extern int set_nmo_dev_dlme_lm_pri_flag(NETWORK_HANDLE , UINT8 *);
extern int set_nmo_dev_dlme_lm_boot_op(NETWORK_HANDLE , UINT8 *);
extern int set_nmo_dev_dlme_lm_conf_link_sett(NETWORK_HANDLE , NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_dev_dlme_lm_curr_link_sett(NETWORK_HANDLE , NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_dev_dlme_bsc_stat(NETWORK_HANDLE, NM_OBJ_DLME_STAT *);
extern int set_nmo_dev_vcr_lst_dyn(NETWORK_HANDLE, NM_OBJ_VCR_DYN_ENTRY *);
extern int set_nmo_vcr_stat_entry(NM_OBJ_VCR_STAT_ENTRY *);
extern int set_nmo_dev_vcr_stat_entry(NETWORK_HANDLE, NM_OBJ_VCR_STAT_ENTRY *);
extern int set_nmo_dlme_bsc_stat(NM_OBJ_DLME_STAT *);
extern int set_nmo_dev_dlme_bsc_stat(NETWORK_HANDLE, NM_OBJ_DLME_STAT *);
extern int set_nmo_dlme_live_list_arr(UINT8 *);
extern int set_nmo_dev_dlme_live_list_arr(NETWORK_HANDLE, UINT8 *);
extern int set_nmo_dlme_lm_config_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_dev_dlme_lm_config_link_sett(NETWORK_HANDLE, NM_OBJ_DLME_LM_LINKSETT *);
extern int set_nmo_vcr_lst_dyn(NM_OBJ_VCR_DYN_ENTRY *);

/* Function Prototypes for Active Block Information */

extern int get_abt_tag (BLOCK_HANDLE, char *);
extern int get_abt_count (void);
extern int get_abt_op_index (BLOCK_HANDLE, OBJECT_INDEX *);
extern int get_abt_param_count (BLOCK_HANDLE, unsigned int *);
extern int get_abt_dd_blk_id (BLOCK_HANDLE, ITEM_ID *);
extern int get_abt_dd_blk_tbl_offset (BLOCK_HANDLE, int *);


extern int get_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE *) ;
extern int get_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ;
extern int get_abt_app_info (BLOCK_HANDLE, void **);
extern int get_abt_app_info (BLOCK_HANDLE, void **);
// CPMHACK - Declare get method for pc info
extern int get_abt_pc_info (BLOCK_HANDLE, void **);
extern int get_abt_adt_offset (BLOCK_HANDLE, DEVICE_HANDLE *);
extern int get_abt_usage (BLOCK_HANDLE, int *);
extern int get_abt_dd_handle (BLOCK_HANDLE, DD_HANDLE *);
extern int get_abt_dd_dev_tbls (BLOCK_HANDLE, void **);
extern int get_abt_dev_family (BLOCK_HANDLE, int *);

/* Function Prototypes for Active Device Information */

extern int get_adt_count (void);
extern int get_adt_network (DEVICE_HANDLE, NETWORK_HANDLE *);
extern int get_adt_station_address (DEVICE_HANDLE, UINT16 *);
extern int get_adt_oprod_handle (DEVICE_HANDLE, ROD_HANDLE *);
extern int get_adt_app_info (DEVICE_HANDLE, void **);
extern int get_adt_adtt_offset (DEVICE_HANDLE, DEVICE_TYPE_HANDLE *);
extern int get_adt_usage (DEVICE_HANDLE, int *);


extern int get_adt_a_opod_cref (DEVICE_HANDLE, CREF *);
extern int get_adt_a_ddod_cref (DEVICE_HANDLE, CREF *);
extern int get_adt_a_mib_cref (DEVICE_HANDLE, CREF *);


extern int get_adt_dd_handle (DEVICE_HANDLE, DD_HANDLE *);
extern int get_adt_dd_device_id (DEVICE_HANDLE, DD_DEVICE_ID *) ;
extern int get_adt_dd_dev_tbls (DEVICE_HANDLE, void **);
extern int get_adt_dev_family (DEVICE_HANDLE, int *);
extern int get_adt_net_type (DEVICE_HANDLE, NET_TYPE *);
extern int get_adt_net_route (DEVICE_HANDLE, NET_ROUTE *);

extern int get_adt_fbap_cr(DEVICE_HANDLE, UINT8 *);

/* Function Prototype for Active Thread and mutex Information */
extern pthread_t *get_adt_thread_handle( void );
extern pthread_mutex_t *get_adt_thread_mutex( void );
extern ACTIVE_THREAD_DATA *get_adt_thread_data( void );

/* Function Prototypes for Active Device Type Information */

extern int get_adtt_count (void);
extern int get_adtt_dd_device_id (DEVICE_TYPE_HANDLE, DD_DEVICE_ID *);
extern int get_adtt_dd_handle (DEVICE_TYPE_HANDLE, DD_HANDLE *);
extern int get_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE, void **);
extern int get_adtt_app_info (DEVICE_TYPE_HANDLE, void **);
extern int get_adtt_usage (DEVICE_TYPE_HANDLE, int *);

/* Function Prototypes for Network Information */

extern int get_nt_net_count (int *);
extern int get_nt_net_handle_by_cr(int, UINT16, NETWORK_HANDLE *);
extern int get_nt_net_handle_by_addr(int, UINT16, NETWORK_HANDLE *);
extern int get_nt_addr_by_net_handle(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_type (NETWORK_HANDLE, NET_TYPE *);
extern int get_nt_net_route (NETWORK_HANDLE, NET_ROUTE *);
extern int get_nt_dev_family (NETWORK_HANDLE, int *);
extern int get_nt_cfg_file_id (NETWORK_HANDLE, int *);

extern int get_nt_net_fbap_cr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_mib_cr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_fbap_addr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_mib_addr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_fbap_local_addr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_net_mib_local_addr(NETWORK_HANDLE, UINT16 *);
extern int get_nt_dev_tbl (NETWORK_HANDLE, DEV_TBL *);
extern int get_nt_device_addr (NETWORK_HANDLE nh, UINT8 *station_address);
extern int get_nt_net_fbap_od(NETWORK_HANDLE , OD_DESCRIPTION_SPECIFIC *);
extern int get_nt_net_mib_od(NETWORK_HANDLE , OD_DESCRIPTION_SPECIFIC *);
extern int get_nt_net_sm_dir(NETWORK_HANDLE ,T_SM_DIR *);
extern int get_nt_net_nm_dir(NETWORK_HANDLE ,T_NM_DIR *);

/*Function Prototypes for Network Management Objects*/

extern int get_nmo_stack_cap_rec(NM_OBJ_STACK_CAP *);
extern int get_nmo_vcr_lst_ctrl(UINT8 *);
extern int get_nmo_vcr_lst_charac(NM_OBJ_VCR_LST_CHARAC *);
extern int get_nmo_vcr_lst_static(NM_OBJ_VCR_STATIC_ENRY *);
extern int get_nmo_vcr_lst_dyn(NM_OBJ_VCR_DYN_ENTRY *);
extern int get_nmo_dev_vcr_lst_dyn(NETWORK_HANDLE , NM_OBJ_VCR_DYN_ENTRY *);
extern int get_nmo_vcr_stat_entry(NM_OBJ_VCR_STAT_ENTRY *);
extern int get_nmo_dev_vcr_stat_entry(NETWORK_HANDLE , NM_OBJ_VCR_STAT_ENTRY *);
extern int get_nmo_dev_dlme_lm_config_link_sett(NETWORK_HANDLE , NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dlme_bsc_charac(NM_OBJ_DLME_BSC_CHARAC *);
extern int get_nmo_dlme_bsc_info(NM_OBJ_DLME_BSC_INFO *);
extern int get_nmo_dlme_lm_cap(UINT8 *);
extern int get_nmo_dlme_lm_info(NM_OBJ_DLME_LM_INFO *);
extern int get_nmo_dlme_lm_pri_flag(UINT8 *);
extern int get_nmo_dlme_lm_boot_op(UINT8 *);
extern int get_nmo_dlme_lm_conf_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dlme_lm_curr_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dev_stack_cap_rec(NETWORK_HANDLE , NM_OBJ_STACK_CAP *);
extern int get_nmo_dev_vcr_lst_ctrl(NETWORK_HANDLE , UINT8 *);
extern int get_nmo_dev_vcr_lst_charac(NETWORK_HANDLE , NM_OBJ_VCR_LST_CHARAC *);
extern int get_nmo_dev_vcr_lst_static(NETWORK_HANDLE , NM_OBJ_VCR_STATIC_ENRY *);
extern int get_nmo_dev_dlme_bsc_charac(NETWORK_HANDLE , NM_OBJ_DLME_BSC_CHARAC *);
extern int get_nmo_dev_dlme_bsc_info(NETWORK_HANDLE , NM_OBJ_DLME_BSC_INFO *);
extern int get_nmo_dev_dlme_lm_cap(NETWORK_HANDLE , UINT8 *);
extern int get_nmo_dlme_lm_config_link_sett(NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dev_dlme_lm_info(NETWORK_HANDLE , NM_OBJ_DLME_LM_INFO *);
extern int get_nmo_dev_dlme_lm_pri_flag(NETWORK_HANDLE , UINT8 *);
extern int get_nmo_dev_dlme_lm_boot_op(NETWORK_HANDLE , UINT8 *);
extern int get_nmo_dev_dlme_lm_conf_link_sett(NETWORK_HANDLE , NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dev_dlme_lm_curr_link_sett(NETWORK_HANDLE , NM_OBJ_DLME_LM_LINKSETT *);
extern int get_nmo_dev_dlme_live_list_arr(NETWORK_HANDLE , UINT8 *);
extern int get_nmo_dlme_live_list_arr(UINT8 *);
extern int get_nmo_dlme_bsc_stat(NM_OBJ_DLME_STAT *);
extern int get_nmo_dev_dlme_bsc_stat(NETWORK_HANDLE, NM_OBJ_DLME_STAT *);

/* Function Prototypes for Handle Validity */

extern int valid_block_handle (BLOCK_HANDLE);
extern int valid_device_handle (DEVICE_HANDLE);
extern int valid_device_type_handle (DEVICE_TYPE_HANDLE);
extern int valid_network_handle (NETWORK_HANDLE);
extern int valid_rod_handle (ROD_HANDLE);
extern int valid_dd_handle (DD_HANDLE *);

/*
 *  Connection Manager Interface Function Prototypes
 */


/* cm_init.c - Connection Manager Initialization Functions */

extern int cm_init (ENV_INFO *, const char *, HOST_CONFIG *);
extern int cm_config_stack (ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG *);
extern void cm_init_dds_tbl_fn_ptrs (DDS_TBL_FN_PTRS *);
extern void cm_init_app_info_fn_ptrs (APP_INFO_FN_PTRS *);
extern int cm_get_stack_config(ENV_INFO *, CM_NMA_INFO *);

extern int cm_events(ENV_INFO *, char *);

/* cm_tbl.c - Connection Manager Table Functions */

extern int ct_block_open (char *, BLOCK_HANDLE *);
extern int ct_block_close (BLOCK_HANDLE);
extern BLOCK_HANDLE ct_block_search (const char *);
extern BLOCK_HANDLE ct_substation_search (DEVICE_HANDLE, int);
extern void ct_display_tables (void);

extern int ct_op_index_to_param_offset (BLOCK_HANDLE, OBJECT_INDEX, int *);
extern int ct_param_offset_to_op_index (BLOCK_HANDLE, int, OBJECT_INDEX *);



/* cm_blkld.c - Connection Manager Block Load Functions */

extern int bl_block_load (BLOCK_HANDLE, ENV_INFO *);


/* cm_dds.c - Connection Manager DD Support Functions */

extern int ds_dd_device_load (DEVICE_HANDLE, DD_HANDLE *);
extern int ds_dd_block_load (BLOCK_HANDLE, DD_HANDLE *);
extern int ds_dd_device_load_ex (DEVICE_HANDLE, char *, DD_HANDLE *);
extern int ds_dd_block_load_ex (BLOCK_HANDLE, char *, DD_HANDLE *);
extern int ds_ddod_file_load (char *, ROD_HANDLE *, DEVICE_HANDLE );

/* Definitions for symbol file search mode.*/
#define SM_EXPLICIT_DDREV   1
#define SM_LATEST_DDREV     2
#define SM_EXPLICIT_FFO 3
#define SM_EXPLICIT_FF5 4

extern int ds_blk_name_to_item_id (DD_DEVICE_ID *, int, char *, UINT32 *);
extern int ds_blk_item_id_to_name (DD_DEVICE_ID *, int, char *, UINT32 *);

/* evl_enum.c - Enumeration suppression support */
/* This function allows the host to return the current list of suppressed
 * enumeration strings to DDS, so that DDS may edit the list of enumerations
 * to be returned to the host.  If max_len is too small for the entire list
 * then the host must return a nonzero error code AND fill in the max_len
 * required for the full list.  DDS will free the list when it is done with
 * the current enumeration evaluation.  If the host returns a nonzero return
 * code without changing max_len, the call is considered to have failed, and
 * no suppression will occur.
 */
//extern int get_enum_suppression_list(ENV_INFO2 *env, char *str, unsigned int *max_len);
extern int (*get_enum_suppression_list_ptr) (ENV_INFO2 *env, char *str, unsigned int *max_len);

/* cm_comm.c - Connection Manager Communication Support Functions */

extern int cr_comm_get
    (DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT **, ENV_INFO *);
extern int cr_comm_put
    (DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT *, ENV_INFO *);
extern int cr_comm_read
    (DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX, CM_VALUE_LIST *,
            ENV_INFO *);
extern int cr_comm_write
    (DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX, CM_VALUE_LIST *,
            ENV_INFO *);
extern int cr_comm_upload
    (DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, UINT8 *, ENV_INFO *);
extern void cr_object_free (OBJECT *);


extern int ct_addr_open
    (DEVICE_HANDLE, OBJECT_INDEX, BLOCK_HANDLE *) ;


extern int cr_param_read (ENV_INFO *, const P_REF *) ;
extern int cr_param_write (ENV_INFO *, const P_REF *) ;
extern int cr_param_load (ENV_INFO *, const P_REF *, COMM_VALUE_LIST *) ;
extern int cr_param_unload (ENV_INFO *,const P_REF *, COMM_VALUE_LIST *) ;
extern void cr_free_value_list (COMM_VALUE_LIST *);

extern int cr_init (ENV_INFO *, HOST_CONFIG *);
extern int cr_config_stack (ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG *);
extern int cr_scan_network(ENV_INFO *, SCAN_DEV_TBL *);
extern int cr_build_device_table( ENV_INFO*, NETWORK_HANDLE );
extern int cr_delete_device_table( ENV_INFO*, NETWORK_HANDLE );
extern int cr_get_stack_config(ENV_INFO *, CM_NMA_INFO *);

extern int cr_initiate_comm (DEVICE_HANDLE, ENV_INFO *);
extern int cr_terminate_comm (DEVICE_HANDLE, ENV_INFO *);
extern int cr_find_block (char *, FIND_CNF *, ENV_INFO *);

extern int cr_ident (NETWORK_HANDLE, ENV_INFO *, const IDENTIFY_CNF *);


extern int cr_assign_tag (ENV_INFO *,
                          unsigned short int, CR_SM_IDENTIFY_CNF *) ;
extern int cr_read_tag (ENV_INFO *) ;
extern int cr_send_cmd (ENV_INFO *, unsigned short) ;

extern int cr_create_block
    (DEVICE_HANDLE, OBJECT_INDEX, ITEM_ID, char *, ENV_INFO *);
extern int cr_delete_block ( ENV_INFO *) ;


/*
 *  System Management related functions
 */

extern int cr_sm_ident
    (ENV_INFO *, unsigned short, CR_SM_IDENTIFY_CNF *);
extern int cr_set_poll_addr (ENV_INFO *, int, char *) ;
extern int cr_clear_poll_addr(ENV_INFO *, int,
                              CR_SM_IDENTIFY_CNF* );
extern int cr_clear_tag(ENV_INFO *, int,
                        CR_SM_IDENTIFY_CNF* );

extern int cr_assign_tag (ENV_INFO *, unsigned short int, CR_SM_IDENTIFY_CNF *);

/*
 *  Network Management Related functions
 */

extern int cr_build_network_table(NETWORK_HANDLE );

extern int cr_wr_local_vcr (ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ) ;
extern int cr_fms_initiate (ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ) ;
extern int cr_get_od (ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD* ) ;
extern int cr_nma_read (ENV_INFO *, NETWORK_HANDLE, unsigned short, int, int) ;
extern int cr_dev_boot (ENV_INFO *, NETWORK_HANDLE, unsigned short, int) ;
extern int cr_cm_exit (ENV_INFO *, unsigned char) ;
extern int cr_cm_firmware_download (ENV_INFO *, char *) ;
extern int cr_cm_get_host_info(ENV_INFO *, HOST_INFO *, HOST_CONFIG *);
extern int cr_cm_reset_fbk(ENV_INFO *, NETWORK_HANDLE , HOST_CONFIG *);
extern int cr_set_appl_state_busy(ENV_INFO *, int *);
extern int cr_set_appl_state_normal(ENV_INFO *);
extern int cr_abort_cr(ENV_INFO *, unsigned short, unsigned short);

/*
 * Unsolicited error or indication event either from FBK2 board or remote
 * device
 */

extern int (*appl_event_indication) (ENV_INFO *env_info, char *buffer);

extern int offline_initialize (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* CM_LIB_H */

