/*****************************************************************************/
/*  Filename    : ffh1_error.h                                               */
/*                                                                           */
/*  Description : This file redirects the reader to fbkhost_error.h in the    */
/*                Softing FF stack for the FBK HOST configuration            */
/*                                                                           */
/*****************************************************************************/

#ifndef __FFH1_ERROR__
#define __FFH1_ERROR__

#include "fbkhost_error.h"

#endif /* __FFH1_ERROR__ */
