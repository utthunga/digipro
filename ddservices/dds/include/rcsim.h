/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)rcsim.h	30.3 14 Nov 1996
 */

#ifndef RCSIM__H
#define RCSIM__H

#include "std.h"
#include "cm_lib.h"

/*
 * response code classes.
 * (be sure to update tst_rc.c lists if this added to / removed from.)
 */
#define RCSIM_TYPE_RESPONSE_CODE		1
#define RCSIM_TYPE_DISCONNECT			2
#define RCSIM_TYPE_RECONNECT			3
#define RCSIM_TYPE_COMM_ERR				4
#define RCSIM_TYPE_RETURN_CODE			5
#define RCSIM_TYPE_DEVICE_STATUS		6


/* indicates that response code is for network level and on up */
#define RCSIM_NO_DEVICE_ADDRESS			-1


/*
 * rcsim_set() / rcsim_get() parameters structure.
 */
typedef struct {
	int				network_handle;
	NET_TYPE		network_type;	/* as defined in cm_lib.h (NT_xxx types)*/
	int				device_station_address;
}	RCSIM_PARAM;

/*
 * Response value type.
 */
typedef struct {
	unsigned short	type;	/* valid types are defined in ddldefs.h (ie. DDS_INTEGER) */
	size_t			size;
	void			*value; /* pointer to value. */
} RCSIM_VALUE; 


/*
 * exported functions.
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int rcsim_init P(());
extern int rcsim_free P(());
extern int rcsim_set  P((int, const RCSIM_PARAM *, const RCSIM_VALUE *));
extern int rcsim_get  P((int, const RCSIM_PARAM *, RCSIM_VALUE *, size_t));
extern int rcsim_get_first  P((int *, RCSIM_PARAM **, RCSIM_VALUE *, size_t));
extern int rcsim_get_next  P((int *, RCSIM_PARAM **, RCSIM_VALUE *, size_t));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif				/* RCSIM__H */
