/*****************************************************************************/
/*                            LOCALFUNCTION DECLARATION                     */
/*                                                                           */
/*  Filename    : device_access.h                                                   */
/*                                                                           */
/*  Description : This file contains the local function declarations of      */
/*                FFusb device access demonstration application              */
/*                                                                           */
/*  CHANGE_NOTES                                                             */
/*                                                                           */
/*  date      name      change                                               */
/*  -----------------------------------------------------------------------  */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

#ifndef __DEVACC_H__
#define __DEVACC_H__

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include "cm_lib.h"
#include "fbapi_if.h"
#include "ff_if.h"
#include "ff_defaults.h"

#define MAX_FF_DATA_LEN					   512

#define DEVACC_APPL_STATE_START            0x01
#define DEVACC_APPL_STATE_CFG              0x02
#define DEVACC_APPL_STATE_LOAD_SM          0x03
#define DEVACC_APPL_STATE_LOAD_DLL         0x04
#define DEVACC_APPL_STATE_VCRL_INIT        0x05
#define DEVACC_APPL_STATE_VCRL_LOAD        0x06
#define DEVACC_APPL_STATE_VCRL_TERM        0x07
#define DEVACC_APPL_STATE_READY            0x08
#define DEVACC_APPL_STATE_END              0x09
#define DEVACC_APPL_STATE_FMS_INITIATE     0x0a
#define DEVACC_APPL_STATE_VCR_LOCAL_WR     0x0b
#define DEVACC_APPL_STATE_SCAN             0x0c
#define DEVACC_APPL_STATE_MGMT_READ        0x0d
#define DEVACC_APPL_STATE_MGMT_WRITE       0x0e
#define DEVACC_APPL_STATE_FMS_READ         0x0f
#define DEVACC_APPL_STATE_FMS_WRITE        0x10
#define DEVACC_APPL_STATE_ABORT            0x11
#define DEVACC_APPL_STATE_EVENT_REQ        0x12
#define DEVACC_APPL_STATE_SM_SET_CLR_TAG   0x13
#define DEVACC_APPL_STATE_SM_SET_ADDR      0x14
#define DEVACC_APPL_STATE_SM_CLR_ADDR      0x15
#define DEVACC_APPL_STATE_SM_IDENTIFY      0x16
#define DEVACC_APPL_STATE_NM_RESET         0x17
#define DEVACC_APPL_STATE_NMA_DATA_QUERY   0x18

#define DEVACC_APPL_ADD_VCR_ENTRY          0x79


typedef int  (*PROCESS_CON_FNC)   (T_FIELDBUS_SERVICE_DESCR *p_con_sdb, USIGN8 *p_con_data);

#define VFD_NR_MIB      1
#define VFD_NR_FBAP     2

// Max VCR reference supported by FBK host board
#define MAX_VCR_REF     5

typedef struct _T_VCR_TABLE
{
    USIGN8      type_and_role;  /* as in VCR object */
    USIGN8      node_address;   /* Node address of device if client VCR */
    USIGN8      vfd_nr;         /* 1: MIB; 2. FBAP VFD */

} T_VCR_TABLE;


typedef struct _T_VCR_INIT
{

  USIGN8  fas_ar_type_and_role;
           /* 1) applic. relationship type / role  */
  USIGN8  fas_dll_priority;
               /* 4.4) priority                        */
  USIGN8  fas_dll_authentication;
         /* 4.3) authentication                  */
  USIGN8  fas_dll_delivery;
               /* 4.2) delivery                        */
  USIGN16 fas_dll_size;
                   /* 7) FasDllMaxDlsduSize                */
  USIGN32 fas_dll_local_addr;
             /* 2) FasDllLocalAddr                   */
  USIGN32 fas_dll_configured_remote_addr;
 /* 3) FasDllConfiguredRemoteAddr        */

} T_VCR_INIT;

typedef struct _T_VFD_LIST
{
    USIGN32     vfd_ref;
    USIGN8      vfd_tag[SM_TAG_SIZE];
} T_VFD_LIST;

typedef struct _T_VCR_INFO
{
    USIGN16     max_entries;
    USIGN16     num_perm_entries;
    USIGN16     num_config_entries;
    USIGN16     first_unconfig_entry;

} T_VCR_INFO;

typedef struct _T_DEV_INFO
{
    USIGN32             manufacturer_id;
    USIGN16             device_type;
    USIGN8              device_rev;
    USIGN8              dd_rev;
    USIGN32             min_cycle_time;

} T_DEV_INFO;

typedef struct _T_DIR_ENTRY
{
    USIGN16     first_index;
    USIGN16     n_entries;

} T_DIR_ENTRY;


typedef struct _T_BLOCK_INFO
{
    USIGN16             index;
    USIGN16             n_param;
    USIGN32             dd_member_id;
    USIGN32             dd_item_id;
    USIGN16             profile_number;
    USIGN16             profile_rev;

} T_BLOCK_INFO;

typedef struct _T_VCR_ENTRY
{
    USIGN8          type_and_role;
    USIGN16         loc_addr;
    USIGN16         rem_addr;
    USIGN8          sdap;
    USIGN8          features[4];

} T_VCR_ENTRY;

#define N_VCR_ENTRIES       128

/** Directory objects:
  */
#define N_SM_DIR_ENTRIES    7       /* number of SM directory entries due to FF spec */
#define N_NM_DIR_ENTRIES    9       /* number of NM directory entries due to FF spec */
#define N_FBAP_DIR_ENTRIES  150     /* limit of this implemenation, real value depends on device */

/* Directory entry indices */
#define SM_DIR_SM_AGENT             0
#define SM_DIR_SYNC_AND_SCHEDULING  1
#define SM_DIR_ADDRESS_ASSIGNMENT   2
#define SM_DIR_VFD_LIST             3
#define SM_DIR_FB_SCHEDULE          4
#define SM_DIR_SOFTWARE_DOWNLOAD    5
#define SM_DIR_SW_DOWNLOAD_DOMAINS  6

#define NM_DIR_STACK_MGT            0
#define NM_DIR_VCR_LIST             1
#define NM_DIR_DLME_BASIC           2
#define NM_DIR_DLME_LINKMASTER      3
#define NM_DIR_LINK_SCHEDULELIST    4
#define NM_DIR_DLME_BRIDGE          5
#define NM_DIR_PLME_BASIC           6

#define FBAP_DIR_ACTION_OBJECT      0
#define FBAP_DIR_LINK_OBJECT        1
#define FBAP_DIR_ALERT_OBJECT       2
#define FBAP_DIR_TREND_OBJECT       3
#define FBAP_DIR_DOMAIN_OBJECT      4
#define FBAP_DIR_MVC_OBJECT         5
#define FBAP_DIR_MVC_VARIABLE_LIST  6
#define FBAP_DIR_SIS_LINK_OBJECT    7

#define FBAP_DIR_RB_DIR_INDEX       0
#define FBAP_DIR_TB_DIR_INDEX       1
#define FBAP_DIR_FB_DIR_INDEX       2

#define MAX_NO_OF_BLOCKS            40

typedef enum _T_DEVACC_STATE {

   DEVACC_STATE_IDLE,
   DEVACC_STATE_OPEN_MGMT_VCR,
   DEVACC_STATE_WAIT_FOR_MGMT_VCR,
   DEVACC_STATE_GET_MGMT_OD_HDR,
   DEVACC_STATE_READ_SM_DIR,
   DEVACC_STATE_READ_NM_DIR,
   DEVACC_STATE_READ_VFD_REF_LIST,
   DEVACC_STATE_READ_VCR_CHARACT,
   DEVACC_STATE_READ_MGMT_VCR_ENTRY,
   DEVACC_STATE_SCAN_VCR_LIST,
   DEVACC_STATE_WRITE_VCR_ENTRY,
   DEVACC_STATE_CLOSE_MGMT_VCR,
   DEVACC_STATE_OPEN_FBAP_VCR,
   DEVACC_STATE_GET_FBAP_OD_HDR,
   DEVACC_STATE_READ_AP_DIR,
   DEVACC_STATE_READ_RB_HDR,
   DEVACC_STATE_READ_TB_HDR,
   DEVACC_STATE_READ_FB_HDR,
   DEVACC_STATE_READ_MFG_ID,
   DEVACC_STATE_READ_DEV_TYPE,
   DEVACC_STATE_READ_DEV_REV,
   DEVACC_STATE_READ_DD_REV,
   DEVACC_STATE_READ_MIN_CYCLE_T

} T_DEVACC_STATE;

typedef struct _DEVICE_ATTRIBUTES
{
    USIGN8              node_address;
    USIGN8              pd_tag[SM_TAG_SIZE];
    USIGN8              dev_id[SM_TAG_SIZE];
    T_DEVACC_STATE      device_state;
    USIGN16             index;
    T_VCR_ENTRY         vcr_entry[N_VCR_ENTRIES];
    USIGN16             start_s_od_mib;
    USIGN16             start_s_od_fbap_vfd;
    T_DIR_HEAD        sm_dir_header;
    T_DIR_ENTRY         sm_directory[N_SM_DIR_ENTRIES];
    T_DIR_HEAD        nm_dir_header;
    T_DIR_ENTRY         nm_directory[N_NM_DIR_ENTRIES];
    T_DIR_HEAD        fbap_dir_header;
    T_DIR_ENTRY         fbap_directory[N_FBAP_DIR_ENTRIES];
    T_DIR_ENTRY         block_list[FBAP_DIR_FB_DIR_INDEX+1];
    int                 fbap_dir_entry;
    USIGN16             start_vcr_od;
    USIGN32             mgmt_vfd_ref;
    USIGN32             fbap_vfd_ref;
    T_VFD_LIST          vfd_list[2];
    T_VCR_INFO          vcr_info;
    T_DEV_INFO          dev_info;
    T_BLOCK_INFO        rb;
    T_BLOCK_INFO        tb[MAX_NO_OF_BLOCKS];
    T_BLOCK_INFO        fb[MAX_NO_OF_BLOCKS];

}   T_DEVICE_ATTRIBUTES;

typedef struct _T_CURR_PARARM_INDEX
{
    unsigned short index;
    int sub_index;
    unsigned short cr;
} T_CURR_PARARM_INDEX;


typedef struct _T_APPL_ADMIN
{
    USIGN8              host_node_addr;
    int					host_type;
    USIGN8              role;
    int                 state;
    int					appl_state;
    int                 channel;
    FB_BOOL             conn_open;
    USIGN16             local_nma_read;
    USIGN16             local_nma_write;
    USIGN16             aux;
    FB_INT16            od_version;
    FB_INT8             invoke_id;
    USIGN16             index;
    int					socket;
    T_NMA_INFO          host_stack_mib_directory;
    PROCESS_CON_FNC     proc_mgmt_con;
    T_VCR_TABLE         vcr_list[MAX_VCR_REF];
    T_DEVICE_ATTRIBUTES dev_list;
    T_FBAPI_DEVICE_INFO       fbk_info;
    T_CURR_PARARM_INDEX	curr_param_index;
} T_APPL_ADMIN;

/* ------------------------------------------------------------------------------ */
/** following H1 data structures are byte aligned as they represent part of the
    protocol data unit
  */
#if defined (WIN32) || defined (_WIN32)

  #pragma warning (disable : 4103)     /* used #pragma pack to change alignment */
  #pragma pack (push, 1)
  #pragma warning (default : 4103)

  #ifndef PACK_LWORD_ALIGNMENT
    #define PACK_LWORD_ALIGNMENT(Struct)   Struct
  #endif
  #ifndef PACK_BYTE_ALIGNMENT
    #define PACK_BYTE_ALIGNMENT(Struct)   Struct
  #endif

#elif defined __GNUC__

  #ifndef PACK_LWORD_ALIGNMENT
    #define PACK_LWORD_ALIGNMENT(Struct)   __attribute__ ((packed, aligned (4))) Struct
  #endif
  #ifndef PACK_BYTE_ALIGNMENT
    #define PACK_BYTE_ALIGNMENT(Struct)   __attribute__ ((packed, aligned (1))) Struct
  #endif
#endif


/* ------------------------------------------------------------------------------ */
/** @brief H1 ID format description
 *  For H1 telegrams ASN.1 encoding is used. In ASN.1 encoding every component
 *  is described by a tag and the data length of the component that are preceding
 *  the component data. Depending on the values of tag and length the number of
 *  bytes necessary for the encoding of tag/length varies. The highest bit in the first
 *  byte (bit7) indicates if the component is a simple(bit=0) or complex (bit=1) structure
 *  that contains a list of additional components.
 *  The following cases exist for the encoding:
 *  Case 1: Tag 0-6, length 0 - 14: Encoding: length in bit0 - bit3, tag in bit4-bit6
 *          Example: Tag=2, length=5; Encoding: 0x25
 *  Case 2: Tag 7-255, length=0-14: Encoding: length in bit0-bit3, bit4-bit6=7, tag value is in second byte
 *          Example: Tag=8, length=5; Encoding: 0x75 0x08
 *  Case 3: Tag 0-6, length=15-255: Encoding: bit0-bit3=0xF, tag in bit4-bit6=7, length value in second byte
 *          Example: Tag=2, length=20; Encoding: 0x2F 0x14
 *  Case 4: Tag 7-255, length=15-255: Encoding: bit0-bit3=0xF, bit4-bit6=7, tag value is in
 *          second byte, length value in third byte
 *          Example: Tag=8, length=20; Encoding: 0x7F 0x08 0x14
 *
 *  The GetOD response encoding is described in ASN.1 specification as follows:
 *  GetOD-Response ::= SEQUENCE {
 *    list-of-objectdescription [0] IMPLICIT SEQUENCE OF {
 *      objectdescription [0] IMPLICIT Objectdescription
 *    }, ...
 *
 *  This means the tag value for a GetOD response is 0. So cases 1 and 3 can apply for the
 *  GetOD response. To parse the GetOD, the first byte has to be checked. If it is less then
 *  0x0F the object description length is the byte value. Otherwise the object description
 *  value is the value of the second byte.
 *  C-example: GetOD response contained in a buffer pointed to by USIGN8        *puchData;
 *   USIGN8         ucLength;
 *   USIGN16        object_index;
 *   USIGN8         ucObjectCode;
 *   USIGN8       * pucDescription;
 *
 *   for (USIGN8        i = 0; i < uchNumberOfObjectDescr; i++)
 *   {
 *      if (puchData[0] < 0x0Fu)
 *      {
 *         ucLength       = puchData[0];
 *         pucDescription = &puchData[1];
 *      }
 *      else if (puchData[0] == 0x0Fu)
 *      {
 *         ucLength       = puchData[1];
 *         pucDescription = &puchData[2];
 *      }
 *
 *      usObjectIndex = pucDescription[0]*0x100u + pucDescription[1];
 *      ucObjectCode  = pucDescription[2];
 *      puchData      = pucDescription + ucLength;
 *
 *   }
 *
 *   The value of the object code identifies the type of the description (see list of object codes)
 *   Exception is the OD description or OD header. This uses always index=0. Therefore the
 *   object code is not necessary to identify the OD description and is missing.
 *
 *   In the following are data types that show the data of the H1 object descriptions. As the H1 specification
 *   has the limit of using minimum data sizes the data structure are not 4-byte aligned as in HSE but are
 *   aligned to one byte.
 */

/** @brief H1 object dictionary description
 *         Hdr.Index = 0
 *
 */
typedef struct _T_H1_OD_DESCRIPTION
{
  USIGN16           ObjectIndex;                 /**< Object index := 0 */
  FB_BOOL           ROM_RAMFlag;                 /**< OD in ROM (write-protected) or RAM  */
  USIGN8            NameLength;                  /**< length of object names */
  FB_BOOL           AccessProtectionSupported;   /**< access protection */
  FB_INT16          VersionOD;                   /**< version number of the OD */
  USIGN16           ST_OD_Length;                /**< length of the data type description part */
  USIGN16           FirstIndex_S_OD;             /**< first index of the #static OD, contains variables */
  USIGN16           S_OD_Length;                 /**< length of the #static OD */
  USIGN16           FirstIndex_DV_OD;            /**< first index of the dynamic variable OD, contains variable list */
  USIGN16           DV_OD_Length;                /**< length of the dynamic variable OD */
  USIGN16           FirstIndex_DP_OD;            /**< first index of the dynamic PI OD */
  USIGN16           DP_OD_Length;                /**< first index of the dynamic PI OD */

} PACK_BYTE_ALIGNMENT (T_H1_OD_DESCRIPTION);

typedef struct _T_H1_OD_DESCRIPTION_LONG_FORMAT
{
  USIGN16           ObjectIndex;                 /**< Object index := 0 */
  FB_BOOL           ROM_RAMFlag;                 /**< OD in ROM (write-protected) or RAM  */
  USIGN8            NameLength;                  /**< length of object names */
  FB_BOOL           AccessProtectionSupported;   /**< access protection */
  FB_INT16          VersionOD;                   /**< version number of the OD */
  USIGN32           LocalAddressOD_ODES;         /**< address of the OD object description */
  USIGN16           ST_OD_Length;                /**< length of the data type description part */
  USIGN32           LocalAddressST_OD;           /**< address of the #static OD */
  USIGN16           FirstIndex_S_OD;             /**< first index of the #static OD, contains variables */
  USIGN16           S_OD_Length;                 /**< length of the #static type OD */
  USIGN32           LocalAddressS_OD;            /**< address of the #static OD */
  USIGN16           FirstIndex_DV_OD;            /**< first index of the dynamic variable OD, contains variable list */
  USIGN16           DV_OD_Length;                /**< length of the dynamic variable OD */
  USIGN32           LocalAddressDV_OD;           /**< address of the dynamic variable OD */
  USIGN16           FirstIndex_DP_OD;            /**< first index of the dynamic PI OD */
  USIGN16           DP_OD_Length;                /**< length of the dynamic PI OD */
  USIGN32           LocalAddressDP_OD;           /**< address of the dynamic PI OD */

} PACK_BYTE_ALIGNMENT (T_H1_OD_DESCRIPTION_LONG_FORMAT);


/** @brief Description of an H1 domain object
 *         Hdr.ObjectCode = 2
 *
 */
typedef struct _T_H1_DOMAIN_DESCRIPTION
{
  USIGN16           ObjectIndex;                 /**< Object index := 0 */
  USIGN8            ObjectCode;                  /**< Object code := 2 */
  USIGN16           MaxOctets;                   /**< maximum size of the domain */
  USIGN8            DomainState;                 /**< state of the domain */
  USIGN8            UploadState;                 /**< upload state of the domain */
  char              Counter;                     /**< number of PIs using the domain */

} PACK_BYTE_ALIGNMENT (T_H1_DOMAIN_DESCRIPTION);


typedef struct _T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT
{
  USIGN16           ObjectIndex;                 /**< Object index := 0 */
  USIGN8            ObjectCode;                  /**< Object code := 2 */
  USIGN16           MaxOctets;                   /**< maximum size of the domain */
  USIGN8            Password;                    /**< password, unused for FF */
  USIGN8            AccessGroup;                 /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                /**< definition of access rights for object, unused in FF */
  USIGN32           LocalAddres;                 /**< local address of the object description */
  USIGN8            DomainState;                 /**< state of the domain */
  USIGN8            UploadState;                 /**< upload state of the domain */
  char              Counter;                     /**< number of PIs using the domain */
  char				*Name;						 /**< object name if name length is set, usually unused in FF */
  USIGN8			ExtensionLength;              /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension;			   	     /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_DOMAIN_DESCRIPTION_LONG_FORMAT);

/** @brief Description of an H1 program invocation object
 *         Hdr.ObjectCode = 3
 *
 */
typedef struct _T_H1_PI_DESCRIPTION
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 3 */
  USIGN8            NumberOfDomains;
  FB_BOOL           Deletable;
  FB_BOOL           Reusable;
  USIGN8            PIState;
  /*    USIGN16         DomainIndex[Number Of Domains] List of Domain Indexes */

} PACK_BYTE_ALIGNMENT (T_H1_PI_DESCRIPTION);

typedef struct _T_H1_PI_DESCRIPTION_LONG_FORMAT
{

  USIGN16           ObjectIndex;                  /**< Object index */
  USIGN8            ObjectCode;                   /**< Object code := 3 */
  USIGN8            NumberOfDomains;
  USIGN8            Password;                     /**< password, unused for FF */
  USIGN8            AccessGroup;                  /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                 /**< definition of access rights for object, unused in FF */
  FB_BOOL           Deletable;
  FB_BOOL           Reusable;
  USIGN8            PIState;
  USIGN16           *DomainIndex;				  /**< List of Domain Indexes */
  char				*Name;		                  /**< object name if name length is set, usually unused in FF */
  USIGN8		    ExtensionLength;              /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension; 				  /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */
  /*    USIGN16         DomainIndex[Number Of Domains] */ /**< List of Domain Indexes */
  /* char Name[OD.NameLength] */                 /**< object name if name length is set, usually unused in FF */
  /* USIGN8        ExtensionLength */            /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  /* USIGN8        Extension[ExtensionLength */  /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_PI_DESCRIPTION_LONG_FORMAT);


/** @brief Description of an H1 event object
 *         Hdr.ObjectCode = 4
 *
 */
typedef struct _T_H1_EVENT_OBJECT
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 4 */
  USIGN16           IndexEventData;              /**< Index of the event data object */
  USIGN32           Length;
  FB_BOOL           Enabled;

} PACK_BYTE_ALIGNMENT (T_H1_EVENT_OBJECT);

typedef struct _T_H1_EVENT_DESCRIPTION_LONG_FORMAT
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 4 */
  USIGN16           IndexEventData;              /**< Index of the event data object */
  USIGN32           Length;
  USIGN8            Password;                    /**< password, unused for FF */
  USIGN8            AccessGroup;                 /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                /**< definition of access rights for object, unused in FF */
  FB_BOOL           Enabled;
  char				*Name;	                     /**< object name if name length is set, usually unused in FF */
  USIGN8		    ExtensionLength;             /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension;				     /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */
  /* char Name[OD.NameLength] */                 /**< object name if name length is set, usually unused in FF */
  /* USIGN8        ExtensionLength */            /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  /* USIGN8        Extension[ExtensionLength */  /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_EVENT_DESCRIPTION_LONG_FORMAT);

/** @brief H1 data type description
 *         Hdr.ObjectCode = 5
 *
 */
typedef struct _T_H1_OD_DATA_TYPE_DESCRIPTION
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 5*/
  USIGN8            SymbolicNameLength;          /**< length of the symbolic data type name */

  /* USIGN8        SymbolicName[SymbolicNameLength] */ /**< data type symbolic name */

} PACK_BYTE_ALIGNMENT (T_H1_OD_DATA_TYPE_DESCRIPTION);

/** @brief HSE data type structure description
 *         Hdr.ObjectCode = 6
 *
 */
typedef struct _T_H1_DATA_TYPE_ELEMENT
{
  USIGN16           DataTypeIndex;              /**< index of the data type of the element */
  USIGN8            Length;                     /**< length of the element */

} PACK_BYTE_ALIGNMENT (T_H1_DATA_TYPE_ELEMENT);

//typedef struct _T_H1_OD_DATA_TYPE_STRUCTURE
//{
//  USIGN16           ObjectIndex;                 /**< Object index */
//  USIGN8            ObjectCode;                  /**< Object code := 6 */
//  USIGN8            NumberOfElements;            /**< number of elements in the data type structure */
//  T_H1_DATA_TYPE_ELEMENT list[50];   /**< list of the data type elements */
//
//} PACK_BYTE_ALIGNMENT (T_H1_OD_DATA_TYPE_STRUCTURE);

typedef struct _T_H1_OD_DATA_TYPE_STRUCTURE
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 6 */
  USIGN8            NumberOfElements;            /**< number of elements in the data type structure */
  T_H1_DATA_TYPE_ELEMENT *list;   /**< list of the data type elements */

} PACK_BYTE_ALIGNMENT (T_H1_OD_DATA_TYPE_STRUCTURE);



/** @brief description of an H1 simple variable
 *         Hdr.ObjectCode = 27
 *
 */
typedef struct _T_H1_SIMPLE_VAR_OBJECT
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 7 */
  USIGN16           DataTypeIndex;               /**< index of the data type of the variable */
  USIGN8            Length;                      /**< length if the variable */

} PACK_BYTE_ALIGNMENT (T_H1_SIMPLE_VAR_OBJECT);

typedef struct _T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT
{

  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 7 */
  USIGN16           DataTypeIndex;               /**< index of the data type of the variable */
  USIGN8            Length;                      /**< length if the variable */
  USIGN8            Password;                    /**< password, unused for FF */
  USIGN8            AccessGroup;                 /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                /**< definition of access rights for object, unused in FF */
  USIGN32           LocalAddres;                 /**< local address of the object */
  char				*Name;		                 /**< object name if name length is set, usually unused in FF */
  USIGN8			ExtensionLength;             /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension;				     /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_SIMPLE_VAR_DESCRIPTION_LONG_FORMAT);

/** @brief description of an HSE array object
 *         Hdr.ObjectCode = 8
 *
 */
typedef struct _T_H1_ARRAY_OBJECT
{
  USIGN16           ObjectIndex;                 /**< Object index  */
  USIGN8            ObjectCode;                  /**< Object code := 8 */
  USIGN16           DataTypeIndex;               /**< index of the data type of an array element */
  USIGN8            Length;                      /**< length of an array element */
  USIGN8            NumberOfElements;            /**< number of array elements */

} PACK_BYTE_ALIGNMENT (T_H1_ARRAY_OBJECT);

typedef struct _T_H1_ARRAY_DESCRIPTION_LONG_FORMAT
{

  USIGN16           ObjectIndex;                  /**< Object index */
  USIGN8            ObjectCode;                   /**< Object code := 8 */
  USIGN16           DataTypeIndex;                /**< index of the data type of an array element */
  USIGN8            Length;                       /**< length of an array element */
  USIGN8            NumberOfElements;             /**< number of array elements */
  USIGN8            Password;                     /**< password, unused for FF */
  USIGN8            AccessGroup;                  /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                 /**< definition of access rights for object, unused in FF */
  USIGN32           LocalAddres;                  /**< local address of the object */
  char				*Name;			              /**< object name if name length is set, usually unused in FF */
  USIGN8			ExtensionLength;              /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension;				      /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_ARRAY_DESCRIPTION_LONG_FORMAT);


/** @brief description of an HSE record object
 *         Hdr.ObjectCode = 9
 *
 */
typedef struct _T_H1_RECORD_OBJECT
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 9 */
  USIGN16           DataTypeIndex;               /**< index of the data type structure object describing the record */

} PACK_BYTE_ALIGNMENT (T_H1_RECORD_OBJECT);

typedef struct _T_H1_RECORD_LONG_FORMAT
{

  USIGN16           ObjectIndex;                /**< Object index */
  USIGN8            ObjectCode;                 /**< Object code := 9 */
  USIGN16           DataTypeIndex;              /**< index of the data type structure object describing the record */
  USIGN8            Password;                   /**< password, unused for FF */
  USIGN8            AccessGroup;                /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;               /**< definition of access rights for object, unused in FF */
  char				*Name;                       /**< local address of the object */
  USIGN8	        ExtensionLength;            /**< object name if name length is set, usually unused in FF */
  USIGN8			*Extension;                  /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN32		    *LocalAddres; 		        /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_RECORD_DESCRIPTION_LONG_FORMAT);

/** @brief description of an HSE variable list object
 *         Hdr.ObjectCode = 26
 *
 */
typedef struct _T_H1_VARIABLE_LIST
{
  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 9 */
  USIGN8            NumberOfElements;            /**< number of variable list elements */
  FB_BOOL           Deletable;                   /**< TRUE: variable list can be delete via FMS service */

  /* USIGN16       ElementIndex[NumberOfElements]     ListOfElementIndexes */

} PACK_BYTE_ALIGNMENT (T_H1_VARIABLE_LIST);

typedef struct _T_H1_VARIABLE_LIST_LONG_FORMAT
{

  USIGN16           ObjectIndex;                 /**< Object index */
  USIGN8            ObjectCode;                  /**< Object code := 9 */
  USIGN8            NumberOfElements;            /**< number of variable list elements */
  USIGN8            Password;                    /**< password, unused for FF */
  USIGN8            AccessGroup;                 /**< definition of access group for object, unused in FF */
  USIGN16           AccessRights;                /**< definition of access rights for object, unused in FF */
  FB_BOOL           Deletable;                   /**< TRUE: variable list can be delete via FMS service */
  USIGN16			*ElementIndex;				 /**< list of object indices */
  char				*Name;		                 /**< object name if name length is set, usually unused in FF */
  USIGN8			ExtensionLength;             /**< length of the extension, 9 for FBAP VFD objects, 0 for MIB VFD objects */
  USIGN8			*Extension;				     /**< FBAP objects: usage (1 byte), DD member id (4 byte), DD item id (4 byte) */

} PACK_BYTE_ALIGNMENT (T_H1_VARIABLE_LIST_DESCRIPTION_LONG_FORMAT);


/* -------------------------------------------------------------------- */

/** @brief description of the H1 analog alert, data type structure 75
 *
 */
typedef struct _T_H1_ALERT_FLOAT
{
  USIGN16             block_index;          /**< Index of the block triggering the event */
  USIGN8              alert_key;            /**< Value of alert key */
  USIGN8              standard_type;        /**< Standard type of the alert */
  USIGN8              mfgr_type;            /**< alert identification using a manufacturer specific DeviceDescription */
  USIGN8              message_type;         /**< identifies the reason of the notification */
  USIGN8              priority;             /**< event priority */
  unsigned long long  time_stamp;           /**< current System Management time when event was created */
  USIGN16             subcode;              /**< subcode of the alarm/event parameter */
  float               value;                /**< value causing alarm */
  USIGN16             relative_index;       /**< relative index of the alert object within the block */
  USIGN16             unit_index;           /**< index for the unit word */

} PACK_BYTE_ALIGNMENT (T_H1_ALERT_FLOAT);

/** @brief description of the H1 discrete alert, data type structure 76
 *
 */
typedef struct T_H1_ALERT_DISCRETE
{
  USIGN16             block_index;          /**< Index of the block triggering the event */
  USIGN8              alert_key;            /**< Value of alert key */
  USIGN8              standard_type;        /**< Standard type of the alert */
  USIGN8              mfgr_type;            /**< alert identification using a manufacturer specific DeviceDescription */
  USIGN8              message_type;         /**< identifies the reason of the notification */
  USIGN8              priority;             /**< event priority */
  unsigned long long  time_stamp;           /**< current System Management time when event was created */
  USIGN16             subcode;              /**< subcode of the alarm/event parameter */
  USIGN8              value;                /**< value causing alarm */
  USIGN16             relative_index;       /**< relative index of the alert object within the block */
  USIGN16             unit_index;           /**< index for the unit word */

} PACK_BYTE_ALIGNMENT (T_H1_ALERT_DISCRETE);

/** @brief description of the H1 update alert, data type structure 77
 *
 */
typedef struct _T_H1_ALERT_EVENT
{
  USIGN16             block_index;          /**< Index of the block triggering the event */
  USIGN8              alert_key;            /**< Value of alert key */
  USIGN8              standard_type;        /**< Standard type of the alert */
  USIGN8              mfgr_type;            /**< alert identification using a manufacturer specific DeviceDescription */
  USIGN8              message_type;         /**< identifies the reason of the notification */
  USIGN8              priority;             /**< event priority */
  unsigned long long  time_stamp;           /**< current System Management time when event was created */
  USIGN16             static_revision;      /**< value of #static revision after the update */
  USIGN16             relative_index;       /**< relative index of the changed #static parameter */

} PACK_BYTE_ALIGNMENT (T_H1_ALERT_EVENT);

/** @brief description of the H1 field diagnostic alert, data type structure 88
 *
 */
typedef  struct _T_H1_ALERT_FD_DIAG
{
  USIGN16             block_index;           /**< Index of the block triggering the event */
  USIGN8              alert_key;             /**< Value of alert key */
  USIGN8              standard_type;         /**< Standard type of the alert */
  USIGN8              mfgr_type;             /**< alert identification using a manufacturer specific DeviceDescription */
  USIGN8              message_type;          /**< identifies the reason of the notification */
  USIGN8              priority;              /**< event priority */
  unsigned long long  time_stamp;            /**< current System Management time when event was created */
  USIGN32             subcode;               /**< subcode of the alarm/event parameter */
  USIGN8              value;                 /**< value causing alarm */
  USIGN16             relative_index;        /**< relative index of the alert object within the block */
  USIGN16             unit_index;            /**< index for the unit word */

} PACK_BYTE_ALIGNMENT (T_H1_ALERT_FD_DIAG);

/** @brief list of standard data types
 * 0 = Undefined
 * 1 = LO - Low limit
 * 2 = HI - High limit
 * 3 = LO LO - Critical low limit
 * 4 = HI HI - Critical high limit
 * 5 = DV LO - Deviation low
 * 6 = DV HI - Deviation high
 * 7 = DISC - Discrete
 * 8 = BLOCK - Block Alarm
 * 9 = UPDATE - Static data update
 * 10 = WRITE - Write protect changed.
 * 11 = UPDATE - Link associated with function block
 * 12 = UPDATE - Trend associated with block
 * 13 = IGNORE - One or more bits in the IGNORE bit string has changed state
 * 14 = RESET - Integrator has been reset and report option enabled
 * 15 = Fail Alarm
 * 16 = Off Spec Alarm
 * 17 = Maintenance Alarm
 * 18 = Check Alarm
 * 19-64 = Reserved by FF for future use
 */

/** @brief list of message types
 * 0 = Undefined
 * 1 = Event Notification
 * 2 = Alarm Clear
 * 3 = Alarm Occur
 * 4 = Bit Clear - Bit Clear is sent to report that a bit-alarm has cleared.
 * 5 = Bit Occur - Bit Occur is sent to report that a bit-alarm has been detected.
 */

typedef struct _T_DEVICE_ERROR {
    int reason_code;
    int host_state;
    int service;
    int layer;
    int primitive;
    int result;
}T_DEVICE_ERROR;


/* DLL configuration parameters */
typedef struct {
    USIGN16         this_link;
    USIGN16         slot_time;
    USIGN8          phlo;
    USIGN8          mrd;
    USIGN8          mipd;
    USIGN8          tsc;
    USIGN8          phis_skews;
    USIGN8          phpe;
    USIGN8          phge;
    USIGN8          fun;
    USIGN8          nun;
}T_DLL_CONFIG_REQ;

typedef struct _FBAP_NMA_REQ
{
    USIGN16             cr_index;
    USIGN32             index;
    USIGN16             sub_index;
    USIGN8              value[64];
    USIGN16             size;
    USIGN16             loc_add;
    USIGN16             rem_add;
    USIGN32         	vfd_ref;
    T_NMA_INFO          nma_info;
    T_DLL_CONFIG_REQ    dll_config;
    UINT8               nma_wr_flag;;
}FBAP_NMA_REQ;

typedef struct _SM_REQ
{
    unsigned short dev_addr;
    unsigned char clear;
    char pd_tag[34];
    char dev_id[34];
    char vendor_name[64];
    char model_name[64];
    char revision_string[64];
    char host_pd_tag[64];
}SM_REQ;


/*
 * Structure used to write into the FFH1 modem
 */
typedef struct comm_req {
    unsigned int condition;
    FBAP_NMA_REQ fbap_nma_req;
    SM_REQ sm_req;
}COMM_REQ;

/****************************************************************************/
/* local function declarations from source module                           */
/****************************************************************************/
int handle_sm_config_req(SM_REQ *);
int handle_sm_config_con(void);
#ifdef ALL_FF_SERVICES_SUPPORTED
int handle_config_req(void);
#endif

extern int handle_read_req(USIGN16 cr, USIGN16 index);
int handle_read_con(void);
void devacc_end(void);
int handle_initiate_req(USIGN16 cr);
int handle_initiate_con(void);
int handle_read_response(int);
int handle_vcrl_init_req(void);
int handle_vcrl_init_con(void);
int handle_dll_config_req(FBAP_NMA_REQ *);
int handle_dll_config_con(T_NMA_INFO *);
int handle_load_vcr_local_write_req(FBAP_NMA_REQ *);
int handle_vcrl_load_req(void);
int handle_vcrl_load_con(void);
int handle_set_addr_req(SM_REQ *);
int handle_clear_addr_req(SM_REQ *);
int handle_set_pd_tag_req(SM_REQ *);
int handle_nma_read_req(USIGN16 );
int handle_nma_write_req(USIGN16, USIGN8, USIGN8, USIGN8 *);
int handle_mgmt_write_req(FBAP_NMA_REQ *);
int handle_vcrl_term_req(void);
int handle_vcrl_term_con(void);
int handle_mgmt_read_req(FBAP_NMA_REQ *);
int handle_identify_req(USIGN8 node);
int handle_identify_con(void);
int handle_get_od_req(FBAP_NMA_REQ *, USIGN16 );
int handle_get_od_con(void);
int handle_abort_req(USIGN16 );
int handle_vcr_write_req(FBAP_NMA_REQ * );
int handle_nma_reset_req(void);
int handle_nma_reset_con(void);
void handle_print_local_mib_directory (void);
int handle_write_con (void);
int handle_mgmt_initiate_ind(USIGN8 );
int handle_mgmt_abort_ind(void);
int handle_mgmt_read_con(USIGN8 );
int handle_mgmt_read_ind(USIGN8 );
int handle_mgmt_write_con(USIGN8 );
int handle_mgmt_write_ind(USIGN8 );
int handle_event_ind (void);
int handle_ack_event_req(USIGN16, USIGN16, USIGN8);
int handle_ack_event_con(void);
int handle_reject_ind(void);
int handle_set_pd_tag_con(void);
int handle_set_addr_con(void);
int handle_find_tag_query_req(USIGN8, FB_BOOL);
int handle_nma_data_req(void);
int handle_nma_data_query_con(void);

void print_service_err(void);
void print_sm_err(VOID);
void log_interface_error(USIGN8 primitive, FB_INT16 error);
int save_od_header(USIGN16, USIGN8 *);

extern void devadm_init(USIGN8 node_address, int alive);
extern void devadm_identify_con(USIGN8 , T_SM_IDENTIFY_CNF *);
int devadm_save_od_header_info(USIGN16 , USIGN8 , USIGN8 ,
                                   T_PACKED_OBJECT_DESCR *, USIGN16 *);
extern void devadm_read_con( T_VAR_READ_CNF *);

int device_admin_read_sm_dir(T_VAR_READ_CNF *, USIGN16 *, USIGN16);
int device_admin_read_nm_dir(T_VAR_READ_CNF *, USIGN16 *, USIGN16);
int device_admin_read_vfd_ref(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_vcr_char(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_scan_vcr_list(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_wr_vcr_entry(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_appl_dir(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_tb(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_fb(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_rb(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_manufac(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_dev_type(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_dev_rev(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_dd_rev(T_VAR_READ_CNF *, USIGN16 *);
int device_admin_read_cycle(T_VAR_READ_CNF *, USIGN16 *);

void set_device_error(int state, int code);
void set_device_return_error(int state, int code);


/*
 * Functions to store and access the global data structure, which maintains
 * the informations of the remote Device information and host device
 * information.
 */
USIGN16 get_index_to_scan(void);

void set_service_comm_ref(USIGN16 );
void set_service_layer(USIGN8 );
void set_service_ref(USIGN8 );
void set_service_primitive(USIGN8 );
void set_service_result(FB_INT16 );
void set_service_invoke_id(FB_INT8 );
void set_device_admin_state(int );
void set_device_admin_invokeid(FB_INT8 );
void set_device_admin_proc_con(void *proc_mgmt_con);
void set_device_admin_local_nma_read(USIGN16 );
void set_device_admin_channel(int );
void set_device_admin_local_nma_write(USIGN16 );
void set_device_admin_vfd_nr(USIGN16 , USIGN16 );
void set_device_admin_device_addr(USIGN16 , USIGN16 );
void set_device_admin_type_role(USIGN8 type_role, USIGN16 cr_index);
void set_device_admin_conn(FB_BOOL conn);
void set_device_admin_host_addr(USIGN16 device_addr);
void set_device_admin_aux(USIGN8 val);
void set_admin_dev_state(T_DEVACC_STATE device_state);
void set_device_admin_host_type(int);
void set_device_admin_msg_q(mqd_t);
void set_device_admin_appl_state(int);

int get_size_of_desc_header(void);
FB_INT16 get_service_result(void);
FB_INT16 get_service_primitive(void);
USIGN16 get_service_comm_ref(void);
USIGN8 get_service_layer(void);
USIGN8 get_service_ref(void);
FB_INT8 get_service_invoke_id(void);
T_FIELDBUS_SERVICE_DESCR *get_desc_header(void);
void set_desc_header(T_FIELDBUS_SERVICE_DESCR *);
int get_device_admin_state(void);
FB_INT8 get_device_admin_invokeid(void);
void clear_device_admin_live_list(void);
USIGN8 get_device_admin_vfd_nr(USIGN16 );
USIGN8 get_device_admin_host_addr(void);
USIGN8 get_device_admin_device_addr(USIGN16 );
USIGN32 get_device_admin_remote_vfd(void);
USIGN32 get_device_admin_manufacturer(void);
USIGN16 get_device_admin_device_type(void);
USIGN8 get_device_admin_device_rev(void);
USIGN8 get_device_admin_dd_rev(void);
USIGN16 get_device_admin_tb_index(int count);
USIGN16 get_device_admin_rb_index(void);
USIGN16 get_device_admin_max_entries(void);
USIGN16 get_device_admin_tb_entries(void);
USIGN16 get_device_admin_fb_entries(void);
USIGN16 get_device_admin_fb_index(int count);
T_DEVACC_STATE get_admin_dev_state(void);
USIGN8 get_admin_dev_type_role(USIGN16 );
USIGN16 get_admin_dev_nm_first_index(void);
USIGN8 get_device_admin_aux(void);
USIGN16 get_device_admin_local_nma_read(void);
USIGN16 get_device_admin_local_nma_write(void);
PROCESS_CON_FNC device_admin_proc_con(void);
USIGN16 get_admin_dev_loc_addr(USIGN16 cr_index);
USIGN16 get_admin_dev_rem_addr(USIGN16 cr_index);
USIGN8 get_admin_dev_sdap(USIGN16 cr_index);
void get_device_error(T_DEVICE_ERROR *);
void get_device_return_error(T_DEVICE_ERROR *);
int get_device_admin_appl_state(void);

int get_device_admin_channel(void);
unsigned char *get_admin_live_list(void);

int device_send_req_res(unsigned char *send_data);
void check_device_list(USIGN8 , SCAN_DEV_TBL *, int );
void save_admin_live_list(unsigned char , int );

void save_device_admin_stack_mib_dir(T_NMA_INFO *);
void get_device_admin_stack_mib_dir(T_NMA_INFO *);

void set_fbk_info(T_FBAPI_DEVICE_INFO *);
void get_fbk_info(T_FBAPI_DEVICE_INFO *);

void set_device_admin_manufac(int);
void set_device_admin_dev_type(int);
void set_device_admin_dd_rev(int);
void set_device_admin_dev_rev(int);

int get_device_admin_socket(void);
void set_device_admin_socket(int);

#if 0
void get_device_admin_data(T_APPL_ADMIN *appl_admin);
void set_device_admin_data(T_APPL_ADMIN *appl_admin);
#endif

int get_device_admin_host_type(void);

void  save_fbap_current_index(OBJECT_INDEX , SUBINDEX, USIGN16 cr);
void  get_fbap_current_index(OBJECT_INDEX *, SUBINDEX *, USIGN16 *);

//get LAS information
extern unsigned int (*setLASNodeAddress) P((unsigned int));

/* To notify connection manager about device disconnection manually. */
extern int (*connectdisconnNotification) P((unsigned char));
extern int (*getConnectionState) P(());

#endif /*def __DEV_ACC_H__*/
