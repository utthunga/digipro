/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  @(#)int_inc.h	30.3  30  14 Nov 1996
 *
 *  Description : Include file for the method interpreter include files, that
 *                are used frequently, but are changed infrequently  
 *
 ******************************************************************************/

/******************************************************************************
 *
 * Standard C header files
 *
 ******************************************************************************/
#include	<stdio.h>
#include 	<stdlib.h>
#include	<malloc.h>
#include	<string.h>
#include	<ctype.h>
#include	<assert.h>
#include    <stddef.h>
#include	<limits.h>

#if defined(__linux__)
#define STDARGS
#endif


#ifdef STDARGS
#  ifndef STDARG_H
#	 include <stdarg.h>
#    define   STDARG_H	1
#  endif
#else
#	include	<varargs.h>
#endif


/******************************************************************************
 *
 * Operating System dependent header files
 *
 ******************************************************************************/

#if defined MSDOS || defined __MSDOS__ || defined _WINDOWS			/* DOS and Windows System     */

#	include 	<string.h>						/* DOS and Windows System     */

#	if defined _WINDOWS							/* Windows System             */

#		if defined WIN32 || defined __WIN32__	/* Windows 32                 */

#			include <wchar.h>					/* Windows 32                 */
#			include <windowsx.h>				/* Windows Memory             */
#			include <setjmp.h>					/* Windows Memory             */

#			if defined __cplusplus 				/* For C++ compilation only   */
#				include <afx.h>    				/* MFC core and standard      */
#				include <afxwin.h> 				/* MFC core and standard      */
#				include <afxext.h> 				/* MFC core and standard      */
#				include <afxole.h> 				/* MFC core and standard      */
#				include <windef.h>				/* Windows 32                 */
#				include <winbase.h>				/* Windows 32                 */
#			endif /* __cplusplus */

#		else									/* Windows 16                 */

#			if defined __STDC__
#			undef __STDC__                  
#				include <afx.h>        			/* MFC core and standard      */
#				include <afxwin.h>     			/* MFC core and standard      */
#				include <afxext.h>     			/* MFC extensions             */
#				include <afxole.h>     			/* MFC OLE classes            */
#				include <windowsx.h>			/* Windows Memory             */
#				define __STDC__
#			endif
#			include "int_slp.h"	                /* Windows 16                 */
#			include "jmpcpp.h"	                /* Windows 16                 */
#			include "wprint.h"	                /* Windows 16                 */

#		endif /* WIN32 */

#	else										/* DOS System                 */

#		include		<dos.h>						/* DOS System                 */

#	endif /* WINDOWS */

#else											/* UNIX System                */

#	include 	<memory.h>						/* UNIX System                */

#endif /* MSDOS */

#if defined(SUN)
# include		<unistd.h>						/* UNIX System                */
#endif /* SUN */

/******************************************************************************
 *
 * Interpreter header files
 *
 ******************************************************************************/
#include "app_xmal.h"
#include "attrs.h"
#include "bi_codes.h"
#include "cm_lib.h"
#include "cm_loc.h"
#include "cm_rod.h"
#include "comm.h"
#include "ddi_lib.h"
#include "ddi_tab.h"
#include "ddldefs.h"
#include "dict.h"
#include "env_info.h"
#include "err_gen.h"
#include "evl_lib.h"
#include "evl_loc.h"
#include "evl_ret.h"
#include "fch_lib.h"
#include "flats.h"
#include "int_lib.h"
#include "int_loc.h"
#include "int_meth.h"
#include "dds_lock.h"
#include "ddsbltin.h"
#include "od_defs.h"
#include "options.h"
#include "pc_lib.h"
#include "pc_loc.h"
#include "rmalloc.h"
#include "rtn_code.h"
#include "std.h"
#include "sysparam.h"
#include "sysproto.h"
#include "table.h"
#include "tags_sa.h"


/******************************************************************************
 *
 * Interpreter header file for platform specific functions and definitions.
 *
 ******************************************************************************/

#include "int_diff.h"

/******************************************************************************/
