/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *
 *	@(#)flats.h	30.3  30  14 Nov 1996
 */

#ifndef FLATS_H
#define FLATS_H

#include "attrs.h"


/*
 *	FLAT structures
 */

typedef struct {
	ulong           bin_exists;		/* There is binary for this attribute */
	ulong           bin_hooked;		/* Binary is attached for this attribute */
	ulong           attr_avail;		/* The attribute has been evaluated	 */
	ulong           dynamic;		/* The attribute is dynamic */
}               FLAT_MASKS;


/*
 * VARIABLE item
 */

typedef struct {
	DEPBIN         *db_pre_edit_act;
	DEPBIN         *db_post_edit_act;
	DEPBIN         *db_pre_read_act;
	DEPBIN         *db_post_read_act;
	DEPBIN         *db_pre_write_act;
	DEPBIN         *db_post_write_act;
	DEPBIN         *db_refresh_act;
}               VAR_ACTIONS_DEPBIN;

typedef struct {
	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;
	ITEM_ID_LIST    pre_read_act;
	ITEM_ID_LIST    post_read_act;
	ITEM_ID_LIST    pre_write_act;
	ITEM_ID_LIST    post_write_act;
    ITEM_ID_LIST    refresh_act;
	VAR_ACTIONS_DEPBIN *depbin;
}               FLAT_VAR_ACTIONS;

typedef struct {
	DEPBIN         *db_unit;
	DEPBIN         *db_read_time_out;
	DEPBIN         *db_write_time_out;
	DEPBIN         *db_min_val;
	DEPBIN         *db_max_val;
	DEPBIN         *db_scale;
	DEPBIN         *db_valid;
    DEPBIN         *db_write_mode;
}               VAR_MISC_DEPBIN;

typedef struct {
	STRING          unit;			/**/
	unsigned long   read_time_out;	/**/
	unsigned long   write_time_out;	/**/
	RANGE_DATA_LIST min_val;		/**/
	RANGE_DATA_LIST max_val;		/**/
	EXPR            scale;			/* scale factor */
	ulong           valid;			/* validity */
	VAR_MISC_DEPBIN *depbin;
	unsigned long   write_mode;	/**/
}               FLAT_VAR_MISC;

typedef struct {
	DEPBIN         *db_class;
	DEPBIN         *db_handling;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_type_size;
	DEPBIN         *db_display;
	DEPBIN         *db_edit;
	DEPBIN         *db_enums;
	DEPBIN         *db_index_item_array;
	DEPBIN         *db_resp_codes;
}               VAR_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           class_attr;
	ulong           handling;
	STRING          help;
	STRING          label;
	TYPE_SIZE       type_size;
	STRING          display;
	STRING          edit;
	ENUM_VALUE_LIST enums;
	ITEM_ID         index_item_array;
	ITEM_ID         resp_codes;
	VAR_DEPBIN     *depbin;
	FLAT_VAR_ACTIONS *actions;
	FLAT_VAR_MISC  *misc;
}               FLAT_VAR;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_min_value;
    DEPBIN      *db_max_value;
    DEPBIN      *db_scaling;
    DEPBIN      *db_constant_unit;
}               AXIS_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
    EXPR            min_value;
    EXPR            max_value;
    DDS_AXIS_SCALE  scaling;
	STRING          constant_unit;
    AXIS_DEPBIN     *depbin;
}               FLAT_AXIS;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_cycle_time;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_length;
    DEPBIN      *db_type;
    DEPBIN      *db_valid;
}               CHART_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    UINT32          height;
    UINT32          width;
    UINT32          cycle_time;
    UINT32          length;
    UINT32          type;
    ulong           valid;
    CHART_DEPBIN     *depbin;
}  FLAT_CHART;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
}               FILE_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    FILE_DEPBIN     *depbin;
}  FLAT_FILE;


typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_x_axis;
    DEPBIN      *db_valid;
	DEPBIN		*db_cycle_time;
}               GRAPH_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    UINT32          height;
    UINT32          width;
    ITEM_ID         x_axis;
    ulong           valid;
	UINT32			cycle_time;
    GRAPH_DEPBIN     *depbin;
}  FLAT_GRAPH;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_vectors;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_handling;
    DEPBIN      *db_valid;
    DEPBIN      *db_orientation;
}               GRID_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	VECTOR_LIST     vectors;
    UINT32          height;
    UINT32          width;
    UINT32          handling;
    UINT32          validity;
    UINT32          orientation;
    GRID_DEPBIN     *depbin;
}  FLAT_GRID;


typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_type;
    DEPBIN      *db_capacity;
    DEPBIN      *db_count;
    DEPBIN      *db_valid;
}               LIST_DEPBIN;
typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
    ITEM_ID         type;
    UINT32          capacity;
    UINT32          count;
    ulong           valid;
    LIST_DEPBIN     *depbin;
}  FLAT_LIST;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_emphasis;
    DEPBIN      *db_y_axis;
    DEPBIN      *db_line_type;
    DEPBIN      *db_line_color;
    DEPBIN      *db_init_actions;
    DEPBIN      *db_refresh_actions;
    DEPBIN      *db_exit_actions;
    DEPBIN      *db_valid;
}               SOURCE_DEPBIN;
typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	OP_MEMBER_LIST  members;
    UINT32          emphasis;
    ITEM_ID         y_axis;
    UINT32          line_type;
    UINT32          line_color;
	ITEM_ID_LIST    init_actions;
	ITEM_ID_LIST    refresh_actions;
    ITEM_ID_LIST    exit_actions;
    ulong           valid;
    SOURCE_DEPBIN     *depbin;
}  FLAT_SOURCE;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_emphasis;
    DEPBIN      *db_line_type;
    DEPBIN      *db_handling;
    DEPBIN      *db_init_actions;
    DEPBIN      *db_refresh_actions;
    DEPBIN      *db_exit_actions;
    DEPBIN      *db_type;
    DEPBIN      *db_x_initial;
    DEPBIN      *db_x_increment;
    DEPBIN      *db_number_of_points;
    DEPBIN      *db_x_values;
    DEPBIN      *db_y_values;
    DEPBIN      *db_key_x_values;
    DEPBIN      *db_key_y_values;
    DEPBIN      *db_y_axis;
    DEPBIN      *db_line_color;
    DEPBIN      *db_valid;
}               WAVEFORM_DEPBIN;
typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    UINT32          emphasis;
    UINT32          line_type;
    UINT32          handling;
	ITEM_ID_LIST    init_actions;
	ITEM_ID_LIST    refresh_actions;
    ITEM_ID_LIST    exit_actions;
    UINT32          type;
    EXPR            x_initial;
    EXPR            x_increment;
    UINT32          number_of_points;
   	DATA_ITEM_LIST  x_values;
    DATA_ITEM_LIST  y_values;
   	DATA_ITEM_LIST  key_x_values;
    DATA_ITEM_LIST  key_y_values;
    UINT32          line_color;
    ITEM_ID         y_axis;
    ulong           valid;
    WAVEFORM_DEPBIN     *depbin;
}  FLAT_WAVEFORM;


/*
 * PROGRAM item
 */

typedef struct {
	DEPBIN         *db_args;
	DEPBIN         *db_resp_codes;
}               PROGRAM_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	DATA_ITEM_LIST  args;
	ITEM_ID         resp_codes;
	PROGRAM_DEPBIN *depbin;
}               FLAT_PROGRAM;

/*
 * MENU item
 */

typedef struct {
	DEPBIN         *db_label;
	DEPBIN         *db_items;
	DEPBIN         *db_style;
	DEPBIN         *db_style_string;
    DEPBIN         *db_valid;
}               MENU_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	STRING          label;
    UINT32          style;
    STRING          style_string;
	MENU_ITEM_LIST  items;
    ulong           valid;
	MENU_DEPBIN    *depbin;
}               FLAT_MENU;

/*
 * Image
 */


typedef struct {
    DEPBIN          *db_help;
    DEPBIN          *db_label;
	DEPBIN          *db_path;
	DEPBIN          *db_link;
	DEPBIN          *db_valid;
}               IMAGE_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
    STRING          label;
    STRING          help;

    STRING          path;
    DESC_REF        link;
	ulong           valid;			/* validity */
    BINARY_IMAGE    entry;          /* This image */
    IMAGE_DEPBIN    *depbin;
}               FLAT_IMAGE;


/*
 * EDIT_DISPLAY item
 */

typedef struct {
	DEPBIN         *db_disp_items;
	DEPBIN         *db_edit_items;
	DEPBIN         *db_label;
	DEPBIN         *db_pre_edit_act;
	DEPBIN         *db_post_edit_act;
}               EDIT_DISPLAY_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	OP_REF_TRAIL_LIST disp_items;
	OP_REF_TRAIL_LIST edit_items;
	STRING          label;
	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;
	EDIT_DISPLAY_DEPBIN *depbin;
}               FLAT_EDIT_DISPLAY;


/*
 * METHOD item
 */

typedef struct {
	DEPBIN         *db_class;
	DEPBIN         *db_def;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_valid;
	DEPBIN         *db_scope;
}               METHOD_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           class_attr ;  
	DEFINITION      def;
	STRING          help;
	STRING          label;	 
	ulong           valid;
	ulong			scope;
	METHOD_DEPBIN  *depbin;
}               FLAT_METHOD;

/*
 * REFRESH item
 */

typedef struct {
	DEPBIN         *db_items;
}               REFRESH_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	REFRESH_RELATION items;
	REFRESH_DEPBIN *depbin;
}               FLAT_REFRESH;


/*
 * UNIT item
 */

typedef struct {
	DEPBIN         *db_items;
}               UNIT_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	UNIT_RELATION   items;
	UNIT_DEPBIN    *depbin;
}               FLAT_UNIT;


/*
 * WRITE AS ONE item
 */

typedef struct {
	DEPBIN         *db_items;
}               WAO_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	OP_REF_TRAIL_LIST items;
	WAO_DEPBIN     *depbin;
}               FLAT_WAO;


/*
 * ITEM_ARRAY item
 */

typedef struct {
	DEPBIN         *db_elements;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
    DEPBIN         *db_valid;
}               ITEM_ARRAY_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
	ITEM_ARRAY_ELEMENT_LIST elements;
	STRING          help;
	STRING          label;
    ulong           valid;
	ITEM_ARRAY_DEPBIN *depbin;
}               FLAT_ITEM_ARRAY;


/*
 * ARRAY item
 */

typedef struct {
	DEPBIN         *db_num_of_elements;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_type;
	DEPBIN         *db_resp_codes;
    DEPBIN         *db_valid;
    DEPBIN         *db_write_mode;
}               ARRAY_DEPBIN;


typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           num_of_elements;
	STRING          help;
	STRING          label;
	ITEM_ID         type;
	ITEM_ID         resp_codes;
    ulong           valid;
    ulong           write_mode;
	ARRAY_DEPBIN   *depbin;
}               FLAT_ARRAY;


/*
 * COLLECTION item
 */

typedef struct {
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
}               COLLECTION_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
	OP_MEMBER_LIST  members;
	STRING          help;
	STRING          label;
	COLLECTION_DEPBIN *depbin;
}               FLAT_COLLECTION;


/*
 * RECORD item
 */

typedef struct {
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_resp_codes;
    DEPBIN         *db_valid;
    DEPBIN         *db_write_mode;
}               RECORD_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
    ulong           valid;
	ulong           write_mode;
	RECORD_DEPBIN  *depbin;
}               FLAT_RECORD;


/*
 * VARIABLE	LIST item
 */

typedef struct {
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_resp_codes;
}               VAR_LIST_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
	VAR_LIST_DEPBIN *depbin;
}               FLAT_VAR_LIST;


/*
 * BLOCK item
 */

typedef struct {
	DEPBIN         *db_characteristic;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_param;
	DEPBIN         *db_param_list;
	DEPBIN         *db_item_array;
	DEPBIN         *db_collect;
	DEPBIN         *db_menu;
	DEPBIN         *db_edit_disp;
	DEPBIN         *db_method;
	DEPBIN         *db_unit;
	DEPBIN         *db_refresh;
	DEPBIN         *db_wao;
	DEPBIN         *db_axis;
	DEPBIN         *db_chart;
	DEPBIN         *db_file;
	DEPBIN         *db_graph;
	DEPBIN         *db_list;
	DEPBIN         *db_source;
	DEPBIN         *db_waveform;
    DEPBIN         *db_local_param;
    DEPBIN         *db_grid;
    DEPBIN         *db_image;
    DEPBIN         *db_chart_members;
    DEPBIN         *db_file_members;
    DEPBIN         *db_graph_members;
    DEPBIN         *db_grid_members;
    DEPBIN         *db_menu_members;
    DEPBIN         *db_method_members;
    DEPBIN         *db_list_members;
}               BLOCK_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_ID         characteristic;
	STRING          help;
	STRING          label;
	MEMBER_LIST     param;
	MEMBER_LIST     param_list;
	ITEM_ID_LIST    item_array;
	ITEM_ID_LIST    collect;
	ITEM_ID_LIST    menu;
	ITEM_ID_LIST    edit_disp;
	ITEM_ID_LIST    method;
	ITEM_ID_LIST    unit;
	ITEM_ID_LIST    refresh;
	ITEM_ID_LIST    wao;
	ITEM_ID_LIST    axis;
	ITEM_ID_LIST    chart;
	ITEM_ID_LIST    file;
	ITEM_ID_LIST    graph;
	ITEM_ID_LIST    list;
	ITEM_ID_LIST    source;
	ITEM_ID_LIST    waveform;
	ITEM_ID_LIST    grid;
	ITEM_ID_LIST    image;
	MEMBER_LIST     local_param;
    MEMBER_LIST     chart_members;
    MEMBER_LIST     file_members;
    MEMBER_LIST     graph_members;
    MEMBER_LIST     grid_members;
    MEMBER_LIST     menu_members;
    MEMBER_LIST     method_members;
    MEMBER_LIST     list_members;
	BLOCK_DEPBIN   *depbin;
}               FLAT_BLOCK;


/*
 * RESPONSE CODE item
 */

typedef struct {
	DEPBIN         *db_member;
}               RESP_CODE_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	RESPONSE_CODE_LIST member;
	RESP_CODE_DEPBIN *depbin;
}               FLAT_RESP_CODE;


/*
 * DOMAIN item
 */

typedef struct {
	DEPBIN         *db_handling;
	DEPBIN         *db_resp_codes;
}               DOMAIN_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           handling;
	ITEM_ID         resp_codes;
	DOMAIN_DEPBIN  *depbin;
}               FLAT_DOMAIN;


/*
 * COMMAND item
 */

typedef struct {
	DEPBIN         *db_number;
	DEPBIN         *db_oper;
	DEPBIN         *db_trans;
	DEPBIN         *db_resp_codes;
}               COMMAND_DEPBIN;

typedef struct {
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           number;
	ulong           oper;
	TRANSACTION_LIST trans;
	RESPONSE_CODE_LIST resp_codes;
	COMMAND_DEPBIN *depbin;
}               FLAT_COMMAND;

#endif /* FLATS_H */
