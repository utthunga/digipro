/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)devsim.h	30.3  30  14 Nov 1996
 *
 *	devsim.h - Device definitions
 */

#ifndef	MESSAGE_Q_H
#define	MESSAGE_Q_H

#define MESSAGE_Q_NAME "queue_from_socket"
#define MAX_ACK_BYTES	10

mqd_t do_open( const char *, int );


#endif
