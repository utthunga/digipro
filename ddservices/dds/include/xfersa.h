/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/* 
 *	@(#)xfersa.h	30.3  30  14 Nov 1996
 */ 

#ifndef HL_transfer_H
#define HL_transfer_H


#include "responsa.h"
#include "tx_hart.h"
#include "cm_lib.h"
#include "fakexmtr.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

    extern int mgt_xfer_init_transaction P(( NETWORK_HANDLE, DEVICE_HANDLE, DL_REQUEST *, 
		DL_RESPONSE *));

extern void xfer_build_hart_resp P(( unsigned char *, DL_RESPONSE *, int ));
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* HL_transfer_H */
