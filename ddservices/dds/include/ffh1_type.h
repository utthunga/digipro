/*  Filename    : ffh1_type.h                                                */
/*                                                                           */
/*  Description : This file "links" to fbkhost_type.h (FFusb basic types)    */
/*                                                                           */
/*****************************************************************************/

#ifndef __FFH1_TYPE__
#define __FFH1_TYPE__

#include "fbkhost_type.h"

#endif /* __FFH1_TYPE__ */
