/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)env_info.h	30.3  30  14 Nov 1996
 */
 
#ifndef ENV_INFO_H
#define ENV_INFO_H

/*
 * This file contains the definition for ENV_INFO.  It is used to pass
 * environment information throughout the application and libraries.  This
 * structure may be modified as needed to communicate needed parameters to
 * appropriate sub-systems.  The block_handle element within ENV_INFO is
 * the only element that is required by DDS.
 */

#include "evl_ret.h"
#include "comm.h"


typedef struct {
	BLOCK_HANDLE	block_handle;	/* This is used to specify the block */
  	void			*app_info;
} ENV_INFO;

typedef enum {ENV_BLOCK_HANDLE, ENV_DEVICE_HANDLE} env_handle_type;
typedef struct {
	BLOCK_HANDLE	block_handle;	/* These first two fields must stay the same */
  	void			*app_info;      /* as ENV_INFO's first two fields */
    env_handle_type type;
    DEVICE_HANDLE   device_handle;
} ENV_INFO2;


#endif 	/* ENV_INFO_H */
