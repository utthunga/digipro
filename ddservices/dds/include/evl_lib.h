/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *
 *	@(#)evl_lib.h	30.3  30  14 Nov 1996
 */

#ifndef EVL_LIB_H
#define EVL_LIB_H

#include <stddef.h>
#include <limits.h>

#include "rtn_code.h"		/** All of these .h files are needed by the eval library **/
#include "tags_sa.h"		/** To make it easier for a user to use eval, the user **/
#include "ddldefs.h"		/** only needs to include evl_lib.h **/
#include "flats.h"
#include "table.h"
#include "cm_lib.h"
#include "env_info.h"
#include "evl_ret.h"
#ifdef DEBUG
#include "dds_chk.h"
#endif


/**
 * Range Values
 **/

#ifndef DDL_HUGE_INTEGER
#define DDL_HUGE_INTEGER ULONG_MAX	/** defines the maximum size of an unsigned **/
									/** integer (also defined in ddldefs.h) **/
#endif /* DDL_HUGE_INTEGER */


/**
 ** Structures and function prototypes to support upcalls to an application
 **/

typedef struct {
	unsigned short  type;	/** valid types are defined in ddldefs.h (ie. DDS_INTEGER) **/
	unsigned short  size;	/** number of bytes a value can occupy **/
	union {
		float				f;
		double				d;
		unsigned long		u;
#if defined(SUN) || defined (SVR4) || defined (CODECENTER) || defined (__linux__)
		unsigned long long	ll;
#endif /* SUN */
#if defined(WIN32) || defined(_MSVC)
		unsigned __int64	ll;
#endif /* WIN32 || _MSVC */
		long				i;
		STRING				s;
		unsigned short		sa[4];
		char				ca[8];

	}               val;	/** value of a variable (obtained from app) **/
}               EVAL_VAR_VALUE;


/*
 *	Structure used to pass info to the device specific string service
 */
typedef struct {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;

#ifdef __cplusplus
extern "C" {
#endif /* __CPLUSPLUS */

/**
 ** flat interface functions
 **/

extern int eval_item P((void *, unsigned long, ENV_INFO2 *, RETURN_LIST *, ITEM_TYPE));
extern int eval_dir_block_tables P(( FLAT_BLOCK_DIR *, BIN_BLOCK_DIR *, unsigned long ));
extern int eval_dir_device_tables P(( FLAT_DEVICE_DIR *, BIN_DEVICE_DIR *, unsigned long));


/**
 ** flat destructor functions
 **/

extern void eval_clean_item P((void *, ITEM_TYPE));


/**
 ** directory (ie. table) destructor functions
 **/

extern void eval_clean_device_dir P((FLAT_DEVICE_DIR *));
extern void eval_clean_block_dir P((FLAT_BLOCK_DIR *));

/* Utility function for ENV_INFO to ENV_INFO2 conversion */
extern void dds_env_to_env2(ENV_INFO *env_info, ENV_INFO2 *env_info2);

/**
 ** application function prototypes
 **/

extern int app_func_get_param_value P((ENV_INFO *, OP_REF *, EVAL_VAR_VALUE *));
extern int app_func_get_dict_string P((ENV_INFO *, DDL_UINT, STRING *));
extern int app_func_get_dev_spec_string P((ENV_INFO *, DEV_STRING_INFO *, STRING *));

/* Optional (5.1 and later) function pointer prototypes */
extern int (*app_func_get_block_handle) P((ENV_INFO *env_info, ITEM_ID block_id,
                                     unsigned int instance_num, BLOCK_HANDLE *out_bh));

#ifdef __cplusplus
}
#endif /* __CPLUSPLUS */

#endif /* EVL_LIB_H */
