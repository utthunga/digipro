/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)layer7.h	30.3  30  14 Nov 1996
 */

#ifndef HL_LAYER7_H
#define HL_LAYER7_H

#include "attrs.h"
#include "ddldefs.h"
#include "rtn_code.h"
/*
 *	#include "hsym0002.h"
 */
#include "responsa.h"
#include "pc_lib.h"
#include "flats.h"
#include "l7_fms.h"

/* data global to this file */
typedef enum {
		L7_READ_PARAM, L7_WRITE_PARAM, L7_SEND_CMD, L7_BI_SEND_CMD,
		L7_MORE_DATA, L7_BURN_EEPROM
} L7_REQUEST_TYPE;

#define MAX_DATA_BYTES 32
#define MAX_DATA_BITS (MAX_DATA_BYTES * 8)
#define MAX_COMM_STATUS_MESSAGES	 8
#define MAX_DEVICE_STATUS_MESSAGES	100
#define MAX_REPLY_ITEM_COUNT 50
#define MAX_REQUEST_ITEM_COUNT 50
#define MAX_RESP_CODE_COUNT 50
#define MAX_COMM_STAT_COUNT 8
#define MAX_DEVICE_STAT_COUNT 32

#define IDENT_CMD 0
#define REV4_READTAG 4
#define REV4_WRITETAG 5
#define TAG_IDENT 11
#define REV5_READTAG 13
#define REV5_WRITETAG 18

#define TRANSACTION_NUMBER unsigned short

typedef struct {
	unsigned short	resp_code_class;
	char			*string;			 /* address of string */
 } RESP_CODE_LIST;

typedef struct {
	OUTPUT_STATUS_LIST	*sts_class;
	char				*string;		 /* address of string */
} COMM_STATUS_ENTRY;

typedef struct {
	unsigned char		size;
	COMM_STATUS_ENTRY	comm_status[MAX_COMM_STATUS_MESSAGES];
} COMM_STATUS_LIST;

typedef struct {
	OUTPUT_STATUS_LIST	*sts_class;
	char				*string;		 /* address of string */
} DEVICE_STATUS_ENTRY;

typedef struct {
	unsigned char		size;
	DEVICE_STATUS_ENTRY	device_status[MAX_DEVICE_STATUS_MESSAGES];
} DEVICE_STATUS_LIST;

typedef struct {
	RESP_CODE_LIST		response_code_list;
	COMM_STATUS_LIST	comm_status_list;
	DEVICE_STATUS_LIST	device_status_list;
} ERROR_STATUS_LIST;

typedef struct {
	int					status;
	P_REF				param_ref;
	ERROR_STATUS_LIST	error_status;
} LAYER7_RESPONSE;

typedef struct {
	unsigned char	byte_index;
	char			bit_index;
} DATA_INDEX;

typedef struct {
	int				status;
	unsigned char	comm_status;
	unsigned char	response_code;
	unsigned char	device_status;
} RESP_CODE_BYTES;

#define SUCCESS_CLASS 1
#define WARNING_CLASS 2
#define ERROR_CLASS 3


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* interface prototypes */

/* interface to layer7 using a "read_param_request" */
extern int l7_read_param_request P(( ENV_INFO * , P_REF * ));

/* interface to layer7 using a "write_param_request" */
extern int l7_write_param_request P(( ENV_INFO *, P_REF *));

/* interface to layer7 using a "send_cmd_request" */ 
extern int l7_send_cmd_request P(( ENV_INFO *, unsigned short ));

/* interface to layer7 using a "send_cmd_request", transaction and indices */
extern int l7_bi_send_cmd_request P((ENV_INFO *	, unsigned short, 
	unsigned long, DL_RESPONSE *));

/* interface to layer7 which allows the user to generate the response */
/* code lists for the 3 bytes of the response code */
extern int l7_build_error_status_codes P(( ENV_INFO,
	RESP_CODE_BYTES *, FLAT_COMMAND *, short, DL_RESPONSE *));


/* filter functions between layer 7 and ptoc to get command descriptions */

extern int l7_cmd_filter P(( ENV_INFO *, unsigned short, unsigned long,
	FLAT_COMMAND *, unsigned long *));

extern int l7_param_cmd_filter P((ENV_INFO *, P_REF *, unsigned short,
	unsigned long *, FLAT_COMMAND *));

extern int trans_num_to_trans_index P(( FLAT_COMMAND *cmd, unsigned long trans_num,
	unsigned long *trans_index ));


/*
 *  This is not really an interface routine, but l7_fms.c needs this routine.
 */

extern int l7_unparse_masked_data P(( DATA_INDEX *, unsigned long *, unsigned char *,
	unsigned char ));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* HL_LAYER7_H */
