/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *		@(#)app_xmal.h	21.5  21  G%
 */

#ifndef APP_XMAL_H
#define APP_XMAL_H

#include <stddef.h>
#include "std.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void    *xcalloc P((size_t, size_t));
extern void    *xmalloc P((size_t));
extern void    *xrealloc P((void *, size_t));
extern void xfree P((void **));

extern void bigError();
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* APP_XMAL_H */
