/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)dds_chk.h	30.3	 30.3  14 Nov 1996
 */

#ifndef DDS_CHK_H
#define DDS_CHK_H

#include "std.h"
#include "table.h"
#include "tags_sa.h"
#include "ddldefs.h"
#include "flats.h"

/*
 * Flat item list structure checking macros
 */

#define	CHK_FLAT_LIST(A,M,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_VAR_FLAT_LIST(A,M,F,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else if (F) {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_STRING(A,M,S)	\
{	\
	if ((A) & (M)) {	\
		if ((S).len) {	\
			ASSERT_DBG((S).str);	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(S).str && !(S).len);	\
	}	\
}

/*
 * Directory table structure checking macros
 */

#define CHK_BIN_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG((T).chunk && (T).size);	\
	}	\
	else {	\
		ASSERT_DBG(!(T).chunk && !(T).size);	\
	}	\
}

#define CHK_FLAT_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG(((T).list && (T).count) ||	\
			(!(T).list && !(T).count));	\
	}	\
	else {	\
		ASSERT_DBG(!(T).list && !(T).count);	\
	}	\
}

#ifdef DEBUG
/*
 * Function prototypes
 */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void dds_item_flat_check P(( void *, ITEM_TYPE ));
extern void dds_dev_dir_flat_chk P(( BIN_DEVICE_DIR *, FLAT_DEVICE_DIR * ));
extern void dds_blk_dir_flat_chk P(( BIN_BLOCK_DIR *, FLAT_BLOCK_DIR * ));
#endif /* DEBUG */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DDS_CHK_H */
