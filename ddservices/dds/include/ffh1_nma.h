/*  Filename    : ffh1_nma.h                                                 */
/*                                                                           */
/*  Description : This file contains the types and defines for the NMA       */
/*                service interface (by including fbkhost_nma.h)             *
/*                                                                           */
/*****************************************************************************/


#ifndef __FFH1_NMA__
#define __FFH1_NMA__

#include "fbkhost_nma.h"

#endif /*defined __FFH1_NMA__*/
