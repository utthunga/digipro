/*  Filename    : ffh1_sm.h                                                  */
/*                                                                           */
/*  Description : The file "links" to fbkhost_sm.h ( to elminate duplication */
/*                                                                           */
/*****************************************************************************/


#ifndef __FFH1_SM__
#define __FFH1_SM__

#include "fbkhost_sm.h"

#endif  /* __FFH1_SM__ */
