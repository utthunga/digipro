#ifndef _CFFIO_H_
#define _CFFIO_H_

#include <evl_lib.h>


class CFFIO
{
public:
    void SendMessage(char *buffer);
    int  IsAnyACKReceived(void);
    void GetData(EVAL_VAR_VALUE *var_value);
    void SendMenuList(int option,char * menudata);
};


/* C extern functions which needed acess in C++ functions,
 * can be placed in below block.
 */

#ifdef __cplusplus
extern "C" {
#endif

extern  int         check_for_input();

#ifdef __cplusplus
}
#endif

#endif
