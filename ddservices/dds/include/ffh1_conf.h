/*****************************************************************************/
/*  Filename    : ffh1_conf.h                                                */
/*                                                                           */
/*  Description : This file redirects the reader to fbkhost_conf.h in the    */
/*                Softing FF stack for the FBK HOST configuration            */
/*                                                                           */
/*****************************************************************************/

#ifndef __FFH1_CONF__
#define __FFH1_CONF__

#include "fbkhost_conf.h"

#endif /* __FFH1_CONF__ */
