/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)ddsbltin.h	30.3 14 Nov 1996
 */

#ifndef DDSBLTIN_H
#define DDSBLTIN_H

#include "env_info.h"

typedef struct {
	char			*select_string;		/*Pointer to string to display*/
	char			*select_str_head;	/*Pointer for freeing memory*/
}				SELECT_OPTION;

typedef struct {
	int				select_count;
	SELECT_OPTION	*select_option;
}				SELECT_LIST;

/* Max. Param's which can be specified in a prompt string ( 0 to 9 ) */

#define MAX_PROMPT_MSG_PARAMS 10

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 *	Builtin support routines.
 */

#ifdef _MSC_VER
extern void sleep P((unsigned int)) ;
#endif /* _MSC_VER */

extern int bltin_format_string P((char *out_buf, int max_length,
			char *prompt, ITEM_ID *var_ids,	SUBINDEX *var_subindices, 
            ENV_INFO *env_info));

extern int bltin_format_string2 P((char *out_buf, int max_length,
			char *prompt, ITEM_ID *var_ids,
			SUBINDEX *var_subindices, BLOCK_HANDLE *bhs, int usb_bh,
			ENV_INFO2 *env_info));

extern long bltin_assign P((ITEM_ID dest_id, ITEM_ID dest_member_id, ITEM_ID src_id, 
		                  ITEM_ID src_member_id, BLOCK_HANDLE dstBH, BLOCK_HANDLE srcBH, 
						  int use_bh, ENV_INFO2 *env_info));

extern long bltin_display_message P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
		                           long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));

extern long bltin_delayfor P((long duration, char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
		                    long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));

extern long bltin_edit_device_value P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
	                                 long id_count, ITEM_ID param_id, ITEM_ID member_id, 
	                                 BLOCK_HANDLE bh, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));

extern long bltin_edit_local_value P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
		                            long id_count, char *localparam, BLOCK_HANDLE *bhs, 
									int use_bh, ENV_INFO2 *env_info));

extern long bltin_select_from_menu2 P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
	                                long id_count, char *options, long *selection, 
									BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));

extern long bltin_get_acknowledgement2 P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 		   
										 long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));

extern long bltin_display_dynamics P((char *prompt, ITEM_ID *param_ids, ITEM_ID *member_ids, 
		                            long id_count, BLOCK_HANDLE *bhs, int use_bh, ENV_INFO2 *env_info));



/*
 *	Services common to all builtins
 */

extern char *find_lang_string P((char *text));
extern int bltin_error_filter P((int error_code, int retry_ok,
			ENV_INFO2 *env_info));


/*
 *	Abort Builtins
 */

extern long add_abort_method P((ITEM_ID method_id, ENV_INFO2 *env_info));
extern long remove_abort_method P((ITEM_ID method_id, ENV_INFO2 *env_info));
extern void remove_all_abort_methods P((ENV_INFO2 *env_info));
extern void method_abort P((char *prompt, ENV_INFO2 *env_info));


/*
 *	Parameter Cache Builtins
 */

extern void discard_on_exit P((ENV_INFO2 *env_info));
extern void send_on_exit P((ENV_INFO2 *env_info));
extern void save_on_exit P((ENV_INFO2 *env_info));

extern long get_date_lelem P((ITEM_ID list_id, ITEM_ID list_index, 
                             ITEM_ID element_id, ITEM_ID subelement_id, 
                             char *datap, long *lengthp, ENV_INFO2 *env_info));
extern long get_string_lelem P((ITEM_ID list_id, ITEM_ID list_index, 
                             ITEM_ID element_id, ITEM_ID subelement_id, 
                             char *datap, long *lengthp, ENV_INFO2 *env_info));
extern float get_float_lelem P((ITEM_ID list_id, unsigned long list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern double get_double_lelem P((ITEM_ID list_id, unsigned long list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern unsigned long get_unsigned_lelem P((ITEM_ID list_id, unsigned long list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern signed long get_signed_lelem P((ITEM_ID list_id, unsigned long list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));

extern long get_date_lelem2 P((ITEM_ID list_id, ITEM_ID list_index,
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id, 
                             char *datap, long *lengthp, ENV_INFO2 *env_info));
extern long get_string_lelem2 P((ITEM_ID list_id, ITEM_ID list_index, 
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
                             ITEM_ID element_id, ITEM_ID subelement_id, 
                             char *datap, long *lengthp, ENV_INFO2 *env_info));
extern float get_float_lelem2 P((ITEM_ID list_id, unsigned long list_index,
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern double get_double_lelem2 P((ITEM_ID list_id, unsigned long list_index,
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern unsigned long get_unsigned_lelem2 P((ITEM_ID list_id, unsigned long list_index,
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));
extern signed long get_signed_lelem2 P((ITEM_ID list_id, unsigned long list_index,
                              ITEM_ID embedded_list_id, ITEM_ID embedded_list_index,
			ITEM_ID subelement, ITEM_ID subsubelement, ENV_INFO2 *env_info));

extern long get_sel_string P((char *out_str, ITEM_ID id, ITEM_ID selector,
			ITEM_ID additional, long *out_len, ENV_INFO2 *env_info));
extern double get_sel_double P((ITEM_ID var_id, ITEM_ID selector,
			ITEM_ID additional, ENV_INFO2 *env_info));
extern double get_sel_double2 P((ITEM_ID id, int index, ITEM_ID embedded_id, 
                                 ITEM_ID selector, ITEM_ID additional, 
                                 ENV_INFO2 *env_info));
extern long get_float_value P((ITEM_ID var_id, ITEM_ID member_id,
			float *valuep, ENV_INFO2 *env_info));
extern float ret_float_value P((ITEM_ID var_id, ITEM_ID member_id,
			ENV_INFO2 *env_info));
extern long put_float_value P((ITEM_ID var_id, ITEM_ID member_id,
			double value, ENV_INFO2 *env_info));
extern long get_double_value P((ITEM_ID var_id, ITEM_ID member_id, 
			double *valuep, ENV_INFO2 *env_info));
extern double ret_double_value P((ITEM_ID var_id, ITEM_ID member_id, 
			ENV_INFO2 *env_info));
extern long put_double_value P((ITEM_ID var_id, ITEM_ID member_id,
			double value, ENV_INFO2 *env_info));
extern long get_signed_value P((ITEM_ID var_id, ITEM_ID member_id,
			long *valuep, ENV_INFO2 *env_info));
extern long ret_signed_value P((ITEM_ID var_id, ITEM_ID member_id, 
			ENV_INFO2 *env_info));
extern long put_signed_value P((ITEM_ID var_id, ITEM_ID member_id,
			long value, ENV_INFO2 *env_info));
extern long get_unsigned_value P((ITEM_ID var_id, ITEM_ID member_id,
			unsigned long *valuep, ENV_INFO2 *env_info));
extern unsigned long ret_unsigned_value P((ITEM_ID var_id, ITEM_ID member_id, 
			ENV_INFO2 *env_info));
extern long put_unsigned_value P((ITEM_ID var_id, ITEM_ID member_id,
			unsigned long value, ENV_INFO2 *env_info));
extern long get_string_value P((ITEM_ID var_id, ITEM_ID member_id,
			char *valuep, long *lengthp, ENV_INFO2 *env_info));
extern long put_string_value P((ITEM_ID var_id, ITEM_ID member_id,
			char *valuep, long length, ENV_INFO2 *env_info));
extern long get_date_value P((ITEM_ID var_id, ITEM_ID member_id,
			char *datap, long *sizep, ENV_INFO2 *env_info));
extern long put_date_value P((ITEM_ID var_id, ITEM_ID member_id,
			char *datap, long size, ENV_INFO2 *env_info));
extern long assign P((ITEM_ID dest_id, ITEM_ID dest_member_id,
			ITEM_ID src_id, ITEM_ID src_member_id, ENV_INFO2 *env_info));


/*
 *	Communication Builtins
 */

extern long read_value P((ITEM_ID var_id, ITEM_ID member_id,
			ENV_INFO2 *env_info));
extern long send_value P((ITEM_ID var_id, ITEM_ID member_id,
			ENV_INFO2 *env_info));
extern long send_all_values P((ENV_INFO2 *env_info));
extern void get_response_code P((UINT32 *resp_codep, ITEM_ID *itemp,
			ITEM_ID *member_idp, ENV_INFO2 *env_info));
extern UINT32 get_comm_error P((ENV_INFO2 *env_info));
extern void abort_on_all_comm_errors P((ENV_INFO2 *env_info));
extern void abort_on_all_response_codes P((ENV_INFO2 *env_info));
extern long abort_on_comm_error P((UINT32 error, ENV_INFO2 *env_info));
extern long abort_on_response_code P((UINT32 code, ENV_INFO2 *env_info));
extern void fail_on_all_comm_errors P((ENV_INFO2 *env_info));
extern void fail_on_all_response_codes P((ENV_INFO2 *env_info));
extern long fail_on_comm_error P((UINT32 error, ENV_INFO2 *env_info));
extern long fail_on_response_code P((UINT32 code, ENV_INFO2 *env_info));
extern void retry_on_all_comm_errors P((ENV_INFO2 *env_info));
extern void retry_on_all_response_codes P((ENV_INFO2 *env_info));
extern long retry_on_comm_error P((UINT32 error, ENV_INFO2 *env_info));
extern long retry_on_response_code P((UINT32 code, ENV_INFO2 *env_info));
extern long get_status_string P((ITEM_ID id, ITEM_ID member_id,
			UINT32 status, char *out_buf, long maxlen,
			ENV_INFO2 *env_info));
extern long get_comm_error_string P((UINT32 error, char *out_buf,
			long maxlen, ENV_INFO2 *env_info));
extern long get_response_code_string P((ITEM_ID id, ITEM_ID member_id,
			UINT32 code, char *out_buf, long maxlen, ENV_INFO2 *env_info));


/*
 *	Display Builtins
 */

extern long display_builtin_error P((long error, ENV_INFO2 *env_info));
extern long plot_create P((unsigned long id, ENV_INFO2 *env_info));
extern long plot_destroy P((long plot_handle, ENV_INFO2 *env_info));
extern long plot_display P((long plot_handle, ENV_INFO2 *env_info));
extern long plot_edit P((long plot_handle, unsigned long waveform_id, 
                         ENV_INFO2 *env_info));
extern long list_create_element P((unsigned long id, int index, 
                                  unsigned long elementID, ENV_INFO2 *env_info));
extern long list_delete_element P((unsigned long id, int index, 
                                  ENV_INFO2 *env_info));
extern long list_create_element2 P((unsigned long id, int index, 
                                   unsigned long embedded_id, int embedded_index,
                                  unsigned long elementID, ENV_INFO2 *env_info));
extern long list_delete_element2 P((unsigned long id, int index, 
                                  unsigned long embedded_id, int embedded_index, 
                                  ENV_INFO2 *env_info));
extern long display_message P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, ENV_INFO2 *env_info));
extern long display_dynamics P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, ENV_INFO2 *env_info));
extern long display_comm_error P((UINT32 error, ENV_INFO2 *env_info));
extern long display_response_code P((ITEM_ID id, ITEM_ID member_id,
			UINT32 code, ENV_INFO2 *env_info));
extern long delayfor P((long duration, char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, ENV_INFO2 *env_info));
extern long edit_device_value P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, ITEM_ID param_id,
			ITEM_ID param_member_id, ENV_INFO2 *env_info));
extern long edit_local_value P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, char *localvar, 
			ENV_INFO2 *env_info));
extern long select_from_menu P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, char *options,
			long *selection, ENV_INFO2 *env_info));
extern long get_acknowledgement P((char *prompt, ITEM_ID *var_ids,
			ITEM_ID *member_ids, long id_count, ENV_INFO2 *env_info));
extern long menu_display P((ITEM_ID menuid, char *prompt, 
                           long *selection, ENV_INFO2 *env_info));


/*
 *	Miscellaneous Builtins
 */

extern long is_NaN P((double dvalue));
extern long NaN_value P((double *dvaluep));
extern long get_stddict_string P((UINT32 id, char *str_buf, 
			long maxlen, ENV_INFO2 *env_info));
extern ITEM_ID resolve_block_ref P((ITEM_ID member, 
			ENV_INFO2 *env_info));
extern ITEM_ID resolve_param_ref P((ITEM_ID member, 
			ENV_INFO2 *env_info));
extern ITEM_ID resolve_local_ref P((ITEM_ID member, 
			ENV_INFO2 *env_info));
extern ITEM_ID resolve_param_list_ref P((ITEM_ID member, 
			ENV_INFO2 *env_info));
extern ITEM_ID resolve_array_ref P((ITEM_ID array_id,
			UINT32 index, ENV_INFO2 *env_info));
extern ITEM_ID resolve_record_ref P((ITEM_ID collection_id,
			ITEM_ID member, ENV_INFO2 *env_info));
extern ITEM_ID resolve_list_ref P((ITEM_ID list_id,
			ENV_INFO2 *env_info));
extern ITEM_ID resolve_selector_ref P((ITEM_ID collection_id,
			ITEM_ID selector, ITEM_ID additional, ENV_INFO2 *env_info));
extern long get_resolve_status P((ENV_INFO2 *env_info));
extern long get_dds_error P((char *buffer, long maxlen, ENV_INFO2 *env_info));



/*
 *	Pre/post Read/write builtins
 */

extern long get_float P((float *valuep, ENV_INFO2 *env_info));
extern long put_float P((double value, ENV_INFO2 *env_info));
extern long get_double P((double *valuep, ENV_INFO2 *env_info));
extern long put_double P((double value, ENV_INFO2 *env_info));
extern long get_signed P((long *valuep, ENV_INFO2 *env_info));
extern long put_signed P((long value, ENV_INFO2 *env_info));
extern long get_unsigned P((unsigned long *valuep, ENV_INFO2 *env_info));
extern long put_unsigned P((unsigned long value, ENV_INFO2 *env_info));
extern long get_string P((char *valuep, long *lengthp, ENV_INFO2 *env_info));
extern long put_string P((char *valuep, long length, ENV_INFO2 *env_info));
extern long get_date P((char *datap, long *sizep, ENV_INFO2 *env_info));
extern long put_date P((char *datap, long size, ENV_INFO2 *env_info));

/* Debug functions */
extern long _l P((unsigned long lineNo, void *env_info));

/* 5.1 additions */
extern long   assign2 P((unsigned long   dstBlockId,
               unsigned long   dstBlockInstance,
               unsigned long   dstParamId,
               unsigned long   dstMemberId,
               unsigned long   srcBlockId,
               unsigned long   srcBlockInstance,
               unsigned long   srcParamId,
               unsigned long   srcMemberId, ENV_INFO2 *env_info));

extern long   delayfor2 P((long            duration,
                 char           *prompt,
                 unsigned long   blockIds[],
                 unsigned long   blockInstances[],
                 unsigned long   paramIds[],
                 unsigned long   memberIds[],
                 long            count, ENV_INFO2 *env_info));

extern long   display_dynamics2 P((char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count, ENV_INFO2 *env_info));

extern long   display_message2 P((char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count, ENV_INFO2 *env_info));

extern long   edit_device_value2 P((char           *prompt,
                          unsigned long   blockIds[],
                          unsigned long   blockInstances[],
                          unsigned long   paramIds[],
                          unsigned long   memberIds[],
                          long            count,
                          unsigned long   blockId,
                          unsigned long   blockInstance,
                          unsigned long   paramId,
                          unsigned long   memberId, ENV_INFO2 *env_info));

extern long   edit_local_value2 P((char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count,
                         char           *localVariable, ENV_INFO2 *env_info));

extern long   get_acknowledgement2 P((char           *prompt,
                            unsigned long   blockIds[],
                            unsigned long   blockInstances[],
                            unsigned long   paramIds[],
                            unsigned long   memberIds[],
                            long            count, ENV_INFO2 *env_info));

extern long   get_block_instance_by_object_index P((unsigned long   objectIndex,
                                          unsigned long  *relativeIndex, ENV_INFO2 *env_info));

extern long   get_block_instance_by_tag P((unsigned long   blockId,
                                 char           *blockTag,
                                 unsigned long  *relativeIndex, ENV_INFO2 *env_info));

extern long   get_block_instance_count P((unsigned long   blockId,
                                unsigned long  *count, ENV_INFO2 *env_info));

extern long   get_date_value2 P((unsigned long   blockId,
                       unsigned long   blockInstance,
                       unsigned long   paramId,
                       unsigned long   memberId,
                       char           *value,
                       long           *length, ENV_INFO2 *env_info));

extern long   get_double_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         double         *value, ENV_INFO2 *env_info));

extern long   get_float_value2 P((unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        float          *value, ENV_INFO2 *env_info));

extern long   get_signed_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         long           *value, ENV_INFO2 *env_info));

extern long   get_string_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         char           *value,
                         long           *length, ENV_INFO2 *env_info));

extern long   get_unsigned_value2 P((unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           unsigned long  *value, ENV_INFO2 *env_info));

extern double   ret_double_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         ENV_INFO2 *env_info));

extern float   ret_float_value2 P((unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        ENV_INFO2 *env_info));

extern long   ret_signed_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         ENV_INFO2 *env_info));

extern unsigned long   ret_unsigned_value2 P((unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           ENV_INFO2 *env_info));

extern long   put_date_value2 P((unsigned long   blockId,
                       unsigned long   blockInstance,
                       unsigned long   paramId,
                       unsigned long   memberId,
                       char           *value,
                       long            length, ENV_INFO2 *env_info));

extern long   put_double_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         double          value, ENV_INFO2 *env_info));

extern long   put_float_value2 P((unsigned long   blockId,
                        unsigned long   blockInstance,
                        unsigned long   paramId,
                        unsigned long   memberId,
                        double          value, ENV_INFO2 *env_info));

extern long   put_signed_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         long            value, ENV_INFO2 *env_info));

extern long   put_string_value2 P((unsigned long   blockId,
                         unsigned long   blockInstance,
                         unsigned long   paramId,
                         unsigned long   memberId,
                         char           *value,
                         long            length, ENV_INFO2 *env_info));

extern long   put_unsigned_value2 P((unsigned long   blockId,
                           unsigned long   blockInstance,
                           unsigned long   paramId,
                           unsigned long   memberId,
                           unsigned long   value, ENV_INFO2 *env_info));

extern long   read_value2 P((unsigned long   blockId,
                   unsigned long   blockInstance,
                   unsigned long   paramId,
                   unsigned long   memberId, ENV_INFO2 *env_info));

extern long   select_from_menu2 P((char           *prompt,
                         unsigned long   blockIds[],
                         unsigned long   blockInstances[],
                         unsigned long   paramIds[],
                         unsigned long   memberIds[],
                         long            count,
                         char           *options,
                         long           *selection, ENV_INFO2 *env_info));

extern long   send_value2 P((unsigned long   blockId,
                   unsigned long   blockInstance,
                   unsigned long   paramId,
                   unsigned long   memberId, ENV_INFO2 *env_info));

extern ITEM_ID resolve_param_ref2 P((ITEM_ID block_id, 
                   unsigned long instance, 
                   ITEM_ID id, 
                   ENV_INFO2 *env_info));

extern ITEM_ID resolve_block_ref2 P((ITEM_ID block_id, 
                   unsigned long instance, 
                   ITEM_ID id, 
                   ENV_INFO2 *env_info));

extern ITEM_ID resolve_local_ref2 P((ITEM_ID block_id, 
                   unsigned long instance, 
                   ITEM_ID id, 
                   ENV_INFO2 *env_info));

extern ITEM_ID resolve_array_ref2 P((ITEM_ID block_id, 
                   unsigned long instance, 
                   ITEM_ID  id, 
                   unsigned long subindex,
                   ENV_INFO2 *env_info));

extern ITEM_ID resolve_record_ref2 P((ITEM_ID block_id, 
                   unsigned long instance, 
                   ITEM_ID id, 
                   ITEM_ID member,
                   ENV_INFO2 *env_info));

extern long get_block_instance_by_object_index P((unsigned long   objectIndex,
                                               unsigned long  *instance, 
                                               ENV_INFO2       *env_info));

extern long   get_block_instance_by_tag P((ITEM_ID  blockId,
                                 char           *blockTag,
                                 unsigned long  *instance, 
                                 ENV_INFO2       *env_info));

extern long   get_block_instance_count P((ITEM_ID  blockId,
                                unsigned long  *count, 
                                ENV_INFO2       *env_info));

/* end 5.1 additons */











#if defined(DEBUG) || defined(DDSTEST)
/*
 *  Debug/DDSTEST function builtins.
 *
 *  See the ddsbltin.c for detailed explanation of parameters and functions.
 */
extern long bi_rcsim_set_comm_err P((int network_handle, int device_stn_address,
						UINT32 comm_err, ENV_INFO2 *env_info));
extern long bi_rcsim_set_response_code P((int network_handle, int device_stn_address,
						UINT32 response_code, ENV_INFO2 *env_info));

#endif /* defined(DEBUG) || defined(DDSTEST) */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /*DDSBLTIN_H*/
