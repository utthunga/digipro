/******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    Language Upcall definitions
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef LANG_UPCL_H
#define LANG_UPCL_H

#include "attrs.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	extern int (*get_lang_str_upcall) P((char* src_str, STRING* dst_str));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /*LANG_UPCL_H*/
