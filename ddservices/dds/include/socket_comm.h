#ifndef	SOCKET_COMM_H
#define	SOCKET_COMM_H

#include "ff_if.h"
#include "device_access.h"

#define 	PORT_NUMBER 7999
#define 	ADDRESS "127.0.0.1"
#define		MAX_RX_SOCKET_BUFF_SIZE		1024
#define		LISTENQ	1
#define VALID_SOCKET	1
#define INVALID_SOCKET	0

enum {
    MSG_TYPE_INVALID_REQ = 0,
    MSG_TYPE_DAT_REQ,
    MSG_TYPE_ACK_REQ,
    MSG_TYPE_NMA_REQ,
};

enum {
    MSG_TYPE_INVALID_RES = 0,
    MSG_TYPE_DAT_RES,
    MSG_TYPE_ACK_RES,
    MSG_TYPE_NMA_RES,
};


enum {
	NMA_DATA_TYPE = 1,
	NMA_DATA_VALID_TYPE,
	LISTEN_FD_DATA_TYPE,
	ACCEPT_FD_DATA_TYPE,
};


#define ACCEPT_FD_LOC 		0
#define LISTEN_FD_LOC		(ACCEPT_FD_LOC + sizeof(int))
#define CM_NMA_LOC			(LISTEN_FD_LOC + sizeof(int))
#define	CM_NMA_VALID_LOC	(CM_NMA_LOC + sizeof(CM_NMA_INFO))


typedef union {
	char buffer[MAX_RX_SOCKET_BUFF_SIZE];
	struct {
        int msg_type;
		int channel;
		unsigned short code;
		T_FIELDBUS_SERVICE_DESCR devacc_sdb;
		T_DEVICE_ERROR	dev_err;
		unsigned char data[512];
		COMM_REQ comm_req;
	};
}SOCKET_DATA;

int create_server_socket(char *, int *);
int create_client_socket(int *);
int send_ack_to_client(int);
int send_data_to_server(COMM_REQ *);
int send_server_req_res(int, T_FIELDBUS_SERVICE_DESCR *,unsigned char *);
int get_ack_from_server(int *);
void *handle_socket_data(void *arg);
int recv_data_from_server(unsigned short *code, T_DEVICE_ERROR *dev_err,
                          T_FIELDBUS_SERVICE_DESCR *devacc_sdb, unsigned char *data_buf);
int send_data_to_client(unsigned short code, T_DEVICE_ERROR *dev_err,
                        T_FIELDBUS_SERVICE_DESCR *devacc_sdb, unsigned char *data);
int create_thread (int /*void *(*function)(void *argc)*/, void *arg);



#endif
