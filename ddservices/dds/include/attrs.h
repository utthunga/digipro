/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)attrs.h	30.3 14 Nov 1996
 */

#ifndef ATTRS_H
#define ATTRS_H

#include "ddldefs.h"
#include "od_defs.h"
#include <stddef.h>
/*
 * Expressions
 */

/*
 * Types of expressions.
 * The permitted type fields are the same as variables types (ddldefs.h)
 */

typedef struct {
	unsigned short  type;	/* valid types are defined in ddldefs.h (ie. INTEGER) */
	unsigned short  size;
	union {
		float           f;
		double          d;
		unsigned long   u;
		long            i;

	}               val;
}               EXPR;

/*
 *  RANGE_DATA_LIST
 */

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	EXPR           *list;
}               RANGE_DATA_LIST;


/*
 * Defines for REFERENCE_ITEM types
 */

typedef struct {
	unsigned short  count;		/* Number of used item IDs */
	unsigned short  limit;		/* Total number of item IDs */
	ITEM_ID        *list;		/* Pointer to list of IDs */
}               ITEM_ID_LIST;


typedef struct {

	ITEM_ID         id;
	ITEM_TYPE       type;
}               DESC_REF;

typedef struct {

	ITEM_ID         id;
	SUBINDEX        subindex;
	ITEM_TYPE       type;
    ITEM_ID         block_id;       /* crossblock block id, or 0 if unused */
    unsigned long   block_instance; /* crossblock instance, only used if block_id is nonzero */
	ITEM_ID			container_id;	/* AR 3746 - ID of LIST or other container */
	SUBINDEX		container_index;/* AR 3746 - index within LIST or other container */
	ITEM_TYPE		container_type;	 /* AR 3746 - type of container (LIST_ITYPE or other) */
}               OP_REF;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OP_REF         *list;
}               OP_REF_LIST;

typedef struct {
	ITEM_TYPE       type;
	ITEM_ID         id;
	unsigned long   element;
}               RESOLVE_INFO;

typedef struct {
	ITEM_ID         op_id;
	SUBINDEX        op_subindex;
	ITEM_TYPE       op_type;
    ITEM_ID         op_block_id; /* Cross-block referencing, 0 if unused */
    unsigned long   op_block_instance; /* Only used if op_block_id is nonzero */
	ITEM_ID			container_id;	/* AR 3746 - LIST or other containing entity */
	SUBINDEX		container_index; /* AR 3746 - index within LIST or other containing entity */
	ITEM_TYPE		container_type;	 /* AR 3746 - type of container (LIST_ITYPE or other) */
	ITEM_ID         desc_id;
	ITEM_TYPE       desc_type;
    unsigned long   bit_mask;  /* Used for BIT_ENUM variables on menus, set to 0 if unused */
	unsigned short  trail_count;
	unsigned short  trail_limit;
	RESOLVE_INFO   *trail;
}               OP_REF_TRAIL;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OP_REF_TRAIL   *list;
}               OP_REF_TRAIL_LIST;


typedef struct
{
    unsigned long length;
    unsigned char *data;
} BINARY_IMAGE;


/*
 * Binary and Dependency Info
 */

typedef struct {
	unsigned long   size;
	unsigned char  *chunk;
}               BININFO;

typedef struct {
	OP_REF_LIST     dep;
	unsigned long   bin_size;
	unsigned char  *bin_chunk;
}               DEPBIN;


#define	DONT_FREE_STRING	 0x00
#define	FREE_STRING			 0x01
#define DDS_SUPPRESS         0x02 /* This string contained an active unit-code suppression flag */
// CPMHACK: Added new option for flags witin string
#define DICT_STRING          0x04 /* This indicates the string is an dictionary string */
#define DD_STRING            0x08 /* This indiactes the string is an dd string */

typedef struct {
	char            *str;	/* the pointer to the string */
	unsigned short  len;	/* the length of the string */
	unsigned short  flags;	/* memory allocation and unit-code suppression flags */
    wchar_t         *wstr;  /* unused by DDS - usable by the host for storage */
    unsigned int    wlen;   /* unused by DDS - usable by the host for storage */
    unsigned short  wflags; /* low order bit used by DDS: if set, DDS will free
                             * the field "wstr" when this STRING structure is freed.
                             * All other bits are usable by the host, and ignored
                             * by DDS.  Regardless of this flag, if ddl_free_string()
                             * is called for this struct, DDS will clear the "wlen" and 
                             * "wflags" fields as well as the "wstr" field.
                             */
}               STRING;

/* The masks for ITEM_ARRAY_ELEMENT */

#define IA_DESC_EVALED		0X01
#define IA_HELP_EVALED		0X02
#define IA_INDEX_EVALED		0X04
#define IA_REF_EVALED		0X08


typedef struct {
	unsigned short  evaled;
	unsigned long   index;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
}               ITEM_ARRAY_ELEMENT;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	ITEM_ARRAY_ELEMENT *list;
}               ITEM_ARRAY_ELEMENT_LIST;


/* The masks for MEMBER */

#define MEM_DESC_EVALED		0X01
#define MEM_HELP_EVALED		0X02
#define MEM_NAME_EVALED		0X04
#define MEM_REF_EVALED		0X08

typedef struct {
	unsigned short  evaled;
	unsigned long   name;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
}               MEMBER;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	MEMBER         *list;
}               MEMBER_LIST;

typedef struct {
	unsigned short  evaled;
	unsigned long   name;
	OP_REF_TRAIL    ref;
	STRING          desc;
	STRING          help;
}               OP_MEMBER;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OP_MEMBER       *list;
}               OP_MEMBER_LIST;



/*
 * Menu
 */

typedef struct {
	OP_REF_TRAIL    ref;
	unsigned short  qual;
    STRING          user_string;
}               MENU_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	MENU_ITEM      *list;
}               MENU_ITEM_LIST;


/*
 * Response Codes
 */

/* The masks for RESPONSE_CODE */

#define RS_DESC_EVALED		0X01
#define RS_HELP_EVALED		0X02
#define RS_TYPE_EVALED		0X04
#define RS_VAL_EVALED		0X08


typedef struct {
	unsigned short  evaled;
	unsigned short  val;
	unsigned short  type;
	STRING          desc;
	STRING          help;
}               RESPONSE_CODE;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	RESPONSE_CODE  *list;
}               RESPONSE_CODE_LIST;


/*
 * Relation Types
 */

typedef struct {

	OP_REF_TRAIL_LIST depend_items;
	OP_REF_TRAIL_LIST update_items;
}               REFRESH_RELATION;

typedef struct {
	OP_REF_TRAIL    var;
	OP_REF_TRAIL_LIST var_units;
}               UNIT_RELATION;

//Added by Lnt
typedef struct {
	ITEM_ID         id;
	SUBINDEX        subindex;
	ITEM_TYPE		op_type;
}		VAR_DETAILS;

typedef struct {
	unsigned short  count;
	VAR_DETAILS		*list;
}		VAR_DETAILS_LIST;

typedef struct {
	unsigned short  count;
	VAR_DETAILS		*unit_main;
	VAR_DETAILS_LIST *unit_dep;
}UNIT_REL;

typedef struct {
    unsigned short  ref_rel_count;
	VAR_DETAILS_LIST *depend_items;
	VAR_DETAILS_LIST *update_items;
}REF_REL;

typedef struct {
    unsigned short  wao_rel_count;
	VAR_DETAILS_LIST *wao_list;
}WAO_REL;
//End
/*
 * Definitions
 */

typedef struct {
	unsigned long   size;
	char           *data;
}               DEFINITION;


/*
 * Variable Types
 */

typedef struct {
	unsigned short  type;
	unsigned short  size;
}               TYPE_SIZE;


/*
 * Enumerations
 */


typedef struct {
	unsigned long   status_class;
}               BIT_ENUM_STATUS;


/* The masks for ENUM_VALUE */

#define ENUM_ACTIONS_EVALED		0X01
#define ENUM_CLASS_EVALED		0X02
#define ENUM_DESC_EVALED		0X04
#define ENUM_HELP_EVALED		0X08
#define ENUM_STATUS_EVALED		0X10
#define ENUM_VAL_EVALED			0X20


typedef struct {
	unsigned short  evaled;
	unsigned long   val;
	STRING          desc;
	STRING          help;
	unsigned long   func_class;	/* functional class */
	BIT_ENUM_STATUS status;
	ITEM_ID         actions;
}               ENUM_VALUE;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	ENUM_VALUE     *list;
}               ENUM_VALUE_LIST;


/*
 * Data Fields
 */

typedef struct {
	union {
		short  iconst;
		OP_REF_TRAIL    ref;
		float           fconst;
	}               data;
	unsigned short  type;
	unsigned short  flags;
	unsigned short  width;
}               DATA_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	DATA_ITEM      *list;
}               DATA_ITEM_LIST;


/*
 * Vector Fields
 */

typedef struct {
	union {
		long            iconst;
		OP_REF_TRAIL    ref;
		float           fconst;
        STRING          str;
	}               vector;
	unsigned short  type;
}               VECTOR_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	VECTOR_ITEM      *list;
}               VECTOR;


typedef struct {
	unsigned short  count;
	unsigned short  limit;
	VECTOR      *vectors;
}               VECTOR_LIST;

/*
 * Transactions
 */

typedef struct {
	unsigned long   number;
	DATA_ITEM_LIST  request;
	DATA_ITEM_LIST  reply;
	RESPONSE_CODE_LIST rcodes;
}               TRANSACTION;


typedef struct {
	unsigned short  count;
	unsigned short  limit;
	TRANSACTION    *list;
}               TRANSACTION_LIST;


/*
 * AXIS
 */
typedef enum {DDS_AXIS_LINEAR, DDS_AXIS_LOGARITHMIC} DDS_AXIS_SCALE;


#endif	/* ATTRS_H */
