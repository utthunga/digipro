/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)attrs.h	30.3 14 Nov 1996
 */

#ifndef OBJ_DESC_H
#define OBJ_DESC_H


int obj_domain_desc(
            ENV_INFO *,
            UINT16,
            OBJECT_INDEX,
            SUBINDEX,
            OBJECT **, 
            UINT8 **, 
            int *);

int obj_invocation_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);

int obj_event_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);


int obj_type_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);

int obj_type_struct_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);

int obj_simple_var_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);

int obj_array_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);


int obj_record_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);


int obj_var_list_desc(
            ENV_INFO *env_info,
            UINT16 cr,
            OBJECT_INDEX object_index,
            SUBINDEX sub_index,
            OBJECT **type_object, 
            UINT8 **value, 
            int *value_size);

#endif
