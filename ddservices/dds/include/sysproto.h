/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 * @(#)sysproto.h	30.3  30  14 Nov 1996
 *
 * This file provides prototypes for library functions with no
 * corresponding #include file prototype (K & R)
 *
 ***/

#ifndef SYSPROTO_H
#define SYSPROTO_H

#if !defined(_MSC_VER) && !defined(__linux__) 

extern int	printf();
extern int	fprintf();
extern int	vprintf();
extern int	sscanf();
extern int	scanf();
extern int	fread();
extern int	fseek();
extern int	fclose();
extern void rewind();

#else

#include <stdio.h>
#endif /* _MSC_VER */

#endif /* SYSPROTO_H */
