/**
 *      Device Description Services Rel. 4.3
 *      Copyright 1994-2002 - Fieldbus Foundation
 *      All rights reserved.
 */

/*
 *  @(#)cm_loc.h    30.4  30  14 Nov 1996
 *  cm_loc.h - Connection Manager Local Definitions
 *
 *  This file contains all internal definitions for
 *  the Connection Manager Library.
 */


#ifndef CM_LOC_H
#define CM_LOC_H

#include "cm_rod.h"

#ifdef  WIN_APP
#ifndef DD_VIEWER
extern __declspec(dllimport) ACTIVE_BLK_TBL         active_blk_tbl;
extern __declspec(dllimport) ACTIVE_DEV_TBL         active_dev_tbl;
extern __declspec(dllimport) ACTIVE_DEV_TYPE_TBL    active_dev_type_tbl;
extern __declspec(dllimport) NETWORK_TBL            network_tbl;

extern __declspec(dllimport) CM_NM_OBJ               nm_obj;

#endif
extern __declspec(dllimport) ROD_TBL                rod_tbl;

#else

#ifndef DD_VIEWER
extern ACTIVE_BLK_TBL       active_blk_tbl;
extern ACTIVE_DEV_TBL       active_dev_tbl;
extern ACTIVE_DEV_TYPE_TBL  active_dev_type_tbl;
extern NETWORK_TBL          network_tbl;
extern ACTIVE_THREAD_DATA   active_thread_data;

extern CM_NM_OBJ            nm_obj;

#endif
extern ROD_TBL              rod_tbl;

#endif /* WIN_APP */


/* Active Block Information Macros */

#ifndef DD_VIEWER
#define ABT_TAG(bh) \
    (active_blk_tbl.list[bh]->tag)
#define SET_ABT_TAG(bh, t) \
    active_blk_tbl.list[bh]->tag = t


#define ABT_A_OP_INDEX(bh) \
    (active_blk_tbl.list[bh]->abt_net_spec.abt_sp_a.op_index)
#define SET_ABT_A_OP_INDEX(bh, oi) \
    active_blk_tbl.list[bh]->abt_net_spec.abt_sp_a.op_index = oi



#define ABT_PARAM_COUNT(bh) \
    (active_blk_tbl.list[bh]->param_count)
#define SET_ABT_PARAM_COUNT(bh, pc) \
    active_blk_tbl.list[bh]->param_count = pc
#define ABT_DD_BLK_ID(bh) \
    (active_blk_tbl.list[bh]->dd_blk_id)
#define SET_ABT_DD_BLK_ID(bh, dbi) \
    active_blk_tbl.list[bh]->dd_blk_id = dbi
#define ABT_DD_BLK_TBL_OFFSET(bh) \
    (active_blk_tbl.list[bh]->dd_blk_tbl_offset)
#define SET_ABT_DD_BLK_TBL_OFFSET(bh, dbto) \
    active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto
#define ABT_APP_INFO(bh) \
    (active_blk_tbl.list[bh]->app_info)
#define SET_ABT_APP_INFO(bh, ai) \
    active_blk_tbl.list[bh]->app_info = ai

// CPMHACK - Added code to get / set pc info
// This code has been added as dd-services is using set_abt_app_info
// to set both APP_INFO and PC structure
#define ABT_PC_INFO(bh) \
    (active_blk_tbl.list[bh]->pc_info)
#define SET_ABT_PC_INFO(bh, pci) \
    active_blk_tbl.list[bh]->pc_info = pci

#define ABT_ADT_OFFSET(bh) \
    (active_blk_tbl.list[bh]->active_dev_tbl_offset)
#define SET_ABT_ADT_OFFSET(bh, adto) \
    active_blk_tbl.list[bh]->active_dev_tbl_offset = adto
#define ABT_USAGE(bh) \
    (active_blk_tbl.list[bh]->usage)
#define SET_ABT_USAGE(bh, u) \
    active_blk_tbl.list[bh]->usage = u
#else
#define ABT_DD_BLK_ID(bh) get_abt_dd_blk_id(bh, NULL)    // FD
#define ABT_ADT_OFFSET(bh) (DEVICE_HANDLE)get_abt_adt_offset(bh, NULL)  // FD
#endif


#define ABT_ADTT_OFFSET(bh) \
    ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh))
#define SET_ABT_ADTT_OFFSET(bh, adtto) \
    SET_ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh), adtto)
#define ABT_DD_DEVICE_ID(bh) \
    ADT_DD_DEVICE_ID(ABT_ADT_OFFSET(bh))
#define SET_ABT_DD_DEVICE_ID(bh, ddi) \
    SET_ADT_DD_DEVICE_ID(ABT_ADT_OFFSET(bh), ddi)
#define ABT_DD_HANDLE(bh) \
    ADT_DD_HANDLE(ABT_ADT_OFFSET(bh))
#define SET_ABT_DD_HANDLE(bh, ddh) \
    SET_ADT_DD_HANDLE(ABT_ADT_OFFSET(bh), ddh)
#define ABT_DD_DEV_TBLS(bh) \
    ADT_DD_DEV_TBLS(ABT_ADT_OFFSET(bh))
#define SET_ABT_DD_DEV_TBLS(bh, ddt) \
    SET_ADT_DD_DEV_TBLS(ABT_ADT_OFFSET(bh), ddt)
#define ABT_DEV_FAMILY(bh) \
    ADT_DEV_FAMILY(ABT_ADT_OFFSET(bh))
#define SET_ABT_DEV_FAMILY(bh, df) \
    SET_ADT_DEV_FAMILY(ABT_ADT_OFFSET(bh), df)


/* Active Device Information Macros */

#define ADT_NETWORK(dh) \
    (active_dev_tbl.list[dh]->network)
#define SET_ADT_NETWORK(dh, n) \
    active_dev_tbl.list[dh]->network = n



#define ADT_STATION_ADDRESS(dh) \
    (active_dev_tbl.list[dh]->station_address)
#define SET_ADT_STATION_ADDRESS(dh, sa) \
    active_dev_tbl.list[dh]->station_address = sa


#define ADT_A_OPOD_CREF(dh) \
    (active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref)
#define SET_ADT_A_OPOD_CREF(dh, oc) \
    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref = oc
#define ADT_A_DDOD_CREF(dh) \
    (active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref)
#define SET_ADT_A_DDOD_CREF(dh, dc) \
    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref = dc
#define ADT_A_MIB_CREF(dh) \
    (active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref)
#define SET_ADT_A_MIB_CREF(dh, mc) \
    active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref = mc


#define ADT_OPROD_HANDLE(dh) \
    (active_dev_tbl.list[dh]->oprod_handle)
#define SET_ADT_OPROD_HANDLE(dh, oh) \
    active_dev_tbl.list[dh]->oprod_handle = oh
#define ADT_APP_INFO(dh) \
    (active_dev_tbl.list[dh]->app_info)
#define SET_ADT_APP_INFO(dh, ai) \
    active_dev_tbl.list[dh]->app_info = ai

#ifndef DD_VIEWER
#define ADT_ADTT_OFFSET(dh) \
    (active_dev_tbl.list[dh]->active_dev_type_tbl_offset)
#else
#define ADT_ADTT_OFFSET(dh) (DEVICE_TYPE_HANDLE)get_adt_adtt_offset(dh, NULL)
#endif

#define SET_ADT_ADTT_OFFSET(dh, adtto) \
    active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto
#define ADT_USAGE(dh) \
    (active_dev_tbl.list[dh]->usage)
#define SET_ADT_USAGE(dh, u) \
    active_dev_tbl.list[dh]->usage = u

#define ADT_DD_DEVICE_ID(dh) \
    ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh))
#define SET_ADT_DD_DEVICE_ID(dh, ddi) \
    SET_ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh), ddi)
#define ADT_DD_HANDLE(dh) \
    ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh))
#define SET_ADT_DD_HANDLE(dh, ddh) \
    SET_ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh), ddh)
#define ADT_DD_DEV_TBLS(dh) \
    ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh))
#define SET_ADT_DD_DEV_TBLS(dh, ddt) \
    SET_ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh), ddt)

#ifndef DD_VIEWER
#define ADT_NET_TYPE(dh) \
    NT_NET_TYPE(ADT_NETWORK(dh))
#else
#define ADT_NET_TYPE(dh) get_adt_net_type(dh, NULL)
#endif

#define ADT_FBAP_CR(dh)  \
    NT_FBAP_CR(ADT_NETWORK(dh))

#define SET_ADT_NET_TYPE(dh, nt) \
    SET_NT_NET_TYPE(ADT_NETWORK(dh), nt)
#define ADT_NET_ROUTE(dh) \
    NT_NET_ROUTE(ADT_NETWORK(dh))
#define SET_ADT_NET_ROUTE(dh, nr) \
    SET_NT_NET_ROUTE(ADT_NETWORK(dh), nr)
#define ADT_DEV_FAMILY(dh) \
    NT_DEV_FAMILY(ADT_NETWORK(dh))
#define SET_ADT_DEV_FAMILY(dh, df) \
    SET_NT_DEV_FAMILY(ADT_NETWORK(dh), df)


/* Active Device Type Information Macros */
#ifndef DD_VIEWER
#define ADTT_DD_DEVICE_ID(dth) \
    (&active_dev_type_tbl.list[dth]->dd_device_id)
#else
#define ADTT_DD_DEVICE_ID(dth) get_adtt_dd_device_id(dth, NULL)
#endif

#define SET_ADTT_DD_DEVICE_ID(dth, ddi) \
	(void *)memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_device_id), \
    (char *)ddi, sizeof(DD_DEVICE_ID))

#ifndef DD_VIEWER
#define ADTT_DD_HANDLE(dth) \
    (&active_dev_type_tbl.list[dth]->dd_handle)
#else
#define ADTT_DD_HANDLE(dth) (DD_HANDLE *)(get_adtt_dd_handle(dth, NULL))
#endif

#ifndef DD_VIEWER
#define SET_ADTT_DD_HANDLE(dth, ddh) \
    (void *)memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_handle), \
    (char *)ddh, sizeof(DD_HANDLE))
#else
#define SET_ADTT_DD_HANDLE(dth, ddh) set_adtt_dd_handle(dth, ddh) 
#endif

#define ADTT_DD_DEV_TBLS(dth) \
    (active_dev_type_tbl.list[dth]->dd_dev_tbls)
#define SET_ADTT_DD_DEV_TBLS(dth, ddt) \
    active_dev_type_tbl.list[dth]->dd_dev_tbls = ddt
#define ADTT_APP_INFO(dth) \
    (active_dev_type_tbl.list[dth]->app_info)
#define SET_ADTT_APP_INFO(dth, ai) \
    active_dev_type_tbl.list[dth]->app_info = ai
#define ADTT_USAGE(dth) \
    (active_dev_type_tbl.list[dth]->usage)
#define SET_ADTT_USAGE(dth, u) \
    active_dev_type_tbl.list[dth]->usage = u


/* Network Information Macros */

#define NT_NET_TYPE(nh) \
    (network_tbl.network_type)
#define SET_NT_NET_TYPE(nh, nt) \
    network_tbl.network_type = nt
#define NT_NET_ROUTE(nh) \
    (&(network_tbl.list[nh].network_route))
#define SET_NT_NET_ROUTE(nh, nr) \
    memcpy((char *)&(network_tbl.list[nh].network_route), \
    (char *)nr, sizeof(NET_ROUTE))
#define NT_DEV_FAMILY(nh) \
    (network_tbl.list[nh].device_family)
#define SET_NT_DEV_FAMILY(nh, df) \
    network_tbl.list[nh].device_family = df
#define NT_CFG_FILE_ID(nh) \
    (network_tbl.list[nh].config_file_id)
#define SET_NT_CFG_FILE_ID(nh, cfi) \
    network_tbl.list[nh].config_file_id = cfi

/* 
 * Macros for assigning MIB_CR and FBAP_CR to 
 * handle the communication between the 
 * connected device in the network
 */
#define NT_MIB_CR(nh) \
    (network_tbl.list[nh].mib_addr.cr)

#define NT_FBAP_CR(nh) \
    (network_tbl.list[nh].fbap_addr.cr)

#define SET_NT_MIB_CR(nh, cfi) \
    network_tbl.list[nh].mib_addr.cr = cfi
#define SET_NT_FBAP_CR(nh, cfi) \
    network_tbl.list[nh].fbap_addr.cr = cfi

#define SET_NT_MIB_ADDR(nh, cfi) \
    network_tbl.list[nh].mib_addr.remote = cfi
#define SET_NT_FBAP_ADDR(nh, cfi) \
    network_tbl.list[nh].fbap_addr.remote = cfi
#define SET_NT_STATION_ADDR(nh, cfi) \
    network_tbl.list[nh].dev_table.station_address = cfi

#define SET_NT_MIB_LOCAL_ADDR(nh, cfi) \
    network_tbl.list[nh].mib_addr.local = cfi
#define SET_NT_FBAP_LOCAL_ADDR(nh, cfi) \
    network_tbl.list[nh].fbap_addr.local = cfi


/* Handle Validity Macros */
#ifndef DD_VIEWER
#define VALID_BLOCK_HANDLE(bh) \
    ((bh >= 0) && (bh < active_blk_tbl.count) \
    && (active_blk_tbl.list[bh]))
#define VALID_DEVICE_HANDLE(dh) \
    ((dh >= 0) && (dh < active_dev_tbl.count) \
    && (active_dev_tbl.list[dh]))
#define VALID_DEVICE_TYPE_HANDLE(dth) \
    ((dth >= 0) && (dth < active_dev_type_tbl.count) \
    && (active_dev_type_tbl.list[dth]))
#define VALID_DD_HANDLE(ddh) \
    ((((ddh)->dd_handle_type == DDHT_ROD) || \
    ((ddh)->dd_handle_type == DDHT_FLAT)) && \
    ((ddh)->dd_handle_number >= 0))
#define VALID_THREAD_HANDLE(th)  ((&th != NULL)) 
#else
#define VALID_BLOCK_HANDLE(bh) valid_block_handle(bh)
#define VALID_DEVICE_HANDLE(dh) valid_device_handle(dh)
#define VALID_DEVICE_TYPE_HANDLE(dth) valid_device_type_handle(dth)
#define VALID_DD_HANDLE(ddh) valid_dd_handle(ddh)
#endif

#define VALID_ROD_HANDLE(rh) \
    ((rh >= 0) && (rh < rod_tbl.count) \
    && (rod_tbl.list[rh]))
#define VALID_NETWORK_HANDLE(nh) \
    ((nh >= 0) && (nh < rod_tbl.count))

#define VALID_NETWORK_COUNT(ct) \
    (ct > 0)

#define VALID_NETWORK_CR(cr) \
    (cr >= 1) 
#define VALID_NETWORK_ADDR(addr) \
    ((addr >= 0x10) && (addr <= 0xf7))
/*
 *  Connection Manager Internal Function Prototypes
 */

/* cm_tbl.c - Connection Manager Table Functions */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int ct_init P((void));
extern void ct_init_dds_tbl_fn_ptrs P((const DDS_TBL_FN_PTRS *));
extern void ct_init_app_info_fn_ptrs P((const APP_INFO_FN_PTRS *));


/* cm_blkld.c - Connection Manager Block Load Functions */

extern void bl_init_app_info_fn_ptrs P((const APP_INFO_FN_PTRS *));


/* cm_dds.c - Connection Manager DD Support Functions */

extern int ds_init P((const char *));
extern void ds_init_dds_tbl_fn_ptrs P((DDS_TBL_FN_PTRS *));
extern void ds_init_app_info_fn_ptrs P((APP_INFO_FN_PTRS *));
extern int find_symbol_file P((DD_DEVICE_ID *dd_device_id, char *out_filename, int out_filename_size));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* CM_LOC_H */
