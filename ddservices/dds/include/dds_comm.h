/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)dds_comm.h	30.3  30  14 Nov 1996
 *
 *	dds_comm.h - Comm Stack definitions
 */

#ifndef	DDS_COMM_H
#define	DDS_COMM_H

#include "cm_lib.h"
#include "host.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int initialize P((ENV_INFO *, HOST_CONFIG *));
extern int configure_stack P((ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG *));
extern int get_stack_configuration P((ENV_INFO *, CM_NMA_INFO *));
extern int init_network P((NETWORK_HANDLE ));
extern int scan_network P((ENV_INFO *, SCAN_DEV_TBL *));
extern int build_dev_tbl P(( ENV_INFO *, NETWORK_HANDLE));
extern int delete_dev_tbl P(( ENV_INFO *, NETWORK_HANDLE));
extern int find_block P((char *,NETWORK_HANDLE,FIND_CNF_A **, ENV_INFO *));
extern int initiate_comm P((DEVICE_HANDLE,CREF *,CREF *,CREF *, ENV_INFO *));
extern int terminate_comm P((DEVICE_HANDLE,ENV_INFO *));

extern int comm_get P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT **, ENV_INFO *));
extern int comm_put P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT *, ENV_INFO *));
extern int comm_read P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
		CM_VALUE_LIST *, ENV_INFO *));
extern int comm_write P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
		CM_VALUE_LIST *, ENV_INFO *));
extern int comm_upload P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, UINT8 *, ENV_INFO *));

extern int param_read P((ENV_INFO *, int, SUBINDEX)) ;
extern int param_write P((ENV_INFO *, int, SUBINDEX)) ;
extern int param_load P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *)) ;
extern int param_unload P((ENV_INFO *,int, SUBINDEX, COMM_VALUE_LIST *)) ;
extern void free_value_list P((CM_VALUE_LIST *)) ;
extern int type_to_ddl_type P((CM_VALUE_LIST *, COMM_VALUE_LIST *)) ;
extern int type_from_ddl_type P((COMM_VALUE_LIST *, CM_VALUE_LIST *)) ;
extern int offline_init P((NETWORK_HANDLE)); //LNT

/*
 * System management functionalities
 */
extern int sm_identify P((ENV_INFO* , unsigned short, CR_SM_IDENTIFY_CNF* ));
extern int sm_clr_node P((ENV_INFO* , unsigned short,
                         CR_SM_IDENTIFY_CNF* ));
extern int sm_set_node P((ENV_INFO* , unsigned short,
                         char* ));
extern int sm_clr_tag P((ENV_INFO* , unsigned short,
                         CR_SM_IDENTIFY_CNF* ));
extern int sm_assign_tag P((ENV_INFO* , unsigned short,
                         CR_SM_IDENTIFY_CNF* ));

/*
 * Network Management Functions
 */
extern int nm_wr_local_vcr P((ENV_INFO* ,NETWORK_HANDLE, 
                             CM_NMA_VCR_LOAD* ));
extern int nm_fms_initiate P((ENV_INFO* ,NETWORK_HANDLE, 
                             CM_NMA_VCR_LOAD* ));
extern int nm_get_od P((ENV_INFO* ,NETWORK_HANDLE,
                       CM_NMA_VCR_LOAD* ));
extern int nm_read P((ENV_INFO* , NETWORK_HANDLE,
                       unsigned short, int, int ));
extern int nm_dev_boot P((ENV_INFO* , NETWORK_HANDLE, unsigned short, int));
extern int cm_exit P((ENV_INFO*, NET_TYPE ));
extern int cm_firmware_download P((ENV_INFO*, char * ));
extern int cm_get_host_info P((ENV_INFO*, HOST_INFO * ));
extern int cm_reset_fbk P((ENV_INFO*, NETWORK_HANDLE ));
extern int cm_set_appl_state P((ENV_INFO *, int *, int));
extern int cm_get_actual_index(unsigned short *, int *, unsigned short *);
extern int cm_dds_bypass_read(ENV_INFO *, unsigned short, int,
		unsigned short, void *);
extern int cm_abort_cr_fn P((ENV_INFO *env_info, unsigned short, unsigned short));


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DDS_COMM_H */
