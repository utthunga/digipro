/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *  @(#)int_wrap.h	30.3  30  14 Nov 1996
 *
 *  Description : Function prototypes for wrapper functions. 
 *
 ******************************************************************************/

#ifndef INT_WRAP_H
#define INT_WRAP_H

#ifdef __cplusplus
	extern "C" {
#endif /* __cplusplus */

extern VALUE r_mhd(SYM *params, ENVIRON *env_ptr);
extern VALUE r_putchar(SYM *params, ENVIRON *env_ptr);
extern VALUE r_puts(SYM *params, ENVIRON *env_ptr);

extern VALUE wrap_ret_unsigned_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_signed_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_sel_double (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_float_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_double_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_menu_display (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_list_create_element (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_list_create_element2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_list_delete_element (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_list_delete_element2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_debug_line (SYM *params, ENVIRON *env_ptr);

extern VALUE wrap_add_abort_method (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_remove_abort_method (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_remove_all_abort_methods (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_method_abort (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_discard_on_exit (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_send_on_exit (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_save_on_exit (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_date_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_double_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_float_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_signed_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_string_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_unsigned_lelem (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_sel_float (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_sel_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_float_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_float_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_double_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_double_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_signed_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_signed_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_unsigned_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_unsigned_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_string_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_string_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_date_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_date_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_assign (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_read_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_send_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_send_all_values (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_response_code (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_comm_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_abort_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_abort_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_abort_on_comm_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_abort_on_response_code (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_fail_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_fail_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_fail_on_comm_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_fail_on_response_code (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_retry_on_all_comm_errors (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_retry_on_all_response_codes (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_retry_on_comm_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_retry_on_response_code (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_status_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_comm_error_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_response_code_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_builtin_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_message (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_dynamics (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_comm_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_response_code (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_delayfor (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_edit_device_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_edit_local_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_select_from_menu (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_acknowledgement (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_is_NaN (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_NaN_value (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_stddict_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_block_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_local_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_param_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_param_list_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_array_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_list_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_record_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_selector_ref (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_resolve_status (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_dds_error (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_float (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_float (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_double (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_double (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_signed (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_signed (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_unsigned (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_unsigned (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_string (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_date (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_date (SYM *params, ENVIRON *env_ptr);

/* FF-900 5.1 Additions Wrappers */
extern VALUE wrap_assign2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_delayfor2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_dynamics2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_display_message2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_edit_device_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_edit_local_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_acknowledgement2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_block_instance_by_object_index (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_block_instance_by_tag (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_block_instance_count (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_date_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_double_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_float_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_signed_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_string_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_get_unsigned_value2 (SYM *params, ENVIRON *env_ptr);

extern VALUE wrap_ret_double_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_float_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_signed_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_ret_unsigned_value2 (SYM *params, ENVIRON *env_ptr);

extern VALUE wrap_put_date_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_double_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_float_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_signed_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_string_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_put_unsigned_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_read_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_select_from_menu2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_send_value2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_local_ref2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_param_ref2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_array_ref2 (SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_resolve_record_ref2 (SYM *params, ENVIRON *env_ptr);


/*
 * These next two functions aren't used anywhere but the
 * declarations are included for the sake of completeness.
 */
extern VALUE wrap_bi_rcsim_set_response_code(SYM *params, ENVIRON *env_ptr);
extern VALUE wrap_bi_rcsim_set_comm_err(SYM *params, ENVIRON *env_ptr);

#ifdef __cplusplus
	}
#endif /* __cplusplus */

#endif /* INT_WRAP_H */

