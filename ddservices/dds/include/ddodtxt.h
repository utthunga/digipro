/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)ddodtxt.h	30.3  30  14 Nov 1996
 *	This file contains all of the definitions pertaining to the DDOD 
 *	(Device Description Object Dictionary).
 *
 */

#ifndef	DDODTXT_H
#define	DDODTXT_H

#include "attrs.h"
#include "table.h"
#include "sysproto.h"

/*
 *	Error Definitions 
 */

#define	FILE_OPEN_ERROR				   1
#define	FILE_CLOSE_ERROR			   2
#define	FILE_READ_ERROR				   3
#define	FILE_WRITE_ERROR			   4
#define	MEMORY_ALLOCATION_ERROR		   5

#define	OBJECT_NOT_FOUND			   6

#define	DDODTXT_ERROR				   7

/* Value used for an absent block directory object */

#define	ABSENT_BLOCK_DIRECTORY	  0xFFFF

/*
 *	Typedefs
 */

typedef struct {
	OBJECT_INDEX		 object_index;
	unsigned long		 local_address;
	BININFO				 extension;
} DDODTXT_OBJECT;

typedef struct {
	unsigned short		 object_count;
	DDODTXT_OBJECT		*object_list;
	BININFO				 data;
} DDODTXT_ROD;

typedef struct {
	BININFO				 table_list[MAX_DEVICE_TBL_ID];
} DDODTXT_DEV_DIR;

typedef struct {
	ITEM_ID				 block_id;
	int					 exists;
	BININFO				 table_list[MAX_BLOCK_TBL_ID];
} DDODTXT_BLOCK_DIR;

typedef struct {
	int					 count;
	DDODTXT_BLOCK_DIR	*list;
} DDODTXT_BLOCK_DIRS;

typedef struct {
	unsigned char		 tag;
	BININFO				 data;	
} DDODTXT_AL_ELEMENT;

typedef struct {
	ITEM_TYPE			 item_type;
	ITEM_TYPE			 item_subtype;
	ITEM_ID				 item_id;
	unsigned long		 attr_mask;
	unsigned short		 attr_count;
	DDODTXT_AL_ELEMENT	*attr_list;
} DDODTXT_ITEM;

typedef struct {
	int					 count;
	DDODTXT_ITEM		*list;
} DDODTXT_ITEMS;

typedef struct {
	int					 count;
	OBJECT_INDEX		*list;
} NON_ITEM_TABLE;


/*
 *	Function Prototypes
 */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void				 handle_error P((int));
extern unsigned long	 get_value P((unsigned char *,unsigned short));
extern unsigned char	*get_var_length_int P((unsigned char *,
	unsigned long *));
extern unsigned char	*get_attr_id P((unsigned char *,unsigned char *,
	unsigned long *, unsigned char *));
extern void				 print_out_bin_data P((unsigned char *,
	unsigned long,FILE *));
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

