/**
 *		Device Description Services Rel. 3.0
 *		Copyright 1995 - Fieldbus Foundation
 *		All rights reserved.
 */

//
//	@(#)int_slp.h	21.1  21  20 Oct 1995
//
//  Description : Sleep function for 16-bit Windows.
// ----------------------------------------------------------------------------

#ifndef INT_SLP_H
#define INT_SLP_H

#ifdef _MSC_VER

typedef		unsigned long	ULONG ;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void sleep( ULONG mSecs );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MSC_VER */

#endif /* INT_SLP_H */

