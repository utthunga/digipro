/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/**
 *	@(#)l7_fms.h	30.3  30  14 Nov 1996
 */

#ifndef L7_FMS_H
#define L7_FMS_H

#include "env_info.h"
#include "ddldefs.h"
#include "comm.h"
#include "flats.h"

#define OSC_ELEMENT_LIMIT    10

/* 
 *	These are the "statuses" associated with a value+status record.
 *	This is only a temporary holding place for these macros.
 */

#define STATUS_BAD			12
#define NOT_FROM_PROCESS	13

/* end of temporary DDS stuff */

typedef struct {
    int				dv_member;	/* this item is a member of a pv+status record */
    unsigned short	kind;		/* DV, TV, AO, ALL */
    unsigned short	which;
    unsigned short	oclass;		/* MANUAL|AUTO & GOOD|BAD */
} ADDITIONAL_STATUS;
 
typedef struct {
    unsigned short		count;
    ADDITIONAL_STATUS	list[24];
} ADDITIONAL_STATUS_LIST;
 
typedef struct {
    ITEM_ID			id;		/* id of the reply item, may not be needed */
    unsigned short	index;	/* index of the reply item */
    unsigned short	kind;	/* DV, TV, AO, ALL; only DV is currently supported */
    unsigned short	which;
}   OSC_ELEMENT;    /* List of items in current cmd_desc which are members of */
                    /* valu+status record */
 
typedef struct {
    unsigned short	count;
    OSC_ELEMENT		list[OSC_ELEMENT_LIMIT];
}   ACTIVE_OSC_LIST;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int ps_read_param_request P(( ENV_INFO * , P_REF * ));
 
extern int ps_write_param_request P(( ENV_INFO *, P_REF *));

extern void l7_get_additional_status P(( ENV_INFO *, FLAT_COMMAND *, unsigned long, 
	FLAT_COMMAND *, ACTIVE_OSC_LIST *, ADDITIONAL_STATUS_LIST *, unsigned char ));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif	/* L7_FMS_H */
