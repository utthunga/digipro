/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)dict.h	30.3	14 Nov 1996
 */


#ifndef DICT_H
#define DICT_H


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * Name of program for dict.c.
 */

extern char *dict_program;

extern char *strip P((char *s));
extern int makedict P((char *, void (*func)(), int));
void setlanguage(const char *);
void removeDict (void (*func)());
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DICT_H */
