/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/**
 *	@(#)ddssetup.h	30.3  30  14 Nov 1996
 *
 *	ddssetup.h
 */

#ifndef DDSSETUP_H
#define DDSSETUP_H

#include "cm_lib.h"
#include "host.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct {
	int		(*initialize_fn_ptr) 
            P((ENV_INFO *, HOST_CONFIG *));
	int		(*config_stack_fn_ptr) 
            P((ENV_INFO *, CM_NMA_INFO *, HOST_CONFIG *));
	int		(*get_stack_config_fn_ptr) 
            P((ENV_INFO *, CM_NMA_INFO *));
	int		(*init_network_fn_ptr) P((NETWORK_HANDLE ));
	int		(*find_block_fn_ptr)
			P((char *, NETWORK_HANDLE, FIND_CNF_A **, ENV_INFO *));
	int		(*initiate_comm_fn_ptr)
			P((DEVICE_HANDLE, CREF *, CREF *, CREF *, ENV_INFO *));
	int		(*terminate_comm_fn_ptr) P((DEVICE_HANDLE, ENV_INFO *));
	int		(*comm_get_fn_ptr)
			P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT **, ENV_INFO *));
	int		(*comm_put_fn_ptr)
			P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT *, ENV_INFO *));
	int		(*comm_read_fn_ptr)
			P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX, CM_VALUE_LIST *,
			ENV_INFO *));
	int		(*comm_write_fn_ptr)
			P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX, CM_VALUE_LIST *,
			ENV_INFO *));
	int		(*comm_upload_fn_ptr)
			P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, UINT8 *, ENV_INFO *));
	int		(*param_read_fn_ptr) P((ENV_INFO *, int, SUBINDEX));
	int		(*param_write_fn_ptr) P((ENV_INFO *, int, SUBINDEX));
	int		(*param_load_fn_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *));
	int		(*param_unload_fn_ptr) P((ENV_INFO *, int, SUBINDEX, COMM_VALUE_LIST *));
	int		(*scan_network_fn_ptr) P((ENV_INFO *, SCAN_DEV_TBL *));
    int		(*build_dev_tbl_fn_ptr) P(( ENV_INFO *, NETWORK_HANDLE));
    int		(*delete_dev_tbl_fn_ptr) P(( ENV_INFO *, NETWORK_HANDLE));
	int		(*sm_identify_fn_ptr) P((ENV_INFO *, unsigned short, 
			CR_SM_IDENTIFY_CNF* ));
	int     (*offline_init_fn_ptr) P((NETWORK_HANDLE));
    int     (*sm_clr_node_fn_ptr) P((ENV_INFO *, unsigned short,
            CR_SM_IDENTIFY_CNF* ));
    int     (*sm_set_node_fn_ptr) P((ENV_INFO *, unsigned short,
            char *));
    int     (*sm_clr_tag_fn_ptr) P((ENV_INFO *, unsigned short,
            CR_SM_IDENTIFY_CNF* ));
    int     (*sm_assign_tag_fn_ptr) P((ENV_INFO *, unsigned short,
            CR_SM_IDENTIFY_CNF* ));
    // Network related functions
    int     (*nm_wr_local_vcr_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD*));
    int     (*nm_fms_initiate_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD*));
    int     (*nm_get_od_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, CM_NMA_VCR_LOAD*));
    int     (*nm_read_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, unsigned short, int, int));
    int     (*nm_dev_boot_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, unsigned short, int));
    int     (*cm_exit_fn_ptr) P((ENV_INFO *, NET_TYPE ));
    int     (*cm_firmware_download_fn_ptr) P((ENV_INFO *, char * ));
    int     (*cm_get_host_info_fn_ptr) P((ENV_INFO *, HOST_INFO *, HOST_CONFIG * ));
    int     (*cm_reset_fbk_fn_ptr) P((ENV_INFO *, NETWORK_HANDLE, HOST_CONFIG *));
    int 	(*cm_set_appl_state_fn_ptr) P((ENV_INFO *, int *, int));
    int     (*cm_get_actual_index_fn_ptr) P((unsigned short *, int *, unsigned short *));
    int		(*cm_dds_bypass_read_fn_ptr) P((ENV_INFO *, unsigned short , int ,
    		unsigned short, void * ));
    int     (*cm_abort_cr_fn_ptr) P((ENV_INFO *, unsigned short, unsigned short ));

} CSTK_A_FN_PTRS;

extern void connect_comm_stack_a P(());
extern void disconnect_comm_stack_a P(());
extern void cr_init_cstk_a_fn_ptrs P((const CSTK_A_FN_PTRS *));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif	/* DDSSETUP_H */

