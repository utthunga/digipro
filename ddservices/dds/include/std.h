/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)std.h	30.3  30  14 Nov 1996
 */

#ifndef STD_H
#define STD_H

/*
 *	Allow "prototyping" for reference, but use K&R syntax for code
 */

#ifdef __cplusplus
	extern "C" {
#endif


#define P(x)	x

/*
 * Define platform specific parameters
 */

#if defined(SUN) || defined(SVR4)
#define	DDS_BIG_ENDIAN	1	/* Uses DDS_BIG_ENDIAN byte order */
#undef DDS_LITTLE_ENDIAN
#endif				/* SUN or SVR4*/

#if defined(SUN)

#ifndef glint
#define const			/* Not a keyword on the SUN */
#define volatile		/* Not a keyword on the SUN */
#endif /* glint */

#define strtoul(B, L, R)	((unsigned long) strtol((B), (L), (R)))
extern long strtol();

#endif		/* SUN */

/*
 *	For use with qsort() and bsearch()
 */

typedef int (*CMP_FN_PTR) P((const void *, const void *));

#if defined(MSDOS) || defined(WIN32) || defined(_MSC_VER) || defined(__linux__)
#ifndef DDS_LITTLE_ENDIAN
#define	DDS_LITTLE_ENDIAN	1	/* Uses DDS_LITTLE_ENDIAN byte order */
#endif
#undef DDS_BIG_ENDIAN 
#endif /* MSDOS or WIN32 or _MSC_VER */


typedef enum { FMS_FALSE, FMS_TRUE } fms_boolean;
extern const char *getFilePart(const char *fullPath);


/*
 *  All purpose fatal_msg() and warn_msg() messages.
 */



#define REQUIRE(cond) \
		/*lint -save -e506*/ \
		extern int __require[(cond) ? 1 : -1] \
		/*lint -restore*/

#define ASSERT_ALL(cond) \
		if(!(cond)) panic("%s(%d) - Condition not true\n", getFilePart(__FILE__), __LINE__)

#define CRASH_ALL() \
		panic("%s(%d) - Intentional termination\n", getFilePart(__FILE__), __LINE__) /* NOTREACHED */

#define insist(x) { if (!(x)) (*(int*) 1) = 0; }

#define ASSERT_RET(cond,param) \
		if(!(cond)) panic_but_return("%s(%d) - Condition not true: passed value = %d\n", \
			getFilePart(__FILE__), __LINE__, (param)); 
#if 0            
#define ASSERT_RET(cond,param) \
		if(!(cond)) { panic_but_return("%s(%d) - Condition not true: passed value = %d\n", \
			getFilePart(__FILE__), __LINE__, (param)); \
            return(param); }
#endif       

#define CRASH_RET(param) \
		panic("%s(%d) - Termination on return: passed value = %d\n", getFilePart(__FILE__), __LINE__, (param)); \

#define CRASH_BUT_RET_CODE(param) \
		panic_but_return("%s(%d) - Termination on return: passed value = %d\n", getFilePart(__FILE__), __LINE__, (param)); \
        return (param)

#define ASSERT_DBG(cond)	ASSERT_ALL(cond)

#ifdef DEBUG
#define CRASH_DBG() CRASH_ALL()
#else
#define CRASH_DBG() printf("PANIC: Intentional termination bypassed\n");
#endif


#ifdef DEBUG

#if defined WIN32 || defined __WIN32__
__declspec(dllimport) void __stdcall DebugBreak( void);
#endif /* WIN32 or __WIN32__ */




#else




#endif		/* DEBUG */


/*
 *	Standard defines
 */
#undef FALSE
#define FALSE 0
#undef TRUE
#define TRUE 1

#ifndef max
//#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
//#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

/*
 *	For DDSTEST on the PC, if standard out exists (and it should), give
 *	standard error the same definition, so that messages and errors go
 *	to the same file in the proper (i.e., chronological) order.
 */

#if (defined(_WINDOWS) || defined(_MSC_VER) || defined(__linux__)) && defined(DDSTEST)
#ifdef stdout
#ifdef stderr
#undef stderr
#endif /* stderr */
#define stderr stdout
#endif /* stdout */
#endif /* (defined(_WINDOWS) || defined(_MSC_VER)) && defined(DDSTEST) */

/*
 *	Standard typedefs
 */

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned long	ulong;

#if (defined (_WINDOWS) || defined(__linux__))
        //typedef signed char INT8;
	typedef unsigned char UINT8;
	typedef signed short INT16;
	typedef unsigned short UINT16;
	typedef signed int INT32;
	typedef unsigned int UINT32;
#else
	typedef char INT8;
	typedef unsigned char UINT8;
	typedef short INT16;
	typedef unsigned short UINT16;
	typedef long INT32;
	typedef unsigned long UINT32;
#endif

typedef ulong DDITEM;

struct BLOCK;

#ifdef __cplusplus
	}
#endif /* __cplusplus */

#include "panic.h"

#endif				/* STD_H */
