/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *
 *	@(#)ddi_lib.h	30.3  30  14 Nov 1996
 */
 
#ifndef DDI_LIBR_H
#define DDI_LIBR_H

#include "std.h"
#include "table.h"
#include "attrs.h"
#include "evl_lib.h"
#include "env_info.h"

/*
 *	The DDS Revision number is a 3-part id defined as follows:
 *	
 *	<major ddod revision>.<minor ddod revision>.<software revision>
 *
 *	The major and minor revisions are shared with the Tokenizer
 *	and are found in ddldefs.h
 */
 
/* This is the software Revision of the DDS Library */

#define DDS_REVISION_NUMBER		1


/*
 *  DDI interface structures
 */

typedef struct {
	DD_HANDLE		handle;
	DD_REFERENCE	ref;
}		DIR_SPECIFIER_REFERENCE;

typedef struct {
	DEVICE_TYPE_HANDLE	dt_handle;
	ITEM_ID				blockname;
}		DIR_SPECIFIER_BLOCKNAME;

typedef struct {
	OP_REF				op_ref;
	DESC_REF			desc_ref;
}		OP_DESC;

typedef struct {
	int					count;
	OP_DESC				*list;
}		OP_DESC_LIST;


/*
 *	DDI defines for calling ddi_block_dir_request() and
 *	ddi_device_dir_request().
 */

#define	BD_BLOCK_HANDLE		1	/* BLOCK DIR request by BLOCK_HANDLE */
#define BD_DD_HANDLE		2	/* BLOCK_DIR request by DD_REFERENCE, DD_HANDLE */
#define BD_BLOCKNAME		3	/* BLOCK_DIR reauest by DEVICE_TYPE_HANDLE_BLOCKNAME */
#define	DD_DEVICE_HANDLE	4	/* DEVICE DIR request by DEVICE_HANDLE */
#define DD_DD_HANDLE		5	/* DEVICE_DIR request by DD_REFERENCE, DD_HANDLE */
#define	DD_DT_HANDLE		6	/* DEVICE_DIR request by DEVICE_TYPE_HANDLE */

typedef struct {
	unsigned short			type;			/* See defines above */
	unsigned long 			mask;			/* bit enum: */
	union {
		DEVICE_TYPE_HANDLE				device_type_handle;
		DIR_SPECIFIER_REFERENCE			ref;
		DEVICE_HANDLE					device_handle;
	} 			spec;
}	DDI_DEVICE_DIR_REQUEST;

typedef struct {
	unsigned short			type;			/* See defines above */
	unsigned long 			mask;			/* bit enum: */
	union {
		DIR_SPECIFIER_REFERENCE			ref;
		BLOCK_HANDLE					block_handle;
		DIR_SPECIFIER_BLOCKNAME			block;
	}		spec;
}	DDI_BLOCK_DIR_REQUEST;


/*
 * These are the possible types of item specifier for calling ddi_get_item().
 */

/* DD Item types without subindex */

#define	DDI_ITEM_BLOCK              0
#define	DDI_ITEM_ID                 1
#define	DDI_ITEM_NAME               2
#define	DDI_ITEM_OP_INDEX           3
#define	DDI_ITEM_PARAM              4
#define	DDI_ITEM_PARAM_LIST         5
#define	DDI_ITEM_CHARACTERISTICS    6


/* DD Item types with subindex */

#define	DDI_ITEM_ID_SI              7
#define	DDI_ITEM_NAME_SI			8
#define	DDI_ITEM_OP_INDEX_SI        9
#define	DDI_ITEM_PARAM_SI          10


typedef struct {
	int			type;				/* see defines above */
	SUBINDEX	subindex;

	union {
		ITEM_ID			id;
		ITEM_ID			name;
		OBJECT_INDEX	op_index;
		int				param;
		int				param_list;
		SUBINDEX		characteristics;

	}	item;

} DDI_ITEM_SPECIFIER;

/*
 *	Typedef for Item Attribute Request plus a define for getting
 *	every Item Attribute in one request
 */

#define ALL_ITEM_ATTR_MASK   0xFFFFFFFF

typedef unsigned long DDI_ATTR_REQUEST;


/*
 * This is the Generic DDI Item
 */

typedef struct {

	ITEM_TYPE	item_type;   /* the type of item */
	void  		*item;       /* a pointer to the item */
	RETURN_LIST errors;      /* list of errors encountered 
							  * during the last evaluation */
} DDI_GENERIC_ITEM;


/*
 *	DDI convenience Parameter Specifier.  This structure is used to specify
 *	a DDL parameter and it contains every meaningful way of describing a
 *	parameter to DDS.
 */

/* parameter specifier types */

#define	DDI_PS_ITEM_ID          0  /* specify parameter with item ID */
#define	DDI_PS_OP_INDEX	        1  /* specify parameter with operational index */
#define	DDI_PS_PARAM_NAME       2  /* specify parameter with parameter name */
#define	DDI_PS_PARAM_OFFSET     3  /* specify parameter with parameter offset */

/* Parameter specifier with subindex */
/* for when the parameter is a member of an ARRAY or RECORD */

#define	DDI_PS_ITEM_ID_SI			4 /* spec param with item ID and subindex */
#define	DDI_PS_OP_INDEX_SI			5 /* spec param with operational index and subindex */
#define	DDI_PS_PARAM_NAME_SI		6 /* spec param with parameter name and subindex */
#define	DDI_PS_PARAM_OFFSET_SI		7 /* spec param with parameter offset and subindex */
#define	DDI_PS_CHARACTERISTICS_SI	8 /* spec param with subindex of the characteristics record */


typedef struct {
	int			type;			/* see defines above */
	SUBINDEX	subindex;		

	union {
		ITEM_ID			id;
		ITEM_ID			name;
		OBJECT_INDEX	op_index;
		int				param;

	}	item;

} DDI_PARAM_SPECIFIER;


/*
 * These are the possible types of DDI_BLOCK_SPECIFIER
 */

#define DDI_BLOCK_TAG      0
#define DDI_BLOCK_HANDLE   1

typedef char *BLOCK_TAG;

typedef struct {
	int			type;				/* see defines above */

	union {
		BLOCK_TAG		tag;
		BLOCK_HANDLE	handle;

	}	block;

} DDI_BLOCK_SPECIFIER;


#define DDI_ITEM_ID_PARAM_NAME				0
#define	DDI_ITEM_ID_PARAM_LIST_MEMBER		1
#define DDI_ITEM_ID_CHARACTERISTIC			2
#define	DDI_ITEM_ID_PARAM_LIST_ID_MEMBER	3


typedef struct {
	int			type;				/* see defines above */
	ITEM_ID		param_name_or_id;
	ITEM_ID		member_name;

} DDI_ITEM_ID_REQUEST;


/*
 * defines for the command request type
 */

#define DDI_READ_COMMAND    1
#define DDI_WRITE_COMMAND   2



/*
 * Structure used with ddi_get_param_info()
 */

typedef struct {
  ITEM_TYPE	item_type;
  ITEM_ID       item_id; 
  ITEM_ID       param_name;
  OBJECT_INDEX	op_index;
  int           param_offset;

} DDI_PARAM_INFO;


typedef enum { ENCODING_UNKNOWN, ISO_LATIN_1, UTF_8 } DDI_STRING_ENCODING;


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 *	DDI routines called by the Connection Manager.
 */

extern int ddi_build_dd_dev_tbls		P((DEVICE_TYPE_HANDLE));
extern int ddi_remove_dd_dev_tbls		P((DEVICE_TYPE_HANDLE));
extern int ddi_build_dd_blk_tbls		P((BLOCK_HANDLE));
extern int ddi_remove_dd_blk_tbls		P((BLOCK_HANDLE));

/*
 *	DDI_LOAD_TABLES interface routines
 */

extern int ddi_load_device_tables P(( DIR_SPECIFIER_REFERENCE *, FLAT_DEVICE_DIR * ));
extern int ddi_load_block_tables  P(( DIR_SPECIFIER_REFERENCE *, FLAT_BLOCK_DIR * ));

/*
 *	DDI_GET_DIRECTORY interface routines
 */

extern int ddi_device_dir_request P(( DDI_DEVICE_DIR_REQUEST *, FLAT_DEVICE_DIR *));
extern int ddi_block_dir_request  P(( DDI_BLOCK_DIR_REQUEST *, FLAT_BLOCK_DIR * ));

/*
 *	DDI_DESTRUCT interface routines
 */

extern void ddi_clean_device_dir P(( FLAT_DEVICE_DIR * ));
extern void ddi_clean_block_dir  P(( FLAT_BLOCK_DIR * ));

/*
 *	DDI item interface functions
 */

extern int ddi_get_item P((DDI_BLOCK_SPECIFIER *, DDI_ITEM_SPECIFIER *,
						ENV_INFO *, DDI_ATTR_REQUEST, DDI_GENERIC_ITEM *));
extern int ddi_get_item2 P((DDI_ITEM_SPECIFIER *item_spec,
	                    ENV_INFO2 *env_info2, DDI_ATTR_REQUEST req_mask, 
                        DDI_GENERIC_ITEM *generic_item));

extern int ddi_clean_item P((DDI_GENERIC_ITEM *));

/*
 * DDI convenience
 */

extern int ddi_check_enum_var_value P((FLAT_VAR *, unsigned long *));

extern int ddi_get_type2(ENV_INFO2 *env_info2, DDI_ITEM_SPECIFIER *item_spec,
	                     ITEM_TYPE *item_type);
extern int ddi_get_type P((DDI_BLOCK_SPECIFIER *, DDI_ITEM_SPECIFIER *, ITEM_TYPE *));

extern int ddi_get_param_info P((DDI_BLOCK_SPECIFIER *,
								DDI_PARAM_SPECIFIER *, DDI_PARAM_INFO *));

extern int ddi_get_unit P((DDI_BLOCK_SPECIFIER *,
							DDI_PARAM_SPECIFIER *, ITEM_ID *));

extern int ddi_get_wao P((DDI_BLOCK_SPECIFIER 	*,
						   DDI_PARAM_SPECIFIER *, ITEM_ID *));

extern int ddi_get_update_items P((DDI_BLOCK_SPECIFIER *,
								DDI_PARAM_SPECIFIER *, OP_DESC_LIST *));

extern int ddi_get_var_type P((DDI_BLOCK_SPECIFIER *,
							   DDI_PARAM_SPECIFIER *, TYPE_SIZE *));

extern int ddi_get_item_id P((DDI_BLOCK_SPECIFIER *,
							  DDI_ITEM_ID_REQUEST *, ITEM_ID *));

extern int ddi_get_subindex P((DDI_BLOCK_SPECIFIER *, ITEM_ID,
							   ITEM_ID, SUBINDEX *));

extern void ddi_clean_update_item_list P((OP_DESC_LIST *));

extern int ddi_find_unit_ids(DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID id_to_find,
	ITEM_ID *item_id_of_unit_rel, ITEM_ID *item_id_of_unit_var);

extern int params_are_conditional(ENV_INFO2 *env, ITEM_ID block_id, int *has_conditionals);

extern int locate_symbol_id(ENV_INFO2 *env, char *sym_name, ITEM_ID *out_id);

/*
 * DDI language
 */
extern char g_dds_language_code[];

extern int ddi_iso_to_pc P((char *, char *, int));
extern int ddi_pc_to_iso P((char *, char *, int));
extern int ddi_get_string_translation P((const char *, char *, char *, int));
extern int ddi_set_language_code(char *new_code);
extern int ddi_get_string_encoding(ENV_INFO2 *env_info2, DDI_STRING_ENCODING *enc);
extern int ddi_iso_to_utf8(char * pszIso, char * pszOut, size_t sizeOut);
extern int ddi_max_utf8_size(char *pszIso);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DDI_LIBR_H */
