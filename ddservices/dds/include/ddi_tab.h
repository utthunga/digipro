/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)ddi_tab.h	30.3  30  14 Nov 1996
 */

#ifndef DDI_TAB_H
#define DDI_TAB_H

#include "cm_lib.h"
#include "ddi_lib.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int compare_bt_elem P((BLK_TBL_ELEM *,BLK_TBL_ELEM *));

extern int conv_block_spec P((const DDI_BLOCK_SPECIFIER *,
                              BLOCK_HANDLE *));

/*	
 * 	function prototypes needed for conversion to DD reference 
 */

extern int	tr_gdr_block_handle	P((BLOCK_HANDLE,DD_REFERENCE *)) ;

extern int 	tr_gdr_device_type_handle_blockname P((DEVICE_TYPE_HANDLE, ITEM_ID,
	DD_REFERENCE * )) ;

extern int	tr_block_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, ITEM_TBL_ELEM **)) ;

extern int	tr_id_to_ite P((ITEM_TBL *, ITEM_ID, ITEM_TBL_ELEM **)) ;

extern int	tr_name_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, ITEM_ID, ITEM_TBL_ELEM **)) ;

extern int	tr_param_offset_to_op_index P((BLOCK_HANDLE,int,OBJECT_INDEX *));

extern int	tr_op_index_to_ite P((BLOCK_HANDLE, ITEM_TBL *, BLK_TBL_ELEM *,OBJECT_INDEX,
	ITEM_TBL_ELEM **)) ;

extern int	tr_param_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *,int, ITEM_TBL_ELEM **)) ;

extern int	tr_param_list_to_ite P((ITEM_TBL *,BLK_TBL_ELEM *,int, ITEM_TBL_ELEM **)) ;

extern int	tr_name_si_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *,ITEM_ID,SUBINDEX,
	ITEM_TBL_ELEM **)) ;

extern int	tr_op_index_si_to_ite P((BLOCK_HANDLE,ITEM_TBL *,BLK_TBL_ELEM *,OBJECT_INDEX,
	SUBINDEX, ITEM_TBL_ELEM **));

/*	
 * 	function prototypes needed for conversion to type and size 
 */
extern int	tr_param_to_size P((BLOCK_HANDLE,BLK_TBL_ELEM *,int,TYPE_SIZE *)) ;
extern int	tr_param_si_to_size P((BLOCK_HANDLE,BLK_TBL_ELEM *,int,SUBINDEX,TYPE_SIZE *)) ;
extern int	tr_char_to_size P((BLK_TBL_ELEM *,SUBINDEX,TYPE_SIZE *)) ;

extern int	tr_block_tag_to_block_handle P((BLOCK_TAG, BLOCK_HANDLE *));

/*
 * Prototypes needed for Relation Conveniences
 */

extern int	tr_id_to_bint_offset P((BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_name_to_bint_offset P((BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_op_index_to_bint_offset P((BLOCK_HANDLE, BLK_TBL_ELEM *,OBJECT_INDEX, int *)) ;
extern int	tr_op_index_si_to_bint_offset P((BLOCK_HANDLE, BLK_TBL_ELEM *,OBJECT_INDEX, int *,SUBINDEX)) ;
extern int	tr_param_to_bint_offset P((BLK_TBL_ELEM *,int, int *)) ;
extern int	tr_char_to_bint_offset P((BLK_TBL_ELEM *, int *)) ;

extern int	tr_id_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,ITEM_ID, int *)) ;
extern int	tr_name_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,ITEM_ID,int *)) ;
extern int	tr_param_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,int, int *)) ;
extern int	tr_bint_offset_to_rt_offset P((BLK_TBL_ELEM *,int,int *)) ;
extern int	tr_bint_offset_si_to_rt_offset P((BLK_TBL_ELEM *, SUBINDEX, int, int *)) ;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif		/* DDI_TAB_H */

