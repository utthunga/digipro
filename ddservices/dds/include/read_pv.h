#ifndef READ_PV_H
#define READ_PV_H

#include "ret_info.h"
#include "pv_tbl.h"

typedef union {
	unsigned char   c[4];
	float           f;
	double          d;
	unsigned long   u;
	long            i;
}VAR_ABS_VALUE;



int read_process_variables(RET_INFO *, char * );
int init_pv_tbl(void);
int set_pv_count(void);
void set_pv_block_tag(const char *block_tag);
ProcessValTable *get_pv_tbl_index(void);
void chk_update_duplicate_PV(char *);

#endif
