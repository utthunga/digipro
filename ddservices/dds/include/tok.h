/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
/*
 *	@(#)tok.h	30.3  30  14 Nov 1996
 */

#ifndef TOK_H
#define TOK_H 1

#include	<stdio.h>

/* 
 * Defines 
 */

#define HASHSIZE 1009

#define streq(S, T) (!strcmp((S), (T)))

#ifndef SEEK_SET
#define SEEK_SET 0
#endif

#define MAXBUF		1000	/* Maximum buffer size          */
#ifndef TRUE
#define TRUE	1			/* Boolean value                */
#define FALSE	0			/* Boolean value                */
#endif

/* Global tree tables */
#define TABLECHUNKSZ   1000 /* Reallocation size for table  */

/* Defines for the PC */
#ifndef F_OK
#define	F_OK	0
#endif
#ifndef X_OK
#define	X_OK	0x1
#endif
#ifndef W_OK
#define	W_OK	0x2
#endif
#ifndef R_OK
#define	R_OK	0x4
#endif

/* Defines for References */
#define TOK_S_REF 501	/* Symbol */
#define TOK_C_REF 502	/* Collection */
#define TOK_A_REF 503	/* Array */
#define TOK_P_REF 504	/* Parameter */
#define TOK_L_REF 505	/* Parameter List */
#define TOK_B_REF 506	/* Block */
#define TOK_E_REF 507   /* sElector reference */
#define TOK_F_REF 508   /* list reFerence */
#define TOK_O_REF 509   /* lOcal reference */
/* DDS 5.1 introduced the concept of a list of members
 * of types CHART, FILE, GRAPH, GRID, MENU, and METHOD for each
 * block.
 * The following are for referring to these member lists of 
 * a block.
 */
#define TOK_BCHART_REF  510 /* Block->Chart reference */
#define TOK_BFILE_REF   511 /* Block->File reference */
#define TOK_BGRAPH_REF  512 /* Block->Graph reference */
#define TOK_BGRID_REF   513 /* Block->Grid reference */
#define TOK_BMENU_REF   514 /* Block->Menu reference */
#define TOK_BMETHOD_REF 515 /* Block->Method reference */

#define TOK_METH_XREF   516 /* Inside-method crossblock reference */

/* Strings for method definition fixups */
#define FIXUP_BLOCK_MEMBER_TYPE  "$$FBxxxxx"
#define FIXUP_BLOCK_CHARACTERISTIC_TYPE  "$$FCxxxxx"
#define FIXUP_RECORD_MEMBER_TYPE "$$FRxxxxx"
#define FIXUP_ARRAY_TYPE         "$$FAxxxxx"
#define FIXUP_SELECTOR_ID        "$$FExxxxx"
#define FIXUP_LIST_ELEMENT_TYPE  "$$FLxxxxx"  /* This one gets parsed twice, hence the $$$$ */
#define FIXUP_XREF               "$$FXxxxxx"  /* Crossblock reference */

#define DD_INFO_SYMBOL "__ffddinfo"

/* 
 * Typedefs 
 */

/* 
 * Prototypes 
 */

/* parser.y */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void parse_init (void);
extern void parse_finish (void);

extern int is_dd_info_symbol(char *sym);

/* supp.c */

#if defined(__linux__)
	#define STDARGS
#endif

#ifdef STDARGS

extern void panic                (const char *, ...);
extern void panic_but_return                (const char *, ...);

#else /*STDARGS*/

extern void panic                ();
extern void panic_but_return                ();

#endif /*STDARGS*/

extern void *emalloc             (size_t);
extern void *erealloc            (void *, size_t);
extern void exit_gracefully		 (void);
extern char *fgetline            (char *, size_t *, FILE *);
extern void  copyfile            (FILE *, FILE *);
#if defined (SUN)
extern void *memmove (void *, void *, size_t);
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

extern int opterr;
extern int optind;
extern int optopt;
extern char *optarg;

extern char g_device_menu_detected;
extern unsigned char source_is_utf8; // Byte Order Mark found indicating utf-8 encoding

#endif /* TOK_H */
