/* dds_lock.h
*
*      @(#)dds_lock.h	30.3     14 Nov 1996
*/
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *      This file contains definitions for the locking mechanism.
 *
 */


#ifndef DDS_LOCK_H
#define DDS_LOCK_H

#include "std.h"

#ifdef __cplusplus
    extern "C" {
#endif

/*
 * The identifiers of the valid locks
 */
#define	PARSE_LOCK			0	/* Lock from the parser */
#define	BUILTIN_LOCK		1	/* Lock from the builtin */
#define	GET_METH_TAG_LOCK	2	/* from method handle assign */

#define	MAX_LOCKS			3

/*
 * The valid states of each lock
 */
#define UNLOCKED			0
#define	LOCKED				1

/* dds_lock.c */
extern	void	lock P((int));
extern	void	unlock P((int)) ;

#ifdef __cplusplus
    }
#endif

#endif /* DDS_LOCK_H */
