#ifndef DEBUG_H
#define DEBUG_H

#define DEBUGOUT     (void)fprintf
#define OUTFILE      outfileptr 
#define DEBUGERR     (void)fprintf
#define ERRFILE      outfileptr
#define FMSASSERT    assert


#endif //DEBUG_H 
