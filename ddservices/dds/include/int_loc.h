/**
 *              Device Description Services Rel. 4.3
 *              Copyright 1994-2002 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *		@(#)int_loc.h	30.3  30  14 Nov 1996
 *
 *		int_loc.h - globally used defines and types
 */

#ifndef INT_LOC_H
#define INT_LOC_H

#include "int_meth.h"

#ifdef __cplusplus
    extern "C" {
#endif

/*
 * The operator in expression.
 * Use in structure EXPRESS for specifying an operator in expression.
 */
#define ADD				1				/* binary-operator '+' */
#define SUB				2 				/* binary-operator '-' */
#define MULT			3 				/* binary-operator '*' */
#define DIV				4 				/* binary-operator '/' */
#define MOD				5 				/* binary-operator '%' */

#define UNARY_PLUS		6				/* unary operator '+' */
#define UNARY_MINUS		7 				/* unary operator '-' */
#define PRE_INCR		8 				/* unary operator '++' */
#define PRE_DECR		9 				/* unary operator '--' */
#define POST_INCR		10 				/* unary operator '++' */
#define POST_DECR		11 				/* unary operator '--' */
#define SHIFT_L			12 				/* unary operator '<<' */
#define SHIFT_R			13 				/* unary operator '>>' */

#define BIT_OR			14 				/* binary operator '|' */
#define BIT_AND			15 				/* binary operator '&' */
#define BIT_XOR			16 				/* binary operator '^' */
#define COMPLEMENT		17 				/* unary operator '~' */

#define LOG_OR			18 				/* binary-operator '||' */
#define LOG_AND			19 				/* binary-operator '&&' */
#define LOG_NOT			20 				/* binary-operator '!' */
#define EQ				21 				/* binary-operator '==' */
#define NEQ				22 				/* binary-operator '!=' */
#define LT				23 				/* binary-operator '<' */
#define LE				24 				/* binary-operator '<=' */
#define GT				25 				/* binary-operator '>' */
#define GE				26 				/* binary-operator '>=' */

#define COND			27				/* ex. (a == b) ? 5 : 4 */
#define COMMA			28				/* ex. expr,expr or a,b */ 

#define ASSIGN			29				/* binary-operator '=' */
#define ASSIGN_ADD		30 				/* binary-operator '+=' */
#define ASSIGN_SUB		31 				/* binary-operator '-=' */
#define ASSIGN_MULT		32 				/* binary-operator '*=' */
#define ASSIGN_DIV		33 				/* binary-operator '/=' */
#define ASSIGN_MOD		34 				/* binary-operator '%=' */
#define ASSIGN_BIT_OR	35 				/* binary-operator '|=' */
#define ASSIGN_BIT_AND	36 				/* binary-operator '&=' */
#define ASSIGN_BIT_XOR	37 				/* binary-operator '^=' */
#define ASSIGN_SHIFT_L	38 				/* binary-operator '<<=' */
#define ASSIGN_SHIFT_R	39 				/* binary-operator '>>=' */

#define SYMREF			40				/* Symbol reference */
#define CONSTREF		41				/* Constant reference */
#define FUNCREF			42				/* Function reference */
#define ARRAYREF		43				/* Array reference */
#define ADDRESS_OF		44				/* Address-of operator */

/*
 * Parameter types for builtin prototypes.
 */
#define VOID_T					( TYPE_SPEC *)(&v_type)
#define CHAR_T					( TYPE_SPEC *)(&c_type)
#define SHORT_T					( TYPE_SPEC *)(&s_type)
#define LONG_T					( TYPE_SPEC *)(&l_type)
#define U_CHAR_T				( TYPE_SPEC *)(&uc_type)
#define U_SHORT_T				( TYPE_SPEC *)(&us_type)
#define U_LONG_T				( TYPE_SPEC *)(&ul_type)
#define FLOAT_T					( TYPE_SPEC *)(&f_type)
#define DOUBLE_T				( TYPE_SPEC *)(&d_type)
#define ARRAY_OF_CHAR_T			( TYPE_SPEC *)(&aofc_type)
#define ARRAY_OF_SHORT_T		( TYPE_SPEC *)(&aofs_type)
#define ARRAY_OF_LONG_T			( TYPE_SPEC *)(&aofl_type)
#define ARRAY_OF_U_SHORT_T		( TYPE_SPEC *)(&aofus_type)
#define ARRAY_OF_U_LONG_T		( TYPE_SPEC *)(&aoful_type)

#define PTR_TO_CHAR_T			( TYPE_SPEC *)(&ptoc_type)
#define PTR_TO_SHORT_T			( TYPE_SPEC *)(&ptos_type)
#define PTR_TO_LONG_T			( TYPE_SPEC *)(&ptol_type)
#define PTR_TO_U_CHAR_T			( TYPE_SPEC *)(&ptouc_type)
#define PTR_TO_U_SHORT_T		( TYPE_SPEC *)(&ptous_type)
#define PTR_TO_U_LONG_T			( TYPE_SPEC *)(&ptoul_type)
#define PTR_TO_FLOAT_T			( TYPE_SPEC *)(&ptof_type)
#define PTR_TO_DOUBLE_T			( TYPE_SPEC *)(&ptod_type)

/*
 * Macros for easy access to structure ENVIRON.
 */

#define ENV_SCOPE		(env_ptr->cur_scope)
#define ENV_PROTO		(env_ptr->proto_lst)
#define ENV_SWITCH		(env_ptr->cur_switch)
#define ENV_GLOB_SYMS	(env_ptr->global_scope->symbols)

/*
 * Macro definitions.
 */

#define INTEGRAL_TYPE(typ)	((typ == METH_CHAR_TYPE) ||(typ == METH_U_CHAR_TYPE) ||	\
					 (typ == METH_SHORT_TYPE) || (typ == METH_U_SHORT_TYPE) ||		\
					 (typ == METH_LONG_TYPE)  || (typ == METH_U_LONG_TYPE))

#define FLOATING_TYPE(typ)	((typ == METH_FLOAT_TYPE) || (typ == METH_DOUBLE_TYPE))

#define ARITH_TYPE(typ)		(INTEGRAL_TYPE(typ) || FLOATING_TYPE(typ))

#define LOGICAL(val,typ) ((typ==METH_CHAR_TYPE) ? (val.c != (char) 0)	 :	\
			 (typ==METH_U_CHAR_TYPE)  	? (val.uc != (unsigned char) 0)	 :	\
			 (typ==METH_SHORT_TYPE)		? (val.s != (short) 0)			 :	\
			 (typ==METH_U_SHORT_TYPE) 	? (val.us != (unsigned short) 0) :	\
			 (typ==METH_LONG_TYPE) 	 	? (val.l != (long) 0)			 :	\
			 (typ==METH_U_LONG_TYPE)	? (val.ul != (unsigned long) 0)	 :	\
			 (typ==METH_FLOAT_TYPE)		? (val.f != (float) 0.0)		 :	\
   	 									  (val.d != 0.0))

#define INTEGRAL_PROMOTION(typ) 									\
	{																\
		if ((typ==METH_U_SHORT_TYPE) || (typ==METH_U_CHAR_TYPE)) 	\
				typ=METH_U_LONG_TYPE;								\
		else if ((typ==METH_SHORT_TYPE) || (typ==METH_CHAR_TYPE)) 	\
				typ=METH_LONG_TYPE;									\
	}

#define SAME_TYPE(typ1,typ2) (														\
	((typ1==METH_DOUBLE_TYPE)  || (typ2==METH_DOUBLE_TYPE))  ? METH_DOUBLE_TYPE   :	\
	((typ1==METH_FLOAT_TYPE)   || (typ2==METH_FLOAT_TYPE))   ? METH_FLOAT_TYPE    :	\
	((typ1==METH_U_LONG_TYPE)  || (typ2==METH_U_LONG_TYPE))  ? METH_U_LONG_TYPE   :	\
	((typ1==METH_LONG_TYPE)    || (typ2==METH_LONG_TYPE))    ? METH_LONG_TYPE     :	\
				METH_LONG_TYPE)

#define RESULT_TYPE_PTR(typ,typ_ptr)		\
	{										\
		if (typ == METH_DOUBLE_TYPE)		\
			typ_ptr = DOUBLE_T;				\
		else if (typ == METH_FLOAT_TYPE)	\
			typ_ptr = FLOAT_T;				\
		else if (typ == METH_U_LONG_TYPE)	\
			typ_ptr = U_LONG_T;				\
		else								\
			typ_ptr = LONG_T;				\
	}

#define ADJ_CAST(val, type, new_typ, env_ptr)								\
							{ 												\
								if ((type)->typ != new_typ)					\
							    	cast(val, &type, new_typ, env_ptr);		\
		    				}

/*
 * Max number of parameters for using in builtins function.
 * If builtin function that has more than the maximum number of 
 * parameters is added to the builtin prototypes(in wrappers, 
 * builtin), the maximum NUM_ARGS has to be changed accordingly.
 */
//#define NUM_ARGS		6
#define NUM_ARGS		10

/*
 * Built-in prototype structure.
 */
typedef struct bi_proto BI_PROTO;
struct bi_proto {
	char		blkMenu;
	char		devMenu;
	VALUE 		(*bltin_ptr)( SYM *, ENVIRON * );			/* Ptr to a built-in function */
	TYPE_SPEC	*ret_type;				/* A type of its return value */
	char 		*bltin_name;			/* ASCII built-in name string */
	TYPE_SPEC 	*arg_type[NUM_ARGS];	/* The built-in parameter types */
};


/**************************************************************
 *	Defines
 **************************************************************/
/*
 * The eval_expr() calls itself recursively when evaluating an expression.
 * For the test system to output an expression entry point, this wrapper
 * function was created. In a non-testing execution, the eval_expr() calls
 * itself recursively without going through the wrapper function that causes
 * slower execution.
 */ 
#define do_eval_expr( expr, type, env_ptr, value ) \
					    eval_expr_wrap( expr, type, env_ptr, value )
extern void eval_expr_wrap P(( EXPRESS *, TYPE_SPEC **, ENVIRON *, VALUE * ));

/**************************************************************
 *	Externals
 **************************************************************/
extern const TYPE_SPEC c_type;
extern const TYPE_SPEC s_type;
extern const TYPE_SPEC l_type;
extern const TYPE_SPEC uc_type;
extern const TYPE_SPEC us_type;
extern const TYPE_SPEC ul_type;
extern const TYPE_SPEC f_type;
extern const TYPE_SPEC d_type;

extern const TYPE_SPEC ptoc_type;
extern const TYPE_SPEC ptous_type;
extern const TYPE_SPEC ptol_type;
extern const TYPE_SPEC ptoul_type;
extern const TYPE_SPEC ptof_type;
extern const TYPE_SPEC ptod_type;

extern const TYPE_SPEC aofc_type;
extern const TYPE_SPEC aofus_type;
extern const TYPE_SPEC aoful_type;

extern const TYPE_SPEC v_type;
 
/**************************************************************
 *	Function Prototypes
 **************************************************************/

	/* int_exec.c */
extern long case_const P((EXPRESS *, ENVIRON *));
extern void add_switch P((ENVIRON *));
extern void del_switch P((ENVIRON *));
extern int exec_stmt P((STATEMENT *, ENVIRON *));

	/* int_expr.c */
extern void cast P((VALUE *, TYPE_SPEC **, int, ENVIRON *));
extern void get_val P((VALUE_P *, TYPE_SPEC *, VALUE *, ENVIRON *));
extern void assign_val P((VALUE_P *, int, VALUE *, TYPE_SPEC **, ENVIRON *));

	/* st_parse.c */
int st_parse P((ENVIRON *));

	/* int_wrap.c */
extern void init_a_bi_symbols P((void));


	/* int_stab.c */
extern void add_scope P((SYM *, ENVIRON *));
extern void del_scope P((ENVIRON *env_ptr));
extern SYM *cpy_sym P((SYM *,int ,ENVIRON *));
extern void environ_init P((ENVIRON *, RHEAP *));
extern SYM *lookup_global P((char *, ENVIRON *));
extern void del_sym_lst P((SYM *,int , ENVIRON *));

	/* int_vfy.c */
extern long valid_array_sub P((EXPRESS *, ENVIRON *));
extern int valid_stmt P((STATEMENT *, ENVIRON *));
extern TYPE_SPEC *valid_expression P((EXPRESS *, ENVIRON *));
extern int valid_bltin_call_args P((EXPRESS *, EXPRESS *, ENVIRON *));


	/* int_bld.c */
extern void init_bld P((void));

 	/* Expressions */
extern EXPRESS *bld_exp_lst P((EXPRESS *, EXPRESS *));
extern EXPRESS *bld_string P((unsigned int, char *, ENVIRON *));
extern EXPRESS *bld_symref P((unsigned int, char *, ENVIRON *));
extern TYPE_SPEC *bld_type P((int, TYPE_SPEC *, long , ENVIRON *));
extern EXPRESS *bld_bltin_ref P((unsigned int, 
									char *, EXPRESS *, ENVIRON *));
extern EXPRESS *bld_arrayref P((unsigned int,
									 EXPRESS *, EXPRESS *, ENVIRON *));
extern EXPRESS *bld_exp P((unsigned int, int, EXPRESS *, 
									EXPRESS *, EXPRESS *, ENVIRON *));

	/* Statements */
extern STMT_PTRS *bld_stmt_lst P((STATEMENT *, STMT_PTRS *, ENVIRON *));
extern STATEMENT *bld_stmt P((unsigned int, int, STATEMENT *, 
						EXPRESS *, EXPRESS *, EXPRESS *, ENVIRON *));

	/* Declarations */
extern SYM *bld_sym_lst P((ENVIRON *, SYM *, SYM *));
extern SYM *bld_sym P((unsigned int, char *, ENVIRON *));
extern SYM *bld_add_type P((int, SYM *, ENVIRON *));
extern SYM *bld_array P((SYM *, EXPRESS *, ENVIRON *));
extern SYM *bld_abstr_sym P((unsigned int, TYPE_SPEC *, ENVIRON *));
extern SYM *bld_bltin_decl P((unsigned int, char *, SYM *, ENVIRON *));

	/* External Definitions */
extern void bld_val_p P((TYPE_SPEC *, size_t, ENVIRON *, VALUE_P *));
extern void env_to_env2 P((ENV_INFO *env_info, ENV_INFO2 *env_info2));
extern int env2_to_env P((ENV_INFO2 *env_info2, ENV_INFO *env_info, ITEM_ID blk_id, unsigned long instance));
extern int env2_handle_to_env P((ENV_INFO2 *env_info2, ENV_INFO *env_info, BLOCK_HANDLE bh));
extern int env2_handle_to_env2 P((ENV_INFO2 *env_info2, ENV_INFO2 *env_info, BLOCK_HANDLE bh));
extern int env2_to_env2 P((ENV_INFO2 *env_info2, ENV_INFO2 *env_info, ITEM_ID blockId, unsigned long instance));


	/* Used by scan.l */
extern char *bld_str P((char *));
extern EXPRESS *bld_char P((unsigned int, char *, ENVIRON *));
extern EXPRESS *bld_int_const P((unsigned int, char *, ENVIRON *));
extern EXPRESS *bld_float_const P((unsigned int, char *, ENVIRON *));

	/* imalloc.c */
extern	void 	*imalloc_chk P((ENVIRON *, size_t));
extern	void 	*irealloc_chk P((ENVIRON *, char *, size_t));

#ifdef __cplusplus
    }
#endif

#endif
