/**
 *		System management parameters configuration
 *      and its functionalities
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)sm.h	1.0  00  28 April 2014
 *
 *	sm.h - System management definitions
 */

#ifndef	SM_H
#define	SM_H


#include "cm_lib.h"

#define SM_PD_TAG_SIZE  34
#define SM_DEV_ID_SIZE  34

extern int softing_sm_identify 
    P((ENV_INFO* ,unsigned short, CR_SM_IDENTIFY_CNF* ));
extern int softing_sm_clr_node 
    P((ENV_INFO* ,unsigned short, CR_SM_IDENTIFY_CNF* )) ;
extern int softing_sm_set_node 
    P((ENV_INFO* ,unsigned short, char* )) ;
extern int softing_sm_clr_tag 
    P((ENV_INFO* ,unsigned short, CR_SM_IDENTIFY_CNF* )) ;
extern int softing_sm_assign_tag 
    P((ENV_INFO* ,unsigned short, CR_SM_IDENTIFY_CNF* )) ;

#endif
