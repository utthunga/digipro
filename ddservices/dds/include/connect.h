#ifndef CONNECT_H
#define CONNECT_H

#define VFD_MIB     1
#define VFD_FBAP    2

/*
 * The CR no 1 is used for the server VCR of the device and is
 * used by the HOST device by default when VCR_init is done for
 * MIB access
 */
#define STARTING_CR_NO    2

/*
* The local address for the MIB_VCR and 
* FBAP_VCR are nothing but fas_dll_local_addr
* ( which is local endpoint address) and has to be unique 
* within host stack (values 0x20  0xf7) 
*/

typedef enum _LOCAL_ADDR_TBL {
    INVALID_ADDR = 0,
    LOCAL_ADDR1 = 0x20,
    LOCAL_ADDR2 = 0x21,
    LOCAL_ADDR3 = 0x22,
    LOCAL_ADDR4 = 0x23,
    LOCAL_ADDR5 = 0x24,
}LOCAL_ADDR_TABLE;

typedef enum _CR_TABLE {
    INVALID_CR = 0,
    CR1,
    CR2,
    CR3,
    CR4,
    CR5,
}CR_TABLE;

/*
 * Console User input directive
 */
enum DEV_CONNECT_STAGE {
    INVALID_STAGE = 0,
	SET_APPL_STATE_BUSY,
    MIB_WR_LOCAL_VCR,
    MIB_FMS_INITIATE,
    MIB_GET_OD,
    FBAP_WR_FMS_LOCAL_VCR,
    FBAP_WR_LOCAL_VCR,
    FBAP_FMS_INITIATE,
    FBAP_GET_OD,
	LAST_STAGE,
};

/*
 * Enum data of the mode block
 */

enum MODE_BLOCK_ENUM {
    INVALID = 0,
    ROUT = 1,
    RCAS = 2,
    CAS = 4,
    AUTO = 8,
    MAN = 16,
    LO = 32,
    IMAN = 64,
    OOS = 128,
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int write_nma_local_vcr(ENV_INFO *, unsigned short, int, NETWORK_HANDLE);
extern int initiate_fms(ENV_INFO *, unsigned short, int, NETWORK_HANDLE);
extern int get_od(ENV_INFO *, unsigned short, int, NETWORK_HANDLE);
extern int write_fms_local_vcr(ENV_INFO *, unsigned short, NETWORK_HANDLE);
extern int dds_connect_device(ENV_INFO* , NETWORK_HANDLE);
extern int abort_mib_fbap_cr(ENV_INFO *, NETWORK_HANDLE);

#ifdef __cplusplus
}
#endif /* __cplusplus */

int re_setup_device_connection(ENV_INFO *, unsigned short,
		NETWORK_HANDLE);

#endif
