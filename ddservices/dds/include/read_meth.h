#ifndef READ_METH_H
#define READ_METH_H

#include "ret_info.h"
#include "method_tbl.h"

int init_method_tbl(void);
int inc_meth_count(void);
void set_meth_block_tag(const char *block_tag);
struct methodTable *get_meth_tbl_index(void);

#endif



