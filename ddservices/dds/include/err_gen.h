/*
 * err_gen.h.h (GENERATED FILE)
 *
 * Copyright 1993 Rosemount, Inc., All Rights Reserved.
 * Copyright 1996 Fieldbus Foundation,  All Rights Reserved.
 *
 * This is the header file for the system error messages.
 */

#ifndef ERR_GEN_H
#define ERR_GEN_H

#ifdef __cplusplus
    extern "C" { 
#endif

/*
 * Prototypes
 */
#ifdef STDARGS
extern void message (int,...);
extern void info (int,...);
extern void warning (int,...);
extern void error (ENV_INFO2 *,...);
extern void fatal (int,...);
#else
extern void message ();
extern void info ();
extern void warning ();
extern void error ();
extern void fatal ();
#endif

/*
 * Defines
 */
#define OUT_OF_MEMORY                            100
#define SEEK_ERROR                               101
#define WRITE_ERROR                              102
#define READ_ERROR                               103
#define STAT_ERROR                               104
#define FILE_OPEN_ERROR                          105
#define FILENAME_IN_CONFIG_FILE                  106
#define CONFIG_RECURSION_LIMIT                   107
#define MULT_DFLT_CONFIG_FILES                   108
#define MISSING_PARAMETER                        109
#define BAD_OPTION                               110
#define BAD_DFLT_CONFIG_SPEC                     111
#define INVALID_DDL_TYPE                         112
#define INVALID_OPTION                           113
#define CANNOT_OPEN_SETUP_FILE                   114
#define INFILE_IS_OUTFILE                        115
#define ITEM_NOT_FOUND                           201
#define ATTRIBUTES_TOO_LARGE                     202
#define IMPORT_TOO_LARGE                         203
#define LIKE_TOO_LARGE                           204
#define DEV_DIR_TOO_LARGE                        205
#define BLK_DIR_TOO_LARGE                        206
#define INCOMPATIBLE_DDOD_REV                    210
#define INCOMPATIBLE_DDOD_FMT                    211
#define DDOD_FMT_ERROR                           212
#define NULL_ENUMERATIONS_SPECIFIER              220
#define NULL_ELEMENTS_SPECIFIER                  221
#define NULL_MEMBERS_SPECIFIER                   222
#define NULL_RESPCODES_SPECIFIER                 223
#define NO_ATTRIBUTES                            224
#define INVALID_DICT_REF                         225
#define COMMAND_TOO_MANY_ITERATIONS              226
#define COMMAND_MULTIBYTE_VAR                    227
#define COMMAND_BAD_INDEX                        228
#define COMMAND_VAR_NOT_FOUND                    229
#define OUT_VAR_NO_CLASS                         230
#define SHAPE_FILE_SYNTAX                        231
#define SHAPE_SYMBOL_MISMATCH                    232
#define TOO_LARGE_FOR_4X                         233
#define INVALID_MIN_VALUE                        300
#define INVALID_MAX_VALUE                        301
#define TREEFILE_ALREADY_OPEN                    302
#define TREEFILE_ALREADY_CLOSED                  303
#define TREEFILE_NONEXISTANT                     304
#define ILLEGAL_SRC_CHARACTER                    305
#define ILLEGAL_LB_DIRECTIVE                     306
#define DUPLICATE_COUNTRY_CODE                   307
#define APPEARS_TWICE                            308
#define PARSER_ERROR                             309
#define ZERO_SIZED_VARIABLE                      310
#define INVALID_ESCAPE_SEQUENCE                  311
#define ALREADY_DEFINED                          312
#define INVALID_REFERENCE                        313
#define EXCESS_ARGUMENTS                         314
#define BAD_INPUT_FILE                           315
#define UNKNOWN_LANGUAGE                         316
#define INVALID_DICTSTR_NAME                     317
#define NO_RELEASE_DIR                           318
#define CREATING_DEVICE_DIR                      319
#define CANT_CREATE_DIR                          320
#define INVALID_IMPORT                           321
#define CREATING_MANUFACTURER_DIR                322
#define INCOMPATIBLE_BUILTINS                    323
#define SUBATTR_ALREADY_DEFINED                  324
#define MISSING_COUNTRY_CODE_DELIMITER           327
#define BAD_LANG_COUNTRY_CODE                    328
#define INVALID_DICTSTR_NAME_IMPORT              329
#define IMPORT_ITEM_NOT_FOUND                    400
#define ENUMERATOR_NOT_FOUND                     401
#define MIN_NOT_FOUND                            402
#define MAX_NOT_FOUND                            403
#define MEMBER_NOT_FOUND                         404
#define ELEMENT_NOT_FOUND                        405
#define RESPCODE_NOT_FOUND                       406
#define VAR_TYPE_WRONG_IN_REDEF                  408
#define REDEFINED_WITHOUT_IMPORT                 411
#define NO_MIN_VALUE                             412
#define NO_MAX_VALUE                             413
#define TRANSACTION_NOT_FOUND                    414
#define UNRESOLVED_IMPORT_LIKE                   415
#define IMPORT_VAR_NO_LABEL                      416
#define INVALID_ATTR_REDEFINITION                417
#define MEMBER_SYMBOL_ID_INVALID                 500
#define STD_SYMBOL_ID_INVALID                    501
#define STD_SYMBOL_NOT_IN_FILE                   502
#define SYMBOL_REDEFINED                         503
#define SHOULD_IMPORT_SYMBOL                     504
#define CANT_OPEN_SYMBOL_TABLE                   505
#define SYMBOL_FILE_SYNTAX                       506
#define INVALID_SYMBOL_FILE_FLAG                 507
#define DUPLICATE_SYMBOL_IDS                     508
#define SYMBOL_NOT_DEFINED                       509
#define SYMBOL_NOT_USED                          510
#define SYMBOL_USED_BUT_NOT_REGISTERED           511
#define MANDATORY_SYMBOL_NOT_DEFINED             512
#define SYMBOL_DEFINED_IMPORTED                  513
#define SYMBOL_FILES_NESTED_TOO_DEEP             514
#define NO_SYMBOL_NAME_DEFINED                   515
#define SYMBOL_DEFINED_WITH_NO_TYPE              516
#define SYMBOL_IMPORTED_BUT_NOT_REGISTERED       517
#define SYMBOL_TYPE_REDEFINED                    518
#define SYMBOL_ID_REDEFINED                      519
#define SYMBOL_ID_CONFLICT                       520
#define SYMBOL_ITEM_ID_OVERFLOW                  521
#define SYMBOL_ITEM_ID_INACTIVE                  522
#define SYMBOL_FILE_ERRS_DETECT                  523
#define DEVICE_FILE_SYNTAX                       530
#define VFY_IMPORTS_SELF                         600
#define VFY_REQ_ATTR_MISSING                     601
#define VFY_DUPLICATE_ENTRIES                    602
#define VFY_IMPROPER_USE                         603
#define VFY_ITEM_NOT_IN_BLOCK                    604
#define VFY_DUPLICATE_ELEMENTS                   605
#define VFY_DUPLICATE_RESP_CODES                 606
#define VFY_DUPLICATE_ENUMS                      607
#define VFY_DUPLICATE_IMPORTS                    608
#define VFY_DUPLICATE_REDEFINITIONS              609
#define VFY_UNDEFINED_ATTR_DELETED               611
#define VFY_UNDEFINED_LIKE_ITEM                  612
#define VFY_BAD_MENU_FLAGS                       613
#define VFY_UNCONDITIONALLY_INVALID              614
#define VFY_INVALID_CLASS                        615
#define VFY_DUAL_UNITS                           616
#define VFY_BAD_VAR_SIZE                         617
#define VFY_VLIST_MEMBER_COUNT                   618
#define VFY_NOT_ATTACHED                         619
#define VFY_MEMBER_TYPES_DIFFER                  620
#define VFY_ELEMENT_TYPES_DIFFER                 621
#define VFY_FMT_STRING_ERROR                     622
#define VFY_MIN_MAX_COUNT_ERROR                  623
#define VFY_DEFAULT_NOT_LAST                     624
#define VFY_BAD_BLOCK_REF                        625
#define VFY_CONDITIONAL_ATTRIBUTE                626
#define VFY_CHAR_REC_IN_PARAM                    627
#define VFY_PARAM_ITEM_NOT_FOUND                 628
#define VFY_INVALID_REFERENCE_INFO               629
#define VFY_NO_MEMBERS                           630
#define VFY_NON_SCALING_BUILTINS                 631
#define VFY_SCALING_BUILTINS                     632
#define VFY_WRONG_CHAR_MEM_COUNT                 633
#define VFY_WRONG_CHAR_MEM_TYPE                  634
#define VFY_DUPLICATE_WHICH_VALUES               635
#define VFY_ITEM_TOO_LARGE                       636
#define VFY_MULTIPLE_TRANSACTIONS                637
#define VFY_INVALID_ARGUMENT_FLAGS               638
#define VFY_INVALID_ARGUMENT_WIDTH               639
#define VFY_MANUFACTURER_REQUIRED                640
#define VFY_MANUFACTURER_NOT_PERMIT              641
#define VFY_COMMAND_IMPROPER_USE                 642
#define VFY_DUP_TRANS_INDEX                      643
#define VFY_INV_RESP_CODE_VALUE                  644
#define VFY_NO_RESP_CODE_DESC                    646
#define VFY_MASKS_LEAVE_GAP                      647
#define VFY_NO_TRANS_DATA_ITEMS                  648
#define VFY_NO_TRANS_OPERATION                   649
#define VFY_CMD_RPLY_MAX_OCTETS                  650
#define VFY_WRONG_CMD_MASK_TYPE                  651
#define VFY_NO_REDEF_CMD_ATTR                    652
#define VFY_NO_UNIV_CMD_REDEF                    653
#define VFY_CMN_PRAC_CMD_REDEF                   654
#define VFY_BLOCK_ITEMS_UNDEFINED                655
#define VFY_LOCAL_VAR_IN_BLOCK                   656
#define VFY_ITEM_QUAL_INVALID                    657
#define VFY_ITEM_QUAL_DUPED                      658
#define VFY_INVALID_FUNC_CLASS                   659
#define VFY_INVALID_BIT_ENUM_VAL                 660
#define VFY_INVALID_STATUS_CLASS                 661
#define VFY_INDEX_REF_UNDEFINED                  662
#define VFY_DUPL_CMD_NUMBER                      663
#define VFY_ITEM_UNDEFINED                       664
#define VFY_INVALID_ITEM_TYPE                    665
#define VFY_INVALID_ATTRIBUTE                    666
#define VFY_BIT_ENUM_CLASS                       667
#define VFY_BIT_ENUM_OUTPUT_STATUS               668
#define VFY_CMD_NUMBER_MAX                       669
#define VFY_COUNT_NOT_ALLOWED                    670
#define VFY_MENU_SUFFIX_CHANGED                  671
#define VFY_MENU_TYPE_ALREADY_DEFINED            672
#define VFY_MENU_PREFIX_NOT_STANDARD             673
#define VFY_MENU_NO_SUFFIX                       674
/* from dds :                                     */
#define UNDECLARED_IDENTIFIER                    700
#define NON_ARRAY_SUBSCRIPTED                    701
#define NON_INTEGRAL_INDEX                       702
#define NON_FUNCTION_CALL                        703
#define IDENTIFIER_RE_DEFINITION                 704
#define ARRAY_SIZE_MISSING                       705
#define SUBSCRIPT_OUT_OF_BOUNDS                  706
#define NEGATIVE_SUBSCRIPT                       707
#define CONSTANT_MISSING                         708
#define INVALID_CHARACTER                        709
#define INVALID_BIN_CHARACTER                    710
#define RETURN_VALUE_TYPE_MISMATCH               711
#define ARGUMENT_TYPE_MISMATCH                   712
#define ARGUMENT_TYPE_MISMATCH2                  713
#define ARGUMENT_COUNT_MISMATCH                  714
#define ARGUMENT_COUNT_MISMATCH2                 715
#define ERRANT_DUPLICATE_FUNCTION_PROTOTYPE      716
#define DUPLICATE_FUNCTION_PROTOTYPE             717
#define LOSS_OF_PRECISION                        718
#define NON_ARITHMETIC_TYPE_OPERAND_1            719
#define NON_ARITHMETIC_TYPE_OPERAND_2            720
#define OPERAND_MISMATCH_OPER_2_AND_3            721
#define NON_INTEGRAL_TYPE_OPERAND_1              722
#define NON_INTEGRAL_TYPE_OPERAND_2              723
#define LVALUE_REQUIRED                          724
#define ADDR_OF_OPERATOR_NOT_IN_FUNC_REF         725
#define FOR_EXPRESSION_NOT_ARITHMETIC            726
#define MISPLACED_CONTINUE                       727
#define MISPLACED_BREAK                          728
#define MISPLACED_CASE                           729
#define MISPLACED_DEFAULT_CASE                   730
#define MULTIPLE_DEFAULT_CASES                   731
#define STATEMENT_REQUIRES_ARITHMETIC            732
#define STATEMENT_REQUIRES_INTEGRAL              733
#define DUPLICATE_CASE_VALUE                     734
#define PARSE_ERROR                              735
#define CANNOT_FIND_IMAGE                        736
#define STRING_MUST_BE_LITERAL                   737
#define NO_IMPLICIT_STRING_SYNTAX                738
#define FIXUP_UNRESOLVED                         739
#define IMAGE_SIZE_EXCEEDED                      740
#define NOT_IN_PROFILE                           741
#define DUPLICATE_IMAGE_NAME_IMPORTED            742
#define VARIABLE_NOT_IN_BLOCK                    743
#define VFY_REC_ON_MENU                          744
#define VFY_ARRAY_ON_MENU                        745
#define UNKNOWN_BUILTIN							 746
#define VFY_CROSS_BLOCK_NOT_ALLOWED              747
#define DEV_BUILTIN_NOT_ALLOWED					 748
#define BLK_BUILTIN_NOT_ALLOWED					 749
#define WRONG_BLTIN_ARG_COUNT                    750
#define UNSUPPORTED_IN_4X                        751
#define NO_DEBUG_WITH_40                         752
#define INVALID_OUTSIDE_OF_BLOCK                 753
#define ERR_NO_MENU_ITEMS                        754
#define NO_DIRECT_LIST_ASSIGNMENT                755
#define NO_BLOCK_CONTEXT_FOR_REFERENCE           756
#define MENU_DISPLAY_BUILTIN_STYLE				 757
#define BLOCK_LIKE_LABEL_MUST_BE_REDEFINED       758
#define REF_MUST_SPECIFY_BLOCK                   759
#define LOCAL_PARAM_NOT_LOCAL					 760
#define NEGATING_UNSIGNED                        761
#define BIT_ENUM_ZERO_ILLEGAL                    762
#define VFY_DEVICE_MENU_PRESENT                  763
#define VFY_CLASS_MUST_BE_LOCAL                  764
#define DUP_DICT_STRING_NAME                     765
#define MENU_NOT_BLOCK_OR_DEVICE                 766
#define VFY_BIT_ENUM_TOO_LARGE_FOR_H1            767
#define VFY_STRING_LARGER_THAN_PROFILE           768

#ifdef __cplusplus
    } 
#endif

#endif
