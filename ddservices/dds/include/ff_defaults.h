/*****************************************************************************
    Copyright (c) 2017, 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    FF Default Values
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* ========================================================================== */

#ifndef __FF_DEFAULTS_H__
#define __FF_DEFAULTS_H__

/// MACRO DEFINATIONS for FF Bus Parameters
#define DFLT_SLOT_TIME              8
#define DFLT_PHL_OVERHEAD           6
#define DFLT_MAX_RPLY_DELAY         10
#define DFLT_MIN_INTER_PDU_DELAY    16
#define DFLT_TIME_SYNC_CLASS        3
#define DFLT_MAX_INTER_CHAN_SIG_SKEW 0
#define DFLT_PHL_PREAMBLE_EXTN      2
#define DFLT_PHL_GAP_EXTN           0
#define DFLT_TIME_DIST_PERIOD       5000
#define DFLT_DTB_STA_SPDU_DIST_PERIOD   5000
#define DFLT_TOKEN_HOLD_TIME        300
#define DFLT_MIN_TOKEN_DELEGATE_TIME    80
#define DFLT_TARGET_TOKEN_ROT_TIME  60000
#define DFLT_FIRST_UNPOLLED_NODE    200 // TODO(verify this number)
#define DFLT_NUM_UNPOLLED_NODE      0
#define DFLT_MAX_LINK_MAINT_TIME    0x150
#define DFLT_MAX_INACT_CLAIM_LAS_DELAY  30

#endif //  __FF_DEFAULTS_H__
