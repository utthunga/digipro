/**
 *		Network management parameters configuration
 *      and its functionalities
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)sm.h	1.0  00  28 April 2014
 *
 *	nm.h - Network Management definitions
 */

#ifndef	NM_H
#define	NM_H

#define FEAT_SUPP_LEN 8
// Network Management Helper functions

#include "cm_lib.h"

typedef enum _NM_OBJECTS {
    DUMMY_CMD = 0,
    STACK_CAP_REC,
    VCR_LST_CTRL_VAR,
    VCR_LST_CHARAC_REC,
    VCR_STATIC_ENTRY_REC,
    VCR_DYNC_ENTY_REC,
    VCR_STAT_ENTY_REC,
    DLME_BASIC_CHARAC_REC,
    DLME_BASIC_INFO_REC,
    DLME_BASIC_STAT_REC,
    DLME_LM_CAP_VAR,
    DLME_LM_INFO_REC,
    PRIMARY_LM_FLAG_VAR,
    LIVE_LST_ARR_VAR,
    MAX_TOK_HOLD_TIME_ARR,
    BOOT_OPERAT_FUNC,
    CURR_LINK_SETT_REC,
    CONF_LINK_SETT_REC,
    DLME_LM_STAT_REC,
    LST_VAL_REC,
    LS_ACT_VAR,
    LS_CHARAC_REC,
    SCH_DES_REC,
    SCH_DOMAIN,
    PLME_BSC_CHARAC_REC,
    CHAN_STATUS_VAR,
    PLME_BASIC_INFO_REC,
    MME_WIRE_STAT_REC,    
}NM_OBJECTS;


extern int softing_nm_wr_local_vcr P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
extern int softing_nm_fms_initiate P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
extern int softing_nm_get_od P((ENV_INFO* ,CM_NMA_VCR_LOAD* )) ;
extern int softing_nm_read P((ENV_INFO* , NETWORK_HANDLE, unsigned short, int, int )) ;
extern int softing_nm_dev_boot P((ENV_INFO* , NETWORK_HANDLE, unsigned short, int)) ;
extern int softing_scan_network P((ENV_INFO* , SCAN_DEV_TBL *));

extern unsigned int convert_to_ltl_endian(unsigned int);

#endif
