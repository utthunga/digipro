/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)ddsvtsim.h	30.3  30  14 Nov 1996
 */


#ifndef DDSVTSIM_H
#define DDSVTSIM_H

#include "cm_lib.h"
#include "pc_loc.h"
#include "table.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern int vtsim_destroy_device P((DEVICE_HANDLE device_handle));
extern int vtsim_init_device P((DEVICE_HANDLE device_handle));
extern int vtsim_make_sim_params P((BLOCK_HANDLE bh, const PC *param_tablep,
					const FLAT_BLOCK_DIR *flat_block_dirp));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* FF */


