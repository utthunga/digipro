/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	%W%  %R%  %G%
 *
 *	rtn_def.h is used to generate rtn_code.h and as an include
 *	file to dds_err.c
 */

#ifdef GEN_RTN_H
#define rcode_def(x,y,z) \#define x	y
#define lb_ \#
/********************************************************************
 * rtn_code.h is a generated from rtn_def.h!!
 *     Edit rtn_def.h to make changes!!!
 ********************************************************************/
lb_ ifndef RTN_CODE_H
lb_ define RTN_CODE_H

lb_ include "std.h"

lb_ define RETURN_CODE int

lb_ ifdef __cplusplus
extern "C" {
lb_ endif /* __cplusplus */

extern char *dds_error_string P((int));

lb_ ifdef __cplusplus
}
lb_ endif /* __cplusplus */

#else
#define rcode_def(x,y,z) { y, z }, 
#endif


#define DDS_START	1400
#define EVAL_START	1500
#define FETCH_START 1600
#define DDI_START	1700
#define BLTIN_START	1800
#define METH_START	1900
#define PC_START    2000
#define L7_START	2100
#define NM_START	2200
#define CS_START	2300
#define CM_START	2400
#define FFDS_START	2500

#define RCSIM_START 2700
#define SM_START	2800
#define CM_IF_START 2900

#define CS_FMS_START    0x10000
#define CS_NMA_START    0x20000
#define CS_SM_START     0x30000
#define	CS_ABORT_USR_START 0x41000
#define CS_CM_IF_FMS_ABT   0x42000
#define CS_CM_IF_FAS_ABT   0x43000
#define CS_CM_IF_DLL_ABT   0x44000

/* DDS error codes */
rcode_def (	SUCCESS,						(0),	"|en|Success")
rcode_def (	FAILURE,						(1),	"|en|Failure")
rcode_def (	DDS_SUCCESS,					(0),	"|en|DDS Success")
rcode_def (	DDS_OUT_OF_MEMORY,				(DDS_START + 2),	"|en|Out of Memory")
rcode_def (	VARIABLE_VALUE_NEEDED,			(DDS_START + 3),	"|en|Variable Value Needed")
rcode_def (	DDS_WRONG_DDOD_REVISION,		(DDS_START + 4),	"|en|DDS Wrong DDOD Revision")

rcode_def (	DDS_ERROR_END,					(DDS_START + 99),	"|en|end of DDS error numbers")

/* EVAL error codes */
rcode_def (	DDL_SUCCESS,					(0),	"|en|EVAL Success")
rcode_def (	DDL_MEMORY_ERROR,				(EVAL_START + 1),	"|en|EVAL Memory Error")
rcode_def (	DDL_INSUFFICIENT_OCTETS,		(EVAL_START + 2),	"|en|EVAL Insufficient octets")
rcode_def (	DDL_SHORT_BUFFER,				(EVAL_START + 3),	"|en|EVAL Short buffer")
rcode_def (	DDL_ENCODING_ERROR,				(EVAL_START + 4),	"|en|EVAL Encoding error")
rcode_def (	DDL_LARGE_VALUE,				(EVAL_START + 5),	"|en|EVAL Large value")
rcode_def (	DDL_DIVIDE_BY_ZERO,				(EVAL_START + 6),	"|en|EVAL Divide by zero")
rcode_def (	DDL_BAD_VALUE_TYPE,				(EVAL_START + 7),	"|en|EVAL Bad value type")
rcode_def (	DDL_SERVICE_ERROR,				(EVAL_START + 8),	"|en|EVAL Service error")
rcode_def (	DDL_FILE_ERROR,					(EVAL_START + 9),	"|en|EVAL File error")
rcode_def (	DDL_BAD_FILE_TYPE,				(EVAL_START + 10),	"|en|EVAL Bad file type")
rcode_def (	DDL_FILE_NOT_FOUND,				(EVAL_START + 11),	"|en|EVAL File not found")
rcode_def (	DDL_OUT_OF_DATA,				(EVAL_START + 12),	"|en|EVAL Out of data")
rcode_def (	DDL_DEFAULT_ATTR,				(EVAL_START + 13),	"|en|EVAL Default attr")
rcode_def (	DDL_NULL_POINTER,				(EVAL_START + 14),	"|en|EVAL Null pointer")
rcode_def (	DDL_INCORRECT_FILE_FORMAT,		(EVAL_START + 15),	"|en|EVAL Incorrect file format")
rcode_def (	DDL_INVALID_PARAM,				(EVAL_START + 16),	"|en|EVAL Invalid parameter")
rcode_def (	DDL_CHECK_RETURN_LIST,			(EVAL_START + 17),	"|en|EVAL errors are loaded in the return list")
rcode_def (	DDL_DEV_SPEC_STRING_NOT_FOUND,		(EVAL_START + 18),	"|en|EVAL Device Specific string not found")
rcode_def (	DDL_DICT_STRING_NOT_FOUND,		(EVAL_START + 19),	"|en|EVAL Dict string not found")
rcode_def (	DDL_READ_VAR_VALUE_FAILED,		(EVAL_START + 20),	"|en|EVAL Read var value failed")
rcode_def (	DDL_BINARY_REQUIRED,			(EVAL_START + 21),	"|en|EVAL Binary required")
rcode_def (	DDL_VAR_TYPE_NEEDED,			(EVAL_START + 22),	"|en|EVAL Var type needed")
rcode_def (	DDL_EXPR_STACK_OVERFLOW,		(EVAL_START + 23),	"|en|EVAL Expression stack has overflowed")
rcode_def (	DDL_INVALID_TYPE_SUBATTR,		(EVAL_START + 24),	"|en|EVAL subattr not supported by var type")
rcode_def (	DDL_BAD_FETCH_TYPE,				(EVAL_START + 25),	"|en|EVAL bad fetch type returned by ABT_DEV_FAMILY")
rcode_def (	DDL_RESOLVE_FETCH_FAIL,			(EVAL_START + 26),	"|en|EVAL null fetch upcall pointer")
rcode_def (	DDL_BLOCK_TABLES_NOT_LOADED,	(EVAL_START + 27),	"|en|EVAL block tables are not loaded")

rcode_def (	DDL_ERROR_END,					(EVAL_START + 99),	"|en|end of EVAL error numbers")

/* FETCH error codes */
rcode_def (	FETCH_INVALID_DEVICE_HANDLE,	(FETCH_START + 0),	"|en|FETCH Invalid device handle")
rcode_def (	FETCH_DEVICE_NOT_FOUND,			(FETCH_START + 1),	"|en|FETCH Device not found")
rcode_def (	FETCH_INVALID_DEV_TYPE_HANDLE,	(FETCH_START + 2),	"|en|FETCH Invalid Device type handle")
rcode_def (	FETCH_DEV_TYPE_NOT_FOUND,		(FETCH_START + 3),	"|en|FETCH Device type not found")
rcode_def (	FETCH_INVALID_DD_HANDLE_TYPE,	(FETCH_START + 4),	"|en|FETCH Invalid dd handle type")
rcode_def (	FETCH_TABLES_NOT_FOUND,			(FETCH_START + 5),	"|en|FETCH Tables not found")
rcode_def (	FETCH_ITEM_NOT_FOUND,			(FETCH_START + 6),	"|en|FETCH Item not found")
rcode_def (	FETCH_DIRECTORY_NOT_FOUND,		(FETCH_START + 7),	"|en|FETCH Block not found")
rcode_def (	FETCH_INSUFFICIENT_SCRATCHPAD,	(FETCH_START + 10),	"|en|FETCH Insufficient scratchpad memory provided")
rcode_def (	FETCH_NULL_POINTER,				(FETCH_START + 11),	"|en|FETCH Null pointer passed")
rcode_def (	FETCH_ITEM_TYPE_MISMATCH,		(FETCH_START + 12),	"|en|FETCH Requested item type not in this object")
rcode_def (	FETCH_INVALID_ATTRIBUTE,		(FETCH_START + 13),	"|en|FETCH Invalid attribute for item type")
rcode_def (	FETCH_INVALID_RI,				(FETCH_START + 14),	"|en|FETCH Invalid reference indicator")
rcode_def (	FETCH_INVALID_ITEM_TYPE,		(FETCH_START + 15),	"|en|FETCH Invalid item type")
rcode_def (	FETCH_EMPTY_ITEM_MASK,			(FETCH_START + 16),	"|en|FETCH Object has no mask bits set")
rcode_def (	FETCH_ATTRIBUTE_NO_MASK_BIT,	(FETCH_START + 17),	"|en|FETCH Mask bit not set for attribute")
rcode_def (	FETCH_ATTRIBUTE_NOT_FOUND,		(FETCH_START + 18),	"|en|FETCH Attribute not found in object")
rcode_def (	FETCH_ATTR_LENGTH_OVERFLOW,		(FETCH_START + 19),	"|en|FETCH Unpacked integer exceeds maximum size")
rcode_def (	FETCH_ATTR_ZERO_LENGTH,			(FETCH_START + 20),	"|en|FETCH Attribute has zero length")
rcode_def (	FETCH_OBJECT_NOT_FOUND,			(FETCH_START + 21),	"|en|FETCH Object not found")
rcode_def (	FETCH_DATA_NOT_FOUND,			(FETCH_START + 22),	"|en|FETCH Local data area not found")
rcode_def (	FETCH_DATA_OUT_OF_RANGE,		(FETCH_START + 23),	"|en|FETCH Requested data out of local data range")
rcode_def (	FETCH_INVALID_DIR_TYPE,			(FETCH_START + 24),	"|en|FETCH Invalid directory type")
rcode_def (	FETCH_INVALID_TABLE,			(FETCH_START + 25),	"|en|FETCH Invalid table for directory type")
rcode_def (	FETCH_INVALID_EXTN_LEN,			(FETCH_START + 26),	"|en|FETCH Invalid extension length")
rcode_def (	FETCH_INVALID_PARAM,			(FETCH_START + 27),	"|en|FETCH Invalid parameter")
rcode_def (	FETCH_INVALID_ITEM_ID,			(FETCH_START + 28),	"|en|FETCH Invalid item ID")
rcode_def (	FETCH_INVALID_ATTR_LENGTH,		(FETCH_START + 29),	"|en|FETCH Invalid attribute length value")
rcode_def (	FETCH_BAD_DD_DEVICE_LOAD,		(FETCH_START + 30),	"|en|FETCH Bad dd device load")
rcode_def (	FETCH_DIR_TYPE_MISMATCH,		(FETCH_START + 31),	"|en|FETCH Directory type mismatch")
rcode_def (	FETCH_NO_OBJ_EXTN,				(FETCH_START + 32),	"|en|FETCH No object extension")
rcode_def (	FETCH_ERROR_END,				(FETCH_START + 99),	"|en|end of FETCH error numbers")


/* DDI error codes */
rcode_def (	DDI_INVALID_FLAT_FORMAT,		(DDI_START + 1),	"|en|DDI reports an invalid flat structure")
rcode_def (	DDI_INVALID_FLAT_MASKS,			(DDI_START + 2),	"|en|DDI reports an invalid flat masks")
rcode_def (	DDI_INVALID_TYPE,				(DDI_START + 3),	"|en|DDI reports an invalid specifier type")
rcode_def (	DDI_INVALID_ITEM_TYPE,			(DDI_START + 4),	"|en|DDI reports an invalid item type")
rcode_def (	DDI_INVALID_PARAM,				(DDI_START + 5),	"|en|DDI reports bad parameters in the call")
rcode_def (	DDI_MEMORY_ERROR,				(DDI_START + 6),	"|en|DDI reports memory error")
rcode_def (	DDI_TAB_BAD_NAME,				(DDI_START + 7),	"|en|DDI bad param name or param list name")
rcode_def (	DDI_TAB_BAD_DDID,				(DDI_START + 8),	"|en|DDI bad DDID")
rcode_def (	DDI_TAB_BAD_OP_INDEX,			(DDI_START + 9),	"|en|DDI bad operational index")
rcode_def (	DDI_TAB_NO_DEVICE,				(DDI_START + 10),	"|en|DDI device does not exist")
rcode_def (	DDI_TAB_NO_BLOCK,				(DDI_START + 11),	"|en|DDI block does not exist")
rcode_def (	DDI_TAB_BAD_PARAM_OFFSET,		(DDI_START + 12),	"|en|DDI bad param table offset")
rcode_def (	DDI_TAB_BAD_PARAM_LIST_OFFSET,	(DDI_START + 13),	"|en|DDI bad param list table offset")
rcode_def (	DDI_TAB_BAD_CHAR_OFFSET,		(DDI_START + 14),	"|en|DDI bad character offset")
rcode_def (	DDI_TAB_BAD_PARAM_MEMBER,		(DDI_START + 15),	"|en|DDI bad param table member")
rcode_def (	DDI_TAB_BAD_PARAM_LIST_MEMBER,	(DDI_START + 16),	"|en|DDI bad param list table member")
rcode_def (	DDI_TAB_BAD_SUBINDEX,			(DDI_START + 17),	"|en|DDI bad table subindex")
rcode_def (	DDI_TAB_BAD_PARAM_TYPE,			(DDI_START + 18),	"|en|DDI bad parameter item type")
rcode_def (	DDI_INVALID_BLOCK_HANDLE,		(DDI_START + 19),	"|en|DDI invalid block handle")
rcode_def (	DDI_BLOCK_NOT_FOUND,			(DDI_START + 20),	"|en|DDI block not found")
rcode_def (	DDI_INVALID_DEVICE_HANDLE,		(DDI_START + 21),	"|en|DDI invalid_device handle")
rcode_def (	DDI_DEVICE_NOT_FOUND,			(DDI_START + 22),	"|en|DDI device not found")
rcode_def (	DDI_INVALID_DEV_TYPE_HANDLE,	(DDI_START + 23),	"|en|DDI invalid device type handle")
rcode_def (	DDI_DEV_TYPE_NOT_FOUND,			(DDI_START + 24),	"|en|DDI device type not found")
rcode_def (	DDI_INVALID_DD_HANDLE,			(DDI_START + 25),	"|en|DDI invalid dd handle")
rcode_def (	DDI_INVALID_DD_HANDLE_TYPE,		(DDI_START + 26),	"|en|DDI invalid dd handle type")
rcode_def (	DDI_TAB_BAD,					(DDI_START + 27),	"|en|DDI invalid table")
rcode_def (	DDI_LEGAL_ENUM_VAR_VALUE,		(DDI_START + 28),	"|en|DDI found the value in the flat variable")
rcode_def (	DDI_ILLEGAL_ENUM_VAR_VALUE,		(DDI_START + 29),	"|en|DDI did not find the value in the flat variable")
rcode_def (	DDI_DD_BLOCK_REF_NOT_FOUND,		(DDI_START + 30),	"|en|DDI DD block reference not found")
rcode_def (	DDI_DEVICE_TABLES_NOT_FOUND,	(DDI_START + 31),	"|en|DDI device tables not found")
rcode_def (	DDI_BLOCK_TABLES_NOT_FOUND,		(DDI_START + 32),	"|en|DDI block tables not found")
rcode_def (	DDI_INVALID_BLOCK_TAG,			(DDI_START + 33),	"|en|DDI invalid block tag")
rcode_def (	DDI_INVALID_REQUEST_TYPE,		(DDI_START + 34),	"|en|DDI invalid request type")
rcode_def (	DDI_TAB_NO_UNIT,				(DDI_START + 35),	"|en|DDI no unit available")
rcode_def (	DDI_TAB_NO_WAO,					(DDI_START + 36),	"|en|DDI no wao available")
rcode_def (	DDI_TAB_NO_UPDATE,				(DDI_START + 37),	"|en|DDI no update available")
rcode_def (	DDI_INVALID_DEV_SPEC_STRING,	(DDI_START + 38),	"|en|DDI invalid device specific string")
rcode_def (	DDI_INSUFFICIENT_BUFFER,		(DDI_START + 39),	"|en|DDI invalid device specific string")
rcode_def (	DDI_BAD_DD_BLOCK_LOAD,			(DDI_START + 40),	"|en|DDI Bad dd block load")
rcode_def (	DDI_BAD_DD_DEVICE_LOAD,			(DDI_START + 41),	"|en|DDI Bad dd device load")
rcode_def (	DDI_NO_COMMANDS_FOR_PARAM,		(DDI_START + 42),	"|en|DDI Param has no commands")
rcode_def (	DDI_NO_READ_COMMANDS_FOR_PARAM,	(DDI_START + 43),	"|en|DDI Param has no read commands")
rcode_def (	DDI_NO_WRITE_COMMANDS_FOR_PARAM,(DDI_START + 44),	"|en|DDI Param has no write commands")
rcode_def (	DDI_COMMAND_NOT_FOUND,          (DDI_START + 45),	"|en|DDI Command was not found")
rcode_def (	DDI_TAB_BAD_PARAM_COUNT,		(DDI_START + 46),	"|en|DDI Bad param count")
rcode_def (	DDI_ERROR_END,					(DDI_START + 99),	"|en|end of DDI error numbers")

rcode_def (	BLTIN_SUCCESS,					0,					"|en|Success")
rcode_def (	BLTIN_NO_MEMORY,				(BLTIN_START+1),	"|en|Out of Memory")
rcode_def (	BLTIN_VAR_NOT_FOUND,			(BLTIN_START+2),	"|en|Cannot find variable")
rcode_def (	BLTIN_BAD_ID,					(BLTIN_START+3),	"|en|Specified ID does not exist")
rcode_def (	BLTIN_NO_DATA_TO_SEND,			(BLTIN_START+4),	"|en|No value to send to device")
rcode_def (	BLTIN_WRONG_DATA_TYPE,			(BLTIN_START+5),	"|en|Wrong variable type for builtin")
rcode_def (	BLTIN_NO_RESP_CODES,			(BLTIN_START+6),	"|en|Item type does not have response codes")
rcode_def (	BLTIN_BAD_METHOD_ID,			(BLTIN_START+7),	"|en|Invalid method ID")
rcode_def (	BLTIN_BUFFER_TOO_SMALL,			(BLTIN_START+8),	"|en|Supplied buffer for string is too small")
rcode_def (	BLTIN_CANNOT_READ_VARIABLE,		(BLTIN_START+9),	"|en|Cannot read a modified variable")
rcode_def (	BLTIN_INVALID_PROMPT,			(BLTIN_START+10),	"|en|Bad prompt string")
rcode_def (	BLTIN_NO_LANGUAGE_STRING,		(BLTIN_START+11),	"|en|No valid string for current language")
rcode_def (	BLTIN_DDS_ERROR,				(BLTIN_START+12),	"|en|A DDS error has occurred")
rcode_def (	BLTIN_FAIL_RESPONSE_CODE,		(BLTIN_START+13),	"|en|Response Code return caused builtin to fail")
rcode_def (	BLTIN_FAIL_COMM_ERROR,			(BLTIN_START+14),	"|en|Communication Error caused builtin to fail")
rcode_def (	BLTIN_NOT_IMPLEMENTED,			(BLTIN_START+15),	"|en|Builtin function not yet implemented")
rcode_def ( BLTIN_BAD_ITEM_TYPE,			(BLTIN_START+16),	"|en|Wrong item type for builtin")
rcode_def ( BLTIN_VALUE_NOT_SET,			(BLTIN_START+17),	"|en|Value not set in Param Cache")
rcode_def ( BLTIN_BAD_POINTER,				(BLTIN_START+18),	"|en|Parameter pointer expected to be non-NULL")
rcode_def (	BLTN_ERROR_END,					(BLTIN_START+99),	"|en|end of builtin error numbers")

rcode_def (	METH_METHOD_COLLISION,			(METH_START+1),		"|en|Multiple incompatible methods active")
rcode_def (	METH_INTERNAL_ERROR,			(METH_START+2),		"|en|Internal program problem - Aborting")
rcode_def (	METH_TOO_MANY_METHODS,			(METH_START+3),		"|en|Too many methods active at once")
rcode_def (	METH_METHOD_ABORT,				(METH_START+4),		"|en|Method aborted")
rcode_def (	METH_METHOD_RETRY,				(METH_START+5),		"|en|Method retrying")
rcode_def ( METH_PARSE_ERROR,				(METH_START+6),		"|en|Method syntax error")
rcode_def (	METH_ERROR_END,					(METH_START+99),	"|en|end of method error numbers")

/* Parameter Cache Errors */
rcode_def ( PC_SUCCESS,						0,					"|en|Success")
rcode_def ( PC_ERROR_START,					(PC_START+1),		"|en|The start of the PC errors")
rcode_def ( PC_METHOD_COLLISION,			(PC_START+1),		"|en|Multiple methods attempting to change one param")
rcode_def ( PC_NO_READ_OVER_CHANGED_PARAM,	(PC_START+2),		"|en|Attempt to read a value which was changed")
rcode_def ( PC_INTERNAL_ERROR,				(PC_START+3),		"|en|Inconsistent state detected internally")
rcode_def ( PC_WRONG_DATA_TYPE,				(PC_START+4),		"|en|A request expected the param to have a specific valid type")
rcode_def ( PC_NO_MEMORY,					(PC_START+5),		"|en|No memory to perform requested action")
rcode_def ( PC_VALUE_NOT_SET,				(PC_START+6),		"|en|An attempted get would result in a read which was explicitly restricted")
rcode_def ( PC_ENTRY_BUSY,					(PC_START+7),		"|en|Another process has the requested parameter cache entry in use")
rcode_def ( PC_PARAM_NOT_FOUND,				(PC_START+8),		"|en|The requested param reference does not exist")
rcode_def ( PC_BUFFER_TOO_SMALL,			(PC_START+9),		"|en|The return buffer is too small for data, data truncated")
rcode_def ( PC_BAD_STATUS_INFO,				(PC_START+10),		"|en|The status_info parameter is not set or set incorrectly")
rcode_def ( PC_BAD_REQ_TYPE,   				(PC_START+11),		"|en|The parameter req_type is not set properly")
rcode_def ( PC_BAD_BLOCK_HANDLE,			(PC_START+12),		"|en|The block_handle parameter is invalid")
rcode_def ( PC_NO_DEV_VALUE_WRITE,			(PC_START+13),		"|en|The device values cannot be written")
rcode_def ( PC_NO_ACTION_VALUE,				(PC_START+14),		"|en|No action value exists")
rcode_def ( PC_PARAM_CANNOT_BE_WRITTEN,		(PC_START+15),		"|en|The param is of a class which cannot be written to a device")
rcode_def ( PC_PARAM_CANNOT_BE_READ,		(PC_START+16),		"|en|The param is of a class which cannot be read from a device")
rcode_def ( PC_BAD_POINTER,					(PC_START+17),		"|en|A passed parameter which is a pointer is NULL")
rcode_def ( PC_BAD_METHOD_TAG,				(PC_START+18),		"|en|status_info set to USE_METH_VALUE but invalid method_tag")
rcode_def ( PC_BAD_PARAM_REQUEST,			(PC_START+19),		"|en|value access restricted to single parameter values")
rcode_def ( PC_NO_DATA_TO_SEND,				(PC_START+20),		"|en|the requested param to send has not been changed")
rcode_def ( PC_ERROR_END,					(PC_START+99),	"|en|end of Parameter Cache error numbers")

/* layer 7 errors */
rcode_def ( L7_ERROR_START,                 (L7_START+1),   "|en|start of Layer 7 error numbers")
rcode_def ( L7_INVALID_VAR_TYPE,            (L7_START+2),   "|en|var_type not an element of enum VAR_TYPE")
rcode_def ( L7_INVALID_DD_ITEM_TYPE,        (L7_START+3),   "|en|DDLI passed L7 Invalid Item Type")
rcode_def ( L7_ALREADY_ACTIVE,              (L7_START+4),   "|en|layer7 is not reentrant") /* NOT USED?? */
rcode_def ( L7_INVALID_FUNCTION_PTR,        (L7_START+5),   "|en|pointer to function was not initialized")
rcode_def ( L7_NO_RESPONSE,                 (L7_START+6),   "|en|field device did not respond to a layer7 request")
rcode_def ( L7_LOCAL_COMM_ERROR,            (L7_START+7),   "|en|Comm error associated with host comm port ")
rcode_def ( L7_REMOTE_COMM_ERROR,           (L7_START+8),   "|en|Comm error associated with field device comm port")
rcode_def ( L7_DEVICE_BUSY,                 (L7_START+9),   "|en|field device can not be interrupted")
rcode_def ( L7_CMD_NOT_IMPLEMENTED,         (L7_START+10),   "|en|field device does not support current command")
rcode_def ( L7_DEVICE_ERROR,                (L7_START+11),   "|en|Device error occurred during identification")
rcode_def ( L7_INVALID_RESPONSE_CODE,       (L7_START+12),   "|en|response code not found in command description")
rcode_def ( L7_UNKNOWN_DEVICE,              (L7_START+13),   "|en|netman: device does not exist")
rcode_def ( L7_NO_VAR_MATCH,                (L7_START+14),   "|en|variable not found in cmd trans") /*NOT USED??*/
rcode_def ( L7_NO_READ_COMMAND,             (L7_START+15),   "|en|No read command for variable")
rcode_def ( L7_NO_WRITE_COMMAND,            (L7_START+16),   "|en|No Write Command for Variable")
rcode_def ( L7_NO_COMMAND,                  (L7_START+17),   "|en|No Command description")
rcode_def ( L7_INVALID_REQ_DATA_FIELD,      (L7_START+18),   "|en|DD specifies too many request data bytes")
rcode_def ( L7_INVALID_REPLY_DATA_FIELD,    (L7_START+19),   "|en|DD expecting more data bytes in response")
rcode_def ( L7_INVALID_NUM_OF_REQ_ITEMS,    (L7_START+20),   "|en|Too many request items")
rcode_def ( L7_INVALID_NUM_OF_REPLY_ITEMS,  (L7_START+21),   "|en|Too many reply items")
rcode_def ( L7_INVALID_VARIABLE_LENGTH,     (L7_START+22),   "|en|Variable is larger than expected")
rcode_def ( L7_INVALID_RESP_CODE_COUNT,     (L7_START+23),   "|en|Too many response code items")
rcode_def ( L7_INVALID_COMM_STAT_COUNT,     (L7_START+24),   "|en|Too many communication status items")
rcode_def ( L7_INVALID_DEVICE_STAT_COUNT,   (L7_START+25),   "|en|Too many device_status items")
rcode_def ( L7_DDI_NO_MATCH,                (L7_START+26),   "|en|Variable not found in command list")
rcode_def ( L7_INVALID_RESPONSE_CMD,        (L7_START+27),   "|en|Response command does not equal request command" )
rcode_def ( L7_POLL_ADDR_ALREADY_USED,      (L7_START+28),   "|en|Poll address is already used" )
rcode_def ( L7_WARNING_DATA_EXPECTED,       (L7_START+29),   "|en|Too few data bytes received")
rcode_def ( L7_TRANSMITTER_FAULT,           (L7_START+30),   "|en|Transmitter fault")
rcode_def ( L7_INVALID_TRANSACTION_NUMBER,  (L7_START+31),   "|en|Invalid transaction number")
rcode_def ( L7_INVALID_DEVICE_HANDLE,  		(L7_START+31),   "|en|Invalid device handle")
rcode_def ( L7_INVALID_BLOCK_HANDLE,  		(L7_START+32),   "|en|Invalid block handle")
 
rcode_def ( L7_ERROR_END,                   (L7_START+99),   "|en|end of Layer 7 error numbers")

/* network management errors */
rcode_def ( NM_ERROR_START,                 (NM_START+1),   "|en|start of netman error numbers")
rcode_def ( HC_UNKNOWN_DEVICE,              (NM_START+2),   "|en|data transmission error")
rcode_def ( HC_MSG_NOT_SENT,                (NM_START+3),   "|en|DLL was not able to send request")
rcode_def ( HC_NO_RESPONSE,                 (NM_START+4),   "|en|field device did not respond")
rcode_def ( HC_COMM_ERROR,                  (NM_START+5),   "|en|Comm error associated with host comm port")
rcode_def ( HC_REMOTE_COMM_ERROR,           (NM_START+6),   "|en|Comm error associated with field device comm port")
rcode_def ( HC_INVALID_CMD,                 (NM_START+7),   "|en|field device does not support this command")
rcode_def ( HC_DEVICE_ERROR,                (NM_START+8),   "|en|bits 0-5 of first response code byte were set")
rcode_def ( HC_INVALID_PARAM,               (NM_START+9),   "|en|invalid_parameter")
rcode_def ( HC_NO_MEM,                      (NM_START+10),   "|en|Memory exhausted")
rcode_def ( HC_ACTIVE ,                     (NM_START+11),   "|en|Communications port already initialized")
rcode_def ( HC_INACTIVE,                    (NM_START+12),   "|en|Communications port not initialized")
rcode_def ( HC_NO_PROCESS,                  (NM_START+13),   "|en|All communications ports active")
rcode_def ( HC_CLEANUP,                     (NM_START+14),   "|en|Installation of cleanup procedures failed ")
rcode_def ( HC_EXIST,                       (NM_START+15),   "|en|Master address has been initilized")
rcode_def ( HC_UART_FAILURE,                (NM_START+16),   "|en|Uart initialization error")
rcode_def ( HC_CMD_NOT_IMPLEMENTED,         (NM_START+17),   "|en|Command not implemented")
rcode_def ( HC_DEVICE_BUSY,                 (NM_START+18),   "|en|Device is currently busy")
rcode_def ( HC_RE_IDENT,                    (NM_START+19),   "|en|Attempt to re-identify device")
rcode_def ( HC_UPDATE_KNOWN_DEVICE,         (NM_START+20),   "|en|Update netman table for existing device")
rcode_def ( HC_HANDLE_UNAVAILABLE,          (NM_START+21),   "|en|DDLI was not able to provide a handle")
rcode_def ( NM_UNKNOWN_VARIABLE,            (NM_START+22),   "|en|Variable not in netman table")
rcode_def ( NM_UNKNOWN_TABLE,               (NM_START+23),   "|en|No netman table for specified handle")
rcode_def ( HC_DEVICE_DISCONNECTED,         (NM_START+24),   "|en|Device has been disconnected")
rcode_def ( NM_ERROR_END,                   (NM_START+99),   "|en|end of netman error numbers")
 
/* Comm Stack errors */
rcode_def ( CS_ERROR_START,                 (CS_START+1),    "|en|start of comm. stack error numbers")
rcode_def ( PS_SUCCESS,                     0,               "|en|Success")
rcode_def ( PS_INVALID_BLOCK_HANDLE,        (CS_START+2),    "|en|Provider Service invalid block handle")
rcode_def ( PS_INVALID_DEVICE_HANDLE,       (CS_START+3),    "|en|Provider Service invalid device handle")

rcode_def ( CS_ERROR_END,                   (CS_START+99),   "|en|end of comm. stack error numbers")

/* Connection Manager Errors */
rcode_def ( CM_SUCCESS,						0,				"|en|Success")
rcode_def ( CM_NO_MEMORY,					(CM_START+0),	"|en|CM No memory")
rcode_def ( CM_BLOCK_NOT_FOUND,				(CM_START+1),	"|en|CM Block not found")
rcode_def ( CM_DEVICE_NOT_FOUND,			(CM_START+2),	"|en|CM Device not found")
rcode_def ( CM_DEVICE_TYPE_NOT_FOUND,		(CM_START+3),	"|en|CM Device type not found")
rcode_def ( CM_BAD_BLOCK_OPEN,				(CM_START+4),	"|en|CM Bad block open")
rcode_def ( CM_BAD_BLOCK_CLOSE,				(CM_START+5),	"|en|CM Bad block close")
rcode_def ( CM_BAD_BLOCK_HANDLE,			(CM_START+6),	"|en|CM Bad block handle")
rcode_def ( CM_BAD_PARAM_OFFSET,			(CM_START+7),	"|en|CM Bad parameter offset")
rcode_def ( CM_BAD_INDEX,					(CM_START+8),	"|en|CM Bad index")
rcode_def ( CM_BAD_OPEN,					(CM_START+9),	"|en|CM Bad open")
rcode_def ( CM_NOT_DDOD_FILE,				(CM_START+10),	"|en|CM Not DDOD file")
rcode_def ( CM_FILE_ERROR,					(CM_START+11),	"|en|CM File error")
rcode_def ( CM_NO_DEVICE,					(CM_START+12),	"|en|CM No device")
rcode_def ( CM_NO_BLOCK,					(CM_START+13),	"|en|CM No block")
rcode_def ( CM_BAD_BLOCK_OBJECT,			(CM_START+14),	"|en|CM Bad block object")
rcode_def ( CM_NO_FUNCTION,					(CM_START+15),	"|en|CM No function")
rcode_def ( CM_BAD_SYMBOL,					(CM_START+16),	"|en|CM Bad symbol")
rcode_def ( CM_BAD_DEVICE_HANDLE,			(CM_START+17),	"|en|CM Bad device handle")
rcode_def ( CM_NO_COMM,						(CM_START+18),	"|en|CM No communications")
rcode_def ( CM_BAD_ROD_OPEN,				(CM_START+19),	"|en|CM Bad ROD open")
rcode_def ( CM_BLOCK_TAG_NOT_FOUND,			(CM_START+20),	"|en|CM Block tag not found")
rcode_def ( CM_INVALID_ROD_HANDLE,			(CM_START+21),	"|en|CM Invalid ROD handle")
rcode_def ( CM_ILLEGAL_PUT,					(CM_START+22),	"|en|CM Illegal put")
rcode_def ( CM_NO_ROD_MEMORY,				(CM_START+23),	"|en|CM No ROD memory")
rcode_def ( CM_OBJECT_NOT_IN_HEAP,			(CM_START+24),	"|en|CM Object not in heap")
rcode_def ( CM_NO_OBJECT,					(CM_START+25),	"|en|CM No object")
rcode_def ( CM_NO_VALUE,					(CM_START+26),	"|en|CM No value")
rcode_def ( CM_BAD_DD_BLK_LOAD,				(CM_START+27),	"|en|CM Bad DD block load")
rcode_def ( CM_BAD_DD_DEV_LOAD,				(CM_START+28),	"|en|CM Bad DD device load")
rcode_def ( CM_NO_DD_BLK_REF,				(CM_START+29),	"|en|CM No DD block reference")
rcode_def ( CM_FILE_NOT_FOUND,				(CM_START+30),	"|en|CM File not found")
rcode_def ( CM_BAD_DD_FLAT_LOAD,			(CM_START+31),	"|en|CM Bad DD flat load")
rcode_def ( CM_BAD_DDOD_LOAD,				(CM_START+32),	"|en|CM Bad DDOD load")
rcode_def ( CM_BAD_DD_HANDLE,				(CM_START+33),	"|en|CM Bad DD handle")
rcode_def ( CM_BAD_BLOCK_LOAD,				(CM_START+34),	"|en|CM Bad block load")
rcode_def ( CM_ILLEGAL_WRITE,				(CM_START+35),	"|en|CM Illegal write")
rcode_def ( CM_VALUE_NOT_IN_HEAP,			(CM_START+36),	"|en|CM Value not in heap")
rcode_def ( CM_NO_DATA_TYPE,				(CM_START+37),	"|en|CM No data type")
rcode_def ( CM_BAD_OBJECT,					(CM_START+38),	"|en|CM Bad object")
rcode_def ( CM_BAD_SUBINDEX,				(CM_START+39),	"|en|CM Bad subindex")
rcode_def ( CM_BAD_OPROD_OPEN,				(CM_START+40),	"|en|CM Bad OPROD open")
rcode_def ( CM_BAD_FILE_OPEN,				(CM_START+41),	"|en|CM Bad file open")
rcode_def ( CM_BAD_FILE_CLOSE,				(CM_START+42),	"|en|CM Bad file close")
rcode_def ( CM_COMM_ERR,					(CM_START+43),	"|en|CM Communications error")
rcode_def ( CM_RESPONSE_CODE,				(CM_START+44),	"|en|CM Response code")
rcode_def ( CM_BAD_COMM_TYPE,				(CM_START+45),	"|en|CM Bad communications type")
rcode_def ( CM_FF_PARAM_LOAD_FAILED,		(CM_START+46),	"|en|CM FF parameter load failed")
rcode_def ( CM_FF_PARAM_WRITE_FAILED,		(CM_START+47),	"|en|CM FF parameter write failed")
rcode_def ( CM_FF_PARAM_READ_FAILED,		(CM_START+48),	"|en|CM FF parameter read failed")
rcode_def ( CM_BAD_POINTER,					(CM_START+49),	"|en|CM Bad pointer")
rcode_def ( CM_INTERNAL_ERROR,				(CM_START+50),	"|en|CM Internal error")
rcode_def ( CM_BAD_NETWORK_HANDLE,			(CM_START+51),	"|en|CM Bad network handle")
rcode_def ( CM_BAD_DEVICE_TYPE_HANDLE,		(CM_START+52),	"|en|CM Bad device type handle")
rcode_def ( CM_BAD_DD_PATH,					(CM_START+53),	"|en|CM Bad DD path")
rcode_def ( CM_BAD_CM_MODE,					(CM_START+54),	"|en|CM Bad CM mode")
rcode_def ( CM_BAD_THREAD_HANDLE,           (CM_START+55),	"|en|CM Bad Thread Handle")
rcode_def ( CM_BAD_MUTEX,                   (CM_START+56),  "|en|CM Failed to create Thread Mutex")
rcode_def ( CM_BAD_RESET,                   (CM_START+57),  "|en|CM Failed to reset communication manager")
rcode_def ( CM_BAD_VCR,                     (CM_START+58),  "|en|CM No VCR or BAD VCR entry found")
rcode_def ( CM_INVALID_SM_DIR,              (CM_START+59),  "|en|CM Invalid SM directory")
rcode_def ( CM_INVALID_VCR_CHAR,            (CM_START+60),  "|en|CM Invalid VCR characteristics")
rcode_def ( CM_BAD_MANUFACTURER,            (CM_START+61),  "|en|CM Invalid Mnufacturer")
rcode_def ( CM_BAD_DEV_TYPE,                (CM_START+62),  "|en|CM Invalid Device Type")
rcode_def ( CM_BAD_DEV_REV,                 (CM_START+63),  "|en|CM Invalid Device Revision")
rcode_def ( CM_BAD_DD_REV,                  (CM_START+64),  "|en|CM Invalid DD Revision")
rcode_def ( CM_BAD_CYCLE_TIME,              (CM_START+65),  "|en|CM Invalid Min Cycle Time")
rcode_def ( CM_INVALID_NM_DIR,              (CM_START+66),  "|en|CM Invalid NM Directory")
rcode_def ( CM_SIM_NO_FILE,                 (CM_START+67),  "|en|CM Error in opening Fake Transmitter file")
rcode_def ( CM_BAD_MESSAGE_Q,               (CM_START+68),  "|en|CM Bad message queue")
rcode_def ( CM_BAD_CLIENT_SOCKET,           (CM_START+69),  "|en|CM Bad Client socket")
rcode_def ( CM_BAD_BIND_SOCKET,             (CM_START+70),  "|en|CM Failed to bind with socket")
rcode_def ( CM_BAD_RECVFROM,                (CM_START+71),  "|en|CM Failed to read from socket")
rcode_def ( CM_BAD_SOCKET,					(CM_START+72),	"|en|CM Bad socket creation")
rcode_def ( CM_DPC_BUSY,					(CM_START+76),	"|en|CM DPC is busy, please try later")
rcode_def ( CM_READ_TIMEOUT,				(CM_START+77),	"|en|CM Read Time out")
rcode_def ( CM_WRITE_TIMEOUT,				(CM_START+78),	"|en|CM Write Time out")

rcode_def ( CM_ERROR_END,					(CM_START+99),	"|en|End of Connection Manager error numbers")

/* FF Device Simulator Errors */
rcode_def ( FFDS_SUCCESS,					0,				"|en|Success")
rcode_def ( FFDS_MEMORY_ERR,				(FFDS_START+0),	"|en|FFDS Memory error")
rcode_def ( FFDS_FILE_OPEN_ERR,			    (FFDS_START+1),	"|en|FFDS File open error")
rcode_def ( FFDS_FILE_READ_ERR,			    (FFDS_START+2),	"|en|FFDS File read error")
rcode_def ( FFDS_FILE_CLOSE_ERR,			(FFDS_START+3),	"|en|FFDS File close error")
rcode_def ( FFDS_MULT_BLK_ERR,				(FFDS_START+4),	"|en|FFDS Multiple block error")
rcode_def ( FFDS_MULT_DEV_ERR,				(FFDS_START+5),	"|en|FFDS Multiple device error")
rcode_def ( FFDS_BLK_CLASS_ERR,			    (FFDS_START+6),	"|en|FFDS Block class error")
rcode_def ( FFDS_PHYS_BLK_ERR,				(FFDS_START+7),	"|en|FFDS Physical block error")
rcode_def ( FFDS_SYM_FILE_ERR,				(FFDS_START+8),	"|en|FFDS Symbol file error")
rcode_def ( FFDS_SYMBOL_ERR,				(FFDS_START+9),	"|en|FFDS Symbol error")
rcode_def ( FFDS_DEV_HANDLE_ERR,			(FFDS_START+10),	"|en|FFDS Device handle error")
rcode_def ( FFDS_ROD_ERR,					(FFDS_START+11),	"|en|FFDS ROD error")
rcode_def ( FFDS_COMM_ERR,					(FFDS_START+12),	"|en|FFDS Communications error")
rcode_def ( FFDS_FIND_BLK_ERR,				(FFDS_START+13),	"|en|FFDS Find block error")
rcode_def ( FFDS_CFG_FILE_ID_ERR,			(FFDS_START+14),	"|en|FFDS Configuration file id error")
rcode_def ( FFDS_PUT_INT_ERR,				(FFDS_START+15),	"|en|FFDS Put integer error")
rcode_def ( FFDS_OBJECT_ERR,				(FFDS_START+16),	"|en|FFDS Object error")
rcode_def ( FFDS_MULT_DEV_INIT_ERR, 		(FFDS_START+17),	"|en|FFDS Multiple Device initialization")
rcode_def ( FFDS_INVALID_BLOCK_MODE,		(FFDS_START+18),	"|en|FFDS Invalid Block Mode")
rcode_def ( FFDS_OD_CONFLICT_ERR,			(FFDS_START+19),	"|en|FFDS OD conflict error")
rcode_def ( FFDS_ERROR_END,				    (FFDS_START+99),	"|en|End of Device Simulator error numbers")


/* RCSIM return types. */
rcode_def ( RCSIM_SUCCESS, 	 				0,				"|en|Success")
rcode_def ( RCSIM_MATCH, 	 				(RCSIM_START+0),	"|en|RCSIM Match found")
rcode_def ( RCSIM_NOMATCH, 	 				(RCSIM_START+5),	"|en|RCSIM No match found")
rcode_def ( RCSIM_END_OF_LIST,				(RCSIM_START+6),	"|en|RCSIM End of response code list")
rcode_def ( RCSIM_NO_ITEMS,					(RCSIM_START+7),	"|en|RCSIM No response codes in list")
rcode_def ( RCSIM_ERR_CLASS_FIND, 	 		(RCSIM_START+10),	"|en|RCSIM Response message type not found")
rcode_def ( RCSIM_ERR_NETWORK_FIND, 	 	(RCSIM_START+11),	"|en|RCSIM Network not found in network file")
rcode_def ( RCSIM_ERR_STATION_FIND, 	 	(RCSIM_START+12),	"|en|RCSIM Station not found in sim file")
rcode_def ( RCSIM_ERR_MEMORY, 	 			(RCSIM_START+20),	"|en|RCSIM Memory error")
rcode_def ( RCSIM_ERR_NET_ID, 	 			(RCSIM_START+21),	"|en|RCSIM Invalid network handle")
rcode_def ( RCSIM_ERR_CLASS, 	 			(RCSIM_START+22),	"|en|RCSIM Invalid rcsim message type")
rcode_def ( RCSIM_ERR_VALUE_TYPE, 	 		(RCSIM_START+23),	"|en|RCSIM Invalid response code value type")
rcode_def ( RCSIM_ERR_NET_FILE_OPEN, 	 	(RCSIM_START+24),	"|en|RCSIM Network data file open error")
rcode_def ( RCSIM_ERR_NET_FILE_READ, 	 	(RCSIM_START+25),	"|en|RCSIM Network data file read error")
rcode_def ( RCSIM_ERR_NET_FILE_CLOSE, 	 	(RCSIM_START+26),	"|en|RCSIM Network data file close error")
rcode_def ( RCSIM_ERR_SIM_FILE_OPEN, 	 	(RCSIM_START+27),	"|en|RCSIM Sim data file open error")
rcode_def ( RCSIM_ERR_SIM_FILE_READ, 	 	(RCSIM_START+28),	"|en|RCSIM Sim data file read error")
rcode_def ( RCSIM_ERR_SIM_FILE_CLOSE, 	 	(RCSIM_START+29),	"|en|RCSIM Sim data file close error")
rcode_def ( RCSIM_ERROR_END, 	 			(RCSIM_START+99),	"|en|End of RCSIM Simulator error numbers")

/* Symbol File error codes - numbers match those in AMS rtn_def.h */
rcode_def (	SM_SUCCESS,						(0),				"|en|SM Success")
rcode_def (	SM_SYMFILE_OPEN_ERR,			(SM_START+30),		"|en|SM Symbol file open error")
rcode_def (	SM_ID_NOT_FOUND,				(SM_START+31),		"|en|SM Item ID not found")
rcode_def (	SM_NAME_NOT_FOUND,				(SM_START+32),		"|en|SM Item name not found")
rcode_def (	SM_NO_SYMFILE_FOUND,			(SM_START+33),		"|en|SM No symbol file found")

/* Connection Manager Interface errors */
rcode_def ( CM_IF_OK,                       (CM_IF_START + 0),  "|en|CM Interface success")
rcode_def ( CM_IF_FATAL_ERROR,              (CM_IF_START + 7),  "|en|CM Unrecoverable error on FBK board")
rcode_def ( CM_IF_DOWNLOAD_FIRMWARE,        (CM_IF_START + 8),  "|en|CM Download firmware error")
rcode_def ( CM_IF_INVALID_CHANNEL_ID,       (CM_IF_START + 9),  "|en|CM Invalid channel ID")
rcode_def ( CM_IF_NO_DEVICE,                (CM_IF_START + 10), "|en|CM Bad TTY port or No FBK board")
rcode_def ( CM_IF_DEVICE_IN_USE,            (CM_IF_START + 11), "|en|CM Device is in use/busy")
rcode_def ( CM_IF_INVALID_LAYER,            (CM_IF_START + 12), "|en|CM Invalid Service layer")
rcode_def ( CM_IF_INVALID_SERVICE,          (CM_IF_START + 13), "|en|CM Invalid Service Identifier")
rcode_def ( CM_IF_INVALID_PRIMITIVE,        (CM_IF_START + 14), "|en|CM Invalid Service Primitive")
rcode_def ( CM_IF_INVALID_DATA_SIZE,        (CM_IF_START + 15), "|en|CM Not enough cmi data block memory")
rcode_def ( CM_IF_INVALID_COMM_REF,         (CM_IF_START + 16), "|en|CM Invalid Communication Reference number")
rcode_def ( CM_IF_COM_ERROR,                (CM_IF_START + 20), "|en|CM error occured in COM send/rx")
rcode_def ( CM_IF_RESOURCE_UNAVAILABLE,     (CM_IF_START + 21), "|en|CM No Resource available")
rcode_def ( CM_IF_NO_PARALLEL_SERVICES,     (CM_IF_START + 22), "|en|CM no parallel services allowed")
rcode_def ( CM_IF_SERVICE_CONSTR_CONFLICT,  (CM_IF_START + 23), "|en|CM Service. temporary. not executable")
rcode_def ( CM_IF_SERVICE_NOT_SUPPORTED,    (CM_IF_START + 24), "|en|CM Service not supported")
rcode_def ( CM_IF_SERVICE_NOT_EXECUTABLE,   (CM_IF_START + 25), "|en|CM Service not executable")
rcode_def ( CM_IF_INVALID_PARAMETER,        (CM_IF_START + 30), "|en|CM wrong parameter in REQ or RES")
rcode_def ( CM_IF_INIT_FAILED,              (CM_IF_START + 31), "|en|CM init. API or Controller failed")
rcode_def ( CM_IF_EXIT_FAILED,              (CM_IF_START + 32), "|en|CM Exit. API or Controller failed")
rcode_def ( CM_IF_API_NOT_INITIALIZED,      (CM_IF_START + 33), "|en|CM API not initialized")
rcode_def ( CM_IF_OS_ERROR,                 (CM_IF_START + 255),"|en|CM OS system (WIN,DOS) error")

/* Communication stack FMS errors */
rcode_def ( CS_FMS_INIT,                         (CS_FMS_START + 0x0000), "|en|CS FMS initialization error")
rcode_def ( CS_FMS_INIT_OTHER,                   (CS_FMS_START + 0x0000), "|en|CS FMS initialization error")
rcode_def ( CS_FMS_INIT_MAX_PDU_SIZE_INSUFF,     (CS_FMS_START + 0x0001), "|en|CS FMS Max PDU size error")
rcode_def ( CS_FMS_INIT_FEAT_NOT_SUPPORTED,      (CS_FMS_START + 0x0002), "|en|CS FMS FEAT not supported")
rcode_def ( CS_FMS_INIT_OD_VERSION_INCOMP,       (CS_FMS_START + 0x0003), "|en|CS OD Version in compatible")
rcode_def ( CS_FMS_INIT_USER_DENIED,             (CS_FMS_START + 0x0004), "|en|CS FMS user defined error")
rcode_def ( CS_FMS_INIT_PASSWORD_ERROR,          (CS_FMS_START + 0x0005), "|en|CS FMS Password error")
rcode_def ( CS_FMS_INIT_PROFILE_NUMB_INCOMP,     (CS_FMS_START + 0x0006), "|en|CS FMS Profile number incompatible")
                                                                               
rcode_def ( CS_FMS_VFD_STATE_OTHER,              (CS_FMS_START + 0x0100), "|en|CS FMS VFD state error")                              
                                                                               
rcode_def ( CS_FMS_APPLICATION_OTHER,            (CS_FMS_START + 0x0200), "|en|CS FMS Application error")
rcode_def ( CS_FMS_APPLICATION_UNREACHABLE,      (CS_FMS_START + 0x0201), "|en|CS FMS Application unreachable")
                                                                               
rcode_def ( CS_FMS_DEF_OTHER,                    (CS_FMS_START + 0x0300), "|en|CS FMS Definition error")
rcode_def ( CS_FMS_DEF_OBJ_UNDEF,                (CS_FMS_START + 0x0301), "|en|CS FMS object undefined")
rcode_def ( CS_FMS_DEF_OBJ_ATTR_INCONSIST,       (CS_FMS_START + 0x0302), "|en|CS FMS object attribute inconsistent")
rcode_def ( CS_FMS_DEF_OBJECT_ALREADY_EXISTS,    (CS_FMS_START + 0x0303), "|en|CS FMS object already exist")
                                                                               
rcode_def ( CS_FMS_RESOURCE_OTHER,               (CS_FMS_START + 0x0400), "|en|CS FMS Resource error")
rcode_def ( CS_FMS_RESOURCE_MEM_UNAVAILABLE,     (CS_FMS_START + 0x0401), "|en|CS FMS Resource memory unavilable")
                                                                               
rcode_def ( CS_FMS_SERV_OTHER,                   (CS_FMS_START + 0x0500), "|en|CS FMS Service error")                              
rcode_def ( CS_FMS_SERV_OBJ_STATE_CONFLICT,      (CS_FMS_START + 0x0501), "|en|CS FMS service object state conflict")
rcode_def ( CS_FMS_SERV_PDU_SIZE,                (CS_FMS_START + 0x0502), "|en|CS FMS service PDU size error")
rcode_def ( CS_FMS_SERV_OBJ_CONSTR_CONFLICT,     (CS_FMS_START + 0x0503), "|en|CS FMS service onject conflict")
rcode_def ( CS_FMS_SERV_PARAM_INCONSIST,         (CS_FMS_START + 0x0504), "|en|CS FMS service parameter inconsisten")
rcode_def ( CS_FMS_SERV_ILLEGAL_PARAM,           (CS_FMS_START + 0x0505), "|en|CS FMS service illegal parameter")
                                                                               
rcode_def ( CS_FMS_ACCESS_OTHER,                 (CS_FMS_START + 0x0600), "|en|CS FMS access error")
rcode_def ( CS_FMS_ACCESS_OBJ_INVALIDATED,       (CS_FMS_START + 0x0601), "|en|CS FMS access object invalidated")
rcode_def ( CS_FMS_ACCESS_HARDWARE_FAULT,        (CS_FMS_START + 0x0602), "|en|CS FMS access Hardware fault")
rcode_def ( CS_FMS_ACCESS_OBJ_ACCESS_DENIED,     (CS_FMS_START + 0x0603), "|en|CS FMS access denied")
rcode_def ( CS_FMS_ACCESS_ADDR_INVALID,          (CS_FMS_START + 0x0604), "|en|CS FMS access invalid address")
rcode_def ( CS_FMS_ACCESS_OBJ_ATTR_INCONST,      (CS_FMS_START + 0x0605), "|en|CS FMS access object attribute inconsistent")
rcode_def ( CS_FMS_ACCESS_OBJ_ACCESS_UNSUPP,     (CS_FMS_START + 0x0606), "|en|CS FMS access not spported")
rcode_def ( CS_FMS_ACCESS_OBJ_NON_EXIST,         (CS_FMS_START + 0x0607), "|en|CS FMS access object not exist")
rcode_def ( CS_FMS_ACCESS_TYPE_CONFLICT,         (CS_FMS_START + 0x0608), "|en|CS FMS access type conflict")
rcode_def ( CS_FMS_ACCESS_NAME_ACCESS_UNSUP,     (CS_FMS_START + 0x0609), "|en|CS FMS name access no supported")
rcode_def ( CS_FMS_ACCESS_TO_ELEMENT_UNSUPP,     (CS_FMS_START + 0x060A), "|en|CS FMS access to element not supported")

rcode_def ( CS_FMS_OD_OTHER,                     (CS_FMS_START + 0x0700), "|en|CS FMS OD error")
rcode_def ( CS_FMS_OD_NAME_LEN_OVERFLOW,         (CS_FMS_START + 0x0701), "|en|CS FMS OD name length overflow")
rcode_def ( CS_FMS_OD_OVERFLOW,                  (CS_FMS_START + 0x0702), "|en|CS FMS OD overflow")
rcode_def ( CS_FMS_OD_WRITE_PROTECT,             (CS_FMS_START + 0x0703), "|en|CS FMS OD write protect")
rcode_def ( CS_FMS_OD_EXTENSION_LEN_OVERFLOW,    (CS_FMS_START + 0x0704), "|en|CS FMS OD extension len overflow")
rcode_def ( CS_FMS_OD_OBJ_DESCR_OVERFLOW,        (CS_FMS_START + 0x0705), "|en|CS FMS OD object description overflow")
rcode_def ( CS_FMS_OD_OPERAT_PROBLEM,            (CS_FMS_START + 0x0706), "|en|CS FMS OD operation problem")
                                                                               
rcode_def ( CS_FMS_OTHER,                        (CS_FMS_START + 0x0800), "|en|CS FMS other error")

/* Communication stack NMA errors */

rcode_def ( CS_NMA_APPLICATION_OTHER,            (CS_NMA_START + 0x0100), "|en|CS NMA Application error")
rcode_def ( CS_NMA_APPLICATION_UNREACHABLE,      (CS_NMA_START + 0x0101), "|en|CS NMA Application unreachable")
                                                                               
rcode_def ( CS_NMA_RESOURCE_OTHER,               (CS_NMA_START + 0x0200), "|en|CS NMA Resource error")
rcode_def ( CS_NMA_RESOURCE_MEM_UNAVAILABLE,     (CS_NMA_START + 0x0201), "|en|CS NMA Resource memory error")
                                                                               
rcode_def ( CS_NMA_SERV_OTHER,                   (CS_NMA_START + 0x0300), "|en|CS NMA service error")
rcode_def ( CS_NMA_SERV_OBJ_STATE_CONFLICT,      (CS_NMA_START + 0x0301), "|en|CS NMA service object conflict")
rcode_def ( CS_NMA_SERV_OBJ_CONSTR_CONFLICT,     (CS_NMA_START + 0x0302), "|en|CS NMA service conflict")
rcode_def ( CS_NMA_SERV_PARAM_INCONSIST,         (CS_NMA_START + 0x0303), "|en|CS NMA service parameter inconsistent")
rcode_def ( CS_NMA_SERV_ILLEGAL_PARAM,           (CS_NMA_START + 0x0304), "|en|CS NMA service illegal parameter")
rcode_def ( CS_NMA_SERV_PERM_INTERN_FAULT,       (CS_NMA_START + 0x0305), "|en|CS NMA service permanent internal fault")
                                                                               
rcode_def ( CS_NMA_USR_OTHER,                    (CS_NMA_START + 0x0400), "|en|CS NMA user error")
rcode_def ( CS_NMA_USR_DONT_WORRY_BE_HAPPY,      (CS_NMA_START + 0x0401), "|en|CS NMA user error")
rcode_def ( CS_NMA_USR_MEM_UNAVAILABLE,          (CS_NMA_START + 0x0402), "|en|CS NMA user memory error")
                                                                               
rcode_def ( CS_NMA_ACCESS_OTHER,                 (CS_NMA_START + 0x0500), "|en|CS NMA access error")
rcode_def ( CS_NMA_ACCESS_OBJ_ACC_UNSUP,         (CS_NMA_START + 0x0501), "|en|CS NMA object access not supported")
rcode_def ( CS_NMA_ACCESS_OBJ_NON_EXIST,         (CS_NMA_START + 0x0502), "|en|CS NMA object does not exist")
rcode_def ( CS_NMA_ACCESS_OBJ_ACCESS_DENIED,     (CS_NMA_START + 0x0503), "|en|CS NMA object access denied")
rcode_def ( CS_NMA_ACCESS_HARDWARE_FAULT,        (CS_NMA_START + 0x0504), "|en|CS NMA access hardware fault")
rcode_def ( CS_NMA_ACCESS_TYPE_CONFLICT,         (CS_NMA_START + 0x0505), "|en|CS NMA access type conflict")
                                                                               
rcode_def ( CS_NMA_CRL_OTHER,                    (CS_NMA_START + 0x0600), "|en|CS NMA Communication ref error")
rcode_def ( CS_NMA_CRL_INVALID_ENTRY,            (CS_NMA_START + 0x0601), "|en|CS NMA CR invalid entry")
rcode_def ( CS_NMA_CRL_NO_CRL_ENTRY,             (CS_NMA_START + 0x0602), "|en|CS NMA No CR entry found")
rcode_def ( CS_NMA_CRL_INVALID_CRL,              (CS_NMA_START + 0x0603), "|en|CS NMA invalid CR")
rcode_def ( CS_NMA_CRL_NO_CRL,                   (CS_NMA_START + 0x0604), "|en|CS NMA No CR")
rcode_def ( CS_NMA_CRL_WRITE_PROTECTED,          (CS_NMA_START + 0x0605), "|en|CS NMA CR write protected")
rcode_def ( CS_NMA_CRL_NO_ENTRY_FOUND,           (CS_NMA_START + 0x0606), "|en|CS NMA No CR entry found")
rcode_def ( CS_NMA_CRL_NO_MULT_VFD_SUPP,         (CS_NMA_START + 0x0607), "|en|CS NMA No multiple VFD supported")
                                                                               
rcode_def ( CS_NMA_OTHER,                        (CS_NMA_START + 0x0700), "|en|CS NMA other error")

/* Communication stack SM errors */

rcode_def ( CS_SM_INVALID_STATE,                 (CS_SM_START + 0x01), "|en|CS SM Remote error invalid state")
rcode_def ( CS_SM_WRONG_PD_TAG,                  (CS_SM_START + 0x02), "|en|CS SM Remote error PD Tag not match")
rcode_def ( CS_SM_WRONG_DEVICE_ID,               (CS_SM_START + 0x03), "|en|CS SM Remote error device id not match")
rcode_def ( CS_SM_WRITE_FAILED,                  (CS_SM_START + 0x04), "|en|CS SM Remote error SMIB object write failed")    
rcode_def ( CS_SM_SM_NOT_TO_OP,                  (CS_SM_START + 0x05), "|en|CS SM Remote error starting SM operational")    
rcode_def ( CS_SM_DLL_NO_RESOURCE,               (CS_SM_START + 0x10), "|en|CS SM DLL error insufficient resources")   
rcode_def ( CS_SM_DLL_QUEUE_FULL,                (CS_SM_START + 0x11), "|en|CS SM DLL error send queue full")    
rcode_def ( CS_SM_DLL_TIMEOUT,                   (CS_SM_START + 0x12), "|en|CS SM DLL error timeout")   
rcode_def ( CS_SM_DLL_UNKNOWN,                   (CS_SM_START + 0x13), "|en|CS SM DLL error unspecified reason")    
rcode_def ( CS_SM_SERV_NOT_SUPPORTED,            (CS_SM_START + 0x20), "|en|CS SM Service not supported")    
rcode_def ( CS_SM_PARAM_IV,                      (CS_SM_START + 0x21), "|en|CS SM Parameter invalid")    
rcode_def ( CS_SM_NR_TO_SET_PD_TAG,              (CS_SM_START + 0x22), "|en|CS SM No response to SET_PD_TAG")   
rcode_def ( CS_SM_NR_TO_WHO_HAS_PD_TAG,          (CS_SM_START + 0x23), "|en|CS SM No response to WHO_HAS_PD_TAG")    
rcode_def ( CS_SM_NR_TO_SET_ADDR,                (CS_SM_START + 0x24), "|en|CS SM No response to SET_ADDR")    
rcode_def ( CS_SM_NR_TO_IDENTIFY,                (CS_SM_START + 0x25), "|en|CS SM No response to IDENTIFY")    
rcode_def ( CS_SM_NR_TO_ENABLE_SM_OP,            (CS_SM_START + 0x26), "|en|CS SM No response to ENABLE_SM_OP")    
rcode_def ( CS_SM_NR_TO_CLEAR_ADDRESS,           (CS_SM_START + 0x27), "|en|CS SM No response to CLEAR_ADDRESS")    
rcode_def ( CS_SM_MULTIPLE_TAGS,                 (CS_SM_START + 0x28), "|en|CS SM Multiple response to WHO_HAS_PD_TAG")    
rcode_def ( CS_SM_TAG_WHO_HAS_PD_TAG,            (CS_SM_START + 0x29), "|en|CS SM Non-Matching PD-TAG for WHO_HAS_TAG_PDU")    
rcode_def ( CS_SM_PD_TAG_IDENTIFY,               (CS_SM_START + 0x2a), "|en|CS SM Non-Matching PD-TAG for IDENTIFY")    
rcode_def ( CS_SM_DEV_ID_IDENTIFY,               (CS_SM_START + 0x2b), "|en|CS SM Non-Matching DEV_ID for IDENTIFY")    
                                                                               
/* --- Error codes of system management --------------------------------- */
rcode_def ( CS_SM_CONFIGURATION,                 (CS_SM_START + 0x31), "|en|CS SM configuration data false")    
rcode_def ( CS_SM_ALREADY_CONFIGURED,            (CS_SM_START + 0x32), "|en|CS SM SM is already configured")    
rcode_def ( CS_SM_LIST_TO_LONG,                  (CS_SM_START + 0x33), "|en|CS SM list has to many elements")    
rcode_def ( CS_SM_ALREADY_STARTED,               (CS_SM_START + 0x34), "|en|CS SM time master is activated")    
rcode_def ( CS_SM_ALREADY_STOPPED,               (CS_SM_START + 0x35), "|en|CS SM time master is stopped")    
rcode_def ( CS_SM_TI_NOT_SET,                    (CS_SM_START + 0x36), "|en|CS SM clock interval is not set")    
rcode_def ( CS_SM_LOCAL_ERROR,                   (CS_SM_START + 0x41), "|en|CS SM local SM error occured")
rcode_def ( CS_SM_NO_RESOURCE,                   (CS_SM_START + 0x42), "|en|CS SM no SM ressources available")    
rcode_def ( CS_SM_WRONG_ADDRESS,                 (CS_SM_START + 0x43), "|en|CS SM wrong source address")    
rcode_def ( CS_SM_ENCODING,                      (CS_SM_START + 0x51), "|en|CS SM encoding failed")    
rcode_def ( CS_SM_ILLEGAL_PDU,                   (CS_SM_START + 0x52), "|en|CS SM wrong type of SM pdu")


rcode_def ( CM_IF_USR_ABT_RC1,					 (CS_ABORT_USR_START + 0), "|en|USR Disconnected by user")
rcode_def ( CM_IF_USR_ABT_RC2,					 (CS_ABORT_USR_START + 1), "|en|USR Version object dictionary incompatible")
rcode_def ( CM_IF_USR_ABT_RC3,					 (CS_ABORT_USR_START + 2), "|en|USR Password error")
rcode_def ( CM_IF_USR_ABT_RC4,					 (CS_ABORT_USR_START + 3), "|en|USR Profile number incompatible")
rcode_def ( CM_IF_USR_ABT_RC5,					 (CS_ABORT_USR_START + 4), "|en|USR limited services permitted")
rcode_def ( CM_IF_USR_ABT_RC6,					 (CS_ABORT_USR_START + 5), "|en|USR OD loading interacting (OD is being loaded)")
rcode_def ( CM_IF_FMS_ABT_RC1,					 (CS_CM_IF_FMS_ABT + 0), "|en|FMS FMS-VCR error (VCRL entry invalid)")
rcode_def ( CM_IF_FMS_ABT_RC2,					 (CS_CM_IF_FMS_ABT + 1), "|en|FMS user error (protocol violation by user)")
rcode_def ( CM_IF_FMS_ABT_RC3,					 (CS_CM_IF_FMS_ABT + 2), "|en|FMS FMS-PDU error (invalid FMS PDU received)")
rcode_def ( CM_IF_FMS_ABT_RC4,					 (CS_CM_IF_FMS_ABT + 3), "|en|FMS connection state conflict FAS")
rcode_def ( CM_IF_FMS_ABT_RC5,					 (CS_CM_IF_FMS_ABT + 4), "|en|FMS FAS error")
rcode_def ( CM_IF_FMS_ABT_RC6,					 (CS_CM_IF_FMS_ABT + 5), "|en|FMS PDU size exceeds maximum PDU size")
rcode_def ( CM_IF_FMS_ABT_RC7,					 (CS_CM_IF_FMS_ABT + 6), "|en|FMS feature not supported")
rcode_def ( CM_IF_FMS_ABT_RC8,					 (CS_CM_IF_FMS_ABT + 7), "|en|FMS invoke id error in service response")
rcode_def ( CM_IF_FMS_ABT_RC9,					 (CS_CM_IF_FMS_ABT + 8), "|en|FMS max services overflow")
rcode_def ( CM_IF_FMS_ABT_RC10,					 (CS_CM_IF_FMS_ABT + 9), "|en|FMS connection state conflict FMS (Initiate.req)")
rcode_def ( CM_IF_FMS_ABT_RC11,					 (CS_CM_IF_FMS_ABT + 10), "|en|FMS service error (res != ind or con != req)")
rcode_def ( CM_IF_FMS_ABT_RC12,					 (CS_CM_IF_FMS_ABT + 11), "|en|FMS invoke id error in service request")
rcode_def ( CM_IF_FMS_ABT_RC13,					 (CS_CM_IF_FMS_ABT + 12), "|en|FMS FMS is disabled")
rcode_def ( CM_IF_FAS_ABT_RC1,					 (CS_CM_IF_FAS_ABT + 0x31), "|en|FAS invalid FAS PDU")
rcode_def ( CM_IF_FAS_ABT_RC2,					 (CS_CM_IF_FAS_ABT + 0x32), "|en|FAS remote address mismatch")
rcode_def ( CM_IF_FAS_ABT_RC3,					 (CS_CM_IF_FAS_ABT + 0x33), "|en|FAS multiple initiators")
rcode_def ( CM_IF_FAS_ABT_RC4,					 (CS_CM_IF_FAS_ABT + 0x34), "|en|FAS invalid DL event")
rcode_def ( CM_IF_FAS_ABT_RC5,					 (CS_CM_IF_FAS_ABT + 0x35), "|en|FAS AREP busy")
rcode_def ( CM_IF_FAS_ABT_RC6,					 (CS_CM_IF_FAS_ABT + 0x36), "|en|FAS AREP not found")
rcode_def ( CM_IF_FAS_ABT_RC7,					 (CS_CM_IF_FAS_ABT + 0x37), "|en|FAS invalid AREP type")
rcode_def ( CM_IF_FAS_ABT_RC8,					 (CS_CM_IF_FAS_ABT + 0x38), "|en|FAS DLCEP not found")

/*
 * DLL Error codes
 */

rcode_def ( CM_IF_DLL_ABT_RC1,					 (CS_CM_IF_DLL_ABT + 0x40), "|en|DLL Incorrect DLCEP pairing, permanent")
rcode_def ( CM_IF_DLL_ABT_RC2,					 (CS_CM_IF_DLL_ABT + 0x41), "|en|DLL Incorrect DLCEP revision, permanent")
rcode_def ( CM_IF_DLL_ABT_RC3,					 (CS_CM_IF_DLL_ABT + 0x42), "|en|DLL other permanent condition")
rcode_def ( CM_IF_DLL_ABT_RC4,					 (CS_CM_IF_DLL_ABT + 0x43), "|en|DLL wrong DLPDU format, permanent")
rcode_def ( CM_IF_DLL_ABT_RC5,					 (CS_CM_IF_DLL_ABT + 0x44), "|en|DLL wrong DLSDU size, permanent")
rcode_def ( CM_IF_DLL_ABT_RC6,					 (CS_CM_IF_DLL_ABT + 0x45), "|en|DLL transient condition")
rcode_def ( CM_IF_DLL_ABT_RC7,					 (CS_CM_IF_DLL_ABT + 0x46), "|en|DLL timeout")
rcode_def ( CM_IF_DLL_ABT_RC8,					 (CS_CM_IF_DLL_ABT + 0x5E), "|en|DLL un-specified connection rejection")
rcode_def ( CM_IF_DLL_ABT_RC9,					 (CS_CM_IF_DLL_ABT + 0x60), "|en|DLL DL(SAP) address unknown")
rcode_def ( CM_IF_DLL_ABT_RC10,					 (CS_CM_IF_DLL_ABT + 0x62), "|en|DLL DLSAP unreachable, permanent")
rcode_def ( CM_IF_DLL_ABT_RC11,					 (CS_CM_IF_DLL_ABT + 0x64), "|en|DLL DLSAP unreachable, transient")
rcode_def ( CM_IF_DLL_ABT_RC12,					 (CS_CM_IF_DLL_ABT + 0x66), "|en|DLL QoS unavailable,permanent condition")
rcode_def ( CM_IF_DLL_ABT_RC13,					 (CS_CM_IF_DLL_ABT + 0x65), "|en|DLL wrong DLCEP state, permanent")
rcode_def ( CM_IF_DLL_ABT_RC14,					 (CS_CM_IF_DLL_ABT + 0x68), "|en|DLL QoS unavailable,transient condition")
rcode_def ( CM_IF_DLL_ABT_RC15,					 (CS_CM_IF_DLL_ABT + 0x7E), "|en|DLL unknown origin-reason unspecified")


#ifdef GEN_RTN_H
lb_ endif		/* RTN_CODE_H */
#endif
