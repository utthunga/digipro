/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)hart_drv.h	30.3  30  14 Nov 1996
 *  This file contains the prototypes of the routines needed to interface a 
 *  HART commstack with the Micro Motion device driver.
 */

#ifndef HART_DRV_H
#define HART_DRV_H

#endif /* HART_DRV_H */
