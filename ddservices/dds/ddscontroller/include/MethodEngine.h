/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    MethodEngine class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _METHOD_ENGINE_H
#define _METHOD_ENGINE_H

/* INCLUDE FILES **************************************************************/

#include "PMErrorCode.h"
#include "PMStatus.h"
#include "int_inc.h"

/* MACRO DIFINITIONS **********************************************************/

#define TAG_LENGTH 34
#define DDS_STR_LENGTH 32

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of DDSController, IConnectionManager and IParameterCache
*/
class DDSController;
class FFAdaptor;
class IConnectionManager;
class IParameterCache;

/*============================================================================*/
/** MethodEngine Class Definition.

    MethodEngine class implements the method execution
    functionality for Foundation Fieldbus protocol.
*/

class MethodEngine
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a MethodEngine
    MethodEngine();

    /*------------------------------------------------------------------------*/
    /// Destruct a MethodEngine
    virtual ~MethodEngine();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the MethodEngine with IConnectionManager and
        IParameterCache instances

        @param dds - DDSController instance
        @param cm - IConnectionManager instance
        @param pc - IParameterCache instance

        @return status
    */
    PMStatus initialize(DDSController* dds, FFAdaptor* ffa,
                        IConnectionManager* cm, IParameterCache* pc);

    /// MethodEngine functionalities
    /*------------------------------------------------------------------------*/
    /** Executes the method

        @param blockTag - contains block Tag
        @param itemID - contains method ID

        @return status
    */
    PMStatus executeMethod(std::string blockTag, ITEM_ID methodID);

    /*= Explicitly Disabled Methods ==========================================*/
    MethodEngine& operator=(const MethodEngine& rhs) = delete;
    MethodEngine(const MethodEngine& rhs) = delete;

private:
    /* Private Data Members ==================================================*/
    /// DDS Controller instance
    DDSController* m_pDDS;

    /// Connection Manager instance
    IConnectionManager* m_pCM;

    /// Parameter cache instance
    IParameterCache* m_pPC;

    /// FFAdaptor instance
    FFAdaptor* m_pFFA;
};

#endif  // _METHOD_ENGINE_H
