/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    DDSController class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _DDS_CONTROLLER_H
#define _DDS_CONTROLLER_H

/* INCLUDE FILES **************************************************************/

#include <condition_variable>
#include "IConnectionManager.h"
#include "IParameterCache.h"
#include "FFAdaptor.h"
#include "MethodEngine.h"
#include "PMErrorCode.h"
#include "PMStatus.h"
#include "ddi_tab.h"
#include "dds_upcl.h"
#include "ret_info.h"
#include "nm.h"
#include "tst_dds.h"
#include "ffh1_util.h"
#include "lang_upcl.h"

#define READ_PARAM_VALUE  1
#define WRITE_PARAM_VALUE 2
#define DD_FILE_EXTN_LEN  3

#define APP_FUNC_GET_BLK_HANDLE             1
#define APP_GET_BLOCK_INSTANCE_BY_INDEX     2
#define APP_GET_BLOCK_INSTANCE_BY_TAG       3
#define APP_GET_BLOCK_INSTANCE_COUNT        4
#define APP_GET_SELECTOR_UPCALL             5
#define DISPLAY_UPCALL                      6
#define DISPLAY_CONTINUOUS_UPCALL           7
#define DISPLAY_CONTINUOUS_UPCALL2          8
#define DISPLAY_GET_UPCALL                  9
#define DISPLAY_MENU_UPCALL                 10
#define DISPLAY_MENU_UPCALL2                11
#define DISPLAY_ACK_UPCALL                  12
#define METHOD_START_UPCALL                 13
#define METHOD_COMPLETE_UPCALL              14
#define METHOD_ERROR_UPCALL                 15
#define LIST_DELETE_ELEMENT_UPCALL          16
#define LIST_DELETE_ELEMENT2_UPCALL         17
#define LIST_INSERT_ELEMENT_UPCALL          18
#define LIST_INSERT_ELEMENT2_UPCALL         19
#define LIST_READ_ELEMENT_UPCALL            20
#define LIST_READ_ELEMENT2_UPCALL           21
#define LIST_COUNT_ELEMENT_UPCALL           22
#define LIST_COUNT_ELEMENT2_UPCALL          23
#define PLOT_CREATE_UPCALL                  24
#define PLOT_DESTROY_UPCALL                 25
#define PLOT_DISPLAY_UPCALL                 26
#define PLOT_EDIT_UPCALL                    27
#define GET_DD_SYM_FILE_EXTN                28
#define GET_LANG_STRING_UPCALL              29
#define CHECK_DEVICE_STATUS                 30

#define FILE_PATH_LEN               128
#define MESSAGE_DELAY_INTERVAL      500
#define NTOK_PATH_ENV_NAME          "TOK_RELEASE_DIR"

/*----------------------------------------------------------------------------*/
/// Method Execution data structure
typedef struct MethodExecutionData
{
    bool isItemSet    = false; /// Is user input item has been set or not
    bool isAborted    = false; /// Is user abort the current method or not
    int currentInputType = 0;  /// Currently exepecting input type
    ITEM_ID blockID      = 0;  /// Currently executing method's block ID
    ITEM_ID methodID     = 0;  /// Currently executing method ID
    std::string methodLabel; /// Currently executing method Label
    std::string methodHelp; /// Currently executing method help
    EVAL_VAR_VALUE* userInputVal = nullptr; /// User input value structure
    int m_methodResult    = 0;  /// for checking upcalls/method status.
    bool isWaitingForUserInput = false;
} METHOD_EXECUTION_DATA;

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** DDSController Class Definition.

    DDSController class implements the DD Services
    mechanism for Foundation Fieldbus protocol.
*/
class DDSController
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a DDSController
    DDSController();

    /*------------------------------------------------------------------------*/
    /// Destruct a DDSController
    virtual ~DDSController();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the DDSController with IConnectionManager and
        IParameterCache instances

        @param cm - IConnectionManager instance
        @param pc - IParameterCache instance

        @return status
    */
    PMStatus initialize(FFAdaptor* ffa, IConnectionManager* cm,
                        IParameterCache* pc);

    /// DDSController functionalities
    /*------------------------------------------------------------------------*/
    /** This function is used to get the device handle

        @param adh - contains active device handle

        @return status
    */
    PMStatus getActiveDeviceHandle(DEVICE_HANDLE* adh);

    /*------------------------------------------------------------------------*/
    /** This function is used to set the device handle

        @param adh - contains active device handle

        @return status
    */
    PMStatus setActiveDeviceHandle(DEVICE_HANDLE* adh);

    /*------------------------------------------------------------------------*/
    /** Gets the requested item and returns it to the caller

        @param blockSpec - specifies the block with either a BLOCK_TAG or
        BLOCK_HANDLE
        @param itemSpec - specifies the item being requested
        @param envInfo - contains environment info
        @param reqMask - the mask which specifies the item attributes
        the caller wants ddi_get_item() to fetch and evaluate for the
        requested item
        @param genericItem - contains the generic DDI item

        @return status
    */
    PMStatus ddiGetItem(
        DDI_BLOCK_SPECIFIER* blockSpec,
        DDI_ITEM_SPECIFIER* itemSpec,
        ENV_INFO* envInfo,
        DDI_ATTR_REQUEST reqMask,
        DDI_GENERIC_ITEM* genericItem);

    /*------------------------------------------------------------------------*/
    /** Gets the requested item and returns it to the caller and allow access
        to items outside of blocks in the DD

        @param itemSpec - specifies the item being requested
        @param envInfo2 - the application's environment information
        @param reqMask - the mask which specifies the item attributes the
        caller wants ddi_get_item2() to fetch and evaluate for the requested
        item
        @param genericItem - contains the generic DDI item

        @return status
    */
    PMStatus ddiGetItem2(
        DDI_ITEM_SPECIFIER* itemSpec,
        ENV_INFO2* envInfo2,
        DDI_ATTR_REQUEST reqMask,
        DDI_GENERIC_ITEM* genericItem);

    /*------------------------------------------------------------------------*/
    /** Gets the requested WAO and returns it to the caller

        @param blockSpec - specifies the block being requested
        @param paramSpec - holds the param spec to get wao ID
        @param itemID - holds the fetched write as one ID

        @return errorCode
    */
    int ddiGetWao(DDI_BLOCK_SPECIFIER *blockSpec,
                  DDI_PARAM_SPECIFIER *paramSpec, ITEM_ID *itemID);

    /*------------------------------------------------------------------------*/
    /** This function frees memory allocated to the generic item structure

        @param genericItem - contains generic item

        @return status
    */
    PMStatus ddiCleanItem(DDI_GENERIC_ITEM* genericItem);

    /*------------------------------------------------------------------------*/
    /** This function returns the item ID for the given item from Symbol file

        @param sym_name - item name
        @param out_id - item id 

        @return status
    */
    PMStatus ddiLocateSymbolId(ENV_INFO2* env, char* symName, ITEM_ID* outId);

    /*------------------------------------------------------------------------*/
    /** This function used to load a DD for a block

        @param bh - contains block handle
        @param ddhPtr - contains dd handle

        @return status
    */
    PMStatus dsDdBlockLoad(BLOCK_HANDLE bh, DD_HANDLE* ddhPtr);

    /// Callbacks
    /*------------------------------------------------------------------------*/
    /** Callback function to get the block handle

        @param envInfo - contains environment info
        @param blockID - contains block ID
        @param instanceNum - contains instance number
        @param outBlkHandle - contains block handle

        @return error code
    */
    static int appFuncGetBlockHandleCallback(
        ENV_INFO* envInfo,
        ITEM_ID blockID,
        unsigned int instanceNum,
        BLOCK_HANDLE* outBlkHandle);

    /*------------------------------------------------------------------------*/
    /** Callback function to get the block instance by index

        @param envInfo - contains environment info
        @param blockID - contains block ID
        @param instanceNum - contains instance number
        @param outBlkHandle - contains block handle

        @return error code
    */
    static int appGetBlockInstanceByIndex(ENV_INFO* envInfo, ITEM_ID blockID,
                                          unsigned int instanceNum,
                                          unsigned long* outBlkHandle);

    /*------------------------------------------------------------------------*/
    /** Callback function to get the block instance by tag

        @param envInfo - contains environment info
        @param blockID - contains block ID
        @param blockTag - contains block tag
        @param outInstance - contains block instance

        @return error code
    */
    static int appGetBlockInstanceByTag(ENV_INFO* envInfo, ITEM_ID blockID,
                                        char *blockTag, unsigned long *outInstance);

    /*------------------------------------------------------------------------*/
    /** Callback function to get the block instance count

        @param envInfo - contains environment info
        @param blockID - contains block ID
        @param outCount - contains block instance count

        @return error code
    */
    static int appGetBlockInstanceCount(ENV_INFO* envInfo, ITEM_ID blockID,
                                        unsigned long *outCount);

    /*------------------------------------------------------------------------*/
    /** Callback function to get the selector (currently not being used)

        @param envInfo - contains environment info
        @param itemID - contains item ID
        @param selector - contains selector ID
        @param additionalID - contains additional ID
        @param selValue - contains selector value

        @return error code
    */
    static int appGetSelectorUpcall(ENV_INFO* envInfo, ITEM_ID itemID,
                                    ITEM_ID selector, ITEM_ID additionalID,
                                    double *selValue);

    /*------------------------------------------------------------------------*/
    /** This function links the protocol adapter and common service layer
        function pointers with DDS functions

        @param funID - contains function ID
        @param funPtr - contains function pointer
        @return status
    */
    PMStatus setCallback(int funID, void* funPtr);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user

        @param envInfo - contains environment info
        @param response - contains response to UI

        @return status
    */
    static int displayUpcall(ENV_INFO* envInfo, char* response);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user and display continously
        until user acknowledge

        @param envInfo - contains environment info
        @param prompt - contain formatted string which refer to the values of
        the provided variables
        @param varIDs - contains the IDs of the variables used in the prompt
        message
        @param varSubindices - contains subindicies of the variables used in
        the prompt message

        @return status
    */
    static int displayContinuousUpcall(ENV_INFO* envInfo, char* prompt,
                                       ITEM_ID* varIDs,
                                       SUBINDEX* varSubindices);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user and display continously
        until user acknowledge

        @param envInfo - contains environment info
        @param prompt - contain formatted string which refer to the values of
        the provided variables
        @param varIDs - contains the IDs of the variables used in the prompt
        message
        @param varSubindices - contains subindicies of the variables used in
        the prompt message

        @return status
    */
    static int displayContinuousUpcall2(ENV_INFO* envInfo, char* prompt,
                                        ITEM_ID* varIDs,
                                        SUBINDEX* varSubindices,
                                        BLOCK_HANDLE *blockHandles);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user and get new value from
        user in particular field

        @param envInfo - contains environment info
        @param response - contains response to UI
        @param pcPRef - contains an optional pointer to the P_REF structure
        containing the ITEM_ID and subindex of the variable to read
        @param varValue

        @return status
    */
    static int displayGetUpcall(ENV_INFO* envInfo, char* response,
                                P_REF* pcPRef, EVAL_VAR_VALUE* varValue);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user and get selected option
        from user

        @param envInfo - contains environment info
        @param response - contains response to UI
        @param selectList - contains the structure containing the menu
        selection strings
        @param selection - contains user selection value

        @return status
    */
    static int displayMenuUpcall(ENV_INFO* envInfo, char* response,
                                 SELECT_LIST* selectList, int* selection);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user and get selected option
        from user

        @param envInfo - contains environment info
        @param selectList - contains the structure containing the menu
        selection strings
        @param selection - contains user selection value
        @param menuID - contains menu ID

        @return status
    */
    static int displayMenuUpcall2(ENV_INFO* envInfo, SELECT_LIST* selectList,
                                  int* selection, ITEM_ID menuID);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message and wait's until user
        acknowledges

        @param envInfo - contains environment info
        @param response - contains response to UI

        @return status
    */
    static int displayAckUpcall(ENV_INFO* envInfo, char* response);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user about Method start status

        @param envInfo - contains environment info
        @param methItemID - contains the method ID

        @return status
    */
    static long methodStartUpcall(ENV_INFO* envInfo, ITEM_ID methItemID);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user about Method completion
        status

        @param envInfo - contains environment info
        @param methItemID - contains the method ID
        @param methodResult - contains the method result

        @return status
    */
    static long methodCompleteUpcall(ENV_INFO* envInfo, ITEM_ID methItemID,
                                     int methodResult);

    /*------------------------------------------------------------------------*/
    /** Callback function used to send message to user about Method error status

        @param envInfo - contains environment info
        @param errorStr - contains error message

        @return status
    */
    static long methodErrorUpcall(ENV_INFO* envInfo, char* errorStr);

    /** ------------------------------------------------------------------------
        TODO(developer): Implement list and plot releated upcalls to send the
        list of data to UI when list support is added to the application.
        Currently list upcalls are defined and NOT being used.
    */
    /*------------------------------------------------------------------------*/
    /** Callback function used to delete the specific element from the UI list

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element

        @return status
    */
    static long listDeleteElementUpcall(ENV_INFO* envInfo, ITEM_ID listID,
                                        int index);

    /*------------------------------------------------------------------------*/
    /** Callback function used to delete the specific element from the UI list
        by using embedded ID and index

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param embeddedListID - contains embedded ID of list
        @param embeddedIndex - contains embedded index of the element

        @return status
    */
    static long listDeleteElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                         int index, ITEM_ID embeddedListID,
                                         int embeddedIndex);

    /*------------------------------------------------------------------------*/
    /** Callback function used to insert the element to UI list

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param element - contains element to insert

        @return status
    */
    static long listInsertElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                        int index, ITEM_ID element);

    /*------------------------------------------------------------------------*/
    /** Callback function used to insert the element to UI list
        by using embedded ID and index

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param embeddedListID - contains embedded ID of list
        @param embeddedIndex - contains embedde index of the element
        @param embeddedElement - contains embedded element to insert

        @return status
    */
    static long listInsertElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                         int index, ITEM_ID embeddedListID,
                                         int embeddedIndex,
                                         ITEM_ID embeddedElement);

    /*------------------------------------------------------------------------*/
    /** Callback function used to read the element from UI list

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param subElement - contains sub element to read
        @param subSubElement - contains sub sub element to read
        @param outElemData - contains read element data

        @return status
    */
    static long listReadElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                      int index, ITEM_ID subElement,
                                      ITEM_ID subSubElement,
                                      EVAL_VAR_VALUE *outElemData);

    /*------------------------------------------------------------------------*/
    /** Callback function used to read the element from UI list
        by using embedded ID and index

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param embeddedListID - contains embedded ID of list
        @param embeddedIndex - contains embedde index of the element
        @param subElement - contains sub element to read
        @param subSubElement - contains sub sub element to read
        @param outElemData - contains read element data

        @return status
    */
    static long listReadElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                       int index, ITEM_ID embeddedListID,
                                       int embeddedListIndex,
                                       ITEM_ID subElement,
                                       ITEM_ID subSubElement,
                                       EVAL_VAR_VALUE *outElemData);

    /*------------------------------------------------------------------------*/
    /** Callback function used to count the number of elements in UI list

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param count - contains number of elements in UI list

        @return status
    */
    static long listCountElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                       unsigned long *count);

    /*------------------------------------------------------------------------*/
    /** Callback function used to count the number of elements in UI list
        by using embedded ID and index

        @param envInfo - contains environment info
        @param listID - contains ID of list
        @param index - contains index of the element
        @param embeddedListID - contains embedded ID of list
        @param count - contains number of elements in UI list

        @return status
    */
    static long listCountElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                        int index, ITEM_ID embeddedListID,
                                        unsigned long *count);

    /*------------------------------------------------------------------------*/
    /** Callback function used to create a plot in UI

        @param envInfo - contains environment info
        @param plot - contains a plot

        @return status
    */
    static long plotCreateUpcall(ENV_INFO *envInfo, ITEM_ID plot);

    /*------------------------------------------------------------------------*/
    /** Callback function used to destroy a plot of UI

        @param envInfo - contains environment info
        @param plot - contains a plot

        @return status
    */
    static long plotDestroyUpcall(ENV_INFO *envInfo, ITEM_ID plot);

    /*------------------------------------------------------------------------*/
    /** Callback function used to display a plot in UI

        @param envInfo - contains environment info
        @param plotHandle - contains a plot handle

        @return status
    */
    static long plotDisplayUpcall(ENV_INFO *envInfo, long plotHandle);

    /*------------------------------------------------------------------------*/
    /** Callback function used to edit a plot of UI

        @param envInfo - contains environment info
        @param plotHandle - contains a plot handle
        @param waveFormID - contains waveform ID

        @return status
    */
    static long plotEditUpcall(ENV_INFO *envInfo, long plotHandle,
                               unsigned long waveFormID);

    /*------------------------------------------------------------------------*/
    /** Callback function which is used get the DD file path which is loaded
        during device connection

        @param filePath - contains DD file path

        @return status
    */
    static int loadedDDCallback(char *filePath);

    /// Method Interpreter functions
    /*------------------------------------------------------------------------*/
    /** Executes the method

        @param blockTag - contains block Tag
        @param itemID - contains method ID

        @return status
    */
    PMStatus executeMethod(string blockTag, ITEM_ID methodID);

    /*------------------------------------------------------------------------*/
    /** Function is used to get block handle by block tag

        @param tag - contains block tag

        @return - block handle
    */
    BLOCK_HANDLE getBlockHandleByBlockTag(const char* tag);

    /*------------------------------------------------------------------------*/
    /** Function is used to get block handle by block ID

        @param id - contains block ID

        @return block handle
    */
    BLOCK_HANDLE getBlockHandleByBlockID(ITEM_ID id);

    /*------------------------------------------------------------------------*/
    /** Function is used to get block tag from block ID

        @param id - contains block ID
        @param instanceNum - contains block instance number

        @return block tag
    */
    char* getBlockTagByBlockID(ITEM_ID id, unsigned int instanceNum);

    /*------------------------------------------------------------------------*/
    /** Function is used to get block ID by block handle

        @param bh - contains block handle
        @param id - contains block ID

        @return
    */
    PMStatus getBlockIDByBlockHandle(BLOCK_HANDLE bh, ITEM_ID* id);

    /*------------------------------------------------------------------------*/
    /** Function is used to convert builitin format string

        @param outBuf - contains output buffer
        @param maxLength - contains max length
        @param passedPrompt - contains prompt string
        @param paramIDs - contaisn parameter IDs
        @param subIndices - contains sub indeices
        @param envInfo - contains environment info

        @return error value
    */
    PMStatus builtinFormatString(char* outBuf, int maxLength, char* passedPrompt,
                    ITEM_ID* paramIDs, SUBINDEX* subIndices, ENV_INFO* envInfo);

    /*------------------------------------------------------------------------*/
    /** Function is used to convert builitin format string

        @param outBuf - contains output buffer
        @param maxLength - contains max length
        @param passedPrompt - contains prompt string
        @param paramIDs - contaisn parameter IDs
        @param subIndices - contains sub indeices
        @param blockHandle - contains block handle
        @param useBH - contains use block handle or not
        @param envInfo - contains environment info

        @return error value
    */
    PMStatus builtinFormatString2(char* outBuf, int maxLength, char* passedPrompt,
                                  ITEM_ID* paramIDs, SUBINDEX* subIndices,
                                  BLOCK_HANDLE *blockHandle,
                                  int useBH, ENV_INFO *envInfo);

    /*------------------------------------------------------------------------*/
    /** Converts DDS date & time format to User date & time format

        @param charArray - contains variable value
        @param dateArray - contains date in array format
    */
    void convDDSDateNTimeToUserDateNTime(char charArray[], int* dateArray);

    /*------------------------------------------------------------------------*/
    /** Converts user date & time format to DDS date & time format

        @param dateArray - contains date in array format
        @param charArray - contains variable value
    */
    void convUserDateNTimeToDDSDateNTime(int* dateArray, char charArray[]);

    /*------------------------------------------------------------------------*/
    /** Converts DDS date format to User date format

        @param charArray - contains variable value
        @param dateArray - contains date in array format
    */
    void convDDSDateToUserDate(char charArray[], int* dateArray);

    /*------------------------------------------------------------------------*/
    /** Converts User date format to DDS date format

        @param dateArray - contains date in array format
        @param charArray - contains variable value
    */
    void convUserDateToDDSDate(int *dateArray, char charArray[]);

    /*------------------------------------------------------------------------*/
    /** Converts DDS time format to User time format

        @param charArray - contains variable value
        @param dateArray - contains date in array format
    */
    void convDDSTimeToUserTime(char charArray[], int* dateArray);

    /*------------------------------------------------------------------------*/
    /** Converts DDS time format to User time format

        @param dateArray - contains date in array format
        @param charArray - contains variable value
    */
    void convUserTimeToDDSTime(int *dateArray, char charArray[]);

    /*------------------------------------------------------------------------*/
    /** Converts DDS duration format to User duration format

        @param charArray - contains variable value
        @param dateArray - contains duration in milli second
    */
    void convDDSDurationToUserDuration(char charArray[], unsigned long long *mSec);

    /*------------------------------------------------------------------------*/
    /** Converts User duration format to DDS duration format

        @param dateArray - contains duraion value in milli second
        @param charArray - contains variable value
    */
    void convUserDurationToDDSDuration(unsigned long long mSec, char charArray[]);

    /*------------------------------------------------------------------------*/
    /** Converts DDS Time Value format to User Time Value format

        @param charArray - contains variable value
        @param dateArray - contains date in array format
    */
    void convDDSTimeValToUserTimeVal(char charArray[], int *dateArray);

    /** Converts User Time Value format to DDS Time Value format

        @param epochTime - holds time value in epoch
        @param dateArray - contains date in array format
        @param charArray - contains time value in array format
    */
    void convUserTimeValToDDSTimeVal(unsigned long epochTime, int *dateArray, char *charArray);

    /*------------------------------------------------------------------------*/
    /** Converts epoch time to user time

        @param epochTime - contains the epoch time
        @param format - contains the time format
    */
    std::string convertsEpochTimeToUserTime(time_t epochTime, const char* format);

    /*------------------------------------------------------------------------*/
    /** Converts user time to epoch time

        @param userTime - contains the user time
        @param format - contains the time format
    */
    time_t convertsUserTimeToEpochTime(const char* userTime, const char* format);

    /*------------------------------------------------------------------------*/
    /** Fetch the item type by item ID

        @param itemID - contains item ID
        @param blockTag - contains block tag

        @return item type
    */
    ITEM_TYPE getItemType(ITEM_ID itemID, string blockTag);

    /*------------------------------------------------------------------------*/
    /** Fetch the item type by item ID and Block tag

        @param blockSpecifier - contains block specifier
        @param itemSpecifier - contains item specifier
        @param itemType - returns the item type
    */
    void ddiGetItemType(DDI_BLOCK_SPECIFIER *blockSpecifier,
                        DDI_ITEM_SPECIFIER *itemSpecifier,
                        ITEM_TYPE *itemType);

    /*------------------------------------------------------------------------*/
    /** Fetch the item type by item ID

        @param envInfo2 - contains environment info
        @param itemSpecifier - contains item specifier
        @param itemType - returns the item type
    */
    void ddiGetItemType2(ENV_INFO2 *envInfo2,
                         DDI_ITEM_SPECIFIER *itemSpecifier,
                         ITEM_TYPE *itemType);

    /*------------------------------------------------------------------------*/
    /// It aquires the thread lock and wait till release
    void acquireLock();

    /*------------------------------------------------------------------------*/
    /// It releases the locked thread and notify for further process
    void releaseLock();

	/*------------------------------------------------------------------------*/
    /** Fetches device ID information

        @param ddDeviceID Structure containing device ID information
        
        @return error status of method
    */
    PMStatus getDeviceIDInfo(DD_DEVICE_ID& ddDeviceID); 

    /*------------------------------------------------------------------------*/
    /** Function to get DD file's extension which is loaded during device
        connection

        @return DD file extension
    */
    std::string getDDFileExtn();

    /*------------------------------------------------------------------------*/
    /** Function to get the language string based on language code
        
        @param srcString, source string containing complete DD string

        @param dstString, destination string to which language specific string 
                must be extracted

        @param length, length of the source string

    */    
    void getLanguageString(const char* srcString, string& dstString, unsigned int length);

    /*------------------------------------------------------------------------*/
    /** Function to get string encoding used in loaded DD binary
          
        @return DD encoding (ENCODING_UNKNOWN, ISO_LATIN_1, UTF_8)
    */
    int getDDStringEncoding(void);

    /*------------------------------------------------------------------------*/
	/** Function to check is device is connected or not

	   @return  CPM connection state.
	*/
    static int isConnectionAlive();

    /*------------------------------------------------------------------------*/
    /** Converts ASCII string to UTF8
        
        @param srcString : ASCII date to be converted to UTF8 format

        @return string in UTF8 format
    */
    std::string getUTF8String(char* srcString);

    /*= Explicitly Disabled Methods ==========================================*/
    DDSController& operator=(const DDSController& rhs) = delete;
    DDSController(const DDSController& rhs) = delete;

public:
    /* Static Data Members ===================================================*/
    /// Currently exepecting input type
    static METHOD_EXECUTION_DATA s_pMethodExecutionData;

    /// Mutex Thread Lock
    mutex m_methodExecutionLock;

    /// Condition variable for handle thread
    condition_variable m_methodExecutionConditionVariable;

private:
    /* Private Data Members ==================================================*/
    /// Active Device Handle
    DEVICE_HANDLE m_activeDeviceHandle;

    /// Connection manager instance
    IConnectionManager* m_pCM;

    /// Method Engine instance
    MethodEngine* m_pMI;

    /// Parameter Cache instance
    IParameterCache* m_pPC;

    /// FFAdaptor instance
    FFAdaptor* m_pFFA;

    /// Holds symbol file extention
    static std::string m_symFileExt;

    /// Holds language code
    static std::string m_langCode;
};

#endif  // _DDS_CONTROLLER_H
