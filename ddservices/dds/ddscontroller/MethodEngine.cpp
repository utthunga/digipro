/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    MethodEngine class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "DDSController.h"
#include "IConnectionManager.h"
#include "IParameterCache.h"
#include "MethodEngine.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include "tst_dds.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    MethodEngine class instance initialization
*/
MethodEngine::MethodEngine() : m_pDDS(nullptr), m_pCM(nullptr), m_pPC(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
MethodEngine::~MethodEngine()
{
    // Clear the member variables
    m_pDDS = nullptr;
    m_pCM = nullptr;
    m_pPC = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function is used to set the DDSController, IConnectionManager,
    IParameterCache instances
*/
PMStatus MethodEngine::initialize(DDSController* dds, FFAdaptor* ffa,
                                  IConnectionManager* cm, IParameterCache* pc)
{
    PMStatus status;

    if (nullptr != dds && nullptr != ffa && nullptr != cm && nullptr != pc)
    {
        m_pDDS = dds;
        m_pFFA = ffa;
        m_pCM  = cm;
        m_pPC  = pc;
    }
    else
    {
        status.setError(
            DDS_CTRL_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_DDS_CTRL,
            FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ---------------------------------------------------------------------------
    This function is used to execute the method
*/
PMStatus MethodEngine::executeMethod(std::string blockTag, ITEM_ID methodID)
{
    PMStatus status;
    int errorCode = FAILURE;
    APP_INFO appInfo{};
    ENV_INFO envInfo{};
    OP_REF opRef{};
    DDI_ITEM_SPECIFIER itemSpec{};
    DDI_BLOCK_SPECIFIER blockSpec{};

    envInfo.block_handle = 0;

    if (!blockTag.empty())
    {
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(blockTag.c_str());
    }

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_METH_VALUE;
    appInfo.adaptorInstance = m_pFFA;
    appInfo.ddsControllerInstance = m_pDDS;

    blockSpec.type = DDI_BLOCK_HANDLE;
    blockSpec.block.handle = envInfo.block_handle;

    itemSpec.type = DDI_ITEM_ID;
    itemSpec.item.id = methodID;

    opRef.id = itemSpec.item.id;
    opRef.type = METHOD_ITYPE;

    errorCode = dds_exec_method(envInfo, opRef, itemSpec, blockSpec);

    if (SUCCESS != errorCode)
    {
        status.setError(
            DDS_CTRL_ERR_EXECUTE_METHOD_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}
