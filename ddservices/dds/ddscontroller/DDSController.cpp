/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    DDSController class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include "DDSController.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

/* ========================================================================== */
METHOD_EXECUTION_DATA DDSController::s_pMethodExecutionData = {};
std::string DDSController::m_symFileExt = {""};
/// Holds language code
std::string DDSController::m_langCode = {""};

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    DDSController class instance initialization
*/
DDSController::DDSController()
    : m_activeDeviceHandle(INVALID_ID_OR_HANDLE),
      m_pCM(nullptr), 
      m_pMI(nullptr), 
      m_pPC(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
DDSController::~DDSController()
{
    // Deletes MethodEngine instance
    if (nullptr != m_pMI)
    {
        delete m_pMI;
        m_pMI = nullptr;
    }

    // Clear the member variables
    m_pCM = nullptr;
    m_pPC = nullptr;

    m_langCode.clear();
}

/*  ----------------------------------------------------------------------------
    This function is used to initialize the module
*/
PMStatus DDSController::initialize(FFAdaptor* ffa, IConnectionManager* cm,
                                   IParameterCache* pc)
{
    PMStatus status;

    if (nullptr != ffa && nullptr != cm && nullptr != pc)
    {
        m_pFFA = ffa;
        m_pCM  = cm;
        m_pPC  = pc;
        m_pMI  = new MethodEngine();
        status = m_pMI->initialize(this, m_pFFA, m_pCM, m_pPC);

        // Register callsbacks for blocks operation
        setCallback(CHECK_DEVICE_STATUS,
                           reinterpret_cast<void *>(isConnectionAlive));
        setCallback(APP_FUNC_GET_BLK_HANDLE,
                    reinterpret_cast<void *>(appFuncGetBlockHandleCallback));
        setCallback(APP_GET_BLOCK_INSTANCE_BY_INDEX,
                    reinterpret_cast<void *>(appGetBlockInstanceByIndex));
        setCallback(APP_GET_BLOCK_INSTANCE_BY_TAG,
                    reinterpret_cast<void *>(appGetBlockInstanceByTag));
        setCallback(APP_GET_BLOCK_INSTANCE_COUNT,
                    reinterpret_cast<void *>(appGetBlockInstanceCount));
        setCallback(APP_GET_SELECTOR_UPCALL,
                    reinterpret_cast<void *>(appGetSelectorUpcall));
        setCallback(GET_DD_SYM_FILE_EXTN,
                    reinterpret_cast<void *>(loadedDDCallback));

        // Register callback functions for method execution
        setCallback(DISPLAY_UPCALL,
                    reinterpret_cast<void*>(displayUpcall));
        setCallback(DISPLAY_CONTINUOUS_UPCALL,
                    reinterpret_cast<void*>(displayContinuousUpcall));
        setCallback(DISPLAY_CONTINUOUS_UPCALL2,
                    reinterpret_cast<void*>(displayContinuousUpcall2));
        setCallback(DISPLAY_GET_UPCALL,
                    reinterpret_cast<void*>(displayGetUpcall));
        setCallback(DISPLAY_MENU_UPCALL,
                    reinterpret_cast<void*>(displayMenuUpcall));
        setCallback(DISPLAY_MENU_UPCALL2,
                    reinterpret_cast<void*>(displayMenuUpcall2));
        setCallback(DISPLAY_ACK_UPCALL,
                    reinterpret_cast<void*>(displayAckUpcall));
        setCallback(METHOD_START_UPCALL,
                    reinterpret_cast<void*>(methodStartUpcall));
        setCallback(METHOD_COMPLETE_UPCALL,
                    reinterpret_cast<void*>(methodCompleteUpcall));
        setCallback(METHOD_ERROR_UPCALL,
                    reinterpret_cast<void*>(methodErrorUpcall));
        setCallback(LIST_DELETE_ELEMENT_UPCALL,
                    reinterpret_cast<void*>(listDeleteElementUpcall));
        setCallback(LIST_DELETE_ELEMENT2_UPCALL,
                    reinterpret_cast<void*>(listDeleteElement2Upcall));
        setCallback(LIST_INSERT_ELEMENT_UPCALL,
                    reinterpret_cast<void*>(listInsertElementUpcall));
        setCallback(LIST_INSERT_ELEMENT2_UPCALL,
                    reinterpret_cast<void*>(listInsertElement2Upcall));
        setCallback(LIST_READ_ELEMENT_UPCALL,
                    reinterpret_cast<void*>(listReadElementUpcall));
        setCallback(LIST_READ_ELEMENT2_UPCALL,
                    reinterpret_cast<void*>(listReadElement2Upcall));
        setCallback(LIST_COUNT_ELEMENT_UPCALL,
                    reinterpret_cast<void*>(listCountElementUpcall));
        setCallback(LIST_COUNT_ELEMENT2_UPCALL,
                    reinterpret_cast<void*>(listCountElement2Upcall));
        setCallback(PLOT_CREATE_UPCALL,
                    reinterpret_cast<void*>(plotCreateUpcall));
        setCallback(PLOT_DESTROY_UPCALL,
                    reinterpret_cast<void*>(plotDestroyUpcall));
        setCallback(PLOT_DISPLAY_UPCALL,
                    reinterpret_cast<void*>(plotDisplayUpcall));
        setCallback(PLOT_EDIT_UPCALL,
                    reinterpret_cast<void*>(plotEditUpcall));
    }
    else
    {
        status.setError(
                    DDS_CTRL_ERR_INITIALIZE_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get the device handle
*/
PMStatus DDSController::getActiveDeviceHandle(DEVICE_HANDLE* adh)
{
    PMStatus status;

    /* If active device handle is set before it will return that,
       orelse fetch the latest device handle based on active device table
       count and return that.
    */
    if (m_activeDeviceHandle == -1)
    {
        *adh = active_dev_tbl.count - 1;
        setActiveDeviceHandle(adh);
    }
    else
    {
        *adh = m_activeDeviceHandle;
    }

    if (*adh < 0)
    {
        status.setError(
                    DDS_CTRL_ERR_WRONG_DEVICE_HANDLE,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to set the device handle
*/
PMStatus DDSController::setActiveDeviceHandle(DEVICE_HANDLE* adh)
{
    PMStatus status;

    if (*adh >= 0)
    {
        m_activeDeviceHandle = *adh;
    }
    else
    {
        status.setError(
                    DDS_CTRL_ERR_WRONG_DEVICE_HANDLE,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Gets the requested item and returns it to the caller
*/
PMStatus DDSController::ddiGetItem(
        DDI_BLOCK_SPECIFIER* blockSpec,
        DDI_ITEM_SPECIFIER* itemSpec,
        ENV_INFO* envInfo,
        DDI_ATTR_REQUEST reqMask,
        DDI_GENERIC_ITEM* genericItem)
{
    PMStatus status;
    int errorCode = FAILURE;

    errorCode =
            ::ddi_get_item(blockSpec, itemSpec, envInfo, reqMask, genericItem);

    if (SUCCESS != errorCode)
    {
        status.setError(
                    DDS_CTRL_ERR_DDI_GET_ITEM_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Gets the requested item and returns it to the caller
*/
PMStatus DDSController::ddiGetItem2(
        DDI_ITEM_SPECIFIER* itemSpec,
        ENV_INFO2* envInfo2,
        DDI_ATTR_REQUEST reqMask,
        DDI_GENERIC_ITEM* genericItem)
{
    PMStatus status;
    int errorCode = FAILURE;

    errorCode = ::ddi_get_item2(itemSpec, envInfo2, reqMask, genericItem);

    if (errorCode != DDL_CHECK_RETURN_LIST && SUCCESS != errorCode)
    {
		// TODO (VINAY) Temporary fix for ignoring the error from eval sub system while reading the parameter Value
        status.setError(
                    DDS_CTRL_ERR_DDI_GET_ITEM2_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Gets the requested write as one ID and returns it to the caller
*/
int DDSController::ddiGetWao(DDI_BLOCK_SPECIFIER *blockSpec,
                             DDI_PARAM_SPECIFIER *paramSpec,
                             ITEM_ID *itemID)
{
    int errorCode = FAILURE;

    errorCode = ::ddi_get_wao(blockSpec, paramSpec, itemID);

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This function frees memory allocated to the generic item structure
*/
PMStatus DDSController::ddiCleanItem(DDI_GENERIC_ITEM* genericItem)
{
    PMStatus status;
    int errorCode = FAILURE;

    errorCode = ::ddi_clean_item(genericItem);

    if (SUCCESS != errorCode)
    {
        status.setError(
                    DDS_CTRL_ERR_DDI_CLEAN_ITEM_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/** ---------------------------------------------------------------------------
	This function returns the item ID for the given item from Symbol file

*/
PMStatus DDSController::ddiLocateSymbolId(ENV_INFO2* env, char* symName, ITEM_ID* outId)
{
	PMStatus status;
    int errorCode = FAILURE;

    errorCode = ::locate_symbol_id(env, symName, outId);

    if (SUCCESS != errorCode)
    {
        status.setError(
            DDS_CTRL_ERR_DDI_LOCATE_ITEM_FAILED,
			PM_ERR_CL_DDS_CTRL,
            FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to load a DD for a block
*/
PMStatus DDSController::dsDdBlockLoad(BLOCK_HANDLE bh, DD_HANDLE* ddhPtr)
{
    PMStatus status;
    int errorCode = FAILURE;

    errorCode = ::ds_dd_block_load(bh, ddhPtr);

    if (SUCCESS != errorCode)
    {
        status.setError(
                    DDS_CTRL_ERR_DD_BLK_LOAD_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/* Callback functions *********************************************************/
/*  ----------------------------------------------------------------------------
    This function links the protocol adapter and common service layer function
    pointers with DDS functions
*/
PMStatus DDSController::setCallback(int funID, void* funPtr)
{
    PMStatus status;
    switch (funID)
    {
    case APP_FUNC_GET_BLK_HANDLE:
        app_func_get_block_handle = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        ITEM_ID blockID, unsigned int instanceNum, BLOCK_HANDLE* outBlkHandle)>
        (funPtr);
        break;
    case APP_GET_BLOCK_INSTANCE_BY_INDEX:
        app_get_block_instance_by_index = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        unsigned long objectIndex, unsigned long *outInstance)>(funPtr);
        break;
    case APP_GET_BLOCK_INSTANCE_BY_TAG:
        app_get_block_instance_by_tag = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        ITEM_ID blockID, char *blockTag, unsigned long *outInstance)>(funPtr);
        break;
    case APP_GET_BLOCK_INSTANCE_COUNT:
        app_get_block_instance_count = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        ITEM_ID blockID, unsigned long *outCount)>(funPtr);
        break;
    case APP_GET_SELECTOR_UPCALL:
        app_get_selector_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        ITEM_ID itemID, ITEM_ID selector, ITEM_ID additionalID, double *selValue)>
        (funPtr);
        break;
    case GET_DD_SYM_FILE_EXTN:
        dd_loaded_callback = reinterpret_cast<int (*)(char *fileName)>(funPtr);
        break;
    case DISPLAY_UPCALL:
        display_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* buffer)>(funPtr);
        break;
    case DISPLAY_CONTINUOUS_UPCALL:
        display_continuous_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* prompt, ITEM_ID* varIDs, SUBINDEX* varSubIndices)>(funPtr);
        break;
    case DISPLAY_CONTINUOUS_UPCALL2:
        display_continuous_upcall2 = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* prompt, ITEM_ID* varIDs, SUBINDEX* varSubIndices,
        BLOCK_HANDLE* blockHandles)>(funPtr);
        break;
    case DISPLAY_GET_UPCALL:
        display_get_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* buffer, P_REF* pcPRef, EVAL_VAR_VALUE* varValue)>(funPtr);
        break;
    case DISPLAY_MENU_UPCALL:
        display_menu_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* buffer, SELECT_LIST* selectList, int* selection)>(funPtr);
        break;
    case DISPLAY_MENU_UPCALL2:
        display_menu_upcall2 = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        SELECT_LIST* selectList, int* selection, ITEM_ID menuID)>(funPtr);
        break;
    case DISPLAY_ACK_UPCALL:
        display_ack_upcall = reinterpret_cast<int (*)(ENV_INFO* envInfo,
        char* buffer)>(funPtr);
        break;
    case METHOD_START_UPCALL:
        method_start_upcall = reinterpret_cast<long (*)(ENV_INFO* envInfo,
        ITEM_ID methItemID)>(funPtr);
        break;
    case METHOD_COMPLETE_UPCALL:
        method_complete_upcall = reinterpret_cast<long (*)(ENV_INFO* envInfo,
        ITEM_ID methItemID, int rs)>(funPtr);
        break;
    case METHOD_ERROR_UPCALL:
        method_error_upcall = reinterpret_cast<long (*)(ENV_INFO* envInfo,
        char* errorStr)>(funPtr);
        break;
    case LIST_DELETE_ELEMENT_UPCALL:
        list_delete_element_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID list_id, int index)>(funPtr);
        break;
    case LIST_DELETE_ELEMENT2_UPCALL:
        list_delete_element2_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID list_id, int index, ITEM_ID embedded_id, int embedded_index)>
        (funPtr);
        break;
    case LIST_INSERT_ELEMENT_UPCALL:
        list_insert_element_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID list_id, int index, ITEM_ID element)>(funPtr);
        break;
    case LIST_INSERT_ELEMENT2_UPCALL:
        list_insert_element2_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID listID, int index, ITEM_ID embeddedListID, int embeddedIndex,
        ITEM_ID embeddedElement)>(funPtr);
        break;
    case LIST_READ_ELEMENT_UPCALL:
        list_read_element_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID listID, int index, ITEM_ID subElement, ITEM_ID subSubElement,
        EVAL_VAR_VALUE *outElemData)>(funPtr);
        break;
    case LIST_READ_ELEMENT2_UPCALL:
        list_read_element2_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID listID, int index, ITEM_ID embeddedListID, int embeddedListIndex,
        ITEM_ID subElement, ITEM_ID subSubElement, EVAL_VAR_VALUE *outElemData)>
        (funPtr);
        break;
    case LIST_COUNT_ELEMENT_UPCALL:
        list_count_element_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID listID, unsigned long *count)>(funPtr);
        break;
    case LIST_COUNT_ELEMENT2_UPCALL:
        list_count_element2_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID listID, int index, ITEM_ID embeddedListID, unsigned long *count)>
        (funPtr);
        break;
    case PLOT_CREATE_UPCALL:
        plot_create_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        ITEM_ID plot)>(funPtr);
        break;
    case PLOT_DESTROY_UPCALL:
        plot_destroy_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        long plotHandle)>(funPtr);
        break;
    case PLOT_DISPLAY_UPCALL:
        plot_display_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        long plotHandle)>(funPtr);
        break;
    case PLOT_EDIT_UPCALL:
        plot_edit_upcall = reinterpret_cast<long (*)(ENV_INFO *envInfo,
        long plotHandle, unsigned long waveFormID)>(funPtr);
        break;
    case CHECK_DEVICE_STATUS:
    	getCurrentStatus = reinterpret_cast<int (*)()>(isConnectionAlive);
    	break;
    default:
        LOGF_ERROR(CPMAPP, "Callback function is not registered\n");
        status.setError(DDS_CTRL_ERR_SET_FUN_CALLBACK_FAILED,
                        PM_ERR_CL_DDS_CTRL, FFDDS_ERR_SC_DDS);
        break;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Callback function to get the block handle
*/
int DDSController::appFuncGetBlockHandleCallback(ENV_INFO* envInfo,
    ITEM_ID blockID, unsigned int instanceNum, BLOCK_HANDLE* outBlkHandle)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)envInfo;

    unsigned int currentInstanceNum = 0;
    for(int blockIndex = 0; blockIndex < active_blk_tbl.count;
        blockIndex++)
    {
        ACTIVE_BLK_TBL_ELEM* bte = active_blk_tbl.list[blockIndex];
        if (bte->dd_blk_id == blockID)
        {
            if (instanceNum == 0 ||  currentInstanceNum == instanceNum)
            {
                *outBlkHandle = blockIndex;
                return SUCCESS;
            }
            currentInstanceNum++;
        }
    }

    *outBlkHandle = 0;

    LOGF_ERROR(CPMAPP, "CM block not found\n");
    return CM_BLOCK_NOT_FOUND;
}

/*  ----------------------------------------------------------------------------
    Callback function to get the block instance by index
*/
int DDSController::appGetBlockInstanceByIndex(ENV_INFO* envInfo, ITEM_ID blockID,
                                              unsigned int instanceNum,
                                              unsigned long* outBlkHandle)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)envInfo;

    unsigned int currentInstanceNum = 0;
    for(int blockIndex = 0; blockIndex < active_blk_tbl.count;
        blockIndex++)
    {
        ACTIVE_BLK_TBL_ELEM* bte = active_blk_tbl.list[blockIndex];
        if (bte->dd_blk_id == blockID)
        {
            if (instanceNum == 0 ||  currentInstanceNum == instanceNum)
            {
                *outBlkHandle = blockIndex;
                return SUCCESS;
            }
            currentInstanceNum++;
        }
    }

    *outBlkHandle = 0;

    LOGF_ERROR(CPMAPP, "CM block not found\n");
    return CM_BLOCK_NOT_FOUND;
}

/*  ----------------------------------------------------------------------------
    Callback function to get the block instance by tag
*/
int DDSController::appGetBlockInstanceByTag(ENV_INFO* envInfo, ITEM_ID blockID,
                                            char *blockTag,
                                            unsigned long *outInstance)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)envInfo;

    for(int blockIndex = 0; blockIndex < active_blk_tbl.count;
        blockIndex++)
    {
        ACTIVE_BLK_TBL_ELEM* bte = active_blk_tbl.list[blockIndex];
        if (bte->dd_blk_id == blockID)
        {
            if (strcmp(bte->tag, blockTag) == 0)
            {
                *outInstance = blockIndex;
                return SUCCESS;
            }
        }
    }

    *outInstance = 0;

    LOGF_ERROR(CPMAPP, "CM block not found\n");
    return CM_BLOCK_NOT_FOUND;
}

/*  ----------------------------------------------------------------------------
    Callback function to get the block instance count
*/
int DDSController::appGetBlockInstanceCount(ENV_INFO* envInfo, ITEM_ID blockID,
                                            unsigned long *outCount)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)envInfo;

    for(int blockIndex = 0; blockIndex < active_blk_tbl.count;
        blockIndex++)
    {
        ACTIVE_BLK_TBL_ELEM* bte = active_blk_tbl.list[blockIndex];
        if (bte->dd_blk_id == blockID)
        {
            *outCount = bte->param_count;
            return SUCCESS;
        }
    }

    *outCount = 0;

    LOGF_ERROR(CPMAPP, "CM block not found\n");
    return CM_BLOCK_NOT_FOUND;
}

/*  ----------------------------------------------------------------------------
    Callback function to get the selector (currently not being used)
*/
int DDSController::appGetSelectorUpcall(ENV_INFO* envInfo, ITEM_ID itemID,
                                        ITEM_ID selector, ITEM_ID additionalID,
                                        double *selValue)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) itemID;
    (void) selector;
    (void) additionalID;
    (void) selValue;
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user
*/
int DDSController::displayUpcall(ENV_INFO* envInfo, char* response)
{
    if (nullptr == envInfo || nullptr == response)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);

    if (nullptr == appInfo || nullptr == adaptor)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    // Sends the message to display in UI
    adaptor->sendMethodUIMessage(response, JSON_METHOD_NOTIFY_DISPLAY);

    std::this_thread::sleep_for (std::chrono::milliseconds(MESSAGE_DELAY_INTERVAL));

    // Sends ABORT status to DDS is user abort the ME
    int retValue = BLTIN_SUCCESS;
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        retValue = CM_IF_USR_ABORT;
    }
    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user and display continously
    until user acknowledge
*/
int DDSController::displayContinuousUpcall(ENV_INFO* envInfo, char* prompt,
                                       ITEM_ID* varIDs, SUBINDEX* varSubindices)
{
    if (nullptr == envInfo || nullptr == prompt ||
        nullptr == varIDs  || nullptr == varSubindices)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);
    DDSController* dds = static_cast<DDSController*>(appInfo->ddsControllerInstance);

    if (nullptr == appInfo || nullptr == adaptor || nullptr == dds)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    // Waits until get the input from user
    do
    {
        DDSController::s_pMethodExecutionData.isItemSet = false;

        PMStatus status;
        char response[MAX_PATH_LEN]{};

        // Gets the builtin format string to display in UI
        status = dds->builtinFormatString(response,
                 sizeof(response), prompt, varIDs, varSubindices, envInfo);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Failed to get builtin format string\n");
            return (BLTIN_FAIL_RESPONSE_CODE);
        }

        // Sends the builtin message to display in UI
        std::string str(response);
        adaptor->sendMethodUIMessage(str, JSON_METHOD_NOTIFY_CONTINUOUS);

        std::this_thread::sleep_for (std::chrono::milliseconds(MESSAGE_DELAY_INTERVAL));

    } while(!DDSController::s_pMethodExecutionData.isItemSet &&
            THREAD_STATE_RUN == adaptor->getMethodExecutionThreadState());

    DDSController::s_pMethodExecutionData.isItemSet = false;

    // Sends ABORT status to DDS is user abort the ME
    int retValue = BLTIN_SUCCESS;
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        retValue = CM_IF_USR_ABORT;
    }
    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user and display continously
    until user acknowledge
*/
int DDSController::displayContinuousUpcall2(ENV_INFO *envInfo, char *prompt,
                                            ITEM_ID *varIDs,
                                            SUBINDEX *varSubindices,
                                            BLOCK_HANDLE *blockHandles)
{
    if (nullptr == envInfo || nullptr == prompt ||
        nullptr == varIDs  || nullptr == varSubindices)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);
    DDSController* dds = static_cast<DDSController*>(appInfo->ddsControllerInstance);

    if (nullptr == appInfo || nullptr == adaptor || nullptr == dds)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    // Waits until get the input from user
    do
    {
        DDSController::s_pMethodExecutionData.isItemSet = false;

        PMStatus status;
        char response[MAX_PATH_LEN]{};

        // Gets the builtin format string to display in UI
        status = dds->builtinFormatString2(response,
                 sizeof(response), prompt, varIDs, varSubindices,
                 blockHandles, TRUE, envInfo);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Failed to get builtin format string\n");
            return (BLTIN_FAIL_RESPONSE_CODE);
        }

        // Sends the builtin message to display in UI
        std::string str(response);
        adaptor->sendMethodUIMessage(str, JSON_METHOD_NOTIFY_CONTINUOUS);

        std::this_thread::sleep_for (std::chrono::milliseconds(MESSAGE_DELAY_INTERVAL));

    } while(!DDSController::s_pMethodExecutionData.isItemSet &&
            THREAD_STATE_RUN == adaptor->getMethodExecutionThreadState());

    DDSController::s_pMethodExecutionData.isItemSet = false;

    // Sends ABORT status to DDS is user abort the ME
    int retValue = BLTIN_SUCCESS;
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        retValue = CM_IF_USR_ABORT;
    }
    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user and get new value from user
    in particular field
*/
int DDSController::displayGetUpcall(ENV_INFO* envInfo, char* response,
                                P_REF* pcPRef, EVAL_VAR_VALUE* varValue)
{
    int retValue = BLTIN_SUCCESS;
    PMStatus status;
    unsigned long       mask = INIT_STD_VALUE;
    DDI_BLOCK_SPECIFIER blockSpec;
    DDI_ITEM_SPECIFIER  itemSpec{};
    DDI_GENERIC_ITEM    genericItem{};

    if (nullptr != envInfo || nullptr != varValue)
    {
        APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
        FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);
        DDSController* dds = static_cast<DDSController*>(appInfo->ddsControllerInstance);

        if (nullptr == appInfo || nullptr == adaptor || nullptr == dds)
        {
            LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
            return BLTIN_FAIL_COMM_ERROR;
        }

        DDSController::s_pMethodExecutionData.userInputVal = new EVAL_VAR_VALUE;
        memcpy(DDSController::s_pMethodExecutionData.userInputVal, varValue,
               sizeof(EVAL_VAR_VALUE));

        if (nullptr != pcPRef)
        {
            blockSpec.type = DDI_BLOCK_HANDLE;
            blockSpec.block.handle = envInfo->block_handle;
            itemSpec.type = DDI_ITEM_ID;
            itemSpec.item.id = pcPRef->id;
            itemSpec.subindex = pcPRef->subindex;

            // Get the item for specific item spec
            status = dds->ddiGetItem(&blockSpec,
                              &itemSpec, envInfo, mask, &genericItem);
            if (status.hasError())
            {
                if (nullptr != DDSController::s_pMethodExecutionData.userInputVal)
                {
                    delete DDSController::s_pMethodExecutionData.userInputVal;
                    DDSController::s_pMethodExecutionData.userInputVal = nullptr;
                }

                LOGF_ERROR(CPMAPP, "Failed to get the DD item\n");
                return BLTIN_DDS_ERROR;
            }

            // Expect input from user
            adaptor->getMethodData(response, &genericItem, itemSpec.subindex);
        }
        else
        {
            /* PC reference variable is expected to be NULL in some case,
               in this case we need to fill the default data to proceed further
               execution of method
            */
            adaptor->getMethodDefaultData(response, varValue);
        }

        // Waits until get the input from user
        dds->acquireLock();
        DDSController::s_pMethodExecutionData.isItemSet = false;
        memcpy(varValue, DDSController::s_pMethodExecutionData.userInputVal,
               sizeof(EVAL_VAR_VALUE));

        // Sends ABORT status to DDS is user abort the ME
        if (DDSController::s_pMethodExecutionData.isAborted)
        {
            retValue = CM_IF_USR_ABORT;
        }

        if (nullptr != DDSController::s_pMethodExecutionData.userInputVal)
        {
            delete DDSController::s_pMethodExecutionData.userInputVal;
            DDSController::s_pMethodExecutionData.userInputVal = nullptr;
        }

        dds->ddiCleanItem(&genericItem);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user and get selected option
    from user
*/
int DDSController::displayMenuUpcall(ENV_INFO* envInfo, char* response,
                                 SELECT_LIST* selectList, int* selection)
{
    if (nullptr == envInfo || nullptr == response ||
        nullptr == selectList || nullptr == selection)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);
    DDSController* dds = static_cast<DDSController*>(appInfo->ddsControllerInstance);

    if (nullptr == appInfo || nullptr == adaptor || nullptr == dds)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    SELECT_OPTION* selectOptionP;
    SELECT_OPTION* selectOptionEndP;
    unsigned int   optionLimit;
    string inpBuff;
    std::map<int, std::string> enumMap;

    selectOptionP = selectList->select_option;
    optionLimit = static_cast<unsigned>(selectList->select_count);
    selectOptionEndP = selectOptionP + optionLimit;

    // Key should be starts with 1, because the enumeration value is starts qith 1
    int key = 1;
    enumMap.clear();

    // Parse the selection and add into enumeration map
    while (selectOptionP < selectOptionEndP)
    {
        inpBuff = selectOptionP++->select_string;
        enumMap.insert(std::pair<int, std::string>(key, inpBuff));
        ++key;
    }

    DDSController::s_pMethodExecutionData.userInputVal = new EVAL_VAR_VALUE;
    DDSController::s_pMethodExecutionData.userInputVal->type = DDS_INTEGER;

    // Constructs the enumeration value JSon to send to UI
    adaptor->fillMethodEnumValAsJson(response, enumMap, *selection);

    // Waits until get the input from user
    dds->acquireLock();
    DDSController::s_pMethodExecutionData.isItemSet = false;

    *selection = DDSController::s_pMethodExecutionData.userInputVal->val.i;

    if (nullptr != DDSController::s_pMethodExecutionData.userInputVal)
    {
        delete DDSController::s_pMethodExecutionData.userInputVal;
        DDSController::s_pMethodExecutionData.userInputVal = nullptr;
    }

    // Sends ABORT status to DDS if user abort the ME
    int retValue = BLTIN_SUCCESS;
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        retValue = CM_IF_USR_ABORT;
    }

    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user and get selected option
    from user
    TODO (Bharath) : Implement the Menu Display once the design is approved
*/
int DDSController::displayMenuUpcall2(ENV_INFO* envInfo, SELECT_LIST* selectList,
                                      int* selection, ITEM_ID menuID)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)envInfo;
    (void)selectList;
    (void)selection;
    (void)menuID;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message and wait's until user acknowledges
*/
int DDSController::displayAckUpcall(ENV_INFO* envInfo, char* response)
{
    if (nullptr == envInfo || nullptr == response)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);
    DDSController* dds = static_cast<DDSController*>(appInfo->ddsControllerInstance);

    if (nullptr == appInfo || nullptr == adaptor || nullptr == dds)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    std::string str = response;
    if (SUCCESS == str.compare(JSON_METHOD_NOTIFY_ABORT))
    {
        LOGF_DEBUG(CPMAPP, "Method aborted by user\n");
        return BLTIN_SUCCESS;
    }

    // Sends ack message and expect acknowledgement from UI
    adaptor->sendMethodUIMessage(response, JSON_METHOD_NOTIFY_ACK);

    // Waits until get the input from user
    dds->acquireLock();
    DDSController::s_pMethodExecutionData.isItemSet = false;

    // Sends ABORT status to DDS is user abort the ME
    int retValue = BLTIN_SUCCESS;
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        retValue = CM_IF_USR_ABORT;
    }

    return retValue;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user about Method start status
*/
long DDSController::methodStartUpcall(ENV_INFO* envInfo, ITEM_ID methItemID)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)methItemID;

    if (nullptr == envInfo)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);

    if (nullptr == appInfo || nullptr == adaptor)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    DDSController::s_pMethodExecutionData.isAborted = false;

    adaptor->sendMethodUIMessage("", JSON_METHOD_NOTIFY_START);

    return BLTIN_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user about Method completion
    status
*/
long DDSController::methodCompleteUpcall(ENV_INFO* envInfo,
                                         ITEM_ID methItemID, int methodResult)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void)methItemID;

    if (nullptr == envInfo)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);

    if (nullptr == appInfo || nullptr == adaptor)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    // to store the result of upcalls
    DDSController::s_pMethodExecutionData.m_methodResult = methodResult;

    std::string str(DDSController::s_pMethodExecutionData.methodLabel);
    if (methodResult != BLTIN_SUCCESS)
    {
        LOGF_ERROR(CPMAPP, "Method execution " << str
                  << " completed with error\n");
    }
    else
    {
        LOGF_DEBUG(CPMAPP, "Method execution " << str
                  << " completed successfullyr\n");
    }
    // Intimate UI, Method execution completed
    adaptor->sendMethodUIMessage("", JSON_METHOD_NOTIFY_COMPLETED);

    // Destroys the ME thread after completion
    adaptor->destroyMethodExecutionThread();

    return BLTIN_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Callback function used to send message to user about Method error status
*/
long DDSController::methodErrorUpcall(ENV_INFO* envInfo, char* errorStr)
{
    if (nullptr == envInfo || nullptr == errorStr)
    {
        LOGF_ERROR(CPMAPP, "Bad Data received\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    APP_INFO* appInfo = static_cast<APP_INFO*>(envInfo->app_info);
    FFAdaptor* adaptor = static_cast<FFAdaptor*>(appInfo->adaptorInstance);

    if (nullptr == appInfo || nullptr == adaptor)
    {
        LOGF_ERROR(CPMAPP, "Invalid data received from DDS\n");
        return BLTIN_FAIL_COMM_ERROR;
    }

    // Sends the error message to UI
    adaptor->sendMethodUIMessage(errorStr, "ERROR");
    LOGF_ERROR(CPMAPP, "ERROR occured in Method execution " << errorStr << " \n");

    return BLTIN_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    TODO(developer): Implement list and plot releated upcalls to send the list
    of data to UI when list support is added to the application.
    Currently list upcalls are defined and NOT being used.
*/
/*  ----------------------------------------------------------------------------
    Callback function used to delete the specific element from the UI list
*/
long DDSController::listDeleteElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                            int index)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to delete the specific element from the UI list by
    using embedded ID and index
*/
long DDSController::listDeleteElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                             int index, ITEM_ID embeddedListID,
                                             int embeddedIndex)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) embeddedListID;
    (void) embeddedIndex;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to insert the element to UI list
*/
long DDSController::listInsertElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                            int index, ITEM_ID element)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) element;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to insert the element to UI list by using embedded ID
    and index
*/
long DDSController::listInsertElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                             int index, ITEM_ID embeddedListID,
                                             int embeddedIndex,
                                             ITEM_ID embeddedElement)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) embeddedListID;
    (void) embeddedIndex;
    (void) embeddedElement;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to read the specific element from the UI list
*/
long DDSController::listReadElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                          int index, ITEM_ID subElement,
                                          ITEM_ID subSubElement,
                                          EVAL_VAR_VALUE *outElemData)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) subElement;
    (void) subSubElement;
    (void) outElemData;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to read the specific element from the UI list by
    using embedded ID and index
*/
long DDSController::listReadElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                           int index, ITEM_ID embeddedListID,
                                           int embeddedListIndex,
                                           ITEM_ID subElement,
                                           ITEM_ID subSubElement,
                                           EVAL_VAR_VALUE *outElemData)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) embeddedListID;
    (void) embeddedListIndex;
    (void) subElement;
    (void) subSubElement;
    (void) outElemData;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to count the number of elements in UI list
*/
long DDSController::listCountElementUpcall(ENV_INFO *envInfo, ITEM_ID listID,
                                           unsigned long *count)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) count;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to count the number of elements in UI list by
    using embedded ID and index
*/
long DDSController::listCountElement2Upcall(ENV_INFO *envInfo, ITEM_ID listID,
                                            int index, ITEM_ID embeddedListID,
                                            unsigned long *count)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) listID;
    (void) index;
    (void) embeddedListID;
    (void) count;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to create a plot in UI
*/
long DDSController::plotCreateUpcall(ENV_INFO *envInfo, ITEM_ID plot)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) plot;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to destroy a plot of UI
*/
long DDSController::plotDestroyUpcall(ENV_INFO *envInfo, ITEM_ID plot)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) plot;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to display a plot in UI
*/
long DDSController::plotDisplayUpcall(ENV_INFO *envInfo, long plotHandle)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) plotHandle;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function used to edit a plot of UI
*/
long DDSController::plotEditUpcall(ENV_INFO *envInfo, long plotHandle,
                                   unsigned long waveFormID)
{
    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) envInfo;
    (void) plotHandle;
    (void) waveFormID;
    return BLTIN_NOT_IMPLEMENTED;
}

/*  ----------------------------------------------------------------------------
    Callback function which is used get the DD filename which is loaded during
    device connection
*/
int DDSController::loadedDDCallback(char *filePath)
{
    if (nullptr != filePath)
    {
        std::string fileExt(filePath);

        // Parse the file extension from file path
        m_symFileExt = fileExt.substr(fileExt.length() - DD_FILE_EXTN_LEN);

        LOGF_INFO(CPMAPP, "Loaded DD binary file: " << filePath);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Failed to get DD filename\n");
        return FAILURE;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Function to get string encoding used in loaded dd binary
*/
int DDSController::getDDStringEncoding(void)
{
    DDI_STRING_ENCODING ddEncoding = ENCODING_UNKNOWN;

    // Check for string encoding from loaded DD binary file
    ENV_INFO2 envInfo2{};
    APP_INFO appInfo{};
    DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

    // Get the current Device handle
    getActiveDeviceHandle(&deviceHandle);

    // Set the Environment Info for ddi_get_tem2
    envInfo2.device_handle = deviceHandle;
    envInfo2.type = ENV_DEVICE_HANDLE;
    envInfo2.app_info = &appInfo;

    if (0 != ddi_get_string_encoding (&envInfo2, &ddEncoding))
        LOGF_ERROR(CPMAPP, "Could not retrieve string encoding");

    return ddEncoding;
}

/*  ----------------------------------------------------------------------------
    It aquires the thread lock and wait till release
*/
void DDSController::acquireLock()
{
    // Waits until get the input from user
    std::unique_lock<std::mutex> methodLock(m_methodExecutionLock);
    DDSController::s_pMethodExecutionData.isWaitingForUserInput = true;
    m_methodExecutionConditionVariable.wait(methodLock, []
    {
        return DDSController::s_pMethodExecutionData.isItemSet;
    });
}

/*  ----------------------------------------------------------------------------
    It releases the locked thread and notify for further process
*/
void DDSController::releaseLock()
{
    // Unlocks the thread and notify that user's input arrived
    std::unique_lock<std::mutex> methodLock(m_methodExecutionLock);
    methodLock.unlock();
    m_methodExecutionConditionVariable.notify_one();
}


/* MI wrapper functions********************************************************/
/*  ----------------------------------------------------------------------------
    This function is used to execute the method
*/
PMStatus DDSController::executeMethod(std::string blockTag, ITEM_ID methodID)
{
    PMStatus status;

    if (nullptr != m_pMI)
    {
        status = m_pMI->executeMethod(blockTag, methodID);
    }
    else
    {
        status.setError(
                    DDS_CTRL_ERR_EXECUTE_METHOD_FAILED,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get block handle by block tag
*/
BLOCK_HANDLE DDSController::getBlockHandleByBlockTag(const char* tag)
{
    if (nullptr == tag)
    {
        return INVALID_ID_OR_HANDLE;
    }
    else
    {
        if (strlen(tag) <= 0)
        {
            return INVALID_ID_OR_HANDLE;
        }
        else
        {
            return :: ct_block_search(tag);
        }
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to get block handle by block ID
*/
BLOCK_HANDLE DDSController::getBlockHandleByBlockID(ITEM_ID id)
{
    BLOCK_HANDLE        block_handle;

    // Go through each element in the Active Block Table.
    for (block_handle = 0; block_handle < active_blk_tbl.count; block_handle++)
    {
        // Check to make sure that the element is valid.
        if (active_blk_tbl.list[block_handle])
        {
            // Check the Id of the element.
            if (ABT_DD_BLK_ID(block_handle) == id)
            {
                return(block_handle);
            }
        }
    }
    return(-1);
}

/*  ----------------------------------------------------------------------------
    This function is used to get block tag from block ID
*/
char* DDSController::getBlockTagByBlockID(ITEM_ID id, unsigned int instanceNum)
{
    BLOCK_HANDLE        block_handle;
    unsigned int currentInstanceNum = 0;

    // Go through each element in the Active Block Table.
    for (block_handle = 0; block_handle < active_blk_tbl.count; block_handle++)
    {
        // Check to make sure that the element is valid.
        if (active_blk_tbl.list[block_handle])
        {
            // Check the Id of the element.
            if (ABT_DD_BLK_ID(block_handle) == id)
            {
                if (instanceNum == 0 || ++currentInstanceNum == instanceNum)
                    return active_blk_tbl.list[block_handle]->tag;
            }
        }
    }
    return NULL;
}

/*  ----------------------------------------------------------------------------
    This function is used to get block ID by block handle
*/
PMStatus DDSController::getBlockIDByBlockHandle(BLOCK_HANDLE bh, ITEM_ID* id)
{
    PMStatus status;

    int errorCode = :: get_abt_dd_blk_id(bh, id);

    if (SUCCESS != errorCode || id <= 0)
    {
        status.setError(
                    DDS_CTRL_ERR_GET_BLTN_FORMAT_STRING,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to convert builitin format string
*/
PMStatus DDSController::builtinFormatString(char* outBuf, int maxLength,
                                       char* passedPrompt, ITEM_ID* paramIDs,
                                       SUBINDEX* subIndices, ENV_INFO* envInfo)
{
    PMStatus status;

    int errorCode = :: bltin_format_string(outBuf, maxLength, passedPrompt,
                                           paramIDs, subIndices, envInfo);

    if (SUCCESS != errorCode)
    {
        status.setError(
                    DDS_CTRL_ERR_GET_BLTN_FORMAT_STRING,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to convert builitin format string
*/
PMStatus DDSController::builtinFormatString2(char* outBuf, int maxLength,
                                       char* passedPrompt, ITEM_ID* paramIDs,
                                       SUBINDEX* subIndices, BLOCK_HANDLE *blockHandle,
                                       int useBH, ENV_INFO* envInfo)
{
    PMStatus status;

    ENV_INFO2 envInfo2{};
    env_to_env2(envInfo, &envInfo2);

    int errorCode = :: bltin_format_string2(outBuf, maxLength, passedPrompt,
                                            paramIDs, subIndices, blockHandle,
                                            useBH, &envInfo2);

    if (SUCCESS != errorCode)
    {
        status.setError(
                    DDS_CTRL_ERR_GET_BLTN_FORMAT_STRING,
                    PM_ERR_CL_DDS_CTRL,
                    FFDDS_ERR_SC_DDS);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Converts DDS date & time format to User date & time format
*/
void DDSController::convDDSDateNTimeToUserDateNTime(char charArray[],
                                                    int* dateArray)
{
    :: conv_dds_datentime_user_datentime(charArray, dateArray);
}

/*  ----------------------------------------------------------------------------
    Converts user date & time format to DDS date & time format
*/
void DDSController::convUserDateNTimeToDDSDateNTime(int* dateArray,
                                                    char charArray[])
{
    :: conv_user_datentime_to_dds_datentime(dateArray, charArray);
}

/*  ----------------------------------------------------------------------------
    Converts dds date format to user date format
*/
void DDSController::convDDSDateToUserDate(char charArray[], int* dateArray)
{
    :: conv_dds_date_to_user_date(charArray, dateArray);
}

/*  ----------------------------------------------------------------------------
    Converts user date format to dds date format
*/
void DDSController::convUserDateToDDSDate(int *dateArray, char charArray[])
{
    :: conv_user_date_to_dds_date(dateArray, charArray);
}

/*  ----------------------------------------------------------------------------
    Converts dds time variable to user time format
*/
void DDSController::convDDSTimeToUserTime(char charArray[], int* dateArray)
{
    :: conv_dds_time_user_time(charArray, dateArray);
}

/*  ----------------------------------------------------------------------------
    Converts dds time variable to user time format
*/
void DDSController::convUserTimeToDDSTime(int *dateArray, char charArray[])
{
    :: conv_user_time_dds_time(dateArray, charArray);
}

/*  ----------------------------------------------------------------------------
    Converts dds duration format to user duration format
*/
void DDSController::convDDSDurationToUserDuration(char charArray[], unsigned long long *mSec)
{
    :: conv_dds_duration_to_user_duration(charArray, mSec);
}

/*  ----------------------------------------------------------------------------
    Converts User duration format to DDS duration format
*/
void DDSController::convUserDurationToDDSDuration(unsigned long long mSec, char charArray[])
{
    :: conv_user_duration_to_dds_duration(mSec, charArray);
}

/*  ----------------------------------------------------------------------------
    Converts dds timeval format to user timeval format
*/
void DDSController::convDDSTimeValToUserTimeVal(char charArray[], int *dateArray)
{
    :: conv_dds_timeval_to_user_timeval(charArray, dateArray);
}

/*  ----------------------------------------------------------------------------
    Converts user timeval format to dds timeval format
*/
void DDSController::convUserTimeValToDDSTimeVal(unsigned long epochtime, int *dateArray, char *charArray)
{
    :: conv_user_timeval_to_dds_timeval(epochtime, dateArray, charArray);
}
/*  ----------------------------------------------------------------------------
    Converts epoch time to user time
*/
std::string DDSController::convertsEpochTimeToUserTime(time_t epochTime, const char* format)
{
    char timestamp[64] = {0};
    strftime(timestamp, sizeof(timestamp), format, gmtime(&epochTime));
    return timestamp;
}

/*  ----------------------------------------------------------------------------
    Converts user time to epoch time
*/
time_t DDSController::convertsUserTimeToEpochTime(const char* userTime, const char* format)
{
    std::tm tmTime;
    memset(&tmTime, 0, sizeof(tmTime));
    strptime(userTime, format, &tmTime);
    return timegm(&tmTime);
}

/*  ----------------------------------------------------------------------------
    Fetch the item type by item ID
*/
ITEM_TYPE DDSController::getItemType(ITEM_ID itemID, string blockTag)
{
    ITEM_TYPE itemType;
    DDI_ITEM_SPECIFIER itemSpec;
    APP_INFO appInfo{};

    itemSpec.type = DDI_ITEM_ID;
    itemSpec.item.id = itemID;

    if (blockTag.empty())
    {
        /* Device Level Standard Menu's, are not associated with any blocks,
           needs to be handled seperately
        */
        ENV_INFO2 envInfo2{};
        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

        // Get the current Device handle
        getActiveDeviceHandle(&deviceHandle);

        // Set the Environment Info for ddi_get_tem2
        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;
        envInfo2.app_info = &appInfo;
        appInfo.status_info = USE_EDIT_VALUE;

        ddiGetItemType2(&envInfo2, &itemSpec, &itemType);
    }
    else
    {
        DDI_BLOCK_SPECIFIER blockSpec;
        blockSpec.type = DDI_BLOCK_TAG;
        blockSpec.block.tag = const_cast<char *>(blockTag.c_str());
        ddiGetItemType(&blockSpec, &itemSpec, &itemType);
    }

    return itemType;
}

/*  ----------------------------------------------------------------------------
    Fetch the item type by item ID
*/
void DDSController::ddiGetItemType(DDI_BLOCK_SPECIFIER *blockSpecifier,
                                   DDI_ITEM_SPECIFIER *itemSpecifier,
                                   ITEM_TYPE *itemType)
{
    :: ddi_get_type(blockSpecifier, itemSpecifier, itemType);
}

/*  ----------------------------------------------------------------------------
    Fetch the item type by item ID
*/
void DDSController::ddiGetItemType2(ENV_INFO2 *envInfo2,
                                   DDI_ITEM_SPECIFIER *itemSpecifier,
                                   ITEM_TYPE *itemType)
{
    :: ddi_get_type2(envInfo2, itemSpecifier, itemType);
}

/*  ----------------------------------------------------------------------------
    Fetches device ID information
*/
PMStatus DDSController::getDeviceIDInfo(DD_DEVICE_ID& ddDeviceID)
{
    PMStatus status;
    BLOCK_HANDLE bh{0};

    if (0 != get_abt_dd_device_id(bh, &ddDeviceID))
    {    
        status.setError(DDS_CTRL_ERR_GET_DEVICE_INFO,PM_ERR_CL_DDS_CTRL);
    }    
    return status;
}

/*  ----------------------------------------------------------------------------
    Function to get DD file's extension which is loaded during device connection
*/
std::string DDSController::getDDFileExtn()
{
    return m_symFileExt;
}


/*  ----------------------------------------------------------------------------
    Function to check if device connection is alive
*/
int DDSController::isConnectionAlive()
{
	if(CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_DISCONNECTION_IN_PROGRESS )
	{
		return SUCCESS;
	}
	return FAILURE;
}

/*  ----------------------------------------------------------------------------
    Function to get language specific string based on language code
*/
void DDSController::getLanguageString
(
    const char* srcString, 
    string& dstString, 
    unsigned int length
)
{
    auto convStr = new char[length+1];
    memset(convStr,'\0', length+1);

    if (m_langCode.empty())
    {
        m_langCode = CommonProtocolManager::getInstance()->getLangCode();
    }

    // Retrieve language specific string
    if ((nullptr != srcString) && (nullptr != convStr))
	{
         if (0 == ddi_get_string_translation( srcString,
                                              const_cast<char*>(m_langCode.c_str()),
									          convStr, 
                                              length))
         {

             dstString.assign(convStr);
         }
         else
         {
             dstString.assign(srcString);
         }

        delete[] convStr;
	}
}

/*  ----------------------------------------------------------------------------
    Convert ASCII string to UTF8
*/
std::string DDSController::getUTF8String(char* srcString)
{
    std::string utf8Str = "";
    char utf8Data[MAX_UTF8_STR_LENGTH]{};

    // Convert to UTF8 string data
    ddi_iso_to_utf8(srcString, utf8Data, MAX_UTF8_STR_LENGTH);

    utf8Str = utf8Data;

    return utf8Str;
}
