
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 * Name: rcsim
 * ShortDesc: provide DDS simulated response codes for type A and
 *		type B devices.
 *
 * Description:
 *		rcsim file provides functions for setting up and retrieving
 *		different types of response codes for devices in a simulated DDS
 *		environment.
 *
 * Author:
 *		Joe Fisher
 *
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     /* ANSI */

#include "cm_lib.h"
#include "rcsim.h"

typedef struct {
	int				rcsim_class;
	RCSIM_PARAM		*rcsim_param;
	RCSIM_VALUE		rcsim_value;
	void			*next_rcv_rec;
}	VALUE_RECORD;
 
typedef struct {
	int				station_address;
	void			*next_dr;	/* points to next device in list */
} DEVICE_RECORD;

typedef struct {
	int				network_handle;	/* network configuration id */
	int				network_type;		/* type A or type B */
	DEVICE_RECORD	*first_dr;			/* points to first device */
	void			*next_nr;			/* points to next network */
} NETWORK_RECORD;

/* private macros */

/* first_nr points to beginning of network list. */
NETWORK_RECORD	*first_nr = NULL;
/* first_rcv_rec points to beginning of response code list. */
VALUE_RECORD	*first_rcv_rec = NULL;

const VALUE_RECORD	*get_next_rcv_rec;	/* points to last get record */
									/* used in get_next() */
 
/************************************************************************
 *
 * Name: create_rcv_rec()
 *
 * ShortDesc: Create response code value record.
 *
 * Description:
 *		create_rcv_rec creates response code value record and
 *		assigns items from parameters.
 *
 *		call free_rcv_rec() to free up storage.
 *
 * Inputs:
 *		rcsim_class	-	RCSIM_TYPE_xxx response code type.
 *		rcsim_param -	pointer to RCSIM_PARAM buffer containing network
 *				and device information.
 *		rcsim_value - 	pointer to RCSIM_VALUE buffer containing
 *				response code value.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns pointer to new value record.
 *		returns NULL if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
VALUE_RECORD *
create_rcv_rec(int rcsim_class, const RCSIM_PARAM *rcsim_param, 
        const RCSIM_VALUE *rcsim_value)
{

	VALUE_RECORD	*rcv_rec;
	RCSIM_PARAM		*rcp;
	void			*rc_value;
	
	/* allocate space for rc value record. */
	rcv_rec = (VALUE_RECORD *)malloc(sizeof(VALUE_RECORD));
	if (rcv_rec == (VALUE_RECORD *)NULL) {
		return (VALUE_RECORD *)NULL;
	}
	(void)memset(rcv_rec, 0, sizeof(VALUE_RECORD));
	
	/* allocate space for rc value */
	rc_value = malloc(rcsim_value->size);
	if (rc_value == NULL) {
		free (rcv_rec);
		return (VALUE_RECORD *)NULL;
	}
	(void)memset(rc_value, 0, rcsim_value->size);

	/* allocate space for rc parameters if available. */
	rcp = NULL;
	if (rcsim_param != NULL) {
		rcp = (RCSIM_PARAM *)malloc(sizeof(RCSIM_PARAM));
		if (rcp == NULL) {
			free (rc_value);
			free (rcv_rec);
			return (VALUE_RECORD *)NULL;
		}
		(void)memset(rcp, 0, sizeof(RCSIM_PARAM));
	}
	
	/* transfer arguments to new value record. */
	rcv_rec->rcsim_class = rcsim_class;
	rcv_rec->rcsim_param = rcp;
	if ((rcp != NULL) &&(rcsim_param != NULL)) {
		(void)memcpy(rcp, rcsim_param, sizeof(RCSIM_PARAM));
	}
	rcv_rec->rcsim_value.type = rcsim_value->type;
	rcv_rec->rcsim_value.size = rcsim_value->size;
	rcv_rec->rcsim_value.value = rc_value;
	(void)memcpy(rc_value, rcsim_value->value, rcsim_value->size);

	return rcv_rec;
}


/************************************************************************
 *
 * Name: free_rcv_rec()
 *
 * ShortDesc: free response code value record memory.
 *
 * Description:
 *		free_dr frees response code value record memory.
 *
 *		call create_rcv_rec() to allocate storage.
 *
 * Inputs:
 *		rcv_rec - pointer to response code value record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns 0 if successful.
 *		returns 1 if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_rcv_rec(VALUE_RECORD *rcv_rec)
{

	if (rcv_rec != NULL) {
		if (rcv_rec->rcsim_param != NULL) {
			free (rcv_rec->rcsim_param);
		}
		if (rcv_rec->rcsim_value.value != NULL) {
			free (rcv_rec->rcsim_value.value);
		}
		free(rcv_rec);
	}
	return 0;
}


/************************************************************************
 *
 * Name: add_rcv_rec()
 *
 * ShortDesc: adds response code value record to list.
 *
 * Description:
 *		add_rcv_rec adds response code value record to list.
 *
 *		use create_rcv_rec() to create record and setting items before
 *		calling this function.
 *
 * Inputs:
 *		rcv_rec - pointer to response code value record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
add_rcv_rec(VALUE_RECORD	*rcv_rec)
{
	VALUE_RECORD	*this_rcv_rec;

	/* if this is first record in list then update list header. */
	if (first_rcv_rec == NULL) {
		first_rcv_rec = rcv_rec;
	} else {	
		/* add device to bottom of device record list. */
		/* point to first device in list. */
		this_rcv_rec = first_rcv_rec;
		while (this_rcv_rec->next_rcv_rec != NULL) {
			this_rcv_rec = this_rcv_rec->next_rcv_rec;
		}
		this_rcv_rec->next_rcv_rec = rcv_rec;
	}

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: free_rcv_rec_list()
 *
 * ShortDesc: free all response code value records in list.
 *
 * Description:
 *		free_rcv_rec_list frees all response code value record memory
 *		from list.
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_rcv_rec_list()
{

	VALUE_RECORD	*next_rcv_rec, *this_rcv_rec;
	int				r_code;

	this_rcv_rec = first_rcv_rec;
	while (this_rcv_rec != NULL) {
		/* save pointer to next record in list. */
		next_rcv_rec = this_rcv_rec->next_rcv_rec;
		/* free up this device record. */
		r_code = free_rcv_rec(this_rcv_rec);
		if (r_code != RCSIM_SUCCESS) {
			return (r_code);
		}
		/* point to next device record in list. */
		this_rcv_rec = next_rcv_rec;
	}

	/* indicate that this list is free'd. */
	first_rcv_rec = NULL;

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: create_dr()
 *
 * ShortDesc: Create empty device record.
 *
 * Description:
 *		create_dr creates empty device record.
 *
 *		call free_dr() to free up storage.
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns pointer to new device record.
 *		returns NULL if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
DEVICE_RECORD *
create_dr()
{
	DEVICE_RECORD	*dr;

	dr = (DEVICE_RECORD *)malloc(sizeof(DEVICE_RECORD));
	if (dr == (DEVICE_RECORD *)NULL) {
		return (DEVICE_RECORD *)NULL;
	}
	(void)memset(dr, 0, sizeof(DEVICE_RECORD));

	return (dr);
}


/************************************************************************
 *
 * Name: free_dr()
 *
 * ShortDesc: free device record memory.
 *
 * Description:
 *		free_dr frees device record memory.
 *
 *		call create_dr() to allocate storage.
 *
 * Inputs:
 *		dr - pointer to device record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns 0 if successful.
 *		returns 1 if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_dr(DEVICE_RECORD *dr)
{

	if (dr != NULL) {
		free(dr);
	}
	return 0;
}


/************************************************************************
 *
 * Name: create_nr()
 *
 * ShortDesc: Create empty network record.
 *
 * Description:
 *		create_nr creates empty network record.
 *
 *		call free_nr() to free up storage.
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns pointer to new network record.
 *		returns NULL if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
NETWORK_RECORD *
create_nr()
{
	NETWORK_RECORD	*nr;

	nr = (NETWORK_RECORD *)malloc(sizeof(NETWORK_RECORD));
	if (nr == (NETWORK_RECORD *)NULL) {
		return (NETWORK_RECORD *)NULL;
	}
	(void)memset(nr, 0, sizeof(NETWORK_RECORD));
	return (nr);
}


/************************************************************************
 *
 * Name: free_nr()
 *
 * ShortDesc: free network record memory.
 *
 * Description:
 *		free_nr frees network record memory.
 *
 *		call create_nr() to allocate storage.
 *
 * Inputs:
 *		nr - pointer to network record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns 0 if successful.
 *		returns 1 if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_nr(NETWORK_RECORD *nr)
{

	if (nr != NULL) {
		free(nr);
	}
	return 0;
}


/************************************************************************
 *
 * Name: add_nr()
 *
 * ShortDesc: adds network record to network list.
 *
 * Description:
 *		add_nr adds network record to network list.
 *
 *		use create_nr() to create network record and setting items before
 *		calling this function.
 *
 * Inputs:
 *		nr - pointer to network record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
add_nr(NETWORK_RECORD *nr)
{
	NETWORK_RECORD	*this_nr;

	/* if this is first network in list then update first network */
	/* record pointer. */
	if (first_nr == NULL) {
		first_nr = nr;
	} else {	
		/* add device to bottom of device record list. */
		/* point to first device in list. */
		this_nr = first_nr;
		while (this_nr->next_nr != NULL) {
			this_nr = this_nr->next_nr;
		}
		this_nr->next_nr = nr;
	}

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: free_dr_list()
 *
 * ShortDesc: free all devices in list.
 *
 * Description:
 *		free_dr_list frees all device record memory from list.
 *
 * Inputs:
 *		first_dr - pointer to first device record in list.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_dr_list(DEVICE_RECORD *first_dr)
{

	DEVICE_RECORD	*next_dr, *this_dr;
	int				r_code;

	this_dr = first_dr;
	while (this_dr != NULL) {
		/* save pointer to next device record in list. */
		next_dr = this_dr->next_dr;
		/* free up this device record. */
		r_code = free_dr(this_dr);
		if (r_code != RCSIM_SUCCESS) {
			return (r_code);
		}
		/* point to next device record in list. */
		this_dr = next_dr;
	}
	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: free_nr_list()
 *
 * ShortDesc: frees all networks and associated devices from memory.
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
free_nr_list()
{

	NETWORK_RECORD		*this_nr, *next_nr;
	int					r_code;

	this_nr = first_nr;
	while (this_nr != NULL) {
		/* save pointer to next network record in list. */
		next_nr = this_nr->next_nr;
		/* free up device record list first. */
		r_code = free_dr_list(this_nr->first_dr);
		if (r_code != RCSIM_SUCCESS) {
			return (r_code);
		}
		/* go ahead and free up this network record. */
		r_code = free_nr(this_nr);
		if (r_code != RCSIM_SUCCESS) {
			return (r_code);
		}
		/* point to next network record in list. */
		this_nr = next_nr;
	}

	/* indicate that list is free'd. */
	first_nr = NULL;

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: get_nr()
 *
 * ShortDesc: get network record from list.
 *
 * Description:
 *		get_nr gets network record from network list.
 *
 * Inputs:
 *		net_id - integer containing device's network configuration id.
 *		network_type - integer containing network type.
 *
 * Outputs:
 *		nr - address of pointer which will contain network record address.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
get_nr(int net_id, int network_type, NETWORK_RECORD **nr)
{
	NETWORK_RECORD		*this_nr;

	this_nr = first_nr;
	while (this_nr != NULL) {
		if ((this_nr->network_handle == net_id) &&
			(this_nr->network_type == network_type)) {
			*nr = this_nr;
			return (RCSIM_SUCCESS);
		}
		/* point to next network record in list. */
		this_nr = this_nr->next_nr;
	}

	return (RCSIM_ERR_NETWORK_FIND);
}


/************************************************************************
 *
 * Name: add_dr()
 *
 * ShortDesc: adds device record to device list.
 *
 * Description:
 *		add_dr adds device record to network's device list.
 *
 *		use create_dr() to create device record and setting items before
 *		calling this function.
 *
 * Inputs:
 *		net_id - integer containing network configuration id for which
 *			device belongs to.
 *		network_type - integer containing network type.
 *		dr - pointer to device record.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		returns RCSIM_SUCCESS if successful.
 *		returns RCSIM_ERR_xxx if unsuccessful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static 
int
add_dr(int net_id, int network_type, DEVICE_RECORD *dr)
{

	int				r_code;
	NETWORK_RECORD	*nr;
	DEVICE_RECORD	*this_dr;

	/* find network in list and get pointer to device list. */
	r_code = get_nr(net_id, network_type, &nr);
	if (r_code != RCSIM_SUCCESS) {
		return (r_code);
	}

	/* if this is first device in list then update network record. */
	if (nr->first_dr == NULL) {
		nr->first_dr = dr;
	} else {	
		/* add device to bottom of device record list. */
		/* point to first device in list. */
		this_dr = nr->first_dr;
		while (this_dr->next_dr != NULL) {
			this_dr = this_dr->next_dr;
		}
		this_dr->next_dr = dr;
	}

	return RCSIM_SUCCESS;
}




/***********************************************************************
 *
 *	Name:  read_a_sim_file
 *
 *	ShortDesc:  Read type A Device Simulator data file.
 *
 *	Description:
 *
 *	Inputs:
 *		config_file_id - the Configuration File ID of the simulated
 *						 type A Network.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *
 *	Author:
 *		RCSIM_SUCCESS if all is successful.
 *		RCSIM_ERR_xxx if error encountered (see rcsim.h for defines.)
 *
 *	Author:
 *		Joe Fisher
 *
 **********************************************************************/
static
int
read_a_sim_file(int config_file_id)
{

	char			data_file_name[128];
	FILE			*data_file;
	char			file_line[256];
	int				r_code;
	DEVICE_RECORD	*dr;
	int				last_device_station_address;
	char			block_tag[128];
	int				device_station_address;
	int				mfg_id;
	int				dev_type;
	int				dev_rev;
	int				block_op_index;
	int				block_parm_ct;
	char			block_dd_name[128];
	int				block_class;

	/*
	 *	Open the data file
	 */

	(void)sprintf(data_file_name, "%s%02d.dat", "a_sim", config_file_id);

	data_file = fopen(data_file_name, "r");
	if (!data_file) {
		/* CRASH_DBG(); */
		return(RCSIM_ERR_SIM_FILE_OPEN);
	}

	last_device_station_address = -1;

	while(NULL != fgets(file_line, 256, data_file)) 
    {
        if ((file_line[0] == '#') || (file_line[0] == '\n') || (file_line[0] == '\r')) {

			/*
			 *	Go on to read the next line of the data file.
			 */

			continue;

		} else {

			/*
			 *	scan line block entry.
			 */
			r_code = sscanf(file_line, "%s %d %d %d %d %d %d %s %d",
				block_tag, &device_station_address, &mfg_id, &dev_type,
				&dev_rev, &block_op_index, &block_parm_ct, block_dd_name,
				&block_class);

			if (r_code != 9) {
				return (RCSIM_ERR_SIM_FILE_READ);
			}

			/*
			 *	if block device same as last then ignore.
			 */
			if (device_station_address == last_device_station_address) {
				continue;
			}
			last_device_station_address = device_station_address;

			/*
			 *	create device record.
			 */
			dr = create_dr();
			if (dr == NULL) {
				return (RCSIM_ERR_MEMORY);
			} 

			/*
			 *	fill in device record.
			 */
			dr->station_address = device_station_address;

			/*
			 *	add device record to device's list.
			 */
			r_code = add_dr(config_file_id, NT_A_SIM, dr);
			if (r_code != RCSIM_SUCCESS) {
				return (r_code);
			}
		}
	}

	r_code = fclose(data_file);
	if (r_code) {
		return(RCSIM_ERR_SIM_FILE_CLOSE);
	}

	return(RCSIM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:	init_network_record_list
 *
 *	ShortDesc:	Initialize the Network record list.
 *
 *	Description:
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		RCSIM_SUCCESS if all is successful.
 *		RCSIM_ERR_xxx if error encountered (see rcsim.h for defines.)
 *
 *	Author:
 *		Joe Fisher
 *
 **********************************************************************/
static 
int
init_network_record_list()
{

	FILE				*data_file;
	char				 file_line[256];
	NETWORK_RECORD		*nr;
	NET_TYPE			 net_dat_network_type;
	UINT32				 network_route_1;
	UINT32				 network_route_2;
	UINT32				 network_route_3;
	int					 device_family;
	int					 config_file_id;
	int					 r_code;

	/*
	 *	Open the network data file.
	 */

	data_file = fopen("network.dat", "r");
	if (!data_file) {
		/* CRASH_DBG(); */
		return(RCSIM_ERR_NET_FILE_OPEN);
	}

	/*
	 *	Go through the file and read in each network entry.
	 */

	while(fgets(file_line, 256, data_file) != NULL) {
		/*
		 *	Check for comments or blank lines.
		 */

		if ((file_line[0] == '#') || (file_line[0] == '\n') || (file_line[0] == '\r')) {

			/*
			 *	Go on to read the next line of the data file.
			 */

			continue;

		} else {

			/*
			 *	Scan in network entry.
			 */

			r_code = sscanf(file_line, "%d %d %d %d %d %d",
				&net_dat_network_type, &network_route_1, &network_route_2,
				&network_route_3, &device_family, &config_file_id);

			if (r_code != 6) {
				return(RCSIM_ERR_NET_FILE_READ);
			}
			
			/*
			 *	create new network list record.
			 */
			nr = create_nr();
            if (NULL == nr)
            {
                return (RCSIM_ERR_MEMORY);
			} 

			/*
			 *	setup network record information.
			 */
            nr->network_handle = config_file_id;
            nr->network_type = net_dat_network_type;

			/*
			 *	add network record to network list.
			 */
			r_code = add_nr(nr);
			if (r_code != RCSIM_SUCCESS) {
				return (r_code);
			}

			/*
			 *	build network's devices list from simulation file.
			 *	based on network type.
			 */

			switch (net_dat_network_type) {
			case NT_A_SIM:            
			 	r_code = read_a_sim_file(config_file_id);
			 	if (r_code != RCSIM_SUCCESS) {
			 		return (r_code);
			 	}
			 	break;
			case NT_OFF_LINE:
			case NT_A:
			case NT_ETHER:
			case NT_CONTROL:
				break;	/* we do not support these types at this time. */
			default:
				break;	/* invalid type altogether!!!*/
			}
		}
	}

	/*
	 *	Close the network data file.
	 */

	r_code = fclose(data_file);
	if (r_code) {
		return(RCSIM_ERR_NET_FILE_CLOSE);
	}

	return(RCSIM_SUCCESS);
}


/************************************************************************
 *
 * Name: rcsim_init()
 *
 * ShortDesc: Initialize DDS simulator response code tables.
 *
 * Description:
 *		rcsim_init reads network.dat file (found in current directory)
 *		and allocates storage for each simulated network and each network
 *		device.
 *
 *		should call rcsim_free() to free up storage.
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		RCSIM_SUCCESS if everything o.k.
 *		RCSIM_ERR_xxx if unsuccessful (see rcsim.h)
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_init()
{
	int		r_code;

	(void) free_nr_list();
	(void) free_rcv_rec_list();	
	r_code = init_network_record_list();
	return r_code;
}


/************************************************************************
 *
 * Name: rcsim_free()
 *
 * ShortDesc: Free-up DDS simulator response code tables.
 *
 * Description:
 *		rcsim_free tears-down memory allocated for rcsim_init().
 *
 * Inputs:
 *		None.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		RCSIM_SUCCESS if everything o.k.
 *		RCSIM_ERR_xxx if unsuccessful (see rcsim.h)
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_free()
{
	int		r_code;

	r_code = free_nr_list();
	if (r_code != RCSIM_SUCCESS) {
		return r_code;
	}
	r_code = free_rcv_rec_list();
	return r_code;
}


/************************************************************************
 *
 * Name: find_rcv_rec()
 *
 * ShortDesc: Find response code value record in list.
 *
 * Description:
 *		find_rcv_rec searches response code value record list for a
 *		match on response code class item, and items in the additional
 *		parameters object.
 *
 *		If additional parameters object points to NULL then a value
 *		record with a parameter object having NULL is searched for.
 *		Otherwise, there is a match attempted on value record parameter
 *		objects.
 *
 * Inputs:
 *		rcsim_class -	type of response code to set (see
 *			rcsim.h for response code class types -
 *			RCSIM_TYPE_xxx.)
 *		rcsim_param -		pointer to RCSIM_PARAM buffer containing
 *			additional parameters for this value (ie. network config id,
 *			station address, etc.)  This allows caller to narrow scope
 *			of response code down to a particular device if needed.
 *
 * Outputs:
 *		rcv_rec	-	address to VALUE_RECORD pointer which will point
 *			to response code value record if found.
 *
 * Returns:
 *		1 if response code value record found.
 *		0 if record not found.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static
int
find_rcv_rec(int rcsim_class, const RCSIM_PARAM *rcsim_param, VALUE_RECORD **rcv_rec)
{

	int					r_code;
	VALUE_RECORD		*current_rcv_rec;

	current_rcv_rec = first_rcv_rec;
	while (current_rcv_rec != NULL) {
		if (current_rcv_rec->rcsim_class == rcsim_class) {
			if (current_rcv_rec->rcsim_param == NULL) {
				if (rcsim_param == NULL) {
					*rcv_rec = current_rcv_rec;
					return 1;
				}
			} else {
				r_code = memcmp ((char*)current_rcv_rec->rcsim_param,
							(char*)rcsim_param, sizeof(RCSIM_PARAM));
				if (r_code == 0) {
					*rcv_rec = current_rcv_rec;
					return 1;
				}
			}
		}
		current_rcv_rec = current_rcv_rec->next_rcv_rec;	
	}

	return 0;
}


/************************************************************************
 *
 * Name: modify_rcv_rec_value()
 *
 * ShortDesc: Modify response code record's value item.
 *
 * Description:
 *		modify_rcv_rec_value replaces a response code value record's
 *		response code value item.
 *
 *		No changes are made to the response code value record's
 *		rcsim_class or rcsim_param items.
 *
 * Inputs:
 *		rcv_rec -	pointer to VALUE_RECORD which needs modification.
 *		new_rcsim_value -	pointer RCSIM_VALUE item which will replace
 *			rcv_rec's rcsim_value items.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		RCSIM_SUCCESS if successful.
 *		RCSIM_ERR_FAILURE if not successful.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
static
int
modify_rcv_rec_value(VALUE_RECORD *rcv_rec, const RCSIM_VALUE *new_rcsim_value)
{
	void	*new_value;

	/* reallocate value item memory. */
	new_value = realloc (rcv_rec->rcsim_value.value, new_rcsim_value->size);
	if (new_value == NULL) {
		return RCSIM_ERR_MEMORY;
	}
	(void)memcpy (new_value, new_rcsim_value->value, new_rcsim_value->size);

	rcv_rec->rcsim_value.type = new_rcsim_value->type;
	rcv_rec->rcsim_value.size = new_rcsim_value->size;
	rcv_rec->rcsim_value.value = new_value;

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: rcsim_set()
 *
 * ShortDesc: Set DDS simulator response code for device.
 *
 * Description:
 *		rcsim_set sets a particular response code class for a
 *		device on a network.  The device is specified by a station
 *		address.  The network is specified by the simulated network
 *		configuration id.
 *
 * Inputs:
 *		rcsim_class -	type of response code to set (see
 *			rcsim.h for response code class types -
 *			RCSIM_TYPE_xxx.)
 *		rcsim_param -		pointer to RCSIM_PARAM buffer containing
 *			additional parameters for this value (ie. network config id,
 *			station address, etc.)  This allows caller to narrow scope
 *			of response code down to a particular device if needed.
 *		rcsim_value -		pointer to RCSIM_VALUE containing response
 *			code value.
 *
 * Outputs:
 *		None.
 *
 * Returns:
 *		RCSIM_SUCCESS if everything o.k.
 *		RCSIM_ERR_xxx if unsuccessful (see rcsim.h)
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_set(int rcsim_class, const RCSIM_PARAM *rcsim_param, const RCSIM_VALUE *rcsim_value)
{

	int					r_code;
	VALUE_RECORD		*rcv_rec;

	/* we want to check response code value record list for a record */
	/* that matches. */
	r_code = find_rcv_rec (rcsim_class, rcsim_param, &rcv_rec);

	if (r_code == 1) {
		/* we have a response code value record meeting these parameters */
		/* modify this response code value record with new info. */
		r_code = modify_rcv_rec_value (rcv_rec,	rcsim_value);
		if (r_code != 0) {
			return RCSIM_ERR_MEMORY;
		}
	} else {
		/* we do not have a response code value record. */
		/* create new and add to list */
		rcv_rec = create_rcv_rec (rcsim_class, rcsim_param, rcsim_value);
		if (rcv_rec == NULL) {
			return RCSIM_ERR_MEMORY;
		}
		r_code = add_rcv_rec (rcv_rec);
		if (r_code != 0) {
			return RCSIM_ERR_MEMORY;
		}
	}

	/* *
	 * may want to carry set further to positively identify network,
	 * station address, etc.  This data was taken from simulation file
	 * network.dat and network simulation files.
	 */ 

	 /*** TO BE ADDED LATER ***/

	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: rcsim_get()
 *
 * ShortDesc: Get DDS simulator response code for device.
 *
 * Description:
 *		rcsim_get gets a particular response code class for a
 *		device on a network.  The device is specified by a station
 *		address.  The network is specified by the simulated network
 *		configuration id.
 *
 * Inputs:
 *		rcsim_class -	type of response code to set (see
 *			rcsim.h for response code class types -
 *			RCSIM_TYPE_xxx.)
 *		rcsim_param -	pointer to RCSIM_PARAM buffer containing
 *			additional parameters for this value (ie. network config id,
 *			station address, etc.)  This allows caller to narrow scope
 *			of response code down to a particular device if needed. If
 *			the value of the pointer is NULL then a 'general' type of
 *			rcsim class is searched for.
 *		max_value_buf_size -	calling program needs to allocate storage
 *			for actual value before calling and this value indicates
 *			maximum size of buffer.
 *
 * Outputs:
 *		rcsim_value -	address of pointer to RCSIM_VALUE which will
 *			contain response code value.
 *
 * Returns:
 *		RCSIM_MATCH if match found.
 *		RCSIM_NOMATCH if match not found.
 *		RCSIM_ERR_xxx if other bad things happenned (see rcsim.h)
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_get(int rcsim_class, const RCSIM_PARAM *rcsim_param, RCSIM_VALUE *rcsim_value,
	size_t max_value_buf_size)
{

	int					r_code;
	size_t				buf_size;
	VALUE_RECORD		*rcv_rec;

	/* we want to check response code value record list for a record */
	/* that matches. */
	r_code = find_rcv_rec (rcsim_class, rcsim_param, &rcv_rec);
	if (r_code == 1) {
		/* we have a match, copy record's value into caller's buffer */
		/* clean out receiving value buffer. */
		(void)memset (rcsim_value->value, 0, max_value_buf_size);
		/* we do not want to exceed user's value buffer size. */
		buf_size = rcv_rec->rcsim_value.size;
		if (buf_size > max_value_buf_size) {
			buf_size = max_value_buf_size;	/* there will be truncation */
		}
		(void)memcpy (rcsim_value->value, 
				rcv_rec->rcsim_value.value, buf_size);
		rcsim_value->type = rcv_rec->rcsim_value.type;
		rcsim_value->size = buf_size;
		return RCSIM_MATCH;
	}

	return RCSIM_NOMATCH;
}


/************************************************************************
 *
 * Name: rcsim_get_first()
 *
 * ShortDesc: Get first simulator response code from list.
 *
 * Description:
 *		rcsim_get_first() obtains first response code value record from
 *		list.
 *
 * Inputs:
 *		max_value_buf_size -	calling program needs to allocate storage
 *			for actual value before calling and this value indicates
 *			maximum size of buffer.  The address of this buffer needs to
 *			be set into rcsim_value->value before calling.
 *
 * Outputs:
 *		rcsim_class -	pointer response code class buffer (see
 *			rcsim.h for response code class types -
 *			RCSIM_TYPE_xxx.)
 *		rcsim_param -	address of pointer to RCSIM_PARAM which will
 *			contain parameter values.  If no parameter values are in
 *			rcsim record then this value will be NULL (which indicates
 *			a response code for a general environment.)
 *		rcsim_value -	pointer to RCSIM_VALUE which will
 *			contain response code value.
 *
 * Returns:
 *		RCSIM_SUCCESS if first item in list returned.
 *		RCSIM_NO_ITEMS if no items in list.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_get_first(int *rcsim_class, RCSIM_PARAM **rcsim_param,
	RCSIM_VALUE *rcsim_value, size_t max_value_buf_size)
{
	size_t				buf_size;

	/* set global so we can keep track in subsequent calls to get_next() */
	if (first_rcv_rec == NULL) {
		return RCSIM_NO_ITEMS;
	}
	get_next_rcv_rec = first_rcv_rec;

	/* clean out receiving value buffer. */
	(void)memset (rcsim_value->value, 0, max_value_buf_size);

	/* copy rcsim class item. */
	*rcsim_class = get_next_rcv_rec->rcsim_class;
	/* copy record items to parameters. */
	if (get_next_rcv_rec->rcsim_param != NULL) {
		(void)memcpy (*rcsim_param,
				get_next_rcv_rec->rcsim_param, sizeof(RCSIM_PARAM));
	} else {
		*rcsim_param = (RCSIM_PARAM *)NULL;
	}
	/* we do not want to exceed user's value buffer size. */
	buf_size = get_next_rcv_rec->rcsim_value.size;
	if (buf_size > max_value_buf_size) {
		buf_size = max_value_buf_size;	/* there will be truncation */
	}
	(void)memcpy (rcsim_value->value, 
			get_next_rcv_rec->rcsim_value.value, buf_size);
	rcsim_value->type = get_next_rcv_rec->rcsim_value.type;
	rcsim_value->size = buf_size;
	return RCSIM_SUCCESS;
}


/************************************************************************
 *
 * Name: rcsim_get_next()
 *
 * ShortDesc: Get next simulator response code from list.
 *
 * Description:
 *		rcsim_get_next() obtains next response code value record from
 *		list.
 *
 *		Note: should use rcsim_get_first() to set searching list properly.
 *
 * Inputs:
 *		max_value_buf_size -	calling program needs to allocate storage
 *			for actual value before calling and this value indicates
 *			maximum size of buffer.  The address of this buffer needs to
 *			be set into rcsim_value->value before calling.
 *
 * Outputs:
 *		rcsim_class -	pointer response code class buffer (see
 *			rcsim.h for response code class types -
 *			RCSIM_TYPE_xxx.)
 *		rcsim_param -	address of pointer to RCSIM_PARAM which will
 *			contain parameter values.  If no parameter values are in
 *			rcsim record then this value will be NULL (which indicates
 *			a response code for a general environment.)
 *		rcsim_value -	pointer to RCSIM_VALUE which will
 *			contain response code value.
 *
 * Returns:
 *		RCSIM_SUCCESS if next item in list returned.
 *		RCSIM_END_OF_LIST if no items left in list.
 *
 * Author:
 *		Joe Fisher
 *
 ***********************************************************************/
int
rcsim_get_next(int *rcsim_class, RCSIM_PARAM **rcsim_param,
	RCSIM_VALUE *rcsim_value, size_t max_value_buf_size)
{
	size_t				buf_size;

	/*
	 *	point to next rcsim record in list.
	 */
	if (get_next_rcv_rec == NULL) {
		return RCSIM_END_OF_LIST;
	}
	get_next_rcv_rec = get_next_rcv_rec->next_rcv_rec;
	if (get_next_rcv_rec == NULL) {
		return RCSIM_END_OF_LIST;
	}

	/* clean out receiving value buffer. */
	(void)memset (rcsim_value->value, 0, max_value_buf_size);

	/* copy rcsim class item. */
	*rcsim_class = get_next_rcv_rec->rcsim_class;
	/* copy record items to parameters. */
	if (get_next_rcv_rec->rcsim_param != NULL) {
		(void)memcpy (*rcsim_param,
				get_next_rcv_rec->rcsim_param, sizeof(RCSIM_PARAM));
	} else {
		*rcsim_param = (RCSIM_PARAM *)NULL;
	}
	/* we do not want to exceed user's value buffer size. */
	buf_size = get_next_rcv_rec->rcsim_value.size;
	if (buf_size > max_value_buf_size) {
		buf_size = max_value_buf_size;	/* there will be truncation */
	}
	(void)memcpy (rcsim_value->value, 
			get_next_rcv_rec->rcsim_value.value, buf_size);
	rcsim_value->type = get_next_rcv_rec->rcsim_value.type;
	rcsim_value->size = buf_size;
	return RCSIM_SUCCESS;
}
