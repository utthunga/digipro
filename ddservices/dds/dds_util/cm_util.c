/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FF Connection Manager utility definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "std.h"
#include "tst_dds.h"
#include "ddi_lib.h"
#include "app_xmal.h"
#include "pc_loc.h"
#include "cm_loc.h"
#include "ddssetup.h"
#include "dds_comm.h"
#include "int_lib.h"
#include "read_meth.h"
#include "int_diff.h"
#include "connect.h"
#include "ffh1_util.h"
#include "method_tbl.h"
#include "CommonStructures.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#define STATIC_VIEW_INDEX   1
#define VIEW1_2             2
#define ACTUAL_MODE_SI      2
#define MODE_BLOCK_INDEX    4
#define	MAX_DICT_TABLE_SIZE 10000

#define DATE   (0)
#define MONTH  (1)
#define YEAR   (2)
#define HOUR   (3)
#define MINUTE (4)
#define SECOND (5)
#define MILLISECOND (6)
#define MICROSECOND (7)

#define NUM_OF_SEC_IN_DAY (24*60*60)
#define NUM_OF_MSEC_IN_SEC (1000)

int                 warning_level   =  1;
DEVICE_HANDLE       global_dhandle  = -1;
DEVICE_TYPE_HANDLE  global_dthandle = -1;
SP_PTR_TABLE        sp_ptr_table;
char                release_path[FILE_PATH_LEN];

CM_NMA_VCR_LOAD nma_vcr_load;
CM_NMA_VCR_LOAD nma_vcr_load;
CM_NMA_INFO     nma_info;

ID_TABLE_HEADER id_table;

UNIT_REL   unit_rel;
REF_REL    ref_rel;
WAO_REL    wao_rel;

methTableList meth_tbl_lst;

unsigned long 		num_dict_table_entries = 0;
DICT_TABLE_ENTRY 	dict_table[MAX_DICT_TABLE_SIZE];

extern	ID_TABLE_HEADER  	id_table;

extern FILE    *infileptr;
extern int      file_line_number;
extern int		strings_flag;
extern	int		atoi() ;
extern UNIT_REL   unit_rel;
extern REF_REL    ref_rel;
extern WAO_REL wao_rel;

typedef struct {
    ITEM_ID item_id;
    char str[64];
    BLOCK_HANDLE bh;
}SAVE_ITEM;

SAVE_ITEM saveItem[20];
int saveIdCount = 0;

int isUnitInTransducer = FALSE;
int isTranducer = FALSE;
int isUnitAvail = FALSE;

/*  ----------------------------------------------------------------------------
    Build the network table of the list of connected devices in the network
*/
int build_device_table(ENV_INFO* env_info, NETWORK_HANDLE nh)
{
    int rs;
    rs = cr_build_device_table(env_info, nh); 
    if (rs) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in Building the device table, rs = %d\n", rs);
        return rs;
    }
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Connects the device specified with address for connection in the network
*/
int dds_connect_device(ENV_INFO* env_info, NETWORK_HANDLE nh)
{
    int             rs;

    rs = build_device_table( env_info, nh); 
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in building Device table\n");
        return rs;
    }
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    API to set application state to normal
*/
static void set_appl_normal(ENV_INFO *env_info)
{
	cr_set_appl_state_normal(env_info);
}

/*  ----------------------------------------------------------------------------
    API to abort the mib fbap cr
*/
int abort_mib_fbap_cr(ENV_INFO *env_info, NETWORK_HANDLE nh)
{
	int rs;
	unsigned short mib_cr = 0;
	unsigned short fbap_cr = 0;

	rs = get_nt_net_mib_cr(nh, &mib_cr);
	if(rs != SUCCESS)
	{
                LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
	    return rs;
	}

    rs = get_nt_net_fbap_cr(nh, &fbap_cr);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB Local address\n");
        return rs;
    }

    rs = cr_abort_cr(env_info, mib_cr, fbap_cr);

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to write the nma to local vcr
*/
int write_nma_local_vcr(ENV_INFO *env_info,
    unsigned short dev_addr, int vfd, NETWORK_HANDLE nh)
{
    int rs;
    unsigned short addr;

    if(vfd == VFD_MIB)
    {
        rs = get_nt_net_mib_addr((NETWORK_HANDLE)nh, &addr);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB address\n");
            return rs;
        }

        rs = get_nt_net_mib_cr(0, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
            return rs;
        }
        rs = get_nt_net_mib_local_addr(0, &nma_vcr_load.loc_add);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB Local address\n");
            return rs;
        }
    }
    else if(vfd == VFD_FBAP)
    {
        rs = get_nt_net_fbap_addr(0, &addr);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB Local address\n");
            return rs;
        }

        rs = get_nt_net_fbap_cr(0, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB Local address\n");
            return rs;
        }

        /* The local address for the MIB_VCR and
         * FBAP_VCR are nothing but fas_dll_local_addr
         * ( which is local endpoint address) and has to be unique 
         * within host stack (values 0x20 to 0xf7) 
         */

        rs = get_nt_net_fbap_local_addr(0, &nma_vcr_load.loc_add);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB Local address\n");
            return rs;
        }
    }

    nma_vcr_load.rem_add = (UINT16) addr;
    (void *)memcpy((CM_NMA_INFO *)&nma_vcr_load.nma_info,
                   (CM_NMA_INFO *)&nma_info, sizeof(CM_NMA_INFO));
    nma_vcr_load.nma_wr_flag = TRUE;

    rs = cr_wr_local_vcr(env_info, 0, &nma_vcr_load);

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to initiate the fms
*/
int initiate_fms(ENV_INFO *env_info, unsigned short dev_addr,
        int vfd, NETWORK_HANDLE nh)
{
    int rs;
    if(VFD_MIB == vfd)
    {
        rs = get_nt_net_mib_cr((NETWORK_HANDLE)nh, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
                    return rs;
        }
    }
    else if(VFD_FBAP == vfd)
    {
        rs = get_nt_net_fbap_cr((NETWORK_HANDLE)nh, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
                return rs;
        }
    }

    /* FMS initiation on CR is required to get the
       information on the local CR
    */
    rs = cr_fms_initiate(env_info, nh, &nma_vcr_load);

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to get the object description
*/
int get_od(ENV_INFO *env_info, unsigned short dev_addr, int vfd,
        NETWORK_HANDLE nh)
{
    int rs;
    if(VFD_MIB == vfd)
    {
        rs = get_nt_net_mib_cr((NETWORK_HANDLE)nh, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
            return rs;
        }
                    
    }
    else if(VFD_FBAP == vfd)
    {
        rs = get_nt_net_fbap_cr((NETWORK_HANDLE)nh, &nma_vcr_load.cr_index);
        if(rs != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the FBAP CR\n");
            return rs;
        }
    }

    /*
     * Get Object Description and 
     * information on the local CR
     */
            
    rs = cr_get_od(env_info, nh, &nma_vcr_load);
    
    return rs;
}

/*  ----------------------------------------------------------------------------
    API to write the fm to local vcr
*/
int write_fms_local_vcr(ENV_INFO *env_info,
        unsigned short dev_addr, NETWORK_HANDLE nh)
{
    unsigned short device_address;
    int count;
    int rs;

    rs = get_nt_net_mib_addr(nh, &device_address);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
        return rs;
    }

    // Find the FBAP index whose local address is not of remote MIB address
             
    for (count = 0; count < nma_vcr_load.nma_no_vcr_entries; count++)
    {
        if(AR_TYPE_SERVER == nma_vcr_load.nma_vcr_entry[count].type_and_role) 
        if (nma_vcr_load.nma_vcr_entry[count].loc_addr != device_address)
        {
            nma_vcr_load.index = 
                (UINT16) nma_vcr_load.nma_vcr_entry[count].index;
            nma_vcr_load.loc_add = 
                (UINT16) nma_vcr_load.nma_vcr_entry[count].loc_addr;
            SET_NT_FBAP_ADDR((NETWORK_HANDLE)nh, nma_vcr_load.loc_add);
            break;
        }
    }

    rs = get_nt_net_mib_cr((NETWORK_HANDLE)nh, &nma_vcr_load.cr_index);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in getting the MIB CR\n");
        return rs;
    }

    /*
     * The local address for the MIB_VCR and 
     * FBAP_VCR are nothing but fas_dll_local_addr
     * ( which is local endpoint address) and has to be unique 
     * within host stack (values 0x20 to 0xf7) 
     */
                
    (void *)memcpy((CM_NMA_INFO *)&nma_vcr_load.nma_info,
                   (CM_NMA_INFO *)&nma_info, sizeof(CM_NMA_INFO));
    nma_vcr_load.nma_wr_flag = FALSE;

    rs = cr_wr_local_vcr(env_info, (NETWORK_HANDLE)nh, &nma_vcr_load);

    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in  Setting the VCR at device\n");
    }
    return rs;
}

/*  ----------------------------------------------------------------------------
    API to setup the device connection
*/
int setup_device_connection(ENV_INFO *env_info, unsigned short dev_addr, 
        NETWORK_HANDLE nh)
{
    int rs = SUCCESS;
    int choice = SET_APPL_STATE_BUSY;
    int m_retry = 0;

    while(choice != LAST_STAGE)
    {
        switch(choice)
        {
            case SET_APPL_STATE_BUSY:
            case MIB_WR_LOCAL_VCR: 
                rs = write_nma_local_vcr(env_info, dev_addr, VFD_MIB, nh);
                break;

            case MIB_FMS_INITIATE:
                rs = initiate_fms(env_info, dev_addr, VFD_MIB, nh);
                break;

            case MIB_GET_OD:
                rs = get_od(env_info, dev_addr, VFD_MIB, nh);
                break;
            
            case FBAP_WR_FMS_LOCAL_VCR:
                rs = write_fms_local_vcr(env_info, dev_addr, nh);
                break;

            case FBAP_WR_LOCAL_VCR:
                rs = write_nma_local_vcr(env_info, dev_addr, VFD_FBAP, nh);
                break;

            case FBAP_FMS_INITIATE:
                rs = initiate_fms(env_info, dev_addr, VFD_FBAP, nh);
                break;

            case FBAP_GET_OD:
                rs = get_od(env_info, dev_addr, VFD_FBAP, nh);
                set_appl_normal(env_info);
                break;
        }
        choice++;

        if (rs != SUCCESS) 
        {
        	/*
        	 * IF the return code is FAS busy, try disconnection and then
        	 * re-initiate the communication.
        	 */
        	if(rs == CM_IF_FAS_ABT_RC5)
        	{
        		rs = abort_mib_fbap_cr(env_info, nh);
        	}
            sleep(2); //Increased from 1

            /* Re-try to create local VCR */
            m_retry++;
            LOGC_INFO(CPM_FF_DDSERVICE,"Retrying to connect = %d\n", m_retry);
            choice = MIB_WR_LOCAL_VCR;
            if (m_retry > 3)
            {
                m_retry = 0;
                break;
            }
        }
    }

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to establish connection with the device.
*/
int ff_connect(RET_INFO* ret_info, unsigned short dev_addr, void *device)
{
    DeviceInfo* deviceinfo = (DeviceInfo*)device;

    int rs = FAILURE;
    APP_INFO        app_info;
    ENV_INFO        env_info;
    NETWORK_HANDLE  nh = 0;
    NT_COUNT        nt_count = 0;
    DEV_TBL   *list;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    app_info.status_info = USE_EDIT_VALUE;

    /* Init table manager */
    rs = ct_init();
    if (rs != CM_SUCCESS) {
        return(rs);
    }
             
    /* ROD Manager */
    rs = rod_init();
    if (rs != CM_SUCCESS) {
        return(rs);
    }

    network_tbl.list = (NETWORK_TBL_ELEM *) realloc(
        (char *)network_tbl.list,
        (unsigned int)(sizeof(NETWORK_TBL_ELEM) *
        (network_tbl.count + 1)));

    if (!network_tbl.list) {
        rs = CM_NO_MEMORY;
        goto exit_error;
    }

    (void)memset((char *)network_tbl.list, 0, (sizeof(NETWORK_TBL_ELEM)));

    /*
     *  Set up the Network Table entry with network type,
     *  device family, and config file id information.
     */

    SET_NT_NET_TYPE(network_tbl.count, NT_NET_TYPE(0));
    SET_NT_DEV_FAMILY(network_tbl.count, DF_FF);
    SET_NT_CFG_FILE_ID(network_tbl.count, NO_CONFIG_ID);
    SET_NT_STATION_ADDR(network_tbl.count, dev_addr);
    SET_NT_MIB_CR(network_tbl.count, CR2);
    SET_NT_FBAP_CR(network_tbl.count, CR3);
    SET_NT_MIB_ADDR(network_tbl.count, (UINT16) (dev_addr << 8 | 0xF8));
    SET_NT_MIB_LOCAL_ADDR(network_tbl.count, LOCAL_ADDR1);
    SET_NT_FBAP_LOCAL_ADDR(network_tbl.count, LOCAL_ADDR2);

    rs = cr_build_network_table((NETWORK_HANDLE)(network_tbl.count));
    if (rs != SUCCESS) {
       LOGC_DEBUG(CPM_FF_DDSERVICE,"Error in building the network table");
       goto exit_error;
    }

    network_tbl.count++;
    rs = get_nt_net_count(&nt_count);
    if (rs != SUCCESS) {
        goto exit_error;
    }
    rs = get_nt_net_handle_by_addr(nt_count, dev_addr, &nh);
    if (rs != SUCCESS) {
        goto exit_error;
    }

    if(NT_A == NT_NET_TYPE(0))
        rs = setup_device_connection(&env_info, dev_addr, nh);
        if(rs != SUCCESS) {
            goto exit_error;
    }

    rs = dds_connect_device(&env_info, nh);
    if(rs != SUCCESS)
    {
        if(FFDS_MULT_DEV_INIT_ERR == rs)
        {
            LOGC_INFO(CPM_FF_DDSERVICE,"Multiple device connection, rs = %d\n", rs);
        }

        goto exit_error;
    }

    LOGC_INFO(CPM_FF_DDSERVICE,"Finished reading device information for node 0x%02x", dev_addr);

    LOGC_INFO(CPM_FF_DDSERVICE,"Manufacturer ID: %06x",
        network_tbl.list->dev_table.dd_device_id.ddid_manufacturer);
    LOGC_INFO(CPM_FF_DDSERVICE,"Device Type: %04x",
        network_tbl.list->dev_table.dd_device_id.ddid_device_type);
    LOGC_INFO(CPM_FF_DDSERVICE,"Device Revision: %02x",
        network_tbl.list->dev_table.dd_device_id.ddid_device_rev);
    LOGC_INFO(CPM_FF_DDSERVICE,"DD Revision: %02x",
        network_tbl.list->dev_table.dd_device_id.ddid_dd_rev);

    list = &network_tbl.list->dev_table;
    deviceinfo->dd_rev = list->dd_device_id.ddid_dd_rev;
    deviceinfo->device_rev = list->dd_device_id.ddid_device_rev;
    deviceinfo->device_type = list->dd_device_id.ddid_device_type;
    deviceinfo->manufacturer = list->dd_device_id.ddid_manufacturer;
    deviceinfo->blk_count = list->blk_data_tbl.count;

    int block = 0;
    for(; block < list->blk_data_tbl.count; block++)
    {
        deviceinfo->blk_tag[block] = list->blk_data_tbl.list->blk_tag;
        deviceinfo->dd_tag[block] = list->blk_data_tbl.list->dd_tag;
        list->blk_data_tbl.list++;
    }

/* exit: */
    LOGC_DEBUG(CPM_FF_DDSERVICE,"Connect Device successful");
    return rs;

exit_error:
    LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in Connect Device %d %s\n", rs,
            dds_error_string(rs));
    
    ret_info->reason_code = rs;
    strcpy(ret_info->reason_str, dds_error_string(rs));

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to initialize the FF Back-end application
*/
int ff_initialize(RET_INFO *ret_info, const char *dd_path, HOST_CONFIG *host_config)
{
    int                 rs;
    const char          *dict_file = "standard.dct";
    const char          *dict_file1 = "standard.dc8";
    char                *env_var_path;
    DDS_TBL_FN_PTRS     dds_tbl_fn_ptrs;
    APP_INFO_FN_PTRS    app_info_fn_ptrs;
    char                dict_file_path[FILE_PATH_LEN];
    char                dict_file_path1[FILE_PATH_LEN];
    APP_INFO            app_info;
    ENV_INFO            env_info;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;

    if(NULL == dd_path)
    {
        env_var_path = getenv(NTOK_PATH_ENV_NAME);
        if(NULL == env_var_path)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"No DD lib path %s\n",
                    dds_error_string(CM_BAD_DD_PATH));
            ret_info->reason_code = CM_BAD_DD_PATH;
            strcpy(ret_info->reason_str, dds_error_string(CM_BAD_DD_PATH));
            return CM_BAD_DD_PATH;
        }
        (void)strcpy(release_path, env_var_path);
    }
    else
    {
        (void)strcpy(release_path, dd_path);
    }

    memset(&nma_vcr_load, 0, sizeof(CM_NMA_VCR_LOAD));

    /*
    * Initialize Connection Manager and set appropriate routines
    */

    dds_tbl_fn_ptrs.build_dd_dev_tbl_fn_ptr = ddi_build_dd_dev_tbls;
    dds_tbl_fn_ptrs.build_dd_blk_tbl_fn_ptr = ddi_build_dd_blk_tbls;
    dds_tbl_fn_ptrs.remove_dd_dev_tbl_fn_ptr = ddi_remove_dd_dev_tbls;
    dds_tbl_fn_ptrs.remove_dd_blk_tbl_fn_ptr = ddi_remove_dd_blk_tbls;

    cm_init_dds_tbl_fn_ptrs(&dds_tbl_fn_ptrs);

    /*
     * Initialization of the connection manager call back function
     */
    connect_comm_stack_a();

    /*
     * Initialization of the communication manager and start of the
     * FF stack and initiating the FBK board by calling FBK_init()
     */
    rs = cm_init(&env_info, release_path, host_config);

    if (rs) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initialize the communication manager %s\n",
               dds_error_string(rs) );
        if (CM_BAD_DD_PATH == rs)
            LOGC_ERROR(CPM_FF_DDSERVICE,"Incorrect DD path: %s\n", dd_path);
        ret_info->reason_code = rs;
        strcpy(ret_info->reason_str, dds_error_string(rs));

        return rs;
    }

    /*
     *  Initialize the Parameter Cache routines
     */

    (void)memset ((char *)&app_info_fn_ptrs, 0, sizeof (app_info_fn_ptrs));
    app_info_fn_ptrs.build_blk_app_info_fn_ptr = pc_open_block;
    app_info_fn_ptrs.remove_blk_app_info_fn_ptr = pc_close_block;
    app_info_fn_ptrs.build_dev_app_info_fn_ptr = NULL;
    app_info_fn_ptrs.remove_dev_app_info_fn_ptr = NULL;
    cm_init_app_info_fn_ptrs (&app_info_fn_ptrs);

    /*
    * Initialize dictionary table from the standard.dct file
    */

    LOGC_DEBUG(CPM_FF_DDSERVICE,"Initializing Standard Dictionary Table...");

    // CPMHACK: Loading only dc8 dictionary as it contains UTF8 encoded strings
//    (void)sprintf(dict_file_path, "%s/%s", release_path, dict_file);
    (void)sprintf(dict_file_path1, "%s/%s", release_path, dict_file1);
//    (void) makedict(dict_file_path, (void (*)())dict_table_install, 0);
    (void) makedict(dict_file_path1, (void (*)())dict_table_install, 0);

    LOGC_TRACE(CPM_FF_DDSERVICE,"Dictionary File Path: %s.", release_path);

    /*
     * Intialize all memory objects for nm
     */
    memset(&nm_obj,0x0,sizeof(CM_NM_OBJ));

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    API to un-initialize the FF Back-end application
*/
int ff_uninitialize(RET_INFO *ret_info, unsigned char hm)
{
    ENV_INFO            env_info;
    APP_INFO            app_info;
    int                 rs;
    char                dict_file_path[FILE_PATH_LEN];

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0,sizeof(ENV_INFO));
    env_info.app_info = &app_info;

    rs = cr_cm_exit(&env_info, hm);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initialize the communication manager %s\n",
               dds_error_string(rs) );
        ret_info->reason_code = rs;
        strcpy(ret_info->reason_str, dds_error_string(rs));
    }

    free(network_tbl.list);
    removeDict(dict_table_uninstall);
    return rs;
}


/*  ----------------------------------------------------------------------------
    remove dict table entry
*/
void dict_table_uninstall()
{
	int entries;

    for(entries = 0 ; entries < num_dict_table_entries ; entries++ )
    {
    	free(dict_table[entries].str);
    }

}

/*  ----------------------------------------------------------------------------
    Removes the device from the active list table
*/
int delete_device_table( ENV_INFO *env_info, NETWORK_HANDLE nh)
{
    int rs;
    rs = cr_delete_device_table(env_info, nh);
    if (rs) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Error in Building the device table, rs = %d\n", rs);
        return rs;
    }
    id_table.num_entries = 0;
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Dis Connects the device specified with address and deletes all its memeory
*/
int dds_dis_connect_device(ENV_INFO * envInfo, NETWORK_HANDLE nh)
{
    int r_code;
    r_code = delete_device_table( envInfo, nh);
    if (r_code != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in deleting Device table\n");
        return r_code;
    }
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Connects the device specified with address for connection in the network.
*/
int ff_disconnect(RET_INFO *ret_info, unsigned short devAddr)
{
    int rs = FAILURE;
    APP_INFO        app_info;
    ENV_INFO        env_info;
    NETWORK_HANDLE  nh = 0;
    NT_COUNT        nt_count = 0;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    app_info.status_info = USE_EDIT_VALUE;

    rs = get_nt_net_count(&nt_count);
    if (rs != SUCCESS) {
        goto disconnection_err;
    }

    rs = get_nt_net_handle_by_addr(nt_count, devAddr, &nh);
    if (rs != SUCCESS) {
        goto disconnection_err;
    }

    rs = dds_dis_connect_device(&env_info, nh);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in Connect Device\n");
        goto disconnection_err;
    }

    network_tbl.count = 0;

    return SUCCESS;

disconnection_err:
    LOGC_ERROR(CPM_FF_DDSERVICE,"Failed in Connect Device %d %s\n", rs,
            dds_error_string(rs));

    ret_info->reason_code = rs;
    strcpy(ret_info->reason_str, dds_error_string(rs));

    return rs;
}

/*  ----------------------------------------------------------------------------
    API to get the host FBK board information
*/
int ff_get_host_info(RET_INFO *ret_info, HOST_INFO *host_info, HOST_CONFIG *hc)
{
    int                 rs;
    APP_INFO            app_info;
    ENV_INFO            env_info;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));
    (void)memset((char *) host_info, 0, sizeof(HOST_INFO));

    env_info.app_info = &app_info;

    /*
    * Initialize Connection Manager and set appropriate routines
    */

    /*
     * Initialization of the communication manager and start of the
     * FF stack and initiating the FBK board by calling FBK_init()
     */
    rs = cr_cm_get_host_info(&env_info, host_info, hc);

    if (rs) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initialize the communication manager %s\n",
               dds_error_string(rs) );
        ret_info->reason_code = rs;
        strcpy(ret_info->reason_str, dds_error_string(rs));

        return rs;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Clears the memory allocated for unit relation
*/
static void clean_up_unit_info(void)
{
    int i;

    if(unit_rel.count != 0)
    {
        for(i=0; i<unit_rel.count; i++)
        {
            if((unit_rel.unit_dep[i].list) != NULL)
                free(unit_rel.unit_dep[i].list);
        }
        if(unit_rel.unit_dep != NULL)
            free(unit_rel.unit_dep);
        if(unit_rel.unit_main != NULL)
            free(unit_rel.unit_main);
        unit_rel.unit_dep = NULL;
        unit_rel.unit_main = NULL;
    }
    return;
}

/*  ----------------------------------------------------------------------------
    API to establish connection to the block
*/
int ff_connect_block(RET_INFO *ret_info, const char *block_tag)
{
    int rs, i;
    APP_INFO app_info;
    ENV_INFO env_info;
    int nh = 0;//get_network_handle();

    memset(&app_info, 0, sizeof(APP_INFO));
    memset(&env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    app_info.status_info = USE_EDIT_VALUE;

    init_method_tbl();

    rs = dds_open_block(&env_info, nh, block_tag);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Problem performing Open Block command");
        ret_info->reason_code = rs;
        strcpy((char *)ret_info->reason_str, dds_error_string(rs));
        goto err_ret;
    }

    rs = dds_get_unit_info(&env_info, block_tag);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Problem in getting Unit information");
        ret_info->reason_code = rs;
        strcpy((char *)ret_info->reason_str, dds_error_string(rs));
        goto err_ret;
    }

    set_meth_block_tag(block_tag);

    rs = dds_get_method_info(&env_info, block_tag);
    if(rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Problem in getting Method information of the block");
        ret_info->reason_code = rs;
        strcpy((char *)ret_info->reason_str, dds_error_string(rs));
        goto err_ret;
    }

err_ret:
    return rs;
}

/*  ----------------------------------------------------------------------------
    API to disconnect connection to the block
*/
int ff_disconnect_block(RET_INFO *ret_info, const char *block_tag)
{
    int rs;
    APP_INFO app_info;
    ENV_INFO env_info;
    int nh = 0;//get_network_handle();

    memset(&app_info, 0, sizeof(APP_INFO));
    memset(&env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    app_info.status_info = USE_EDIT_VALUE;

    rs = dds_close_block(&env_info, nh, block_tag);
    if(rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Problem performing Close Block command");
        ret_info->reason_code = rs;
        strcpy((char *)ret_info->reason_str, dds_error_string(rs));
    }

    clean_up_unit_info();
    return rs;
}

/*  ----------------------------------------------------------------------------
    Get the Actual mode value of the block from the remote device
*/
static int get_mode_block_val(ENV_INFO *env_info,
        FLAT_BLOCK *flat_block, char *mode_str)
{
    DDI_GENERIC_ITEM	  generic_item = {0};
    unsigned long         mask = VAR_LABEL | VAR_ENUMS;
    EVAL_VAR_VALUE        eval_value;
    ENUM_VALUE_LIST       *enums = NULL;
    ENUM_VALUE            *temp_enum;
    DDI_ITEM_SPECIFIER    item_spec;
    DDI_BLOCK_SPECIFIER	  block_spec;
    FLAT_VAR              *flat_var = NULL;
    MEMBER_LIST           *mem_list = &flat_block->param;
    MEMBER                *temp_member = mem_list->list;
    int                   r_code, i;
    APP_INFO              *app_info=(APP_INFO *)env_info->app_info;

    // Assign the zero errors before calling read function
    app_info->env_returns.response_code = 0;
    app_info->env_returns.comm_err = 0;

    temp_member = temp_member + MODE_BLOCK_INDEX;
    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;
    item_spec.item.id = temp_member->ref.id;
    item_spec.type = DDI_ITEM_ID_SI;
    item_spec.subindex = ACTUAL_MODE_SI;

    /* Check if the Item Type is of Record type. Mode block
       is RECORD type with 4 members in it.
    */
    if(RECORD_ITYPE != temp_member->ref.type)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"get_mode_block_id: Wrong Item ID 0x%lx",item_spec.item.id);
        return FFDS_INVALID_BLOCK_MODE;
    }

    // Read the Mode block value from the transmitter
    if (NT_A == NT_NET_TYPE(0))
    {
        r_code = pc_get_parm_val_with_id_sub(env_info,
                temp_member->ref.id, ACTUAL_MODE_SI, &eval_value);
        if(r_code != SUCCESS)
        {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to get the Unit value from transmitter\n");
            return r_code;
        }
    }

    (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

    // Get the Enum string and value from the DD file
    r_code = ddi_get_item(&block_spec, &item_spec,
            env_info, mask, &generic_item);
    if (r_code != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"get_mode_block_id: ITEM_ID = 0x%lx rs = %d, %s",
                item_spec.item.id, r_code, dds_error_string(r_code));
    }

    flat_var = (FLAT_VAR *) generic_item.item;
    enums = &flat_var->enums;

    /* Compare the read value from the transmitter with the enum list read
       from the DD file and return the string whose enum value is same as the
       mode block value read from the transmitter
    */
    if (NT_A == NT_NET_TYPE(0))
    {
        temp_enum = enums->list;
        for (i = 0; i < enums->count; temp_enum++, i++) {
            if(eval_value.val.u == temp_enum->val) {
                strcpy((char *)mode_str, temp_enum->desc.str);
            }
        }
    }
    else if (NT_OFF_LINE == NT_NET_TYPE(0))
    {
        temp_enum = enums->list;
        strcpy((char *)mode_str, temp_enum->desc.str);
    }

    /*
     * Clear the memory allocated by the ddi_get_item()
     */
    r_code = ddi_clean_item(&generic_item);
    if (r_code != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"get_mode_block_val: Error cleaning item `%ld`\n",
                item_spec.item.id);
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Get the Actual mode item and value of the block from the remote device
*/
static int get_block_mode_item(ENV_INFO *env_info,
        char *block_tag, char *mode_str)
{
    int							rs;
    BLOCK_HANDLE				block_handle;
    unsigned long				mask = BLOCK_PARAM;
    DDI_ITEM_SPECIFIER      	item_spec;
    DDI_GENERIC_ITEM			generic_item;
    DDI_BLOCK_SPECIFIER			block_spec;
    FLAT_BLOCK 					*flat_block = NULL;

    /* Get the Block handle from searching the block tag in the
       network_table.
    */
    block_handle = ct_block_search(block_tag);

    (void)memset((char *)&item_spec, 0, sizeof(DDI_ITEM_SPECIFIER));

    env_info->block_handle = block_handle;
    block_spec.block.handle = env_info->block_handle;
    block_spec.type = DDI_BLOCK_HANDLE;
    item_spec.type = DDI_ITEM_BLOCK;

    (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

    // Get the pointer to the list of the parameter in the block
    rs = ddi_get_item(&block_spec, &item_spec, env_info, mask,
                      &generic_item);

    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"get_block_mode_item: rs = %d, %s\n",
                rs, dds_error_string(rs));
        return rs;
    }
    flat_block = (FLAT_BLOCK *)generic_item.item;

    // Function call to get the actual mode string value of the block.
    rs = get_mode_block_val(env_info, flat_block, mode_str);
    if (rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in getting the Item ID of mode_block");
        return rs;
    }

    // Clear the memory allocated by the ddi_get_item()
    rs = ddi_clean_item(&generic_item);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"get_mode_block_val: Error cleaning item `%ld`\n",
                item_spec.item.id);
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    API to read the actual mode of the block
*/
int ff_get_mode_block(RET_INFO* ret_info, char *block_tag, char *mode_str)
{
    int rs;
    APP_INFO        app_info;
    ENV_INFO        env_info;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    app_info.status_info = USE_EDIT_VALUE;

    rs = dds_open_block(&env_info, 0,block_tag);
    if (rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in opening Block %s: %s",
                block_tag, dds_error_string(rs));
        goto error;
    }

    rs = get_block_mode_item(&env_info, block_tag, mode_str);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in getting Block Mode item: %s",
                dds_error_string(rs));
        goto error;
    }

    rs = dds_close_block(&env_info, 0, block_tag);
    if(rs != SUCCESS)
    {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Failure in closing block %s: %s",
                block_tag, dds_error_string(rs));
        goto error;
    }

    ret_info->reason_code = 0;
    return 0;

error:
    ret_info->reason_code = rs;
    strcpy(ret_info->reason_str, dds_error_string(rs));
    return rs;
}

/*  ----------------------------------------------------------------------------
    API to configure the FF stack
*/
int ff_configure(RET_INFO *ret_info, HOST_CONFIG *host_config)
{
    int                 rs;
    APP_INFO            app_info;
    ENV_INFO            env_info;

    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    memset(&nma_info, 0, sizeof(CM_NMA_INFO));

    switch(host_config->host_type)
    {
        case HOST_STANDALONE:
        case HOST_SERVER:
            rs = cm_config_stack(&env_info, &nma_info, host_config);
            if (rs) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initialize the communication manager"
                           " %s\n", dds_error_string(rs) );
                ret_info->reason_code = rs;
                strcpy(ret_info->reason_str, dds_error_string(rs));
                return rs;
            }
            break;

        case HOST_CLIENT:
            rs = cm_get_stack_config(&env_info, &nma_info);
            if (rs) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"Failed to initialize the communication manager"
                           " %s\n", dds_error_string(rs));
                ret_info->reason_code = rs;
                strcpy(ret_info->reason_str, dds_error_string(rs));

                return rs;
            }
            break;

        default:
            LOGC_ERROR(CPM_FF_DDSERVICE,"BAD HOST_TYPE");
            break;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Compare the reference numbers in the DICT_TABLE_ENTRY struct passed.
    This routine is used by the bsearch library function called in
    app_func_get_dict_string upcall
*/
static int dict_compare(void const *ptr_a, void const *ptr_b)
{
    DICT_TABLE_ENTRY *dict_a, *dict_b;

    dict_a = (DICT_TABLE_ENTRY *) ptr_a;
    dict_b = (DICT_TABLE_ENTRY *) ptr_b;

    return ((int) ((long) dict_a->ref - (long) dict_b->ref));
}

/*  ----------------------------------------------------------------------------
    Put new entry in dict_table
*/
void dict_table_install(ulong ref, char *name, char *str)
{
    dict_table[num_dict_table_entries].ref = ref;
    dict_table[num_dict_table_entries].len = (unsigned short) strlen(str);
    dict_table[num_dict_table_entries].str = str;

    xfree((void **) &name);
    num_dict_table_entries++;
}

/*  ----------------------------------------------------------------------------
    Upcall to retrieve a standard text dictionary string
*/
int app_func_get_dict_string(ENV_INFO *env_info,
                             DDL_UINT ddl_index, STRING *str)
{

    DICT_TABLE_ENTRY *found_ptr;
    DICT_TABLE_ENTRY key;

    key.ref = ddl_index;

    /* Perform a binary search on the standard dictionary table to find the
       entry we're looking for
    */
    found_ptr = (DICT_TABLE_ENTRY *) bsearch((char *) &key,
        (char *) dict_table, (unsigned) num_dict_table_entries,
        sizeof(DICT_TABLE_ENTRY), (CMP_FN_PTR)dict_compare);

    if (found_ptr == NULL) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"app_func_get_dict_string: Dictionary string, index %ld,"
                   " not found\n", ddl_index);
        return DDL_DICT_STRING_NOT_FOUND;
    }
    else
    {
	    str->flags = DONT_FREE_STRING | DICT_STRING;
		str->len = found_ptr->len;
		str->str = found_ptr->str;
		return DDL_SUCCESS;
    }
}

/*  ----------------------------------------------------------------------------
    Initialize the method table
*/
int init_method_tbl(void)
{
    meth_tbl_lst.count = 0;
    meth_tbl_lst.meth_tbl = (methodTable *) malloc(sizeof(methodTable));

    if (!meth_tbl_lst.meth_tbl) {
        return(CM_NO_MEMORY);
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Increments the method count
*/
int inc_meth_count(void)
{
    meth_tbl_lst.count++;
    meth_tbl_lst.meth_tbl = (methodTable *) realloc(
            (methodTable *)meth_tbl_lst.meth_tbl,
            (unsigned int)(sizeof(methodTable) * (meth_tbl_lst.count)));

    if (!meth_tbl_lst.meth_tbl) {
        return(CM_NO_MEMORY);
    }

    (void *)memset((char *)&meth_tbl_lst.meth_tbl[meth_tbl_lst.count - 1] , 0,
        (sizeof(methodTable)));

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sets the method block tag
*/
void set_meth_block_tag(const char *block_tag)
{
    (void)strcpy((char *)meth_tbl_lst.block_tag, block_tag);
}

/*  ----------------------------------------------------------------------------
    Gets the method table index
*/
struct methodTable *get_meth_tbl_index(void)
{
    return &meth_tbl_lst.meth_tbl[meth_tbl_lst.count - 1];
}

/*  ----------------------------------------------------------------------------
    Upcall to retrieve a device specific string
*/
int app_func_get_dev_spec_string(ENV_INFO *env_info,
                                 DEV_STRING_INFO *dev_str_info, STRING *str)
{

    int             rs;
    STRING_TBL		*string_tbl_handle;
    ENV_INFO2       *env2 = (ENV_INFO2 *)env_info;

    // Find the pointer to the device specific table corresponding to block handle
    if (env2->type == ENV_DEVICE_HANDLE)
    {
        rs = get_string_tbl_handle_by_device(env2->device_handle,
                                             &string_tbl_handle);
    }
    else
    {
        rs = get_string_tbl_handle(env_info->block_handle, &string_tbl_handle);
    }

    if (rs != SUCCESS) {
        printf("app_func_get_dev_spec_string: Device specific table not found\n");
        return DDL_DEV_SPEC_STRING_NOT_FOUND;
    }

    if (dev_str_info->id >= (unsigned long) string_tbl_handle->count) {

        // If string not found
        LOGC_ERROR(CPM_FF_DDSERVICE,"app_func_get_dev_spec_string: Device specific string of id %ld"
                   " not found\n", dev_str_info->id);
        return DDL_DEV_SPEC_STRING_NOT_FOUND;
    }

    else
    {
        str->flags = DONT_FREE_STRING | DD_STRING;
		str->str = string_tbl_handle->list[dev_str_info->id].str;
		str->len = (unsigned short) strlen(str->str);
		return DDL_SUCCESS;
    }
}

/*  ----------------------------------------------------------------------------
    Gets string table handle by device
*/
int get_string_tbl_handle_by_device(DEVICE_HANDLE device_handle,
                                    STRING_TBL **string_tbl_handle)
{
    DEVICE_TYPE_HANDLE	 device_type_handle;
    FLAT_DEVICE_DIR		*flat_device_dir = 0;

    device_type_handle = active_dev_tbl.list[device_handle]->active_dev_type_tbl_offset;
    flat_device_dir = active_dev_type_tbl.list[device_type_handle]->dd_dev_tbls;

    if (flat_device_dir == 0)
    {
        return(FAILURE);
    }

    *string_tbl_handle = &flat_device_dir->string_tbl;

    return(SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Gets string table handle
*/
int get_string_tbl_handle(BLOCK_HANDLE block_handle,
                          STRING_TBL **string_tbl_handle)
{
    DEVICE_HANDLE		 device_handle = -5;
    DEVICE_TYPE_HANDLE	 device_type_handle;
    FLAT_DEVICE_DIR		*flat_device_dir = 0;

    device_handle = active_blk_tbl.list[block_handle]->active_dev_tbl_offset;
    device_type_handle = active_dev_tbl.list[device_handle]->active_dev_type_tbl_offset;
    flat_device_dir = active_dev_type_tbl.list[device_type_handle]->dd_dev_tbls;

    if (flat_device_dir == 0)
    {
        return(FAILURE);
    }

    *string_tbl_handle = &flat_device_dir->string_tbl;

    return(SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Parse the input file line and get arguments for the open block command
*/
int dds_open_block(ENV_INFO *env_info, NETWORK_HANDLE nh, char *block_tag)
{
    int							i, rs;
    STRING						*strptr = NULL;
    BLOCK_HANDLE				block_handle = -1;
    DEVICE_HANDLE				dhandle = -1;
    DEVICE_TYPE_HANDLE			dthandle = -1;
    DD_HANDLE					dd_handle;
    FLAT_DEVICE_DIR *			flat_device_dir = NULL;
    DDI_DEVICE_DIR_REQUEST		ddi_dir_req;
    ITEM_ID						block_id ;
    DD_DEVICE_ID				dd_device_id ;

    // Open the block/
    rs = ct_block_open(block_tag, &block_handle);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: ct_block_open failed\n");
        return(rs);
    }

    // Load operational information for the the block
    rs = bl_block_load(block_handle, env_info);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: bl_block_load failed\n");
        return(rs);
    }

    // Check to make sure there is a valid DDID for the block.
    rs = get_abt_dd_blk_id(block_handle, &block_id) ;
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: get_abt_dd_blk_id failed\n");
        return(rs);
    }
    if (!block_id) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: No DDID for block - ");
        LOGC_ERROR(CPM_FF_DDSERVICE, "most likely due to missing symbol file\n");
        return(FAILURE);
    }

    // Load the DD information for the block
    rs = ds_dd_block_load(block_handle, &dd_handle);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: ds_dd_block_load failed\n");
        LOGC_ERROR(CPM_FF_DDSERVICE,"DD Block load failed: Please check for the DD file"
                   " in the DD Library\n");
        return(rs);
    }

    // Check to see if more that one device or device type is loaded
    rs = get_abt_adtt_offset(block_handle, &dthandle);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: get_abt_adtt_offset failed\n");
        return(rs);
    }
    if ((global_dthandle != -1) && (global_dthandle != dthandle)) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: Multiple device types are NOT allowed\n");
        return(FAILURE);
    }
    rs = get_abt_adt_offset(block_handle, &dhandle) ;
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: get_abt_adt_offset failed\n");
        return(rs);
    }
    if ((global_dhandle != -1) && (global_dhandle != dhandle)) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: Multiple devices are NOT allowed\n");
        return(FAILURE);
    }

    // Save global information
    global_dhandle = dhandle;
    global_dthandle = dthandle;

    // Fill in the global id_table, if first time with device type
    rs = get_adtt_dd_dev_tbls(global_dthandle, (void **)&flat_device_dir);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: get_adtt_dd_dev_tbls failed, rs = `%d`\n", rs);
        return(FAILURE);
    }

    if (id_table.num_entries == 0) {
        rs = get_adtt_dd_device_id(global_dthandle, &dd_device_id) ;
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: get_adtt_dd_device_id failed, rs"
                        " = `%d`\n", rs);
            return(FAILURE);
        }

        rs = fill_id_table(&(flat_device_dir->item_tbl), &dd_device_id,
                           &id_table);
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: Filling id_table failed, rs = `%d`\n", rs);
            return(FAILURE);
        }
    }

    /* Attach the device specific string table to the dds_tables field in
     * the device type table.  Check first if the device specific string
     * table is already there
     */
    if (!(flat_device_dir->attr_avail & STRING_TBL_MASK)) {
        (void)memset((char *)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
        ddi_dir_req.type = DD_DT_HANDLE;
        ddi_dir_req.mask = STRING_TBL_MASK;
        ddi_dir_req.spec.device_type_handle = dthandle;
        rs = ddi_device_dir_request(&ddi_dir_req, flat_device_dir);
        if (rs != DDS_SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "open_block: getting device specific string table failed\n");
            return(rs);
        }
        else
        {
            if (strings_flag) {
                LOGC_INFO(CPM_FF_DDSERVICE, "\n------STRING TABLE------\n");
                strptr = flat_device_dir->string_tbl.list;
                for (i = 0; i < flat_device_dir->string_tbl.count; i++) {
                    LOGC_INFO(CPM_FF_DDSERVICE, "string_tbl[%d].str = %s\n", i, strptr[i].str);
                }
            }
        }
    }

    return (SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Parse the input file line and get arguments for the close command
*/
int dds_close_block(ENV_INFO *env_info, NETWORK_HANDLE nh, char *block_tag)
{
    int             			rs;
    BLOCK_HANDLE				block_handle = -1;

    // Find block handle for the tag
    block_handle = ct_block_search(block_tag);
    if (block_handle < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"close_block: finding block handle failed, block tag"
                   " `%s`\n", block_tag);
        return(block_handle);
    }

    // Remove the block and device info if necessary from conman tables
    rs = ct_block_close(block_handle);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,
            "close_block: removing block failed, block tag `%s`\n", block_tag);
        return(rs);
    }

    return (SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Retrieve all ITEM_ID_LIST items
*/
static int get_id_item(void *flat_item, ITEM_ID *id_ptr,
                       unsigned short list_count, char *block_name,
                       char *header_name, ENV_INFO *env_info,
                       unsigned long mask)
{
    int							p, i, rs = SUCCESS;
    ITEM_ID						item_id;
    ITEM_TYPE					item_type;
    char 						*item_name;
    DDI_ITEM_SPECIFIER      	item_spec = {0};

    DDI_BLOCK_SPECIFIER			block_spec = {0};
    DDI_GENERIC_ITEM			generic_item = {0};

    EVAL_VAR_VALUE	eval_value;
    UNIT_RELATION	*unit;
    REFRESH_RELATION	*refr;
    OP_REF_TRAIL_LIST   *wao;

    (void)memset((char *)&item_spec, 0, sizeof(DDI_ITEM_SPECIFIER));
    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    for (p = 0; p < list_count; p++, id_ptr++)
    {
        flat_item = NULL;
        rs = item_id_to_type(*id_ptr, &item_type);
        item_id = *id_ptr;
        item_spec.type = DDI_ITEM_ID;
        item_spec.item.id = item_id;

        (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

        rs = ddi_get_item(&block_spec, &item_spec, env_info, mask,
                          &generic_item);
        flat_item = generic_item.item;

        if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,
            "dump_block: rs = %d, %s\n", rs, dds_error_string(rs));
        }
        rs = item_id_to_name(item_id, &item_name);

        switch (item_type) {

            case REFRESH_ITYPE:
                refr = &(((FLAT_REFRESH *) flat_item)->items);

                ref_rel.depend_items = (VAR_DETAILS_LIST *)realloc(
                            ref_rel.depend_items,sizeof(VAR_DETAILS_LIST) * (p+1));
                ref_rel.update_items = (VAR_DETAILS_LIST *)realloc(
                            ref_rel.update_items,sizeof(VAR_DETAILS_LIST) * (p+1));
                ref_rel.depend_items[p].list =
                    (VAR_DETAILS *)calloc(refr->depend_items.count,sizeof(VAR_DETAILS));
                ref_rel.update_items[p].list =
                    (VAR_DETAILS *)calloc(refr->update_items.count,sizeof(VAR_DETAILS));
                ref_rel.depend_items[p].count = refr->depend_items.count;
                ref_rel.update_items[p].count = refr->update_items.count;
                for(i=0; i < ref_rel.depend_items[p].count; i++){
                    ref_rel.depend_items[p].list[i].id = refr->depend_items.list[i].op_id;
                    ref_rel.depend_items[p].list[i].subindex
                            = refr->depend_items.list[i].op_subindex;
                }
                for(i=0; i < ref_rel.update_items[p].count; i++){
                    ref_rel.update_items[p].list[i].op_type
                            = refr->update_items.list[i].op_type;
                    ref_rel.update_items[p].list[i].id
                            = refr->update_items.list[i].op_id;
                    ref_rel.update_items[p].list[i].subindex
                            = refr->update_items.list[i].op_subindex;
                }
                break;

            case UNIT_ITYPE:
                unit = &(((FLAT_UNIT *) flat_item)->items);

                unit_rel.unit_main = (VAR_DETAILS *)realloc(
                            unit_rel.unit_main, sizeof(VAR_DETAILS) * (p+1));
                unit_rel.unit_dep = (VAR_DETAILS_LIST *)realloc(
                            unit_rel.unit_dep,sizeof(VAR_DETAILS_LIST) * (p+1));
                unit_rel.unit_dep[p].list =
                    (VAR_DETAILS *)calloc(unit->var_units.count,sizeof(VAR_DETAILS));
                unit_rel.count = list_count;
                unit_rel.unit_main[p].id = unit->var.op_id;
                unit_rel.unit_main[p].subindex = unit->var.op_subindex;
                unit_rel.unit_dep[p].count = unit->var_units.count;
                for(i=0; i < unit->var_units.count; i++){
                    unit_rel.unit_dep[p].list[i].id = unit->var_units.list[i].op_id;
                    unit_rel.unit_dep[p].list[i].subindex
                            = unit->var_units.list[i].op_subindex;
                }
                break;

            case WAO_ITYPE:
                wao = &(((FLAT_WAO *) flat_item)->items);
                wao_rel.wao_list = (VAR_DETAILS_LIST *)realloc(
                            wao_rel.wao_list, sizeof(VAR_DETAILS_LIST)*(p+1));
                wao_rel.wao_list[p].list = (VAR_DETAILS *)calloc(
                            wao->count,sizeof(VAR_DETAILS));
                for(i=0; i < wao->count; i++){
                    wao_rel.wao_list[p].list[i].id = wao->list[i].op_id;
                    wao_rel.wao_list[p].list[i].subindex = wao->list[i].op_subindex;
                    rs = pc_get_parm_val_with_id_sub(env_info, wao->list[i].op_id,
                                        wao->list[i].op_subindex, &eval_value);
                }
                break;

            default:
                break;
        }

        rs = ddi_clean_item(&generic_item);
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE,
                "dump_block: Error cleaning item `%ld`\n", item_id);
        }
    }
    return rs;
}

/*  ----------------------------------------------------------------------------
    Test the pc_get_param_value() function using an item id and subindex
*/
int pc_get_parm_val_with_id_sub(ENV_INFO *env_info, ITEM_ID ddItemID,
                                unsigned int si, EVAL_VAR_VALUE *eval_var_value)
{
    int             		rs;
    P_REF					pc_p_ref;

    (void)memset((char *)&pc_p_ref, 0, sizeof(P_REF));

    pc_p_ref.type = PC_ITEM_ID_REF;

    // Parse the line for its arguments
    if (env_info->block_handle < 0) {
        rs = FAILURE;
    }

    pc_p_ref.id = (ITEM_ID) ddItemID;
    LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Id `0x%lx` \n", pc_p_ref.id);

    pc_p_ref.subindex = (SUBINDEX) si;
    LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Subindex `%d`\n", pc_p_ref.subindex);
    if (pc_p_ref.subindex < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"*** Error Insertion `Bad Subindex`\n");
    }

    // Perform the requested service
    rs = pc_get_param_value(env_info, &pc_p_ref, eval_var_value);

    if (rs != PC_SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,	"pc_get_parm_val_with_id_sub: No Get, rs = %d\n", rs);
        LOGC_ERROR(CPM_FF_DDSERVICE,"pc_get_parm_val_with_id_sub: %s\n", dds_error_string(rs));
        return rs;

    }

    return (SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Get the method ID by name
*/
static int method_id_item(ITEM_ID_LIST *method_list, ENV_INFO *env_info,
                          unsigned long mask)
{
    int							p, i, rs = SUCCESS;
    ITEM_ID						item_id;
    ITEM_TYPE					item_type;
    char 						*item_name;
    FLAT_BLOCK 					*flat_block = NULL;
    void 						*flat_item = NULL;
    DDI_ITEM_SPECIFIER      	item_spec = {0};
    DDI_BLOCK_SPECIFIER			block_spec = {0};
    DDI_GENERIC_ITEM			generic_item = {0};
    ITEM_ID						*meth_id_list;
    methodTable             	*meth_tbl = NULL;
    FLAT_METHOD 				*flat_method = NULL;

    meth_id_list = (ITEM_ID *) method_list->list;
    for(i = 0; i < method_list->count; i++)
    {
        (void)memset((char *)&block_spec, 0, sizeof(DDI_BLOCK_SPECIFIER));
        (void)memset((char *)&item_spec, 0, sizeof(DDI_ITEM_SPECIFIER));

        block_spec.type = DDI_BLOCK_HANDLE;
        block_spec.block.handle = env_info->block_handle;
        item_spec.type = DDI_ITEM_ID;
        item_spec.item.id = (ITEM_ID)(*meth_id_list);

        (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));
        rs = ddi_get_item(&block_spec,&item_spec, env_info, mask, &generic_item);
        flat_item = generic_item.item;
        flat_block = (FLAT_BLOCK *)generic_item.item;
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE,"Error retrieving item `%ld`\n", item_spec.item.id);
            LOGC_ERROR(CPM_FF_DDSERVICE,"rs = %d, %s\n", rs, dds_error_string(rs));
            return rs;
        }
        (void)item_id_to_name(item_spec.item.id, &item_name);

        flat_method = (FLAT_METHOD *)flat_item;

        inc_meth_count();
        meth_tbl = get_meth_tbl_index();
        meth_tbl->meth_id = item_spec.item.id;

        /* Copy the Display name of method */
        if (flat_method->label.str == NULL)
        {
            strcpy(meth_tbl->meth_disp_name, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");
        }
        else if (flat_method->label.str <= 0)
        {
            strcpy(meth_tbl->meth_disp_name, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");
        }
        else if (flat_method->label.str == NULL)
        {
            strcpy(meth_tbl->meth_disp_name, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");
        }
        else
        {
            strcpy(meth_tbl->meth_disp_name, flat_method->label.str);
        }

        /* Copy the Help string of method */
        if (flat_method->help.str == NULL) {
            strcpy(meth_tbl->meth_help_str, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");

        }
        else if (flat_method->help.str <= 0)
        {
            strcpy(meth_tbl->meth_help_str, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");
        }
        else if (flat_method->help.str == NULL)
        {
            strcpy(meth_tbl->meth_help_str, "No string available");
            LOGC_DEBUG(CPM_FF_DDSERVICE, "No string available\n");
        }
        else
        {
            strcpy(meth_tbl->meth_help_str, flat_method->help.str);
        }

        rs = ddi_clean_item(&generic_item);
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, " rs = %d, %s\n", rs, dds_error_string(rs));
        }

        meth_id_list++;
    }
    return rs;
}

/*  ----------------------------------------------------------------------------
    Gets the method information
*/
int dds_get_method_info( ENV_INFO *env_info, char *block_tag)
{
    int							rs, i;
    BLOCK_HANDLE				block_handle;
    unsigned long				mask = 0xffffffff;
    char						*item_name;
    FLAT_BLOCK 					*flat_block = NULL;
    void 						*flat_item = NULL;
    DDI_ITEM_SPECIFIER      	item_spec = {0};
    DDI_GENERIC_ITEM			generic_item = {0};
    DDI_BLOCK_SPECIFIER			block_spec = {0};

    (void)memset((char *)&item_spec, 0, sizeof(DDI_ITEM_SPECIFIER));

    block_handle = ct_block_search(block_tag);

    env_info->block_handle = block_handle;

    if (env_info->block_handle < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Finding block handle failed, block tag `%s`\n", block_tag);
        return(block_handle);
    }

    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

    rs = ddi_get_item(&block_spec,&item_spec, env_info, mask, &generic_item);

    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, " rs = %d, %s\n", rs, dds_error_string(rs));
    }

    flat_block = (FLAT_BLOCK *)generic_item.item;
    flat_item = generic_item.item;

    rs = method_id_item(&flat_block->method, env_info, mask);
    if (rs != SUCCESS) {
        return (rs);
    }

    rs = ddi_clean_item(&generic_item);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, " rs = %d, %s\n", rs, dds_error_string(rs));
    }

    return (rs);
}

/*  ----------------------------------------------------------------------------
    Gets the unit information
*/
int dds_get_unit_info(ENV_INFO *env_info, char *block_tag)
{
    int							rs,i;
    BLOCK_HANDLE				block_handle;
    unsigned long				mask = 0xffffffff;
    FLAT_BLOCK 					*flat_block = NULL;
    void 						*flat_item = NULL;
    DDI_ITEM_SPECIFIER      	item_spec = {0};
    DDI_GENERIC_ITEM			generic_item = {0};
    DDI_BLOCK_SPECIFIER			block_spec = {0};


    (void)memset((char *)&item_spec, 0, sizeof(DDI_ITEM_SPECIFIER));

    unit_rel.unit_main = (VAR_DETAILS *)malloc(1);
    unit_rel.unit_dep = (VAR_DETAILS_LIST *)malloc(1);

    block_handle = ct_block_search(block_tag);

    env_info->block_handle = block_handle;

    if (env_info->block_handle < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"Finding block handle failed, block tag `%s`\n", block_tag);
        return(block_handle);
    }

    block_spec.type = DDI_BLOCK_HANDLE;
    block_spec.block.handle = env_info->block_handle;

    (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

    rs = ddi_get_item(&block_spec, &item_spec, env_info, mask, &generic_item);

    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, " rs = %d, %s\n", rs, dds_error_string(rs));
    }

    flat_block = (FLAT_BLOCK *)generic_item.item;
    flat_item = generic_item.item;

    unit_rel.count = flat_block->unit.count;

    if (flat_block->unit.count == 0)
    {
        LOGC_INFO(CPM_FF_DDSERVICE, "No Unit Relationship in the block = %s\n", block_tag);
    }

    rs = get_id_item(flat_item, flat_block->unit.list,
                    flat_block->unit.count, block_tag, "Units", env_info, mask);
    if (rs != SUCCESS) {
        return (rs);
    }

    rs = ddi_clean_item(&generic_item);
    if (rs != SUCCESS) {
        LOGC_ERROR(CPM_FF_DDSERVICE, "dump_block: rs = %d, %s\n", rs, dds_error_string(rs));
    }

    return (rs);
}

/*  ----------------------------------------------------------------------------
    Executes the method
*/
int dds_exec_method(ENV_INFO env_info, OP_REF op_ref,
                    DDI_ITEM_SPECIFIER item_spec, DDI_BLOCK_SPECIFIER block_spec)
{
    int             			i;
    int							rs;
    char *                      curr_item_name = NULL;
    P_REF						pc_p_ref;
    FLAT_VAR *					flat_var;
    ITEM_ID *					act_list;
    void **                     flat_item = NULL;
    int							force_bad_item = 0;
    DDI_GENERIC_ITEM			generic_item;

    /*
     * Execute the specified method.
     */
    if (op_ref.type == METHOD_ITYPE) {
        rs = mi_exec_user_meth(env_info, item_spec.item.id);
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_user_method failed,"
                        " rs = %d\n", rs);
        }
    }
    else
    {
        (void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));

        rs = check_to_clean(flat_item, 1, 0, VARIABLE_ITYPE,
                            DDI_COMMAND, DDI_COMMAND);
        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: Clean of item failed,"
                        " rs = %d\n", rs);
            return(FAILURE);
        }

        generic_item.item = *flat_item;
        generic_item.item_type = op_ref.type;
        rs = ddi_get_item(&block_spec, &item_spec, &env_info,
                  VAR_PRE_EDIT_ACT | VAR_POST_EDIT_ACT | VAR_PRE_READ_ACT |
                  VAR_POST_READ_ACT | VAR_PRE_WRITE_ACT | VAR_POST_WRITE_ACT,
                  &generic_item);

        if (rs != SUCCESS) {
            LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: ddi_get_item failed, rs = %d\n", rs);
            LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: %s\n", dds_error_string(rs));
        }

        *flat_item = generic_item.item;
        flat_var = (FLAT_VAR *) *flat_item;

        if (force_bad_item) {
            pc_p_ref.id			= 0x00000000;
            pc_p_ref.subindex	= 989;
            pc_p_ref.type		= PC_ITEM_ID_REF;
        }
        else {
            pc_p_ref.id			= op_ref.id;
            pc_p_ref.subindex	= op_ref.subindex;
            pc_p_ref.type		= PC_ITEM_ID_REF;
        }

        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** PRE_EDIT_ACTIONS\n");
        act_list = flat_var->actions->pre_edit_act.list;
        for (i = 0; i < flat_var->actions->pre_edit_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR( "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** POST_EDIT_ACTIONS\n");
        act_list = flat_var->actions->post_edit_act.list;
        for (i = 0; i < flat_var->actions->post_edit_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** PRE_READ_ACTIONS\n");
        act_list = flat_var->actions->pre_read_act.list;
        for (i = 0; i < flat_var->actions->pre_read_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** POST_READ_ACTIONS\n");
        act_list = flat_var->actions->post_read_act.list;
        for (i = 0; i < flat_var->actions->post_read_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** PRE_WRITE_ACTIONS\n");
        act_list = flat_var->actions->pre_write_act.list;
        for (i = 0; i < flat_var->actions->pre_write_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
        LOGC_DEBUG(CPM_FF_DDSERVICE, "*** POST_WRITE_ACTIONS\n");
        act_list = flat_var->actions->post_write_act.list;
        for (i = 0; i < flat_var->actions->post_write_act.count; i++) {
            rs = item_id_to_name(*act_list, &curr_item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE, "*** Method Name `%s`\n", curr_item_name);
            rs = mi_exec_edit_meth(env_info, act_list[i], &pc_p_ref);
            if (rs != SUCCESS) {
                LOGC_ERROR(CPM_FF_DDSERVICE, "exec_method: execute_edit_method failed,"
                            " rs = %d\n", rs);
            }
        }
    }

    return (SUCCESS);
}

/*  ----------------------------------------------------------------------------
    Connect the type A Comm Stack
*/
void connect_comm_stack_a()
{
    CSTK_A_FN_PTRS  cstk_a_fn_ptrs;

    cstk_a_fn_ptrs.initialize_fn_ptr = initialize;
    cstk_a_fn_ptrs.config_stack_fn_ptr = configure_stack;
    cstk_a_fn_ptrs.get_stack_config_fn_ptr = get_stack_configuration;
    cstk_a_fn_ptrs.init_network_fn_ptr = init_network;
    cstk_a_fn_ptrs.find_block_fn_ptr = find_block;
    cstk_a_fn_ptrs.initiate_comm_fn_ptr = initiate_comm;
    cstk_a_fn_ptrs.terminate_comm_fn_ptr = terminate_comm;
    cstk_a_fn_ptrs.comm_get_fn_ptr = comm_get;
    cstk_a_fn_ptrs.comm_put_fn_ptr = comm_put;
    cstk_a_fn_ptrs.comm_read_fn_ptr = comm_read;
    cstk_a_fn_ptrs.comm_write_fn_ptr = comm_write;
    cstk_a_fn_ptrs.comm_upload_fn_ptr = comm_upload;
    cstk_a_fn_ptrs.param_read_fn_ptr = param_read;
    cstk_a_fn_ptrs.param_write_fn_ptr = param_write;
    cstk_a_fn_ptrs.param_load_fn_ptr = param_load;
    cstk_a_fn_ptrs.param_unload_fn_ptr = param_unload;
    cstk_a_fn_ptrs.scan_network_fn_ptr = scan_network;
    cstk_a_fn_ptrs.build_dev_tbl_fn_ptr = build_dev_tbl;
    cstk_a_fn_ptrs.delete_dev_tbl_fn_ptr = delete_dev_tbl;
    cstk_a_fn_ptrs.sm_identify_fn_ptr = sm_identify;
    cstk_a_fn_ptrs.offline_init_fn_ptr= offline_init;
    cstk_a_fn_ptrs.sm_clr_node_fn_ptr = sm_clr_node;
    cstk_a_fn_ptrs.sm_set_node_fn_ptr = sm_set_node;
    cstk_a_fn_ptrs.sm_clr_tag_fn_ptr = sm_clr_tag;
    cstk_a_fn_ptrs.sm_assign_tag_fn_ptr = sm_assign_tag;
    cstk_a_fn_ptrs.nm_wr_local_vcr_fn_ptr = nm_wr_local_vcr;
    cstk_a_fn_ptrs.nm_fms_initiate_fn_ptr = nm_fms_initiate;
    cstk_a_fn_ptrs.nm_get_od_fn_ptr = nm_get_od;
    cstk_a_fn_ptrs.nm_read_fn_ptr = nm_read;
    cstk_a_fn_ptrs.nm_dev_boot_fn_ptr = nm_dev_boot;
    cstk_a_fn_ptrs.cm_exit_fn_ptr = cm_exit;
    cstk_a_fn_ptrs.cm_firmware_download_fn_ptr = cm_firmware_download;
    cstk_a_fn_ptrs.cm_get_host_info_fn_ptr = cm_get_host_info;
    cstk_a_fn_ptrs.cm_reset_fbk_fn_ptr = cm_reset_fbk;
    cstk_a_fn_ptrs.cm_set_appl_state_fn_ptr = cm_set_appl_state;
    cstk_a_fn_ptrs.cm_get_actual_index_fn_ptr = cm_get_actual_index;
    cstk_a_fn_ptrs.cm_dds_bypass_read_fn_ptr = cm_dds_bypass_read;
    cstk_a_fn_ptrs.cm_abort_cr_fn_ptr = cm_abort_cr_fn;

    cr_init_cstk_a_fn_ptrs(&cstk_a_fn_ptrs);
}

/*  ----------------------------------------------------------------------------
    Disconnect the type A Comm Stack
*/
void disconnect_comm_stack_a()
{
    CSTK_A_FN_PTRS  cstk_a_fn_ptrs;

    cstk_a_fn_ptrs.initialize_fn_ptr = 0;
    cstk_a_fn_ptrs.config_stack_fn_ptr = 0;
    cstk_a_fn_ptrs.get_stack_config_fn_ptr = 0;
    cstk_a_fn_ptrs.init_network_fn_ptr = 0;
    cstk_a_fn_ptrs.find_block_fn_ptr = 0;
    cstk_a_fn_ptrs.initiate_comm_fn_ptr = 0;
    cstk_a_fn_ptrs.terminate_comm_fn_ptr = 0;
    cstk_a_fn_ptrs.comm_get_fn_ptr = 0;
    cstk_a_fn_ptrs.comm_put_fn_ptr = 0;
    cstk_a_fn_ptrs.comm_read_fn_ptr = 0;
    cstk_a_fn_ptrs.comm_write_fn_ptr = 0;
    cstk_a_fn_ptrs.comm_upload_fn_ptr = 0;
    cstk_a_fn_ptrs.param_read_fn_ptr = 0;
    cstk_a_fn_ptrs.param_write_fn_ptr = 0;
    cstk_a_fn_ptrs.param_load_fn_ptr = 0;
    cstk_a_fn_ptrs.param_unload_fn_ptr = 0;
    cstk_a_fn_ptrs.scan_network_fn_ptr = 0;
    cstk_a_fn_ptrs.sm_identify_fn_ptr = 0;
    cstk_a_fn_ptrs.offline_init_fn_ptr = 0;
    cstk_a_fn_ptrs.sm_clr_node_fn_ptr = 0;
    cstk_a_fn_ptrs.sm_set_node_fn_ptr = 0;
    cstk_a_fn_ptrs.sm_clr_tag_fn_ptr = 0;
    cstk_a_fn_ptrs.sm_assign_tag_fn_ptr = 0;
    cstk_a_fn_ptrs.nm_wr_local_vcr_fn_ptr = 0;
    cstk_a_fn_ptrs.nm_fms_initiate_fn_ptr = 0;
    cstk_a_fn_ptrs.nm_get_od_fn_ptr = 0;
    cstk_a_fn_ptrs.nm_read_fn_ptr = 0;
    cstk_a_fn_ptrs.nm_dev_boot_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_exit_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_firmware_download_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_get_host_info_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_reset_fbk_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_set_appl_state_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_get_actual_index_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_dds_bypass_read_fn_ptr = 0;
    cstk_a_fn_ptrs.cm_abort_cr_fn_ptr = 0;

    cr_init_cstk_a_fn_ptrs(&cstk_a_fn_ptrs);
}

/* ----------------------------------------------------------------------------
 * Below Time format conversion functions are implemented as per the
 * FF documentation FF-870 (Page 95) can be referred for more information.
 */

/*  ----------------------------------------------------------------------------
    Converts user readable time format to epoch time format
*/
static time_t get_epochtime(int *date_array)
{
    time_t		rawtime;
    struct tm*	timeinfo;
    time(&rawtime);
    timeinfo = gmtime ( &rawtime );

    timeinfo->tm_year = date_array[YEAR] - 1900;
    timeinfo->tm_mon = date_array[MONTH] - 1;
    timeinfo->tm_mday = date_array[DATE];
    timeinfo->tm_hour = date_array[HOUR];
    timeinfo->tm_min = date_array[MINUTE];
    timeinfo->tm_sec = date_array[SECOND];

    return timegm (timeinfo);
}

/*  ----------------------------------------------------------------------------
    Converts epoch time user readable time format
*/
static void get_date_array(time_t epochtime,int *date_array)
{
    struct tm*	timeinfo;

    timeinfo = gmtime(&epochtime);

    date_array[YEAR] = timeinfo->tm_year + 1900;
    date_array[MONTH] = timeinfo->tm_mon + 1;
    date_array[DATE] = timeinfo->tm_mday;
    date_array[HOUR] = timeinfo->tm_hour;
    date_array[MINUTE] = timeinfo->tm_min;
    date_array[SECOND] = timeinfo->tm_sec;
}

/*  ----------------------------------------------------------------------------
    Converts DDS date and time to user date and time format
*/
void conv_dds_datentime_user_datentime(char char_array[],
                                       int *date_array)
{
    long ms;

    if (date_array != NULL)
    {
        date_array[DATE] = ((unsigned char)char_array[4] & 0x1F);  // 1..31 day
        date_array[MONTH] = (unsigned char)char_array[5];			// 1..12 month
        date_array[YEAR] = (unsigned char)char_array[6];			// 1..255 years
        date_array[YEAR] += 1900;
        date_array[HOUR] = ((unsigned char)char_array[3] & ~0x80);	// 0..23 hr
        date_array[MINUTE] = ((unsigned char)char_array[2] & 0x3F);	// 0..59 min

        ms = ((unsigned char)char_array[0]) * 256;
        ms += (unsigned char)char_array[1];

        date_array[SECOND] = ms/NUM_OF_MSEC_IN_SEC; 		        // 0..59999 ms
        date_array[MILLISECOND] = ms%NUM_OF_MSEC_IN_SEC;
    }
}

/*  ----------------------------------------------------------------------------
    Converts user date & time format to DDS date & time format
*/
void conv_user_datentime_to_dds_datentime(int *date_array, char char_array[])
{
    long ms;

    if(date_array != NULL)
    {
        ms = date_array[SECOND] * NUM_OF_MSEC_IN_SEC;

        char_array[0] = (char) (ms / 256);					// 0..59999 ms
        char_array[1] = (char) (ms % 256);
        char_array[2] = (char) (date_array[MINUTE] & 0x3F);		// 0..59 min
        char_array[3] = (char) (date_array[HOUR] & 0x1F);		// 0..23 hr
        char_array[4] = (char) (date_array[DATE] & 0x1F);		// 1..31 day
        char_array[5] = (char) (date_array[MONTH] & 0x3F);		// 1..12 month
        date_array[YEAR] -= 1900;
        char_array[6] = (char) date_array[YEAR];				// 1..255 years
    }
}

/*  ----------------------------------------------------------------------------
    Converts user date format to dds date format
*/
void conv_user_date_to_dds_date(int *date_array, char char_array[])
{
    if(date_array != NULL)
    {
        char_array[0] = (char)(date_array[DATE] & 0xFF); // 1..31 day
        char_array[1] = (char)(date_array[MONTH] & 0xFF); // 1..12 month
        date_array[YEAR] -= 1900;
        char_array[2] = (char)(date_array[YEAR] & 0xFF); // 1..255 years
    }
}

/*  ----------------------------------------------------------------------------
    Converts dds date format to user date format
*/
void conv_dds_date_to_user_date(char char_array[], int *date_array)
{
    if((date_array != NULL) || ((date_array +1) != NULL) || ((date_array + 2) != NULL))
    {
        date_array[DATE] = (unsigned char) char_array[0]; // 1..31 day
        date_array[MONTH] = (unsigned char) char_array[1]; // 1..12 month
        date_array[YEAR] = (unsigned char) char_array[2]; // 1..255 years
        date_array[YEAR] += 1900;
    }
}

/*  ----------------------------------------------------------------------------
    Converts user duration format to dds duration format
*/
void conv_user_duration_to_dds_duration(unsigned long long mSec, char char_array[])
{
    unsigned int days = 0; 
    unsigned int temp = 0;

    /*
    *  Convert milli seconds into date
    *  divide available milli second by number of millisecond per day
    */ 
    days = mSec/(NUM_OF_SEC_IN_DAY * NUM_OF_MSEC_IN_SEC);

    char_array[5] = (char) (days & 0xFF); // LSB
    char_array[4] = (char) ((days >> 8) & 0xFF); // MSB

    /*
    *  Remaining milli seconds after date conversion shall be added to 
    *  milli second partof duration
    */ 
    temp = mSec%(NUM_OF_SEC_IN_DAY * NUM_OF_MSEC_IN_SEC);

    char_array[3] = (char) (temp & 0xFF);	    //(LSB)		// 0..268435455 secs
    char_array[2] = (char) ((temp >> 8) & 0xFF);
    char_array[1] = (char) ((temp >> 16) & 0xFF);
    char_array[0] = (char) ((temp >> 24) & 0xFF);
}

/*  ----------------------------------------------------------------------------
    Converts dds duration format to user duration format
*/
void conv_dds_duration_to_user_duration(char char_array[],
                                        unsigned long long *mSec)
{
    unsigned int days = 0;
    unsigned long temp = 0;
    
    if(mSec != NULL)
    {
        temp = (unsigned char)char_array[0];
        *mSec = (temp << 24);
        temp = (unsigned char)char_array[1];
        *mSec |= (temp << 16);
        temp = (unsigned char)char_array[2];
        *mSec |= (temp << 8);
        temp = (unsigned char)char_array[3];
        *mSec |= temp;
    }

    /* 
    * Convert date part into millisecond and add into the output
    */
    temp = (unsigned char)char_array[4]; // MSB
    days = (temp << 8);
    temp = (unsigned char)char_array[5]; // LSB
    days |= temp;

    *mSec += (days * NUM_OF_SEC_IN_DAY * NUM_OF_MSEC_IN_SEC);
}

/*  ----------------------------------------------------------------------------
    Converts user time variable to dds time format
*/
void conv_user_time_dds_time(int *date_array, char char_array[])
{
    long	days=0,mSec=0;
    time_t  epochtime=0;

    epochtime = get_epochtime(date_array);

    /*
    * Convert days to secs
    */
    days = epochtime/NUM_OF_SEC_IN_DAY;

    /*
    * As epoch starts from 01.01.1970 and we are only interested
    * from 01.01.1984 we have to remove 5113 (days from 1970 to 1984)
    */
    days -= 5113;

    /*
    * Convert remaining secs to milliseconds
    */
    mSec = ((epochtime)%(NUM_OF_SEC_IN_DAY))*NUM_OF_MSEC_IN_SEC;

    /*
    * Add the milli second part which received from user
    */ 
    mSec += date_array[MILLISECOND];

    ASSERT_RET(days >= 0, days);

    days = (days & 0xFFFF);
    char_array[5] = (char) (days & 0xFF);		//(LSB)		// 0..65535 days
    char_array[4] = (char) ((days >> 8) & 0xFF);	//(MSB)

    mSec = (mSec & 0xFFFFFFF);

    char_array[3] = (char) (mSec & 0xFF);			//(LSB)		// 0..268435455 secs
    char_array[2] = (char) ((mSec >> 8) & 0xFF);
    char_array[1] = (char) ((mSec >> 16) & 0xFF);
    char_array[0] = (char) ((mSec >> 24) & 0xFF);
}

/*  ----------------------------------------------------------------------------
    Converts dds time variable to user time format
*/
void conv_dds_time_user_time(char char_array[], int *date_array)
{
    long	days=0,mSec=0;
    time_t  epochtime = 0;
    int		temp=0;

    temp = (unsigned char)char_array[4];
    days = (temp << 8);
    temp = (unsigned char)char_array[5];
    days |= temp;

    /*
     * Add the removed days to form proper epoch time
     */

    days += 5113;

    /*
     * Convert Days to second (num of hours/day * min/hour * sec/min)
     */
    epochtime = days * NUM_OF_SEC_IN_DAY;

    temp = (unsigned char)char_array[0];
    mSec = (temp << 24);
    temp = (unsigned char)char_array[1];
    mSec |= (temp << 16);
    temp = (unsigned char)char_array[2];
    mSec |= (temp << 8);
    temp = (unsigned char)char_array[3];
    mSec |= temp;

    /*
     * Convert millisecond to second and add it to millisecond part of date array
     */
    mSec /= NUM_OF_MSEC_IN_SEC;
    date_array[MILLISECOND] = mSec%NUM_OF_MSEC_IN_SEC;

    /*
     * Add the remmaing seconds to epoch time.
     */
    epochtime +=  mSec;

    get_date_array(epochtime,date_array);
}

/*  ----------------------------------------------------------------------------
    Converts dds time value to user time value format
*/
void conv_dds_timeval_to_user_timeval(char char_array[], int* date_array)
{
    unsigned long mSec = 0;
    unsigned long  uSec = 0;
    unsigned long temp = 0;
    time_t  epochtime = 0;
    
    temp = (unsigned char)char_array[0];
    mSec |= (temp << 56);
    temp = (unsigned char)char_array[1];
    mSec |= (temp << 48);
    temp = (unsigned char)char_array[2];
    mSec |= (temp << 40);
    temp = (unsigned char)char_array[3];
    mSec |= (temp << 32);
    temp = (unsigned char)char_array[4];
    mSec |= (temp << 24);
    temp = (unsigned char)char_array[5];
    mSec |= (temp << 16);
    temp = (unsigned char)char_array[6];
    mSec |= (temp << 8);
    temp = (unsigned char)char_array[7];
    mSec |= temp;

    mSec = mSec/32;
    uSec = mSec%32;

    epochtime = (mSec/NUM_OF_MSEC_IN_SEC);
    /*  
     *  Since the epoch is 1 Jan 1970 and we're interested in time since
     *  1 Jan 1972, we'll have to cut off a few seconds (730 days' worth).
     */
    epochtime += (730 * NUM_OF_SEC_IN_DAY);

    /*
     * Store the milli second in millisecond part of date_array
     */
    date_array[MILLISECOND] = mSec%NUM_OF_MSEC_IN_SEC;

    /*
     * Store the micro second in microsecond part of date_array
     */
    date_array[MICROSECOND] = uSec;

    get_date_array(epochtime,date_array);
}


/*  ----------------------------------------------------------------------------
    Converts user time value to dds time value format
*/
void conv_user_timeval_to_dds_timeval(unsigned long epochtime, int *date_array, char *char_array)
{
    unsigned long long mSec = 0;
    unsigned long balance_days = 0;

    
    mSec = epochtime;
        
    /*
    * Add milli second part of TIME_VALUE received from user
    */
    mSec += date_array[MILLISECOND];

    
    /*
     * Add micro second part of TIME_VALUE received from user by converting it to millisecond
    */
    mSec += (date_array[MICROSECOND] * NUM_OF_MSEC_IN_SEC);
    
    /*  
     *  Since the epoch is 1 Jan 1970 and we're interested in time since
     *  1 Jan 1972, we'll have to cut off a few seconds (730 days' worth).
     */
    balance_days = (unsigned long) (730 * NUM_OF_SEC_IN_DAY);

    if (mSec > balance_days)
    {
        mSec -= balance_days;
        mSec *= 32000;
    }
    else
    {
        mSec = 0;
    }

    char_array[7] = (char) (mSec & 0xff);
    char_array[6] = (char) ((mSec >> 8) & 0xff);
    char_array[5] = (char) ((mSec >> 16) & 0xff);
    char_array[4] = (char) ((mSec >> 24) & 0xff);
    char_array[3] = (char) ((mSec >> 32) & 0xff);
    char_array[2] = (char) ((mSec >> 40) & 0xff);
    char_array[1] = (char) ((mSec >> 48) & 0xff);
    char_array[0] = (char) ((mSec >> 56) & 0xff);
}
