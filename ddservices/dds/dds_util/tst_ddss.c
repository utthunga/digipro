
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */
#include    <stdarg.h>
#include    <stdlib.h>
#include    <stdio.h>
#include 	<sys/stat.h>
#include 	<errno.h>
#include 	<unistd.h>
#include    <assert.h>
#include    <ctype.h>
#include    <string.h>

#include 	"std.h"
#include	"cm_lib.h"
#include	"ddi_lib.h"
#include	"ddi_tab.h"
#include	"dds_tab.h"
#include	"tst_cmn.h"
#include	"tst_dds.h"
#include 	"tst_fail.h"
#include	"ff_if.h"
#include    "flk_log4cplus_clogger.h"
#include    "flk_log4cplus_defs.h"

extern	char     		release_path[FILE_PATH_LEN];
extern	ID_TABLE_HEADER  	id_table;

extern int 		symfile_flag;
extern char     symfilename[FILE_PATH_LEN];
extern FILE		*symfileptr;
extern int		idtable_flag;
extern void		xfree(void **);
extern void 	*xmalloc(size_t);
extern SP_PTR_TABLE	sp_ptr_table;

/*
 * Table of Command Strings and Corresponding Functions
 */
#ifdef DDS_TEST_MODE

CMD	cmds[MAXCMD] = {
	{"ddi_get_item_with_parm_offset_subindex",	ddi_get_item_parm_offset_sub},
	{"ddi_get_item_with_op_index_subindex",		ddi_get_item_op_index_sub},
	{"ddi_get_item_with_ddid_subindex",			ddi_get_item_ddid_sub},
	{"ddi_get_item_with_name_subindex",			ddi_get_item_name_sub},
	{"ddi_get_item_with_parm_offset",			ddi_get_item_parm_offset},
	{"ddi_get_item_with_parmlist_offset",		ddi_get_item_parmlist_offset},
	{"ddi_get_item_with_char_offset",			ddi_get_item_char_offset},
	{"ddi_get_item_with_op_index",				ddi_get_item_op_index},
	{"ddi_get_item_with_ddid",					ddi_get_item_ddid},
	{"ddi_get_item_with_name",					ddi_get_item_name},
	{"open_block",								open_block},
	{"close_block",								close_block},
	{"ddi_get_block",							ddi_get_block},
	{"ddi_conv_op_index_parm_offset_subindex",	ddi_conv_opix_parm_offset_sub},
	{"ddi_conv_op_index_ddid_subindex",			ddi_conv_opix_ddid_sub},
	{"ddi_conv_op_index_name_subindex",			ddi_conv_opix_name_sub},
	{"ddi_conv_op_index_parm_offset",			ddi_conv_opix_parm_offset},
	{"ddi_conv_op_index_ddid",					ddi_conv_opix_ddid},
	{"ddi_conv_op_index_name",					ddi_conv_opix_name},
	{"ddi_conv_unit_name_subindex",				ddi_conv_unit_name_sub},
	{"ddi_conv_unit_parm_offset_subindex",		ddi_conv_unit_parm_offset_sub},
	{"ddi_conv_unit_ddid_subindex",				ddi_conv_unit_ddid_sub},
	{"ddi_conv_unit_op_idx_subindex",			ddi_conv_unit_op_idx_sub},
	{"ddi_conv_unit_char_subindex",				ddi_conv_unit_char_sub},
	{"ddi_conv_unit_name",						ddi_conv_unit_name},
	{"ddi_conv_unit_parm_offset",				ddi_conv_unit_parm_offset},
	{"ddi_conv_unit_ddid",						ddi_conv_unit_ddid},
	{"ddi_conv_unit_op_idx",					ddi_conv_unit_op_idx},
	{"ddi_conv_wao_name_subindex",				ddi_conv_wao_name_sub},
	{"ddi_conv_wao_parm_offset_subindex",		ddi_conv_wao_parm_offset_sub},
	{"ddi_conv_wao_ddid_subindex",				ddi_conv_wao_ddid_sub},
	{"ddi_conv_wao_op_idx_subindex",			ddi_conv_wao_op_idx_sub},
	{"ddi_conv_wao_char_subindex",				ddi_conv_wao_char_sub},
	{"ddi_conv_wao_name",						ddi_conv_wao_name},
	{"ddi_conv_wao_parm_offset",				ddi_conv_wao_parm_offset},
	{"ddi_conv_wao_ddid",						ddi_conv_wao_ddid},
	{"ddi_conv_wao_op_idx",						ddi_conv_wao_op_idx},
	{"ddi_conv_update_name_subindex",			ddi_conv_update_name_sub},
	{"ddi_conv_update_parm_offset_subindex",	ddi_conv_update_parm_off_sub},
	{"ddi_conv_update_ddid_subindex",			ddi_conv_update_ddid_sub},
	{"ddi_conv_update_op_idx_subindex",			ddi_conv_update_op_idx_sub},
	{"ddi_conv_update_char_subindex",			ddi_conv_update_char_sub},
	{"ddi_conv_update_name",					ddi_conv_update_name},
	{"ddi_conv_update_parm_offset",				ddi_conv_update_parm_offset},
	{"ddi_conv_update_ddid",					ddi_conv_update_ddid},
	{"ddi_conv_update_op_idx",					ddi_conv_update_op_idx},
	{"ddi_conv_type_parm_offset_subindex",		ddi_conv_type_parm_offset_sub},
	{"ddi_conv_type_ddid_subindex",				ddi_conv_type_ddid_sub},
	{"ddi_conv_type_name_subindex",				ddi_conv_type_name_sub},
	{"ddi_conv_type_op_idx_subindex",			ddi_conv_type_op_idx_sub},
	{"ddi_conv_type_name",						ddi_conv_type_name},
	{"ddi_conv_type_parm_offset",				ddi_conv_type_parm_offset},
	{"ddi_conv_type_ddid",						ddi_conv_type_ddid},
	{"ddi_conv_type_op_idx",					ddi_conv_type_op_idx},
	{"ddi_conv_var_type",	                    ddi_conv_vtype},
	{"ddi_conv_item_id_name",					ddi_conv_item_id_name},
	{"ddi_conv_item_id_list_name",				ddi_conv_item_id_list_name},
	{"ddi_conv_item_id_list_id",				ddi_conv_item_id_list_id},
	{"ddi_conv_item_id_char",					ddi_conv_item_id_char},
	{"ddi_conv_subindex",						ddi_conv_subindex},
	{"ddi_conv_check_enum_var_value",			ddi_conv_check_enum_var_value},
	{"ddi_conv_param_info",                     ddi_conv_param_info},
	{"ddi_block_dir_bhandle",					ddi_block_dir_bhandle},
	{"ddi_block_dir_ddhandle_ddref",			ddi_block_dir_ddhandle_ddref},
	{"ddi_block_dir_dthandle_bname",			ddi_block_dir_dthandle_bname},
	{"ddi_device_dir_dhandle",					ddi_device_dir_dhandle},
	{"ddi_device_dir_ddhandle_ddref",			ddi_device_dir_ddhandle_ddref},
	{"ddi_device_dir_dthandle",					ddi_device_dir_dthandle},
	{"ddi_block_dir_all",						ddi_block_dir_all},
	{"ddi_device_dir_all",						ddi_device_dir_all},
	{"eval_item",								item_eval},
	{"eval_dir",								dir_eval},
	{"fetch_item",								item_fetch},
	{"fetch_dir",	   		 					dir_fetch},
	{"rod_fetch_item",							item_rod_fetch},
	{"rod_fetch_dir",							dir_rod_fetch},
	{"change_var_value",						var_value_change},
	{"ddi_translate",							ddi_translate},
	{"fail",									fail_command},
	{"pc_open_blk",								pc_open_blk},
	{"pc_close_blk",							pc_close_blk},
	{"pc_get_parm_val_with_id_sub",				pc_get_parm_val_with_id_sub},
	{"pc_get_parm_val_with_po_sub",				pc_get_parm_val_with_po_sub},
	{"pc_put_parm_val_with_id_sub",				pc_put_parm_val_with_id_sub},
	{"pc_put_parm_val_with_po_sub",				pc_put_parm_val_with_po_sub},
	{"pc_read_parm_val_with_id_sub",			pc_read_parm_val_with_id_sub},
	{"pc_read_parm_val_with_po_sub",			pc_read_parm_val_with_po_sub},
	{"pc_write_parm_val_with_id_sub",			pc_write_parm_val_with_id_sub},
	{"pc_write_parm_val_with_po_sub",			pc_write_parm_val_with_po_sub},
	{"pc_write_all_parm_vals",					pc_write_all_parm_vals},
	{"pc_discard_put_parm_val_id_sub",			pc_discard_put_parm_val_id_sub},
	{"pc_discard_put_parm_val_po_sub",			pc_discard_put_parm_val_po_sub},
	{"pc_discard_all_put_parms",				pc_discard_all_put_parms},
	{"pc_parm_val_mod_with_id_sub",				pc_parm_val_mod_with_id_sub},
	{"pc_parm_val_mod_with_po_sub",				pc_parm_val_mod_with_po_sub},
	{"pc_any_parm_val_mod",						pc_any_parm_val_mod},
	{"pc_clear_parm_flag_with_id_sub",			pc_clear_parm_flag_with_id_sub},
	{"pc_clear_parm_flag_with_po_sub",			pc_clear_parm_flag_with_po_sub},
	{"pc_set_parm_flag_with_id_sub",			pc_set_parm_flag_with_id_sub},
	{"pc_set_parm_flag_with_po_sub",			pc_set_parm_flag_with_po_sub},
	{"pc_test_parm_flag_with_id_sub",			pc_test_parm_flag_with_id_sub},
	{"pc_test_parm_flag_with_po_sub",			pc_test_parm_flag_with_po_sub},
	{"pc_get_parm_val_with_bad_type",			pc_get_parm_val_with_bad_type},
	{"cr_param_read_with_id_sub",				cr_param_read_with_id_sub},
	{"cr_param_read_with_po_sub",				cr_param_read_with_po_sub},
	{"cr_param_write_with_id_sub",				cr_param_write_with_id_sub},
	{"cr_param_write_with_po_sub",				cr_param_write_with_po_sub},
	{"cr_param_load_with_id_sub",				cr_param_load_with_id_sub},
	{"cr_param_load_with_po_sub",				cr_param_load_with_po_sub},
	{"cr_param_unload_with_id_sub",				cr_param_unload_with_id_sub},
	{"cr_param_unload_with_po_sub",				cr_param_unload_with_po_sub},
	{"cr_identify_with_short_addr",				cr_identify_with_short_addr},
	{"cr_read_tag_from_device",					cr_read_tag_from_device},
	{"cr_send_cmd_to_device",					cr_send_cmd_to_device},
	{"rcsim_set_general",						rcsim_set_general},
	{"rcsim_set_network",						rcsim_set_network},
	{"rcsim_set_device",						rcsim_set_device},
	{"rcsim_initialize",						tstrc_initialize},
	{"rcsim_free",								tstrc_free},
	{"rcsim_dump",								rcsim_dump},
    {"dump_block",                              dump_block},
    {"pc_read_all_params",                      pc_read_all_params}
    , {"exec_method",                             exec_method}
};

#endif

/*
 * Table of function entries for the fail command.
 * NOTE: The order this array is in has to reflect the order of
 *       the defines in tst_fail.h for function names.
 *       The defines in tst_fail.h are indices into this table.
 */
static FAIL_FUNC	fail_func_table[MAXFAILFUNC] = {
	{"ddl_parse_integer_func", 0, 0, 0, FALSE},
	{"ddl_parse_tag_func", 0, 0, 0, FALSE},   
	{"ddl_parse_float", 0, 0, 0, FALSE},     
	{"ddl_ulong_choice", 0, 0, 0, FALSE},   
	{"eval_attr_ulong", 0, 0, 0, FALSE},           
	{"ddl_parse_bitstring", 0, 0, 0, FALSE},   
	{"ddl_bitstring_choice", 0, 0, 0, FALSE},   
	{"eval_attr_bitstring", 0, 0, 0, FALSE},        
	{"eval_attr_definition", 0, 0, 0, FALSE},      
	{"ddl_shrink_depinfo", 0, 0, 0, FALSE},   
	{"append_depinfo", 0, 0, 0, FALSE},             
	{"ddl_add_chunk_list", 0, 0, 0, FALSE},        
	{"ddl_cond", 0, 0, 0, FALSE},                 
	{"ddl_cond_list", 0, 0, 0, FALSE},           
	{"eval_dir_param_list_tbl", 0, 0, 0, FALSE},    
	{"eval_dir_string_tbl", 0, 0, 0, FALSE},       
	{"eval_dir_domain_tbl", 0, 0, 0, FALSE},       
	{"eval_dir_item_tbl", 0, 0, 0, FALSE},       
	{"eval_dir_dict_ref_tbl", 0, 0, 0, FALSE},   
	{"eval_dir_prog_tbl", 0, 0, 0, FALSE},      
	{"eval_dir_blk_tbl", 0, 0, 0, FALSE},     
	{"eval_dir_rel_tbl", 0, 0, 0, FALSE},    
	{"eval_dir_param_tbl", 0, 0, 0, FALSE},              
	{"eval_dir_blk_item_tbl", 0, 0, 0, FALSE},          
	{"eval_dir_param_mem_name_tbl", 0, 0, 0, FALSE},    
	{"eval_dir_char_mem_name_tbl", 0, 0, 0, FALSE},    
	{"eval_dir_param_list_mem_name_tbl", 0, 0, 0, FALSE},    
	{"eval_dir_blk_item_name_tbl", 0, 0, 0, FALSE},      
	{"eval_dir_char_mem_tbl", 0, 0, 0, FALSE},          
	{"eval_dir_param_list_mem_tbl", 0, 0, 0, FALSE},   
	{"eval_dir_param_mem_tbl", 0, 0, 0, FALSE},          
	{"mask_man", 0, 0, 0, FALSE},                   
	{"eval_dir_block_tables", 0, 0, 0, FALSE},           
	{"eval_dir_device_tables", 0, 0, 0, FALSE},         
	{"ddl_shrink_output_class", 0, 0, 0, FALSE},    
	{"ddl_shrink_enum_list", 0, 0, 0, FALSE},       
	{"ddl_parse_enums", 0, 0, 0, FALSE},          
	{"ddl_enums_choice", 0, 0, 0, FALSE},        
	{"ddl_pop", 0, 0, 0, FALSE},                
	{"ddl_convert_type", 0, 0, 0, FALSE},      
	{"find_var_min_max", 0, 0, 0, FALSE},     
	{"ddl_expr_nonzero", 0, 0, 0, FALSE},    
	{"ddl_apply_op", 0, 0, 0, FALSE},       
	{"ddl_do_eval_expr", 0, 0, 0, FALSE},            
	{"ddl_eval_expr", 0, 0, 0, FALSE},              
	{"ddl_expr_choice", 0, 0, 0, FALSE},           
	{"ddl_promote_to", 0, 0, 0, FALSE},            
	{"ddl_exprs_equal", 0, 0, 0, FALSE},         
	{"eval_attr_expr", 0, 0, 0, FALSE},              
	{"eval_attr_int_expr", 0, 0, 0, FALSE},         
	{"set_var_value_upcall", 0, 0, 0, FALSE},    
	{"ddl_shrink_itemarray_list", 0, 0, 0, FALSE},    
	{"ddl_parse_itemarray", 0, 0, 0, FALSE},      
	{"ddl_item_arraychoice", 0, 0, 0, FALSE},    
	{"eval_attr_itemarray", 0, 0, 0, FALSE},         
	{"mask_man", 0, 0, 0, FALSE},               
	{"eval_item_var", 0, 0, 0, FALSE},            
	{"eval_item_program", 0, 0, 0, FALSE},       
	{"eval_item_menu", 0, 0, 0, FALSE},         
	{"eval_item_edit_display", 0, 0, 0, FALSE},    
	{"eval_item_method", 0, 0, 0, FALSE},          
	{"eval_item_refresh", 0, 0, 0, FALSE},        
	{"eval_item_unit", 0, 0, 0, FALSE},           
	{"eval_item_wao", 0, 0, 0, FALSE},           
	{"eval_item_itemarray", 0, 0, 0, FALSE},   
	{"eval_item_array", 0, 0, 0, FALSE},              
	{"eval_item_collection", 0, 0, 0, FALSE},        
	{"eval_item_record", 0, 0, 0, FALSE},           
	{"eval_item_var_list", 0, 0, 0, FALSE},        
	{"eval_item_block", 0, 0, 0, FALSE},          
	{"eval_item_response_code", 0, 0, 0, FALSE},    
	{"eval_item_domain", 0, 0, 0, FALSE},          
	{"eval_item_command", 0, 0, 0, FALSE},        
	{"eval_item", 0, 0, 0, FALSE},        
	{"ddl_shrink_members_list", 0, 0, 0, FALSE},    
	{"ddl_parse_members", 0, 0, 0, FALSE},        
	{"ddl_members_choice", 0, 0, 0, FALSE},        
	{"eval_attr_members", 0, 0, 0, FALSE},               
	{"ddl_shrink_menuitems_list", 0, 0, 0, FALSE},   
	{"ddl_parse_menuitems", 0, 0, 0, FALSE},    
	{"ddl_menuitems_choice", 0, 0, 0, FALSE},    
	{"eval_attr_menuitems", 0, 0, 0, FALSE},      
	{"ddl_parse_ref", 0, 0, 0, FALSE},        
	{"ddl_ref_choice", 0, 0, 0, FALSE},        
	{"eval_attr_ref", 0, 0, 0, FALSE},               
	{"ddl_shrink_reflist", 0, 0, 0, FALSE},      
	{"ddl_parse_reflist", 0, 0, 0, FALSE},        
	{"ddl_reflist_choice", 0, 0, 0, FALSE},        
	{"ddl_op_ref_trail_choice", 0, 0, 0, FALSE},    
	{"eval_reflist", 0, 0, 0, FALSE},                
	{"ddl_shrink_op_ref_trail_list", 0, 0, 0, FALSE},   
	{"ddl_op_ref_trail_list_choice", 0, 0, 0, FALSE},   
	{"eval_op_ref_trail_list", 0, 0, 0, FALSE},    
	{"ddl_shrink_refresh", 0, 0, 0, FALSE},     
	{"ddl_refresh_choice", 0, 0, 0, FALSE},      
	{"eval_attr_refresh", 0, 0, 0, FALSE},        
	{"ddl_shrink_unit", 0, 0, 0, FALSE},      
	{"ddl_unit_choice", 0, 0, 0, FALSE},       
	{"eval_attr_unit", 0, 0, 0, FALSE},              
	{"ddl_shrink_resp_codes", 0, 0, 0, FALSE},   
	{"ddl_parse_resp_codes", 0, 0, 0, FALSE},     
	{"ddl_rspcodes_choice", 0, 0, 0, FALSE},       
	{"eval_attr_resp_codes", 0, 0, 0, FALSE},      
	{"set_fetch_func_ptr", 0, 0, 0, FALSE},    
	{"resolve_fetch", 0, 0, 0, FALSE},       
	{"resolve_ref", 0, 0, 0, FALSE},          
	{"get_block_id", 0, 0, 0, FALSE},          
	{"string_enum_lookup", 0, 0, 0, FALSE},     
	{"ddl_parse_string", 0, 0, 0, FALSE},        
	{"ddl_string_choice", 0, 0, 0, FALSE},        
	{"eval_string", 0, 0, 0, FALSE},               
	{"set_dict_string_upcall", 0, 0, 0, FALSE},     
	{"set_dev_spec_string_upcall", 0, 0, 0, FALSE},       
	{"ddl_shrink_dataitems_list", 0, 0, 0, FALSE},    
	{"ddl_mask_width", 0, 0, 0, FALSE},        
	{"ddl_parse_dataitems", 0, 0, 0, FALSE},   
	{"ddl_dataitems_choice", 0, 0, 0, FALSE},   
	{"eval_attr_dataitems", 0, 0, 0, FALSE},          
	{"ddl_shrink_trans_list", 0, 0, 0, FALSE},     
	{"ddl_add_transaction_list", 0, 0, 0, FALSE},   
	{"eval_transaction", 0, 0, 0, FALSE},       
	{"eval_attr_transaction_list", 0, 0, 0, FALSE},   
	{"ddl_shrink_range_list", 0, 0, 0, FALSE},   
	{"ddl_add_value", 0, 0, 0, FALSE},           
	{"ddl_parse_arth_options", 0, 0, 0, FALSE},   
	{"ddl_parse_options", 0, 0, 0, FALSE},      
	{"ddl_parse_type_size", 0, 0, 0, FALSE},    
	{"ddl_parse_type", 0, 0, 0, FALSE},      
	{"eval_typeinfo", 0, 0, 0, FALSE},        
	{"eval_attr_type", 0, 0, 0, FALSE},             
	{"eval_attr_display_format", 0, 0, 0, FALSE},    
	{"eval_attr_edit_format", 0, 0, 0, FALSE},     
	{"eval_attr_scaling_factor", 0, 0, 0, FALSE},   
	{"eval_attr_min_values", 0, 0, 0, FALSE},        
	{"eval_attr_arrayname", 0, 0, 0, FALSE},     
	{"eval_attr_max_values", 0, 0, 0, FALSE},          
	{"eval_attr_enum", 0, 0, 0, FALSE},                 
	{"block_handle_to_bt_elem", 0, 0, 0, FALSE},                 
	{"find_bt_elem", 0, 0, 0, FALSE},                 
	{"", 0, 0, 0, FALSE}, /* A function was removed, this slot is now unused. */
	{"", 0, 0, 0, FALSE}, /* A function was removed, this slot is now unused. */
	{"convert_item_spec_to_item_type", 0, 0, 0, FALSE},                 
	{"get_extn_hdr", 0, 0, 0, FALSE},                 
	{"parse_attribute_id", 0, 0, 0, FALSE},                 
	{"get_item_attr", 0, 0, 0, FALSE},                 
	{"get_rod_item", 0, 0, 0, FALSE},                 
	{"fch_rod_item_spad_size", 0, 0, 0, FALSE},                 
	{"fch_rod_dir_spad_size", 0, 0, 0, FALSE},                 
	{"fch_rod_device_dir", 0, 0, 0, FALSE},                 
	{"fch_rod_block_dir", 0, 0, 0, FALSE},                 
	{"get_dt_and_dd_handles", 0, 0, 0, FALSE},                 
	{"get_item_dd_ref", 0, 0, 0, FALSE},                 
	{"fch_item_spad_size", 0, 0, 0, FALSE},                 
	{"fch_item", 0, 0, 0, FALSE},                 
	{"get_blk_dir_dd_ref", 0, 0, 0, FALSE},                 
	{"fch_block_dir_spad_size", 0, 0, 0, FALSE},                 
	{"fch_block_dir", 0, 0, 0, FALSE},                 
	{"fch_device_dir_spad_size", 0, 0, 0, FALSE},                 
	{"fch_device_dir", 0, 0, 0, FALSE},                 
	{"get_rod_object", 0, 0, 0, FALSE},                 
	{"get_local_data", 0, 0, 0, FALSE},                 
	{"eval_update_tbl", 0, 0, 0, FALSE},                 
	{"eval_param_elem_tbl", 0, 0, 0, FALSE}
};

/*
 * Table of function entries for the fail command.
 */

static RETURN_STATUS	return_status_table[MAXRTNSTAT] = {
	{"SUCCESS",							SUCCESS},
	{"DDS_SUCCESS",						DDS_SUCCESS},
	{"FAILURE",							FAILURE},
	{"DDS_OUT_OF_MEMORY",				DDS_OUT_OF_MEMORY},
	{"VARIABLE_VALUE_NEEDED",			VARIABLE_VALUE_NEEDED},
	{"DDS_ERROR_END",					DDS_ERROR_END},
	{"DDL_SUCCESS",						DDL_SUCCESS},
	{"DDL_MEMORY_ERROR",				DDL_MEMORY_ERROR},
	{"DDL_INSUFFICIENT_OCTETS",			DDL_INSUFFICIENT_OCTETS},
	{"DDL_SHORT_BUFFER",				DDL_SHORT_BUFFER},
	{"DDL_ENCODING_ERROR",				DDL_ENCODING_ERROR},
	{"DDL_LARGE_VALUE",					DDL_LARGE_VALUE},
	{"DDL_DIVIDE_BY_ZERO",				DDL_DIVIDE_BY_ZERO},
	{"DDL_BAD_VALUE_TYPE",				DDL_BAD_VALUE_TYPE},
	{"DDL_SERVICE_ERROR",				DDL_SERVICE_ERROR},
	{"DDL_FILE_ERROR",					DDL_FILE_ERROR},
	{"DDL_BAD_FILE_TYPE",				DDL_BAD_FILE_TYPE},
	{"DDL_FILE_NOT_FOUND",				DDL_FILE_NOT_FOUND},
	{"DDL_OUT_OF_DATA",					DDL_OUT_OF_DATA},
	{"DDL_DEFAULT_ATTR",				DDL_DEFAULT_ATTR},
	{"DDL_NULL_POINTER",				DDL_NULL_POINTER},
	{"DDL_INCORRECT_FILE_FORMAT",		DDL_INCORRECT_FILE_FORMAT},
	{"DDL_INVALID_PARAM",				DDL_INVALID_PARAM},
	{"DDL_CHECK_RETURN_LIST",			DDL_CHECK_RETURN_LIST},
	{"DDL_DEV_SPEC_STRING_NOT_FOUND",	DDL_DEV_SPEC_STRING_NOT_FOUND},
	{"DDL_DICT_STRING_NOT_FOUND",		DDL_DICT_STRING_NOT_FOUND},
	{"DDL_READ_VAR_VALUE_FAILED",		DDL_READ_VAR_VALUE_FAILED},
	{"DDL_BINARY_REQUIRED",				DDL_BINARY_REQUIRED},
	{"DDL_VAR_TYPE_NEEDED",				DDL_VAR_TYPE_NEEDED},
	{"DDL_EXPR_STACK_OVERFLOW",			DDL_EXPR_STACK_OVERFLOW},
	{"DDL_ERROR_END",					DDL_ERROR_END},
	{"FETCH_INVALID_DEVICE_HANDLE",		FETCH_INVALID_DEVICE_HANDLE},
	{"FETCH_DEVICE_NOT_FOUND",			FETCH_DEVICE_NOT_FOUND},
	{"FETCH_INVALID_DEV_TYPE_HANDLE",	FETCH_INVALID_DEV_TYPE_HANDLE},
	{"FETCH_DEV_TYPE_NOT_FOUND",		FETCH_DEV_TYPE_NOT_FOUND},
	{"FETCH_INVALID_DD_HANDLE_TYPE",	FETCH_INVALID_DD_HANDLE_TYPE},
	{"FETCH_TABLES_NOT_FOUND",			FETCH_TABLES_NOT_FOUND},
	{"FETCH_ITEM_NOT_FOUND",			FETCH_ITEM_NOT_FOUND},
	{"FETCH_DIRECTORY_NOT_FOUND",		FETCH_DIRECTORY_NOT_FOUND},
	{"FETCH_INSUFFICIENT_SCRATCHPAD",	FETCH_INSUFFICIENT_SCRATCHPAD},
	{"FETCH_NULL_POINTER",				FETCH_NULL_POINTER},
	{"FETCH_ITEM_TYPE_MISMATCH",		FETCH_ITEM_TYPE_MISMATCH},
	{"FETCH_INVALID_ATTRIBUTE",			FETCH_INVALID_ATTRIBUTE},
	{"FETCH_INVALID_RI",				FETCH_INVALID_RI},
	{"FETCH_INVALID_ITEM_TYPE",			FETCH_INVALID_ITEM_TYPE},
	{"FETCH_EMPTY_ITEM_MASK",			FETCH_EMPTY_ITEM_MASK},
	{"FETCH_ATTRIBUTE_NO_MASK_BIT",		FETCH_ATTRIBUTE_NO_MASK_BIT},
	{"FETCH_ATTRIBUTE_NOT_FOUND",		FETCH_ATTRIBUTE_NOT_FOUND},
	{"FETCH_ATTR_LENGTH_OVERFLOW",		FETCH_ATTR_LENGTH_OVERFLOW},
	{"FETCH_ATTR_ZERO_LENGTH",			FETCH_ATTR_ZERO_LENGTH},
	{"FETCH_OBJECT_NOT_FOUND",			FETCH_OBJECT_NOT_FOUND},
	{"FETCH_DATA_NOT_FOUND",			FETCH_DATA_NOT_FOUND},
	{"FETCH_DATA_OUT_OF_RANGE",			FETCH_DATA_OUT_OF_RANGE},
	{"FETCH_INVALID_DIR_TYPE",			FETCH_INVALID_DIR_TYPE},
	{"FETCH_INVALID_TABLE",				FETCH_INVALID_TABLE},
	{"FETCH_INVALID_EXTN_LEN",			FETCH_INVALID_EXTN_LEN},
	{"FETCH_INVALID_PARAM",				FETCH_INVALID_PARAM},
	{"FETCH_INVALID_ITEM_ID",			FETCH_INVALID_ITEM_ID},
	{"FETCH_INVALID_ATTR_LENGTH",		FETCH_INVALID_ATTR_LENGTH},
	{"FETCH_ERROR_END",					FETCH_ERROR_END},
	{"DDI_INVALID_FLAT_FORMAT",			DDI_INVALID_FLAT_FORMAT},
	{"DDI_INVALID_FLAT_MASKS",			DDI_INVALID_FLAT_MASKS},
	{"DDI_INVALID_TYPE",				DDI_INVALID_TYPE},
	{"DDI_INVALID_ITEM_TYPE",			DDI_INVALID_ITEM_TYPE},
	{"DDI_INVALID_PARAM",				DDI_INVALID_PARAM},
	{"DDI_MEMORY_ERROR",				DDI_MEMORY_ERROR},
	{"DDI_TAB_BAD_NAME",				DDI_TAB_BAD_NAME},
	{"DDI_TAB_BAD_DDID",				DDI_TAB_BAD_DDID},
	{"DDI_TAB_BAD_OP_INDEX",			DDI_TAB_BAD_OP_INDEX},
	{"DDI_TAB_NO_DEVICE",				DDI_TAB_NO_DEVICE},
	{"DDI_TAB_NO_BLOCK",				DDI_TAB_NO_BLOCK},
	{"DDI_TAB_BAD_PARAM_OFFSET",		DDI_TAB_BAD_PARAM_OFFSET},
	{"DDI_TAB_BAD_PARAM_LIST_OFFSET",	DDI_TAB_BAD_PARAM_LIST_OFFSET},
	{"DDI_TAB_BAD_CHAR_OFFSET",			DDI_TAB_BAD_CHAR_OFFSET},
	{"DDI_TAB_BAD_PARAM_MEMBER",		DDI_TAB_BAD_PARAM_MEMBER},
	{"DDI_TAB_BAD_PARAM_LIST_MEMBER",	DDI_TAB_BAD_PARAM_LIST_MEMBER},
	{"DDI_TAB_BAD_SUBINDEX",			DDI_TAB_BAD_SUBINDEX},
	{"DDI_TAB_BAD_PARAM_TYPE",			DDI_TAB_BAD_PARAM_TYPE},
	{"DDI_INVALID_BLOCK_HANDLE",		DDI_INVALID_BLOCK_HANDLE},
	{"DDI_BLOCK_NOT_FOUND",				DDI_BLOCK_NOT_FOUND},
	{"DDI_INVALID_DEVICE_HANDLE",		DDI_INVALID_DEVICE_HANDLE},
	{"DDI_DEVICE_NOT_FOUND",			DDI_DEVICE_NOT_FOUND},
	{"DDI_INVALID_DEV_TYPE_HANDLE",		DDI_INVALID_DEV_TYPE_HANDLE},
	{"DDI_DEV_TYPE_NOT_FOUND",			DDI_DEV_TYPE_NOT_FOUND},
	{"DDI_INVALID_DD_HANDLE",			DDI_INVALID_DD_HANDLE},
	{"DDI_INVALID_DD_HANDLE_TYPE",		DDI_INVALID_DD_HANDLE_TYPE},
	{"DDI_TAB_BAD",						DDI_TAB_BAD},
	{"DDI_LEGAL_ENUM_VAR_VALUE",		DDI_LEGAL_ENUM_VAR_VALUE},
	{"DDI_ILLEGAL_ENUM_VAR_VALUE",		DDI_ILLEGAL_ENUM_VAR_VALUE},
	{"DDI_DD_BLOCK_REF_NOT_FOUND",		DDI_DD_BLOCK_REF_NOT_FOUND},
	{"DDI_DEVICE_TABLES_NOT_FOUND",		DDI_DEVICE_TABLES_NOT_FOUND},
	{"DDI_BLOCK_TABLES_NOT_FOUND",		DDI_BLOCK_TABLES_NOT_FOUND},
	{"DDI_INVALID_BLOCK_TAG",			DDI_INVALID_BLOCK_TAG},
	{"DDI_INVALID_REQUEST_TYPE",		DDI_INVALID_REQUEST_TYPE},
	{"DDI_TAB_NO_UNIT",					DDI_TAB_NO_UNIT},
	{"DDI_TAB_NO_WAO",					DDI_TAB_NO_WAO},
	{"DDI_TAB_NO_UPDATE",				DDI_TAB_NO_UPDATE},
	{"DDI_INVALID_DEV_SPEC_STRING",		DDI_INVALID_DEV_SPEC_STRING},
	{"DDI_INSUFFICIENT_BUFFER",			DDI_INSUFFICIENT_BUFFER},
	{"DDI_ERROR_END",					DDI_ERROR_END}
};


/***********************************************************************
 *
 * Name: clean_sp_ptr_table()
 *
 * ShortDesc: Free scratch pad pointers in the table.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		This routine frees the scratch pad pointers created by eval or
 *		fetch tests, for the item that has just been cleaned.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
static void
clean_sp_ptr_table()
{
	unsigned int		i;

	for (i = 0; i < sp_ptr_table.count; i++) { 
		if (sp_ptr_table.list[i] != NULL) {
			xfree((void **) &sp_ptr_table.list[i]);
		}
	}
	sp_ptr_table.count = 0;
}


/***********************************************************************
 *
 * Name: check_to_clean()
 *
 * ShortDesc: Cleans out a flat item if need be.
 *
 * Description:
 *		This routine checks if the previous item_name, passed in from one
 *		of the cmds routines, is the same as the current one.  If it's not,
 *		the flat_item is cleaned and nulled out, if it is the same, the
 *		flat_item is left unchanged, so we can test reusability of the flats.
 *		Directories are handled in a special way.
 *
 * Inputs:
 *		flat_item -- void pointer to a previous flat item.
 *		prev_item_name -- name of the previous flat_item.
 *		curr_object_index -- object index of the current flat_item.
 *		prev_object_index -- object index of the previous flat_item.
 *
 * Outputs:
 *		prev_flat_item
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS
 *			FAILURE
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
check_to_clean(void **flat_item, OBJECT_INDEX prev_object_index,
	OBJECT_INDEX curr_object_index, ITEM_TYPE prev_item_type,
	unsigned int prev_cmd_type, unsigned int curr_cmd_type)
{

	int				rs;
	FLAT_VAR *		temp_flat_var;

	DDI_GENERIC_ITEM generic_item;

	if (((prev_cmd_type != curr_cmd_type) && (prev_cmd_type != ABSURD_NUMBER)) ||
		((prev_object_index != curr_object_index) &&
		 (*flat_item != NULL) && (prev_object_index != 0))) {

		if ((prev_cmd_type == EVAL_COMMAND) || (prev_cmd_type == FETCH_COMMAND)) {
			if (prev_item_type == DEVICE_DIR_TYPE) {
				eval_clean_device_dir((FLAT_DEVICE_DIR *) *flat_item);
			}
			else if	(prev_item_type == BLOCK_DIR_TYPE) {
				eval_clean_block_dir((FLAT_BLOCK_DIR *) *flat_item);
			}
			else {
				eval_clean_item(*flat_item, prev_item_type);
			}
			clean_sp_ptr_table();
		}
		else {
			if (prev_item_type == DEVICE_DIR_TYPE) { 
				ddi_clean_device_dir((FLAT_DEVICE_DIR *) *flat_item);
			}
			else if (prev_item_type == BLOCK_DIR_TYPE) {
				ddi_clean_block_dir((FLAT_BLOCK_DIR *) *flat_item);
			}
			else {
			        generic_item.item_type = prev_item_type;
				generic_item.item = *flat_item;
				rs = ddi_clean_item(&generic_item);
				*flat_item = NULL;
				return(rs);
			}
		}

		if (prev_cmd_type != DDI_COMMAND) {
		  if (prev_item_type == VARIABLE_ITYPE) {
		    temp_flat_var = (FLAT_VAR *) *flat_item;
		    if (temp_flat_var->actions) {
		      xfree((void **) &temp_flat_var->actions);
		    }
		    if (temp_flat_var->misc) {
		      xfree((void **) &temp_flat_var->misc);
		    }
		  }
		}

		  xfree(flat_item);
		*flat_item = NULL;
	}

	return(SUCCESS);
}


/***********************************************************************
 *
 * Name: allocate_flat_item()
 *
 * ShortDesc: Allocate enough space for a flat item.
 *
 * Description:
 *		This routine allocates memory depending on the item_type.  For
 *		variable item_type, it also looks at the request mask to see if
 *		allocation of misc and/or actions portions is needed.
 *
 * Inputs:
 *		flat_item -- void pointer to a flat item.
 *		item_type -- type of the item.
 *		mask -- request mask, used only for variable item_type.
 *		cmd_type -- type of command we're allocating the flat for.
 *
 * Outputs:
 *		str_out
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS
 *			FAILURE
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
allocate_flat_item(void **flat_item, ITEM_TYPE item_type,
		unsigned long mask, unsigned int cmd_type)
{

	FLAT_VAR *		flat_var;

	switch (item_type) {

	case VARIABLE_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_VAR));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_VAR));
		}
		if (cmd_type != DDI_COMMAND) {
			flat_var = (FLAT_VAR *) *flat_item;
			if ((flat_var->actions == NULL) && (mask & VAR_ACT_MASKS)) {
				flat_var->actions = xmalloc(sizeof(FLAT_VAR_ACTIONS));
				(void)memset((char *) (flat_var->actions), 0, sizeof(FLAT_VAR_ACTIONS));
			}
			if ((flat_var->misc == NULL) && (mask & VAR_MISC_MASKS)) {
				flat_var->misc = xmalloc(sizeof(FLAT_VAR_MISC));
				(void)memset((char *) (flat_var->misc), 0, sizeof(FLAT_VAR_MISC));
			}
		}
		break;

	case MENU_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_MENU));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_MENU));
		}
		break;

	case EDIT_DISP_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_EDIT_DISPLAY));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_EDIT_DISPLAY));
		}
		break;

	case METHOD_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_METHOD));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_METHOD));
		}
		break;

	case REFRESH_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_REFRESH));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_REFRESH));
		}
		break;

	case UNIT_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_UNIT));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_UNIT));
		}
		break;

	case WAO_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_WAO));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_WAO));
		}
		break;

	case ITEM_ARRAY_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_ITEM_ARRAY));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_ITEM_ARRAY));
		}
		break;

	case COLLECTION_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_COLLECTION));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_COLLECTION));
		}
		break;

	case PROGRAM_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_PROGRAM));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_PROGRAM));
		}
		break;


	case ARRAY_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_ARRAY));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_ARRAY));
		}
		break;

	case RECORD_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_RECORD));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_RECORD));
		}
		break;

	case VAR_LIST_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_VAR_LIST));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_VAR_LIST));
		}
		break;

	case BLOCK_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_BLOCK));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_BLOCK));
		}
		break;

	case RESP_CODES_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_RESP_CODE));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_RESP_CODE));
		}
		break;

	case DOMAIN_ITYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_DOMAIN));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_DOMAIN));
		}
		break;

	case DEVICE_DIR_TYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_DEVICE_DIR));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_DEVICE_DIR));
		}
		break;

	case BLOCK_DIR_TYPE:
		if (*flat_item == NULL) {
			*flat_item = xmalloc(sizeof(FLAT_BLOCK_DIR));
			(void)memset((char *) (*flat_item), 0, sizeof(FLAT_BLOCK_DIR));
		}
		break;

	default:
        LOGC_ERROR (CPM_FF_DDSERVICE,"allocate_flat_item: Bad ITEM type %d\n", item_type);
		return (FAILURE);
	}

	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name: get_dd_ref
 *
 *	ShortDesc: Convert the item specifier into a dd_ref
 *
 *	Description:
 *		get_dd_ref takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the device description reference and therefore object
 *		index.  This function was copied from ddi_item.c and is
 *		used to get the object_index so we can check for the item
 *		being looked, whether it's the same or not between evals.
 *
 *	Inputs:
 *		dd_item_spec --	a pointer to the DDI_ITEM_SPECIFIER structure
 *		block_handle --	the current block handle
 *		dd_ref -- includes object_index of the item
 *
 *	Outputs:
 *		dd_ref
 *
 *	Returns: DDI_BAD_ITEM_SPECIFIER_TYPE and returns from dd_ref table
 *  request functions
 *
 *	Author: Dave Raskin
 *
 **********************************************************************/
int
get_dd_ref(DDI_ITEM_SPECIFIER *dd_item_spec, BLOCK_HANDLE block_handle,
		DD_REFERENCE *dd_ref, BLK_TBL_ELEM *bt_elem)
{

	int             rc;	/* return code */
	ITEM_TBL		*it ;
	ITEM_TBL_ELEM	*it_elem ;
	FLAT_DEVICE_DIR	*flat_device_dir = NULL ;
	int				r_code ;

	it_elem = NULL ;

	r_code = get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	it = &flat_device_dir->item_tbl ;

	switch (dd_item_spec->type) {

	case DDI_ITEM_BLOCK:
		rc = tr_block_to_ite(it, bt_elem, &it_elem);
		break;

	case DDI_ITEM_ID:
		rc = tr_id_to_ite(it, dd_item_spec->item.id, &it_elem);
		break;

	case DDI_ITEM_NAME:
		rc = tr_name_to_ite(it, bt_elem, dd_item_spec->item.name, &it_elem);
		break;

	case DDI_ITEM_OP_INDEX:
		rc = tr_op_index_to_ite(block_handle, it, bt_elem, dd_item_spec->item.op_index,
			&it_elem);
		break;

	case DDI_ITEM_PARAM:
		rc = tr_param_to_ite(it, bt_elem, dd_item_spec->item.param, &it_elem);
		break;

	case DDI_ITEM_PARAM_LIST:
		rc = tr_param_list_to_ite(it, bt_elem, dd_item_spec->item.param_list, &it_elem);
		break;

	case DDI_ITEM_CHARACTERISTICS:
		rc = tr_characteristics_to_ite(it, bt_elem, dd_item_spec->item.characteristics,
			&it_elem);
		break;

	case DDI_ITEM_ID_SI:
		rc = tr_id_si_to_ite(it, bt_elem, dd_item_spec->item.id, dd_item_spec->subindex,
			&it_elem);
		break;

	case DDI_ITEM_NAME_SI:
		rc = tr_name_si_to_ite(it, bt_elem, dd_item_spec->item.name, dd_item_spec->subindex,
			&it_elem);
		break;

	case DDI_ITEM_OP_INDEX_SI:
		rc = tr_op_index_si_to_ite(block_handle, it, bt_elem, dd_item_spec->item.op_index,
			dd_item_spec->subindex, &it_elem);
		break;

	case DDI_ITEM_PARAM_SI:
		rc = tr_param_si_to_ite(it, bt_elem, dd_item_spec->item.param,
			dd_item_spec->subindex, &it_elem);
		break;

	default:
		return (DDI_INVALID_TYPE);
	}

	if (it_elem != NULL) {
		(void)memcpy((char *)dd_ref,(char *)&(ITE_DD_REF(it_elem)),
				sizeof(*dd_ref)) ;
	}

	return rc;

}


/***********************************************************************
 *
 * Name: build_sym_path()
 *
 * ShortDesc: Build path to the tokanized file.
 *
 * Description:
 *		This routine composes the path from the environment variable and
 *		information in the device_id structure to the tokanized file.
 *
 * Inputs:
 *		device_id -- pointer to the device_id structure.
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- id_table initialized successfully.
 *			FAILURE -- id_table initialization failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
static void
build_sym_path(DD_DEVICE_ID *device_id, char *path)
{
	if (symfile_flag != TRUE) {
		(void)sprintf(path, "%s/%06X/%04X/%02X%02X.sy5", release_path, 
				device_id->ddid_manufacturer, device_id->ddid_device_type, 
				device_id->ddid_device_rev, device_id->ddid_dd_rev);

        if( !(access( path, F_OK ) != -1) ) {
		    (void)sprintf(path, "%s/%06x/%04x/%02x%02x.sym", release_path, 
				device_id->ddid_manufacturer, device_id->ddid_device_type, 
				device_id->ddid_device_rev, device_id->ddid_dd_rev);
        }
	}
	else {
		(void)sprintf(path, "%s", symfilename);
	}
}


/*********************************************************************
 *
 *  Name: item_compare
 *
 *  ShortDesc:  Compare the item_id numbers in the ITEM_TBL_ELEM
 *				struct passed.  This routine is used by the bsearch
 *				library function called in init_id_table routine.
 *
 *  Inputs:
 *		ptr_a - pointer to void.
 *		ptr_b - pointer to void.
 *
 *  Returns:
 *		< 0, > 0, or = 0 -- comparison result;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
static int
item_compare(void const *ptr_a, void const *ptr_b)
{
	ITEM_TBL_ELEM *item_a, *item_b;

	item_a = (ITEM_TBL_ELEM *) ptr_a;
	item_b = (ITEM_TBL_ELEM *) ptr_b;

    if (item_a->item_id < item_b->item_id) {
		return(-1);
	}
	else if (item_a->item_id > item_b->item_id) {
		return(1);
	}
	else {
		return(0);
	}
}


/***********************************************************************
 *
 * Name: fill_id_table()
 *
 * ShortDesc: Initialize id table.
 *
 * Description:
 *		This routine accesses the symbol file in the directory indicated
 *		by the path.  It creates an internal table of id numbers and their 
 *		names as defined in the ddl source file.  This table is later used to 
 *		cross reference the item names from the input test file with internal 
 *		id numbers.
 *
 * Inputs:
 *		rod_item_tbl -- pointer to the ITEM_TBL in the conman tables.
 *		rod_device_id -- pointer to DEVICE_ID information in the conman tables.
 *		id_table_header -- header of the id_table to fill this time.
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- id_table initialized successfully.
 *			FAILURE -- id_table initialization failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
fill_id_table(ITEM_TBL *rod_item_tbl, DD_DEVICE_ID *rod_device_id,
	ID_TABLE_HEADER *id_table_header)
{
	unsigned int			i;
	char            		sym_file_path[FILE_PATH_LEN];
	char            		file_line[MAX_LINE_LEN + 1];
	char *					file_line_ptr;
	int             		fourth_field_id;
	int						member_type = FALSE;
	ITEM_TBL_ELEM *			found_ptr;
	ITEM_TBL_ELEM			key;

	/*
	 * Build the symbol file path
     */

	build_sym_path(rod_device_id, sym_file_path);

	/*
	 * Open the symbol file
	 */

    symfileptr = fopen(sym_file_path, "r");
	if (symfileptr == NULL) {
		(void)printf("fill_id_table: Symbol file `%s` could not be opened\n", sym_file_path);
		return (FAILURE);
	}
	rewind(symfileptr);

	/*
	 * Fill in the id table
	 */

	(void) fgets(file_line, MAX_LINE_LEN, symfileptr);
	while (!feof(symfileptr)) {

		fourth_field_id = FALSE;
		file_line_ptr = strtok(file_line, " \t");

		if (strncmp(file_line_ptr, "member", 6) == 0) {
			member_type = TRUE;
			fourth_field_id = TRUE;
		}
		else if ((strncmp(file_line_ptr, "variable", 8) == 0) ||
				(strncmp(file_line_ptr, "collection", 10) == 0) ||
				(strncmp(file_line_ptr, "item-array", 10) == 0)) {
			fourth_field_id = TRUE;
		}

		/*
		 * Get the second field and fill in the item name
		 */

		file_line_ptr = strtok((char *) NULL, " \t");
		(void) strcpy(id_table_header->table[id_table_header->num_entries].item_name,
			file_line_ptr);

		/*
		 * Get the third field
		 *	In some instances, members will not have field three 
		 *	(see ANALOG_UNITS in 00000d/000f/0101.sym)
		 *	I am going to read field three and if the first two characters are "0x"
		 *	I will treat field three as the "item_id" (ie. field 4).
		 *
		 */

		file_line_ptr = strtok((char *) NULL, " \t");

		/*
		 * Get the fourth field if it's one of the following item
		 * types: variable, collection, array
		 */

        if ( (fourth_field_id) && (strncmp( file_line_ptr, "0x", 2) != 0 ) ) {
            file_line_ptr = strtok((char *) NULL, " \t");
        }

        /*
         * Fill in the item id
         */

        id_table_header->table[id_table_header->num_entries].item_id =
                            strtoul(file_line_ptr, (char**) NULL, 16);

        /*
         * If a parameter/member, then do NOT fill in the object_index and type
         */

        if (member_type == TRUE) {
            (void) fgets(file_line, MAX_LINE_LEN, symfileptr);
            id_table_header->num_entries++;
            member_type = FALSE;
            continue;
        }

		/*
		 * Do a binary search on the rod_item_tbl to get the object_index
		 */

		key.item_id = id_table_header->table[id_table_header->num_entries].item_id;
	    found_ptr = (ITEM_TBL_ELEM *) bsearch((char *) &key,
					(char *) rod_item_tbl->list, (unsigned) rod_item_tbl->count,
					sizeof(ITEM_TBL_ELEM), (CMP_FN_PTR)item_compare);
		if (found_ptr != NULL) {
			id_table_header->table[id_table_header->num_entries].object_index = 
							found_ptr->dd_ref.object_index;
			id_table_header->table[id_table_header->num_entries].item_type = 
							found_ptr->item_type;
		}

		id_table_header->num_entries++;

		/*
		 * Get the next line in the symbol file
		 */

		(void) fgets(file_line, MAX_LINE_LEN, symfileptr);
	}


	if (idtable_flag) {
        LOGC_DEBUG(CPM_FF_DDSERVICE,"\n------ID TABLE------\n");
		for (i = 0; i < id_table_header->num_entries; i++) {
            LOGC_DEBUG(CPM_FF_DDSERVICE,"id_table[%d].item_name    = %s\n",
				i, id_table_header->table[i].item_name);
            LOGC_DEBUG(CPM_FF_DDSERVICE,"id_table[%d].item_id      = %lx\n",
				i, id_table_header->table[i].item_id);
            LOGC_DEBUG(CPM_FF_DDSERVICE,"id_table[%d].item_type    = %x\n",
				i, id_table_header->table[i].item_type);
            LOGC_DEBUG(CPM_FF_DDSERVICE,"id_table[%d].object_index = %d\n",
				i, id_table_header->table[i].object_index);
		}

	}

	/*
	 * Close the symbol file
	 */
	(void) fclose(symfileptr);

	/*
	 * Initialize two entries in the table to bad item and bad member,
	 * these are used for inserting bad values on the script lines
	 */

	(void) strcpy(id_table_header->table[id_table_header->num_entries].item_name,
						"Bad_Item_Name");
	id_table_header->table[id_table_header->num_entries].item_id = 1;
	id_table_header->table[id_table_header->num_entries].item_type = RESERVED_ITYPE1;
	id_table_header->table[id_table_header->num_entries].object_index = 1;
	id_table_header->num_entries++;

	(void) strcpy(id_table_header->table[id_table_header->num_entries].item_name,
						"Bad_Param_Name");
	id_table_header->table[id_table_header->num_entries].item_id = 2;
	id_table_header->table[id_table_header->num_entries].item_type = RESERVED_ITYPE2;
	id_table_header->table[id_table_header->num_entries].object_index = 2;
	id_table_header->num_entries++;

	return (SUCCESS);
}


/***********************************************************************
 *
 * Name: item_id_to_ob_idx()
 *
 * ShortDesc: Convert item id to object index.
 *
 * Description:
 *		This routine accesses the id table and returns the object index
 *		of the item id that is passed in.
 *
 * Inputs:
 *		item_id -- item id
 *		object_index -- object index of the item id passed in.
 *
 * Outputs:
 *		object_index
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- item name converted successfully.
 *			FAILURE -- item name conversion failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
item_id_to_ob_idx(ITEM_ID item_id, OBJECT_INDEX *object_index)
{
	unsigned int             i;

	*object_index = 0;

	for (i = 0; i < id_table.num_entries; i++) {
		if (id_table.table[i].item_id == item_id) {
			*object_index = id_table.table[i].object_index;
			return (SUCCESS);
		}
	}

	return (FAILURE);
}


/***********************************************************************
 *
 * Name: ob_idx_to_name()
 *
 * ShortDesc: Convert object index to item name string.
 *
 * Description:
 *		This routine accesses the symbol table and returns the item name
 *		of the object index number that is passed in.
 *
 * Inputs:
 *		object_index -- item id number.
 *		item_name -- item name string as it is in ddl and input test file.
 *
 * Outputs:
 *		item_name
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- item name converted successfully.
 *			FAILURE -- item name conversion failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
ob_idx_to_name(OBJECT_INDEX  	object_index,char  **item_name)
{
	unsigned int             i;

	for (i = 0; i < id_table.num_entries; i++) {
		if (id_table.table[i].object_index == object_index) {
			*item_name = id_table.table[i].item_name;
			return (SUCCESS);
		}
	}

	return (FAILURE);
}


/***********************************************************************
 *
 * Name: item_id_to_type()
 *
 * ShortDesc: Convert item id to item type.
 *
 * Description:
 *		This routine accesses the id table and returns the item type 
 *		of the item id that is passed in.
 *
 * Inputs:
 *		item_id -- item id
 *		item_type -- item_type of the item id passed in.
 *
 * Outputs:
 *		item_type
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- item name converted successfully.
 *			FAILURE -- item name conversion failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
item_id_to_type(ITEM_ID	item_id, ITEM_TYPE *item_type)
{
	unsigned int             i;

	*item_type = 0;

	for (i = 0; i < id_table.num_entries; i++) {
		if (id_table.table[i].item_id == item_id) {
			*item_type = id_table.table[i].item_type;
			return (SUCCESS);
		}
	}

	return (FAILURE);
}


/***********************************************************************
 *
 * Name: set_eval_var_value()
 *
 * ShortDesc: Convert object index to item name string.
 *
 * Description:
 *		This routine accesses the symbol table and returns the item name
 *		of the object index number that is passed in.
 *
 * Inputs:
 *		type -- type of the value passed in.
 *		size -- itype of the value passed in.
 *		value -- value to set to.
 *		eval_var_value -- EVAL_VAR_VALUE struct to fill.
 *
 * Outputs:
 *		eval_var_value
 *
 * Returns:
 *		One of the following codes is returned.
 *			SUCCESS -- item name converted successfully.
 *			FAILURE -- item name conversion failed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
set_eval_var_value(unsigned short type, unsigned short size,
		char *value, EVAL_VAR_VALUE *eval_var_value)
{

	eval_var_value->size = size;
	eval_var_value->type = type;

	switch (type) {

		case DDS_DOUBLE:
			eval_var_value->val.d = atof(value);
			break;

		case DDS_FLOAT:
			eval_var_value->val.f = (float) atof(value);
			break;

		case DDS_BOOLEAN_T:
		case DDS_INTEGER:
			eval_var_value->val.i = atol(value);
			break;

		case DDS_UNSIGNED:
		case DDS_INDEX:
		case DDS_ENUMERATED:
		case DDS_BIT_ENUMERATED:
			eval_var_value->val.u = (unsigned long) atol(value);
			break;

		case DDS_TIME:
		case DDS_TIME_VALUE:
		case DDS_DATE_AND_TIME:
		case DDS_DURATION:
			(void)memcpy (eval_var_value->val.ca, value, size);
			break;

		case DDS_ASCII:
		case DDS_PASSWORD:
		case DDS_BITSTRING:
		case DDS_OCTETSTRING:
		case DDS_VISIBLESTRING:
		case DDS_EUC:
			if (eval_var_value->val.s.str != NULL) {
				xfree((void *) &eval_var_value->val.s.str);
			}

			if ((eval_var_value->val.s.str = (char *)xmalloc(50)) == NULL) {
				return DDS_OUT_OF_MEMORY;
			}

			eval_var_value->val.s.flags = FREE_STRING;
			(void)strcpy(eval_var_value->val.s.str, value);
			eval_var_value->size = (unsigned short) strlen(eval_var_value->val.s.str);
			eval_var_value->val.s.len = eval_var_value->size;
			break;

		default:
			(void)printf("set_eval_var_value: Reached default in type switch\n");
			return (FAILURE);

	}
	
	return(SUCCESS);
}




/***********************************************************************
 *
 * Name: init_sp_ptr_table()
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *      Initialize scratch pad pointer table.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
void
init_sp_ptr_table()
{
	(void) memset((char *) &sp_ptr_table, 0, sizeof(SP_PTR_TABLE));
}


/***********************************************************************
 *
 * Name: save_sp_ptr()
 *
 * ShortDesc: Save scratch pad pointer.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		This routine saves a pointer to a scratch pad to be freed
 *		at the time of the flat item clean.
 *
 * Inputs:
 *		sp_pad_ptr -- pointer to scratch pad to store.
 *
 * Return:
 *		SUCCESS
 *		FAILURE
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
save_sp_ptr(unsigned char *sp_pad_ptr)
{

	if (sp_ptr_table.count > MAX_SP_PTRS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"save_sp_ptr: No room to save scratch pad pointer\n");
		return FAILURE;
	}

	sp_ptr_table.list[sp_ptr_table.count] = sp_pad_ptr;
	sp_ptr_table.count++;

	if (sp_ptr_table.count > MAX_SP_PTRS) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"save_sp_ptr: No room to save scratch pad pointers in the future\n");
	}
	return SUCCESS;
}


/***********************************************************************
 *
 * Name: incr_fail_func_count()
 *
 * ShortDesc: Increments the fail count and returns status.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		This function increments the fail count for the function index
 *		passed in.  If the count has reached its max count, a desired
 *		return status is returned.
 *
 * Inputs:
 *		func_name -- name of the function to increment count for.
 *
 * Output:
 *		SUCCESS or the return status specified in the fail func table for
 *		the function index passed.
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
static int
incr_fail_func_count(int func_index)
{
	
	/*
	 * Check if this function is marked for failure
	 */

	if (fail_func_table[func_index].turned_on != TRUE) {
		return SUCCESS;
	}

	fail_func_table[func_index].curr_count++;

	/*
	 * If current count (just incremented) has reached the number of
	 * iterations to go before failing, then return the return status
	 * in the entry for this function.  (Number of iterations is
	 * specified on the script line for the fail command.)
	 */

	if (fail_func_table[func_index].curr_count > 
		fail_func_table[func_index].iteration) {
		fail_func_table[func_index].turned_on = FALSE;
		return (fail_func_table[func_index].return_status);
	}

	return SUCCESS;
}


/***********************************************************************
 *
 * Name: func_name_search()
 *
 * ShortDesc: Free scratch pad pointers in the table.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		Searches for the function name passed in and returns -1 if
 *		not found, or the index of the function name in the table.
 *
 * Inputs:
 *		func_name -- name of the function to search for
 *
 * Output:
 *		index of the func_name entry in the fail func table
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
static int
func_name_search(char *func_name)
{
	int		i;

	for (i = 0; i < MAXFAILFUNC; i++) {
		if (strcmp(fail_func_table[i].func_name, func_name) == 0) {
			return i;
		}
	}

	return -1;
}


/***********************************************************************
 *
 * Name: return_status_search()
 *
 * ShortDesc: Searches for the return status passed in.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		Searches for the return status passed in and returns -1 if
 *		not found, or the number of the return status.
 *
 * Inputs:
 *		return_status -- name of the return_status to search for
 *
 * Output:
 *		defined number of the return status
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
static int
return_status_search(char *return_status)
{
	int		i;

	for (i = 0; i < MAXRTNSTAT; i++) {
		if (strcmp(return_status_table[i].rs_name, return_status) == 0) {
			return (return_status_table[i].rs_number);
		}
	}

	return -1;
}

/***********************************************************************
 *
 * Name: init_fail_func()
 *
 * ShortDesc: Initialize func array entry.
 *
 * Include:
 *		tst_dds.h
 *
 * Description:
 *		This function initializes the entry in the array for the function
 *		passed in.  
 *
 * Inputs:
 *		func_name -- name of the function to increment count for.
 *		return_status -- return status to return at fail time.
 *		iteration -- number of times to not fail before failing.
 *
 * Inputs:
 *		SUCCESS or FAILURE
 *		
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
int
init_fail_func(char *func_name, char *rs_name, int iteration)
{
	int		func_index = -1;
	int		return_status = -1;

	/*
	 * Make sure the incr_fail_func_count_ptr function pointer is set.
	 */

	incr_fail_func_count_ptr = incr_fail_func_count;

	/*
	 * Find the entry for this function
	 */

	func_index = func_name_search(func_name);
	if (func_index < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"init_fail_func: Searching for function name failed\n");
		return FAILURE;
	}
	
	/*
	 * Find the defined number of the return status in the rc table
	 */

	return_status = return_status_search(rs_name);
	if (return_status < 0) {
        LOGC_ERROR(CPM_FF_DDSERVICE,"init_fail_func: Searching for return status name failed\n");
		return FAILURE;
	}
	
	/*
	 * Initialize the entry
	 */

	fail_func_table[func_index].return_status = return_status;
	fail_func_table[func_index].iteration = iteration;
	fail_func_table[func_index].curr_count = 0;
	fail_func_table[func_index].turned_on = TRUE;

	return SUCCESS;
}
