
/**
 *		Device Description Services Rel. 4.3
 *		Copyright 1994-2002 - Fieldbus Foundation
 *		All rights reserved.
 */


/************************************************************
 * Includes
 ************************************************************/

#include    <stdarg.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <assert.h>
#include    <ctype.h>
#include    <string.h>

#include    "std.h"
#include    "app_xmal.h"
#include    "dict.h"
#include    "cm_lib.h"
#include    "tst_cmn.h"
#include    "flk_log4cplus_clogger.h"
#include    "flk_log4cplus_defs.h"

/************************************************************
 * Global command line flags definitions
 ************************************************************/

int         help_flag = FALSE;
int         strings_flag = FALSE;
int         linenum_flag = FALSE;
static int	infile_flag = FALSE;
static int	outfile_flag = FALSE;
static int	error_flag = FALSE;
int         symfile_flag = FALSE;
int         idtable_flag = FALSE;
int         release_flag = FALSE;


/************************************************************
 * Global variable definitions
 ************************************************************/

/*
 * Holds the environment variable for DEVICE_PATH or NTOK_PATH
 */
char *      env_var_path;

/*
 * Current line number of the input test file
 */
int      file_line_number = 0;

/*
 * File handle for the input test file
 */
FILE    *infileptr;
char     infilename[MAX_FILE_NAME_LEN];

/*
 * File handle for the symbol file
 */
FILE    *symfileptr;
char     symfilename[FILE_PATH_LEN];

/*
 * File handle for the error file
 */
FILE           *errfileptr;
char     errorfilename[MAX_FILE_NAME_LEN];

/*
 * Release path for a_test, or device_path for alternative test
 */
char     release_path[FILE_PATH_LEN];
char     firmware_path[FILE_PATH_LEN];

/*
 * Id table definition
 */
ID_TABLE_HEADER   id_table;

/*
 * Standard Dictionary Table definition
 */
//unsigned long   num_dict_table_entries = 0;
DICT_TABLE_ENTRY dict_table[MAX_DICT_TABLE_SIZE];


/************************************************************
 * Local declarations
 ************************************************************/

#define MAX_UNI_ATTRS   1
static  ATTR    uni_at[MAX_UNI_ATTRS]={
    {"UNIT_ITEMS"       ,UNIT_ITEMS}};

#define MAX_REF_ATTRS   1
static  ATTR    ref_at[MAX_REF_ATTRS]={
    {"REFRESH_ITEMS"    ,REFRESH_ITEMS}};

#define MAX_WAO_ATTRS   1
static  ATTR    wao_at[MAX_WAO_ATTRS]={
    {"WAO_ITEMS"        ,WAO_ITEMS}};

#define MAX_DOM_ATTRS   2
static  ATTR    dom_at[MAX_DOM_ATTRS]={
    {"DOMAIN_HANDLING"  ,DOMAIN_HANDLING},
    {"DOMAIN_RESP_CODES"    ,DOMAIN_RESP_CODES}};

#define MAX_MET_ATTRS   6
static  ATTR    met_at[MAX_MET_ATTRS]={
    {"METHOD_CLASS"     ,METHOD_CLASS},
    {"METHOD_LABEL"     ,METHOD_LABEL},
    {"METHOD_HELP"      ,METHOD_HELP},
    {"METHOD_VALID"     ,METHOD_VALID},
    {"METHOD_DEF"       ,METHOD_DEF},
	{"METHOD_SCOPE"		,METHOD_SCOPE}};

#define MAX_LIS_ATTRS   4
static  ATTR    lis_at[MAX_LIS_ATTRS]={
    {"VAR_LIST_MEMBERS" ,VAR_LIST_MEMBERS},
    {"VAR_LIST_LABEL"   ,VAR_LIST_LABEL},
    {"VAR_LIST_RESP_CODES"  ,VAR_LIST_RESP_CODES},
    {"VAR_LIST_HELP"    ,VAR_LIST_HELP}};

#define MAX_RES_ATTRS   1
static  ATTR    res_at[MAX_RES_ATTRS]={
    {"RESP_CODE_MEMBER" ,RESP_CODE_MEMBER}};

#define MAX_PRO_ATTRS   2
static  ATTR    pro_at[MAX_PRO_ATTRS]={
    {"PROGRAM_ARGS"     ,PROGRAM_ARGS},
    {"PROGRAM_RESP_CODES"   ,PROGRAM_RESP_CODES}};

#define MAX_EDI_ATTRS   5
static  ATTR    edi_at[MAX_EDI_ATTRS]={
    {"EDIT_DISPLAY_LABEL"   ,EDIT_DISPLAY_LABEL},
    {"EDIT_DISPLAY_EDIT_ITEMS",EDIT_DISPLAY_EDIT_ITEMS},
    {"EDIT_DISPLAY_DISP_ITEMS",EDIT_DISPLAY_DISP_ITEMS},
    {"EDIT_DISPLAY_PRE_EDIT_ACT",EDIT_DISPLAY_PRE_EDIT_ACT},
    {"EDIT_DISPLAY_POST_EDIT_ACT",EDIT_DISPLAY_POST_EDIT_ACT}};

#define MAX_MEN_ATTRS   2
static  ATTR    men_at[MAX_MEN_ATTRS]={
    {"MENU_LABEL"       ,MENU_LABEL},
    {"MENU_ITEMS"       ,MENU_ITEMS}};

#define MAX_COL_ATTRS   3
static  ATTR    col_at[MAX_COL_ATTRS]={
    {"COLLECTION_HELP"  ,COLLECTION_HELP},
    {"COLLECTION_LABEL" ,COLLECTION_LABEL},
    {"COLLECTION_MEMBERS"   ,COLLECTION_MEMBERS}};

#define MAX_ARR_ATTRS   5
static  ATTR    arr_at[MAX_ARR_ATTRS]={
    {"ARRAY_TYPE"       ,ARRAY_TYPE},
    {"ARRAY_NUM_OF_ELEMENTS",ARRAY_NUM_OF_ELEMENTS},
    {"ARRAY_LABEL"      ,ARRAY_LABEL},
    {"ARRAY_RESP_CODES" ,ARRAY_RESP_CODES},
    {"ARRAY_HELP"       ,ARRAY_HELP}};

#define MAX_IAR_ATTRS   3
static  ATTR    iar_at[MAX_IAR_ATTRS]={
    {"ITEM_ARRAY_HELP"  ,ITEM_ARRAY_HELP},
    {"ITEM_ARRAY_LABEL" ,ITEM_ARRAY_LABEL},
    {"ITEM_ARRAY_ELEMENTS"  ,ITEM_ARRAY_ELEMENTS}};

#define MAX_VAR_ATTRS   23
static  ATTR    var_at[MAX_VAR_ATTRS]={
    {"VAR_CLASS"        ,VAR_CLASS},
    {"VAR_HANDLING"     ,VAR_HANDLING},
    {"VAR_UNIT"     ,VAR_UNIT},
    {"VAR_LABEL"        ,VAR_LABEL},
    {"VAR_HELP"     ,VAR_HELP},
    {"VAR_READ_TIME_OUT"    ,VAR_READ_TIME_OUT},
    {"VAR_WRITE_TIME_OUT"   ,VAR_WRITE_TIME_OUT},
    {"VAR_VALID"        ,VAR_VALID},
    {"VAR_PRE_READ_ACT" ,VAR_PRE_READ_ACT},
    {"VAR_POST_READ_ACT"    ,VAR_POST_READ_ACT},
    {"VAR_PRE_WRITE_ACT"    ,VAR_PRE_WRITE_ACT},
    {"VAR_POST_WRITE_ACT"   ,VAR_POST_WRITE_ACT},
    {"VAR_PRE_EDIT_ACT" ,VAR_PRE_EDIT_ACT},
    {"VAR_POST_EDIT_ACT"    ,VAR_POST_EDIT_ACT},
    {"VAR_RESP_CODES"   ,VAR_RESP_CODES},
    {"VAR_TYPE_SIZE"    ,VAR_TYPE_SIZE},
    {"VAR_DISPLAY"      ,VAR_DISPLAY},
    {"VAR_EDIT"     ,VAR_EDIT},
    {"VAR_MIN_VAL"      ,VAR_MIN_VAL},
    {"VAR_MAX_VAL"      ,VAR_MAX_VAL},
    {"VAR_SCALE"        ,VAR_SCALE},
    {"VAR_ENUMS"        ,VAR_ENUMS},
    {"VAR_INDEX_ITEM_ARRAY" ,VAR_INDEX_ITEM_ARRAY}};

#define MAX_BLK_ATTRS   13
static  ATTR    blk_at[MAX_BLK_ATTRS]={
    {"BLOCK_CHARACTERISTIC" ,BLOCK_CHARACTERISTIC},
    {"BLOCK_LABEL"      ,BLOCK_LABEL},
    {"BLOCK_HELP"       ,BLOCK_HELP},
    {"BLOCK_PARAM_LIST" ,BLOCK_PARAM_LIST},
    {"BLOCK_PARAM"      ,BLOCK_PARAM},
    {"BLOCK_COLLECT"    ,BLOCK_COLLECT},
    {"BLOCK_MENU"       ,BLOCK_MENU},
    {"BLOCK_EDIT_DISP"  ,BLOCK_EDIT_DISP},
    {"BLOCK_METHOD"     ,BLOCK_METHOD},
    {"BLOCK_REFRESH"    ,BLOCK_REFRESH},
    {"BLOCK_UNIT"       ,BLOCK_UNIT},
    {"BLOCK_WAO"        ,BLOCK_WAO},
    {"BLOCK_ITEM_ARRAY" ,BLOCK_ITEM_ARRAY}};

#define MAX_REC_ATTRS   4
static  ATTR    rec_at[MAX_REC_ATTRS]={
    {"RECORD_LABEL"     ,RECORD_LABEL},
    {"RECORD_MEMBERS"   ,RECORD_MEMBERS},
    {"RECORD_RESP_CODES",RECORD_RESP_CODES},
    {"RECORD_HELP"      ,RECORD_HELP}};

#define MAX_DEV_DIR_ATTRS   6
static  ATTR    dev_dir_at[MAX_DEV_DIR_ATTRS]={
    {"BLK_TBL_MASK"             ,BLK_TBL_MASK},
    {"ITEM_TBL_MASK"            ,ITEM_TBL_MASK},
    {"PROG_TBL_MASK"            ,PROG_TBL_MASK},
    {"DOMAIN_TBL_MASK"          ,DOMAIN_TBL_MASK},
    {"STRING_TBL_MASK"          ,STRING_TBL_MASK},
    {"DICT_REF_TBL_MASK"        ,DICT_REF_TBL_MASK},
};

#define MAX_BLK_DIR_ATTRS   13

static  ATTR    blk_dir_at[MAX_BLK_DIR_ATTRS]={
    {"BLK_ITEM_TBL_MASK"            ,BLK_ITEM_TBL_MASK},
    {"BLK_ITEM_NAME_TBL_MASK"       ,BLK_ITEM_NAME_TBL_MASK},
    {"PARAM_TBL_MASK"               ,PARAM_TBL_MASK},
    {"PARAM_MEM_TBL_MASK"           ,PARAM_MEM_TBL_MASK},
    {"PARAM_MEM_NAME_TBL_MASK"      ,PARAM_MEM_NAME_TBL_MASK},
    {"PARAM_ELEM_TBL_MASK"          ,PARAM_ELEM_TBL_MASK},
    {"PARAM_LIST_TBL_MASK"          ,PARAM_LIST_TBL_MASK},
    {"PARAM_LIST_MEM_TBL_MASK"      ,PARAM_LIST_MEM_TBL_MASK},
    {"PARAM_LIST_MEM_NAME_TBL_MASK" ,PARAM_LIST_MEM_NAME_TBL_MASK},
    {"CHAR_MEM_TBL_MASK"            ,CHAR_MEM_TBL_MASK},
    {"CHAR_MEM_NAME_TBL_MASK"       ,CHAR_MEM_NAME_TBL_MASK},
    {"REL_TBL_MASK"                 ,REL_TBL_MASK},
    {"UPDATE_TBL_MASK"              ,UPDATE_TBL_MASK},
};


ITEM_ATTR   types[DDS_MAX_TYPES]={
{"RESERVED_ITYPE1"  ,0,0,0},
{"VARIABLE_ITYPE"   ,var_at,MAX_VAR_ATTRS,VARIABLE_ITYPE},
{"COMMAND_ITYPE"    ,0,0,0},
{"MENU_ITYPE"       ,men_at,MAX_MEN_ATTRS,MENU_ITYPE},
{"EDIT_DISP_ITYPE"  ,edi_at,MAX_EDI_ATTRS,EDIT_DISP_ITYPE},
{"METHOD_ITYPE"     ,met_at,MAX_MET_ATTRS,METHOD_ITYPE},
{"REFRESH_ITYPE"    ,ref_at,MAX_REF_ATTRS,REFRESH_ITYPE},
{"UNIT_ITYPE"       ,uni_at,MAX_UNI_ATTRS,UNIT_ITYPE},
{"WAO_ITYPE"        ,wao_at,MAX_WAO_ATTRS,WAO_ITYPE},
{"ITEM_ARRAY_ITYPE" ,iar_at,MAX_IAR_ATTRS,ITEM_ARRAY_ITYPE},
{"COLLECTION_ITYPE" ,col_at,MAX_COL_ATTRS,COLLECTION_ITYPE},
{"RESERVED_ITYPE2"  ,0,0,0},
{"BLOCK_ITYPE"      ,blk_at,MAX_BLK_ATTRS,BLOCK_ITYPE},
{"PROGRAM_ITYPE"    ,pro_at,MAX_PRO_ATTRS,PROGRAM_ITYPE},
{"RECORD_ITYPE"     ,rec_at,MAX_REC_ATTRS,RECORD_ITYPE},
{"ARRAY_ITYPE"      ,arr_at,MAX_ARR_ATTRS,ARRAY_ITYPE},
{"VAR_LIST_ITYPE"   ,lis_at,MAX_LIS_ATTRS,VAR_LIST_ITYPE},
{"RESP_CODES_ITYPE" ,res_at,MAX_RES_ATTRS,RESP_CODES_ITYPE},
{"DOMAIN_ITYPE"     ,dom_at,MAX_DOM_ATTRS,DOMAIN_ITYPE},
{"MEMBER_ITYPE"     ,0,0,0},
{"MAX_ITYPE"        ,0,0,0},
{"DEVICE_DIR_TYPE"  ,dev_dir_at,MAX_DEV_DIR_ATTRS,DEVICE_DIR_TYPE},
{"BLOCK_DIR_TYPE"   ,blk_dir_at,MAX_BLK_DIR_ATTRS,BLOCK_DIR_TYPE}
};


/***********************************************************************
 *
 * Name: item_type_to_name()
 *
 * ShortDesc: Get the string for an item type.
 *
 * Description:
 *      This routine searches the types array and finds a string
 *      corresponding to the item_type passed in.
 *
 * Inputs:
 *      item_type -- item type to search types array for.
 *
 * Return:
 *      item name (char *)
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/
char *
item_type_to_name(ITEM_TYPE item_type)
{
    int         i;

    for (i = 0; i < DDS_MAX_TYPES; i++) {
        if (types[i].type_value == item_type) {
            return(types[i].type_name);
        }
    }
    return(NULL);
}


/***********************************************************************
 *
 * Name:            args_to_attr_mask()
 *
 * ShortDesc: Parse the input file line and get argumments
 *        that compose the attribute mask and convert the
 *        strings into an attribute mask.
 *
 * Include:
 *      dditest.h
 *
 * Description:
 *      This routine parses a file line, finds the arguments for the
 *              request and converts the arguments into a single request mask.
 *
 * Inputs:
 *      type_name -- pointer to the next field on the line.
 *      mask - a pointer to the variable to fill with the mask.
 *      item_type -- type of the item we are looking at.
 *
 * Outputs:
 *      mask
 *      item_type
 *
 * Returns:
 *      One of the following codes is returned.
 *          SUCCESS -- fields from the test file are retrieved successfully.
 *          FAILURE -- retrieval of fields failed on a specific line.
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/

int
args_to_attr_mask(char *type_name, IMASK *mask, ITEM_TYPE *item_type)
{

    unsigned short  i;
	int				item_index = 0;
    int             rtn_code_flag = SUCCESS;
    char            *char_ptr;
    char            *file_line_ptr;
    char            file_line[MAX_LINE_LEN + 1];
    int             attr_count = 0;

    /*
     * Parse the line for an Item type and Attribute names
     */

    *item_type = 0;
    *mask = 0;
    file_line_ptr = type_name;
    for (;;) {
	printf("file_line_ptr = %s\n", file_line_ptr);
        if ((file_line_ptr == NULL) || (*file_line_ptr == 0) ||
            (*file_line_ptr == '\n')) {

            /*
             * The end of the attribute names
             */

            if (rtn_code_flag != SUCCESS) {
                return(FAILURE);
            }
            else {
                return(SUCCESS);
            }
        }
        else if (*file_line_ptr == '\\') {

            /*
             * Get the next line
             */

            do {
                if (feof(infileptr)) {
                    return(FAILURE);
                }
                (void) fgets(file_line, 100, infileptr);
                file_line_number++;
                file_line_ptr = strtok(file_line," \t");
                while((file_line_ptr != NULL) && (*file_line_ptr != 0) &&
                        (isspace(*file_line_ptr)))
                    file_line_ptr++;

            } while ((*file_line_ptr == '#') || (*file_line_ptr == '\n'));
            continue;
        }
        else  if (*item_type == 0) {

            /*
             * Found an Item Type
             */

            for (i = 0; i < DDS_MAX_TYPES; i++) {
                if (strncmp(types[i].type_name, file_line_ptr,
                            strlen(types[i].type_name)) == 0) {
					item_index = i;
                    *item_type = types[item_index].type_value;
                    break;
                }
            }
            if (*item_type == 0) {
                LOGC_ERROR(CPM_FF_DDSERVICE,"args_to_attr_mask: %s is not a type\n", file_line_ptr);
                rtn_code_flag = FAILURE;
            }
        }
        else {

            /*
             * Found an Attribute Name
             */

            attr_count++;
            char_ptr = strstr(file_line_ptr, "0x");
            if ((char_ptr != NULL) && (attr_count <= 1)) {
                *mask = strtoul(file_line_ptr, (char**) NULL, 16);
            }
            else {
                for (i = 0; i < types[item_index].max_attrs; i++) {
                    if (strncmp(types[item_index].attrs[i].attrstr, file_line_ptr,
                                strlen(types[item_index].attrs[i].attrstr)) == 0){

                        *mask |= types[item_index].attrs[i].attrmask;
                        break;
                    }
                }
                if (i == types[item_index].max_attrs) {
                    LOGC_ERROR(CPM_FF_DDSERVICE,"args_to_attr_mask: %s is not an attr\n", file_line_ptr);
                    rtn_code_flag = FAILURE;
                }
            }
        }

        /*
         * Get the next argument
         */

        file_line_ptr = strtok((char *)NULL, " \t");
        while((file_line_ptr != NULL) && (*file_line_ptr != 0) &&
                (isspace(*file_line_ptr)))
            file_line_ptr++;
    }
}


/***********************************************************************
 *
 * Name: print_help
 *
 * ShortDesc: Print help.
 *
 * Description:
 *      This routine prints the help information for this program.
 *
 * Returns:
 *      void.
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/
void
print_help()
{
    printf("\n");
    printf("The command line syntax is:\n");
    printf("    ffddsapp [options]\n");
    printf("\n");
    printf("The following options are supported by ffddsapp:\n");
    printf("   -h, -H        Print out this help message.\n");
    printf("   -s            Print out the strings contained in the binary.\n");
    printf("   -l            Print input file line #s for every eval output.\n");
    printf("   -i            Print out the id table.\n");
    printf("   -O <file>     Specify output <file>, default is stdout.\n");
    printf("   -S <file>     Specify alternate symbol <file> to use.\n");
    printf("   -R <path>     Specify overriding release <path> to use.\n");
    printf("                 NOTE: -R option is for test only.\n");
    printf("\n");
}


/***********************************************************************
 *
 * Name: get_cmd_line_args()
 *
 * ShortDesc: Retrieve the command line arguments.
 *
 * Description:
 *      This routine gets the command line arguments and checks for their
 *      validity.
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/
int
get_cmd_line_args(int argc, char *argv[])
{
    int             i;

    if (argc <= 1) {
        printf("get_cmd_line_args: Need to specify at least the input test file\n");
        return (FAILURE);
    }

    for (i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            infile_flag = TRUE;
            (void) strcpy(infilename, argv[i]);
        }
        else {
            switch (argv[i][1]) {
            case 'h':
            case 'H':
                help_flag = TRUE;
                break;

            case 's':
                strings_flag = TRUE;
                break;

            case 'l':
                linenum_flag = TRUE;
                break;

            case 'i':
                idtable_flag = TRUE;
                break;

            case 'S':
                symfile_flag = TRUE;
                i++;
                if (i != argc) {
                    (void) strcpy(symfilename, argv[i]);
                }
                else {
                    printf("get_cmd_line_args: Need to specify symbol file name after '-S' option\n");
                    return (FAILURE);
                }
                break;

            case 'R':
                release_flag = TRUE;
                i++;
                if (i != argc) {
                    (void) strcpy(release_path, argv[i]);
                }
                else {
                    printf("get_cmd_line_args: Need to specify release path after '-R' option\n");
                    return (FAILURE);
                }
                break;

			case 'E':
				error_flag = TRUE;
                i++;
				if (i != argc) {
                    (void) strcpy(errorfilename, argv[i]);
                }
                else {
                    printf("get_cmd_line_args: Need to specify error file name after '-E' option\n");
                    return (FAILURE);
                }

                (void) strcpy(errorfilename, argv[i]);
                errfileptr = fopen(errorfilename, "a");
                if (errfileptr == NULL) {
                    printf("get_cmd_line_args: Error Opening Error File %s\n",
						errorfilename);
                    return (FAILURE);
                }
                break;
            default:
                printf("get_cmd_line_args: Invalid option -- `%s`\n", argv[i]);
                return (FAILURE);
            }
        }
    }

#ifdef IN_FILE_SUPPORT
    if ((infile_flag != TRUE) && (help_flag != TRUE)) {
        printf("get_cmd_line_args: Must specify an input test file\n");
        return (FAILURE);
    }
#endif

    return (SUCCESS);
}


/***********************************************************************
 *
 * Name: item_name_to_id()
 *
 * ShortDesc: Convert item name string to item id number.
 *
 * Description:
 *      This routine accesses the symbol table and returns the id number
 *      of the item name string that is passed in.
 *
 * Inputs:
 *      item_name -- item name string as it is in ddl and input test file.
 *      item_id -- item id number of the passed in string.
 *
 * Outputs:
 *      item_id
 *
 * Returns:
 *      One of the following codes is returned.
 *          SUCCESS -- item name converted successfully.
 *          FAILURE -- item name conversion failed.
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/
int
item_name_to_id(char *item_name, ITEM_ID *item_id)
{
    unsigned int             i;

    *item_id = 0;

    for (i = 0; i < id_table.num_entries; i++) {
        if (strcmp(id_table.table[i].item_name, item_name) == 0) {
            *item_id = id_table.table[i].item_id;
            return (SUCCESS);
        }
    }

    return (FAILURE);
}


/***********************************************************************
 *
 * Name: item_id_to_name()
 *
 * ShortDesc: Convert item id to item name string.
 *
 * Description:
 *      This routine accesses the symbol table and returns the item name
 *      of the item id number that is passed in.
 *
 * Inputs:
 *      item_id -- item id number.
 *      item_name -- item name string as it is in ddl and input test file.
 *
 * Outputs:
 *      item_name
 *
 * Returns:
 *      One of the following codes is returned.
 *          SUCCESS -- item name converted successfully.
 *          FAILURE -- item name conversion failed.
 *
 * Author:
 *      Dave Raskin
 *
 **********************************************************************/
int
item_id_to_name(ITEM_ID item_id, char **item_name)
{
    unsigned int             i;

    for (i = 0; i < id_table.num_entries; i++) {
        if (id_table.table[i].item_id == item_id) {
            *item_name = id_table.table[i].item_name;
            return (SUCCESS);
        }
    }

    return (FAILURE);
}


/**************************************************************************
 *  Name: ascii_to_packed_ascii
 *
 *  ShortDesc:  Convert an Ascii string TO a Packed Ascii string.
 *
 *  Description:
 *      Convert an Ascii string TO a Packed Ascii string.
 *
 *  Inputs:
 *      src     -   source string (the ASCII one)
 *      len     -   number of characters in the ascii string
 *
 *  Outputs:
 *      dest    -   destination string (the packed ASCII one)
 *
 *  Returns
 *      Nothing.
 *
 *  Cautions
 *      Both strings are expected to have adequate space allocated for them
 *  before this function is called.
 *      Also, this function does NOT null terminate the string.
 *      Also, this function does not check to see that the characters in the
 *  ASCII string are valid packed ASCII characters.
 *
 *  Author: Steve Beyerl
 ***************************************************************************/
void
ascii_to_packed_ascii(char *src, char *dest, int len)
{

	/*--DECLARATIONS SECTION----*/
	int             x;       /* loop index */
 
	char *astp;     /* regular ASCII string */
	char *pstp;     /* packed ASCII string */
	char tmp[100];  /* temp buffer to store ascii string */
 
	/*--CODE SECTION------------*/
	ASSERT_DBG(len < 100);
	(void)strncpy( (char *)tmp, (char *)src, ( int)len);
	pstp = dest;
	astp = tmp;
	for( x = 0; x < ( int)len; x++) {
		/*lint -save -e7??*/
		if(( *astp >= 0x40) && ( *astp <= 0x5F)) *astp -= 0x40;
		switch( x % 4) {
		case 0:
			/*SUPPRESS 53*/
			*pstp = (unsigned char)(*astp << 2);
			break;
		case 1:
			*pstp |= ( *astp & 0x30) >> 4;
			pstp++;
			/*SUPPRESS 53*/
			*pstp = ( *astp & 0x0F) << 4;
			break;
		case 2:
			*pstp |= ( *astp & 0x3C) >> 2;
			pstp++;
			/*SUPPRESS 53*/
			*pstp = ( *astp & 0x03) << 6;
			break;
		case 3:
			*pstp |= *astp & 0x3F;
			pstp++;
			break;
		default:
			break;
		}
		astp++;
		/*lint -restore*/
	}
 
	return;
}
 
 
/**************************************************************************
 *  Name: packed_ascii_to_ascii
 *
 *  ShortDesc:  Convert an Packed Ascii string TO an Ascii string.
 *
 *  Description:
 *      Convert an Packed Ascii string TO an Ascii string.
 *
 *  Inputs:
 *      src - ptr to the Packed Ascii string
 *      len - number of characters in the ascii string
 *
 *  Outputs:
 *      dest - ptr to the Ascii string
 *
 *  Returns
 *      Nothing.
 *
 *  Cautions
 *      Both strings are expected to have adequate space allocated for them
 *  before this function is called.
 *      Also, this function does NOT null terminate the string.
 *
 *  Author: Steve Beyerl
 ***************************************************************************/
void
packed_ascii_to_ascii(char *src, char *dest, int len)
{

    /*--DECLARATIONS SECTION----*/
    int     x;               /* loop index */

    char *  astp;   /* pointer to regular ascii string */
    char *  pstp;   /* pointer to packed ascii string  */

    /*--CODE SECTION------------*/
    pstp = src;
    astp = dest;
    for( x = 0; x < len; x++) {
		/*lint -save -e7??*/
        switch( x % 4) {
        case 0:
            *astp = *pstp >> 2;
            break;
        case 1:
            *astp = ( *pstp & 0x03) << 4;
            pstp++;
            *astp |= ( *pstp & 0xF0) >> 4;
            break;
        case 2:
            *astp = ( *pstp & 0x0F) << 2;
            pstp++;
            *astp |= ( *pstp & 0xC0) >> 6;
            break;
        case 3:
            *astp = *pstp & 0x3F;
            pstp++;
            break;
        default:
            break;
        }
        if( *astp <= 0x1F) *astp += 0x40;
        astp++;
		/*lint -restore*/
    }

    return;
}
