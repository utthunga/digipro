/****************************************************************************************
 * Filename: Main.ddl
 *
 * Description: The enhanced device description file for 
 *                Fieldbus transmitter
 *
 * Includes:  
 *
 * 
 ****************************************************************************************/

/* DD revisions use following rules....
  Odd numbers do not use conditionals or Menus.
  Even numbers use conditionals and Menus.

  example dd rev 1   = no conditional and no Menus
          dd rev 2   = conditional with Menus
          dd rev 100  = Factory, conditional, rev = 100 + conditional rev
          development dd rev 200 = Factory, conditional, rev = 200 ++
*/



#include "std_defs.h"
#include "com_tbls.h"
#include "builtins.h"
#include "macros.h"
#define _ARRAY_SIZE     5
#define _DATATYPESTB_ENUM8              \
        {0x00000000,    "0"         },  \
        {0x00000001,    "1"         },  \
        {0x00000002,    "2"         },  \
        {0x00000004,    "4"         },  \
        {0x00000008,    "8"         },  \
        {0x00000010,    "16"        },  \
        {0x00000020,    "32"        },  \
        {0x00000040,    "64"        },  \
        {0x00000080,    "128"       }

/* -------------------------------------------------------------------
 * DEVICE DEFINITION
 * -------------------------------------------------------------------*/
MANUFACTURER      0x001151,
DEVICE_TYPE       0x0644,
DEVICE_REVISION   0x02,
DD_REVISION       0x01


/* -------------------------------------------------------------------
 * STANDARD PARAMETER DEFINITION
 * -------------------------------------------------------------------*/
IMPORT MANUFACTURER   __FF,
DEVICE_TYPE           __STD_PARM,
DEVICE_REVISION       __STD_PARM_rel_dev_rev,
DD_REVISION           __STD_PARM_rel_dd_rev
{
	EVERYTHING;
	

    REDEFINITIONS
    {
       
        REDEFINE VARIABLE    __manufac_id         /*  Redefined to be enumeration  */
        {
            LABEL           "|en|Manufacturer|en zz|Manufacturer" ;/*[manufac_id_label]*/
            HELP            [manufac_id_help] ;
            CLASS           CONTAINED ;
            TYPE            ENUMERATED (4)
            {
				{ 0x001151, "SuperDD" }
            }
            CONSTANT_UNIT   [blank] ;
            HANDLING        READ ;
        }
      
        REDEFINE VARIABLE __dev_type    /*  Redefined to be enumeration  */
        {
          LABEL "" ;
          HELP  [dev_type_help] ;
          CLASS CONTAINED ;
          TYPE  ENUMERATED (2)
          {
			{ 0x0644, "Fluke" }

          }
          CONSTANT_UNIT   [blank] ;
          HANDLING        READ ;
        }
			
		REDEFINE VARIABLE    __target_mode
	{
		LABEL           [target_mode] ;
		HELP            [target_mode_help] ;
		CLASS           CONTAINED & OPERATE;
		TYPE            BIT_ENUMERATED (1)
		{
			_MODE_REMOTE_OUTPUT,	
			_MODE_REMOTE_CASCADE,
			_MODE_CASCADE,
			 {0x08, "Target_Auto", [mode_automatic_help]},
			{ 0x10, "Target_Manual",           [mode_manual] },
			/*_MODE_LOCKED, AR4487*/
			/*_MODE_INITIALIZATION, AR4487*/
			{0x80, "Target_OOS"}
		}
		CONSTANT_UNIT   [blank] ;
		HANDLING        READ & WRITE ;
	/*  RESPONSE_CODES  xxx ; */
	}
	REDEFINE VARIABLE    __normal_mode
	{
		LABEL           [normal_mode] ;
		HELP            [normal_mode_help] ;
		CLASS           CONTAINED & OPERATE ;
		TYPE            BIT_ENUMERATED (1)
		{
			_MODE_REMOTE_OUTPUT,
			_MODE_REMOTE_CASCADE,
			_MODE_CASCADE,
			{0x08, "Normal_Auto"},
			{0x10, "Normal_Manual"},
			_MODE_LOCKED,
			_MODE_INITIALIZATION,
			 {0x80, "Normal_OOS"}
		}
		CONSTANT_UNIT   [blank] ;
		 HANDLING       READ & WRITE ;
	/*  RESPONSE_CODES  xxx ; */
	}
    } /* End REDEFINITIONS */
} /* End __STD_PARM */



/* -------------------------------------------------------------------
 * RESOURCE BLOCK DEFINITION
 * -------------------------------------------------------------------*/
IMPORT MANUFACTURER   __FF,
DEVICE_TYPE           __RES_BLOCK_2,
DEVICE_REVISION       __RES_BLOCK_2_rel_dev_rev,
DD_REVISION           __RES_BLOCK_2_rel_dd_rev
{
  EVERYTHING;
      REDEFINITIONS
	{
		BLOCK __resource_block_2
        {
				
			   PARAMETERS
				{
					ADD ARRAY_TYPE,             					array_type;
					ADD	PRE_EDIT_ACTION_SINGLE_METHOD,				Pre_Edit_Action_Single_Method			  ;
					ADD POST_EDIT_ACTION_SINGLE_METHOD,				Post_Edit_Action_Single_Method			  ;
					ADD PRE_POST_EDIT_ACTION_SINGLE_METHOD,			Pre_Post_Edit_Action_Single_Method        ;
					ADD PRE_EDIT_ACTION_SINGLE_METHOD_ABORT,		Pre_Edit_Action_Single_Method_Abort       ;
					ADD POST_EDIT_ACTION_SINGLE_METHOD_ABORT,		Post_Edit_Action_Single_Method_Abort	  ;
					ADD PRE_POST_EDIT_ACTION_SINGLE_METHOD_ABORT,	Pre_Post_Edit_Action_Single_Method_Abort  ;
					ADD PRE_EDIT_ACTION_MULTI_METHOD,				Pre_Edit_Action_Multi_Method              ;
					ADD POST_EDIT_ACTION_MULTI_METHOD,				Post_Edit_Action_Multi_Method	          ;
					ADD PRE_POST_EDIT_ACTION_MULTI_METHOD,			Pre_Post_Edit_Action_Multi_Method         ;
					ADD PRE_EDIT_ACTION_MULTI_METHOD_ABORT,			Pre_Edit_Action_Multi_Method_Abort        ;
					ADD POST_EDIT_ACTION_MULTI_METHOD_ABORT,		Post_Edit_Action_Multi_Method_Abort       ;
					ADD PRE_POST_EDIT_ACTION_MULTI_METHOD_ABORT ,	Pre_Post_Edit_Action_Multi_Method_Abort   ;
				}
			     LOCAL_PARAMETERS
				{
					ADD	 LOC_INPUT_VAR,			fl_input_var;
				}				
				REDEFINE	COLLECTION_ITEMS
				{
					  collection_type/* ,
					  variable_example20 */
				}
				  /* REDEFINE LISTS
				{
					 ADD LIST_EXAMPLES, fl_input_var_list;
				}    */
				 
				  REDEFINE LIST_ITEMS
				{
					  list_examples,
					  fl_input_var_list,
					  fl_input_rec_list
				} 
			 REDEFINE	METHOD_ITEMS
				{
					TC_File_005_step_1,
					TC_File_005_step_2,
					test
				} 
				/*REDEFINE	METHODS
				{
					
					MUST_SEND_UNITS,		must_send_units;
					REFRESH_METHOD,			refresh_method;
					CONVERT_TO_PERCENT,		convert_To_Percent;
					PRE_EDIT_METHOD,		pre_edit_method;
					POST_EDIT_METHOD,		post_edit_method;
					ACTION_METHOD,			action_method	;
					SAMPLE_METHOD,			sample_method	;
				}*/
			REDEFINE REFRESH_ITEMS
				{
					mode_index_refresh_relation
				}
				REDEFINE WRITE_AS_ONE_ITEMS
				{
					write_as_one,
					write_as_one_1,
					/*write_as_one_2 */
					write_as_one_3/* ,
					write_as_one_4 */
				}
				REDEFINE EDIT_DISPLAY_ITEMS
				{ 
					edit_display_example,
					edit_display_record,
					edit_display_record_members/* ,
					edit_display_array */
				}
				REDEFINE 	MENU_ITEMS
				{
				   Menu_Top_resource_block_2
				  
				} 
			
		}

REDEFINE VARIABLE    __tag_desc
		{
			LABEL           [tag_desc_label] ;
			HELP            [tag_desc_help] ;
			CLASS           CONTAINED ;
			TYPE            OCTET (32) ;
			CONSTANT_UNIT   [blank] ;
			HANDLING        READ & WRITE ;
			POST_EDIT_ACTIONS {post_edit_method,post_edit_method5,post_edit_method6}
			WRITE_MODE 7;
		}
		
REDEFINE VARIABLE    __st_rev
		{
			 LABEL           st_rev_label_local ;
			 HELP            st_rev_help_local;
			 CLASS           CONTAINED ;
			 TYPE            UNSIGNED_INTEGER (2) 
			 {
				 DISPLAY_FORMAT "3.1d";
				 EDIT_FORMAT "3.2d";
			 }
			 HANDLING        READ ;
			 WRITE_MODE	7;
		}
		
	REDEFINE	VARIABLE    __alert_key
		{
			LABEL           "Alert Key" ;			
			HELP            "Alert key help local";
			CLASS           CONTAINED ;
			TYPE            UNSIGNED_INTEGER (1)
			{
				DISPLAY_FORMAT "3d";
				EDIT_FORMAT "5d";
				MIN_VALUE  1;
				MAX_VALUE 50;
			}
			PRE_EDIT_ACTIONS { post_edit_method1 }
			CONSTANT_UNIT   [blank] ;
			HANDLING        READ & WRITE ;
			VALIDITY IF(PARAM.MODE_BLK.TARGET == 0x80)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
		}
		
REDEFINE VARIABLE    __set_fstate
		{
			LABEL           set_fstate_label_local ;
			HELP            set_fstate_help_local ;
			CLASS           CONTAINED & OPERATE & DYNAMIC;
			TYPE            ENUMERATED (1)
			{
				{ 0x01,  [faultstate_off],  [faultstate_off_help] },
				{ 0x02,  [faultstate_set],  [faultstate_set_help] }

			}
			HANDLING        READ & WRITE ;
		/*  RESPONSE_CODES  xxx ; */
		}
		
REDEFINE VARIABLE    __free_space
		{
			LABEL           [free_space_label] ;		
			HELP            "Class:Contained , Type: Float , Display format , Edit format ,Min value , Max value , Constant unit , Validity " ;
			CLASS           CONTAINED & DYNAMIC ;
			TYPE            FLOAT
			{
				DISPLAY_FORMAT 
				IF(__set_fstate == 0x01 )
				{
					"3.0f";
				}
				ELSE
				{
						"3.5f";
				}
				
				EDIT_FORMAT
				IF(__set_fstate == 0x01 )
				{
					"3.0f";
				}
				ELSE
				{
						"3.5f";

				}	
				
				MIN_VALUE
				IF(__set_fstate == 0x01 )
				{
					0;
				}
				ELSE
				{
						10;
				}	
				
				MAX_VALUE
				IF(__set_fstate == 0x01 )
				{
					90;
				}
				ELSE
				{
						100;
				}		
			}
			CONSTANT_UNIT   [percent] ;
			HANDLING        READ ;	
		}
		
REDEFINE VARIABLE    __test_time_diff
		{
			LABEL           test_time_diff_local ;		
			HELP            test_time_diff_help_local;
			CLASS           CONTAINED & DYNAMIC ;
			TYPE            DURATION ;
			CONSTANT_UNIT   "Sec" ;
			HANDLING        READ & WRITE ;
			VALIDITY IF(PARAM.MODE_BLK.ACTUAL == 0x80)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
			WRITE_MODE 7;
		/*  RESPONSE_CODES  xxx ; */
		}
		
REDEFINE VARIABLE    __test_dll_time
		{
			LABEL           test_dll_time_local ;
			HELP            test_dll_time_help_local;
			CLASS           CONTAINED & DYNAMIC ;
			TYPE            TIME_VALUE ;
			CONSTANT_UNIT   "Sec" ;
			HANDLING        READ & WRITE ;
			VALIDITY IF(PARAM.MODE_BLK.ACTUAL == 0x40)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
			WRITE_MODE 7;
		}
		
REDEFINE VARIABLE    __test_date
		{
			LABEL           test_date_local ;
			HELP            test_date_help_local;
			CLASS           CONTAINED & DYNAMIC ;
			TYPE            DATE_AND_TIME ;
			CONSTANT_UNIT   [blank] ;
			HANDLING        READ & WRITE ;
			VALIDITY IF(PARAM.MODE_BLK.ACTUAL == 0x10)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
			WRITE_MODE 7;
		}
		
/*REDEFINE VARIABLE_LIST   __res_view_1
		{
			LABEL       res_view_1;
			HELP        res_view_1_help ;
			MEMBERS
			{
				VL_ST_REV,      PARAM.ST_REV ;
				VL_MODE_BLK,    PARAM.MODE_BLK ;
				VL_BLOCK_ERR,   PARAM.BLOCK_ERR ;
				VL_RS_STATE,    PARAM.RS_STATE ;
				VL_FREE_TIME,   PARAM.FREE_TIME ;
				VL_FAULT_STATE,  PARAM.FAULT_STATE ;
				VL_ALARM_SUM,   PARAM.ALARM_SUM ;
			}
			VALIDITY IF(PARAM.MODE_BLK.TARGET == 0x08)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
		}

REDEFINE VARIABLE_LIST   __res_view_2
		{
			LABEL       res_view_2_local ;
			HELP        res_view_2_help_local ;
			MEMBERS
			{
				VL_ST_REV,          PARAM.ST_REV ;
				VL_GRANT_DENY,      PARAM.GRANT_DENY ;
				VL_FEATURE_SEL,     PARAM.FEATURE_SEL ;
				VL_CYCLE_SEL,       PARAM.CYCLE_SEL ;
				VL_NV_CYCLE_T,      PARAM.NV_CYCLE_T ;
				VL_FREE_SPACE,      PARAM.FREE_SPACE ;
				VL_SHED_RCAS,       PARAM.SHED_RCAS ;
				VL_SHED_ROUT,       PARAM.SHED_ROUT ;
				VL_LIM_NOTIFY,      PARAM.LIM_NOTIFY ;
				VL_CONFIRM_TIME,    PARAM.CONFIRM_TIME ;
				VL_WRITE_LOCK,      PARAM.WRITE_LOCK ;
			}
			VALIDITY IF(PARAM.MODE_BLK.TARGET == 0x10)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
		}*/
		
REDEFINE RECORD      __test_read_write
		{
			LABEL           [test_read_write_label] ;
			HELP            "Record , Validity , Write mode" ;
			MEMBERS
			{
				VALUE_1,     __test_boolean ;
				VALUE_2,     __test_integer8 ;
				VALUE_3,     __test_integer16 ;
				VALUE_4,     __test_integer32 ;
				VALUE_5,     __test_unsigned8 ;
				VALUE_6,     __test_unsigned16 ;
				VALUE_7,     __test_unsigned32 ;
				VALUE_8,     __test_float ;
				VALUE_9,     __test_visible_string ;
				VALUE_10,    __test_octet_string ;
				VALUE_11,    __test_date ;
				VALUE_12,    __test_time ;
				VALUE_13,    __test_time_diff ;
				VALUE_14,    __test_bitstring ;
				VALUE_15,    __test_dll_time ;
			}
			VALIDITY IF(PARAM.MODE_BLK.ACTUAL == 0x08)
			{
				 TRUE;
			}
			ELSE
			{
				 FALSE;
			}
			WRITE_MODE 7;
		}
		
			REDEFINE VARIABLE    __feature_sel
		{
			LABEL           [feature_sel_label] ;
			HELP            [feature_sel_help] ;
			CLASS           CONTAINED ;
			TYPE            BIT_ENUMERATED (2)
			{
				__FF_FEATURES_ENUMS
			}
			CONSTANT_UNIT   [blank] ;
			HANDLING        READ & WRITE ;
			PRE_EDIT_ACTIONS {bit_enumeration_method}
		/*  RESPONSE_CODES  xxx ; */
		}
	}
	
} 

/* -------------------------------------------------------------------
 * AI BLOCK DEFINITION
 * -------------------------------------------------------------------*/
IMPORT MANUFACTURER   __FF,
DEVICE_TYPE           __AI_BLOCK,
DEVICE_REVISION       __AI_BLOCK_rel_dev_rev,
DD_REVISION           __AI_BLOCK_rel_dd_rev
{
     EVERYTHING;
	 REDEFINITIONS
	{
		BLOCK __analog_input_block
        {

			    LOCAL_PARAMETERS
				{
				ADD	  ARRAY_TYPE1,              array_type1; 
				/*ADD	 ARRAY_TYPE2,              array_type2;*/  
				}	
			REDEFINE UNIT_ITEMS
				{
						__unit_ai_xd,
						__unit_ai_out,
						unit_relation
				}
		}
    } 
} 

WRITE_AS_ONE write_as_one
{
    __tag_desc,
	__alert_key
}

WRITE_AS_ONE write_as_one_1
{
	__alert_key,
	__strategy
}

/* WRITE_AS_ONE write_as_one_2
{
	__test_read_write,
		array_type
} */

WRITE_AS_ONE write_as_one_3
{
	__test_read_write,
	__mode_blk	
}

/* WRITE_AS_ONE write_as_one_4
{
	collection_directory_entry,
	transducer_directory	
} */

EDIT_DISPLAY edit_display_example
{
	LABEL "Edit Display";
	EDIT_ITEMS { __alert_key, __write_pri,write_as_one_1 }
	DISPLAY_ITEMS {  __st_rev}
	PRE_EDIT_ACTIONS { method_edit_display}
	POST_EDIT_ACTIONS { method_edit_display} 
}

EDIT_DISPLAY edit_display_record
{
	LABEL "Edit Display Record";
	EDIT_ITEMS { __mode_blk }
	DISPLAY_ITEMS {  __test_read_write}
}

EDIT_DISPLAY edit_display_record_members
{
	LABEL "Edit Display Record Members";
	EDIT_ITEMS { PARAM.MODE_BLK.ACTUAL }
	DISPLAY_ITEMS {  PARAM.MODE_BLK.TARGET}
}

EDIT_DISPLAY edit_display_wao
{
	LABEL "Edit Display WAO";
	EDIT_ITEMS { write_as_one_3 }
	DISPLAY_ITEMS {  PARAM.MODE_BLK.TARGET}
}

/* EDIT_DISPLAY edit_display_array
{
	LABEL "Edit Display Record";
	EDIT_ITEMS { array_type }
	/* DISPLAY_ITEMS {  transducer_directory} 
} */

/**************************************************************/
#include "SuperDDVariables.dd"
#include "SuperDDMethods.dd"
#include "SuperDDNonWindowsMenus.dd"