#define  alert_key_label_local		"|en|Alert Key"
#define  alert_key_help_local		"|en|Class:Contained , Type: Unsigned Integer , Display format , Edit format , Min value , Max value , Pre edit actions , Constant unit"

#define  st_rev_label_local			"|en|Static Revision"
#define  st_rev_help_local			"|en|Class:Contained , Type:Unsigned Integer , Display format , Edit format , post edit actions , Constant unit , Validity ,write mode"

#define  res_view_1_local			"|en|Resource View 1"
#define  res_view_1_help_local		"|en|Resource View 1 help"

#define  res_view_2_local			"|en|Resource View 2"
#define  res_view_2_help_local		"|en|Resource View 2 help"

#define  test_date_local			"|en|Test Date"
#define  test_date_help_local		"|en|Class:Contained & Dynamic , Type: Date & Time , Constant unit"

#define  test_dll_time_local		"|en|Test Date"
#define  test_dll_time_help_local	"|en|Class:Contained & Dynamic , Type: Time value , Constant unit"

#define  test_time_diff_local		"|en|Test Date"
#define  test_time_diff_help_local	"|en|Class:Contained & Dynamic , Type:Duration , Constant unit"

#define  set_fstate_label_local		"|en|Set Fault State"
#define  set_fstate_help_local		"|en|Set Fault State Help Text"

#define  Resource_Variables_Label		"|en|Resource Variables"


/*Shiv*/
#define msgMethodaborted_m373_LABEL "|en|Method aborted."
#define	ReturningdevicetoAutomoden_m324_LABEL "|en|Returning device to Auto mode\n"
#define	ReturntoAutomode_m323_LABEL "|en|Return to Auto mode"