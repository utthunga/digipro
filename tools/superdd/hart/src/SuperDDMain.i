#line 1 "SuperDDMain.ddl"
    #line 18
MANUFACTURER ROSEMOUNT, DEVICE_TYPE _644_EXP, DEVICE_REVISION 9, DD_REVISION 3


IMPORT STANDARD _TABLES, DEVICE_REVISION 20, DD_REVISION 4
{
    
EVERYTHING;
    REDEFINITIONS
 {
     VARIABLE device_type
     {
      REDEFINE TYPE ENUMERATED (2)
      {
       { 0x2618, "Fluke" }
      }
     }
     VARIABLE manufacturer_id
     {
      REDEFINE TYPE ENUMERATED (2)
      {
       { 0x0026, "SuperDD" }
      }
     }
     VARIABLE private_label_distributor
     {
      REDEFINE TYPE ENUMERATED (2)
      {
       { 0x0026, "SuperDD" }
   }
  }
    }
}



IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 7,  DD_REVISION 2
{
 EVERYTHING;
 REDEFINITIONS
 {
  COMMAND read_additional_device_status
        {
            REDEFINE TRANSACTION 0
            {
                REQUEST
                {}
                REPLY
                {
                    response_code, device_status,
                    device_specific_status_0, device_specific_status_1, device_specific_status_2,
                    device_specific_status_3, device_specific_status_4, device_specific_status_5,
                    
extended_fld_device_status, 0x00, standardized_status_0
                }
            }
            
REDEFINE TRANSACTION 1
            {
                REQUEST
                {
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),
                    
extended_fld_device_status (INFO), 0x00, standardized_status_0 (INFO)
                }
                REPLY
                {
                    response_code, device_status,
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),
                    
extended_fld_device_status (INFO), 0x00, standardized_status_0(INFO)
                }
            }
        }
 }
}


IMPORT STANDARD _PV, DEVICE_REVISION 1, DD_REVISION 1
{
    EVERYTHING;
    REDEFINITIONS
    {
  REDEFINE VARIABLE upperRange_value
  {
   LABEL [pv_urv];
   HELP "Class:Range, Type:Float, Display format , Edit format , Scaling Factor 2, Pre write actions";
   CLASS INPUT;
   TYPE FLOAT
   {
    DISPLAY_FORMAT "3.3f";
    EDIT_FORMAT "5.3f";
    SCALING_FACTOR 2;
   }
                                        



PRE_WRITE_ACTIONS { convert_From_Percent }
  }
    
REDEFINE VARIABLE lowerRange_value
  {
    LABEL [pv_lrv];
    HELP "Class:Range , Type:Float , Display format , Edit format , Pre read actions, Post write actions, Pre write actions";
    CLASS INPUT;
    TYPE FLOAT
    {
     DISPLAY_FORMAT "3.3f";
     EDIT_FORMAT "5.3f";
    }
    
POST_READ_ACTIONS { convert_To_Percent }
    PRE_WRITE_ACTIONS { convert_From_Percent }
    POST_WRITE_ACTIONS { convert_To_Percent }
  }
        ARRAY OF COLLECTION dynamic_variables
        {
            ELEMENTS
            {
                DELETE 1;
                DELETE 2;
                DELETE 3;
            }
        }
     
COLLECTION OF VARIABLE scaling
  {
      MEMBERS
   {
       ADD RANGE_UNITS, deviceVariables[0].DIGITAL_UNITS, [range_units], [range_units_help];
   }
  }
        
REDEFINE UNIT scaling_units_relation
        {
                deviceVariables[0].DIGITAL_UNITS:
                upperRange_value,
                lowerRange_value
        }
        DELETE COLLECTION secondary_variable;
     DELETE COLLECTION tertiary_variable;
        DELETE COLLECTION quaternary_variable;
    }
}
IMPORT STANDARD _COMMON_PRACTICE, DEVICE_REVISION 9, DD_REVISION 2
{
    COMMAND write_pv_range_values;
    REDEFINITIONS
    {
     REDEFINE COMMAND write_pv_range_values
        {
            NUMBER 35;
            OPERATION WRITE;
            TRANSACTION
            {
                REQUEST
                {
                    dynamic_variables[0].RANGING.RANGE_UNITS (INFO), dynamic_variables[0].RANGING.UPPER_RANGE_VALUE, dynamic_variables[0].RANGING.LOWER_RANGE_VALUE
                }
                REPLY
                {
                    response_code, device_status,
                    dynamic_variables[0].RANGING.RANGE_UNITS (INFO), dynamic_variables[0].RANGING.UPPER_RANGE_VALUE, dynamic_variables[0].RANGING.LOWER_RANGE_VALUE
                }
                         
}
            RESPONSE_CODES
            {
                0,  SUCCESS,            [no_command_specific_errors];
                2,  DATA_ENTRY_ERROR,   [invalid_selection];
                5,  DATA_ENTRY_ERROR,   [too_few_data_bytes_recieved];
                6,  MISC_ERROR,         [xmtr_specific_command_error];
                7,  MODE_ERROR,         [in_write_protect_mode];
                8,  DATA_ENTRY_WARNING, [set_to_nearest_possible_value];
                9,  DATA_ENTRY_ERROR,   [lower_range_value_too_high];
                10, DATA_ENTRY_ERROR,   [lower_range_value_too_low];
                11, DATA_ENTRY_ERROR,   [upper_range_value_too_high];
                12, DATA_ENTRY_ERROR,   [upper_range_value_too_low];
                13, DATA_ENTRY_ERROR,   [URV_and_LRV_out_of_limits];
                14, DATA_ENTRY_WARNING, [span_too_small];
                16, MODE_ERROR,         [access_restricted];
                18, DATA_ENTRY_ERROR,   [invalid_units];
                29, DATA_ENTRY_ERROR,   [invalid_span];
                32, MODE_ERROR,         [busy];
            }
           
}
    }
}
#line 1 "SuperDDVariables.dd"
ARRAY OF COLLECTION deviceVariables
{
    ELEMENTS
    {
        0, pressure;
    }
}

COLLECTION OF VARIABLE pressure
{
    LABEL "Example2";
    MEMBERS
    {
    DIGITAL_UNITS, pres_digital_units, [units];
    DAMPING_VALUE, pressure_damp, "";
 SENSOR_SERIAL_NUMBER, serial_number;
 DIGITAL_VALUE, pres_digital_value, "";
    UPPER_SENSOR_LIMIT, pres_upper_sensor_limit, "";
    LOWER_SENSOR_LIMIT, pres_lower_sensor_limit, "";
    MINIMUM_SPAN, pres_minimum_span, "";
    CLASSIFICATION, pressureClassificationCode, "";
    DEVICE_FAMILY, pressureFamily, "";
    DATA_QUALITY, pressurePDQ, "";
    LIMIT_STATUS, pressureLS, "";
    DEVICE_FAMILY_STATUS, pressureStatus,"";
   }
   VALIDITY TRUE;
}

VARIABLE pres_digital_units
{
    CLASS INPUT;
    LABEL "Units";
    TYPE ENUMERATED
    {
    { 7,   [bar],       [bars_help]}
    }
    HANDLING READ&WRITE;
}
  
VARIABLE pressure_damp
{
    LABEL "Pressure Damp";
    CLASS INPUT;
    CONSTANT_UNIT "Sec";
    TYPE FLOAT
    {
        DISPLAY_FORMAT "4.2f";
        EDIT_FORMAT "4.2f";
        MIN_VALUE 0;
        MAX_VALUE 60;
    }
    HANDLING READ&WRITE;
}

VARIABLE pres_digital_value
{
    CLASS DYNAMIC & CORRECTION;
    LABEL [pressure_family];
 HELP "Class:Dynamic & Correction , Type:Float ,Display format , Pre read actions , Post userchange actions";
 TYPE FLOAT
    {
        DISPLAY_FORMAT "4.2f";
    }
    HANDLING READ;
 
PRE_READ_ACTIONS { convert_From_Percent}
}

VARIABLE pres_upper_sensor_limit
{
    CLASS CORRECTION;
    LABEL "";
    TYPE FLOAT
    {
        DISPLAY_FORMAT "4.2f";
    }
    HANDLING READ;
}

VARIABLE pres_lower_sensor_limit
{
    CLASS CORRECTION;
    LABEL "Lower Sendor";
    TYPE FLOAT
    {
        DISPLAY_FORMAT "4.2f";
    }
    HANDLING READ;
}

VARIABLE pres_minimum_span
{
    CLASS CORRECTION;
    LABEL "Minimum Span";
    TYPE FLOAT
    {
        DISPLAY_FORMAT "4.2f";
    }
    HANDLING READ;
}

VARIABLE pressureUpdatePeriod
{
    LABEL "Type:Time_value,Edit_format,Time_scale,Time_format";
    HANDLING READ;
    TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "0X10";
  EDIT_FORMAT "0X10";
    
TIME_SCALE SECONDS;
 }
}

VARIABLE pressureClassificationCode
{
    LABEL "";
    HANDLING READ;
    TYPE ENUMERATED
    {
       { 65, [pressure_classification] }
    }
}

VARIABLE pressureFamily
{
    LABEL "";
    CLASS DEVICE;
    HANDLING READ;
    TYPE ENUMERATED
    {
        { 4,  [temperature_family] }
    }
}

VARIABLE pressurePDQ
{
    LABEL "";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        { 0, [bad] },
        { 1, [poor_accuracy] },
        { 2, [manual_fixed] },
        { 3, [good] }
    }
}

VARIABLE pressureLS
{
    LABEL "";
 CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        { 0, [not_limited] },
        { 1, [low_limited] },
        { 2, [high_limited] },
        { 3, [constant] }
    }
}

VARIABLE pressureStatus
{
    LABEL "";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE BIT_ENUMERATED
    {
        { 0x08, [more_device_family_status_available] }
    }
}

VARIABLE serial_number
{
    LABEL "";
    CLASS DEVICE;
    #line 183
TYPE UNSIGNED_INTEGER (3)
    {
        DISPLAY_FORMAT "8d";
        EDIT_FORMAT "8d";
    }
    HANDLING READ&WRITE;
}

VARIABLE example_PV_variable
{
    HELP "Example4";
    CLASS INPUT;
    LABEL "Example4";
    TYPE ENUMERATED
    {
    { 7,   "enum2",       "enum2"}
    }
    HANDLING READ&WRITE;
 PRE_READ_ACTIONS { convert_To_Percent}
}

VARIABLE variable_example1
{
 LABEL "Temperature";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE1 10;
  MAX_VALUE1 50;
  MIN_VALUE2 70;
  MAX_VALUE2 100;
      
}
 DEFAULT_VALUE 14.56;
 POST_EDIT_ACTIONS { method_post_edit_action_edit_disp_var }
 PRE_EDIT_ACTIONS { method_pre_edit_action_edit_disp_var }
 VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}

VARIABLE min_example1
{
 LABEL "Var 1";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
    }
}

VARIABLE min_example2
{
 LABEL "Var 2";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE INTEGER (1)
    {
        DISPLAY_FORMAT "2d";
        EDIT_FORMAT "2d";
        MIN_VALUE 100;
        MAX_VALUE 200;
    }
}

VARIABLE min_example3
{
 LABEL "Var 3";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE INTEGER (1)
    {
        DISPLAY_FORMAT "2d";
        EDIT_FORMAT "3d";
    }
}

VARIABLE min_example4
{
 LABEL "Var 4";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE INTEGER (1);
}

VARIABLE min_example5
{
 LABEL "Var 5";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "2.2f";
        EDIT_FORMAT "2.2f";
    }
}


VARIABLE float_var
{
 LABEL "Float";
 HELP "Class:Local , Constant Unit , Default Value, Type:Float, Display Format, Edit Format, Min Value, Max Value";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "3.2f";
        MIN_VALUE1 10;
  MAX_VALUE1 50;
  MIN_VALUE2 70;
  MAX_VALUE2 100;
      
}
 DEFAULT_VALUE 14.56;
}

VARIABLE float_var_defaults
{
 LABEL "Float";
 HELP "Class:Local , Constant Unit, Type:Float";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT;
}



VARIABLE variable_example2
{
 CLASS LOCAL ;
 CONSTANT_UNIT "kelvin";
 HANDLING READ&WRITE;
 HELP "Class:Local ,Constant Unit , Default Value , Validity , Post Read Actions , Type:Double , Display Format , Edit Format , Min Value , Max Value , Scaling Factor";
 LABEL "Temperature Kelvin";
 TYPE DOUBLE
 {
  DISPLAY_FORMAT "4.2f";
        EDIT_FORMAT "4.2f";
        MIN_VALUE 0;
        MAX_VALUE 80;
   
}
 DEFAULT_VALUE 1.8;
 VALIDITY TRUE;
 POST_READ_ACTIONS { convert_To_Percent }
}

VARIABLE variable_example3
{
 CLASS LOCAL;
 HANDLING READ & WRITE;
 HELP "Class:Local , Default Value , Validity , Type:Integer , Display Format , Edit Format , Min Value , Max Value , Scaling Factor, Post Write Actions";
 LABEL "Post write";
 TYPE INTEGER (4)
    {
  DISPLAY_FORMAT "3.1d";
        EDIT_FORMAT "3.2d";
        MIN_VALUE1 -100;
  MAX_VALUE1 -50;
  MIN_VALUE2 50;
  MAX_VALUE2 100;
         
}
 DEFAULT_VALUE 4;
 VALIDITY TRUE;
    POST_WRITE_ACTIONS { convert_To_Percent }
}

VARIABLE integer_var
{
 CLASS LOCAL;
 HANDLING READ & WRITE;
 HELP "Class:Local , Default Value, Type:Integer , Display Format , Edit Format , Min Value , Max Value";
 LABEL "Integer";
    CONSTANT_UNIT "degC";
 TYPE INTEGER (2)
    {
  DISPLAY_FORMAT "4d";
        EDIT_FORMAT "4d";
        MIN_VALUE1 -100;
  MAX_VALUE1 -50;
  MIN_VALUE2 1;
  MAX_VALUE2 50;
         
}
 DEFAULT_VALUE 4;
}

VARIABLE integer_var_defaults
{
 CLASS LOCAL;
 HANDLING READ & WRITE;
 HELP "Class:Local , Type:Integer ";
 LABEL "Integer";
 TYPE INTEGER (2);
}

VARIABLE variable_example4
{
 CLASS LOCAL ;
 HANDLING READ & WRITE;
 HELP "Type:Unsigned Integer , Display Format , Edit Format , Min Value , Max Value , Scaling Factor ,Default Value , Post Edit Actions , Pre read Actions";
 LABEL "Post Edit";
 TYPE UNSIGNED_INTEGER(2)
    {
  DISPLAY_FORMAT "3.1d";
        EDIT_FORMAT "2.2d";
        MIN_VALUE1 5;
  MAX_VALUE1 50;
  MIN_VALUE2 10;
  MAX_VALUE2 100;
      
}
 DEFAULT_VALUE 10;
 PRE_READ_ACTIONS { convert_To_Percent}
 POST_EDIT_ACTIONS {method_post_edit_action_one}
}

VARIABLE unsignedInt_var
{
 CLASS LOCAL ;
 HANDLING READ & WRITE;
 HELP "Class : Local, Type:Unsigned Integer , Display Format , Edit Format , Min Value , Max Value , Scaling Factor ,Default Value , Post Edit Actions , Pre read Actions";
 LABEL "Unsigned Integer";
    CONSTANT_UNIT "degC";
 TYPE UNSIGNED_INTEGER(3)
    {
  DISPLAY_FORMAT "8u";
        EDIT_FORMAT "8u";
        MIN_VALUE1 5;
  MAX_VALUE1 50;
  MIN_VALUE2 80;
  MAX_VALUE2 100;
    }
 DEFAULT_VALUE 10;
}

VARIABLE unsignedInt_var_defaults
{
 CLASS LOCAL ;
 HANDLING READ & WRITE;
 HELP "Class : Local, Type:Unsigned Integer";
 LABEL "Unsigned Integer";
 TYPE UNSIGNED_INTEGER(3);
}


VARIABLE date_var
{
 CLASS LOCAL ;
 HANDLING READ & WRITE;
 HELP "Class : Local , Type : Date , Default Value";
 LABEL "Date";
 TYPE DATE;
    DEFAULT_VALUE (((24<<8)+6)<<8)+93;
}

VARIABLE date_var_defaults
{
 CLASS LOCAL ;
 HANDLING READ & WRITE;
 HELP "Class : Local , Type : Date";
 LABEL "Date";
 TYPE DATE;
}


VARIABLE variable_example6
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value , Display format , Edit_format , Time_scale: Hours , Post userchange actions";
 LABEL "Post Userchange";
 TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "11u";
  EDIT_FORMAT "11u";
  TIME_SCALE HOURS;
 }
    POST_USERCHANGE_ACTIONS { post_userchange_method }
}

VARIABLE time_var_hr
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value , Display format , Edit_format , Time_scale: Hours";
 LABEL "Time In Hours";
 TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "11u";
  EDIT_FORMAT "11u";
  TIME_SCALE HOURS;
        #line 499
}
    DEFAULT_VALUE 32000*60*60;
}

VARIABLE time_var_sec
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value , Display format , Edit_format , Time_scale: Seconds";
 LABEL "Time In Seconds";
 TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "11u";
  EDIT_FORMAT "11u";
  TIME_SCALE SECONDS;
         #line 518
}
    DEFAULT_VALUE 32000;
}

VARIABLE time_var_min
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value , Display format , Edit_format , Time_scale: Minutes";
 LABEL "Time In Minutes";
 TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "11u";
  EDIT_FORMAT "11u";
  TIME_SCALE MINUTES;
         #line 537
}
    DEFAULT_VALUE 32000*60;
}

VARIABLE time_var_time_format
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value , Display format , Edit_format , Time_Format: %H:%M:%S";
 LABEL "Time In Format";
 TYPE TIME_VALUE
 {
        TIME_FORMAT "%H:%M:%S";
 }
    DEFAULT_VALUE 32000*60*60;
}

VARIABLE time_var_defaults
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class: Local , Type:Time_value";
 LABEL "Time";
 TYPE TIME_VALUE;
}

VARIABLE variable_example7
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class:Local , Type:Bit Enumerated ,Size ,Status class , Action , Post Rqstupdate actions";
 LABEL "Bit Enumeration";
 TYPE BIT_ENUMERATED(2)
    {
        { 0x08, "Bit Enumeration", "help" ,LOCAL ,ERROR,action_method}
    }
   POST_RQSTUPDATE_ACTIONS{ post_rqstupdate_method}
}

VARIABLE enum_var
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Enumerated, Size";
 LABEL "Enumeration";
 TYPE ENUMERATED(2)
   {
        { 0, "good" },
        { 1, "bad" },
        { 2, "moderate" },
        { 3, "emergency" }
    }
}

VARIABLE variable_example9
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class:Local , Type : Double , Refresh actions";
 LABEL "Refresh";
    TYPE DOUBLE
    {
        MIN_VALUE1 10;
  MAX_VALUE1 50;
  MIN_VALUE2 70;
  MAX_VALUE2 100;
    }
    REFRESH_ACTIONS {refresh_method}
 
}

VARIABLE double_var
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class:Local , Type : Double";
 LABEL "Double";
    CONSTANT_UNIT "degC";
    TYPE DOUBLE
    {
        DISPLAY_FORMAT "12.8g";
        EDIT_FORMAT "12.8g";
        
MIN_VALUE1 10;
  MAX_VALUE1 50;
  MIN_VALUE2 70;
  MAX_VALUE2 100;
    }
    
DEFAULT_VALUE 14.56;
}

VARIABLE double_var_defaults
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class:Local , Type : Double";
 LABEL "Double";
    TYPE DOUBLE;
}

VARIABLE variable_example25
{
 CLASS LOCAL ;
 HANDLING READ;
 HELP "Class : Local , Type : Date , Default Value";
 LABEL "Date";
 TYPE DATE;
 DEFAULT_VALUE (((24<<8)+6)<<8)+93;
}

VARIABLE variable_example10
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Ascii , Default Value, Size";
 LABEL "ASCII";
 TYPE ASCII(8);
    DEFAULT_VALUE "AB";
}

VARIABLE ascii_var
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Ascii , Default Value, Size";
 LABEL "ASCII";
 TYPE ASCII(8);
    DEFAULT_VALUE "AB";
}

VARIABLE ascii_var_defaults
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Ascii , Size";
 LABEL "ASCII";
 TYPE ASCII(8);
}


VARIABLE packed_ascii_var
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local, Type : Packed Ascii , Default Value";
 LABEL "Packed ASCII";
 TYPE PACKED_ASCII(8);
     
DEFAULT_VALUE "AB";
}

VARIABLE packed_ascii_var_defaults
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local, Type : Packed Ascii";
 LABEL "Packed ASCII";
 TYPE PACKED_ASCII(8);
    DEFAULT_VALUE "AB";
}

VARIABLE password_var
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Password , Size (8)";
 LABEL "Password";
 TYPE PASSWORD(8);
    DEFAULT_VALUE "AB";
}

VARIABLE password_var_defaults
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Password , Size (8)";
 LABEL "Password";
 TYPE PASSWORD(8);
}

VARIABLE variable_example13
{
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 DEFAULT_VALUE 14.56;
 HANDLING READ&WRITE;
 HELP "Class : Local , Pre Write Actions , Type : Float , Display Format , Edit Format , Min Value , Max Value , Scaling Factor, Validity";
 LABEL "Pre Write";
 PRE_WRITE_ACTIONS { convert_To_Percent }
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE 10;
        MAX_VALUE 50;
      
}
 VALIDITY TRUE;
}

VARIABLE variable_example14
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type:Packed Ascii , Default Value";
 LABEL "Packed Ascii";
 TYPE PACKED_ASCII(8);
    DEFAULT_VALUE "AB";
}

VARIABLE variable_example15
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Type : Time_Value , Time_format , Post Userchange Actions";
 LABEL "Time Format";
 TYPE TIME_VALUE
 {
  TIME_FORMAT "%H,%M,%S";
 }
  POST_USERCHANGE_ACTIONS { post_userchange_method }
}

VARIABLE variable_example16
{
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 HELP "Type : Time_value , Time_scale ,Display Format , Edit Format , Default Value";
 LABEL "Time Scale";
 TYPE TIME_VALUE
 {
  DISPLAY_FORMAT "5.3f" ;
        EDIT_FORMAT "5.3f";
  TIME_SCALE HOURS;
   #line 774
}
    DEFAULT_VALUE ((5*60+14)*60+26)*32000 ;
}

VARIABLE variable_example17
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Bit Enumerated ,Status class , Action ,Post Rqstupdate actions";
 LABEL "Bit Enumeration";
 TYPE BIT_ENUMERATED(2)
    {
        { 0x08, device_family_status(0x08), "help" ,LOCAL ,ERROR,action_method}
  { 0X01, "enumeration", "help" }
    }
    POST_RQSTUPDATE_ACTIONS{ post_rqstupdate_method}
}


VARIABLE bit_enum_var
{
 CLASS LOCAL;
 HANDLING READ&WRITE;
 HELP "Class : Local , Type : Bit Enumerated ,Status class";
 LABEL "Bit Enumeration";
 TYPE BIT_ENUMERATED(2)
    {
                
{0x01, "Bit Index 1"},
        {0x02, "Bit Index 2"},
        {0x04, "Bit Index 3"},
        {0x08, "Bit Index 4"},
        {0x10, "Bit Index 5"},
        {0x20, "Bit Index 6"},
        {0x40, "Bit Index 7"},
        {0x80, "Bit Index 8"}
        
}
}

ARRAY OF VARIABLE variable_example18
{
 LABEL "Array of variable";
 ELEMENTS
 {
  0, variable_example1;
  1, variable_example2;
  2, variable_example3;
  3, variable_example4;
 }
  
}

ARRAY variable_example19
{
    LABEL "Value Array";
    HELP "Value Array  ,Number of elements , Validity";
    TYPE variable_example20;
    NUMBER_OF_ELEMENTS 4;
  
VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}

COLLECTION OF COLLECTION sub_device
{
    LABEL "Collection of Collection";
    HELP "Help: Collection of Collection";
    
MEMBERS
    {
        IDENT,           subdev_identity                    ;
        STATS,           subdev_stats                       ;
    }
}

COLLECTION OF VARIABLE subdev_identity
{
 LABEL "Collection of Variable 2";
    HELP "Help: Collection of Variable 2";
    MEMBERS
    {
       VAR_1, variable_example21;
       VAR_2, variable_example22;
       VAR_3, variable_example23;
       VAR_4, variable_example24;
    }
}
COLLECTION OF VARIABLE subdev_stats
{
 LABEL "Collection of Variable 1";
    HELP "Help: Collection of Variable 1";
    MEMBERS
    {
       VAR_1, variable_example1;
       VAR_2, variable_example2;
       VAR_3, variable_example3;
       VAR_4, variable_example4;
    }
  
VALIDITY FALSE;
}

COLLECTION  variable_example20
{
 LABEL "Collection";
 HELP "Help: Collection of Different Types";
 MEMBERS
    {
       VAR_1, variable_example21;
       MEN_1, menu_1;
       REF_ARRAY_1, dynamic_variables[0].RANGING.UPPER_RANGE_VALUE;
       EDIT_DISP_1, edit_display_example1;
       ICON_1, icon_Example1;
       METH_1, must_send_units;
       LIST_COL_1, list_examples;
    }
 VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}

COLLECTION OF MENU menu_collection
{
 LABEL "Collection of Menu";
 HELP "Help: Collection of Menu";
 MEMBERS
    {
       MEN_1, menu_1;
       MEN_2, test_variables;
    }
}

COLLECTION ref_array_collection
{
 LABEL "Collection of Ref Array";
 HELP "Help: Collection of Ref Array";
 MEMBERS
    {
       REF_ARRAY_1, dynamic_variables[0].RANGING.UPPER_RANGE_VALUE;
       REF_ARRAY_2, dynamic_variables[0].RANGING.LOWER_RANGE_VALUE;
    }
}

COLLECTION OF EDIT_DISPLAY edit_disp_collection
{
 LABEL "Collection of ED";
 HELP "Help: Collection of ED";
 MEMBERS
    {
       EDIT_DISP_1, edit_display_example1;
       EDIT_DISP_2, edit_display_example2;
    }
}

COLLECTION image_collection
{
 LABEL "Collection of Images";
 HELP "Help: Collection of Images";
 MEMBERS
    {
       ICON_1, icon_Example1;
       ICON_2, icon_Example2;
    }
}

COLLECTION OF METHOD method_collection
{
 LABEL "Collection of Methods";
 HELP "Help: Collection of Methods";
 MEMBERS
    {
       METH_1, must_send_units;
       METH_2, sample_method;
    }
}

COLLECTION OF LIST list_collection
{
 LABEL "Collection of List";
 HELP "Help: Collection of List";
 MEMBERS
    {
       LIST_COL_1, list_examples;
    }
}

VARIABLE variable_example21
{
 LABEL "varaiable 1";
 HELP "Type : Float , Default Value";
 CLASS LOCAL;
 TYPE FLOAT;
 DEFAULT_VALUE 21;
}

VARIABLE validity_var
{
 LABEL "Validity Variable";
 HELP "Type : INTEGER , Default Value";
 CLASS LOCAL;
    TYPE ENUMERATED
    {
        {0, "ON"},
        {1,  "OFF"}
    }
    HANDLING READ&WRITE;
}

VARIABLE variable_example22
{
 LABEL "varaiable 2";
 HELP "Type : Float , Default Value";
 CLASS LOCAL;
 TYPE FLOAT;
 DEFAULT_VALUE 22;
}

VARIABLE variable_example23
{
 LABEL "varaiable 3";
 HELP "Type : Float , Default Value";
 CLASS LOCAL;
 TYPE FLOAT;
 DEFAULT_VALUE 23;
}

VARIABLE variable_example24
{
 LABEL "varaiable 4";
 HELP "Type : Float , Default Value";
 CLASS LOCAL;
 TYPE FLOAT;
 DEFAULT_VALUE 24;
}

VARIABLE digital_units
{
    LABEL "Digital Units";
    HELP "Digital Units";
    CLASS LOCAL;
    TYPE ENUMERATED
    {
        { 26, units_code(26) },
        { 15, units_code(15) },
        { 130, units_code(130) },
        { 27, units_code(27) },
        { 28, units_code(28) },
        { 131, units_code(131) },
        { 19, units_code(19) },
        { 29, units_code(29) },
        { 22, units_code(22) },
        { 16, units_code(16) },
        { 136, units_code(136) },
        { 235, units_code(235) },
        { 23, units_code(23) },
        { 24, units_code(24) },
        { 17, units_code(17) },
        { 138, units_code(138) },
        { 25, units_code(25) },
        { 137, units_code(137) },
        { 18, units_code(18) },
        { 30, units_code(30) },
        { 31, units_code(31) },
        { 132, units_code(132) },
        { 133, units_code(133) },
        { 134, units_code(134) },
        { 135, units_code(135) }
    }
    HANDLING READ&WRITE;
    POST_EDIT_ACTIONS { must_send_units }
}

LIST list_examples
{
    LABEL "List";
    HELP "Capacity, Type , Validity ,Count";
    CAPACITY 10;
    COUNT 2;
    VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
    TYPE sub_device;
 
}

VARIABLE waveform_number_0
{
    LABEL "WF0";
    HELP  "WF0";
    CLASS LOCAL;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_0
{
    LABEL " ";
    HELP  " ";
    CLASS LOCAL;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_0
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_0;
        LPO_NORM,    waveform_lpo_norm_0;
    }
}

VARIABLE waveform_number_1
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_1
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_1
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_1;
        LPO_NORM, waveform_lpo_norm_1;
    }
}

VARIABLE waveform_number_2
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_2
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_2
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_2;
        LPO_NORM,    waveform_lpo_norm_2;
    }
}

VARIABLE waveform_number_3
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_3
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_3
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_3;
        LPO_NORM, waveform_lpo_norm_3;
    }
}

VARIABLE waveform_number_4
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_4
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_4
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_4;
        LPO_NORM,    waveform_lpo_norm_4;
    }
}

VARIABLE waveform_number_5
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_5
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_5
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_5;
        LPO_NORM,    waveform_lpo_norm_5;
    }
}

VARIABLE waveform_number_6
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_6
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_6
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_6;
        LPO_NORM,    waveform_lpo_norm_6;
    }
}

VARIABLE waveform_number_7
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_7
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_7
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_7;
        LPO_NORM,    waveform_lpo_norm_7;
    }
}

VARIABLE waveform_number_8
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_8
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_8
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_8;
        LPO_NORM, waveform_lpo_norm_8;
    }
}

VARIABLE waveform_number_9
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_9
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_9
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_9;
        LPO_NORM,    waveform_lpo_norm_9;
    }
}

VARIABLE waveform_number_10
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_10
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_10
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_10;
        LPO_NORM,    waveform_lpo_norm_10;
    }
}

VARIABLE waveform_number_11
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_11
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_11
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_11;
        LPO_NORM, waveform_lpo_norm_11;
    }
}

VARIABLE waveform_number_12
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_12
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_12
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_12;
        LPO_NORM,    waveform_lpo_norm_12;
    }
}

VARIABLE waveform_number_13
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_13
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_13
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_13;
        LPO_NORM, waveform_lpo_norm_13;
    }
}

VARIABLE waveform_number_14
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_14
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_14
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_14;
        LPO_NORM,   waveform_lpo_norm_14;
    }
}

VARIABLE waveform_number_15
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_15
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_15
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_15;
        LPO_NORM,    waveform_lpo_norm_15;
    }
}

VARIABLE waveform_number_16
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_16
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_16
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_16;
        LPO_NORM,    waveform_lpo_norm_16;
    }
}

VARIABLE waveform_number_17
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_17
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_17
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_17;
        LPO_NORM,    waveform_lpo_norm_17;
    }
}

VARIABLE waveform_number_18
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE waveform_lpo_norm_18
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE FLOAT { DISPLAY_FORMAT "4.2f"; }
    CONSTANT_UNIT [Percent];
    HANDLING READ;
}

COLLECTION OF VARIABLE FCF_Result_18
{
    LABEL "|en| ";
    MEMBERS
    {
        RUN_NUMBER,  waveform_number_18;
        LPO_NORM,    waveform_lpo_norm_18;
    }
}

VARIABLE waveform_number_19
{
    LABEL " ";
    HELP  " ";
    CLASS CORRECTION;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ;
}

VARIABLE const_value
{
    LABEL "constant value";
    HANDLING READ & WRITE;
    TYPE INTEGER;
    DEFAULT_VALUE 10;
}

VARIABLE display_value_qual
{
 LABEL "Display Value";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value , Scaling factor";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE 10;
  MAX_VALUE 50;
             
}
 DEFAULT_VALUE 14.56;
 VALIDITY TRUE;
}

VARIABLE read_only_qual
{
 LABEL "Read Only";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE 10;
  MAX_VALUE 50;
    }
 DEFAULT_VALUE 14.56;
 VALIDITY TRUE;
}

VARIABLE no_unit_qual
{
 LABEL "No Unit";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE 10;
  MAX_VALUE 50;
    }
 DEFAULT_VALUE 14.56;
 VALIDITY TRUE;
}

VARIABLE no_label_qual
{
 LABEL "No Label";
 HELP "Class:Local , Constant Unit , Default Value , Validity , Post Edit Actions , Pre Edit Actions , Type:Float , Display Format , Edit Format , Min Value  ,Max Value";
 CLASS LOCAL ;
 CONSTANT_UNIT "degC";
 HANDLING READ&WRITE;
 TYPE FLOAT
    {
        DISPLAY_FORMAT "3.2f";
        EDIT_FORMAT "5.2f";
        MIN_VALUE 10;
  MAX_VALUE 50;
    }
 DEFAULT_VALUE 14.56;
 VALIDITY TRUE;
}

VARIABLE pre_edit_action_single_method
{
 LABEL "Pre Edit Single";
 HELP "Class:Local , Pre Edit Action Single Action only";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    PRE_EDIT_ACTIONS { method_pre_edit_action_one}
}

VARIABLE post_edit_action_single_method
{
 LABEL "Post Edit Single";
 HELP "Class:Local , Post Edit Action Single Action only";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    POST_EDIT_ACTIONS { method_post_edit_action_one}
}

VARIABLE pre_post_edit_action_single_method
{
 LABEL "Pre Post Edit Single";
 HELP "Class:Local , Pre and Post Edit Action Single Action only";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    PRE_EDIT_ACTIONS { method_pre_edit_action_one}
    POST_EDIT_ACTIONS { method_post_edit_action_one}
}

VARIABLE pre_edit_action_multiple_method
{
 LABEL "Pre Edit Multiple";
 HELP "Class:Local , Pre Edit Multiple Actions";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    PRE_EDIT_ACTIONS { method_pre_edit_action_one, method_pre_edit_action_two}
   
}

VARIABLE post_edit_action_multiple_method
{
 LABEL "Post Edit Multiple";
 HELP "Class:Local , Post Edit Multiple Actions";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    POST_EDIT_ACTIONS {method_post_edit_action_one, method_post_edit_action_two}
}

VARIABLE pre_post_edit_action_multiple_method
{
 LABEL "Pre Post Edit Multiple";
 HELP "Class:Local , Pre and Post Edit Multiple Actions";
    CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE FLOAT;
 DEFAULT_VALUE 1.2;
    PRE_EDIT_ACTIONS { method_pre_edit_action_one, method_pre_edit_action_two}
    POST_EDIT_ACTIONS {method_post_edit_action_one, method_post_edit_action_two}
}


VARIABLE int_var_size_1
{
 LABEL "Int Size 1";
 HELP "Class:Local , Size 1";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (1)
    {
        DISPLAY_FORMAT "2d";
        EDIT_FORMAT "3d";
    }
}

VARIABLE int_var_size_2
{
 LABEL "Integer Size 2";
 HELP "Class:Local, Size 2";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
    TYPE INTEGER (2);
}

VARIABLE int_var_size_3
{
 LABEL "Integer Size 3";
 HELP "Class:Local, Size 3";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (3);
}

VARIABLE int_var_size_4
{
 LABEL "Integer Size 4";
 HELP "Class:Local, Size 4";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (4);
}

VARIABLE int_var_size_5
{
 LABEL "Integer Size 5";
 HELP "Class:Local, Size 5";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (5);
}

VARIABLE int_var_size_6
{
 LABEL "Integer Size 6";
 HELP "Class:Local, Size 6";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (6);
}

VARIABLE int_var_size_7
{
 LABEL "Integer Size 7";
 HELP "Class:Local, Size 7";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (7);
}

VARIABLE int_var_size_8
{
 LABEL "Integer Size 8";
 HELP "Class:Local, Size 8";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE INTEGER (8);
}

VARIABLE uint_var_size_1
{
 LABEL "UInt Size 1";
 HELP "Class:Local, Size 1";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (1);
}

VARIABLE uint_var_size_2
{
 LABEL "UInt Size 2";
 HELP "Class:Local, Size 2";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (2);
}

VARIABLE uint_var_size_3
{
 LABEL "UInt Size 3";
 HELP "Class:Local, Size 3";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (3);
}

VARIABLE uint_var_size_4
{
 LABEL "UInt Size 4";
 HELP "Class:Local, Size 4";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (4);
}


VARIABLE uint_var_size_5
{
 LABEL "UInt Size 5";
 HELP "Class:Local, Size 5";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (5);
}

VARIABLE uint_var_size_6
{
 LABEL "UInt Size 6";
 HELP "Class:Local, Size 6";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (6);
}

VARIABLE uint_var_size_7
{
 LABEL "UInt Size 7";
 HELP "Class:Local, Size 7";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (7);
}

VARIABLE uint_var_size_8
{
 LABEL "UInt Size 8";
 HELP "Class:Local, Size 8";
 CLASS LOCAL ;
 HANDLING READ&WRITE;
 TYPE UNSIGNED_INTEGER (8);
}
#line 1 "SuperDDMethods.dd"
METHOD must_send_units
{
 LABEL "Send Units";
 HELP "Class:Device , Validity";
    CLASS DEVICE;
 DEFINITION
 {
  char status[3];
  char disp_string[160];
  int slen;
        long select;
  slen = 160;
             
select = 0;
        
ACKNOWLEDGE("NOTE: Variables which use this as the units code will be in the previous units until this value is sent to the device.");
        
_MenuDisplay (METHODID(test_menu_coll),("Next"),"select");
 }
    VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}


METHOD post_rqstupdate_method
{
    LABEL "Post Rqstupdate";
 DEFINITION
 {
      ACKNOWLEDGE("Post-rqstupdate method is being executed");
    }
}

METHOD post_userchange_method
{
    LABEL "Post Userchange";
 DEFINITION
 {
      ACKNOWLEDGE("Post-userchange method is being executed");
    }
}

METHOD pre_write_method
{
    LABEL "Sample Method 2";
 HELP "Class: Device , Validity";
 DEFINITION
 {
      ACKNOWLEDGE("Pre-Write method is being executed");
    }
}

METHOD refresh_method
{
    LABEL "Sample Method 2";
 HELP "Class: Device , Validity";
 DEFINITION
 {
      ACKNOWLEDGE("Refresh method is being executed");
    }
 VALIDITY TRUE;
}

METHOD sample_method
{
 LABEL "Sample Method 1";
 HELP "Class : Device , Validity";
 CLASS DEVICE;
 DEFINITION
 {
  char status[3];
  char disp_string[160];
  int slen;
  
slen = 160;
  
ACKNOWLEDGE("The current method is executed!!");
  
_set_xmtr_device_status(0xFF,0);
  _set_xmtr_all_resp_code(0);
  _set_device_status(0xFF,0);
  _set_all_resp_code(0);
 }
 VALIDITY TRUE;
}

METHOD convert_To_Percent
{
    LABEL "|en| ";
 DEFINITION
 {
       float x, y;
       y = fgetval();
       x = y * 2;
       fsetval(x);
    }
}

METHOD convert_From_Percent
{
    LABEL "|en| ";
 DEFINITION
 {
       float x, y;
       x = fgetval();
       y = x / 2;
       fsetval(y);
    }
}

METHOD method_assign_urv
{
    LABEL "|en| ";
 DEFINITION
 {
       fsetval(upperRange_value, 100.0);
    }
}

METHOD action_method
{
    LABEL "Action Method";
 DEFINITION
 {
      ACKNOWLEDGE("Action method is being executed");
    }
}

METHOD menu_action
{
 LABEL "Menu Action";
 DEFINITION
 {
  float x, y;
 }
}

METHOD multiple_abort_methods
{
 LABEL "Multiple Abort Methods";
 DEFINITION
 {
        SELECT_FROM_LIST("Selection Options", "Option 1; Option 2");
        
_add_abort_method(METHODID(method_abort_1));
        _add_abort_method(METHODID(method_abort_2));
        _add_abort_method(METHODID(method_abort_3));
        
if (_ivar_value(METHODID(integer_var)) <= 51)
        {
           process_abort();
        }
        else
        {
            ACKNOWLEDGE("Press 'Cancel' to execute the Abort methods");
        }
          
}
}

METHOD method_abort_1
{
 LABEL "Abort Method 1";
 DEFINITION
 {
       ACKNOWLEDGE("Abort method 1 is executing.");
    }
}

METHOD method_abort_2
{
 LABEL "Abort Method 2";
 DEFINITION
 {
       int choice;
       
ACKNOWLEDGE("Abort method 2 is executing.");
          
choice = SELECT_FROM_LIST("Selection Options", "Local Variable; Device Variable");
       
if (choice == 0)
       {
           _get_dev_var_value(("Enter local variable value."),0,METHODID(integer_var));
       #line 185
}
       else
       {
           _get_dev_var_value(("Enter lower range value:"),0,METHODID(lowerRange_value));
       }
       
ACKNOWLEDGE("Abort method 2 is executed successfully.");
    }
}

METHOD method_abort_3
{
 LABEL "Abort Method 3";
 DEFINITION
 {
       ACKNOWLEDGE("Abort method 3 is executing.");
    }
}

METHOD method_and_edit_actions
{
 LABEL "Actions in method.";
 DEFINITION
 {
       char status[3];
            
_get_dev_var_value(("Enter upper range value:"),0,METHODID(upperRange_value));
       _get_dev_var_value(("Enter lower range value:"),0,METHODID(lowerRange_value));
       
ACKNOWLEDGE("Method is executed successfully.");
    }
}

METHOD method_pre_edit_action_one
{
 LABEL "Pre Edit Action1";
 DEFINITION
 {
       int choice;
       _add_abort_method(METHODID(method_abort_1));
       
ACKNOWLEDGE("Pre Edit Action 1 method begins.");
       
_get_dev_var_value(("Enter lower range value:"),0,METHODID(lowerRange_value));
       
choice = SELECT_FROM_LIST("Select the option below:", "Continue; Abort(Single Method); Abort(Multiple Methods)");
       
if (choice == 1)
       {
         process_abort();
       }
       
if (choice == 2)
       {
           _add_abort_method(METHODID(method_abort_2));
           process_abort();
       }
            
ACKNOWLEDGE("Pre Edit Action 1 method executed successfully.");
    }
}

METHOD method_post_edit_action_one
{
 LABEL "Post Edit Action 1";
 DEFINITION
 {
         int choice;
       _add_abort_method(METHODID(method_abort_1));
       ACKNOWLEDGE("Post Edit Action 1 method begins.");
            
_get_dev_var_value(("Enter lower range value:"),0,METHODID(lowerRange_value));
       
choice = SELECT_FROM_LIST("Selection Options", "Continue; Abort(Single Method); Abort(Multiple Methods)");
       
if (choice == 1)
       {
         process_abort();
       }
       
if (choice == 2)
       {
           _add_abort_method(METHODID(method_abort_2));
           process_abort();
       }
       
ACKNOWLEDGE("Post Edit Action 1 method executed successfully.");
    }
}

METHOD method_pre_edit_action_two
{
 LABEL "Pre Edit Action 2";
 DEFINITION
 {
       ACKNOWLEDGE("Pre Edit Action 2 method begins.");
       _get_dev_var_value(("Enter local variable value."),0,METHODID(integer_var));
       #line 283
ACKNOWLEDGE("Pre Edit Action 2 method executed successfully.");
    }
}

METHOD method_post_edit_action_two
{
 LABEL "Post Edit Action 2";
 DEFINITION
 {
       ACKNOWLEDGE("Post Edit Action 2 method begins.");
       _get_dev_var_value(("Enter local variable value."),0,METHODID(integer_var));
       #line 295
ACKNOWLEDGE("Post Edit Action 2 method executed successfully.");
    }
}

METHOD method_test_date
{
 LABEL "Method Test Date";
 DEFINITION
 {
       ACKNOWLEDGE("Test Date Type method begins.");
       _get_dev_var_value(("Enter Date value:"),0,METHODID(date_var));
       #line 307
ACKNOWLEDGE("Test Date Type method executed successfully.");
    }
}

METHOD method_test_time
{
 LABEL "Method Test Time_Value";
 DEFINITION
 {
       ACKNOWLEDGE("Test Time Value Type method begins.");
       _get_dev_var_value(("Enter Time In Hours:"),0,METHODID(time_var_hr));
       #line 319
_get_dev_var_value(("Enter Time In Seconds:"),0,METHODID(time_var_sec));
       #line 321
_get_dev_var_value(("Enter Time In Minutes:"),0,METHODID(time_var_min));
       #line 323
_get_dev_var_value(("Enter Time value:"),0,METHODID(time_var_time_format));
       #line 325
ACKNOWLEDGE("Test Time Value Type method executed successfully.");
    }
}

METHOD method_pre_edit_action_edit_disp_var
{
 LABEL "Pre Edit Action";
 DEFINITION
 {
       ACKNOWLEDGE("Pre Edit Action for variable executing.");
        
}
}

METHOD method_post_edit_action_edit_disp_var
{
 LABEL "Post Edit Action";
 DEFINITION
 {
       ACKNOWLEDGE("Post Edit Action for variable executing.");
    }
}
#line 1 "SuperDDNonWindowsMenus.dd"
MENU root_menu
{
 LABEL "Root Menu";
 ITEMS
 {
  menu_1,
  menu_2,
        menu_3
 }
}

MENU menu_1
{
    LABEL "Variables";
 HELP  "Variables Help Text";
    ITEMS
    {
        menu_var1,
        test_collection_Items,
        test_edit_Display
                


}
}

MENU test_menu_Items
{
   LABEL "Test Menu Items";
   HELP  "Test Menu Items: Menu Label, Menu Help, Menu Items";
   ITEMS
   {
       variable_example20,
       edit_display_example1,
       icon_Example2,
       list_examples[1],
       menu_var1,
       must_send_units,
       deviceVariables[0].DIGITAL_VALUE
    variable_example19[1],
       variable_example1
       gauge_example,
       graph_example,
       trend_example
   }
}

MENU test_collection_Items
{
    LABEL "Test Collection Items";
   HELP  "Test Collection Items: Menu Label, Menu Help, Menu Items";
   ITEMS
   {
       variable_example20,
       subdev_identity,
       menu_collection,
       ref_array_collection,
       sub_device,
       edit_disp_collection,
       image_collection,
       method_collection,
       list_collection
   }
}

MENU test_variables
{
   LABEL "Test Variables";
   HELP  "Test Variables : INTEGER, UNSIGNED INTEGER, FLOAT, DOUBLE, ASCII,"
         "PACKED ASCII, PASSWORD, ENUMERATED, BIT_ENUMERATED, DATE, TIME_VALUE";
   ITEMS
   {
       integer_var,
       unsignedInt_var,
       float_var,
       double_var,
       ascii_var,
       packed_ascii_var,
       password_var,
    enum_var,
       bit_enum_var,
       date_var,
       time_var_hr
   }
}

MENU test_time_variables
{
   LABEL "Test Time Variables";
   HELP  "Test Variables : INTEGER, UNSIGNED INTEGER, FLOAT, DOUBLE, ASCII,"
         "PACKED ASCII, PASSWORD, ENUMERATED, BIT_ENUMERATED, DATE, TIME_VALUE";
   ITEMS
   {
      time_var_hr,
      time_var_sec,
      time_var_min,
      time_var_time_format,
      time_var_defaults,
      variable_example17[0x01]
   }
}

MENU test_variables_defaults
{
   LABEL "Test Variables Defaults";
   HELP  "Test Variables Defaults: Default MIN, MAX, DISPLAY_FORMAT, EDIT_FORMAT, and Value";
   ITEMS
   {
       integer_var_defaults,
       unsignedInt_var_defaults,
       float_var_defaults,
       double_var_defaults,
       ascii_var_defaults,
       packed_ascii_var_defaults,
       password_var_defaults,
       date_var_defaults,
       time_var_defaults
   }
}

MENU test_menu_qualifiers
{
   LABEL "Test Menu Qualifiers";
   HELP  "Menu Qualifiers : DISPLAY_VALUE, READ_ONLY, NO_LABEL, NO_UNIT";
   ITEMS
   {
       display_value_qual(DISPLAY_VALUE),
       read_only_qual(DISPLAY_VALUE, READ_ONLY),
       no_label_qual(NO_LABEL),
       no_unit_qual(NO_UNIT)
   }
}

MENU menu_var1
{
    LABEL "Test Variables";
    ITEMS
    {
        test_variables,
        test_variables_defaults,
        test_time_variables,
        test_edit_read_write_actions,
        test_int_uint_size_attributes
    }
     VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}


MENU test_edit_Display
{
    LABEL "Test Edit Display";
    ITEMS
    {
       edit_display_example2,
       edit_display_example1,
       edit_display_example3
    }
     VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}

MENU test_int_uint_size_attributes
{
    LABEL "Test Size Attributes";
    ITEMS
    {
          int_var_size_1,
          int_var_size_2,
          int_var_size_3,
          int_var_size_4,
          int_var_size_5,
          int_var_size_6,
          int_var_size_7,
          int_var_size_8,
          uint_var_size_1,
          uint_var_size_2,
          uint_var_size_3,
          uint_var_size_4,
          uint_var_size_5,
          uint_var_size_6,
          uint_var_size_7,
          uint_var_size_8
    }

}

MENU test_edit_read_write_actions
{
    LABEL "Variable Actions";
    ITEMS
    {
       pre_edit_action_single_method,
       post_edit_action_single_method
       pre_post_edit_action_single_method,
       pre_edit_action_multiple_method,
       post_edit_action_multiple_method,
       pre_post_edit_action_multiple_method,
       upperRange_value,
       method_and_edit_actions
    }
}

MENU test_min_max
{
    LABEL "Variables Ranges";
    ITEMS
    {
      min_example1,
      min_example2,
      min_example3,
      min_example4,
      min_example5
    }
}

MENU menu_var2
{
    LABEL "Variables Menu 2";
    ITEMS
    {
        variable_example25,
  variable_example9,
  variable_example10,
        password_var,
        variable_example13,
  variable_example14,
  variable_example15,
  variable_example16,
  variable_example17
    }
}

MENU menu_var3
{
    LABEL "Variables Menu 3";
    ITEMS
    {
  deviceVariables[0].DIGITAL_VALUE,
  variable_example18[2],
  variable_example18[1],
  variable_example20.VAR_4,
  list_examples[1],
  sub_device.STATS.VAR_1,
        lock_variable
    }
}

MENU menu_2
{
    LABEL "Menu";
 HELP "Test Menu Attributes : LABEL, HELP, MENU_ITEMS";
    ITEMS
    {
       validity_var,
       test_menu_Items,
       test_menu_qualifiers,
       image_link_method,
       image_link_menu
    }
}

MENU menu_3
{
    LABEL "Methods";
    ITEMS
    {
        sample_method,
  pre_write_method,
  must_send_units,
        multiple_abort_methods,
        method_and_edit_actions,
        method_post_edit_action_two,
        method_test_date,
        method_test_time
    }
}

MENU test_menu_coll
{
    LABEL "Menu Display";
    ITEMS
    {
        variable_example20,
        list_examples[1],
        pre_post_edit_action_single_method
    }
}

EDIT_DISPLAY edit_display_example2
{
 LABEL "Edit Items Wao";
 EDIT_ITEMS {pv_range_wao }
 DISPLAY_ITEMS {variable_example1,variable_example2}
 VALIDITY TRUE;
 
}

EDIT_DISPLAY edit_display_example1
{
 LABEL "Edit Display";
 EDIT_ITEMS { variable_example1,variable_example2 }
 DISPLAY_ITEMS { pres_digital_value, pressure_damp}
 VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
  

}

EDIT_DISPLAY edit_display_example3
{
 LABEL "Edit Actions";
 EDIT_ITEMS { variable_example1,variable_example2 }
 DISPLAY_ITEMS { pres_digital_value, pressure_damp}
 VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
     

PRE_EDIT_ACTIONS { method_pre_edit_action_one, method_pre_edit_action_two}
    POST_EDIT_ACTIONS {method_post_edit_action_one, method_post_edit_action_two}
}
#line 1 "SuperDDEnhancements.dd"
WAVEFORM lpo_waveform
{
    LABEL "Waveform";
    HELP "Waveform";
    Y_AXIS wf_axis;
    EMPHASIS TRUE;
    HANDLING READ&WRITE;
    EXIT_ACTIONS { must_send_units }
    INIT_ACTIONS { must_send_units }
    LINE_COLOR 0xFF0000;
    KEY_POINTS
    {
      X_VALUES { 10.3 }
      Y_VALUES { 3.6 }
    }
    TYPE XY
    {
        X_VALUES {waveform_number_0, waveform_number_1, waveform_number_2, waveform_number_3, waveform_number_4,
                waveform_number_5, waveform_number_6, waveform_number_7, waveform_number_8, waveform_number_9,
                waveform_number_10, waveform_number_11, waveform_number_12, waveform_number_13, waveform_number_14,
                waveform_number_15, waveform_number_16, waveform_number_17, waveform_number_18}
        Y_VALUES {waveform_lpo_norm_0, waveform_lpo_norm_1, waveform_lpo_norm_2, waveform_lpo_norm_3, waveform_lpo_norm_4,
                waveform_lpo_norm_5, waveform_lpo_norm_6, waveform_lpo_norm_7, waveform_lpo_norm_8, waveform_lpo_norm_9,
                waveform_lpo_norm_10, waveform_lpo_norm_11, waveform_lpo_norm_12, waveform_lpo_norm_13, waveform_lpo_norm_14,
                waveform_lpo_norm_15, waveform_lpo_norm_16, waveform_lpo_norm_17, waveform_lpo_norm_18}
    }
    LINE_TYPE DATA;
 
}

AXIS wf_axis
{
    LABEL "%";
    MIN_VALUE 10;
 MAX_VALUE 100;
}

CHART gauge_example
{
    LABEL "Gauge";
    TYPE GAUGE;
 CYCLE_TIME 1000;
 HEIGHT LARGE;
 WIDTH LARGE;
 MEMBERS
    {
  EXT_TEMP, gauge_source;
        MASSBARCOLOR, ext_temp_color_bar;
 }
}

CHART trend_example
{
    LABEL "Trend";
    TYPE STRIP;
 CYCLE_TIME 1000;
 HEIGHT LARGE;
 WIDTH LARGE;
 MEMBERS
    {
  EXT_TEMP, gauge_source;
        MASSBARCOLOR, ext_temp_color_bar;
 }
}

SOURCE gauge_source
{
    LINE_COLOR 0x000000;
    Y_AXIS ext_digital_value;
 MEMBERS
    {
  EXT_DIG_VALUE, const_value;
 }
}

AXIS ext_digital_value
{
    MIN_VALUE 0;
    MAX_VALUE 100;
}

SOURCE ext_temp_color_bar
{
    LABEL   "|en| " ;
    Y_AXIS  ext_digital_value ;
    LINE_COLOR 0xC4C0BC;
    LINE_TYPE HIGH_LIMIT;
    MEMBERS
    {
        EXT_TEMP_VALUE, const_value;
    }
}

GRAPH graph_example
{
    LABEL "Graph";
    MEMBERS
    {
  LPO_STIFF_WAVEFORM, lpo_waveform;
 }
 CYCLE_TIME 1000;
 HEIGHT MEDIUM;
 WIDTH MEDIUM;
}

IMAGE icon_Example1
{
    LABEL "Icon 1";
    PATH "icon_2.png";
 VALIDITY FALSE;
}

IMAGE icon_Example2
{
    LABEL "Icon 2";
 HELP "Label ,Path";
    PATH "icon.png";
 VALIDITY IF (validity_var == 0)
    {TRUE; } ELSE {FALSE;}
}

IMAGE image_link_menu
{
    LABEL "Image with Link Menu";
    PATH "26180902_sensor_open_short.gif";
    LINK method_pre_edit_action_one;
}

IMAGE image_link_method
{
    LABEL "Image with Link Method";
    PATH "26180902_sensor_open_short.gif";
    LINK menu_var2;
}
 #line 3 "SuperDDRelations.dd"
REFRESH pv_refresh_relation
{
    deviceVariables[0].DIGITAL_VALUE:
        dynamic_variables[0].RANGING.RANGE_UNITS,
        dynamic_variables[0].RANGING.UPPER_RANGE_VALUE,
        dynamic_variables[0].RANGING.LOWER_RANGE_VALUE
}
  #line 18
WRITE_AS_ONE pv_range_wao1
{
    dynamic_variables[0].RANGING.UPPER_RANGE_VALUE,
    deviceVariables[0].DIGITAL_VALUE
}

WRITE_AS_ONE pv_range_wao
{
   dynamic_variables[0].RANGING.UPPER_RANGE_VALUE,
   dynamic_variables[0].RANGING.LOWER_RANGE_VALUE
}

WRITE_AS_ONE range_wao
{
   lowerRange_value,
   upperRange_value
}
#line 217 "SuperDDMain.ddl"
lock_variable LIKE VARIABLE write_protect { REDEFINE LABEL "Lock"; }
