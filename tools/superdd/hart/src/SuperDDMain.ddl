﻿/*******************************************************************
**	FILE NAME: SuperDDMain.ddl
**	
**	DESCRIPTION: This file contains all of the import definitions
**                used in the creation of the DD for the Fluke.
**                This is the file to be used as the 
**                argument for the 'tokenize' command since it 
**                includes all other files that it needs.
**              
*******************************************************************/

#include "methods.h"
#include "Macros.h"

#include "SuperDDLabels.dd"


MANUFACTURER ROSEMOUNT, DEVICE_TYPE _644_EXP, DEVICE_REVISION 9, DD_REVISION 3	/*LAYOUT_TYPE COLUMNWIDTH_EQUAL*/	//fm8 and fma is not supporting

/*COMMON TABLES*/
IMPORT STANDARD _TABLES, DEVICE_REVISION 20, DD_REVISION 4
{

    EVERYTHING;
    REDEFINITIONS
	{
    	VARIABLE device_type
    	{
    		REDEFINE TYPE ENUMERATED (2)
    		{
    			{ 0x2618, "Fluke" }
    		}
    	}
    	VARIABLE manufacturer_id
    	{
    		REDEFINE TYPE ENUMERATED (2)
    		{
    			{ 0x0026, "SuperDD" }
    		}
    	}
    	VARIABLE private_label_distributor
    	{
    		REDEFINE TYPE ENUMERATED (2)
    		{
    			{ 0x0026, "SuperDD" }
			}
		}		
    }
}

/*UNIVERSAL*/

IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 7,  DD_REVISION 2
{
	EVERYTHING;
	REDEFINITIONS
	{
		COMMAND read_additional_device_status  /* Cmd 48 */
        {
            REDEFINE TRANSACTION 0
            {
                REQUEST
                {}
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0, device_specific_status_1, device_specific_status_2,
                    device_specific_status_3, device_specific_status_4, device_specific_status_5,

                    extended_fld_device_status, 0x00, standardized_status_0
                }
            }

            REDEFINE TRANSACTION 1
            {
                REQUEST
                {
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),

                    extended_fld_device_status (INFO), 0x00, standardized_status_0 (INFO)
                }
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),

                    extended_fld_device_status (INFO), 0x00, standardized_status_0(INFO)
                }
            }
        }
	}
}

/*PV*/
IMPORT STANDARD _PV, DEVICE_REVISION 1, DD_REVISION 1
{
    EVERYTHING;
    REDEFINITIONS
    {
		REDEFINE VARIABLE upperRange_value
		{
			LABEL [pv_urv];
			HELP "Class:Range, Type:Float, Display format , Edit format , Scaling Factor 2, Pre write actions";
			CLASS RANGE;
			TYPE FLOAT
			{
				DISPLAY_FORMAT "3.3f";
				EDIT_FORMAT "5.3f";
				SCALING_FACTOR 2;
			}
           
           // PRE_EDIT_ACTIONS { method_pre_edit_action_one, method_pre_edit_action_two }
            //POST_EDIT_ACTIONS {method_post_edit_action_one ,method_post_edit_action_two }
			//POST_USERCHANGE_ACTIONS { post_userchange_method }
			PRE_WRITE_ACTIONS { convert_From_Percent }
		}
		
		REDEFINE VARIABLE lowerRange_value
		{
				LABEL [pv_lrv];
				HELP "Class:Range , Type:Float , Display format , Edit format , Pre read actions, Post write actions, Pre write actions";
				CLASS RANGE;
				TYPE FLOAT
				{
					DISPLAY_FORMAT "3.3f";
					EDIT_FORMAT "5.3f";
				}

				POST_READ_ACTIONS { convert_To_Percent }
				PRE_WRITE_ACTIONS { convert_From_Percent }
				POST_WRITE_ACTIONS { convert_To_Percent }
		}
        ARRAY OF COLLECTION dynamic_variables
        {
            ELEMENTS
            {
                DELETE SECONDARY;
                DELETE TERTIARY;
                DELETE QUATERNARY;
            }         
        }

	    COLLECTION OF VARIABLE scaling
		{
		    MEMBERS
			{
			    ADD RANGE_UNITS, deviceVariables[0].DIGITAL_UNITS, [range_units], [range_units_help];
			} 
		}

        REDEFINE UNIT scaling_units_relation
        {
                deviceVariables[0].DIGITAL_UNITS:
                upperRange_value,
                lowerRange_value
        }
        DELETE COLLECTION secondary_variable;
	    DELETE COLLECTION tertiary_variable;
        DELETE COLLECTION quaternary_variable;
    }
}
IMPORT STANDARD _COMMON_PRACTICE, DEVICE_REVISION 9, DD_REVISION 2
{
    COMMAND write_pv_range_values;
    REDEFINITIONS
    {
    	REDEFINE COMMAND write_pv_range_values
        {
            NUMBER 35;
            OPERATION WRITE;
            TRANSACTION 
            {
                REQUEST
                {
                    PV.RANGING.RANGE_UNITS (INFO), PV.RANGING.UPPER_RANGE_VALUE, PV.RANGING.LOWER_RANGE_VALUE
                }
                REPLY
                {
                    response_code, device_status,
                    PV.RANGING.RANGE_UNITS (INFO), PV.RANGING.UPPER_RANGE_VALUE, PV.RANGING.LOWER_RANGE_VALUE
                }
             //   POST_RQSTRECEIVE_ACTIONS { action_method}		//If you add this one "hhd" file is not creating 
            }
            RESPONSE_CODES
            {
                0,  SUCCESS,            [no_command_specific_errors];
                2,  DATA_ENTRY_ERROR,   [invalid_selection];
                5,  DATA_ENTRY_ERROR,   [too_few_data_bytes_recieved];
                6,  MISC_ERROR,         [xmtr_specific_command_error];
                7,  MODE_ERROR,         [in_write_protect_mode];
                8,  DATA_ENTRY_WARNING, [set_to_nearest_possible_value];
                9,  DATA_ENTRY_ERROR,   [lower_range_value_too_high];
                10, DATA_ENTRY_ERROR,   [lower_range_value_too_low];
                11, DATA_ENTRY_ERROR,   [upper_range_value_too_high];
                12, DATA_ENTRY_ERROR,   [upper_range_value_too_low];
                13, DATA_ENTRY_ERROR,   [URV_and_LRV_out_of_limits];
                14, DATA_ENTRY_WARNING, [span_too_small];
                16, MODE_ERROR,         [access_restricted];
                18, DATA_ENTRY_ERROR,   [invalid_units];
                29, DATA_ENTRY_ERROR,   [invalid_span];
                32, MODE_ERROR,         [busy]; 
            }
			
        }
    }
}
#include "SuperDDVariables.dd"
#include "SuperDDMethods.dd"
#include "SuperDDNonWindowsMenus.dd"
#include "SuperDDEnhancements.dd"
#include "SuperDDRelations.dd"



lock_variable LIKE VARIABLE write_protect { REDEFINE LABEL "Lock"; }

