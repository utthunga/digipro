##############################################################################
#
# File: makefile
# Version: 5.0.0
# Date: 08/18/14
#
##############################################################################
# Copyright (c) 2014, HART Communication Foundation, All Rights Reserved
##############################################################################
#
# Description: This is the main makefile included with the DD-IDE.  
#
# It currently contains everything needed to rebuild all the DDs supplied 
# with the DD-IDE. There are 5 targets of key interest in this makefile.
# 
# all:         This is the classic target in many makefiles.  It causes all 
#              files to be updated using the rules in the makefile.
# 
# Simulate:    Starts up the SDC625 in simulation mode using XmtrDD. 
# 
# Execute:     Starts up and looks for a real devices connected to the serial 
#              port.
# 
# Build:       Performs a build -- updates all targets.  
# 
# Tokenize:    Only tokenizes the DD that is currently being viewed in DD 
#              Edit.
# 
# PLEASE DO NOT EDIT THIS FILE AS IT MAY AFFECT PROGRAM OPERATION!!!
#
##############################################################################

##############################################################################
#
#     PLEASE DO NOT EDIT THIS FILE AS IT MAY AFFECT PROGRAM OPERATION!!!
#
##############################################################################

##############################################################################
#
# Basic macros used to simplify our life described as follows
#

RELEASE = $(TOK_RELEASE_DIR)
INCLUDES = methods.h macros.h

include include.mk
include user.mk

TOK_DD = $(DD)$(DD_EXT)

##############################################################################
#
# HCF Tokenizer Options
#

TOK_OPT = $(OPTION) -k 615 -k 672 $(USER_OPTION)

##############################################################################
#
# The default rule for a building a DD.  
#

%$(DD_EXT)::
	$(DDT) $(OPTION) -k 615 -k 672 $<

##############################################################################
#
# The Sample DDs
#

SAMPLE = $(RELEASE)\0000f9
SAMPLE1_1	 = $(SAMPLE)\00f9\0101$(DD_EXT)
SAMPLE1_2	 = $(SAMPLE)\00f9\0201$(DD_EXT)
SAMPLE1_3	 = $(SAMPLE)\f9f9\0301$(DD_EXT)
SAMPLE1_4	 = $(SAMPLE)\f9f9\0401$(DD_EXT)
SAMPLE2_WIRELESS = $(SAMPLE)\f9f8\0101$(DD_EXT)
SAMPLE3_ADAPTER  = $(SAMPLE)\f9f7\0101$(DD_EXT)
SAMPLES = $(SAMPLE1_1) $(SAMPLE1_2) $(SAMPLE1_3) $(SAMPLE1_4) $(SAMPLE2_WIRELESS) $(SAMPLE3_ADAPTER)

##############################################################################
#
# This macro is the compiled list of all the fm6 files we want loaded for use 
# while developing and testing your dd.
#
# Note: The use of the continuation lines using the backslash ( \ )
#
# If you prefer, you can add your rules for making your DD to the end of this
# file.  Use the rules for the sample DDs as a guide for developing you rules.
# Remember to add any include files you write to the dependency list
#

FM =  $(SAMPLES)

############################################################
#
# The rules
#
# make all.  a classic entry point in many makefiles.
#

all:	$(USER_DD)

##############################################################################
#
# These are entry points invoked by the DD-IDE
#
# Starts up the SDC625 in simulation mode using Xmtr_DD.
# Runsim needs your manufacturer, device type, device revision, dd revision
# entered as parameters.  Right now it is setup for samplea.ddl	runsim
# syntax is
#
# runsim mfr dev rev dd_rev
# 

Simulate:	Build
	runsim.bat $(SIM)
	wait 7000
	SDC625 -S -G sdcLog.txt -E sdcErr.txt -O sdcOut.txt

# Starts up and looks for a real devices connected to the serial port

Execute:	Build
	SDC625 -v 0 -G sdcLog.txt -E sdcErr.txt -O sdcOut.txt

# Performs a build -- updates all targets including the DD currently being 
#                     viewed in DD Edit.

Build:	all  $(TOK_DD)

# Only tokenizes the DD currently being viewed in DD Edit.

Tokenize:	$(TOK_DD)

# You may need to add additional -k options here for a smoother build  

$(TOK_DD):	$(TARGET) $(DD_DEPEND)
	$(DDT) $(TOK_OPT) $(TARGET)
#
# End of DD-IDE Entry points.
##############################################################################

# A few other useful targets (entry points)
#
# You can add yourself a batch file to zip up a save away your
# work if you like
#

zip:
	zip.bat 

# This entry point will delete stuff so you can do a clean re-build of the
# whole world.

clean:
	make -f standards.mk clean
	

#
# Makes the standard DDs if necessary.  ***WARNING*** loads all the 
# standard DD sources into development directory, but removes them
# when finished.
#

standards:
	make -f standards.mk all clean

# 
# Forced Rebuild - all the standard DDs will be rebuilt, even if the
# cuurent output file is up to date with the source
#

rebuild_standards:	
	make -B -f standards.mk all clean

#
# Makes the sample DDs 
#

samples:	$(FM)

##############################################################################
#
# The rules for the sample DDs
#
# HART 5 pressure transmitter with temperature compensation
#

$(SAMPLE1_1):	sample1_r1.ddl
	$(DDT) -r $(OPTION) -k 615 -k 672 $<

# HART 6 pressure transmitter with temperature compensation

$(SAMPLE1_2):	sample1_r2.ddl
	$(DDT) -r $(OPTION) -k 615 -k 672 $<

# HART 7 pressure transmitter with temperature compensation

$(SAMPLE1_3):	sample1_r3.ddl
ifeq ($(VER_8_OR_GREATER),true)
# .fm8 is included in the 16 bit (f9f9) directory
	$(DDT) -r $(OPTION) -k 615 -k 672 $<
else
# .fms/.fm6 is included in the 8 bit (00f9) directory
# Note:  A syntax error is created when Version 5 Tokenizer is uesed here
# so a -D is necessary for the Manufacturer even though it is not being changed
	$(DDT) -DHCF=0x0000f9 -D_SAMPLE1_WIRE_EXP=0x00f9 -r $(OPTION) $<
endif

# HART 7 pressure transmitter with temperature compensation

$(SAMPLE1_4):	sample1_r4.ddl
ifeq ($(VER_8_OR_GREATER),true)
# .fm8 is included in the 16 bit (f9f9) directory
	$(DDT) -r $(OPTION) -k 615 -k 672 $<
else
# .fms/.fm6 is included in the 8 bit (00f9) directory
# Note:  A syntax error is created when Version 5 Tokenizer is uesed here
# so a -D is necessary for the Manufacturer even though it is not being changed
	$(DDT) -DHCF=0x0000f9 -D_SAMPLE1_WIRE_EXP=0x00f9 -r $(OPTION) $<
endif

# HART 7 wireless device (generic process)

$(SAMPLE2_WIRELESS):	sample2_wireless.ddl
ifeq ($(VER_8_OR_GREATER),true)
# .fm8 is included in the 16 bit (f9f8) directory
	$(DDT) -r $(OPTION) -k 615 -k 672 $<
else
# .fms/.fm6 is included in the 8 bit (00f9) directory
# Note:  A syntax error is created when Version 5 Tokenizer is uesed here
# so a -D is necessary for the Manufacturer even though it is not being changed
	$(DDT) -DHCF=0x0000f9 -D_SAMPLE2_WIRELESS=0x00f8 -r $(OPTION) $<
endif

# HART 7 wireless adapter

$(SAMPLE3_ADAPTER):	sample3_adapter.ddl
ifeq ($(VER_8_OR_GREATER),true)
# .fm8 is included in the 16 bit (f9f7) directory
	$(DDT) -r $(OPTION) -k 615 -k 672 $<
else
# .fms/.fm6 is included in the 8 bit (00f9) directory
# Note:  A syntax error is created when Version 5 Tokenizer is uesed here
# so a -D is necessary for the Manufacturer even though it is not being changed
	$(DDT) -DHCF=0x0000f9 -D_SAMPLE3_ADAPTER=0x00f7 -r $(OPTION) $<
endif
