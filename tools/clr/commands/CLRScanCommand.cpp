/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRScanCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRScanCommand.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus_defs.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

static int m_commandID = -1;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructor
*/
CLRScanCommand::CLRScanCommand(CLRCommandHandler* pCommandHandler):
    m_testInterface(nullptr),
    m_scanSeqId(-1),
    m_watchDevice(false),
    m_condVarScan(false)
{
    m_commandHandler = pCommandHandler;
}

/*  ----------------------------------------------------------------------------
    Destructor
*/
CLRScanCommand::~CLRScanCommand()
{
    if (m_testInterface)
    {
        delete m_testInterface;
        m_testInterface = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Processses CLR command
*/
int CLRScanCommand::processCommand(const std::string& userCmndStr)
{
    if ((0 == userCmndStr.compare("help")) ||
        (0 == userCmndStr.compare("h")))
        printCommand();
    else
    {
        if ((0 == userCmndStr.compare("list devices")) ||
            (0 == userCmndStr.compare("watch devices")) ||
            (0 == userCmndStr.compare("l d")) ||
            (0 == userCmndStr.compare("w d")))
        {
            if (nullptr == m_testInterface)
                m_testInterface = new ScanTestInterface();

#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
            CommonProtocolManager::getInstance()->startScan("");
#else
            m_commandID = CMD_START_SCAN_ID;
            Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_START_SCAN_ID, "StartScan", "");
            CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, "");
            m_commandHandler->acquireSemaphoreLock();
#endif
            /// Set the scan state as started
            m_commandHandler->setCommandState(SCAN_STARTED);

            LOGF_INFO (CLR, "Scan operation started...")

            if ((0 == userCmndStr.compare("list devices")) ||
                (0 == userCmndStr.compare("l d")))
            {
                m_commandHandler->releaseCommandLock();
                acquireScanLock();
            }
            else
            {   
                /// Watch the live devices in the network
                m_watchDevice = true;
            }        
        }
        else if (0 == userCmndStr.compare("s"))
        {
            /// Stop the scan operation 
#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
            CommonProtocolManager::getInstance()->stopScan();
#else
            m_commandID = CMD_STOP_SCAN_ID;
            Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_STOP_SCAN_ID, "StopScan", "");
            CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, "");
            m_commandHandler->acquireSemaphoreLock();
#endif
            m_commandHandler->setCommandState(SCAN_STOPPED);
            m_watchDevice = false;
        }
        else if ((0 == userCmndStr.compare("back")) ||
                 (0 == userCmndStr.compare("b")))
        {
            /// Go back to the previous state
            m_commandHandler->setCommandState(PROTOCOL_INITIALIZED);
        }
    }
   
    return CLR_SUCCESS;
}

int CLRScanCommand::processAsyncCommandResponse(const Json::Value& jsonData)
{
    CLRCommandLogger::LOG_COMMAND(m_commandID, RESPONSE, jsonData);
    m_commandHandler->releaseSemaphoreLock();
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
int CLRScanCommand::processCommandResponse(const REQUEST_TYPE& requestType, const Json::Value& jsonData)
{
     CLRCommandLogger::LOG_COMMAND(m_commandID, NOTIFY, jsonData);

    if (TESTINTF_SUCCESS != m_testInterface->parseResponseData (requestType, jsonData))
    {
        cout << "Scan operation failed, error code: " << m_testInterface->getErrorCode() << ", error response: " << m_testInterface->getErrorMessage() << endl;

#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
        CommonProtocolManager::getInstance()->stopScan();
#else
        m_commandID = CMD_STOP_SCAN_ID;
        Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_STOP_SCAN_ID, "StopScan", "");   
        CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, "");
        m_commandHandler->acquireSemaphoreLock();
#endif

        m_commandHandler->setCommandState(SCAN_STOPPED);
        releaseScanLock();

        return CLR_FAILURE;
    }
 
    if (m_scanSeqId != m_testInterface->getScanSeqId())
    {
        if ((false == m_watchDevice) && (m_scanSeqId > -1))
        {
            cout << "Stop scanning operation " << endl;

#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
            CommonProtocolManager::getInstance()->stopScan();
#else
            m_commandID = CMD_STOP_SCAN_ID;
            Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_STOP_SCAN_ID, "StopScan", "");
            CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, "");
            m_commandHandler->acquireSemaphoreLock();
#endif
            m_commandHandler->setCommandState(SCAN_STOPPED);

            releaseScanLock();

            return CLR_SUCCESS;
        }
        else
        {
            m_scanSeqId = m_testInterface->getScanSeqId();
            cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
            cout << "Scan sequence id: " << m_scanSeqId << endl;
            cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
        }    
    }

    /// Get the information respective to the protocol
    if (PROTOCOL_TYPE_FF == m_commandHandler->getProtocolType())
    {
        cout << "----------------------------------------------------------" << endl;
        cout << "Device Node Address: " << m_testInterface->getNodeAddress() << endl;
        cout << "Device Physical Tag: " << m_testInterface->getDeviceTag() << endl;
        cout << "Device Id Tag: " << m_testInterface->getDeviceIdTag() << endl;
        cout << "----------------------------------------------------------" << endl;
    }
    
    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
void CLRScanCommand::printCommand()
{   
    cout << "----------- Available Commands -----------" << endl;
    cout << "list devices (l d)     - List all the available devices in the network" << endl;
    cout << "watch devices (w d)    - Watch the live network updated of the connected / detached devices" << endl;
    cout << "back (b)               - Go back to the previous command list" << endl;
}
