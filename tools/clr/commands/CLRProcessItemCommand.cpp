/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    CLRSetProtocolCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRProcessItemCommand.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus_defs.h"
#include <iomanip>

#define ENABLE_SEND_JSON

#if defined ENABLE_SEND_JSON
#include "SendJson.h"
#endif

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

#define MENU_ITEM_CHARWIDTH     (1)
#define MENU_INDEX_WIDTH        (3)
#define MENU_SUBIN_WIDTH        (2)
#define MENU_ITEMID_WIDTH       (10)
#define MENU_VALUE_WIDTH        (20)
#define MENU_VARTYPE_WIDTH      (16)
#define MENU_TYPE_WIDTH         (8)
#define MENU_LABEL_WIDTH        (50)

#define CUSTOM_ID_BLOCK_PARAM_LIST (4)
#define CUSTOM_ID_BLOCK_ALARMS  (12)
#define CUSTOM_ID_BLOCK_CONFIG  (7)

#define CUSTOM_ID_DEVICE_MENU           (0)
#define CUSTOM_ID_DEVICE_MENU_ADVANCED  (1)
#define CUSTOM_ID_BLOCK_MENU            (2) 
#define CUSTOM_ID_BLOCK_MENU_ADVANCED   (3)  
/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructor
*/
CLRProcessItemCommand::CLRProcessItemCommand(CLRCommandHandler* pCommandHandler)  
{
    m_commandHandler = pCommandHandler;
}

/*  ----------------------------------------------------------------------------
    Destructor
*/
CLRProcessItemCommand::~CLRProcessItemCommand()
{
}

/*  ----------------------------------------------------------------------------
    Processses CLR command
*/
int CLRProcessItemCommand::processCommand(const std::string& userCmndStr)
{   
    if ((0 == userCmndStr.compare("help")) ||
        (0 == userCmndStr.compare("h")))
    {
         printCommand();
    }
    else
    {
        if (0 == userCmndStr.compare("get item") || 
            0 == userCmndStr.compare("g i"))
        {
            int option = -1;
            do 
            {

                cout << "Which Item ? (Enter option Number)" << endl;
                cin >> option;

            }while (option == -1);

            ItemBasicTestInterface itemIntf;
            std::string itemType;
            Json::Value menu;
            m_commandHandler->getCurrentMenu(menu);
            m_testInterface.parseResponseData(REQUEST_TYPE_NONE, menu);

            if (TESTINTF_SUCCESS == itemIntf.parseResponseData(REQUEST_TYPE_NONE, m_testInterface.getMenuItemListInfo(option, itemType)))
            {
                 if(itemType.compare("VARIABLE") == 0)
                 {
                     getVariableItem(itemIntf);
                 }
                 else
                 {
					 CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, REQUEST, itemIntf.getRequestData(REQUEST_TYPE_NONE));
                     Json::Value requestData = itemIntf.getRequestData(REQUEST_TYPE_NONE);
                     Json::Value jsonData = CommonProtocolManager::getInstance()->getItem(requestData);
					 CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, RESPONSE, jsonData);

                     if (jsonData.isMember("result"))
                        jsonData = jsonData["result"];
                     else if (jsonData.isMember("error"))
                        jsonData = jsonData["error"];

					 if (CLR_SUCCESS == processCommandResponse(REQUEST_TYPE_NONE, jsonData))
					 {
						LOGF_INFO(CLR, "Get Item Successful");
						m_commandHandler->pushMenu(jsonData);
					 }
                 }
            }
        }
        else if ((0 == userCmndStr.compare("set item")) ||
                 (0 == userCmndStr.compare("s i")))
        {
            int option = -1;
            do 
            {

                cout << "Which Item ? (Enter option Number)" << endl;
                cin >> option;

            }while (option == -1);
            
            ItemBasicTestInterface itemIntf;
            std::string itemType;
            Json::Value menu;
            m_commandHandler->getCurrentMenu(menu);
            m_testInterface.parseResponseData(REQUEST_TYPE_GET, menu);
            
            if (TESTINTF_SUCCESS == itemIntf.parseResponseData(REQUEST_TYPE_NONE, m_testInterface.getMenuItemListInfo(option, itemType)))
            {
                if(itemType.compare("VARIABLE") == 0)
                {
                    return setVariableItem(option, itemType);
                }
                else
                {
                    cout << "Set item is not supported on item type "<< itemType << endl;
                }
            }
        }
        else if ((0 == userCmndStr.compare("disconnect")) ||
                 (0 == userCmndStr.compare("d")))
        {
            cout << "Disconnecting device...." << endl;
            Json::Value jsonData = CommonProtocolManager::getInstance()->disconnect();
            {
                // TODO(Vinay) Add a condition to check if it succeeded
                m_commandHandler->setCommandState(PROTOCOL_INITIALIZED);
                cout << "Disconnected successfully." << endl;
                m_commandHandler->clearMenuStack();
                m_commandHandler->destroyCommandInstance();

            }
        }
        else if ((0 == userCmndStr.compare("back")) ||
                 (0 == userCmndStr.compare("b")))
        {
            Json::Value menu;

            if (m_commandHandler->getStackSize() <= 1)
            {
                cout << "CAN NOT GO BACK. Try Disconnect !!!" << endl;
            }
            else
            {
                // As the user wants to go back, remove the current previous menu
                m_commandHandler->popMenu();
            
                // Get the previous menu which is now at the top
                m_commandHandler->getCurrentMenu(menu);
                                            
                // Remove the current menu as it will fetche from CPM again
                m_commandHandler->popMenu();

                // Parse the menu information
                m_testInterface.parseResponseData(REQUEST_TYPE_NONE, menu);

                // Invoke get Item
                cout << "Going Back to previous screen ....." << endl;
                CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, REQUEST, m_testInterface.getRequestData(REQUEST_TYPE_NONE));
                Json::Value jsonData = CommonProtocolManager::getInstance()->getItem(m_testInterface.getRequestData(REQUEST_TYPE_NONE));
                CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, RESPONSE, jsonData);

                if (jsonData.isMember("result"))
                    jsonData = jsonData["result"];
                else if (jsonData.isMember("error"))
                    jsonData = jsonData["error"];

                if (CLR_SUCCESS == processCommandResponse(REQUEST_TYPE_NONE, jsonData))
                {
                    LOGF_INFO(CLR, "Get Item Successful");
                    m_commandHandler->pushMenu(jsonData);
                }
            }
        }
#if defined ENABLE_SEND_JSON
        else if(0 == userCmndStr.compare("s j"))
        {
            SendJson sj;
            Json::Value jsonReq = sj.prepareReq();

            string choice;

            cout <<"########### Enter Choice ##################" <<endl;
            cout <<"########### Enter g i for getItem #########" <<endl;
            cout <<"########### Enter s i for setItem #########" <<endl;
            cout <<"###########################################" <<endl;
            cin >> choice;

            cout <<"###########JSON REQ #########" <<endl;
            cout<<jsonReq<<endl;
            cout <<"#############################" <<endl;

            Json::Value jsonResp;
            if (0 == choice.compare("g i"))
            {
                jsonResp = CommonProtocolManager::getInstance()->getItem(jsonReq);
            }

            else if (0 == choice.compare("s i"))
            {
                jsonResp = CommonProtocolManager::getInstance()->setItem(jsonReq);
            }

            else
            {
                cout<<"Invalid type"<<endl;
            }
            
            cout <<"***********JSON RESP *********" <<endl;
            cout<<jsonResp<<endl;
            cout <<"******************************" <<endl;
        }
#endif
    }
    return CLR_SUCCESS;
    
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
int CLRProcessItemCommand::processCommandResponse
(
    const REQUEST_TYPE& requestType, 
    const Json::Value& jsonData
)
{
   if (TESTINTF_SUCCESS != m_testInterface.parseResponseData(requestType, jsonData))
   {
       cout << "Failed to get the selected item, error code: " 
            << m_testInterface.getErrorCode() 
            << ", error response: " 
            << m_testInterface.getErrorMessage() << endl;

       return CLR_FAILURE;
   }
   else
   {
       if ((CUSTOM_ID_BLOCK_MENU) ==  m_testInterface.getItemID() || 
           (CUSTOM_ID_DEVICE_MENU_ADVANCED) == m_testInterface.getItemID() ||
           (CUSTOM_ID_DEVICE_MENU) == m_testInterface.getItemID() ||
           (CUSTOM_ID_BLOCK_MENU_ADVANCED) == m_testInterface.getItemID())
       {
           cout << m_testInterface.getItemLabel() << ":"  << endl;
           printMenuTableBorder();

           cout << setfill(' ') << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_INDEX_WIDTH)<< left << "IND" 
                                << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << "ITEM ID" 
                                << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_LABEL_WIDTH)<< left << "LABEL" 
                                << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_TYPE_WIDTH) << left << "TYPE" 
                                << setw(MENU_ITEM_CHARWIDTH) << "|" << endl;

            printMenuTableBorder();

            for (unsigned short nIndex = 0; nIndex < m_testInterface.getMenuItemCount(); nIndex++)
            {
                std::string itemType;
                m_testInterface.getMenuItemListInfo(nIndex, itemType);

                ItemBasicTestInterface itemIntf;
                Json::Value menuItemInfo = m_testInterface.getMenuItemListInfo(nIndex, itemType);
                if (TESTINTF_SUCCESS == itemIntf.parseResponseData(REQUEST_TYPE_NONE, menuItemInfo))
                {
                    printMenuEntries(   nIndex, 
                                        itemIntf.getItemID(), 
                                        itemIntf.getItemLabel(), 
                                        itemIntf.getItemType());
                    printMenuTableBorder();
                }
           }    
       }
       else 
       {
            getBlockParameters();
       }
            
       printMenuTableBorder();
   }


   return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
void CLRProcessItemCommand::printCommand()
{   
    switch (m_commandHandler->getCommandState())
    {
        case PROCESS_ITEM:
            {
                cout << "--------- Available Commands ---------" << endl;
                cout << "get item (g i)   - Get the selected item" << endl;
                cout << "set item (s i)   - Set the selected item" << endl;
                cout << "disconnect (d)   - Disconnect from the device" << endl;
                cout << "back (b)         - Go Back to the Previous Screen " << endl;
#if defined ENABLE_SEND_JSON
                cout<< "send json (s j)   - Sends the json request to CPM" <<endl;
#endif
            }
            break;

        default:
            break;
    }
}

/*  ----------------------------------------------------------------------------
    Get Variable Item
*/
void CLRProcessItemCommand::getVariableItem(ItemBasicTestInterface& itemIntf)
{
	CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, REQUEST, itemIntf.getRequestData(REQUEST_TYPE_NONE));
	Json::Value varReq = itemIntf.getRequestData(REQUEST_TYPE_NONE);
    Json::Value jsonData = CommonProtocolManager::getInstance()->getItem(varReq);
	CLRCommandLogger::LOG_COMMAND(CMD_GET_ITEM_ID, RESPONSE, jsonData);
	
    if (jsonData.isMember("result"))
        jsonData = jsonData["result"];
    else if (jsonData.isMember("error"))
        jsonData = jsonData["error"];

    itemIntf.parseResponseData(REQUEST_TYPE_NONE, jsonData);
    m_variableTestInterface.parseResponseData(REQUEST_TYPE_NONE, jsonData);
    cout<<"------------------------+-----------------------+---------------"<<endl;

    cout<<"ITEM Type     "<<"       "<<"   : "<<itemIntf.getItemType()<<endl;
    cout<<"ID            "<<"		"<<": "<<itemIntf.getItemID()<<endl;
    cout<<"LABEL         "<<"		"<<": "<<itemIntf.getItemLabel()<<endl;
    cout<<"HELP          "<<"       "<<"   : "<<itemIntf.getItemHelp()<<endl;
    cout<<"Container Tag "<<"		"<<": "<<itemIntf.getItemContainerTag()<<endl;

    if (TRUE == itemIntf.isItemInDD())
        cout<<"Item IN DD    "<<"		"<<": "<<"TRUE"<<endl;
    else
        cout<<"Item IN DD    "<<"		"<<": "<<"FALSE"<<endl;

	cout<<"VAR TYPE      "<<"		"<<":"<<m_variableTestInterface.getVarType()<<endl;
	cout<<"VAR ContainerID      "<<"		"<<":"<<m_variableTestInterface.getVarContainerID()<<endl;
	cout<<"VAR SubIndex      "<<"		"<<":"<<m_variableTestInterface.getVarSubIndex()<<endl;
	cout<<"Item Read only     "<<"		"<<":"<<m_variableTestInterface.getIsReadOnlyStatus()<<endl;


		if (m_variableTestInterface.getVarType().compare("ENUMERATION") == 0)
		{
			m_variableTestInterface.getEnumValues();
		}

		if (m_variableTestInterface.getVarType().compare("STRING") == 0)
		{
			 m_variableTestInterface.getStringValues();
		}

		cout<<"Value     "<<"		"<<":"<<m_variableTestInterface.getVarValue()<<endl;

		if ((m_variableTestInterface.getVarType().compare("ARITHMETIC") == 0))
		{
			 m_variableTestInterface.getMinMaxValue();
			 if((m_variableTestInterface.getVarValType().compare("FLOAT") == 0)
					  || (m_variableTestInterface.getVarValType().compare("DOUBLE") == 0))
			 {
				 if(m_variableTestInterface.getNANStatus().asBool())
                 {
					 cout<<"VarValIsNAN     "<<"		"<<": TRUE"<<endl;
                 }
				 else
                 {
					 cout<<"VarValIsNAN     "<<"		"<<": FALSE"<<endl;
                 }

                 if (m_variableTestInterface.getINFStatus().asBool())
                 {
					 cout<<"VarValIsINF     "<<"		"<<": TRUE"<<endl;
                 }
                 else
                 {
					 cout<<"VarValIsINF     "<<"		"<<": FALSE"<<endl;
                 }
			 }
		}

		cout<<"VarValType "<<"		"<<":"<<m_variableTestInterface.getVarValType()<<endl;
		cout<<"UNIT      "<<"		"<<":"<<m_variableTestInterface.getVarUnit()<<endl;
		cout<<"DISPLAY FORMAT      "<<"		"<<":"<<m_variableTestInterface.getVarValDisplayFormat()<<endl;
		cout<<"EDIT FORMAT      "<<"		"<<":"<<m_variableTestInterface.getVarValEditFormat()<<endl;
	cout<<"------------------------+-----------------------+---------------"<<endl;
}

/*  ----------------------------------------------------------------------------
    Set Variable Item
*/
int CLRProcessItemCommand::setVariableItem(int& option, std::string& itemType)
{
    // Edit action
    VariableTestInterface varIntf;
    Json::Value menuItemInfo = m_testInterface.getMenuItemListInfo(option, itemType);
    varIntf.parseResponseData(REQUEST_TYPE_GET, menuItemInfo);
    Json::Value setItemReq = varIntf.getRequestData(REQUEST_TYPE_SET_EDIT);
    CLRCommandLogger::LOG_COMMAND(CMD_SET_ITEM_ID, REQUEST, setItemReq);
    Json::Value jsonData = CommonProtocolManager::getInstance()->setItem(setItemReq);
    CLRCommandLogger::LOG_COMMAND(CMD_SET_ITEM_ID, RESPONSE, jsonData);

    if (jsonData.isMember("result"))
        jsonData = jsonData["result"];
    else if (jsonData.isMember("error"))
        jsonData = jsonData["error"];
    
    if (TESTINTF_SUCCESS == varIntf.parseResponseData(REQUEST_TYPE_SET_EDIT, jsonData))
    {
        // Update action
        std::string newValue;
        cout << "Enter the new " <<varIntf.getVarValType()<<" value" <<endl;
        cin.ignore();
        std::getline(std::cin, newValue);

        varIntf.setVarValue(newValue);
        Json::Value jsonResp;
        setItemReq.clear();

        setItemReq = varIntf.getRequestData(REQUEST_TYPE_SET_UPDATE);
        
        CLRCommandLogger::LOG_COMMAND(CMD_SET_ITEM_ID, REQUEST, setItemReq);
        jsonResp = CommonProtocolManager::getInstance()->setItem(setItemReq);
        CLRCommandLogger::LOG_COMMAND(CMD_SET_ITEM_ID, RESPONSE, jsonData);


        if (jsonResp.isMember("result"))
            jsonResp= jsonResp["result"];
        else if (jsonResp.isMember("error"))
            jsonResp= jsonResp["error"];

        if (TESTINTF_FAILURE == varIntf.parseResponseData(REQUEST_TYPE_SET_UPDATE, jsonResp))
        {
            return CLR_FAILURE;
        }
    }
    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Prints menu table border
*/
void CLRProcessItemCommand:: printMenuTableBorder()
{
    cout << setfill('-')<< setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_INDEX_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_ITEMID_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_LABEL_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_TYPE_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << endl;
}

/*  ----------------------------------------------------------------------------
    Prints menu labels
*/
void CLRProcessItemCommand:: printMenuEntries
(
    const unsigned short& nIndex,
    const unsigned long& itemID, 
    const std::string& itemLabel,
    const std::string& itemType
)
{
    cout << setfill(' ') << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_INDEX_WIDTH)<< left << nIndex  
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << itemID 
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_LABEL_WIDTH)<< left << itemLabel 
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_TYPE_WIDTH) << left << itemType
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << endl;
}

/*  ----------------------------------------------------------------------------
    Prints block parameters table border
*/
void CLRProcessItemCommand:: printBlkParamTableBorder()
{
    cout << setfill('-')<< setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_INDEX_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_ITEMID_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_LABEL_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_TYPE_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_VARTYPE_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_VALUE_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_ITEMID_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << setw(MENU_SUBIN_WIDTH) 
                 << "-" << setw(MENU_ITEM_CHARWIDTH) << "+" << endl;
}

/*  ----------------------------------------------------------------------------
    Prints Block Parameters
*/
void CLRProcessItemCommand::printBlkParamEntries
(
    const unsigned short& nIndex,
    const unsigned long& itemID, 
    const std::string& itemLabel,
    const std::string& itemType,
    const std::string& itemVarValType,
    const std::string& itemValue,
    const unsigned long& itemContainerId,
    const unsigned short& itemSubIndex
)
{
    cout << setfill(' ') << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_INDEX_WIDTH)<< left << nIndex  
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << itemID 
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_LABEL_WIDTH)<< left << itemLabel 
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_TYPE_WIDTH) << left << itemType
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_VARTYPE_WIDTH) << left << itemVarValType
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_VALUE_WIDTH)<< left << itemValue
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << itemContainerId
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_SUBIN_WIDTH)<< left << itemSubIndex
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << endl;
}

/*  ----------------------------------------------------------------------------
*    Get Block Parameters
*/
void CLRProcessItemCommand::getBlockParameters()
{
    printBlkParamTableBorder();
    cout << setfill(' ') << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_INDEX_WIDTH)<< left << "IND"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << "ITEM ID"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_LABEL_WIDTH)<< left << "LABEL"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_TYPE_WIDTH) << left << "TYPE"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_VARTYPE_WIDTH) << left << "VAR TYPE"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_VALUE_WIDTH)<< left << "VALUE"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_ITEMID_WIDTH) << left << "CONT ID"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << setw(MENU_SUBIN_WIDTH)<< left << "SI"
                         << setw(MENU_ITEM_CHARWIDTH) << "|" << endl;

    printBlkParamTableBorder();
    for (int nIndex = 0; nIndex < m_testInterface.getMenuItemCount(); nIndex++)
    {
        std::string itemType;
        m_testInterface.getMenuItemListInfo(nIndex, itemType);

        ItemBasicTestInterface itemIntf;
        Json::Value menuItemInfo = m_testInterface.getMenuItemListInfo(nIndex, itemType);
        if (TESTINTF_SUCCESS == itemIntf.parseResponseData(REQUEST_TYPE_NONE, menuItemInfo))
        {
            m_variableTestInterface.parseResponseData(REQUEST_TYPE_NONE, menuItemInfo);
            printBlkParamEntries(   nIndex,
                                    itemIntf.getItemID(),
                                    itemIntf.getItemLabel(),
                                    itemIntf.getItemType(),
                                    (m_variableTestInterface.getVarValType()).asString(),
                                    (m_variableTestInterface.getVarValue()).asString(),
                                    (m_variableTestInterface.getVarContainerID()).asLargestUInt(),
                                    (m_variableTestInterface.getVarSubIndex()).asUInt());
             printBlkParamTableBorder();
        }
    }
}
