/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRFFBusParamCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRFFBusParamCommand.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

/*  ----------------------------------------------------------------------------
    Set bus parameter
*/
void CLRFFBusParamCommand::setBusParamData(const std::string& param, const Json::Value& variantData)
{
    m_ffBusParamTestIntf.setBusParam(param, variantData);
}

/*  ----------------------------------------------------------------------------
    Get the request data
*/
Json::Value CLRFFBusParamCommand::getRequestData(const REQUEST_TYPE& requestType)
{
    return m_ffBusParamTestIntf.getRequestData(requestType);
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
int CLRFFBusParamCommand::processCommandResponse(const REQUEST_TYPE& requestType, const Json::Value& jsonData)
{
   if (TESTINTF_SUCCESS != m_ffBusParamTestIntf.parseResponseData(requestType, jsonData))
   {
       cout << "Bus Parameter, error code: " << m_ffBusParamTestIntf.getErrorCode() << ", error response: " << m_ffBusParamTestIntf.getErrorMessage() << endl;
       return CLR_FAILURE;
   }

    switch (requestType)
    {
        case REQUEST_TYPE_GET:
            {
                cout << "Slot Time (E)          : " << m_ffBusParamTestIntf.getSlotTime() << endl;
                cout << "FUN (E)                : " << m_ffBusParamTestIntf.getFun() << endl;
                cout << "NUN (E)                : " << m_ffBusParamTestIntf.getNun() << endl;
                cout << "Link Node              : " << m_ffBusParamTestIntf.getLink() << endl;
                cout << "DL PDU Overload        : " << m_ffBusParamTestIntf.getDlPduOverload() << endl;
                cout << "Max. Resp. Delay       : " <<  m_ffBusParamTestIntf.getMaxResDelay() << endl;
                cout << "Min. Inter PDU Delay   : " << m_ffBusParamTestIntf.getMinInterPDUDelay() << endl;
                cout << "Time Sync Class        : " << m_ffBusParamTestIntf.getTimeSyncClass() << endl;
                cout << "Signal Skew            : " << m_ffBusParamTestIntf.getInterChannelSignalSkew() << endl;
                cout << "PHL Preamble Ext.      : " << m_ffBusParamTestIntf.getPhlPreambleExt() << endl;
                cout << "PHL Gap Ext.           : " << m_ffBusParamTestIntf.getPhlGapExt() << endl;
            }
            break;

        default:
            break;
    }
}
