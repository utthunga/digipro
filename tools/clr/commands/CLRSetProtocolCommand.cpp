/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRSetProtocolCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRSetProtocolCommand.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus_defs.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

static int m_commandID = -1;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructor
*/
CLRSetProtocolCommand::CLRSetProtocolCommand(CLRCommandHandler* pCommandHandler):
    m_testInterface(nullptr),
    m_nUserSelectedProtcol(-1),
    m_bProtocolRdyToInit(false)
{
    m_protocolType.push_back("FF");
    m_protocolType.push_back("HART (Not Supported)");
    m_protocolType.push_back("PROFIBUS (Not Supported)");

    m_commandHandler = pCommandHandler;
}

/*  ----------------------------------------------------------------------------
    Destructor
*/
CLRSetProtocolCommand::~CLRSetProtocolCommand()
{
    if (nullptr != m_testInterface)
        delete m_testInterface;
}

/*  ----------------------------------------------------------------------------
    Processses CLR command
*/
int CLRSetProtocolCommand::processCommand(const std::string& userCmndStr)
{   
    if ((0 == userCmndStr.compare("help")) ||
        (0 == userCmndStr.compare("h")))
        printCommand();
    else if (userCmndStr.length() > 0)
    {
        switch (m_commandHandler->getCommandState())
        {
            case PROTOCOL_UNINITIALIZED:
                {
                    int nUserChoice = atoi (userCmndStr.c_str());
                        
                    if (nUserChoice > 0 && nUserChoice <= m_protocolType.size())
                    {
                        m_testInterface = new SetProtocolTestInterface();
                        m_testInterface->setProtocolType(m_protocolType.at(nUserChoice - 1));

                        m_nUserSelectedProtcol = nUserChoice;

#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
                        Json::Value response = CommonProtocolManager::getInstance()->setProtocolType(m_testInterface->getRequestData(REQUEST_TYPE_NONE));
                        processCommandResponse(REQUEST_TYPE_NONE, response);
#else            
                        m_commandID = CMD_SET_PROTOCOL_TYPE_ID;
                        Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_SET_PROTOCOL_TYPE_ID, "SetProtocolType", m_testInterface->getRequestData(REQUEST_TYPE_NONE));
                        CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, m_testInterface->getRequestData(REQUEST_TYPE_NONE));
                        m_commandHandler->acquireSemaphoreLock();
#endif

#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
                        response = CommonProtocolManager::getInstance()->initialize();
                        processCommandResponse(REQUEST_TYPE_NONE, response);
#else            
                        m_commandID = CMD_INITIALIZE_ID;
                        jsonResult = m_commandHandler->executeAsyncCommand (CMD_INITIALIZE_ID, "Initialize", Json::Value(""));
                        CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, Json::Value(""));
                        m_commandHandler->acquireSemaphoreLock();
#endif
                    }
                    else
                    {
                        printCommand();
                    } 
                }
                break;

            case PROTOCOL_INITIALIZED:
                {
                     if ((0 == userCmndStr.compare("scan n/w")) ||
                         (0 == userCmndStr.compare("s n")))
                     {
                         if (m_commandHandler)
                             m_commandHandler->setCommandState(SCAN_TRIGGER);

                         LOGF_INFO(CLR, "Set state as scan operation");   
                     }
                     else if ((0 == userCmndStr.compare("bus config")) ||
                              (0 == userCmndStr.compare("b c")))
                     {
                         if (m_commandHandler)
                             m_commandHandler->setCommandState(BUS_PARAM_CONFIG);

                         LOGF_INFO(CLR, "Set state as bus config operation");   
                     }
                     else if ((0 == userCmndStr.compare("connect")) ||
                              (0 == userCmndStr.compare("c")))
                     {
                         if (m_commandHandler)
                             m_commandHandler->setCommandState(CONNECT_DEVICE);
                     }          
                     else if ((0 == userCmndStr.compare("back")) ||
                              (0 == userCmndStr.compare("b")))
                     {
#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
                        Json::Value response = CommonProtocolManager::getInstance()->shutdown();
                        processCommandResponse(REQUEST_TYPE_NONE, response);
#else
                        m_commandID = CMD_SHUTDOWN_ID;
                        Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_SHUTDOWN_ID, "Shutdown", Json::Value(""));
                        CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, Json::Value(""));
                        m_commandHandler->acquireSemaphoreLock();
#endif
                     }
                     else
                     {
                         printCommand();
                     }
                }
                break;

            default:
                break;
         }    
    }
    
    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Process Async Command Response
*/
int CLRSetProtocolCommand::processAsyncCommandResponse(const Json::Value& jsonData)
{
    CLRCommandLogger::LOG_COMMAND(m_commandID, RESPONSE, jsonData);
    processCommandResponse (REQUEST_TYPE_NONE, jsonData);
    m_commandHandler->releaseSemaphoreLock();
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
int CLRSetProtocolCommand::processCommandResponse(const REQUEST_TYPE& requestType, const Json::Value& jsonData)
{
    int returnStatus = CLR_SUCCESS;

    if (TESTINTF_SUCCESS != m_testInterface->parseResponseData (requestType, jsonData))
    {
        cout << "Protocol operation failed, error code: " << m_testInterface->getErrorCode() << ", error response: " << m_testInterface->getErrorMessage() << endl;
        return CLR_FAILURE;
    }

    switch (m_commandHandler->getCommandState())
    {
        case PROTOCOL_UNINITIALIZED:
        {
            cout << "Protccol ready to be initialized " << endl;
            if (false == m_bProtocolRdyToInit)
            {
                m_commandHandler->setCommandState(PROTOCOL_READY_TO_INITIALIZE);
                m_bProtocolRdyToInit = true;
            }
        }   
        break;

        case PROTOCOL_READY_TO_INITIALIZE:
        {
            cout << "Protocol initialized successfully !!! " << endl;
            if (m_commandHandler)
            {
                m_commandHandler->setProtocolType(m_nUserSelectedProtcol);
                m_commandHandler->setCommandState(PROTOCOL_INITIALIZED);
            }   
        }
        break;

        case PROTOCOL_INITIALIZED:
        {
            cout << "Protocol shuts down successfully !!! " << endl;
            if (m_commandHandler)
            {
                m_commandHandler->setCommandState(PROTOCOL_UNINITIALIZED);
            }
        }
        break;

        default:
            break;
    }

    return returnStatus;
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
void CLRSetProtocolCommand::printCommand()
{   
    switch (m_commandHandler->getCommandState())
    {
        case PROTOCOL_UNINITIALIZED:
            {
                cout << "Protocol List: " << endl;
                for (int nIndex = 0; nIndex < m_protocolType.size(); nIndex++)
                    cout << (nIndex + 1) << ". " << m_protocolType.at(nIndex) << endl;
                cout << "Select index from above list to proceed" << endl;
            }
            break;

        case PROTOCOL_INITIALIZED:
            {
                cout << "--------- Available Commands ---------" << endl;
                cout << "scan n/w (s n)     - Scan the network depending upon the selected protocol selected" << endl;
                cout << "bus config (b c)   - Configure the bus parameters for the selected protocol" << endl;
                cout << "connect (c)  - Connect to the specific device in the network" << endl;
                cout << "back (b) - Go back to the previous command list" << endl;
            }
            break;

        default:
            break;
    }
}
