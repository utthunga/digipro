/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRFFConnectCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRFFConnectCommand.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

/*  ----------------------------------------------------------------------------
    Get the request data
*/
Json::Value CLRFFConnectCommand::getRequestData(const REQUEST_TYPE& requestType)
{
    return m_ffConnectTestIntf.getRequestData(requestType);
}    

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
int CLRFFConnectCommand::processCommandResponse(const REQUEST_TYPE& requestType, const Json::Value& jsonData)
{
   if (TESTINTF_SUCCESS != m_ffConnectTestIntf.parseResponseData(requestType, jsonData))
   {
       cout << "Connect failure, error code: " << m_ffConnectTestIntf.getErrorCode() << ", error response: " << m_ffConnectTestIntf.getErrorMessage() << endl;
       return CLR_FAILURE;
   }

   return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Process connect command required for FF protocol
*/
int CLRFFConnectCommand::processCommand(const std::string& userCmndStr)
{   
    int retStatus = CLR_FAILURE;
    std::string token;
    std::vector<std::string> connectParam;
    std::stringstream ss(userCmndStr);

    while (ss >> token)
            connectParam.push_back(token);

    if (connectParam.size() == 2)
    {
        if ((0 == connectParam.at(0).compare("connect")) ||
            (0 == connectParam.at(0).compare("c")))
        {
            try
            {
                m_ffConnectTestIntf.setNodeAddress(stoi(connectParam.at(1)));
                retStatus = CLR_SUCCESS;
            }
            catch(...)
            {
                cout << "Invalid Entry - Enter Integer for device Address" << endl;
            }
        }
    }
    
    return retStatus;
}

/*  ----------------------------------------------------------------------------
    Print command required for FF connect command
*/
void CLRFFConnectCommand::printCommand()
{
    cout << "connect <n> (c <n>)    - Connect to the specified device in the network" << endl;
    cout << "                           <n> - Node address of the device to be connected" << endl;  
}
