/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "ICLRCommand.h"
#include "CLRCommandHandler.h"
#include "ScanTestInterface.h"

/* ========================================================================== */
/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRScanCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRScanCommand : public ICLRCommand
{
public:
    
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRScanCommand Class.
    */
    CLRScanCommand(CLRCommandHandler* pCommandHandler);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRScanCommand class instance.
    */
    virtual ~CLRScanCommand();


    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    int processCommand (const std::string& userCmndStr);
    
    /*------------------------------------------------------------------------*/
    /** Process Async Command Response */
    int processAsyncCommandResponse (const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Process command response */
    int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData);

protected:
    /* Protected Member Functions =============================================*/
    /*------------------------------------------------------------------------*/
    /** Print command */
    void printCommand ();

private:
    /* Private Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/    
    /** Acquire scan lock required for scan response data */
    void acquireScanLock()
    {
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        while (!m_condVarScan)
            m_condVarScanLock.wait(lock);
        m_condVarScan = false;
    }    

    /*------------------------------------------------------------------------*/    
    /** Acquire scan lock required for scan response data */
    void releaseScanLock()  
    {
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        m_condVarScan = true;
        m_condVarScanLock.notify_one();
    }

    /* Private Data Member ===================================================*/
    
    /// Holds the test interface object
    ScanTestInterface* m_testInterface;

    /// Holds the command handler object instance
    CLRCommandHandler* m_commandHandler;

    /// Holds the mutex lock
    std::mutex m_mutex;

    /// Holds condition variable for asynchronous events
    std::condition_variable m_condVarScanLock;

    /// Holds variable value for condition lock
    bool m_condVarScan;

    /// Hold the scan sequence id
    int m_scanSeqId;

    /// Holds whether the user wants live update or the current devices in n/w
    bool m_watchDevice;
};
