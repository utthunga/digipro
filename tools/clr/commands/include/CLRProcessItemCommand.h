/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "ICLRCommand.h"
#include "CLRCommandHandler.h"
#include "MenuTestInterface.h"
#include "VariableTestInterface.h"


#define TRUE 1
/* ========================================================================== */
/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRSetProtocolCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRProcessItemCommand : public ICLRCommand
{
public:
    
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRProcessItemCommand Class.
    */
    CLRProcessItemCommand(CLRCommandHandler* pCommandHandler);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRProcessItemCommand class instance.
    */
    virtual ~CLRProcessItemCommand();


    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    int processCommand (const std::string& userCmndStr);

    /*------------------------------------------------------------------------*/
    /** Process command response */
    int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData);

protected:
    /* Protected Member Functions =============================================*/
    /*------------------------------------------------------------------------*/
    /** Print command */
    void printCommand ();

private:
    /* Private Member Functions =============================================*/
    /*------------------------------------------------------------------------*/
    /** Get variable Item */
    void getVariableItem(ItemBasicTestInterface& itemIntf);
    
    /*------------------------------------------------------------------------*/
    /** Get Record Item */
    void getRecordItem(ItemBasicTestInterface& itemIntf);
    
    /*------------------------------------------------------------------------*/
    /** Get Block Parameters */
    void getBlockParameters();
    
    /*------------------------------------------------------------------------*/
    /** Set variable Item */
    int setVariableItem(int& option, std::string& itemType);

    /*------------------------------------------------------------------------*/
    /** Print menu table Border */
    void printMenuTableBorder();
    
    /*------------------------------------------------------------------------*/
    /** Print menu table Border */
    void printBlkParamTableBorder();
    
    /*------------------------------------------------------------------------*/
    /** Print menu entris */
    void printMenuEntries(  const unsigned short& index, 
                            const unsigned long& itemID, 
                            const std::string& itemLabel, 
                            const std::string& itemType);

    /*------------------------------------------------------------------------*/
    /** Print block parameter entris */
    void printBlkParamEntries(  const unsigned short& index, 
                                const unsigned long& itemID, 
                                const std::string& itemLabel, 
                                const std::string& itemType,
                                const std::string& itemVarValType,
                                const std::string& itemVarValue,
                                const unsigned long& itemContainerId,
                                const unsigned short& itemSubIndex);
private:
    /* Private Data Member ===================================================*/
    
    /// Holds the test interface object
    MenuTestInterface m_testInterface;

    /// Holds the test interface object of variable
    VariableTestInterface m_variableTestInterface;

    /// Holds the command handler object instance
    CLRCommandHandler* m_commandHandler;
};
