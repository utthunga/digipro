/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "CLRConnectCommand.h"
#include "FFConnectTestInterface.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRConnectCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRFFConnectCommand : public CLRConnectCommand
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRFFConnectCommand Class.
    */
    CLRFFConnectCommand(CLRCommandHandler* pCommandHadler):
                CLRConnectCommand(pCommandHadler)
    {
    }

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRFFConnectCommand class instance.
    */
    virtual ~CLRFFConnectCommand() = default;


    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /* Process command request required for FF procotol */
    int processCommand(const std::string& userCmdStr);

    /*------------------------------------------------------------------------*/
    /* Print command request required for FF protocol */
    void printCommand();

    /*------------------------------------------------------------------------*/
    /* Fetch the request data for connect to be processed */
    Json::Value getRequestData(const REQUEST_TYPE& requestType);

    /*------------------------------------------------------------------------*/
    /** Process command response */
    int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData);

private:
    /* Private Data Member ===================================================*/
    /// Holds ff bus param test interface instance
    FFConnectTestInterface m_ffConnectTestIntf;
};
