/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "CLRBusParamCommand.h"
#include "FFBusParamTestInterface.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRBusParamCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRFFBusParamCommand : public CLRBusParamCommand
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRFFBusParamCommand Class.
    */
    CLRFFBusParamCommand(CLRCommandHandler* pCommandHadler):
                CLRBusParamCommand(pCommandHadler)
    {
    }

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRFFBusParamCommand class instance.
    */
    virtual ~CLRFFBusParamCommand() = default;


    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    Json::Value getRequestData(const REQUEST_TYPE& requestType);

    /*------------------------------------------------------------------------*/
    /** Process command response */
    int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Set bus parameter data */
    void setBusParamData(const std::string& param, const Json::Value& jsonData);

private:
    /* Private Data Member ===================================================*/
    /// Holds ff bus param test interface instance
    FFBusParamTestInterface m_ffBusParamTestIntf;
};
