/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "ICLRCommand.h"
#include "CLRCommandHandler.h"
#include "BusParamTestInterface.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRBusParamCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRBusParamCommand : public ICLRCommand
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRBusParamCommand Class.
    */
    CLRBusParamCommand(CLRCommandHandler* pCommandHandler);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRBusParamCommand class instance.
    */
    virtual ~CLRBusParamCommand();

    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    int processCommand (const std::string& userCmndStr);

    /*------------------------------------------------------------------------*/
    /** Print command */
    void printCommand ();
    
    /*------------------------------------------------------------------------*/
    /** Process Async Command Response */
    int processAsyncCommandResponse (const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Process command response */
    virtual int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData)
    {
        return CLR_SUCCESS;
    }    

    /*---------------------------------------------------------------------------*/
    /** Process user provided command */
    virtual Json::Value getRequestData(const REQUEST_TYPE& requestType)
    {
        return Json::Value("");
    }

    /*------------------------------------------------------------------------*/
    /** Set bus parameter 
    */
    virtual void setBusParamData(const std::string& param, const Json::Value& jsonData)
    {
    }

private:
    /* Private Data Member ===================================================*/
    /// Holds the command handler object instance
    CLRCommandHandler* m_commandHandler;

    /// Holds CLR command object
    CLRBusParamCommand* m_busParamCommand;

    /// Holds the command request type
    REQUEST_TYPE m_requestType;

    /* Private Member Function ===================================================*/
    /*------------------------------------------------------------------------*/
    /** Get corrsponding bus param command instance
    */
    void createBusParamCmndInstance();
};
