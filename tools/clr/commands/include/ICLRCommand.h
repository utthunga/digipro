/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "CLRCommandLogger.h"
#include "CommonTestInterface.h"

/* ========================================================================== */

/* Macros definition **********************************************************/

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of ICLRCommand
    
    Interface class for all CLR commands
*/
class ICLRCommand
{
public:
    /* Public Member Functions ===============================================*/
    virtual ~ICLRCommand(){};

    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    virtual int processCommand (const std::string& userCmndStr) = 0;

    /*------------------------------------------------------------------------*/
    /*8 Process Async Command Response */
    virtual int processAsyncCommandResponse(const Json::Value& jsonData)
    {
        
    }

    /*------------------------------------------------------------------------*/
    /** Process command response */
    virtual int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData) = 0;

    /*-----------------------------------------------------------------------*/
    /** Print command */
    virtual void printCommand () = 0;
};
