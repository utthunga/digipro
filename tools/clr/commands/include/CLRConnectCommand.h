/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "ICLRCommand.h"
#include "CLRCommandHandler.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRConnectCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRConnectCommand : public ICLRCommand
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRConnectCommand Class.
    */
    CLRConnectCommand(CLRCommandHandler* pCommandHandler);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRConnectCommand class instance.
    */
    virtual ~CLRConnectCommand();

    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    virtual int processCommand (const std::string& userCmndStr);

    /*------------------------------------------------------------------------*/
    /** Print command */
    virtual void printCommand ();
    
    /*------------------------------------------------------------------------*/
    /** Process Async Command Response */
    int processAsyncCommandResponse (const Json::Value& jsonData);

    /*---------------------------------------------------------------------------*/
    /** Process user provided command */
    virtual Json::Value getRequestData(const REQUEST_TYPE& requestType)
    {
        return Json::Value("");
    }

    /*------------------------------------------------------------------------*/
    /** Process command response */
    virtual int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData)
    {
        return CLR_SUCCESS;
    }
    
private:
    /* Public Member Functions ===============================================*/
    /*---------------------------------------------------------------------------*/
    /** Process connect response data */
    void processConnectResponse(const Json::Value& jsonData);
    

    /* Private Data Member ===================================================*/
    /// Holds the command handler object instance
    CLRCommandHandler* m_commandHandler;

    /// Holds CLR command object
    CLRConnectCommand* m_connectCommand;

    /* Private Member Function ===================================================*/
    /*------------------------------------------------------------------------*/
    /** Get corresponding connect command instance
    */
    void createConnectCmndInstance();
};
