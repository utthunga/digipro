/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include "ICLRCommand.h"
#include "CLRCommandHandler.h"
#include "SetProtocolTestInterface.h"

/* ========================================================================== */
/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRSetProtocolCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRSetProtocolCommand : public ICLRCommand
{
public:
    
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Constructs CLRSetProtocolCommand Class.
    */
    CLRSetProtocolCommand(CLRCommandHandler* pCommandHandler);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CLRSetProtocolCommand class instance.
    */
    virtual ~CLRSetProtocolCommand();


    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Process user provided command */
    int processCommand (const std::string& userCmndStr);

    /*------------------------------------------------------------------------*/
    /** Process Async Command Response */
    int processAsyncCommandResponse (const Json::Value& jsonData);
    
    /*------------------------------------------------------------------------*/
    /** Process command response */
    int processCommandResponse (const REQUEST_TYPE& requestType, const Json::Value& jsonData);

protected:
    /* Protected Member Functions =============================================*/
    /*------------------------------------------------------------------------*/
    /** Print command */
    void printCommand ();

private:
    /* Private Data Member ===================================================*/
    
    /// Holds the protocol type information
    std::vector<string> m_protocolType;

    /// Holds the test interface object
    SetProtocolTestInterface* m_testInterface;

    /// Holds the command handler object instance
    CLRCommandHandler* m_commandHandler;

    /// Holds the user entered choice
    int m_nUserSelectedProtcol;

    /// Holds whether the protocol is ready to be initialized
    bool m_bProtocolRdyToInit;
};
