/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRBusParamCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRBusParamCommand.h"
#include "CLRFFBusParamCommand.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus_defs.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

static int m_commandID = -1;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructor
*/
CLRBusParamCommand::CLRBusParamCommand(CLRCommandHandler* pCommandHandler):
    m_busParamCommand(nullptr),
    m_requestType(REQUEST_TYPE_NONE)
{
    m_commandHandler = pCommandHandler;
}

/*  ----------------------------------------------------------------------------
    Destructor
*/
CLRBusParamCommand::~CLRBusParamCommand()
{
    if (nullptr != m_busParamCommand)
    {
        delete m_busParamCommand;
        m_busParamCommand = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Processses CLR command
*/
int CLRBusParamCommand::processCommand(const std::string& userCmndStr)
{
    if ((0 == userCmndStr.compare("help")) ||
        (0 == userCmndStr.compare("h")))
    {
        printCommand();
    }
    else if ((0 == userCmndStr.compare("back")) ||
             (0 == userCmndStr.compare("b")))
    {
        m_commandHandler->setCommandState(PROTOCOL_INITIALIZED);
    }
    else
    {
        if ((0 == userCmndStr.compare("get busparams")) ||
            (0 == userCmndStr.compare("g bp")))
        {
            createBusParamCmndInstance();

            if (m_busParamCommand)
            {
#ifndef ENABLE_ASYNC_COMMAND_SUPPORT    
                Json::Value jsonData = CommonProtocolManager::getInstance()->getBusParameters();
                m_busParamCommand->processCommandResponse(REQUEST_TYPE_GET, jsonData);
#else
                m_commandID = CMD_GET_BUS_PARAMETERS_ID;
                m_requestType = REQUEST_TYPE_GET;
                Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_GET_BUS_PARAMETERS_ID, "GetBusParameters", "");
                CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, "");
                m_commandHandler->acquireSemaphoreLock();
#endif
            }
        }
        else
        {
            std::vector<std::string> setParam;
            std::string token;
            std::stringstream ss(userCmndStr);
            
            while (ss >> token)
                    setParam.push_back(token);

            if ((3 == setParam.size()) &&
                (0 == setParam.at(0).compare("s")) &&
                (0 == setParam.at(1).compare("bp")))
            {
                std::istringstream iss(setParam.at(2));
                std::vector<std::string> paramValue;
                while (std::getline(iss, token, ':'))
                        paramValue.push_back(token);

                if (2 != paramValue.size())
                {
                    LOGF_ERROR (CLR, "Invalid command for setting bus parameters");
                    return CLR_FAILURE;
                }
        
                createBusParamCmndInstance();

                if (m_busParamCommand)
                {
                    m_busParamCommand->setBusParamData(paramValue.at(0), Json::Value(atoi(paramValue.at(1).c_str())));
#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
                    Json::Value jsonData = CommonProtocolManager::getInstance()->setBusParameters(m_busParamCommand->getRequestData(REQUEST_TYPE_SET));
                    m_busParamCommand->processCommandResponse(REQUEST_TYPE_SET, jsonData);
#else
                    m_commandID = CMD_SET_BUS_PARAMETERS_ID;
                    m_requestType = REQUEST_TYPE_SET;    
                    Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_SET_BUS_PARAMETERS_ID, "SetBusParameters", m_busParamCommand->getRequestData(REQUEST_TYPE_SET));
                    CLRCommandLogger::LOG_COMMAND(m_commandID, REQUEST, m_busParamCommand->getRequestData(REQUEST_TYPE_SET));
                    m_commandHandler->acquireSemaphoreLock();
#endif
                }
            }
        }
    }

    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Process Async Command Response
*/
int CLRBusParamCommand::processAsyncCommandResponse(const Json::Value& jsonData)
{
   CLRCommandLogger::LOG_COMMAND(m_commandID, RESPONSE, jsonData);   

   Json::Value jsonReply;

   if (jsonData.isMember("result"))
       jsonReply = jsonData["result"];
   else if (jsonData.isMember("error"))
       jsonReply = jsonData["error"];

    m_busParamCommand->processCommandResponse(m_requestType, jsonReply);
    m_commandHandler->releaseSemaphoreLock();
}

/*  ----------------------------------------------------------------------------
    Get corresponding bus param instance
*/
void CLRBusParamCommand::createBusParamCmndInstance()
{
    switch (m_commandHandler->getProtocolType())
    {
        case PROTOCOL_TYPE_FF:
            {
                if (nullptr == m_busParamCommand)
                    m_busParamCommand = new CLRFFBusParamCommand(m_commandHandler);
            }
            break;

        default:
            break;
    }
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
void CLRBusParamCommand::printCommand()
{   
    cout << "----------- Available Commands -----------" << endl;
    cout << "get busparams (g bp)                       - Get bus parameter for the selected protocol" << endl;
    cout << "set busparams (s bp (<param_name:value>)   - Set specific bus parameters. For parameter name please check in wiki and provide it accordingly" << endl;
    cout << "                                              Wiki Link: https://flukept.atlassian.net/wiki/display/DP/SetBusParameters" << endl;
    cout << "back (b)                                   - Go back to the previous command list" << endl;
}
