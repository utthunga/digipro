/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRConnectCommand class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include "CLRConnectCommand.h"
#include "CLRFFConnectCommand.h"
#include "CommonProtocolManager.h"
#include "MenuTestInterface.h"
#include <iomanip>
#include <flk_log4cplus/Log4cplusAdapter.h>

/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructor
*/
CLRConnectCommand::CLRConnectCommand(CLRCommandHandler* pCommandHandler):
    m_connectCommand(nullptr)
{
    m_commandHandler = pCommandHandler;
}

/*  ----------------------------------------------------------------------------
    Destructor
*/
CLRConnectCommand::~CLRConnectCommand()
{
    if (nullptr != m_connectCommand)
    {
        delete m_connectCommand;
        m_connectCommand = nullptr;
    }
}

void CLRConnectCommand::processConnectResponse(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (jsonData.isMember("result"))
        jsonResult = jsonData["result"];
    else
        jsonResult = jsonData["error"];
    
    if (CLR_SUCCESS == m_connectCommand->processCommandResponse(REQUEST_TYPE_NONE, jsonResult))
    {
        MenuTestInterface menuTestIntf;
        if (TESTINTF_SUCCESS == menuTestIntf.parseResponseData(REQUEST_TYPE_NONE, jsonResult))
        {
            cout << menuTestIntf.getItemLabel() << ":"  << endl;
            cout << setfill('-') << setw(1) << "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << endl;
            cout << setfill(' ') << setw(1) << "|" << setw(15) << left << "ID" << setw(1) << "|" << setw(15) << left << "LABEL" << setw(1) << "|" << setw(15) << left << "Type" << setw(1) << "|" << endl;
            cout << setfill('-') << setw(1) << "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << endl;
            for (int nIndex = 0; nIndex < menuTestIntf.getMenuItemCount(); nIndex++)
            {
                ItemBasicTestInterface itemIntf;
                std::string itemType;
                if (TESTINTF_SUCCESS == itemIntf.parseResponseData(REQUEST_TYPE_NONE, menuTestIntf.getMenuItemListInfo(nIndex, itemType)))
                {
                    cout << setfill(' ') << setw(1) << "|" << setw(15) << left << nIndex << " : " << itemIntf.getItemID() << setw(1) << "|" << setw(15) << left <<  itemIntf.getItemLabel() << setw(1) << "|" << setw(15) << left << itemIntf.getItemType() << setw(1) << "|" << endl;
                    cout << setfill('-') << setw(1) << "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << endl;
                }
            }
            cout << setfill('-') << setw(1) << "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << setw(15) << "-" << setw(1) <<  "+" << endl;
        }

        cout << "Connected successfully." << endl;
        m_commandHandler->setCommandState(PROCESS_ITEM);
        m_commandHandler->pushMenu(jsonResult);
    }
}

/*  ----------------------------------------------------------------------------
    Processses CLR command
*/
int CLRConnectCommand::processCommand(const std::string& userCmndStr)
{
    if ((0 == userCmndStr.compare("help")) ||
        (0 == userCmndStr.compare("h")))
    {
         printCommand();
    }
    else 
    {
        createConnectCmndInstance();

        if (CLR_SUCCESS == m_connectCommand->processCommand(userCmndStr))
        {
            cout << "Connecting to device....." << endl;
#ifndef ENABLE_ASYNC_COMMAND_SUPPORT
            Json::Value jsonData = CommonProtocolManager::getInstance()->connect(m_connectCommand->getRequestData(REQUEST_TYPE_NONE));
            processConnectResponse (jsonData);
#else
            Json::Value jsonResult = m_commandHandler->executeAsyncCommand (CMD_CONNECT_ID, "Connect", m_connectCommand->getRequestData(REQUEST_TYPE_NONE));
            CLRCommandLogger::LOG_COMMAND(CMD_CONNECT_ID, REQUEST, m_connectCommand->getRequestData(REQUEST_TYPE_NONE));    
            m_commandHandler->acquireSemaphoreLock();
#endif
        }
    }

    return CLR_SUCCESS;
}

int CLRConnectCommand::processAsyncCommandResponse(const Json::Value& jsonData)
{
    CLRCommandLogger::LOG_COMMAND(CMD_CONNECT_ID, RESPONSE, jsonData);
    processConnectResponse(jsonData);
    m_commandHandler->releaseSemaphoreLock();
}

/*  ----------------------------------------------------------------------------
    Get corresponding bus param instance
*/
void CLRConnectCommand::createConnectCmndInstance()
{
    switch (m_commandHandler->getProtocolType())
    {
        case PROTOCOL_TYPE_FF:
            {
                if (nullptr == m_connectCommand)
                    m_connectCommand = new CLRFFConnectCommand(m_commandHandler);
            }
            break;

        default:
            break;
    }
}

/*  ----------------------------------------------------------------------------
    Processses CLR command response
*/
void CLRConnectCommand::printCommand()
{
    createConnectCmndInstance();
   
    cout << "----------- Available Commands -----------" << endl;
    if (nullptr != m_connectCommand)
        m_connectCommand->printCommand();
    cout << "back (b)           - Go back to the previous command list" << endl;
}
