/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Contains macro and enum definition that can be used within test lib
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* ===========================================================================*/
/// Indicates success case for any module that is out of PM scope
#define CLR_SUCCESS    0
/// Indicates failure case for any module that is out of PM scope
#define CLR_FAILURE    -1

/// Enumeration defines the Request type required for test lib
enum CLR_COMMAND_STATE
{
    /// Protocol states within CLR
    PROTOCOL_UNINITIALIZED,  ///< Protcol is not yet initialized
    PROTOCOL_READY_TO_INITIALIZE, ///< Protocol has been set within CPM
    PROTOCOL_INITIALIZED, ///< Protocol has been initialized

    /// Scan states within CLR
    SCAN_TRIGGER, ///< Trigger the scan operation
    SCAN_STARTED, ///< Scan operation has been started
    SCAN_STOPPED, ///< Stop operation has been triggered from user

    /// Bus paramater configuration states within CLR
    BUS_PARAM_CONFIG, ///< Bus parameter configuration has been triggered

    /// Connect state for the device
    CONNECT_DEVICE, ///< Connect and disconnect operation       
    PROCESS_ITEM    ///< After Connect, ready for get and set item and disconnection
};

/// Enumeration for the set of protocols that is supported wihtin CLR
enum PROTOCOL_TYPE
{
    PROTOCOL_TYPE_NONE = 0,
    PROTOCOL_TYPE_FF,
    PROTOCOL_TYPE_HART,
    PROTOCOL_TYPE_PB
};

/// Specifies the type of communication
enum COMM_TYPE
{
    REQUEST = 0,
    RESPONSE,
    NOTIFY
};
