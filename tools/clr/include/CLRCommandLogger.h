/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface for CLR command class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "clrdefs.h"
#include <json/json.h>
#include <iostream>
#include <fstream>    
#include <string>
#include <sys/time.h>

using namespace std;
/* ========================================================================== */
/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRSetProtocolCommand
    
    This class handles protocol set request and response from CPM
*/
class CLRCommandLogger
{
public:
    /// Holds whether the protocol is ready to be initialized
    static void LOG_COMMAND(const int& commandID, const int& communicationType, const Json::Value& value)
    {
        timeval curTime;
        gettimeofday(&curTime, NULL);
        int milli = curTime.tv_usec / 1000;

        char buffer [80];
        strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));
        char currentTime[84] = "";
        sprintf(currentTime, "%s:%d", buffer, milli);

        ofstream clr_comm_log_file("clr_comm.log", std::ios_base::out | std::ios_base::app);
        
        string commType("REQUEST");

        if (RESPONSE == communicationType)
            commType = "RESPONSE";
        else if (NOTIFY == communicationType)
            commType = "NOTIFY";
        
        clr_comm_log_file << "[" << currentTime << "]: " << " CLR Command - CommandID: " << commandID << ", Comm. Type: " << commType  << ", Param: " << value << endl;
        if ((RESPONSE == communicationType) || (NOTIFY == communicationType))
            clr_comm_log_file << "-----------------------------------------------------------------------------------------------------------------------------------" << endl;
    }
};
