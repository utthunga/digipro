/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <string>
#include <mutex>
#include <condition_variable>
#include "json/json.h"
#include "ICLRCommand.h"
#include "ICommandHandler.h"
#include "CommonProtocolManager.h"

using namespace std;

/* ========================================================================== */
/* Macros definition **********************************************************/

/* ========================================================================== */
/* CLASS DECLARATIONS *********************************************************/
/** class description of CLRCommandHandler 

    CLRCommandHandler acts as an interface layer between ProStar application 
    and User.
    This inteface will be using Fluke IDC team for development purpose
*/
class CLRCommandHandler : public ICommandHandler
{
public:

   /* Construction and Destruction ==========================================*/

   /*------------------------------------------------------------------------*/
   /** Constructs CLRCommandHandler Class.
   */
   CLRCommandHandler();

   /*------------------------------------------------------------------------*/
   /** virtual destructor, destroyes CLRCommandHandler class instance.
   */
   virtual ~CLRCommandHandler() = default;


   /* Public Member Functions ===============================================*/

   /*------------------------------------------------------------------------*/
   /** Initializes CLRCommandHandler with the below scope

       1. Start DBus service for the ProStar application with URI as
          DBUS_CPM_URI and object name as "/cpmcommand"
            
       2. Register the required commands that can be accessed from outside
          world

       @return status of initialise function (SUCCESS/FAULRE)
   */
   int initialise();

   /*------------------------------------------------------------------------*/
   /** Shutdown CLRCommandHandler
       
       @return status of a shut down status (SUCCESS/FAILURE)
   */
   int shutDown();

   /*------------------------------------------------------------------------*/
   /**  Add publisher to the puiblisher table

        @param  publisherName,publisher name that needs to be added to the 
                             publisher table maintained by this command
                             handler
        @return status of adding publisher to the table (SUCCESS/FAILURE)
   */
   int addPublisher (std::string& publisherName);

   /*------------------------------------------------------------------------*/
   /**  Remove publisher from the publisher table

        @param publisherName,publisher name that needs to be removed from the 
                             publisher table
        @return status of removing publisher from the table (SUCCESS/FAILURE)
   */
   int removePublisher (std::string& publisherName);

   /*------------------------------------------------------------------------*/
   /**  Publish data to the subscribed clients via DBus channel

        @param  publisherName,publisher name to which the data to be sent
        @param  data,data that need to be published to the subscribers
        @return status of removing publisher from the table (SUCCESS/FAILURE)
   */
   int publishData(std::string& publisherName, Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Handle command proviced by the user 
        
        @param userCmndStr, process user input
    */
    int handleCommand(const std::string& userCmndStr);

    /*------------------------------------------------------------------------*/
    /** Set the protocol type required for further processing
        
        @param protoclType, holds the current protocol
    */
    void setProtocolType(int protocolType);

    /*------------------------------------------------------------------------*/
    /** Get the protocol type required for further processing
    */
    int getProtocolType();

    /*------------------------------------------------------------------------*/
    /** Set the command state 
        
        @param commandState,holds the command state
    */
    void setCommandState(int commandState);

    /*------------------------------------------------------------------------*/
    /** Set the command state 
    */
    int getCommandState();

    /** Destroy the command instance
    */
    void destroyCommandInstance();

    /*------------------------------------------------------------------------*/
    /** Acquire semaphore lock to synchronize data between threadds
    */
    void acquireSemaphoreLock();

    /*------------------------------------------------------------------------*/
    /** Release semaphore lock
    */
    void releaseSemaphoreLock();

    /*------------------------------------------------------------------------*/
    /** Acquire command lock
    */
    void acquireCommandLock();

    /*------------------------------------------------------------------------*/
    /** Release command lock
    */
    void releaseCommandLock();
   
    /* -----------------------------------------------------------------------*/
    /**   Push Menu to Satck
    */
    void pushMenu(const Json::Value &);

    /* -----------------------------------------------------------------------*/
    /** Pop Menu from Satck
    */
    void popMenu();

    /* -----------------------------------------------------------------------*/
    /** Get Current Menu
    */
    void getCurrentMenu(Json::Value &);
    
    /* -----------------------------------------------------------------------*/
    /** Returns Stack Size
    */
    int getStackSize();

    /* -----------------------------------------------------------------------*/
    /** Cleans up the menu Stack upon Disconnect
    */
    void clearMenuStack();

    /* -----------------------------------------------------------------------*/
    /** Execute Async command with the help of Common Protocol Manager 
    */
    Json::Value executeAsyncCommand(const uint32_t& commandID, const std::string& commandName,
                                    const Json::Value& jsonReqData);

private:

    /* Private Data Members =================================================*/

    /// Holds the current active command
    ICLRCommand* m_clrCommand;

    /// Maintain a stack fo screens
    std::stack<Json::Value> m_menuStack;

    /// Holds the current active state of command handler
    int m_clrCommandState;

    /// Holds currently active protocol
    int m_protocolType;

    /// Holds mutex lock for aynchronous events (mainly for notifying)
    std::mutex m_mutex;

    /// Holds the command lock
    std::mutex m_clrCommandLock;

    /// Holds condition variable for asynchronous events
    std::condition_variable m_condVarLock;

    /// Holds the mutex command lock state
    bool m_clrCmnLckAcquired;

    /// Holds variable value for condition lock
    bool m_condVar;
};
