/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://adeshsoni@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
     command Line renderer class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#include <iostream>
#include "CPMUtil.h"
#include "CLRCommandHandler.h"
#include <flk_log4cplus/Log4cplusAdapter.h>

using namespace std;

int main()
{
    /// Initialize the logging mechanism
    log4cplus::Log4cplusAdapter logAdapter;

    /// Initializes the required modules for the clr application
    CPMUtil::initCPM();

    /// Create a new instance of CLR command handler
    CLRCommandHandler* pCLRCommandHandler = new CLRCommandHandler();

    if (nullptr != pCLRCommandHandler)
    {
        /// Initialize the CLR command handler instance
        pCLRCommandHandler->initialise();

        /// Holds user input command request
        std::string userCmndStr;

        cout << "!!! Type 'help' or 'h' to get more information for the available commands !!! " << endl;
        cout << "!!! User 'quit' or 'q' to exit from clr terminal !!! " << endl;

        do
        {
            cout << "[clr terminal] > ";

            /// Get user input from terminal
            std::getline(std::cin, userCmndStr);

            if ((0 == userCmndStr.compare("quit")) || 
                (0 == userCmndStr.compare("q")))
                break;

            pCLRCommandHandler->handleCommand (userCmndStr);

        } while (true);

        /// Initialize the CLR command handler instance
        pCLRCommandHandler->shutDown();
    }

	return 0;
}

