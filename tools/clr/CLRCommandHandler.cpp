/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CLRCommandHandler class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <clrdefs.h>
#include <iostream>
#include "CLRCommandHandler.h"
#include "CLRSetProtocolCommand.h"
#include "CLRScanCommand.h"
#include "CLRBusParamCommand.h"
#include "CLRConnectCommand.h"
#include "CLRProcessItemCommand.h"
#include <stack>
#include "flk_log4cplus_defs.h"

#define CLR Clr
/* ========================================================================== */
/* Constants and Macros *******************************************************/
using namespace std;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs CLRCommandHandler Class Instance
*/
CLRCommandHandler::CLRCommandHandler():
    m_clrCommand(nullptr),
    m_clrCommandState(-1),
    m_protocolType(PROTOCOL_TYPE_NONE),
    m_clrCmnLckAcquired(false),
    m_condVar(false)
{
}

/*  ----------------------------------------------------------------------------
    Initializes CLRCommandHandler 
*/
int CLRCommandHandler::initialise()
{   
    LOGF_INFO (CLR, "Initialize CLR command handler");
    
    // Register command handler object within CPM
    CommonProtocolManager::getInstance()->registerCommandHandler(this);
    
    m_clrCommandState = PROTOCOL_UNINITIALIZED;

    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Shutsdown CLRCommandHandler
*/
int CLRCommandHandler::shutDown()
{
    CommonProtocolManager::getInstance()->shutdown();
    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Add publishers to the publisher table
*/
int CLRCommandHandler::addPublisher(std::string& publisherName)
{
    LOGF_INFO (CLR, "Add publisher " << publisherName << " to the publisher list");

    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Remove publisher from the publisher table
*/
int CLRCommandHandler::removePublisher(std::string& publisherName)
{
    LOGF_INFO (CLR, "Remove publisher " << publisherName << " from the publisher list");

    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Remove publisher from the publisher table
*/
Json::Value CLRCommandHandler::executeAsyncCommand
(
    const uint32_t& commandID,
    const std::string& commandName, 
    const Json::Value& jsonReqData
)
{
    LOGF_INFO(CLR, "Executing command in async - commandID: " << commandID << ", commandName: " << commandName << ", jsonReqData: " << jsonReqData);

    Json::Value jsonResponse = CommonProtocolManager::getInstance()->executeCommandAsync(
                                          commandID, commandName, jsonReqData);

    LOGF_INFO(CLR, "Reply for the command: " << commandName << ", response: " << jsonResponse);    

    return jsonResponse;
}

/*  ----------------------------------------------------------------------------
    Handle command request from the user
*/
int CLRCommandHandler::handleCommand(const std::string& userCmndStr)
{
    switch (m_clrCommandState)
    {
         case PROTOCOL_UNINITIALIZED:
         case PROTOCOL_INITIALIZED:
            {
                if (nullptr == m_clrCommand)
                    m_clrCommand = new CLRSetProtocolCommand(this);

                m_clrCommand->processCommand(userCmndStr);

                if ((PROTOCOL_INITIALIZED != m_clrCommandState) && 
                    (PROTOCOL_UNINITIALIZED != m_clrCommandState))
                    destroyCommandInstance();
            }
            break;

        case SCAN_TRIGGER:
        case SCAN_STARTED:
        case SCAN_STOPPED:
            {
                acquireCommandLock();

                if (SCAN_TRIGGER == m_clrCommandState)
                {
                    destroyCommandInstance();
                    m_clrCommand = new CLRScanCommand(this);
                }

                if (m_clrCommand)
                    m_clrCommand->processCommand(userCmndStr);

                /// User like to stop the scan operation
                if (SCAN_STOPPED == m_clrCommandState)
                {
                    destroyCommandInstance();
                    m_clrCommandState = SCAN_TRIGGER;
                }
                
                /// User like to go the previous menu
                if (PROTOCOL_INITIALIZED == m_clrCommandState)
                    destroyCommandInstance();
            
                releaseCommandLock();
            }
            break;

        case BUS_PARAM_CONFIG:
            {
                if (nullptr == m_clrCommand)
                    m_clrCommand = new CLRBusParamCommand(this);

                m_clrCommand->processCommand(userCmndStr);
    
                /// User like to go the previous menu
                if (PROTOCOL_INITIALIZED == m_clrCommandState)
                    destroyCommandInstance();
            }
            break;

        case CONNECT_DEVICE:
            {
                if (nullptr == m_clrCommand)
                    m_clrCommand = new CLRConnectCommand(this);

                m_clrCommand->processCommand(userCmndStr);
                destroyCommandInstance();
            }
            break;
        case PROCESS_ITEM:
            {
                if (nullptr == m_clrCommand)
                {
                    m_clrCommand = new CLRProcessItemCommand(this);
                }
                // Forward the Command to process
                m_clrCommand->processCommand(userCmndStr);
            }
            break;

        default:
            break;
    }
}

/*  ----------------------------------------------------------------------------
    Find the publisher from the publisher table and notifies data to the 
    subscribed client
*/
int CLRCommandHandler::publishData(std::string& publisherName, Json::Value& data)
{
    Json::Value jsonData = data;

    if (publisherName == "CMD_RESP_PUBLISHER")
    {
        if (data.isMember("CommandName") && data.isMember("CommandResponse"))
        {
            jsonData = data["CommandResponse"];
            if (m_clrCommand)
                m_clrCommand->processAsyncCommandResponse(jsonData);
        }
    }
    else
    {
        acquireCommandLock();

        switch (m_clrCommandState)
        {
            case SCAN_STARTED:
                {
                    if (m_clrCommand)
                        m_clrCommand->processCommandResponse(REQUEST_TYPE_NONE, jsonData);
                }
                break;

            default:
                break;
        }

        releaseCommandLock();
    }

    return CLR_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sets command state
*/
void CLRCommandHandler::setCommandState(int commandState)
{
    m_clrCommandState = commandState;
}

/*  ----------------------------------------------------------------------------
    Get the command state
*/
int CLRCommandHandler::getCommandState()
{
    return m_clrCommandState;
}

/*  ----------------------------------------------------------------------------
    Set the protocol type
*/
void CLRCommandHandler::setProtocolType(int protocolType)
{
    m_protocolType = protocolType;
}

/*  ----------------------------------------------------------------------------
    Get the protocol type
*/
int CLRCommandHandler::getProtocolType()
{
    return m_protocolType;
}

/* ----------------------------------------------------------------------------
   Acquires lock
*/
void CLRCommandHandler::acquireSemaphoreLock()
{
    std::unique_lock<decltype(m_mutex)> lock(m_mutex);
    while (!m_condVar)
        m_condVarLock.wait(lock);
    m_condVar = false;
}

/* ----------------------------------------------------------------------------
   Release lock
*/
void CLRCommandHandler::releaseSemaphoreLock()
{
    std::unique_lock<decltype(m_mutex)> lock(m_mutex);
    m_condVar = true;
    m_condVarLock.notify_one();
}

/* ----------------------------------------------------------------------------
   Acquires lock
*/
void CLRCommandHandler::acquireCommandLock()
{
    if (false == m_clrCmnLckAcquired)
    {
        m_clrCommandLock.lock();
        m_clrCmnLckAcquired = true;
    }    
}

/* ----------------------------------------------------------------------------
   Release lock
*/
void CLRCommandHandler::releaseCommandLock()
{
    if (true == m_clrCmnLckAcquired)
    {
        m_clrCommandLock.unlock();
        m_clrCmnLckAcquired = false;
    }
}

/* ----------------------------------------------------------------------------
   Clean command instance
*/
void CLRCommandHandler::destroyCommandInstance()
{
    if (m_clrCommand)
    {
        delete m_clrCommand;
        m_clrCommand = nullptr;    
    }
}

/* ----------------------------------------------------------------------------
   Push Menu to Satck
*/
void CLRCommandHandler::pushMenu(const Json::Value & data)
{
    m_menuStack.push(data);
}

/* ----------------------------------------------------------------------------
   Pop Menu from Satck
*/
void CLRCommandHandler::popMenu()
{
    if (!m_menuStack.empty())
    {
        m_menuStack.pop();
    }
}

/* ----------------------------------------------------------------------------
   Get Current Menu
*/
void CLRCommandHandler::getCurrentMenu(Json::Value & menu)
{
    if (!m_menuStack.empty())
    {
        menu =  m_menuStack.top();
    }
}

/* ----------------------------------------------------------------------------
  Returns Stack Size
*/
int CLRCommandHandler::getStackSize()
{
    return m_menuStack.size();
}

/* -----------------------------------------------------------------------
  Cleans up the menu Stack upon Disconnect
*/
void CLRCommandHandler::clearMenuStack()
{
    while (!m_menuStack.empty())
    {
        m_menuStack.pop();
    }
}

