/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    StandardMetadata class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "IMetadata.h"
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string.h>
#include <sstream>
#include <algorithm>
#include <istream>
#include <vector>
#include "StandardMetadata.h"
#include "sqlite3.h"
#include "MetadataSQLite.h"

using namespace std;

// Below macros define the columns in the output CSV file containing metadata
#define HEADER 0
#define NUM_METADATA_ELEMENTS   6
#define MANUFACTURER_NAME       0
#define MANUFACTURER_ID         1
#define DEVICE_NAME             2
#define DEVICE_TYPE             3
#define DEVICE_REVISION         4
#define DD_REVISION             5

// Macros for finding data in CFF file
#define FOUND_ALL_ITEMS         0x0F
#define FOUND_DEVICE_NAME       0x01
#define FOUND_MANUFACTURER_NAME 0x02
#define FOUND_MANUFACTURER_ID   0x04
#define FOUND_DEVICE_TYPE       0x08

// Macros for finding Device Revision and DD Revision
#define DD_FILENAME_LENGTH  4
#define DD_REV_SIZE         2
#define DEV_REV_SIZE        2

// Macros for HEX value conversion
#define BASE 16

// Macros for metadata file names
#define FF_METADATA_FILE    "FFMetadata.csv"
#define DD_METADATA_FILE    "DDMetadata"

// Macros for file extensions
#define FILE_TYPE_FFO "ffo"
#define FILE_TYPE_FF5 "ff5"

// Macros for database tables
#define TABLE_FF    "FF"
#define TABLE_HART  "HART"

// Macros for database results
#define DB_SUCCESS 0

// Used for DEBUG Purpose only !!!
unsigned int g_count = 0;

/*  ----------------------------------------------------------------------------
    Instantiates the database interface class object
*/
StandardMetadata::StandardMetadata()
{
    m_pDB = new MetadataSQLite;
}
/*  ----------------------------------------------------------------------------
    Deletes the database interface class object
*/
StandardMetadata::~StandardMetadata()
{
    if (m_pDB != nullptr)
    {
        delete m_pDB;
        m_pDB = nullptr;
    }
}
/*  ----------------------------------------------------------------------------
    Initializes the protocol and DD Library path
*/
void StandardMetadata::initialise(MetadataRequest & request)
{
    this->m_ddlPath             = request.ddLibraryPath;
    this->m_protocol            = request.protocol;
    this->m_outputPath          = request.outputPath;
    this->m_hartMetadataFile    = request.hartMetadataFile;
}
/*  ----------------------------------------------------------------------------
    Generates Metadata
*/
STATUS StandardMetadata::generateMetadata()
{
    STATUS status = STATUS::SUCCESS;
    if (this->m_protocol == PROTOCOL::FF)
    {
        status = createMetadataForFF();
    }
    else if (this->m_protocol == PROTOCOL::HART)
    {
        status = createMetadataForHART();
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Creates Metadata for FF DD's
*/
STATUS StandardMetadata::createMetadataForFF()
{
    STATUS status = STATUS::SUCCESS;

    // Check if we have the handle to the database object
    if (m_pDB == nullptr)
        return STATUS::FAILURE;

    // Create a ouput stream object for writing data to CSV file
    ofstream outputFile;
    // Build File Path - Create the FF Metadata file in the output path
    string ffMetadataCSV = m_outputPath + "/" + FF_METADATA_FILE;
    // Opening the file for writing / appending the metadata for FF
    outputFile.open(ffMetadataCSV);

    if (outputFile.is_open())
    {
        // Insert the header
        outputFile << "Manufacturer Name, Manufacturer ID, Device Name / Model, Device Type, Device revision, DD Revision \n";
        // Read and store metadata to the file
        status = findDDFilesAndExtractMetadata(this->m_ddlPath, outputFile);
        // Close the output file
        outputFile.close();
    }
    else
    {
        cout << "Failed to create the FFMetadata.csv file" << endl;
        cout << "Check the Output Path in the metadata_settings.yaml file !!! " << endl;
        status = STATUS::FAILURE;
    }

    if (status == STATUS::SUCCESS)
    {
        // Read Metadata from CSV file and write to SQLite file
        ifstream inputFile;
        inputFile.open(ffMetadataCSV);

        if (inputFile.is_open())
        {
            int dbResult{};

            // Database filename
            string databaseFile = m_outputPath + "/" + DD_METADATA_FILE;

            // Open connection to the database
            dbResult = m_pDB->open(databaseFile);

            if (dbResult == DB_SUCCESS)
            {
                // Delete existing FF table if present and create a new FF Table
                dbResult = m_pDB->dropTable(TABLE_FF);
            }

            if (dbResult == DB_SUCCESS)
            {
                // Create the table FF
                dbResult = m_pDB->createTable(TABLE_FF);
            }

            if (dbResult == DB_SUCCESS)
            {
                 string line;
                // Skip the Header row - Read and do nothing
                getline(inputFile, line);

                // Now read all the rows and process the data
                while(getline(inputFile, line))
                {
                    // Fill Device Metadata structure
                    DeviceMetadata metadata;
                    fillDeviceMetadata(line, PROTOCOL::FF, metadata);

                    // Write device metadata to database
                    dbResult = m_pDB->writeData(&metadata);
                    if (dbResult != DB_SUCCESS)
                    {
                        cout << "Database Write Error !" << endl;
                        status = STATUS::FAILURE;
                        break;
                    }
                }
            }
            else
            {
                cout << "Database Operational Error !" << endl;
                status = STATUS::FAILURE;
            }
            // Close the database
            m_pDB->close();
            // Close the input file
            inputFile.close();
	    // Clean up the temporary file created
	    std::remove(ffMetadataCSV.c_str());
        }
        else
        {
            cout << "Failed to open the FFMetadata.csv file." << endl;
            status = STATUS::FAILURE;
        }
    }

    return status;
}
/*  ----------------------------------------------------------------------------
    Creates Metadata for HART DD's
*/
STATUS StandardMetadata::createMetadataForHART()
{
    ::g_count = 0; // To Be deleted
    STATUS status = STATUS::SUCCESS;

    // Check if we have the handle to the database object
    if (m_pDB == nullptr)
        return STATUS::FAILURE;

    // Read Metadata from CSV file and write to SQLite file
    ifstream inputFile;
    inputFile.open(m_hartMetadataFile);

    if (inputFile.is_open())
    {
        int dbResult{};

        // Database filename
        string databaseFile = m_outputPath + "/" + DD_METADATA_FILE;

        // Open connection to the database
        dbResult = m_pDB->open(databaseFile);

        if (dbResult == DB_SUCCESS)
        {
            // Delete existing HART table if present and create a new FF Table
            dbResult = m_pDB->dropTable(TABLE_HART);
        }

        if (dbResult == DB_SUCCESS)
        {
            // Create a new table for HART
            dbResult = m_pDB->createTable(TABLE_HART);
        }

        if(dbResult == DB_SUCCESS)
        {
            // Read the metadata information from the CSV file and store to database

            string line;
            // Skip the Header row - Read and do nothing
            getline(inputFile, line);

            // Now read all the rows and process the data
            while(getline(inputFile, line))
            {
                // Remove comma's if any in the Manufacturer name Ex: Harold Beck and Sons, Inc.
                size_t pos1 = line.find_first_of(",");
                size_t pos2 = line.find("0x", pos1);

                if (pos1 != string::npos && ((pos2 - pos1) > 1))
                {
                    // Manufacturer's name contains a ',' - erase it
                    line.erase(pos1, 1);

		    // Remove the double quotes
		    line.erase(std::remove(line.begin(), line.end(), '"'), line.end());
                }

                // Fill Device Metadata structure
                DeviceMetadata metadata;
                fillDeviceMetadata(line, PROTOCOL::HART, metadata);

                // Write device metadata to database
                dbResult = m_pDB->writeData(&metadata);
                if (dbResult != DB_SUCCESS)
                {
                    cout << "Database Write Error !" << endl;
                    status = STATUS::FAILURE;
                    break;
                }
            }
        }
        else
        {
            cout << "Database Operational Error !" << endl;
            status = STATUS::FAILURE;
        }
        // Close the database
        m_pDB->close();
        // Close the input file
        inputFile.close();
    }
    else
    {
        cout << "Failed to open the HART Metadata CSV file - " << m_hartMetadataFile << endl;
        cout << "Check the HART metadata CSV filename in metadata_settings.yaml file !!! " << endl;
        status = STATUS::FAILURE;
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Recursively searches the FF DD files in the given Path
*/
STATUS StandardMetadata::findDDFilesAndExtractMetadata(string path, ofstream & outputFile)
{
    DIR * d = opendir(path.c_str()); // open the directory
    if(d == NULL)
    {
        cout << "FAILURE : Invalid or Wrong Directory Path" << endl;
        cout << "Check FF DD library Path in the metadta_settings.yaml file !!! " << endl;
        return STATUS::FAILURE; // return
    }

    struct dirent * dir; // for the directory entries
    string filename;
    vector<string> files; // store the filenames within a directory for finding out duplicates

    while ((dir = readdir(d)) != NULL)
    {
        if(dir-> d_type != DT_DIR)
        {
            // Reset the file name
            filename.clear();
            filename = dir->d_name;

            // Check for only .FFO & .FF5 files and skip the remaining
            string fileType = getFileExt(filename);
            if ((fileType == FILE_TYPE_FFO) || (fileType == FILE_TYPE_FF5))
            {
                bool writeToFile = true;
                if (!files.empty())
                {
                    for (auto it:files)
                    {
                        if (it.compare(filename.substr(0, DD_FILENAME_LENGTH)) == 0)
                        {
                            // Skip this entry to avoid duplicates
                            writeToFile = false;
                            break;
                        }
                    }
                }
                // Store to CSV file
                if (writeToFile)
                {
                    // Store the data to file
                    storeFFMetadataToCSVFile(path, filename, outputFile);
                    // Store the file name without extension
                    files.push_back(filename.substr(0, DD_FILENAME_LENGTH));
                }
            }
        }
        else if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".")!=0 &&
                strcmp(dir->d_name,"..")!=0 && strcmp(dir->d_name,"HCF")!=0 && strcmp(dir->d_name,"FF")!=0) // if it is a directory
        {
            files.clear(); // Reset the vector
            string directoryPath = path + "/" + dir->d_name;
            findDDFilesAndExtractMetadata(directoryPath, outputFile); // recall with the new path
        }
    }
    closedir(d); // finally close the directory

    // return
    return STATUS::SUCCESS;
}
/*  ----------------------------------------------------------------------------
    Reads FF Metadata information and stores it to FFMetadata.csv file
*/
void StandardMetadata::storeFFMetadataToCSVFile(string path, const string& filename, ofstream & outputFile)
{
    // Store all the metadata information in a string array
    string data[NUM_METADATA_ELEMENTS];

    // Search for CFF file and read the following common information
    DIR *dir = opendir(path.c_str());
    if(dir)
    {
        dirent *entry;

        while((entry = readdir(dir))!= NULL)
        {
            if(has_suffix(entry->d_name, ".cff") || has_suffix(entry->d_name, ".CFF"))
            {
                // Open cff file and read the below info
                ifstream input;
                size_t pos;
                string line;
                string file = path + "/" + entry->d_name;

                // Open the cff file to read the required parameters
                input.open(file);

                if(input.is_open())
                {
                    unsigned  char foundItems{0};
                    while(getline(input, line))
                    {
                        // If all the required information is found exit the loop and close the file
                        if (foundItems == FOUND_ALL_ITEMS)
                            break;

                        // Ignore if the line is a comment
                        string comment = "//";
                        if (line.compare(0, comment.length(), comment) == 0)
                            continue;

                        // Search for the Device Name
                        pos = line.find("DeviceName");
                        if(pos != string::npos) // string::npos is returned if string is not found
                        {
                            auto index = line.find_last_of('=');
                            data[DEVICE_NAME] = line.substr(++index);

                            // Remove comments if any in the Device Name
                            removeComments(data[DEVICE_NAME]);

                            // Remove comma's if any in the Device name Ex: TB82EC Conductivity, Concentration Transmitter
                            findAndReplace(data[DEVICE_NAME], ",", "");

                            // Update the found items entry
                            foundItems |= FOUND_DEVICE_NAME;
                            continue;
                        }
                        // Search for Manufacturer Name
                        pos = line.find("VendorName");
                        if(pos != string::npos) // string::npos is returned if string is not found
                        {
                            auto index = line.find_last_of('=');
                            data[MANUFACTURER_NAME] = line.substr(++index);

                            // Remove comments if any in the manufacturer Name
                            removeComments(data[MANUFACTURER_NAME]);

                            // Remove comma's if any in the Vendor name Ex: Micromotion Motion, INC
                            findAndReplace(data[MANUFACTURER_NAME], ",", "");

                            // Update the found items entry
                            foundItems |= FOUND_MANUFACTURER_NAME;
                            continue;
                        }
                        // Search for Manufacturer ID
                        pos = line.find("MANUFAC_ID");
                        if(pos != string::npos) // string::npos is returned if string is not found
                        {
                            auto index = line.find_last_of('=');
                            data[MANUFACTURER_ID] = line.substr(++index);

                            // Remove comments if any in the manufacturer ID
                            removeComments(data[MANUFACTURER_ID]);

                            // Update the found items entry
                            foundItems |= FOUND_MANUFACTURER_ID;
                            continue;
                        }
                        // Search for Device Type
                        pos = line.find("DEV_TYPE");
                        if(pos != string::npos) // string::npos is returned if string is not found
                        {
                            auto index = line.find_last_of('=');
                            data[DEVICE_TYPE] = line.substr(++index);

                            // Remove comments if any in the device type
                            removeComments(data[DEVICE_TYPE]);

                            foundItems |= FOUND_DEVICE_TYPE;
                            continue;
                        }
                    }
                    input.close();
                }
                break;
            }
        }
    }

    // Write to the output file only if CFF exists and the required information is read
    if (!data[MANUFACTURER_NAME].empty())
    {
        // Extract the device revision and file revision from the file name
        data[DEVICE_REVISION] = filename.substr(0, DEV_REV_SIZE); // read the 1st two characters
        data[DD_REVISION] = filename.substr(DEV_REV_SIZE, DD_REV_SIZE); // read the next 2 characters

        // Remove double quotes from Manufacturer Name & Device Name
        string manufacturerName = data[MANUFACTURER_NAME];
        string deviceName = data[DEVICE_NAME];

        manufacturerName.erase( remove(manufacturerName.begin(), manufacturerName.end(), '\"'), manufacturerName.end() );
        deviceName.erase( remove(deviceName.begin(), deviceName.end(), '\"'), deviceName.end() );

        // Set the value back to corresponding strings
        data[MANUFACTURER_NAME] = manufacturerName;
        data[DEVICE_NAME] = deviceName;

        // Finally write the data to the output file
        for (auto it : data)
        {
            // Remove carriage return from the string value
            if (!it.empty() && it[it.size() - 1] == '\r')
                it.erase(it.size() - 1);

            // Write the value to the file
            outputFile << it << ",";
        }
        outputFile << "\n";
    }
}
/*  ----------------------------------------------------------------------------
    Returns the file extension of the given file
*/
string StandardMetadata::getFileExt(const string & filename)
{
   size_t i = filename.rfind('.', filename.length());
   if (i != string::npos)
   {
      return(filename.substr(i+1, filename.length() - i));
   }
   return("");
}
/*  ----------------------------------------------------------------------------
    Checks for the given suffix in a file name
*/
bool StandardMetadata::has_suffix(const string & filename, const string& suffix)
{
    return (filename.size() >= suffix.size()) && equal(suffix.rbegin(), suffix.rend(), filename.rbegin());
}
/*  ----------------------------------------------------------------------------
    Populates the metadata structure with the information available in the given string
*/
void StandardMetadata::fillDeviceMetadata(string & line, PROTOCOL protocol, DeviceMetadata & metadata)
{
    // create a string stream for tokenising the data seperated by comma
    istringstream ss(line);
    vector<string> recordData;
    string token;
    unsigned index{0};

    // Read all the fields seperated by comma's
    while(getline(ss, token, ','))
    {
        recordData.push_back(token);
    }

    // For DEBUG purpose just printing the data on console !!!
    /* ++::g_count;
    cout << "Count = " << ::g_count << endl;
    cout << "Data = " << line << endl; */

    metadata.manufacturerName    = recordData.at(index++);    
    metadata.manufacturerID      = stoul(recordData.at(index++), nullptr, BASE);
    metadata.deviceName          = recordData.at(index++);
    metadata.deviceTypeCode      = stoul(recordData.at(index++), nullptr, BASE);
    metadata.deviceRevision      = stoul(recordData.at(index++), nullptr, BASE);
    metadata.ddRevision          = stoul(recordData.at(index++), nullptr, BASE);

    // Erase the leading white spaces in Manufacturer Name if any
    size_t pos = metadata.manufacturerName.find_first_not_of(" \t");
    if (pos != string::npos)
    {
   	 metadata.manufacturerName.erase(0, pos);
    }

    // Erase the leading white spaces in Device  Name if any
    pos = metadata.deviceName.find_first_not_of(" \t");
    if(pos != string::npos)
    {
	metadata.deviceName.erase(0, pos);
    }

    // Finally insert the key and update the protocol
    getKey(metadata);
    metadata.protocol = protocol;
}
/*  ----------------------------------------------------------------------------
    Generates a key for each record entry
*/
void StandardMetadata::getKey(DeviceMetadata & metadata)
{
    // get the decimal value of device type code
    auto deviceCode     = to_string(metadata.deviceTypeCode);
    auto manufacturerID = to_string(metadata.manufacturerID);
    auto deviceRevision = to_string(metadata.deviceRevision);
    auto ddRevision     = to_string(metadata.ddRevision);

    // Generate a string key
    metadata.key = manufacturerID + deviceCode + deviceRevision + ddRevision;
}
/**  --------------------------------------------------------------------------
    Finds and erases the comments - all data starting from "//" in the given string
*/
void StandardMetadata::removeComments(string & data)
{
    // Finds and erases the comments in a given string
    size_t position = data.find("//");
    if (position != string::npos)
    {
        data.erase(position, data.length() - position);
    }
}
/**  --------------------------------------------------------------------------
    Finds and replaces the search string with the given string
*/
void StandardMetadata::findAndReplace(std::string& subject, const std::string& search, const std::string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos)
    {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}
