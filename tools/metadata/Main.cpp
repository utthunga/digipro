/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Entry point to the application
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include <string>
#include "IMetadata.h"
#include "StandardMetadata.h"
#include "yaml-cpp/yaml.h"

using namespace std;

int main(int argc, char *argv[])
{
    string ffDDPath;
    string hartDDPath;
    string outputPath;
    string hartMetadataFile;

    if (argc <= 1)
    {
        cout << "Missing argument : Provide the metadata_settings.yaml file as an argument " << endl;
        cout << "FAILURE : Metadata not generated !!!" << endl;
        return -1;
    }

    // base Node for Configuration file
    YAML::Node baseNode;

    // Load configuration file
    try
    {
        // In case some junk value is entered for the argument
        baseNode = YAML::LoadFile(argv[1]);
    }
    catch(...)
    {
        cout << "Invalid argument : Provide the metadata_settings.yaml file as an argument " << endl;
        cout << "FAILURE : Metadata not generated !!!" << endl;
        return -1;
    }

    if (baseNode.IsNull())
    {
        cout << "Confguration File Not Found : metadata_settings.yaml" << endl;
        cout << "FAILURE : Metadata not generated !!!" << endl;
        return -1;
    }

    // Read the FF & HART Nodes
    YAML::Node ffNode   = baseNode["FF_METADATA_SETTINGS"];
    YAML::Node hartNode = baseNode["HART_METADATA_SETTINGS"];

    // Parse settings file for configuration information
    if (ffNode["ffDDPath"] && hartNode["hartDDPath"] && hartNode["hartMetadataFile"] && baseNode["outputPath"])
    {
        ffDDPath            = ffNode["ffDDPath"].as<string>();
        hartDDPath          = hartNode["hartDDPath"].as<string>();
        hartMetadataFile    = hartNode["hartMetadataFile"].as<string>();
        outputPath          = baseNode["outputPath"].as<string>();
    }
    else
    {
        cout << "Configuration File Incorrect - Missing attributes" << endl;
        cout << "FAILURE : Metadata not generated !!!" << endl;
        return -1;
    }

    // Create the Metadata Object
    IMetadata * pIMetadata = new StandardMetadata;

    // Initialise and generate Metadata
    if (pIMetadata != nullptr)
    {
        // Create a Metadata request Object
        MetadataRequest request;

        // FF Metadata
        cout << "Starting Metadata generation for FF ... " << endl;

        // Initialise Metadata request for FF
        request.ddLibraryPath   = ffDDPath;
        request.outputPath      = outputPath;
        request.protocol        = PROTOCOL::FF;

        // Initialise and Generate Metadata
        pIMetadata->initialise(request);
        if (pIMetadata->generateMetadata() != STATUS::SUCCESS)
        {
            cout << "Failed to Generate Metadata for FF" << endl;
            delete pIMetadata;
            return -1;
        }
        cout << "Metadata generation for FF Successful !!! " << endl;
        cout << "===========================================" << endl;

        // HART Metadata
        cout << "Starting Metadata generation for HART ... " << endl;

        // Initialise Metadata request for HART
        request.ddLibraryPath       = hartDDPath;
        request.protocol            = PROTOCOL::HART;
        request.hartMetadataFile    = hartMetadataFile;

        // Initialise and Generate Metadata
        pIMetadata->initialise(request);
        if (pIMetadata->generateMetadata() != STATUS::SUCCESS)
        {
            cout << "Failed to Generate Metadata for HART" << endl;
            delete pIMetadata;
            return -1;
        }

        cout << "Metadata generation for HART Successful !!!" << endl;
        cout << "===========================================" << endl;
        cout << "SUCCESS : Metadata generation Complete !!! " << endl;
    }

    // Clear in memory objects
    delete pIMetadata;

    return 0;
}
