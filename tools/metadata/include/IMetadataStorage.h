/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for Metadata Storage Interface class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef IMETADATASTORAGE_H
#define IMETADATASTORAGE_H

/* INCLUDE FILES **************************************************************/
#include <string>
#include "IMetadata.h"

class IMetadataStorage
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs IMetadataStorage Object
    */
    IMetadataStorage(){}

    /*-----------------------------------------------------------------------*/
    /** Destroys IMetadataStorage object
    */
    virtual ~IMetadataStorage(){}

    /// Interface functions to implemented by the derrived classes

    /*-----------------------------------------------------------------------*/
    /** Opens connection to the database

     @param filename - Database file to be created / opened

     @return staus of opening the database
    */
    virtual int open(const std::string & filename) = 0;

    /*-----------------------------------------------------------------------*/
    /** Closes connection to the database

     @return database closure status
    */
    virtual int close() = 0;

    /*-----------------------------------------------------------------------*/
    /** Reads data from the database

     @param metadata - Device metadata information

     @return staus of the read operation
    */
    virtual int readData(DeviceMetadata * metadata) = 0;

    /*-----------------------------------------------------------------------*/
    /** Writes metadata to the database

     @param metadata - Device metadata information

     @return staus of the write operation
    */
    virtual int writeData(DeviceMetadata * metadata) = 0;

    /*-----------------------------------------------------------------------*/
    /** Drops table from the database

     @param table - Name of the table in database

     @return staus of the database operation
    */
    virtual int dropTable(const std::string & table) = 0;

    /*-----------------------------------------------------------------------*/
    /** Creates table in the database

     @param table - Name of the table

     @return staus of the database operation
    */
    virtual int createTable(const std::string & table) = 0;
};

#endif // IMETADATASTORAGE_H
