/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition of Metadata SQLite class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef METADATASQLITE_H
#define METADATASQLITE_H

/* INCLUDE FILES **************************************************************/
#include "IMetadataStorage.h"
#include "sqlite3.h"

class MetadataSQLite : public IMetadataStorage
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs MetadataSQLite Object
    */
    MetadataSQLite();
    /*-----------------------------------------------------------------------*/
    /** Destroys MetadataSQLite object
    */
    ~MetadataSQLite();

    // Interface functions imlementation

    /*-----------------------------------------------------------------------*/
    /** Opens connection to the database

     @param filename - Database file to be created / opened

     @return staus of opening the database
    */
    virtual int open(const std::string & filename) override;

    /*-----------------------------------------------------------------------*/
    /** Closes connection to the database

     @return database closure status
    */
    virtual int close() override;

    /*-----------------------------------------------------------------------*/
    /** Reads data from the database

     @param metadata - Device metadata information

     @return staus of the read operation
    */
    virtual int readData(DeviceMetadata * metadata) override;

    /*-----------------------------------------------------------------------*/
    /** Writes metadata to the database

     @param metadata - Device metadata information

     @return staus of the write operation
    */
    virtual int writeData(DeviceMetadata * metadata) override;

    /*-----------------------------------------------------------------------*/
    /** Drops table from the database

     @param table - Name of the table in database

     @return staus of the database operation
    */
    virtual int dropTable(const std::string & table) override;

    /*-----------------------------------------------------------------------*/
    /** Creates table in the database

     @param table - Name of the table

     @return staus of the database operation
    */
    virtual int createTable(const std::string & table) override;

private:
    /// Handle to the SQLite database
    sqlite3 * m_pDBHandle;
};

#endif // METADATASQLITE_H
