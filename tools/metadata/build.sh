#!/bin/bash
#############################################################################
# Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org/flukept/digipro.git
# Author    : Vinay Shreyas
# Origin    : ProStar
#############################################################################

# Build make files
cmake CMakeLists.txt

# Run make
make

# Run the application
./metadata metadata_settings.yaml

