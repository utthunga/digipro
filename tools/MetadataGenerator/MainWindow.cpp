/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    MainWindow class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "MetadataManager.h"

#include <QCheckBox>
#include <QApplication>
#include <QFileDialog>
#include <QString>
#include <QMessageBox>

/*  ----------------------------------------------------------------------------
    Main Window constructor
*/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
/*  ----------------------------------------------------------------------------
    Main Window destructor
*/
MainWindow::~MainWindow()
{
    delete ui;
}
/*  ----------------------------------------------------------------------------
    Processes the Exit button click event
*/
void MainWindow::on_exit_clicked()
{
    MetadataManager::getInstance()->shutDown();
    exit(0);
}
/*  ----------------------------------------------------------------------------
    Processes the Generate Metadata button click event
*/
void MainWindow::on_generate_metadata_clicked()
{
    STATUS status;
    QMessageBox msgBox;

    // Generate Metadata function
    status = MetadataManager::getInstance()->generateMetadata();

    // Set the Message Box title to Error by default.
    msgBox.setWindowTitle("Error");

    switch (status)
    {
        case STATUS::SUCCESS:
        {
            msgBox.setText("Metadata generated successfully");
            msgBox.setWindowTitle("Success");
            msgBox.exec();
        }
        break;
        case STATUS::FAILURE:
        {
            msgBox.setText("Failed to generate metadata ! Failure could be due to one of the following reasons: \n"
                           "1. Incorrect DD Library Path.\n"
                           "2. HARTMetadata.csv filename is incorrect or is in the wrong folder.\n"
                           "3. SQLite3 is not installed.");
            msgBox.exec();
        }
        break;
        case STATUS::PROTOCOL_NOT_SET:
        {
            msgBox.setText("Protocol is not set !");
            msgBox.exec();
        }
        break;
        case STATUS::LIBRARY_PATH_NOT_SET:
        {
            msgBox.setText("DD Library Path is not set !");
            msgBox.exec();
        }
        break;
    case STATUS::FATAL_ERROR:
        {
            msgBox.setText("Fatal Error !");
            msgBox.exec();
        }
        break;
        default:
        break;
    }
}
/*  ----------------------------------------------------------------------------
    Processes Localization Clicked event
*/
void MainWindow::on_localization_clicked(bool checked)
{
    if (checked)
    {
        QMessageBox msgBox;
        msgBox.setText("Localization is not supported currently !");
        msgBox.setWindowTitle("Enable Localization");
        msgBox.exec();

        // Reset the localization option
        this->ui->localization->setCheckState(Qt::CheckState::Unchecked);
    }
}
/*  ----------------------------------------------------------------------------
    Processes the Protocol selection event
*/
void MainWindow::on_protocol_currentIndexChanged(int index)
{
    // Set the Protocol
    MetadataManager::getInstance()->setProtocol(index);

    if (index == static_cast<int>(PROTOCOL::HART))
    {
        QMessageBox msgBox;
        msgBox.setText("Make sure the Metadata information for HART (from FCG) is saved as a CSV file (HARTMetadata.csv) and stored in the output path !");
        msgBox.setWindowTitle("Warning");
        msgBox.exec();
    }
}
/*  ----------------------------------------------------------------------------
    Processes the Browse button click event for DD Library Path
*/
void MainWindow::on_browse_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    // Set the diectory path in UI
    this->ui->ddlPath->setText(dir);
    // Set the DD Library path
    MetadataManager::getInstance()->setDDLibraryPath(dir.toStdString());
}
/*  ----------------------------------------------------------------------------
    Processes the Text Edit event for DD Library Path
*/
void MainWindow::on_ddlPath_textEdited(const QString &arg1)
{
    this->on_browse_clicked();
}
/*  ----------------------------------------------------------------------------
    Processes the Text Edit event for Output Path
*/
void MainWindow::on_output_path_textEdited(const QString &arg1)
{
    this->on_browse_2_clicked();
}
/*  ----------------------------------------------------------------------------
    Processes the Browse button click event for Output Path
*/
void MainWindow::on_browse_2_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    // Set the diectory path in UI
    this->ui->output_path->setText(dir);
    // Set the output path for the database file to be written
    MetadataManager::getInstance()->setOutputPath(dir.toStdString());
}
