/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    FFMetadata class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "FFMetadata.h"

/*  ----------------------------------------------------------------------------
    FFMetadata Constructor
*/
FFMetadata::FFMetadata()
{

}
/*  ----------------------------------------------------------------------------
    FFMetadata Destructor
*/
FFMetadata::~FFMetadata()
{

}
/*  ----------------------------------------------------------------------------
    Initializes FFMetadata class object
*/
void FFMetadata::initialise(MetadataRequest & request)
{
    // Initialise all the required components of DD Services
}
/*  ----------------------------------------------------------------------------
    Generates DD Metadata for FF
*/
STATUS FFMetadata::generateMetadata()
{
    // Currently not implemented as support for Localisation
    // will be added POST Starsky MAP
    return STATUS::SUCCESS;
}
