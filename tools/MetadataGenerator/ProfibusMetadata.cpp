/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    ProfibusMetadata class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ProfibusMetadata.h"

/*  ----------------------------------------------------------------------------
    ProfibusMetadata Constructor
*/
ProfibusMetadata::ProfibusMetadata()
{

}
/*  ----------------------------------------------------------------------------
    ProfibusMetadata Destructor
*/
ProfibusMetadata::~ProfibusMetadata()
{

}
/*  ----------------------------------------------------------------------------
    Initializes ProfibusMetadata class object
*/
void ProfibusMetadata::initialise(MetadataRequest & request)
{
    // Initialise all the required components of Profibus DD Services
}
/*  ----------------------------------------------------------------------------
    Generates DD Metadata for Profibus
*/
STATUS ProfibusMetadata::generateMetadata()
{
    // Currently not implemented as support for Localisation
    // will be added POST Starsky MAP
    return STATUS::SUCCESS;
}
