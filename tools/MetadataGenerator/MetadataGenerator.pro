#-------------------------------------------------
#
# Project created by QtCreator 2018-03-06T09:14:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MetadataGenerator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    include/

SOURCES += \
    MetadataManager.cpp \
    Main.cpp \
    MainWindow.cpp \
    StandardMetadata.cpp \
    FFMetadata.cpp \
    HartMetadata.cpp \
    ProfibusMetadata.cpp \
    MetadataSQLite.cpp

HEADERS += \
    include/FFMetadata.h \
    include/HartMetadata.h \
    include/IMetadataStorage.h \
    include/LocalisedMetadata.h \
    include/MainWindow.h \
    include/MetadataManager.h \
    include/MetadataSQLite.h \
    include/ProfibusMetadata.h \
    include/sqlite3.h \
    include/StandardMetadata.h \
    include/IMetadata.h

FORMS += \
    MainWindow.ui

LIBS='-ldl'

unix:!macx: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lsqlite3

INCLUDEPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu

unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/libsqlite3.a
