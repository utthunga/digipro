/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    HARTMetadata class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "HartMetadata.h"

/*  ----------------------------------------------------------------------------
    HARTMetadata Constructor
*/
HARTMetadata::HARTMetadata()
{

}
/*  ----------------------------------------------------------------------------
    HARTMetadata Destructor
*/
HARTMetadata::~HARTMetadata()
{

}
/*  ----------------------------------------------------------------------------
    Initializes HARTMetadata class object
*/
void HARTMetadata::initialise(MetadataRequest & request)
{
    // Initialise all the required components of SDC-625
}
/*  ----------------------------------------------------------------------------
    Generates DD Metadata for HART
*/
STATUS HARTMetadata::generateMetadata()
{
    // Currently not implemented as support for Localisation
    // will be added POST Starsky MAP
    return STATUS::SUCCESS;
}
