/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for Localised Metadata base class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef LOCALISEDMETADATA_H
#define LOCALISEDMETADATA_H

/* INCLUDE FILES **************************************************************/
#include "IMetadata.h"

class LocalisedMetadata : public IMetadata
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs LocalisedMetadata Object
    */
    LocalisedMetadata() {}

    /*-----------------------------------------------------------------------*/
    /** Destroys LocalisedMetadata object
    */
    virtual ~LocalisedMetadata(){}

    // Interface functions to be overriden
    virtual void initialise(MetadataRequest) = 0;
    virtual STATUS generateMetadata() = 0;
};

#endif // LOCALISEDMETADATA_H
