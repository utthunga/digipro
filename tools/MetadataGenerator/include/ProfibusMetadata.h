/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for Profibus Metadata class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef PROFIBUSMETADATA_H
#define PROFIBUSMETADATA_H

/* INCLUDE FILES **************************************************************/
#include "LocalisedMetadata.h"

class ProfibusMetadata : public LocalisedMetadata
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs ProfibusMetadata Object
    */
    ProfibusMetadata();
    /*-----------------------------------------------------------------------*/
    /** Destroys ProfibusMetadata object
    */
    virtual ~ProfibusMetadata();

    /// Implementing the Interface functions
    /*-----------------------------------------------------------------------*/
    /** Initialization of ProfibusMetadata class members

     @param request - Contains initialization information like DD Library path, output file path and the protocol
    */
    void initialise(MetadataRequest & request) override;

    /*-----------------------------------------------------------------------*/
    /** Implementation of metadata generation for Profibus DD files

     @return Status of metadata generation for Profibus
    */
    STATUS generateMetadata() override;
};

#endif // PROFIBUSMETADATA_H
