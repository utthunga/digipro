/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for Main Window class in QT
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/* INCLUDE FILES **************************************************************/
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*-----------------------------------------------------------------------*/
    /** Constructs MainWindow Object
    */
    explicit MainWindow(QWidget *parent = 0);

    /*-----------------------------------------------------------------------*/
    /** Destroys MainWindow object
    */
    ~MainWindow();

private slots:

    /*-----------------------------------------------------------------------*/
    /** Button Click Event Handler for Exit

    */
    void on_exit_clicked();
    /*-----------------------------------------------------------------------*/
    /** Button Click Event Handler for Generate Metadata

    */
    void on_generate_metadata_clicked();
    /*-----------------------------------------------------------------------*/
    /** Sets Localization

     @param checked - Enable / Disable Localization
    */
    void on_localization_clicked(bool checked);
    /*-----------------------------------------------------------------------*/
    /** Returns the file extension of the given file

     @param index - Selected Protocol index
    */
    void on_protocol_currentIndexChanged(int index);
    /*-----------------------------------------------------------------------*/
    /** Browse button Click Event Handler for DD Library Path

    */
    void on_browse_clicked();
    /*-----------------------------------------------------------------------*/
    /** Text Edit Event Handler for setting DD Library Path

     @param arg1 - Output Directory Path
    */
    void on_ddlPath_textEdited(const QString & arg1);
    /*-----------------------------------------------------------------------*/
    /** Text Edit Event Handler for setting output directory path

     @param arg1 - Output Directory Path
    */
    void on_output_path_textEdited(const QString & arg1);
    /*-----------------------------------------------------------------------*/
    /** Browse button Click Event Handler for Output Path

    */
    void on_browse_2_clicked();

private:
    /// Main Window Handle
    Ui::MainWindow * ui;
};

#endif // MAINWINDOW_H
