/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for Metadata Manager class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef METADATAMANAGER_H
#define METADATAMANAGER_H

/* INCLUDE FILES **************************************************************/
#include "IMetadata.h"

// Metadata Manager class definition
class MetadataManager
{
private:
    /*-----------------------------------------------------------------------*/
    /** Constructor made Private for Metadata manager as there should only be
     * one instance of this class
    */
    MetadataManager();

    /*-----------------------------------------------------------------------*/
    /** Destroys MetadataManager object
    */
    ~MetadataManager();

    /// Data members
    /// Protocol Set
    PROTOCOL    m_protocol;
    /// Localisation Settings
    bool        m_localization;
    // DD Library Path
    std::string m_ddlPath;
    /// Output File Path
    std::string m_outputPath;

    /// Handle to IMetadata
    IMetadata * m_pIMetadata;

public:

    // Disable copy constructor and assignment operator
    /*-----------------------------------------------------------------------*/
    /** Disable assignment operator
    */
    MetadataManager& operator=(const MetadataManager& rhs) = delete;
    /*-----------------------------------------------------------------------*/
    /** Disable copy constructor
    */
    MetadataManager(const MetadataManager& rhs) = delete;

    /*-----------------------------------------------------------------------*/
    /** Returns the singleton instance of Metadata Manager

     @return Singleton instance of MetadataManager Class
    */
    static MetadataManager* getInstance();

    /*-----------------------------------------------------------------------*/
    /** Sets the Protocol

     @param index - Selected Protocol Index
    */
    void setProtocol(int index);

    /*-----------------------------------------------------------------------*/
    /** Enables Localization

     @param value - Enable / disable localization
    */
    void setLocalization(bool value);

    /*-----------------------------------------------------------------------*/
    /** Stores the DD Library Path

     @param ddlPath - DD Library Path
    */
    void setDDLibraryPath(std::string ddlPath);

    // Set the output path
    /*-----------------------------------------------------------------------*/
    /** Set the output directory path

     @param outputPath - Output Directory path
    */
    void setOutputPath(std::string outputPath);

    /*-----------------------------------------------------------------------*/
    /** Generate Metadata

     @return Status of Metadata generation
    */
    STATUS generateMetadata(void);

    /*-----------------------------------------------------------------------*/
    /** Shut down - Perform clean up

    */
    void shutDown(void);

private:
    /*-----------------------------------------------------------------------*/
    /** Initialise Metadata Manager

    */
    void initialise();
};

#endif // METADATAMANAGER_H
