/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    HART Metadata class definition
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef HARTMETADATA_H
#define HARTMETADATA_H

/* INCLUDE FILES **************************************************************/
#include "LocalisedMetadata.h"

class HARTMetadata : public LocalisedMetadata
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs HARTMetadata Object
    */
    HARTMetadata();

    /*-----------------------------------------------------------------------*/
    /** Destroys HARTMetadata object
    */
    virtual ~HARTMetadata();

    /// Implementing the Interface functions
    /*-----------------------------------------------------------------------*/
    /** Initialization of HARTMetadata class members

     @param request - Contains initialization information like DD Library path, output file path and the protocol
    */
    void initialise(MetadataRequest & request) override;

    /*-----------------------------------------------------------------------*/
    /** Implementation of metadata generation for HART DD files

     @return Status of metadata generation for HART
    */
    STATUS generateMetadata() override;
};

#endif // HARTMETADATA_H
