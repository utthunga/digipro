/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Definition for IMetadata interface class
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef IMETADATA_H
#define IMETADATA_H

/* INCLUDE FILES **************************************************************/
#include <string>

// Enum for differentiating Protocols
enum class PROTOCOL
{
    NONE = 0,
    FF,
    HART,
    PROFIBUS
};

// Enum for result of an operation
enum class STATUS
{
    SUCCESS = 0,
    FAILURE,
    PROTOCOL_NOT_SET,
    LIBRARY_PATH_NOT_SET,
    OUTPUT_PATH_NOT_SET,
    FATAL_ERROR
};

// Structure holds required information for Metadata generation
struct MetadataRequest
{
    PROTOCOL protocol;
    std::string   ddLibraryPath;
    std::string   outputPath;
};

// Interface structure for Metadata storage class
struct DeviceMetadata
{
    PROTOCOL        protocol;
    std::string     manufacturerName;
    std::string     deviceName;
    unsigned long   key;
    unsigned long   checksum;
    unsigned int    manufacturerID;
    unsigned int    deviceTypeCode;
    unsigned int    deviceRevision;
    unsigned int    ddRevision;

    DeviceMetadata()
    {
        key = 0;
        checksum = 0;
        protocol = PROTOCOL::NONE;
    }
};

// Metadata Interface definition
class IMetadata
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs IMetadata Object
    */
    IMetadata(){}

    /*-----------------------------------------------------------------------*/
    /** Destroys IMetadata object
    */
    virtual ~IMetadata(){}

    /// Interface functions to be overriden
    /*-----------------------------------------------------------------------*/
    /** Initialization of IMetadata class object

     @param request - Contains initialization information like DD Library path, output file path and the protocol
    */
    virtual void initialise(MetadataRequest & request) = 0;

    /*-----------------------------------------------------------------------*/
    /** Implementation of metadata generation

     @return Status of metadata generation
    */
    virtual STATUS generateMetadata() = 0;
};
#endif // IMETADATA_H
