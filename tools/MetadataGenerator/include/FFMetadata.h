/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    FF Metadata class definition
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef FFMETADATA_H
#define FFMETADATA_H

/* INCLUDE FILES **************************************************************/
#include "LocalisedMetadata.h"

class FFMetadata : public LocalisedMetadata
{
public:
    /*-----------------------------------------------------------------------*/
    /** Constructs FFMetadata Object
    */
    FFMetadata();

    /*-----------------------------------------------------------------------*/
    /** Destroys FFMetadata object
    */
    virtual ~FFMetadata();

    /// Implementing the Interface functions

    /*-----------------------------------------------------------------------*/
    /** Initialization of FFMetadata class members

     @param request - Contains initialization information like DD Library path, output file path and the protocol
    */
    void initialise(MetadataRequest & request) override;

    /*-----------------------------------------------------------------------*/
    /** Implementation of metadata generation for FF DD files

     @return Status of metadata generation for FF
    */
    STATUS generateMetadata() override;
};

#endif // FFMETADATA_H
