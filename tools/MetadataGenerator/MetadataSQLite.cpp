/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    MetadataSQLite class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "sqlite3.h"
#include "MetadataSQLite.h"
#include <iostream>

using namespace std;

// Macros for SQL query length
#define MIN_QUERY_LENGTH 256
#define MAX_QUERY_LENGTH 512
/*  ----------------------------------------------------------------------------
    MetadataSQLite construtor
*/
MetadataSQLite::MetadataSQLite()
{

}
/*  ----------------------------------------------------------------------------
    MetadataSQLite destrutor
*/
MetadataSQLite::~MetadataSQLite()
{

}
/*  ----------------------------------------------------------------------------
    Open connection to the database
*/
int MetadataSQLite::open(const string & fileName)
{
    // Open the database
    int status = sqlite3_open(fileName.c_str(), &m_pDBHandle);
    if (status != SQLITE_OK)
    {
        cout << "Failed to open the databse. Error Code = " << status << endl;
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Close connection to the database
*/
int MetadataSQLite::close()
{
    int status = sqlite3_close_v2(m_pDBHandle);
    if (status != SQLITE_OK)
    {
        cout << "Failed to close the database. Error Code = " << status << endl;
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Drop the table from the database
*/
int MetadataSQLite::dropTable(const std::string & tableName)
{
    int status{};
    char sql[MIN_QUERY_LENGTH]{};
    char * errMsg;

    // Construct the query
    sprintf (sql, "DROP TABLE IF EXISTS %s;", tableName.c_str());

    // Execute the query
    status = sqlite3_exec(m_pDBHandle ,sql, NULL, NULL, &errMsg);

    if (status != SQLITE_OK)
    {
        cout << "Failed to drop table. Error Code = " << status << ". Error Message : "<< errMsg << endl;
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Create the table in the database
*/
int MetadataSQLite::createTable(const std::string & tableName)
{
    int status{};
    char sql[MAX_QUERY_LENGTH]{};
    char * errMsg;

    // Construct the query
    sprintf (sql, "CREATE TABLE IF NOT EXISTS %s("
                  "ID INT PRIMARY        KEY      NOT NULL,"
                  "CHECKSUM              INT      NOT NULL,"
                  "MANUFAC_NAME          TEXT     NOT NULL,"
                  "MANUFAC_ID            INT      NOT NULL,"
                  "DEVICE_NAME           TEXT     NOT NULL,"
                  "DEVICE_TYPE           INT      NOT NULL,"
                  "DEVICE_REVISION       INT      NOT NULL,"
                  "DD_REVISION           INT      NOT NULL);", tableName.c_str());

    // Execute the query to create the table
    status = sqlite3_exec(m_pDBHandle ,sql, NULL, NULL, &errMsg);

    if (status != SQLITE_OK)
    {
        cout << "Failed to create table. Error Code = " << status << ". Error Message : "<< errMsg << endl;
    }
    return status;
}
/*  ----------------------------------------------------------------------------
    Read a record from the database
*/
int MetadataSQLite::readData(DeviceMetadata *)
{
    return 0;
}
/*  ----------------------------------------------------------------------------
    Insert a record to the database
*/
int MetadataSQLite::writeData(DeviceMetadata * metadata)
{
    int status{};
    char sql[MIN_QUERY_LENGTH]{};
    char * errMsg;

    string tableName;
    switch (metadata->protocol)
    {
        case PROTOCOL::FF:
            tableName = "FF";
            break;
        case PROTOCOL::HART:
            tableName = "HART";
            break;
        default:break;
    }

    // construct the query for insertion
    sprintf (sql, "INSERT INTO %s VALUES(%lu, %lu, '%s', '%u', '%s', '%u', '%u', '%u');",
                 tableName.c_str(), metadata->key, metadata->checksum,
                 metadata->manufacturerName.c_str(), metadata->manufacturerID,
                 metadata->deviceName.c_str(), metadata->deviceTypeCode,
                 metadata->deviceRevision, metadata->ddRevision);

    // Perfrom Insert operation by executing the query
    status = sqlite3_exec(m_pDBHandle ,sql, NULL, NULL, &errMsg);

    if (status != SQLITE_OK)
    {
        cout << "Failed to insert record. Error Code = " << status << ". Error Message : "<< errMsg << endl;
    }
    return status;
}
