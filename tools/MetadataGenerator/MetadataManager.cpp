/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/*  @file
    Implementation of Metadata Manager
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "IMetadata.h"
#include "MetadataManager.h"
#include "StandardMetadata.h"

/*  ----------------------------------------------------------------------------
    Metadata Manager Construtor
*/
MetadataManager::MetadataManager() : m_protocol(PROTOCOL::NONE), m_localization(false), m_pIMetadata(nullptr)
{

}
/*  ----------------------------------------------------------------------------
    Metadata Manager Destructor
*/
MetadataManager::~MetadataManager()
{

}
/*  ----------------------------------------------------------------------------
    Returns the singleton instance of Metadata Manager
*/
MetadataManager* MetadataManager::getInstance()
{
    static MetadataManager metadataManager;
    return &metadataManager;
}
/*  ----------------------------------------------------------------------------
    Sets the Protocol
*/
void MetadataManager::setProtocol(int index)
{
    PROTOCOL newProtocol = static_cast<PROTOCOL>(index);

    if (newProtocol == this->m_protocol)
    {
        // Just Ignore
    }
    else
    {
        // Set the protocol
        this->m_protocol = newProtocol;

        if (m_localization == true)
        {
            // Handle Protocol Specific Instantiation - Post Starsky MAP
        }
        else
        {
            if (m_pIMetadata == nullptr)
            {
                m_pIMetadata = new StandardMetadata;
            }
        }
    }
}
/*  ----------------------------------------------------------------------------
    Enables Localization
*/
void MetadataManager::setLocalization(bool value)
{
    this->m_localization = value;
}
/*  ----------------------------------------------------------------------------
    Stores the DD Library Path
*/
void MetadataManager::setDDLibraryPath(std::string ddlPath)
{
    // Set the DD Library path
    this->m_ddlPath = ddlPath;
}

/*  ----------------------------------------------------------------------------
    Stores the Output file Path
*/
void MetadataManager::setOutputPath(std::string outputPath)
{
    // Set the output path
    this->m_outputPath = outputPath;
}
/*  ----------------------------------------------------------------------------
    Perform Initialisation
*/
void MetadataManager::initialise()
{
    // Create request object
    MetadataRequest request;
    request.ddLibraryPath   = this->m_ddlPath;
    request.protocol        = this->m_protocol;
    request.outputPath      = this->m_outputPath;

    // Peform Initialization
    m_pIMetadata->initialise(request);
}
/*  ----------------------------------------------------------------------------
    Perform Shut Down
*/
void MetadataManager::shutDown(void)
{
    if (nullptr != m_pIMetadata)
        delete m_pIMetadata;
}
/*  ----------------------------------------------------------------------------
    Invokes the appropriate generateMetadata function
*/
STATUS MetadataManager::generateMetadata(void)
{
    STATUS status = STATUS::SUCCESS;

    // Validate if DD Library Path & Protocol is set
    if (this->m_protocol == PROTOCOL::NONE)
    {
        status = STATUS::PROTOCOL_NOT_SET;
    }
    else if(this->m_ddlPath.empty())
    {
        status = STATUS::LIBRARY_PATH_NOT_SET;
    }
    else if(this->m_outputPath.empty())
    {
        status = STATUS::OUTPUT_PATH_NOT_SET;
    }
    else
    {
        if (m_pIMetadata != nullptr)
        {
            // Perform Initialisation
            this->initialise();

            // Invoke the corresponding metadat generation function
            status = m_pIMetadata->generateMetadata();
        }
        else
        {
            status = STATUS::FATAL_ERROR;
        }
    }
    return status;
}
