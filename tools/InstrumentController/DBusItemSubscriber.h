/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <json/json.h>
#include "DBusSubscriber.h"
#include "dialog.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusItemSubscriber

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusItemSubscriber : public DBusSubscriber
{
public:

    Dialog* ui;

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs DBusItemSubscriber Class.
    */
    DBusItemSubscriber(Dialog* dialog);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes DBusItemSubscriber class instance.
    */
    virtual ~DBusItemSubscriber();

    /*------------------------------------------------------------------------*/
    /** Capture the notify messages from the subscribed client
    */
    void notify(Json::Value message);
};
