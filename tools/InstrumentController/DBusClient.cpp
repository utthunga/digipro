/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Client class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
/* INCLUDE FILES **************************************************************/
#include "DBusClient.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusPendingCall>
#include <QCoreApplication>
#include <QDBusPendingReply>
#include "DBusSubscriber.h"
#include <flk_log4cplus/Log4cplusAdapter.h>

/* ========================================================================== */
/* Constants and Macros *******************************************************/
/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs DBusClient Class Instance
*/
DBusClient::DBusClient()
{
}

DBusClient::~DBusClient()
{

}

bool DBusClient::tryConnect(std::string serviceName, std::string objectPath)
{
    m_serviceName = serviceName;
    m_objectPath = objectPath;
    return m_dbusClient.tryConnect(g_CpmCommandServer);
}
    
 ProtoRpc::JsonResponse DBusClient::sendToServer(ProtoRpc::JsonClientCommand& command, int timeout)
{
     (void) timeout;

     auto response = command.execute(m_dbusClient);
     //LOG_INFO (CPM_DBUS_CLIENT_APP, "Set Protocol Response JSON value" << setPrtclResp.toJsonValue());

    return response;
}

std::string DBusClient::dbusServerSlotName() const
{
    return "handleRequest";
}

