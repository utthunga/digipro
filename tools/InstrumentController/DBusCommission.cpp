/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Subscriber class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
/* INCLUDE FILES **************************************************************/
#include "DBusCommission.h"
#include <flk_log4cplus/Log4cplusAdapter.h>

/* ========================================================================== */
/* Constants and Macros *******************************************************/



/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs DBusCommission Class Instance
*/
DBusCommission::DBusCommission(Dialog* dialog)
{
    ui = dialog;
}

/*  ----------------------------------------------------------------------------
    Destructs DBusCommission Class Instance
*/
DBusCommission::~DBusCommission()
{
}

/*  ----------------------------------------------------------------------------
    Get the notify message fromt he publisher and do the requireed things
*/
void DBusCommission::notify(Json::Value message)
{
    ui->handleCommissionResult(message);
    LOGF_INFO(CPM_DBUS_CLIENT_APP, "Message from publisher: " << message);
}
