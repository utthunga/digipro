/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <json/json.h>
#include "DBusSubscriber.h"
#include "dialog.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusScanSubscriber 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusScanSubscriber : public DBusSubscriber
{
public:

    Dialog* ui;

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs DBusScanSubscriber Class.
    */
    DBusScanSubscriber(Dialog* dialog);

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes DBusScanSubscriber class instance.
    */
    virtual ~DBusScanSubscriber();

    /*------------------------------------------------------------------------*/
    /** Capture the notify messages from the subscribed client
    */
    void notify(Json::Value message);
};
