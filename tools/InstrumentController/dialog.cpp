#include "dialog.h"
#include "ui_dialog.h"
#include "qmessagebox.h"
#include <cstdlib>

#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include <time.h>

#include <QtDBus/qdbusconnection.h>
#include <QtDBus/qdbusinterface.h>
#include <QtDBus/qdbusreply.h>
#include <QDBusServiceWatcher>
#include <QStringList>
#include <QtDBus>
#include <QImage>
#include <QLabel>

#include "scp/ScpWrapper.h"

#include "DBusClient.h"
#include "DBusAsyncCommandSubscriber.h"
#include "DBusScanSubscriber.h"
#include "DBusItemSubscriber.h"
#include "DBusCommission.h"

#include "commands/SetProtocolTypeClntCmnd.h"
#include "commands/InitializeClntCmnd.h"
#include "commands/StartScanClntCmnd.h"
#include "commands/StopScanClntCmnd.h"
#include "commands/ConnectClntCmnd.h"
#include "commands/DisconnectClntCmnd.h"
#include "commands/GetItemClntCmnd.h"
#include "commands/SetItemClntCmnd.h"
#include "commands/ShutdownClntCmnd.h"
#include "commands/SetClntFieldDeviceConfig.h"
#include "commands/GetClntFieldDeviceConfig.h"
#include "commands/ProcessSubscriptionClntCmnd.h"
#include "commands/GetClntBusParams.h"
#include "commands/SetClntBusParams.h"
#include "commands/SetClntAppSettings.h"
#include "commands/GetClntAppSettings.h"
#include "commands/SendJsonRequest.h"
#include "commands/SetLogLevelClntCmnd.h"
#include "commands/SetLangCodeCmnd.h"
#include "commands/ExecPreEditActionClntCmd.h"
#include "commands/ExecPostEditActionClntCmd.h"

int currAddress = 0;
string strTag;

map<unsigned int, ItemInfo> itemMap;
map<int, QString> pathMap;
int pathIndex = 0;
map<int, GetItemClntCmnd> backMap;
int backIndex = 0;
bool preED = false;
bool postED = false;
bool isVariableEdit = false;
SetItemClntCmnd EDSetItemClntCmnd;
ExecPreEditActionClntCmd execPreEditActionClntCmd;

string selectedType;
ProtoDBus::Client cpmClient;
QStringList blkTagList;
map<string, int> mapObj;
int scanIndex = 0;
int setRow = 0;
QString connectedDevice;
bool itemInDD = false;
bool isReadOnly = false;
bool isWAO = false;
bool isWAOMenu = false;
int isDate = 0;
bool isScanning = false;
unsigned long currentItemID = 0;
int stopAfterProcessID = 0;

unsigned int subSubscriptionID = 0;
unsigned long subItemID = 0;
unsigned long subVarContainerID = 0;
unsigned long subSubIndex = 0;
unsigned int bitIndex = 0;
string subContainerTag;
string subItemType;
GetItemClntCmnd tempGetItemClntCmd;

unsigned short currentVarValType = DDS_UNUSED;


//added for menu support
bool isBack = false;

Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog)
{
    isSettingRequest = false;
    isBusParamRequest = false;
    ui->setupUi(this);
    isInitialized = false;
    ui->close_2->setVisible(false);
    ui->MEFrame->setVisible(false);

    bool connected = cpmClient.tryConnect(g_CpmCommandServer);
    if (connected)
    {
        setEnable(true, "");
        ui->connectBtn->setEnabled(false);
        ui->setupBtn->setEnabled(false);
        ui->stopscan->setEnabled(false);
        ui->frameHelp->setVisible(false);
        ui->radioButton1_1->setVisible(false);
        ui->radioButton1_2->setVisible(false);
        ui->radioButton1_3->setVisible(false);
        ui->radioButton1_4->setVisible(false);
        ui->radioButton1_5->setVisible(false);
        ui->resultTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->resultTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->tableWidget_2->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->tableWidget_2->setSelectionMode(QAbstractItemView::SingleSelection);

        // Add client commands lists
        ui->client_cmnds_list->clear();
        QStringList stringList1;
        stringList1.append("SetProtocolType");
        ui->client_cmnds_list->addItems(stringList1);
        InitializeAsyncCommands();

        // Add log level
        QStringList logLevelList;
        logLevelList.append("FATAL - 0");
        logLevelList.append("ERROR - 1");
        logLevelList.append("WARN - 2");
        logLevelList.append("INFO - 3");
        logLevelList.append("DEBUG - 4");
        logLevelList.append("TRACE - 5");
        ui->loglevel->addItems(logLevelList);

        connect(ui->loglevel, SIGNAL(currentIndexChanged(int)), this, SLOT(triggerLogLevel(int)));
    }
    else
    {
        print_log("\nClient not Connected.. Problem with DBus interface..!");
    }
    block = false;

    string subscriberName = "CMD_RESP_PUBLISHER";
    DBusSubscriber* pDBusConnectSubscriber = new DBusAsyncCommandSubscriber(this);
    pDBusConnectSubscriber->initialise();
    pDBusConnectSubscriber->subscribeClient(subscriberName);

    subscriberName = "SCAN_PUBLISHER";
    DBusSubscriber* pDBusScanSubscriber = new DBusScanSubscriber(this);
    pDBusScanSubscriber->initialise();
    pDBusScanSubscriber->subscribeClient(subscriberName);

    subscriberName = "ITEM_PUBLISHER";
    DBusSubscriber* pDBusItemSubscriber = new DBusItemSubscriber(this);
    pDBusItemSubscriber->initialise();
    pDBusItemSubscriber->subscribeClient(subscriberName);

    subscriberName = "STATUS_PUBLISHER";
    DBusSubscriber* pDBusCommissionSubscriber = new DBusCommission(this);
    pDBusCommissionSubscriber->initialise();
    pDBusCommissionSubscriber->subscribeClient(subscriberName);

    Show(1, true);
}

Dialog::~Dialog()
{
    isSettingRequest = false;
    isBusParamRequest = false;
    unitsMap.clear();
    modeMap.clear();
    mapObj.clear();
    scanIndex = 0;
    setRow = 0;
    qlfrObj.clear();
    delete ui;
}

void Dialog::InitializeAsyncCommands()
{
    m_commandProcMapTable["SetProtocolType"] = &Dialog::handleSetProtocolTypeResult;
    m_commandProcMapTable["Initialize"] = &Dialog::handleInitializeResult;
    m_commandProcMapTable["StartScan"] = &Dialog::handleStartScanResult;
    m_commandProcMapTable["StopScan"] = &Dialog::handleStopScanResult;
    m_commandProcMapTable["Connect"] = &Dialog::handleConnectResult;
    m_commandProcMapTable["Disconnect"] = &Dialog::handleDisconnectResult;
    m_commandProcMapTable["Shutdown"] = &Dialog::handleShutdownResult;
    m_commandProcMapTable["GetItem"] = &Dialog::handleGetItemResult;
    m_commandProcMapTable["SetItem"] = &Dialog::handleSetItemResult;
    m_commandProcMapTable["GetBusParameters"] = &Dialog::handleGetBusParametersResult;
    m_commandProcMapTable["SetBusParameters"] = &Dialog::handleSetBusParametersResult;
    m_commandProcMapTable["GetFieldDeviceConfig"] = &Dialog::handleGetFieldDeviceConfigResult;
    m_commandProcMapTable["SetFieldDeviceConfig"] = &Dialog::handleSetFieldDeviceConfigResult;
    m_commandProcMapTable["GetAppSettings"] = &Dialog::handleGetAppSettingsResult;
    m_commandProcMapTable["SetAppSettings"] = &Dialog::handleSetAppSettingsResult;
    m_commandProcMapTable["ProcessSubscription"] = &Dialog::handleProcessSubscriptionResult;
    m_commandProcMapTable["SetLangCode"] = &Dialog::handleSetLangCodeResult;
    m_commandProcMapTable["ExecPreEditAction"] = &Dialog::handleExecPreEditAction;
    m_commandProcMapTable["ExecPostEditAction"] = &Dialog::handleExecPostEditAction;
}

void Dialog::triggerLogLevel(int logLevel)
{
    SetLogLevelClntCmnd setLogLevelClntCmnd;
    setLogLevelClntCmnd.setLogLevel(logLevel);

    ProtoRpc::ClientCommand& command = setLogLevelClntCmnd;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&setLogLevelClntCmnd);
}

int Dialog::refresh()
{
    QStringList header;
    header << "Device Tag";
    header << "Device Address";
    header << "Device ID";
    ui->resultTable->setHorizontalHeaderLabels(header);

    int row = ui->resultTable->selectionModel()->currentIndex().row();
    if (row < 0)
    {
        ui->connectBtn->setEnabled(false);
        ui->setupBtn->setEnabled(false);
    }
    return EXIT_SUCCESS;
}

void Dialog::on_FFButton_clicked()
{
    SetProtocolType(ProtocolType::FF);
}

void Dialog::on_HartButton_clicked()
{
    SetProtocolType(ProtocolType::HART);
}

void Dialog::on_ProfibusButton_clicked()
{
    SetProtocolType(ProtocolType::PB);
}

void Dialog::on_initBtn_clicked()
{

    if (ui->initBtn->text().compare("Init Stack") == 0)
    {
        InitializeClntCmnd initializeClntCmnd;
        if (protocolType == ProtocolType::FF)
        {
            initializeClntCmnd.type = "FF";
            setEnable(false, "Initializing the FF Communication stack.\nPlease wait..!");
        }
        else if (protocolType == ProtocolType::HART)
        {
            initializeClntCmnd.type = "HART";
            setEnable(false, "Initializing the HART Communication stack.\nPlease wait..!");
        }
        else if (protocolType == ProtocolType::PB)
        {
            initializeClntCmnd.type = "PROFIBUS";
            setEnable(false, "Initializing the ProfiBus Communication stack.\nPlease wait..!");
        }

        ProtoRpc::ClientCommand& command = initializeClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&initializeClntCmnd);
    }
    else
    {
        mapObj.clear();
        scanIndex = 0;
        setRow = 0;
        on_clearLogs_clicked();

        block = true;
        setEnable(false, "Shutting down the Protocol.\nPlease wait..!");
        ShutdownClntCmnd shutdownClntCmnd;

        ProtoRpc::ClientCommand& command = shutdownClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&shutdownClntCmnd);
    }
}

void Dialog::on_startscanBtn_clicked()
{
    StartScan();
}

void Dialog::on_connectBtn_clicked()
{
    Connect();
}

void Dialog::SetProtocolType(ProtocolType pType)
{
    if (ProtocolType::FF == pType)
    {
        if (!block)
        {
            ui->resultTable->clear();
            block = true;
            protocolType = ProtocolType::FF;
            setEnable(false, "Initializing the FF Protocol.\nPlease wait..!");
            SetProtocolTypeClntCmnd setPrtclClntCmnd;
            setPrtclClntCmnd.type = "FF";

            ProtoRpc::ClientCommand& command = setPrtclClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&setPrtclClntCmnd);
        }
    }
    else if (ProtocolType::HART == pType)
    {
        if (!block)
        {
            ui->resultTable->clear();
            block = true;
            protocolType = ProtocolType::HART;
            setEnable(false, "Initializing the HART Protocol.\nPlease wait..!");
            SetProtocolTypeClntCmnd setPrtclClntCmnd;
            setPrtclClntCmnd.type = "HART";

            ProtoRpc::ClientCommand& command = setPrtclClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&setPrtclClntCmnd);
        }
    }
    else if (ProtocolType::PB == pType)
    {
        if (!block)
        {
            ui->resultTable->clear();
            block = true;
            protocolType = ProtocolType::PB;
            setEnable(false, "Initializing the ProfiBus Protocol.\nPlease wait..!");
            SetProtocolTypeClntCmnd setPrtclClntCmnd;
            setPrtclClntCmnd.type = "PROFIBUS";

            ProtoRpc::ClientCommand& command = setPrtclClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&setPrtclClntCmnd);
        }
    }
}

void Dialog::setLangCode()
{
    SetLangCodeCmnd SetLangCodeCmnd;
    ProtoRpc::ClientCommand& command = SetLangCodeCmnd;
    SetLangCodeCmnd.m_langCode = ui->lang_edit->text().toStdString();
    SetLangCodeCmnd.m_shortStrFlag = ui->shortstr_chkbox->isChecked();

    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&SetLangCodeCmnd);

}

void Dialog::StartScan()
{
    if (!block)
    {
        ui->busparam->setEnabled(false);
        ui->resultTable->setEnabled(true);
        ui->resultTable->clear();
        ui->connectBtn->setEnabled(false);
        ui->setupBtn->setEnabled(false);
        ui->startscanBtn->setEnabled(false);
        ui->stopscan->setEnabled(true);

        block = true;
        refresh();
        setEnable(false, "Device scanning is In Progress.\nPlease wait..!");

        StartScanClntCmnd startScanClntCmnd;

        ProtoRpc::ClientCommand& command = startScanClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&startScanClntCmnd);
    }
}

void Dialog::on_stopscan_clicked()
{
    isScanning = false;

    //clears map
    mapObj.clear();
    //sets row index in scan page for table
    scanIndex = 0;
    //sets row count to zero
    setRow = 0;

    if (!block)
    {
        block = true;
        setEnable(false, "Stopping the device scanning process.\nPlease wait..!");

        StopScanClntCmnd stopScanClntCmnd;

        ProtoRpc::ClientCommand& command = stopScanClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&stopScanClntCmnd);
    }
}

void Dialog::handleScannedResult(Json::Value result)
{
    if (isScanning)
    {
        QString str = QString::fromUtf8(result.toStyledString().c_str());
        string str1 = str.toStdString();

        Json::Value devNode;
        Json::Reader parse;
        parse.parse(str1, devNode, true);
        int scanSequenceId = devNode["ScanSeqID"].asInt();
        string dataAddress = devNode["DeviceInfo"]["DeviceNodeAddress"].asString();
        if(!dataAddress.empty())
        {
            if (mapObj.find(dataAddress) == mapObj.end())
            {
                mapObj[dataAddress] = scanSequenceId;
                ui->resultTable->setRowCount(++setRow);
                if (devNode.isObject() != false)
                {
                    string dataTag = devNode["DeviceInfo"]["DeviceTag"].asString();
                    ui->resultTable->setItem(scanIndex, 0, new QTableWidgetItem(QString::fromStdString(dataTag)));
                    ui->resultTable->setItem(scanIndex, 1, new QTableWidgetItem(QString::fromStdString(dataAddress)));
                    string dataId = devNode["DeviceInfo"]["DeviceID"].asString();
                    ui->resultTable->setItem(scanIndex, 2, new QTableWidgetItem(QString::fromStdString(dataId)));
                    scanIndex++;
                }
            }
        }
        devNode.clear();
        str1.clear();
        setEnable(true, "");

        block = false;
    }
}

Json::Value val;
void Dialog::Connect()
{
    if (!block)
    {
        if (isScanning)
        {
            stopAfterProcessID = 1;
            on_stopscan_clicked();
        }
        else
        {
            //clears map
            mapObj.clear();
            //sets row index in scan page for table
            scanIndex = 0;
            //sets row count to zero
            setRow = 0;

            int row = ui->resultTable->selectionModel()->currentIndex().row();
            QString nodeAddress = ui->resultTable->model()->index(row, 1).data().toString();
            connectedDevice = ui->resultTable->model()->index(row, 0).data().toString();

            block = true;
            setEnable(false, "Connecting to the device " + connectedDevice + "\nPlease wait..!");

            ConnectClntCmnd connectClntCmnd;
            connectClntCmnd.nodeAddress = stoul(nodeAddress.toStdString());

            ProtoRpc::ClientCommand& command1 = connectClntCmnd;
            ProtoRpc::Request request1 = command1.buildRequest();
            string requestString1 = "\nRequest: \n" + request1.debugString();
            print_log(requestString1.c_str());

            executeClientCommand(&connectClntCmnd);
        }
    }
    else
    {
        print_log("\nConnect() cannot be performed\n");
    }
}

void Dialog::menuNavigation(Json::Value result)
{
    Show(10, true);
    int count = result["result"]["ItemInfo"]["ItemList"].size();
    ui->tableWidget_2->setRowCount(count);
    ui->tableWidget_2->hideColumn(3);
    itemMap.clear();
    int index = 0;

    int paramCount = 0;
    for (auto &PInfo : result["result"]["ItemInfo"]["ItemList"])
    {
        unsigned int id = PInfo["ItemID"].asUInt();
        string itemType = PInfo["ItemType"].asString();
        string tag = PInfo["ItemContainerTag"].asString();
        bool itemInDD = PInfo["ItemInDD"].asBool();
        string itemLabel = PInfo["ItemLabel"].asString();
        string itemHelp = PInfo["ItemHelp"].asString();

        bool itemHasRelation = PInfo["ItemInfo"]["ItemHasRelation"].asBool();
        bool itemReadOnly = PInfo["ItemInfo"]["ItemReadOnly"].asBool();
        unsigned int varContainerID = PInfo["ItemInfo"]["VarContainerID"].asUInt();
        unsigned int varSubIndex = PInfo["ItemInfo"]["VarSubIndex"].asUInt();
        int varBitIndex = 0;
        if (PInfo["ItemInfo"]["VarType"].asString().compare("ENUMERATION") == 0)
        {
            if (PInfo["ItemInfo"]["VarInfo"]["VarValType"].asString().compare("BITENUMERATION_ITEM") == 0)
            {
                varBitIndex = PInfo["ItemInfo"]["VarInfo"]["VarBitIndex"].asLargestInt();
            }
        }

        ItemInfo itemInfo{};
        itemInfo.itemID = id;
        itemInfo.itemContainerTag = tag;
        itemInfo.itemType = itemType;
        itemInfo.itemInDD = itemInDD;
        itemInfo.itemHelp = itemHelp;
        itemInfo.itemLabel = itemLabel;
        itemInfo.itemHasRelation = itemHasRelation;
        itemInfo.itemReadOnly = itemReadOnly;
        itemInfo.varContainerID = varContainerID;
        itemInfo.varSubIndex = varSubIndex;
        itemInfo.bitIndex = varBitIndex;
        itemMap.insert(pair<unsigned int, ItemInfo>(++index, itemInfo));

        string str = to_string(id);

        QTableWidgetItem *item = ui->tableWidget_2->item(paramCount, 0);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 0, item);
        }
        item->setText(QString::fromStdString(str.c_str()));

        item = ui->tableWidget_2->item(paramCount, 1);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 1, item);
        }
        item->setText(QString::fromStdString(itemType.c_str()));

        item = ui->tableWidget_2->item(paramCount, 2);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 2, item);
        }
        item->setText(QString::fromStdString(itemLabel.c_str()));

        item = ui->tableWidget_2->item(paramCount, 3);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 3, item);
        }
        item->setText(QString::fromStdString(tag.c_str()));

        item = nullptr;
        paramCount++;
   }
}

void Dialog::Disconnect()
{
    //below 5 lines of code added for menu navigation
    isBack = false;
    ui->tableWidget_2->clear();
    ui->tableWidget_2->setRowCount(int(0));
    ui->tableWidget_2->hideColumn(3);
    ui->jsonLogsTxt->clear();

    if (!block)
    {
        block = true;
        setEnable(false, "Disconnecting from the device.\nPlease wait..!");
        DisconnectClntCmnd disconnectClntCmnd;

        ProtoRpc::ClientCommand& command = disconnectClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());
        executeClientCommand(&disconnectClntCmnd);
    }
}

void Dialog::on_close_clicked()
{
    if (ui->devRecgFrame->isVisible() || ui->HartBusParamFrame_2->isVisible() ||
            ui->PBBusParamFrame->isVisible())
    {
        if (isScanning)
        {
            stopAfterProcessID = 3;
            on_stopscan_clicked();
        }
        else
        {
            if (!isInitialized)
            {
                if (ui->HartBusParamFrame_2->isVisible() || ui->PBBusParamFrame->isVisible())
                {
                    Show(3, true);
                }
                else
                {
                    mapObj.clear();
                    scanIndex = 0;
                    setRow = 0;
                    on_clearLogs_clicked();
                    Show(1, true);
                }
                // Reset language controls
                setLanUICntrlState(true);
            }
            else
            {
                block = true;
                setEnable(false, "Shutting down the Protocol.\nPlease wait..!");
                ShutdownClntCmnd shutdownClntCmnd;

                ProtoRpc::ClientCommand& command = shutdownClntCmnd;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                executeClientCommand(&shutdownClntCmnd);
            }
        }
    }
    else if (ui->mainFrame->isVisible())
    {
        exit(0);
    }
    else if(ui->Hart_Menu_Frame->isVisible() || ui->MEFrame->isVisible())
    {
        ui->tableWidget_2->clear();
        ui->tableWidget_2->setRowCount(0);
        ui->tableWidget_2->hideColumn(3);
        isBack = true;
        Show(4, true);
    }
    else if (ui->FDSetupFrame->isVisible())
    {
        Show(3, true);
    }
    else if (ui->channel_params->isVisible())
    {
        block = true;
        setEnable(false, "Shutting down the Protocol.\nPlease wait..!");
        ShutdownClntCmnd shutdownClntCmnd;

        ProtoRpc::ClientCommand& command = shutdownClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&shutdownClntCmnd);
    }
}

void Dialog::on_clearLogs_clicked()
{
    ui->jsonLogsTxt->setText("");
}

void Dialog::print_log(const QString str)
{
    ui->jsonLogsTxt->append(str);
    ui->jsonLogsTxt->textCursor().movePosition(QTextCursor::End);
}

void Dialog::setEnable(bool enable, QString text)
{
    ui->mainFrame->setEnabled(enable);
    ui->devRecgFrame->setEnabled(enable);
    ui->connectedFrame->setEnabled(enable);
    ui->FDSetupFrame->setEnabled(enable);
    ui->Hart_Menu_Frame->setEnabled(enable);
    ui->Hart_Read_Write->setEnabled(enable);
    ui->close->setEnabled(enable);
    ui->loglevel->setEnabled(enable);
    ui->send_json_req_btn->setEnabled(enable);

    ui->wait->setVisible(!enable);
    ui->wait->setText(text);
    ui->wait->raise();
}

void Dialog::handleShutdownResult(const Json::Value& response)
{
    string result = response.toStyledString();
    if (result.find("ok") != string::npos)
    {
        isInitialized = false;
        setEnable(true, "");
        block = false;

        // Reset language controls
        setLanUICntrlState(true);
        Show(1, true);
    }
}

void Dialog::Show(int frame, bool enable)
{
    switch (frame)
    {
    case 1:
        if (!block)
        {
            ui->mainFrame->setVisible(true);
            ui->devRecgFrame->setVisible(false);
            ui->connectedFrame->setVisible(false);
            ui->FDSetupFrame->setVisible(false);
            ui->close->setText("E&xit");
            ui->Hart_Menu_Frame->setVisible(false);
            for (int i = 0; i < ui->resultTable->rowCount(); i++)
            {
                ui->resultTable->removeRow(i);
                this->update();
            }
            ui->MEFrame->setVisible(false);
            ui->channel_params->setVisible(false);
            ui->close_2->setVisible(false);
            ui->HartBusParamFrame_2->setVisible(false);
            ui->PBBusParamFrame->setVisible(false);
            ui->Hart_Read_Write->setVisible(false);

            ui->client_cmnds_list->clear();
            QStringList stringList1;
            stringList1.append("SetProtocolType");
            stringList1.append("GetBusParameters");
            stringList1.append("GetAppSettings");
            ui->client_cmnds_list->addItems(stringList1);
            InitializeAsyncCommands();

            unitsMap.clear();
            modeMap.clear();
            mapObj.clear();
        }
        break;
    case 3:
        isBusParamRequest = false;
        isSettingRequest = false;
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(true);
        ui->connectedFrame->setVisible(false);
        ui->FDSetupFrame->setVisible(false);
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        if (isInitialized)
        {
            ui->close->setText("&Shutdown");
        }
        else
        {
            ui->close->setText("&Back");
        }
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        if(protocolType == ProtocolType::FF)
            ui->busparam->setEnabled(false);
        break;
    case 4:
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(true);
        ui->FDSetupFrame->setVisible(false);
        ui->close->setText("&Back");
        ui->close->setEnabled(false);
        ui->MEFrame->setVisible(false);
        ui->disconnect->setEnabled(true);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        ui->GetMenu->setEnabled(true);
        postED = false;
        preED = false;
        break;
    case 5:
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->FDSetupFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->MEFrame->setVisible(false);
        ui->close->setText("&Back");
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 6:
        ui->FDSetupFrame->setVisible(true);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 7:
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 8:
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");

        ui->MEFrame->setVisible(true);
        ui->MEfirst->setVisible(false);
        ui->MEsecond->setVisible(false);
        ui->MEthird->setVisible(false);
        ui->MEmessage->setVisible(false);
        ui->MEcomboBox->setVisible(false);
        ui->MElineEdit->setVisible(false);
        ui->dateTimeEdit->setVisible(false);

        ui->MEmessage->setText("");
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 9:
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(true);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 10:
        methodExecutedFromMenu = false;
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(true);
        ui->close_2->setVisible(false);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(false);
        break;
    case 11:
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(false);
        if(protocolType == ProtocolType::HART)
           ui->HartBusParamFrame_2->setVisible(true);
        if(protocolType == ProtocolType::PB)
            ui->PBBusParamFrame->setVisible(true);

        ui->Hart_Read_Write->setVisible(false);
        break;
    case 12:
        ui->FDSetupFrame->setVisible(false);
        ui->mainFrame->setVisible(false);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->close->setEnabled(true);
        ui->close->setText("&Back");
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->Hart_Menu_Frame->setVisible(false);
        ui->close_2->setVisible(true);
        ui->HartBusParamFrame_2->setVisible(false);
        ui->PBBusParamFrame->setVisible(false);
        ui->Hart_Read_Write->setVisible(true);
        ui->customPlot->setVisible(false);
        ui->groupBox_12->setVisible(true);
        ui->checkBox_1->setVisible(false);
        ui->checkBox_2->setVisible(false);
        ui->checkBox_3->setVisible(false);
        ui->checkBox_4->setVisible(false);
        ui->checkBox_5->setVisible(false);
        ui->checkBox_6->setVisible(false);
        ui->checkBox_7->setVisible(false);
        ui->checkBox_8->setVisible(false);
        break;
    default:
        ui->mainFrame->setVisible(true);
        ui->devRecgFrame->setVisible(false);
        ui->connectedFrame->setVisible(false);
        ui->FDSetupFrame->setVisible(false);
        ui->MEFrame->setVisible(false);
        ui->channel_params->setVisible(false);
        ui->close->setText("E&xit");
        break;
    }
    ui->mainFrame->setEnabled(enable);
    ui->devRecgFrame->setEnabled(enable);
    ui->connectedFrame->setEnabled(enable);
    QStringList header;
    header << "Device Tag";
    header << "Device Address";
    header << "Device ID";
    ui->resultTable->setHorizontalHeaderLabels(header);
}

void Dialog::on_resultTable_itemSelectionChanged()
{
    int index = ui->resultTable->selectionModel()->currentIndex().row();
    int addr = ui->resultTable->model()->index(index, 1).data().toInt();
    if (index >= 0)
    {
        if(addr < 248)
        {
            ui->connectBtn->setEnabled(true);
            if(protocolType == ProtocolType::FF)
                ui->setupBtn->setEnabled(true);
            else
                ui->setupBtn->setEnabled(false);
        }
        else
        {
            if(protocolType == ProtocolType::FF)
                ui->setupBtn->setEnabled(true);
            else
                ui->setupBtn->setEnabled(false);

            ui->connectBtn->setEnabled(false);
        }
    }
    else
    {
        ui->connectBtn->setEnabled(false);
        ui->setupBtn->setEnabled(false);
    }
}

void Dialog::on_disconnect_clicked()
{
    Disconnect();
}

int Dialog::getStaticMenuID(int index)
{
    int itemID = 0;

    if(index == 0)
    {
        itemID = CUSTOM_ID_BLOCK_PARAM_LIST;
    }
    else if(index == 1)
    {
        itemID = CUSTOM_ID_BLOCK_VIEW1;
    }
    else if(index == 2)
    {
        itemID = CUSTOM_ID_BLOCK_VIEW2;
    }
    else if(index == 3)
    {
        itemID = CUSTOM_ID_BLOCK_VIEW3;
    }
    else if(index == 4)
    {
        itemID = CUSTOM_ID_BLOCK_VIEW4;
    }
    else if(index == 5)
    {
        itemID = CUSTOM_ID_BLOCK_CONFIG;
    }
    else if(index == 6)
    {
        itemID = CUSTOM_ID_BLOCK_ALARAM;
    }
    else if(index == 7)
    {
        itemID = CUSTOM_ID_BLOCK_STATUS;
    }
    else if(index == 8)
    {
        itemID = CUSTOM_ID_BLOCK_VIEWS;
    }

    return itemID;
}

void Dialog::on_Adv_Dev_Setup_clicked()
{
    Show(7, true);

    modeMap.clear();
    unitsMap.clear();
}

//commission
void Dialog::on_pushButton_7_clicked()
{
    if (!block)
    {
        QMessageBox msgBox;
        msgBox.setText("The node address is invalid.");
        msgBox.setDefaultButton(QMessageBox::Ok);

        SetClntFieldDeviceConfig setClntFieldDeviceConfig;
        setClntFieldDeviceConfig.deviceConfigType = "COMMISSION";
        int row = ui->resultTable->selectionModel()->currentIndex().row();
        int currAddress = ui->resultTable->model()->index(row, 1).data().toInt();

        if (currAddress != ui->textEdit->toPlainText().toInt())
        {
            int address = ui->textEdit->toPlainText().toInt();
            if(address > 16 && address < 248)
            {
                setClntFieldDeviceConfig.newDevAddress = ui->textEdit->toPlainText().toInt();
                setClntFieldDeviceConfig.devAddress = currAddress;

                if (strTag != ui->textEdit_11->toPlainText().toStdString())
                {
                    setClntFieldDeviceConfig.deviceTag = ui->textEdit_11->toPlainText().toStdString();
                }
                block = true;
                setEnable(false, "Commissioning the device.\nPlease wait..!");
                CommissionType = 1;

                ProtoRpc::ClientCommand& command = setClntFieldDeviceConfig;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                executeClientCommand(&setClntFieldDeviceConfig);
            }
            else
            {
                msgBox.exec();
            }
        }
    }
}

//decommission
void Dialog::on_decommission_btn_clicked()
{
    if (!block)
    {
        SetClntFieldDeviceConfig setClntFieldDeviceConfig;

        int addr = ui->textEdit->toPlainText().toInt();
        setClntFieldDeviceConfig.deviceConfigType = "DECOMMISSION";
        setClntFieldDeviceConfig.devAddress = addr;
        CommissionType = 0;

        ProtoRpc::ClientCommand& command = setClntFieldDeviceConfig;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&setClntFieldDeviceConfig);
    }
}

//device class
void Dialog::on_device_class_write_btn_clicked()
{
    if (!block)
    {
        SetClntFieldDeviceConfig setClntFieldDeviceConfig;

        int deviceClass = ui->dev_class_combo->currentIndex();
        int addr = ui->textEdit->toPlainText().toInt();
        if(deviceClass == 0)
        {
            deviceClass = 1;
        }
        else if(deviceClass == 1)
        {
            deviceClass = 2;
        }

        block = true;
        setEnable(false, "Field device setup is in progress.\nPlease wait..!");

        setClntFieldDeviceConfig.deviceConfigType = "DEVICECLASS";
        setClntFieldDeviceConfig.devAddress = addr;
        setClntFieldDeviceConfig.devClass = deviceClass;

        ProtoRpc::ClientCommand& command = setClntFieldDeviceConfig;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&setClntFieldDeviceConfig);
    }
}

void Dialog::handleItemResult(Json::Value result)
{
    QString str = QString::fromUtf8(result.toStyledString().c_str());
    print_log("\nNotify Message: \n" + str);

    string itemType = result["ItemType"].asString();
    if (itemType.compare("METHOD") == 0)
    {
        executeMethod(result);
    }
    else if (itemType.compare("VARIABLE") == 0)
    {
        updateVariableValue(result);
    }
}

void Dialog::handleCommissionResult(Json::Value result)
{
    QString str = QString::fromUtf8(result.toStyledString().c_str());
    print_log("\nNotify Message: \n" + str);
}

void Dialog::executeMethod(Json::Value result)
{
    if (result.isMember("error"))
    {
        ui->MEmessage->setText("");
        ui->close->setEnabled(true);
        ui->MEfirst->setVisible(false);
        ui->MEsecond->setVisible(false);
        ui->MEthird->setVisible(false);
        ui->MEFrame->setVisible(false);
        if(methodExecutedFromMenu)
        {
            Show(10,true);
        }
        else
        {
            ui->close_2->setVisible(true);
            ui->Hart_Read_Write->setVisible(true);
        }
        if (pathIndex > 0)
        {
            pathMap.erase(prev(pathMap.end()));
            --pathIndex;
            displayMenuPath();
        }
    }
    else
    {
        if (!ui->MEFrame->isVisible())
        {
            Show(8, true);
        }
        ui->MEFrame->setVisible(true);
        enumMap.clear();
        ui->frameHelp->setVisible(false);
        ui->MEfirst->setVisible(true);
        ui->MEsecond->setVisible(true);
        ui->MEthird->setVisible(true);

        ui->radioButton1_1->setVisible(false);
        ui->radioButton1_2->setVisible(false);
        ui->radioButton1_3->setVisible(false);
        ui->radioButton1_4->setVisible(false);
        ui->radioButton1_5->setVisible(false);

        string itemHelp = result["ItemHelp"].asString();
        string itemCallbackType = result["ItemInfo"]["NotificationType"].asString();
        string itemMessage = result["ItemInfo"]["Message"].asString();

        string methodStateStr = result["MethodState"].asString();
        int methodState = 0;

        if (!methodStateStr.empty())
        {
            methodState = stoi(methodStateStr);
        }

        if (itemHelp.length() <= 0)
        {
            itemHelp = "No help available..!";
        }
        ui->MEHelp->setText(QString::fromStdString(itemHelp));
        ui->MEthird->setEnabled(true);

        ui->Hart_Read_Write->setVisible(false);

        if (itemCallbackType.compare("START") == 0)
        {
            ui->close->setEnabled(false);

            ui->MEfirst->setEnabled(false);
            ui->MEsecond->setEnabled(false);
            ui->MEthird->setEnabled(false);
            ui->MEmessage->setText(QString::fromStdString(itemMessage));

            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);
            ui->MEmessage->setText("");
        }
        else if (itemCallbackType.compare("COMPLETED") == 0)
        {
            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);

            ui->radioButton1_1->setVisible(false);
            ui->radioButton1_2->setVisible(false);
            ui->radioButton1_3->setVisible(false);
            ui->radioButton1_4->setVisible(false);
            ui->radioButton1_5->setVisible(false);

            ui->MEfirst->setEnabled(true);
            ui->MEfirst->setText("FINISH");
            ui->MEsecond->setEnabled(false);
            ui->MEthird->setVisible(false);
        }
        else if (itemCallbackType.compare("DISPLAY") == 0)
        {
            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);
            ui->MEmessage->setText(QString::fromStdString(itemMessage));
            ui->MEmessage->setVisible(true);

            ui->MEfirst->setEnabled(false);
            ui->MEfirst->setText("NEXT");
            if (methodState == 0)
            {
                ui->MEsecond->setEnabled(true);
            }
            else
            {
                ui->MEsecond->setEnabled(false);
            }
        }
        else if (itemCallbackType.compare("ACK") == 0 ||
                 itemCallbackType.compare("ERROR") == 0)
        {
            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);
            ui->MEmessage->setVisible(true);
            ui->MEmessage->setText(QString::fromStdString(itemMessage));

            ui->MEfirst->setEnabled(true);
            ui->MEfirst->setText("NEXT");
            if (methodState == 0)
            {
                ui->MEsecond->setEnabled(true);
            }
            else
            {
                ui->MEsecond->setEnabled(false);
            }
        }
        else if ((itemCallbackType.compare("CONTINUOUS") == 0))
        {
            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);
            ui->MEmessage->setVisible(true);
            ui->MEmessage->setText(QString::fromStdString(itemMessage));

            ui->MEfirst->setEnabled(true);
            if (methodState == 0)
            {
                ui->MEsecond->setEnabled(true);
            }
            else
            {
                ui->MEsecond->setEnabled(false);
            }
        }
        else if (itemCallbackType.compare("SELECTION") == 0)
        {
            ui->MEfirst->setEnabled(true);
            ui->MEfirst->setText("NEXT");
            if (methodState == 0)
            {
                ui->MEsecond->setEnabled(true);
            }
            else
            {
                ui->MEsecond->setEnabled(false);
            }

            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);
            ui->MEmessage->setVisible(true);
            ui->MEmessage->setText(QString::fromStdString(itemMessage));

            Json::Value itemNode = result["ItemInfo"]["MethodVarInfo"];

            if (itemNode.isObject())
            {
                string itemType = itemNode["ItemType"].asString();
                if (itemType.compare("VARIABLE") == 0)
                {
                    string varType = itemNode["ItemInfo"]["VarType"].asString();

                    if (varType.compare("ENUMERATION") == 0)
                    {
                        string varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                        int varVal = stoi(varValue);

                        for(auto &enumNode : itemNode["ItemInfo"]["VarInfo"]["VarEnumInfo"])
                        {
                            if (enumNode.isObject())
                            {
                                string varEnumValue = enumNode["VarEnumValue"].asString();
                                int varEnumVal = stoi(varEnumValue);
                                string varDescription = enumNode["VarEnumLabel"].asString();
                                QString str = QString::fromUtf8(varDescription.c_str());
                                enumMap.insert(pair<QString, int>(str, varEnumVal));

                                if (varEnumVal == 1)
                                {
                                    if (varEnumVal == varVal)
                                    {
                                        ui->radioButton1_1->setChecked(true);
                                    }
                                    ui->radioButton1_1->setText(str);
                                    ui->radioButton1_1->setVisible(true);
                                }
                                if (varEnumVal == 2)
                                {
                                    if (varEnumVal == varVal)
                                    {
                                        ui->radioButton1_2->setChecked(true);
                                    }
                                    ui->radioButton1_2->setText(str);
                                    ui->radioButton1_2->setVisible(true);
                                }
                                if (varEnumVal == 3)
                                {
                                    if (varEnumVal == varVal)
                                    {
                                        ui->radioButton1_3->setChecked(true);
                                    }
                                    ui->radioButton1_3->setText(str);
                                    ui->radioButton1_3->setVisible(true);
                                }
                                if (varEnumVal == 4)
                                {
                                    if (varEnumVal == varVal)
                                    {
                                        ui->radioButton1_4->setChecked(true);
                                    }
                                    ui->radioButton1_4->setText(str);
                                    ui->radioButton1_4->setVisible(true);
                                }
                                if (varEnumVal == 5)
                                {
                                    if (varEnumVal == varVal)
                                    {
                                        ui->radioButton1_5->setChecked(true);
                                    }
                                    ui->radioButton1_5->setText(str);
                                    ui->radioButton1_5->setVisible(true);
                                }
                                ui->MEfirst->setEnabled(true);
                            }
                        }
                    }
                }
            }
        }
        else if (itemCallbackType.compare("GETVALUE") == 0)
        {
            string varValType = "";

            ui->MEcomboBox->setVisible(false);
            ui->MElineEdit->setVisible(false);
            ui->dateTimeEdit->setVisible(false);

            ui->MEfirst->setEnabled(true);
            ui->MEfirst->setText("NEXT");
            if (methodState == 0)
            {
                ui->MEsecond->setEnabled(true);
            }
            else
            {
                ui->MEsecond->setEnabled(false);
            }

            string itemMessage = result["ItemInfo"]["Message"].asString();
            ui->MEmessage->setText(QString::fromStdString(itemMessage));
            ui->MEmessage->setVisible(true);

            Json::Value itemNode = result["ItemInfo"]["MethodVarInfo"];

            if (itemNode.isObject())
            {
                string itemType = itemNode["ItemType"].asString();
                if (itemType.compare("VARIABLE") == 0)
                {
                    string varType = itemNode["ItemInfo"]["VarType"].asString();

                    if (varType.compare("ARITHMETIC") == 0)
                    {
                        isDate = 0;
                        varValType = itemNode["ItemInfo"]["VarInfo"]["VarValType"].asString();
                        string varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                        ui->MElineEdit->setText(varValue.c_str());
                        ui->MElineEdit->setVisible(true);
                        ui->MElineEdit->setEnabled(true);
                    }
                    else if (varType.compare("ENUMERATION") == 0)
                    {
                        isDate = 0;
                        ui->MEcomboBox->clear();
                        QStringList enumList;
                        varValType = itemNode["ItemInfo"]["VarInfo"]["VarValType"].asString();
                        string varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                        for(auto &enumNode : itemNode["ItemInfo"]["VarInfo"]["VarEnumInfo"])
                        {
                            if (enumNode.isObject())
                            {
                                string varEnumValue = enumNode["VarEnumValue"].asString();
                                int varEnumVal = stoi(varEnumValue);
                                string varDescription = enumNode["VarEnumLabel"].asString();
                                QString str = QString::fromUtf8(varDescription.c_str());
                                enumMap.insert(pair<QString, int>(str, varEnumVal));
                                enumList.append(str);
                            }
                        }
                        ui->MEcomboBox->addItems(enumList);

                        int valu = stoi(varValue.c_str());
                        map<QString, int>::const_iterator it;
                        int index = 0;
                        for (it = enumMap.begin(); it != enumMap.end(); ++it)
                        {
                            if (it->second == valu)
                            {
                                for (int ind = 0; ind < ui->MEcomboBox->count(); ind++)
                                {
                                    if (ui->MEcomboBox->itemText(ind) == it->first)
                                    {
                                        ui->MEcomboBox->setCurrentIndex(ind);
                                        break;
                                    }
                                }
                                break;
                            }
                            index++;
                        }
                        ui->MEcomboBox->setVisible(true);
                        ui->MEcomboBox->setEnabled(true);
                    }
                    else if (varType.compare("STRING") == 0)
                    {
                        isDate = 0;
                        varValType = itemNode["ItemInfo"]["VarInfo"]["VarValType"].asString();
                        string varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();

                        ui->MElineEdit->setText(varValue.c_str());
                        ui->MElineEdit->setVisible(true);
                        ui->MElineEdit->setEnabled(true);
                    }
                    else if (varType.compare("DATE_TIME") == 0)
                    {
                        string varValType = itemNode["ItemInfo"]["VarInfo"]["VarValType"].asString();
                        string varValue;

                        if (varValType.compare("TIME_VALUE_EPOCH") == 0)
                        {
                            string results = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                            unsigned long Epoch_time = stoul(results);
                            string result_ms = itemNode["ItemInfo"]["VarInfo"]["VarValInfo"]["VarValMilliSecPart"].asString();
                            string result_us = itemNode["ItemInfo"]["VarInfo"]["VarValInfo"]["VarValMicroSecPart"].asString();
                            unsigned long millisec = stoul(result_ms);
                            millisec += stoul(result_us);
                            Epoch_time += (millisec/1000);
                            string varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");

                            QDate date;
                            QTime time;
                            date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
                            time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

                            QDate minDate;
                            minDate.setDate(1972, 01, 01);
                            ui->dateTimeEdit->setMinimumDate(minDate);
                            ui->dateTimeEdit->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                            ui->dateTimeEdit->clear();
                            ui->dateTimeEdit->setDate(date);
                            ui->dateTimeEdit->setTime(time);
                            ui->MElineEdit->setVisible(false);
                            ui->MElineEdit->setEnabled(false);
                            ui->dateTimeEdit->setVisible(true);
                            ui->dateTimeEdit->setEnabled(true);
                        }
                        else if (varValType.compare("TIME_VALUE_DIFF") == 0)
                        {
                            string results = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                            unsigned long time_val_ms = stoul(results);
                            unsigned long time_val_sec = time_val_ms/32000;
                            string time_format = itemNode["ItemInfo"]["VarInfo"]["VarValDisplayFormat"].asString();

                            if (time_format.empty())
                            {
                                time_format = "%H:%M:%S";
                            }
                            string varValue = longToDateAndTime(time_val_sec, time_format.c_str());

                            QTime time;
                            time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

                            ui->dateTimeEdit->setDisplayFormat("hh:mm:ss");
                            ui->dateTimeEdit->setTime(time);
                            ui->MElineEdit->setVisible(false);
                            ui->MElineEdit->setEnabled(false);
                            ui->dateTimeEdit->setVisible(true);
                            ui->dateTimeEdit->setEnabled(true);
                        }
                        else if (varValType.compare("TIME_VALUE_SCALED") == 0)
                        {
                            string results = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                            unsigned long time_val_ms = stoul(results);
                            unsigned long time_val_sec = time_val_ms/32000;
                            string time_format = "%H:%M:%S";
                            string varValue = longToDateAndTime(time_val_sec, time_format.c_str());

                            QTime time;
                            time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

                            ui->dateTimeEdit->setDisplayFormat("hh:mm:ss");
                            ui->dateTimeEdit->setTime(time);
                            ui->MElineEdit->setVisible(false);
                            ui->MElineEdit->setEnabled(false);
                            ui->dateTimeEdit->setVisible(true);
                            ui->dateTimeEdit->setEnabled(true);
                        }
                        else if (varValType.compare("DURATION") == 0)
                        {
                            varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                            isDate = 0;
                            ui->MElineEdit->setText(varValue.c_str());
                            ui->MElineEdit->setVisible(true);
                            ui->MElineEdit->setEnabled(true);
                        }
                        else if (varValType.compare("DATE_AND_TIME") == 0)
                        {
                            time_t Epoch_time = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
                            unsigned long milliSec = itemNode["ItemInfo"]["VarInfo"]["VarValInfo"]["VarValMilliSecPart"].asLargestInt();
                            Epoch_time += (milliSec / 1000);
                            varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");
                            QDate date;
                            QTime time;
                            date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
                            time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));
                            QDate minDate;
                            minDate.setDate(1900, 01, 01);
                            ui->dateTimeEdit->setMinimumDate(minDate);
                            ui->dateTimeEdit->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                            isDate = 3;
                            ui->dateTimeEdit->clear();
                            ui->dateTimeEdit->setDate(date);
                            ui->dateTimeEdit->setTime(time);

                            ui->MElineEdit->setVisible(false);
                            ui->MElineEdit->setEnabled(false);
                            ui->dateTimeEdit->setVisible(true);
                            ui->dateTimeEdit->setEnabled(true);
                        }
                        else
                        {
                            time_t Epoch_time = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
                            varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");
                            QDate date;
                            QTime time;
                            date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
                            time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

                            if (varValType.compare("DATE") == 0)
                            {
                                QDate minDate;
                                minDate.setDate(1900, 01, 01);
                                ui->dateTimeEdit->setMinimumDate(minDate);
                                ui->dateTimeEdit->setDisplayFormat("MM/dd/yyyy");
                                isDate = 1;
                            }
                            else if (varValType.compare("TIME") == 0)
                            {
                                QDate minDate;
                                minDate.setDate(1984, 01, 01);
                                ui->dateTimeEdit->setMinimumDate(minDate);
                                ui->dateTimeEdit->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                                isDate = 2;
                            }
                            else
                            {

                            }
                            ui->dateTimeEdit->clear();
                            ui->dateTimeEdit->setDate(date);
                            ui->dateTimeEdit->setTime(time);

                            ui->MElineEdit->setVisible(false);
                            ui->MElineEdit->setEnabled(false);
                            ui->dateTimeEdit->setVisible(true);
                            ui->dateTimeEdit->setEnabled(true);
                        }
                    }
                    currentVarValType = getVarValType(varValType);
                }
            }
        }
    }

    block = false;
}

unsigned short Dialog::getVarValType(string val)
{
    unsigned short type = DDS_UNUSED;
    if (val.compare("INTEGER") == 0)
    {
        type = DDS_INTEGER;
    }
    else if (val.compare("FLOAT") == 0)
    {
        type = DDS_FLOAT;
    }
    else if (val.compare("DOUBLE") == 0)
    {
        type = DDS_DOUBLE;
    }
    else if (val.compare("UNSIGNED_INTEGER") == 0 ||
         val.compare("INDEX") == 0 ||
         val.compare("MAXTYPE") == 0 ||
         val.compare("ACKTYPE") == 0 ||
         val.compare("BOOLEAN") == 0 ||
         val.compare("BITENUMERATION") == 0 ||
         val.compare("BITENUMERATION_ITEM") == 0 ||
         val.compare("ENUMERATION") == 0)
    {
        type = DDS_UNSIGNED;
    }
    return type;
}

void Dialog::on_MEfirst_clicked()
{
    SetItemClntCmnd setItemClntCmnd;
    if (ui->MEfirst->text().compare("FINISH") == 0)
    {
        ui->MEmessage->setText("");
        ui->close->setEnabled(true);
        ui->MEfirst->setVisible(false);
        ui->MEsecond->setVisible(false);
        ui->MEthird->setVisible(false);
        ui->MEFrame->setVisible(false);
        if(methodExecutedFromMenu)
        {
            Show(10,true);
        }
        else
        {
            ui->close_2->setVisible(true);
            ui->Hart_Read_Write->setVisible(true);
        }
        if (pathIndex > 0)
        {
            pathMap.erase(prev(pathMap.end()));
            --pathIndex;
            displayMenuPath();
        }
        if (preED)
        {
            Show(10,true);
            preED = false;
            postED = true;

            backMap.insert(pair<int, GetItemClntCmnd>(++backIndex, tempGetItemClntCmd));
            ProtoRpc::ClientCommand& command = tempGetItemClntCmd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            block = true;
            setEnable(false, "Fetching Menu.\nPlease wait..!");

            executeClientCommand(&tempGetItemClntCmd);
        }
        else if (postED)
        {
            Show(10,true);
            postED = false;

            ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
            ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
            ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
            ui->tableWidget_2->hideColumn(3);

            if (backMap.size() > 0)
            {
                backMap.erase(prev(backMap.end()));
                --backIndex;
            }
            if (backMap.size() < 1)
            {
                qlfrObj.clear();
                backIndex = 0;
                backMap.clear();
                menuNavigation(val);
            }
            else
            {
                map<int, GetItemClntCmnd>::iterator itr;
                itr = backMap.end();
                --itr;
                GetItemClntCmnd cmd = itr->second;

                selectedType = "MENU";
                ProtoRpc::ClientCommand& command = cmd;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                block = true;
                setEnable(false, "Fetching Menu.\nPlease wait..!");

                executeClientCommand(&cmd);
            }
            currentItemID = 0;
            if (pathIndex > 0)
            {
                pathMap.erase(prev(pathMap.end()));
                --pathIndex;
            }
            displayMenuPath();
        }
    }
    else
    {
        ui->MEfirst->setEnabled(false);
        ui->MEsecond->setEnabled(false);
        setItemClntCmnd.itemID = 0;
        setItemClntCmnd.itemType = "METHOD";
        setItemClntCmnd.abort = "";
        setItemClntCmnd.varValue = 0;
        if (protocolType == ProtocolType::HART)
        {
            setItemClntCmnd.itemInDD = true;
        }
        if (ui->MEcomboBox->isVisible())
        {
            QString enumVal = ui->MEcomboBox->currentText();

            map<QString, int>::const_iterator it;
            for (it = enumMap.begin(); it != enumMap.end(); ++it)
            {
                if (it->first == enumVal)
                {
                    int val = it->second;
                    setItemClntCmnd.varValue = val;
                    break;
                }
            }
        }
        else if (ui->dateTimeEdit->isVisible())
        {
            if (isDate == 1)
            {
                string str = ui->dateTimeEdit->text().toStdString();
                time_t value = dateAndTimeToLong(str.c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = value;
            }
            else if (isDate == 2)
            {
                string str = ui->dateTimeEdit->text().toStdString();
                time_t value = dateAndTimeToLong(str.c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = value;
            }
            else if (isDate == 3)
            {
                string str = ui->dateTimeEdit->text().toStdString();
                time_t value = dateAndTimeToLong(str.c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = value;
            }
        }
        else if (ui->MElineEdit->isVisible())
        {
            string value = ui->MElineEdit->text().toStdString();
            if (currentVarValType == DDS_INTEGER)
            {
                setItemClntCmnd.varValue = stoi(value);
            }
            else if (currentVarValType == DDS_FLOAT)
            {
                setItemClntCmnd.varValue = stof(value);
            }
            else if (currentVarValType == DDS_DOUBLE)
            {
                setItemClntCmnd.varValue = stod(value);
            }
            else if (currentVarValType == DDS_UNSIGNED)
            {
                setItemClntCmnd.varValue = stoul(value);
            }
            else
            {
                QString val = QString::fromStdString(value.c_str());
                setItemClntCmnd.varValue = val.toUtf8().data();
            }
        }
        else if (ui->radioButton1_1->isVisible())
        {
            setItemClntCmnd.abort = "SELECTION";

            QString enumVal;
            if (ui->radioButton1_2->isChecked())
            {
                enumVal = ui->radioButton1_2->text();
            }
            else if (ui->radioButton1_3->isChecked())
            {
                enumVal = ui->radioButton1_3->text();
            }
            else if (ui->radioButton1_4->isChecked())
            {
                enumVal = ui->radioButton1_4->text();
            }
            else if (ui->radioButton1_5->isChecked())
            {
                enumVal = ui->radioButton1_5->text();
            }
            else
            {
                enumVal = ui->radioButton1_1->text();
            }

            map<QString, int>::const_iterator it;
            for (it = enumMap.begin(); it != enumMap.end(); ++it)
            {
                if (it->first == enumVal)
                {
                    int val = it->second;
                    setItemClntCmnd.varValue = val;
                    break;
                }
            }
        }

        ProtoRpc::ClientCommand& command = setItemClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&setItemClntCmnd);
    }
}

void Dialog::on_MEsecond_clicked()
{
    SetItemClntCmnd setItemClntCmnd;
    setItemClntCmnd.itemID = 0;
    setItemClntCmnd.abort = "ABORT";
    userAbort = true;
    setItemClntCmnd.itemType = "METHOD";
    setItemClntCmnd.varValue = "";
    if (protocolType == ProtocolType::HART)
    {
        setItemClntCmnd.itemInDD = true;
    }
    ui->MEfirst->setEnabled(false);
    ui->MEsecond->setEnabled(false);

    ProtoRpc::ClientCommand& command = setItemClntCmnd;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&setItemClntCmnd);
}

void Dialog::on_block_channel_back_clicked()
{
    Show(7, true);
}

void Dialog::executeMethodFromMenu(unsigned long itemID, string tag)
{
    methodExecutedFromMenu = true;
    userAbort = false;

    block = false;
    setEnable(true, "");

    Show(8, true);

    selectedType = "METHOD";

    block = true;
    setEnable(false, "Executing Method.\nPlease wait..!");

    GetItemClntCmnd getItemClntCmd;
    getItemClntCmd.itemID = itemID;

    map<unsigned int, ItemInfo>::const_iterator it;
    for (it = itemMap.begin(); it != itemMap.end(); ++it)
    {
        unsigned int index = ui->tableWidget_2->selectionModel()->currentIndex().row();
        if (it->first == (index + 1))
        {
            ItemInfo itemInfo = it->second;
            getItemClntCmd.itemInDD = itemInfo.itemInDD;
            getItemClntCmd.itemType = itemInfo.itemType;
            getItemClntCmd.itemContainerTag = tag;
            break;
        }
    }

    ProtoRpc::ClientCommand& command = getItemClntCmd;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&getItemClntCmd);
}

//get menu info button
void Dialog::getMenu()
{
    QString itemIDStr;
    QString itemTypeStr;
    QString itemNameStr;
    QString itemTag;
    int row = 0;

    ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
    ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
    ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
    ui->tableWidget_2->hideColumn(3);
    ui->close->setEnabled(true);
    GetItemClntCmnd getItemClntCmnd;

    if(isBack == false)
    {
        if(ui->tableWidget_2->rowCount() > 0)
        {
            row = ui->tableWidget_2->selectedRanges().front().bottomRow();
            if(row >= 0)
            {
                itemIDStr   = ui->tableWidget_2->item(row, 0)->text();
                itemTypeStr = ui->tableWidget_2->item(row, 1)->text();
                itemNameStr = ui->tableWidget_2->item(row, 2)->text();
                itemTag     = ui->tableWidget_2->item(row, 3)->text();

                pathMap.insert(pair<int, QString>(++pathIndex, itemNameStr));
                displayMenuPath();
            }
            getItemClntCmnd.itemID = itemIDStr.toULong();
        }
    }
    else
    {
        ui->tableWidget_2->setRowCount(0);
    }

    getItemClntCmnd.itemContainerTag = itemTag.toStdString();

    if(itemTypeStr.toStdString().compare("VARIABLE") == 0 || itemTypeStr.toStdString().compare("IMAGE") == 0)
    {
        block = true;
        setEnable(false, "Reading parameter.\nPlease wait..!");

        if (itemTypeStr.toStdString().compare("VARIABLE") == 0)
        {
            selectedType = "VARIABLE";
        }
        else if (itemTypeStr.toStdString().compare("IMAGE") == 0)
        {
            selectedType = "IMAGE";
        }

        ReadValue(stoul(itemIDStr.toStdString()), itemTag.toStdString());
        return;
    }
    else if(itemTypeStr.toStdString().compare("METHOD") == 0)
    {
        block = true;
        setEnable(false, "Executing Method.\nPlease wait..!");

        executeMethodFromMenu(stoul(itemIDStr.toStdString()), itemTag.toStdString());
        return;
    }

    QString idStr = ui->tableWidget_2->item(row, 0)->text();
    unsigned int id = idStr.toLongLong();
    itemID = id;
    map<unsigned int, ItemInfo>::const_iterator it;
    for (it = itemMap.begin(); it != itemMap.end(); ++it)
    {
        unsigned int index = ui->tableWidget_2->selectionModel()->currentIndex().row();
        if (it->first == (index + 1))
        {
            ItemInfo itemInfo = it->second;
            getItemClntCmnd.itemInDD = itemInfo.itemInDD;
            getItemClntCmnd.itemType = itemInfo.itemType;
            getItemClntCmnd.varContainerID = itemInfo.varContainerID;
            getItemClntCmnd.varSubIndex = itemInfo.varSubIndex;
            getItemClntCmnd.itemHasRelation = itemInfo.itemHasRelation;
            isWAO = itemInfo.itemHasRelation;

            if (itemInfo.bitIndex > 0)
            {
                getItemClntCmnd.varBitIndex = itemInfo.bitIndex;
            }
            break;
        }
    }

    if(itemTypeStr.toStdString().compare("VARIABLE") != 0 ||
       itemTypeStr.toStdString().compare("METHOD")   != 0 ||
       itemTypeStr.toStdString().compare("IMAGE")    != 0)
    {
        selectedType = "MENU";
        if (getItemClntCmnd.varContainerID != 0 || getItemClntCmnd.varSubIndex != 0)
        {
            selectedType = "VARIABLE";
        }
        if (itemTypeStr.toStdString().compare("MENU") == 0)
        {
            preED = true;
            selectedType = "MENU";

            tempGetItemClntCmd = getItemClntCmnd;

            execPreEditActionClntCmd.itemID = getItemClntCmnd.itemID;
            execPreEditActionClntCmd.itemContainerTag = getItemClntCmnd.itemContainerTag;
            execPreEditActionClntCmd.itemType = getItemClntCmnd.itemType;
            execPreEditActionClntCmd.itemInDD =getItemClntCmnd.itemInDD;

            ProtoRpc::ClientCommand& command = execPreEditActionClntCmd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&execPreEditActionClntCmd);
        }
        else
        {
            backMap.insert(pair<int, GetItemClntCmnd>(++backIndex, getItemClntCmnd));
            ProtoRpc::ClientCommand& command = getItemClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            block = true;
            setEnable(false, "Fetching Menu.\nPlease wait..!");

            executeClientCommand(&getItemClntCmnd);
        }
    }
}

//get menu button
void Dialog::on_GetMenu_clicked()
{
    ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
    ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
    ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
    ui->tableWidget_2->hideColumn(3);

    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    backIndex = 0;
    backMap.clear();
    menuNavigation(val);

    Show(10,true);
    pathIndex = 0;
    pathMap.clear();
    displayMenuPath();
}

void Dialog::on_tableWidget_2_cellClicked(int row, int column)
{
    (void)row;
    (void)column;
    isBack = false;
    ui->close->setEnabled(true);
    getMenu();
}

//back menu button clicked
void Dialog::on_getMenuButton_2_clicked()
{
    string type = selectedType;
    if (postED && type.compare("MENU") == 0)
    {
        ExecPostEditActionClntCmd execPostEditActionClntCmd;

        execPostEditActionClntCmd.itemID = execPreEditActionClntCmd.itemID;
        execPostEditActionClntCmd.itemContainerTag = execPreEditActionClntCmd.itemContainerTag;
        execPostEditActionClntCmd.itemType = execPreEditActionClntCmd.itemType;
        execPostEditActionClntCmd.itemInDD =execPreEditActionClntCmd.itemInDD;

        ProtoRpc::ClientCommand& command = execPostEditActionClntCmd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&execPostEditActionClntCmd);
    }
    else
    {
        ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
        ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
        ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
        ui->tableWidget_2->hideColumn(3);

        if (backMap.size() > 0)
        {
            backMap.erase(prev(backMap.end()));
            --backIndex;
        }
        if (backMap.size() < 1)
        {
            qlfrObj.clear();
            backIndex = 0;
            backMap.clear();
            menuNavigation(val);
        }
        else
        {
            map<int, GetItemClntCmnd>::iterator itr;
            itr = backMap.end();
            --itr;
            GetItemClntCmnd cmd = itr->second;

            selectedType = "MENU";
            ProtoRpc::ClientCommand& command = cmd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            block = true;
            setEnable(false, "Fetching Menu.\nPlease wait..!");

            executeClientCommand(&cmd);
        }
        currentItemID = 0;
        if (pathIndex > 0)
        {
            pathMap.erase(prev(pathMap.end()));
            --pathIndex;
        }
        displayMenuPath();
    }
}

void Dialog::ReadValue(unsigned long itemID, string tag)
{
    this->itemID = itemID;
    GetItemClntCmnd getItemClntCmnd;
    getItemClntCmnd.itemID = itemID;

    map<unsigned int, ItemInfo>::const_iterator it;
    for (it = itemMap.begin(); it != itemMap.end(); ++it)
    {
        unsigned int index = ui->tableWidget_2->selectionModel()->currentIndex().row();
        if (it->first == (index + 1))
        {
            ItemInfo itemInfo = it->second;
            getItemClntCmnd.itemInDD = itemInfo.itemInDD;
            getItemClntCmnd.itemType = itemInfo.itemType;

            if (0 == itemInfo.itemType.compare("VARIABLE"))
            {
                getItemClntCmnd.bVariable = true;
            }
            getItemClntCmnd.itemContainerTag = tag;

            getItemClntCmnd.itemHasRelation = itemInfo.itemHasRelation;
            isWAO = itemInfo.itemHasRelation;

            if (isWAOMenu)
            {
                getItemClntCmnd.itemHasRelation = false;
            }

            getItemClntCmnd.varContainerID = itemInfo.varContainerID;
            getItemClntCmnd.varSubIndex = itemInfo.varSubIndex;
            if (itemInfo.bitIndex > 0)
            {
                getItemClntCmnd.varBitIndex = itemInfo.bitIndex;
            }
            break;
        }
    }

    if (isWAO)
    {
        backMap.insert(pair<int, GetItemClntCmnd>(++backIndex, getItemClntCmnd));
    }
    ProtoRpc::ClientCommand& command = getItemClntCmnd;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&getItemClntCmnd);
}

//support to navigate to previous screen
void Dialog::on_close_2_clicked()
{
    ui->HARTItemValTxt->clear();
    ui->HARTItemValcmb->clear();
    ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
    ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
    ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
    ui->tableWidget_2->hideColumn(3);
    Show(10, true);
    if (pathIndex > 0)
    {
        pathMap.erase(prev(pathMap.end()));
        --pathIndex;
        displayMenuPath();
    }
}

int poll = 0;
int master = 0;
//bus param config
void Dialog::on_bus_config_btn_2_clicked()
{
    if(!block)
    {
        ConfigureSettings (false);
    }
}

void Dialog::ConfigureSettings (bool reset)
{
    SetClntAppSettings setSettingClntCmnd;

    int polloption = ui->pollbyoption_combo_2->currentIndex();
    int setDefault = reset == true ? 1 : 0;
    if(poll != polloption || setDefault == 1)
    {
        if(setDefault != 1)
        {
            setSettingClntCmnd.pollOptionType = polloption;
        }
        setSettingClntCmnd.setDefaultType = setDefault;

        ProtoRpc::ClientCommand& command = setSettingClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        auto settingsClntCmdResp = setSettingClntCmnd.execute(cpmClient);

        string response = settingsClntCmdResp.toJsonString();
        Json::Reader parser;
        Json::Value readResult;
        parser.parse(response, readResult, true);
        string st = "\nResponse: \n" + response;
        QString s = QString::fromUtf8(st.c_str());
        print_log(s);
        poll = polloption;
    }

    SetClntBusParams setBusParams;
    setBusParams.protocolType = 1;
    int masterType = ui->master_combo_3->currentIndex();
    if(master != masterType || setDefault == 1)
    {
        if(setDefault != 1)
        {
            setBusParams.masterType = masterType;
        }
        setBusParams.setDefaultType = setDefault;
        ProtoRpc::ClientCommand& command1 = setBusParams;
        ProtoRpc::Request request1 = command1.buildRequest();
        string requestString1 = "\nRequest: \n" + request1.debugString();
        print_log(requestString1.c_str());


        auto settingsClntCmdResp = setBusParams.execute(cpmClient);

        string response1 = settingsClntCmdResp.toJsonString();
        Json::Reader parser;
        Json::Value readResult;
        parser.parse(response1, readResult, true);
        string st = "\nResponse: \n" + response1;
        QString s = QString::fromUtf8(st.c_str());
        print_log(s);
    }
}

void Dialog::on_HARTSendBtn_clicked()
{
    if (ui->HARTSendBtn->text().compare("Edit") == 0)
    {
        isVariableEdit = true;
        SetItemClntCmnd setItemClntCmnd;
        setItemClntCmnd.itemID = itemID;
        map<unsigned int, ItemInfo>::const_iterator it;
        for (it = itemMap.begin(); it != itemMap.end(); ++it)
        {
            unsigned int index = ui->tableWidget_2->selectionModel()->currentIndex().row();
            if (it->first == (index + 1))
            {
                ItemInfo itemInfo = it->second;
                setItemClntCmnd.itemContainerTag = itemInfo.itemContainerTag;
                setItemClntCmnd.itemType = itemInfo.itemType;
                if (protocolType == ProtocolType::HART)
                {
                    setItemClntCmnd.itemInDD = true;
                }
                if (0 == itemInfo.itemType.compare("VARIABLE"))
                {
                    setItemClntCmnd.bVariable = true;
                }
                setItemClntCmnd.containerId = itemInfo.varContainerID;
                setItemClntCmnd.subindex = itemInfo.varSubIndex;

                if (itemInfo.bitIndex > 0)
                {
                    setItemClntCmnd.varBitIndex = itemInfo.bitIndex;
                }
                break;
            }
        }

        setItemClntCmnd.abort = "EDIT";

        ProtoRpc::ClientCommand& command = setItemClntCmnd;
        ProtoRpc::Request request = command.buildRequest();
        string requestString = "\nRequest: \n" + request.debugString();
        print_log(requestString.c_str());

        executeClientCommand(&setItemClntCmnd);
        ui->HARTItemValcmb->setEnabled(true);
        ui->HARTItemValTxt->setEnabled(true);
        ui->dateTimeEdit_2->setEnabled(true);
    }
    else
    {
        if (!block)
        {
            QString value;
            bool bitEnumItemValue = false;
            if (isEnum == 3)
            {
                bitEnumItemValue = ui->checkBoxOnOff->isChecked();
            }
            else if (isEnum == 1)
            {
                int val = 0;
                QString enumVal = ui->HARTItemValcmb->currentText();

                map<QString, int>::const_iterator it;
                for (it = paramMap.begin(); it != paramMap.end(); ++it)
                {
                    if (it->first == enumVal)
                    {
                        val = it->second;
                        break;
                    }
                }
                value = QString::number(val);
            }
            else if (isEnum == 2)
            {
                int val = 0;
                if (ui->checkBox_1->isChecked())
                {
                    val += 1;
                }
                if (ui->checkBox_2->isChecked())
                {
                    val += 2;
                }
                if (ui->checkBox_3->isChecked())
                {
                    val += 4;
                }
                if (ui->checkBox_4->isChecked())
                {
                    val += 8;
                }
                if (ui->checkBox_5->isChecked())
                {
                    val += 16;
                }
                if (ui->checkBox_6->isChecked())
                {
                    val += 32;
                }
                if (ui->checkBox_7->isChecked())
                {
                    val += 64;
                }
                if (ui->checkBox_8->isChecked())
                {
                    val += 128;
                }
                value = QString::number(val);
            }
            else
            {
                value = ui->HARTItemValTxt->toPlainText();
            }

            block = true;
            setEnable(false, "Sending parameter value.\nPlease wait..!");

            SetItemClntCmnd setItemClntCmnd;
            setItemClntCmnd.itemID = itemID;
            map<unsigned int, ItemInfo>::const_iterator it;
            for (it = itemMap.begin(); it != itemMap.end(); ++it)
            {
                unsigned int index = ui->tableWidget_2->selectionModel()->currentIndex().row();
                if (it->first == (index + 1))
                {
                    ItemInfo itemInfo = it->second;
                    setItemClntCmnd.itemContainerTag = itemInfo.itemContainerTag;
                    setItemClntCmnd.itemType = itemInfo.itemType;
                    if (protocolType == ProtocolType::HART)
                    {
                        setItemClntCmnd.itemInDD = true;
                    }
                    if (0 == itemInfo.itemType.compare("VARIABLE"))
                    {
                        setItemClntCmnd.bVariable = true;
                    }
                    setItemClntCmnd.containerId = itemInfo.varContainerID;
                    setItemClntCmnd.subindex = itemInfo.varSubIndex;
                    if (itemInfo.bitIndex > 0)
                    {
                        setItemClntCmnd.varBitIndex = itemInfo.bitIndex;
                    }
                    break;
                }
            }

            bool success = true;

            if (currentVarType.compare("UNSIGNED_INTEGER") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    unsigned long val = stoul(value.toStdString());
                    setItemClntCmnd.varValue = val;
                }
                else
                {
                    success = false;
                }
            }
            else if (currentVarType.compare("BOOLEAN") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    unsigned long val = stoul(value.toStdString());
                    if (0 == val)
                    {
                        bool bVal = false;
                        setItemClntCmnd.varValue = bVal;
                    }
                    else if (1 == val)
                    {
                        bool bVal = true;
                        setItemClntCmnd.varValue = bVal;
                    }
                    else
                    {
                        success = false;
                    }
                }
                else
                {
                    success = false;
                }

            }
            else if (currentVarType.compare("DURATION") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    unsigned long val = stoul(value.toStdString());
                    setItemClntCmnd.varValue = val;
                }
                else
                {
                    success = false;
                }
            }
            else if (currentVarType.compare("TIME_VALUE_EPOCH") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = val;
            }
            else if (currentVarType.compare("TIME_VALUE_SCALED") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = (val*32000);
            }
            else if (currentVarType.compare("TIME_VALUE_DIFF") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = (val*32000);
            }
            else if (currentVarType.compare("DATE") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = val;
            }
            else if (currentVarType.compare("TIME") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = val;
            }
            else if (currentVarType.compare("DATE_AND_TIME") == 0)
            {
                value = ui->dateTimeEdit_2->text();
                time_t val = dateAndTimeToLong(value.toStdString().c_str(), "%m/%d/%Y %H:%M:%S");
                setItemClntCmnd.varValue = val;
            }
            else if (currentVarType.compare("INTEGER") == 0 ||
                    currentVarType.compare("ENUMERATION") == 0 ||
                    currentVarType.compare("BITENUMERATION") == 0 ||
                    currentVarType.compare("INDEX") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    int val = stoi(value.toStdString());
                    setItemClntCmnd.varValue = val;
                }
                else
                {
                    success = false;
                }
            }
            else if (currentVarType.compare("BITENUMERATION_ITEM") == 0)
            {
                setItemClntCmnd.varValue = bitEnumItemValue;
            }
            else if (currentVarType.compare("FLOAT") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    float val = stof(value.toStdString());
                    setItemClntCmnd.varValue = val;
                }
                else
                {
                    success = false;
                }
            }
            else if(currentVarType.compare("DOUBLE") == 0)
            {
                if (isNumeric(value.toStdString()))
                {
                    double val = stod(value.toStdString());
                    setItemClntCmnd.varValue = val;
                }
                else
                {
                    success = false;
                }
            }
            else if(currentVarType.compare("BITSTRING") == 0 ||
                currentVarType.compare("PASSWORD") == 0 ||
                currentVarType.compare("OCTETSTRING") == 0 ||
                currentVarType.compare("PACKED_ASCII") == 0 ||
                currentVarType.compare("VISIBLESTRING") == 0 ||
                currentVarType.compare("EUC") == 0 ||
                currentVarType.compare("ASCII") == 0)
            {
                setItemClntCmnd.varValue = value.toUtf8().data();
            }

            if (success)
            {
                setItemClntCmnd.abort = "WRITE";

                ProtoRpc::ClientCommand& command = setItemClntCmnd;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                executeClientCommand(&setItemClntCmnd);
            }
            else
            {
                block = false;
                setEnable(true, "");
                QMessageBox::question(this, "Error", "Enter correct value..!",
                                                QMessageBox::Ok);
            }
        }
    }
}

bool Dialog::isNumeric(string s)
{
    bool valid = true;
    char *endptr[1];
    strtol(s.c_str(), endptr, 0);
    if (*endptr == s.c_str())
        valid = false;
    return valid;
}

void Dialog::on_settings_reset_btn_clicked()
{
    if(!block)
    {
        ConfigureSettings(true);
    }
}

void Dialog::on_send_json_req_btn_clicked()
{
    if(protocolType == ProtocolType::HART)
    {
        ui->pollbyoption_combo_2->addItem("0");
        ui->pollbyoption_combo_2->addItem("0-15");
        ui->pollbyoption_combo_2->addItem("0-63");

        ui->master_combo_3->addItem("SECONDARY");
        ui->master_combo_3->addItem("PRIMARY");
    }

    SendJsonRequest sendJsonRequest;
    sendJsonRequest.command =
            ui->client_cmnds_list->currentText().toStdString();

    string inputTxt = ui->json_input_txt->toPlainText().toStdString();

    Json::Reader parser;
    Json::Value readResult;
    parser.parse(inputTxt, readResult, true);

    sendJsonRequest.params = readResult;

    ProtoRpc::ClientCommand& command = sendJsonRequest;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    executeClientCommand(&sendJsonRequest);
}

void Dialog::on_MEthird_clicked()
{
    ui->frameHelp->setVisible(true);
}

void Dialog::on_MEHelpClose_clicked()
{
    ui->frameHelp->setVisible(false);
}

void Dialog::executeClientCommand(ProtoRpc::JsonClientCommand* pClientCommand)
{
    Json::Value resultData = pClientCommand->execute(cpmClient).toJsonValue();

    if (resultData.isObject() &&
        resultData.isMember("error"))
    {
        Json::Value errorData = resultData["error"];
        if (errorData.isMember("code"))
        {
            int errorCode = errorData["code"].asInt();
            if (EC_COMMAND_IN_PROGRESS == errorCode)
            {
                qDebug() << QString("Command in progress...");
                qDebug() << QString("Sending the command once again...");
                executeClientCommand(pClientCommand);
            }
            else
            {
                qDebug() << "Error code for command in progress: " << errorCode;
            }
        }
    }
}

void Dialog::handleSetProtocolTypeResult(const Json::Value& result)
{
    string response = result.toStyledString();
    setEnable(true, "");
    block = false;
    if (response.find("ok") != string::npos || response.find("90101") != string::npos)
    {
        Show(3, true);
        ui->initBtn->setEnabled(true);
        ui->connectBtn->setEnabled(false);

        if(protocolType == ProtocolType::HART || protocolType == ProtocolType::PB)
        {
            ui->busparam->setEnabled(true);
        }
        ui->startscanBtn->setEnabled(false);

        block = true;

        // Add client commands lists
        ui->client_cmnds_list->clear();
        QStringList stringList1;
        if (protocolType == ProtocolType::HART)
        {
            stringList1.append("SetProtocolType");
            stringList1.append("Initialize");
            stringList1.append("StartScan");
            stringList1.append("StopScan");
            stringList1.append("Connect");
            stringList1.append("Disconnect");
            stringList1.append("Shutdown");
            stringList1.append("GetAppSettings");
            stringList1.append("SetAppSettings");
            stringList1.append("GetBusParameters");
            stringList1.append("SetBusParameters");
            stringList1.append("GetItem");
            stringList1.append("SetItem");
            stringList1.append("ProcessSubscription");
            stringList1.append("SetLangCode");
        }
        else
        {
            stringList1.append("SetProtocolType");
            stringList1.append("Initialize");
            stringList1.append("StartScan");
            stringList1.append("StopScan");
            stringList1.append("Connect");
            stringList1.append("Disconnect");
            stringList1.append("Shutdown");

            stringList1.append("GetAppSettings");
            stringList1.append("SetAppSettings");

            stringList1.append("GetBusParameters");
            stringList1.append("SetBusParameters");
            stringList1.append("GetFieldDeviceConfig");
            stringList1.append("SetFieldDeviceConfig");
            stringList1.append("GetItem");
            stringList1.append("SetItem");
            stringList1.append("ProcessSubscription");
            stringList1.append("SetLangCode");
        }

        ui->client_cmnds_list->addItems(stringList1);

        setEnable(false, "Language code setting is in progress.\nPlease wait..!");

        // Set the language code once initialization is successful
        setLangCode();
    }
    else
    {
        Show(1, true);
    }
}

void Dialog::handleInitializeResult(const Json::Value& result)
{
    string response = result.toStyledString();
    setEnable(true, "");
    block = false;

    if (response.find("ok") != string::npos)
    {
        isInitialized = true;
        ui->close->setText("&Shutdown");
        ui->initBtn->setEnabled(false);
        ui->busparam->setEnabled(false);
        ui->startscanBtn->setEnabled(true);
    }
    else
    {
        Show(3, true);
    }
}

void::Dialog::handleSetLangCodeResult(const Json::Value &result)
{
    setEnable(true, "");
    block = false;

    string response = result.toStyledString();

    if (response.find("ok") != string::npos)
    {
        setLanUICntrlState(false);
    }
}

void::Dialog::handleExecPreEditAction(const Json::Value &result)
{
    if (!ui->MEFrame->isVisible())
    {
        if (preED)
        {
            string root = result.toStyledString();
            if (root.find("91107") != string::npos)
            {
                preED = false;
                postED = true;

                backMap.insert(pair<int, GetItemClntCmnd>(++backIndex, tempGetItemClntCmd));
                ProtoRpc::ClientCommand& command = tempGetItemClntCmd;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                block = true;
                setEnable(false, "Fetching Menu.\nPlease wait..!");

                executeClientCommand(&tempGetItemClntCmd);
            }
        }
    }
}

void Dialog::handleExecPostEditAction(const Json::Value &result)
{
    if (!ui->MEFrame->isVisible())
    {
        if (postED)
        {
            string root = result.toStyledString();
            if (root.find("91106") != string::npos)
            {
                postED = false;

                ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
                ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
                ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
                ui->tableWidget_2->hideColumn(3);

                if (backMap.size() > 0)
                {
                    backMap.erase(prev(backMap.end()));
                    --backIndex;
                }
                if (backMap.size() < 1)
                {
                    qlfrObj.clear();
                    backIndex = 0;
                    backMap.clear();
                    menuNavigation(val);
                }
                else
                {
                    map<int, GetItemClntCmnd>::iterator itr;
                    itr = backMap.end();
                    --itr;
                    GetItemClntCmnd cmd = itr->second;

                    selectedType = "MENU";
                    ProtoRpc::ClientCommand& command = cmd;
                    ProtoRpc::Request request = command.buildRequest();
                    string requestString = "\nRequest: \n" + request.debugString();
                    print_log(requestString.c_str());

                    block = true;
                    setEnable(false, "Fetching Menu.\nPlease wait..!");

                    executeClientCommand(&cmd);
                }
                currentItemID = 0;
                if (pathIndex > 0)
                {
                    pathMap.erase(prev(pathMap.end()));
                    --pathIndex;
                }
                displayMenuPath();
            }
        }
    }
}

void::Dialog::setLanUICntrlState(bool state)
{
    ui->lang_edit->setEnabled(state);
    ui->shortstr_chkbox->setEnabled(state);
}


void Dialog::handleStartScanResult(const Json::Value& result)
{
    string response = result.toStyledString();
    if (response.find("error") != string::npos)
    {
        ui->startscanBtn->setEnabled(true);
        ui->stopscan->setEnabled(false);
        isScanning = false;
    }
    else
    {
        ui->startscanBtn->setEnabled(false);
        ui->stopscan->setEnabled(true);
        isScanning = true;
    }
    setEnable(true, "");
    block = false;
}

void Dialog::handleStopScanResult(const Json::Value& result)
{
    setEnable(true, "");
    block = false;
    string response = result.toStyledString();
    if (response.find("error") != string::npos)
    {
        ui->startscanBtn->setEnabled(false);
        ui->stopscan->setEnabled(true);
        isScanning = true;
    }
    else
    {
        ui->startscanBtn->setEnabled(true);
        ui->stopscan->setEnabled(false);
        isScanning = false;
        if (stopAfterProcessID == 1)
        {
            Connect();
        }
        else if (stopAfterProcessID == 2)
        {
            on_setupBtn_clicked();
        }
        else if (stopAfterProcessID == 3)
        {
            on_close_clicked();
        }
        else
        {
            Show(3, true);
        }
        stopAfterProcessID = 0;
    }
}

void Dialog::handleConnectResult(const Json::Value& result)
{
    val = result;

    setEnable(true, "");
    block = false;

    ui->connectBtn->setEnabled(false);
    ui->setupBtn->setEnabled(false);
    ui->resultTable->setEnabled(false);
    ui->close->setEnabled(false);
    ui->startscanBtn->setEnabled(false);

    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    if(protocolType == ProtocolType::FF)
    {
        string response = result.toStyledString();
        setEnable(true, "");
        block = false;

        Json::Reader parser;
        Json::Value readResult;
        parser.parse(response, readResult, true);

        itemInDD = readResult["result"]["ItemInfo"]["ItemInDD"].asBool();
        int blkCount = 0;
        for(auto &devNode : readResult["result"]["ItemInfo"]["ItemList"])
        {
            if (devNode.isObject())
            {
                string dataTag = devNode["ItemContainerTag"].asString();
                blkTagList.append(dataTag.c_str());
                blkCount++;
            }
        }

        Show(4, true);
    }
    pathMap.clear();
    pathIndex = 0;
    displayMenuPath();
    backMap.clear();
    backIndex = 0;
    menuNavigation(result);
}

void Dialog::handleDisconnectResult(const Json::Value& response )
{
    string result = response.toStyledString();
    setEnable(true, "");
    block = false;

    if (result.find("ok") != string::npos)
    {
        ui->connectBtn->setEnabled(true);
        if(protocolType == ProtocolType::FF)
        {
            ui->setupBtn->setEnabled(true);
            ui->busparam->setEnabled(false);
        }
        else
        {
            ui->setupBtn->setEnabled(false);
            ui->busparam->setEnabled(false);
        }
        ui->resultTable->setEnabled(true);
        ui->close->setEnabled(true);
        ui->startscanBtn->setEnabled(true);
        Show(3, true);
    }
    else
    {
        //Show(4, true);
    }
}

void Dialog::handleGetItemResult(const Json::Value& result)
{
    string response = result.toStyledString();
    Json::Reader parser;
    Json::Value readResult;
    parser.parse(response, readResult, true);
    if (selectedType.compare("METHOD") != 0)
    {
        string type = result["result"]["ItemType"].asString();

        if (type.compare("VARIABLE") == 0)
        {
            handleVariableResult(readResult);
        }
        else if (type.compare("IMAGE") == 0)
        {
            handleImageResult(readResult);
        }
        else
        {
            handleMenuResult(readResult);
        }
    }
    block = false;
    setEnable(true, "");
}

void Dialog::handleMenuResult(Json::Value readResult)
{
    int count = 0;
    Json::Value itemlist;

    count = readResult["result"]["ItemInfo"]["ItemList"].size();
    itemlist =  readResult["result"]["ItemInfo"]["ItemList"];
    isWAOMenu = readResult["result"]["ItemInfo"]["ItemHasRelation"].asBool();

    ui->tableWidget_2->setRowCount(count);
    ui->tableWidget_2->hideColumn(3);
    itemMap.clear();
    int index = 0;

    int paramCount = 0;
    for (auto &PInfo : itemlist)
    {
        unsigned int id = PInfo["ItemID"].asUInt();
        string itemType = PInfo["ItemType"].asString();
        string tag = PInfo["ItemContainerTag"].asString();
        bool itemInDD = PInfo["ItemInDD"].asBool();
        string itemLabel = PInfo["ItemLabel"].asString();
        string itemHelp = PInfo["ItemHelp"].asString();

        bool itemHasRelation = PInfo["ItemInfo"]["ItemHasRelation"].asBool();

        bool itemReadOnly = PInfo["ItemInfo"]["ItemReadOnly"].asBool();
        unsigned int varContainerID = PInfo["ItemInfo"]["VarContainerID"].asUInt();
        unsigned int varSubIndex = PInfo["ItemInfo"]["VarSubIndex"].asUInt();
        unsigned int varBitIndex = 0;
        if (PInfo["ItemInfo"]["VarType"].asString().compare("ENUMERATION") == 0)
        {
            if (PInfo["ItemInfo"]["VarInfo"]["VarValType"].asString().compare("BITENUMERATION_ITEM") == 0)
            {
                varBitIndex = PInfo["ItemInfo"]["VarInfo"]["VarBitIndex"].asLargestInt();
            }
        }

        qlfrObj[id] = itemReadOnly;

        ItemInfo itemInfo{};
        itemInfo.itemID = id;
        itemInfo.itemContainerTag = tag;
        itemInfo.itemType = itemType;
        itemInfo.itemInDD = itemInDD;
        itemInfo.itemHelp = itemHelp;
        itemInfo.itemLabel = itemLabel;
        itemInfo.itemHasRelation = itemHasRelation;
        itemInfo.itemReadOnly = itemReadOnly;
        itemInfo.varContainerID = varContainerID;
        itemInfo.varSubIndex = varSubIndex;
        itemInfo.bitIndex = varBitIndex;

        itemMap.insert(pair<unsigned int, ItemInfo>(++index, itemInfo));

        string str = to_string(id);

        QTableWidgetItem *item = ui->tableWidget_2->item(paramCount, 0);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 0, item);
        }
        item->setText(QString::fromStdString(str.c_str()));

        item = ui->tableWidget_2->item(paramCount, 1);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 1, item);
        }
        item->setText(QString::fromStdString(itemType.c_str()));

        item = ui->tableWidget_2->item(paramCount, 2);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 2, item);
        }
        item->setText(QString::fromStdString(itemLabel.c_str()));

        item = ui->tableWidget_2->item(paramCount, 3);
        if(!item) {
            item = new QTableWidgetItem();
            ui->tableWidget_2->setItem(paramCount, 3, item);
        }
        item->setText(QString::fromStdString(tag.c_str()));

        item = nullptr;

        paramCount++;
    }
}

void Dialog::handleVariableResult(Json::Value readResult)
{
    Show(12,true);

    string results;
    string itemType = readResult["result"]["ItemInfo"]["VarType"].asString();
    string itemVarType = readResult["result"]["ItemInfo"]["VarInfo"]["VarValType"].asString();
    currentVarType = itemVarType;

    bitIndex = 0;

    unsigned long ID = readResult["result"]["ItemID"].asLargestInt();
    unsigned long containerID = readResult["result"]["ItemInfo"]["VarContainerID"].asLargestInt();
    unsigned long subIndex = readResult["result"]["ItemInfo"]["VarSubIndex"].asLargestInt();
    string containerTag = readResult["result"]["ItemContainerTag"].asString();
    subItemID = ID;
    subVarContainerID = containerID;
    subSubIndex = subIndex;
    subItemType = itemType;
    subContainerTag = containerTag;

    bool readOnly = readResult["result"]["ItemInfo"]["ItemReadOnly"].asBool();
    ui->HARTSendBtn->setEnabled(!readOnly);
    ui->HARTItemValcmb->setEnabled(false);
    ui->HARTItemValTxt->setEnabled(false);
    ui->dateTimeEdit_2->setVisible(false);
    ui->dateTimeEdit_2->setEnabled(false);
    ui->checkBoxOnOff->setVisible(false);
    isReadOnly = readOnly;

    if (!isReadOnly)
    {
        ui->HARTSendBtn->setText("Edit");
    }

    if (itemType.compare("ENUMERATION") == 0)
    {
        paramMap.clear();
        ui->HARTItemValcmb->setVisible(false);
        ui->HARTItemValTxt->setVisible(false);
        ui->unit->setVisible(false);

        int varEnumVal = readResult["result"]["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
        ui->HARTItemValcmb->clear();
        QStringList enumList;
        for (auto &PInfo : readResult["result"]["ItemInfo"]["VarInfo"]["VarEnumInfo"])
        {
            string item = PInfo["VarEnumLabel"].asString();
            int varValue = PInfo["VarEnumValue"].asLargestInt();
            QString str = QString::fromUtf8(item.c_str());
            paramMap.insert(pair<QString, int>(str, varValue));
            enumList.append(str);
        }

        if (itemVarType.compare("BITENUMERATION_ITEM") == 0)
        {
            isEnum = 3;
            bool varVal = readResult["result"]["ItemInfo"]["VarInfo"]["VarValue"].asBool();
            unsigned int varBitIndex = readResult["result"]["ItemInfo"]["VarInfo"]["VarBitIndex"].asLargestUInt();
            ui->checkBoxOnOff->setVisible(true);
            ui->checkBoxOnOff->setEnabled(!readOnly);
            ui->checkBoxOnOff->setChecked(varVal);

            bitIndex = varBitIndex;
        }
        else if (itemVarType.compare("BITENUMERATION") == 0)
        {
            isEnum = 2;

            map<QString, int>::const_iterator it;
            for (it = paramMap.begin(); it != paramMap.end(); ++it)
            {
                if (it->second == 1)
                {
                    ui->checkBox_1->setText(it->first);
                    ui->checkBox_1->setVisible(true);
                }
                if (it->second == 2)
                {
                    ui->checkBox_2->setText(it->first);
                    ui->checkBox_2->setVisible(true);
                }
                if (it->second == 4)
                {
                    ui->checkBox_3->setText(it->first);
                    ui->checkBox_3->setVisible(true);
                }
                if (it->second == 8)
                {
                    ui->checkBox_4->setText(it->first);
                    ui->checkBox_4->setVisible(true);
                }
                if (it->second == 16)
                {
                    ui->checkBox_5->setText(it->first);
                    ui->checkBox_5->setVisible(true);
                }
                if (it->second == 32)
                {
                    ui->checkBox_6->setText(it->first);
                    ui->checkBox_6->setVisible(true);
                }
                if (it->second == 64)
                {
                    ui->checkBox_7->setText(it->first);
                    ui->checkBox_7->setVisible(true);
                }
                if (it->second == 128)
                {
                    ui->checkBox_8->setText(it->first);
                    ui->checkBox_8->setVisible(true);
                }
            }
            ui->checkBox_1->setChecked(false);
            ui->checkBox_2->setChecked(false);
            ui->checkBox_3->setChecked(false);
            ui->checkBox_4->setChecked(false);
            ui->checkBox_5->setChecked(false);
            ui->checkBox_6->setChecked(false);
            ui->checkBox_7->setChecked(false);
            ui->checkBox_8->setChecked(false);
            ui->checkBox_1->setEnabled(!readOnly);
            ui->checkBox_2->setEnabled(!readOnly);
            ui->checkBox_3->setEnabled(!readOnly);
            ui->checkBox_4->setEnabled(!readOnly);
            ui->checkBox_5->setEnabled(!readOnly);
            ui->checkBox_6->setEnabled(!readOnly);
            ui->checkBox_7->setEnabled(!readOnly);
            ui->checkBox_8->setEnabled(!readOnly);

            string temp = "";
            int bin = 0;
            int num = varEnumVal;
            while (num > 0)
            {
                bin = num % 2;
                temp += to_string(bin);
                num /= 2;
            }
            for(unsigned int x = 0; x != temp.length(); x++)
            {
                if (temp[x] == '1')
                {
                    if (x == 0)
                    {
                        ui->checkBox_1->setChecked(true);
                    }
                    else if (x == 1)
                    {
                        ui->checkBox_2->setChecked(true);
                    }
                    else if (x == 2)
                    {
                        ui->checkBox_3->setChecked(true);
                    }
                    else if (x == 3)
                    {
                        ui->checkBox_4->setChecked(true);
                    }
                    else if (x == 4)
                    {
                        ui->checkBox_5->setChecked(true);
                    }
                    else if (x == 5)
                    {
                        ui->checkBox_6->setChecked(true);
                    }
                    else if (x == 6)
                    {
                        ui->checkBox_7->setChecked(true);
                    }
                    else if (x == 7)
                    {
                        ui->checkBox_8->setChecked(true);
                    }
                }
            }
        }
        else
        {
            isEnum = 1;
            ui->HARTItemValcmb->addItems(enumList);
            map<QString, int>::const_iterator it;
            int index = 0;
            for (it = paramMap.begin(); it != paramMap.end(); ++it)
            {
                if (it->second == varEnumVal)
                {
                    for (int ind = 0; ind < ui->HARTItemValcmb->count(); ind++)
                    {
                        if (ui->HARTItemValcmb->itemText(ind) == it->first)
                        {
                            ui->HARTItemValcmb->setCurrentIndex(ind);
                            break;
                        }
                    }
                    break;
                }
                index++;
            }
            ui->HARTItemValTxt->setVisible(false);
            ui->HARTItemValcmb->setVisible(true);
        }
    }
    else if (itemType.compare("DATE_TIME") == 0)
    {
        isEnum = 0;
        results = readResult["result"]["ItemInfo"]["VarInfo"]["VarUnit"].asString();
        ui->unit->setText(results.c_str());
        results = readResult["result"]["ItemInfo"]["VarInfo"]["VarValue"].asString();
        ui->dateTimeEdit_2->setVisible(false);
        if (itemVarType.compare("TIME_VALUE_EPOCH") == 0)
        {
            unsigned long Epoch_time = stoul(results);
            string result_ms = readResult["result"]["ItemInfo"]["VarInfo"]["VarValInfo"]["VarValMilliSecPart"].asString();
            string result_us = readResult["result"]["ItemInfo"]["VarInfo"]["VarValInfo"]["VarValMicroSecPart"].asString();
            unsigned long millisec = stoul(result_ms);
            millisec += stoul(result_us);
            Epoch_time += (millisec/1000);
            string varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");

            QDate date;
            QTime time;
            date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
            time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

            QDate minDate;
            minDate.setDate(1972, 01, 01);
            ui->dateTimeEdit_2->setMinimumDate(minDate);
            ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
            ui->dateTimeEdit_2->clear();
            ui->dateTimeEdit_2->setDate(date);
            ui->dateTimeEdit_2->setTime(time);

            ui->HARTItemValTxt->setVisible(false);
            ui->dateTimeEdit_2->setVisible(true);
        }
        else if (itemVarType.compare("TIME_VALUE_DIFF") == 0)
        {
            unsigned long time_val_ms = stoul(results);
            unsigned long time_val_sec = time_val_ms/32000;
            string time_format = readResult["result"]["ItemInfo"]["VarInfo"]["VarValDisplayFormat"].asString();

            if (time_format.empty())
            {
                time_format = "%H:%M:%S";
            }
            string varValue = longToDateAndTime(time_val_sec, time_format.c_str());

            QTime time;
            time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

            ui->dateTimeEdit_2->setDisplayFormat("hh:mm:ss");
            ui->dateTimeEdit_2->setTime(time);

            ui->HARTItemValTxt->setVisible(false);
            ui->dateTimeEdit_2->setVisible(true);
        }
        else if (itemVarType.compare("TIME_VALUE_SCALED") == 0)
        {
            unsigned long time_val_ms = stoul(results);
            unsigned long time_val_sec = time_val_ms/32000;
            string time_format = "%H:%M:%S";
            string varValue = longToDateAndTime(time_val_sec, time_format.c_str());

            QTime time;
            time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

            ui->dateTimeEdit_2->setDisplayFormat("hh:mm:ss");
            ui->dateTimeEdit_2->setTime(time);

            ui->HARTItemValTxt->setVisible(false);
            ui->dateTimeEdit_2->setVisible(true);
        }
        else if (itemVarType.compare("DURATION") == 0)
        {
            ui->HARTItemValTxt->setText(results.c_str());
            ui->HARTItemValTxt->setVisible(true);
        }
        else
        {
            long Epoch_time = stol(results);
            string varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");

            if (itemVarType.compare("DATE") == 0)
            {
                QDate minDate;
                minDate.setDate(1900, 01, 01);
                ui->dateTimeEdit_2->setMinimumDate(minDate);
                ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy");
            }
            else if (itemVarType.compare("TIME") == 0)
            {
                QDate minDate;
                minDate.setDate(1984, 01, 01);
                ui->dateTimeEdit_2->setMinimumDate(minDate);
                ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
            }
            else if (itemVarType.compare("DATE_AND_TIME") == 0)
            {
                QDate minDate;
                minDate.setDate(1900, 01, 01);
                ui->dateTimeEdit_2->setMinimumDate(minDate);
                ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
            }
            else
            {

            }
            QDate date;
            QTime time;
            date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
            time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

            ui->dateTimeEdit_2->clear();
            ui->dateTimeEdit_2->setDate(date);
            ui->dateTimeEdit_2->setTime(time);

            ui->HARTItemValTxt->setVisible(false);
            ui->dateTimeEdit_2->setVisible(true);
        }
        ui->HARTItemValcmb->setVisible(false);
    }
    else
    {
        isEnum = 0;
        results = readResult["result"]["ItemInfo"]["VarInfo"]["VarValue"].asString();
        ui->HARTItemValTxt->setText(results.c_str());
        results = readResult["result"]["ItemInfo"]["VarInfo"]["VarUnit"].asString();
        ui->unit->setText(results.c_str());
        ui->HARTItemValcmb->setVisible(false);
        ui->HARTItemValTxt->setVisible(true);
    }
    ui->HARTItemValcmb->setEnabled(false);
    ui->HARTItemValTxt->setEnabled(false);
    ui->dateTimeEdit_2->setEnabled(false);

    results = readResult["result"]["ItemLabel"].asString();
    ui->label_17->setText(results.c_str());

    ui->createSub->setEnabled(true);
    ui->deleteSub->setEnabled(false);
    ui->startSub->setEnabled(false);
    ui->stopSub->setEnabled(false);
}

void Dialog::handleImageResult(Json::Value readResult)
{
    Show(12,true);
    int imageID = readResult["result"]["ItemID"].asLargestInt();
    (void)imageID;
    string imageName = readResult["result"]["ItemLabel"].asString();
    (void)imageName;
    string imagePath = readResult["result"]["ItemInfo"]["ImagePath"].asString();

    // Check whether it is required to query image data from remote machine
    if(remoteIC)
    {
        // Image file is generated in remote machine (BeagleBone/Starsky)
        string remICImgPath = QCoreApplication::applicationDirPath().toStdString();
        remICImgPath += "/remICImage";

        // Fetch image data from remote machine
        if (0 != scp.copyFile(imagePath.c_str(), remICImgPath.c_str()))
        {
            print_log("Unable to fetch remote file; Cannot display Image");
        }
        // Update the image path from remote path to host path
        imagePath = remICImgPath;
    }

    QPixmap* myPixmapPi = new QPixmap(imagePath.c_str());
    if (myPixmapPi->width()  > ui->customPlot->width() ||
        myPixmapPi->height() > ui->customPlot->height())
    {
        ui->customPlot->setPixmap(myPixmapPi->scaled(
                            ui->customPlot->size(),
                            Qt::KeepAspectRatio,
                            Qt::SmoothTransformation));
    }
    else
    {
        ui->customPlot->setPixmap(*myPixmapPi);
    }
    ui->groupBox_12->setVisible(false);
    ui->customPlot->setVisible(true);
}

void Dialog::handleSetItemResult(const Json::Value& result)
{
    ui->HARTSendBtn->setText("Send");
    ui->HARTItemValcmb->setEnabled(!isReadOnly);
    ui->HARTItemValTxt->setEnabled(!isReadOnly);
    ui->dateTimeEdit_2->setEnabled(!isReadOnly);

    if (ui->MEFrame->isVisible())
    {
        string root = result.toStyledString();
        if (root.find("error") != string::npos)
        {
            ui->MEfirst->setEnabled(true);
            ui->MEsecond->setEnabled(true);
        }
    }

    if (isVariableEdit)
    {
        preED = false;
        postED = false;
    }
    isVariableEdit = false;

    setEnable(true, "");
    block = false;

    if (!ui->MEFrame->isVisible())
    {
        if (preED)
        {
            string root = result.toStyledString();
            if (root.find("91107") != string::npos)
            {
                preED = false;
                postED = true;

                backMap.insert(pair<int, GetItemClntCmnd>(++backIndex, tempGetItemClntCmd));
                ProtoRpc::ClientCommand& command = tempGetItemClntCmd;
                ProtoRpc::Request request = command.buildRequest();
                string requestString = "\nRequest: \n" + request.debugString();
                print_log(requestString.c_str());

                block = true;
                setEnable(false, "Fetching Menu.\nPlease wait..!");

                executeClientCommand(&tempGetItemClntCmd);
            }
        }
        else if (postED)
        {
            string root = result.toStyledString();
            if (root.find("91106") != string::npos)
            {
                postED = false;

                ui->tableWidget_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Item ID"));
                ui->tableWidget_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Item Type"));
                ui->tableWidget_2->setHorizontalHeaderItem(2, new QTableWidgetItem("Item Label"));
                ui->tableWidget_2->hideColumn(3);

                if (backMap.size() > 0)
                {
                    backMap.erase(prev(backMap.end()));
                    --backIndex;
                }
                if (backMap.size() < 1)
                {
                    qlfrObj.clear();
                    backIndex = 0;
                    backMap.clear();
                    menuNavigation(val);
                }
                else
                {
                    map<int, GetItemClntCmnd>::iterator itr;
                    itr = backMap.end();
                    --itr;
                    GetItemClntCmnd cmd = itr->second;

                    selectedType = "MENU";
                    ProtoRpc::ClientCommand& command = cmd;
                    ProtoRpc::Request request = command.buildRequest();
                    string requestString = "\nRequest: \n" + request.debugString();
                    print_log(requestString.c_str());

                    block = true;
                    setEnable(false, "Fetching Menu.\nPlease wait..!");

                    executeClientCommand(&cmd);
                }
                currentItemID = 0;
                if (pathIndex > 0)
                {
                    pathMap.erase(prev(pathMap.end()));
                    --pathIndex;
                }
                displayMenuPath();
            }
        }
    }
}

void Dialog::handleGetBusParametersResult(const Json::Value& result)
{
    setEnable(true, "");
    block = false;
    string root = result.toStyledString();
    if (root.find("error") != string::npos)
    {
        Show(3, true);
    }
    else
    {
        if(protocolType == ProtocolType::HART)
        {
            int masterType = result["result"]["HART_BUS_PARAMS"]["masterType"].asInt();
            master = masterType;
            ui->master_combo_3->setCurrentIndex(masterType == 0 ? 0 : 1);

            if(!isSettingRequest)
            {
                GetClntAppSettings getAppSetting;

                ProtoRpc::ClientCommand& command1 = getAppSetting;
                ProtoRpc::Request request1 = command1.buildRequest();
                string requestString1 = "\nRequest: \n" + request1.debugString();
                print_log(requestString1.c_str());

                executeClientCommand(&getAppSetting);
                isSettingRequest = true;
                isBusParamRequest = true;
            }
        }
        else
        {
            tsl = result["result"]["PB_BUS_PARAMS"]["pb_bus_tsl"].asInt();
            minTsdr = result["result"]["PB_BUS_PARAMS"]["pb_bus_min_tsdr"].asInt();
            maxTsdr = result["result"]["PB_BUS_PARAMS"]["pb_bus_max_tsdr"].asInt();
            qTime = result["result"]["PB_BUS_PARAMS"]["pb_bus_tqui"].asInt();
            sTime = result["result"]["PB_BUS_PARAMS"]["pb_bus_tset"].asInt();
            ttr = result["result"]["PB_BUS_PARAMS"]["pb_bus_ttr"].asInt();
            g = result["result"]["PB_BUS_PARAMS"]["pb_bus_g"].asInt();
            hsa = result["result"]["PB_BUS_PARAMS"]["pb_bus_hsa"].asInt();
            maxrRetLimit = result["result"]["PB_BUS_PARAMS"]["pb_bus_max_retry_limit"].asInt();

            ui->MaxRetryLimit->setText(QString::number(maxrRetLimit));
            ui->MinTsdr->setText(QString::number(minTsdr));
            ui->MaxTsdr->setText(QString::number(maxTsdr));
            ui->SlotTime->setText(QString::number(tsl));
            ui->QuietTime->setText(QString::number(qTime));
            ui->SetupTime->setText(QString::number(sTime));
            ui->TargetRotationTime->setText(QString::number(ttr));
            ui->GapUpdateFactor->setText(QString::number(g));
            ui->HSA->setText(QString::number(hsa));

            GetClntAppSettings getAppSetting;

            ProtoRpc::ClientCommand& command1 = getAppSetting;
            ProtoRpc::Request request1 = command1.buildRequest();
            string requestString1 = "\nRequest: \n" + request1.debugString();
            print_log(requestString1.c_str());

            executeClientCommand(&getAppSetting);
        }

        Show(11, true);
    }
}

void Dialog::handleSetBusParametersResult(const Json::Value& result)
{
    (void)result;
}

void Dialog::handleGetFieldDeviceConfigResult(const Json::Value& result)
{
    if (protocolType != ProtocolType::HART)
    {
        int address = result["result"]["DeviceNodeAddress"].asInt();
        int deviceClass = result["result"]["DeviceClass"].asInt();
        string devTag = result["result"]["DeviceTag"].asString();

        QString strAddress = QString::number(address);

        ui->dev_class_combo->setCurrentIndex(deviceClass == 1 ? 0 : 1);
        ui->textEdit->setText(strAddress);
        ui->textEdit_11->setText(devTag.c_str());

        setEnable(true, "");
        block = false;

        Show(6, true);
    }
}

void Dialog::handleSetFieldDeviceConfigResult(const Json::Value& result)
{
    (void)result;
    block = true;
    refresh();
    block = false;

    ui->pushButton_7->setEnabled(false);
    ui->decommission_btn->setEnabled(true);
    ui->device_class_write_btn->setEnabled(true);
    ui->textEdit->setReadOnly(true);
    ui->textEdit_11->setReadOnly(true);
    ui->dev_class_combo->show();
    ui->Device_class_lbl->show();

    mapObj.clear();
    scanIndex = 0;
    setRow = 0;

    setEnable(true, "");
    Show(3, true);
}

void Dialog::handleGetAppSettingsResult(const Json::Value& result)
{
    if (protocolType == ProtocolType::HART)
    {
        int polloption = result["result"]["HART_APP_SETTINGS"]["PollByAddressOption"].asInt();
        poll = polloption;
        if(polloption == 0)
            ui->pollbyoption_combo_2->setCurrentIndex(0);
        else if(polloption == 1)
            ui->pollbyoption_combo_2->setCurrentIndex(1);
        else
            ui->pollbyoption_combo_2->setCurrentIndex(2);

        if(!isBusParamRequest)
        {
             isBusParamRequest = true;
            isSettingRequest = true;
            GetClntBusParams getBusParams;

            ProtoRpc::ClientCommand& command1 = getBusParams;
            ProtoRpc::Request request1 = command1.buildRequest();
            string requestString1 = "\nRequest: \n" + request1.debugString();
            print_log(requestString1.c_str());

            executeClientCommand(&getBusParams);
        }
    }
    else if(protocolType == ProtocolType::PB)
    {
        int nodeAddress = result["result"]["PB_APP_SETTINGS"]["pb_loc_add"].asInt();
        ui->Address->setText(QString::number(nodeAddress));
    }
    else
    {
       print_log("Feature Not Suported");
    }
}

void Dialog::handleSetAppSettingsResult(const Json::Value& result)
{
    (void)result;
}

void Dialog::handleProcessSubscriptionResult(const Json::Value& result)
{
    Json::FastWriter writer;
    string str = writer.write(result);
    if (str.find("ItemSubscriptionID") != string::npos)
    {
        int subscriptionID = result["result"]["ItemSubscriptionID"].asInt();
        subSubscriptionID = subscriptionID;
    }
    block = false;
    setEnable(true, "");
}

void Dialog::on_setupBtn_clicked()
{
    if (!block)
    {
        if (isScanning)
        {
            stopAfterProcessID = 2;
            on_stopscan_clicked();
        }
        else
        {
            int row = ui->resultTable->selectionModel()->currentIndex().row();
            int addr = ui->resultTable->model()->index(row, 1).data().toInt();
            if(addr > 16 && addr <= 247)
            {
                ui->textEdit->setReadOnly(true);
                ui->textEdit_11->setReadOnly(true);
                ui->dev_class_combo->show();
                ui->Device_class_lbl->show();
                ui->pushButton_7->setEnabled(false);
                ui->decommission_btn->setEnabled(true);
                ui->device_class_write_btn->setEnabled(true);
            }
            else
            {
                ui->textEdit->setReadOnly(false);
                ui->textEdit_11->setReadOnly(false);
                ui->dev_class_combo->hide();
                ui->Device_class_lbl->hide();
                ui->pushButton_7->setEnabled(true);
                ui->decommission_btn->setEnabled(false);
                ui->device_class_write_btn->setEnabled(false);
            }

            ui->dev_class_combo->clear();
            ui->dev_class_combo->addItem("Basic");
            ui->dev_class_combo->addItem("Link Master (LM)");

            block = true;
            setEnable(false, "Reading basic field device information.\nPlease wait..!");

            GetClntFieldDeviceConfig getClntFieldDeviceConfig;
            getClntFieldDeviceConfig.address = addr;

            ProtoRpc::ClientCommand& command1 = getClntFieldDeviceConfig;
            ProtoRpc::Request request1 = command1.buildRequest();
            string requestString1 = "\nRequest: \n" + request1.debugString();
            print_log(requestString1.c_str());

            executeClientCommand(&getClntFieldDeviceConfig);
        }
    }
}

void Dialog::on_busparam_clicked()
{
    if(!block)
    {
        if(protocolType == ProtocolType::HART)
        {
            ui->pollbyoption_combo_2->clear();
            ui->pollbyoption_combo_2->addItem("0");
            ui->pollbyoption_combo_2->addItem("0-15");
            ui->pollbyoption_combo_2->addItem("0-63");

            ui->master_combo_3->clear();
            ui->master_combo_3->addItem("SECONDARY");
            ui->master_combo_3->addItem("PRIMARY");

            GetClntAppSettings getSettingClntCmnd;

            ProtoRpc::ClientCommand& command = getSettingClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&getSettingClntCmnd);
        }
        else
        {
            ui->comboBox->setEnabled(false);
            ui->comboBox->addItem("31.25");
            GetClntBusParams getBusParamClntCmnd;

            ProtoRpc::ClientCommand& command = getBusParamClntCmnd;
            ProtoRpc::Request request = command.buildRequest();
            string requestString = "\nRequest: \n" + request.debugString();
            print_log(requestString.c_str());

            executeClientCommand(&getBusParamClntCmnd);
        }
    }
}

time_t Dialog::dateAndTimeToLong(const char* theTime, const char* format)
{
    tm tmTime;
    memset(&tmTime, 0, sizeof(tmTime));
    strptime(theTime, format, &tmTime);
    time_t val = timegm(&tmTime);
    return val;
}

string Dialog::longToDateAndTime(time_t epochTime, const char* format)
{
    char timestamp[64] = {0};
    strftime(timestamp, sizeof(timestamp), format, gmtime(&epochTime));
    return timestamp;
}

void Dialog::on_createSub_clicked()
{
    ui->createSub->setEnabled(false);

    SendJsonRequest sendJsonRequest;
    sendJsonRequest.command = "ProcessSubscription";

    string inputTxt =  "{\"ItemAction\":\"create\",\"ItemRefreshRate\":1,\"ItemList\":["
                       "{\"ItemContainerTag\":\"" + subContainerTag;
    inputTxt += "\",\"ItemID\":" + to_string(subItemID);
    inputTxt += ",\"ItemInfo\":{\"VarContainerID\":" + to_string(subVarContainerID)
            +  ",\"VarSubIndex\":" + to_string(subSubIndex)
            +  ",\"VarBitIndex\":" + to_string(bitIndex) + "}";
    inputTxt += "}]}";

    Json::Reader parser;
    Json::Value readValue;
    parser.parse(inputTxt, readValue, true);

    sendJsonRequest.params = readValue;

    ProtoRpc::ClientCommand& command = sendJsonRequest;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    block = true;
    setEnable(false, "Creating Variable Subscription.\nPlease wait..!");
    executeClientCommand(&sendJsonRequest);

    ui->stopSub->setEnabled(false);
    ui->startSub->setEnabled(true);
    ui->deleteSub->setEnabled(true);
    ui->close_2->setEnabled(false);
}

void Dialog::on_deleteSub_clicked()
{
    ui->createSub->setEnabled(true);

    SendJsonRequest sendJsonRequest;
    sendJsonRequest.command = "ProcessSubscription";

    string inputTxt = "{\"ItemAction\":\"delete\",\"ItemSubscriptionID\":" + to_string(subSubscriptionID) + "}";

    Json::Reader parser;
    Json::Value readValue;
    parser.parse(inputTxt, readValue, true);

    sendJsonRequest.params = readValue;

    ProtoRpc::ClientCommand& command = sendJsonRequest;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    block = true;
    setEnable(false, "Deleting Variable Subscription.\nPlease wait..!");
    executeClientCommand(&sendJsonRequest);

    ui->stopSub->setEnabled(false);
    ui->startSub->setEnabled(false);
    ui->deleteSub->setEnabled(false);
    ui->close_2->setEnabled(true);
}

void Dialog::on_startSub_clicked()
{
    ui->startSub->setEnabled(false);

    SendJsonRequest sendJsonRequest;
    sendJsonRequest.command = "ProcessSubscription";

    string inputTxt = "{\"ItemAction\":\"start\",\"ItemSubscriptionID\":" + to_string(subSubscriptionID) + "}";

    Json::Reader parser;
    Json::Value readValue;
    parser.parse(inputTxt, readValue, true);

    sendJsonRequest.params = readValue;

    ProtoRpc::ClientCommand& command = sendJsonRequest;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    block = true;
    setEnable(false, "Starting Variable Subscription.\nPlease wait..!");
    executeClientCommand(&sendJsonRequest);

    ui->createSub->setEnabled(false);
    ui->stopSub->setEnabled(true);
    ui->deleteSub->setEnabled(false);
}

void Dialog::on_stopSub_clicked()
{
    ui->stopSub->setEnabled(false);

    SendJsonRequest sendJsonRequest;
    sendJsonRequest.command = "ProcessSubscription";

    string inputTxt = "{\"ItemAction\":\"stop\",\"ItemSubscriptionID\":" + to_string(subSubscriptionID) + "}";

    Json::Reader parser;
    Json::Value readValue;
    parser.parse(inputTxt, readValue, true);

    sendJsonRequest.params = readValue;

    ProtoRpc::ClientCommand& command = sendJsonRequest;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());

    block = true;
    setEnable(false, "Stopping Variable Subscription.\nPlease wait..!");
    executeClientCommand(&sendJsonRequest);

    ui->createSub->setEnabled(false);
    ui->startSub->setEnabled(true);
    ui->deleteSub->setEnabled(true);
}

void Dialog::updateVariableValue(Json::Value result)
{
    for(auto &itemNode : result["ItemList"])
    {
        if (itemNode.isObject())
        {
            unsigned long itemID = itemNode["ItemID"].asLargestInt();
            if (itemID == subItemID)
            {
                ui->dateTimeEdit_2->setVisible(false);
                if (subItemType.compare("ENUMERATION") == 0)
                {
                    if (currentVarType.compare("BITENUMERATION_ITEM") == 0)
                    {
                        bool varEnumVal = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asBool();
                        if (varEnumVal)
                        {
                            ui->checkBoxOnOff->setChecked(true);
                        }
                        else
                        {
                            ui->checkBoxOnOff->setChecked(false);
                        }
                    }
                    else if (currentVarType.compare("BITENUMERATION") == 0)
                    {
                        int varEnumVal = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
                        ui->checkBox_1->setChecked(false);
                        ui->checkBox_2->setChecked(false);
                        ui->checkBox_3->setChecked(false);
                        ui->checkBox_4->setChecked(false);
                        ui->checkBox_5->setChecked(false);
                        ui->checkBox_6->setChecked(false);
                        ui->checkBox_7->setChecked(false);
                        ui->checkBox_8->setChecked(false);

                        string temp = "";
                        int bin = 0;
                        int num = varEnumVal;
                        while (num > 0)
                        {
                            bin = num % 2;
                            temp += to_string(bin);
                            num /= 2;
                        }
                        for(unsigned int x = 0; x != temp.length(); x++)
                        {
                            if (temp[x] == '1')
                            {
                                if (x == 0)
                                {
                                    ui->checkBox_1->setChecked(true);
                                }
                                else if (x == 1)
                                {
                                    ui->checkBox_2->setChecked(true);
                                }
                                else if (x == 2)
                                {
                                    ui->checkBox_3->setChecked(true);
                                }
                                else if (x == 3)
                                {
                                    ui->checkBox_4->setChecked(true);
                                }
                                else if (x == 4)
                                {
                                    ui->checkBox_5->setChecked(true);
                                }
                                else if (x == 5)
                                {
                                    ui->checkBox_6->setChecked(true);
                                }
                                else if (x == 6)
                                {
                                    ui->checkBox_7->setChecked(true);
                                }
                                else if (x == 7)
                                {
                                    ui->checkBox_8->setChecked(true);
                                }
                            }
                        }
                    }
                    else
                    {
                        int varEnumVal = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
                        map<QString, int>::const_iterator it;
                        int index = 0;
                        for (it = paramMap.begin(); it != paramMap.end(); ++it)
                        {
                            if (it->second == varEnumVal)
                            {
                                for (int ind = 0; ind < ui->HARTItemValcmb->count(); ind++)
                                {
                                    if (ui->HARTItemValcmb->itemText(ind) == it->first)
                                    {
                                        ui->HARTItemValcmb->setCurrentIndex(ind);
                                        break;
                                    }
                                }
                                break;
                            }
                            index++;
                        }
                    }
                }
                else if (subItemType.compare("DATE_TIME") == 0)
                {
                    ui->dateTimeEdit_2->setVisible(false);
                    string varUnit = itemNode["result"]["ItemInfo"]["VarInfo"]["VarUnit"].asString();
                    ui->unit->setText(varUnit.c_str());
                    if (currentVarType.compare("TIME_VALUE_EPOCH") == 0)
                    {
                        unsigned long Epoch_time = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestUInt();
                        string varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");

                        QDate date;
                        QTime time;
                        date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
                        time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

                        QDate minDate;
                        minDate.setDate(1972, 01, 01);
                        ui->dateTimeEdit_2->setMinimumDate(minDate);
                        ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                        ui->dateTimeEdit_2->clear();
                        ui->dateTimeEdit_2->setDate(date);
                        ui->dateTimeEdit_2->setTime(time);

                        ui->HARTItemValTxt->setVisible(false);
                        ui->dateTimeEdit_2->setVisible(true);
                        ui->dateTimeEdit_2->setEnabled(true);
                    }
                    else if (currentVarType.compare("TIME_VALUE_DIFF") == 0)
                    {
                        unsigned long time_diff_ms = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestUInt();
                        time_diff_ms /= 32000;
                        string time_format = itemNode["ItemInfo"]["VarInfo"]["VarValDisplayFormat"].asString();
                        if (time_format.empty())
                        {
                            time_format = "%H:%M:%S";
                        }
                        string varValue = longToDateAndTime(time_diff_ms, time_format.c_str());

                        QTime time;
                        time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

                        ui->dateTimeEdit_2->setDisplayFormat("hh:mm:ss");
                        ui->dateTimeEdit_2->setTime(time);

                        ui->HARTItemValTxt->setVisible(false);
                        ui->dateTimeEdit_2->setVisible(true);
                        ui->dateTimeEdit_2->setEnabled(true);
                    }
                    else if (currentVarType.compare("TIME_VALUE_SCALED") == 0)
                    {
                        unsigned long time_scale_ms = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestUInt();
                        unsigned long time_val_sec = time_scale_ms/32000;
                        string time_format = "%H:%M:%S";
                        string varValue = longToDateAndTime(time_val_sec, time_format.c_str());

                        QTime time;
                        time.setHMS(stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)), stoi(varValue.substr(6, 8)));

                        ui->dateTimeEdit_2->setDisplayFormat("hh:mm:ss");
                        ui->dateTimeEdit_2->setTime(time);

                        ui->HARTItemValTxt->setVisible(false);
                        ui->dateTimeEdit_2->setVisible(true);
                        ui->dateTimeEdit_2->setEnabled(true);
                    }
                    else if (currentVarType.compare("DURATION") == 0)
                    {
                        string result = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                        ui->HARTItemValTxt->setText(result.c_str());
                    }
                    else
                    {
                        long Epoch_time = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asLargestInt();
                        string varValue = longToDateAndTime(Epoch_time, "%m/%d/%Y %H:%M:%S");
                        QDate date;
                        QTime time;
                        date.setDate(stoi(varValue.substr(6, 10)), stoi(varValue.substr(0, 2)), stoi(varValue.substr(3, 5)));
                        time.setHMS(stoi(varValue.substr(11, 13)), stoi(varValue.substr(14, 16)), stoi(varValue.substr(17, 19)));

                        if (currentVarType.compare("DATE") == 0)
                        {
                            QDate minDate;
                            minDate.setDate(1900, 01, 01);
                            ui->dateTimeEdit_2->setMinimumDate(minDate);
                            ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy");
                        }
                        else if (currentVarType.compare("TIME") == 0)
                        {
                            QDate minDate;
                            minDate.setDate(1984, 01, 01);
                            ui->dateTimeEdit_2->setMinimumDate(minDate);
                            ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                        }
                        else if (currentVarType.compare("DATE_AND_TIME") == 0)
                        {
                            QDate minDate;
                            minDate.setDate(1900, 01, 01);
                            ui->dateTimeEdit_2->setMinimumDate(minDate);
                            ui->dateTimeEdit_2->setDisplayFormat("MM/dd/yyyy hh:mm:ss");
                        }
                        else
                        {

                        }
                        ui->dateTimeEdit_2->clear();
                        ui->dateTimeEdit_2->setDate(date);
                        ui->dateTimeEdit_2->setTime(time);

                        ui->HARTItemValTxt->setVisible(false);
                        ui->dateTimeEdit_2->setVisible(true);
                        ui->dateTimeEdit_2->setEnabled(true);
                    }
                }
                else
                {
                    string varValue = itemNode["ItemInfo"]["VarInfo"]["VarValue"].asString();
                    ui->HARTItemValTxt->setText(QString::fromStdString(varValue));
                }
            }
        }
    }
}

void Dialog::displayMenuPath()
{
    map<int, QString>::const_iterator it;
    QString path = "/";
    for (it = pathMap.begin(); it != pathMap.end(); ++it)
    {
        QString str = path + it->second;
        path = str.trimmed() + "/";
    }
    path = path.trimmed();
    ui->path->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->path_2->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->path_3->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->path->setText(path);
    ui->path_2->setText(path);
    ui->path_3->setText(path);
}

//on Profibus bus param config button click
void Dialog::on_bus_config_btn_3_clicked()
{
    int32_t retryLimit;
    uint32_t minTsdr;
    uint32_t maxTsdr;
    uint32_t slotTime;
    int32_t quietTime;
    uint32_t setUpTime;
    uint32_t hsa;
    uint32_t guf;
    uint32_t ttr;

    try
    {
        retryLimit = stoul((ui->MaxRetryLimit->toPlainText().toStdString()));
        minTsdr = stoul((ui->MinTsdr->toPlainText().toStdString()));
        maxTsdr = stoul((ui->MaxTsdr->toPlainText().toStdString()));
        slotTime = stoul((ui->SlotTime->toPlainText().toStdString()));
        quietTime = stoul((ui->QuietTime->toPlainText().toStdString()));
        setUpTime = stoul((ui->SetupTime->toPlainText().toStdString()));
        hsa = stoul((ui->HSA->toPlainText().toStdString()));
        guf = stoul((ui->GapUpdateFactor->toPlainText().toStdString()));
        ttr = stoul((ui->TargetRotationTime->toPlainText().toStdString()));
    }
    catch(exception ex)
    {
        QMessageBox msgBox;
        msgBox.setText("Invalid Value");
        msgBox.exec();
        return;
    }

    bool isValid = true;

    /*if(retryLimit < 0 || retryLimit > 7)
    {
        isValid = false;
        inValidRange("retryLimit");
    }

    if(minTsdr < 11 || minTsdr > 1023)
    {
        isValid = false;
        inValidRange("minTsdr");
    }

    if(maxTsdr < 35 || maxTsdr > 65535)
    {
        isValid = false;
        inValidRange("maxTsdr");
    }

    if(slotTime < 37 || slotTime > 16383)
    {
        isValid = false;
        inValidRange("slotTime");
    }

    if(quietTime < 0 || quietTime > 493)
    {
        isValid = false;
        inValidRange("quietTime");
    }

    if(setUpTime < 1 || setUpTime > 494)
    {
        isValid = false;
        inValidRange("setUpTime");
    }

    if(hsa < 1 || hsa > 126)
    {
        isValid = false;
        inValidRange("Host station address");
    }

    if(guf < 1 || guf > 100)
    {
        isValid = false;
        inValidRange("Gap Update Factor");
    }

    if(ttr < 256 || ttr > 16776960)
    {
        isValid = false;
        inValidRange("Target rotation time");
    }

    if(addr < 0 || addr > 125)
    {
        isValid = false;
        inValidRange("master address");
    }*/

    if(isValid)
    {
        SetClntBusParams setBusParams;
        setBusParams.protocolType = 2;
        setBusParams.setDefaultType = 0;
        setBusParams.guf = guf;
        setBusParams.hsa = hsa;
        setBusParams.ttr = ttr;
        setBusParams.setUpTime = setUpTime;
        setBusParams.quietTime = quietTime;
        setBusParams.slotTime = slotTime;
        setBusParams.maxTsdr = maxTsdr;
        setBusParams.minTsdr = minTsdr;
        setBusParams.retryLimit = retryLimit;
        ProtoRpc::ClientCommand& command1 = setBusParams;
        ProtoRpc::Request request1 = command1.buildRequest();
        string requestString1 = "\nRequest: \n" + request1.debugString();
        print_log(requestString1.c_str());


        auto settingsClntCmdResp = setBusParams.execute(cpmClient);

        string response1 = settingsClntCmdResp.toJsonString();
        Json::Reader parser;
        Json::Value readResult;
        parser.parse(response1, readResult, true);
        string st = "\nResponse: \n" + response1;
        QString s = QString::fromUtf8(st.c_str());
        print_log(s);

    /*    if(readResult["result"].asString() == "ok")
        {
            tsl = setBusParams.slotTime;
            minTsdr = setBusParams.minTsdr;
            maxTsdr = setBusParams.maxTsdr;
            qTime = setBusParams.quietTime;
            sTime = setBusParams.setUpTime;
            ttr = setBusParams.ttr;
            g = setBusParams.guf;
            hsa = setBusParams.hsa;
            maxrRetLimit = setBusParams.retryLimit;
        } */
    }
}

//gives invalid range info for pb bus params
void Dialog::inValidRange(const QString str)
{
    QMessageBox msgBox;
    QString str1 = str + "\tentered value is invalid";
    msgBox.setText(str1);
    msgBox.exec();
}

void Dialog::on_bus_config_btn_4_clicked()
{
    SetClntBusParams setBusParams;
    setBusParams.protocolType = 2;
    setBusParams.setDefaultType = 1;
   /* setBusParams.guf = 1;
    setBusParams.hsa = 126;
    setBusParams.ttr = 60000;
    setBusParams.setUpTime = 1;
    setBusParams.quietTime = 0;
    setBusParams.slotTime = 600;
    setBusParams.maxTsdr = 60;
    setBusParams.minTsdr = 11;
    setBusParams.retryLimit = 3;*/
    ProtoRpc::ClientCommand& command1 = setBusParams;
    ProtoRpc::Request request1 = command1.buildRequest();
    string requestString1 = "\nRequest: \n" + request1.debugString();
    print_log(requestString1.c_str());

    auto settingsClntCmdResp = setBusParams.execute(cpmClient);

    string response1 = settingsClntCmdResp.toJsonString();
    Json::Reader parser;
    Json::Value readResult;
    parser.parse(response1, readResult, true);
    string st = "\nResponse: \n" + response1;
    QString s = QString::fromUtf8(st.c_str());
    print_log(s);

 /*   if (readResult["result"].asString() == "ok")
    {
        ui->MaxRetryLimit->setText("3");
        ui->MinTsdr->setText("11");
        ui->MaxTsdr->setText("60");
        ui->SlotTime->setText("600");
        ui->QuietTime->setText("0");
        ui->SetupTime->setText("1");
        ui->HSA->setText("126");
        ui->GapUpdateFactor->setText("1");
        ui->TargetRotationTime->setText("60000");
    }*/


}

void Dialog::on_bus_config_btn_5_clicked()
{

    SetClntAppSettings setAppSettings;
     setAppSettings.protocolType = 2;
    setAppSettings.setDefaultType = 1;
    ProtoRpc::ClientCommand& command = setAppSettings;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());


    auto settingsClntCmdResp1 = setAppSettings.execute(cpmClient);

    string response2 = settingsClntCmdResp1.toJsonString();
    Json::Reader parser1;
    Json::Value readResult1;
    parser1.parse(response2, readResult1, true);
    string st1 = "\nResponse: \n" + response2;
    QString s1 = QString::fromUtf8(st1.c_str());
    print_log(s1);

    /*if(readResult1["result"].asString() == "ok")
    {
        ui->Address->setText("0");
    }*/
}

void Dialog::on_bus_config_btn_6_clicked()
{
    int32_t addr;
    try
    {
        addr = stoul((ui->Address->toPlainText().toStdString()));
    }
    catch(exception ex)
    {
        QMessageBox msgBox;
        msgBox.setText("Invalid Value");
        msgBox.exec();
        return;
    }

    SetClntAppSettings setAppSettings;
    setAppSettings.protocolType = 2;
    setAppSettings.address = addr;
    setAppSettings.setDefaultType = 0;
    ProtoRpc::ClientCommand& command = setAppSettings;
    ProtoRpc::Request request = command.buildRequest();
    string requestString = "\nRequest: \n" + request.debugString();
    print_log(requestString.c_str());


    auto settingsClntCmdResp1 = setAppSettings.execute(cpmClient);

    string response2 = settingsClntCmdResp1.toJsonString();
    Json::Reader parser1;
    Json::Value readResult1;
    parser1.parse(response2, readResult1, true);
    string st1 = "\nResponse: \n" + response2;
    QString s1 = QString::fromUtf8(st1.c_str());
    print_log(s1);

   /* if(readResult1["result"].asString() == "ok")
    {
        locAddress = setAppSettings.address;
    }*/
}
