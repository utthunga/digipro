/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Origin:            Starsky
*/

/** @file
    DBus client application
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#pragma once

#include <string>
#include "ProtoRpc/ClientCommand.h"
#include "error/SharedErrors.h"
#include "ProtoDBus/Client.h"
#include "ProtoRpc/Response.h"
#include "ProtoRpc/Error.h"


using namespace std;
using namespace ProtoRpc;

class DBusClient
{
public:
    DBusClient();
    virtual ~DBusClient();
    bool tryConnect(std::string serviceName, std::string objectPath);
     ProtoRpc::JsonResponse sendToServer(ProtoRpc::JsonClientCommand& command, int timeout);
private:        
    std::string m_serviceName;
    std::string m_objectPath;
    std::string dbusServerSlotName() const;
    ProtoDBus::Client m_dbusClient;
};
