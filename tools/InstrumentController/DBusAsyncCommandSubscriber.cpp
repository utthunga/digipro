/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Subscriber class definition
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/
/* INCLUDE FILES **************************************************************/
#include "DBusAsyncCommandSubscriber.h"
#include <flk_log4cplus/Log4cplusAdapter.h>

/* ========================================================================== */
/* Constants and Macros *******************************************************/



/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs DBusMethodExecutionSubscriber Class Instance
*/
DBusAsyncCommandSubscriber::DBusAsyncCommandSubscriber(Dialog* dialog)
{
    ui = dialog;
}

/*  ----------------------------------------------------------------------------
    Destructs DBusMethodExecutionSubscriber Class Instance
*/
DBusAsyncCommandSubscriber::~DBusAsyncCommandSubscriber()
{
}

/*  ----------------------------------------------------------------------------
    Get the notify message fromt he publisher and do the requireed things
*/
void DBusAsyncCommandSubscriber::notify(Json::Value message)
{
    string commandName = message["CommandName"].asString();

    std::string result = "\nResponse: \n" + message.toStyledString();
    ui->print_log(result.c_str());

    // Get the command procecure from the comamnd proc map table
    auto commandProc = ui->m_commandProcMapTable.find(commandName);
    if (commandProc != ui->m_commandProcMapTable.end())
    {
        (ui->*(commandProc->second))(message["CommandResponse"]);
    }
    LOGF_INFO(CPM_DBUS_CLIENT_APP, "Message from publisher: " << message);
}
