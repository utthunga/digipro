#include "dialog.h"
#include <QApplication>
#include <QString>
#include <vector>
#include <sstream>

// This shall be enabled only when UI application needs to be debugged
//#define DEBUG_REMOTE_IC

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    bool remoteIC = false;

    vector <string> envVarName = {"ipAddr", "portNum", "userName", "pwd"};
    std::map  <string, string> envVapMap;

#ifdef DEBUG_REMOTE_IC
    // Modify the IP address, port number, username and password of remote device
    // (Ex : BeagleBone or Starsky Hardware)
    qputenv("IC_ENV_VAR","ipaddr=172.16.1.2,port=55554,uname=debian,pwd=temppwd");
#endif

    string envVar = (qgetenv("IC_ENV_VAR")).toStdString();
    if (!envVar.empty())
    {
        vector <string> envVec;


        std::stringstream ss(envVar);

        // Read Environment variable which is comma seperated
        while(ss.good())
        {
            string substr;
            getline(ss, substr, ',');
            envVec.push_back(substr);
        }

        for (unsigned int i=0; i< envVec.size(); ++i)
        {
            // Extract the values of Environment variables
            string tempVar = envVec.at(i);

            string envVar = tempVar.substr(tempVar.find("=")+1);

            envVapMap.emplace ( std::pair<string,string>(envVarName.at(i),envVar) );
        }

        string dbusEnvVar;
        dbusEnvVar += "tcp:host=";
        dbusEnvVar += envVapMap.at(envVarName.at(0));
        dbusEnvVar += ",port=";
        dbusEnvVar += envVapMap.at(envVarName.at(1));
        dbusEnvVar += ",family=ipv4";

        // Set DBUS_SESSION_BUS_ADDRESS environment variable
        bool status = qputenv("DBUS_SESSION_BUS_ADDRESS", dbusEnvVar.c_str());

        if (status)
        {
            remoteIC = true;
        }
    }



    Dialog w;
    w.remoteIC = remoteIC;
    if (remoteIC)
    {

        try
        {
            w.scp.hostAddr = envVapMap.at(envVarName.at(0));
        }
        catch(...)
        {
            cout << "Host Address missing in ENV variable"<<endl;
            w.scp.hostAddr = "";
        }
        try
        {
            w.scp.userName = envVapMap.at(envVarName.at(2));
        }
        catch(...)
        {
            cout << "User Name missing in ENV variable"<<endl;
            w.scp.userName = "";
        }
        try
        {
            w.scp.pwd = envVapMap.at(envVarName.at(3));
        }
        catch(...)
        {
            cout << "Password missing in ENV variable"<<endl;
            w.scp.pwd = "";
        }
        //Initialize SSH2
        int initRet = w.scp.initSSH2();
        if (0 != initRet)
        {
            w.scp.shutDownSSH2();
            cout << "Unable to initialize SSH2 :" << initRet;
        }
    }


    w.show();

    return a.exec();
}
