/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Client.h"
#include "ProtoRpc/Response.h"
#include "ProtoRpc/Error.h"
#include "PubSub/Subscriber.h"
#include "PubSub/PublisherEndpoint.h"

/* ========================================================================== */

/* Macros definition ***************************************************1*******/
#define CPM_CMD_OBJ_PATH    "/Cpmcommand"
#define CPM_DBUS_CLIENT_URI "com.fluke.cpm.client"
#define CPM_DBUS_CLIENT_APP cpm-dbus-client

static ProtoDBus::Endpoint g_CpmCommandServer(DBus::DBUS_CPM_URI, "Cpmcommand");
static pubsub::PublisherEndpoint g_CpmScanDataPublisher(DBus::DBUS_CPM_URI, "SCAN_PUBLISHER");
static pubsub::PublisherEndpoint g_CpmCmndRespDataPublisher(DBus::DBUS_CPM_URI, "CMD_RESP_PUBLISHER");
static pubsub::PublisherEndpoint g_CpmItemDataPublisher(DBus::DBUS_CPM_URI, "ITEM_PUBLISHER");


/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusSubscriber 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusSubscriber : public pubsub::Subscriber
{
public:
    /* Public Member Functions ===============================================*/
    
    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    DBusSubscriber();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~DBusSubscriber();

    /*------------------------------------------------------------------------*/
    /** Initializes DBusScanSubscriber with the below scope

       @return Error string
    */
    virtual int initialise();

    /*------------------------------------------------------------------------*/
    /** Shutdown DBusCommandHandler
    */
    virtual int shutDown();

    /*------------------------------------------------------------------------*/
    /** Subscribe to the described client and it handles only the scan
        information

        @param  subscriberName,subscribe to the provided subscriber name
        @return status 
    */
    virtual int subscribeClient(std::string& subscriberName);

private:
    /// DBus Client
    ProtoDBus::Client* m_pDbusClient;
};
