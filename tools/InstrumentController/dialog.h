#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTableWidgetItem>
#include <QRadioButton>
#include <iostream>
#include <map>
#include <json/json.h>
#include "DBusClient.h"
#include "scp/ScpWrapper.h"

using namespace std;

namespace Ui {
class Dialog;
}


#define DDS_UNUSED 		    0
#define DDS_INTEGER 		2
#define DDS_UNSIGNED 		3
#define DDS_FLOAT 			4
#define DDS_DOUBLE 			5
#define DDS_ENUMERATED 		6
#define DDS_BIT_ENUMERATED 	7
#define DDS_INDEX 			8
#define DDS_ASCII 			9
#define DDS_PACKED_ASCII 	10
#define DDS_PASSWORD 		11
#define DDS_BITSTRING 		12
#define DDS_DATE 			13
#define DDS_TIME 			14
#define DDS_DATE_AND_TIME 	15
#define DDS_DURATION 		16
#define DDS_EUC				17
#define DDS_OCTETSTRING		18
#define DDS_VISIBLESTRING	19
#define DDS_TIME_VALUE		20
#define DDS_BOOLEAN_T		21
#define DDS_MAX_TYPE		22
#define DDS_ACK_TYPE        (DDS_MAX_TYPE + 1)


/// Advanced Block Configuration
#define MODE_BLOCK_ID                  0x80020126
#define TARGET_MODE_ID                 0x8002001F
#define ACTUAL_MODE_ID                 0x80020020
#define PERMITTED_MODE_ID              0x80020195
#define NORMAL_MODE_ID                 0x80020021
#define CHANNEL_ID                     0x203C3
#define XD_SCALE_ID                    0x80020192
#define UNITS_INDEX_ID                 0x8002001D
#define DECIMAL_ID                     0x8002001E
#define OUT_SCALE_ID                   0x80020132
#define BLOCK_TAG_ID                   0x80020001
#define XD_EU_100_ID                   0x80020001
#define XD_EU_100_ID                   0x80020001
#define EU_100_ID_STD               0x8002001B
#define XD_EU_0_ID                     0x100053D
#define EU_0_ID_STD                 0x8002001C
#define OUT_EU_100_ID                  0x100053E
#define OUT_EU_0_ID                  0x100053F
#define XD_SCALE_UNITS_ID              0x100001A
#define SCALE_UNITS_ID_STD          0x8002001D
#define OUT_SCALE_UNITS_ID             0x1000540
#define L_TYPE_ID                      0x8002010C

#define EC_COMMAND_IN_PROGRESS      90001

enum ProtocolType
{
    FF = 0,
    HART,
    PB
};

enum CUSTOM_ID
{
    // Do NOT change the order or values of the items and the starting element MUST be 0
    // Just append new elements at the end

    CUSTOM_ID_DEVICE_MENU = 0,      /** Top Level Device Menu */
    CUSTOM_ID_DEVICE_MENU_ADVANCED, /** Advanced option in the top level Device Menu */
    CUSTOM_ID_BLOCK_MENU,           /** Block Level Top Menu for any block */
    CUSTOM_ID_BLOCK_MENU_ADVANCED,  /** Advanced option in Top Level Block Menu */
    CUSTOM_ID_BLOCK_PARAM_LIST,     /** Block Parameter List */
    CUSTOM_ID_BLOCK_VIEWS,          /** Block Views */
    CUSTOM_ID_BLOCK_METHODS,        /** Block Methods */
    CUSTOM_ID_BLOCK_CONFIG,         /** Advanced Block Configuration */
    CUSTOM_ID_BLOCK_VIEW1,          /** Block view 1 */
    CUSTOM_ID_BLOCK_VIEW2,          /** Block view 2 */
    CUSTOM_ID_BLOCK_VIEW3,          /** Block view 3 */
    CUSTOM_ID_BLOCK_VIEW4,          /** Block view 4 */
    CUSTOM_ID_BLOCK_ALARAM,          /** Block view 3 */
    CUSTOM_ID_BLOCK_STATUS,          /** Block view 4 */

    // Add New Custom Menus above this line. The below Item should be the last element
    CUSTOM_ID_MENU_COUNT			/** This is just for count. DO NOT DELETE / MODIFY */
};


typedef struct Item_Info
{
    unsigned long itemID = 0;
    unsigned long itemContainerID = 0;
    string itemContainerTag = "";
    string itemLabel = "";
    string itemHelp = "";
    string itemType = "";
    bool itemInDD = false;
    bool itemReadOnly = false;
    bool itemHasRelation = false;
    string name = "";
    int subindex = 0;
    int size = 0;
    int dataType = 0;
    unsigned int varContainerID = 0;
    unsigned int varSubIndex = 0;
    unsigned long response_code = 0;
    unsigned int bitIndex = 0;
}ItemInfo;

class Dialog : public QDialog
{
    Q_OBJECT
private:
    bool isInitialized = false;
    bool block = false;
    int protocolType = false;
    int CommissionType = false;

    int isEnum = 0;
    bool userAbort = false;
    bool methodExecutedFromMenu = false;
    unsigned long itemID = 0;
    string currentVarType = "";

    uint32_t tsl;
    uint32_t minTsdr;
    uint32_t maxTsdr;
    uint32_t qTime;
    uint32_t sTime;
    uint32_t ttr;
    uint32_t g;
    uint32_t hsa;
    uint32_t maxrRetLimit;
    uint32_t locAddress;

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    QString subscriptionID;

    map<int, QString> modeMap;
    map<int, int> channelMap;
    map<QString, int> enumMap;
    map<QString, int> paramMap;
    map<int, int> unitsMap;
    map<int, int> OutunitsMap;
    map<QString, int> methodIDMap;
    map<string, ItemInfo*> paramList;  // Parameter List
    map<QString, string> enumVarMap;
    /// Function pointer for process item methods
    typedef void (Dialog::*commandProc)(const Json::Value&);
    map<string, commandProc> m_commandProcMapTable;

    bool remoteIC;

    ScpWrapper scp;


    static void SendCommand();
    void print_log(const QString str);
    void handleScannedResult(Json::Value result);
    void handleItemResult(Json::Value result);
    void handleCommissionResult(Json::Value result);
    void handleSetItemResult(Json::Value result);
    void InitializeAsyncCommands();

    void handleSetProtocolTypeResult(const Json::Value& result);
    void handleInitializeResult(const Json::Value& result);
    void handleStartScanResult(const Json::Value& result);
    void handleStopScanResult(const Json::Value& result);
    void handleConnectResult(const Json::Value& result);
    void handleDisconnectResult(const Json::Value& response);
    void handleGetItemResult(const Json::Value& result);
    void handleSetItemResult(const Json::Value& result);
    void handleGetBusParametersResult(const Json::Value& result);
    void handleSetBusParametersResult(const Json::Value& result);
    void handleGetFieldDeviceConfigResult(const Json::Value& result);
    void handleSetFieldDeviceConfigResult(const Json::Value& result);
    void handleGetAppSettingsResult(const Json::Value& result);
    void handleSetAppSettingsResult(const Json::Value& result);
    void handleProcessSubscriptionResult(const Json::Value& result);
    void handleMenuResult(Json::Value readResult);
    void handleVariableResult(Json::Value readResult);
    void handleImageResult(Json::Value readResult);
    void handleShutdownResult(const Json::Value& response);
    void handleSetLangCodeResult(const Json::Value& result);
    void handleExecPreEditAction(const Json::Value& result);
    void handleExecPostEditAction(const Json::Value& result);

    void executeClientCommand(ProtoRpc::JsonClientCommand* pClientCommand);

private slots:

    int refresh();

    void SetProtocolType(ProtocolType pType);

    void StartScan();

    int getStaticMenuID(int index);

    void Connect();

    void Disconnect();

    void menuNavigation(Json::Value result);

    void on_close_clicked();

    void on_clearLogs_clicked();

    void on_FFButton_clicked();

    void setEnable(bool enable, QString text);

    void Show(int frame, bool enable);

    void on_HartButton_clicked();

    void on_ProfibusButton_clicked();

    void on_connectBtn_clicked();

    void on_startscanBtn_clicked();

    void on_stopscan_clicked();

    void on_resultTable_itemSelectionChanged();

    void on_disconnect_clicked();

    void on_pushButton_7_clicked();

    void on_Adv_Dev_Setup_clicked();

    void on_decommission_btn_clicked();

    void on_device_class_write_btn_clicked();

    void executeMethod(Json::Value result);

    void updateVariableValue(Json::Value result);

    void on_MEfirst_clicked();

    void on_MEsecond_clicked();

    void on_block_channel_back_clicked();

    void getMenu();

    void on_GetMenu_clicked();

    void on_tableWidget_2_cellClicked(int row, int column);

    void on_getMenuButton_2_clicked();

    void ReadValue(unsigned long itemID, string tag);

    void on_close_2_clicked();

    void on_bus_config_btn_2_clicked();

    void executeMethodFromMenu(unsigned long itemID, string tag);

    void ConfigureSettings (bool reset);

    void on_HARTSendBtn_clicked();

    void on_settings_reset_btn_clicked();

    void on_send_json_req_btn_clicked();

    void on_MEthird_clicked();

    void on_MEHelpClose_clicked();

    void on_setupBtn_clicked();

    void on_busparam_clicked();

    void on_createSub_clicked();

    void on_deleteSub_clicked();

    void on_startSub_clicked();

    void on_stopSub_clicked();

    void triggerLogLevel(int logLevel);

    void displayMenuPath();
	
	void on_initBtn_clicked();

    void setLangCode();

    void setLanUICntrlState(bool state);

    void on_bus_config_btn_3_clicked();

    void on_bus_config_btn_4_clicked();

    void on_bus_config_btn_5_clicked();

    void on_bus_config_btn_6_clicked();

private:
    Ui::Dialog *ui;

    bool isNumeric(string s);

    void inValidRange(const QString str);

    time_t dateAndTimeToLong(const char* theTime, const char* format);

    string longToDateAndTime(time_t epochTime, const char* format);

    unsigned short getVarValType(string val);

    map<unsigned int , bool> qlfrObj;

    bool isSettingRequest ;
    bool isBusParamRequest;
};

#endif // DIALOG_H
