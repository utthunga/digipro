#-------------------------------------------------
#
# Project created by QtCreator 2017-05-23T12:28:44
#
#-------------------------------------------------

QT += core gui dbus

CONFIG(release, debug|release) {
  BUILD_TYPE="release"
  BUILD_TYPE_SHORT="rel"
}
CONFIG(debug, debug|release) {
  BUILD_TYPE="debug"
  BUILD_TYPE_SHORT="deb"
}

# Consider building ProStar UI application only for x86 64 bit platform.
# building for 32 bit ARM is not necessary as CPM shall be called through DBus over Ethernet
BUILD_PLATFORM="x86"



unix: QMAKE_LFLAGS += $${QMAKE_LFLAGS_RPATH}$$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib/
unix: QMAKE_LFLAGS += $${QMAKE_LFLAGS_RPATH}$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = InstrumentController
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        dialog.cpp \
    DBusScanSubscriber.cpp \
    DBusSubscriber.cpp \
    commands/SetProtocolTypeClntCmnd.cpp \
    commands/InitializeClntCmnd.cpp \
    commands/StartScanClntCmnd.cpp \
    commands/ConnectClntCmnd.cpp \
    commands/DisconnectClntCmnd.cpp \
    commands/StopScanClntCmnd.cpp \
    commands/ShutdownClntCmnd.cpp \
    DBusCommission.cpp \
    DBusClient.cpp \
    commands/GetItemClntCmnd.cpp \
    commands/SetItemClntCmnd.cpp \
    commands/SetClntFieldDeviceConfig.cpp \
    commands/GetClntFieldDeviceConfig.cpp \
    commands/ProcessSubscriptionClntCmnd.cpp \
    commands/SetClntAppSettings.cpp \
    commands/SetClntBusParams.cpp \
    commands/GetClntBusParams.cpp \
    commands/GetClntAppSettings.cpp \
    commands/SendJsonRequest.cpp \
    DBusAsyncCommandSubscriber.cpp \
    DBusItemSubscriber.cpp \
    commands/SetLogLevelClntCmnd.cpp \
    commands/SetLangCodeCmnd.cpp \
    commands/ExecPreEditActionClntCmd.cpp \
    commands/ExecPostEditActionClntCmd.cpp \
    scp/scpwrapper.cpp

HEADERS += dialog.h \
    DBusScanSubscriber.h \
    DBusSubscriber.h \
    commands/SetProtocolTypeClntCmnd.h \
    commands/InitializeClntCmnd.h \
    commands/StartScanClntCmnd.h \
    commands/ConnectClntCmnd.h \
    commands/DisconnectClntCmnd.h \
    commands/StopScanClntCmnd.h \
    commands/ShutdownClntCmnd.h \
    DBusItemSubscriber.h \
    DBusClient.h \
    DBusCommission.h \
    commands/GetItemClntCmnd.h \
    commands/SetItemClntCmnd.h \
    commands/SetClntFieldDeviceConfig.h \
    commands/GetClntFieldDeviceConfig.h \
    commands/ProcessSubscriptionClntCmnd.h \
    commands/GetClntBusParams.h \
    commands/GetClntAppSettings.h \
    commands/SetClntAppSettings.h \
    commands/SetClntBusParams.h \
    commands/SendJsonRequest.h \
    DBusAsyncCommandSubscriber.h \
    commands/SetLogLevelClntCmnd.h \
    commands/SetLangCodeCmnd.h \
    commands/ExecPreEditActionClntCmd.h \
    commands/ExecPostEditActionClntCmd.h \
    scp/ScpWrapper.h

FORMS += dialog.ui

INCLUDEPATH += $$PWD/../../dbus/include
INCLUDEPATH += $$PWD/../../commands/include
INCLUDEPATH += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/include
INCLUDEPATH += $$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/include

unix: LIBS += -lssh2
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/ -lapi
unix: PRE_TARGETDEPS += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/libapi.a
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/ -lpubsub
unix: PRE_TARGETDEPS += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/libpubsub.a
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/ -lprotodbus
unix: PRE_TARGETDEPS += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/libprotodbus.a
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_TYPE_SHORT}/lib/ -lprotorpc
unix: PRE_TARGETDEPS += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/libprotorpc.a
unix: LIBS += -L$$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib/ -lprotobuf
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/ -lshared_proto_message
unix: PRE_TARGETDEPS += $$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/libshared_proto_message.a
unix: LIBS += -L$$PWD/../../build/cmake_$${BUILD_PLATFORM}_$${BUILD_TYPE_SHORT}/lib/ -lshared_errors
unix: PRE_TARGETDEPS += $$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib/libjsoncpp.a
unix: QMAKE_LFLAGS += $${QMAKE_LFLAGS_RPATH}$$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib
unix: LIBS += -L$$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib/ -llog4cplus
unix: LIBS += -L$$PWD/../../shared-libs/3rd-party-$${BUILD_PLATFORM}-$${BUILD_TYPE}/lib/ -ljsoncpp
unix: LIBS += -L$$/usr/lib/ -lgcov
