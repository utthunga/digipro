/******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    ScpWrapper class declaration
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef SCPWRAPPER_H
#define SCPWRAPPER_H

/* INCLUDE FILES **************************************************************/

#include <libssh2.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string>
using namespace std;

#define SUCCESS (0)
#define SSH2_INIT_FAILELD   (-1)
#define SSH2_CONNECT_FAILELD (-2)
#define SSH2_SESSION_INIT_FAILELD (-3)
#define FILE_OPEN_FAILED (-4)
#define SSH2_SESSION_AUTH_FAILELD (-5)
#define SCP_COPY_FILE_FAILED (-6)

#define DATA_BUF_SIZE (1024)
/* ===========================================================================*/

/* CLASS DECLARATIONS *********************************************************/
/** class description of ScpWrapper class.

    ScpWrapper Class consists of methods related to copy files from remote server.
*/

class ScpWrapper
{
public:

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs ScpWrapper Class.
    */
    ScpWrapper();

    /*------------------------------------------------------------------------*/
    /** Destroys ScpWrapper Class.
    */
    ~ScpWrapper();

    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Initializes SSH2 Connection and related sessions.
     *
     * @return : error occured during initialization operation
    */
    int initSSH2();

    /*------------------------------------------------------------------------*/
    /** Copies a file from remote source to host machine.
     *
     * @param : srcPath - complete path of file to be copied
     *
     * @param : dstPath - path at host machine to which fiels needs to be copied
     *
     * @return : error occured during remote file copy operation
    */
    int copyFile(const char* srcPath, const char* dstPath);

    /*------------------------------------------------------------------------*/
    /** Destroyes SSH2 Connection and related sessions.
    */
    void shutDownSSH2();

private:
    /* Private Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Initializes SSH2 Session.
     *
     *  @return : error occured during SSH2 session initislization
    */
    int initSSH2Session();

    /*------------------------------------------------------------------------*/
    /** Destroys SSH2 Session.
    */
    void shutDownSSH2Session();


public:

    /* Public Data Members ==================================================*/

    /// Holds IP address of remote machine
    string hostAddr;

    /// Holds user name for remote machine
    string userName;

    /// Holds password of remote machine
    string pwd;


private:
    /* Private Data Members ==================================================*/

    /// Holds socket ID
    int sockID;

    /// Structure holding socket information
    struct sockaddr_in sin;

    /// Holds SSH2 sessiont information
    LIBSSH2_SESSION *session;

    /// Holds SSH2 channel information
    LIBSSH2_CHANNEL *channel;
};

#endif // SCPWRAPPER_H
