/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    ScpWrapper class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <libssh2_publickey.h>
#include <libssh2_sftp.h>
#include "ScpWrapper.h"
#include <unistd.h>
#include <QDebug>

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs ScpWrapper Class
*/
ScpWrapper::ScpWrapper() :
    sockID(0),
    sin{},
    session(nullptr),
    channel(nullptr)
{

}

/*  ----------------------------------------------------------------------------
    Destroyes ScpWrapper Class
*/
ScpWrapper::~ScpWrapper()
{
    sockID = 0;
    session = nullptr;
    channel = nullptr;
}

/*  ----------------------------------------------------------------------------
    Initializes SSH2 Connection and related sessions.
*/
int ScpWrapper::initSSH2()
{
    int retVal = SUCCESS;

    retVal = libssh2_init (0);

    if (retVal != 0)
    {
        qDebug() << "libssh2 initialization failed" << retVal;
        return SSH2_INIT_FAILELD;
    }

    // Create a socket
    sockID = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = inet_addr(hostAddr.c_str());

    // Connect to the Socket
    if (0 != connect(sockID, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)))
    {
        qDebug()<<"failed to connect!";
        return SSH2_CONNECT_FAILELD;
    }

    // Initialize SSH2 session
    retVal = initSSH2Session();
    if (0 != retVal)
    {
        qDebug()<<"failed to initialize session!" << retVal;
        return retVal;
    }

    return retVal;
}


/*  ----------------------------------------------------------------------------
    Destroyes SSH2 Connection and related sessions.
*/
void ScpWrapper::shutDownSSH2()
{
    shutDownSSH2Session();
}


/*  ----------------------------------------------------------------------------
    Initializes SSH2 Session
*/
int ScpWrapper::initSSH2Session()
{
    int retVal = SUCCESS;
    // Create a session instance

    session = libssh2_session_init();

    if(!session)
    {
        return SSH2_SESSION_INIT_FAILELD;
    }

    retVal = libssh2_session_handshake(session, sockID);

    if(0 != retVal)
    {
        qDebug() <<"Failure establishing SSH session: "<< retVal;
        shutDownSSH2Session();
        return SSH2_SESSION_INIT_FAILELD;
    }

    retVal = libssh2_userauth_password(session, userName.c_str(), pwd.c_str());
    if (0 != retVal)
    {
        qDebug()<< "Authentication by password failed." << retVal;
        shutDownSSH2Session();
        retVal = SSH2_SESSION_AUTH_FAILELD;
    }
    return retVal;
}

/*  ----------------------------------------------------------------------------
    Destroys SSH2 Session
*/
void ScpWrapper::shutDownSSH2Session()
{
    libssh2_session_disconnect(session,"");

    libssh2_session_free(session);
}

/*  ----------------------------------------------------------------------------
    Copies a file from remote source to host machine.
*/
int ScpWrapper::copyFile(const char* srcPath, const char* dstPath)
{
    unsigned int dataIndex = 0;
    struct stat fileinfo;

    FILE *fp;
    fp = fopen(dstPath, "wb");
    if (nullptr == fp)
    {
        qDebug()<<"Creating destination file failed";
        return FILE_OPEN_FAILED;
    }

    // Request a file via SCP
    channel = libssh2_scp_recv(session, srcPath, &fileinfo);

    if (!channel)
    {
        qDebug() << "Unable to open a session: " << libssh2_session_last_errno(session);

        shutDownSSH2();
        return SCP_COPY_FILE_FAILED;
    }

    // Copy data from remote file
    while (dataIndex < fileinfo.st_size)
    {
        char data[DATA_BUF_SIZE];
        int readDataLen = DATA_BUF_SIZE;

        if ((fileinfo.st_size - readDataLen) < DATA_BUF_SIZE)
        {
            readDataLen = fileinfo.st_size - dataIndex;
        }

        int rc = libssh2_channel_read(channel, data, readDataLen);

        if (rc > 0)
        {
            // Write to destination binary file
            fwrite(data,readDataLen,1,fp);
        }
        else if (rc < 0)
        {
            qDebug()<< "libssh2_channel_read() failed: " << rc;
            break;
        }
        dataIndex += rc;
    }

    fclose(fp);

    libssh2_channel_free(channel);

    channel = nullptr;

    return SUCCESS;
}

