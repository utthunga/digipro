/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "ExecPreEditActionClntCmd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
ExecPreEditActionClntCmd::ExecPreEditActionClntCmd()
{
}

ExecPreEditActionClntCmd::~ExecPreEditActionClntCmd()
{
}

std::string ExecPreEditActionClntCmd::commandName() const
{
   return "ExecPreEditAction";
}

Json::Value ExecPreEditActionClntCmd::buildParams() const
{
    Json::Value params;

    params["ItemID"] = itemID;
    params["ItemType"] = itemType;
    params["ItemContainerTag"] = itemContainerTag;
    params["ItemInDD"] = itemInDD;

    return params;
}
