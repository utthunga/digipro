/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Set Protocol Type class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"
/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of StartScanClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
class SetItemClntCmnd : public  ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    SetItemClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~SetItemClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    unsigned long itemID = 0;
    unsigned long containerId = 0;
    int subindex = 0;
    std::string abort;
    std::string itemContainerTag;
    bool itemInDD = false;

    Json::Value varValue;
    std::string itemType;
    int dataType = 0;
    int size = 0;
    unsigned int varBitIndex = 0;
    bool bVariable = false;
 };
