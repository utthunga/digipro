#include "SetClntAppSettings.h"

SetClntAppSettings::SetClntAppSettings()
{
    pollOptionType = -1;
}

SetClntAppSettings::~SetClntAppSettings()
{
}

std::string SetClntAppSettings::commandName() const
{
   return "SetAppSettings";
}

Json::Value SetClntAppSettings::buildParams() const
{
   Json::Value params;

   if(protocolType == 1)
   {
       if (pollOptionType != -1)
       {
           params["HART_APP_SETTINGS"]["PollByAddressOption"] = pollOptionType;
       }

       params["HART_APP_SETTINGS"]["set_default"] = setDefaultType;
   }

   if(protocolType == 2)
   {
      params["PB_APP_SETTINGS"]["pb_loc_add"] = address;
      params["PB_APP_SETTINGS"]["set_default"] = setDefaultType;
   }

   return params;
}
