#ifndef GETCLNTAPPSETTINGS_H
#define GETCLNTAPPSETTINGS_H

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of GetClntAppSettings

    This inteface will be using Starsky DBus framework for any external
    communication
*/

class GetClntAppSettings : public ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    GetClntAppSettings();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~GetClntAppSettings();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    std::string type;
};

#endif // GETCLNTAPPSETTINGS_H
