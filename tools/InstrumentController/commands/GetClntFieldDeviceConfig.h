#ifndef GETCLNTFIELDDEVICECONFIG_H
#define GETCLNTFIELDDEVICECONFIG_H


#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

using namespace std;

class GetClntFieldDeviceConfig : public ProtoRpc::JsonClientCommand
{
public:
    GetClntFieldDeviceConfig();

    virtual ~GetClntFieldDeviceConfig();

    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    unsigned int address;
};

#endif // GETCLNTFIELDDEVICECONFIG_H
