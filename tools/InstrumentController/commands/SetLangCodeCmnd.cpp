/*****************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    Set Language Code  class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "SetLangCodeCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
SetLangCodeCmnd::SetLangCodeCmnd()
{
}

SetLangCodeCmnd::~SetLangCodeCmnd()
{
}

std::string SetLangCodeCmnd::commandName() const
{
   return "SetLangCode";
}

Json::Value SetLangCodeCmnd::buildParams() const
{
   Json::Value params;

   params["LangCode"] = m_langCode;
   params["ShortStrFlag"] = m_shortStrFlag;

   return params;
}
