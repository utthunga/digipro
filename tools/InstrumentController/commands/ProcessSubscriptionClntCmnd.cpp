/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "ProcessSubscriptionClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
ProcessSubscriptionClntCmnd::ProcessSubscriptionClntCmnd()
{
}

ProcessSubscriptionClntCmnd::~ProcessSubscriptionClntCmnd()
{
}

std::string ProcessSubscriptionClntCmnd::commandName() const
{
   return "ProcessSubscription";
}

Json::Value ProcessSubscriptionClntCmnd::buildParams() const
{
   Json::Value params;

   params["ItemAction"] = action;
   if(action.compare("create") == 0)
   {
       params["ItemRefreshRate"] = itemRefreshRate;
       params["ItemList"] = itemList;
       params["ItemContainerTag"] = blockTag;
   }
   else
   {
       params["ItemSubscriptionID"] = subscriptionID;
   }


   return params;
}
