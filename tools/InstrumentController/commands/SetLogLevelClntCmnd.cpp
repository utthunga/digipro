/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/* INCLUDE FILES *************************************************************/
#include "SetLogLevelClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
SetLogLevelClntCmnd::SetLogLevelClntCmnd():
        m_logLevel(0)
{
}

SetLogLevelClntCmnd::~SetLogLevelClntCmnd()
{
}

std::string SetLogLevelClntCmnd::commandName() const
{
   return "SetLogLevel";
}

void SetLogLevelClntCmnd::setLogLevel(int logLevel)
{
    m_logLevel = logLevel;
}

Json::Value SetLogLevelClntCmnd::buildParams() const
{
   Json::Value params;

   params["LogLevel"] = m_logLevel;

   return params;
}
