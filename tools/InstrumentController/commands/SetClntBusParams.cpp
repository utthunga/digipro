#include "SetClntBusParams.h"

SetClntBusParams::SetClntBusParams()
{
    masterType = -1;
}

SetClntBusParams::~SetClntBusParams()
{
}

std::string SetClntBusParams::commandName() const
{
   return "SetBusParameters";
}

Json::Value SetClntBusParams::buildParams() const
{
   Json::Value params;

   if (protocolType == 1)
   {
       if (masterType != -1)
       {
           params["HART_BUS_PARAMS"]["masterType"] = masterType;
       }
       params["HART_BUS_PARAMS"]["set_default"] = setDefaultType;
   }

   if(protocolType == 2)
   {
      params["PB_BUS_PARAMS"]["pb_bus_tsl"] = slotTime;
      params["PB_BUS_PARAMS"]["pb_bus_min_tsdr"] = minTsdr;
      params["PB_BUS_PARAMS"]["pb_bus_max_tsdr"] = maxTsdr;
      params["PB_BUS_PARAMS"]["pb_bus_tqui"] = quietTime;
      params["PB_BUS_PARAMS"]["pb_bus_tset"] = setUpTime;
      params["PB_BUS_PARAMS"]["pb_bus_ttr"] = ttr;
      params["PB_BUS_PARAMS"]["pb_bus_g"] = guf;
      params["PB_BUS_PARAMS"]["pb_bus_hsa"] = hsa;
      params["PB_BUS_PARAMS"]["pb_bus_max_retry_limit"] = retryLimit;
      params["PB_BUS_PARAMS"]["set_default"] = setDefaultType;
   }

   return params;
}
