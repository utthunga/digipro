/*****************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    Set Language Code class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of StartScanClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
class SetLangCodeCmnd : public  ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    SetLangCodeCmnd();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~SetLangCodeCmnd();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    std::string m_langCode;

    bool m_shortStrFlag;
};
