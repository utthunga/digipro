/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Set Protocol Type class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"
/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of ShutdownClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
class ShutdownClntCmnd : public  ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    ShutdownClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~ShutdownClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;
 };
