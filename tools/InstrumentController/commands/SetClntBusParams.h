#ifndef SETCLNTBUSPARAMS_H
#define SETCLNTBUSPARAMS_H

#pragma once

/* INCLUDE FILES **************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetClntBusParams

    This inteface will be using Starsky DBus framework for any external
    communication
*/
class SetClntBusParams : public ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    SetClntBusParams();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~SetClntBusParams();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    int masterType;
    int setDefaultType;

    int protocolType;

    //profibus bus params
    uint32_t retryLimit;
    uint32_t minTsdr;
    uint32_t maxTsdr;
    uint32_t slotTime;
    uint32_t quietTime;
    uint32_t setUpTime;
    uint32_t hsa;
    uint32_t guf;
    uint32_t ttr;
};

#endif // SETCLNTBUSPARAMS_H
