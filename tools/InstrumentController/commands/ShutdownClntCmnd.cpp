/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "ShutdownClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ShutdownClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
ShutdownClntCmnd::ShutdownClntCmnd()
{
}

ShutdownClntCmnd::~ShutdownClntCmnd()
{
}

std::string ShutdownClntCmnd::commandName() const
{
   return "Shutdown";
}

Json::Value ShutdownClntCmnd::buildParams() const
{
   Json::Value params("");

   return params;
}
