#ifndef SETCLNTAPPSETTINGS_H
#define SETCLNTAPPSETTINGS_H

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetClntAppSettings

    This inteface will be using Starsky DBus framework for any external
    communication
*/

class SetClntAppSettings : public ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    SetClntAppSettings();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~SetClntAppSettings();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    int pollOptionType;
    int setDefaultType;
    int address;
    int protocolType;
};

#endif // SETCLNTAPPSETTINGS_H
