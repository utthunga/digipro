/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "SetItemClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
SetItemClntCmnd::SetItemClntCmnd()
{
}

SetItemClntCmnd::~SetItemClntCmnd()
{
}

std::string SetItemClntCmnd::commandName() const
{
   return "SetItem";
}

Json::Value SetItemClntCmnd::buildParams() const
{
   Json::Value params;

   params["ItemID"] = itemID;
   params["ItemType"] = itemType;
   params["ItemReqType"] = abort;
   params["ItemInfo"]["VarValue"] = varValue;
   params["ItemContainerTag"] = itemContainerTag;
   params["ItemInDD"] = itemInDD;
   if (bVariable)
   {
       params["ItemInfo"]["VarContainerID"] = containerId;
       params["ItemInfo"]["VarSubIndex"] = subindex;

       if (varBitIndex > 0)
       {
           params["ItemInfo"]["VarBitIndex"] = varBitIndex;
       }
   }

   return params;
}
