/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "GetItemClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
GetItemClntCmnd::GetItemClntCmnd() : varContainerID(0), varSubIndex(0)
{
}

GetItemClntCmnd::~GetItemClntCmnd()
{
}

std::string GetItemClntCmnd::commandName() const
{
   return "GetItem";
}

Json::Value GetItemClntCmnd::buildParams() const
{
    Json::Value params;

    params["ItemID"] = itemID;
    params["ItemType"] = itemType;
    params["ItemContainerTag"] = itemContainerTag;
    params["ItemInDD"] = itemInDD;
    if (bVariable)
    {
        params["ItemInfo"]["VarContainerID"] = varContainerID;
        params["ItemInfo"]["VarSubIndex"] = varSubIndex;
        params["ItemInfo"]["ItemHasRelation"] = itemHasRelation;

        if (varBitIndex > 0)
        {
            params["ItemInfo"]["VarBitIndex"] = varBitIndex;
        }
    }

    return params;
}
