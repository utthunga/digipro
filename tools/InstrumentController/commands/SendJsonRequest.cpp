/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "SendJsonRequest.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of SendJsonRequest

    This inteface will be using Starsky DBus framework for any external
    communication
*/
SendJsonRequest::SendJsonRequest()
{
}

SendJsonRequest::~SendJsonRequest()
{
}

std::string SendJsonRequest::commandName() const
{
   return command;
}

Json::Value SendJsonRequest::buildParams() const
{
   return params;
}
