#ifndef GETCLNTBUSPARAMS_H
#define GETCLNTBUSPARAMS_H

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of GetClntBusParams

    This inteface will be using Starsky DBus framework for any external
    communication
*/

class GetClntBusParams : public ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    GetClntBusParams();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~GetClntBusParams();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    std::string type;
};

#endif // GETCLNTBUSPARAMS_H
