#ifndef SETCLNTFIELDDEVICECONFIG_H
#define SETCLNTFIELDDEVICECONFIG_H


#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

using namespace std;

class SetClntFieldDeviceConfig : public  ProtoRpc::JsonClientCommand
{
public:
    SetClntFieldDeviceConfig();

    virtual ~SetClntFieldDeviceConfig();

    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    unsigned int devAddress;
    unsigned int newDevAddress;
    unsigned int devClass;
    std::string deviceTag;
    std::string deviceConfigType;
};

#endif // SETCLNTFIELDDEVICECONFIG_H
