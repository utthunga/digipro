#include "SetClntFieldDeviceConfig.h"

SetClntFieldDeviceConfig::SetClntFieldDeviceConfig()
{

}

SetClntFieldDeviceConfig::~SetClntFieldDeviceConfig()
{
}

std::string SetClntFieldDeviceConfig::commandName() const
{
   return "SetFieldDeviceConfig";
}

Json::Value SetClntFieldDeviceConfig::buildParams() const
{
   Json::Value params;

   params["DeviceConfig"] = deviceConfigType;
   params["DeviceNodeAddress"] = devAddress;
   params["NewDeviceNodeAddress"] = newDevAddress;
   params["NewDeviceTag"] = deviceTag;
   params["DeviceClass"] = devClass;

   return params;
}
