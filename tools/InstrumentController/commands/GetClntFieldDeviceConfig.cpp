#include "GetClntFieldDeviceConfig.h"

GetClntFieldDeviceConfig::GetClntFieldDeviceConfig()
{

}

GetClntFieldDeviceConfig::~GetClntFieldDeviceConfig()
{
}

std::string GetClntFieldDeviceConfig::commandName() const
{
   return "GetFieldDeviceConfig";
}

Json::Value GetClntFieldDeviceConfig::buildParams() const
{
   Json::Value params;

   params["DeviceNodeAddress"] = address;

   return params;
}
