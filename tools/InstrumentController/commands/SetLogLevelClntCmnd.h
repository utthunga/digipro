#ifndef SETLOGLEVELCLNTCMND_H
#define SETLOGLEVELCLNTCMND_H

/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/

class SetLogLevelClntCmnd : public  ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    SetLogLevelClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~SetLogLevelClntCmnd();

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override;

    Json::Value buildParams() const override;

    // Set lgo level for the CPM application
    void setLogLevel(int logLevel);

private:
    // Holds current log level for cpm application
    unsigned int m_logLevel;
};

#endif // SETLOGLEVELCLNTCMND_H
