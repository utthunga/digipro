/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Connect device class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "ConnectClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd

    This inteface will be using Starsky DBus framework for any external
    communication
*/
ConnectClntCmnd::ConnectClntCmnd()
{
}

ConnectClntCmnd::~ConnectClntCmnd()
{
}

std::string ConnectClntCmnd::commandName() const
{
   return "Connect";
}

Json::Value ConnectClntCmnd::buildParams() const
{
   Json::Value params;

   params["DeviceNodeAddress"] = nodeAddress;

   return params;
}
