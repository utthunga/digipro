/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Set Protocol Type class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "StopScanClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of StartScanClntCmnd 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
StopScanClntCmnd::StopScanClntCmnd()
{
}

StopScanClntCmnd::~StopScanClntCmnd()
{
}

std::string StopScanClntCmnd::commandName() const
{
   return "StopScan";
}

Json::Value StopScanClntCmnd::buildParams() const
{
   Json::Value params;

    params[""] = "";

   return params;
}
