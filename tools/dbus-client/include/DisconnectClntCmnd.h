/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Disconnect Command definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "ProtoRpc/ClientCommand.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DisconnectClntCmnd 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DisconnectClntCmnd : public ProtoRpc::JsonClientCommand
{
public:
    /* Public Member Functions ===============================================*/
    
    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    DisconnectClntCmnd() = default;

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    ~DisconnectClntCmnd() = default;

    /*------------------------------------------------------------------------*/
    /* Command Name
    */
    inline std::string commandName() const override final;

    Json::Value buildParams() const override final;
};
