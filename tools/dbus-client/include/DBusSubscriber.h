/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <mutex>
#include <condition_variable>
#include <flk_log4cplus/Log4cplusAdapter.h>

#include "DBus/Uri.h"
#include "ProtoDBus/Client.h"
#include "PubSub/Subscriber.h"
#include "PubSub/PublisherEndpoint.h"

/* ========================================================================== */

/* Macros definition **********************************************************/
// TODO(selvaraj) It seems that the service name (for example, "com.fluke.cpm.client")
// and the object path (in this case "/Cpmcommand") are always used together. Therefore,
// we decided to use type-safe objects called "endpoints." Consider using the endpoints
// below instead of using macros to define object paths.
//#define CPM_CMD_OBJ_PATH    "/Cpmcommand"

#define CPM_DBUS_CLIENT_URI "com.fluke.cpm.client"
#define CPM_DBUS_CLIENT_APP cpm-dbus-client

// TODO(kraus, selvaraj) remove the g_ prefix and place into a namespace
// TODO(kraus, selvaraj) consider moving these to a separate .h include file
static ProtoDBus::Endpoint g_CpmCommandServer(DBus::DBUS_CPM_URI, "Cpmcommand");
static pubsub::PublisherEndpoint g_CpmScanDataPublisher(DBus::DBUS_CPM_URI, "SCAN_PUBLISHER");
static pubsub::PublisherEndpoint g_CpmCmndRespDataPublisher(DBus::DBUS_CPM_URI, "CMD_RESP_PUBLISHER");
static pubsub::PublisherEndpoint g_CpmItemDataPublisher(DBus::DBUS_CPM_URI, "ITEM_PUBLISHER");

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusSubscriber 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusSubscriber : public pubsub::Subscriber
{
public:
    /* Public Member Functions ===============================================*/
    
    /*------------------------------------------------------------------------*/
    /** Constructor
    */
    DBusSubscriber();

    /*------------------------------------------------------------------------*/
    /* Destructor
    */
    virtual ~DBusSubscriber();

    /*------------------------------------------------------------------------*/
    /** Initializes DBusScanSubscriber with the below scope

       @return Error string
    */
    virtual void initialise();

    /*------------------------------------------------------------------------*/
    /** Shutdown DBusCommandHandler
    */
    virtual void shutDown();

    /*------------------------------------------------------------------------*/
    /** Subscribe to the described client and it handles only the scan
        information

        @param  subscriberName,subscribe to the provided subscriber name
        @return status 
    */
    virtual void subscribeClient(std::string& subscriberName);


    /*------------------------------------------------------------------------*/
    virtual void waitForResponse()
    {
        acquireLock();
    }

protected:
    virtual void acquireLock()
    {
        LOGF_INFO (dbus_client_subscriber, "Acquire Lock");
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        while (!m_condVar)
            m_condVarLock.wait(lock);
        m_condVar = false;
    }

    virtual void releaseLock()
    {
        LOGF_INFO (dbus_client_subscriber, "Release Lock");
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        m_condVar = true;
        m_condVarLock.notify_one();
    }

private:
    /// DBus Client
    ProtoDBus::Client* m_pDbusClient;

    /// Holds the mutex lock required to lock subsription
    std::mutex m_mutex;

    /// Holds condition variable for asynchronous events
    std::condition_variable m_condVarLock;

    /// Holds variable value for condition lock
    bool m_condVar;
};
