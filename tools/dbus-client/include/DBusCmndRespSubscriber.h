/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <json/json.h>
#include "DBusSubscriber.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusCmndRespSubscriber 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusCmndRespSubscriber : public DBusSubscriber
{
public:
    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs DBusCmndRespSubscriber Class.
    */
    DBusCmndRespSubscriber();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes DBusCmndRespSubscriber class instance.
    */
    virtual ~DBusCmndRespSubscriber();

    /*------------------------------------------------------------------------*/
    /** Capture the notify messages from the subscribed client
    */
    void notify(Json::Value message);
};
