/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************
    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhsoh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    The main entry point into the Common Protocol Manager.  Starts the
    CPM's D-Bus client.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/
#include <QCoreApplication>
#include <QDBusServiceWatcher>
#include <QStringList>
#include <QtDBus>
#include "DBusScanSubscriber.h"
#include "DBusCmndRespSubscriber.h"
#include <flk_log4cplus/Log4cplusAdapter.h>
#include "ProtoDBus/Endpoint.h"

#include "SetProtocolTypeClntCmnd.h"
#include "StartScanClntCmnd.h"
#include "ConnectClntCmnd.h"
#include "DisconnectClntCmnd.h"
#include "unistd.h"
#include <thread>

using namespace std;

ProtoDBus::Client cpmClient;

//#define THREAD_SUPPORT

/*------------------------------------------------------------------------*/
/** Subscribe to the publisher provided by cpm
*/
DBusSubscriber* subscribeToPublisher(std::string subscriberName)
{
    if (0 == subscriberName.compare("SCAN_PUBLISHER"))
    {
        // Create dbus command handler instance
        DBusSubscriber* pDBusScanSubscriber = new DBusScanSubscriber();

        // Initializa the command handler interface
        pDBusScanSubscriber->initialise();

        // Subscribe to the publisher
        pDBusScanSubscriber->subscribeClient(subscriberName);

        return pDBusScanSubscriber; 
    }
    else if (0 == subscriberName.compare("CMD_RESP_PUBLISHER"))
    {
        DBusSubscriber* pDBusCmndRespSubscriber = new DBusCmndRespSubscriber();

        pDBusCmndRespSubscriber->initialise();

        pDBusCmndRespSubscriber->subscribeClient(subscriberName);

        return pDBusCmndRespSubscriber;
    }
}

#ifdef THREAD_SUPPORT
void cmndThreadProc(void* arg)
{
    // Sample code for invoking client command
    SetProtocolTypeClntCmnd setPrtclClntCmnd;
    auto setPrtclResp = setPrtclClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Set Protocol Response JSON value" << setPrtclResp.toJsonValue());

#if 0
    pCmndRespSubscriber->waitForResponse();

    // Sample code for invoking client command
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Trigger start scan command");
    StartScanClntCmnd startScanClntCmnd;
    auto startScanClntResp = startScanClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "StartScan Response JSON value" << startScanClntResp.toJsonValue());

    pCmndRespSubscriber->waitForResponse();
#endif
}
#endif

/*------------------------------------------------------------------------*/
/** Main for the cpm dbus client application.

    Main creates the client interface, connects the signal received by the
    D-Bus client interface to the signal receiver class.
*/
int main(
    int argc,
    char* argv[]
)
{
    (void) argv;
	
    QCoreApplication app(argc, argv);

	// Initialize the logging mechanism
	log4cplus::Log4cplusAdapter logAdapter;	

    ProtoDBus::Client cpmClient;
    bool connected = cpmClient.tryConnect(g_CpmCommandServer);
    if (!connected)
    {
        LOGF_ERROR(CPM_DBUS_CLIENT_APP, "Unable to connect to server.");
        // TODO: add error handling   
    }

    // Command response subscriber object subscribed to the command response data
    DBusSubscriber* pCmndRespSubscriber = subscribeToPublisher("CMD_RESP_PUBLISHER");

    // Scan subscriber object subscribes to the scan related data
    DBusSubscriber* pScanSubscriber = subscribeToPublisher("SCAN_PUBLISHER");

#ifdef THREAD_SUPPORT
    std::thread cmndThread(cmndThreadProc, nullptr);

    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Thread has been created with thread id: " << cmndThread.get_id());
#else
    // Sample code for invoking client command
    SetProtocolTypeClntCmnd setPrtclClntCmnd;
    auto setPrtclResp = setPrtclClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Set Protocol Response JSON value" << setPrtclResp.toJsonValue());

#if 0
    pCmndRespSubscriber->waitForResponse();

    // Sample code for invoking client command
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Trigger start scan command");
    StartScanClntCmnd startScanClntCmnd;
    auto startScanClntResp = startScanClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "StartScan Response JSON value" << startScanClntResp.toJsonValue());

    pCmndRespSubscriber->waitForResponse();
#endif
#endif

//    pScanSubscriber->waitForResponse();

#if 0
    // Client command for Connect
    ConnectClntCmnd connectClntCmnd;
    auto connectClntResp = connectClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Connect Respose JSON value" << connectClntResp.toJsonValue());

    // Client command for Connect
    DisconnectClntCmnd disconnectClntCmnd;
    auto disconnectClntResp = disconnectClntCmnd.execute(cpmClient);
    LOGF_INFO (CPM_DBUS_CLIENT_APP, "Disconnect Respose JSON value" << disconnectClntResp.toJsonValue());
#endif


    return app.exec();
}
