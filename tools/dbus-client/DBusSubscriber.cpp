/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Subscriber class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
/* INCLUDE FILES **************************************************************/
#include "DBusSubscriber.h"
#include <flk_log4cplus/Log4cplusAdapter.h>

/* ========================================================================== */
/* Constants and Macros *******************************************************/



/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs DBusSubscriber Class Instance
*/
DBusSubscriber::DBusSubscriber():
    m_pDbusClient(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Destructs DBusSubscriber Class Instance
*/
DBusSubscriber::~DBusSubscriber()
{
}

/*  ----------------------------------------------------------------------------
    Initialise the dbus scrbscriber instance
*/
void DBusSubscriber::initialise()
{
    // Connect dbus service for CPM
    if (nullptr == m_pDbusClient)
    {
        m_pDbusClient = new ProtoDBus::Client;

        if (nullptr != m_pDbusClient)
        {
            bool connectResponse = m_pDbusClient->tryConnect(g_CpmCommandServer);
            if (false == connectResponse)
            {
                LOGF_FATAL(CPM_DBUS_CLIENT_APP, "Cannot connect to the dbus service");
            }
        }
    }
}

/*  ----------------------------------------------------------------------------
    Shutdown the dbus scrbscriber instance
*/
void DBusSubscriber::shutDown()
{
}

/*  ----------------------------------------------------------------------------
    Subscribe to the publisher
*/
void DBusSubscriber::subscribeClient(std::string& subscriberName)
{
    std::string subscriber("Subscriber_");
    
    subscriber = subscriber + subscriberName;

    auto response = pubsub::Subscriber::tryConnect(
            subscriberName,
            DBus::DBUS_CPM_URI,
            subscriber,
            CPM_DBUS_CLIENT_URI);
            
    if (response.isError())
    {
        LOGF_FATAL(CPM_DBUS_CLIENT_APP, "Subscription connection failed:" << response.getErrorMessage());
    }
}
