/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Connect Command class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "ConnectClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of ConnectClntCmnd 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/

std::string ConnectClntCmnd::commandName() const
{
   return "Connect";
}

Json::Value ConnectClntCmnd::buildParams() const
{
   Json::Value params;

   // Sample Test Code
   params["DeviceNodeAddress"] = 21;
   params["DeviceID"]["DeviceManufacturer"] = 0x1151;
   params["DeviceID"]["DeviceType"] = 0x2051;
   params["DeviceID"]["DeviceRevision"] = 0x02;
   params["DeviceID"]["DDRevision"] = 0x01;

   return params;
}
