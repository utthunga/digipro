/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Disconnect Command Class implementation    
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES *************************************************************/
#include "DisconnectClntCmnd.h"

/* ========================================================================== */

/* CLASS DEFINITIONS *********************************************************/
/** class description of DisconnectClntCmnd 

    This inteface will be using Starsky DBus framework for any external 
    communication
*/

std::string DisconnectClntCmnd::commandName() const
{
   return "Disconnect";
}

Json::Value DisconnectClntCmnd::buildParams() const
{
   Json::Value params;

   // Sample test Code
   params["DeviceNodeAddress"] = 21;

   return params;
}

