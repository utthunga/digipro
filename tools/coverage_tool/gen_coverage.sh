#!/bin/bash
#############################################################################
# Copyright (c) 2017,2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: git@bitbucket.org:flukept/digipro.git
# Author    : Aravind Nagaraj
# Origin    : ProStar
# Note      : Generates code coverage report using gcov and lcov
#

# Checks for gcov
which gcov
if [ $? != 0 ]; then
    echo " GCOV not installed; try installing using sudo apt-get install gcov "
    exit 1
fi

# Checks for lcov
which lcov
if [ $? != 0 ]; then
    echo " LCOV not installed; try installing using sudo apt-get install lcov "
    exit 1
fi

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

# Fetch the directory path passed as argument
DIR_PATH=$1
COV_FILE_NAME="coverage"
COV_DIR_NAME="$1"/"$COV_FILE_NAME"

# Copy all coverage related files to coverage directory
cd $DIR_PATH
mkdir -p $COV_DIR_NAME
find . -name \*.gcda -exec cp {} $COV_DIR_NAME \;

find . -name \*.gcno -exec cp {} $COV_DIR_NAME \;

cd $COV_DIR_NAME
find . -name \*.gcda -exec gcov -bf {} \;

# Run lcov on the files
lcov --capture --directory . --output-file $COV_FILE_NAME.info

# Prepare html file
HTML_DIR="$COV_FILE_NAME"_"html"
mkdir -p $HTML_DIR
genhtml $COV_FILE_NAME.info --output-directory $HTML_DIR

echo "#############################################################################"
echo "Code Coverage Report In HTML Format Is Generated at : $COV_DIR_NAME"/"$HTML_DIR"
echo "#############################################################################"
