/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Shutdown command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "Shutdown.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs Shutdown Class Instance
*/
Shutdown::Shutdown()
{
}

/*  ----------------------------------------------------------------------------
    Destructs Shutdown Class Instance
*/
Shutdown::~Shutdown()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string Shutdown::commandName() const
{
    return "Shutdown";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse Shutdown::execute(Json::Value params) const
{
    // Call CPM Shutdown functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_SHUTDOWN_ID, commandName(), params);
#else   
    statusValue = CommonProtocolManager::getInstance()->shutdown(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
