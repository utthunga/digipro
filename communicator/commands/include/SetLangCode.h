/*****************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetLangCode

    SetLogLevel class exposes SetLangCode command to the outside world and handles
    request and response for setting language code operation
*/
class SetLangCode : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs SetLogLevel Class.
        */
        SetLangCode();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes SetLogLevel class instance.
        */
        virtual ~SetLangCode();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const override;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the Set Language Code command with the requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const override;      
};
