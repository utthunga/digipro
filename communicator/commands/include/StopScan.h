/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of StopScan

    StopScan class exposes StopSacn command to the outside world and handles
    request and response for stop scan operation
*/
class StopScan : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs DBusCommandHandler Class.
        */
        StopScan();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes DBusCommandHandler class instance.
        */
        virtual ~StopScan();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const override;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the start scan command with the requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const override;      
};
