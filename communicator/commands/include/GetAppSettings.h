/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand Ugare
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of GetAppSettings

    GetAppSettings class exposes GetAppSettings command to the outside world and handles
    request and response for GetAppSettings.
*/
class GetAppSettings : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs GetAppSettings Class.
        */
        GetAppSettings();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes GetAppSettings class instance.
        */
        virtual ~GetAppSettings();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const final;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the GetAppSetting command with requested parameter
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const final;      
};

