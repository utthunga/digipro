/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://sadanandugare@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand ugare
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetFieldDeviceConfig

    SetFieldDeviceConfig class exposes SetFieldDeviceConfig command to the
    outside world and handles request and response for setting field device
    info
*/
class SetFieldDeviceConfig : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs SetFieldDeviceConfig Class.
        */
        SetFieldDeviceConfig();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes SetFieldDeviceConfig class instance.
        */
        virtual ~SetFieldDeviceConfig();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const final;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the SetFieldDeviceConfig command with the requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const final;
};
