/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of ProcessSubscription

    ProcessSubscription class exposes ProcessSubscription command to the outside world and handles
    for subscribing or unsubscribing parameters
*/
class ProcessSubscription : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs ProcessSubscription Class.
        */
        ProcessSubscription();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes ProcessSubscription class instance.
        */
        virtual ~ProcessSubscription();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const override;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the start scan command with the requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const override;      
};
