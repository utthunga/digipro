/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://sadanandugare@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand ugare
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of GetFieldDeviceConfig

    GetFieldDeviceConfig class exposes GetFieldDeviceConfig command to the outside world and handles
    request and response for getting field device information
*/
class GetFieldDeviceConfig : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs GetFieldDeviceConfig Class.
        */
        GetFieldDeviceConfig();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes GetFieldDeviceConfig class instance.
        */
        virtual ~GetFieldDeviceConfig();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const final;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the get field device command with the requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const final; };
