/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include <string>
#include "ProtoRpc/ServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of StartScan

    StartScan class exposes StartScan command to the outside world and handles
    request and response for start scan operation
*/
class JsonRpcServerCommand : public ProtoRpc::JsonServerCommand
{
public:
    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes DBusCommandHandler class instance.
    */
    virtual ~JsonRpcServerCommand();


    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /* Provides Json Response string
    */
    ProtoRpc::JsonResponse prepareJsonRpcResponse(Json::Value params) const;
};
