/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand Ugare
    Origin:            ProStar
*/

/** @file
    This file contains the Json Rpc command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#pragma once

/* INCLUDE FILES *************************************************************/
#include "JsonRpcServerCommand.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetAppSettings

    SetAppSettings class exposes SetAppSettings command to the outside world and handles
    request and response for SetAppSettings.
*/
class SetAppSettings : public JsonRpcServerCommand
{
    public:
        /* Construction and Destruction ==========================================*/

        /*------------------------------------------------------------------------*/
        /** Constructs SetAppSettings Class.
        */
        SetAppSettings();

        /*------------------------------------------------------------------------*/
        /** virtual destructor, destroyes SetAppSettings class instance.
        */
        virtual ~SetAppSettings();

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Provides the command name for this class instance
        */
        
        std::string commandName() const final;

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /* Execute the set app setting command with requested parameters
        */
        ProtoRpc::JsonResponse execute(Json::Value params) const final;      
};

