/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the SetBusParameters command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetBusParameters.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetBusParameters Class Instance
*/
SetBusParameters::SetBusParameters()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetBusParameters Class Instance
*/
SetBusParameters::~SetBusParameters()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string SetBusParameters::commandName() const
{
    return "SetBusParameters";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetBusParameters::execute(Json::Value params) const
{
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(        
                              CMD_SET_BUS_PARAMETERS_ID, commandName(), params);
#else
    // Call CPM SetBusParameters functionality
    statusValue = CommonProtocolManager::getInstance()->setBusParameters(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
