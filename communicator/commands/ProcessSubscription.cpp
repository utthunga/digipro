/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            Prostar
*/

/** @file
    This file contains the ProcessSubscription command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ProcessSubscription.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs ProcessSubscription Class Instance
*/
ProcessSubscription::ProcessSubscription()
{
}

/*  ----------------------------------------------------------------------------
    Destructs ProcessSubscription Class Instance
*/
ProcessSubscription::~ProcessSubscription()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string ProcessSubscription::commandName() const
{
    return "ProcessSubscription";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse ProcessSubscription::execute(Json::Value params) const
{
    // Call CPM ProcessSubscription functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_PROCESS_SUBSCRIPTION_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->processSubscription(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
