/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Disconnect command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "Disconnect.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs Disconnect Class Instance
*/
Disconnect::Disconnect()
{
}

/*  ----------------------------------------------------------------------------
    Destructs Disconnect Class Instance
*/
Disconnect::~Disconnect()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string Disconnect::commandName() const
{
    return "Disconnect";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse Disconnect::execute(Json::Value params) const
{
    // Call CPM Disconnect functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                   CMD_DISCONNECT_ID, commandName(), params);
#else   
    statusValue = CommonProtocolManager::getInstance()->disconnect();
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
