/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the GetItem command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "GetItem.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs GetItem Class Instance
*/
GetItem::GetItem()
{
}

/*  ----------------------------------------------------------------------------
    Destructs GetItem Class Instance
*/
GetItem::~GetItem()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string GetItem::commandName() const
{
    return "GetItem";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse GetItem::execute(Json::Value params) const
{
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_GET_ITEM_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->getItem(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
