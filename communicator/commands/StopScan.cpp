/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the StopScan command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "StopScan.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs StopScan Class Instance
*/
StopScan::StopScan()
{
}

/*  ----------------------------------------------------------------------------
    Destructs StopScan Class Instance
*/
StopScan::~StopScan()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string StopScan::commandName() const
{
    return "StopScan";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse StopScan::execute(Json::Value params) const
{
    // Call CPM StartScan functionality
    Json::Value statusValue;

    // Call CPM stop scan functionality
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_STOP_SCAN_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->stopScan();
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
