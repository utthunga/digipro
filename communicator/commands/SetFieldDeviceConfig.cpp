/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://sadanandugare@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand ugare
    Origin:            ProStar
*/

/** @file
    This file contains the SetFieldDeviceConfig command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetFieldDeviceConfig.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetFieldDeviceConfig Class Instance
*/
SetFieldDeviceConfig::SetFieldDeviceConfig()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetFieldDeviceConfig Class Instance
*/
SetFieldDeviceConfig::~SetFieldDeviceConfig()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string SetFieldDeviceConfig::commandName() const
{
    return "SetFieldDeviceConfig";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetFieldDeviceConfig::execute(Json::Value params) const
{
    Json::Value statusValue;

#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_SET_FIELD_DEVICE_CONFIG_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->setFieldDeviceConfig(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
