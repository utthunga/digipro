/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the SetItem command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetItem.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetItem Class Instance
*/
SetItem::SetItem()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetItem Class Instance
*/
SetItem::~SetItem()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string SetItem::commandName() const
{
    return "SetItem";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetItem::execute(Json::Value params) const
{
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_SET_ITEM_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->setItem(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
