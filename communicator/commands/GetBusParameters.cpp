/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the GetBusParameters command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "GetBusParameters.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs GetBusParameters Class Instance
*/
GetBusParameters::GetBusParameters()
{
}

/*  ----------------------------------------------------------------------------
    Destructs GetBusParameters Class Instance
*/
GetBusParameters::~GetBusParameters()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string GetBusParameters::commandName() const
{
    return "GetBusParameters";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse GetBusParameters::execute(Json::Value params) const
{
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                               CMD_GET_BUS_PARAMETERS_ID, commandName(), params);
#else   
    // Call CPM GetBusParameters functionality
    statusValue = CommonProtocolManager::getInstance()->getBusParameters();
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
