/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the SetProtocolType command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetProtocolType.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetProtocolType Class Instance
*/
SetProtocolType::SetProtocolType()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetProtocolType Class Instance
*/
SetProtocolType::~SetProtocolType()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string SetProtocolType::commandName() const
{
    return "SetProtocolType";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetProtocolType::execute(Json::Value params) const
{
    // Call CPM SetProtocolType functionality
    Json::Value statusValue;

#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                               CMD_SET_PROTOCOL_TYPE_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->setProtocolType(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
