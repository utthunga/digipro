/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the StartScan command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "StartScan.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs StartScan Class Instance
*/
StartScan::StartScan()
{
}

/*  ----------------------------------------------------------------------------
    Destructs StartScan Class Instance
*/
StartScan::~StartScan()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string StartScan::commandName() const
{
    return "StartScan";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse StartScan::execute(Json::Value params) const
{
    // Call CPM StartScan functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                CMD_START_SCAN_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->startScan(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
