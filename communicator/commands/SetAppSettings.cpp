/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand Ugare
    Origin:            ProStar
*/

/** @file
    This file contains the SetAppSettings command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetAppSettings.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetAppSettings Class Instance
*/
SetAppSettings::SetAppSettings()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetAppSettings Class Instance
*/
SetAppSettings::~SetAppSettings()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for saving app settings
*/
std::string SetAppSettings::commandName() const
{
    return "SetAppSettings";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetAppSettings::execute(Json::Value params) const
{
    // Call CPM SetAppSettings functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                              CMD_SET_APP_SETTINGS_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->setAppSettings(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
