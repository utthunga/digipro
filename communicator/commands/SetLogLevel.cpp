/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the SetLogLevel command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetLogLevel.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetLogLevel Class Instance
*/
SetLogLevel::SetLogLevel()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetLogLevel Class Instance
*/
SetLogLevel::~SetLogLevel()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for set log level operation
*/
std::string SetLogLevel::commandName() const
{
    return "SetLogLevel";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetLogLevel::execute(Json::Value params) const
{
    // Call CPM SetLogLevel functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                CMD_SET_LOG_LEVEL_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->startScan(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
