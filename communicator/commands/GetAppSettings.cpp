/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand Ugare
    Origin:            ProStar
*/

/** @file
    This file contains the GetAppSettings command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "GetAppSettings.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs GetAppSettings Class Instance
*/
GetAppSettings::GetAppSettings()
{
}

/*  ----------------------------------------------------------------------------
    Destructs GetAppSettings Class Instance
*/
GetAppSettings::~GetAppSettings()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for getting app settings
*/
std::string GetAppSettings::commandName() const
{
    return "GetAppSettings";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse GetAppSettings::execute(Json::Value params) const
{
    (void)params;
    // Call CPM GetAppSettings functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                             CMD_GET_APP_SETTINGS_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->getAppSettings();
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
