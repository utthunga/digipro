/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://sadanandugare@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand ugare
    Origin:            ProStar
*/

/** @file
    This file contains the GetFieldDeviceConfig command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "GetFieldDeviceConfig.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs GetFieldDeviceConfig Class Instance
*/
GetFieldDeviceConfig::GetFieldDeviceConfig()
{
}

/*  ----------------------------------------------------------------------------
    Destructs GetFieldDeviceConfig Class Instance
*/
GetFieldDeviceConfig::~GetFieldDeviceConfig()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string GetFieldDeviceConfig::commandName() const
{
    return "GetFieldDeviceConfig";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse GetFieldDeviceConfig::execute(Json::Value params) const
{
    Json::Value statusValue;

#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_GET_FIELD_DEVICE_CONFIG_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->getFieldDeviceConfig(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
