/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the Initialize command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "Initialize.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs Initialize Class Instance
*/
Initialize::Initialize()
{
}

/*  ----------------------------------------------------------------------------
    Destructs Initialize Class Instance
*/
Initialize::~Initialize()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string Initialize::commandName() const
{
    return "Initialize";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse Initialize::execute(Json::Value params) const
{
    // Call CPM Initialize functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_INITIALIZE_ID, commandName(), params);
#else   
    statusValue = CommonProtocolManager::getInstance()->initialize(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
