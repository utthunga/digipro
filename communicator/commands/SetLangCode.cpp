/*****************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    This file contains the SetLangCode command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetLangCode.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs SetLangCode Class Instance
*/
SetLangCode::SetLangCode()
{
}

/*  ----------------------------------------------------------------------------
    Destructs SetLogLevel Class Instance
*/
SetLangCode::~SetLangCode()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for set language code  operation
*/
std::string SetLangCode::commandName() const
{
    return "SetLangCode";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse SetLangCode::execute(Json::Value params) const
{
    // Call CPM SetLangCode functionality
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                CMD_SET_LANG_CODE_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->setLangCode(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
