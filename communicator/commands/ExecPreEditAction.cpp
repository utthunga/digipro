/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana
    Origin:            ProStar
*/

/** @file
    This file contains the ExecPreEditAction command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ExecPreEditAction.h"
#include "CommonProtocolManager.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Constructs ExecPreEditAction Class Instance
*/
ExecPreEditAction::ExecPreEditAction()
{
}

/*  ----------------------------------------------------------------------------
    Destructs ExecPreEditAction Class Instance
*/
ExecPreEditAction::~ExecPreEditAction()
{
}

/*  ----------------------------------------------------------------------------
    Returns command name for start scan operation
*/
std::string ExecPreEditAction::commandName() const
{
    return "ExecPreEditAction";
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse ExecPreEditAction::execute(Json::Value params) const
{
    Json::Value statusValue;
 
#ifdef ENABLE_ASYNC_COMMAND_SUPPORT
    statusValue = CommonProtocolManager::getInstance()->executeCommandAsync(
                                        CMD_EXEC_PRE_EDIT_ACTION_ID, commandName(), params);
#else
    statusValue = CommonProtocolManager::getInstance()->ExecPreEditAction(params);
#endif

    // Check if the status value is either valid response or error response
    return JsonRpcServerCommand::prepareJsonRpcResponse(statusValue);
}
