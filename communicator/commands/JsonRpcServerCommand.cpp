/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    This file contains the JsonRpcServerCommand command classes for remote procedure calls
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "JsonRpcServerCommand.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Destructs JsonRpcServerCommand Class Instance
*/
JsonRpcServerCommand::~JsonRpcServerCommand()
{
}

/*  ----------------------------------------------------------------------------
    Returns json rpc formatted response data
*/
ProtoRpc::JsonResponse JsonRpcServerCommand::prepareJsonRpcResponse(Json::Value jsonData) const
{
    // Convert json value to Json::Error and send it to the above layer
    if ((Json::objectValue == jsonData.type()) &&
        (true == jsonData.isMember("error")))
    {
        ProtoRpc::JsonResponse jsonResponse(ProtoRpc::JsonError(jsonData["error"]["message"].asString(),
                                             jsonData["error"]["code"].asInt()));
        return jsonResponse;
    }
    else if (true == jsonData.isMember("result"))
    {
        ProtoRpc::JsonResponse jsonResponse(jsonData["result"]);

        return jsonResponse;
    }

    return ProtoRpc::JsonResponse(jsonData);
}   
