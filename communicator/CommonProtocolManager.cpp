/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga
*/

/** @file
    Creates the singleton instance of CPM and implements CPM methods
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "json/json.h"
#include "CommonProtocolManager.h"
#include "FFAdaptor.h"
#include "HARTAdaptor.h"
#include "PBAdaptor.h"
#include "JsonRpcUtil.h"
#include "PMErrorCode.h"
#include "ThreadManager.h"
#include "json/json.h"
#include <unistd.h>
#include "error/CpmErrors.h"
#include "YAMLAppConfigParser.h"
#include <flk_log4cplus/Log4cplusAdapter.h>
#include "flk_log4cplus_defs.h"

using namespace std;

/// extern declaration
/* 
 * TODO (Santhosh Kumar Selvaraj): This will be removed from common protocol
 *                                 manager and implemented in dbus comman
 */
extern PMStatus setLog4cplusLogLevel(int nLogLevel);
extern int getLog4cplusLogLevel();

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    CPM class instance initialization
*/
CommonProtocolManager::CommonProtocolManager() : 
        m_pProtocolAdaptor(nullptr),
        m_commandPublisher(CPM_CMD_RESP_PUBLISHER),
        m_commandInProgress(0),
        m_langCode("|en|"),
        m_appConfigParser(nullptr),
        m_bWaitForResp(false)
{
    // Initialize the command proc map table
    initializeCommandProcMapTable();

    // Initialize connfig json parser
    createAppConfigParser();
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
CommonProtocolManager::~CommonProtocolManager()
{
    // clear any allocated memory
    clear();
}

/*  ----------------------------------------------------------------------------
    Implements and returns the singleton instance
*/
CommonProtocolManager* CommonProtocolManager::getInstance()
{
    static CommonProtocolManager cpm;
    return &cpm;
}

/*  ----------------------------------------------------------------------------
    Clears up the memory allocated by CommonProtocolManager
*/
void CommonProtocolManager::clear()
{
    // m_destroyLock.lock();

    // Initialize the command proc map table
    uninitializeCommandProcMapTable();

    // Deletes the ProtocolAdaptor instance
    if (nullptr != m_pProtocolAdaptor)
    {
        // This is an extra safe guard added to CPM application as to clear up all the 
        // memory properly before exiting the application
        if (CPMSTATE_PROTOCOL_INITIALIZED == ((CPMStateMachine::getInstance()->getCpmState()) & CPMSTATE_PROTOCOL_INITIALIZED))
            m_pProtocolAdaptor->shutDown();

        delete m_pProtocolAdaptor;
        m_pProtocolAdaptor = nullptr;
    }

    m_langCode.clear();

    if (NULL != m_appConfigParser)
    {
        delete m_appConfigParser;
        m_appConfigParser = nullptr;
    }

    // m_destroyLock.unlock();
}

/*  ----------------------------------------------------------------------------
    Implements and returns the singleton instance
*/
void CommonProtocolManager::createAppConfigParser()
{
#ifdef APP_CONFIG_PARSER
    if (nullptr == m_appConfigParser)
    {
        m_appConfigParser = new APP_CONFIG_PARSER;
    }
#endif
}

/*  ----------------------------------------------------------------------------
    Implements and returns the singleton instance
*/
IAppConfigParser* CommonProtocolManager::getAppConfigParser()
{
    return m_appConfigParser;
}

/*  ----------------------------------------------------------------------------
    Add publisher to the publisher list table
*/
void CommonProtocolManager::initializePublisherList()
{
    // Insert publisher in the publisher list
    m_publisherList.push_back(m_commandPublisher);
    // Publisher for sending scan information
    m_publisherList.push_back(std::string(SCAN_PUBLISHER));
    // Publisher for sending item information
    m_publisherList.push_back(std::string(ITEM_PUBLISHER));
    // Publisher for sending status message
    m_publisherList.push_back(std::string(STATUS_PUBLISHER));
}

/*  ----------------------------------------------------------------------------
    Register command handler instance
*/
void CommonProtocolManager::registerCommandHandler(ICommandHandler* commandHandlerObj)
{
    m_pCommandHandler = commandHandlerObj;

    // Add the command response publisher to the publisher list
    if (nullptr != m_pCommandHandler)
    {
        initializePublisherList();

        // Add publisher to the command handler publisher table
        for (auto & listElement : m_publisherList)
        {
            addPublisher(listElement);
        }
    }
}

/*  ----------------------------------------------------------------------------
    Unregister command handler instance
*/
void CommonProtocolManager::unregisterCommandHandler()
{
    // Remove the command response publisher from the publisher list
    if (nullptr != m_pCommandHandler)
    {
        for (auto& listElement : m_publisherList)
        {
            removePublisher(listElement);
        }
    }

    m_pCommandHandler = nullptr;;
}

/*  ----------------------------------------------------------------------------
    Provides command id of the current command that is getting executed
*/
uint32_t CommonProtocolManager::getCommandID() const
{
    return m_asyncCommandData.m_commandId;
}

/*  ----------------------------------------------------------------------------
    Provides command name of the current command that is getting executed
*/
std::string CommonProtocolManager::getCommandName() const
{
    return m_asyncCommandData.m_commandName;
}

/*  ----------------------------------------------------------------------------
    Provides command request data for processing
*/
Json::Value CommonProtocolManager::getCommandRequestData() const
{
    return m_asyncCommandData.m_commandRequestData;
}

/*  ----------------------------------------------------------------------------
    Initializes the command proc map table
*/
void CommonProtocolManager::initializeCommandProcMapTable()
{
    m_commandProcMapTable[CMD_SET_PROTOCOL_TYPE_ID] = &CommonProtocolManager::setProtocolType;
    m_commandProcMapTable[CMD_INITIALIZE_ID] = &CommonProtocolManager::initialize;
    m_commandProcMapTable[CMD_SHUTDOWN_ID] = &CommonProtocolManager::shutdown;
    m_commandProcMapTable[CMD_GET_BUS_PARAMETERS_ID] = &CommonProtocolManager::getBusParameters;
    m_commandProcMapTable[CMD_SET_BUS_PARAMETERS_ID] = &CommonProtocolManager::setBusParameters;
    m_commandProcMapTable[CMD_GET_APP_SETTINGS_ID] = &CommonProtocolManager::getAppSettings;
    m_commandProcMapTable[CMD_SET_APP_SETTINGS_ID] = &CommonProtocolManager::setAppSettings;
    m_commandProcMapTable[CMD_START_SCAN_ID] = &CommonProtocolManager::startScan;
    m_commandProcMapTable[CMD_STOP_SCAN_ID] = &CommonProtocolManager::stopScan;
    m_commandProcMapTable[CMD_CONNECT_ID] = &CommonProtocolManager::connect;
    m_commandProcMapTable[CMD_DISCONNECT_ID] = &CommonProtocolManager::disconnect;
    m_commandProcMapTable[CMD_GET_ITEM_ID] = &CommonProtocolManager::getItem;
    m_commandProcMapTable[CMD_SET_ITEM_ID] = &CommonProtocolManager::setItem;
    m_commandProcMapTable[CMD_GET_FIELD_DEVICE_CONFIG_ID] = &CommonProtocolManager::getFieldDeviceConfig;
    m_commandProcMapTable[CMD_SET_FIELD_DEVICE_CONFIG_ID] = &CommonProtocolManager::setFieldDeviceConfig;
    m_commandProcMapTable[CMD_GET_DD_HEADER_INFO_ID] = &CommonProtocolManager::getDDHeaderInfo;
    m_commandProcMapTable[CMD_PROCESS_SUBSCRIPTION_ID] = &CommonProtocolManager::processSubscription;
    m_commandProcMapTable[CMD_SET_LOG_LEVEL_ID] = &CommonProtocolManager::setLogLevel;
    m_commandProcMapTable[CMD_SET_LANG_CODE_ID] = &CommonProtocolManager::setLangCode;
    m_commandProcMapTable[CMD_EXEC_PRE_EDIT_ACTION_ID] = &CommonProtocolManager::execPreEditAction;
    m_commandProcMapTable[CMD_EXEC_POST_EDIT_ACTION_ID] = &CommonProtocolManager::execPostEditAction;
}  

/*  ----------------------------------------------------------------------------
    Uninitialize command proc map table
*/
void CommonProtocolManager::uninitializeCommandProcMapTable()
{   
    m_commandProcMapTable.clear();
}

/*  ----------------------------------------------------------------------------
    Create the thread and associate the thread proc function to it
*/
Json::Value CommonProtocolManager::executeCommandAsync
(
    uint32_t commandId, 
    const std::string& commandName, 
    const Json::Value& jsonData
)
{
    Json::Value jsonResult;

    LOGF_TRACE(CPMAPP, "Request command\n\"CommandName\" : \"" <<
               commandName << "\",\n\"CommandRequest\" : \n" << jsonData);

    m_commandLock.lock();
    
    // Check if any command is currently in progress
    // If no command in progress, start executing the command in different thread
    if (CMD_INVALID_ID == m_commandInProgress)
    {
        std::function<void(const void*)> executeCommandFuncPtr = 
                std::bind (&CommonProtocolManager::executeCommandThreadProc, this, nullptr);

        // Set command id, command name, and command request data
        m_asyncCommandData.m_commandId = commandId;
        m_asyncCommandData.m_commandName = commandName;
        m_asyncCommandData.m_commandRequestData = jsonData;

        // Create a thread and execute the command in that thread
        (void) ThreadManager::getInstance()->createThread(executeCommandFuncPtr, nullptr);

        // Enable particular bit field to indicate this command is in the process of
        // execution
        m_commandInProgress |= commandId;

        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {   
        // If the command is already in progress, inform to the client that the command
        // is already in progress
        PMStatus status;

        Error::Entry error = Error::cpm.COMMAND_IN_PROGRESS;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    
    m_commandLock.unlock();

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Execute the command in this thread proc function
*/
void CommonProtocolManager::executeCommandThreadProc(const void* argData)
{
    // Unused variable
    (void) argData;

    // m_destroyLock.lock();

    // Get the currently executed command id
    uint32_t commandID = this->getCommandID();

    // Inform notify to hold the notifications till the response for the command
    // is sent to Instrument controller; this ensures the following pattern
    
    /*      ---> Command Request
            <--- Command Response
            <<-- Notification 1
            <<-- Notification 2
    */
    if ((CMD_GET_ITEM_ID == commandID) || (CMD_SET_ITEM_ID == commandID))
    {
        setWaitForRespState(true);
    }

    // Get the command procecure from the comamnd proc map table
    auto commandProc = m_commandProcMapTable.find(commandID);

    if (commandProc != m_commandProcMapTable.end())
    {
        Json::Value notifyResp;

        if (true == verifyCmdStateAndExecute(commandID))
        {
            // Execute the requested command and prepare notify message with the response data
            Json::Value jsonCmndResp = (this->*(commandProc->second))(this->getCommandRequestData());
            notifyResp[JSON_COMMAND_NAME_STR] = this->getCommandName();
            notifyResp[JSON_COMMAND_RESP_STR] = jsonCmndResp;

            m_commandLock.lock();
            // Notify the command response data
            m_pCommandHandler->publishData(m_commandPublisher, notifyResp);
            m_commandLock.unlock();
        }

        // Remove the thread id from the thread table maintained by 
        // thread manager
        std::stringstream ss;
        ss << std::this_thread::get_id();
        uint64_t threadId = stoull (ss.str());
        ThreadManager::getInstance()->destroyThread(threadId);

        // Unset the bit field as to indicate this command finished it's
        // execution
        m_commandLock.lock();
        m_commandInProgress &= ~commandID;
        m_commandLock.unlock();

        // Inform the waiting notify method to proceed 
        if ((CMD_GET_ITEM_ID == commandID) || (CMD_SET_ITEM_ID == commandID))
        {
            setWaitForRespState(false);
        }
    }
    else
    {
        // is already in progress
        PMStatus status;

        Error::Entry error = Error::cpm.UNKNOWN_ERROR;
        Json::Value notifyResp = JsonRpcUtil::setErrorObject(error.code, error.message);

        m_commandLock.lock();
        // Notify the command response data
        m_pCommandHandler->publishData(m_commandPublisher, notifyResp);
        m_commandLock.unlock();
    }
   
    // m_destroyLock.unlock();
}


/*  ----------------------------------------------------------------------------
    Initializes the stack and other protocol related information required for 
    the user selected protocol
*/
Json::Value CommonProtocolManager::initialize(const Json::Value& jsonData)
{
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->initialise();
    }

    if (jsonResult.isNull())
    {       
        Error::Entry error = Error::cpm.PROTOCOL_NOT_SET;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    
    return jsonResult;
}


/*  ----------------------------------------------------------------------------
    Shutdown and other protocol related information required for
    the user selected protocol
*/
Json::Value CommonProtocolManager::shutdown(const Json::Value& jsonData)
{
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->shutDown();

        if (nullptr != m_pProtocolAdaptor)
        {
            delete m_pProtocolAdaptor;
            m_pProtocolAdaptor = nullptr;
        }
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.PROTOCOL_NOT_SET;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}


/*  ----------------------------------------------------------------------------
    Uses command handler object to add publisher in the publisher table
*/
int CommonProtocolManager::addPublisher(std::string& publisherName)
{
    int addPubStatus = -1;

    if (nullptr != m_pCommandHandler)
    {
        addPubStatus = m_pCommandHandler->addPublisher(publisherName);
    }

    return addPubStatus;
}

/*  ----------------------------------------------------------------------------
    Uses command handler object to remove publisher from the publisher table
*/
int CommonProtocolManager::removePublisher(std::string& publisherName)
{
    int removePubStatus = -1;

    if (nullptr != m_pCommandHandler)
    {
        removePubStatus = m_pCommandHandler->removePublisher(publisherName);
    }

    return removePubStatus;
}

/*  ----------------------------------------------------------------------------
    Notifies the messsage to Instrument Controller over D-Bus
*/
void CommonProtocolManager::notifyMessage(std::string& publisherName, 
                                          Json::Value& jsonData)
{
    if (nullptr != m_pCommandHandler)
    {
        waitForResponse();
        m_pCommandHandler->publishData(publisherName, jsonData);
    }
}

/*  ----------------------------------------------------------------------------
    Stores the DD binary file in the default DD storage location using
    Protocol Adaptor method.
*/
Json::Value CommonProtocolManager::updateDD(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->updateDD(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.UPDATE_DD_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Parses the Protocol Type and initializes the appropriate Protocol Adaptor
    Instance
*/
Json::Value CommonProtocolManager::setProtocolType(const Json::Value& jsonData)
{
    /* Instantiates the protocol adaptor instance only if it is not already
       initialized for the requested protocol else it returns as it is*/
    PMStatus status;
    Json::Value jsonResult;	
    CPMPROTOCOL newProtocolType = CPMPROTOCOL::CPMPROTOCOL_NONE;
    Error::Entry error;

    // Parses protocol type
    status = parseProtocolType(jsonData, newProtocolType);

    if (!status.hasError())
    {
        CPMPROTOCOL currentActiveProtocol = CPMPROTOCOL::CPMPROTOCOL_NONE;
        
        if (m_pProtocolAdaptor)
            currentActiveProtocol = m_pProtocolAdaptor->getActiveProtocol();

        if ((nullptr == m_pProtocolAdaptor) || 
            (currentActiveProtocol != newProtocolType))
        {
            // Instantiates Protocol Adaptor instance if it is not created
            if (newProtocolType == CPMPROTOCOL::CPMPROTOCOL_FF)
            {
                if (m_pProtocolAdaptor)
                {
                    delete m_pProtocolAdaptor;
                    m_pProtocolAdaptor = nullptr;
                }

                if (nullptr != m_appConfigParser)
                {
                    if (true == m_appConfigParser->loadAndParseAppConfig(newProtocolType))    
                    {
                        m_pProtocolAdaptor = new FFAdaptor();
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Error parsing application configuration file");
                        error = Error::cpm.SET_PROTOCOL_FAILED;
                        status.setError(CPM_ERR_INVALID_PROTOCOL_TYPE, PM_ERR_CL_CPM);
                    }
                }
            }
            else if (newProtocolType == CPMPROTOCOL::CPMPROTOCOL_HART)
            {
                if (m_pProtocolAdaptor)
                {
                    delete m_pProtocolAdaptor;
                    m_pProtocolAdaptor = nullptr;
                }

                if (nullptr != m_appConfigParser)
                {
                    if (true == m_appConfigParser->loadAndParseAppConfig(newProtocolType))    
                    {
                        m_pProtocolAdaptor = new HARTAdaptor();
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Error parsing application configuration file");
                        error = Error::cpm.SET_PROTOCOL_FAILED;
                        status.setError(CPM_ERR_INVALID_PROTOCOL_TYPE, PM_ERR_CL_CPM);
                    }
                }
            }
            else if (newProtocolType == CPMPROTOCOL::CPMPROTOCOL_PROFIBUS)
            {
                if (m_pProtocolAdaptor)
                {
                    delete m_pProtocolAdaptor;
                    m_pProtocolAdaptor = nullptr;
                }

                if (nullptr != m_appConfigParser)
                {
                    if (true == m_appConfigParser->loadAndParseAppConfig(newProtocolType))
                    {
                        m_pProtocolAdaptor = new PBAdaptor();
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Error parsing application configuration file");
                        error = Error::cpm.SET_PROTOCOL_FAILED;
                        status.setError(CPM_ERR_INVALID_PROTOCOL_TYPE, PM_ERR_CL_CPM);
                    }
                }
            }
            else
            {
                error = Error::cpm.SET_PROTOCOL_FAILED;
                status.setError(CPM_ERR_INVALID_PROTOCOL_TYPE, PM_ERR_CL_CPM);
            }
        }
        else
        {
            error = Error::cpm.PROTOCOL_ALREADY_SET;
            status.setError(CPM_ERR_PROTOCOL_ADAPTOR_ALREADY_INITIALIZED,
                                PM_ERR_CL_CPM);
        }
    }
    else
    {
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        // Set the protocol state as initialized
        CPMStateMachine::getInstance()->setCpmState(CPMSTATE_PROTOCOL_SET);

        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Parses the SetProtocolType Json data
*/
PMStatus CommonProtocolManager::parseProtocolType(
    const Json::Value& data, CPMPROTOCOL& protocolType)
{
    PMStatus status;

    if (data.isNull())
    {
        status.setError(CPM_ERR_JSON_PARSING_FAILED, PM_ERR_CL_CPM);
    }
    else 
    {
        if (data.isMember(JSON_ITEM_PROTOCOL) && data[JSON_ITEM_PROTOCOL].isString())
        {
            std::string protocol = data[JSON_ITEM_PROTOCOL].asString();

            if (0 == protocol.compare(FF_PROTOCOL))
            {
                protocolType = CPMPROTOCOL::CPMPROTOCOL_FF;
            }
            else if (0 == protocol.compare(HART_PROTOCOL))
            {
                protocolType = CPMPROTOCOL::CPMPROTOCOL_HART;
            }
            else if (0 == protocol.compare(PROFIBUS_PROTOCOL))
            {
                protocolType = CPMPROTOCOL::CPMPROTOCOL_PROFIBUS;
            }
            else
            {
                status.setError(CPM_ERR_INVALID_PROTOCOL_TYPE, PM_ERR_CL_CPM);
            }
        }
        else
        {
            status.setError(CPM_ERR_JSON_PARSING_FAILED, PM_ERR_CL_CPM);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Provides the currently loaded DD Header information
*/
Json::Value CommonProtocolManager::getDDHeaderInfo(const Json::Value& jsonData)
{
    // Unused input parameter
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->getDDHeaderInfo();
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.GET_DD_HEADER_INFO_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}


/*  ----------------------------------------------------------------------------
    Retrieves the requested DD Item info
*/
Json::Value CommonProtocolManager::getItem(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->getItem(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.GET_ITEM_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Stores the value to cache table/writes the value to the transmitter
*/
Json::Value CommonProtocolManager::setItem(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->setItem(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.SET_ITEM_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    processes diferent action for subscription operation
*/
Json::Value CommonProtocolManager::processSubscription(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->processSubscription(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.PROCESS_SUBSCRIPTION_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Initiates the network scanning operation using Protocol Adaptor method
*/
Json::Value CommonProtocolManager::startScan(const Json::Value& jsonData)
{
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->startScan(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.START_SCAN_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Stops the network scanning operation using Protocol Adaptor method
*/
Json::Value CommonProtocolManager::stopScan(const Json::Value& jsonData)
{
    // Unused iniput parameter
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->stopScan();
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.STOP_SCAN_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Connects the requested device in the network
*/
Json::Value CommonProtocolManager::connect(const Json::Value& jsonData)
{
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->connect(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Disconnects the currently connected device using Protocol Adaptor method
*/
Json::Value CommonProtocolManager::disconnect(const Json::Value& jsonData)
{
    // Unused input parameter
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->disconnect();
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Stores the bus parameters in the persistent file using Protocol Adaptor
    method
*/
Json::Value CommonProtocolManager::setBusParameters(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->setBusParameters(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.SET_BUS_PARAM_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Provides the bus parameters from persistent file using Protocol Adaptor
    method
*/
Json::Value CommonProtocolManager::getBusParameters(const Json::Value& jsonData)
{
    // Unused input parameter
    (void) jsonData;
    
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->getBusParameters();
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.GET_BUS_PARAM_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    configures field device information using Protocol Adaptor
    method
*/
Json::Value CommonProtocolManager::setFieldDeviceConfig
(
    const Json::Value& jsonData
)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->setFieldDeviceConfig(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.SET_FIELD_DEVICE_CONFIG_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    retrieves field device information using Protocol Adaptor
    method
*/
Json::Value CommonProtocolManager::getFieldDeviceConfig
(
    const Json::Value& jsonData
)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->getFieldDeviceConfig(jsonData);
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.GET_FIELD_DEVICE_CONFIG_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Sets Log level for communicator application
*/
Json::Value CommonProtocolManager::setLogLevel(const Json::Value& jsonData)
{
    PMStatus logLevelStatus;
    Json::Value jsonResult;

	if ((jsonData == Json::nullValue) || (jsonData["LogLevel"].isNull()))
	{
		LOGF_ERROR(CPMAPP,"Failed to parse json set log level request");
        Error::Entry error = Error::cpm.INVALID_LOG_LEVEL;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
	}

	int nLogLevel = jsonData["LogLevel"].asInt();
    logLevelStatus = setLog4cplusLogLevel (nLogLevel);

	if (GENERIC_PM_ERR_SET_LL_INVALID_LL == logLevelStatus.getErrorCode())
	{
		LOGF_ERROR(CPMAPP,"Invalid log level request");
        Error::Entry error = Error::cpm.INVALID_LOG_LEVEL;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
	}

	jsonResult["result"] = Json::Value("ok");

	return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Gets Current Log level for communicator application
*/
int CommonProtocolManager::getLogLevel()
{
    int logLevel = getLog4cplusLogLevel();

	return logLevel;
}

/* ----------------------------------------------------------------------------
   Get the applicaiton settings required for any protocol
   Currently this has been specificaly used for HART protocol
*/
Json::Value CommonProtocolManager::getAppSettings(const Json::Value& jsonData )
{
    // Unused input parameter
    (void) jsonData;

    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->getAppSettings();
    }

    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.GET_APP_SETTINGS_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/* ----------------------------------------------------------------------------
   Set the applicaiton settings required for any protocol
   Currently this has been specificaly used for HART protocol
*/
Json::Value CommonProtocolManager::setAppSettings(const Json::Value& jsonData)
{
    Json::Value jsonResult;
    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->setAppSettings(jsonData);
    }
    if (jsonResult.isNull())
    {
        Error::Entry error = Error::cpm.SET_APP_SETTINGS_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    return jsonResult;
}

/* ----------------------------------------------------------------------------
   Sets the language code to extract language specific string
*/
Json::Value CommonProtocolManager::setLangCode(const Json::Value& langCodeReq)
{
    Json::Value langCodeResp;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    // Check for JSON request validity
    if ( langCodeReq.isMember(JSON_LANG_CODE) &&
         langCodeReq.isMember(JSON_SHORT_STR_FLAG) &&
         langCodeReq[JSON_LANG_CODE].isString() &&
         langCodeReq[JSON_SHORT_STR_FLAG].isBool())
    {
        std::string countryLangCode = langCodeReq[JSON_LANG_CODE].asString();

        /*
         * language code is set in following format "|<langcode> <short string>|
         * Ex : "|en zz|", "|de zz|" , "|en|"
         */

        m_langCode.clear();

        m_langCode += "|";

        if (countryLangCode.empty())
        {
            // No language code is selected; setting english as default language
            LOGF_ERROR(CPMAPP, "Setting English as default language code");
            m_langCode += DEFAULT_LANG_CODE;
        }
        else
        {
            m_langCode += countryLangCode;
        }

        if (langCodeReq[JSON_SHORT_STR_FLAG].asBool())
        {
            // Short string version of DD strings is selected
            m_langCode += " ";
            m_langCode += LANG_SHORT_STR;
        }
        m_langCode += "|";

        langCodeResp = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }

    else
    {
        // Invalid JSON request; setting English as default language
        m_langCode += "|";
        m_langCode += DEFAULT_LANG_CODE;
        m_langCode += "|";

        // Setting language failed
        error = Error::cpm.SET_LANG_CODE_FAILED;
        langCodeResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return langCodeResp;
}

/* ----------------------------------------------------------------------------
   Gets language code set for CPM
*/
std::string CommonProtocolManager::getLangCode()
{
    return m_langCode;
}

/* ----------------------------------------------------------------------------
   Executes Pre Edit Action for Menu
*/
Json::Value CommonProtocolManager::execPreEditAction(
        const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->execPreEditAction(jsonData);
    }

    if (jsonResult.isNull())
    {
        // TODO (BHUVANA) : Add proper error code
        Error::Entry error = Error::cpm.SET_LANG_CODE_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/* ----------------------------------------------------------------------------
   Executes Post Edit Action for Menu
*/
Json::Value CommonProtocolManager::execPostEditAction(
        const Json::Value& jsonData)
{
    Json::Value jsonResult;

    if (nullptr != m_pProtocolAdaptor)
    {
        jsonResult = m_pProtocolAdaptor->execPostEditAction(jsonData);
    }

    if (jsonResult.isNull())
    {
        // TODO (BHUVANA) : Add proper error code
        Error::Entry error = Error::cpm.SET_LANG_CODE_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/* ----------------------------------------------------------------------------
   Verify triggered command can be executed in CPM application

   The state machine within the CPM application has been mentioned below  

   ------------------         -----------------              ------------------------------------
  | Set Language Code|<-------| Set Protocol    |  --------> | Configure BusParameter/AppSettings |
   ------------------         -----------------              ------------------------------------
                                    |                     
                                    |                     
                                    V                      
                                ------------------         
                     --------- | Initialize Stack | ---
                    |           ------------------      |
                    |               |                   |     
                    |               |                   | 
                    V               V                   V
            ----------------    -------------------     --------------------------
           | Shutdown Stack |  | Connect to Device |   | Scan / Field Device Setup|
            ----------------    -------------------     --------------------------      
                                    |
                                    |
                         ----------- ----------      
                        |                      | 
                        |                      | 
                        V                      V 
                    ------------         --------------------------------------------------    
                   | Disconnect |       | Get / Set / Method Exec / Subscription (DD Items)|     
                    ------------         --------------------------------------------------
*/
bool CommonProtocolManager::verifyCmdStateAndExecute(const unsigned int commandID)
{
    Json::Value jsonResult;

    CPMStateMachine *pStateMachineInstance = CPMStateMachine::getInstance();

    unsigned int currentCpmState = pStateMachineInstance->getCpmState();

    switch(commandID)
    {
        case CMD_SET_PROTOCOL_TYPE_ID:
            if (currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED)
            {
                Error::Entry error = Error::cpm.SET_PROTOCOL_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }

            else if (currentCpmState & CPMSTATE_LANGUAGE_CODE_SET)
            {
                Error::Entry error = Error::cpm.LANG_CODE_ALREADY_SET;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            break;

        case CMD_SET_BUS_PARAMETERS_ID:
        case CMD_GET_BUS_PARAMETERS_ID:
        case CMD_GET_APP_SETTINGS_ID:
        case CMD_SET_APP_SETTINGS_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_SET))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_SET;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            else if (currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED)
            {
                if (CMD_GET_BUS_PARAMETERS_ID == commandID)
                {    
                    Error::Entry error = Error::cpm.GET_BUS_PARAM_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
                }
                else if (CMD_SET_BUS_PARAMETERS_ID == commandID)
                {
                    Error::Entry error = Error::cpm.SET_BUS_PARAM_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message); 
                }
                else if (CMD_GET_APP_SETTINGS_ID == commandID)
                {
                    Error::Entry error = Error::cpm.GET_APP_SETTINGS_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message); 
                }
                else
                {
                    Error::Entry error = Error::cpm.SET_APP_SETTINGS_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message); 
                }
            }
            break;

        case CMD_INITIALIZE_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_SET))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_SET;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            else if (currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED)
            {
                Error::Entry error = Error::cpm.PROTOCOL_ALREADY_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            break;

        case CMD_SHUTDOWN_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            break;

        case CMD_START_SCAN_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if ((currentCpmState & CPMSTATE_SCANNING_IN_PROGRESS) ||
                     (currentCpmState & CPMSTATE_DEVICE_CONNECTED))
            {
                Error::Entry error = Error::cpm.START_SCAN_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            break;    

        case CMD_STOP_SCAN_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if (!(currentCpmState & CPMSTATE_SCANNING_IN_PROGRESS))
            {
                Error::Entry error = Error::cpm.STOP_SCAN_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            break;

        case CMD_GET_FIELD_DEVICE_CONFIG_ID:
        case CMD_SET_FIELD_DEVICE_CONFIG_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if ((currentCpmState & CPMSTATE_SCANNING_IN_PROGRESS) ||
                     (currentCpmState & CPMSTATE_DEVICE_CONNECTED))
            {
                if (CMD_GET_FIELD_DEVICE_CONFIG_ID == commandID)
                {    
                    Error::Entry error = Error::cpm.GET_FIELD_DEVICE_CONFIG_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
                }
                else
                {
                    Error::Entry error = Error::cpm.SET_FIELD_DEVICE_CONFIG_FAILED;
                    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message); 
                }
            }
            break;    

        case CMD_CONNECT_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if ((currentCpmState & CPMSTATE_SCANNING_IN_PROGRESS) ||
                     (currentCpmState & CPMSTATE_DEVICE_CONNECTED))
            {
                Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            break;

        case CMD_DISCONNECT_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if (!(currentCpmState & CPMSTATE_DEVICE_CONNECTED))
            {
                Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            break;

        case CMD_GET_ITEM_ID:
        case CMD_SET_ITEM_ID:
        case CMD_PROCESS_SUBSCRIPTION_ID:
            if (!(currentCpmState & CPMSTATE_PROTOCOL_INITIALIZED))
            {
                Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);                                 
            }
            else if (!(currentCpmState & CPMSTATE_DEVICE_CONNECTED))
            {
                Error::Entry error;

                if (commandID == CMD_GET_ITEM_ID)   
                    error = Error::cpm.GET_ITEM_FAILED;
                else if (commandID == CMD_SET_ITEM_ID)
                    error = Error::cpm.SET_ITEM_FAILED;
                else
                    error = Error::cpm.PROCESS_SUBSCRIPTION_FAILED;

                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            break;

        default:
            break;
    }

    if (false == jsonResult.isNull())
    {
        Json::Value notifyResp;
        notifyResp[JSON_COMMAND_NAME_STR] = this->getCommandName();
        notifyResp[JSON_COMMAND_RESP_STR] = jsonResult;

        // Notify the command response data
        m_pCommandHandler->publishData(m_commandPublisher, notifyResp);
        return false;
    }

    return true;   
}

/* ----------------------------------------------------------------------------
   Gets adaptor instance
*/
IProtocolAdaptor* CommonProtocolManager::getAdaptorInstance()
{
	return m_pProtocolAdaptor;
}

/* ----------------------------------------------------------------------------
   Sets wait state for notification
*/
void CommonProtocolManager::setWaitForRespState(bool waitState)
{
    m_bWaitForResp = waitState;

    if (false == m_bWaitForResp)
    {
        // Signal waiting thread to proceed 
        m_notifyProceed.notify_one();
    }
}
/* ----------------------------------------------------------------------------
   Waits till the response for CPM request is sent to IC
*/
void CommonProtocolManager::waitForResponse()
{
    // Wait until the response is sent to IC
    std::unique_lock<std::mutex> notifyLock(m_ResponseMutex);
    m_notifyProceed.wait(notifyLock,[this] {return false == m_bWaitForResp;});
}
