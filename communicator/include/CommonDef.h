/******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Global declaration require for common modules
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _COMMON_DEF_H
#define _COMMON_DEF_H

/* STRUCTURES and ENUMS DECLARATIONS ******************************************/
/// enum declaration for protocol
enum class CPMPROTOCOL
{
    CPMPROTOCOL_NONE       = 0, ///< No protocol
    CPMPROTOCOL_FF         = 1, ///< Specifies FF protocol
    CPMPROTOCOL_HART       = 2, ///< Specifies HART protocol
    CPMPROTOCOL_PROFIBUS   = 3  ///< Specifies PROFIBUS protocol
};

#endif // _COMMON_DEF_H

