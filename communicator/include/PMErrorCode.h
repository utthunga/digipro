/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://git@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Protocol Manager Error Class & Error Sub Class definitions along with Error Codes
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#ifndef PM_ERROR_H
#define PM_ERROR_H

/********************** !!! NOTE !!!! *****************************************
**  Developers can extend error classes and also define error subclasses
**  for each class as and when needed !

********************** !!! NOTE !!!! *****************************************/

/// Indicates success case for any module that is out of PM scope
#define PM_SUCCESS     0
/// Indicates failure case for any module that is out of PM scope
#define PM_FAILURE     -1

/* ===========================================================================*/
/// Enumeration defines the Error Classes for Protocol Manager
enum PM_ERR_CL
{
    PM_ERR_CL_OS_ERRORS = 0,
    PM_ERR_CL_GENERIC,
    PM_ERR_CL_CPM,
    PM_ERR_CL_FF_ADAPTOR,
    PM_ERR_CL_HART_ADAPTOR,
    PM_ERR_CL_PB_ADAPTOR,
    PM_ERR_CL_DDS_CTRL,
    PM_ERR_CL_SDC_CTRL,
    PM_ERR_CL_PB_CTRL,
    PM_ERR_CL_FF_DDS,
    PM_ERR_CL_SDC_625,
    PM_ERR_CL_PB_DDS,
    PM_ERR_CL_CSL,
    PM_ERR_CL_FF_CSL,
    PM_ERR_CL_HART_CSL,
    PM_ERR_CL_PB_CSL,
    PM_ERR_CL_FF_STACK,
    PM_ERR_CL_HART_STACK,
    PM_ERR_CL_PB_STACK
    /// Error codes 15 to 63 is available for future use
};

/* ===========================================================================*/
/// Enumeration defines the Error Sub Classes for FF_DDS class of errors
enum FFDDS_ERR_SC
{
    FFDDS_ERR_SC_RESERVED = 0, 	/// Sub Class Not available
    FFDDS_ERR_SC_DDS,
    FFDDS_ERR_SC_EVAL,
    FFDDS_ERR_SC_FETCH,
    FFDDS_ERR_SC_DDI,
    FFDDS_ERR_SC_BUILTINS,
    FFDDS_ERR_SC_METHODS,
    FFDDS_ERR_SC_PC,
    FFDDS_ERR_SC_NM,
    FFDDS_ERR_SC_CSTK,
    FFDDS_ERR_SC_CM
};


/* ===========================================================================*/
/// Enumeration defines the Error Sub Classes for HART_DDS class of errors
enum HARTDDS_ERR_SC
{
    HARTDDS_ERR_SC_RESERVED = 0, 	/// Sub Class Not available
    HARTDDS_ERR_SC_DDS,
    HARTDDS_ERR_SC_EVAL,
    HARTDDS_ERR_SC_FETCH,
    HARTDDS_ERR_SC_DDI,
    HARTDDS_ERR_SC_BUILTINS,
    HARTDDS_ERR_SC_METHODS,
    HARTDDS_ERR_SC_PC,
    HARTDDS_ERR_SC_NM,
    HARTDDS_ERR_SC_CSTK,
    HARTDDS_ERR_SC_CM
};


/* ===========================================================================*/
/// Enumeration defines the Error Sub Classes for PB_DDS class of errors
enum PBDDS_ERR_SC
{
    PBDDS_ERR_SC_RESERVED = 0, 	/// Sub Class Not available
    PBDDS_ERR_SC_DDS
};

/* ===========================================================================*/
/// Enumeration defines the Error Sub Classes for CSL_ERR class of errors
enum CSL_ERR_SC
{
    CSL_ERR_SC_RESERVED = 0, 	/// Sub Class Not Available
    CSL_ERR_SC_CM,
    CSL_ERR_SC_PC,
    CSL_ERR_SC_SBC              ///added for subscription controller
};

/* ===========================================================================*/
/// Enumeration defines the Error Sub Classes for FF_ADAPTOR_ERR class of errors
enum FF_ADAPTOR_ERR_SC
{
    FF_ADAPTOR_SC_RESERVED = 0, 	/// Sub Class Not Available
    FF_ADAPTOR_SC_FF,
    FF_ADAPTOR_SC_BL,
    FF_ADAPTOR_SC_PSI
};

enum GENERIC_ERR_SC
{
    GENERIC_ERR_SC_RESERVED = 0,
    GENERIC_ERR_SC_LL,
    GENERIC_ERR_SC_MEMORY,
};

////////////////////////////////////// NOTE !!!! //////////////////////////////

/// Define Error Codes for each class and subclass as needed
/// Error Definition needed only for our application code and Errors Need not be
/// defined for 3rd party code (FF_DDS, SDC_625 and PB_DDS and all the Stacks)

////////////////////////////////////// NOTE !!!! //////////////////////////////
/* ===========================================================================*/
/// Enumeration defines the possible error codes returned for generic errors
enum GENERIC_PM_ERR
{
    GENERIC_PM_ERR_SET_LL_INVALID_JSON = 0,
    GENERIC_PM_ERR_SET_LL_INVALID_LL,
    GENERIC_PM_ERR_NO_MEMORY
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from CPM
enum CPM_ERR
{
    /// Add CPM Specific Error Codes here
    CPM_ERR_JSON_PARSING_FAILED = 0,
    CPM_ERR_INVALID_PROTOCOL_TYPE,
    CPM_ERR_ADAPTOR_ERROR,
    CPM_ERR_PROTOCOL_ADAPTOR_ALREADY_INITIALIZED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from FF Adaptor
enum FF_ADAPTOR_ERR
{
    /// Add FF Adaptor specific Error Codes here
    FF_ADAPTOR_ERR_INITIALIZE_FAILED = 0,
    FF_ADAPTOR_ERR_SCAN_NETWORK,
    FF_ADAPTOR_ERR_GET_DEV_INFO,
    FF_ADAPTOR_ERR_SCAN_THREAD_STOPPED,
    FF_ADAPTOR_ERR_STOP_SCAN_FAILED,
    FF_ADAPTOR_ERR_BUSPARAM_GET_BUS_PARAMS_FAIL,
    FF_ADAPTOR_ERR_BUSPARAM_BL_JSON_PARSE_FAIL,
    FF_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_OPEN_FAIL,
    FF_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_READ_FAIL,
    FF_ADAPTOR_ERR_BUSPARAM_BL_SAVE_PARAM_PSI_WRITE_FAIL,
    FF_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL,
    FF_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL,
    FF_ADAPTOR_ERR_INVALID_TAG,
    FF_ADAPTOR_ERR_INVALID_ADDRESS,
    FF_ADAPTOR_ERR_DEVICE_ALREADY_COMMISSIONED,
    FF_ADAPTOR_INVALID_JSON_REQUEST,
    FF_ADAPTOR_ERR_INVALID_DEVICE_HANDLE,
    FF_ADAPTOR_ERR_INVALID_STATE_OF_ME_THREAD,
	FF_ADAPTOR_ERR_DATA_EDIT_ACTION,
	FF_ADAPTOR_ERR_INVALID_STATE_OF_PRE_EDIT_ACTION_THREAD,
	FF_ADAPTOR_ERR_INVALID_STATE_OF_POST_EDIT_ACTION_THREAD,
    FF_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED,
    FF_ADAPTOR_ERR_PROCESS_SUBSCRIPTION_FAILED,
    FF_ADAPTOR_ERR_START_SUBSCRIPTION_FAILED,
    FF_ADAPTOR_ERR_STOP_SUBSCRIPTION_FAILED,
    FF_ADAPTOR_ERR_DELETE_SUBSCRIPTION_FAILED,
    FF_ADAPTOR_ERR_GET_ADVANCED_BLOCK_VIEW_FAILED,
    FF_ADAPTOR_ERR_PREP_SYM_TABLE_FAILED,
    FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
	FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
	FF_ADAPTOR_ERR_NOT_CURRENT_NODE_ADDRESS,
    FF_ADAPTOR_ERR_DDI_GET_ITEM_FAILED,
    FF_ADAPTOR_ERR_NO_PRE_POST_EDIT_ACTION_CONFIG
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from HART Adaptor
enum HART_ADAPTOR_ERR
{
    /// Add HART Adaptor specific Error Codes here
    HART_ADAPTOR_ERR_JSON_PARSE = 0,
    HART_ADAPTOR_ERR_POLL_OPTION_INVALID,
    HART_ADAPTOR_ERR_GET_BUS_PARAM,
    HART_ADAPTOR_ERR_INVALID_MASTERTYPE,
    HART_ADAPTOR_ERR_BUS_PARAM_NOT_AVAIL,
    HART_ADAPTOR_ERR_APP_SETTINGS_NOT_AVAIL,
    HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
    HART_ADAPTOR_ERR_METHOD_ALREADY_RUNNING,
    HART_ADAPTOR_ERR_GET_METHOD_FAILED,
    HART_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED,
    HART_ADAPTOR_ERR_START_SUBSCRIPTION_FAILED,
    HART_ADAPTOR_ERR_STOP_SUBSCRIPTION_FAILED,
    HART_ADAPTOR_ERR_DELETE_SUBSCRIPTION_FAILED,
    HART_ADAPTOR_INVALID_JSON_REQUEST,
    HART_ADAPTOR_GET_DD_MENU_FAILED,
    HART_ADAPTOR_GET_EDIT_DISPLAY_FAILED,
    HART_ADAPTOR_GET_WAO_FAILED,
	HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
    HART_ADAPTOR_EDIT_ACTION_IN_PROGRESS, //Temporary error code
    HART_ADAPTOR_ERR_EDIT_ACTION_FAILED,
    HART_ADAPTOR_ERR_NULL_INSTANCE
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned Profibus Adaptor
enum PB_ADAPTOR_ERR
{
    /// Add Profibus Adaptor specific Error Codes here
    PB_ADAPTOR_ERR_INITIALIZE_FAILED = 0,
    PB_ADAPTOR_ERR_SCAN_NETWORK,
    PB_ADAPTOR_ERR_GET_DEV_INFO,
    PB_ADAPTOR_ERR_SCAN_THREAD_STOPPED,
    PB_ADAPTOR_ERR_GET_BUS_PARAMS_FAIL,
    PB_ADAPTOR_ERR_BUSPARAM_BL_JSON_PARSE_FAIL,
    PB_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_OPEN_FAIL,
    PB_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_READ_FAIL,
    PB_ADAPTOR_ERR_BUSPARAM_BL_SAVE_PARAM_PSI_WRITE_FAIL,
    PB_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL,
    PB_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL,
    PB_ADAPTOR_INVALID_JSON_REQUEST,
    PB_ADAPTOR_ERR_INVALID_DEVICE_HANDLE,
    PB_ADAPTOR_ERR_INVALID_STATE_OF_ME_THREAD,
    PB_ADAPTOR_ERR_DATA_EDIT_ACTION,
    PB_ADAPTOR_ERR_INVALID_STATE_OF_PRE_EDIT_ACTION_THREAD,
    PB_ADAPTOR_ERR_INVALID_STATE_OF_POST_EDIT_ACTION_THREAD,
    PB_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED,
    PB_ADAPTOR_ERR_PROCESS_SUBSCRIPTION_FAILED,
    PB_ADAPTOR_ERR_START_SUBSCRIPTION_FAILED,
    PB_ADAPTOR_ERR_STOP_SUBSCRIPTION_FAILED,
    PB_ADAPTOR_ERR_DELETE_SUBSCRIPTION_FAILED,
    PB_ADAPTOR_ERR_GET_ADVANCED_BLOCK_VIEW_FAILED,
    PB_ADAPTOR_ERR_PREP_SYM_TABLE_FAILED,
    PB_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
    PB_ADAPTOR_ERR_EDIT_ACTION_FAILED,
    PB_ADAPTOR_ERR_NOT_CURRENT_NODE_ADDRESS,
    PB_ADAPTOR_ERR_DDI_GET_ITEM_FAILED,
    PB_ADAPTOR_ERR_NO_PRE_POST_EDIT_ACTION_CONFIG,
    PB_ADAPTOR_ERR_JSON_PARSE,
    PB_ADAPTOR_ERR_INVALID_JSON_REQUEST,
    PB_ADAPTOR_ERR_INVALID_LOCAL_MASTER_NODE_ADDRESS,
    PB_ADAPTOR_ERR_APP_SETTINGS_NOT_AVAIL,
    PB_ADAPTOR_ERR_FETCHING_STACK_CONFIGURATION,
    PB_ADAPTOR_GET_DD_MENU_FAILED
};

/* ===========================================================================*/
/// Enumeration defines the posible error codes returned from DDS Controller
enum DDS_CTRL_ERR
{
    /// Add DDS Controller specific Error Codes here
    DDS_CTRL_ERR_INITIALIZE_FAILED = 0,
    DDS_CTRL_ERR_WRONG_DEVICE_HANDLE,
    DDS_CTRL_ERR_DDI_GET_ITEM_FAILED,
    DDS_CTRL_ERR_DDI_GET_ITEM2_FAILED,
    DDS_CTRL_ERR_DDI_GET_WAO_FAILED,
    DDS_CTRL_ERR_DDI_CLEAN_ITEM_FAILED,
    DDS_CTRL_ERR_DDI_LOCATE_ITEM_FAILED,
    DDS_CTRL_ERR_CT_BLOCK_SEARCH_FAILED,
    DDS_CTRL_ERR_INVALID_BLOCK_HANDLE,
    DDS_CTRL_ERR_DD_BLK_LOAD_FAILED,
    DDS_CTRL_ERR_SET_FUN_CALLBACK_FAILED,
    DDS_CTRL_ERR_EXECUTE_METHOD_FAILED,
    DDS_CTRL_ERR_GET_BLTN_FORMAT_STRING,
    DDS_CTRL_ERR_GET_DEVICE_INFO,
    DDS_CTRL_ERR_GET_STR_ENCODE
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from SDC-625 Controller
enum SDC_CTRL_ERR
{
    /// Add SDC Controller specific Error Codes here
    SDC_CTRL_ERR_INITIALIZE_FAILED = 0,
    SDC_CTRL_CREATE_DEVICE_FAILED,
    SDC_CTRL_DELETE_DEVICE_FAILED,
    SDC_CTRL_GET_MENU_FAILED,
    SDC_CTRL_GET_EDIT_DISPLAY_FAILED,
    SDC_CTRL_GET_WAO_FAILED,
    SDC_CTRL_GET_METHOD_FAILED,
    SDC_CTRL_EXECUTE_METHOD_FAILED,
    SDC_CTRL_EXECUTE_EDIT_METHOD_FAILED,
    SDC_CTRL_GET_PARAMETER_FAILED,
    SDC_CTRL_READ_VALUE_FAILED,
    SDC_CTRL_WRITE_VALUE_FAILED,
    SDC_CTRL_GET_ITEM_TYPE_FAILED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from Profibus Controller
enum PB_CTRL_ERR
{
    /// Add Profibus Controller specific Error Codes here
    PB_CTRL_ERR_INITIALIZE_FAILED = 0,
    PB_CTRL_FETCH_MENU_ITEMS_FAILED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from Connection Manager
enum CM_ERR
{
    /// Add Connection Manager (inside CSL) specific Error Codes here
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from Parameter Cache
enum PC_ERR
{
    /// Add Parameter Cache (inside CSL) specific Error Codes here
    PC_ERR_CREATE_SUBSCRIPTION_FAILED = 0
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from FF Connection Manager
enum FF_CM_ERR
{
    FF_CM_ERR_INITIALIZE_FAILED = 0,
    FF_CM_ERR_GET_DD_PATH_FAILED,
    FF_CM_ERR_GET_BUS_PARAM_FAILED,
    FF_CM_ERR_ACCESS_PORT_FAILED,
    FF_CM_ERR_GET_HOST_INFO_FAILED,
    FF_CM_ERR_CONFIG_FF_FAILED,
    FF_CM_ERR_INITIALIZE_FF_FAILED,
    FF_CM_ERR_SCAN_NETWORK_FAILED,
    FF_CM_ERR_GET_DEVICE_INFO_FAILED,
    FF_CM_ERR_DEVICE_ALREADY_CONNECTED,
    FF_CM_ERR_LOAD_BLOCK_FAILED,
    FF_CM_ERR_OPEN_BLOCK_FAILED,
    FF_CM_ERR_CONNECT_DEVICE_FAILED,
    FF_CM_ERR_DISCONNECT_DEVICE_FAILED,
    FF_CM_ERR_NO_CONNECTED_DEVICE,
    FF_CM_ERR_UNINITIALIZE_FF_FAILED,
    FF_CM_ERR_FILL_BLK_TABLE_FAILED,
    FF_CM_ERR_SET_DEVICE_CLASS_FAILED,
    FF_CM_ERR_SET_NEW_DEVICE_TAG_FAILED,
    FF_CM_ERR_SET_NEW_DEVICE_ADDRESS_FAILED,
    FF_CM_ERR_GET_NETWORK_HANDLE_FAILED,
    FF_CM_ERR_GET_NETWORK_COUNT_FAILED,
    FF_CM_ERR_READ_NM_OBJECTS_FAILED,
    FF_CM_ERR_GET_DEVICE_CLASS_VALUE_FAILED,
    FF_CM_ERR_SM_IDENTITY_FAILED,
    FF_CM_ERR_GET_NM_DIR_FAILED,
    FF_CM_ERR_SM_CLEAR_ADDRESS_FAILED,
    FF_CM_ERR_RESTART_DEVICE_FAILED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from HART Connection Manager
enum HART_CM_ERR
{
    HART_CM_ERR_INITIALIZE_FAILED = 0,
    HART_CM_ERR_INIT_STACK_FAILED,
    HART_CM_ERR_CONNECT_DEVICE_FAILED,
    HART_CM_ERR_DEVICE_ALREADY_CONNECTED,
    HART_CM_ERR_STACK_ALREADY_INITIALIZED,
    HART_CM_ERR_INVALID_POLL_BY_ADDRESS_OPTION,
    HART_CM_SCAN_INTERRUPTED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from Profibus Connection Manager
enum PB_CM_ERR
{
    /// Add Connection Manager (inside CSL) specific Error Codes here
    PB_CM_ERR_INITIALIZE_FAILED = 0,
    PB_CM_ERR_GET_DD_PATH_FAILED,
    PB_CM_ERR_GET_BUS_PARAM_FAILED,
    PB_CM_ERR_ACCESS_PORT_FAILED,
    PB_CM_ERR_GET_HOST_INFO_FAILED,
    PB_CM_ERR_CONFIG_PB_FAILED,
    PB_CM_ERR_INITIALIZE_PB_FAILED,
    PB_CM_ERR_SCAN_NETWORK_FAILED,
    PB_CM_ERR_GET_DEVICE_INFO_FAILED,
    PB_CM_ERR_DEVICE_ALREADY_CONNECTED,
    PB_CM_ERR_LOAD_BLOCK_FAILED,
    PB_CM_ERR_OPEN_BLOCK_FAILED,
    PB_CM_ERR_CONNECT_DEVICE_FAILED,
    PB_CM_ERR_DISCONNECT_DEVICE_FAILED,
    PB_CM_ERR_NO_CONNECTED_DEVICE,
    PB_CM_ERR_UNINITIALIZE_PB_FAILED,
    PB_CM_ERR_FILL_BLK_TABLE_FAILED,
    PB_CM_ERR_SET_DEVICE_CLASS_FAILED,
    PB_CM_ERR_SET_NEW_DEVICE_TAG_FAILED,
    PB_CM_ERR_SET_NEW_DEVICE_ADDRESS_FAILED,
    PB_CM_ERR_GET_NETWORK_HANDLE_FAILED,
    PB_CM_ERR_GET_NETWORK_COUNT_FAILED,
    PB_CM_ERR_READ_NM_OBJECTS_FAILED,
    PB_CM_ERR_GET_DEVICE_CLASS_VALUE_FAILED,
    PB_CM_ERR_SM_IDENTITY_FAILED,
    PB_CM_ERR_GET_NM_DIR_FAILED,
    PB_CM_ERR_SM_CLEAR_ADDRESS_FAILED,
    PB_CM_ERR_RESTART_DEVICE_FAILED,
    PB_CM_ERR_SET_BUS_PARAMETER_FAILED,
    PB_CM_ERR_SET_APP_SETTING_FAILED,
    PB_CM_ERR_READ_DATA_FAILED,
    PB_CM_ERR_WRITE_DATA_FAILED
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from FF Parameter Cache
enum FF_PC_ERR
{
    FF_PC_ERR_INITIALIZE_FAILED = 0,
    FF_PC_ERR_READ_PARAM_VALUES_FAILED,
    FF_PC_ERR_GET_PARAMETER_FAILED,
    FF_PC_ERR_SELECT_BLOCKVIEW_PARAMETER_FAILED,
    FF_PC_ERR_GET_ENVIRON_INFO_FAILED,
    FF_PC_ERR_GET_PARAM_ITEM_FAILED,
    FF_PC_ERR_SELECT_PARAM_ITEM_FAILED,
    FF_PC_ERR_WRITE_VALUE_FAILED,
    FF_PC_ERR_READ_EDIT_VALUE_FAILED,
    FF_PC_ERR_WRITE_EDIT_VALUE_FAILED,
    FF_PC_ERR_READ_DEVICE_VALUE_FAILED,
    FF_PC_ERR_WRITE_DEVICE_VALUE_FAILED,
    FF_PC_ERR_SUBSCRIPTION_ID_NOT_FOUND
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from HART Parameter Cache
enum HART_PC_ERR
{
    HART_PC_ERR_INITIALIZE_FAILED = 0,
    HART_PC_ERR_REQUEST_ITEM_NULL
};

/* ===========================================================================*/
/// Enumeration defines the possible error codes returned from Profibus Parameter Cache
enum PB_PC_ERR
{
    /// Add Parameter Cache (inside CSL) specific Error Codes here
    PB_PC_ERR_INITIALIZE_FAILED = 0,
};


/* GENERALIZED ERROR CODE BASE DECLARATION ************************************/
/// Macro declaration for the error code base

/* ===========================================================================*/
/// Concatenate strings
#define CONCATENATE_STR(STRING1, STRING2)	#STRING1 #STRING2
/// Convert generalized enum error code to error string id's
#define GET_ERROR_STR_ID(VAL)			CONCATENATE_STR(VAL,_STR)
/// Create map with enum's and string id's
#define DECLARE_STR_ID(VAL)				{VAL, GET_ERROR_STR_ID(VAL)}

/* ===========================================================================*/
/// Error code base for complete protocol manager and this is set as per
/// the Starsky project
#define PM_ERROR_CODE_BASE				90000

/* ===========================================================================*/
/// Error code base for generic errors such as Log level, DBUS, JSON, OS call
/// error, etc
/// Generic errors
#define CPM_GENERIC_ERROR_BASE          (PM_ERROR_CODE_BASE)
/// Protocol specific errors
#define CPM_PROTOCOL_ERROR_BASE         (PM_ERROR_CODE_BASE + 100)
/// Scanning specific errors
#define CPM_SCANNING_ERROR_BASE         (PM_ERROR_CODE_BASE + 200)
/// Bus parameter specific errors
#define CPM_BUS_PARAM_ERROR_BASE        (PM_ERROR_CODE_BASE + 300)
/// Device configuration specific errors
#define CPM_DEVICE_CONFIG_ERROR_BASE    (PM_ERROR_CODE_BASE + 400)
/// Application setting specific errors
#define CPM_APP_SETTINGS_ERROR_BASE     (PM_ERROR_CODE_BASE + 500)
/// Communication specific errors
#define CPM_COMMUNICATION_ERROR_BASE    (PM_ERROR_CODE_BASE + 600)
/// Item specific errors
#define CPM_ITEM_ERROR_BASE             (PM_ERROR_CODE_BASE + 1000)

#endif /* PM_ERROR_H */
