/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Command Handler Interface class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

#include <string>


/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Command Handler interface class
class ICommandHandler
{
    public:
        /* Public Types ==========================================================+*/

        /* Construction and Destruction ==========================================+*/
        /*-------------------------------------------------------------------------*/
        /** Destruct a IProtocolAdaptor.
         *
         */
        virtual ~ICommandHandler()
        {
        }

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /** Interface function for command handler initialization
         * 
         *  @return status of the initiaslization in json string format
         */
        virtual int initialise() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for destroying objects and shutdown it properly
         * 
         */
        virtual int shutDown() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for adding publisher to the corresponding command
         *  handler publisher table
         *
         *  @return status of adding publisher to its corrrsponding table
         */
        virtual int addPublisher(std::string& publisherName) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for removing publisher from the corresponding
         *  cpommand handler publisher table
         *
         *  @return status of removing publisher from the corresponding table
         */
        virtual int removePublisher(std::string& publisherName) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for publishing data to the subscribed client
         *
         *  @return status of the update
         */
        virtual int publishData(std::string& publisherName, Json::Value& data) = 0;
};
