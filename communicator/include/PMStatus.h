/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://git@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Protocol Manager Error Class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#ifndef PM_STATUS_H
#define PM_STATUS_H

/* INCLUDE FILES **************************************************************/
#include <string>

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** Definition of PMStatus Class which handles errors for Protocol Manager

    Description of class: This class has interfaces for processing the errors
	within the Protocol Manager and also has methods for setting and getting 
	different types of error information like error class & subclass.
*/

class PMStatus
{
public:

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs PMStatus object.

    */
    PMStatus();

    /*------------------------------------------------------------------------*/
    /** Destructs PMStatus object

    */
    ~PMStatus() = default;

    /// Set and Get Access API's
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** One-line summary of member function.

        Description of function. If overriding a virtual function, use the
        keyword override.

        @return Description of return value.
    */

    /*------------------------------------------------------------------------*/
    /** Returns if an error exists or not

        @ Returns True if an error exists else False
    */
    bool hasError();
	
    /*------------------------------------------------------------------------*/
    /** Retrieves Error Class information

        @ Returns the error class bit-field value in the Error Object as an integer
    */
    int getErrorClass();

    /*------------------------------------------------------------------------*/
    /** Retrieves Error Sub Class information

        @ Returns the error sub class bit-field value in the Error Object as an integer
    */
    int getErrorSubClass();

    /*------------------------------------------------------------------------*/
    /** Retrieves the actual error code information

        @ Returns the actual error code value in the Error Object as an integer
    */
    int getErrorCode();

    /*------------------------------------------------------------------------*/
    /** Retrieves the Error object (integer) of Protocol Manager

        @ Returns the PM Error Object as an integer
    */
    int getPMError();

    /*------------------------------------------------------------------------*/
    /** Retrieves the correspodning Error string for the error object

        @ Returns the correspodning error message as a string 
    */
    std::string getErrorString();

    /*------------------------------------------------------------------------*/
    /** Retrieves the sub system Error as required while filling the JSON error object  

        @ Returns the sub system error code as an integer as required by JSON error object
    */
    int getSubsystemError(); /// Returns the Value as required for filling in JSON response

    /*------------------------------------------------------------------------*/
    /** Set error code class, subclass, pm error and generalized error code 
	
	The protocol manager status message will be encapsulated as below
	----------------------------------------------------------------------
	| Sign Bit | Status code | Error Class | Error Subclass | Error code |
	|  1 bit   |   3 bits    |   6 bits    |    6 bits      |   16 bits  |
        ----------------------------------------------------------------------
	Refer enum definition in PMErrorCode.h for more information
	
	@param errorCode Holds error code set by pm modules
	@param errorClass Holds error class set by pm modules
	@param errorSubClass Holds error subclass set by pm modules
	@param generalizedError Holds generalized error code set by pm module
								for outside world
        @return indicates success/failure status for setError call
    */
    bool setError(
	int errorCode, ///< Error code set by the corresponding module within protocol manager
	int errorClass, ///< Error class set by the corresponding module within protocol manager
	int errorSubClass = 0 ///< Error subclass set by the corresponding module within protocol manager
    );

	/** Clear error code

    This API clears the error code
    */
    void clear();

private:
    /* Private Data Members ==================================================*/

    /// Protocol Manager Error Object
    int m_pmError;
};


#endif /* PM_STATUS_H */
