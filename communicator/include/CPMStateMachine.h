/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CPM State Machine declaration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _CPM_STATE_MACHINE_H
#define _CPM_STATE_MACHINE_H

/* INCLUDE FILES **************************************************************/
#include <mutex>

/* MACRO DEFINITION ***********************************************************/

enum CPMSTATE
{
    CPMSTATE_PROTOCOL_NOT_SET = 0x00000000,
    CPMSTATE_PROTOCOL_SET = 0x000000001,
    CPMSTATE_PROTOCOL_INITIALIZED = 0x00000002,
    CPMSTATE_SCANNING_IN_PROGRESS = 0x00000004,
    CPMSTATE_DEVICE_CONNECTED = 0x00000008,
    CPMSTATE_METHOD_EXEC_IN_PROGRESS = 0x00000010,
    CPMSTATE_LANGUAGE_CODE_SET = 0x00000020,
	CPMSTATE_DISCONNECTION_IN_PROGRESS = 0x00000040,
	CPMSTATE_CONNECTION_IN_PROGRESS = 0x00000080,
	CPMSTATE_COMMSSION_IS_IN_PROGRESS = 0x000000100
};

/*============================================================================*/
/** CPMStateMachine Class Definition.

    CPMStateMachine class is used to maintain state machine in CPM application
*/

class CPMStateMachine
{
public:
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Implements and returns the singleton instance

        @return CPMStateMachine instance
    */
    static CPMStateMachine* getInstance();

    /*------------------------------------------------------------------------*/
    /** Set CPM state in ProStar application
        
        @param eCPMState, CPM state that need to be set 
    */
    void setCpmState(/*[IN]*/ unsigned int eCPMState);

    /*------------------------------------------------------------------------*/
    /** Get CPM state
        
        @return current state of the CPM application
    */
    unsigned int getCpmState();

    /*------------------------------------------------------------------------*/
    /** Clear state of the CPM application
    */
    void clearCpmState(unsigned int cpmStates);

    /*------------------------------------------------------------------------*/
    /** Reset state of the CPM application
    */
    void resetCpmState();

    /* Explicitly Disabled Methods ===========================================*/
    CPMStateMachine& operator=(const CPMStateMachine& rhs) = delete;
    CPMStateMachine(const CPMStateMachine& rhs) = delete;

private:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Construct a CPMStateMachine
    */
    CPMStateMachine();

    /*------------------------------------------------------------------------*/
    /** Destruct a CPMStateMachine
    */
    virtual ~CPMStateMachine();

    /* Private Data Members ==================================================*/

    // Holds lock for CPM stte machine
    std::mutex m_lock;

    // Holds CPM state
    unsigned int m_cpmState;
};

#endif  // _CPM_STATE_MACHINE_H
