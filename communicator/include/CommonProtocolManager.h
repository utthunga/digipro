/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga
*/

/** @file
    Common Protocol Manager class declaration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _COMMON_PROTOCOL_MANAGER_H
#define _COMMON_PROTOCOL_MANAGER_H

/* INCLUDE FILES **************************************************************/

#include <string>
#include <mutex>
#include "IProtocolAdaptor.h"
#include "ICommandHandler.h"
#include "CPMStateMachine.h"
#include "flk_log4cplus_clogger.h"
#include "IAppConfigParser.h"
#include <condition_variable>

/* MACRO DEFINITION ***********************************************************/

/*============================================================================*/
#define CPM_CMD_RESP_PUBLISHER      "CMD_RESP_PUBLISHER"
#define JSON_COMMAND_NAME_STR       "CommandName"
#define JSON_COMMAND_RESP_STR       "CommandResponse"

/*============================================================================*/
/* Holds command macros for each command */
#define CMD_INVALID_ID                    0x00000000    
#define CMD_SET_PROTOCOL_TYPE_ID          0x00000001
#define CMD_INITIALIZE_ID                 0x00000002
#define CMD_SHUTDOWN_ID                   0x00000004
#define CMD_GET_BUS_PARAMETERS_ID         0x00000008
#define CMD_SET_BUS_PARAMETERS_ID         0x00000010
#define CMD_GET_APP_SETTINGS_ID           0x00000020 
#define CMD_SET_APP_SETTINGS_ID           0x00000040 
#define CMD_START_SCAN_ID                 0x00000080
#define CMD_STOP_SCAN_ID                  0x00000100
#define CMD_CONNECT_ID                    0x00000200
#define CMD_DISCONNECT_ID                 0x00000400
#define CMD_GET_ITEM_ID                   0x00000800
#define CMD_SET_ITEM_ID                   0x00001000
#define CMD_GET_FIELD_DEVICE_CONFIG_ID    0x00002000
#define CMD_SET_FIELD_DEVICE_CONFIG_ID    0x00004000
#define CMD_GET_DD_HEADER_INFO_ID         0x00008000
#define CMD_PROCESS_SUBSCRIPTION_ID       0x00010000
#define CMD_SET_LANG_CODE_ID              0x00020000
#define CMD_EXEC_PRE_EDIT_ACTION_ID       0x00040000
#define CMD_EXEC_POST_EDIT_ACTION_ID      0x00080000
/* Add new command ID before CMD_SET_LOG_LEVEL_ID */
#define CMD_SET_LOG_LEVEL_ID              0x80000000

#define JSON_LANG_CODE      "LangCode"
#define JSON_SHORT_STR_FLAG "ShortStrFlag"
#define DEFAULT_LANG_CODE   "en"
#define LANG_SHORT_STR      "zz"

/*============================================================================*/
typedef struct AsyncCommandData
{
    uint32_t m_commandId;
    std::string m_commandName;
    Json::Value m_commandRequestData;
}AsyncCommandInfo;

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** CommonProtocolManager Class Definition.

    CommonProtocolManager class is interface between UI
    and Adaptor over DBus.
*/

class CommonProtocolManager
{
public:
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Implements and returns the singleton instance

        @return commonprotocolmanager instance
    */
    static CommonProtocolManager* getInstance();

    /*------------------------------------------------------------------------*/
    /** Cleans up the memory allocated by CommonProtocolManager instance
    */
    void clear();

    /*------------------------------------------------------------------------*/
    /** Uses command handler object to add publisher to the publisher table
        
        @param publisherName,provided publisher name from the adaptor
        @return status,status of the adding publisher to the publisher table
    */
    int addPublisher(std::string& publisherName);

    /*------------------------------------------------------------------------*/
    /** Uses command handler object to remove publisher from the publisher table
        
        @param publisherName,provided publisher name from the adaptor
        @return status,status of the adding publisher to the publisher table
    */
    int removePublisher(std::string& publisherName);

    /*------------------------------------------------------------------------*/
    /** Initialized the command proc map table
    */
    void initializeCommandProcMapTable();

    /*------------------------------------------------------------------------*/
    /** Initialized the command proc map table
    */
    void uninitializeCommandProcMapTable();

    /*------------------------------------------------------------------------*/
    /** Provides currently executed command ID 
        
        @return Command ID
    */
    uint32_t getCommandID() const;

    /*------------------------------------------------------------------------*/
    /** Provides currently executed command name

        @return Command ID
    */
    std::string getCommandName() const;
    
    /*------------------------------------------------------------------------*/
    /** Provides currently executed command request data
        
        @return command request data    
    */
    Json::Value getCommandRequestData() const;

    /*------------------------------------------------------------------------*/
    /** Creates a seperate thread and execute the command in that thread proc

        @return command request data
    */
    Json::Value executeCommandAsync(uint32_t commandId, 
                                    const std::string& commandName, 
                                    const Json::Value& jsonData);
    
    /*------------------------------------------------------------------------*/
    /** Thread proc function that actually executes the command depending upon
        command id
    */
    void executeCommandThreadProc(const void* argData);

    /*------------------------------------------------------------------------*/
    /** Notifies the messsage to Instrument Controller over D-Bus

        @param publisherName,publisher to which the message to be sent
        @param jsonData,json message data
    */
    void notifyMessage(std::string& publisherName, Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Register command handler instance within CPM
        
        @param commandHandlerObj,Command handler instance
    */
    void registerCommandHandler(ICommandHandler* commandHandlerObj);

    /*------------------------------------------------------------------------*/
    /** Unregister command handler instance within CPM
    */
    void unregisterCommandHandler();

    /*------------------------------------------------------------------------*/
    /** Provides the currently loaded DD Header information

        @return result or error returns from
        m_pProtocolAdaptor->GetDDHeaderInfo() method in the JSON format
    */
    Json::Value getDDHeaderInfo(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Initializes the stack and other information for the set protocol
        Protocol Adaptor method.
    */
    Json::Value initialize(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** Shutdown the stack and other information for the initialized protocol
    */
    Json::Value shutdown(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** Stores the DD binary file in the default DD storage location using
        Protocol Adaptor method.

        @param jsonData - contains the request details in JSON format

        @return success or error status returns from
        m_pProtocolAdaptor->UpdateDD method() in JSON format
    */
    Json::Value updateDD(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Parses the Protocol Type and initializes the appropriate Protocol
        Adaptor Instance

        @param jsonData - contains the protocol type

        @return success or error status
    */
    Json::Value setProtocolType(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stores the value to cache table/writes the value to the transmitter

        @param jsonData - contains the request details in JSON format

        @return result or error returns from m_pProtocolAdaptor->setItem()
        method in the JSON format
    */
    Json::Value setItem(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves the requested DD Item info

        @param jsonData - contains the request details in JSON format

        @return result or error returns from m_pProtocolAdaptor->getItem()
        method in the JSON format
    */
    Json::Value getItem(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Processes required subscription action

        @param jsonData - contains the request details in JSON format

        @return result or error returns from
    */
    Json::Value processSubscription(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Initiates the network scanning operation using Protocol Adaptor method

        @return success or error status returns from
        m_pProtocolAdaptor->StartScan() method in the JSON format
    */
    Json::Value startScan(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stops the network scanning operation using Protocol Adaptor method

        @return success or error status returns from
        m_pProtocolAdaptor->StopScan() method in the JSON format
    */
    Json::Value stopScan(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** Connects the requested device in the network

        @param jsonData - contains the request details in JSON format

        @return success or error status returns from
        m_pProtocolAdaptor->Connect() method in the JSON format
    */
    Json::Value connect(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Disconnects the currently connected device using Protocol Adaptor method

        @return success or error status returns from
        m_pProtocolAdaptor->Disconnect() method in the JSON format
    */
    Json::Value disconnect(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** Stores the bus parameters in the persistent file using Protocol Adaptor
        method

        @param jsonData - contains the bus parameters in JSON format

        @return success or error status returns from
        m_pProtocolAdaptor->SetBusParameters() method in the JSON format
    */
    Json::Value setBusParameters(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Provides the bus parameters from persistent file using Protocol Adaptor
        method

        @return result or error returns from
        m_pProtocolAdaptor->GetBusParameters() method in the JSON format
    */
    Json::Value getBusParameters(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** Stores the application configuration settings in the persistent storage.

        @param jsonData - application setting in JSON structure

        @return return  - error/success JSON data
    */
    Json::Value setAppSettings(const Json::Value &jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves application configuration settings from the persistent storage.

        @return - error/success JSON data
    */
    Json::Value getAppSettings(const Json::Value& jsonData = Json::nullValue);

    /*------------------------------------------------------------------------*/
    /** configures field device information

        @param jsonData - field device info

        @return Json result
     */
    Json::Value setFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** retrieves field device information

        @param jsonData - field device info

        @return Json result
     */
    Json::Value getFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Sets Log level for communicator application
        @param jsonData - json request for setting log level
        @return response json string while trying to set log level within
        log4cplus module
    */
    Json::Value setLogLevel(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Gets Current Log level for communicator application
        @return current loglevel set to log4cplus module
    */
    int getLogLevel();

    /*------------------------------------------------------------------------*/
    /** Sets language code to extract language specific strings

        @param jsonData - language code info

        @return Json result
     */
    Json::Value setLangCode(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Gets language code set for CPM

        @return language code for language based DD string extraction
     */
     std::string getLangCode();

     /*------------------------------------------------------------------------*/
     /** Executes Pre Edit Action for Menu

         @return Pre Edit Action status for Menu
     */
    Json::Value execPreEditAction(const Json::Value& jsonData);

     /*------------------------------------------------------------------------*/
     /** Executes Post Edit Action for Menu

          @return Post Edit Action status for Menu
     */
     Json::Value execPostEditAction(const Json::Value& jsonData);
    
    /*------------------------------------------------------------------------*/
    /** Check for command execution state and depending upon this CPM will
        execute the current command
    */
    bool verifyCmdStateAndExecute(const unsigned int commandID);

    /*------------------------------------------------------------------------*/
    /** Provide application configuration parser instance 
     */
    IAppConfigParser* getAppConfigParser();
	
	/*------------------------------------------------------------------------*/
	/** Gets adaptor instance
	*/
	IProtocolAdaptor* getAdaptorInstance();

     /*------------------------------------------------------------------------*/
     /** Waits till the response for CPM request is sent to IC
     */
     void waitForResponse();
	
    /* Explicitly Disabled Methods ===========================================*/
    CommonProtocolManager& operator=(const CommonProtocolManager& rhs) = delete;
    CommonProtocolManager(const CommonProtocolManager& rhs) = delete;

private:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Construct a CommonProtocolManager
    */
    CommonProtocolManager();

    /*------------------------------------------------------------------------*/
    /** Destruct a CommonProtocolManager
    */
    virtual ~CommonProtocolManager();

    /* Private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /** Parses the SetProtocolType Json data

        @param data - contains json data
        @param protocolType - contains protocol type

        @return status of the SetProtocolType function
    */
    PMStatus parseProtocolType(const Json::Value& data, CPMPROTOCOL& protocolType);

    /*------------------------------------------------------------------------*/
    /** Initializes the publisher list
    */
    void initializePublisherList();

    /*------------------------------------------------------------------------*/
    /** Creates Application Configuration Parser Instance
    */
    void createAppConfigParser();

    /*------------------------------------------------------------------------*/
    /** Sets wait state for notification
    */ 
    void setWaitForRespState(bool waitState);

    /* Private Data Members ==================================================*/

    /// Holds the ProtocolAdapter instance.
    IProtocolAdaptor* m_pProtocolAdaptor;

    /// Hold the CommandHandler instance
    ICommandHandler* m_pCommandHandler;

    /// Holds the command response publisher name
    std::string m_commandPublisher;
    
    /// Holds the status of the command
    uint32_t m_commandInProgress;

    /// Holds the command execution lock
    std::mutex m_commandLock;

    /// Holds the command request data
    AsyncCommandInfo m_asyncCommandData;

    /// Function pointer for process item methods
    typedef Json::Value (CommonProtocolManager::*commandProc)(const Json::Value&);

    /// Holds the command map table
    std::map<uint32_t, commandProc> m_commandProcMapTable;

    /// Holds publisher list
    std::vector<string> m_publisherList;

    /// Holds language code for CPM
    std::string m_langCode;

    /// Holds app config parser instance
    IAppConfigParser* m_appConfigParser;

    /// Condition variable for notify proceed operation
    std::condition_variable m_notifyProceed;
    
    /// Boolean state to holds the notify status
    bool m_bWaitForResp;
    
    /// Mutex for to wait for response signal
    std::mutex m_ResponseMutex;

    /// Mutex for to wait for response signal
    // std::mutex m_destroyLock;
};

#endif  // _COMMON_PROTOCOL_MANAGER_H
