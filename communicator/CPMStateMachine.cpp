/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Creates the singleton instance of CPM State Machine and implements CPM methods
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "CPMStateMachine.h"

using namespace std;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    CPM class instance initialization
*/
CPMStateMachine::CPMStateMachine() : 
    m_cpmState(CPMSTATE_PROTOCOL_NOT_SET)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
CPMStateMachine::~CPMStateMachine()
{
}

/*  ----------------------------------------------------------------------------
    Implements and returns the singleton instance
*/
CPMStateMachine* CPMStateMachine::getInstance()
{
    static CPMStateMachine cpmStateMachine;
    return &cpmStateMachine;
}

/*  ----------------------------------------------------------------------------
    Returns current state of the CPM application
*/
unsigned int CPMStateMachine::getCpmState()
{
    return m_cpmState;
}

/*  ----------------------------------------------------------------------------
    Set current state of the CPM application
*/
void CPMStateMachine::setCpmState
(
    unsigned int cpmState
)
{
    m_lock.lock();

    m_cpmState = m_cpmState | cpmState;

    m_lock.unlock();
}

/* ----------------------------------------------------------------------------
   Clear any corresponding state from state machine  
*/
void CPMStateMachine::clearCpmState
(
    unsigned int cpmStates   
)
{
    m_lock.lock();
    
    m_cpmState = (m_cpmState & ~cpmStates);
    
    m_lock.unlock();
}

/* ----------------------------------------------------------------------------
   Clear all state int he CPM application
*/
void CPMStateMachine::resetCpmState()
{
    m_cpmState = CPMSTATE_PROTOCOL_NOT_SET;
}

