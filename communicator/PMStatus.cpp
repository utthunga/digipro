/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://git@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Protocol Manager Error Class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "PMErrorCode.h"
#include "PMStatus.h"
#include "rtn_code.h"
#include <map>
#include <string>

// MASKS Definition for extracting the Error fields
#define HAS_ERROR 		0x70000000
#define ERROR_CODE		0x8000FFFF
#define ERROR_CLASS		0x0FC00000
#define ERROR_SUB_CLASS 0x003F0000

// Definitions for Setting Error
#define ERROR_CLASS_START_BIT	 	22
#define ERROR_SUBCLASS_START_BIT	16
#define SET_ERROR_BIT 0x10000000

/* NON MEMBER VARIABLE DECLARATION AND DEFINITION *****************************/

////////////////////////////////////// NOTE !!!! //////////////////////////////

// DDS already defines the error stings and dds_error_string() can be used to
// get the error string. For all the newly added errors, error strings
// need to be defined.

////////////////////////////////////// NOTE !!!! //////////////////////////////

/** Constructs PMStatus object.

    */
PMStatus::PMStatus():
    m_pmError(0)
{
}

/** Interface for setting the error information

*/
bool PMStatus::setError(int errorCode, int errorClass, int errorSubClass)
{
   bool status = false;
   if (!hasError())
   {
       /// only if error is already not set
       m_pmError |= errorCode;
       m_pmError |= (errorClass << ERROR_CLASS_START_BIT);
       if (errorSubClass)
       {
           m_pmError |= (errorSubClass << ERROR_SUBCLASS_START_BIT);
       }
       m_pmError |= SET_ERROR_BIT;

       /// Set the status flag
       status = true;

       /// Log the error - TODO
   }

   return status;
}

/** Returns if an error exists or not

*/
bool PMStatus::hasError()
{
    return static_cast<bool>(m_pmError & HAS_ERROR);
}

/** Retrieves Error Class information

*/
int PMStatus::getErrorClass()
{
    return (m_pmError & ERROR_CLASS);
}

/** Retrieves Error Sub Class information

*/
int PMStatus::getErrorSubClass()
{
    return (m_pmError & ERROR_SUB_CLASS);
}

/** Retrieves the actual error code information

*/
int PMStatus::getErrorCode()
{
    return (m_pmError & ERROR_CODE);
}

/** Retrieves the Error object (integer) of Protocol Manager

*/
int PMStatus::getPMError()
{
    return m_pmError;
}

/** Retrieves the sub system Error as required while filling the JSON error object

*/
int PMStatus::getSubsystemError()
{
    return m_pmError;
}

/** Retrieves the corresponding Error string for the error object

*/
std::string PMStatus::getErrorString()
{
    std::string errorMessage;
    int code = getErrorCode();

    if (this->hasError())
    {
        switch (getErrorClass())
        {
        case PM_ERR_CL_CPM:
            break;
        case PM_ERR_CL_FF_ADAPTOR:
            break;
        case PM_ERR_CL_HART_ADAPTOR:
            break;
        case PM_ERR_CL_PB_ADAPTOR:
            break;
        case PM_ERR_CL_DDS_CTRL:
            break;
        case PM_ERR_CL_SDC_CTRL:
            break;
        case PM_ERR_CL_PB_CTRL:
            break;
        case PM_ERR_CL_FF_DDS:
            {
                errorMessage.append(static_cast<const char *>(dds_error_string(code)));
            }
            break;
        case PM_ERR_CL_SDC_625:
            break;
        case PM_ERR_CL_PB_DDS:
            break;

        case PM_ERR_CL_FF_CSL:
            break;
        case PM_ERR_CL_HART_CSL:
            break;
        case PM_ERR_CL_PB_CSL:
            break;

        case PM_ERR_CL_FF_STACK:
            break;
        case PM_ERR_CL_HART_STACK:
            break;
        case PM_ERR_CL_PB_STACK:
            break;
        default:
            break;
        }
    }

    return errorMessage;
}

/*------------------------------------------------------------------------*/
/** Clear error code

This API clears the error code
*/
void PMStatus::clear()
{
    m_pmError = 0;
}
