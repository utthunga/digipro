/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************
    Repository URL:    https://bitbucket.org/flukept/starsky.git
    Authored By:       Jennifer Kowalsky, Lisa Leif
    Origin:            Starsky
*/

/** @file
    The main entry point into the Common Protocol Manager.  Starts the
    CPM's D-Bus client.
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/

#include <iostream>
#include <unistd.h>
#include <QCoreApplication>
#include <flk_log4cplus/Log4cplusAdapter.h>
#include "DBusCommandHandler.h"
//#include <QCPMAppManager.h>
#include "CPMUtil.h"
#include "CommonProtocolManager.h"

#define DEVSCANNING_PROSTAR_VERIFICATION
//#define QT_CREATOR_CPM_DEBUG

using namespace std;

#ifdef DEBUG
// Handler for qutting the application
void quitHandler(int signal)
{
    cout << "Quit application with signal: " << signal << endl;
    QCoreApplication::quit();
}

// Function to register UNIX signal with corresponding handler
void registerSignal (int signal, auto handler)
{
    sigset_t blocking_mask;

    sigemptyset(&blocking_mask);
    sigaddset(&blocking_mask, signal);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    sigaction(signal, &sa, nullptr);
}
#endif

///
/// Doxygen comment cpm
///
/*------------------------------------------------------------------------*/
/** Main for the cpm application.

    Main creates the client interface, connects the signal received by the
    D-Bus client interface to the signal receiver class.
*/
int main(
    int argc,
    char* argv[]
)
{
    (void) argc;
    (void) argv;

    int cpmStatus;

    QCoreApplication app(argc, argv);

    /// Initialize the logging mechanism
    log4cplus::Log4cplusAdapter logAdapter;

#ifdef QT_CREATOR_CPM_DEBUG
    // Set the environment variable to set the the starsky settings path
    // This is required to set here to run CPM application in QT Creator
    qputenv("STARSKY_SETTINGS", "");
#endif

    /// Create a new dbus command handler
    ICommandHandler* pCommandHandler = new DBusCommandHandler();

    /// Initialize the dbus command handler instance
    if (nullptr != pCommandHandler)
    {
        pCommandHandler->initialise();
    }

#ifdef DEBUG
    // TODO: Below line can be removed from final product delivery
    // This code is added to catch memory leak from address sanitizer
    // Register SIGINT signal to shutdown the application by clearing all the required memory
    // and show only leak memory
    registerSignal (SIGINT, quitHandler);
#endif

    /// start the qt event loop
    cpmStatus = app.exec();

    /// Initialize the dbus command handler instance
    if (nullptr != pCommandHandler)
    {
        pCommandHandler->shutDown();
        delete pCommandHandler;
        pCommandHandler = nullptr;
    }

    return cpmStatus;
}
