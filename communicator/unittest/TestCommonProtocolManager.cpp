#include "gtest/gtest.h"
#include "CommonProtocolManager.h"
#include "json/json.h"

class TestCommonProtocolManager : public testing::Test
{

public:

    TestCommonProtocolManager( )
    {
        
    }

    ~TestCommonProtocolManager( )
    {
           // cleanup any pending stuff, but no exceptions allowed
    }

    virtual void SetUp( )
    {


    }

     void TearDown( )
     {
             // Code here will be called immediately after each test (right before the destructor).

     }

};

TEST_F(TestCommonProtocolManager, SetProtocolType)
{
    cout << "SetProtocolType";
    std::string resultJson = "";
    std::string reqJson = "";

//    Json::Value root;
//    root["Protocol"] = "FF";

//    Json::StyledWriter writer;
//    reqJson = writer.write(root);
    resultJson = CommonProtocolManager::GetInstance()->setProtocolType("FF");
    EXPECT_EQ("ok", resultJson);

  }

TEST_F(TestCommonProtocolManager, StartScan)
{
    std::string reqJson = "";
    std::string resultJson = "";

    resultJson = CommonProtocolManager::GetInstance()->startScan(reqJson);

    EXPECT_NE("", resultJson);
}

TEST_F(TestCommonProtocolManager, StopScan)
{
    std::string resultJson = "";

    resultJson = CommonProtocolManager::GetInstance()->stopScan();

    EXPECT_NE("", resultJson);
}

