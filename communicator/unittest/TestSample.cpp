#include"gtest/gtest.h"
#include "sample.h"

class TestSample : public ::testing::Test
{
public:
  TestSample()
  {
    // perform initialization
  }

  ~TestSample()
  {
    // Perform cleanup
  }
protected:
  virtual void SetUp()
  {
    // Called just before the test executes
  }

  virtual void TearDown()
  {
    // Called just after test execution
  }
};

TEST_F (TestSample, TestAdditionPositive)
{
  Sample SampleObject;
  EXPECT_EQ(10, SampleObject.Add(5, 5));
  EXPECT_EQ(40, SampleObject.Add(20, 20));
}

TEST_F (TestSample, TestAdditionNegative)
{
  Sample SampleObject;
  EXPECT_EQ(-10, SampleObject.Add(-5,-5));
  EXPECT_EQ(-100, SampleObject.Add(-50, -90));
}

TEST_F (TestSample, TestMultiplyPositive)
{
  Sample SampleObject;
  EXPECT_EQ(25, SampleObject.Multiply(5, 5));
  EXPECT_EQ(625, SampleObject.Multiply (25, 25));
}

TEST_F (TestSample, TestMultiplyNegative)
{
  Sample SampleObject;
  EXPECT_EQ (25, SampleObject.Multiply(-5, -5));
  EXPECT_EQ(-100, SampleObject.Multiply(-10, 10));

}

