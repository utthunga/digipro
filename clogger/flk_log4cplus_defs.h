/******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    Global declaration require for CPM log definitions
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef __FLK_LOG4CPP_DEF_H_
#define __FLK_LOG4CPP_DEF_H_

#define CPMAPP              Cpm
#define CPM_FF_DDSERVICE    Cpm_FF_DDService
#define CPM_HART_SDC625     Cpm_HART_SDC625
#define CPM_PB_IFAK         Cpm_PB_iFak
#define CPM_FF_STACK        Cpm_FF_Stack
#define CPM_HART_STACK      Cpm_HART_Stack
#define CPM_PB_STACK        Cpm_PB_Stack
#define CLR                 Clr


#endif //__FLK_LOG4CPP_DEF_H_
