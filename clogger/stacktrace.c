/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    trace file implementation for HART stack trace logs
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <stdio.h>
#include <stdarg.h>
#include "stacktrace.h"
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

static FILE  *h_TraceFile = NULL;
static unsigned short gProtocolType = 0;

/*===========================================================================*/
bool createLogFile(unsigned short protocolType, const char* traceFileName, bool isTraceFile)
{
    gProtocolType = protocolType;
    if (isTraceFile)
    {
        if (NULL == h_TraceFile)
        {
            h_TraceFile = fopen(traceFileName, "w");
            if (h_TraceFile == NULL)
            {
                return false;
            }
        }
    }

    return true;
}

/*===========================================================================*/
void closeLogFile(void)
{
    if (h_TraceFile != NULL)
    {
        fclose(h_TraceFile);
        h_TraceFile = NULL;
    }
}

/*===========================================================================*/
void dbgPrint (const char* pString, ...)
{
  char      Buffer [512];
  va_list   Arguments;

  va_start (Arguments, pString);

  if (h_TraceFile != NULL)
  {
     vfprintf (h_TraceFile, pString, Arguments);
     fflush(h_TraceFile);
  }
  
  // Trace File is not created, redirecting trace messages to Log4Cpp
  else
  {
      if (FF_PROTOCOL == gProtocolType)
      {
        LOGC_TRACE(CPM_FF_STACK, pString, Arguments);
      }
      else if (HART_PROTOCOL == gProtocolType)
      {
        LOGC_TRACE(CPM_HART_STACK, pString, Arguments);
      }
  }
  va_end (Arguments);

  return;
}

