/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    
    Contains implementation of C API for log4cplus module
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"

#include <vector>
#include <log4cplus/logger.h>
#include <log4cplus/fileappender.h>
#include <log4cplus/consoleappender.h>
#include <log4cplus/layout.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/pointer.h>
#include <log4cplus/helpers/property.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/helpers/snprintf.h>

#include <PMErrorCode.h>
#include <PMStatus.h>
#include <iostream>
#include <string>
using namespace std;

/*
 * @brief Set log level within log4cplus module
 *
 * This API helps to set log level within log4cplus module for
 * communicator application
 *
 * @param nLogLvel, log level internal to communicator
 *
 * @return -1, for failure cae other wise 0 will be returned
 */
//extern PMStatus setLog4cplusLogLevel(int nLogLevel);


/*
 * Logs debug level messages using log4cplus library
 */
void log_debug (const char* name, loglevel_t logLevel, const char* fileName,
				int lineNumber, const char* functionName, const log4cplus_char_t* msgfmt, ...)
{
	int retval = -1;

    using namespace log4cplus;
	using namespace log4cplus::helpers;

	Logger logger = Logger::getInstance(LOG4CPLUS_TEXT(name));

	if (logger.isEnabledFor(logLevel) )
	{
		const tchar * msg = 0;
		snprintf_buf buf;
		std::va_list ap;
		int retval = -1;

        do
        {
            va_start(ap, msgfmt);
            retval = buf.print_va_list(msg, msgfmt, ap);
            va_end(ap);
        }
        while (retval == -1);
        logger.log(logLevel, msg, fileName, lineNumber, functionName);
	}
}

/*
 * @brief Set log level within log4cplus module
 *
 * This API helps to set log level within log4cplus module for
 * communicator application
 *
 * @param nLogLvel, log level internal to communicator
 *
 * @return setLogLevelStatus, status of setting log level operation
 *
 */
PMStatus setLog4cplusLogLevel(int nLogLevel)
{
    PMStatus setLogLevelStatus;

	if ((nLogLevel < LOG_LEVEL_OFF) || (nLogLevel > MAX_LOG_LEVEL))
    {
        setLogLevelStatus.setError(GENERIC_PM_ERR_SET_LL_INVALID_LL,
                                   PM_ERR_CL_GENERIC,
                                   GENERIC_ERR_SC_LL);
        return setLogLevelStatus;
    }
    using namespace log4cplus;
    Logger logger = Logger::getRoot();
	
	switch (nLogLevel)
	{
		case LOG_LEVEL_OFF:
			{
				logger.setLogLevel(OFF_LOG_LEVEL);
				break;
			}
		case LOG_LEVEL_FATAL:
			{
				logger.setLogLevel(FATAL_LOG_LEVEL);
				break;
			}
		case LOG_LEVEL_ERROR:
			{
				logger.setLogLevel(ERROR_LOG_LEVEL);
				break;
			}
		case LOG_LEVEL_WARN:
			{
				logger.setLogLevel(WARN_LOG_LEVEL);
				break;
			}	
		case LOG_LEVEL_INFO:
			{
				logger.setLogLevel(INFO_LOG_LEVEL);
				break;
			}	
		case LOG_LEVEL_TRACE:
			{
				logger.setLogLevel(TRACE_LOG_LEVEL);
				break;
			}
		case LOG_LEVEL_DEBUG:
			{
				logger.setLogLevel(DEBUG_LOG_LEVEL);
				break;
			}	
		default:
			{
				logger.setLogLevel(TRACE_LOG_LEVEL);
				break;
			}
	}

    return setLogLevelStatus;
}

/*
 * @brief Gets log level set to the Log4Cplus module
 *
 * This API helps to get the current log level set to Log4Cplus module
 *
 * @return current log level
 *
 */
int getLog4cplusLogLevel()
{
    int currLogLevel =  LOG_LEVEL_TRACE;

    using namespace log4cplus;
    Logger logger = Logger::getRoot();
	
    switch (logger.getLogLevel())
	{
		case OFF_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_OFF;
				break;
			}
		case FATAL_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_FATAL;
				break;
			}
		case ERROR_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_ERROR;
                break;
			}
		case WARN_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_WARN;
				break;
			}	
		case INFO_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_INFO;
				break;
			}	
		case TRACE_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_TRACE;
				break;
			}
		case DEBUG_LOG_LEVEL:
			{
				currLogLevel = LOG_LEVEL_DEBUG;
				break;
			}	
		default:
			{
				currLogLevel = LOG_LEVEL_TRACE;
				break;
			}
	}
    return currLogLevel;
}

