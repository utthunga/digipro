/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    
    Contains enum and macro definition for clogger
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#ifndef GLOBAL_LOGGER_H
#define GLOBAL_LOGGER_H

#include <log4cplus/clogger.h>

#define STRINGIZE(x) # x

/// enum 
/// Declaration for log level that is used within protocol manager
enum LOG_LEVEL
{
    LOG_LEVEL_OFF = 0, 	/// Disable log messages
    LOG_LEVEL_FATAL,	/// Enable fatal log messages	
    LOG_LEVEL_ERROR,	/// Enable error log messages	
    LOG_LEVEL_WARN,		/// Enable warning log messages
    LOG_LEVEL_INFO,		/// Enable info log messages
    LOG_LEVEL_TRACE,	/// Enable trace log messages
    LOG_LEVEL_DEBUG	    /// Enable debug log messages
};

/// Set macro for maximum log level
#define MAX_LOG_LEVEL 7

#ifdef __cplusplus
extern "C" {
#endif

/*
 * @brief Logs debug level messages
 *
 * This API helps to log debug level messages within communicator application. This
 * is usable only in debug mode
 *
 * @param   name, logger name set within log4cplus for fetching correspongin instance
 * @param   logLevel, log level enabled by application
 * @param   fileName, file name from where this function been triggered
 * @param   lineNumber, line number wthin the file where this function has been triggered
 * @param   functionName, function name where this log_debug has ben invoked
 * @param   msgfmt, messages logged will be formatted depending upon input
 *
 */
void log_debug (const char* loggerName, loglevel_t logLevel, const char* fileName, int lineNumber, const char* functionName, 
				const log4cplus_char_t* msg, ...);

/*
 * @brief Set log level within log4cplus module
 *
 * This API helps to set log level within log4cplus module for
 * communicator application
 *
 * @param nLogLvel, log level internal to communicator
 *
 * @return -1, for failure cae other wise 0 will be returned
 */
 //PMStatus setLog4cplusLogLevel(int nLogLevel);

/*
 * @brief Gets log level within log4cplus module
 *
 * This API helps to get log level within log4cplus module for
 * communicator application
 *
 * @return current log level set in log4cplus module 
 */
 int getLog4cplusLogLevel();


/* Designates very severe error events that will presumably lead 
 * the application to abort.
 */
#define LOGC_FATAL(name,msg, ...) log4cplus_logger_log (   \
        STRINGIZE(name),           \
        L4CP_FATAL_LOG_LEVEL,      \
        msg,                       \
        ##__VA_ARGS__              \
        );

/* Designates error events that might still allow the application
 * to continue running.
 */
#define LOGC_ERROR(name,msg, ...) log4cplus_logger_log (   \
        STRINGIZE(name),           \
        L4CP_ERROR_LOG_LEVEL,      \
        msg,                       \
        ##__VA_ARGS__              \
        );

/// Designates potentially harmful situations.
#define LOGC_WARN(name,msg, ...) log4cplus_logger_log (   \
        STRINGIZE(name),           \
        L4CP_WARN_LOG_LEVEL,       \
        msg,                       \
        ##__VA_ARGS__              \
        );

/* Designates informational messages  that highlight the progress
 * of the application at coarse-grained  level.
 */
#define LOGC_INFO(name,msg, ...) log4cplus_logger_log (   \
        STRINGIZE(name),           \
        L4CP_INFO_LOG_LEVEL,       \
        msg,                       \
        ##__VA_ARGS__              \
        );

/// Typically used to trace entry and exit of methods.
#define LOGC_TRACE(name,msg, ...) log4cplus_logger_log (   \
        STRINGIZE(name),           \
        L4CP_TRACE_LOG_LEVEL,      \
        msg,                       \
        ##__VA_ARGS__              \
        );

/* Designates fine-grained informational events that are most
 * useful to debug an application.
 */
#define LOGC_DEBUG(name,msg, ...) log_debug (    \
	    STRINGIZE(name),	    		\
		L4CP_DEBUG_LOG_LEVEL,           \
		__FILE__,						\
		__LINE__,						\
		__FUNCTION__,					\
	    msg,                            \
		##__VA_ARGS__                   \
		);

#ifdef __cplusplus
}
#endif

#endif /* GLOBAL_LOGGER_H */

