/*******************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    trace file definition for HART stack trace logs
*/

#ifndef TRACE_H
#define TRACE_H

#include <stdbool.h>

#define FF_PROTOCOL 1
#define HART_PROTOCOL 2

#define ERR_PRINT  dbgPrint

#ifdef _DEBUG

#ifndef DBG_PRINT
 #define DBG_PRINT(_x_)  dbgPrint _x_
#endif

 #ifndef ASSERT
   #define ASSERT(c) {if(!(c)) dbgPrint("AssertionFailure File: %s Line: %s\n",__FILE__,__LINE__);}
 #endif

  #ifndef VERIFY
    #define VERIFY(c) ASSERT(c)
  #endif

#else

  #ifndef DBG_PRINT
    #define DBG_PRINT(_x_)
  #endif

  #ifndef ASSERT
    #define ASSERT(c) ((VOID)0)
  #endif

  #ifndef VERIFY
    #define VERIFY(c) c
  #endif

#endif

#ifdef __cplusplus
extern "C" {
#endif

bool createLogFile(unsigned short protocolType, const char* traceFileName, bool isTraceFile);
void closeLogFile(void);
void dbgPrint (const char* pString, ...);

#ifdef __cplusplus
}
#endif

#endif
