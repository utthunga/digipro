/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Json Cpp utility class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

#include <string>


class CPMUtil
{
public:
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /**  Initialized the required modules for CPM
    */
    static void initCPM();

    /*------------------------------------------------------------------------*/
    /** Checks whether number is power of 2
    *
    *   @param num - to check whether number is power of 2
    *
    *   @return true if number is power of 2
    */
    static bool isPow2(unsigned int num);
    
    /*------------------------------------------------------------------------*/
    /** Gets the bit position of set bit
    *
    *   @param bitIndex - number for which the bit position needs to be fetched
    *
    *   @return bit position of set bit
    */
    static unsigned int getBitPos(unsigned long bitIndex);

    /*------------------------------------------------------------------------*/
    /** Utility function to create directory
    */
    static bool createDirectory(std::string directory);

    /*------------------------------------------------------------------------*/
    /** Utility function to clear directory content
    */
    static bool clearDirectory(std::string directory);

    /*------------------------------------------------------------------------*/
    /** Utility function to check directory exists or not
    */
    static bool isDirExists(std::string directory);
	
    /*------------------------------------------------------------------------*/
    /** Utility function to convert UTF8 to ISO-Latin-1 string
    */
    static bool convertUtf8ToIsolatin1(char **utf8Str, size_t *utf8StrLen, char **iso8859Str, 
                                    size_t *iso8859StrLen);
private:
    /* Private Member Functions ===============================================*/
 
#ifdef SET_CURRENT_DIR   
    /*------------------------------------------------------------------------*/
    /**  Sets the current working directory for the application
    */
    static void setCurrentWorkingDir();
#endif
};
