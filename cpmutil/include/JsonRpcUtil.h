/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Json Cpp utility class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <json/json.h>

/* CLASS DECLARATIONS *********************************************************/
/** class description of Json Cpp Utilility

    This class will be responsible for preparing jsoncpp error and result 
    value
*/
class JsonRpcUtil
{
public:
    /* Public Member Functions ===============================================*/
    
    /*------------------------------------------------------------------------*/
    /**  Prepares the Json RPC error value
            
        @param errorCode, holds value of the "code" attribuet in JSON RPC
        @param errorMsg, holds error message of the "message" attribute 
                          in JSON RPC
        @param data, holds data part of the JSON RPC error 
        @return json value   
    */
    static Json::Value setErrorObject(int errorCode, const char* errorMsg);

    /*------------------------------------------------------------------------*/
    /**  Prepares the Json RPC error value
            
        @param responseMsg, Json response

        @return json value   
    */
    static Json::Value getResponseObject(Json::Value responseMsg);
};
