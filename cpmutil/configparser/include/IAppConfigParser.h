/*****************************************************************************
    Copyright (c) 2018 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Interface class for Application configuration parser
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

#include <string>
#include <CommonDef.h>


/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Application Configuration Parser interface class
class IAppConfigParser
{
    public:
        /* Public Types ==========================================================+*/

        /* Construction and Destruction ==========================================+*/
        /*-------------------------------------------------------------------------*/
        /** Destruct a IAppConfigParser.
         *
         */
        virtual ~IAppConfigParser()
        {
        }

        /* Public Member Functions ===============================================*/

        /*------------------------------------------------------------------------*/
        /** Interface function for loading and parsing configuration file
         * 
         *  @return status of parsing the configuration file
         */
        virtual bool loadAndParseAppConfig(CPMPROTOCOL& currentProtocol) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting Device Description (DD) path
         * 
         *  @return path to DD directory
         */
        virtual std::string getDeviceDescriptionPath() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting device port 
         * 
         */
        virtual std::string getDevicePort() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting modem name
         * 
         */
        virtual std::string getModemName() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting subscription rate
         *
         *  @return subscription rate for parameter (provided in secs)
         */
        virtual unsigned int getSubscriptionRate() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting image path
         *
         *  @return path to the image directory
         */
        virtual std::string getImagePath() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for getting home path
         *
         *  @return path to the home directory set to cpm application
         */
        virtual std::string getSettingsPath() = 0;

	    /*------------------------------------------------------------------------*/
        /** Interface function for getting host name
         *
         *  @return host name to the cpm application
         */
        virtual std::string getHostName() = 0;
        
        /*  ----------------------------------------------------------------------*/
        /** Gets stack trace file name
         *  
         *  @return stack trace file name
         */
        virtual std::string getTraceFileName() = 0;
        
        /*------------------------------------------------------------------------*/
        /** Interface function for getting trace log folder path
         *
         *  @return path to the trace log directory
         */
        virtual std::string getTraceLogPath() = 0;
};
