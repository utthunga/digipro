/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    YAML Applicaton Configuration Parser class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

#include <IAppConfigParser.h>

#define DD_PREFIX_PATH_ATTR         "ddPrefixPath"
#define DD_IMAGE_PATH_ATTR          "ddImagePath"

/// FF app information attributes
#define FF_APP_INFO_ATTR            "ffAppInfo"
#define FF_DD_PATH_ATTR             "ffDDPath"
#define FF_PORT_ATTR                "ffPort"
#define FF_SUBSCRIPTION_RATE_ATTR   "ffSubscriptionRate"

/// HART app information attributes
#define HART_APP_INFO_ATTR            "hartAppInfo"
#define HART_DD_PATH_ATTR             "hartDDPath"
#define HART_PORT_ATTR                "hartPort"
#define HART_MODEM                    "hartModem"
#define HART_SUBSCRIPTION_RATE_ATTR   "hartSubscriptionRate"

/// PB app information attributes
#define PB_APP_INFO_ATTR            "PBAppInfo"
#define PB_DD_PATH_ATTR             "PBDDPath"
#define PB_PORT_ATTR                "PBPort"
#define PB_SUBSCRIPTION_RATE_ATTR   "SubscriptionRate"

/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Command Handler interface class
class YAMLAppConfigParser : public IAppConfigParser
{
    public:
        /* Public Types ==========================================================+*/
        YAMLAppConfigParser();
        ~YAMLAppConfigParser();

        /* Public Member Functions ===============================================*/
        /*------------------------------------------------------------------------*/
        /** Interface function for loading and parsing configuration file
         * 
         *  @return status of parsing the configuration file
         */
        bool loadAndParseAppConfig(CPMPROTOCOL& currentProtocol);

        /*------------------------------------------------------------------------*/
        /** Interface function for getting Device Description (DD) path
         * 
         *  @return path to DD directory
         */
        std::string getDeviceDescriptionPath();

        /*------------------------------------------------------------------------*/
        /** Interface function for getting device port 
         * 
         */
        std::string getDevicePort();

        /*------------------------------------------------------------------------*/
        /** Interface function for getting modem name
         * 
         */
        std::string getModemName();

        /*------------------------------------------------------------------------*/
        /** Interface function for getting subscription rate
         *
         *  @return subscription rate for parameter (provided in secs)
         */
        unsigned int getSubscriptionRate();

        /*------------------------------------------------------------------------*/
        /** Interface function for getting image path
         *
         *  @return path to the image directory
         */
        std::string getImagePath();

        /*------------------------------------------------------------------------*/
        /** Interface function for getting Settings Path
         *
         *  @return path to the settings  directory
         */
        std::string getSettingsPath();

	    /*------------------------------------------------------------------------*/
        /** Interface function for getting Host name
         * 
         */
        std::string getHostName();

        /*  ----------------------------------------------------------------------*/
        /** Gets stack trace file name
         *  
         *  @return stack trace file name
        */
        std::string getTraceFileName();
        
        /*------------------------------------------------------------------------*/
        /** Interface function for getting trace log folder path
         *
         *  @return path to the trace log directory
         */
        std::string getTraceLogPath();


    private:

        /*------------------------------------------------------------------------*/
        /* Parses protocol specific attributes specified in the config file
         *
         * return status of loading the configuration file
         */
        bool parseYAMLConfig(CPMPROTOCOL& currentProtocol);

    private:
        // Holds dd prefix path for loading corresponding dd files
        std::string m_ddPrefixPath;

        // Holds DD path for the corresponding protocol
        std::string m_ddPath;

        // Holds device port for the corresponding protocol
        std::string m_devicePort;

        // Holds the subscription rate for the corresponding protocol
        unsigned int m_subscriptionRate;

        // Holds image generation path
        std::string m_imagePath;

        // Holds image generation path
        std::string m_modemName;

        // Holds the settings path for CPM application
        std::string m_settingsPath;
	
	    // Holds the Host Name to be displayed on the network (for other hosts)
	    std::string m_hostName;

        // Holds the trace file name
        std::string m_traceFileName;

        // Holds the trace log folder path
        std::string m_traceLogPath;
};
