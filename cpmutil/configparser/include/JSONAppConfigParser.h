/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Command Handler Interface class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

#include <IAppConfigParser.h>
#include <json/json.h>

#define CPM_SETTINGS_FILE           "STARSKY_SETTINGS" // Specifies the path to cpmappinfo.json file

#define DD_PREFIX_PATH_ATTR         "DDPrefixPath"
#define DD_PREFIX_PATH              "DD_PREFIX_PATH"
#define DD_IMAGE_PATH_ATTR          "DDImagePath"

/// FF app information attributes
#define FF_APP_INFO_ATTR            "FFAppInfo"
#define FF_DD_PATH_ATTR             "FFDDPath"
#define FF_PORT_ATTR                "FFPort"
#define FF_SUBSCRIPTION_RATE_ATTR   "SubscriptionRate"

/// HART app information attributes
#define HART_APP_INFO_ATTR            "HARTAppInfo"
#define HART_DD_PATH_ATTR             "HARTDDPath"
#define HART_PORT_ATTR                "HARTPort"
#define HART_MODEM                    "HARTModem"
#define HART_SUBSCRIPTION_RATE_ATTR   "SubscriptionRate"

/// PB app information attributes
#define PB_APP_INFO_ATTR            "PBAppInfo"
#define PB_DD_PATH_ATTR             "PBDDPath"
#define PB_PORT_ATTR                "PBPort"
#define PB_SUBSCRIPTION_RATE_ATTR   "SubscriptionRate"

/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Command Handler interface class
class JSONAppConfigParser : public IAppConfigParser
{
    public:
        /* Public Types ==========================================================+*/
        JSONAppConfigParser();
        ~JSONAppConfigParser();

        /* Public Member Functions ===============================================*/
        /*------------------------------------------------------------------------*/
        /** Method for loading and parsing configuration file
         * 
         *  @return status of parsing the configuration file
         */
        bool loadAndParseAppConfig(PROTOCOL& currentProtocol);

        /*------------------------------------------------------------------------*/
        /** Method for getting Device Description (DD) path
         * 
         *  @return path to DD directory
         */
        std::string getDeviceDescriptionPath();

        /*------------------------------------------------------------------------*/
        /** Method for getting device port 
         * 
         */
        std::string getDevicePort();

        /*------------------------------------------------------------------------*/
        /** Method for getting modem name
         * 
         */
        std::string getModemName();

        /*------------------------------------------------------------------------*/
        /** Method for getting subscription rate
         *
         *  @return subscription rate for parameter (provided in secs)
         */
        unsigned int getSubscriptionRate();

        /*------------------------------------------------------------------------*/
        /** Method for getting image path
         *
         *  @return path to the image directory
         */
        std::string getImagePath();

        /*------------------------------------------------------------------------*/
        /** Method for getting home path
         *
         *  @return path to fetch home path
         */
        std::string getSettingsPath();

    private:
        /*------------------------------------------------------------------------*/
        /** Method for fetch and set settings file 
         *
         */
        void setSettingsFile();

        /*------------------------------------------------------------------------*/
        /* Parses protocol specific attributes specified in the config file
         *
         * return status of loading the configuration file
         */
        bool parseJsonConfig(PROTOCOL& currentProtocol, Json::Value& configObj);

    private:
        // Holds dd prefix path for loading corresponding dd files
        std::string m_ddPrefixPath;

        // Holds DD path for the corresponding protocol
        std::string m_ddPath;

        // Holds device port for the corresponding protocol
        std::string m_devicePort;

        // Holds the subscription rate for the corresponding protocol
        unsigned int m_subscriptionRate;

        // Holds image generation path
        std::string m_imagePath;

        // Holds image generation path
        std::string m_modemName;

        // Holds setting file
        std::string m_settingsFile;

        // Holds read / write settings path
        std::string m_settingsPath;
};
