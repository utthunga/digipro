/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Provides YAML class interface for app config parser
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <YAMLAppConfigParser.h>
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "config/StarskySettings.h"

 /* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Abstract constructor for app config parser
*/
YAMLAppConfigParser::YAMLAppConfigParser():
    m_ddPrefixPath(""),
    m_ddPath(""),
    m_devicePort(""),
    m_subscriptionRate(0),
    m_imagePath(""),
    m_modemName(""),
    m_hostName(""),
    m_traceFileName("")
{   
}


/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
YAMLAppConfigParser::~YAMLAppConfigParser()
{
}

/*  ----------------------------------------------------------------------------
    Load and parse app config json
*/
bool YAMLAppConfigParser::loadAndParseAppConfig(CPMPROTOCOL& currentProtocol)
{
    bool bStatus = false;

    config::StarskySettings cpmSettings;

    // Path where the CPM process can save files
    m_settingsPath = cpmSettings.createComponentHomePath("cpm");

    if (true == m_settingsPath.empty())
        return bStatus;

    m_ddPrefixPath = cpmSettings.findNode("CPM_SETTINGS/ddPrefixPath").as<std::string>();

    if (true == m_ddPrefixPath.empty())
        return false;

    // Save the image file in the settings path appended by images
    m_imagePath = m_settingsPath + '/' + std::string("images");
    
    // Save the image file in the settings path appended by images
    m_traceLogPath = m_settingsPath + '/' + std::string("tracelogs");

    bStatus = parseYAMLConfig(currentProtocol);

    return bStatus;
}

/*  ----------------------------------------------------------------------------
    Load and parse app config json
*/
bool YAMLAppConfigParser::parseYAMLConfig(CPMPROTOCOL& currentProtocol)
{
    bool bStatus = true;
    
    config::StarskySettings cpmSettings;

    switch (currentProtocol)
    {
        case CPMPROTOCOL::CPMPROTOCOL_FF:
        {
            // Default subscription rate for FF protocol
            m_subscriptionRate = 5;
        
            // ffDDPath: <Specifies the FF Device Description DD directory path>    
            std::string ddPath = cpmSettings.findNode("CPM_SETTINGS/ffAppInfo/ffDDPath").as<std::string>();
            // ffPort: <Specifies device file that need to be used for FF communication>
            std::string devicePort = cpmSettings.findNode("CPM_SETTINGS/ffAppInfo/ffPort").as<std::string>();
            // ffSubscriptionRate: <Specifies subscription rate for FF specific parameters>.
            // If this attribute is not specified, default value of 5 (in secs) will be considered
            unsigned int subscriptionRate = cpmSettings.findNode("CPM_SETTINGS/ffAppInfo/ffSubscriptionRate").as<unsigned int>();
        
            // If FF DD path or the device file is not present, stop proceeding further
            if ((true == ddPath.empty()) ||
                (true == devicePort.empty()))
                return false;

            // Append dd prefix path with ff path to get complete dd path
            m_ddPath = m_ddPrefixPath + '/' + ddPath;

            m_devicePort = devicePort;
                 
            m_subscriptionRate = subscriptionRate;
	
	        // Get the Host name to be displayed on the FF network
	        m_hostName = cpmSettings.findNode("CPM_SETTINGS/ffAppInfo/ffHostName").as<std::string>();

            // Get trace file name
            m_traceFileName = cpmSettings.findNode("CPM_SETTINGS/ffAppInfo/ffStackTraceFile").as<std::string>();
        }
        break;

        case CPMPROTOCOL::CPMPROTOCOL_HART:
        {
            // Default subscription rate for HART protocol
            m_subscriptionRate = 5;
        
            // hartDDPath: <Specifies the HART Device Description DD directory path>    
            std::string ddPath = cpmSettings.findNode("CPM_SETTINGS/hartAppInfo/hartDDPath").as<std::string>();
            // hartPort: <Specifies device file that need to be used for HART communication>
            std::string devicePort = cpmSettings.findNode("CPM_SETTINGS/hartAppInfo/hartPort").as<std::string>();
            std::string modemName = cpmSettings.findNode("CPM_SETTINGS/hartAppInfo/hartModem").as<std::string>();
            // hartSubscriptionRate: <Specifies subscription rate for HART specific parameters>.
            // If this attribute is not specified, default value of 5 (in secs) will be considered
            unsigned int subscriptionRate = cpmSettings.findNode("CPM_SETTINGS/hartAppInfo/hartSubscriptionRate").as<unsigned int>();

            // If HART DD path or the device file is not present, stop proceeding further
            if ((true == ddPath.empty()) ||
                (true == devicePort.empty()))
                return false;

            m_ddPath = m_ddPrefixPath + '/' + ddPath;

            m_devicePort = devicePort;

            m_modemName = modemName;
                 
            m_subscriptionRate = subscriptionRate;
            
            // Get trace file name
            m_traceFileName = cpmSettings.findNode("CPM_SETTINGS/hartAppInfo/hartStackTraceFile").as<std::string>();
        }
        break;

        case CPMPROTOCOL::CPMPROTOCOL_PROFIBUS:
        {
            // Default subscription rate for PB protocol
            m_subscriptionRate = 5;

            // pbDDPath: <Specifies the PB Device Description DD directory path>
            std::string ddPath = cpmSettings.findNode("CPM_SETTINGS/pbAppInfo/pbDDPath").as<std::string>();
            // pbPort: <Specifies device file that need to be used for PB communication>
            std::string devicePort = cpmSettings.findNode("CPM_SETTINGS/pbAppInfo/pbPort").as<std::string>();
            // pbSubscriptionRate: <Specifies subscription rate for PB specific parameters>.
            // If this attribute is not specified, default value of 5 (in secs) will be considered
            unsigned int subscriptionRate = cpmSettings.findNode("CPM_SETTINGS/pbAppInfo/pbSubscriptionRate").as<unsigned int>();

            // If PB DD path or the device file is not present, stop proceeding further
            if ((true == ddPath.empty()) ||
                (true == devicePort.empty()))
                return false;

            // Append dd prefix path with pb path to get complete dd path
            m_ddPath = m_ddPrefixPath + '/' + ddPath;

            m_devicePort = devicePort;

            m_subscriptionRate = subscriptionRate;

            // Get the Host name to be displayed on the PB network
            m_hostName = cpmSettings.findNode("CPM_SETTINGS/pbAppInfo/pbHostName").as<std::string>();

            // Get trace file name
            m_traceFileName = cpmSettings.findNode("CPM_SETTINGS/pbAppInfo/pbStackTraceFile").as<std::string>();
        }
        break;

        case CPMPROTOCOL::CPMPROTOCOL_NONE:
        default:
            bStatus = false;
        break;
    }

    return bStatus;
}

/*  ----------------------------------------------------------------------------
    Get device description path
*/
std::string YAMLAppConfigParser::getDeviceDescriptionPath()
{
    return m_ddPath;
}

/*  ----------------------------------------------------------------------------
    Get device port
*/
std::string YAMLAppConfigParser::getDevicePort()
{
    return m_devicePort;
}

/*  ----------------------------------------------------------------------------
    Get Subscription Rate
*/
unsigned int YAMLAppConfigParser::getSubscriptionRate()
{
    return m_subscriptionRate;
}

/*  ----------------------------------------------------------------------------
    Get image path
*/
std::string YAMLAppConfigParser::getImagePath()
{
    return m_imagePath;
}

/*  ----------------------------------------------------------------------------
    Get modem name
*/
std::string YAMLAppConfigParser::getModemName()
{
    return m_modemName;
}

/*  ----------------------------------------------------------------------------
    Get settings path
*/
std::string YAMLAppConfigParser::getSettingsPath()
{
    return m_settingsPath;
}

/*  ----------------------------------------------------------------------------
    Get Host Name
*/
std::string YAMLAppConfigParser::getHostName()
{
    return m_hostName;
}

/*  ----------------------------------------------------------------------------
    Get stack trace file name
*/
std::string YAMLAppConfigParser::getTraceFileName()
{
    return m_traceFileName;
}

/*  ----------------------------------------------------------------------------
    Get trace log folder path
*/
std::string YAMLAppConfigParser::getTraceLogPath()
{
    return m_traceLogPath;
}

