#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    include the source files ###############################################
#############################################################################
set (UTIL_SRC ${UTIL_SRC}
# Not Building JSON Config parser as it will be eventually removed
              #${CMAKE_CURRENT_SOURCE_DIR}/JSONAppConfigParser.cpp
              ${CMAKE_CURRENT_SOURCE_DIR}/YAMLAppConfigParser.cpp
              PARENT_SCOPE)

#############################################################################
#    include the source files ###############################################
#############################################################################
set (UTIL_HDR ${UTIL_SRC}
              ${CMAKE_CURRENT_SOURCE_DIR}/include/IAppConnfigParser.h
# Not Building JSON Config parser as it will be eventually removed
              #${CMAKE_CURRENT_SOURCE_DIR}/include/JSONAppConfigParser.h
              ${CMAKE_CURRENT_SOURCE_DIR}/include/YAMLAppConfigParser.h
              PARENT_SCOPE)
