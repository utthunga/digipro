/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Provides JSON class interface for app config parser
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <JSONAppConfigParser.h>
#include "flk_log4cplus/Log4cplusAdapter.h"
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>

#define MAX_PATH_LEN    1024

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Abstract constructor for app config parser
*/
JSONAppConfigParser::JSONAppConfigParser():
    m_ddPrefixPath(""),
    m_ddPath(""),
    m_devicePort(""),
    m_subscriptionRate(0),
    m_imagePath(""),
    m_modemName("")
{
    // Set home path for the cpm application
    setSettingsFile();
}


/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
JSONAppConfigParser::~JSONAppConfigParser()
{
}

/*  ----------------------------------------------------------------------------
    Set the settings file path
*/
void JSONAppConfigParser::setSettingsFile()
{
    char* envPrefixPath = getenv (CPM_SETTINGS_FILE);

    if (nullptr != envPrefixPath)
    {
        m_settingsFile = envPrefixPath;
    }

    char szExecPath[MAX_PATH_LEN] = {0};
    char szExecDirPath[MAX_PATH_LEN] = {0};

    /// Get the executable complete path including the executable name in it
    sprintf(szExecPath, "/proc/%d/exe", getpid());
    int nBytesRead = readlink(szExecPath, szExecDirPath, MAX_PATH_LEN);

    if (nBytesRead > 0)
    {   
        szExecDirPath[nBytesRead] = '\0';
    }

    /// Extract only the directory path from it 
    char* lastDelimPtr = strrchr(szExecDirPath, '/');
    *(lastDelimPtr + 1) = '\0';

    m_settingsPath = szExecDirPath;
}


/*  ----------------------------------------------------------------------------
    Load and parse app config json
*/
bool JSONAppConfigParser::loadAndParseAppConfig(PROTOCOL& currentProtocol)
{
    bool bConfigStatus = false;
    
    std::string readAppInfoData;
    std::ifstream appInfoFile;

    Json::Value jsonData;
    Json::Reader reader;
    
    appInfoFile.open (m_settingsFile, std::ios::in);
    
    if (appInfoFile.is_open())      
    {
        std::stringstream buff;     
        buff << appInfoFile.rdbuf();
        readAppInfoData = buff.str(); 
    
        bool parsingString = reader.parse(readAppInfoData, jsonData);
        if (!parsingString)
        {
            LOGF_ERROR(cpm,"Failed to parse application configuration file");
        }
        else
        {
            // Parse common attributes here (example: DD prefix path, Image path)
            if (false == jsonData.isNull())
            {
                // Extract the dd prefix path
                if (jsonData.isMember(DD_PREFIX_PATH_ATTR))
                {
                    m_ddPrefixPath = jsonData[DD_PREFIX_PATH_ATTR].asString();
    
                    // If prefix path is not set, try with environment variable
                    // DD_PREFIX_PATH
                    if (m_ddPrefixPath.length() <= 0)
                    {
                        char* envPrefixPath = getenv (DD_PREFIX_PATH);
                        if (nullptr != envPrefixPath)
                        {
                            m_ddPrefixPath = envPrefixPath;
                        }
                        else 
                        {
                            appInfoFile.close();
                            LOGF_ERROR(cpm, "get dd path from env variable failed");
                            return bConfigStatus;
                        }
                    }
                }

                // Set /tmp as the default directory for image generation
                m_imagePath = m_settingsPath + '/' + std::string("images");

                // Parse the json config data
                bConfigStatus = parseJsonConfig(currentProtocol, jsonData);
            }
        }

        appInfoFile.close();
    }
    else
    {
        LOGF_ERROR(cpm, "Failed to open application configuration file");
    }

    return bConfigStatus;
}

/*  ----------------------------------------------------------------------------
    Load and parse app config json
*/
bool JSONAppConfigParser::parseJsonConfig(PROTOCOL& currentProtocol, Json::Value& configObj)
{
    bool bConfigStatus = false;

    if (false == configObj.isNull())
    {
        Json::Value protoObj;

        switch (currentProtocol)
        {
            case PROTOCOL::PROTOCOL_FF:
            {
                // Default subscription rate for FF protocol
                m_subscriptionRate = 5;

                if (configObj.isMember(FF_APP_INFO_ATTR))
                {
                    protoObj = configObj[FF_APP_INFO_ATTR];

                    if ((false == protoObj.isMember(FF_DD_PATH_ATTR)) ||
                        (false == protoObj.isMember(FF_PORT_ATTR))) 
                    {
                        return bConfigStatus;
                    }

                    // Get FF dd path
                    m_ddPath = (m_ddPrefixPath + protoObj[FF_DD_PATH_ATTR].asString());

                    // Get FF device port
                    m_devicePort = protoObj[FF_PORT_ATTR].asString();

                    // Get FF subscription rate
                    m_subscriptionRate = protoObj[FF_SUBSCRIPTION_RATE_ATTR].asUInt();
                }
            }
            break;

            case PROTOCOL::PROTOCOL_HART:
            {
                // Default subscription rate for FF protocol
                m_subscriptionRate = 5;

                if (configObj.isMember(HART_APP_INFO_ATTR))
                {
                    protoObj = configObj[HART_APP_INFO_ATTR];

                    if ((false == protoObj.isMember(HART_DD_PATH_ATTR)) ||
                        (false == protoObj.isMember(HART_PORT_ATTR))) 
                    {
                        return bConfigStatus;
                    }

                    // Get HART dd path
                    m_ddPath = (m_ddPrefixPath + protoObj[HART_DD_PATH_ATTR].asString());

                    // Get HART device port
                    m_devicePort = protoObj[HART_PORT_ATTR].asString();

                    // Get HART modem name
                    m_modemName = protoObj[HART_MODEM].asString();

                    // Get HART subscription rate
                    m_subscriptionRate = protoObj[HART_SUBSCRIPTION_RATE_ATTR].asUInt();
                }
            }
            break;

            case PROTOCOL::PROTOCOL_NONE:
            case PROTOCOL::PROTOCOL_PROFIBUS:
            default:
                break;
        }
    }

    return bConfigStatus;
}

/*  ----------------------------------------------------------------------------
    Get device description path
*/
std::string JSONAppConfigParser::getDeviceDescriptionPath()
{
    return m_ddPath;
}

/*  ----------------------------------------------------------------------------
    Get device port 
*/
std::string JSONAppConfigParser::getDevicePort()
{
    return m_devicePort;
}

/*  ----------------------------------------------------------------------------
    Get device port 
*/
unsigned int JSONAppConfigParser::getSubscriptionRate()
{
    return m_subscriptionRate;
}

/*  ----------------------------------------------------------------------------
    Get image path
*/
std::string JSONAppConfigParser::getImagePath()
{
    return m_imagePath;
}

/*  ----------------------------------------------------------------------------
    Get modem name
*/
std::string JSONAppConfigParser::getModemName()
{
    return m_modemName;
}

/*  ----------------------------------------------------------------------------
    Get home path
*/
std::string JSONAppConfigParser::getSettingsPath()
{
    return m_settingsPath;
}

