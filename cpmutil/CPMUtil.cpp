/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Utility class for the cpm application
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "CPMUtil.h"
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iconv.h>
#include "PMErrorCode.h"
#include <flk_log4cplus/Log4cplusAdapter.h>
#include "flk_log4cplus_defs.h"

/*  ----------------------------------------------------------------------------
    Initializes the CPM modules
*/
void CPMUtil::initCPM()
{
#ifdef SET_CURRENT_DIR
    /// Sets the current working directory the cpm application   
    setCurrentWorkingDir();
#endif
}

#ifdef SET_CURRENT_DIR
/*  ----------------------------------------------------------------------------
    Set the current working directory of the application
*/
void CPMUtil::setCurrentWorkingDir()
{
    char szExecPath[MAX_PATH_LEN] = {0};
    char szExecDirPath[MAX_PATH_LEN] = {0};

    /// Get the executable complete path including the executable name in it
    sprintf(szExecPath, "/proc/%d/exe", getpid());
    int nBytesRead = readlink(szExecPath, szExecDirPath, MAX_PATH_LEN);

    if (nBytesRead > 0)
    {
        szExecDirPath[nBytesRead] = '\0';
    }

    /// Extract only the directory path from it
    char* lastDelimPtr = strrchr(szExecDirPath, '/');
    *(lastDelimPtr + 1) = '\0';

    // change current working directory to the application directory
    int retVal = chdir(szExecDirPath);

    if (PM_SUCCESS == retVal)
        LOGF_INFO(CPMAPP, "Changing current working directory of CPM to " << szExecDirPath);
}
#endif

/*  ----------------------------------------------------------------------------
    Checks whether number is power of 2
*/
bool CPMUtil::isPow2(unsigned int num)
{
    return num && (!(num & (num-1)));
}

/*  ----------------------------------------------------------------------------
    Gets the bit position of set bit
*/
unsigned int CPMUtil::getBitPos(unsigned long bitIndex)
{
    unsigned int bitPos = 0;

    if (isPow2(bitIndex))
    {
        // One by one move the only set bit to right till it reaches end
        while (bitIndex)
        {
            bitIndex = bitIndex >> 1;
            // increment Bit Position of shifts
            ++bitPos;
        }
    }

    return bitPos;
}

/* ----------------------------------------------------------------------------
   Create specified directory
*/
bool CPMUtil::createDirectory(std::string directory)
{
    if (true == directory.empty())
        return false;

    struct stat directoryInfo;

    if (0 == stat(directory.c_str(), &directoryInfo))
    {
        LOGF_INFO(CPMAPP, "Directory " << directory <<  " already exists");
        return false;
    }

    std::string createDirCommand = std::string("mkdir -p ") + directory; 
 
    int rc = system(createDirCommand.c_str());
    (void) rc;
    
    return true;   
}

/* ----------------------------------------------------------------------------
   Clear directory content for the specified directory
*/
bool CPMUtil::clearDirectory(std::string directory)
{
    if (true == directory.empty())
        return false;

    struct stat directoryInfo;

    if (0 != stat (directory.c_str(), &directoryInfo))
    {
        LOGF_INFO(CPMAPP, "Directory " << directory <<  " does not exist");
        return false;
    }

    std::string clearDirCommand = std::string("exec rm -r ") + directory + std::string("/*");
    
    int rc = system (clearDirCommand.c_str());
    (void) rc;

    return true;
}

/* ----------------------------------------------------------------------------
   Checks for directory existance
*/
bool CPMUtil::isDirExists(std::string directory)
{
    if (true == directory.empty())
        return false;

    struct stat directoryInfo;

    if (0 != stat (directory.c_str(), &directoryInfo))
    {
        LOGF_INFO(CPMAPP, "Directory " << directory <<  " does not exist");
        return false;
    }

    return true;
}

/* ----------------------------------------------------------------------------
   Convert UTF-8 string to ISO-Latin-1 (ISO-8859-1)
*/
bool CPMUtil::convertUtf8ToIsolatin1(char** utf8Str, size_t *utf8StrLen, char **iso8859Str, size_t *iso8859StrLen)
{
    bool convStatus = false;

    iconv_t iconvDesc = iconv_open ("ISO−8859-1", "UTF-8//TRANSLIT//IGNORE");

    if (iconvDesc == (iconv_t) - 1) {
        if (errno == EINVAL)
        {
            LOGF_ERROR (CPMAPP, "Failed to open iconv descriptor (for UTF8 to ISO-Latin-1 conversion)");
        }
        else
        {
            LOGF_ERROR (CPMAPP, "iconv initialization failure");          
        }
        return convStatus;
    }

    size_t iconvRetVal = iconv (iconvDesc, utf8Str, utf8StrLen, iso8859Str, iso8859StrLen);

    if (iconvRetVal == (size_t) -1) 
    {    
        switch (errno)
        {
            case EILSEQ:
                LOGF_ERROR (CPMAPP, "Invalid multibyte sequence, in string");
                break;

            case EINVAL:
                LOGF_ERROR (CPMAPP, "Incomplete multibyte sequence, in string");
                break;

            case E2BIG:
                LOGF_ERROR (CPMAPP, "No more room, in string");
                break;

            default:
                LOGF_ERROR (CPMAPP, "Failed to convert UTF8 string to ISO-8859-1, ErrorNo: " << errno);
                break;
        }
    }
    else
    {
        convStatus = true;
    }

    iconv_close (iconvDesc);

    return convStatus;
}
