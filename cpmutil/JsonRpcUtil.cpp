/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Creates the singleton instance of CPM and implements CPM methods
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "JsonRpcUtil.h"

/*  ----------------------------------------------------------------------------
    Set error value in JSON RPC format
*/
Json::Value JsonRpcUtil::setErrorObject(int errorCode, const char* errorMsg)
{
    Json::Value errorValue;
    std::string strErrMsg = "";

    errorValue["error"]["code"] = errorCode;

    if (nullptr != errorMsg)
    {
        strErrMsg = errorMsg;
    }

    errorValue["error"]["message"] = strErrMsg;

    return errorValue;
}

/* ----------------------------------------------------------------------------
    Get response value in JSON RPC format
*/
Json::Value JsonRpcUtil::getResponseObject(Json::Value resultMsg)
{
    Json::Value jsonResult;
    
    if ((false == resultMsg.isNull()) &&
    ((false == resultMsg.isObject()) ||
        ((true == resultMsg.isObject()) &&
         ((false == resultMsg.isMember("error")) &&
          (false == resultMsg.isMember("result"))))))
    {
        jsonResult["result"] = resultMsg;
    }

    else if (resultMsg.isMember("error"))
    {
        jsonResult = resultMsg;
    }
    
    return jsonResult;
}
