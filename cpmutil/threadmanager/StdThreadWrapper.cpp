/******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Std Thread Wrapper class definition
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "StdThreadWrapper.h"
#include <thread>
#include <sstream>

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs StdThreadWrapper class
*/
StdThreadWrapper::StdThreadWrapper()
{
}

/*  ----------------------------------------------------------------------------
    Create thread using std::thread class
*/
uint64_t StdThreadWrapper::createThread(std::function<void(const void *)> funcPtr, 
                                        const void *args, bool detachThread)
{
    // Create a thread instance using std::thread
    std::thread threadInstance = std::thread(funcPtr, args);
    std::stringstream ss;
    ss << threadInstance.get_id();

    uint64_t threadId = stoull (ss.str());
    if (true == detachThread)
    {
        threadInstance.detach();
    }

    return threadId;
}
