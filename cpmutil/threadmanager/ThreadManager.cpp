/******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Thread Manager implementation
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ThreadManager.h"
#include "StdThreadWrapper.h"
#include "PMErrorCode.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs ThreadManager Class
*/
ThreadManager::ThreadManager():
    m_threadWrapper(nullptr)
{
    m_threadWrapper = new StdThreadWrapper();
}

/*  ----------------------------------------------------------------------------
    Destructs ThreadManager Class
*/
ThreadManager::~ThreadManager()
{
    if (nullptr != m_threadWrapper)
    {
        delete m_threadWrapper;
        m_threadWrapper = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Implements and returns the singleton instance
*/
ThreadManager* ThreadManager::getInstance()
{
    static ThreadManager threadMgr;
    return &threadMgr;
}

/*  ----------------------------------------------------------------------------
    Creates thread using thread wrapper instance
*/
uint64_t ThreadManager::createThread(std::function<void(const void *)> funcPtr, 
                                    const void* args, bool detachThread)
{
    uint64_t threadId = m_threadWrapper->createThread(funcPtr, args, detachThread);

    m_threadStateInfo.insert(std::pair<uint64_t, THREAD_STATE>(threadId, THREAD_STATE::THREAD_STATE_RUN));

    return threadId;
}

/*  ----------------------------------------------------------------------------
    Removes thread from the thread map state list
*/
int ThreadManager::destroyThread(uint64_t threadId)
{
    auto threadStateItr = m_threadStateInfo.find(threadId);

    if (threadStateItr == m_threadStateInfo.end())
    {
        return PM_FAILURE;
    }

    m_threadStateInfo.erase(threadId);

    return PM_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Returns whether the thread is running or not
*/
bool ThreadManager::isThreadRunning(uint64_t threadId)
{
    auto threadStateItr = m_threadStateInfo.find(threadId);
    
    if (threadStateItr != m_threadStateInfo.end())
    {
        m_threadStateLock.lock();
        THREAD_STATE threadState = threadStateItr->second;
        m_threadStateLock.unlock();

        if (THREAD_STATE_RUN == threadState)
        {
            return true;
        }
    }

    return false;
}

/*  ----------------------------------------------------------------------------
    Set the state of provided thread
*/
int ThreadManager::setThreadState(uint64_t threadId, THREAD_STATE enThreadState)
{
    auto threadStateItr = m_threadStateInfo.find(threadId);
    
    if (threadStateItr != m_threadStateInfo.end())
    {
        m_threadStateLock.lock();       
        threadStateItr->second = enThreadState;
        m_threadStateLock.unlock();

        return PM_SUCCESS;
    }

    return PM_FAILURE;
}

/*  ----------------------------------------------------------------------------
    Get the state of provided thread
*/
THREAD_STATE ThreadManager::getThreadState(uint64_t threadId)
{
    auto threadStateItr = m_threadStateInfo.find(threadId);

    if (threadStateItr != m_threadStateInfo.end())
    {
        m_threadStateLock.lock();
        THREAD_STATE threadState = threadStateItr->second;
        m_threadStateLock.unlock();

        return threadState;
    }

    return THREAD_STATE_INVALID;
}
