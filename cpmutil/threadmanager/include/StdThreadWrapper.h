/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Std (C++11) Thread Wrapper Interface class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES **************************************************************/
#include "IThreadWrapper.h"
#include <functional>

/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Thread Wrapper Interface Class
class StdThreadWrapper : public IThreadWrapper
{
public:
    
    /* Public Types ==========================================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs StdThreadWrapper Class.
    */
    StdThreadWrapper();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes StdThreadWrapper class.
    */
    virtual ~StdThreadWrapper() = default;
    
    /*------------------------------------------------------------------------*/
    /** Interface function for creating thread within CPM application
    */
    uint64_t createThread(std::function<void(const void *)> funcPtr, 
                          const void *args, bool detachThread = true);
};
