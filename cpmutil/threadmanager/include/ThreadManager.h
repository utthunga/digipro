/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    ThreadManager class for managing threads
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <map>
#include <mutex>
#include <functional>
#include "IThreadWrapper.h"

/* ========================================================================== */
/// Enumeration defines for Thread State within CPM
enum THREAD_STATE
{
    THREAD_STATE_INVALID = 0,
    THREAD_STATE_RUN,
    THREAD_STATE_STOP
};

/* CLASS DECLARATIONS *********************************************************/
/** class description of ThreadManager

    This class will be responsible for thread creation within CPM and it
    maintains currently available threads in Adaptor and CSL layers
*/
class ThreadManager
{
public:
    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Implements and returns the singleton instance

        @return ThreadManager instance
    */
    static ThreadManager* getInstance();

    /*------------------------------------------------------------------------*/
    /** Create thread using the corresponding thread wrapper instance

        @param funcPtr, holds function reference object that will be triggered
                         once the thread is created successfully
        @param args, holds function argument for the function referred by
                      function object
        @param detachThread, holds the option as to run the thread in 
                              background or joinable
        @return thread id information for the created thread otherwise it
                 returns the value as -1
    */
    uint64_t createThread(std::function<void(const void *)> funcPtr, 
                          const void* args, bool detachThread = true);

    /*------------------------------------------------------------------------*/
    /** Removes the thread from the list maintained by the Thread Manager

        @param threadId, thread id that need to be removed from the thread list
                         maintained by Thread Manager
    */
    int destroyThread(uint64_t threadId);

    /*------------------------------------------------------------------------*/
    /** Validates whether the thread is still available in the thread list
        maintained by the Thread Manager

        @param threadId, thread id that need to be removed from the thread list
                         maintained by Thread Manager
    */
    bool isThreadRunning(uint64_t threadId);

    /*------------------------------------------------------------------------*/
    /** Set the thread state for the provided thread id in the thread list
        maintained by the ThreadManager
        
        @param threadId, thread id that need to be removed from the thread list
                         maintained by Thread Manager
        @param threadState, new state of the thread
        @return status of the set operation
    */
    int setThreadState(uint64_t threadId, THREAD_STATE enThreadState);

    /*------------------------------------------------------------------------*/
    /** Get the thread state for the provided thread id from the thread list
        maintained by ThreadManager

        @param threadId, thread id that need to be removed from the thread list
                         maintained by Thread Manager
        @return thread state of the provided thread id
                if thread does not exists, invalid state will be returned
    */
    THREAD_STATE getThreadState(uint64_t threadId);

    
    /** Remove copy constructor and assignment operator explicitly 
        as it's a singleton object
    */
    ThreadManager& operator=(const ThreadManager&) = delete;
    ThreadManager (const ThreadManager&) = delete;

private:
    /* Private Data Members ===============================================*/

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs FFAdaptor Class.
    */
    ThreadManager();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes FFAdaptor class.
    */
    virtual ~ThreadManager();

    /// Holds the thread list along with their corresponding state created 
    /// within Adaptor and CSL layer within CPM
    std::map<uint64_t, THREAD_STATE> m_threadStateInfo;

    /// Holds the r/w lock required for thread state list
    std::mutex m_threadStateLock;

    /// Holds the thread wrapper instance
    IThreadWrapper* m_threadWrapper;
};
