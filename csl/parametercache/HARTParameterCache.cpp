/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    HARTParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "HARTParameterCache.h"
#include "SDC625Controller.h"

#include "ddbVarList.h"
#include "ddbGeneral.h"
#include "ddbMenu.h"
#include <math.h>

#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    HARTParameterCache class instance initialization
*/
HARTParameterCache::HARTParameterCache() : m_pSDC625(nullptr), m_pCM(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
HARTParameterCache::~HARTParameterCache()
{
    // Clear the member variables
    m_pSDC625 = nullptr;
    m_pCM = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function initialize cache tables and scheduler
*/
PMStatus HARTParameterCache::initialize(SDC625Controller* sdc,
                                        IConnectionManager* cm)
{
    PMStatus status;

    if (nullptr != sdc && nullptr != cm)
    {
        m_pSDC625 = sdc;
        m_pCM = cm;
    }
    else
    {
        status.setError(HART_PC_ERR_INITIALIZE_FAILED,
                        PM_ERR_CL_HART_CSL,
                        CSL_ERR_SC_PC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get list of parameters.
    It is uplink responsibility to free ParamInfo memory
*/
PMStatus HARTParameterCache::getParameter(ParamInfo* paramInfo)
{
    PMStatus status;

    // Do not proceed further if paramInfo is null.
    if(nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to get parameter\n");
        status.setError(
                    HART_PC_ERR_REQUEST_ITEM_NULL,
                    PM_ERR_CL_HART_CSL,
                    CSL_ERR_SC_PC);
    }

    if(!status.hasError())
    {
        for (auto item : paramInfo->paramList)
        {
            auto itemInfo = &item.get();

            switch(itemInfo->itemType)
            {
            case PARAM_VARIABLE:
            {
                hCVar* pVar = m_pSDC625->getVariablePtr(itemInfo->itemID);

                if (nullptr != pVar)
                {
                    // Fill variable info to the paramInfo structure
                    status = generateFlatItem(pVar, itemInfo, paramInfo->requestInfo.maskType);
                    if (status.hasError())
                    {
                        LOGF_ERROR(CPMAPP, "Failed to get parameter info\n");
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Failed to get parameter info\n");
                    status.setError(SDC_CTRL_GET_PARAMETER_FAILED,
                                PM_ERR_CL_SDC_CTRL,
                                CSL_ERR_SC_PC);
                }
            }
                break;
            case PARAM_IMAGE:
            {
                CimageItemList* pImageItemList = nullptr;

                // Get the list of image items
                pImageItemList = m_pSDC625->getImageItemList();

                if (nullptr != pImageItemList)
                {
                    int imageIndex = 0;
                    hCimageItem* pImageItem = nullptr;

                    for (auto item : paramInfo->paramList)
                    {
                        auto itemInfo = &item.get();
                        for (auto imageItem : *pImageItemList)
                        {
                            pImageItem = dynamic_cast<hCimageItem*>(imageItem);
                            pImageItem->resolveProcure();

                            if (itemInfo->itemID == pImageItem->getID())
                            {
                                imageIndex = pImageItem->getImageIndex();
                                break;
                            }
                        }
                    }

                    Image_PtrList_t* pImageList = nullptr;

                    // Get the list of image items
                    pImageList = m_pSDC625->getImageList();
                    if(nullptr != pImageList)
                    {
                        hCimage* pImage = pImageList->at(imageIndex);

                        FramePtrList_t* frameList = dynamic_cast<FramePtrList_t*>(pImage);
                        for (auto frame : *frameList)
                        {
                            hCframe* pFrame = nullptr;
                            pFrame = dynamic_cast<hCframe*>(frame);

                            // Fill variable info to Json structure
                            status = generateFlatItem(pImageItem, itemInfo,
                                                      paramInfo->requestInfo.maskType);

                            ImageType* image = dynamic_cast<ImageType*>(itemInfo);
                            image->length = pFrame->uSize;
                            image->data = static_cast<unsigned char*>(pFrame->pbRawImage);

                            if (status.hasError())
                            {
                                LOGF_ERROR(CPMAPP, "Failed to get parameter info\n");
                            }
                        }
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "image list pointer is null\n");
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Failed to get parameters from cache\n");
                    status.setError(SDC_CTRL_GET_PARAMETER_FAILED,
                                PM_ERR_CL_SDC_CTRL,
                                CSL_ERR_SC_PC);
                }
            }
            break;
            default:
                break;
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the cache. if it is not available
    in the cache it reads from device and update cache as well
*/
PMStatus HARTParameterCache::readValue(ParamInfo* paramInfo)
{
    PMStatus status;

       // Do not proceed further if paramInfo is null.
       if (nullptr == paramInfo)
       {
           LOGF_ERROR(CPMAPP, "Failed to read value from cache");
           status.setError(SDC_CTRL_READ_VALUE_FAILED,
                           PM_ERR_CL_SDC_CTRL,
                           CSL_ERR_SC_PC);
       }

       if(!status.hasError())
       {
           CValueVarient value{};

           for (auto paramItem : paramInfo->paramList)
           {
               auto paramItemInfo = &paramItem.get();

               if (PARAM_VARIABLE == paramItemInfo->itemType)
               {
                   auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
                   if (nullptr == varItem)
                   {
                       LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                       continue;
                   }

                   hCVar* pVar = m_pSDC625->getVariablePtr(varItem->itemID);

                   if (nullptr != pVar)
                   {
                      INSTANCE_DATA_STATE_T ids = pVar->getDataState();
                      DATA_QUALITY_T  dq = pVar->getDataQuality();

                      /* Read value from device for dynamic parameters and
                     if the parameter is not yet initialized */
                     if (ids == IDS_UNINITIALIZED || ids == IDS_INVALID
                        ||(ids== IDS_STALE && dq== DA_STALEUNK)
                        ||varItem->misc.class_attr & maskDynamic)
                     {
                         status = m_pSDC625->readDeviceValue(varItem, value);
                     }
                     else
                     {
                        //read the variable value
                        status = m_pSDC625->readCacheValue(varItem, value);
                     }
                }


                if(!status.hasError())
                {
                    // Fill variable value to paramList
                    setValuesToParamInfo(value, varItem);
                }
            }
        }
    }
      
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value into the device and cache tables
*/
PMStatus HARTParameterCache::writeValue(ParamInfo* paramInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to write value to cache\n");
        status.setError(SDC_CTRL_WRITE_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if(!status.hasError())
    {
        this->acquireLock();

        CValueVarient value{};

        for (auto item : paramInfo->paramList)
        {
            auto itemInfo = &item.get();

            if (PARAM_VARIABLE == itemInfo->itemType)
            {
                auto varItem = dynamic_cast<VariableType*>(itemInfo);
                if (nullptr == varItem)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                //verify suppiled value is correct or not
                bool isValid = verifyIsValidValue(varItem);
                if(isValid)
                {
                    // Fill variable info to Json structure
                    errorCode = getValuesFromParamInfo(varItem, value);
                    if (SUCCESS == errorCode)
                    {
                        //writes value to device
                        status = m_pSDC625->writeDeviceValue(varItem, value);
                    }
                }
                else
                {
                    status.setError(SDC_CTRL_WRITE_VALUE_FAILED,
                                    PM_ERR_CL_SDC_CTRL,
                                    CSL_ERR_SC_PC);
                }
            }
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to check for supplied value is valid or not
*/
bool HARTParameterCache::verifyIsValidValue(VariableType* varItem)
{
    uint32_t value{};
    bool isValid = false;

    //gives hCVar instance
    hCVar* var = m_pSDC625->getVariablePtr(varItem->itemID);
    if(nullptr != var)
    {
        value = varItem->value.stdValue.l;

        if(vT_Enumerated == var->VariableType() ||
                vT_BitEnumerated == var->VariableType())
        {
            if (varItem->bitIndex > 0)
            {
                // Check if the requested bit index is valid or not
                if (verifyIsValidBitIndex(varItem))
                {
                    // check the valid bit item value 0 or 1
                    isValid = value == BIT_ON ||value == BIT_OFF ? true : false;
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Invalid bit Index value!!");
                }
            }
            else
            {
                UIntList_t returnList;

                hCEnum* enumPtr = dynamic_cast<hCEnum*>(var);
                if(nullptr != enumPtr)
                {
                    //gives valid all enumeration values
                    enumPtr->getAllValidValues(returnList);
                }

                /* verifies user provided value is present in the returnList
                 * or its available with combinations of items in the list
                 */
                if (returnList.size() > 0)
                {
                    isValid = isValidValue(returnList, returnList.size(), value);
                }
                else
                {
                    isValid = false;
                }
            }
        }
        else if(vT_Index == var->VariableType())
        {
            hCindex* indexPtr = dynamic_cast<hCindex*>(var);
            if(nullptr != indexPtr)
            {
                //verifies index value is valid or not
                isValid = indexPtr->isAvalidValue(value);
            }
        }
        else
        {
            /* it is not required to validate values for other data types except
               enum, bitenum and index types.So always returns true
            */
            isValid = true;
        }
    }

    return isValid;
}

/*  ----------------------------------------------------------------------------
    This function is used to check given bit enumeration value is valid or not
*/
bool HARTParameterCache::isValidValue(UIntList_t list, int size, int value)
{
    if (value == 0)
        return true;

    if (size == 0 && value != 0)
        return false;

    if (list[size - 1] > value)
        return isValidValue(list, size - 1, value);

    return isValidValue(list, size - 1, value) ||
           isValidValue(list, size - 1, value - list[size - 1]);
}

/*  ----------------------------------------------------------------------------
    This function is used to check given bit index value is valid or not
*/
bool HARTParameterCache::verifyIsValidBitIndex(VariableType* varItem)
{
    UIntList_t returnList;
    bool isValid = false;
    uint32_t value{};

    //gives hCVar instance
    hCVar* pVar = m_pSDC625->getVariablePtr(varItem->itemID);

    if(nullptr != pVar)
    {
        value = varItem->bitIndex;

        hCEnum* enumPtr = dynamic_cast<hCEnum*>(pVar);

        if(nullptr != enumPtr)
        {
            //gives valid all enumeration values
            enumPtr->getAllValidValues(returnList);
        }

        /* verifies user provided value is
           present in the returnList
        */
        for(auto item : returnList)
        {
            if(item == value)
            {
                isValid = true;
            }
        }
    }

    return isValid;
}

/*  ----------------------------------------------------------------------------
    Reads value from the device and stores it into the cache table
*/
PMStatus HARTParameterCache::updateCacheTable(ParamInfo paramInfo,
                                         bool dynamicVars)
{
    PMStatus status;

    if (paramInfo.paramList.size() <= 0)
    {
        return status;
    }

    for (auto paramItem : paramInfo.paramList)
    {
        auto paramItemInfo = &paramItem.get();

        if (PARAM_VARIABLE == paramItemInfo->itemType)
        {
            auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
            if (nullptr == varItem)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }

            bool isDynamic = varItem->misc.class_attr & maskDynamic;

            if (dynamicVars == isDynamic)
            {
                hCVar* var = m_pSDC625->getVariablePtr(varItem->itemID);

                if (nullptr != var)
                {
                    //below state set to read value directly from the device
                    var->markItemState(IDS_STALE);
                    var->setDataQuality(DA_STALEUNK);
                }
            }
        }
    }

    this->acquireLock();

    // read the variable value from the device, non-blocking call
    if(nullptr != m_pSDC625)
    {
        m_pSDC625->asyncReadService();
    }

    this->releaseLock();


    return status;

}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the device and stores it into
    the cache table
*/
PMStatus HARTParameterCache::readDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to read value from cache");
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if(!status.hasError())
    {
        this->acquireLock();

        CValueVarient value{};

        for (auto paramItem : paramInfo->paramList)
        {
            auto paramItemInfo = &paramItem.get();

            if (PARAM_VARIABLE == paramItemInfo->itemType)
            {
                auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
                if (nullptr == varItem)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                //read the variable value from the device
                status = m_pSDC625->readDeviceValue(varItem, value);
                if(!status.hasError())
                {
                    status = m_pSDC625->readCacheValue(varItem, value);

                    if (!status.hasError())
                    {
                        // Fill variable value to paramList
                        setValuesToParamInfo(value, varItem);
                    }
                }
            }
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the device and stores it into
    the cache table. Non-Blocking call.
*/
PMStatus HARTParameterCache::asyncReadDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to read value from device");
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if (!status.hasError())
    {
        this->acquireLock();

        for (auto paramItem : paramInfo->paramList)
        {
            auto paramItemInfo = &paramItem.get();

            if (PARAM_VARIABLE == paramItemInfo->itemType)
            {
                auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
                if (nullptr == varItem)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                hCVar* var = m_pSDC625->getVariablePtr(varItem->itemID);

                if (nullptr != var)
                {
                    //below state set to read value directly from the device
                    var->markItemState(IDS_STALE);
                    var->setDataQuality(DA_STALEUNK);
                }
            }
        }

        //read the variable value from the device
        m_pSDC625->asyncReadService();

        for (auto paramItem : paramInfo->paramList)
        {
            auto paramItemInfo = &paramItem.get();

            if (PARAM_VARIABLE == paramItemInfo->itemType)
            {
                auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
                if (nullptr == varItem)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                hCVar* var = m_pSDC625->getVariablePtr(varItem->itemID);

                if (nullptr != var)
                {
                    setValuesToParamInfo(var->getRealValue(), varItem);
                }
            }
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value to the device and on success it
    stores value in cache table
*/
PMStatus HARTParameterCache::writeDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to write value to cache");
        status.setError(SDC_CTRL_WRITE_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if(!status.hasError())
    {
        this->acquireLock();

        CValueVarient value{};

        for (auto item : paramInfo->paramList)
        {
            auto itemInfo = &item.get();

            if (PARAM_VARIABLE == itemInfo->itemType)
            {
                auto varItem = dynamic_cast<VariableType*>(itemInfo);
                if (nullptr == varItem)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                // Fill variable info to Json structure
                errorCode = getValuesFromParamInfo(varItem, value);
                if (SUCCESS == errorCode)
                {
                    status = m_pSDC625->writeDeviceValue(varItem, value);
                }
            }
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read edit value from the cache table
*/
PMStatus HARTParameterCache::readEditValue(ParamInfo* paramInfo)
{
    PMStatus status;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to read edit value from cache\n");
        status.setError(SDC_CTRL_READ_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if (!status.hasError())
    {
        this->acquireLock();

        CValueVarient value{};

        for (auto paramItem : paramInfo->paramList)
        {
            auto paramItemInfo = &paramItem.get();

            auto varItem = dynamic_cast<VariableType*>(paramItemInfo);
            if (nullptr == varItem)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }

            //read the variable value
            status = m_pSDC625->readCacheValue(varItem, value);
            if (!status.hasError())
            {
                // Fill variable value to paramList
                setValuesToParamInfo(value, varItem);
            }

            // Execute pre edit actions
            m_pSDC625->preEditActions(varItem->itemID);
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write edit value to the cache table
*/
PMStatus HARTParameterCache::writeEditValue(ParamInfo* paramInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        LOGF_ERROR(CPMAPP, "Failed to write edit value to cache\n");
        status.setError(SDC_CTRL_WRITE_VALUE_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
    }

    if (!status.hasError())
    {
        this->acquireLock();

        CValueVarient value{};

        for (auto item : paramInfo->paramList)
        {
            auto itemInfo = &item.get();

            auto varItem = dynamic_cast<VariableType*>(itemInfo);
            if (nullptr == varItem)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }

            // Fill variable info to Json structure
            errorCode = getValuesFromParamInfo(varItem, value);

            if (SUCCESS == errorCode)
            {
                //writes value to device
                status = m_pSDC625->writeCacheValue(varItem, value);
            }          
        }

        this->releaseLock();
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Performs pre subscription actions upon subscription timer elapsed
*/
PMStatus HARTParameterCache::preSubscriptionActions(ParamInfo& paramInfo,
                                                    bool readFromDevice)
{
    PMStatus status;

    // Mark all the subscription items to stale before read
    for (auto item : paramInfo.paramList)
    {
        auto itemInfo = &item.get();
        hCVar* pVar = m_pSDC625->getVariablePtr(itemInfo->itemID);

        if (nullptr != pVar)
        {
            if (readFromDevice || pVar->IsDynamic())
            {
                pVar->markItemState(IDS_STALE);
                pVar->setDataQuality(DA_STALEUNK);
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Performs subscription actions upon subscription timer elapsed
*/
PMStatus HARTParameterCache::doSubscriptionActions(ParamInfo& paramInfo,
                                                   bool readFromDevice)
{
    PMStatus status;

    // If it is set to true, reads from device, otherwise from cache
    if (readFromDevice)
    {
        // reads from device
        status  = asyncReadDeviceValue(&paramInfo);
    }
    else
    {
        // reads from cache
        status = readValue(&paramInfo);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the variable to item info
*/
PMStatus HARTParameterCache::generateFlatItem(void* paramItem,
                        ItemInfoType *itemInfo, MaskType mask)
{
    PMStatus status;
    VariableType* variable = nullptr;

    switch(itemInfo->itemType)
    {
    case PARAM_VARIABLE:
    {
        hCVar* item = nullptr;

        item = static_cast<hCVar*>(paramItem);
        variable = dynamic_cast<VariableType*>(itemInfo);
        if(nullptr == variable || nullptr == item)
        {
            LOGF_TRACE(CPMAPP, "null variable data");
            status.setError(SDC_CTRL_GET_PARAMETER_FAILED,
                        PM_ERR_CL_SDC_CTRL,
                        CSL_ERR_SC_PC);
            break;
        }


        if (item->VariableType() == vT_BitEnumerated && variable->bitIndex > 0)
        {
            if (!verifyIsValidBitIndex(variable))
            {
                LOGF_ERROR(CPMAPP, "Invalid Bit Index, unable to get parameter info!");
                status.setError(SDC_CTRL_GET_PARAMETER_FAILED,
                            PM_ERR_CL_SDC_CTRL,
                            CSL_ERR_SC_PC);
                break;
            }
        }


        /* For subscription mask, only the type and class attribute
           info are needed*/
        if (mask == ITEM_ATTR_SUBSCRIPTION_MASK)
        {
            hCbitString* pbs = static_cast<hCbitString*>(item->pClass->procure());
            if (nullptr != pbs)
            {
                variable->misc.class_attr = pbs->getBitStr();
            }

            //gives variable type
            variable->type = item->VariableType();
            break;
        }

        if (mask == ITEM_ATTR_MIN_MASK || mask == ITEM_ATTR_ALL_MASK)
        {
            //This function is used to fill variable miminmum information
            fillMinimumVarInformation(item, variable);
        }

        CValueVarient value{};

        //gives edit format of the variable
        wstring editFormat = item->getEditFormat();
        std::string varEditFormat( editFormat.begin(), editFormat.end());
        if(!varEditFormat.empty())
        {
            assignStringValue(variable->editFormat, varEditFormat);
        }

        //fills variable min max values
        fillMinMaxValue(item, variable->misc);

        // reads value from cache
        m_pSDC625->readCacheValue(variable, value);

        //fills cached values
        setValuesToParamInfo(value, variable);

        variable->itemID = item->getID();
        //gives variable type
        variable->type = item->VariableType();
        variable->size = item->getSize();
        hCbitString* pbs = static_cast<hCbitString*>(item->pClass->procure());
        if (nullptr != pbs)
        {
            variable->misc.class_attr = pbs->getBitStr();
        }
    }
        break;

        case PARAM_IMAGE:
        {
            hCimageItem* item = nullptr;
            ImageType* image = nullptr;

            image = dynamic_cast<ImageType*>(itemInfo);
            item = static_cast<hCimageItem*>(paramItem);
            if(nullptr == image || nullptr == item)
            {
                LOGF_ERROR(CPMAPP, "null image data");
                break;
            }

            string label{};
            string help{};

            //gives variable help
            item->Help(help);

            //gives variable label
            item->Label(label);

            //fill the variable label into paraminfo
            if (!label.empty())
            {
                assignStringValue(image->label, label);
            }

            //fill the variable help into paraminfo
            if (!help.empty())
            {
                assignStringValue(image->help, help);
            }

            image->itemID = item->getID();
            image->itemType = ParamItemType::PARAM_IMAGE;

            string imageLink = "";
            if (item->isLinkString())
            {
                item->getLink(imageLink);
            }
            assignStringValue(image->imagePath, imageLink);
        }
        break;

        default:
        break;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the minimum item information
*/
void HARTParameterCache::fillMinimumVarInformation(hCVar* item, VariableType* variable)
{
    string help{};
    string label{};

    // Added below condition to handle single BITENUMERATION_ITEM
    if (item->VariableType() == vT_BitEnumerated && variable->bitIndex > 0)
    {
        hCBitEnum* bitEnumVar = nullptr;
        bitEnumVar = dynamic_cast<hCBitEnum*>(item);

        if (nullptr != bitEnumVar)
        {
            hCenumDesc enumDes(bitEnumVar->devHndl());
            bitEnumVar->procureEnum(variable->bitIndex, enumDes);

            string enumLabel(enumDes.descS.procureVal().begin(),
                               enumDes.descS.procureVal().end());
            string enumHelp(enumDes.helpS.procureVal().begin(),
                            enumDes.helpS.procureVal().end());

            label = enumLabel;
            help = enumHelp;
        }
    }
    else
    {
        //gives variable help
        item->Help(help);

        //gives variable label
        item->Label(label);
    }


    //fill the variable label into paraminfo
    if(!label.empty())
    {
        assignStringValue(variable->label, label);
    }

    //fill the variable help into paraminfo
    if(!help.empty())
    {
        assignStringValue(variable->help, help);
    }

    variable->itemID = item->getID();

    //gives variable type
    variable->type = item->VariableType();

    //gives variable handling such as read or write
    variable->misc.handling = (item->IsReadOnly() == true) ? READ_HANDLING :
                                                             WRITE_HANDLING;

    hCbitString* pbs = static_cast<hCbitString*>(item->pClass->procure());
    if (nullptr != pbs)
    {
        variable->misc.class_attr = pbs->getBitStr();
    }

    //gives index type variable information
    if(variable->type == vT_Index)
    {
        hCindex* itemIndex = nullptr;

        itemIndex  = dynamic_cast<hCindex*>(item);
        if(nullptr != itemIndex)
        {
            fillIndexItemInformation(itemIndex, variable);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "null index variable data");
        }
    }

    //gives enum variable information
    if(variable->type == vT_Enumerated || variable->type == vT_BitEnumerated)
    {
        hCEnum* varEnum = dynamic_cast<hCEnum*>(item);
        if(nullptr != varEnum)
        {
            fillEnumItemInformation(varEnum, variable);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "null enum variable data");
        }
    }

    wstring unit;

    //gives unit of the variable
    item->getUnitString(unit);
    std::string varUnit( unit.begin(), unit.end());
    if(!varUnit.empty())
    {
        assignStringValue(variable->unit, varUnit);
    }

    wstring dispFormat;

    //gives display format of the variable
    hCNumeric* numeric = dynamic_cast<hCNumeric*>(item);
    if(nullptr != numeric)
    {
        dispFormat = numeric->getDispFormat();
        std::string varDispFormat(dispFormat.begin(), dispFormat.end());
        if(!varDispFormat.empty())
        {
            assignStringValue(variable->displayFormat, varDispFormat);
        }
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the index item information
*/
void HARTParameterCache::fillIndexItemInformation(hCindex* itemIndex, VariableType* variable)
{
    vector<hCitemBase*> itemBase;

    //gives index itemBase
    itemIndex->pIndexed->getAllArrayPtrs(itemBase);

    for(auto item : itemBase)
    {
        vector<UINT32> allIndexedIDsReturned;

        //gives set of index values
        item->getAllindexValues(allIndexedIDsReturned);

        for(auto index : allIndexedIDsReturned)
        {
            EnumValue indexInfo{};
            HreferenceList_t returnedItemRefs;

            //based on index value get the variable information
            item->getAllByIndex(index, returnedItemRefs);

            indexInfo.value = index;
            hCreference itemRef = returnedItemRefs.at(0);

            wstring strLabel;
            wstring strHelp;
            bool validLabel;

            //gives item label and help
            itemRef.getItemLabel(strLabel, validLabel, strHelp);

            std::string label(strLabel.begin(), strLabel.end());
            assignStringValue(indexInfo.label, label);

            std::string help(strHelp.begin(), strHelp.end());
            assignStringValue(indexInfo.help, help);

            variable->enums.push_back(indexInfo);

            //clears item ref list
            returnedItemRefs.clear();
        }

        //clears index list
        allIndexedIDsReturned.clear();
    }

     //clears itemBase list
    itemBase.clear();
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the enum item information
*/
void HARTParameterCache::fillEnumItemInformation(hCEnum* varEnum, VariableType* variable)
{
    hCenumList eList(varEnum->devHndl());
    if (varEnum->procureList(eList) == SUCCESS)
    {
        EnumTriad_t            localData;
        for (hCenumList::iterator iT = eList.begin(); iT != eList.end(); ++iT)
        {
            EnumValue enumVal{};
            localData = *iT;
            if (localData.helpS.length() > 0)
            {
                std::string help(localData.helpS.begin(), localData.helpS.end());
                assignStringValue(enumVal.help, help);
            }

            if (localData.descS.length() > 0)
            {
                std::string label(localData.descS.begin(), localData.descS.end());
                assignStringValue(enumVal.label, label);
            }

            enumVal.value = localData.val;
            variable->enums.push_back(enumVal);
        }
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the read variable value to item info
*/
void HARTParameterCache::setValuesToParamInfo(CValueVarient item,
                                          VariableType *variable)
{
    variable->valueChanged = false;
    // assign variable value based on item type
    switch (variable->type)
    {
    case vT_undefined:
        break;
    case vT_MaxType:
    case vT_Index:
    case vT_Integer:
    case vT_Enumerated:
    case vT_BitEnumerated:
        if (variable->value.stdValue.l != item.vValue.iIntConst)
        {
            variable->value.stdValue.l = item.vValue.iIntConst;
            variable->valueChanged = true;
        }
        break;
    case vT_FloatgPt:
        {
            float floatVarVal = static_cast<float>(item.vValue.fFloatConst);
            if ( variable->value.stdValue.f != floatVarVal)
            {
                variable->value.stdValue.f = floatVarVal;
                variable->valueChanged = true;
            }
        }
        break;
    case vT_Double:
        if (variable->value.stdValue.d != item.vValue.fFloatConst)
        {
            variable->value.stdValue.d = item.vValue.fFloatConst;
            variable->valueChanged = true;
        }
        break;
    case vT_Unsigned:
        if (variable->value.stdValue.ul != item.vValue.longlongVal)
        {
            variable->value.stdValue.ul = item.vValue.longlongVal;
            variable->valueChanged = true;
        }
        break;
    case vT_HartDate:
        {
            hCVar* pVar = m_pSDC625->getVariablePtr(variable->itemID);
            if (nullptr != pVar)
            {
                hChartDate* date = (hChartDate*)pVar;
                if (nullptr != date)
                {
                    UINT32 day, month, year;
                    date->getDispDate(year, month, day);
                    char* dateNTime = new char[DATENTIME_STR_LENGTH]();
                    sprintf(dateNTime, DATENTIME_TYPE_FORMAT,
                                       day, month, year, DATENTIME_NOT_INIT,
                                       DATENTIME_NOT_INIT, DATENTIME_NOT_INIT);
                    time_t timeSinceEpoch = m_pSDC625->convertsUserTimeToEpochTime(
                                dateNTime, DATENTIME_DATA_FORMAT);
                    long val = static_cast<long>(timeSinceEpoch);
                    if (val < DATENTIME_NOT_INIT)
                    {
                        val = DATENTIME_NOT_INIT;
                    }
                    if (nullptr != dateNTime)
                    {
                        delete[] dateNTime;
                        dateNTime = nullptr;
                    }
                    if (variable->value.stdValue.ul != val)
                    {
                        variable->value.stdValue.ul = val;
                        variable->valueChanged = true;
                    }
                }
            }
        }
        break;
    case vT_TimeValue:
        {
            hCVar* pVar = m_pSDC625->getVariablePtr(variable->itemID);
            if (nullptr != pVar)
            {
                hCTimeValue* time = (hCTimeValue*)pVar;
                if (nullptr != time)
                {
                    double scaling_fact = 1;
                    unsigned long val = 0;
                    
                    if (time->isTimeScale())
                    {
                        wstring timeVal = time->getTimeValue();
                        swscanf(timeVal.c_str(), L"%d", &val);
                        variable->misc.timeValType = TIME_VALUE_SCALED;
                        scaling_fact = time->GetScalingFactor();
                    }
                    else
                    {
                        variable->misc.timeValType = TIME_VALUE_DIFF;
                        val = static_cast<unsigned long>(item.vValue.longlongVal);
                    }

                    if (variable->value.stdValue.ul != val)
                    {
                        variable->value.stdValue.ul = lround(val/scaling_fact);
                        variable->valueChanged = true;
                    }
                }
            }
        }
        break;

    case vT_EUC:
    case vT_VisibleString:
        if (nullptr == variable->value.strData.str && !item.sStringVal.empty())
        {
            assignStringValue(variable->value.strData, item.sStringVal,
                              item.vSize);
            variable->valueChanged = true;
        }
        else
        {
            if (nullptr != variable->value.strData.str &&
                    item.sStringVal.compare(variable->value.strData.str) != 0)
            {

                assignStringValue(variable->value.strData, item.sStringVal,
                                  item.vSize);
                variable->valueChanged = true;
            }
        }
        variable->value.strData.ucType = UC_UTF_8;
        break;

    case vT_BitString:
    case vT_OctetString:
    case vT_Password:
    case vT_PackedAscii:
    case vT_Ascii:
        if (nullptr == variable->value.strData.str && !item.sStringVal.empty())
        {
            assignStringValue(variable->value.strData, item.sStringVal,
                              item.vSize);
            variable->valueChanged = true;
        }
        else
        {
            if (nullptr != variable->value.strData.str &&
                    item.sStringVal.compare(variable->value.strData.str) != 0)
            {

                assignStringValue(variable->value.strData, item.sStringVal,
                                  item.vSize);
                variable->valueChanged = true;
            }
        }
        if ((vT_PackedAscii == variable->type) ||
            (vT_BitString == variable->type) ||
            (vT_OctetString == variable->type))
        {
            variable->value.strData.ucType = UC_ASCII;
        }
        else
        {
            variable->value.strData.ucType = UC_ISO_LATIN_1;
        }
        break;

    default:
        break;
    }
}

/*   ----------------------------------------------------------------------------
     Allocates memory for char pointer and asssigns string to it.
*/
void HARTParameterCache::assignStringValue(StringType& variable, std::string str,
                                           int maxLength)
{
    //deallocates memory allocated by HART PC
    if(variable.flag == FREE_STRING)
    {
        delete[] variable.str;
        variable.str = nullptr;
        variable.len = 0;
        variable.flag = DONT_FREE_STRING;
    }

    variable.str =  new char[str.length()+1]();
    strncpy(variable.str, str.c_str(), str.length());
    variable.str[str.length()] = '\0';
    // if maxLength is set, assign it otherwsie assign the current string length
    variable.len = maxLength > 0 ? maxLength : str.length();
    variable.flag = FREE_STRING;
}

/*  ----------------------------------------------------------------------------
    This function is used to fill the variable min max values
*/
void HARTParameterCache::fillMinMaxValue(hCVar* varItem, Misc& misc)
{
    hCRangeList retList;

    //gives min max list for the variable
    varItem->getMinMaxList(retList);

    //verifies map is empty or not
    if(!retList.empty())
    {
        MinMaxRange minMaxRange{};

        for(auto item : retList)
        {
            //get the value from the map
            hCRangeItem rangeItem = item.second;

            //based on type assigns min values
            switch (rangeItem.minVal.vType)
            {
            case CValueVarient::valueType_t::invalid:
                break;
            case CValueVarient::valueType_t::isIntConst:
                if(varItem->VariableType() == vT_Integer)
                {
                    minMaxRange.min.l = rangeItem.minVal.vValue.iIntConst;
                }
                else if(varItem->VariableType() == vT_HartDate)
                {
                    minMaxRange.min.l = rangeItem.minVal.vValue.iIntConst;
                }
                break;
            case CValueVarient::valueType_t::isDepIndex:
                minMaxRange.min.l = rangeItem.minVal.vValue.depIndex;
                break;
            case CValueVarient::valueType_t::isVeryLong:
                if(varItem->VariableType() == vT_Unsigned)
                {
                    minMaxRange.min.ul = rangeItem.minVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_FloatgPt)
                {
                    minMaxRange.min.f = rangeItem.minVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_Double)
                {
                    minMaxRange.min.d = rangeItem.minVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_Integer)
                {
                    minMaxRange.min.l = rangeItem.minVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_TimeValue)
                {
                    minMaxRange.min.l = rangeItem.minVal.vValue.longlongVal;
                }
                break;
            case CValueVarient::valueType_t::isFloatConst:
                if(varItem->VariableType() == vT_FloatgPt)
                {
                    minMaxRange.min.f = rangeItem.minVal.vValue.fFloatConst;
                }
                else if(varItem->VariableType() == vT_Double)
                {
                    minMaxRange.min.d = rangeItem.minVal.vValue.fFloatConst;
                }
                break;
            default:
                break;
            }

            //based on type assigns max values
            switch (rangeItem.maxVal.vType)
            {
            case CValueVarient::valueType_t::invalid:
                break;
            case CValueVarient::valueType_t::isIntConst:
                if(varItem->VariableType() == vT_Integer)
                {
                    minMaxRange.max.l = rangeItem.maxVal.vValue.iIntConst;
                }
                else if(varItem->VariableType() == vT_HartDate)
                {
                    minMaxRange.max.l = rangeItem.maxVal.vValue.iIntConst;
                }
                break;
            case CValueVarient::valueType_t::isDepIndex:
                minMaxRange.min.l = rangeItem.minVal.vValue.depIndex;
                break;
            case CValueVarient::valueType_t::isVeryLong:
                if(varItem->VariableType() == vT_Unsigned)
                {
                    minMaxRange.max.ul = rangeItem.maxVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_FloatgPt)
                {
                    minMaxRange.max.f = rangeItem.maxVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_Double)
                {
                    minMaxRange.max.d = rangeItem.maxVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_Integer)
                {
                    minMaxRange.max.l = rangeItem.maxVal.vValue.longlongVal;
                }
                else if(varItem->VariableType() == vT_TimeValue)
                {
                    minMaxRange.max.l = rangeItem.maxVal.vValue.longlongVal;
                }
                break;
            case CValueVarient::valueType_t::isFloatConst:
                if(varItem->VariableType() == vT_FloatgPt)
                {
                    minMaxRange.max.f = rangeItem.maxVal.vValue.fFloatConst;
                }
                else if(varItem->VariableType() == vT_Double)
                {
                    minMaxRange.max.d = rangeItem.maxVal.vValue.fFloatConst;
                }
                break;
            default:
                break;
            }

            //add the minMaxRange to the vector list.
            misc.minMaxRange.push_back(minMaxRange);
        }
    }
}
/*  ----------------------------------------------------------------------------
    This function is used to fill the variable info to variable value
*/
int HARTParameterCache::getValuesFromParamInfo(VariableType* varInfo,
                                               CValueVarient& varValue)
{
    int errorCode = SUCCESS;

    switch (varInfo->type)
    {
    case vT_MaxType:
    case vT_Index:
    case vT_Integer:
        varValue.vType = varValue.valueType_t::isIntConst;
        varValue.vValue.iIntConst = varInfo->value.stdValue.l;
        break;
    case vT_Unsigned:
        varValue.vType = varValue.valueType_t::isIntConst;
        varValue.vValue.iIntConst = varInfo->value.stdValue.ul;
        break;
    case vT_FloatgPt:
        varValue.vType = varValue.valueType_t::isFloatConst;
        varValue.vValue.fFloatConst = varInfo->value.stdValue.f;
        break;
    case vT_Double:
        varValue.vType = varValue.valueType_t::isFloatConst;
        varValue.vValue.fFloatConst = varInfo->value.stdValue.d;
        break;
    case vT_TimeValue:
        {
            hCVar* pVar = m_pSDC625->getVariablePtr(varInfo->itemID);
            if (nullptr != pVar)
            {
                hCTimeValue* time = (hCTimeValue*)pVar;
                double scaling_fact = 1;
                if (nullptr != time)
                {
                    if (time->isTimeScale())
                    {
                        scaling_fact = time->GetScalingFactor();
                    }
                    else
                    {
                        varValue.vType = varValue.valueType_t::isVeryLong;
                    }
                    
                    varValue.vType = varValue.valueType_t::isVeryLong;
                    varValue.vValue.longlongVal = lround(varInfo->value.stdValue.ul * scaling_fact);
                }
            }
        }
        break;
    case vT_Enumerated:
    case vT_BitEnumerated:
        varValue.vType = varValue.valueType_t::isVeryLong;
        varValue.vValue.longlongVal = varInfo->value.stdValue.ul;
        break;
    case vT_HartDate:
    {
        string dateString = m_pSDC625->convertsEpochTimeToUserTime(
                    varInfo->value.stdValue.ul, DATE_DATA_FORMAT);
        wstring str(dateString.length(), L' ');
        std::copy(dateString.begin(), dateString.end(), str.begin());
        hChartDate* var = (hChartDate*)m_pSDC625->getVariablePtr(varInfo->itemID);
        UINT32 wrtVal = var->str2int(str);
        varValue.vType = varValue.valueType_t::isVeryLong;
        varValue.vValue.longlongVal = wrtVal;
    }
        break;
    case vT_Ascii:
    case vT_PackedAscii:
    case vT_Password:
    case vT_BitString:
    case vT_EUC:
    case vT_OctetString:
    case vT_VisibleString:
        varValue.vType = varValue.valueType_t::isString;
        if(nullptr != varInfo->value.strData.str)
        {
            varValue.sStringVal.assign(varInfo->value.strData.str);
        }

        //deallocates memory allocated by adaptor
        if(varInfo->value.strData.flag == FREE_STRING &&
                nullptr != varInfo->value.strData.str)
        {
            delete[] varInfo->value.strData.str;
            varInfo->value.strData.str = nullptr;
            varInfo->value.strData.len = 0;
            varInfo->value.strData.flag = DONT_FREE_STRING;
        }
        break;
    default:
        errorCode = FAILURE;
        break;
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This function is used to clean the paramInfo members
*/
void HARTParameterCache::cleanParamInfo(ParamInfo* paramInfo)
{
    for(auto item : paramInfo->paramList)
    {
        auto varItem = dynamic_cast<VariableType*>(&item.get());

        if (nullptr != varItem)
        {
            //cleans the memory allocated to string types
            releaseStringValue(varItem->label);
            releaseStringValue(varItem->help);
            releaseStringValue(varItem->editFormat);
            releaseStringValue(varItem->displayFormat);
            releaseStringValue(varItem->unit);
            releaseStringValue(varItem->value.strData);

            //releases the memory allocated to enum/bitenum or index type members
            if (vT_Index == varItem->type || vT_Enumerated == varItem->type
                    || vT_BitEnumerated == varItem->type)
            {
                for(auto item : varItem->enums)
                {
                    releaseStringValue(item.label);
                    releaseStringValue(item.help);
                }
            }

            if (true == varItem->isMemAllocated)
            {
                delete varItem;
                varItem = nullptr;
            }
        }
    }

    //clears the param info list
    paramInfo->paramList.clear();
}

/*  ----------------------------------------------------------------------------
    This function is used to release memory allocated to string type
*/
void HARTParameterCache::releaseStringValue(StringType& strType)
{
    //deallocates memory allocated to string types
    if(strType.flag == FREE_STRING &&
            nullptr != strType.str)
    {
        delete[] strType.str;
        strType.str = nullptr;
        strType.len = 0;
        strType.flag = DONT_FREE_STRING;
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to get the display date
*/
RETURNCODE HARTParameterCache::getDispDate(UINT32 wrtVal, UINT32& day,
                                           UINT32& month, UINT32& year)
{
    year  =  wrtVal & DATE_INIT_VALUE;
    day   = (wrtVal >> CHAR_ARRAY_LENGTH) & DATE_INIT_VALUE;
    month = (wrtVal >> (DATE_STR_LENGTH + DATENTIME_ARRAY_LENGTH)) & DATE_INIT_VALUE;

    year += MIN_VAL_OF_YEAR;
    if (month > MAX_VAL_OF_MONTH)
    {
        year = month = day = DATENTIME_NOT_INIT;
        return FAILURE;
    }
    if (day > MAX_VAL_OF_DAY)
    {
        year = month = day = DATENTIME_NOT_INIT;
        return FAILURE;
    }
    return SUCCESS;
}
