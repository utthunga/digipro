/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sethu
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    SubscriptionController class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "SubscriptionController.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    SubscriptionController class instance initialization
*/
SubscriptionController::SubscriptionController() : m_sequenceID(0)
{
    // Resets the subscription id to 0
    m_idGenerator.reset();
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
SubscriptionController::~SubscriptionController()
{
    deleteAllSubscription();
}

/*  ----------------------------------------------------------------------------
    Create subscription that genarate Subscription ID and add it to
    subscription map
*/
PMStatus SubscriptionController::createSubscription(ParamInfo monitorItems,
                                   uint refreshRate,
                                   bool deviceRead,
                                   DataChangedCallback dataChangedcallback,
                                   SubscriptionTimerCallback notifier,
                                   uint& subscriptionID,
                                   SubscriptionType subscriptionType)
{
    PMStatus status;
    uint newSubscriptionID = m_idGenerator.getId();

    Subscription *subscription = new Subscription(
                            newSubscriptionID,
                            subscriptionType,
                            monitorItems,
                            refreshRate,
                            deviceRead,
                            dataChangedcallback,
                            notifier);

    subscriptionID = newSubscriptionID;

    if (subscriptionID > 0)
    {
        m_Subscription.push_back(subscription);
    }
    else
    {
        status.setError(PC_ERR_CREATE_SUBSCRIPTION_FAILED,
                        PM_ERR_CL_CSL, CSL_ERR_SC_SBC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Find the subscription from the list and start the subscription
*/
PMStatus SubscriptionController::startSubscription(uint subscriptionID)
{
    PMStatus status;
    std::vector<Subscription*>::iterator it;

    for (it = m_Subscription.begin();it != m_Subscription.end(); ++it)
    {
        if (subscriptionID == (*it)->getSubscriptionID())
        {
            m_subIDlist.push_back(subscriptionID);
            (*it)->startSubscription();
            break;
        }
    }

    if (it == m_Subscription.end())
    {
        status.setError(FF_PC_ERR_SUBSCRIPTION_ID_NOT_FOUND,
                        PM_ERR_CL_FF_CSL, CSL_ERR_SC_SBC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Find the subscription from the list and stop the subscription
*/
PMStatus SubscriptionController::unsubscribe(uint subscriptionID)
{
    PMStatus status;
    std::vector<Subscription*>::iterator it;

    for (it = m_Subscription.begin() ; it != m_Subscription.end(); ++it)
    {
        if (subscriptionID == (*it)->getSubscriptionID())
        {
            m_subIDlist.remove(subscriptionID);
            (*it)->stopSubscription();
            break;
        }
    }

    if (it == m_Subscription.end())
    {
        status.setError(FF_PC_ERR_SUBSCRIPTION_ID_NOT_FOUND,
                        PM_ERR_CL_FF_CSL, CSL_ERR_SC_SBC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Stops all the subscription internally to prevent access of the device
*/
void SubscriptionController::pauseSubscription()
{
    std::list<int>::iterator it1;
    std::vector<Subscription*>::iterator it;

    for (it = m_Subscription.begin();it != m_Subscription.end(); ++it)
    {
        for (it1 = m_subIDlist.begin(); it1 != m_subIDlist.end(); ++it1)
        {
            if (*it1 == (*it)->getSubscriptionID())
            {
                (*it)->stopSubscription();
                break;
            }
        }
    }
}

/*  ----------------------------------------------------------------------------
    Restarts all the subscription which stopped internally
*/
void SubscriptionController::resumeSubscription()
{
    std::list<int>::iterator it1;
    std::vector<Subscription*>::iterator it;

    for (it = m_Subscription.begin();it != m_Subscription.end(); ++it)
    {
        for (it1 = m_subIDlist.begin(); it1 != m_subIDlist.end(); ++it1)
        {
            if (*it1 == (*it)->getSubscriptionID())
            {
                (*it)->startSubscription();
                break;
            }
        }
    }
}

/*  ----------------------------------------------------------------------------
    Stop subscription and delete the subscription from the list
*/
PMStatus SubscriptionController::deleteSubscription(uint subscriptionID)
{
    PMStatus status;
    std::vector<Subscription*>::iterator it;

    // Identify the subscription to delete for the given subscription ID
    for (it = m_Subscription.begin();it != m_Subscription.end(); ++it)
    {
        if (subscriptionID == (*it)->getSubscriptionID())
        {
            break;
        }
    }

    // stop and delete the subscriptions
    if(it != m_Subscription.end())
    {
        (*it)->stopSubscription();
        (*it)->cleanSubscriptionMembers();
         if (nullptr != *it)
         {
             delete (*it);
             (*it) = nullptr;
         }
         m_Subscription.erase(it);
    }
    else
    {
        status.setError(FF_PC_ERR_SUBSCRIPTION_ID_NOT_FOUND,
                        PM_ERR_CL_FF_CSL, CSL_ERR_SC_SBC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Stop all subscription and Clear the subscription list
*/
PMStatus SubscriptionController::deleteAllSubscription(
        SubscriptionType subscriptionType)
{
    PMStatus status;

    if (m_Subscription.size() > 0 )
    {
        // If there is no specific type mentioned, delete all subscription
        // Otherwise, delete only the subscriptions of specified type
        if (subscriptionType == SUBSCRIPTION_TYPE_NONE)
        {
            //Stop subcription and clean cloned monitored items
            for (std::vector<Subscription*>::iterator it = m_Subscription.begin();
                 it != m_Subscription.end(); ++it)
            {
                (*it)->stopSubscription();
                (*it)->cleanSubscriptionMembers();

                if(nullptr != *it)
                {
                   delete (*it);
                   (*it) = nullptr;
                }
            }

            m_Subscription.clear();
        }
        else
        {                   
            std::vector<Subscription*>::iterator it;

            //Identify the subscriptions to delete
            for (it = m_Subscription.begin();
                 it != m_Subscription.end();)
            {
                if (subscriptionType == (*it)->getSubscriptionType())
                {
                    (*it)->stopSubscription();
                    (*it)->cleanSubscriptionMembers();

                    if(nullptr != *it)
                    {
                       delete (*it);
                       (*it) = nullptr;
                    }

                    it = m_Subscription.erase(it);
                }
                else
                {
                    ++it;
                }
            }           
        }
    }

    return status;
}


/*  ----------------------------------------------------------------------------
    Retrieves all the subscription members
*/
std::vector<Subscription*> SubscriptionController::getAllSubscription()
{
   return m_Subscription;
}

/*  ----------------------------------------------------------------------------
    Retrieves the subscription items for the given subscritpion ID
*/
ParamInfo SubscriptionController::getMonitorItems(uint subscriptionID)
{
    ParamInfo paramInfo{};

    if (m_Subscription.size() > 0 )
    {
        std::vector<Subscription*>::iterator it;

        for (it = m_Subscription.begin();
             it != m_Subscription.end(); ++it)
        {
            if (subscriptionID == (*it)->getSubscriptionID())
            {
                 break;
            }
        }

        if (it != m_Subscription.end())
        {
            paramInfo = (*it)->getMonitorItems();
        }
    }

    return paramInfo;
}

/*  ----------------------------------------------------------------------------
     Retrieves the subscription state for the given subscription ID
*/
Subscription* SubscriptionController::getSubscription(uint subscriptionID)
{
    std::vector<Subscription*>::iterator it;

    if (m_Subscription.size() > 0 )
    {
        for (it = m_Subscription.begin();
             it != m_Subscription.end(); ++it)
        {
            if (subscriptionID == (*it)->getSubscriptionID())
            {
                 return (*it);
            }
        }
     }

    return nullptr;
}
