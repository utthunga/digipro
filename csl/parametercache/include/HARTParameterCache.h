/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    HARTParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _HART_PARAMETER_CHACHE_H
#define _HART_PARAMETER_CHACHE_H

/* INCLUDE FILES **************************************************************/

#include <list>
#include "IConnectionManager.h"
#include "IParameterCache.h"
#include "ParameterCache.h"

#include "varient.h"
#include "ddbVar.h"
#include "ddbImage.h"

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of SDC625Controller
*/
class SDC625Controller;
class HARTAdaptor;

/*============================================================================*/
/** HARTParameterCache Class Definition.

    HARTParameterCache class implements the Parameter
    Cache mechanism for HART protocol.
*/

class HARTParameterCache : public IParameterCache, public ParameterCache
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a HARTParameterCache
    HARTParameterCache();

    /*------------------------------------------------------------------------*/
    /// Destruct a HARTParameterCache
    ~HARTParameterCache();

    /*------------------------------------------------------------------------*/
    /** Initialize the HART Parameter Cache with SDC625Controller and
        IConnectionManager instances

        @param sdc - SDC625Controller instance
        @param cm - IConnectionManager instance

        @return status
    */
    PMStatus initialize(SDC625Controller* sdc, IConnectionManager* cm);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from cache, if not found then read the device value
        for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus getParameter(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from cache, if not found then read the device value
        for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given value to cache as well as to the devcie

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from device for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
     /** Retrives the value from device for the requested parameter

        @param paramInfo - contains list of params to be read from device
                            and updates the cache
        @param dynamicVars - if it is set to true, only dynamic variables
                            will be read, otherwise static variables
        @return status
    */

    PMStatus updateCacheTable(ParamInfo paramInfo, bool dynamicVars);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from device for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus asyncReadDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given edit value to device

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Performs subscription actions upon subscription timer elapsed

        @param paramInfo - contains subscribed members
        @param readFromDevice - if true, subscribed parameters will be read
               from device, otherise reads from cache

        @return status
    */
    PMStatus doSubscriptionActions(	ParamInfo& paramInfo,
            					bool readFromDevice);

    /*------------------------------------------------------------------------*/
    /** Performs pre subscription actions upon subscription timer elapsed

        @param paramInfo - contains subscribed members
        @param readFromDevice - if true, subscribed parameters will be read
               from device, otherise reads from cache

        @return status
    */
    PMStatus preSubscriptionActions(ParamInfo& paramInfo, bool readFromDevice);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill the variable to item info

        @param item - variable
        @param itemInfo - item info

        @return status
    */
    void setValuesToParamInfo(CValueVarient item, VariableType *variable);

    /*------------------------------------------------------------------------*/
    /** Allocates memory for char pointer and asssigns string to it.

        @param variable - string information to be filled.
        @param str - contains one of the variable information
        @param maxLength - contains defined max length for variable

     */
    void assignStringValue(StringType& variable, std::string str,
                           int maxLength = 0);

    /*------------------------------------------------------------------------*/
    /** This function is to release the StringType variable memory

     *  @param strType - contains string type to be deleted
     */
    void releaseStringValue(StringType& strType);


    /*------------------------------------------------------------------------*/
    /** cleans the paraminfo

        @param paramInfo - contains members of iteminfo
    */
    void cleanParamInfo(ParamInfo* paramInfo);

    /*= Explicitly Disabled Methods ==========================================*/
    HARTParameterCache& operator=(const HARTParameterCache& rhs) = delete;
    HARTParameterCache(const HARTParameterCache& rhs) = delete;

private:
    /* Private member functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /** This function is used to fill the variable to item info

        @param item - variable
        @param itemInfo - item info

        @return status
    */
    PMStatus generateFlatItem(void *paramItem, ItemInfoType* itemInfo,
                                                          MaskType mask);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill the image to parameter info

        @param item - image
        @param paramInfo - parameter info
        @return status
    */
    PMStatus fillImageInformation(hCimageItem* item, ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill the variable min max values

        @param varItem - contains item information
        @param misc - min max value to be filled
    */
    void fillMinMaxValue(hCVar* varItem, Misc& misc);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill the variable info to variable value

        @param varInfo - variable info
        @param varValue - variable value
        @return status
    */
    int getValuesFromParamInfo(VariableType* varInfo, CValueVarient &varValue);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill index type variable information

     * @param itemIndex - contains index item information
     * @param variable - index item info to be filled in this structure
     *
     */
    void fillIndexItemInformation(hCindex* itemIndex, VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill minimal variable information

     * @param itemIndex - contains index item information
     * @param variable - index item info to be filled in this structure
     *
     */
    void fillMinimumVarInformation(hCVar* item, VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** This function is used to fill enum type variable information

     * @param varEnum - contains enum item information
     * @param variable - enum item info to be filled in this structure
     *
     */
    void fillEnumItemInformation(hCEnum* varEnum, VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** This function is used to check for supplied value is valid or not

     *  @param varItem - contains variable info with value
     */
    bool verifyIsValidValue(VariableType *varItem);

    /*------------------------------------------------------------------------*/
    /** This function is used to check for supplied Bit Index is valid or not

     *  @param varItem - contains variable info with value
     */
    bool verifyIsValidBitIndex(VariableType* varItem);

    /*------------------------------------------------------------------------*/
    /** This function is used to check given bit enumeration value is valid or not
     *
     *  @param list - contains bit enumeration list with value
     *  @param size - contains bit enumeration list size
     *  @param value - contains bit enumeration input value
     */
    bool isValidValue(UIntList_t list, int size, int value);

    /*------------------------------------------------------------------------*/
    /** This function is used to get the display date

     *  @param wrtVal - contains date value
     *  @param day    - contains day value
     *  @param month  - contains month value
     *  @param year   - contains year value
     */
    RETURNCODE getDispDate(UINT32 wrtVal, UINT32& day, UINT32& month, UINT32& year);

    /* Private Data Members ==================================================*/
    /// DDS Controller instance
    SDC625Controller* m_pSDC625;

    /// Connection Manager instance
    IConnectionManager* m_pCM;

    /// HART Adaptor instance
    HARTAdaptor* m_pHartAdaptor;
};

#endif  // _HART_PARAMETER_CHACHE_H
