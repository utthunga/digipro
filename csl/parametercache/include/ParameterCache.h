/*******************************************************************************
 Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
 ********************************************************************************

 Repository URL:    https://bitbucket.org/flukept/digipro.git
 Authored By:       Bharath SR
 Origin:            ProStar - Utthunga Technologies
 */

/** @file
 Parameter Cache class definitions
 */

/*******************************************************************************
 Use of the software source code and warranty disclaimers are
 identified in the Software Agreement associated herewith.
 *******************************************************************************/

#ifndef _PARAMETER_CHACHE_H
#define _PARAMETER_CHACHE_H

#define DDI_INVALID_PARAM (1700  + 5)

/* INCLUDE FILES **************************************************************/

#include <functional>
#include <vector>
#include "PMErrorCode.h"
#include "PMStatus.h"
#include <map>
#include <mutex>

#define UC_ASCII        0x00
#define UC_ISO_LATIN_1  0x01
#define UC_UTF_8        0x02

#define BIT_REV_ALLOC_SIZE 8

#define INVALID_ID_OR_HANDLE   (-1)
#define DATENTIME_ARRAY_LENGTH (8)
#define TIMEVAL_ARRAY_LENGTH   (4)


/// Forward decalration
class Subscription;

/* STRUCTURES and ENUMS DECLARATIONS ******************************************/
/*----------------------------------------------------------------------------*/
/// Mask type definitions
enum MaskType 
{
	ITEM_ATTR_MIN_MASK = 0, // Get minimum parameter information
	ITEM_ATTR_ALL_MASK,  // Get complete parameter information
	ITEM_ATTR_SUBSCRIPTION_MASK // Get the information for subscription
};

/// Enumeration defines for subscription type
enum SubscriptionType
{
    SUBSCRIPTION_TYPE_NONE = 0,
    SUBSCRIPTION_TYPE_INTERNAL,
    SUBSCRIPTION_TYPE_EXTERNAL
};

enum ValueState 
{
	VALUE_NOT_READ = 0, 
    VALUE_READ = 1
};

enum TimeValType 
{
	TIME_VALUE_INVALID = 0, 
    TIME_VALUE_SCALED, 
    TIME_VALUE_DIFF
};

/*----------------------------------------------------------------------------*/
/// Mask type definitions
typedef struct 
{
	char* str;             // Pointers to the string character array
	unsigned short len;    // Character array length
	unsigned short flag;   // Memory Free flag
	unsigned short ucType; // Used to set unicode type
} StringType;

/*----------------------------------------------------------------------------*/
/// Enum type definitions
enum EnumType 
{
	ENUM_DEFAULT, 
    ENUM_BIT
};

/*----------------------------------------------------------------------------*/
/// Enumeration/bit_enum value structure
typedef struct 
{
	unsigned long action;           // Action items
	EnumType enumType;         // enumType (Default/Bit)
	StringType label {};            // Variable label
	StringType help {};             // Variable help string
	unsigned long value;            // Value for enum string
	unsigned long functionalClass;  // Function class
	unsigned long status;
} EnumValue;

/*----------------------------------------------------------------------------*/
/// Item type definitions
enum ParamItemType 
{
	PARAM_VARIABLE, // Varaiable list
	PARAM_ARRAY,    // Array
	PARAM_DEFAULT,  // Defaul type
	PARAM_RECORD,   // Record
	PARAM_IMAGE     // Image type
};

/*----------------------------------------------------------------------------*/
/// Basic item Info
typedef struct Item_Info 
{
	//Constructor with no argument
	Item_Info() :
			itemID(0), 
            itemType(PARAM_DEFAULT), 
            response_code(DDI_INVALID_PARAM), 
            isMemAllocated(false) {}

	//Constructor with item id argument
	Item_Info(unsigned long id) :
			itemID(id), 
            itemType(PARAM_DEFAULT), 
            response_code(DDI_INVALID_PARAM) {}

	//Clone object
	virtual Item_Info* clone() const 
    {
		return (new Item_Info(*this));
	}

	//Destructor
	virtual ~Item_Info() {}

	StringType itemContainerTag {};
	unsigned long itemID;
	ParamItemType itemType;
	StringType label {};
	StringType help {};
	StringType name {};
	unsigned long response_code;
	StringType response_label {}; //TODO need to support
	bool isMemAllocated;
	unsigned long validity;
} ItemInfoType;

/*----------------------------------------------------------------------------*/
/// standard value union structure
typedef union Std_Value 
{
	double d;         // Double Value
	float f;         // Float Value
	long l;         // Long Value
	unsigned long ul;        // Unsigned long Value
} StdValue;

/*----------------------------------------------------------------------------*/
/// Item value  structure
typedef struct Item_Value 
{
	//Constructor with no argument
	Item_Value() 
    {
		stdValue.d = stdValue.f = stdValue.l = stdValue.ul = 0;
	}

	//Destructor
	virtual ~Item_Value() 
    {
		// TODO needs to be checked whether this str is cleared
	}

	char CA[DATENTIME_ARRAY_LENGTH] {};     // Used for date, datetime, time
	unsigned short SA[TIMEVAL_ARRAY_LENGTH] {}; // used for TIME_VAL
	StdValue stdValue {};  // Std Value such int, float,long...
	StringType strData {};   // Non std string Values such ASCII, packed...
} ItemValue;

/*----------------------------------------------------------------------------*/
/// Misc  structure
typedef struct min_max_range 
{
	StdValue min {};
	StdValue max {};
} MinMaxRange;

/*----------------------------------------------------------------------------*/
/// Misc  structure
typedef struct misc 
{
	//Constructor with no argument
	misc() :
			class_attr(0), 
            handling(0), 
            visibility(0), 
            isMaxPresent(false), 
            isMinPresent(false), 
            timeValType(TIME_VALUE_INVALID) 
    {}

	//Destructor
	virtual ~misc() 
    {
		minMaxRange.clear();
	}

	unsigned long class_attr;
	unsigned long handling;
	std::vector<MinMaxRange> minMaxRange {};
	unsigned int visibility;
	bool isMaxPresent;
	bool isMinPresent;
	TimeValType timeValType;
} Misc;

/*----------------------------------------------------------------------------*/
/// Image variable type structure
typedef struct Image_Type: ItemInfoType 
{
	//Constructor with no argument
	Image_Type() 
    {
		itemID = 0;
		itemType = PARAM_IMAGE;
	}

	//Constructor with item id argument
	Image_Type(unsigned long id) 
    {
		itemID = id;
		itemType = PARAM_IMAGE;
		response_code = DDI_INVALID_PARAM;
	}
	//Clone object
	virtual Image_Type* clone() const 
    {
		return (new Image_Type(*this));
	}
	//Destructor
	virtual ~Image_Type() {}

	StringType imagePath {};         // image path;
	unsigned long length;              // binary image length
	unsigned char* data;                // binary image data
} ImageType;

/*----------------------------------------------------------------------------*/
/// Variable value structure
typedef struct Variable_Type: ItemInfoType 
{
	//Constructor with no argument
	Variable_Type() :
			containerId(0), size(0), subIndex(0), type(0) 
    {
		itemID = 0;
		itemType = PARAM_VARIABLE;
		response_code = DDI_INVALID_PARAM;
		valueChanged = false;
		valueState = VALUE_NOT_READ;
	}

	//Constructor with item id argument
	Variable_Type(unsigned long id) :
			containerId(0), size(0), subIndex(0), type(0) 
    {
		itemID = id;
		itemType = PARAM_VARIABLE;
		response_code = DDI_INVALID_PARAM;
		valueChanged = false;
		valueState = VALUE_NOT_READ;
	}
	//Clone object
	virtual Variable_Type* clone() const 
    {
		return (new Variable_Type(*this));
	}

	//Destructor
	virtual ~Variable_Type() 
    {
		enums.clear();
	}
	unsigned long containerId;    // id of record/array
	StringType displayFormat {};
	StringType editFormat {};
	std::vector<EnumValue> enums;
	Misc misc {};
	unsigned short size;          // Size of variable
	unsigned short subIndex {};
	unsigned long bitIndex {};
	unsigned short type;          // Variable data type
	StringType unit {};
	ItemValue value;
	bool valueChanged;
	ValueState valueState;  //indicates variable read from device or not
    unsigned int milliSeconds; // Holds the value of milliseconds in case of time value
    unsigned int microSeconds; // Holds the value of microseconds in case of time value
} VariableType;

/*----------------------------------------------------------------------------*/
/// Record value structure
typedef struct Record_Type: ItemInfoType 
{
	//Constructor with no argument
	Record_Type() 
    {
		itemID = 0;
		subIndex = 0;
		itemType = PARAM_RECORD;
		response_code = DDI_INVALID_PARAM;
	}

	//Constructor with item id argument
	Record_Type(unsigned long id) 
    {
		itemID = id;
		subIndex = 0;
		itemType = PARAM_RECORD;
		response_code = DDI_INVALID_PARAM;
	}
	//Clone object
	virtual Record_Type* clone() const 
    {
		return (new Record_Type(*this));
	}
	//Destructor
	virtual ~Record_Type() 
    {
		members.clear();
	}
	//Used to fetch specic item at index from the member list
	unsigned short subIndex;

	//  Record member is a collection of iteminfo
	std::vector<std::reference_wrapper<ItemInfoType>> members {};
} RecordType;

/*----------------------------------------------------------------------------*/
/// Array value structure
typedef struct Array_Type: ItemInfoType 
{
	//Constructor with no argument
	Array_Type() 
    {
		itemID = 0;
		subIndex = 0;
		itemType = PARAM_ARRAY;
		response_code = DDI_INVALID_PARAM;
	}

	//Constructor with item id argument
	Array_Type(unsigned long id) 
    {
		itemID = id;
		subIndex = 0;
		itemType = PARAM_ARRAY;
		response_code = DDI_INVALID_PARAM;
	}

	//Clone object
	virtual Array_Type* clone() const 
    {
		return (new Array_Type(*this));
	}

	//Destructor
	virtual ~Array_Type() 
    {
		members.clear();
	}
	//Used to fetch specic item at index from the member list
	unsigned short subIndex;

	// it could be variable, value,record etc
	unsigned long arrayItemType;
	//  array member is a varaible list
	std::vector<std::reference_wrapper<ItemInfoType>> members {};
} ArrayType;

/*----------------------------------------------------------------------------*/
/// Request Mask structure
typedef struct 
{
	MaskType maskType;  /// Request Mask type of item
} RequestMask;

/*----------------------------------------------------------------------------*/
/// Parameter info structure
typedef struct param_info 
{
	param_info() {}
	virtual param_info clone() 
    {
		param_info paramInfo = *this;
		paramInfo.paramList.clear();
		for (auto& paramItemRef : (*this).paramList) 
        {
			auto paramItem = &paramItemRef.get();
			paramInfo.paramList.push_back(*paramItem->clone());
		}
		return paramInfo;
	}
	//Destructor
	virtual ~param_info() {}

	StringType blockTag {};   // block Tag
	std::vector<std::reference_wrapper<ItemInfoType>> paramList {}; // Parameter List
	RequestMask requestInfo {};  // Request Mask type of item
} ParamInfo;

/*----------------------------------------------------------------------------*/
typedef std::function<void(uint, ParamInfo*)> DataChangedCallback;
typedef std::function<void(Subscription*, bool, DataChangedCallback)> SubscriptionTimerCallback;

/* CLASS DECLARATIONS *********************************************************/

///forward Decelaration
class SubscriptionController;

/*============================================================================*/
/** ParameterCache Class Definition.
 ParameterCache class defines parameter operations.
 */

class ParameterCache 
{
public:
	/* Construction and Destruction ==========================================*/
	/*------------------------------------------------------------------------*/
	/** Destruct a ParameterCache
	 Destructs the ParameterCache class and deallocates the member
	 variables
	 */
	virtual ~ParameterCache();

	/*------------------------------------------------------------------------*/
	/** Reads the parameter value from cache table

	 @param paramInfo - contains parameter info

	 @return status
	 */
	virtual PMStatus readValue(ParamInfo* paramInfo) = 0;

	/*------------------------------------------------------------------------*/
	/** Reads the parameter value from device

	 @param paramInfo - contains parameter info

	 @return status
	 */
	virtual PMStatus readDeviceValue(ParamInfo* paramInfo) = 0;

	/*------------------------------------------------------------------------*/
	/** Create subscription that genarate Subscription ID

	 @param monitorItems - contains monitoring items
	 @param refreshRate - contains refresh rate to update
	 @param refreshRate - flag indicates to read from device or cache
	 @param dataChangedCallback - contains callback function
	 @param subscriptionID - contains subscription ID
     @param subscriptionType - indicates subscription type internal or external

	 @return status
	 */
	PMStatus createSubscription(ParamInfo monitorItems, uint refreshRate,
            bool deviceRead, DataChangedCallback dataChangedCallback,
            uint& subscriptionID, SubscriptionType subscriptionType);

	/*------------------------------------------------------------------------*/
	/** Subscription timer elapsed callback

	 @param subscriptionID - contains subscription ID
	 @param monitorItem - contains monitoring items
	 @param dataChangedCallback - contains callback function
	 */
	void onSubscriptionTimerElapsed(uint subscriptionID, ParamInfo* monitorItem,
			DataChangedCallback dataChangedCallback);

	/*------------------------------------------------------------------------*/
	/** Start the subscription by ID

	 @param subscriptionID - contains subscription ID

	 @return status
	 */
	PMStatus startSubscription(uint subscriptionID);

	/*------------------------------------------------------------------------*/
	/** Unsubscribe the subscription

	 @param subscriptionID - contains subscription ID

	 @return status
	 */
	PMStatus unsubscribe(uint subscriptionID);

	/*------------------------------------------------------------------------*/
	/** Delete Subscription by ID

	 @param subscriptionID - contains subscription ID

	 @return status
	 */
	PMStatus deleteSubscription(uint subscriptionID);

	/*------------------------------------------------------------------------*/
	/** Delete all Subscriptions

	 @return status
	 */
        PMStatus deleteAllSubscription(SubscriptionType subscriptionType);

	/*------------------------------------------------------------------------*/
	/** Performs internal subscription actions

	 @return status
	 */
	virtual PMStatus doSubscriptionActions(ParamInfo& paramInfo,
            bool readFromDevice) = 0;

    	/*------------------------------------------------------------------------*/
    	/** Performs pre subscription actions upon subscription timer elapsed

     	 @param paramInfo - contains subscribed members
      	 @param readFromDevice - if true, subscribed parameters will be read
               from device, otherise reads from cache

     	 @return status
     	 */
     	virtual PMStatus preSubscriptionActions(ParamInfo& paramInfo,
                                    bool readFromDevice) = 0;

	/*------------------------------------------------------------------------*/
	/** Pauses the subscription to prevent accessing device

	 @return none
	 */
	void pauseSubscription();

	/*------------------------------------------------------------------------*/
	/** Resumes the subcription lock

	 @return none
	 */
	void resumeSubscription();

    	/*------------------------------------------------------------------------*/
   	/** Retrieves subscription controller instance

    	 @return returns SubscriptionController instance
    	*/
   	SubscriptionController* getSubscriptionController();

	/*------------------------------------------------------------------------*/
	/// It aquires the thread lock and wait till release
	void acquireLock();

	/*------------------------------------------------------------------------*/
	/// It releases the locked thread and notify for further process
	void releaseLock();

	/*= Explicitly Disabled Methods ==========================================*/
	ParameterCache& operator=(const ParameterCache& rhs) = delete;
	ParameterCache(const ParameterCache& rhs) = delete;

protected:
	/* Construction and Destruction ==========================================*/
	/*------------------------------------------------------------------------*/
	/** Construct a ParameterCache
	 Constructs the ParameterCache class and initializes the member
	 variables
	 */
	ParameterCache();

private:
	/* Private Data Members ==================================================*/
	/// Sequence ID for parameter cache
	int m_sequenceID;

	/// Subscription controller instance
	SubscriptionController *m_pSubscriptionController;

	/// Mutex Thread Lock
	std::mutex m_readLock;
};

#endif  // _PARAMETER_CHACHE_H
