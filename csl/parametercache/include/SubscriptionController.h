/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FFParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _SUBSCRIPTION_CONTROLLER_H
#define _SUBSCRIPTION_CONTROLLER_H

/* INCLUDE FILES **************************************************************/

#include <iostream>
#include <vector>
#include "IParameterCache.h"
#include "Subscription.h"
#include "IDGenerator.h"

/* CLASS DECLARATIONS *********************************************************/
/*============================================================================*/
/** SubscriptionController Class Definition.

    SubscriptionController class implements the subscription controller
    methodology for subscription
*/

class SubscriptionController
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a IDGenerator
    SubscriptionController();

    /*------------------------------------------------------------------------*/
    /// Destruct a IDGenerator
    virtual ~SubscriptionController();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Create subscription that genarate Subscription ID
     *
        @param monitorItems - contains monitoring items
        @param refreshRate - contains refresh rate
        @param deviceRead - flag indicates to read from cache or device
        @param dataChangedcallback - contains callback function for data change
        @param notifier - contains notifier to update
        @param subscriptionID - contains subscription ID
        @param subscriptionType - indicates EXTERNAL or INTERNAL subscription

        @return status
    */
    PMStatus createSubscription(ParamInfo monitorItems,
                                uint refreshRate,
                                bool deviceRead,
                                DataChangedCallback dataChangedcallback,
                                SubscriptionTimerCallback notifier,
                                uint& subscriptionID,
                                SubscriptionType subscriptionType);

    /*------------------------------------------------------------------------*/
    /** Start the subscription by ID
     *
        @param subscriptionID - contains subscription ID

        @return status
    */
    PMStatus startSubscription(uint subscriptionID);

    /*------------------------------------------------------------------------*/
    /** Unsubscribe the subscription
     *
        @param subscriptionID - contains subscription ID

        @return status
    */
    PMStatus unsubscribe(uint subscriptionID);

    /*------------------------------------------------------------------------*/
    /** Delete Subscription by ID
     *
        @param subscriptionID - contains subscription ID

        @return status
    */
    PMStatus deleteSubscription(uint subscriptionID);

    /*------------------------------------------------------------------------*/
    /** Delete all Subscriptions

        @param subscriptionType - indicates subscription type EXTERNAL or INTERNAL

        @return status
    */
    PMStatus deleteAllSubscription(SubscriptionType subscriptionType =
            SUBSCRIPTION_TYPE_NONE);

    /*------------------------------------------------------------------------*/
    /** Stops all the subscription internally to prevent access of the device

        @return none
    */
    void pauseSubscription();

    /*------------------------------------------------------------------------*/
    /** Restarts all the subscription which stopped internally

        @return none
    */
    void resumeSubscription();

    /*------------------------------------------------------------------------*/
    /** Retrieves all the subscription members

        @return returns all the subscription members
    */
    std::vector<Subscription*> getAllSubscription();

    /*------------------------------------------------------------------------*/
    /** Retrieves the subscription items for the given subscription ID

        @return returns the subscription items
    */
    ParamInfo getMonitorItems(uint subscriptionID);

    /*------------------------------------------------------------------------*/
    /** Retrieves the subscription instance for the given subscription ID

        @return returns the subscription items
    */
    Subscription* getSubscription(uint subscriptionID);

    /// Holds the subcription ID list
    std::list<int> m_subIDlist;

    /*= Explicitly Disabled Methods ==========================================*/
    SubscriptionController& operator = (const SubscriptionController& rhs) = delete;
    SubscriptionController(const SubscriptionController& rhs) = delete;

private:
    /* Private Data Members ==================================================*/
    /// Subscription Map
    std::vector<Subscription*> m_Subscription;

    /// ID generator
    IDGenerator m_idGenerator;

    /// Sequence ID for subscription
    int m_sequenceID;
};

#endif  // _SUBSCRIPTION_CONTROLLER_H
