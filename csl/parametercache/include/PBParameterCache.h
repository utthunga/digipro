/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PBParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _PBPARAMETER_CHACHE_H
#define _PBPARAMETER_CHACHE_H

/* INCLUDE FILES **************************************************************/

#include <iostream>
#include <list>
#include "IParameterCache.h"
#include "ParameterCache.h"

#define MAX_UTF8_STR_LENGTH     1024

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of IFakController and IConnectionManager
*/
class IFakController;
class IConnectionManager;
class PBAdaptor;

/*============================================================================*/
/** PBParameterCache Class Definition.

    PBParameterCache class implements the Parameter
    Cache mechanism for ProfiBus protocol.
*/

class PBParameterCache : public IParameterCache, public ParameterCache
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a PBParameterCache
    PBParameterCache();

    /*------------------------------------------------------------------------*/
    /// Destruct a PBParameterCache
    virtual ~PBParameterCache();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the FF Parameter Cache with DDSController and
        IConnectionManager instances

        @param ifak    - IFakController instance
        @param cm      - IConnectionManager instance
        @param adaptor - PBAdaptor instance

        @return status
    */
    PMStatus initialize(IFakController* ifak, IConnectionManager* cm, PBAdaptor* adaptor);

    /*------------------------------------------------------------------------*/
    /** All the interfaces from IParameterCache to be implemented Retrives
        parameter info for the requested parameter,
        It is uplink responsibility to free ParamInfo structure memory


        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus getParameter(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from cache, if not found then read the device value
        for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given value to cache as well as to the devcie

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from device for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given edit value to device

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Performs internal subscription actions

        @return status
    */
    PMStatus doSubscriptionActions(ParamInfo& paramInfo,
                                   bool readFromDevice);

    /*------------------------------------------------------------------------*/
    /** Performs pre subscription actions upon subscription timer elapsed

        @param paramInfo - contains subscribed members
        @param readFromDevice - if true, subscribed parameters will be read
               from device, otherise reads from cache

        @return status
    */
    PMStatus preSubscriptionActions(ParamInfo& paramInfo, bool readFromDevice);

    /* Explicitly Disabled Methods ===========================================*/
    PBParameterCache& operator=(const PBParameterCache& rhs) = delete;
    PBParameterCache(const PBParameterCache& rhs) = delete;

private:
    /* private Member Functions ==============================================*/

    /* Private Data Members ==================================================*/
    /// IFak Controller instance
    IFakController* m_pIFakController;

    /// PB Adaptor instance
    PBAdaptor* m_pPBAdaptor;

    /// List of supported data type
    static std::map<int, bool> s_SupportedDataType;

    /// Connection Manager instance
    IConnectionManager* m_pCM;
};

#endif  // _PBPARAMETER_CHACHE_H
