/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sethu
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    IDGenerator class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _SUBSCRIPTION_H
#define _SUBSCRIPTION_H

/* INCLUDE FILES **************************************************************/

#include <list>
#include <iostream>
#include "IParameterCache.h"
#include "ParameterCache.h"
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "ThreadManager.h"
#include <condition_variable>

using namespace std;

#define DONT_FREE_STRING  0x00
#define FREE_STRING       0x01

/** Defines the subscription thread state taken from adaptor business logic
    TODO(Sethu): use same enum instead of new one
*/
typedef enum class ThreadState
{
    run = 0,
    stop = 1,
    sleep = 2,
} ThreadStateT;

/* CLASS DECLARATIONS *********************************************************/
/*============================================================================*/
/** Subscription Class Definition.

    Subscription class implements the subsription methodology.
*/

class Subscription
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a Subscription
    Subscription(uint subscriptionID,
                SubscriptionType subscriptionType,
                ParamInfo& monitorItems,
                uint refreshRate,
                bool deviceRead,
                DataChangedCallback datachangecallback,
                SubscriptionTimerCallback notifier);

    /*------------------------------------------------------------------------*/
    /// Destruct a FFParameterCache
    virtual ~Subscription() = default;

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Starts the Subscription by starting thread

        @return result with TRUE or FALSE
    */
    bool startSubscription();

    /*------------------------------------------------------------------------*/
    /** Stops the Subscription by stopping thread

        @return result with TRUE or FALSE
    */
    bool stopSubscription();

    /*------------------------------------------------------------------------*/
    /** Retrieves the subscription ID

        @return subscrption ID
    */
    uint getSubscriptionID()
    {
        return m_subscriptionID;
    }

    /*------------------------------------------------------------------------*/
    /** Retrieves the refresh rate for subscription

        @return refresh rate
    */
    uint getRefreshRate()
    {
        return m_updateRate;
    }

    /*------------------------------------------------------------------------*/
    /** Retrieves the monitor items for subscription

        @return monitor items
    */
    ParamInfo getMonitorItems()
    {
        return m_monitoredItem;
    }

    /*------------------------------------------------------------------------*/
    /** Retrieves the subscription type internal or external

        @return subscription type
    */
    SubscriptionType getSubscriptionType()
    {
        return m_subscriptionType;
    }

    /*------------------------------------------------------------------------*/
    /** Retrieves the thread state

        @return thread state
    */
    THREAD_STATE getThreadState();

    /*------------------------------------------------------------------------*/
    /// Deallocate memory for subscritpion
    PMStatus cleanSubscriptionMembers();

    /*------------------------------------------------------------------------*/
    /// Sets the thread state
    void setThreadState(THREAD_STATE threadState);

    /*------------------------------------------------------------------------*/

    /*= Explicitly Disabled Methods ====================================*/
    Subscription& operator = (const Subscription& rhs) = delete;
    Subscription(const Subscription& rhs) = delete;

private:
    /* private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /** Entry point for timer Thread

        @param arg - contains subscription
    */
    void process(void* arg);

    /*------------------------------------------------------------------------*/
    /// Notify Subscription timeout
    void notifySubscriptionTimeout();

    /*------------------------------------------------------------------------*/
    /// It acquires the thread lock and wait till release
    void acquireLock();

    /*------------------------------------------------------------------------*/
    /// It releases the locked thread and notify for further process
    void releaseLock();

    /* Private Data Members ==================================================*/
    /// Subscription ID
    uint m_subscriptionID;

    /// Monitoring subscription item
    ParamInfo m_monitoredItem;

    /// Refresh rate to update
    uint m_updateRate;

    /// Indicates subscripton members are to be read from device/cache
    bool m_bDeviceRead;

    /// Callback function for update data
    DataChangedCallback m_dataChangeCallback;

    /// Notifier for callback
    SubscriptionTimerCallback m_notifer;

    /// Lock process for thread
    std::mutex m_subThreadIdLock;

    /// Lock process for thread
    std::mutex m_subProcessLock;

    /// Condition variable for handle thread
    condition_variable m_subscriptionCV;

    /// Scan thread id
    uint64_t m_subThreadID;

    /// Scan thread id
    SubscriptionType m_subscriptionType;

    bool m_isSubscripitonProcessed;
};

#endif  // _SUBSCRIPTION_H
