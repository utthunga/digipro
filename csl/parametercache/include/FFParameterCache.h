/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FFParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _FFPARAMETER_CHACHE_H
#define _FFPARAMETER_CHACHE_H

/* INCLUDE FILES **************************************************************/

#include <iostream>
#include <list>
#include "IParameterCache.h"
#include "ParameterCache.h"
#include "pc_loc.h"
#include "int_diff.h"
#include "int_meth.h"
#include "ddi_lib.h"

#define MAX_UTF8_STR_LENGTH    128 

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of DDSController and IConnectionManager
*/
class DDSController;
class IConnectionManager;
class FFAdaptor;

/*============================================================================*/
/** FFParameterCache Class Definition.

    FFParameterCache class implements the Parameter
    Cache mechanism for Foundation Fieldbus protocol.
*/

class FFParameterCache : public IParameterCache, public ParameterCache
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a FFParameterCache
    FFParameterCache();

    /*------------------------------------------------------------------------*/
    /// Destruct a FFParameterCache
    virtual ~FFParameterCache();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the FF Parameter Cache with DDSController and
        IConnectionManager instances

        @param dds - DDSController instance
        @param cm - IConnectionManager instance

        @return status
    */
    PMStatus initialize(DDSController* dds, IConnectionManager* cm, FFAdaptor* adaptor);

    /*------------------------------------------------------------------------*/
    /** All the interfaces from IParameterCache to be implemented Retrives
        parameter info for the requested parameter,
        It is uplink responsibility to free ParamInfo structure memory


        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus getParameter(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from cache, if not found then read the device value
        for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given value to cache as well as to the devcie

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the value from device for the requested parameter

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the given edit value to device

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeDeviceValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus readEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Writes the edit values from cache table

        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus writeEditValue(ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives the edit values from cache table

        @param paramInfo - contains subscribed parameters
        @param dynamicVars - if it is set to true, only dynamic variables
                            will be read, otherwise static variables
        @return status
    */
    PMStatus updateCacheTable(ParamInfo paramInfo, bool dynamicVars);

    /*------------------------------------------------------------------------*/
    /** clean the paraminfo items

        @param paramInfo - contains members of iteminfo
    */
    void cleanParamInfo(ParamInfo *paramInfo);

    /*------------------------------------------------------------------------*/
    /** Retrives parameter info for the requested block view
     *  It is uplink responsibility to free ParamInfo structure memory

        @param viewIndex - contains the index of view
        @param paramInfo - contains list of params with value

        @return status
    */
    PMStatus getParameterFromBlockView(int viewIndex, ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Function gives list of parameters from Block Characteristics

         * @param paramInfo - Contains list of params with values
         * @return SUCCESS or FAILURE
         */
    PMStatus getBlockCharacteristics(ParamInfo* paramInfo, int reqSubIndex = 0);

    /*------------------------------------------------------------------------*/
    /** Function gives list of parameters from Block Characteristics

         * @param blockTag - contains block tag information
         * @param blockCharID - contains the block characteristics ID
         * @return SUCCESS or FAILURE
         */
    PMStatus getBlockCharacteristicsID(char* blockTag,
                                                         ITEM_ID& blockCharID);

    /*------------------------------------------------------------------------*/
    /** Performs internal subscription actions

        @return status
    */
    PMStatus doSubscriptionActions(ParamInfo& paramInfo,
                                   bool readFromDevice);

    /*------------------------------------------------------------------------*/
    /** Performs pre subscription actions upon subscription timer elapsed

        @param paramInfo - contains subscribed members
        @param readFromDevice - if true, subscribed parameters will be read
               from device, otherise reads from cache

        @return status
    */
    PMStatus preSubscriptionActions(ParamInfo& paramInfo, bool readFromDevice);

    /*------------------------------------------------------------------------*/
    /** Allocates memory for char pointer and asssigns string to it.

        @param variable - string information to be filled.
        @param str - contains one of the variable information

    */
    void assignStringValue(StringType& variable, STRING str, bool bConvert = false);

    /*------------------------------------------------------------------------*/
    /** Function to use for write value into the paramInfo structure

        @param evalVal - holds the parameter value
        @param paramList - contains parameters list

        @return status
    */
    int setValuesToParamInfo(EVAL_VAR_VALUE* evalVal, VariableType* param);

    /*------------------------------------------------------------------------*/
    /** This private function is used to get value from the paramInfo structure

        @param varVal - contains list of parameters with basic info such as Id,
        help label
        @param evalVal - contains parameter value already read from device

        @return SUCCESS or FAILURE
    */
    int getValuesParamInfo(VariableType* varVal, EVAL_VAR_VALUE* evalVal);

    /*------------------------------------------------------------------------*/
    /** Validates the date and time values
     *
     * @param dateArray - date array to get filled the default values
     * @param length - contains the length of the date array
     * @return validity - contains the value is valid or not
     */
    bool validateDateNTime(int* dateArray, int length);

    /*------------------------------------------------------------------------*/
    /** Reversing the bit string data
     *
     * @param val - Holds the bit string value
     * @param size - Holds the size of the bit string
     */
    void bitReverse(char *val, int size);

    /*------------------------------------------------------------------------*/
    /** Assigns the Hex value to the Bit String
     *
     * @param input - Holds the bit string value in Hex
     * @param output - Holds the bit string value in plain text
     */
    void assignBitStringValue(STRING input, STRING& output);

    /* Explicitly Disabled Methods ===========================================*/
    FFParameterCache& operator=(const FFParameterCache& rhs) = delete;
    FFParameterCache(const FFParameterCache& rhs) = delete;

private:
    /* private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /** Function to use for getting item information from p_ref

        @param Item_Info - contains param
        @param paramInfo - holds the parameter value

        @return status
    */
    int getItemInfoFromRef(ENV_INFO* envInfo,
            VariableType* param);

    /*------------------------------------------------------------------------*/
    void retrieveItemInfoBasedOnItemType(int index, MaskType mask,
        unsigned long containerId, int itemInfoHandle, ENV_INFO *envInfo,
        std::vector<std::reference_wrapper<ItemInfoType>>& members,
                                     DDI_GENERIC_ITEM *genericItem);

    /*------------------------------------------------------------------------*/
    /** Function to use read value from the device if not available in cache

        @param members - contains parameters list
        @param envInfo - contains environment info
        @param maskType - contains parameters mask information
    */
    int getParametersValue(std::vector<std::reference_wrapper<ItemInfoType>> members,
                            ENV_INFO* envInfo, MaskType maskType = ITEM_ATTR_MIN_MASK);

    /*------------------------------------------------------------------------*/
    /** Writes requested values to device

        @param paramList - contains parameters list
        @param envInfo - contains environment info

        @return status
    */
    int writeRequestedValueToDevice(std::vector<std::reference_wrapper<ItemInfoType> > members,
                                    ENV_INFO* envInfo);

    /*------------------------------------------------------------------------*/
    /** This function is used to Retrieve all MEMBER_LIST items

        @param memList - contains members list
        @param listCount - contains list count
        @param envInfo - contains environment info
        @param mask - holds the mask type
        @param paramInfo - contains parameter list
        @param containerId - item id of Record/Array
        @param subIndex - subIndex of a Record. For other type it should be 0
                          If record has subIndex 0 then all members will be
                          fetched

        @return SUCCESS or FAILURE
    */
    int getParamItem(MEMBER* memList,unsigned short listCount,
        ENV_INFO* envInfo, MaskType mask,
        std::vector<std::reference_wrapper<ItemInfoType>>& members,
        unsigned long containerId = 0, unsigned short subIndex = 0);

    /*------------------------------------------------------------------------*/
    /** This function is used to retrieve param value from the parameter
        cache, Null value shall be return if value is not available in
        parameter cache

        @param flatItem - contains flat item
        @param paramInfo - contains members list
        @param binExists - binary info for specified item
        @param itemType - type of item
        @param envInfo - contains environment info

        @return SUCCESS or FAILURE
    */
    int generateFlatItem(void* flatItem,ItemInfoType* param,ulong binExists,
                                          ENV_INFO* envInfo, MaskType mask);

    /*------------------------------------------------------------------------*/
    /** Writes edited values to the cache table

        @param paramList - contains members list
        @param envInfo - contains environment info
    */
    int writesEditedValueToCache(std::vector<std::reference_wrapper<ItemInfoType> > members,
                                                           ENV_INFO* envInfo);

    /*------------------------------------------------------------------------*/
    /** Function is used to get variable info from block view

        @param env_info - contains environment info
        @param member - contains member list
        @param view - block view index

        @return SUCCESS or FAILURE
     */
    int findVariablesInView(ENV_INFO* envInfo, MEMBER* member,
                            ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** Funtion is used to fill minimal variable information

     * @param flatVar - fetched variable info from DDS
     * @param variable - variable info to be filled
     */
    void fillMinimumVarInformation(FLAT_VAR* flatVar, VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** Funtion is used to fill minimal variable information

     * @param flatVar - fetched variable info from DDS
     * @param variable - variable info to be filled
     */
    void fillMaximumVarInformation(FLAT_VAR* flatVar, VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** This function used to get info about each array elements

     * @param envInfo - environment information
     * @param flatArray - fetched array info from DDS
     * @param array - to be filled with array information
     * @param arrSubIndex - holds the index of the array variable to be retrieved
     */
    void lookUpArrayElements(   MaskType mask, 
                                ENV_INFO* envInfo,
                                FLAT_ARRAY* flatArray,
                                ArrayType* array, 
                                unsigned short arrSubIndex);

    /*------------------------------------------------------------------------*/
    /**  This private function is used to fill record information

     * @param mask       - decides whether to have min or max var information
     * @param envInfo    - environment information
     * @param flatRecord - fetched record info from DDS
     * @param record     - to be filled with record information
     */
    void fillRecordInformation(MaskType mask, ENV_INFO* envInfo,
                    FLAT_RECORD* flatRecord, RecordType* record);

    /*------------------------------------------------------------------------*/
    /** Function gives item info based on view requested

        @param envInfo - contains environment info
        @param genericItem - gives item info ie void pointer which can
                            typecasted to particular type
        @param view - block view index
        @param ParamInfo - empty param list to be filled

        @return SUCCESS or FAILURE
     */
    int lookUpVariableList(ENV_INFO* envInfo, DDI_GENERIC_ITEM* genericItem,
                           ParamInfo* paramInfo);

    /*------------------------------------------------------------------------*/
    /** set the default reason code to the paraminfo members

        @param members - contains vectors of iteminfo

        @return void
    */
    void setDefaultReasonCode(std::vector<std::reference_wrapper<ItemInfoType> > members);

    /*------------------------------------------------------------------------*/
    /** Used to clean the Eval_value_structure structure
        * @param evalVal - contains eval var value structure

        * @return errorCode
    */
    int cleanEvalVar(EVAL_VAR_VALUE* evalVal);

    /*------------------------------------------------------------------------*/
	/** Used to clean the string type variable memory
	* @param evalVal - contains eval var value structure

	* @return errorCode
	*/
    int cleanVarMemory(EVAL_VAR_VALUE* evalVal);
	
    /*------------------------------------------------------------------------*/
    /*  This function is used to get the index of the existing item info

        @param itemId - contains item ID
        @param membersIds - contains members ID
        @param outItemInfoHandle - contains item info handle

        @return
    */
    bool isRequestedItem(ITEM_ID itemId, std::vector<ITEM_ID> *membersIds,
                         int *outItemInfoHandle);

    /*------------------------------------------------------------------------*/
    /*  This function is used to holds the item id of the requested
        paramInfo's paramlist

        @param members - contains members
        @param outItemIDs - contains item IDs
    */
    void grabItemId(std::vector<std::reference_wrapper<ItemInfoType> > members,
                    std::vector<ITEM_ID> *outItemIDs, bool &isMemberVar);

    /*------------------------------------------------------------------------*/
    /*  This function is used to log item type mismatch error
        @param param - contains item info type
        @param type - contains type
     */
    void logMismatch(ItemInfoType *param, std::string type);

    /*------------------------------------------------------------------------*/
    /** gives unit for dynamic variables
     *  @param envInfo - environment information
     *  @param variable - varible unit to be filled in this structure
     */
    void getUnitForDynamicVariables(ENV_INFO* envInfo,
                                    VariableType* variable);

    /*------------------------------------------------------------------------*/
    /** verifies whether requested itemId is member variable or not
     *
     * @param members - members contains requested parameter itemId
     * @param itemId - itemId of the variable
     * @param isMemberVar - indicates requested itemId whether member Id or not
     * @param itemSpec - variable itemId and ItemType to be filled
     */
    void resolveParameterType(
            std::vector<std::reference_wrapper<ItemInfoType> > members,
            ITEM_TYPE& itemType, ITEM_ID itemId, bool isMemberVar,
                                          DDI_ITEM_SPECIFIER* itemSpec);

    /*------------------------------------------------------------------------*/
    /** This function is used to release string values

     *  @param strType - contains string type to be deleted
     */
    void releaseStringValue(StringType& strType);

    /*------------------------------------------------------------------------*/
    /** This function is used to clean string types

     *  @param strType - contains string type to be deleted
     */
    void cleanString(VariableType* varType);

    /* Private Data Members ==================================================*/
    /// DDS Controller instance
    DDSController* m_pDDS;

    /// FF Adaptor instance
    FFAdaptor* m_pFFAdaptor;

    /// List of supported data type
    static std::map<int, bool> s_SupportedDataType;

    /// Connection Manager instance
    IConnectionManager* m_pCM;

    /// member variable to hold flatblock instance
    FLAT_BLOCK* m_pFlatBlock;
};

#endif  // _FFPARAMETER_CHACHE_H
