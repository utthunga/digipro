/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sethu
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    IDGenerator class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _ID_GENERATOR_H
#define _ID_GENERATOR_H

/* CLASS DECLARATIONS *********************************************************/
/*============================================================================*/
/** IDGenerator Class Definition.

    IDGenerator class implements the ID generation methodology for subscription
*/

class IDGenerator
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a IDGenerator
    IDGenerator();

    /*------------------------------------------------------------------------*/
    /// Destruct a IDGenerator
    virtual ~IDGenerator() = default;

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Get ID increament the last id and return currently subscribed ID

        @return subscription ID
    */
    unsigned int getId();

    /*------------------------------------------------------------------------*/
    /// Reset function is use to reset the last subscribed ID
    void reset();

private:
    /* Private Data Members ==================================================*/
    /// Last subscription ID
    unsigned int m_lastId;
};

#endif  // _ID_GENERATOR_H
