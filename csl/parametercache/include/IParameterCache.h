/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga
*/

/** @file
    Parameter Cache Interface class declaration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _IPARAMETER_CHACHE_H
#define _IPARAMETER_CHACHE_H

/* INCLUDE FILES **************************************************************/

#include "PMErrorCode.h"
#include "PMStatus.h"
#include "ParameterCache.h"

// Date and time format
#define DATE   (0)
#define MONTH  (1)
#define YEAR   (2)
#define HOUR   (3)
#define MINUTE (4)
#define SECOND (5)
#define MILLISECOND (6)
#define MICROSECOND (7)

#define DATENTIME_NOT_INIT     (0)
#define DATENTIME_DEFAULT      (1)
#define BIT_STRING_OFFSET      (2)
#define DATE_ARRAY_LENGTH      (3)
#define CHAR_ARRAY_LENGTH      (8)
#define DATE_STR_LENGTH        (10)
#define MAX_VAL_OF_MONTH       (12)
#define DATENTIME_STR_LENGTH   (20)
#define MAX_VAL_OF_HOUR        (24)
#define MAX_VAL_OF_DAY         (31)
#define MAX_VAL_OF_MIN         (59)
#define MAX_VAL_OF_SEC         (59)
#define MAX_VAL_OF_MILLISEC    (1000)
#define MAX_VAL_OF_MICROSEC    (1000)
#define MIN_VAL_OF_YEAR        (1900)

#define MAX_VAL_OF_MS          (1000)

#define DATE_INIT_VALUE         0xFF
#define DATE_DATA_FORMAT        "%m/%d/%Y"
#define TIME_DATA_FORMAT        "%H:%M:%S"
#define TIME_TYPE_FORMAT        "%d:%d:%d"
#define DATE_TYPE_FORMAT        "%d/%d/%d"
#define DATENTIME_DATA_FORMAT   "%d/%m/%Y %H:%M:%S"
#define DATENTIME_TYPE_FORMAT   "%d/%d/%d %d:%d:%d"

/* STRUCTURES and ENUMS DECLARATIONS ******************************************/

/*----------------------------------------------------------------------------*/
/// Attribute enumeration
enum VARIABLE_ATTRIBUTE
{
    name,            /// Name attribute
    label,           /// Label attribute
    help,            /// Help attribute
    postEditAction,  /// Post edit action
    preEditAction,   /// Pre edit action
    value            /// Attribute value
};

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** IParameterCache Class Definition.

    IParameterCache class is interface for Parameter
    Cache and Adaptor irrespective of the protocol types.
*/

class IParameterCache
{
public:
    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /// Construct a IParameterCache
    IParameterCache()
    {
    }

    /*------------------------------------------------------------------------*/
    /// Destruct a IParameterCache
    virtual ~IParameterCache()
    {
    }

    /* Public Member Functions ===============================================*/

    /// All the interfaces functions to be implemented in derived classes
    /*------------------------------------------------------------------------*/
    /** Retrives parameter info for the requested parameter

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus getParameter(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Retrives the value from cache, if not found then read the device
        value for the requested parameter

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus readValue(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Writes the given value to cache as well as to the device

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus writeValue(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Retrives the value from device for the requested parameter

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus readDeviceValue(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Writes the given edit value to device

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus writeDeviceValue(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Retrives the edit values from cache table

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus readEditValue(ParamInfo* paramInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Writes the edit values from cache table

        @param paramInfo - contains parameter info

        @return status
    */
    virtual PMStatus writeEditValue(ParamInfo* paramInfo) = 0;

    /*= Explicitly Disabled Methods ==========================================*/
    IParameterCache& operator=(const IParameterCache& rhs) = delete;
    IParameterCache(const IParameterCache& rhs) = delete;
};

#endif  // _IPARAMETER_CHACHE_H
