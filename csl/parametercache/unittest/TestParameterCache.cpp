/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Connection Manager Unit Test
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#include <iostream>
#include "gtest/gtest.h"
#include "CommonProtocolManager.h"
#include "IProtocolAdaptor.h"
#include "ConnectionManager.h"
#include "FFAdaptor.h"
#include "IConnectionManager.h"
#include "FFConnectionManager.h"
#include "DDSController.h"
#include "FFParameterCache.h"
#include "PMErrorCode.h"
#include "PMStatus.h"

#define VIEW_1 1
#define VIEW_2 2
#define VIEW_3 3
#define VIEW_4 4

using namespace std;

typedef struct _devinfo_TBL {
    DeviceInfo  deviceInfo;
    DeviceInfo* next;
} DEVICEINFO_TBL;

typedef struct _SCAN_DEVICEINFO_TBL{
    int             count;
    DEVICEINFO_TBL* list;

} SCAN_DEVICEINFO_TBL;

FFAdaptor *ffadaptor;
FFParameterCache* m_pFFParameterCache;
DDSController* m_pDDSController;
FFConnectionManager* m_pFFConnectionManager;
DeviceInfo devices[10];

class TestParameterCache : public :: testing :: Test
{
public:
    SCAN_DEV_TBL scanDevTable;
    ParamInfo* pInfo;
    int ScanCount;
    DeviceInfo device;
    PMStatus status;

    TestParameterCache()
    {
        // Initialization code
    }

    ~TestParameterCache()
    {
        // cleanup any pending stuff, but no exceptions allowed
    }

    // If the constructor and destructor are not enough for setting up and cleaning up each test, you can define the following methods.

    void SetUp()
    {
        // Code here will be called immediately after the constructor (right before each test).
    }

    void TearDown()
    {
        // Code here will be called immediately after each test (right before the destructor).
    }

    // prints variable value
    void printVariableValues(VariableType* varType);

    // Data members if any
};


TEST_F (TestParameterCache, Initialize)
{
    ffadaptor = new FFAdaptor();

    // Instantiate the Connection Manager, Parameter Cache & DDS Controller
    m_pFFConnectionManager = new FFConnectionManager;
    m_pFFParameterCache	= new FFParameterCache;
    m_pDDSController 	= new DDSController;

    // Check for improper initialisation
    if ((nullptr == m_pFFConnectionManager) ||
        (nullptr == m_pFFParameterCache) ||
        (nullptr == m_pDDSController))
    {
        // When one of them is not intialised no point running further, kill the application
        exit(-1);
    }

    // Initialise DDS Controller with Connection Manger & Parameter cache
    m_pDDSController->initialize(ffadaptor, m_pFFConnectionManager,m_pFFParameterCache);

    // Initialise Connection Manager with DDS Controller & Parameter Cache
    m_pFFConnectionManager->initialize(m_pDDSController, m_pFFParameterCache);

    // Initialise Parameter Cache with DDS Controller & Connection Manager
    m_pFFParameterCache->initialize(m_pDDSController, m_pFFConnectionManager, ffadaptor);

    // set bus parameters
    FF_BUS_PARAMS* busParams = new FF_BUS_PARAMS;
    busParams->this_link = 0;
    busParams->slot_time = 8;
    busParams->phlo = 6;
    busParams->mrd = 10;
    busParams->mipd = 16;
    busParams->tsc = 3;
    busParams->phis_skew = 0;
    busParams->phpe = 2;
    busParams->phge = 0;
    busParams->fun = 248;
    busParams->nun = 0;

    // Below code is applicable for unit test case,
    // In normal scenarios these attribues were read from cpmappinfo.json config file
    const char* ffDDPath = "/usr/share/dd";
    const char* ffDeviceFile = "/dev/ttyUSB0";

    m_pFFConnectionManager->initStack(busParams, ffDDPath, ffDeviceFile);
    if(nullptr != ffDDPath && nullptr != ffDeviceFile)
    {
        ffDDPath = nullptr;
        ffDeviceFile = nullptr;
    }
}

TEST_F (TestParameterCache, Setup)
{
    ENV_INFO env_info;
    APP_INFO  app_info;

    memset(&scanDevTable, 0, sizeof(SCAN_DEV_TBL));
    memset(&app_info, 0, sizeof(APP_INFO));
    memset(&env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    int rs = 0;

    EXPECT_NO_THROW(status = m_pFFConnectionManager->scanDevices(&env_info, &scanDevTable));
    if (status.hasError())
    {
        rs = FAILURE;
    }
    EXPECT_EQ(0, rs);

    SCAN_TBL* pList = scanDevTable.list;
    SCAN_TBL* temp = NULL;

    if (scanDevTable.count > 0)
    {
        ScanCount = 0;
        while(pList != NULL )
        {
            DeviceInfo deviceInfo;
            memset(&deviceInfo, 0, sizeof(DeviceInfo));

            EXPECT_NO_THROW(status = m_pFFConnectionManager->getDeviceInfo(pList->station_address, &deviceInfo));
            if (status.hasError())
            {
                rs = FAILURE;
            }
            EXPECT_EQ(0, rs);
            if (rs == SUCCESS)
            {
                devices[ScanCount++] = deviceInfo;
                device = deviceInfo;
            }
            temp = pList;
            pList = pList->next;
            free(temp);
        }
    }

    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = m_pFFConnectionManager->connectDevice(&devices[0]));
        sleep(5);
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(SUCCESS, rs);
    }
}

#if 0
TEST_F (TestParameterCache, GetParameterFromView1)
{
    int rs = SUCCESS;
    ParamInfo pinfo;

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        pinfo.requestInfo.maskType = ITEM_ATTR_ALL_MASK;

        EXPECT_NO_THROW(status = m_pFFParameterCache->getParameterFromBlockView(VIEW_1, &pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (!status.hasError())
        {
            cout << "----------" << pinfo.blockTag.str << "----------" << endl;
            for (auto &item : pinfo.paramList)
            {
                auto itemInfo = &item.get();
                if (itemInfo->itemType == ParamItemType::PARAM_VARIABLE)
                {
                    auto varType = dynamic_cast<VariableType*>(itemInfo);

                    cout<< varType->itemID << "   Label : " << varType->label.str << endl;

                }
                else if (itemInfo->itemType == ParamItemType::PARAM_RECORD)
                {
                    auto recType = dynamic_cast<RecordType*>(itemInfo);

                    for (auto &itemInfo : recType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_ARRAY)
                {
                    auto arrType = dynamic_cast<ArrayType*>(itemInfo);

                    for (auto &itemInfo : arrType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID <<"   Label : " << varType->label.str << endl;
                    }
                }
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, GetParameterFromView2)
{
    int rs = SUCCESS;
    ParamInfo pinfo;

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        pinfo.requestInfo.maskType = ITEM_ATTR_ALL_MASK;

        EXPECT_NO_THROW(status = m_pFFParameterCache->getParameterFromBlockView(VIEW_2, &pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (!status.hasError())
        {
            cout << "----------" << pinfo.blockTag.str << "----------" << endl;
            for (auto &item : pinfo.paramList)
            {
                auto itemInfo = &item.get();
                if (itemInfo->itemType == ParamItemType::PARAM_VARIABLE)
                {
                    auto varType = dynamic_cast<VariableType*>(itemInfo);

                    cout<< varType->itemID << "   Label : " << varType->label.str << endl;

                }
                else if (itemInfo->itemType == ParamItemType::PARAM_RECORD)
                {
                    auto recType = dynamic_cast<RecordType*>(itemInfo);

                    for (auto &itemInfo : recType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_ARRAY)
                {
                    auto arrType = dynamic_cast<ArrayType*>(itemInfo);

                    for (auto &itemInfo : arrType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID <<"   Label : " << varType->label.str << endl;
                    }
                }
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, GetParameterFromView3)
{
    int rs = SUCCESS;
    ParamInfo pinfo;

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        pinfo.requestInfo.maskType = ITEM_ATTR_ALL_MASK;

        EXPECT_NO_THROW(status = m_pFFParameterCache->getParameterFromBlockView(VIEW_3, &pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (!status.hasError())
        {
            cout << "----------" << pinfo.blockTag.str << "----------" << endl;
            for (auto &item : pinfo.paramList)
            {
                auto itemInfo = &item.get();
                if (itemInfo->itemType == ParamItemType::PARAM_VARIABLE)
                {
                    auto varType = dynamic_cast<VariableType*>(itemInfo);

                    cout<< varType->itemID << "   Label : " << varType->label.str << endl;

                }
                else if (itemInfo->itemType == ParamItemType::PARAM_RECORD)
                {
                    auto recType = dynamic_cast<RecordType*>(itemInfo);

                    for (auto &itemInfo : recType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_ARRAY)
                {
                    auto arrType = dynamic_cast<ArrayType*>(itemInfo);

                    for (auto &itemInfo : arrType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID <<"   Label : " << varType->label.str << endl;
                    }
                }
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, GetParameterFromView4)
{
    int rs = SUCCESS;
    ParamInfo pinfo;

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        pinfo.requestInfo.maskType = ITEM_ATTR_ALL_MASK;

        EXPECT_NO_THROW(status = m_pFFParameterCache->getParameterFromBlockView(VIEW_4, &pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (!status.hasError())
        {
            cout << "----------" << pinfo.blockTag.str << "----------" << endl;
            for (auto &item : pinfo.paramList)
            {
                auto itemInfo = &item.get();
                if (itemInfo->itemType == ParamItemType::PARAM_VARIABLE)
                {
                    auto varType = dynamic_cast<VariableType*>(itemInfo);

                    cout<< varType->itemID << "   Label : " << varType->label.str << endl;

                }
                else if (itemInfo->itemType == ParamItemType::PARAM_RECORD)
                {
                    auto recType = dynamic_cast<RecordType*>(itemInfo);

                    for (auto &itemInfo : recType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_ARRAY)
                {
                    auto arrType = dynamic_cast<ArrayType*>(itemInfo);

                    for (auto &itemInfo : arrType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID <<"   Label : " << varType->label.str << endl;
                    }
                }
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}

#endif
TEST_F (TestParameterCache, GetParameter)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    ImageType recType[1];

    if (nullptr != m_pFFParameterCache)
    {
        DDI_GENERIC_ITEM    ddiGenericItem{};
        DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
        ENV_INFO2 envInfo2{};

        memset(&ddiGenericItem, 0, sizeof(DDI_GENERIC_ITEM));
        memset(&ddiItemSpecifier, 0, sizeof(DDI_ITEM_SPECIFIER));

        DeviceHandle deviceHandle{-1};
        m_pDDSController->getActiveDeviceHandle(&deviceHandle);

        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;
        envInfo2.block_handle = 0;

        ddiItemSpecifier.type = DDI_ITEM_ID;
        ddiItemSpecifier.item.id = 0x2144B;// 136267

        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier,
                     &envInfo2, ALL_ITEM_ATTR_MASK, &ddiGenericItem);

        if (status.hasError())
        {
            m_pDDSController->ddiCleanItem(&ddiGenericItem);
        }

        FLAT_IMAGE *pMenu = static_cast<FLAT_IMAGE*>(ddiGenericItem.item);

        if (nullptr != pMenu)
        {
            cout << pMenu->path.str << endl;
        }



        std::string str = "RB_1000                         ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());

        pinfo.requestInfo.maskType = ITEM_ATTR_MIN_MASK;

        recType[0].itemID = 0x20C0E;
        pinfo.paramList.push_back(recType[0]);
        EXPECT_NO_THROW(status = m_pFFParameterCache->getParameter(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (!status.hasError())
        {
            cout << "----------" << pinfo.blockTag.str << "----------" << endl;
            for (auto &item : pinfo.paramList)
            {
                auto itemInfo = &item.get();
                if (itemInfo->itemType == ParamItemType::PARAM_VARIABLE)
                {
                    auto varType = dynamic_cast<VariableType*>(itemInfo);

                    cout<< varType->itemID << "   Label : " << varType->label.str << endl;

                }
                else if (itemInfo->itemType == ParamItemType::PARAM_RECORD)
                {
                    auto recType = dynamic_cast<RecordType*>(itemInfo);

                    for (auto &itemInfo : recType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout<< varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_ARRAY)
                {
                    auto arrType = dynamic_cast<ArrayType*>(itemInfo);

                    for (auto &itemInfo : arrType->members)
                    {
                        auto type = &itemInfo.get();
                        auto varType = dynamic_cast<VariableType*>(type);

                        cout << varType->itemID << "   Label : " << varType->label.str << endl;
                    }
                }
                else if (itemInfo->itemType == ParamItemType::PARAM_IMAGE)
                {
                    auto imageType = dynamic_cast<ImageType*>(itemInfo);
                    cout << imageType->itemID << "   Path : " << imageType->imagePath.str << endl;
                }
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}



#if 0
TEST_F (TestParameterCache, ReadValue)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    VariableType varType{};

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        varType.itemType = ParamItemType::PARAM_VARIABLE;

        varType.itemID = 16777229;
        pinfo.paramList.push_back(varType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->readValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }
        else
        {
            for(auto item : pinfo.paramList)
            {
                auto type = &item.get();
                auto varType = dynamic_cast<VariableType*>(type);
                printVariableValues(varType);
            }
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}

void TestParameterCache::printVariableValues(VariableType* varType)
{
    switch (varType->type)
    {
    case DDS_INTEGER:
    case DDS_ENUMERATED:
    case DDS_BIT_ENUMERATED:
    case DDS_INDEX:
    case DDS_BOOLEAN_T:
        cout<<"value is : "<<varType->value.stdValue.l<<endl;
        break;
    case DDS_UNSIGNED:
        cout<<"value is : "<<varType->value.stdValue.ul<<endl;
        break;
    case DDS_FLOAT:
        cout<<"value is : "<<varType->value.stdValue.f<<endl;
        break;
    case DDS_DOUBLE:
        cout<<"value is : "<<varType->value.stdValue.d<<endl;
        break;
    case DDS_ASCII:
    case DDS_PACKED_ASCII:
    case DDS_PASSWORD:
    case DDS_BITSTRING:
    case DDS_OCTETSTRING:
    case DDS_VISIBLESTRING:
    case DDS_EUC:
    {
        if(nullptr != varType->value.strData.str)
        cout<<"value is : "<<varType->value.strData.str<<endl;
    }
    case DDS_DATE:
    case DDS_TIME:
    case DDS_TIME_VALUE:
    case DDS_DATE_AND_TIME:
    case DDS_DURATION:
        cout<<"value is : "<<varType->value.CA<<endl;
        break;
    default:
        break;
    }

}


TEST_F (TestParameterCache, WriteValue)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    VariableType varType{};

    if (nullptr != m_pFFParameterCache)
    {
        std::string str = "TRANSDUCER                      ";
        pinfo.blockTag.str = const_cast<char*>(str.c_str());
        varType.itemType = ParamItemType::PARAM_VARIABLE;

        varType.itemID = 16777229;
        varType.type = 6;
        varType.size = 2;
        varType.value.stdValue.l = 1003;

        pinfo.paramList.push_back(varType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->writeValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }
    }
    else
    {
        rs = FAILURE;
    }

    pinfo.paramList.clear();

    EXPECT_EQ(SUCCESS, rs);
}


TEST_F (TestParameterCache, ReadValueAfterWrite)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    ItemInfoType* itemType;
    VariableType varType;

    if (nullptr != m_pFFParameterCache)
    {
        itemType = new ItemInfoType;

        pinfo.blockTag = "RB_1000_362                     ";
        itemType->itemType = ParamItemType::PARAM_VARIABLE;

        varType.itemID = 2147615098;
        itemType = &varType;
        pinfo.paramList.push_back(itemType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->readValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (itemType != nullptr)
        {
            itemType = nullptr;
        }
    }
    else
    {
        rs = FAILURE;
    }

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, ReadDeviceValue)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    ItemInfoType* itemType;
    VariableType varType;

    if (nullptr != m_pFFParameterCache)
    {
        itemType = new ItemInfoType;

        pinfo.blockTag = "RB_1000_362                     ";
        itemType->itemType = ParamItemType::PARAM_VARIABLE;

        varType.itemID = 2147615098;
        itemType = &varType;
        pinfo.paramList.push_back(itemType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->readDeviceValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (itemType != nullptr)
        {
            itemType = nullptr;
        }
    }
    else
    {
        rs = FAILURE;
    }

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, WriteDeviceValue)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    ItemInfoType* itemType;
    VariableType varType;

    if (nullptr != m_pFFParameterCache)
    {
        itemType = new ItemInfoType;

        pinfo.blockTag = "RB_1000_362                     ";
        itemType->itemType = ParamItemType::PARAM_VARIABLE;
        varType.itemID = 2147615104;
        varType.type = 18;
        varType.size = 4;
        std::string tagDesc = "2051FF                          ";
        varType.value.strData.str = const_cast<char*>(tagDesc.c_str());
        itemType = &varType;
        pinfo.paramList.push_back(itemType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->writeDeviceValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }
    }
    else
    {
        rs = FAILURE;
    }
    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestParameterCache, ReadDeviceValueAfterWrite)
{
    int rs = SUCCESS;
    ParamInfo pinfo;
    ItemInfoType* itemType;
    VariableType varType;

    if (nullptr != m_pFFParameterCache)
    {
        itemType = new ItemInfoType;

        pinfo.blockTag = "RB_1000_362                     ";
        itemType->itemType = ParamItemType::PARAM_VARIABLE;

        varType.itemID = 2147615098;
        itemType = &varType;
        pinfo.paramList.push_back(itemType);

        EXPECT_NO_THROW(status = m_pFFParameterCache->readDeviceValue(&pinfo));
        if (status.hasError())
        {
            rs = FAILURE;
        }

        if (itemType != nullptr)
        {
            itemType = nullptr;
        }
    }
    else
    {
        rs = FAILURE;
    }

    EXPECT_EQ(SUCCESS, rs);
}
#endif
