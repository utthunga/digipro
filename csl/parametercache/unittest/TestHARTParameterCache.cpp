/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Unit Test case for HARTParameterCache class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/// Include Files
#include"gtest/gtest.h"


#include "HARTAdaptor.h"
#include "json/json.h"
#include "BusinessLogic.h"
#include "PMStatus.h"
#include "PMErrorCode.h"

using namespace std;

static HARTAdaptor* s_pHartAdaptor =  new HARTAdaptor();

/// Unit Test class definition for HARTAdaptor
class TestHARTParameterCache : public ::testing::Test
{
public:
    TestHARTParameterCache()
    {
        /// perform initialization
    }

    ~TestHARTParameterCache()
    {
        /// Perform cleanup
    }

protected:
    virtual void SetUp()
    {

    }

    virtual void TearDown()
    {
        /// Called just after test execution
    }
};

TEST_F(TestHARTParameterCache, InitialSetup)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 5;

    result = s_pHartAdaptor->connect(jsonData);

    result.clear();
    jsonData.clear();
}

TEST_F(TestHARTParameterCache, testProcessVariableToReadParam)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 5;

    result = s_pHartAdaptor->connect(jsonData);

    result.clear();

    Json::Value jsonReq;
    jsonReq[JSON_ITEM_ID] = 163;           //184(final assembly no), 222(longtag);
    jsonReq[JSON_ITEM_TYPE] = "VARIABLE";
    jsonReq[JSON_ITEM_IN_DD] = TRUE;

    result = s_pHartAdaptor->getItem(jsonReq);

    Json::FastWriter writer;
    std::string str = writer.write(result);
    cout<<" result is  : "<<str<<endl;

    result.clear();
    jsonData.clear();
    jsonReq.clear();
}

TEST_F(TestHARTParameterCache, testProcessVariableToWriteParam)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 5;

    result = s_pHartAdaptor->connect(jsonData);

    result.clear();

    // request to write
    Json::Value jsonReqToWrite;
    jsonReqToWrite[JSON_ITEM_ID] = 163;
    jsonReqToWrite[JSON_ITEM_TYPE] = "VARIABLE";
    jsonReqToWrite[JSON_ITEM_IN_DD] = TRUE;
    jsonReqToWrite[JSON_ITEM_REQ_TYPE] = "UPDATE";
    jsonReqToWrite[JSON_ITEM_INFO][JSON_VAR_VALUE] = "644TAG";

    result = s_pHartAdaptor->setItem(jsonReqToWrite);
    EXPECT_EQ(result.asString(), "ok");
    result.clear();

    // request to read
    Json::Value jsonReqToRead;

    jsonReqToRead[JSON_ITEM_ID] = 159;
    jsonReqToRead[JSON_ITEM_TYPE] = "VARIABLE";
    jsonReqToRead[JSON_ITEM_IN_DD] = TRUE;

    // read the date after wtite
    result = s_pHartAdaptor->getItem(jsonReqToRead);

    for(auto item : result[JSON_ITEM_INFO])
    {
        if(item.isMember(JSON_VAR_INFO))
        unsigned long val = item[JSON_VAR_INFO][JSON_VAR_VALUE].asUInt();
    }

    // clears all members
    result.clear();
    jsonData.clear();
    jsonReqToRead.clear();
    jsonReqToWrite.clear();
}
