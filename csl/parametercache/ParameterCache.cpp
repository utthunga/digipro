/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Parameter Cache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <string.h>
#include "ParameterCache.h"
#include "SubscriptionController.h"
#include "flk_log4cplus/Log4cplusAdapter.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    ParameterCache class instance initialization
*/
ParameterCache::ParameterCache()
    : m_sequenceID(0),
      m_pSubscriptionController(new SubscriptionController)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
ParameterCache::~ParameterCache()
{
    if (nullptr != m_pSubscriptionController)
    {
        delete m_pSubscriptionController;
        m_pSubscriptionController = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Create subscription that genarate Subscription ID and add it to
    subscription map
*/
PMStatus ParameterCache::createSubscription(ParamInfo monitorItems,
                                   uint refreshRate,
                                   bool deviceRead,
                                   DataChangedCallback dataChangedCallback,
                                   uint& subscriptionID,
                                   SubscriptionType subscriptionType)
{
    SubscriptionTimerCallback callback = [this]( Subscription* pSubscription,
                                        bool deviceRead,
                                        DataChangedCallback dataChangedCallback)
    {

        ParamInfo paramInfo{};
        bool checkForValueChange = false;

        if (nullptr != pSubscription)
        {
           paramInfo = pSubscription->getMonitorItems();
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Subscription instance is null!!");
        }

        if (paramInfo.paramList.size() <= 0)
        {
            // callback to send the changed values to IC
            dataChangedCallback(pSubscription->getSubscriptionID(), nullptr);
            return;
        }

        // perform pre subscription actions if any
        this->preSubscriptionActions(paramInfo, deviceRead);

        // Get each individual item in the monitor list
        for (auto item : paramInfo.paramList)
        {
            // if this subscription is stopped by user, exit this loop
            if (THREAD_STATE_STOP == pSubscription->getThreadState())
            {
                break;
            }

            ParamInfo monitorItem;

            // Create an exact copy of the item which is there in monitor list
            monitorItem.paramList.push_back(item.get());
            monitorItem.requestInfo = paramInfo.requestInfo;

            if (nullptr != paramInfo.blockTag.str &&
                    (strlen (paramInfo.blockTag.str) > 0))
            {
                monitorItem.blockTag.str = new char[strlen(paramInfo.blockTag.str) + 1]();
                strncpy (monitorItem.blockTag.str, paramInfo.blockTag.str,
                         strlen(paramInfo.blockTag.str));
                monitorItem.blockTag.len = paramInfo.blockTag.len;
            }

            // Fetch value either from cache or device
            this->doSubscriptionActions(monitorItem, deviceRead);

            auto paramItem = &item.get();
            auto varParam = dynamic_cast<VariableType*>(paramItem);

            // If the value of the parameter is changed, then trigger data changed callback
            if (true == varParam->valueChanged)
            {
                // callback to send the changed values to IC
                dataChangedCallback(pSubscription->getSubscriptionID(), &monitorItem);
                checkForValueChange = true;
            }

            if (nullptr != monitorItem.blockTag.str)
                delete[] monitorItem.blockTag.str;
        }

        // Check for value change in any of the parameter in monitor item list
        // and trigger empty subscription notification message    
        if (false == checkForValueChange)
        {
            // callback to send the changed values to IC
            dataChangedCallback(pSubscription->getSubscriptionID(), nullptr);
        }
    };

    PMStatus status = m_pSubscriptionController->createSubscription(
                                                 monitorItems,
                                                 refreshRate,
                                                 deviceRead,
                                                 dataChangedCallback,
                                                 callback,
                                                 subscriptionID,
                                                 subscriptionType);
    return status;
}

/*  ----------------------------------------------------------------------------
    Start the subscription thread. Once refreshtime elapsed Parameters shall
    be read and send result over callback
*/
void ParameterCache::onSubscriptionTimerElapsed(uint subscriptionID,
                                       ParamInfo* monitorItem,
                                       DataChangedCallback dataChangedCallback)
{
    dataChangedCallback(subscriptionID, monitorItem);
}

/*  ----------------------------------------------------------------------------
    Start the subscription thread. Once refreshtime elapsed Parameters shall
    be read and send result over callback
*/
PMStatus ParameterCache::startSubscription(uint subscriptionID)
{
    return m_pSubscriptionController->startSubscription(subscriptionID);
}

/*  ----------------------------------------------------------------------------
    Unsubscribe and stop the subscription thread
*/
PMStatus ParameterCache::unsubscribe(uint subscriptionID)
{
    return m_pSubscriptionController->unsubscribe(subscriptionID);
}

/*  ----------------------------------------------------------------------------
    Stop supcription and delete the subscription from the list
*/
PMStatus ParameterCache::deleteSubscription(uint subscriptionID)
{
    return m_pSubscriptionController->deleteSubscription(subscriptionID);
}

/*  ----------------------------------------------------------------------------
    Deletes all subscriptions
*/
PMStatus ParameterCache::deleteAllSubscription(SubscriptionType subscriptonType)
{
    return m_pSubscriptionController->deleteAllSubscription(subscriptonType);
}

/*  ----------------------------------------------------------------------------
    Pauses the subscription lock to prevent accessing device
*/
void ParameterCache::pauseSubscription()
{
    return m_pSubscriptionController->pauseSubscription();
}

/*  ----------------------------------------------------------------------------
    Resumes the subcription
*/
void ParameterCache::resumeSubscription()
{
    return m_pSubscriptionController->resumeSubscription();
}

/*  ----------------------------------------------------------------------------
    It aquires the thread lock and wait till release
*/
void ParameterCache::acquireLock()
{
    m_readLock.lock();
}

/*  ----------------------------------------------------------------------------
    It releases the locked thread and notify for further process
*/
void ParameterCache::releaseLock()
{
    m_readLock.unlock();
}

/*  ----------------------------------------------------------------------------
    Retrieves subscription controller instance
*/
SubscriptionController* ParameterCache::getSubscriptionController()
{
    return m_pSubscriptionController;
}
