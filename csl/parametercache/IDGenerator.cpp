/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sethu
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    IDGenerator class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "IDGenerator.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    IDGenerator class instance initialization
*/
IDGenerator::IDGenerator() : m_lastId(0)
{
}

/*  ----------------------------------------------------------------------------
    Get ID increament the last id and return currently subscribed ID
*/
unsigned int IDGenerator::getId()
{
    // If overflow happens, avoid returning 0
    return ++m_lastId != 0u ? m_lastId : ++m_lastId;
}

/*  ----------------------------------------------------------------------------
    Reset function is use to reset the last subscribed ID
*/
void IDGenerator::reset()
{
    m_lastId = 0;
}
