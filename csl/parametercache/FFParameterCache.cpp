/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FFParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include <math.h>
#include "DDSController.h"
#include "FFParameterCache.h"
#include "FFAdaptor.h"
#include "IParameterCache.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

#define EMPTY_HANDLE (-1) //It is -1, Since first item  start at index 0
#define VIEW_LIMIT   (5)

#define DICT_STR_HACK 1

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    FFParameterCache class instance initialization
*/
FFParameterCache::FFParameterCache() : m_pDDS(nullptr),
                                       m_pCM(nullptr),
                                       m_pFFAdaptor(nullptr),
                                       m_pFlatBlock(nullptr)
{

}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
FFParameterCache::~FFParameterCache()
{
    // Clear the member variables
    m_pDDS = nullptr;
    m_pCM = nullptr;
    m_pFFAdaptor = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function initialize cache tables and scheduler
*/
PMStatus FFParameterCache::initialize(
    DDSController* dds, IConnectionManager* cm, FFAdaptor* adaptor)
{
    PMStatus status;

    if (nullptr != dds && nullptr != cm)
    {
        m_pDDS = dds;
        m_pCM = cm;
        m_pFFAdaptor = adaptor;
    }
    else
    {
        status.setError(
            FF_PC_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_PC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get list of parameters.
    It is uplink responsibility to free ParamInfo memory
*/
PMStatus FFParameterCache::getParameter(ParamInfo* paramInfo)
{
    MaskType            attrMask;
    DDI_BLOCK_SPECIFIER blockSpec{};
    ENV_INFO            envInfo{};
    int                 errorCode = SUCCESS;
    FLAT_BLOCK*         flatBlock = nullptr;
    DDI_GENERIC_ITEM    genericItem{};
    DDI_ITEM_SPECIFIER  itemSpec{};
    PMStatus            status{};
    APP_INFO            appInfo;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        status.setError(
                    FF_PC_ERR_GET_PARAMETER_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    else
    {
        // This API gives block handle
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                    paramInfo->blockTag.str);

        // validates block handle
        if (valid_block_handle(envInfo.block_handle) == 0)
        {
            status.setError(
                FF_PC_ERR_GET_ENVIRON_INFO_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_PC);
        }
        else
        {
            //set the defualt reason code
            setDefaultReasonCode(paramInfo->paramList);

            blockSpec.type = DDI_BLOCK_HANDLE;
            blockSpec.block.handle = envInfo.block_handle;

            //Clean once again for every loop
            genericItem = {};

            // Gets the requested block item.
            status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);

            if (!status.hasError())
            {
                // Gets the parameter list with value.
                // value may or maynot available.
                std::vector<std::reference_wrapper<ItemInfoType>> members = paramInfo->paramList;

                // Typecasted generic item to flat block.
                flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
                if (nullptr == flatBlock)
                {
                    errorCode = FAILURE;
                }
                else
                {
                    m_pFlatBlock = flatBlock;
                    attrMask = paramInfo->requestInfo.maskType;
                    getParamItem(flatBlock->param.list,
                                flatBlock->param.count,
                                &envInfo, attrMask, members);
                    
                    // Reassign the members
                    paramInfo->paramList = members;
                }
            }
            else
            {
                // Ignore the else condition since param info set
                // with default reason code DDI_INVALID_PARAM
                LOGF_ERROR(CPMAPP, "Failed to get DDI item");
            }
            
            // Clears the genericItem memory.
            m_pDDS->ddiCleanItem(&genericItem);
            if (SUCCESS != errorCode)
            {
                LOGF_ERROR(CPMAPP, "Failed to get parameter");
                status.setError(
                    FF_PC_ERR_GET_PARAM_ITEM_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
            }
        }
    }

    if(nullptr != m_pFlatBlock)
    {
        m_pFlatBlock = nullptr;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get the index of the existing item info.
*/
bool FFParameterCache::isRequestedItem(ITEM_ID itemId,
                                       std::vector<ITEM_ID> *membersIds,
                                       int *outItemInfoHandle)
{
    bool isRequested = false;
    // This is 0 when getParameter called with paramInfo's Paramlist empty
    if (membersIds->empty())
        isRequested = true;   // All param items needs to be fetched from a block
    else
    {
        // Hanldle (index) of a  requested item
        for (int index=0; index < static_cast<int>(membersIds->size()); index++)
        {
            if (itemId == membersIds->at(index))
            {
                isRequested = true;
                *outItemInfoHandle = index;
                break;
            }
        }
    }
    return isRequested;
}

/*  ----------------------------------------------------------------------------
    This function is used to holds the item id of the requested
    paramInfo's paramlist.
*/
void FFParameterCache::grabItemId(std::vector<std::reference_wrapper<ItemInfoType>> members,
                                  std::vector<ITEM_ID> *outItemIDs, bool& isMemberVar)
{
    for (auto item : members)
    {
        ITEM_ID id = item.get().itemID;
        ItemInfoType* type = &item.get();
        VariableType* recItemType = dynamic_cast<VariableType*>(type);
        if(nullptr != recItemType && recItemType->containerId != 0
                && id != BLOCK_TAG_ID)
        {
            outItemIDs->push_back(recItemType->containerId);
            isMemberVar = true;
        }
        else
        {
            outItemIDs->push_back(id);
        }
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to log item type mismatch error.
*/
void FFParameterCache::logMismatch(ItemInfoType *param, std::string type)
{
    LOGF_ERROR(CPMAPP, "Item type mismatch : for the item " <<
              param->itemID << "\nexpected" << type <<
              " but got " << param->itemType);
}

/*  ----------------------------------------------------------------------------
    This function is used to Retrieve all/requested MEMBER_LIST items
*/
int FFParameterCache::getParamItem(MEMBER* memList, unsigned short listCount,
                    ENV_INFO* envInfo, MaskType mask,
                    std::vector<std::reference_wrapper<ItemInfoType>>& members,
                    unsigned long containerId, unsigned short subIndex)
{
    DDI_BLOCK_SPECIFIER  blockSpec{};
    DDI_GENERIC_ITEM     genericItem{};
    int                  index = 0;
    int                  errorCode = SUCCESS;
    ITEM_ID              itemId(0);
    DDI_ITEM_SPECIFIER   itemSpec{};
    ITEM_TYPE            itemType(0);
    std::vector<ITEM_ID> requestedItemIDs{};
    PMStatus             status{};
    bool                 isMemberVar = false;
    bool                 isSuccess = false;
    int                  itemInfoHandle;

    blockSpec.type = DDI_BLOCK_HANDLE;
    blockSpec.block.handle = envInfo->block_handle;

    // Collect the  item id from the requested iteminfo
    grabItemId(members, &requestedItemIDs, isMemberVar);

    for (index = 0; index < listCount; index++, memList++)
    {
        //Remove below 2 conditions instead fetch item directly
        //Continue until requested subIndex identified
        if (0 != subIndex && subIndex != index+1)
        {
            continue;
        }
        //Break the loop if subIndex already filled
        if (0 != subIndex && subIndex < index+1)
        {
            break;
        }

        itemId = memList->ref.id;

        itemInfoHandle = EMPTY_HANDLE;
        bool isValid = isRequestedItem(itemId, &requestedItemIDs,
                                                &itemInfoHandle);
        // Determine, is this requested itemid. This is  allways true
        // when getParameter called with empty paramlist
        if (!isValid)
        {
            continue;
        }

        isSuccess = true;

        //condition validates subindex for record and array items.
        if(!members.empty() && requestedItemIDs.size() > 0)
        {
            for(auto item : members)
            {
                VariableType* varType = dynamic_cast<VariableType*>(&item.get());
                if(nullptr != varType && varType->containerId > 0)
                {
                    int memCount = 0;
                    itemSpec.item.id = varType->containerId;
                    itemSpec.type = DDI_ITEM_ID;
                    PMStatus getItemStatus = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                       RECORD_ATTR_MASKS | ARRAY_ATTR_MASKS, &genericItem);

                    if (!getItemStatus.hasError())
                    {
                        if(genericItem.item_type == RECORD_ITYPE)
                        {
                            FLAT_RECORD* flatRecord = static_cast<FLAT_RECORD*>(genericItem.item);
                            if (nullptr != flatRecord)
                            {
                                memCount = flatRecord->members.count;
                            }
                        }
                        else
                        {
                            FLAT_ARRAY* flatArray = static_cast<FLAT_ARRAY*>(genericItem.item);
                            if (nullptr != flatArray)
                            {
                                memCount = flatArray->num_of_elements;
                            }
                        }

                        // Validates subindex
                        if(varType->subIndex > memCount)
                        {
                            errorCode = FAILURE;
                        }
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Failed to Get Item : "<< itemSpec.item.id);
                    }

                    //cleans generic item
                    m_pDDS->ddiCleanItem(&genericItem);

                    //Reset itemspec
                    itemSpec = {};
                }
            }
        }

        // based on requested itemId
        resolveParameterType(members, itemType, itemId, isMemberVar, &itemSpec);

        //Reset genericItem for each loop
        genericItem = {};

        // PC doesnot maintain it and should be cleaned by the caller.
        // Unless this is a potential leak
        ItemInfoType* param;

        status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                    ALL_ITEM_ATTR_MASK, &genericItem);

        if (!status.hasError())
        {
            // retrieves item info based on item type
            retrieveItemInfoBasedOnItemType(index, mask, containerId, itemInfoHandle,
                                            envInfo, members, &genericItem);
        }
        else
        {
            if (itemInfoHandle != EMPTY_HANDLE)
            {
                // Since itemInfoHandle assigned from member list
                // it is not required to validate itemInfohandle is exist or not
                ItemInfoType itemInfo = members.at(itemInfoHandle);
                param = &itemInfo;
            }
            else
            {
                // Create new item which should be added in the paramInfo paramlist.
                ItemInfoType parInf {};
                parInf.itemType = PARAM_DEFAULT;
                members.push_back(parInf);
                param = &parInf;
            }
            param->response_code = DDL_INVALID_PARAM;

            errorCode = FAILURE;
        }

        // Memory should be cleaned by the callar
        param = nullptr;

        // Clears the genericItem memory.
        status = m_pDDS->ddiCleanItem(&genericItem);

        if (status.hasError())
        {
            errorCode = FAILURE;
            break;
        }
    }

    /*  If the parameter is not avaiable in the block parameter list,
        then it needs to find in local parameter list
    */
    if (!isSuccess)
    {
        if (nullptr != m_pFlatBlock)
        {
            MEMBER* locMemList = m_pFlatBlock->local_param.list;
            int locMemCount = m_pFlatBlock->local_param.count;
            for (int index = 0; index < locMemCount; index++, locMemList++)
            {
                ITEM_ID itemId = locMemList->ref.id;
                int itemInfoHandle = EMPTY_HANDLE;
                bool isValid = isRequestedItem(itemId, &requestedItemIDs,
                                                        &itemInfoHandle);
                if (!isValid)
                {
                    LOGF_INFO(CPMAPP,"Requested id is not matching with current local item id");
                }
                else
                {
                    resolveParameterType(members, itemType, itemId, isMemberVar, &itemSpec);

                    //Reset genericItem for each loop
                    genericItem = {};

                    //genericItem.item_type = memList->ref.type;
                    status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                                ALL_ITEM_ATTR_MASK, &genericItem);

                    if (!status.hasError())
                    {
                        // retrieves item info based on item type
                        retrieveItemInfoBasedOnItemType(index, mask, containerId,
                                                        itemInfoHandle, envInfo,
                                                        members, &genericItem);
                        isSuccess = true;
                    }

                    // Clears the genericItem memory.
                    m_pDDS->ddiCleanItem(&genericItem);
                }
            }
        }
    }

    //returns error for invalid item id
    if (itemInfoHandle == EMPTY_HANDLE && isSuccess == false)
    {
        errorCode = FAILURE;
    }

    //clear the request item IDs
    requestedItemIDs.clear();

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    verifies whether requested itemId is member variable or not
*/
void FFParameterCache::resolveParameterType(
std::vector<std::reference_wrapper<ItemInfoType>> members, ITEM_TYPE &itemType,
                ITEM_ID itemId, bool isMemberVar, DDI_ITEM_SPECIFIER* itemSpec)
{
    if(isMemberVar)
    {
       for(auto item : members)
       {
            VariableType* varType = dynamic_cast<VariableType*>(&item.get());
            if(nullptr != varType && varType->containerId != 0)
            {
                itemSpec->item.id = varType->itemID;
                itemType = RECORD_ITYPE;
            }
       }
    }
    else
    {
        itemSpec->item.id = itemId;
        itemType = VARIABLE_ITYPE;
    }

    itemSpec->type = DDI_ITEM_ID;
}

/*  ----------------------------------------------------------------------------
    This function is used to Retrieve all/requested MEMBER_LIST items
*/
void FFParameterCache::retrieveItemInfoBasedOnItemType(int index, MaskType mask,
                unsigned long containerId,int itemInfoHandle, ENV_INFO* envInfo,
                     std::vector<std::reference_wrapper<ItemInfoType>>& members,
                                                  DDI_GENERIC_ITEM* genericItem)
{
    ItemInfoType* param;
    int internalErrorCode = FAILURE;

    // Based on itemType retrieves all the items in the MEMBER_LIST.
    if (genericItem->item_type == VARIABLE_ITYPE)
    {
        FLAT_VAR* flatVar = static_cast<FLAT_VAR*>(genericItem->item);
        if (nullptr == flatVar)
        {
            LOGF_FATAL(CPMAPP, "null Flat Var never expected upon"
                           " successfull ddi_get_item call");
        }
        if (itemInfoHandle != EMPTY_HANDLE)
        {
            // Since itemInfoHandle grabbed from member list
            // it is not required to check itemInfohandle is exist or not
            param = &members.at(itemInfoHandle).get();
            // Caller don't know the actual type
            if (PARAM_DEFAULT == param->itemType)
            {
                //Recreate the appropriate structure
                auto temp = new VariableType;
                temp->itemID = param->itemID;
                temp->response_code = param->response_code;

                members.erase(itemInfoHandle+members.begin());

                //insert recreated structure at the same position
                param  = temp;
                members.insert(itemInfoHandle+members.begin(), *param);
                temp = nullptr;
            }
        }
        else
        {
            // Create new item which should be added to the
            // paramInfo paramlist.
            param = new VariableType;
            param->isMemAllocated = true;
            param->response_code = DDI_INVALID_PARAM;
        }

        VariableType* variable = dynamic_cast<VariableType*>(param);
        // expected null when requested param is of different item type
        if (nullptr == variable)
        {
            LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
        }
        else
        {
            if (param->itemType != PARAM_VARIABLE)
            {
                logMismatch(param, std::string("VaraibleType"));
            }
            else
            {
                if(variable->containerId == 0)
                {
                    variable->containerId = containerId;
                    if (0 < containerId)
                    {
                        variable->subIndex = index+1;
                    }
                    else
                    {
                        variable->subIndex = 0;
                    }
                }

                // Retrieves all the attributes of the item
                internalErrorCode = generateFlatItem(flatVar, param,
                            flatVar->masks.bin_exists, envInfo, mask);
            }
        }
    }
    else if (genericItem->item_type == ARRAY_ITYPE)
    {
        internalErrorCode = SUCCESS;
        FLAT_ARRAY* flatArray =
            static_cast<FLAT_ARRAY*>(genericItem->item);

        if (flatArray == nullptr)
        {
            LOGF_FATAL(CPMAPP, "null Flat Array never expected upon"
                           " successfull ddi_get_item call");
        }
        if (itemInfoHandle != EMPTY_HANDLE)
        {
            // Since itemInfoHandle assigned from member list
            // it is not required to validate itemInfohandle is exist or not
            param = &members.at(itemInfoHandle).get();

            //Caller don't know the actual type
            if (PARAM_DEFAULT == param->itemType)
            {
                //Recreate the appropriate structure
                auto temp = new ArrayType;
                temp->itemID = param->itemID;
                temp->response_code = param->response_code;

                members.erase(itemInfoHandle+members.begin());

                //insert recreated structure at the same position
                param  = temp;
                members.insert(itemInfoHandle+members.begin(), *param);
                temp = nullptr;

            }
            else if (param->itemType != PARAM_ARRAY)
            {
                logMismatch(param, std::string("ArrayType"));
                internalErrorCode = FAILURE;
            }

        }
        else
        {
            // Create new item which should be added in the
            // paramInfo paramlist.
            param = new ArrayType;
            param->isMemAllocated = true;
            param->response_code = DDI_INVALID_PARAM;
        }
        if (SUCCESS == internalErrorCode){
            // Retrieves all the attributes of the item
            internalErrorCode = generateFlatItem(
                flatArray,
                param,
                flatArray->masks.bin_exists,
                envInfo, mask);
        }
    }
    else if (genericItem->item_type == RECORD_ITYPE)
    {
        internalErrorCode = SUCCESS;
        FLAT_RECORD* flatRecord =
            static_cast<FLAT_RECORD*>(genericItem->item);

        if (flatRecord == nullptr)
        {
            LOGF_FATAL(CPMAPP, "null Flat Array never expected upon"
                           " successfull ddi_get_item call");
        }
        if (itemInfoHandle != EMPTY_HANDLE)
        {
            param = &members.at(itemInfoHandle).get();

            //Caller don't know the actual type
            if (PARAM_DEFAULT == param->itemType)
            {
                //Recreate the appropriate structure
                auto temp = new RecordType;
                temp->itemID = param->itemID;
                temp->response_code = param->response_code;

                members.erase(itemInfoHandle+members.begin());

                //insert recreated structure at the same position
                param = temp;
                members.insert(itemInfoHandle+members.begin(), *param);
                temp = nullptr;
            }
            else if (param->itemType != PARAM_RECORD)
            {
                logMismatch(param, std::string("RecordType"));
                internalErrorCode = FAILURE;
            }
        }
        else
        {
            // Create new item which should be added in the
            // paramInfo paramlist.
            param = new RecordType;
            param->isMemAllocated = true;
            param->response_code = DDI_INVALID_PARAM;
        }

        // Retrieves all the attributes of the item
        if (SUCCESS == internalErrorCode)
        {
            internalErrorCode = generateFlatItem( flatRecord, param,
                        flatRecord->masks.bin_exists, envInfo, mask);
        }
    }
    else if (genericItem->item_type == IMAGE_ITYPE)
    {
        FLAT_IMAGE* flatImage = static_cast<FLAT_IMAGE*>(genericItem->item);
        if (nullptr == flatImage)
        {
            LOGF_FATAL(CPMAPP, "null Flat Image never expected upon"
                           " successfull ddi_get_item call");
        }
        if (itemInfoHandle != EMPTY_HANDLE)
        {
            // Since itemInfoHandle grabbed from member list
            // it is not required to check itemInfohandle is exist or not
            param = &members.at(itemInfoHandle).get();

            // Caller don't know the actual type
            if (PARAM_DEFAULT == param->itemType)
            {
                //Recreate the appropriate structure
                auto temp = new ImageType;
                temp->itemID = param->itemID;
                temp->response_code = param->response_code;

                members.erase(itemInfoHandle+members.begin());

                //insert recreated structure at the same position
                param = temp;
                members.insert(itemInfoHandle+members.begin(), *param);
                temp = nullptr;
            }
        }
        else
        {
            // Create new item which should be added to the
            // paramInfo paramlist.
            param = new ImageType;
            param->isMemAllocated = true;
            param->response_code = DDI_INVALID_PARAM;
        }

        ImageType* variable = dynamic_cast<ImageType*>(param);
        // expected null when requested param is of different item type
        if (nullptr == variable)
        {
            LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
        }
        else
        {
            if (param->itemType != PARAM_IMAGE)
            {
                logMismatch(param, std::string("ImageType"));
            }
            else
            {
                // Retrieves all the attributes of the item
                internalErrorCode = generateFlatItem(flatImage, param,
                            flatImage->masks.bin_exists, envInfo, mask);
            }
        }
    }

    if (SUCCESS == internalErrorCode)
    {
        if (itemInfoHandle == EMPTY_HANDLE)
        {
            members.push_back(*param);
        }
    }
    else
    {
        // Binary does not exist or requested item type not supported.
        LOG_WARN(CPMAPP, "Binary does not exist or Item type does not"
                      " supported yet : for the item " << param->itemID <<
                      ", item type :  " << param->itemType);
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to retrieve param value from the p_Ref,
    Null value shall be return if value is not available in parameter cache
*/
int FFParameterCache::getItemInfoFromRef(ENV_INFO* envInfo,
                                         VariableType* param)
{
    int            errorCode = SUCCESS;
    EVAL_VAR_VALUE evalVal{};
    P_REF          pRef{};

    if(param->containerId > 0)
    {
        pRef.id = param->containerId;
        pRef.subindex = param->subIndex;
    }
    else
    {
        pRef.id = param->itemID;
    }

    // Mask value for P_REF structure.
    pRef.type = PC_ITEM_ID_REF;

    //reads value from cache
    pc_get_param_cache_value(envInfo, &pRef, &evalVal);

    // CPMHACK: Reverse the bit for bit string record & arrays
    if (evalVal.type == DDS_BITSTRING && param->containerId > 0)
    {
        bitReverse(evalVal.val.s.str, evalVal.val.s.len);
    }

    //assign cache values to paramInfo
    errorCode = setValuesToParamInfo(&evalVal, param);

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This function is used to retrieve param value from the parameter cache,
    Null value shall be return if value is not available in parameter cache
*/
int FFParameterCache::generateFlatItem(void* flatItem, ItemInfoType* param,
                         ulong binExists, ENV_INFO* envInfo, MaskType mask)
{
    ArrayType*      array = nullptr;
    ImageType*      image = nullptr;
    int             errorCode = SUCCESS;
    FLAT_ARRAY*     flatArray = nullptr;
    FLAT_IMAGE*     flatImage = nullptr;
    FLAT_RECORD*    flatRecord = nullptr;
    FLAT_VAR*       flatVar = nullptr;
    RecordType*     record = nullptr;
    VariableType*   variable = nullptr;

    // Verifies binary info available for the supplied item.
    if (binExists == 0u)
    {
        errorCode = FAILURE;
    }

    if (SUCCESS == errorCode)
    {
        /* Based on itemType param information copied item by item to the
           paramList
        */
        switch (param->itemType)
        {
        case PARAM_VARIABLE:
        {
            variable = dynamic_cast<VariableType*>(param);
            flatVar = static_cast<FLAT_VAR*>(flatItem);
            if (nullptr == flatVar)
            {
                LOGF_FATAL(CPMAPP, "null Flat Var never expected upon " <<
                          "successfull ddi_get_item call");
            }
            if (nullptr == variable)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                break;
            }

            /* For subscription mask, only the type and class attribute
               info are needed*/
            if (mask == ITEM_ATTR_SUBSCRIPTION_MASK)
            {
                variable->misc.class_attr = flatVar->class_attr;
                variable->type = flatVar->type_size.type;

                break;
            }

            variable->itemID = flatVar->id;
            variable->type = flatVar->type_size.type;
            variable->misc.class_attr = flatVar->class_attr;
            variable->validity = flatVar->misc->valid;
            variable->size = flatVar->type_size.size;

            //gives unit for dynamic variables
            getUnitForDynamicVariables(envInfo, variable);

            //gives minimum variable information
            fillMinimumVarInformation(flatVar, variable);

            //gives maximum variable information
            fillMaximumVarInformation(flatVar, variable);

            //Fill all attributes available in flatVar
            getItemInfoFromRef(envInfo, variable);

            break;
        }

        case PARAM_ARRAY:
        {
            array = dynamic_cast<ArrayType*>(param);
            unsigned short arrSubIndex = array->subIndex;
            flatArray = static_cast<FLAT_ARRAY*>(flatItem);
            if (nullptr == array || nullptr == flatArray)
            {
                LOGF_FATAL(CPMAPP, "Parsing invalid flatArray or array data");
                break;
            }
            array->itemID = flatArray->id;
            if (0 != flatArray->help.len && nullptr != flatArray->help.str)
            {
                assignStringValue(array->help, flatArray->help);
            }

            if (0 != flatArray->label.len && nullptr != flatArray->label.str)
            {
                assignStringValue(array->label, flatArray->label);
            }
            array->validity = flatArray->valid;
            array->response_code = flatArray->resp_codes;

            // It is used to get each array information
            lookUpArrayElements(mask, envInfo, flatArray, array, arrSubIndex);

            // Still memory is not deleted and should be deleted out side the PC
            // by the caller
            param = nullptr;

            break;
        }
        case PARAM_RECORD:
        {
            record = dynamic_cast<RecordType*>(param);
            flatRecord = static_cast<FLAT_RECORD*>(flatItem);

            // It is used to get each record information
            fillRecordInformation(mask, envInfo, flatRecord, record);
            break;
        }
        case PARAM_IMAGE:
        {
            image = dynamic_cast<ImageType*>(param);
            flatImage = static_cast<FLAT_IMAGE*>(flatItem);
            if (nullptr == image || nullptr == flatImage)
            {
                LOGF_FATAL(CPMAPP, "Parsing invalid flatImage or image data");
                break;
            }

            image->itemID = flatImage->id;
            image->validity = flatImage->valid;

            if (0 != flatImage->help.len && nullptr != flatImage->help.str)
            {
                assignStringValue(image->help, flatImage->help);
            }
            if (0 != flatImage->label.len && nullptr != flatImage->label.str)
            {
                assignStringValue(image->label, flatImage->label);
            }
            if (0 != flatImage->path.len && nullptr != flatImage->path.str)
            {
                assignStringValue(image->imagePath, flatImage->path);
            }
            if (0 != flatImage->entry.length && nullptr != flatImage->entry.data)
            {
                image->length = flatImage->entry.length;
                image->data = flatImage->entry.data;
            }

            break;
        }
        default:
            errorCode = FAILURE;
            break;
        }
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This private function is used to fetch unit for dynamic variables
*/
void FFParameterCache::getUnitForDynamicVariables(ENV_INFO* envInfo,
                                                  VariableType* variable)
{
    PMStatus status;
    int errorCode = SUCCESS;
    bool isRead = false;

    FLAT_BLOCK *flatBlock = nullptr;
    DDI_ITEM_SPECIFIER   itemSpecifier{};
    DDI_GENERIC_ITEM     genericFlatItem{};
    DDI_BLOCK_SPECIFIER  blockSpecifier{};

    // Fill the Block Specifier Attributes
    blockSpecifier.type = DDI_BLOCK_HANDLE;
    blockSpecifier.block.handle = envInfo->block_handle;

    // Fill the item Specifier information
    itemSpecifier.type = DDI_ITEM_BLOCK;

    // Get the block information from the DDS
    status = m_pDDS->ddiGetItem(&blockSpecifier, &itemSpecifier,
                                envInfo, ALL_ITEM_ATTR_MASK, &genericFlatItem);
    if (genericFlatItem.item != NULL && !status.hasError())
    {
        flatBlock = static_cast<FLAT_BLOCK*>(genericFlatItem.item);
    }

    if(nullptr != flatBlock)
    {
        for(int count = 0 ; count < flatBlock->unit.count; count++)
        {
            DDI_GENERIC_ITEM genericItemUnit{};
            DDI_BLOCK_SPECIFIER blockSpec{};
            DDI_ITEM_SPECIFIER itemSpec{};
            itemSpec.type = DDI_ITEM_ID ;
            itemSpec.item.id = flatBlock->unit.list[count] ;
            blockSpec.block.handle = envInfo->block_handle;
            blockSpec.type = DDI_BLOCK_HANDLE;

            //gives unit item information
            status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo, UNIT_ATTR_MASKS,
                                     &genericItemUnit);
            if (!status.hasError())
            {
                FLAT_UNIT* flatUnit = static_cast<FLAT_UNIT*>(genericItemUnit.item);
                if(nullptr != flatUnit)
                {
                    for(int itr = 0 ; itr < flatUnit->items.var_units.count ; itr++)
                    {
                        if ((NULL != flatUnit->items.var_units.list) &&
                            (variable->itemID == flatUnit->items.var_units.list[itr].desc_id &&
                             variable->containerId == flatUnit->items.var_units.list[itr].op_id))
                        {
                            P_REF pRef{};
                            EVAL_VAR_VALUE varVal{};

                            DDI_ITEM_SPECIFIER itemSpec = {};
                            DDI_GENERIC_ITEM genericItem{};

                            if(SUCCESS == errorCode)
                            {
                                if(flatUnit->items.var.op_subindex > 0
                                        && flatUnit->items.var.op_id > 0)
                                {
                                    pRef.id = flatUnit->items.var.op_id;
                                    pRef.subindex = flatUnit->items.var.op_subindex;
                                }
                                else
                                {
                                    pRef.id = flatUnit->items.var.desc_id;
                                    pRef.subindex = 0;
                                }

                                pRef.type = PC_ITEM_ID_REF;

                                //reads the unit value from device
                                errorCode = pc_get_param_value(envInfo, &pRef, &varVal);

                                if(SUCCESS == errorCode)
                                {
                                    itemSpec.type = DDI_ITEM_ID ;
                                    itemSpec.item.id = pRef.id;
                                    blockSpec.block.handle = envInfo->block_handle;
                                    blockSpec.type = DDI_BLOCK_HANDLE;

                                    //gives variable information
                                    status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                                             ALL_ITEM_ATTR_MASK, &genericItem);
                                }
                            }

                            if (!status.hasError())
                            {
                                if(genericItem.item_type == RECORD_ITYPE)
                                {
                                    FLAT_RECORD* flatRecord = static_cast<FLAT_RECORD*>(genericItem.item);
                                    if(nullptr != flatRecord)
                                    {
                                        MEMBER* member = flatRecord->members.list;
                                        for(int rec = 0 ; rec < flatRecord->members.count ; rec ++)
                                        {
                                            DDI_GENERIC_ITEM recGenericItem{};
                                            itemSpec = {};

                                            itemSpec.type = DDI_ITEM_ID ;
                                            itemSpec.item.id = member[rec].ref.id;
                                            blockSpec.block.handle = envInfo->block_handle;
                                            blockSpec.type = DDI_BLOCK_HANDLE;

                                            //gives variable information
                                            status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                                                     ALL_ITEM_ATTR_MASK, &recGenericItem);

                                            //verifies unit item is present in the record member
                                            if(member[rec].ref.id == flatUnit->items.var.desc_id)
                                            {
                                                // cleans generic item
                                                m_pDDS->ddiCleanItem(&genericItem);

                                                genericItem = recGenericItem;

                                                break;
                                            }

                                            // cleans generic item
                                            m_pDDS->ddiCleanItem(&recGenericItem);
                                        }
                                    }
                                    else
                                    {
                                        LOGF_ERROR(CPMAPP, "fetching record info failed");
                                    }
                                }

                                FLAT_VAR* flatVar = static_cast<FLAT_VAR*>(genericItem.item);
                                if(nullptr != flatVar)
                                {
                                    ENUM_VALUE* tempEnum = flatVar->enums.list;
                                    for (int cnt = 0; cnt < flatVar->enums.count; cnt++)
                                    {
                                        // verifies unit value is present in the enum list
                                        if(varVal.val.u == tempEnum[cnt].val)
                                        {
                                            assignStringValue(variable->unit, tempEnum[cnt].desc);
                                            isRead = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    LOGF_ERROR(CPMAPP, "fetching variable info failed");
                                }

                                // cleans generic item
                                m_pDDS->ddiCleanItem(&genericItem);
                                break;
                            }
                            else
                            {
                                LOGF_ERROR(CPMAPP, "fetching ddi_get_item failed");

                                // cleans generic item
                                m_pDDS->ddiCleanItem(&genericItem);
                            }
                        }
                    }
                }

                FLAT_REFRESH* flatRefresh = static_cast<FLAT_REFRESH*>(genericItemUnit.item);
                if(nullptr != flatRefresh)
                {
                    for(int itr = 0 ; itr < flatRefresh->items.update_items.count ; itr++)
                    {
                        if ((NULL != flatRefresh->items.update_items.list) &&
                            (variable->itemID == flatRefresh->items.update_items.list[itr].desc_id))
                        {
                            P_REF pRef{};
                            EVAL_VAR_VALUE varVal{};

                            if(flatRefresh->items.update_items.list[itr].op_subindex > 0
                                    && flatRefresh->items.update_items.list[itr].op_id > 0)
                            {
                                pRef.id = flatRefresh->items.update_items.list[itr].op_id;
                                pRef.subindex = flatRefresh->items.update_items.list[itr].op_subindex;
                            }
                            else
                            {
                                pRef.id = flatRefresh->items.update_items.list[itr].desc_id;
                                pRef.subindex = 0;
                            }

                            pRef.type = PC_ITEM_ID_REF;

                            //reads the unit value from device
                            errorCode = pc_get_param_value(envInfo, &pRef, &varVal);
                        }
                    }
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "fetching unit item failed");
            }

            // cleans generic item
            m_pDDS->ddiCleanItem(&genericItemUnit);

            // break the loop if unit variable read
            if(true == isRead)
            {
                break;
            }
        }

        // cleans generic item
        m_pDDS->ddiCleanItem(&genericFlatItem);
    }
}

/*  ----------------------------------------------------------------------------
    This private function is used to fill mimimal variable information
*/
void FFParameterCache::fillMinimumVarInformation(FLAT_VAR* flatVar,
                                            VariableType* variable)
{
    // Assign label, help
    if (0 != flatVar->help.len && nullptr != flatVar->help.str)
    {
        assignStringValue(variable->help, flatVar->help);
    }
    if (0 != flatVar->label.len && nullptr != flatVar->label.str)
    {
        assignStringValue(variable->label, flatVar->label);
    }

    // Assign unit
    if (0 != flatVar->misc->unit.len && nullptr != flatVar->misc->unit.str)
    {
        // TODO (Aravind): This Hack must be removed once fix is made for dictionary strings
#ifdef DICT_STR_HACK
        if (strcmp(flatVar->misc->unit.str, "ERROR: Dict String Not Found") != 0)
        {
            assignStringValue(variable->unit, flatVar->misc->unit);
        }
#else
        assignStringValue(variable->unit, flatVar->misc->unit);
#endif
    }

    // Assign display format
    if (0 != flatVar->display.len && nullptr != flatVar->display.str)
    {
        assignStringValue(variable->displayFormat, flatVar->display);
    }

    variable->response_code = flatVar->resp_codes;
    variable->misc.handling = flatVar->handling;

    // added for enum variable
    if (flatVar->enums.count)
    {
        // allocates memory for enum list
        ENUM_VALUE* enumVal = flatVar->enums.list;

        for (int enumCount = 0 ; enumCount < flatVar->enums.count;
             enumCount++, enumVal++)
        {
            auto enumValue = new EnumValue{};
            enumValue->value = flatVar->enums.list[enumCount].val;

            if (0 != flatVar->enums.list[enumCount].desc.len && nullptr
                    != flatVar->enums.list[enumCount].desc.str)
            {
                assignStringValue(enumValue->label, flatVar->enums.list[enumCount].desc);
            }

            if (0 != flatVar->enums.list[enumCount].help.len && nullptr
                    != flatVar->enums.list[enumCount].help.str)
            {
                assignStringValue(enumValue->help, flatVar->enums.list[enumCount].help);
            }

            variable->enums.push_back(*enumValue);

            if(nullptr != enumValue)
            {
                delete enumValue;
                enumValue = nullptr;
            }
        }
    }
}

/*  ----------------------------------------------------------------------------
    This private function is used to fill maximum variable information
*/
void FFParameterCache::fillMaximumVarInformation(FLAT_VAR* flatVar,
                                            VariableType* variable)
{
    // Assign edit format
    if (0 != flatVar->edit.len && nullptr != flatVar->edit.str)
    {
        assignStringValue(variable->editFormat, flatVar->edit);
    }

    //added for min max value range
    MinMaxRange minMaxRange{};

    if (0 != flatVar->misc->max_val.count && nullptr != flatVar->misc->max_val.list)
    {
        EXPR *rangeList = flatVar->misc->max_val.list;
        variable->misc.isMaxPresent = true;
        for (int maxCount = 0 ; maxCount < flatVar->misc->max_val.count;
             maxCount++, rangeList++)
        {
            switch(variable->type)
            {
            case DDS_INTEGER:
                minMaxRange.max.l = rangeList->val.i;
                break;

            case DDS_UNSIGNED:
                minMaxRange.max.ul = rangeList->val.u;
                break;

            case DDS_FLOAT:
                minMaxRange.max.f = rangeList->val.f;
                break;

            case DDS_DOUBLE:
                minMaxRange.max.d = rangeList->val.d;
                break;
            }
        }
    }

    if (0 != flatVar->misc->min_val.count && nullptr != flatVar->misc->min_val.list)
    {
        EXPR *rangeList = flatVar->misc->min_val.list;
        variable->misc.isMinPresent = true;
        for(int minCount = 0 ; minCount < flatVar->misc->min_val.count ; minCount++, rangeList++)
        {
            switch(variable->type)
            {
            case DDS_INTEGER:
                minMaxRange.min.l = rangeList->val.i;
                break;

            case DDS_UNSIGNED:
                minMaxRange.min.ul = rangeList->val.u;
                break;

            case DDS_FLOAT:
                minMaxRange.min.f = rangeList->val.f;
                break;

            case DDS_DOUBLE:
                minMaxRange.min.d = rangeList->val.d;
                break;
            }
        }
    }

    if (variable->misc.isMinPresent || variable->misc.isMaxPresent)
    {
        variable->misc.minMaxRange.push_back(minMaxRange);
    }
}

/*  ----------------------------------------------------------------------------
    This private function is used to get each array information
*/
void FFParameterCache::lookUpArrayElements
(
    MaskType mask, 
    ENV_INFO* envInfo,
    FLAT_ARRAY* flatArray, 
    ArrayType* array,
    unsigned short arrSubIndex
)
{
    PMStatus status;
    int errorCode = SUCCESS;
    DDI_ITEM_SPECIFIER itemSpec{};
    DDI_BLOCK_SPECIFIER blockSpec{};
    DDI_GENERIC_ITEM genericItem{};

    blockSpec.type = DDI_BLOCK_HANDLE;
    blockSpec.block.handle = envInfo->block_handle;
    itemSpec.item.id = flatArray->type;
    itemSpec.type = DDI_ITEM_ID;

    if (0 == arrSubIndex)
    {
        // Add all array member into member list
        for (int index = 1; index <= static_cast<int>(flatArray->num_of_elements); index++)
        {
            itemSpec.subindex = index;
            genericItem = {};
            status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                     ALL_ITEM_ATTR_MASK, &genericItem);
            if (!status.hasError())
            {
                auto flatVar = static_cast<FLAT_VAR*>(genericItem.item);
                if (flatVar == nullptr)
                {
                    LOGF_FATAL(CPMAPP,
                              "null Flat Var never expected upon " <<
                              "successfull ddi_get_item call"
                              );
                }

                auto param = new VariableType{};
                param->isMemAllocated = true;
                param->response_code = DDI_INVALID_PARAM;
                param->containerId = flatArray->id;
                param->subIndex = index;
                // Retrieves all the items in MEMBER_LIST
                errorCode = FAILURE;
                errorCode = generateFlatItem(
                            flatVar,
                            param,
                            flatVar->masks.bin_exists,
                            envInfo, mask);

                if (errorCode == SUCCESS)
                {
                    array->members.push_back(*param);
                }
            }

            m_pDDS->ddiCleanItem(&genericItem);
        }
    }
    else
    {
        // Add corresponding member indicated bu sub index into member list
        itemSpec.subindex = arrSubIndex;
        genericItem = {};
        status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo,
                                 ALL_ITEM_ATTR_MASK, &genericItem);

        if (!status.hasError())
        {
            auto flatVar = static_cast<FLAT_VAR*>(genericItem.item);
            if (flatVar == nullptr)
            {
                LOGF_FATAL(CPMAPP,
                          "null Flat Var never expected upon " <<
                          "successfull ddi_get_item call"
                          );
            }

            auto param = new VariableType{};
            param->isMemAllocated = true;
            param->response_code = DDI_INVALID_PARAM;
            param->containerId = flatArray->id;
            param->subIndex = arrSubIndex;
            // Retrieves all the items in MEMBER_LIST
            errorCode = FAILURE;
            errorCode = generateFlatItem(
                        flatVar,
                        param,
                        flatVar->masks.bin_exists,
                        envInfo, mask);

            if (errorCode == SUCCESS)
            {
                array->members.push_back(*param);
            }
        }

        m_pDDS->ddiCleanItem(&genericItem);
    }
}

/*  ----------------------------------------------------------------------------
    This private function is used to fill record information
*/
void FFParameterCache::fillRecordInformation(MaskType mask, ENV_INFO* envInfo,
                                             FLAT_RECORD* flatRecord, RecordType* record)
{
    int errorCode = SUCCESS;
    if (nullptr == flatRecord)
    {
        LOGF_FATAL(CPMAPP,"null Flat record never expected upon " <<
                  "successfull ddi_get_item call");
    }

    if(nullptr != record && nullptr != flatRecord)
    {
        record->itemID = flatRecord->id;
        if (0 != flatRecord->help.len && nullptr != flatRecord->help.str)
        {
            assignStringValue(record->help, flatRecord->help);
        }
        if (0 != flatRecord->label.len && nullptr != flatRecord->label.str)
        {
            assignStringValue(record->label, flatRecord->label);
        }
        record->response_code = flatRecord->resp_codes;
        record->validity = flatRecord->valid;

        std::vector<std::reference_wrapper<ItemInfoType>> members = record->members;
        errorCode = getParamItem(flatRecord->members.list,flatRecord->members.count,
                                 envInfo, mask, members,flatRecord->id,record->subIndex);
        if (SUCCESS != errorCode)
        {
            errorCode = FAILURE;
        }
        else
        {
            record->members = members;
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Cannot parse null record data");
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the cache. if it is not available
    in the cache it reads from device and update cache as well
*/
PMStatus FFParameterCache::readValue(ParamInfo* paramInfo)
{
    APP_INFO    appInfo{};
    ENV_INFO    envInfo{};
    int         errorCode = SUCCESS;
    PMStatus    status{};

    envInfo.app_info = &appInfo;
    envInfo.block_handle = EMPTY_HANDLE;
    appInfo.status_info = USE_DEV_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        errorCode = FAILURE;
    }
    else
    {
        // Locks the thread to prevent access from other thread
        this->acquireLock();

        errorCode = getParametersValue(paramInfo->paramList, &envInfo,
                                       paramInfo->requestInfo.maskType);

        // Unlocks the thread to provide access for other thread
        this->releaseLock();
    }

    if (SUCCESS != errorCode)
    {
        status.setError(
                    FF_PC_ERR_READ_PARAM_VALUES_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This private function is used to read value from the device if not
    available in cache
*/
int FFParameterCache::getParametersValue(
    std::vector<std::reference_wrapper<ItemInfoType>> members,
        ENV_INFO* envInfo, MaskType maskType)
{
    int             errorCode = SUCCESS;
    EVAL_VAR_VALUE  evalVal{};
    P_REF           pRef{};
    bool            hasBlockTag = true;
    if (envInfo->block_handle < 0)
    {
        hasBlockTag = false;
    }
    for(auto item : members)
    {
        // gets the ItemInfo Type
        ItemInfoType* itemInfo = &item.get();

        if (!hasBlockTag)
        {
            envInfo->block_handle = m_pDDS->getBlockHandleByBlockTag(
                        itemInfo->itemContainerTag.str);
            // validates block handle
            if (valid_block_handle(envInfo->block_handle) == 0)
            {
                continue;
            }
        }

        if (PARAM_VARIABLE == itemInfo->itemType)
        {
            auto variable = dynamic_cast<VariableType*>(itemInfo);
            //expected null if itemInfo is of differentType
            if (nullptr == variable)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data");
                continue;
            }

            if (0 != variable->containerId) //set Record or Array Item ID for its members
            {
                pRef.id = variable->containerId;
            }
            else
            {
                pRef.id = variable->itemID;
            }

            pRef.subindex = variable->subIndex;
            pRef.type = PC_ITEM_ID_REF;  // mask value for P_REF.

            /* For Subscription mask, read value from device if not read yet and
               if it is dynamic variable*/
            if (maskType == ITEM_ATTR_SUBSCRIPTION_MASK)
            {
                if((variable->valueState == VALUE_NOT_READ)
                        || variable->misc.class_attr & DYNAMIC_CLASS)
                {

                    errorCode = pc_read_param_value(envInfo, &pRef);                 

                    //gives unit for dynamic variables
                    getUnitForDynamicVariables(envInfo, variable);
                }

                if (errorCode == SUCCESS && variable->valueState == VALUE_NOT_READ)
                {
                    variable->valueState = VALUE_READ;
                }
            }

            // Reads value from the device.
            errorCode = pc_get_param_cache_value(envInfo, &pRef, &evalVal);

            if (SUCCESS == errorCode)
            {
                // CPMHACK: Reverse the bit for bit string record & arrays
                if (evalVal.type == DDS_BITSTRING && variable->containerId > 0)
                {
                    bitReverse(evalVal.val.s.str, evalVal.val.s.len);
                }

                // Based on type values added to the paramList.
                setValuesToParamInfo(&evalVal, variable);
            }
        }
        else if (PARAM_ARRAY == itemInfo->itemType)
        {
            ArrayType* array = dynamic_cast<ArrayType*>(itemInfo);
            //expected null if itemInfo is of differentType
            if (nullptr == array)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null array data");
                continue;
            }

            getParametersValue(array->members, envInfo, maskType);

        }
        else if (PARAM_RECORD == itemInfo->itemType)
        {
            RecordType* record = dynamic_cast<RecordType*>(itemInfo);
            //expected null if itemInfo is of differentType
            if (nullptr == record)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null record data");
                continue;
            }
            getParametersValue(record->members, envInfo, maskType);
        }
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This private function is used to write value into the paramInfo structure
*/
int FFParameterCache::setValuesToParamInfo(EVAL_VAR_VALUE* evalVal,
                                           VariableType* param)
{
    int errorCode = SUCCESS;
    // Assign variable size and type to the paramList contained variable.
    param->size = evalVal->size;
    param->type = evalVal->type;
    param->valueChanged = false;

    // Based on element type value copied to ParamInfo structure
    switch (evalVal->type)
    {
        case DDS_INTEGER:
        if (param->value.stdValue.l != evalVal->val.i)
        {
            param->value.stdValue.l = evalVal->val.i;
            param->valueChanged = true;
        }
        break;

        case DDS_UNSIGNED:      //FALLTHROUGH
        case DDS_ENUMERATED:    //FALLTHROUGH
        case DDS_BIT_ENUMERATED://FALLTHROUGH
        case DDS_INDEX:         //FALLTHROUGH
        case DDS_BOOLEAN_T:     //FALLTHROUGH
        case DDS_ACK_TYPE:      //FALLTHROUGH
        case DDS_MAX_TYPE:
        if (param->value.stdValue.ul != evalVal->val.u)
        {
            param->value.stdValue.ul = evalVal->val.u;
            param->valueChanged  = true;
        }
        break;

        case DDS_FLOAT:
        if (param->value.stdValue.f != evalVal->val.f)
        {
            param->value.stdValue.f = evalVal->val.f;
            param->valueChanged  = true;
        }
        break;

        case DDS_DOUBLE:
        if (param->value.stdValue.d != evalVal->val.d)
        {
            param->value.stdValue.d = evalVal->val.d;
            param->valueChanged  = true;
        }
        break;

        case DDS_BITSTRING:
        {
            if (nullptr != evalVal->val.s.str)
            {
                STRING data{};
                bitReverse(evalVal->val.s.str, evalVal->val.s.len);
                assignBitStringValue(evalVal->val.s, data);
                if (nullptr == param->value.strData.str)
                {
                    assignStringValue(param->value.strData, data);
                    param->valueChanged  = true;
                }
                else
                {
                    if (strcmp(param->value.strData.str, data.str) != 0)
                    {
                        assignStringValue(param->value.strData, data);
                        param->valueChanged  = true;
                    }
                }
                param->value.strData.ucType = UC_ASCII;
                if (nullptr != data.str && FREE_STRING == data.flags)
                {
                    delete[] data.str;
                    data.str = nullptr;
                }
            }
            else
            {
                errorCode = FAILURE;
            }
            cleanEvalVar(evalVal);
        }
        break;

        case DDS_ASCII:         //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        {
            if (nullptr != evalVal->val.s.str)
            {
                if (nullptr == param->value.strData.str)
                {
                    assignStringValue(param->value.strData, evalVal->val.s, true);
                    param->valueChanged  = true;
                }
                else
                {
                    if (strcmp(param->value.strData.str, evalVal->val.s.str) != 0)
                    {
                        assignStringValue(param->value.strData, evalVal->val.s, true);
                        param->valueChanged  = true;
                    }
                }
                param->value.strData.ucType = UC_ISO_LATIN_1;
            }
            else
            {
                errorCode = FAILURE;
            }
            cleanEvalVar(evalVal);
        }
        break;

        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        {
            if (nullptr != evalVal->val.s.str)
            {
                if (nullptr == param->value.strData.str)
                {
                    assignStringValue(param->value.strData, evalVal->val.s, true);
                    param->valueChanged  = true;
                }
                else
                {
                    if (strcmp(param->value.strData.str, evalVal->val.s.str) != 0)
                    {
                        assignStringValue(param->value.strData, evalVal->val.s, true);
                        param->valueChanged  = true;
                    }
                }
                param->value.strData.ucType = UC_ASCII;
                // DP-1312: Octet string may contain unicode character within it.
                // This unicode character after conversion to utf8 may vary actual string length.
                // But when it passed to IC, we need to pass maximum permissible length 
                // configured in DD and not actual length of the octet string.
                if (DDS_OCTETSTRING == evalVal->type)
                {
                    if (param->value.strData.len > evalVal->val.s.len)
                    {           
                        param->value.strData.len = evalVal->val.s.len;
                    }
                }
            }
            else
            {
                errorCode = FAILURE;
            }
            cleanEvalVar(evalVal);
        }
        break;

        case DDS_EUC:
        {
            if (nullptr != evalVal->val.s.str)
            {
                if (nullptr == param->value.strData.str)
                {
                    assignStringValue(param->value.strData, evalVal->val.s);
                    param->valueChanged  = true;
                }
                else
                {
                    if (strcmp(param->value.strData.str, evalVal->val.s.str) != 0)
                    {
                        assignStringValue(param->value.strData, evalVal->val.s);
                        param->valueChanged  = true;
                    }
                }
                param->value.strData.ucType = UC_UTF_8;
            }
            else
            {
                errorCode = FAILURE;
            }
            cleanEvalVar(evalVal);
        }
        break;

        case DDS_TIME:
        {
            if (memcmp(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH) != 0)
            {
                memcpy(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH);
                param->valueChanged  = true;
            }
        }
        break;

        case DDS_TIME_VALUE:
        {
            if (memcmp(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH) != 0)
            {
                memcpy(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH);
                param->valueChanged  = true;
            }
        }
        break;

        case DDS_DATE_AND_TIME:
        {
            if (memcmp(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH) != 0)
            {
                memcpy(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH);
                param->valueChanged  = true;
            }
        }
        break;

        case DDS_DURATION:
        {
            if (memcmp(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH) != 0)
            {
                memcpy(param->value.CA, evalVal->val.ca, DATENTIME_ARRAY_LENGTH);
                param->valueChanged  = true;
            }
        }
        break;

        default:
            errorCode = FAILURE;
        break;
    }

    return errorCode;
}

/*   ----------------------------------------------------------------------------
     Allocates memory for char pointer and asssigns string to it.
*/
void FFParameterCache::assignStringValue(StringType& variable, STRING str, bool bConvert)
{
    char* strData = NULL;
    unsigned int strLength = 0;
    char utf8Data[MAX_UTF8_STR_LENGTH]{};

    //deallocates memory allocated by FF PC
    if(variable.flag == FREE_STRING)
    {
        delete[] variable.str;
        variable.str = nullptr;
        variable.flag = DONT_FREE_STRING;
    }

    // Convert ISO-Latin-1 to UTF8 string data
    // bConvert is true for all string types except EUC
    // If the string is from DD, it may be either ISO_LATIN_1 or UTF_8
    //          If its ISO_LATIN_1, its required to convert from ISO_LATIN_1 to UTF_8
    // If the string is from Dictionary, it will always be UTF_8 (as CPM loads only standard.dc8 file)
    //          And so, its not required to convert
    if ((true == bConvert) || 
        ((str.flags & DD_STRING) && (ISO_LATIN_1 == m_pDDS->getDDStringEncoding())))
    {
        ddi_iso_to_utf8(str.str, utf8Data, MAX_UTF8_STR_LENGTH);
        strData = utf8Data;
        strLength = str.len;    
        if (strlen (utf8Data) > DDS_STR_LENGTH)
            strLength = strlen (utf8Data);
    }
    else // Consider the string as UTF8 
    {
        strData = str.str;
        strLength = str.len;
    }

    variable.str = new char[strLength +  1]();
    strncpy(variable.str, strData, strLength);
    variable.str[strLength] = '\0';
    variable.len = strLength;
    // flags specifies, allocated memory has to free by caller
    variable.flag = FREE_STRING;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value into the device and cache tables
*/
PMStatus FFParameterCache::writeValue(ParamInfo* paramInfo)
{
    APP_INFO appInfo{};
    ENV_INFO envInfo{};
    int      errorCode = SUCCESS;
    PMStatus status;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        errorCode = FAILURE;
    }
    if (SUCCESS == errorCode)
    {
        // Locks the thread to prevent access from other thread
        this->acquireLock();

        // This API gives block handle
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                               paramInfo->blockTag.str);

        // validates block handle
        if (valid_block_handle(envInfo.block_handle) != 0)
        {
            /* Writes requested value to the device as well update the parameter
               cache
            */
            errorCode = writeRequestedValueToDevice(
                       paramInfo->paramList, &envInfo);
        }
        else
        {
            errorCode = FAILURE;
        }

        // Unlocks the thread to provide access for other thread
        this->releaseLock();
    }
    if (SUCCESS != errorCode)
    {
        status.setError(
                    FF_PC_ERR_WRITE_VALUE_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This private function is used to write requested value to the device and
    update the parameter cache
*/
int FFParameterCache::writeRequestedValueToDevice(
                      std::vector<std::reference_wrapper<ItemInfoType>> members,
                                                                ENV_INFO* envInfo)
{
    int             errorCode = SUCCESS;
    EVAL_VAR_VALUE  evalVal{};
    P_REF           pRef{};

    for (auto item : members)
    {
        // gets ItemInfo Type
        ItemInfoType* iteminfo = &item.get();

        if (PARAM_VARIABLE == iteminfo->itemType)
        {
            auto variable = dynamic_cast<VariableType*>(iteminfo);

            //expected null when iteminfo is of different type
            if (nullptr == variable)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }

            // Assigns requested parameter Id to the P_REF structure.
            pRef.id = variable->itemID;
            pRef.subindex = variable->subIndex;

            if (variable->containerId > 0)
            {
                pRef.id = variable->containerId;
            }

            // temporary purpose.
            pRef.type = PC_ITEM_ID_REF;

            // assigns paramList contained value to local evalval.
            errorCode = getValuesParamInfo(variable, &evalVal);

            if (SUCCESS == errorCode)
            {
                // writes requested value to the device as well update the cache
                errorCode = pc_write_param_value(envInfo, &pRef, &evalVal);

                // CPMHACK : The below code is needed for reversing the bit
                // Bit String value needs to be reversed for cache values
                if (variable->type == DDS_BITSTRING)
                {
                    if (nullptr != variable->value.strData.str)
                    {
                        int bitStrLen = (variable->value.strData.len / BIT_STRING_OFFSET);
                        char* pos = variable->value.strData.str;
                        int ssize = strlen(pos);
                        char* tempStr = new char[ssize + 1];
                        strncpy(tempStr, pos, ssize);
                        if (ssize > BIT_STRING_OFFSET)
                        {
                            int lsb = 0;
                            int msb = ssize;
                            for (int i = 0; i < variable->value.strData.len / BIT_STRING_OFFSET; i++)
                            {
                                strncpy(tempStr + lsb, variable->value.strData.str + (msb - BIT_STRING_OFFSET),
                                       BIT_STRING_OFFSET);
                                lsb += BIT_STRING_OFFSET;
                                msb -= BIT_STRING_OFFSET;
                            }
                        }
                        tempStr[variable->value.strData.len] = '\0';

                        size_t count = 0;
                        for (count = 0; count < bitStrLen; ++count)
                        {
                            sscanf(tempStr, "%2hhx", &evalVal.val.s.str[count]);
                            tempStr += BIT_STRING_OFFSET;
                        }
                        evalVal.val.s.flags = FREE_STRING;
                        evalVal.val.s.len = bitStrLen;
                        if (variable->containerId == 0)
                        {
                            bitReverse(evalVal.val.s.str, evalVal.val.s.len);
                        }
                        pc_put_param_value(envInfo, &pRef, &evalVal);
                    }
                }
            }

            //Clean the evalVal structure
            /*
             *  TODO <Adesh> : Remove this work around function.
             *  Releasing BIT-string memory allocated using new.
             *  In other cases we are allocation memory using malloc.
             */
            cleanVarMemory(&evalVal);
        }
        else if (PARAM_ARRAY == iteminfo->itemType)
        {
            // Igonred static cast null checked, expected only Array type
            ArrayType* array = dynamic_cast<ArrayType*>(iteminfo);
            //expected null when iteminfo is of different type
            if (nullptr == array)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null array data\n");
                continue;
            }
            errorCode = writeRequestedValueToDevice(array->members, envInfo);
        }
        else if (PARAM_RECORD == iteminfo->itemType)
        {
            // Igonred static cast null checked, expected only recored type
            RecordType* record = dynamic_cast<RecordType*>(iteminfo);
            //expected null when iteminfo is of different type
            if (nullptr == record)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null record data\n");
                continue;
            }
            errorCode = writeRequestedValueToDevice(record->members, envInfo);
        }
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This private function is used to clean the Eval_value_structure structure
*/
int FFParameterCache::cleanEvalVar(EVAL_VAR_VALUE* evalVal)
{
    int errorCode = SUCCESS;
    // Clean the memory occupied by string type
    switch (evalVal->type)
    {
        case DDS_ASCII:         //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_EUC:
        case DDS_BITSTRING:
        {
            ddl_free_string(&evalVal->val.s);
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid type" << evalVal->type);
        }
        break;
    }
    return errorCode;
}


/*  ----------------------------------------------------------------------------
    This private function is used to clean the Eval_value_structure structure.
    This function is workaround to release memory allocated by new.
*/
int FFParameterCache::cleanVarMemory(EVAL_VAR_VALUE* evalVal)
{
    int errorCode = SUCCESS;
    // Clean the memory occupied by string type
    switch (evalVal->type)
    {
        case DDS_ASCII:         //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_EUC:
        case DDS_BITSTRING:
        {
        	if (evalVal->val.s.flags & FREE_STRING)
			{
				delete [] evalVal->val.s.str;
			}
			if ((evalVal->val.s.wflags & FREE_STRING) && (evalVal->val.s.wstr))
			{
				delete [] evalVal->val.s.wstr;
			}

			evalVal->val.s.flags = 0;
			evalVal->val.s.len = 0;
			evalVal->val.s.str = NULL;

			evalVal->val.s.wflags = 0;
			evalVal->val.s.wlen = 0;
			evalVal->val.s.wstr = NULL;
        }
        break;


        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid type" << evalVal->type);
        }
        break;
    }
    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This private function is used to get value from the paramInfo structure
*/
int FFParameterCache::getValuesParamInfo(
    VariableType* varVal, EVAL_VAR_VALUE* evalVal)
{
    int errorCode = SUCCESS;
    // Assign variable size and type to the EVAL_VAR_VALUE structure.
    evalVal->size = varVal->size;
    evalVal->type = varVal->type;

    // Based on element type value copied to ParamInfo structure
    switch (varVal->type)
    {
        case DDS_INTEGER:
            evalVal->val.i = varVal->value.stdValue.l;
        break;

        case DDS_UNSIGNED:      //FALLTHROUGH
        case DDS_ENUMERATED:    //FALLTHROUGH
        case DDS_BIT_ENUMERATED://FALLTHROUGH
        case DDS_INDEX:         //FALLTHROUGH
        case DDS_BOOLEAN_T:     //FALLTHROUGH
        case DDS_ACK_TYPE:      //FALLTHROUGH
        case DDS_MAX_TYPE:
            evalVal->val.u = varVal->value.stdValue.ul;
        break;

        case DDS_FLOAT:
            evalVal->val.f = varVal->value.stdValue.f;
        break;

        case DDS_DOUBLE:
            evalVal->val.d = varVal->value.stdValue.d;
        break;

        case DDS_BITSTRING:
        {
            if (nullptr != varVal->value.strData.str)
            {
                int bitStrLen = (varVal->value.strData.len / BIT_STRING_OFFSET);
                evalVal->val.s.str = new char[bitStrLen]();
                char* pos = varVal->value.strData.str;
                size_t count = 0;
                for (count = 0; count < bitStrLen; ++count)
                {
                    sscanf(pos, "%2hhx", &evalVal->val.s.str[count]);
                    pos += BIT_STRING_OFFSET;
                }
                evalVal->val.s.flags = FREE_STRING;
                evalVal->val.s.len = bitStrLen;
                bitReverse(evalVal->val.s.str, evalVal->val.s.len);
            }
        }
        break;

        case DDS_ASCII:         //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_EUC:
        {
            // this is a potential leak and should be cleaned by the caller
            // using cleanEvalVar api
            if (nullptr != varVal->value.strData.str)
            {
                std::string str(varVal->value.strData.str);
                evalVal->val.s.str = new char[str.length()]();
                strncpy(evalVal->val.s.str, str.c_str(), str.length());
                evalVal->val.s.len = str.length();
                evalVal->val.s.flags = FREE_STRING;
            }
            else
            {
                errorCode = FAILURE;
            }
        }
        break;

        case DDS_DATE:
        case DDS_TIME:
        case DDS_TIME_VALUE:
        case DDS_DATE_AND_TIME:
        case DDS_DURATION:
        {
            memcpy(evalVal->val.ca, varVal->value.CA, DATENTIME_ARRAY_LENGTH);
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid Variable Type" << varVal->type);
            errorCode = FAILURE;
        }
        break;
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the device and stores it into
    the cache table
*/
PMStatus FFParameterCache::readDeviceValue(ParamInfo* paramInfo)
{
    APP_INFO    appInfo{};
    ENV_INFO    envInfo{};
    int         errorCode = SUCCESS;
    PMStatus    status{};
    EVAL_VAR_VALUE  evalVal{};
    P_REF           pRef{};

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_DEV_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        errorCode = FAILURE;
    }

    if (SUCCESS == errorCode)
    {
        // Locks the thread to prevent access from other thread
        this->acquireLock();

        for(auto item : paramInfo->paramList)
        {
            // gets the ItemInfo Type
            ItemInfoType* itemInfo = &item.get();

            if (PARAM_VARIABLE == itemInfo->itemType)
            {
                auto variable = dynamic_cast<VariableType*>(itemInfo);
                //expected null if itemInfo is of differentType
                if (nullptr == variable)
                {
                    LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                    continue;
                }

                // This API gives block handle
                envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                            variable->itemContainerTag.str);

                // validates block handle
                if (valid_block_handle(envInfo.block_handle) != 0)
                {
                    if (0 != variable->containerId) //set Record or Array Item ID for its members
                    {
                        pRef.id = variable->containerId;
                    }
                    else
                    {
                        pRef.id = variable->itemID;
                    }

                    pRef.subindex = variable->subIndex;
                    pRef.type = PC_ITEM_ID_REF;  // mask value for P_REF.

                    // Reads value from the device.
                    errorCode = pc_read_param_value(&envInfo, &pRef);

                    // Don't read the Unit for internal subscription
                    ITEM_ID symID = EMPTY_HANDLE;
                    ENV_INFO2 envInfo2{};
                    env_to_env2(&envInfo, &envInfo2);
                    m_pDDS->ddiLocateSymbolId(&envInfo2, ST_REV_DD_NAME, &symID);

                    if (symID != variable->itemID)
                    {
                        //gives unit for dynamic variables
                        getUnitForDynamicVariables(&envInfo, variable);
                    }

                    // Set parameter as read once it is read from device
                    if(SUCCESS == errorCode && variable->valueState == VALUE_NOT_READ)
                    {
                        variable->valueState = VALUE_READ;
                    }

                    // Reads value from the cache.
                    errorCode = pc_get_param_cache_value(&envInfo, &pRef, &evalVal);

                    if (SUCCESS == errorCode)
                    {
                        // Based on type values added to the paramList.
                        setValuesToParamInfo(&evalVal, variable);
                    }
                }
                else
                {
                    continue;
                }
            }
        }

        // Unlocks the thread to provide access for other thread
        this->releaseLock();
    }

    if (SUCCESS != errorCode)
    {
        status.setError(
                    FF_PC_ERR_READ_DEVICE_VALUE_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value to the device and on success it
    stores value in cache table
*/
PMStatus FFParameterCache::writeDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;

    if (nullptr != paramInfo)
    {
        status = writeValue(paramInfo);
    }
    else
    {
        status.setError(
            FF_PC_ERR_WRITE_DEVICE_VALUE_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_PC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read edit value from the cache table
*/
PMStatus FFParameterCache::readEditValue(ParamInfo* paramInfo)
{
    int      errorCode = SUCCESS;
    PMStatus status;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        errorCode = FAILURE;
    }
    if (SUCCESS == errorCode)
    {
        // Locks the thread to prevent access from other thread
        this->acquireLock();

        // This API gives block handle
        BLOCK_HANDLE blockHandle = m_pDDS->getBlockHandleByBlockTag(
                               paramInfo->blockTag.str);

        ITEM_ID blockID;

        status = m_pDDS->getBlockIDByBlockHandle(blockHandle, &blockID);

        if (!status.hasError())
        {
            ItemInfoType& itemInfo = paramInfo->paramList.at(0);
            VariableType& varParms = dynamic_cast<VariableType&>(itemInfo);

			//status = m_pFFAdaptor->processExecuteEditMethod(varParms.itemID, blockID);


            if (status.hasError())
            {
                errorCode = FAILURE;
            }
        }
        else
        {
            errorCode = FAILURE;
        }

        // Unlocks the thread to provide access for other thread
        this->releaseLock();
    }
    if (SUCCESS == errorCode)
    {
        status.setError(
                    FF_PC_ERR_READ_EDIT_VALUE_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write edit value to the cache table
*/
PMStatus FFParameterCache::writeEditValue(ParamInfo* paramInfo)
{
    APP_INFO appInfo{};
    ENV_INFO envInfo{};
    int      errorCode = SUCCESS;
    PMStatus status;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        errorCode = FAILURE;
    }
    if (SUCCESS == errorCode)
    {
        // Locks the thread to prevent access from other thread
        this->acquireLock();

        // This API gives block handle
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                               paramInfo->blockTag.str);

        // validates block handle
        if (valid_block_handle(envInfo.block_handle) >= 0)
        {
            // Writes Edited value to the parameter cache.
            writesEditedValueToCache(
                        paramInfo->paramList, &envInfo);
        }
        else
        {
            errorCode = FAILURE;
        }

        // Unlocks the thread to provide access for other thread
        this->releaseLock();
    }
    if (SUCCESS != errorCode)
    {
        status.setError(
                    FF_PC_ERR_WRITE_EDIT_VALUE_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Performs pre subscription actions upon subscription timer elapsed
*/
PMStatus FFParameterCache::preSubscriptionActions(ParamInfo& paramInfo,
                                                  bool readFromDevice)
{
    PMStatus status;

    (void)status;

    return status;
}

/*  ----------------------------------------------------------------------------
    Performs subscription actions upon subscription timer elapsed
*/
PMStatus FFParameterCache::doSubscriptionActions(ParamInfo& paramInfo,
                                                         bool readFromDevice)
{
    PMStatus status;

    // If it is set to true, reads from device, otherwise from cache
    if (readFromDevice)
    {
        // reads from device
        status  = readDeviceValue(&paramInfo);
    }
    else
    {
        // reads from cache
        status = readValue(&paramInfo);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Reads value from the device and stores it into
    the cache table for the specified block
*/
PMStatus FFParameterCache::updateCacheTable(ParamInfo paramInfo, bool dynamicVars)
{
    PMStatus status;
    int             errorCode = SUCCESS;
    P_REF           pRef{};
    ENV_INFO            envInfo{};
    APP_INFO            appInfo;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_DEV_VALUE;

    // Locks the thread to prevent access from other thread
    this->acquireLock();

    // This API gives block handle
    envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                paramInfo.blockTag.str);

    // validates block handle
    if (valid_block_handle(envInfo.block_handle) == 0)
    {
        status.setError(
                    FF_PC_ERR_GET_ENVIRON_INFO_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    else
    {
        for(auto item : paramInfo.paramList)
        {
            // gets the ItemInfo Type
            ItemInfoType* itemInfo = &item.get();

            auto variable = dynamic_cast<VariableType*>(itemInfo);
            //expected null if itemInfo is of differentType
            if (nullptr == variable)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }

            bool isDynamic = variable->misc.class_attr & DYNAMIC_CLASS;

            if (dynamicVars == isDynamic)
            {
                if (0 != variable->containerId) //set Record or Array Item ID for its members
                {
                    pRef.id = variable->containerId;
                }
                else
                {
                    pRef.id = variable->itemID;
                }

                pRef.subindex = variable->subIndex;
                pRef.type = PC_ITEM_ID_REF;  // mask value for P_REF.

                // Reads value from the device.
                errorCode = pc_read_param_value(&envInfo, &pRef);

                if (errorCode == SUCCESS && variable->valueState == VALUE_NOT_READ)
                {
                     // set the value state as read once it is read
                     variable->valueState = VALUE_READ;
                }
            }
        }
    }

    // Unlocks the thread to provide access for other thread
    this->releaseLock();

    if (FAILURE == errorCode)
    {
        status.setError(
            FF_PC_ERR_READ_PARAM_VALUES_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_PC);
    }
}

/*  ----------------------------------------------------------------------------
    This private function is used to write edited value to the parameter cache
*/
int FFParameterCache::writesEditedValueToCache(
                    std::vector<std::reference_wrapper<ItemInfoType>> members,
                                                               ENV_INFO* envInfo)
{
    EVAL_VAR_VALUE  evalVal{};
    P_REF           pRef{};
    int errorCode = SUCCESS;

    for (auto item : members)
    {
        // gets ItemInfo Type
        ItemInfoType* iteminfo = &item.get();

        if (PARAM_VARIABLE == iteminfo->itemType )
        {
            // Assigns requested parameter Id to the P_REF structure.
            pRef.id = iteminfo->itemID;
            auto variable = dynamic_cast<VariableType*>(iteminfo);
            if (nullptr == variable)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null variable data\n");
                continue;
            }
            pRef.subindex = variable->subIndex;
            pRef.type = PC_ITEM_ID_REF;

            // CPMHACK : The below code is needed for reversing the bit
            // Bit String value needs to be reversed for cache values
            if (variable->type == DDS_BITSTRING)
            {
                if (nullptr != variable->value.strData.str)
                {
                    int bitStrLen = (variable->value.strData.len / BIT_STRING_OFFSET);
                    char* pos = variable->value.strData.str;
                    int ssize = strlen(pos);
                    char* tempStr = new char[ssize + 1];
                    strncpy(tempStr, pos, ssize);
                    if (ssize > BIT_STRING_OFFSET)
                    {
                        int lsb = 0;
                        int msb = ssize;
                        for (int i = 0; i < variable->value.strData.len / BIT_STRING_OFFSET; i++)
                        {
                            strncpy(tempStr + lsb, variable->value.strData.str + (msb - BIT_STRING_OFFSET),
                                   BIT_STRING_OFFSET);
                            lsb += BIT_STRING_OFFSET;
                            msb -= BIT_STRING_OFFSET;
                        }
                    }
                    tempStr[variable->value.strData.len] = '\0';

                    size_t count = 0;
                    for (count = 0; count < bitStrLen; ++count)
                    {
                        sscanf(tempStr, "%2hhx", &evalVal.val.s.str[count]);
                        tempStr += BIT_STRING_OFFSET;
                    }
                    evalVal.val.s.flags = FREE_STRING;
                    evalVal.val.s.len = bitStrLen;
                    if (variable->containerId == 0)
                    {
                        bitReverse(evalVal.val.s.str, evalVal.val.s.len);
                    }
                }
            }

            // Writes edited value to the parameter cache
            pc_put_param_value(envInfo, &pRef, &evalVal);

            //Clean the evalVal structure
            cleanEvalVar(&evalVal);
        }
        else if (PARAM_ARRAY == iteminfo->itemType )
        {
            // Igonred static cast null checked, expected only Array type
            ArrayType* array = dynamic_cast<ArrayType*>(iteminfo);

            //expected null when iteminfo is of different type
            if (nullptr == array)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null array data\n");
                continue;
            }
            errorCode = writesEditedValueToCache(array->members, envInfo);
        }
        else if (PARAM_RECORD == iteminfo->itemType )
        {
            // Igonred static cast null checked, expected only recored type
            RecordType* record = dynamic_cast<RecordType*>(iteminfo);

            //expected null when iteminfo is of different type
            if (nullptr == record)
            {
                LOGF_ERROR(CPMAPP, "Cannot parse null record data\n");
                continue;
            }
            errorCode = writesEditedValueToCache(record->members, envInfo);
        }
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    Retrives parameter info for the requested block view
*/
PMStatus FFParameterCache::getParameterFromBlockView(int viewIndex,
                                                     ParamInfo* paramInfo)
{
    APP_INFO            appInfo{};
    DDI_BLOCK_SPECIFIER blockSpec{};
    ENV_INFO            envInfo{};
    int                 errorCode = SUCCESS;
    FLAT_BLOCK*         flatBlock = nullptr;
    DDI_GENERIC_ITEM    genericItem{};
    DDI_ITEM_SPECIFIER  itemSpec{};
    PMStatus            status;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    /* Do not proceed further if paramInfo is null
       or requested view is out of range.
    */
    if (nullptr == paramInfo || viewIndex > VIEW_LIMIT)
    {
        status.setError(
                    FF_PC_ERR_SELECT_BLOCKVIEW_PARAMETER_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    else
    {
        // This API gives block handle
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                               paramInfo->blockTag.str);

        // validates block handle
        if (valid_block_handle(envInfo.block_handle) == 0)
        {
            status.setError(
                        FF_PC_ERR_GET_ENVIRON_INFO_FAILED,
                        PM_ERR_CL_FF_CSL,
                        CSL_ERR_SC_PC);
        }
        else
        {
            blockSpec.type = DDI_BLOCK_HANDLE;
            blockSpec.block.handle = envInfo.block_handle;
            genericItem = {};

            // Gets the requested item.
            status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, &envInfo,
                                     ALL_ITEM_ATTR_MASK, &genericItem);
            if (!status.hasError())
            {
                // Typecasted generic item to flat block.
                flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);

                if (nullptr == flatBlock)
                {
                    LOGF_FATAL(CPMAPP,
                              "null Flat block never expected upon " <<
                              "successfull ddi_get_item call"
                              );
                }

                MEMBER* memList = flatBlock->param_list.list;
                int paramCount = flatBlock->param_list.count;

                for(int index = 1; index <= paramCount; index++, memList++)
                {
                    /* if index matches with requested view then it
                       gets the parameter info otherwise no.
                    */
                    if (index == viewIndex || viewIndex == 0)
                    {
                        // gets parameter info for the requested block view
                        errorCode = findVariablesInView(&envInfo, memList,
                                                        paramInfo);
                        if (errorCode != SUCCESS)
                        {
                            errorCode = FAILURE;
                        }
                        break;
                    }
                }

                if (errorCode != SUCCESS)
                {
                    status.setError(
                                FF_PC_ERR_SELECT_PARAM_ITEM_FAILED,
                                PM_ERR_CL_FF_CSL,
                                CSL_ERR_SC_PC);
                }
                else
                {
                    // Clears the genericItem memory.
                    m_pDDS->ddiCleanItem(&genericItem);
                }
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    gets parameter info for the requested block view
*/
int FFParameterCache::findVariablesInView(ENV_INFO* envInfo, MEMBER* member,
    ParamInfo* paramInfo)

{
    APP_INFO            appInfo{};
    DDI_BLOCK_SPECIFIER blockSpec{};
    int                 errorCode = SUCCESS;
    DDI_ITEM_SPECIFIER  itemSpec{};
    DDI_GENERIC_ITEM    genericItem{};
    PMStatus            status;

    blockSpec.type = DDI_BLOCK_HANDLE;
    blockSpec.block.handle = envInfo->block_handle;
    itemSpec.type = DDI_ITEM_ID;
    itemSpec.item.id = member->ref.id;

    envInfo->app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;
    genericItem = {};

    // Gets the requested item based on mask.
    status = m_pDDS->ddiGetItem(&blockSpec, &itemSpec, envInfo, ALL_ITEM_ATTR_MASK,
                                &genericItem);

    if (!status.hasError())
    {
        // Gets item info for requested view.
        errorCode = lookUpVariableList(envInfo, &genericItem, paramInfo);
    }

    // Clears the genericItem memory.
    m_pDDS->ddiCleanItem(&genericItem);

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    gives item info based on view requested
*/
int FFParameterCache::lookUpVariableList(ENV_INFO* envInfo,
    DDI_GENERIC_ITEM* genericItem, ParamInfo* paramInfo)
{
    int             errorCode = SUCCESS;
    FLAT_VAR_LIST*  flatVarList = nullptr;
    MaskType        mask;
    MEMBER*         tempMember = nullptr;

    flatVarList = static_cast<FLAT_VAR_LIST*>(genericItem->item);

    //Flat var list assigned to local member list
    tempMember = flatVarList->members.list;

    std::vector<std::reference_wrapper<ItemInfoType>> members
            = paramInfo->paramList;

    mask = paramInfo->requestInfo.maskType;
    errorCode = getParamItem(tempMember, flatVarList->members.count, envInfo,
                             mask, members);

    if (SUCCESS == errorCode)
    {
        paramInfo->paramList = members;
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    clean the paramInfo members
*/
void FFParameterCache::cleanParamInfo(ParamInfo *paramInfo)
{
    for(auto item : paramInfo->paramList)
    {
        auto itemInfo = &item.get();
        switch(itemInfo->itemType)
        {
        case PARAM_VARIABLE:
        {
            auto varType = dynamic_cast<VariableType*>(itemInfo);

            //cleans string values
            cleanString(varType);

            if(nullptr != varType && true == varType->isMemAllocated)
            {
                delete varType;
                varType = nullptr;
            }
            break;
        }
        case PARAM_ARRAY:
        {
            auto arrType = dynamic_cast<ArrayType*>(itemInfo);
            if(nullptr != arrType)
            {
                for(auto item : arrType->members)
                {
                    auto varType = dynamic_cast<VariableType*>(&item.get());

                    //cleans string values
                    cleanString(varType);

                    if(nullptr != varType && true == varType->isMemAllocated)
                    {
                        delete varType;
                        varType = nullptr;
                    }
                }

                //cleans string values for array
                releaseStringValue(arrType->help);
                releaseStringValue(arrType->label);

                if(arrType->isMemAllocated == true)
                {
                    delete arrType;
                    arrType = nullptr;
                }
            }
            break;
        }
        case PARAM_RECORD:
        {
            auto recType = dynamic_cast<RecordType*>(itemInfo);
            if(nullptr != recType)
            {
                for(auto item : recType->members)
                {
                    auto varType = dynamic_cast<VariableType*>(&item.get());

                    //cleans string values
                    cleanString(varType);

                    if(nullptr != varType && true == varType->isMemAllocated)
                    {
                        delete varType;
                        varType = nullptr;
                    }
                }

                //cleans string values for record
                releaseStringValue(recType->help);
                releaseStringValue(recType->label);

                if(recType->isMemAllocated == true)
                {
                    delete recType;
                    recType = nullptr;
                }
            }
            break;
        }
        default:
            LOGF_INFO(CPMAPP, "cannot delete memory for invalid type");
            break;
        }
    }

    // clean the blocktag
    paramInfo->blockTag.str = nullptr;

    // clean the member list
    paramInfo->paramList.clear();

    // clean the parameter info
   // paramInfo = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function is used to release memory allocated to string type
*/
void FFParameterCache::cleanString(VariableType* varType)
{
    //cleans the memory allocated to string types
    releaseStringValue(varType->label);
    releaseStringValue(varType->help);
    releaseStringValue(varType->editFormat);
    releaseStringValue(varType->displayFormat);
    releaseStringValue(varType->unit);
    releaseStringValue(varType->value.strData);

    //releases the memory allocated to enum/bitenum or index type members
    if (DDS_ENUMERATED == varType->type ||
            DDS_BIT_ENUMERATED == varType->type)
    {
        for(auto item : varType->enums)
        {
            releaseStringValue(item.label);
            releaseStringValue(item.help);
        }
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to release memory allocated to string type
*/
void FFParameterCache::releaseStringValue(StringType& strType)
{
    //deallocates memory allocated to string types
    if(strType.flag == FREE_STRING &&
            nullptr != strType.str)
    {
        delete[] strType.str;
        strType.str = nullptr;
        strType.len = 0;
        strType.flag = DONT_FREE_STRING;
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to get list of parameters from Block Characteristics.

    Responsibility to free ParamInfo memory is the caller of this function
*/
PMStatus FFParameterCache::getBlockCharacteristics(ParamInfo* paramInfo,
                                                   int reqSubIndex)
{
    APP_INFO            appInfo{};
    DDI_BLOCK_SPECIFIER blockSpec{};
    int                 errorCode = SUCCESS;
    ENV_INFO            envInfo{};
    FLAT_BLOCK*         flatBlock = nullptr;
    DDI_ITEM_SPECIFIER  itemSpec{};
    DDI_GENERIC_ITEM    genericItem{};
    MaskType            attrMask;
    PMStatus            status;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    // Do not proceed further if paramInfo is null.
    if (nullptr == paramInfo)
    {
        status.setError(
                    FF_PC_ERR_GET_PARAMETER_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_PC);
    }
    else
    {
        // This API gives block handle
        envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                               paramInfo->blockTag.str);

        // validates block handle
        if (valid_block_handle(envInfo.block_handle) == 0)
        {
            status.setError(
                        FF_PC_ERR_GET_ENVIRON_INFO_FAILED,
                        PM_ERR_CL_FF_CSL,
                        CSL_ERR_SC_PC);
        }
        else
        {
            blockSpec.type = DDI_BLOCK_HANDLE;
            blockSpec.block.handle = envInfo.block_handle;
            genericItem = {};

            // Gets the requested item.
            status = m_pDDS->ddiGetItem(
                        &blockSpec, &itemSpec, &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);

            if (!status.hasError())
            {
                // Typecasted generic item to flat block.
                flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
                if (nullptr == flatBlock)
                {
                    LOGF_FATAL(CPMAPP,
                              "null Flat Block never expected upon " <<
                              "successfull ddi_get_item call"
                              );
                }
                DDI_ITEM_SPECIFIER charItemSpec;
                charItemSpec.item.id = flatBlock->characteristic;
                charItemSpec.type = DDI_ITEM_ID;
                DDI_GENERIC_ITEM subGenericItem{};

                // Gets the requested item.
                status = m_pDDS->ddiGetItem(
                            &blockSpec, &charItemSpec, &envInfo, ALL_ITEM_ATTR_MASK, &subGenericItem);

                if (!status.hasError())
                {
                    FLAT_RECORD* flatRecord = static_cast<FLAT_RECORD*>(subGenericItem.item);

                    std::vector<std::reference_wrapper<ItemInfoType>> members =
                            paramInfo->paramList;

                    attrMask = paramInfo->requestInfo.maskType;
                    errorCode = getParamItem(
                                flatRecord->members.list,
                                flatRecord->members.count,
                                &envInfo,
                                attrMask,
                                members,
                                flatRecord->id,
                                reqSubIndex
                                );

                    if (errorCode != SUCCESS || members.empty())
                    {
                        status.setError(
                                    FF_PC_ERR_GET_PARAM_ITEM_FAILED,
                                    PM_ERR_CL_FF_CSL,
                                    CSL_ERR_SC_PC);
                    }
                    else
                    {
                        paramInfo->paramList = members;
                    }
                }
                m_pDDS->ddiCleanItem(&subGenericItem);
            }
            m_pDDS->ddiCleanItem(&genericItem);
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get Block Characteristics ITEM ID.

*/
PMStatus FFParameterCache::getBlockCharacteristicsID(char* blockTag,
                                                   ITEM_ID& itemID)
{
    APP_INFO            appInfo{};
    DDI_BLOCK_SPECIFIER blockSpec{};
    ENV_INFO            envInfo{};
    FLAT_BLOCK*         flatBlock = nullptr;
    DDI_ITEM_SPECIFIER  itemSpec{};
    DDI_GENERIC_ITEM    genericItem{};
    PMStatus            status;

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    // Do not proceed further if paramInfo is null.

    // This API gives block handle
    envInfo.block_handle = m_pDDS->getBlockHandleByBlockTag(
                blockTag);

    // validates block handle

    blockSpec.type = DDI_BLOCK_HANDLE;
    blockSpec.block.handle = envInfo.block_handle;
    genericItem = {};

    // Gets the requested item.
    status = m_pDDS->ddiGetItem(
                &blockSpec, &itemSpec, &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);

    if (!status.hasError())
    {
        // Typecasted generic item to flat block.
        flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
        if (nullptr == flatBlock)
        {
            LOGF_FATAL(CPMAPP,
                      "null Flat Block never expected upon " <<
                      "successfull ddi_get_item call"
                      );
        }

        itemID = flatBlock->characteristic;

    }
    m_pDDS->ddiCleanItem(&genericItem);

    return status;
}

/*  ----------------------------------------------------------------------------
    Set the default reason code to the paraminfo members
*/
void FFParameterCache::setDefaultReasonCode(std::vector<std::reference_wrapper<ItemInfoType>> members)
{
    for(auto &iteminfo : members)
    {
       iteminfo.get().response_code = DDI_INVALID_PARAM;
    }
}

/*  ----------------------------------------------------------------------------
    Validates the date and time values
*/
bool FFParameterCache::validateDateNTime(int* dateArray, int length)
{
    if (dateArray[DATE]  < DATENTIME_DEFAULT ||
        dateArray[DATE]  > MAX_VAL_OF_DAY    ||
        dateArray[MONTH] < DATENTIME_DEFAULT ||
        dateArray[MONTH] > MAX_VAL_OF_MONTH  ||
        dateArray[YEAR]  < MIN_VAL_OF_YEAR)
    {
        return false;
    }
    if (length > DATE_ARRAY_LENGTH)
    {
        if (dateArray[HOUR] < DATENTIME_NOT_INIT ||
            dateArray[HOUR] > MAX_VAL_OF_HOUR ||
            dateArray[MINUTE] < DATENTIME_NOT_INIT ||
            dateArray[MINUTE] > MAX_VAL_OF_MIN ||
            dateArray[SECOND] < DATENTIME_NOT_INIT ||
            dateArray[SECOND] > MAX_VAL_OF_MIN)
        {
            return false;
        }
    }
    return true;
}

/*  ----------------------------------------------------------------------------
    Reversing the bit string data
*/
void FFParameterCache::bitReverse(char *val, int size)
{
    int index = 0;
    long tempValue = 0x00, temp = 0;
    (void *)memcpy((long *)&tempValue, (char *)val, size);

    /*
     * Loop through number of bits in val and reverse the the bit
     * Swapping first to last bit through the count
     */
    for(index = 0; index < (size * BIT_REV_ALLOC_SIZE); index++)
    {
        long temp1 = 0;
        temp1 = (0x1) & tempValue >>  index;
        temp |= temp1 << (((size * BIT_REV_ALLOC_SIZE) -1) - index);
    }
    (void *)memcpy((char *)val, (long *)&temp, size);
}

/*  ----------------------------------------------------------------------------
    Assigns the Hex value to the Bit String
*/
void FFParameterCache::assignBitStringValue(STRING input, STRING& output)
{
    // Converting the Hex value to plain string
    char* str = new char[(input.len * BIT_STRING_OFFSET) + 1];
    for (int index = 0; index < input.len; ++index)
    {
        char* temp = new char[CHAR_ARRAY_LENGTH + 1];
        sprintf(temp, "%02X", input.str[index]);
        strncpy(str + (BIT_STRING_OFFSET * index),
                temp + (strlen(temp) - BIT_STRING_OFFSET),
                BIT_STRING_OFFSET);
        if (nullptr != temp)
        {
            delete[] temp;
            temp = nullptr;
        }
    }
    str[input.len * BIT_STRING_OFFSET] = '\0';

    // Swapping first to last bit through the count
    int ssize = strlen(str);
    char* tempStr = new char[ssize + 1];
    strncpy(tempStr, str, ssize);
    if (ssize > BIT_STRING_OFFSET)
    {
        int lsb = 0;
        int msb = ssize;
        for (int i = 0; i < input.len; i++)
        {
            strncpy(tempStr + lsb, str + (msb - BIT_STRING_OFFSET),
                   BIT_STRING_OFFSET);
            lsb += BIT_STRING_OFFSET;
            msb -= BIT_STRING_OFFSET;
        }
    }
    tempStr[input.len * BIT_STRING_OFFSET] = '\0';

    // Copy the final string to output
    int strLen = strlen(tempStr);
    output.str = new char[strLen + 1];
    strncpy(output.str, tempStr, strLen);
    output.str[strLen] = '\0';
    output.len = strLen;
    output.flags = FREE_STRING;

    // Deallocate the memory
    if (nullptr != str)
    {
        delete[] str;
        str = nullptr;
    }
    if (nullptr != tempStr)
    {
        delete[] tempStr;
        tempStr = nullptr;
    }
}
