/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Sethu
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Subscription class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include "Subscription.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include <iostream>
#include <sys/time.h>        // For gettimeofday()
#include "flk_log4cplus_defs.h"
using namespace std;

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    IDGenerator class instance initialization
*/
Subscription::Subscription(uint subscriptionID,
                           SubscriptionType subscriptionType,
                           ParamInfo& monitorItems,
                           uint refreshRate,
                           bool deviceRead,                          
                           DataChangedCallback datachangecallback,
                           SubscriptionTimerCallback notifier)
    :m_subscriptionID(subscriptionID),
     m_subscriptionType(subscriptionType),
     m_updateRate(refreshRate),
     m_bDeviceRead(deviceRead),
     m_subThreadID(0)
{
    m_monitoredItem = monitorItems.clone();
    m_dataChangeCallback = std::move(datachangecallback);
    m_notifer = std::move(notifier);
    m_isSubscripitonProcessed = false;
}

/*  ----------------------------------------------------------------------------
    Start the subscription thread. Once refreshtime elapsed Parameters shall
    be read and send result over callback
*/
bool Subscription::startSubscription()
{
    if (THREAD_STATE_INVALID == getThreadState())
    {
        m_subThreadIdLock.lock();
        std::function<void (const void *)> subFuncPtr =
                std::bind(&Subscription::process, this, this);

        m_subThreadID =
                ThreadManager::getInstance()->createThread(subFuncPtr, this);
        m_subThreadIdLock.unlock();

        return true;
    }

    return false;
}

/*  ----------------------------------------------------------------------------
    Stop the subscription thread
*/
bool Subscription::stopSubscription()
{
    if (THREAD_STATE_RUN == getThreadState())
    {
        setThreadState(THREAD_STATE_STOP);

        // Waits until the subscription thread process completes
        acquireLock();
        m_isSubscripitonProcessed = false;

        return true;
    }

    return false;
}


/*  ----------------------------------------------------------------------------
    It acquires the thread lock and wait till release
*/
void Subscription::acquireLock()
{
    // Waits until get the input from user
    std::unique_lock<std::mutex> subLock(m_subProcessLock);
    m_subscriptionCV.wait(subLock, [this]
    {
        return m_isSubscripitonProcessed;
    });
}

/*  ----------------------------------------------------------------------------
    It releases the locked thread and notify for further process
*/
void Subscription::releaseLock()
{
    // Unlocks the thread and notify that user's input arrived
    std::unique_lock<std::mutex> subLock(m_subProcessLock);
    subLock.unlock();
    m_subscriptionCV.notify_one();
}

/*  ----------------------------------------------------------------------------
    Entry point for the subscription thread. Notify subscription timeout with
    respect to refresh rate
*/
void Subscription::process(void* arg)
{
    Subscription* subscription = static_cast<Subscription*>(arg);
    struct timeval start,end;
    int seconds = 0;

    //save the start time
    gettimeofday(&start, NULL);

    while(THREAD_STATE_RUN == subscription->getThreadState())
    {
        // Only report with resolution of seconds.
        gettimeofday(&end, NULL);
        seconds = end.tv_sec - start.tv_sec;

        // if the refresh interval is elapsed, notify subscription timeout
        if (seconds >= subscription->getRefreshRate())
        {
            subscription->notifySubscriptionTimeout();
            //reset the start time
            gettimeofday(&start, NULL);
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(
                                            10));
        }
    }

    // Remove the thread id from the thread table maintained by thread manager
    ThreadManager::getInstance()->destroyThread(m_subThreadID);

    // Reset the subscription thread id
    m_subThreadID = 0;

    subscription->m_isSubscripitonProcessed = true;
    // Unlocks the waiting thread and notify that data has arrived
    subscription->releaseLock();
}

/*  ----------------------------------------------------------------------------
    Sets the thread state
*/
void Subscription::setThreadState(THREAD_STATE threadState)
{
    m_subThreadIdLock.lock();
    ThreadManager::getInstance()->setThreadState(m_subThreadID, threadState);
    m_subThreadIdLock.unlock();
}

/*  ----------------------------------------------------------------------------
    Retrieves the subscription thread state
*/
THREAD_STATE Subscription::getThreadState()
{
    THREAD_STATE threadState = THREAD_STATE_INVALID;
    m_subThreadIdLock.lock();
    // Return the current thread state
    threadState = ThreadManager::getInstance()->getThreadState(m_subThreadID);

    m_subThreadIdLock.unlock();
    return threadState;
}

/*  ----------------------------------------------------------------------------
    Notify to the subscription about the timeout to call Subscription Timeout
    to upper caller
*/
void Subscription::notifySubscriptionTimeout()
{
    if (THREAD_STATE_RUN == getThreadState())
    {
        (m_notifer)(this, m_bDeviceRead, m_dataChangeCallback);
    }
}

/*  ----------------------------------------------------------------------------
    Deletes and deallocatest the memory allocated for subscription members
*/
PMStatus Subscription::cleanSubscriptionMembers()
{
    if (m_monitoredItem.paramList.size() > 0)
    {
        for (auto iteminfo : m_monitoredItem.paramList)
        {
            auto itemInfo = &iteminfo.get();
            auto varParam = dynamic_cast<VariableType*>(itemInfo);
            if (nullptr == varParam)
            {
                LOGF_ERROR(CPMAPP, "item is already null no need to delete");
                continue;
            }

            //deallocates memory allocated by PC
            if(varParam->value.strData.flag == FREE_STRING)
            {
                delete[] varParam->value.strData.str;
                varParam->value.strData.str = nullptr;
                varParam->value.strData.len = 0;
                varParam->value.strData.flag = DONT_FREE_STRING;
            }

            delete varParam;
            varParam = nullptr;
        }
        m_monitoredItem.paramList.clear();
        m_monitoredItem = {};
    }
}
