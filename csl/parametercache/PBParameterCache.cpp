/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PBParameterCache class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include <math.h>
#include "PBParameterCache.h"
#include "PBAdaptor.h"
#include "IParameterCache.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

#define EMPTY_HANDLE (-1) //It is -1, Since first item  start at index 0
#define VIEW_LIMIT   (5)

#define DICT_STR_HACK 1

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    PBParameterCache class instance initialization
*/
PBParameterCache::PBParameterCache() : m_pIFakController(nullptr),
                                       m_pCM(nullptr),
                                       m_pPBAdaptor(nullptr)
{

}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
PBParameterCache::~PBParameterCache()
{
    // Clear the member variables
    m_pIFakController = nullptr;
    m_pCM = nullptr;
    m_pPBAdaptor = nullptr;
}

/*  ----------------------------------------------------------------------------
    This function initialize cache tables and scheduler
*/
PMStatus PBParameterCache::initialize(IFakController* ifak, IConnectionManager* cm, PBAdaptor *adaptor)
{
    PMStatus status;

    if (nullptr != ifak && nullptr != cm)
    {
        m_pIFakController = ifak;
        m_pCM = cm;
        m_pPBAdaptor = adaptor;
    }
    else
    {
        status.setError(
            PB_PC_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_PC);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to get list of parameters.
    It is uplink responsibility to free ParamInfo memory
*/
PMStatus PBParameterCache::getParameter(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the cache. if it is not available
    in the cache it reads from device and update cache as well
*/
PMStatus PBParameterCache::readValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value into the device and cache tables
*/
PMStatus PBParameterCache::writeValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read value from the device and stores it into
    the cache table
*/
PMStatus PBParameterCache::readDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write value to the device and on success it
    stores value in cache table
*/
PMStatus PBParameterCache::writeDeviceValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to read edit value from the cache table
*/
PMStatus PBParameterCache::readEditValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to write edit value to the cache table
*/
PMStatus PBParameterCache::writeEditValue(ParamInfo* paramInfo)
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    Performs pre subscription actions upon subscription timer elapsed
*/
PMStatus PBParameterCache::preSubscriptionActions(ParamInfo& paramInfo,
                                                  bool readFromDevice)
{
    PMStatus status;

    (void)status;

    return status;
}

/*  ----------------------------------------------------------------------------
    Performs subscription actions upon subscription timer elapsed
*/
PMStatus PBParameterCache::doSubscriptionActions(ParamInfo& paramInfo,
                                                         bool readFromDevice)
{
    PMStatus status;

    return status;
}
