/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Implements interfaces that communicates with HART device using
    HARTCommunicationManager interfaces and uses SDC-625 Controller to access
    any DD information.
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/


/* INCLUDE FILES **************************************************************/

#include <string.h>
#include <sstream>
#include "HARTConnectionManager.h"
#include "IConnectionManager.h"
#include "SDC625Controller.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"


/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    HARTConnectionManager class instance initialization
*/
HARTConnectionManager::HARTConnectionManager()
    : m_pSDC625Controller(nullptr),
      m_pParameterCache(nullptr),
      m_pCommManager(nullptr),
      m_isConnected(false),
      m_isStopScan(false),
      m_maxPollAddress(0)
{
}

/*  ----------------------------------------------------------------------------
    Destructs a HARTConnectionManager
*/
HARTConnectionManager::~HARTConnectionManager()
{
    m_pSDC625Controller = nullptr;
    m_pParameterCache = nullptr;
    m_isConnected = false;

    if (nullptr != m_pCommManager)
    {
        delete m_pCommManager;
        m_pCommManager = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Initialize the HARTConnectionManager with SDC-625 & PC instance
*/
PMStatus HARTConnectionManager::initialize(
    SDC625Controller* sdc, IParameterCache* pc)
{
    PMStatus status;

    if (nullptr != sdc && nullptr != pc)
    {
        m_pSDC625Controller = sdc;
        m_pParameterCache = pc;
    }
    else
    {
        LOGF_ERROR(CPMAPP,"HART Connection Manager initialisation failed!!\n");
        status.setError(
            HART_CM_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_HART_CSL,
                    CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Initializes HART communication using HARTCommunicationManager interfaces
*/
PMStatus HARTConnectionManager::initStack(void* busParams, const char* ddPath,
                                          const char* deviceFile)
{
    (void)ddPath;
    (void)deviceFile;
    (void)busParams;
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    Initializes HART communication using HARTCommunicationManager interfaces
*/
PMStatus HARTConnectionManager::initStack(void* busParams, void *appSettings,
                                const char * ddPath, const char* deviceFile,
                                const char *hartModem)
{
    (void)ddPath;
    PMStatus status;
    int result = FAILURE;
    HART_BUS_PARAMS* hartBusParams = nullptr;
    HART_APP_SETTINGS* hartAppSettings = nullptr;

    // Initialize HART Communication if it is not already initialized
    if (nullptr == m_pCommManager)
    {
        // Validate busparams
        if (nullptr != busParams && nullptr != appSettings)
        {
            hartBusParams = static_cast<HART_BUS_PARAMS*>(busParams);
            hartAppSettings = static_cast<HART_APP_SETTINGS*>(appSettings);

            if (nullptr != hartAppSettings)
            {
                //sets the maximum polling address for scanning
                status = setMaxPollAddress(hartAppSettings->pollAddrOption);
            }
        }

        if (!status.hasError())
        {
            // Instantiates HARTCommunicationManager
            m_pCommManager = new HARTCommunicationManager();

            if (nullptr != m_pCommManager && nullptr != hartBusParams)
            {
                // Initializes HART communication
                result = m_pCommManager->initComm(deviceFile, hartModem);

                if (SUCCESS == result)
                {
                    BYTE masterType = static_cast<BYTE>
                            (hartBusParams->masterType);
                    result = m_pCommManager->setMasterType(masterType);
                    result = m_pCommManager->setPreamble(hartBusParams->preamble);
                }
            }

            if (SUCCESS != result)
            {
                LOGF_ERROR(CPMAPP,"HART Stack initialised failed!!\n");
                status.setError(
                    HART_CM_ERR_INIT_STACK_FAILED,
                    PM_ERR_CL_HART_CSL,
                    CSL_ERR_SC_CM);
            }
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP,"HART Stack already initialised!\n");
        status.setError(
            HART_CM_ERR_STACK_ALREADY_INITIALIZED,
            PM_ERR_CL_HART_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

PMStatus HARTConnectionManager::setMaxPollAddress(PollByAddress pollOption)
{
   PMStatus status;
   int maxPollAddress = 0;
   status = getMaxPollAddress(pollOption, maxPollAddress);

   if(!status.hasError())
   {
       m_maxPollAddress = maxPollAddress;
   }

   return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves the maximum poll address for the option selected
*/
PMStatus HARTConnectionManager::getMaxPollAddress(
    PollByAddress addressOption, int& maxPollAddress)
{
    PMStatus status;

    if (addressOption == PollByAddress::ADDRESS_0)
    {
        maxPollAddress = MAX_POLL_ADDRESS_0;
    }
    else if (addressOption == PollByAddress::ADDRESS_0_15)
    {
        maxPollAddress = MAX_POLL_ADDRESS_15;
    }
    else if (addressOption == PollByAddress::ADDRESS_0_63)
    {
        maxPollAddress = MAX_POLL_ADDRESS_63;
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Invalid poll Settings Option\n");
        status.setError(
            HART_CM_ERR_INVALID_POLL_BY_ADDRESS_OPTION,
            PM_ERR_CL_HART_CSL,
                    CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Scans the available devices in the HART network and notifies device
    info to upper layer through callback
*/
PMStatus HARTConnectionManager::scanDevices
                        (std::function<void(bool, Indentity_t)> callBack)
{
    PMStatus status;
    BYTE pollAddr;
    Indentity_t retIdentity;
    bool deviceFound = false;

    retIdentity.clear();
    if (nullptr != m_pCommManager)
    {
        // Scans the available devices and fills the scan results table
        for (pollAddr = 0; pollAddr <= m_maxPollAddress; pollAddr++)
        {
            /* break the loop if user performs
                  stops scan process
               */
            if(m_isStopScan)
            {
                m_isStopScan = false;
                status.setError(
                            HART_CM_SCAN_INTERRUPTED,
                            PM_ERR_CL_HART_CSL,
                            CSL_ERR_SC_CM);

                return status;
            }

            retIdentity.clear();

            if (SUCCESS == m_pCommManager->GetIdentity(retIdentity, pollAddr))
            {              
				// Read device tag
                if (SUCCESS == m_pCommManager->GetDeviceTag(retIdentity))
                {
                    deviceFound = true;

                    if(!m_isStopScan)
                    {
                        // once device is found, notifies device info to upper layer
                        callBack(deviceFound, retIdentity);
                    }
                }
            }
        }

        // notifies even device not found
        if (!deviceFound && nullptr != callBack)
        {
            callBack(deviceFound, retIdentity);
        }
    }
    else
    {
        status.setError(
                    HART_CM_SCAN_INTERRUPTED,
                    PM_ERR_CL_HART_CSL,
                    CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Sets the stop scan flag and stops the device scanning process
*/
void HARTConnectionManager::stopScan()
{
    m_isStopScan = true;
}

/*  ----------------------------------------------------------------------------
    Scans the available devices in the HART network.This API is deprecated,
    instead using scanDevices(std::function<void(Indentity_t)> callBack)
*/
PMStatus HARTConnectionManager::scanDevices(
        void* envInfo, void* scanDevTable)
{
    PMStatus status;
    (void)envInfo;
    (void)scanDevTable;
    return status;
}

/*  ----------------------------------------------------------------------------
    Establishes field device connection and fills the device information
    for the given slave address
*/
PMStatus HARTConnectionManager::connectDevice(DeviceInfo* deviceInfo)
{
    LOGF_DEBUG(CPMAPP, "Connect Device for Poll Address - " << deviceInfo->node_address);
    PMStatus status;

    // Check if device is already connected
    if (!m_isConnected)
    {
        int result = FAILURE;
        Indentity_t identity;

        if (nullptr != m_pCommManager && nullptr != deviceInfo)
        {
            // Performs connection and gets device identity information
            result =
                m_pCommManager->GetIdentity(identity,
                                            deviceInfo->node_address);
        }
        else
        {
            result = FAILURE;
        }

        if (SUCCESS == result)
        {
            m_pCommManager->thePktQueue.initialize();
            deviceInfo->device_type = identity.wDeviceType;
            deviceInfo->device_rev = identity.cDeviceRev;
            // Conversion from DWORD to char[]
            std::stringstream stream;
            stream << identity.dwDeviceId;
            std::string str = stream.str();
            strncpy(deviceInfo->dev_id, str.c_str(), str.length());
            deviceInfo->manufacturer = identity.wManufacturer;
            strncpy(deviceInfo->pd_tag, identity.sTag.c_str(), str.length());

            deviceInfo->hartRevision = identity.cUniversalRev;

            // Set true if device is successfully connected
            m_isConnected = true;

            //Create SDC device instance
            status = m_pSDC625Controller->createDevice(&identity,
                                                       m_pCommManager);
        }
        else
        {
            LOGF_ERROR(CPMAPP,"HART Device Connection failed!!\n");
            // Set error if connection was not successful
            status.setError(
                HART_CM_ERR_CONNECT_DEVICE_FAILED,
                PM_ERR_CL_HART_CSL,
                CSL_ERR_SC_CM);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP,"HART Device already connected!!\n");
        // Set error if device is already connected
        status.setError(
            HART_CM_ERR_DEVICE_ALREADY_CONNECTED,
            PM_ERR_CL_HART_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves the device information for the given slave address
*/
PMStatus HARTConnectionManager::getDeviceInfo(
    int slaveAddress, DeviceInfo* deviceInfo)
{
    PMStatus status;

    /* Typecasted with void to supress the unused parameter warning, must
       remove when implementation takes place
    */
    (void) slaveAddress;
    (void) deviceInfo;

    return status;
}

/*  ----------------------------------------------------------------------------
    Disconnects the currently connected device
*/
PMStatus HARTConnectionManager::disconnectDevice()
{
    PMStatus status;   

    if (nullptr != m_pSDC625Controller)
    {
        status = m_pSDC625Controller->deleteDevice();
    }

    // Set false when device is disconnected
    m_isConnected = false;

    return status;
}

/*  ----------------------------------------------------------------------------
    Closes the HART communication
*/
PMStatus HARTConnectionManager::shutDownStack()
{
    PMStatus status;

    if (nullptr != m_pCommManager)
    {
       m_pCommManager->setShutDownFlag(true);
       m_pCommManager->shutdownComm();
    }

    return status;
}
