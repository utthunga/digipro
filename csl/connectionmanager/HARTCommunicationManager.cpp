/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    This implements hCommInf(included in SDC-625 code base) interfaces using
    HART stack APIs
    that communicates with phsyical HART devices.

    This class is designed to use by both HARTConnectionManager class &
    SDC-625 core classes to interact with physical HART devices.

    HARTConnectionManager class instantiates this class and the same object
    reference will be used by SDC 625 core classes.
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>

#include "HARTCommunicationManager.h"
#include "microctl.h"    //To call mc_initialization function
#include "intr6811.h"    //for enable_interrupt()
#include "io6811.h"      //for PORTA
#include "mdlldemo.h"    //for find_transmitter()
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

//#define LOG_COMM_TO_FILE

#define MAX_RETRY           3
#define HART6_REV           6
#define HART7_REV           7
#define BYTE_VAL            8
#define HART5_MAX_POLL_ADDR 15
#define CMD_38              38
#define MAX_STRING_LENGTH   255

// Constructor
HARTCommunicationManager::HARTCommunicationManager() : thePktQueue()
{
    m_bShutdownStack = false;
}

// Destructor
HARTCommunicationManager::~HARTCommunicationManager()
{
    thePktQueue.destroy();
}

/*  ----------------------------------------------------------------------------
    Initializes HART communication using HART Stack APIs

    This API is deprecated instead of this using
            initComm(const char* deviceFile, const char* hartModem) API
*/
RETURNCODE HARTCommunicationManager::initComm()
{
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Initializes HART communication using HART Stack APIs
*/
RETURNCODE HARTCommunicationManager::initComm(const char* deviceFile,
                                              const char* hartModem)
{
    ghc.deviceFile = deviceFile;
    ghc.hartModem = hartModem;
    setShutDownFlag(false);

    if (STATUS_OK == mc_initialization())
    {
        // Initialises HART communication
        hll_initialize_hart();
        enable_interrupt();
	// Retry (for HART stack)  has been commented out as this makes all request 
	// to be sent for 4 times which further delays scanning mechanism
	// Retry for read / write request has been handled in applicaiton layer
        // hll_set_retries(MAX_RETRY);
        hll_clear_statistics();
        
        // Setting extended timeout as some HART slave devices 
        // are responsing slowly
        hll_extend_timeout(MAX_EXTEND_TIMEOUT);
    }
    else
    {
        return FAILURE;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    This function invokes find_transmitter() of HART Stack and
    fils the Indentity_t appropriately
*/
RETURNCODE HARTCommunicationManager::GetIdentity(Indentity_t& retIdentity,
                                                 BYTE pollAddr)
{
     // Find transmitter for the requested polling address
    if (STATUS_OK == find_transmitter(pollAddr))
    {
        LOGF_DEBUG(CPMAPP, "Device Available at Poll Address - " << pollAddr);
        retIdentity.PollAddr = vDeviceData.cPollAddr;
        retIdentity.wManufacturer = vDeviceData.cManufacturer;

        // Set extended device type code HART 7 and higher revision
        if (vDeviceData.cUniversalRev >= HART7_REV)
        {
            retIdentity.wDeviceType =
                    vDeviceData.cManufacturer << BYTE_VAL | vDeviceData.cDevice;
        }
        else
        {
            retIdentity.wDeviceType = vDeviceData.cDevice;
        }

        if (pollAddr > HART5_MAX_POLL_ADDR && vDeviceData.cUniversalRev < HART6_REV)
        {
            return FAILURE;
        }

        retIdentity.cUniversalRev = vDeviceData.cUniversalRev;
        retIdentity.cDeviceRev = vDeviceData.cSpecificRev;
        retIdentity.cSoftwareRev = vDeviceData.cSoftwareRev;
        retIdentity.cHardwareRev = vDeviceData.cHardwareRev;
        retIdentity.dwDeviceId = vDeviceData.dwDeviceId;

    }
    else
    {
        LOGF_DEBUG(CPMAPP, "Device not found at Poll Address - " << pollAddr);
        return FAILURE;
    }
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    This function retrieves short tag for the transmitter and fills the
    Indentity_t information
*/
RETURNCODE HARTCommunicationManager::GetDeviceTag(Indentity_t& retIdentity)
{
    RETURNCODE retCode = SUCCESS;

    // Read Transmitter short tag and fill the Indentity_t information
    if (STATUS_OK == ReadCommand(CMD_READ_TAG_DESC_DATE))
    {
        retIdentity.sTag = vDeviceData.szTag;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Read Commad Failed - Failed to read Short Tag..");
        retCode = FAILURE;
    }

    return retCode;
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
bool HARTCommunicationManager::appCommEnabled()
{
    // does nothing for now
    return true;
}

/*  ----------------------------------------------------------------------------
    Gets an empty packet for generating a command
    (gives caller ownership of the packet*)
    pass back ownership with Sendxxx OR putMTPacket if command send aborted
*/
hPkt* HARTCommunicationManager::getMTPacket(bool pendOnMT)
{
    hPkt* pR = thePktQueue.GetEmptyObject(pendOnMT);
    return pR;
}

/*  ----------------------------------------------------------------------------
    Returns the packet memory back to the interface
    (caller relinguishes ownership of the packet*)
*/
void  HARTCommunicationManager::putMTPacket(hPkt* pPkt)
{
    thePktQueue.ReturnMTObject( pPkt );
}

/*  ----------------------------------------------------------------------------
    Returns the packet memory back to the interface
    (caller relinguishes ownership of the packet*)
*/
RETURNCODE HARTCommunicationManager::SendPkt(hPkt* pPkt,
                                             int timeout,
                                             cmdInfo_t& cmdStatus)
{
    RETURNCODE rc = FAILURE;
    if ( pPkt != nullptr)
    {
        cmdStatus.transactionID = pPkt->transactionID;
        rc = SendCmd(pPkt->cmdNum,pPkt->theData,pPkt->dataCnt,
                     timeout,cmdStatus);
    }
    putMTPacket(pPkt);
    return rc;
}

/*  ----------------------------------------------------------------------------
    This method invokes send_receive() method of HART Stack.
*/
RETURNCODE HARTCommunicationManager::SendCmd(int cmdNum, BYTE* pData,
                     BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
    int identity[MAX_STRING_LENGTH] = {0};
    RETURNCODE rc = SUCCESS;
    hPkt* pPkt = nullptr;
    int status = STATUS_OK;
    int retryCount = 0;

    if (cmdNum < 0 || pData == nullptr )
    {
        rc = APP_PARAMETER_ERR;
    }
    else
    {
        pPkt = getMTPacket();
        if ( pPkt == nullptr )
        {
            return APP_MEMORY_ERROR;
        }
        // else continue working

        // convert back to packet
        pPkt->cmdNum = cmdNum;
        pPkt->dataCnt= dataCnt;
        memcpy(pPkt->theData,pData,dataCnt);

        static DEVDATA* vDeviceData2 = nullptr;
        if (vDeviceData2 == nullptr)
        {
            vDeviceData2 = read_data(0);
        }

        memset(vDeviceData2->acData, '0', MAX_HART_MSG_LEN);
        memcpy(vDeviceData2->acData, pPkt->theData, dataCnt);
        vDeviceData2->DataCount = dataCnt;

        do
        {
            status = send_receive(cmdNum);
            retryCount++;
        }
        while(STATUS_OK != status && retryCount <= MAX_RETRY);

        if (STATUS_OK == status && ghc2.rc.cRspCount != 0)
        {
            for(int i = 0; i < ghc2.rc.cRspCount; i++)
            {
                identity[i] = ghc2.rc.acResponse[i];
            }

            memset(&(pPkt->theData[0]), 0, MAX_STRING_LENGTH);
            unsigned int ByteCountPlaceHolder = 7;
            unsigned int startDataIndexOffset = 1;

            if (cmdNum == 0)
            {
                ByteCountPlaceHolder = 3;
            }

            pPkt->dataCnt = identity[ByteCountPlaceHolder];

            for (int i = 0; i < pPkt->dataCnt; i++)
            {
                pPkt->theData[i] = identity[ByteCountPlaceHolder+startDataIndexOffset + i];
            }
        }
        else
        {
            rc = FAILURE;
        }
    }

    if ( rc == SUCCESS) //write went OK
    {
        // parse the packet
        if (cpRcvService != nullptr)
        {
            if (pPkt->cmdNum == CMD_38)
            {
                pPkt->theData[1] &= 0xBF; // clr the cnfg chng bit
            }
            pPkt->transactionID = cmdStatus.transactionID;
            cpRcvService->serviceReceivePacket(pPkt,&cmdStatus);
        }
        else
        {
           rc = APP_PROGRAMMER_ERROR;
        }        
    }
    // else - write failed, no reason to try a read...

    if (pPkt != nullptr )
    {
        putMTPacket(pPkt);
    }
    
    return rc;
}

/*  ----------------------------------------------------------------------------
    This method sends pkt based on priority
*/
RETURNCODE HARTCommunicationManager::SendPriorityPkt(hPkt* pPkt, int timeout,
                                                     cmdInfo_t& cmdStatus)
{
    return SendPkt(pPkt, timeout, cmdStatus);
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
void HARTCommunicationManager::enableAppCommRequests()
{
    // does nothing for now
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
void HARTCommunicationManager::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
    //Typecasted with void to supress the unused parameter warning
   (void)thisMsgCycle;
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
short HARTCommunicationManager::setNumberOfRetries(short newVal)
{
    //Typecasted with void to supress the unused parameter warning
   (void)newVal;
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
RETURNCODE HARTCommunicationManager::pipe2pkt(BYTE* pipeBuf, DWORD pipeLen,
                                              hPkt* pPkt)
{
    //Typecasted with void to supress the unused parameter warning
    (void)pipeBuf;
    (void)pipeLen;
    (void)pPkt;
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Add comments when implemented
*/
RETURNCODE HARTCommunicationManager::pkt2pipe(hPkt* pPkt, BYTE* pipeBuf,
                                              DWORD& pipeLen)
{
    //Typecasted with void to supress the unused parameter warning
    (void)pipeBuf;
    (void)pipeLen;
    (void)pPkt;
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    To send a write command
*/
RETURNCODE HARTCommunicationManager::WriteCommand(unsigned int cmdNum,
                                                  DEVDATA* vDeviceData2)
{
    //Typecasted with void to supress the unused parameter warning
    (void)cmdNum;
    (void)vDeviceData2;
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    To send a read command
*/
RETURNCODE HARTCommunicationManager::ReadCommand(unsigned int cmdNum)
{
    int status = STATUS_ERROR;
    int retry = 0;

    do
    {
        //Send and receive the command
        status = send_receive(cmdNum);
        retry++;

    } while (STATUS_OK != status && retry <= MAX_RETRY);

    if (STATUS_OK != status)
    {
        return FAILURE;
    }
    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    This method sends how many are in the send queue ( 0 == empty ).
*/
int HARTCommunicationManager::sendQsize()
{
    // does nothing for now
    return 0;
}

/*  ----------------------------------------------------------------------------
    This method sends how many will fit in the send queue
*/
int	HARTCommunicationManager::sendQmax()
{
    // does nothing for now
    return 0;
}

/*  ----------------------------------------------------------------------------
    This method sets shutdown flag value
*/
RETURNCODE HARTCommunicationManager::setShutDownFlag(bool bFlag)
{
    m_bShutdownStack = bFlag;

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    This method is used to shutdown the HART communications
*/
RETURNCODE HARTCommunicationManager::shutdownComm()
{
    RETURNCODE rc = SUCCESS;

    thePktQueue.destroy();

    /* Since this API is called two times, upon disconnect request(while
       deleting device object, SDC-625 is invoking this API call) and
       shutdown request, this condition is added to perform
       shutdown stack only for shutdown request */
    if (m_bShutdownStack)
    {
        mc_shutdown();
    }

    return rc;
}

/*  ----------------------------------------------------------------------------
     Add comments when implemented
*/
void HARTCommunicationManager::disableAppCommRequests()
{
    // does nothing for now
}

/*  ----------------------------------------------------------------------------
    Sends and receives the command request and response
*/
RETURNCODE HARTCommunicationManager::sendCmd(int cmdNum)
{
    int status = STATUS_ERROR;
    int retry = 0;
    do
    {
        status = send_receive(cmdNum);
        retry++;

    } while (STATUS_OK != status && retry <= MAX_RETRY);

    if (STATUS_OK != status)
    {
        return FAILURE;
    }

    return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sets Primary or secondary master
*/
RETURNCODE HARTCommunicationManager::setMasterType(BYTE masterType)
{
     hll_set_master(masterType);
     return SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Sets the maximum polling address for device scanning
*/
RETURNCODE HARTCommunicationManager::setPreamble(BYTE preamble)
{
   vDeviceData.cPreambles = preamble;
   return SUCCESS;
}
