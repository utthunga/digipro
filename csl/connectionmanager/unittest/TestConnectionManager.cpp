/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Connection Manager Unit Test
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#include <iostream>
#include "gtest/gtest.h"
#include "CommonProtocolManager.h"
#include "IProtocolAdaptor.h"
#include "ConnectionManager.h"
#include "FFAdaptor.h"
#include "IConnectionManager.h"
#include "FFConnectionManager.h"
#include "DDSController.h"
#include "PMErrorCode.h"
#include "PMStatus.h"

using namespace std;

typedef struct _devinfo_TBL {
    DeviceInfo  deviceInfo;
    DeviceInfo* next;
} DEVICEINFO_TBL;

typedef struct _SCAN_DEVICEINFO_TBL{
    int             count;
    DEVICEINFO_TBL* list;

} SCAN_DEVICEINFO_TBL;

class TestConnectionManager : public :: testing :: Test
{
public:
    FFAdaptor *ffadaptor;
    SCAN_DEV_TBL scanDevTable;
    DeviceInfo devices[10];
    int ScanCount;
    DeviceInfo device;
    PMStatus status;

    DDSController* m_pDDSController;
    FFConnectionManager* m_pFFConnectionManager;
    FFParameterCache* m_pFFParameterCache;

   TestConnectionManager()
   {
       // Initialization code
   }

   ~TestConnectionManager()
   {
       // cleanup any pending stuff, but no exceptions allowed
   }

   // If the constructor and destructor are not enough for setting up and cleaning up each test, you can define the following methods.

   void SetUp()
   {
       // Code here will be called immediately after the constructor (right before each test).
   }

   void TearDown()
   {
       // Code here will be called immediately after each test (right before the destructor).
   }

   // Data members if any
};

TEST_F (TestConnectionManager, Initialize)
{
    ffadaptor = new FFAdaptor();

    // Instantiate the Connection Manager, Parameter Cache & DDS Controller
    m_pFFConnectionManager = new FFConnectionManager;
    m_pFFParameterCache	= new FFParameterCache;
    m_pDDSController 	= new DDSController;

    // Check for improper initialisation
    if ((nullptr == m_pFFConnectionManager) ||
        (nullptr == m_pFFParameterCache) ||
        (nullptr ==  m_pDDSController))
    {
        // When one of them is not intialised no point running further, kill the application
        exit(-1);
    }

    // Initialise DDS Controller with Connection Manger & Parameter cache
    m_pDDSController->initialize(ffadaptor, m_pFFConnectionManager,m_pFFParameterCache);

    // Initialise Connection Manager with DDS Controller & Parameter Cache
    m_pFFConnectionManager->initialize(m_pDDSController, m_pFFParameterCache);

    // Initialise Parameter Cache with DDS Controller & Connection Manager
    m_pFFParameterCache->initialize(m_pDDSController, m_pFFConnectionManager, ffadaptor);

    // set bus parameters
    FF_BUS_PARAMS* busParams = new FF_BUS_PARAMS;
    busParams->this_link = 0;
    busParams->slot_time = 8;
    busParams->phlo = 6;
    busParams->mrd = 10;
    busParams->mipd = 16;
    busParams->tsc = 3;
    busParams->phis_skew = 0;
    busParams->phpe = 2;
    busParams->phge = 0;
    busParams->fun = 248;
    busParams->nun = 0;

    // Below code is applicable for unit test case, 
    // In normal scenarios these attribues were read from cpmappinfo.json config file
    char* ffDDPath = getenv("FF_DD_PATH");
    char* ffDeviceFile = getenv("FF_DEVICE_FILE");

    m_pFFConnectionManager->initStack(busParams, ffDDPath, ffDeviceFile);
}

TEST_F (TestConnectionManager, ScanDevice)
{
    ENV_INFO env_info;
    APP_INFO  app_info;

    (void)memset((char *) &scanDevTable, 0, sizeof(SCAN_DEV_TBL));
    (void)memset((char *) &app_info, 0, sizeof(APP_INFO));
    (void)memset((char *) &env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    int rs = 0;

    EXPECT_NO_THROW(status = m_pFFConnectionManager->scanDevices(&env_info, &scanDevTable));
    if (status.hasError())
    {
        rs = FAILURE;
    }
    EXPECT_EQ(0, rs);
}

TEST_F (TestConnectionManager, GetDeviceInfo)
{
    int rs = 0;
    SCAN_TBL *pList = scanDevTable.list;
    SCAN_TBL *temp = NULL;

    if (scanDevTable.count > 0)
    {
        ScanCount = 0;
        while(pList != NULL )
        {
            DeviceInfo deviceInfo;
            (void)memset((char *) &deviceInfo, 0, sizeof(DeviceInfo));

            EXPECT_NO_THROW(status = m_pFFConnectionManager->getDeviceInfo(pList->station_address, &deviceInfo));
            if (status.hasError())
            {
                rs = FAILURE;
            }
            EXPECT_EQ(0,rs);
            if (rs == SUCCESS)
            {
                devices[ScanCount++] = deviceInfo;
                device = deviceInfo;
            }
            temp = pList;
            pList = pList->next;
            free(temp);
        }
    }
}

TEST_F (TestConnectionManager, ConnectDeviceAtIndexZero)
{
    int rs = 0;
    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = m_pFFConnectionManager->connectDevice(&devices[0]));
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(SUCCESS, rs);
    }
}

TEST_F (TestConnectionManager, ConnectAgainWithoutDisconnect)
{
    int rs = 0;
    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = m_pFFConnectionManager->connectDevice(&devices[0]));
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(FAILURE, rs);
    }
}

TEST_F (TestConnectionManager, DisconnectDevice)
{
    int rs = 0;
    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = m_pFFConnectionManager->disconnectDevice());
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(SUCCESS, rs);
    }
}

TEST_F (TestConnectionManager, DisconnectDeviceAgainWithoutConnect)
{
    int rs = 0;
    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = m_pFFConnectionManager->disconnectDevice());
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(FAILURE, rs);
    }
}
