/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Connection Manager Unit Test
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#include <iostream>
#include "gtest/gtest.h"
#include "HARTConnectionManager.h"
#include "SDC625Controller.h"
#include "HARTAdaptor.h"


using namespace std;

static HARTConnectionManager* s_phartCM =  new HARTConnectionManager();
static SDC625Controller* s_phartSDC =  new SDC625Controller();
static HARTParameterCache* s_phartPC =  new HARTParameterCache();
static HARTAdaptor* s_phartAdaptor =  new HARTAdaptor();

static HART_SCAN_DEV_TBL s_scanDevTable;


class TestHARTConnectionManager : public :: testing :: Test
{
public:


   TestHARTConnectionManager()
   {

   }

   ~TestHARTConnectionManager()
   {
       // cleanup any pending stuff, but no exceptions allowed
   }

   // If the constructor and destructor are not enough for setting up and cleaning up each test, you can define the following methods.

   void SetUp()
   {
       // Initialization code
   }

   void TearDown()
   {
       // Code here will be called immediately after each test (right before the destructor).
   }

   // Data members if any
};

TEST_F (TestHARTConnectionManager, InitStack)
{
   ASSERT_NE(s_phartCM, nullptr);

   PMStatus status;
   HART_APP_SETTINGS appSetting;
   HART_BUS_PARAMS busParams;
   appSetting.pollAddrOption = PollByAddress::ADDRESS_0_15;
   busParams.masterType = HartMasterType::HART_SECONDARY_MASTER;
   busParams.preamble = 5;

   EXPECT_NO_FATAL_FAILURE(s_phartAdaptor->initialise());

   const char* hartDDPath = "/usr/share/dd";
   const char* hartDeviceFile = "/dev/ttyACM0";

   s_phartPC->initialize(s_phartSDC, s_phartCM);
   s_phartSDC->initialize(s_phartCM, s_phartPC, s_phartAdaptor,
                          "/usr/share/dd/HCF/DDL/Library");
   s_phartCM->initialize(s_phartSDC, s_phartPC);

    status =
            s_phartCM->initStack(&busParams, &appSetting, hartDDPath, hartDeviceFile, "VIATOR");

    ASSERT_TRUE(false == status.hasError());
}

TEST_F (TestHARTConnectionManager, StartScan)
{
    PMStatus status;
    status = s_phartCM->scanDevices([this](bool deviceFound, Indentity_t identity)
    {
        ASSERT_TRUE("644HART " == identity.sTag);
        ASSERT_TRUE(1 == identity.PollAddr);
        ASSERT_TRUE(1575244288 == identity.dwDeviceId);
     });
}

TEST_F (TestHARTConnectionManager, ConnectDevice_PostiveTest)
{
   ASSERT_NE(s_phartCM, nullptr);

   PMStatus status;
   int nodeAddr = 1;

   DeviceInfo deviceInfo;
   deviceInfo.node_address = nodeAddr;

   status = s_phartCM->connectDevice(&deviceInfo);

   if(!status.hasError())
   {
       cout<<"\n Connected Device Info: \nAddress :" <<
             deviceInfo.node_address <<
             ":\n Tag : " <<deviceInfo.pd_tag <<
             ":\n DeviceID : "<<deviceInfo.dev_id;
   }
   else
   {
       cout<<"Device failed to connect!!";
   }

   EXPECT_TRUE(false == status.hasError());
}

TEST_F (TestHARTConnectionManager, ConnectDevice_NegativeTest)
{
   ASSERT_NE(s_phartCM, nullptr);

   PMStatus status;
   int nodeAddr = 1;

   DeviceInfo deviceInfo;
   deviceInfo.node_address = nodeAddr;

   status = s_phartCM->connectDevice(&deviceInfo);

   EXPECT_TRUE(true == status.hasError());
}

TEST_F (TestHARTConnectionManager, disconnectDevice)
{
   ASSERT_NE(s_phartCM, nullptr);

   PMStatus status;

   status = s_phartCM->disconnectDevice();

   EXPECT_TRUE(false == status.hasError());
}


TEST_F (TestHARTConnectionManager, shutDownStack)
{
   ASSERT_NE(s_phartCM, nullptr);

   PMStatus status;

   status = s_phartCM->shutDownStack();

   EXPECT_TRUE(false == status.hasError());
}

