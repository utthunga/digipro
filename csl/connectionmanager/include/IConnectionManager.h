/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga
*/

/** @file
    Connection Manager Interface class declaration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _ICONNECTION_MANAGER_H
#define _ICONNECTION_MANAGER_H

#define HOST_ADDRESS 254
#define VENDOR_NAME " Fluke Corporation"
#define MODEL_NAME " Fluke 756 Desktop"
#define REVISION_STRING " V1.0"
#define HOST_TAG " Hutch Desktop 756"

#define RESTART_PARAM_VALUE 4
#define RESTART_PARAM_ID    0x80020152
#define RESOURCE_BLOCK_TAG  "__resource_block"
#define RESOURCE_BLOCK_TAG2 "__resource_block_2"

/* INCLUDE FILES **************************************************************/

#include "PMErrorCode.h"
#include "PMStatus.h"
#include "commonstructure/CommonStructures.h"

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** IConnectionManager Class Definition.

    IConnectionManager class is interface for Connection
    Manager and Adaptor irrespective of the protocol types.
*/

class IConnectionManager
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Construct a IConnectionManager

        Constructs the class and initializes the member variables
    */
    IConnectionManager()
        : m_nodeAddress(0), m_isConnected(false), m_pDeviceInfo(nullptr)
    {
    }

    /*------------------------------------------------------------------------*/
    /** Destruct a IConnectionManager

        Destructs a class and deallocates the members
    */
    virtual ~IConnectionManager()
    {
        m_pDeviceInfo = nullptr;
    }

    /* Public Member Functions ===============================================*/
    /// All the interfaces functions to be implemented in derived classes
    /*------------------------------------------------------------------------*/
    /** Initialize the communication Stack

        @param busParams - contains bus param info required for connection
        @param ddPath - contains DD Path for connection
        @param deviceFile - contains device file to access comunication

        @return status
    */
    virtual PMStatus initStack(void* busParams, const char* ddPath,
                               const char* deviceFile) = 0;

    /*------------------------------------------------------------------------*/
    /** Scans the available devices in the FF network and fills the scan result
        table

        @param envInfo - contains environment info
        @param scanResult - contains scanned device info

        @return status
    */
    virtual PMStatus scanDevices(void* envInfo, void* scanResult) = 0;

    /*------------------------------------------------------------------------*/
    /** Retrives the device information for the given node address

        @param nodeAddress - contains node address
        @param deviceInfo - contains device info

        @return
    */
    virtual PMStatus getDeviceInfo(int nodeAddress, DeviceInfo* deviceInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Connects the physical device for the requested node address

        @param deviceInfo - contains device info

        @return status
    */
    virtual PMStatus connectDevice(DeviceInfo* deviceInfo) = 0;

    /*------------------------------------------------------------------------*/
    /** Disconnects the connected device

        @return status
    */
    virtual PMStatus disconnectDevice() = 0;

    /*------------------------------------------------------------------------*/
    /** Closes the communication Stack

        @return status
    */
    virtual PMStatus shutDownStack() = 0;

    /* Explicitly Disabled Methods ===========================================*/
    IConnectionManager& operator=(const IConnectionManager& rhs) = delete;
    IConnectionManager(const IConnectionManager& rhs) = delete;

protected:
    /* protected Data Members ================================================*/
    /// Device Node address
    int m_nodeAddress;

    /// Device connection state
    bool m_isConnected;

    /// Connected device details
    DeviceInfo* m_pDeviceInfo;
};

#endif  // _ICONNECTION_MANAGER_H
