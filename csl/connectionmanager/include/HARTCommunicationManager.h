/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    This class defines hCommInf(included in SDC-625 code base)interfaces using
    HART stack APIs that communicates with phsyical HART devices.

    This class is designed to use by both HARTConnectionManager & SDC-625 core
    classes to interact with physical HART devices.
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _HART_COMMUNICATION_MANAGER_H
#define _HART_COMMUNICATION_MANAGER_H

/* INCLUDE FILES **************************************************************/

#include "DDLBaseComm.h"
#include "ddbGeneral.h"
#include "hartmdll.h"

/*
 * Extended timeout for slow response HART slave device
 * 
 */
#define MAX_EXTEND_TIMEOUT          500

/*============================================================================*/
/** Class that defines a HARTCommunicationManager.
*/
class HARTCommunicationManager : public CbaseComm
{
private:
    /* Private Data Members ==================================================*/
    bool m_bShutdownStack;

public:
    /* Construction and Destruction ==========================================*/
    /// Construct a HARTCommunicationManager
    HARTCommunicationManager(); // Constructor
    /*------------------------------------------------------------------------*/
    /// Destruct a HARTCommunicationManager
    virtual ~HARTCommunicationManager(); // Destructor

    /*------------------------------------------------------------------------*/
    /** Initializes HART communication using HART Stack APIs

        This API is deprecated instead of this using
                initComm(const char* deviceFile, const char* hartModem) API
    */
    RETURNCODE  initComm();

    /*------------------------------------------------------------------------*/
    /** This function invokes find_transmitter() of HART Stack and
        fils the Indentity_t appropriately
    */
    RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr);

    /*------------------------------------------------------------------------*/
    /** This function retrieves short tag for the transmitter and fills the
        Indentity_t information
    */
    RETURNCODE GetDeviceTag(Indentity_t& retIdentity);

    /*------------------------------------------------------------------------*/
    /** TODO(Sethu) - Add comment when implemented
    */
    bool appCommEnabled ();

    /*------------------------------------------------------------------------*/
    /** Gets an empty packet for generating a command
        (gives caller ownership of the packet*)
        pass back ownership with Sendxxx OR putMTPacket if command send aborted
    */
    hPkt* getMTPacket(bool pendOnMT=false);

    /*------------------------------------------------------------------------*/
    /** Returns the packet memory back to the interface
        (caller relinguishes ownership of the packet*)
    */
    void  putMTPacket(hPkt*);

    /*------------------------------------------------------------------------*/
    /** Returns the packet memory back to the interface
        (caller relinguishes ownership of the packet*)
    */
    RETURNCODE SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);

    /*------------------------------------------------------------------------*/
    /** This method invokes send_receive() method of HART Stack.
    */
    RETURNCODE SendCmd(int cmdNum,
                       BYTE* pData,
                       BYTE& dataCnt,
                       int timeout,
                       cmdInfo_t& cmdStatus);

    /*------------------------------------------------------------------------*/
    /** This method sends pkt based on priority
    */
    RETURNCODE SendPriorityPkt(hPkt* pPkt,
                               int timeout,
                               cmdInfo_t& cmdStatus);

    /*------------------------------------------------------------------------*/
    /** TODO (Sethu) : Add proper comment
    */
    void  enableAppCommRequests ();

    /*------------------------------------------------------------------------*/
    /** This method is for sending read commands.
        To send a write command
        TODO (Sethu) : Add proper comment
    */
    void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);

    /*------------------------------------------------------------------------*/
    /** This method is for setting the number of reties for method.
        TODO (Sethu) : Add proper comment
    */
    short setNumberOfRetries(short newVal);

    /*------------------------------------------------------------------------*/
    /** This method is for sending read commands.
        To send a write command
    */
    RETURNCODE WriteCommand(unsigned int cmdNum,   DEVDATA* vDeviceData2);
    /*------------------------------------------------------------------------*/
    /** This method is for sending read commands.
    */
    RETURNCODE ReadCommand(unsigned int cmdNum); // Send ReadCommand

    /*------------------------------------------------------------------------*/
    /** Initializes HART communication using HART Stack APIs

        @return SUCCESS or FAILURE
    */
    RETURNCODE initComm(const char* deviceFile, const char* hartModem);

    /*------------------------------------------------------------------------*/
    /** Sets Primary or not (is set secondary)

        @param masterType - contains Primary or Secondary master info

        @return SUCCESS or FAILURE
    */
    RETURNCODE setMasterType(BYTE masterType);

    /*------------------------------------------------------------------------*/
    /** Sets preamble

        @param preamble - contains the no of preambles

        @return SUCCESS or FAILURE
    */
    RETURNCODE setPreamble(BYTE preamble);

    /*------------------------------------------------------------------------*/
    /** This method sends how many are in the send queue ( 0 == empty ).
    */
    int sendQsize();

    /*------------------------------------------------------------------------*/
    /** This method sends how many are in the send queue ( 0 == empty ).
    */
    int	sendQmax();

    /*------------------------------------------------------------------------*/
    /** This method is used to shutdown the HART communications
    */
    RETURNCODE shutdownComm();

    /*------------------------------------------------------------------------*/
    /** Add comments when implemented. Does nothing for now.
    */
    void disableAppCommRequests();

    /*------------------------------------------------------------------------*/
    /** Sends the command request and receives the response

        @param cmdNum - command number for request

        @return SUCCESS or FAILURE
     */
     RETURNCODE sendCmd(int cmdNum);

     /*------------------------------------------------------------------------*/
     /** Sets shutdown stack flag value

         @param bFlag - this flag indicates to shutdown HART stack or not

         @return SUCCESS or FAILURE
      */
      RETURNCODE setShutDownFlag(bool bFlag);

     /* Public Data Members ==================================================*/
     hCPktQueue thePktQueue;

protected:
    /*------------------------------------------------------------------------*/
    /** Add comments when implemented. Does nothing for now.
    */
    RETURNCODE  pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt);

    /*------------------------------------------------------------------------*/
    /** Add comments when implemented. Does nothing for now.
    */
    RETURNCODE  pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen);
};
#endif // _HART_COMMUNICATION_MANAGER_H
