/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FF Connection Manager class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _FFCONNECTION_MANAGER_H
#define _FFCONNECTION_MANAGER_H

#define FILE_PATH_SIZE 128
#define INVALID_VALUE (-1)
#define DEVICE_CLASS_INDEX 5
typedef unsigned char UINT8;

/* INCLUDE FILES **************************************************************/

#include <mutex>
#include <condition_variable>
#include "ConnectionManager.h"
#include "IConnectionManager.h"
#include "IProtocolAdaptor.h"
#include "host.h"

using namespace std;

/// callback wrappers
typedef std::function<void(std::string)> CommissionStatusCallBack;

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of DDSController and IParameterCache
*/
class DDSController;
class IParameterCache;

/*============================================================================*/
/** FFConnectionManager Class Definition.

    FFConnectionManager class implements the Connection
    Manager mechanism for Foundation Fieldbus protocol.
*/

class FFConnectionManager : public IConnectionManager, public ConnectionManager
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a FFConnectionManager
    FFConnectionManager();

    /*------------------------------------------------------------------------*/
    /// Destruct a FFConnectionManager
    virtual ~FFConnectionManager();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the FF Connection Manager with DDSController and
        IParameterCache instances

        @param dds - DDSController instance
        @param pc - IParameterCache instance

        @return status
    */
    PMStatus initialize(DDSController* dds, IParameterCache* pc);

    /*------------------------------------------------------------------------*/
    /** This function used to create instance of FF Stack and initialise
        communication

        @param params - contains bus param info required for connection

        @return status
    */
    PMStatus initStack(void* params, const char* ddPath, const char* deviceFile);

    /*------------------------------------------------------------------------*/
    /** Scans the available devices in the FF network and fills the scan
        result table

        @param envInfo - contains application info
        @param scanResult - contains scanned device structure

        @return status
    */
    PMStatus scanDevices(void* envInfo, void* scanDevTable);

    /*------------------------------------------------------------------------*/
    /** Retrives the device information for the given node address

        @param nodeAddress - contains node address
        @param deviceInfo - contains device information

        @return status
    */
    PMStatus getDeviceInfo(int nodeAddress, DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Connects the physical device for the requested node address

        @param deviceInfo - contains device specific information

        @return status
    */
    PMStatus connectDevice(DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Disconnects the connected device

        @return status
    */
    PMStatus disconnectDevice();

    /*------------------------------------------------------------------------*/
    /** Closes the FF communication Stack

        @return status
    */
    PMStatus shutDownStack();

    /*------------------------------------------------------------------------*/
    /** Changes the device class from Basic to LM or LM to Basic

        @param deviceClass - contains the device class info either basic or LM
        @param deviceAddress -  contains device Address

        @return status
    */
    PMStatus setDeviceClass(int deviceClass, int deviceAddress);

    /*------------------------------------------------------------------------*/
    /** Retreives the current device class

        @param deviceAddress - contians device address
        @param deviceClass - contains either basic or LM

        @return status
    */
    PMStatus getDeviceClass(int deviceAddress, UINT8 &deviceClass);

    /*------------------------------------------------------------------------*/
    /** Assigns the requested address and physical device tag to the
           field device

        @param address - current device address
        @param newAddress - contains new device address
        @param newTag - new PD tag info
        @param commissionCallBack - callback function to notify commission
    */
    static void commissionDevice(int address,
                                 int newAddress, string newTag,
                                 CommissionStatusCallBack notifyMessage);
    /*------------------------------------------------------------------------*/
    /** Sets the device to default address (248 to 251)

        @param address - current device address
        @return status
    */
    PMStatus decommissionDevice(int address);

    /*------------------------------------------------------------------------*/
    /** creates commission thread to peform device commissioning

        @param address - current device address
        @param newAddress - contains new device address
        @param newTag - new PD tag info
        @param commissionCallBack - callback function to notify commission
                status
    */
    void createCommissionThread(int address,int newAddress, string newTag,
                               CommissionStatusCallBack commissionStatusCallBack);

    /*------------------------------------------------------------------------*/
    /** Aquires the thread lock and wait till release
	*/
    static void aquireLock();

    /*------------------------------------------------------------------------*/
    /** Releases the locked thread and notify for further process
	*/
    static void releaseLock();

    /*------------------------------------------------------------------------*/
	/** Notifies FFAdaptor about device disconnect
	*/
    static int notifyDisconnect(unsigned char);

   /*------------------------------------------------------------------------*/
   /** Notifies about CPM state change to linux thread
   */
   static int getConnectionStatus();

   /*------------------------------------------------------------------------*/
   /** it pulls the LAS information for stack services
   */

   static void notifyLASAddress(unsigned int activeLAS);

   /*------------------------------------------------------------------------*/
   /** it set the LAS node address.
   */
   void setLASAddress(unsigned int activeLAS);

   /*------------------------------------------------------------------------*/
   /** it get the LAS node address.
   */
   unsigned int getLASAddress();

   /* Explicitly Disabled Methods ===========================================*/
   FFConnectionManager& operator=(const FFConnectionManager& rhs) = delete;
   FFConnectionManager(const FFConnectionManager& rhs) = delete;

   /*------------------------------------------------------------------------*/
   /** Returns the Device Information
   */
  const DeviceInfo & getDeviceInfo();

public:
    /* public data Members ===================================================*/
    /*------------------------------------------------------------------------*/

	// hold LAS node address
    unsigned int m_lasNodeAddress;
	
    /// Holds the field Device Status
    static PMStatus s_fldDevStatus;

    static bool s_isCompleted;

    /// Mutex Thread Lock
    static mutex m_lock;

    /// Condition variable for handle thread
    static condition_variable m_conditionVariable;

private:
    /* private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /** Fills the block table entries

        @return status
    */
    PMStatus fillBlockTable();

    /*------------------------------------------------------------------------*/
    /** Configures the host by assigning bus parameter values

        @param hostConfig - contains host
        @param busParams - contains bus parameter
    */
    void configureHost(HOST_CONFIG* hostConfig, void* params);

    /* Private Data Members ==================================================*/
    /// DDS Controller instance
    DDSController* m_pDDSController;

    /// Parameter Cache instance
    IParameterCache* m_pPC;

    /// Contains host info
    HOST_INFO m_hostInfo;

    /// Contains host configuration
    HOST_CONFIG m_hostconfig;
};

#endif  // _FFCONNECTION_MANAGER_H
