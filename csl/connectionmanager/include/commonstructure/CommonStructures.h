/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Common Structure class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _COMMON_STRUCTURES_H
#define _COMMON_STRUCTURES_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_BLOCK_TAG_SIZE   65
#define MAX_DEVICE_TAG_LENGTH 34

typedef struct
{
    char   pd_tag[MAX_DEVICE_TAG_LENGTH];
    char   dev_id[MAX_DEVICE_TAG_LENGTH];
    int    node_address;
    int    manufacturer;
    int    device_type;
    int    device_rev;
    int    dd_rev;
    void*  device_element;
    int    blk_count;
    char*  blk_tag[MAX_BLOCK_TAG_SIZE];
    char*  dd_tag[MAX_BLOCK_TAG_SIZE];
    int    hartRevision;
} DeviceInfo;

typedef struct
{
    int ISLAS;
} CommunicationStatus;

typedef int DeviceHandle;

static CommunicationStatus state;

#ifdef __cplusplus
}
#endif

#endif  // _COMMON_STRUCTURES_H
