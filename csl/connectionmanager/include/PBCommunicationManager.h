/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    This class defines device_io_pb(included in fbapi stack code base)interfaces
    using ProfiBus stack APIs that communicates with phsyical ProfiBus devices.

    This class is designed to use by both PBConnectionManager & fbapi stack
    classes to interact with physical ProfiBus devices.
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _PB_COMMUNICATION_MANAGER_H
#define _PB_COMMUNICATION_MANAGER_H

/* INCLUDE FILES **************************************************************/

#include <string>
#include <vector>
#include <condition_variable>

#include "ConnectionManager.h"

#include "PBAdaptor.h"
#include "PMStatus.h"
#include "PMErrorCode.h"

#include "pbInterpreter.h"
#include "profibusmm.h"
#include "device_io_pb.h"

/*============================================================================*/
/** Class that defines a PBCommunicationManager.
*/
class PBCommunicationManager : public edd::device_io_pb
{
public:
    /* Construction and Destruction ==========================================*/
    /// Construct a PBCommunicationManager
    PBCommunicationManager(class edd::Interpreter &interpret); // Constructor

    /*------------------------------------------------------------------------*/
    /// Destruct a PBCommunicationManager
    virtual ~PBCommunicationManager(); // Destructor

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Sets the slave device from connection manager

        @param slaveDevice - contains slave device

        @return None
    */
    void setSlaveDevice(io_driver::pb::slave_t slaveDevice);

    /*------------------------------------------------------------------------*/
    /** Sets the connection staate within the communication manager

        @return None
    */
    void setConnectionState(device_io::state newstate);

    /*------------------------------------------------------------------------*/
    /** Reads the data from the device

        @param request - contains data

        @return status
    */
    long pb_read(struct request &request);

    /*------------------------------------------------------------------------*/
    /** Writes the data to the device

        @param request - contains data

        @return status
    */
    long pb_write(const struct request &request);

    /// Contains the device instance
    std::shared_ptr<io_driver::pb> m_device;

    /*= Explicitly Disabled Methods ==========================================*/
    PBCommunicationManager& operator = (const PBCommunicationManager& rhs) = delete;
    PBCommunicationManager(const PBCommunicationManager& rhs) = delete;

private:

    /// Stores the connected device
    io_driver::pb::slave_t m_slaveDevice;
};

#endif // _PB_COMMUNICATION_MANAGER_H
