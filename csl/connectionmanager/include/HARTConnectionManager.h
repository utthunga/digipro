/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bhuvana T
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
   Defines interfaces that communicates with HART devices using HART stack
   APIs
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _HART_CONNECTION_MANAGER_H
#define _HART_CONNECTION_MANAGER_H

/* INCLUDE FILES **************************************************************/

#include "ConnectionManager.h"
#include "HARTAdaptor.h"
#include "AdaptorDef.h"
#include "HARTCommunicationManager.h"
#include "IConnectionManager.h"
#include "PMErrorCode.h"
#include "PMStatus.h"


/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of SDC625Controller
*/
class SDC625Controller;

/*============================================================================*/
/** HARTConnectionManager Class Definition.

    HARTConnectionManager class implements the Connection
    Manager mechanism for HART protocol.
*/

class HARTConnectionManager : public IConnectionManager,
                              public ConnectionManager
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Constructs a HARTConnectionManager
    HARTConnectionManager();

    /*------------------------------------------------------------------------*/
    /// Destructs a HARTConnectionManager
    virtual ~HARTConnectionManager();

    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialise all active tables with respect to HART DD information

        @param sdc - contains SDC625Controller instance
        @param pc - contains IParameterCache instance

        @return status
    */
    PMStatus initialize(SDC625Controller* sdc, IParameterCache* pc);

    /*------------------------------------------------------------------------*/
    /** Initialises HART Stack for the given bus parameters

        @param busParams - contains bus parameters
        @param ddPath - path to dd file
        @param deviceFile - device file

        @return object returns 0 if success;
        else HART_CM_ERR_INIT_STACK_FAILED or
        HART_CM_ERR_STACK_ALREADY_INITIALIZED error code
    */
    PMStatus initStack(void* busParams, const char* ddPath,
                                        const char* deviceFile);

    /*------------------------------------------------------------------------*/
    /** Initialises HART Stack for the given bus parameters

        @param busParams - contains bus parameters
        @param appSettings - contains app settings
        @param ddPath - path to dd file
        @param deviceFile - device file

        @return object returns 0 if success;
        else HART_CM_ERR_INIT_STACK_FAILED or
        HART_CM_ERR_STACK_ALREADY_INITIALIZED error code
    */
    PMStatus initStack(void* busParams, void* appSettings, const char* ddPath,
                                const char* deviceFile, const char* hartModem);

    /*------------------------------------------------------------------------*/
    /**  Scans the available devices in the HART network

        @param callBack - callback function to notify scan result

        @return object returns 0 if success;
        else HART_CM_ERR_NO_DEVICES_FOUND error code
    */
    PMStatus scanDevices(std::function<void(bool, Indentity_t)> callBack);

    /*------------------------------------------------------------------------*/
    /** Scans the available devices in the HART network.
        This API is deprecated, instead using
        scanDevices(std::function<void(Indentity_t)> callBack)

        @param envInfo - contains application info
        @param scanDevTable - contains scanned device information such as
                              device tag, id and node address.
        @return object returns 0 if success;
        else HART_CM_ERR_NO_DEVICES_FOUND error code
    */
    PMStatus scanDevices(void* envInfo, void* scanDevTable);

    /*------------------------------------------------------------------------*/
    /** Retrieves the device information for the given slave address

        @param slaveAddress - contains poll address information
        @param deviceinfo - contains the device information

        @return object returns 0 if success;
        else HART_CM_ERR_NO_DEVICES_FOUND error code
     */
    PMStatus getDeviceInfo(int slaveAddress, DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Establishes field device connection and fills the device
        information structure for the given slave address

        @param deviceInfo - contains the device information

        @return object returns 0 if success;
        else HART_CM_ERR_CONNECT_DEVICE_FAILED,
        HART_CM_ERR_DEVICE_ALREADY_CONNECTED error code
    */
    PMStatus connectDevice(DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Disconnects the currently connected device

        @return object returns SUCCESS
     */
    PMStatus disconnectDevice();

    /*------------------------------------------------------------------------*/
    /** Closes the HART communication

        @return object returns 0 if success
    */
    PMStatus shutDownStack();

    /*------------------------------------------------------------------------*/
    /** stops the device scanning

     */
    void stopScan();


    /* Explicitly Disabled Methods ===========================================*/
    HARTConnectionManager& operator=(const HARTConnectionManager& rhs);
    HARTConnectionManager(const HARTConnectionManager& rhs);

private:
    /* Private Member Functions ==============================================*/

    /*------------------------------------------------------------------------*/
    /** Retrieves the maximum poll address for the option selected

        @param addressOption - addressOption contains polling option selection
        @param maxPollAddress - contains max poll address

        @return status
    */
    PMStatus getMaxPollAddress(
        PollByAddress addressOption, int& maxPollAddress);


    /*------------------------------------------------------------------------*/
    /** Sets the maximum polling address for device scanning

        @param pollOption contoins the polling option selection by user
    */
    PMStatus setMaxPollAddress(PollByAddress pollOption);

    /* Private Data Members ==================================================*/

    /// SDC-625 Controller instance
    SDC625Controller* m_pSDC625Controller;

    /// Parameter Cache instance
    IParameterCache* m_pParameterCache;

    /// CRealCommInfc object that communicates with HART stack
    HARTCommunicationManager* m_pCommManager;

    /// Indicates device is connected or not
    bool m_isConnected;

    /// Indicates stop scan
    bool m_isStopScan;

    /// Maximum poll address
    int m_maxPollAddress;
};

#endif  // _HART_CONNECTION_MANAGER_H
