/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PB Connection Manager class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _PBCONNECTION_MANAGER_H
#define _PBCONNECTION_MANAGER_H

/* INCLUDE FILES **************************************************************/

#include <string>
#include <vector>
#include <condition_variable>
#include "ConnectionManager.h"
#include "IConnectionManager.h"
#include "IProtocolAdaptor.h"
#include "PBCommunicationManager.h"

#include "fbapi_type.h"
#include "pbInterpreter.h"
#include "fbapi_if.h"
#include "profibusmm.h"

#define SUCCESS (0)
#define FAILURE (1)

#define PB_DEVICE_DRIVER_OPT        3
#define DEVICE_ID_INDEX             27
#define PB_STANDARD_TIMEOUT         100
#define STANDARD_DATA_LEN           128
#define FILE_PATH_LEN               128
#define LOOPBACK_DATA_LEN           240
#define TAG_DESC_INDEX              18

using namespace std;

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of IFakController and IParameterCache
*/
class IFakController;
class IParameterCache;
class PBCommunicationManager;

/*============================================================================*/
/** PBConnectionManager Class Definition.

    PBConnectionManager class implements the Connection
    Manager mechanism for ProfiBus protocol.
*/

class PBConnectionManager : public IConnectionManager, public ConnectionManager
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Construct a PBConnectionManager
    PBConnectionManager();

    /*------------------------------------------------------------------------*/
    /// Destruct a PBConnectionManager
    virtual ~PBConnectionManager();

    /* public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Initialize the PB Connection Manager with IFakController and
        IParameterCache instances

        @param ifak - IFakController instance
        @param pc   - IParameterCache instance

        @return status
    */
    PMStatus initialize(IFakController* dds, IParameterCache* pc);

    /*------------------------------------------------------------------------*/
    /** This function used to initialise communication

        @param params - contains bus param info required for connection
        @param ddPath - contains DD Path for connection
        @param deviceFile - contains device file to access comunication

        @return status
    */
    PMStatus initStack(void* params, const char* ddlPath,
                                                  const char* deviceFile);

    /*------------------------------------------------------------------------*/
    /** This function used to initialise communication

        @param params - contains bus param info required for connection
        @param settings - contains setting info required for connection
        @param ddPath - contains DD Path for connection
        @param deviceFile - contains device file to access comunication

        @return status
    */
    PMStatus initStack(void* params, void* settings, const char* ddlPath,
                                                  const char* deviceFile);

    /*------------------------------------------------------------------------*/
    /** Scans the available devices in the Profibus network and fills the scan
        result table

        @param envInfo - contains application info
        @param scanResult - contains scanned device structure

        @return status
    */
    PMStatus scanDevices(void* envInfo, void* scanDevTable);

    /*------------------------------------------------------------------------*/
    /** Retrives the device information for the given node address

        @param nodeAddress - contains node address
        @param deviceInfo - contains device information

        @return status
    */
    PMStatus getDeviceInfo(int nodeAddress, DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Connects the physical device for the requested node address

        @param deviceInfo - contains device specific information

        @return status
    */
    PMStatus connectDevice(DeviceInfo* deviceInfo);

    /*------------------------------------------------------------------------*/
    /** Disconnects the connected device

        @return status
    */
    PMStatus disconnectDevice();

    /*------------------------------------------------------------------------*/
    /** Closes the Profibus communication Stack

        @return status
    */
    PMStatus shutDownStack();

    /** Provides communication manager instance to the requested APIs

        @return communication manager instance
    */
    PBCommunicationManager* getCommunicationManager(void);

    /* Explicitly Disabled Methods ===========================================*/
    PBConnectionManager& operator=(const PBConnectionManager& rhs) = delete;
    PBConnectionManager(const PBConnectionManager& rhs) = delete;

private:
    /* private Member Functions ==============================================*/
    /// IFak Controller instance
    IFakController* m_pIFakController;

    /// Parameter Cache instance
    IParameterCache* m_pPC;

    /// Holds the communication manager
    PBCommunicationManager* m_commMgr;

    /// Holds the connected device address
    int m_slaveAddress;

    /// Contains the device instance
    std::shared_ptr<io_driver::pb> m_device;

    /// Stores the connected device
    io_driver::pb::slave_t m_slaveDevice;

    /// Softing driver
    const unsigned char DRIVER = PB_DEVICE_DRIVER_OPT;
};

#endif  // _PBCONNECTION_MANAGER_H
