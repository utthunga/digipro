/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Connection Manager class decleration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _CONNECTION_MANAGER_H
#define _CONNECTION_MANAGER_H

#define MAX_SCAN_RETRY 3

/* CLASS DECLARATIONS *********************************************************/

/*============================================================================*/
/** Connection Manager Class Definition
    ConnectionManager class is base class which
    implements common functionalities across the protocols
*/

class ConnectionManager
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Destruct a ConnectionManager
        Destructs the connection manager class and deallocates the member
        variables
    */
    virtual ~ConnectionManager() = default;

    /* Explicitly Disabled Methods ===========================================*/
    ConnectionManager& operator=(const ConnectionManager& rhs) = delete;
    ConnectionManager(const ConnectionManager& rhs) = delete;

protected:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /** Construct a ConnectionManager
        Constructs the connection manager class and initializes the member
        variables
    */
    ConnectionManager();

private:
    /* Private Data Members ==================================================*/
    /// Sequence ID for connection manager
    int m_sequenceID;
};

#endif  // _CONNECTION_MANAGER_H
