/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    This implements device_io_pb(included in fbapi stack code base) interfaces
    using ProfiBus stack APIs
    that communicates with phsyical ProfiBus devices.

    This class is designed to use by both PBConnectionManager class &
    fbapi stack classes to interact with physical ProfiBus devices.

    PBConnectionManager class instantiates this class and the same object
    reference will be used by fbapi stack classes.
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include <algorithm>
#include <string.h>

#include "PBCommunicationManager.h"

#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

// Constructor
PBCommunicationManager::PBCommunicationManager(class edd::Interpreter &interpret)
    : edd::device_io_pb(interpret), m_slaveDevice(NULL)
{
}

// Destructor
PBCommunicationManager::~PBCommunicationManager()
{

}

/*  ----------------------------------------------------------------------------
    Set slave device from connection manager
*/
void PBCommunicationManager::setSlaveDevice(io_driver::pb::slave_t slaveDevice)
{
    m_slaveDevice = slaveDevice;
}

/*  ----------------------------------------------------------------------------
    Set slave device from connection manager
*/
void PBCommunicationManager::setConnectionState(device_io::state newstate)
{
    state_change(newstate);
}

/*  ----------------------------------------------------------------------------
    Read the data from device
*/
long PBCommunicationManager::pb_read(struct request &request)
{
    long errorCode = SUCCESS;

    io_driver::pb::request_t req;
    req.slot = request.slot;
    req.index = request.index;

    // Read the data from device
    errorCode = m_device->read(m_slaveDevice, req);

    if (SUCCESS == errorCode)
    {
        // Assign the result in success case
        request.slot = req.slot;
        request.index = req.index;
        request.datalen = req.datalen;
        memcpy(request.data, req.data, req.datalen);
    }

    return errorCode;
}

/*  ----------------------------------------------------------------------------
    Write the data from the device
*/
long PBCommunicationManager::pb_write(const struct request &request)
{
    io_driver::pb::request_t req;
    req.slot = request.slot;
    req.index = request.index;
    req.datalen = request.datalen;
    memcpy(req.data, request.data, request.datalen);

    // Write the data to device
    return m_device->write(m_slaveDevice, req);
}
