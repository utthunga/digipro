/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    FF Connection Manager class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include <algorithm>
#include "DDSController.h"
#include "CommonProtocolManager.h"
#include "FFAdaptor.h"
#include "FFConnectionManager.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include "dds_comm.h"
#include "nm.h"
#include "device_access.h"
#include "ff_sm.h"


#define DEVICE_SELECT 2
#define TAG_LEN 32
#define TAG_SIZE 34
#define POS 1
#define TAG_EMPTY "                                 "

#define SET_ADDRESS "SET_ADDRESS"
#define SET_TAG     "SET_TAG"
#define CLEAR_TAG   "CLEAR_TAG"

// Forward decleration of the field device status
PMStatus FFConnectionManager::s_fldDevStatus;
bool FFConnectionManager::s_isCompleted = false;
mutex FFConnectionManager::m_lock;
condition_variable FFConnectionManager::m_conditionVariable;


/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    FFConnectionManager class instance initialization
*/
FFConnectionManager::FFConnectionManager()
    : m_pDDSController(nullptr), m_pPC(nullptr), m_lasNodeAddress(0)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
FFConnectionManager::~FFConnectionManager()
{
    // Clear the member variables
    m_pDDSController = nullptr;
    m_pPC = nullptr;

    if (nullptr != m_pDeviceInfo)
    {
        delete m_pDeviceInfo;
        m_pDeviceInfo = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to initialise all active tables w.r.to FF DD
*/
PMStatus FFConnectionManager::initialize(
    DDSController* dds, IParameterCache* pc)
{
    PMStatus status;
    if (nullptr != dds && nullptr != pc)
    {
        m_pDDSController = dds;
        m_pPC = pc;   
		connectdisconnNotification = reinterpret_cast<int (*)(unsigned char)>(notifyDisconnect);
        getConnectionState =   reinterpret_cast<int (*)()>(getConnectionStatus);
        setLASNodeAddress =    reinterpret_cast<unsigned int (*)(unsigned int)>(notifyLASAddress);
    }
    else
    {
        status.setError(
            FF_CM_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to create instance of FF Stack and initialise
    communication
*/
PMStatus FFConnectionManager::initStack(void* params, const char* ddPath, 
                                        const char* deviceFile)
{
    PMStatus status;
    int errorCode = SUCCESS;
    RET_INFO retInfo;
    HOST_INFO hostInfo;
    const char* pDdPath = ddPath;

    FF_BUS_PARAMS* busParams = static_cast<FF_BUS_PARAMS*>(params);
    if (nullptr == busParams)
    {
        // No bus parameter
        LOGF_ERROR(CPMAPP,"Failure in getting BUS parameters\n");
        errorCode = FAILURE;
        status.setError(
            FF_CM_ERR_GET_BUS_PARAM_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);

    }
    if (SUCCESS == errorCode)
    {
        if (nullptr == pDdPath)
        {
            // No environment variable has set
            LOGF_ERROR(CPMAPP,"Failure in getting DD path\n");
            errorCode = FAILURE;
            status.setError(
                FF_CM_ERR_GET_DD_PATH_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_CM);
        }
    }
    if (SUCCESS == errorCode)
    {
        auto* hostConfig = new HOST_CONFIG;

#ifdef DESKTOP
        // Get FBK2 PORT from environment file for desktop environment
        if (nullptr != deviceFile)
        {
            strncpy(hostConfig->tty_port, deviceFile, TTY_PORT_BUFFER_LEN);
        }
        else
        {
            // No environment variable has set
            LOGF_ERROR(CPMAPP,"Failure in getting PORT\n");
            errorCode = FAILURE;
            status.setError(
                FF_CM_ERR_ACCESS_PORT_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_CM);
        }
#endif
        if (SUCCESS == errorCode)
        {
            memset(&retInfo, 0, sizeof(RET_INFO));

            // Configure host information
            configureHost(hostConfig, busParams);

            // API to initialize the FF Back-end application
            errorCode = ff_initialize(&retInfo, pDdPath, hostConfig);
            if (errorCode == SUCCESS)
            {
                // API to configure the FF stack
                errorCode = ff_configure(&retInfo, hostConfig);
                if (errorCode == SUCCESS)
                {
                    // To maintain successfull reference for future usage
                    m_hostconfig = *hostConfig;

                    // API to get the host FBK board information
                    errorCode =
                        ff_get_host_info(&retInfo, &hostInfo, hostConfig);
                    if (errorCode == SUCCESS)
                    {
                        // To maintain successfull reference for future usage
                        m_hostInfo = hostInfo;
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP,"Failure in getting Device Information\n");
                        status.setError(
                            FF_CM_ERR_GET_HOST_INFO_FAILED,
                            PM_ERR_CL_FF_CSL,
                            CSL_ERR_SC_CM);
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP,"Failure in configuring the FF backend\n");
                    status.setError(
                        FF_CM_ERR_CONFIG_FF_FAILED,
                        PM_ERR_CL_FF_CSL,
                        CSL_ERR_SC_CM);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP,"Failure in initializing the FF backend\n");
                status.setError(
                    FF_CM_ERR_INITIALIZE_FF_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_CM);
            }
        }
        if (nullptr != hostConfig)
        {
            delete hostConfig;
            hostConfig = nullptr;
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to configure the host by assigning the busparameters and
    required values
*/
void FFConnectionManager::configureHost(HOST_CONFIG* hostConfig,
                                        void* params)
{
    strncpy(hostConfig->sm_config.vendor_name, VENDOR_NAME,
        SYSTEM_CONFIG_MEMBERS_SIZE);
    strncpy(hostConfig->sm_config.model_name, MODEL_NAME,
        SYSTEM_CONFIG_MEMBERS_SIZE);
    strncpy(hostConfig->sm_config.revision_string, REVISION_STRING,
        SYSTEM_CONFIG_MEMBERS_SIZE);

    // Get the host name from the configuartion file
    IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();
    if (pAppConfigParser != nullptr)
    {
	    std::string hostName = pAppConfigParser->getHostName();
	    strncpy(hostConfig->sm_config.host_pd_tag, hostName.c_str(), hostName.length()+1);
    }

    hostConfig->host_addr = HOST_ADDRESS;

    FF_BUS_PARAMS* busParams = static_cast<FF_BUS_PARAMS*>(params);

    hostConfig->dll_config.this_link = busParams->this_link;
    hostConfig->dll_config.slot_time = busParams->slot_time;
    hostConfig->dll_config.phlo = busParams->phlo;
    hostConfig->dll_config.mrd = busParams->mrd;
    hostConfig->dll_config.mipd = busParams->mipd;
    hostConfig->dll_config.tsc = busParams->tsc;
    hostConfig->dll_config.phis_skews = busParams->phis_skew;
    hostConfig->dll_config.phpe = busParams->phpe;
    hostConfig->dll_config.phge = busParams->phge;
    hostConfig->dll_config.fun = busParams->fun;
    hostConfig->dll_config.nun = busParams->nun;

    hostConfig->host_mode = HOST_ONLINE;
    hostConfig->host_type = HOST_STANDALONE;
}

/*  ----------------------------------------------------------------------------
    This function used to scan a device available in network
*/
PMStatus FFConnectionManager::scanDevices(void* envInfo, void* scanDevTable)
{
    PMStatus status;
    int errorCode = FAILURE;
    int retry = 0;
    SCAN_DEV_TBL* ffscanDevTable;

    if (nullptr != scanDevTable)
    {
        ENV_INFO* env = static_cast<ENV_INFO*>(envInfo);
        ffscanDevTable = static_cast<SCAN_DEV_TBL*>(scanDevTable);

        // Maximum times of re-try for scanning the devices
        while (retry < MAX_SCAN_RETRY)
        {
            // Scan list of Device connected to the FF BUS
            errorCode = cr_scan_network(env, ffscanDevTable);

            if (SUCCESS == errorCode)
            {
                break;
            }

            retry++;
        }
    }

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP,"Error scanning the live list\n");
        status.setError(
            FF_CM_ERR_SCAN_NETWORK_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Function to get the device information
*/
PMStatus FFConnectionManager::getDeviceInfo(
    int nodeAddress, DeviceInfo* deviceInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;
    APP_INFO appInfo{};
    ENV_INFO envInfo{};
    CR_SM_IDENTIFY_CNF smIdentifyCnf{};

    envInfo.app_info = &appInfo;

    // Retrieve SM identify tag with device address
    errorCode =
        cr_sm_ident(&envInfo, static_cast<UINT16>(nodeAddress), &smIdentifyCnf);

    if (errorCode == SUCCESS)
    {
        strncpy(
            deviceInfo->pd_tag,
            smIdentifyCnf.pd_tag,
            SYSTEM_CONFIG_MEMBERS_SIZE);
        strncpy(
            deviceInfo->dev_id,
            smIdentifyCnf.dev_id,
            SYSTEM_CONFIG_MEMBERS_SIZE);
        deviceInfo->node_address = nodeAddress;
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Error in getting device Info\n");
        status.setError(
            FF_CM_ERR_GET_DEVICE_INFO_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to connect device with help of device info
*/
PMStatus FFConnectionManager::connectDevice(DeviceInfo* deviceInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Check if already device is connected
    if (m_isConnected)
    {
        LOGF_INFO(CPMAPP,"Error in connecting device, already connected\n");
        status.setError(
            FF_CM_ERR_DEVICE_ALREADY_CONNECTED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
        errorCode = FAILURE;
    }

    if (SUCCESS == errorCode)
    {
        RET_INFO retInfo;

        memset(&retInfo, 0, sizeof(RET_INFO));

        // API to establish connection with the device
        errorCode = ff_connect(&retInfo, deviceInfo->node_address, deviceInfo);

        if (SUCCESS == errorCode)
        {
            // Stores the connected device information for future
            m_pDeviceInfo = new DeviceInfo;
            memcpy(m_pDeviceInfo, deviceInfo, sizeof(DeviceInfo));
            m_nodeAddress = deviceInfo->node_address;

            // Fills the block table
            status = fillBlockTable();
            if (status.hasError())
            {
                disconnectDevice();
            }
            else
            {
                m_isConnected = true;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Failure to establish the connection to the device\n");
            status.setError(
                FF_CM_ERR_CONNECT_DEVICE_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_CM);
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to fill the block tables
*/
PMStatus FFConnectionManager::fillBlockTable()
{
    PMStatus status;
    int errorCode = SUCCESS;
    int blockCount = 0;

    BLOCK_HANDLE blockHandle = INVALID_VALUE;
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    DD_HANDLE ddHandle{};

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    for (auto tag : m_pDeviceInfo->dd_tag)
    {
        ++blockCount;
        if (blockCount > m_pDeviceInfo->blk_count)
        {
            break;
        }

        char* blkTag = tag;
        blockHandle = INVALID_VALUE;

        //  Open a session with a tagged block
        errorCode = ct_block_open(blkTag, &blockHandle);

        if (SUCCESS == errorCode)
        {
            /* Load operational block information from a device into the
               Connection Manager
            */
            errorCode = bl_block_load(blockHandle, &envInfo);

            if (SUCCESS == errorCode)
            {
                if (nullptr != m_pDDSController)
                {
                    // Load a DD for the block
                    status =
                        m_pDDSController->dsDdBlockLoad(blockHandle, &ddHandle);

                    if (status.hasError())
                    {
                        // Ignore it. Error may be DD doesn't have this block
                        // information but block is available in device
                        status.clear();
                        LOGF_ERROR(CPMAPP,"Failure to load the block from DD\n");
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP,"No DDS Controller instance\n");
                    status.setError(
                        FF_CM_ERR_FILL_BLK_TABLE_FAILED,
                        PM_ERR_CL_FF_CSL,
                        CSL_ERR_SC_CM);
                    break;
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP,"Failure to load the block\n");
                status.setError(
                    FF_CM_ERR_LOAD_BLOCK_FAILED,
                    PM_ERR_CL_FF_CSL,
                    CSL_ERR_SC_CM);
                break;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Failure to open the block " << blkTag);
            status.setError(
                FF_CM_ERR_OPEN_BLOCK_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_CM);
            break;
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to disconnect currently connected device
*/
PMStatus FFConnectionManager::disconnectDevice()
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Check if device is connected
    if (!m_isConnected)
    {
        LOGF_INFO(CPMAPP,"Error in disconnecting device, device not connected\n");
        status.setError(
            FF_CM_ERR_NO_CONNECTED_DEVICE,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
        errorCode = FAILURE;
    }

    //gives active block table count
    int blkCount = get_abt_count();

    for (int cnt = 0 ; cnt < blkCount ; cnt++)
    {
        /*disconnects connection to the block
          and deallocates all active tables
        */
        ct_block_close(cnt);

        DEVICE_TYPE_HANDLE dth{0};

        // Gives device type handle
        get_abt_adtt_offset(cnt, &dth);

        // Removes device table
        ddi_remove_dd_dev_tbls(dth);
    }

    if (SUCCESS == errorCode)
    {
        RET_INFO retInfo;
        memset(&retInfo, 0, sizeof(RET_INFO));

        // Disconnect the connected device
        errorCode = ff_disconnect(&retInfo, static_cast<UINT16>(m_nodeAddress));

        if (SUCCESS == errorCode)
        {
            m_nodeAddress = 0;
            m_isConnected = false;
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Failure in disconnecting the device\n");
            status.setError(
                FF_CM_ERR_DISCONNECT_DEVICE_FAILED,
                PM_ERR_CL_FF_CSL,
                CSL_ERR_SC_CM);
        }
    }
    
    // Delete the Device Information memory
    if (nullptr != m_pDeviceInfo)
    {
        delete m_pDeviceInfo;
        m_pDeviceInfo = nullptr;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to close the FF stack communication
*/
PMStatus FFConnectionManager::shutDownStack()
{
    PMStatus status;
    int errorCode = SUCCESS;
    RET_INFO retInfo;
    memset(&retInfo, 0, sizeof(RET_INFO));

    // API to un-initialize the FF Back-end application
    errorCode = ff_uninitialize(&retInfo, m_hostconfig.host_mode);

    if (errorCode != SUCCESS)
    {
        LOGF_ERROR(CPMAPP,"Failure in exit FF\n");
        status.setError(
            FF_CM_ERR_UNINITIALIZE_FF_FAILED,
            PM_ERR_CL_FF_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Changes the device class from Basic to LM or LM to Basic
*/
PMStatus FFConnectionManager::setDeviceClass(int deviceClass, int deviceAddress)
{
    PMStatus status;
    int errorCode = SUCCESS;
    UINT16 index= 0;
    NETWORK_HANDLE networkHandle;
    int networkCount;
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    T_NM_DIR*  pNmDir;
    DeviceInfo deviceInfo;
    int statusCode = SUCCESS;

    envInfo.app_info = &appInfo;

    pNmDir = new T_NM_DIR;

    // establish device connection
    deviceInfo.node_address = deviceAddress;
    status = connectDevice(&deviceInfo);

    if(!status.hasError())
    {
        //gets the network count
        get_nt_net_count(&networkCount);

        //gets newtork handle by passing device address
        get_nt_net_handle_by_addr(networkCount,
                                  deviceAddress, &networkHandle);

        //gets the NM objects.
        errorCode = get_nt_net_nm_dir(networkHandle, pNmDir);

        if (errorCode == SUCCESS && nullptr != pNmDir)
        {
            // gives network table count
            errorCode = get_nt_net_count(&networkCount);
            if (SUCCESS == errorCode)
            {
                // gets the network handle by passing device address
                errorCode = get_nt_net_handle_by_addr(networkCount,
                                                      deviceAddress,
                                                      &networkHandle);
                if (SUCCESS != errorCode)
                {
                    LOGF_ERROR(CPMAPP,"Get Network handle failed!");
                    statusCode = FF_CM_ERR_GET_NETWORK_HANDLE_FAILED;
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP,"Get Network count failed!");
                statusCode = FF_CM_ERR_GET_NETWORK_COUNT_FAILED;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Get Network Directory failed!");
            statusCode = FF_CM_ERR_GET_NM_DIR_FAILED;
        }

        if (SUCCESS == statusCode)
        {
            // index of Boot Operational Class is Link Master Object Index + 5 
            index = pNmDir->dlme_lm_obj + DEVICE_CLASS_INDEX;

            // write value in Boot operational class of the device
            errorCode = cr_dev_boot(&envInfo, networkHandle, index, deviceClass);
            if(SUCCESS != errorCode)
            {
                LOGF_ERROR(CPMAPP,"Failed to set device class info!");
                statusCode = FF_CM_ERR_SET_DEVICE_CLASS_FAILED;
            }
            else
            {
                // restarts the device
                VariableType writeVar{};
                writeVar.itemID = RESTART_PARAM_ID;
                writeVar.value.stdValue.ul = RESTART_PARAM_VALUE;
                writeVar.type = DDS_ENUMERATED;
                writeVar.itemContainerTag.str = const_cast<char*>(RESOURCE_BLOCK_TAG);
                BLOCK_HANDLE blockHandle = m_pDDSController->getBlockHandleByBlockTag(
                            writeVar.itemContainerTag.str);

                // validates block handle
                if (valid_block_handle(blockHandle) == 0)
                {
                    writeVar.itemContainerTag.str = const_cast<char*>(RESOURCE_BLOCK_TAG2);
                    blockHandle = m_pDDSController->getBlockHandleByBlockTag(
                                                writeVar.itemContainerTag.str);
                    if (valid_block_handle(blockHandle) == 0)
                    {
                        errorCode = FAILURE;
                    }
                }
                if (SUCCESS == errorCode)
                {
                    ParamInfo setParamInfo{};
                    setParamInfo.blockTag = writeVar.itemContainerTag;
                    setParamInfo.paramList.push_back(writeVar);
                    status = m_pPC->writeValue(&setParamInfo);
                }
                if (FAILURE == errorCode || status.hasError())
                {
                    LOGF_ERROR(CPMAPP,"Failed to restart the device");
                    statusCode = FF_CM_ERR_RESTART_DEVICE_FAILED;
                }
            }
        }

        // disconnect the connected device
        status = disconnectDevice();
        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Failed to disconnnect the device");
            statusCode = FF_CM_ERR_DISCONNECT_DEVICE_FAILED;
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Failed to connnect the device");
        statusCode = FF_CM_ERR_CONNECT_DEVICE_FAILED;
    }

    // Check for status code and frame error messsage if any
    if (SUCCESS != statusCode || status.hasError() || SUCCESS != errorCode)
    {
        status.setError(statusCode, PM_ERR_CL_FF_CSL, CSL_ERR_SC_CM);
    }

    if(nullptr != pNmDir)
    {
        //deletes the NM object
        delete pNmDir;
        pNmDir = nullptr;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    retreives the current device class information
*/
PMStatus FFConnectionManager::getDeviceClass(int deviceAddress,
                                             UINT8& deviceClass)
{
    PMStatus status;
    int errorCode = SUCCESS;
    UINT16 index = 0;
    NETWORK_HANDLE networkHandle;
    int deviceSelect = DEVICE_SELECT;
    int networkCount;
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    T_NM_DIR*  pNmDir;
    RET_INFO retInfo{};
    DeviceInfo deviceInfo{};
    uint8_t pReadData{0};
    deviceClass = 0;
    int statusCode{0};

    envInfo.app_info = &appInfo;

    //memory allocates to NM objects
    pNmDir = new T_NM_DIR;

    //Establish device connection
    errorCode = ff_connect(&retInfo, deviceAddress, &deviceInfo);
    if (SUCCESS == errorCode)
    {
        //Gets the network count
        errorCode = get_nt_net_count(&networkCount);
        if (SUCCESS == errorCode)
        {
            // gets the network handle by passing device address
            errorCode = get_nt_net_handle_by_addr(networkCount,
                                                  deviceAddress, &networkHandle);
            if (SUCCESS == errorCode)
            {
                //Gets the NM object
                errorCode = get_nt_net_nm_dir(networkHandle, pNmDir);
                if (SUCCESS != errorCode || nullptr == pNmDir)
                {
                    LOGF_ERROR(CPMAPP,"Get Network Directory failed!");
                    statusCode = FF_CM_ERR_GET_NM_DIR_FAILED;
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP,"Get Network Handle failed!");
                statusCode = FF_CM_ERR_GET_NETWORK_HANDLE_FAILED;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Get Network Count failed!");
            statusCode = FF_CM_ERR_GET_NETWORK_COUNT_FAILED;
        }

        if (SUCCESS == statusCode)
        {
            /*Index of Boot Operational Class is Link Master Object Index + 5 */
            index = pNmDir->dlme_lm_obj + DEVICE_CLASS_INDEX;

            errorCode = cr_nma_read(&envInfo, networkHandle, index,
                                    BOOT_OPERAT_FUNC, deviceSelect);
            if (SUCCESS == errorCode)
            {
                errorCode = get_nmo_dev_dlme_lm_boot_op(networkHandle, &pReadData);
                if (SUCCESS == errorCode)
                {
                    deviceClass = pReadData;
                }
                else
                {
                     LOGF_ERROR(CPMAPP,"Get Device Class failed!");
                     statusCode = FF_CM_ERR_GET_DEVICE_CLASS_VALUE_FAILED;
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP,"Get Network Objects failed!");
                statusCode = FF_CM_ERR_READ_NM_OBJECTS_FAILED;
             }
        }

        // Disconnect the connected device
        errorCode = ff_disconnect(&retInfo, deviceAddress);       
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Failed to connect device!");
        statusCode = FF_CM_ERR_CONNECT_DEVICE_FAILED;
    }

    //Check status code and frame error message if any
    if(SUCCESS != statusCode)
    {
         status.setError(statusCode,
               PM_ERR_CL_FF_CSL,
               CSL_ERR_SC_CM);
    }

    if(nullptr != pNmDir)
    {
        //deletes NM objects
        delete pNmDir;
        pNmDir = nullptr;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Creates commission thread to perform device commissioning
*/
void FFConnectionManager::createCommissionThread(int address,int newAddress,
                          std::string newTag,
                          CommissionStatusCallBack commissionStatusCallBack)
{
    LOGF_DEBUG(CPMAPP,"createCommissionThread: "
                                  "creating commission thread");

    // creates the commissioning thread
    std::thread commission(&commissionDevice, address, newAddress, newTag,
                                       commissionStatusCallBack);


    // Detaches from the main thread to run independently
    commission.detach();
}

/*  ----------------------------------------------------------------------------
    Performs device commissioning by assigning the valid address and physical
    device tag to the field device
*/
void FFConnectionManager::commissionDevice(int address,int newAddress,
                                        std::string newTag,
                                        CommissionStatusCallBack notifyMessage)
{
    int errorCode = SUCCESS;
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    CR_SM_IDENTIFY_CNF smIdentifyCnf{'\0'};

    envInfo.app_info = &appInfo;

    //Clears the old device tag and assigns the new device tag
    //set PD Device Tag
    if (!newTag.empty())
    {
        // Retrieve SM identify tag with device address
        errorCode = cr_sm_ident(&envInfo,
                                static_cast<UINT16>(address),
                                &smIdentifyCnf);

        if (SUCCESS == errorCode )
        {
            notifyMessage(CLEAR_TAG);
            // Clear old tag
            cr_clear_tag(&envInfo,
                         static_cast<UINT16>(address),
                         &smIdentifyCnf);

            //Take the maximum length
            int length = newTag.length();
            if (length >  TAG_LEN)
            {
                length = TAG_LEN;
            }

            auto *newDeviceTagCopy = new char[TAG_SIZE];

            //assigns Empty space to string
            strncpy(newDeviceTagCopy, TAG_EMPTY, TAG_SIZE);
            string str = newDeviceTagCopy;

            //replaces string with position specified
            str.replace(POS,length, newTag);

            //assigns new tag to duplicate tag and PD tag
            strncpy(smIdentifyCnf.dup_pd_tag, str.c_str(), TAG_SIZE);
            strncpy(smIdentifyCnf.pd_tag, str.c_str() + POS, TAG_LEN);

            //free string
            delete[] newDeviceTagCopy;

            notifyMessage(SET_TAG);
            //assign new tag
            errorCode = cr_assign_tag(&envInfo,
                                      static_cast<UINT16>(address),
                                      &smIdentifyCnf);

            if (SUCCESS != errorCode)
            {
                LOGF_ERROR(CPMAPP,"commissionDevice: assign tag failed");
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"commissionDevice: set tag: SM identity failed");

        }

        if (SUCCESS != errorCode)
        {
            s_fldDevStatus.setError(FF_CM_ERR_SET_NEW_DEVICE_TAG_FAILED,
                                    PM_ERR_CL_FF_CSL, CSL_ERR_SC_CM);
        }
    }


    // Assigns the new device address
    if(!s_fldDevStatus.hasError())
    {
        errorCode = cr_sm_ident(&envInfo,
                                static_cast<UINT16>(address),
                                &smIdentifyCnf);

        if (SUCCESS == errorCode )
        {
            //Start commissioning device
            notifyMessage(SET_ADDRESS);
            // set new node address
            errorCode = cr_set_poll_addr(&envInfo, newAddress,
                                         smIdentifyCnf.dup_pd_tag);

            if (SUCCESS != errorCode)
            {
               LOGF_ERROR(CPMAPP,"commissionDevice: set address failed");
            }

        }
        else
        {
            LOGF_ERROR(CPMAPP,"commissionDevice: set address: SM identity failed");
        }

        if (SUCCESS != errorCode)
        {

            s_fldDevStatus.setError(FF_CM_ERR_SET_NEW_DEVICE_ADDRESS_FAILED,
                                    PM_ERR_CL_FF_CSL, CSL_ERR_SC_CM);
        }

    }

    s_isCompleted = true;
    releaseLock();
}

/*  ----------------------------------------------------------------------------
    Performs device decommissioning by clearing the address
*/
PMStatus FFConnectionManager::decommissionDevice(int address)
{
    APP_INFO appInfo{};
    ENV_INFO envInfo{};
    CR_SM_IDENTIFY_CNF smIdentifyCnf{'\0'};
    PMStatus status;
    int errorCode = SUCCESS;
    envInfo.app_info = &appInfo;

    errorCode =
            cr_sm_ident(&envInfo, static_cast<UINT16>(address), &smIdentifyCnf);

    if(errorCode == SUCCESS)
    {
        // Clear poll address
        errorCode =
                cr_clear_poll_addr(&envInfo, static_cast<UINT16>(address), &smIdentifyCnf);

        if(errorCode != SUCCESS)
        {
            LOGF_ERROR(CPMAPP,"decommissionDevice: "
                                          "clear address failed");
            status.setError(FF_CM_ERR_SM_CLEAR_ADDRESS_FAILED,
                        PM_ERR_CL_FF_CSL, CSL_ERR_SC_CM);
        }
        else
        {
            LOGF_INFO(FFConnectionManager,"Device Decommissioned Successfully!!");
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP,"decommissionDevice: "
                                      "SM identity failed");
        status.setError(FF_CM_ERR_SM_IDENTITY_FAILED,
                    PM_ERR_CL_FF_CSL, CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    It aquires the thread lock and wait till release
*/
void FFConnectionManager::aquireLock()
{
    // Waits until get the input from user
    std::unique_lock<std::mutex> lock(m_lock);
    m_conditionVariable.wait(lock, []
    {
        return FFConnectionManager::s_isCompleted;
    });
}

/*  ----------------------------------------------------------------------------
    It releases the locked thread and notify for further process
*/
void FFConnectionManager::releaseLock()
{
    // Unlocks the thread and notify that user's input arrived
    std::unique_lock<std::mutex> lock(m_lock);
    lock.unlock();
    m_conditionVariable.notify_one();
}

/*  ----------------------------------------------------------------------------
    Notify about CPM state change to linux thread
*/
int  FFConnectionManager::getConnectionStatus()
{
	if (CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_DISCONNECTION_IN_PROGRESS)
	{
		return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
}

/*  ----------------------------------------------------------------------------
    Notify disconnect and clean up
*/
int FFConnectionManager::notifyDisconnect(unsigned char node_number)
{
   int errorCode= SUCCESS;
   PMStatus status;

   IProtocolAdaptor* protocolAdaptor = CommonProtocolManager::getInstance()->getAdaptorInstance();
   FFAdaptor *adaptor = dynamic_cast<FFAdaptor*>(protocolAdaptor);
   status = adaptor->doCleanUP(node_number);
   if(status.hasError())
   {
	   errorCode = FAILURE;
   }
   return errorCode;
}

/*  ----------------------------------------------------------------------------
    Notify LAS node address
*/
void FFConnectionManager::notifyLASAddress(unsigned int activeLAS)
{
	   IProtocolAdaptor* protocolAdaptor = CommonProtocolManager::getInstance()->getAdaptorInstance();
	   if(protocolAdaptor != nullptr)
	   {
           FFAdaptor *adaptor = dynamic_cast<FFAdaptor*>(protocolAdaptor);
           if (adaptor != nullptr)
		   {
               FFConnectionManager *connInstance = adaptor->getConnMangerInstance();
			   if(connInstance != nullptr)
			   {
				   connInstance->setLASAddress(activeLAS);
			   }
			   else
			   {
				   LOGF_ERROR(CPMAPP,"FF connectionManager Instance is NULL \n");
			   }
		   }
		   else
		   {
			   LOGF_ERROR(CPMAPP,"FF Adaptor Instance is NULL \n");
		   }
	   }
	   else
	   {
		   LOGF_ERROR(CPMAPP,"ProtocolAdaptor Instance is NULL \n");
	   }
}

/*  ----------------------------------------------------------------------------
    Set the LAS node address
*/
void FFConnectionManager::setLASAddress(unsigned int activeLAS)
{
	m_lasNodeAddress = activeLAS;
}

/*  ----------------------------------------------------------------------------
    Get the LAS node address
*/
unsigned int FFConnectionManager::getLASAddress()
{
	return m_lasNodeAddress;
}
/*  ----------------------------------------------------------------------------
    Get the connected device Information
*/
const DeviceInfo & FFConnectionManager::getDeviceInfo()
{
    return *m_pDeviceInfo;
}

