/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PB Connection Manager class definitions
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <unistd.h>
#include <algorithm>
#include <string.h>

#include "CommonProtocolManager.h"
#include "PBAdaptor.h"
#include "PBConnectionManager.h"
#include "pb_softing_data.h"

#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    PBConnectionManager class instance initialization
*/
PBConnectionManager::PBConnectionManager()
    : m_pIFakController(nullptr), m_pPC(nullptr), m_commMgr(nullptr),
      m_slaveAddress(INITIAL_VALUE), m_slaveDevice(NULL)
{
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
PBConnectionManager::~PBConnectionManager()
{
    // Clear the member variables
    m_pIFakController = nullptr;
    m_pPC = nullptr;
    m_device.reset();

    if (nullptr != m_pDeviceInfo)
    {
        delete m_pDeviceInfo;
        m_pDeviceInfo = nullptr;
    }
    if (nullptr != m_commMgr)
    {
        delete m_commMgr;
        m_commMgr = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    This function is used to initialise all active tables w.r.to PB DD
*/
PMStatus PBConnectionManager::initialize(
    IFakController* ifak, IParameterCache* pc)
{
    PMStatus status;
    if (nullptr != ifak && nullptr != pc)
    {
        m_pIFakController = ifak;
        m_pPC = pc;
        m_device.reset();

        // Creates the communication manager's instance
        Interpreter* pInterpret = m_pIFakController->getIFakInterpreter();

        m_commMgr = new PBCommunicationManager(*pInterpret);
    }
    else
    {
        status.setError(
            PB_CM_ERR_INITIALIZE_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to initialise communication
*/
PMStatus PBConnectionManager::initStack(void* params, const char* ddlPath,
                                        const char* deviceFile)
{
    (void)params;
    (void)ddlPath;
    (void)deviceFile;
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to initialise communication
*/
PMStatus PBConnectionManager::initStack(void* params, void* settings, const char* ddlPath,
                                        const char* deviceFile)
{
    PMStatus status;
    int errorCode = SUCCESS;
    const char* pDdlPath = ddlPath;

    if (nullptr == pDdlPath)
    {
        // No environment variable has set
        LOGF_ERROR(CPMAPP,"Failure in getting DDL path\n");
        errorCode = FAILURE;
        status.setError(
            PB_CM_ERR_GET_DD_PATH_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }
    if (SUCCESS == errorCode)
    {
#ifdef DESKTOP
        // Get FBK2 PORT from environment file for desktop environment
        if (nullptr == deviceFile)
        {
            // No environment variable has set
            LOGF_ERROR(CPMAPP,"Failure in getting PORT\n");
            errorCode = FAILURE;
            status.setError(
                PB_CM_ERR_ACCESS_PORT_FAILED,
                PM_ERR_CL_PB_CSL,
                CSL_ERR_SC_CM);
        }
#endif
        if (SUCCESS == errorCode)
        {
            PB_BUS_PARAMS* busParams = static_cast<PB_BUS_PARAMS*>(params);
            if (nullptr == busParams)
            {
                // No bus parameter
                LOGF_ERROR(CPMAPP,"Failure in setting the BUS parameters\n");
                status.setError(
                    PB_CM_ERR_SET_BUS_PARAMETER_FAILED,
                    PM_ERR_CL_PB_CSL,
                    CSL_ERR_SC_CM);

                return status;
            }

            PB_APP_SETTINGS* pbSettings = static_cast<PB_APP_SETTINGS*>(settings);
            if (nullptr == pbSettings)
            {
                // No bus parameter
                LOGF_ERROR(CPMAPP,"Failure in the App settings\n");
                status.setError(
                    PB_CM_ERR_SET_APP_SETTING_FAILED,
                    PM_ERR_CL_PB_CSL,
                    CSL_ERR_SC_CM);

                return status;
            }

            io_driver::pb::bus_parameter_t busParamReq{};
            busParamReq.Baud_rate       = PB_BAUD_31_25_K;
            busParamReq.Tsl             = busParams->tsl;
            busParamReq.minTsdr         = busParams->minTsdr;
            busParamReq.maxTsdr         = busParams->maxTsdr;
            busParamReq.Tqui            = busParams->tqui;
            busParamReq.Tset            = busParams->tset;
            busParamReq.Ttr             = busParams->ttr;
            busParamReq.G               = busParams->g;
            busParamReq.HSA             = busParams->hsa;
            busParamReq.max_retry_limit = busParams->maxRetryLimit;

            busParamReq.FDL_Add = static_cast<unsigned char>(pbSettings->address);

            m_pbHostConfig.deviceChannel = DEFAULT_INITIAL;
            m_pbHostConfig.ackTimeout    = PB_STANDARD_TIMEOUT;
            m_pbHostConfig.readTimeout   = PB_STANDARD_TIMEOUT;
            strcpy(m_pbHostConfig.comPortName, deviceFile);
            strcpy(m_pbHostConfig.masterName, HOST_TAG);

            // Initialize the ProfiBus communication stack
            const unsigned char DEVICE  = DEFAULT_INITIAL;
            const unsigned char CHANNEL = DEFAULT_INITIAL;

            // Creates the device object
            m_device.reset(new io_driver::pb(DRIVER, DEVICE, CHANNEL, busParamReq));
            errorCode = m_device->set_mode(STOP);
            m_commMgr->m_device = m_device;

            if (SUCCESS == errorCode)
            {
                // Start the communication service
                errorCode = m_device->ioctl(PBSERVICE_REQ::START_PBSERVICE_REQ, NULL, NULL);
            }
        }
    }

    if (SUCCESS != errorCode && !status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Failure in initializing the PB backend\n");
        status.setError(
            PB_CM_ERR_INITIALIZE_PB_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to close the PB stack communication
*/
PMStatus PBConnectionManager::shutDownStack()
{
    PMStatus status;
    int errorCode = SUCCESS;

    if (SUCCESS == errorCode)
    {
        // Stop the communication service
        errorCode = m_device->ioctl(PBSERVICE_REQ::STOP_PBSERVICE_REQ, NULL, NULL);
    }

    m_device.reset();

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP,"Failure in uninitializing the PB backend\n");
        status.setError(
            PB_CM_ERR_UNINITIALIZE_PB_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to scan a device available in network
*/
PMStatus PBConnectionManager::scanDevices(void* envInfo, void* scanDevTable)
{
    (void)envInfo;
    PMStatus status;
    int errorCode = SUCCESS;

    io_driver::pb::livelist_info_t liveList;

    // Retreives the devices live list
    errorCode = m_device->livelist(liveList);

    if (SUCCESS == errorCode)
    {
        PB_SCAN_DEV_TBL* pbScanDevTable = static_cast<PB_SCAN_DEV_TBL*>(scanDevTable);
        if (nullptr != pbScanDevTable)
        {
            for (int i = DEFAULT_INITIAL; i < liveList.length; i++)
            {
                // List the passive state devices in the network
                if (PASSIVE == liveList.list[i].state)
                {
                    PB_SCAN_TBL entry{};
                    entry.stationAddress = liveList.list[i].addr;
                    pbScanDevTable->list.push_back(entry);
                }
            }
        }
    }

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP,"Failure in getting live device list\n");
        status.setError(
            PB_CM_ERR_SCAN_NETWORK_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Function to get the device information
*/
PMStatus PBConnectionManager::getDeviceInfo(int nodeAddress,
                                            DeviceInfo* deviceInfo)
{
    PMStatus status;
    long errorCode = SUCCESS;

    deviceInfo->node_address = nodeAddress;

    // Get connect into the device to read the device tag
    status = connectDevice(deviceInfo);
    if (!status.hasError())
    {
        // Get device info by reading the device parameter
        PBCommunicationManager::request request;

        // TODO(Bharath) : Have to check with more PROFIBUS devices
        // as slot number can be non zero
        request.slot  = DEFAULT_INITIAL;
        request.index = TAG_DESC_INDEX;
        request.data = new unsigned char[STANDARD_DATA_LEN];

        // Read data from device
        errorCode = m_commMgr->pb_read(request);
        if (SUCCESS == errorCode)
        {
            memcpy(deviceInfo->pd_tag, request.data, request.datalen);
        }
        if (nullptr != request.data)
        {
            delete[] request.data;
            request.data = nullptr;
        }

        // TODO(Bharath) : Have to check with more PROFIBUS devices
        // as slot number can be non zero
        request.slot  = DEFAULT_INITIAL;
        request.index = DEVICE_ID_INDEX;
        request.data = new unsigned char[STANDARD_DATA_LEN];

        // Read data from device
        errorCode = m_commMgr->pb_read(request);
        if (SUCCESS == errorCode)
        {
            memcpy(deviceInfo->dev_id, request.data, request.datalen);
        }
        if (nullptr != request.data)
        {
            delete[] request.data;
            request.data = nullptr;
        }

        // Disconnects from the device after read the device tag
        status = disconnectDevice();
    }

    if (SUCCESS != errorCode && !status.hasError())
    {
        status.setError(
                    PB_CM_ERR_GET_DEVICE_INFO_FAILED,
                    PM_ERR_CL_PB_CSL,
                    CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function is used to connect device with help of device info
*/
PMStatus PBConnectionManager::connectDevice(DeviceInfo* deviceInfo)
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Connects to the device
    m_slaveDevice = static_cast<void*>(&deviceInfo->node_address);
    errorCode = m_device->connect(deviceInfo->node_address, MAX_CONN_TIMEOUT, m_slaveDevice);

    if (SUCCESS == errorCode)
    {
        m_slaveAddress = deviceInfo->node_address;
        m_commMgr->setConnectionState(device_io::connected);
        m_commMgr->setSlaveDevice(m_slaveDevice);
    }

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP,"Failure in Connecting to device\n");
        status.setError(
            PB_CM_ERR_CONNECT_DEVICE_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to disconnect currently connected device
*/
PMStatus PBConnectionManager::disconnectDevice()
{
    PMStatus status;
    int errorCode = SUCCESS;

    // Disconnects from the device
    errorCode = m_device->disconnect(m_slaveDevice);

    if (SUCCESS == errorCode)
    {
        m_commMgr->setConnectionState(device_io::disconnected);
        m_slaveAddress = INITIAL_VALUE;
    }

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP,"Failure in Disonnecting from device\n");
        status.setError(
            PB_CM_ERR_DISCONNECT_DEVICE_FAILED,
            PM_ERR_CL_PB_CSL,
            CSL_ERR_SC_CM);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    This function used to get PB communication manager instance
*/
PBCommunicationManager* PBConnectionManager::getCommunicationManager(void)
{
    return m_commMgr;
}
