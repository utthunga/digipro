### This code base is part of the ProStar project ###
The home page for this project is located on the
[ProStar Project Wiki](https://flukept.atlassian.net/wiki/display/DP/ProStar+Design).

### ProStar Build Process ###

build.sh is the build script file which prepares make files from CMakeLists.txt and triggers the build. 

### ARCH_TYPE : Architecture type ###
* __x86 | arm__ : specifies the target build architecture.

	x86 is considered as default architecture type for ProStar

### How to build the sources ###
The following command starts the ProStar Build process.

Make sure that you have copied environment-setup-cortexa8hf-neon-oe-linux-gnueabi file before building for arm platform

     bash build.sh <x86|arm> -repo <build|find>> <rel|deb> <app|ut_app> [all] [clean]
    
    * TARGET_BUILD_TYPE --> <rel|deb> - Build type option for compiling prostar application.
    
    *  TARGET_APP --> <app|ut_app> - Specifies the target application to be built.
                    - app option specifies to build only communicator and clr application for ProStar.
                    - ut_app, This option specifies to build unit test applcation for ProStar.
     
     
    * all   This option specifies to build both communicator and unit test application for ProStar.
    
    * clean - cleans build directory.
     
    * -repo <build|find> -  Signifies where to get starsky tar ball that is required for building prostar application.
                         -  build, This option downloads and builds the libraries independent of Starsky / Jenkins.
                         -  find, This option finds dependencies that have already been downloaded or built.

    * -geneclipse   - Generate necessary files for this project that is required to be used within eclipse application. Usage: "bash build.sh -geneclipse".
    * -nocov        - Builds without code coverage information"

Example:

    *  bash build.sh x86 -repo build all     - Builds both communicator and unit test application in release & debug mode for x86 
    *  bash build.sh x86 -repo build rel all - Builds both communicator and unit test application in release mode for x86 
    *  bash build.sh x86 -repo build deb all - Builds both communicator and unit test application in debug mode for x86 
    *  bash build.sh arm -repo find all      - Builds both communicator and unit test application in release & debug mode for arm 
    *  bash build.sh arm -repo find rel all  - Builds both communicator and unit test application in release mode for arm 
    *  bash build.sh arm -repo find deb all  - Builds both communicator and unit test application in debug mode for arm

build directory consusts of following directories and files:

	* cmake_<x86|arm>_<rel|deb> : This directory contains the generates cmake files.
	
	* x86 : Directory structure is as follows
	
	x86
	└── release <TARGET_BUILD_TYPE>
		├── bin	<contains the executables>
		│   ├── clr
		│   └── communicator
		└── lib	<contains generated libraries>
			├── libcoreCpm.a
			├── libcsl.a
			├── ...
			├── ...
			├── libffh1stack.a


### How to run applications ###

The ProStar project generates two executables as part of build.

communicator - ProStar application
    
clr - Command line renderer application.
	
	execute the following command from directory build/<ARCH_TYPE>/<TARGET_BUILD_TYPE>/bin
	./communicator : Executes ProStar application.
	./clr : Executes command line renderer.
		
### How to run tests ###

The ProStar project using the googletest unit test framework. 
execute the following command from directory build/<ARCH_TYPE>/<TARGET_BUILD_TYPE>/bin

    ./ut_communicator : Executes Unittest application
	
### How to Automation Test Framework ###
The ProStar project uses robot framework to execute automated test framework.
More information on Automation Test Framework is available at (https://flukept.atlassian.net/wiki/display/DP/Automation+Test+Framework)
	
### How to run clang ###

The ProStar project uses clang as static code analyser
execute the following command from directory build/cmake_<TARGET_BUILD_TYPE>

    make clang-tidy 
	
### How to generate doxygen documents ###

The ProStar project uses doxygen tool to generate documents
Execute the following command from directory build/cmake_<TARGET_BUILD_TYPE>, documents are generated at directory: build/cmake_<TARGET_BUILD_TYPE>/docs/html

    make docs
	
### Who do I talk to? ###

* Fluke IDC Team Members:
    * [vinay.hg@fluke.com](mailto:vinay.hg@fluke.com)
    * [santhosh.selvaraj@fluke.com](mailto:santhosh.selvaraj@fluke.com)
    * [aravind.nagaraj@fluke.com](mailto:aravind.nagaraj@fluke.com)
    * [adesh.soni@fluke.com](mailto:adesh.soni@fluke.com)
	
* Utthunga Team Members:
    * [shilpa.n@utthunga.com](mailto:shilpa.n@utthunga.com)
    * [sivansethu.g@utthunga.com](mailto:sivansethu.g@utthunga.com)
    * [bharath.sr@utthunga.com](mailto:bharath.sr@utthunga.com)
    * [bhuvaneswari.t@utthunga.com](mailto:bhuvaneswari.t@utthunga.com)
    * [sadanand.s@utthunga.com](mailto:sadanand.s@utthunga.com)
